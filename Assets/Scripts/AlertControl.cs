using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

using Boomlagoon.JSON;
using VoxelBusters.Utility;
using VoxelBusters.NativePlugins;

public class AlertControl : MonoBehaviour  {
	DateTime amTimerStart;
	DateTime amTimerEnd;
	DateTime pmTimerStart;
	DateTime pmTimerEnd;

	DateTime lastCheckTime;
	DateTime nextCheckTime;
	DateTime nextEndTime;

	public Image timeFiller;

	public Text lastCheck;
	public Text lastCheckDate;
	public Text nextAlarmTime;
	public Text nextAlarmDate;
	public Text currentTime;
	public Text currentDate;

    public Text alertCount;

	public GameObject button_checkin;
	public GameObject button_skip;
	public GameObject panel_OK;

	private string url_dev = "http://54.169.192.154/client/watcher/development";

	void Start () {
        NPBinding.NotificationService.RegisterNotificationTypes(NotificationType.Alert | NotificationType.Badge | NotificationType.Sound);

        nextCheckTime = DateTime.Parse (PlayerPrefs.GetString("nextalert"));
		nextEndTime = nextCheckTime.AddMinutes (60);
	}

	// Update is called once per frame
	void Update () {
		currentTime.text = DateTime.Now.ToString ("HH:mm:ss");
		currentDate.text = DateTime.Now.ToString ("dd-MMM-yyyy");

		UpdateTimeDisplay ();
		ShowButton ();
	}

	public void CheckIn()
	{
		StartCoroutine (CheckInServer(() => {
			PlayerPrefs.SetString ("last_check", DateTime.Now.ToString());

			DateTime _dam = DateTime.Parse (PlayerPrefs.GetString("amalert"));
			DateTime _dpm = DateTime.Parse (PlayerPrefs.GetString("pmalert"));

			Debug.Log (string.Format("AlertControl.CheckIn: nextCheckTime: {0:HH:mm:ss tt}; AM: {1:HH:mm:ss tt}; PM: {2:HH:mm:ss tt}", nextCheckTime, _dam, _dpm));

			if(nextCheckTime.ToString("HH:mm tt").Equals(_dam.ToString("HH:mm tt"))) {
				RenewAMTimer();
				nextCheckTime = DateTime.Parse (PlayerPrefs.GetString ("pmalert"));
				nextCheckTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, nextCheckTime.Hour, nextCheckTime.Minute, nextCheckTime.Second);
				nextEndTime = nextCheckTime.AddMinutes(60);
				PlayerPrefs.SetString("pmalert", nextCheckTime.ToString("HH:mm"));
			}

			if (nextCheckTime.ToString("HH:mm tt").Equals (_dpm.ToString("HH:mm tt"))) {
				RenewPMTimer();
				nextCheckTime = DateTime.Parse (PlayerPrefs.GetString ("amalert"));
				nextCheckTime = nextCheckTime.AddDays(1);
				nextEndTime = nextCheckTime.AddMinutes(60);
				PlayerPrefs.SetString("amalert", nextCheckTime.ToString("HH:mm"));
			}

			Debug.Log (string.Format("AlertControl.CheckIn: nextCheckTime={0}", nextCheckTime));
			PlayerPrefs.SetString("nextalert", nextCheckTime.ToString());
			
			button_checkin.SetActive (false);
			panel_OK.SetActive (true);
		}));
	}

	public void SkipCheck()
	{
		Debug.Log ("Skipping NEXT Check-in");
		// Check in server first...
		StartCoroutine (CheckInServer(() => {
			//PlayerPrefs.SetString ("last_check", DateTime.Now.ToString());

			DateTime _dam = DateTime.Parse (PlayerPrefs.GetString("amalert"));
			DateTime _dpm = DateTime.Parse (PlayerPrefs.GetString("pmalert"));

			Debug.Log (string.Format("[00][01] nextCheckTime: {0:HH:mm:ss tt}; AM: {1:HH:mm:ss tt}; PM: {2:HH:mm:ss tt}", nextCheckTime, _dam, _dpm));
			//if(nextCheckTime.Equals(DateTime.Parse(PlayerPrefs.GetString ("amalert")))) {
			if(nextCheckTime.ToString("HH:mm tt").Equals(_dam.ToString("HH:mm tt"))) {
				Debug.Log("Renew AM");
				RenewAMTimer();
				//nextCheckTime = DateTime.Parse (PlayerPrefs.GetString ("pmalert"));
				nextCheckTime = SetNextAlert(DateTime.Parse (PlayerPrefs.GetString ("pmalert")));
				nextEndTime = nextCheckTime.AddMinutes(60);
				PlayerPrefs.SetString("pmalert", nextCheckTime.ToString("HH:mm"));
			} else {
			
			//if (nextCheckTime.Equals (DateTime.Parse (PlayerPrefs.GetString ("pmalert")))) {
			//if (nextCheckTime.ToString("HH:mm tt").Equals (_dpm.ToString("HH:mm tt"))) {
				Debug.Log("Renew PM");
				RenewPMTimer();
				//nextCheckTime = DateTime.Parse (PlayerPrefs.GetString ("amalert"));
				nextCheckTime = SetNextAlert(DateTime.Parse (PlayerPrefs.GetString ("amalert")));
				nextEndTime = nextCheckTime.AddMinutes(60);
				PlayerPrefs.SetString("amalert", nextCheckTime.ToString("HH:mm"));
			}

			Debug.Log (string.Format("Next Check-in time skipped to: {0}", nextCheckTime));
			PlayerPrefs.SetString("nextalert", nextCheckTime.ToString());
		}));
	}

	#region PRIVATE_CALLS
	private void UpdateTimeDisplay (){
        // Debug.Log(string.Format("Compare(Now[{1}], nextCheckTime[{0}])={2}", DateTime.Parse(nextCheckTime.ToString("HH:mm")), DateTime.Now, DateTime.Compare(DateTime.Now, nextCheckTime)));

        bool _isNextDay = false;

        // Time.now has passed the check in time's end time
        if(DateTime.Compare(DateTime.Now, nextEndTime) > 0)
        {
            // Time.now is AM
            if(DateTime.Now.ToString("tt").Equals("AM"))
            {
                // Time.now is still before scheduled AM alert
                if (DateTime.Compare(DateTime.Now, DateTime.Parse(PlayerPrefs.GetString("amalert"))) < 0)
                {
                    Debug.Log("AM_01");
                    PlayerPrefs.SetString("nextalert", PlayerPrefs.GetString("amalert"));
                }
                else
                {
                    Debug.Log("AM_02");
                    PlayerPrefs.SetString("nextalert", PlayerPrefs.GetString("pmalert"));
                }
            }
            else
            {
                // Time.now is still before scheduled PM alert
                if (DateTime.Compare(DateTime.Now, DateTime.Parse(PlayerPrefs.GetString("pmalert"))) < 0)
                {
                    Debug.Log("PM_01");
                    PlayerPrefs.SetString("nextalert", PlayerPrefs.GetString("pmalert"));
                }
                else
                {
                    Debug.Log("PM_02");
                    PlayerPrefs.SetString("nextalert", PlayerPrefs.GetString("amalert"));
                    _isNextDay = true;
                }
            }
        }

		DateTime _nd = DateTime.Parse (PlayerPrefs.GetString("nextalert"));
        if (_isNextDay)
            _nd = _nd.AddDays(1);

		nextAlarmTime.text = _nd.ToString ("HH:mm");
		nextAlarmDate.text = _nd.ToString ("dd-MMM-yyyy");

	}

	private DateTime SetNextAlert(DateTime _time)
	{
		DateTime _n = new DateTime (DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, _time.Hour, _time.Minute, _time.Second);
		_n = _n.AddDays (1);

		return _n;
	}

    private void SetAlertCount(int _count)
    {
        //Debug.Log(string.Format("AlertControl.SetAlertCount: Count={0}", _count));
        alertCount.text = _count.ToString();
    }

	private void ShowButton()
	{
        // Hide the button if the Start time not reached yet
        //Debug.Log (string.Format("Compare(Now[{1}], nextCheckTime[{0}]) = {2}", nextCheckTime, DateTime.Now, DateTime.Compare (DateTime.Now, nextCheckTime)));
        if (DateTime.Compare (DateTime.Now, nextCheckTime) < 0) {
			button_checkin.SetActive(false);
            return;
		}

        // Hide the button if the End time already passed
        //Debug.Log (string.Format("Compare(nextEndTime[{0}], Now[{1}]) = {2}", nextEndTime, DateTime.Now, DateTime.Compare (nextEndTime, DateTime.Now)));
        if (DateTime.Compare (nextEndTime, DateTime.Now) < 0) {
			button_checkin.SetActive (false);

            // Update the next check in time
            DateTime _dam = DateTime.Parse(PlayerPrefs.GetString("amalert"));
            DateTime _dpm = DateTime.Parse(PlayerPrefs.GetString("pmalert"));

            //if(nextCheckTime.Equals(DateTime.Parse(PlayerPrefs.GetString ("amalert")))) {
            if (nextCheckTime.ToString("HH:mm tt").Equals(_dam.ToString("HH:mm tt")))
            {
                RenewAMTimer();
                nextCheckTime = DateTime.Parse(PlayerPrefs.GetString("pmalert"));
                nextCheckTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, nextCheckTime.Hour, nextCheckTime.Minute, nextCheckTime.Second);
                nextEndTime = nextCheckTime.AddMinutes(60);
                PlayerPrefs.SetString("pmalert", nextCheckTime.ToString("HH:mm"));
            }

            //if (nextCheckTime.Equals (DateTime.Parse (PlayerPrefs.GetString ("pmalert")))) {
            if (nextCheckTime.ToString("HH:mm tt").Equals(_dpm.ToString("HH:mm tt")))
            {
                RenewPMTimer();
                nextCheckTime = DateTime.Parse(PlayerPrefs.GetString("amalert"));
                nextCheckTime = nextCheckTime.AddDays(1);
                nextEndTime = nextCheckTime.AddMinutes(60);
                PlayerPrefs.SetString("amalert", nextCheckTime.ToString("HH:mm"));
            }

            PlayerPrefs.SetString("nextalert", nextCheckTime.ToString());
            return;
		}

        SetAlertCount(0);

        button_checkin.SetActive (true);

        // Count the alert possibly missed
        TimeSpan _ts = DateTime.Now - nextCheckTime;
        SetAlertCount(Mathf.RoundToInt((float)_ts.TotalMinutes/15));
    }

    private void ShowAlertDialogWithSingleButton()
    {
        NPBinding.UI.ShowAlertDialogWithSingleButton("報到時間", "請登入 Watcher 並報到！", "確認", (string _buttonPressed) => {

        });
    }
    #endregion PRIVATE_CALLS

    #region NOTIFICATION_OPERTAIONS
    private void RenewAMTimer()
	{
        SetAlertCount(0);

        // Cancel all OLD AM scheduled notifications
        string _amIds = PlayerPrefs.GetString ("amIds");
		string[] _amIdList = _amIds.Split ("," [0]);
		foreach (string _id in _amIdList)
			CancelLocalNotification (_id);

        //string _amStr = string.Format ("{0}/{1}/{2} {3}:{4} AM", DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Year, amTimerStart.Hour.ToString(), amTimerStart.Minute.ToString());
        string _amStr = string.Format("{0}/{1}/{2} {3}:{4} AM", DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Year, nextCheckTime.Hour.ToString(), nextCheckTime.Minute.ToString());
        DateTime _amTime = DateTime.Parse (_amStr);
		_amTime = _amTime.AddDays (1);
		PlayerPrefs.SetString ("amalert", _amTime.ToString("HH:mm"));
		Debug.Log (string.Format("AM Timer renew as {0}", _amTime));
		
		_amIds = "";
		// Register AM alert
		_amIds += ScheduleLocalNotification(CreateNotification(_amTime, eNotificationRepeatInterval.DAY));
		// Register AM 2nd alert
		_amIds += "," + ScheduleLocalNotification(CreateNotification(_amTime.AddMinutes(15), eNotificationRepeatInterval.DAY));
		// Register AM 3rd alert
		_amIds += "," + ScheduleLocalNotification(CreateNotification(_amTime.AddMinutes(30), eNotificationRepeatInterval.DAY));
		// Register AM 4th alert
		_amIds += "," + ScheduleLocalNotification(CreateNotification(_amTime.AddMinutes(45), eNotificationRepeatInterval.DAY));
		// Register AM 5th alert
		_amIds += "," + ScheduleLocalNotification(CreateNotification(_amTime.AddMinutes(60), eNotificationRepeatInterval.DAY));
		
		//Debug.Log(string.Format("All AM Ids = {0}", _amIds));
		PlayerPrefs.SetString ("amIds", _amIds);
	}
	
	private void RenewPMTimer()
	{
		// Cancel all OLD PM scheduled notifications
		string _pmIds = PlayerPrefs.GetString ("pmIds");
		string[] _pmIdList = _pmIds.Split ("," [0]);
		foreach (string _id in _pmIdList)
			CancelLocalNotification (_id);
		
		string _pmStr = string.Format ("{0}/{1}/{2} {3}:{4} PM", DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Year, nextCheckTime.Hour.ToString(), nextCheckTime.Minute.ToString());
		DateTime _pmTime = DateTime.Parse (_pmStr);
		Debug.Log (string.Format("PM Timer older as {0}", _pmTime));
		_pmTime = _pmTime.AddDays (1);
		Debug.Log (string.Format("PM Timer renew as {0}", _pmTime));
		PlayerPrefs.SetString ("pmalert", _pmTime.ToString("HH:mm"));
		
		_pmIds = "";
		// Register AM alert
		_pmIds += ScheduleLocalNotification(CreateNotification(_pmTime, eNotificationRepeatInterval.DAY));
		// Register AM 2nd alert
		_pmIds += "," + ScheduleLocalNotification(CreateNotification(_pmTime.AddMinutes(15), eNotificationRepeatInterval.DAY));
		// Register AM 3rd alert
		_pmIds += "," + ScheduleLocalNotification(CreateNotification(_pmTime.AddMinutes(30), eNotificationRepeatInterval.DAY));
		// Register AM 4th alert
		_pmIds += "," + ScheduleLocalNotification(CreateNotification(_pmTime.AddMinutes(45), eNotificationRepeatInterval.DAY));
		// Register AM 5th alert
		_pmIds += "," + ScheduleLocalNotification(CreateNotification(_pmTime.AddMinutes(60), eNotificationRepeatInterval.DAY));
		
		//Debug.Log(string.Format("All PM Ids = {0}", _pmIds));
		PlayerPrefs.SetString ("pmIds", _pmIds);
	}
	
	private void CancelLocalNotification (string _notificationID)
	{
		NPBinding.NotificationService.CancelLocalNotification(_notificationID);
	}
	
	private string ScheduleLocalNotification (CrossPlatformNotification _notification)
	{
		return NPBinding.NotificationService.ScheduleLocalNotification(_notification);
	}
	
	private CrossPlatformNotification CreateNotification (System.DateTime _time, eNotificationRepeatInterval _repeatInterval)
	{
		// User info
		IDictionary _userInfo			= new Dictionary<string, string>();
		_userInfo["data"]				= "custom data";
		
		CrossPlatformNotification.iOSSpecificProperties _iosProperties			= new CrossPlatformNotification.iOSSpecificProperties();
		_iosProperties.HasAction		= true;
		_iosProperties.AlertAction		= "alert action";
		
		CrossPlatformNotification.AndroidSpecificProperties _androidProperties	= new CrossPlatformNotification.AndroidSpecificProperties();
		_androidProperties.ContentTitle	= "Watcher 報到";
		_androidProperties.TickerText	= "請登入 Watcher 報到!";
        //_androidProperties.LargeIcon	= "NativePlugins.png"; //Keep the files in Assets/PluginResources/Android or Common folder.
        _androidProperties.LargeIcon = "icon.png"; //Keep the files in Assets/PluginResources/Android or Common folder.

        CrossPlatformNotification _notification	= new CrossPlatformNotification();
		_notification.AlertBody			= "alert body"; //On Android, this is considered as ContentText
		_notification.FireDate = _time;
		_notification.RepeatInterval	= _repeatInterval;
		_notification.SoundName			= "Notification.mp3"; //Keep the files in Assets/PluginResources/Android or iOS or Common folder.
		_notification.UserInfo			= _userInfo;
		_notification.iOSProperties		= _iosProperties;
		_notification.AndroidProperties	= _androidProperties;
		
		//Debug.Log (string.Format ("Notification[{0}] scheduled! Trigger time set as {1}", _notification.GetNotificationID(), _notification.FireDate));

		return _notification;
	}	
	#endregion NOTIFICATION_OPERATIONS

	#region NAV
	public void GoToSetting()
	{
		Application.LoadLevel ("Setting");
	}
	#endregion NAV

	#region SERVER_CALL
	IEnumerator CheckInServer(Action _action)
	{
		WWWForm _f = new WWWForm();
		_f.AddField ("user_id", PlayerPrefs.GetString ("user_id"));

		WWW _w;
		_w = new WWW (string.Format("{0}/app/user/checkin", url_dev), _f);
		
		float _elapse = 0f;
		float _time = Time.time;
		float _timeout = 15f;
		
		while (_elapse < _timeout) {
			if(_w.isDone)
				break;
			
			yield return new WaitForSeconds(0.1f);
			_elapse += Time.time - _time;
			_time = Time.time;
		}
		
		if (!_w.isDone) {
			Debug.Log("Connection timed out.");
			yield break;
		}
		
		if (!string.IsNullOrEmpty (_w.error) || string.IsNullOrEmpty(_w.text)) {
			Debug.Log(string.Format("Connection Error: {0}", _w.error));
			yield break;
		}
		
		//Debug.Log (string.Format("Result={0}", _w.text));
		
		JSONObject _json = JSONObject.Parse (_w.text);
		if (_json.GetNumber ("result_code").Equals (0) && _json.GetBoolean("result")) {
			//Debug.Log("Checked in!!");
			_action();
		}
	}

	#endregion SERVER_CALL

	#region PROTECTED
	void OnEnable ()
	{
		// Register for callbacks
		//NotificationService.DidReceiveLocalNotificationEvent 			+= DidReceiveLocalNotificationEvent;
	}
	
	void OnDisable ()
	{
		// Un-Register from callbacks
		// NotificationService.DidReceiveLocalNotificationEvent 			-= DidReceiveLocalNotificationEvent;
	}

	void DidReceiveLocalNotificationEvent (CrossPlatformNotification _notification)
	{
        // When a notification is received, shows an alert dialog
        ShowAlertDialogWithSingleButton();
	}
	#endregion PROTECTED
}
