﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using Boomlagoon.JSON;

public class LoginControl : MonoBehaviour
{
    private string url_dev = "http://54.169.192.154/client/watcher/development";

    public InputField phone;
    public InputField password;

    public GameObject dialog_error;
    public Text error_msg;

    void Start()
    {
        if (!string.IsNullOrEmpty(PlayerPrefs.GetString("phone")) && !string.IsNullOrEmpty(PlayerPrefs.GetString("password")))
        {
            if (string.IsNullOrEmpty(PlayerPrefs.GetString("last_check")))
                PlayerPrefs.SetString("last_check", DateTime.Now.ToString());

            phone.text = PlayerPrefs.GetString("phone");
            password.text = PlayerPrefs.GetString("password");

            Login();
        }
    }

    public void Login()
    {
        StartCoroutine(LoginRoute(phone.text, password.text));
    }

    #region LOGIN_ROUTINE
    IEnumerator LoginRoute(string _phone, string _password)
    {
        WWWForm _f = new WWWForm();
        _f.AddField("user_phone", _phone);
        _f.AddField("user_password", _password);

        WWW _w;
        _w = new WWW(string.Format("{0}/app/user/login", url_dev), _f);

        float _elapse = 0f;
        float _time = Time.time;
        float _timeout = 15f;

        while (_elapse < _timeout)
        {
            if (_w.isDone)
                break;

            yield return new WaitForSeconds(0.1f);
            _elapse += Time.time - _time;
            _time = Time.time;
        }

        if (!_w.isDone)
        {
            DisplayError("Connection timed out.");
            yield break;
        }

        if (!string.IsNullOrEmpty(_w.error) || string.IsNullOrEmpty(_w.text))
        {
            DisplayError(string.Format("Connection Error: {0}", _w.error));
            yield break;
        }

        JSONObject _json = JSONObject.Parse(_w.text);
        if (_json.GetNumber("result_code").Equals(0) && _json.GetBoolean("result"))
        {
            PlayerPrefs.SetInt("settimer", 1);
            PlayerPrefs.SetString("user_id", _json.GetObject("user").GetString("user_id"));
            PlayerPrefs.SetString("phone", _phone);
            PlayerPrefs.SetString("password", _password);

            if (string.IsNullOrEmpty(PlayerPrefs.GetString("last_check")))
                PlayerPrefs.SetString("last_check", DateTime.Now.ToString());

            if (string.IsNullOrEmpty(PlayerPrefs.GetString("amalert")) || string.IsNullOrEmpty(PlayerPrefs.GetString("pmalert")))
                Application.LoadLevel("Setting");
            else
                Application.LoadLevel("Alarm");
        }
        else
        {
            DisplayError(_json.GetString("message"));
        }
    }


    #endregion LOGIN_ROUTINE

    #region ERROR_LOGGING
    private void DisplayError(string _msg)
    {
        dialog_error.SetActive(true);
        error_msg.text = _msg;
    }
    #endregion ERROR_LOGGING
}
