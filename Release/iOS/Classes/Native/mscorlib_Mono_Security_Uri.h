﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1_238;
// Mono.Security.Uri/UriScheme[]
struct UriSchemeU5BU5D_t1_239;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t1_92;

#include "mscorlib_System_Object.h"

// Mono.Security.Uri
struct  Uri_t1_237  : public Object_t
{
	// System.Boolean Mono.Security.Uri::isUnixFilePath
	bool ___isUnixFilePath_0;
	// System.String Mono.Security.Uri::source
	String_t* ___source_1;
	// System.String Mono.Security.Uri::scheme
	String_t* ___scheme_2;
	// System.String Mono.Security.Uri::host
	String_t* ___host_3;
	// System.Int32 Mono.Security.Uri::port
	int32_t ___port_4;
	// System.String Mono.Security.Uri::path
	String_t* ___path_5;
	// System.String Mono.Security.Uri::query
	String_t* ___query_6;
	// System.String Mono.Security.Uri::fragment
	String_t* ___fragment_7;
	// System.String Mono.Security.Uri::userinfo
	String_t* ___userinfo_8;
	// System.Boolean Mono.Security.Uri::isUnc
	bool ___isUnc_9;
	// System.Boolean Mono.Security.Uri::isOpaquePart
	bool ___isOpaquePart_10;
	// System.String[] Mono.Security.Uri::segments
	StringU5BU5D_t1_238* ___segments_11;
	// System.Boolean Mono.Security.Uri::userEscaped
	bool ___userEscaped_12;
	// System.String Mono.Security.Uri::cachedAbsoluteUri
	String_t* ___cachedAbsoluteUri_13;
	// System.String Mono.Security.Uri::cachedToString
	String_t* ___cachedToString_14;
	// System.String Mono.Security.Uri::cachedLocalPath
	String_t* ___cachedLocalPath_15;
	// System.Int32 Mono.Security.Uri::cachedHashCode
	int32_t ___cachedHashCode_16;
	// System.Boolean Mono.Security.Uri::reduce
	bool ___reduce_17;
};
struct Uri_t1_237_StaticFields{
	// System.String Mono.Security.Uri::hexUpperChars
	String_t* ___hexUpperChars_18;
	// System.String Mono.Security.Uri::SchemeDelimiter
	String_t* ___SchemeDelimiter_19;
	// System.String Mono.Security.Uri::UriSchemeFile
	String_t* ___UriSchemeFile_20;
	// System.String Mono.Security.Uri::UriSchemeFtp
	String_t* ___UriSchemeFtp_21;
	// System.String Mono.Security.Uri::UriSchemeGopher
	String_t* ___UriSchemeGopher_22;
	// System.String Mono.Security.Uri::UriSchemeHttp
	String_t* ___UriSchemeHttp_23;
	// System.String Mono.Security.Uri::UriSchemeHttps
	String_t* ___UriSchemeHttps_24;
	// System.String Mono.Security.Uri::UriSchemeMailto
	String_t* ___UriSchemeMailto_25;
	// System.String Mono.Security.Uri::UriSchemeNews
	String_t* ___UriSchemeNews_26;
	// System.String Mono.Security.Uri::UriSchemeNntp
	String_t* ___UriSchemeNntp_27;
	// Mono.Security.Uri/UriScheme[] Mono.Security.Uri::schemes
	UriSchemeU5BU5D_t1_239* ___schemes_28;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Security.Uri::<>f__switch$map17
	Dictionary_2_t1_92 * ___U3CU3Ef__switchU24map17_29;
};
