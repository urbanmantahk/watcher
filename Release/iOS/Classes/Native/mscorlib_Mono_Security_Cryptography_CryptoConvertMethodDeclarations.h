﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.Cryptography.CryptoConvert
struct CryptoConvert_t1_153;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Security.Cryptography.RSA
struct RSA_t1_175;
// System.Security.Cryptography.DSA
struct DSA_t1_160;
// System.Security.Cryptography.AsymmetricAlgorithm
struct AsymmetricAlgorithm_t1_228;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Security.Cryptography.CryptoConvert::.ctor()
extern "C" void CryptoConvert__ctor_m1_1886 (CryptoConvert_t1_153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Security.Cryptography.CryptoConvert::ToInt32LE(System.Byte[],System.Int32)
extern "C" int32_t CryptoConvert_ToInt32LE_m1_1887 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___bytes, int32_t ___offset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Mono.Security.Cryptography.CryptoConvert::ToUInt32LE(System.Byte[],System.Int32)
extern "C" uint32_t CryptoConvert_ToUInt32LE_m1_1888 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___bytes, int32_t ___offset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.CryptoConvert::GetBytesLE(System.Int32)
extern "C" ByteU5BU5D_t1_109* CryptoConvert_GetBytesLE_m1_1889 (Object_t * __this /* static, unused */, int32_t ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.CryptoConvert::Trim(System.Byte[])
extern "C" ByteU5BU5D_t1_109* CryptoConvert_Trim_m1_1890 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___array, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.RSA Mono.Security.Cryptography.CryptoConvert::FromCapiPrivateKeyBlob(System.Byte[])
extern "C" RSA_t1_175 * CryptoConvert_FromCapiPrivateKeyBlob_m1_1891 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___blob, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.RSA Mono.Security.Cryptography.CryptoConvert::FromCapiPrivateKeyBlob(System.Byte[],System.Int32)
extern "C" RSA_t1_175 * CryptoConvert_FromCapiPrivateKeyBlob_m1_1892 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___blob, int32_t ___offset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.DSA Mono.Security.Cryptography.CryptoConvert::FromCapiPrivateKeyBlobDSA(System.Byte[])
extern "C" DSA_t1_160 * CryptoConvert_FromCapiPrivateKeyBlobDSA_m1_1893 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___blob, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.DSA Mono.Security.Cryptography.CryptoConvert::FromCapiPrivateKeyBlobDSA(System.Byte[],System.Int32)
extern "C" DSA_t1_160 * CryptoConvert_FromCapiPrivateKeyBlobDSA_m1_1894 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___blob, int32_t ___offset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.CryptoConvert::ToCapiPrivateKeyBlob(System.Security.Cryptography.RSA)
extern "C" ByteU5BU5D_t1_109* CryptoConvert_ToCapiPrivateKeyBlob_m1_1895 (Object_t * __this /* static, unused */, RSA_t1_175 * ___rsa, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.CryptoConvert::ToCapiPrivateKeyBlob(System.Security.Cryptography.DSA)
extern "C" ByteU5BU5D_t1_109* CryptoConvert_ToCapiPrivateKeyBlob_m1_1896 (Object_t * __this /* static, unused */, DSA_t1_160 * ___dsa, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.RSA Mono.Security.Cryptography.CryptoConvert::FromCapiPublicKeyBlob(System.Byte[])
extern "C" RSA_t1_175 * CryptoConvert_FromCapiPublicKeyBlob_m1_1897 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___blob, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.RSA Mono.Security.Cryptography.CryptoConvert::FromCapiPublicKeyBlob(System.Byte[],System.Int32)
extern "C" RSA_t1_175 * CryptoConvert_FromCapiPublicKeyBlob_m1_1898 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___blob, int32_t ___offset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.DSA Mono.Security.Cryptography.CryptoConvert::FromCapiPublicKeyBlobDSA(System.Byte[])
extern "C" DSA_t1_160 * CryptoConvert_FromCapiPublicKeyBlobDSA_m1_1899 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___blob, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.DSA Mono.Security.Cryptography.CryptoConvert::FromCapiPublicKeyBlobDSA(System.Byte[],System.Int32)
extern "C" DSA_t1_160 * CryptoConvert_FromCapiPublicKeyBlobDSA_m1_1900 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___blob, int32_t ___offset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.CryptoConvert::ToCapiPublicKeyBlob(System.Security.Cryptography.RSA)
extern "C" ByteU5BU5D_t1_109* CryptoConvert_ToCapiPublicKeyBlob_m1_1901 (Object_t * __this /* static, unused */, RSA_t1_175 * ___rsa, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.CryptoConvert::ToCapiPublicKeyBlob(System.Security.Cryptography.DSA)
extern "C" ByteU5BU5D_t1_109* CryptoConvert_ToCapiPublicKeyBlob_m1_1902 (Object_t * __this /* static, unused */, DSA_t1_160 * ___dsa, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.RSA Mono.Security.Cryptography.CryptoConvert::FromCapiKeyBlob(System.Byte[])
extern "C" RSA_t1_175 * CryptoConvert_FromCapiKeyBlob_m1_1903 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___blob, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.RSA Mono.Security.Cryptography.CryptoConvert::FromCapiKeyBlob(System.Byte[],System.Int32)
extern "C" RSA_t1_175 * CryptoConvert_FromCapiKeyBlob_m1_1904 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___blob, int32_t ___offset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.DSA Mono.Security.Cryptography.CryptoConvert::FromCapiKeyBlobDSA(System.Byte[])
extern "C" DSA_t1_160 * CryptoConvert_FromCapiKeyBlobDSA_m1_1905 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___blob, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.DSA Mono.Security.Cryptography.CryptoConvert::FromCapiKeyBlobDSA(System.Byte[],System.Int32)
extern "C" DSA_t1_160 * CryptoConvert_FromCapiKeyBlobDSA_m1_1906 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___blob, int32_t ___offset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.CryptoConvert::ToCapiKeyBlob(System.Security.Cryptography.AsymmetricAlgorithm,System.Boolean)
extern "C" ByteU5BU5D_t1_109* CryptoConvert_ToCapiKeyBlob_m1_1907 (Object_t * __this /* static, unused */, AsymmetricAlgorithm_t1_228 * ___keypair, bool ___includePrivateKey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.CryptoConvert::ToCapiKeyBlob(System.Security.Cryptography.RSA,System.Boolean)
extern "C" ByteU5BU5D_t1_109* CryptoConvert_ToCapiKeyBlob_m1_1908 (Object_t * __this /* static, unused */, RSA_t1_175 * ___rsa, bool ___includePrivateKey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.CryptoConvert::ToCapiKeyBlob(System.Security.Cryptography.DSA,System.Boolean)
extern "C" ByteU5BU5D_t1_109* CryptoConvert_ToCapiKeyBlob_m1_1909 (Object_t * __this /* static, unused */, DSA_t1_160 * ___dsa, bool ___includePrivateKey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.Cryptography.CryptoConvert::ToHex(System.Byte[])
extern "C" String_t* CryptoConvert_ToHex_m1_1910 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___input, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte Mono.Security.Cryptography.CryptoConvert::FromHexChar(System.Char)
extern "C" uint8_t CryptoConvert_FromHexChar_m1_1911 (Object_t * __this /* static, unused */, uint16_t ___c, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.CryptoConvert::FromHex(System.String)
extern "C" ByteU5BU5D_t1_109* CryptoConvert_FromHex_m1_1912 (Object_t * __this /* static, unused */, String_t* ___hex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
