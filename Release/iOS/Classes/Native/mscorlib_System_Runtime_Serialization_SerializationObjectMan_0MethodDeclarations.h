﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Serialization.SerializationObjectManager
struct SerializationObjectManager_t1_1065;
// System.Runtime.Serialization.SerializationCallbacks/CallbackHandler
struct CallbackHandler_t1_1091;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.Runtime.Serialization.SerializationObjectManager::.ctor(System.Runtime.Serialization.StreamingContext)
extern "C" void SerializationObjectManager__ctor_m1_9669 (SerializationObjectManager_t1_1065 * __this, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.SerializationObjectManager::add_callbacks(System.Runtime.Serialization.SerializationCallbacks/CallbackHandler)
extern "C" void SerializationObjectManager_add_callbacks_m1_9670 (SerializationObjectManager_t1_1065 * __this, CallbackHandler_t1_1091 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.SerializationObjectManager::remove_callbacks(System.Runtime.Serialization.SerializationCallbacks/CallbackHandler)
extern "C" void SerializationObjectManager_remove_callbacks_m1_9671 (SerializationObjectManager_t1_1065 * __this, CallbackHandler_t1_1091 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.SerializationObjectManager::RegisterObject(System.Object)
extern "C" void SerializationObjectManager_RegisterObject_m1_9672 (SerializationObjectManager_t1_1065 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.SerializationObjectManager::RaiseOnSerializedEvent()
extern "C" void SerializationObjectManager_RaiseOnSerializedEvent_m1_9673 (SerializationObjectManager_t1_1065 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
