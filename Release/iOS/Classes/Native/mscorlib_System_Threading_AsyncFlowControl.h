﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Threading.Thread
struct Thread_t1_901;

#include "mscorlib_System_ValueType.h"
#include "mscorlib_System_Threading_AsyncFlowControlType.h"

// System.Threading.AsyncFlowControl
struct  AsyncFlowControl_t1_1459 
{
	// System.Threading.Thread System.Threading.AsyncFlowControl::_t
	Thread_t1_901 * ____t_0;
	// System.Threading.AsyncFlowControlType System.Threading.AsyncFlowControl::_type
	int32_t ____type_1;
};
