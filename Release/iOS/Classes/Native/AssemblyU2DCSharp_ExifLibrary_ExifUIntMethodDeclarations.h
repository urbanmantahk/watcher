﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExifLibrary.ExifUInt
struct ExifUInt_t8_118;
// System.Object
struct Object_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTag.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifInterOperability.h"

// System.Void ExifLibrary.ExifUInt::.ctor(ExifLibrary.ExifTag,System.UInt32)
extern "C" void ExifUInt__ctor_m8_549 (ExifUInt_t8_118 * __this, int32_t ___tag, uint32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ExifLibrary.ExifUInt::get__Value()
extern "C" Object_t * ExifUInt_get__Value_m8_550 (ExifUInt_t8_118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.ExifUInt::set__Value(System.Object)
extern "C" void ExifUInt_set__Value_m8_551 (ExifUInt_t8_118 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 ExifLibrary.ExifUInt::get_Value()
extern "C" uint32_t ExifUInt_get_Value_m8_552 (ExifUInt_t8_118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.ExifUInt::set_Value(System.UInt32)
extern "C" void ExifUInt_set_Value_m8_553 (ExifUInt_t8_118 * __this, uint32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExifLibrary.ExifUInt::ToString()
extern "C" String_t* ExifUInt_ToString_m8_554 (ExifUInt_t8_118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.ExifInterOperability ExifLibrary.ExifUInt::get_Interoperability()
extern "C" ExifInterOperability_t8_113  ExifUInt_get_Interoperability_m8_555 (ExifUInt_t8_118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 ExifLibrary.ExifUInt::op_Implicit(ExifLibrary.ExifUInt)
extern "C" uint32_t ExifUInt_op_Implicit_m8_556 (Object_t * __this /* static, unused */, ExifUInt_t8_118 * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
