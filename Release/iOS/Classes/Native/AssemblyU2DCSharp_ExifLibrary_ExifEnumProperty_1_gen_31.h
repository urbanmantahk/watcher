﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Object_t;

#include "AssemblyU2DCSharp_ExifLibrary_ExifProperty.h"

// ExifLibrary.ExifEnumProperty`1<System.Object>
struct  ExifEnumProperty_1_t8_378  : public ExifProperty_t8_99
{
	// T ExifLibrary.ExifEnumProperty`1::mValue
	Object_t * ___mValue_3;
	// System.Boolean ExifLibrary.ExifEnumProperty`1::mIsBitField
	bool ___mIsBitField_4;
};
