﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ApplicationException.h"

// System.Reflection.InvalidFilterCriteriaException
struct  InvalidFilterCriteriaException_t1_604  : public ApplicationException_t1_605
{
};
