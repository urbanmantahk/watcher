﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.Utility.Demo.JSONDemo
struct JSONDemo_t8_49;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.Utility.Demo.JSONDemo::.ctor()
extern "C" void JSONDemo__ctor_m8_258 (JSONDemo_t8_49 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.Demo.JSONDemo::OnGUI()
extern "C" void JSONDemo_OnGUI_m8_259 (JSONDemo_t8_49 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.Demo.JSONDemo::OnGUIButtonPressed(System.String)
extern "C" void JSONDemo_OnGUIButtonPressed_m8_260 (JSONDemo_t8_49 * __this, String_t* ____buttonName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
