﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "System_Xml_System_Xml_Schema_XmlSchemaAnnotated.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaFacet_Facet.h"

// System.Xml.Schema.XmlSchemaFacet
struct  XmlSchemaFacet_t4_61  : public XmlSchemaAnnotated_t4_53
{
};
struct XmlSchemaFacet_t4_61_StaticFields{
	// System.Xml.Schema.XmlSchemaFacet/Facet System.Xml.Schema.XmlSchemaFacet::AllFacets
	int32_t ___AllFacets_3;
};
