﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t1_1978;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__2.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_4.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m1_15709_gshared (Enumerator_t1_1986 * __this, Dictionary_2_t1_1978 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m1_15709(__this, ___dictionary, method) (( void (*) (Enumerator_t1_1986 *, Dictionary_2_t1_1978 *, const MethodInfo*))Enumerator__ctor_m1_15709_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m1_15710_gshared (Enumerator_t1_1986 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_15710(__this, method) (( Object_t * (*) (Enumerator_t1_1986 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_15710_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m1_15711_gshared (Enumerator_t1_1986 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1_15711(__this, method) (( void (*) (Enumerator_t1_1986 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_15711_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t1_284  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_15712_gshared (Enumerator_t1_1986 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_15712(__this, method) (( DictionaryEntry_t1_284  (*) (Enumerator_t1_1986 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_15712_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_15713_gshared (Enumerator_t1_1986 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_15713(__this, method) (( Object_t * (*) (Enumerator_t1_1986 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_15713_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_15714_gshared (Enumerator_t1_1986 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_15714(__this, method) (( Object_t * (*) (Enumerator_t1_1986 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_15714_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::MoveNext()
extern "C" bool Enumerator_MoveNext_m1_15715_gshared (Enumerator_t1_1986 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1_15715(__this, method) (( bool (*) (Enumerator_t1_1986 *, const MethodInfo*))Enumerator_MoveNext_m1_15715_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::get_Current()
extern "C" KeyValuePair_2_t1_1981  Enumerator_get_Current_m1_15716_gshared (Enumerator_t1_1986 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1_15716(__this, method) (( KeyValuePair_2_t1_1981  (*) (Enumerator_t1_1986 *, const MethodInfo*))Enumerator_get_Current_m1_15716_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m1_15717_gshared (Enumerator_t1_1986 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m1_15717(__this, method) (( Object_t * (*) (Enumerator_t1_1986 *, const MethodInfo*))Enumerator_get_CurrentKey_m1_15717_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::get_CurrentValue()
extern "C" int32_t Enumerator_get_CurrentValue_m1_15718_gshared (Enumerator_t1_1986 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m1_15718(__this, method) (( int32_t (*) (Enumerator_t1_1986 *, const MethodInfo*))Enumerator_get_CurrentValue_m1_15718_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::Reset()
extern "C" void Enumerator_Reset_m1_15719_gshared (Enumerator_t1_1986 * __this, const MethodInfo* method);
#define Enumerator_Reset_m1_15719(__this, method) (( void (*) (Enumerator_t1_1986 *, const MethodInfo*))Enumerator_Reset_m1_15719_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::VerifyState()
extern "C" void Enumerator_VerifyState_m1_15720_gshared (Enumerator_t1_1986 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m1_15720(__this, method) (( void (*) (Enumerator_t1_1986 *, const MethodInfo*))Enumerator_VerifyState_m1_15720_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m1_15721_gshared (Enumerator_t1_1986 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m1_15721(__this, method) (( void (*) (Enumerator_t1_1986 *, const MethodInfo*))Enumerator_VerifyCurrent_m1_15721_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::Dispose()
extern "C" void Enumerator_Dispose_m1_15722_gshared (Enumerator_t1_1986 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1_15722(__this, method) (( void (*) (Enumerator_t1_1986 *, const MethodInfo*))Enumerator_Dispose_m1_15722_gshared)(__this, method)
