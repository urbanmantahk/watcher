﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.CrossPlatformNotification/iOSSpecificProperties
struct iOSSpecificProperties_t8_266;
// System.Collections.IDictionary
struct IDictionary_t1_35;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.CrossPlatformNotification/iOSSpecificProperties::.ctor()
extern "C" void iOSSpecificProperties__ctor_m8_1502 (iOSSpecificProperties_t8_266 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.CrossPlatformNotification/iOSSpecificProperties::.ctor(System.Collections.IDictionary)
extern "C" void iOSSpecificProperties__ctor_m8_1503 (iOSSpecificProperties_t8_266 * __this, Object_t * ____jsonDict, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.CrossPlatformNotification/iOSSpecificProperties::get_AlertAction()
extern "C" String_t* iOSSpecificProperties_get_AlertAction_m8_1504 (iOSSpecificProperties_t8_266 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.CrossPlatformNotification/iOSSpecificProperties::set_AlertAction(System.String)
extern "C" void iOSSpecificProperties_set_AlertAction_m8_1505 (iOSSpecificProperties_t8_266 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.NativePlugins.CrossPlatformNotification/iOSSpecificProperties::get_HasAction()
extern "C" bool iOSSpecificProperties_get_HasAction_m8_1506 (iOSSpecificProperties_t8_266 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.CrossPlatformNotification/iOSSpecificProperties::set_HasAction(System.Boolean)
extern "C" void iOSSpecificProperties_set_HasAction_m8_1507 (iOSSpecificProperties_t8_266 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 VoxelBusters.NativePlugins.CrossPlatformNotification/iOSSpecificProperties::get_BadgeCount()
extern "C" int32_t iOSSpecificProperties_get_BadgeCount_m8_1508 (iOSSpecificProperties_t8_266 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.CrossPlatformNotification/iOSSpecificProperties::set_BadgeCount(System.Int32)
extern "C" void iOSSpecificProperties_set_BadgeCount_m8_1509 (iOSSpecificProperties_t8_266 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.CrossPlatformNotification/iOSSpecificProperties::get_LaunchImage()
extern "C" String_t* iOSSpecificProperties_get_LaunchImage_m8_1510 (iOSSpecificProperties_t8_266 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.CrossPlatformNotification/iOSSpecificProperties::set_LaunchImage(System.String)
extern "C" void iOSSpecificProperties_set_LaunchImage_m8_1511 (iOSSpecificProperties_t8_266 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IDictionary VoxelBusters.NativePlugins.CrossPlatformNotification/iOSSpecificProperties::JSONObject()
extern "C" Object_t * iOSSpecificProperties_JSONObject_m8_1512 (iOSSpecificProperties_t8_266 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.CrossPlatformNotification/iOSSpecificProperties::ToString()
extern "C" String_t* iOSSpecificProperties_ToString_m8_1513 (iOSSpecificProperties_t8_266 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
