﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSDistanceRef>
struct ExifEnumProperty_1_t8_368;
// System.Object
struct Object_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTag.h"
#include "AssemblyU2DCSharp_ExifLibrary_GPSDistanceRef.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifInterOperability.h"

// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSDistanceRef>::.ctor(ExifLibrary.ExifTag,T,System.Boolean)
extern "C" void ExifEnumProperty_1__ctor_m8_2342_gshared (ExifEnumProperty_1_t8_368 * __this, int32_t ___tag, uint8_t ___value, bool ___isbitfield, const MethodInfo* method);
#define ExifEnumProperty_1__ctor_m8_2342(__this, ___tag, ___value, ___isbitfield, method) (( void (*) (ExifEnumProperty_1_t8_368 *, int32_t, uint8_t, bool, const MethodInfo*))ExifEnumProperty_1__ctor_m8_2342_gshared)(__this, ___tag, ___value, ___isbitfield, method)
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSDistanceRef>::.ctor(ExifLibrary.ExifTag,T)
extern "C" void ExifEnumProperty_1__ctor_m8_2005_gshared (ExifEnumProperty_1_t8_368 * __this, int32_t ___tag, uint8_t ___value, const MethodInfo* method);
#define ExifEnumProperty_1__ctor_m8_2005(__this, ___tag, ___value, method) (( void (*) (ExifEnumProperty_1_t8_368 *, int32_t, uint8_t, const MethodInfo*))ExifEnumProperty_1__ctor_m8_2005_gshared)(__this, ___tag, ___value, method)
// System.Object ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSDistanceRef>::get__Value()
extern "C" Object_t * ExifEnumProperty_1_get__Value_m8_2343_gshared (ExifEnumProperty_1_t8_368 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get__Value_m8_2343(__this, method) (( Object_t * (*) (ExifEnumProperty_1_t8_368 *, const MethodInfo*))ExifEnumProperty_1_get__Value_m8_2343_gshared)(__this, method)
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSDistanceRef>::set__Value(System.Object)
extern "C" void ExifEnumProperty_1_set__Value_m8_2344_gshared (ExifEnumProperty_1_t8_368 * __this, Object_t * ___value, const MethodInfo* method);
#define ExifEnumProperty_1_set__Value_m8_2344(__this, ___value, method) (( void (*) (ExifEnumProperty_1_t8_368 *, Object_t *, const MethodInfo*))ExifEnumProperty_1_set__Value_m8_2344_gshared)(__this, ___value, method)
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSDistanceRef>::get_Value()
extern "C" uint8_t ExifEnumProperty_1_get_Value_m8_2345_gshared (ExifEnumProperty_1_t8_368 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get_Value_m8_2345(__this, method) (( uint8_t (*) (ExifEnumProperty_1_t8_368 *, const MethodInfo*))ExifEnumProperty_1_get_Value_m8_2345_gshared)(__this, method)
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSDistanceRef>::set_Value(T)
extern "C" void ExifEnumProperty_1_set_Value_m8_2346_gshared (ExifEnumProperty_1_t8_368 * __this, uint8_t ___value, const MethodInfo* method);
#define ExifEnumProperty_1_set_Value_m8_2346(__this, ___value, method) (( void (*) (ExifEnumProperty_1_t8_368 *, uint8_t, const MethodInfo*))ExifEnumProperty_1_set_Value_m8_2346_gshared)(__this, ___value, method)
// System.Boolean ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSDistanceRef>::get_IsBitField()
extern "C" bool ExifEnumProperty_1_get_IsBitField_m8_2347_gshared (ExifEnumProperty_1_t8_368 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get_IsBitField_m8_2347(__this, method) (( bool (*) (ExifEnumProperty_1_t8_368 *, const MethodInfo*))ExifEnumProperty_1_get_IsBitField_m8_2347_gshared)(__this, method)
// System.String ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSDistanceRef>::ToString()
extern "C" String_t* ExifEnumProperty_1_ToString_m8_2348_gshared (ExifEnumProperty_1_t8_368 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_ToString_m8_2348(__this, method) (( String_t* (*) (ExifEnumProperty_1_t8_368 *, const MethodInfo*))ExifEnumProperty_1_ToString_m8_2348_gshared)(__this, method)
// ExifLibrary.ExifInterOperability ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSDistanceRef>::get_Interoperability()
extern "C" ExifInterOperability_t8_113  ExifEnumProperty_1_get_Interoperability_m8_2349_gshared (ExifEnumProperty_1_t8_368 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get_Interoperability_m8_2349(__this, method) (( ExifInterOperability_t8_113  (*) (ExifEnumProperty_1_t8_368 *, const MethodInfo*))ExifEnumProperty_1_get_Interoperability_m8_2349_gshared)(__this, method)
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSDistanceRef>::op_Implicit(ExifLibrary.ExifEnumProperty`1<T>)
extern "C" uint8_t ExifEnumProperty_1_op_Implicit_m8_2350_gshared (Object_t * __this /* static, unused */, ExifEnumProperty_1_t8_368 * ___obj, const MethodInfo* method);
#define ExifEnumProperty_1_op_Implicit_m8_2350(__this /* static, unused */, ___obj, method) (( uint8_t (*) (Object_t * /* static, unused */, ExifEnumProperty_1_t8_368 *, const MethodInfo*))ExifEnumProperty_1_op_Implicit_m8_2350_gshared)(__this /* static, unused */, ___obj, method)
