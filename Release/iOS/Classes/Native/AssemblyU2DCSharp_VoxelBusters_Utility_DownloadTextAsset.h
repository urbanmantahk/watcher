﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VoxelBusters.Utility.DownloadTextAsset/Completion
struct Completion_t8_159;

#include "AssemblyU2DCSharp_VoxelBusters_Utility_Request.h"

// VoxelBusters.Utility.DownloadTextAsset
struct  DownloadTextAsset_t8_160  : public Request_t8_155
{
	// VoxelBusters.Utility.DownloadTextAsset/Completion VoxelBusters.Utility.DownloadTextAsset::<OnCompletion>k__BackingField
	Completion_t8_159 * ___U3COnCompletionU3Ek__BackingField_4;
};
