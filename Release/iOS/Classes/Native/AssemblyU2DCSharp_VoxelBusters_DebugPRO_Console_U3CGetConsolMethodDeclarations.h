﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.DebugPRO.Console/<GetConsoleTag>c__AnonStorey7
struct U3CGetConsoleTagU3Ec__AnonStorey7_t8_167;
// VoxelBusters.DebugPRO.Internal.ConsoleTag
struct ConsoleTag_t8_171;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.DebugPRO.Console/<GetConsoleTag>c__AnonStorey7::.ctor()
extern "C" void U3CGetConsoleTagU3Ec__AnonStorey7__ctor_m8_972 (U3CGetConsoleTagU3Ec__AnonStorey7_t8_167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.DebugPRO.Console/<GetConsoleTag>c__AnonStorey7::<>m__5(VoxelBusters.DebugPRO.Internal.ConsoleTag)
extern "C" bool U3CGetConsoleTagU3Ec__AnonStorey7_U3CU3Em__5_m8_973 (U3CGetConsoleTagU3Ec__AnonStorey7_t8_167 * __this, ConsoleTag_t8_171 * ____consoleTag, const MethodInfo* method) IL2CPP_METHOD_ATTR;
