﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen_6MethodDeclarations.h"

// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.EventSystems.BaseEventData>::.ctor()
#define UnityEvent_1__ctor_m6_1842(__this, method) (( void (*) (UnityEvent_1_t6_300 *, const MethodInfo*))UnityEvent_1__ctor_m6_1999_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.EventSystems.BaseEventData>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
#define UnityEvent_1_AddListener_m6_2030(__this, ___call, method) (( void (*) (UnityEvent_1_t6_300 *, UnityAction_1_t6_348 *, const MethodInfo*))UnityEvent_1_AddListener_m6_2000_gshared)(__this, ___call, method)
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.EventSystems.BaseEventData>::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
#define UnityEvent_1_RemoveListener_m6_2031(__this, ___call, method) (( void (*) (UnityEvent_1_t6_300 *, UnityAction_1_t6_348 *, const MethodInfo*))UnityEvent_1_RemoveListener_m6_2001_gshared)(__this, ___call, method)
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1<UnityEngine.EventSystems.BaseEventData>::FindMethod_Impl(System.String,System.Object)
#define UnityEvent_1_FindMethod_Impl_m6_2032(__this, ___name, ___targetObj, method) (( MethodInfo_t * (*) (UnityEvent_1_t6_300 *, String_t*, Object_t *, const MethodInfo*))UnityEvent_1_FindMethod_Impl_m6_2002_gshared)(__this, ___name, ___targetObj, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<UnityEngine.EventSystems.BaseEventData>::GetDelegate(System.Object,System.Reflection.MethodInfo)
#define UnityEvent_1_GetDelegate_m6_2033(__this, ___target, ___theFunction, method) (( BaseInvokableCall_t6_257 * (*) (UnityEvent_1_t6_300 *, Object_t *, MethodInfo_t *, const MethodInfo*))UnityEvent_1_GetDelegate_m6_2003_gshared)(__this, ___target, ___theFunction, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<UnityEngine.EventSystems.BaseEventData>::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
#define UnityEvent_1_GetDelegate_m6_2034(__this /* static, unused */, ___action, method) (( BaseInvokableCall_t6_257 * (*) (Object_t * /* static, unused */, UnityAction_1_t6_348 *, const MethodInfo*))UnityEvent_1_GetDelegate_m6_2004_gshared)(__this /* static, unused */, ___action, method)
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.EventSystems.BaseEventData>::Invoke(T0)
#define UnityEvent_1_Invoke_m6_1843(__this, ___arg0, method) (( void (*) (UnityEvent_1_t6_300 *, BaseEventData_t7_5 *, const MethodInfo*))UnityEvent_1_Invoke_m6_2005_gshared)(__this, ___arg0, method)
