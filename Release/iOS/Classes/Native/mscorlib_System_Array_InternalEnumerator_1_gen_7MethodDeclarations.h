﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_7.h"

// System.Void System.Array/InternalEnumerator`1<System.Single>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_15432_gshared (InternalEnumerator_1_t1_1955 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1_15432(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1_1955 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m1_15432_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Single>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_15433_gshared (InternalEnumerator_1_t1_1955 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_15433(__this, method) (( void (*) (InternalEnumerator_1_t1_1955 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_15433_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Single>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_15434_gshared (InternalEnumerator_1_t1_1955 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_15434(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1_1955 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_15434_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Single>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_15435_gshared (InternalEnumerator_1_t1_1955 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1_15435(__this, method) (( void (*) (InternalEnumerator_1_t1_1955 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1_15435_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Single>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_15436_gshared (InternalEnumerator_1_t1_1955 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1_15436(__this, method) (( bool (*) (InternalEnumerator_1_t1_1955 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1_15436_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Single>::get_Current()
extern "C" float InternalEnumerator_1_get_Current_m1_15437_gshared (InternalEnumerator_1_t1_1955 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1_15437(__this, method) (( float (*) (InternalEnumerator_1_t1_1955 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1_15437_gshared)(__this, method)
