﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.AccessControl.ObjectAuditRule
struct ObjectAuditRule_t1_1166;
// System.Security.Principal.IdentityReference
struct IdentityReference_t1_1120;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_AccessControl_InheritanceFlags.h"
#include "mscorlib_System_Security_AccessControl_PropagationFlags.h"
#include "mscorlib_System_Guid.h"
#include "mscorlib_System_Security_AccessControl_AuditFlags.h"
#include "mscorlib_System_Security_AccessControl_ObjectAceFlags.h"

// System.Void System.Security.AccessControl.ObjectAuditRule::.ctor(System.Security.Principal.IdentityReference,System.Int32,System.Boolean,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Guid,System.Guid,System.Security.AccessControl.AuditFlags)
extern "C" void ObjectAuditRule__ctor_m1_9984 (ObjectAuditRule_t1_1166 * __this, IdentityReference_t1_1120 * ___identity, int32_t ___accessMask, bool ___isInherited, int32_t ___inheritanceFlags, int32_t ___propagationFlags, Guid_t1_319  ___objectType, Guid_t1_319  ___inheritedObjectType, int32_t ___auditFlags, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Guid System.Security.AccessControl.ObjectAuditRule::get_InheritedObjectType()
extern "C" Guid_t1_319  ObjectAuditRule_get_InheritedObjectType_m1_9985 (ObjectAuditRule_t1_1166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.AccessControl.ObjectAceFlags System.Security.AccessControl.ObjectAuditRule::get_ObjectFlags()
extern "C" int32_t ObjectAuditRule_get_ObjectFlags_m1_9986 (ObjectAuditRule_t1_1166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Guid System.Security.AccessControl.ObjectAuditRule::get_ObjectType()
extern "C" Guid_t1_319  ObjectAuditRule_get_ObjectType_m1_9987 (ObjectAuditRule_t1_1166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
