﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Security_AccessControl_ObjectSecurity.h"

// System.Security.AccessControl.DirectoryObjectSecurity
struct  DirectoryObjectSecurity_t1_1146  : public ObjectSecurity_t1_1127
{
};
