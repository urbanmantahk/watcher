﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Security_Permissions_CodeAccessSecurityAttri.h"
#include "mscorlib_System_Security_Permissions_SecurityPermissionFlag.h"

// System.Security.Permissions.SecurityPermissionAttribute
struct  SecurityPermissionAttribute_t1_1309  : public CodeAccessSecurityAttribute_t1_1266
{
	// System.Security.Permissions.SecurityPermissionFlag System.Security.Permissions.SecurityPermissionAttribute::m_Flags
	int32_t ___m_Flags_2;
};
