﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType.h"
#include "mscorlib_System_IntPtr.h"

// System.Threading.NativeOverlapped
struct  NativeOverlapped_t1_1471 
{
	// System.IntPtr System.Threading.NativeOverlapped::EventHandle
	IntPtr_t ___EventHandle_0;
	// System.IntPtr System.Threading.NativeOverlapped::InternalHigh
	IntPtr_t ___InternalHigh_1;
	// System.IntPtr System.Threading.NativeOverlapped::InternalLow
	IntPtr_t ___InternalLow_2;
	// System.Int32 System.Threading.NativeOverlapped::OffsetHigh
	int32_t ___OffsetHigh_3;
	// System.Int32 System.Threading.NativeOverlapped::OffsetLow
	int32_t ___OffsetLow_4;
	// System.Int32 System.Threading.NativeOverlapped::Handle1
	int32_t ___Handle1_5;
	// System.Int32 System.Threading.NativeOverlapped::Handle2
	int32_t ___Handle2_6;
};
