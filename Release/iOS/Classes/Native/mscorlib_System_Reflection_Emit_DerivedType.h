﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Type
struct Type_t;

#include "mscorlib_System_Type.h"

// System.Reflection.Emit.DerivedType
struct  DerivedType_t1_489  : public Type_t
{
	// System.Type System.Reflection.Emit.DerivedType::elementType
	Type_t * ___elementType_8;
};
