﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Security_Permissions_CodeAccessSecurityAttri.h"

// System.Security.Permissions.PublisherIdentityPermissionAttribute
struct  PublisherIdentityPermissionAttribute_t1_1300  : public CodeAccessSecurityAttribute_t1_1266
{
	// System.String System.Security.Permissions.PublisherIdentityPermissionAttribute::certFile
	String_t* ___certFile_2;
	// System.String System.Security.Permissions.PublisherIdentityPermissionAttribute::signedFile
	String_t* ___signedFile_3;
	// System.String System.Security.Permissions.PublisherIdentityPermissionAttribute::x509data
	String_t* ___x509data_4;
};
