﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.AccessControl.GenericSecurityDescriptor
struct GenericSecurityDescriptor_t1_1131;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_AccessControl_AccessControlSections.h"

// System.Void System.Security.AccessControl.GenericSecurityDescriptor::.ctor()
extern "C" void GenericSecurityDescriptor__ctor_m1_9921 (GenericSecurityDescriptor_t1_1131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.AccessControl.GenericSecurityDescriptor::get_BinaryLength()
extern "C" int32_t GenericSecurityDescriptor_get_BinaryLength_m1_9922 (GenericSecurityDescriptor_t1_1131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte System.Security.AccessControl.GenericSecurityDescriptor::get_Revision()
extern "C" uint8_t GenericSecurityDescriptor_get_Revision_m1_9923 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.GenericSecurityDescriptor::GetBinaryForm(System.Byte[],System.Int32)
extern "C" void GenericSecurityDescriptor_GetBinaryForm_m1_9924 (GenericSecurityDescriptor_t1_1131 * __this, ByteU5BU5D_t1_109* ___binaryForm, int32_t ___offset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.AccessControl.GenericSecurityDescriptor::GetSddlForm(System.Security.AccessControl.AccessControlSections)
extern "C" String_t* GenericSecurityDescriptor_GetSddlForm_m1_9925 (GenericSecurityDescriptor_t1_1131 * __this, int32_t ___includeSections, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.AccessControl.GenericSecurityDescriptor::IsSddlConversionSupported()
extern "C" bool GenericSecurityDescriptor_IsSddlConversionSupported_m1_9926 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
