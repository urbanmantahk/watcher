﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Configuration_Assemblies_AssemblyHash.h"
#include "mscorlib_System_Configuration_Assemblies_AssemblyHashAlgorit.h"

// System.Void System.Configuration.Assemblies.AssemblyHash::.ctor(System.Configuration.Assemblies.AssemblyHashAlgorithm,System.Byte[])
extern "C" void AssemblyHash__ctor_m1_3535 (AssemblyHash_t1_311 * __this, int32_t ___algorithm, ByteU5BU5D_t1_109* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.Assemblies.AssemblyHash::.ctor(System.Byte[])
extern "C" void AssemblyHash__ctor_m1_3536 (AssemblyHash_t1_311 * __this, ByteU5BU5D_t1_109* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.Assemblies.AssemblyHash::.cctor()
extern "C" void AssemblyHash__cctor_m1_3537 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.Assemblies.AssemblyHashAlgorithm System.Configuration.Assemblies.AssemblyHash::get_Algorithm()
extern "C" int32_t AssemblyHash_get_Algorithm_m1_3538 (AssemblyHash_t1_311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.Assemblies.AssemblyHash::set_Algorithm(System.Configuration.Assemblies.AssemblyHashAlgorithm)
extern "C" void AssemblyHash_set_Algorithm_m1_3539 (AssemblyHash_t1_311 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Configuration.Assemblies.AssemblyHash::Clone()
extern "C" Object_t * AssemblyHash_Clone_m1_3540 (AssemblyHash_t1_311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Configuration.Assemblies.AssemblyHash::GetValue()
extern "C" ByteU5BU5D_t1_109* AssemblyHash_GetValue_m1_3541 (AssemblyHash_t1_311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.Assemblies.AssemblyHash::SetValue(System.Byte[])
extern "C" void AssemblyHash_SetValue_m1_3542 (AssemblyHash_t1_311 * __this, ByteU5BU5D_t1_109* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void AssemblyHash_t1_311_marshal(const AssemblyHash_t1_311& unmarshaled, AssemblyHash_t1_311_marshaled& marshaled);
extern "C" void AssemblyHash_t1_311_marshal_back(const AssemblyHash_t1_311_marshaled& marshaled, AssemblyHash_t1_311& unmarshaled);
extern "C" void AssemblyHash_t1_311_marshal_cleanup(AssemblyHash_t1_311_marshaled& marshaled);
