﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>
struct ReadOnlyCollection_1_t1_2722;
// System.Collections.Generic.IList`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>
struct IList_1_t1_2723;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Object
struct Object_t;
// VoxelBusters.DebugPRO.Internal.ConsoleLog[]
struct ConsoleLogU5BU5D_t8_382;
// System.Collections.Generic.IEnumerator`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>
struct IEnumerator_1_t1_2895;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VoxelBusters_DebugPRO_Internal_ConsoleLog.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C" void ReadOnlyCollection_1__ctor_m1_27205_gshared (ReadOnlyCollection_1_t1_2722 * __this, Object_t* ___list, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m1_27205(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t1_2722 *, Object_t*, const MethodInfo*))ReadOnlyCollection_1__ctor_m1_27205_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1_27206_gshared (ReadOnlyCollection_1_t1_2722 * __this, ConsoleLog_t8_170  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1_27206(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t1_2722 *, ConsoleLog_t8_170 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1_27206_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.Generic.ICollection<T>.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1_27207_gshared (ReadOnlyCollection_1_t1_2722 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1_27207(__this, method) (( void (*) (ReadOnlyCollection_1_t1_2722 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1_27207_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1_27208_gshared (ReadOnlyCollection_1_t1_2722 * __this, int32_t ___index, ConsoleLog_t8_170  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1_27208(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t1_2722 *, int32_t, ConsoleLog_t8_170 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1_27208_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1_27209_gshared (ReadOnlyCollection_1_t1_2722 * __this, ConsoleLog_t8_170  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1_27209(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t1_2722 *, ConsoleLog_t8_170 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1_27209_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1_27210_gshared (ReadOnlyCollection_1_t1_2722 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1_27210(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t1_2722 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1_27210_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C" ConsoleLog_t8_170  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1_27211_gshared (ReadOnlyCollection_1_t1_2722 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1_27211(__this, ___index, method) (( ConsoleLog_t8_170  (*) (ReadOnlyCollection_1_t1_2722 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1_27211_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1_27212_gshared (ReadOnlyCollection_1_t1_2722 * __this, int32_t ___index, ConsoleLog_t8_170  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1_27212(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t1_2722 *, int32_t, ConsoleLog_t8_170 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1_27212_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_27213_gshared (ReadOnlyCollection_1_t1_2722 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_27213(__this, method) (( bool (*) (ReadOnlyCollection_1_t1_2722 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_27213_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1_27214_gshared (ReadOnlyCollection_1_t1_2722 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1_27214(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t1_2722 *, Array_t *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1_27214_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1_27215_gshared (ReadOnlyCollection_1_t1_2722 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1_27215(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t1_2722 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1_27215_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IList.Add(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m1_27216_gshared (ReadOnlyCollection_1_t1_2722 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m1_27216(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t1_2722 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m1_27216_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IList.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m1_27217_gshared (ReadOnlyCollection_1_t1_2722 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m1_27217(__this, method) (( void (*) (ReadOnlyCollection_1_t1_2722 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m1_27217_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IList.Contains(System.Object)
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_Contains_m1_27218_gshared (ReadOnlyCollection_1_t1_2722 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m1_27218(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t1_2722 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m1_27218_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1_27219_gshared (ReadOnlyCollection_1_t1_2722 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1_27219(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t1_2722 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1_27219_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m1_27220_gshared (ReadOnlyCollection_1_t1_2722 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m1_27220(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t1_2722 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m1_27220_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IList.Remove(System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m1_27221_gshared (ReadOnlyCollection_1_t1_2722 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m1_27221(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t1_2722 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m1_27221_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IList.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1_27222_gshared (ReadOnlyCollection_1_t1_2722 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1_27222(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t1_2722 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1_27222_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1_27223_gshared (ReadOnlyCollection_1_t1_2722 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1_27223(__this, method) (( bool (*) (ReadOnlyCollection_1_t1_2722 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1_27223_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1_27224_gshared (ReadOnlyCollection_1_t1_2722 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1_27224(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t1_2722 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1_27224_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IList.get_IsFixedSize()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1_27225_gshared (ReadOnlyCollection_1_t1_2722 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1_27225(__this, method) (( bool (*) (ReadOnlyCollection_1_t1_2722 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1_27225_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IList.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1_27226_gshared (ReadOnlyCollection_1_t1_2722 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1_27226(__this, method) (( bool (*) (ReadOnlyCollection_1_t1_2722 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1_27226_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IList_get_Item_m1_27227_gshared (ReadOnlyCollection_1_t1_2722 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m1_27227(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t1_2722 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m1_27227_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m1_27228_gshared (ReadOnlyCollection_1_t1_2722 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m1_27228(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t1_2722 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m1_27228_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Contains(T)
extern "C" bool ReadOnlyCollection_1_Contains_m1_27229_gshared (ReadOnlyCollection_1_t1_2722 * __this, ConsoleLog_t8_170  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m1_27229(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t1_2722 *, ConsoleLog_t8_170 , const MethodInfo*))ReadOnlyCollection_1_Contains_m1_27229_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::CopyTo(T[],System.Int32)
extern "C" void ReadOnlyCollection_1_CopyTo_m1_27230_gshared (ReadOnlyCollection_1_t1_2722 * __this, ConsoleLogU5BU5D_t8_382* ___array, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m1_27230(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t1_2722 *, ConsoleLogU5BU5D_t8_382*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m1_27230_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::GetEnumerator()
extern "C" Object_t* ReadOnlyCollection_1_GetEnumerator_m1_27231_gshared (ReadOnlyCollection_1_t1_2722 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m1_27231(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t1_2722 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m1_27231_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::IndexOf(T)
extern "C" int32_t ReadOnlyCollection_1_IndexOf_m1_27232_gshared (ReadOnlyCollection_1_t1_2722 * __this, ConsoleLog_t8_170  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m1_27232(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t1_2722 *, ConsoleLog_t8_170 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m1_27232_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::get_Count()
extern "C" int32_t ReadOnlyCollection_1_get_Count_m1_27233_gshared (ReadOnlyCollection_1_t1_2722 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m1_27233(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t1_2722 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m1_27233_gshared)(__this, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::get_Items()
extern "C" Object_t* ReadOnlyCollection_1_get_Items_m1_27234_gshared (ReadOnlyCollection_1_t1_2722 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Items_m1_27234(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t1_2722 *, const MethodInfo*))ReadOnlyCollection_1_get_Items_m1_27234_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::get_Item(System.Int32)
extern "C" ConsoleLog_t8_170  ReadOnlyCollection_1_get_Item_m1_27235_gshared (ReadOnlyCollection_1_t1_2722 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m1_27235(__this, ___index, method) (( ConsoleLog_t8_170  (*) (ReadOnlyCollection_1_t1_2722 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m1_27235_gshared)(__this, ___index, method)
