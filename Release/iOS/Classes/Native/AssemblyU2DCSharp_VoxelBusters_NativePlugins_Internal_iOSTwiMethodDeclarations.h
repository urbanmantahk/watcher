﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.Internal.iOSTwitterSession
struct iOSTwitterSession_t8_296;
// System.Collections.IDictionary
struct IDictionary_t1_35;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.Internal.iOSTwitterSession::.ctor(System.Collections.IDictionary)
extern "C" void iOSTwitterSession__ctor_m8_1728 (iOSTwitterSession_t8_296 * __this, Object_t * ____sessionJsonDict, const MethodInfo* method) IL2CPP_METHOD_ATTR;
