﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.Collections.IDictionary
struct IDictionary_t1_35;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1_1742;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_WebResponse.h"

// System.Int32 VoxelBusters.Utility.WebResponse::get_Status()
extern "C" int32_t WebResponse_get_Status_m8_947 (WebResponse_t8_164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.WebResponse::set_Status(System.Int32)
extern "C" void WebResponse_set_Status_m8_948 (WebResponse_t8_164 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.Utility.WebResponse::get_Message()
extern "C" String_t* WebResponse_get_Message_m8_949 (WebResponse_t8_164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.WebResponse::set_Message(System.String)
extern "C" void WebResponse_set_Message_m8_950 (WebResponse_t8_164 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IDictionary VoxelBusters.Utility.WebResponse::get_Data()
extern "C" Object_t * WebResponse_get_Data_m8_951 (WebResponse_t8_164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.WebResponse::set_Data(System.Collections.IDictionary)
extern "C" void WebResponse_set_Data_m8_952 (WebResponse_t8_164 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> VoxelBusters.Utility.WebResponse::get_Errors()
extern "C" List_1_t1_1742 * WebResponse_get_Errors_m8_953 (WebResponse_t8_164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.WebResponse::set_Errors(System.Collections.Generic.List`1<System.String>)
extern "C" void WebResponse_set_Errors_m8_954 (WebResponse_t8_164 * __this, List_1_t1_1742 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.Utility.WebResponse VoxelBusters.Utility.WebResponse::WebResponseOnSuccess(System.Collections.IDictionary)
extern "C" WebResponse_t8_164  WebResponse_WebResponseOnSuccess_m8_955 (Object_t * __this /* static, unused */, Object_t * ____jsonResponse, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.Utility.WebResponse VoxelBusters.Utility.WebResponse::WebResponseOnFail(System.Collections.IDictionary)
extern "C" WebResponse_t8_164  WebResponse_WebResponseOnFail_m8_956 (Object_t * __this /* static, unused */, Object_t * ____jsonResponse, const MethodInfo* method) IL2CPP_METHOD_ATTR;
