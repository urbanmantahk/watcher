﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.String>
struct List_1_t1_1742;
// VoxelBusters.Utility.GUIScrollView
struct GUIScrollView_t8_19;

#include "AssemblyU2DCSharp_VoxelBusters_AssetStoreProductUtility_Demo.h"

// VoxelBusters.AssetStoreProductUtility.Demo.DemoSubMenu
struct  DemoSubMenu_t8_18  : public DemoGUIWindow_t8_14
{
	// System.Collections.Generic.List`1<System.String> VoxelBusters.AssetStoreProductUtility.Demo.DemoSubMenu::m_results
	List_1_t1_1742 * ___m_results_7;
	// VoxelBusters.Utility.GUIScrollView VoxelBusters.AssetStoreProductUtility.Demo.DemoSubMenu::m_resultsScrollView
	GUIScrollView_t8_19 * ___m_resultsScrollView_8;
};
