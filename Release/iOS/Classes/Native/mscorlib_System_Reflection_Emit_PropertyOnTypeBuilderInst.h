﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Reflection.MonoGenericClass
struct MonoGenericClass_t1_485;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;

#include "mscorlib_System_Reflection_PropertyInfo.h"

// System.Reflection.Emit.PropertyOnTypeBuilderInst
struct  PropertyOnTypeBuilderInst_t1_548  : public PropertyInfo_t
{
	// System.Reflection.MonoGenericClass System.Reflection.Emit.PropertyOnTypeBuilderInst::instantiation
	MonoGenericClass_t1_485 * ___instantiation_0;
	// System.Reflection.PropertyInfo System.Reflection.Emit.PropertyOnTypeBuilderInst::prop
	PropertyInfo_t * ___prop_1;
};
