﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// VoxelBusters.Utility.GETRequest
struct GETRequest_t8_23;
// UnityEngine.Texture2D
struct Texture2D_t6_33;

#include "mscorlib_System_Object.h"
#include "AssemblyU2DCSharp_VoxelBusters_AssetStoreProductUtility_Inte_0.h"

// VoxelBusters.AssetStoreProductUtility.AssetStoreProduct
struct  AssetStoreProduct_t8_22  : public Object_t
{
	// VoxelBusters.AssetStoreProductUtility.Internal.ProductUpdateInfo VoxelBusters.AssetStoreProductUtility.AssetStoreProduct::m_productUpdateInfo
	ProductUpdateInfo_t8_21  ___m_productUpdateInfo_7;
	// System.Boolean VoxelBusters.AssetStoreProductUtility.AssetStoreProduct::m_isAutomaticCheck
	bool ___m_isAutomaticCheck_8;
	// VoxelBusters.Utility.GETRequest VoxelBusters.AssetStoreProductUtility.AssetStoreProduct::m_updateGETRequest
	GETRequest_t8_23 * ___m_updateGETRequest_9;
	// System.String VoxelBusters.AssetStoreProductUtility.AssetStoreProduct::<ProductName>k__BackingField
	String_t* ___U3CProductNameU3Ek__BackingField_10;
	// System.String VoxelBusters.AssetStoreProductUtility.AssetStoreProduct::<ProductVersion>k__BackingField
	String_t* ___U3CProductVersionU3Ek__BackingField_11;
	// UnityEngine.Texture2D VoxelBusters.AssetStoreProductUtility.AssetStoreProduct::<LogoTexture>k__BackingField
	Texture2D_t6_33 * ___U3CLogoTextureU3Ek__BackingField_12;
};
