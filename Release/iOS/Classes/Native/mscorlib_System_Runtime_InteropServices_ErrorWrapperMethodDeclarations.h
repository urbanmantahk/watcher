﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.InteropServices.ErrorWrapper
struct ErrorWrapper_t1_788;
// System.Exception
struct Exception_t1_33;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.InteropServices.ErrorWrapper::.ctor(System.Exception)
extern "C" void ErrorWrapper__ctor_m1_7635 (ErrorWrapper_t1_788 * __this, Exception_t1_33 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.ErrorWrapper::.ctor(System.Int32)
extern "C" void ErrorWrapper__ctor_m1_7636 (ErrorWrapper_t1_788 * __this, int32_t ___errorCode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.ErrorWrapper::.ctor(System.Object)
extern "C" void ErrorWrapper__ctor_m1_7637 (ErrorWrapper_t1_788 * __this, Object_t * ___errorCode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.InteropServices.ErrorWrapper::get_ErrorCode()
extern "C" int32_t ErrorWrapper_get_ErrorCode_m1_7638 (ErrorWrapper_t1_788 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
