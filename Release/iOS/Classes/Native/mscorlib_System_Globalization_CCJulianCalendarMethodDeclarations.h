﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Globalization.CCJulianCalendar
struct CCJulianCalendar_t1_346;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Globalization.CCJulianCalendar::.ctor()
extern "C" void CCJulianCalendar__ctor_m1_3739 (CCJulianCalendar_t1_346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.CCJulianCalendar::is_leap_year(System.Int32)
extern "C" bool CCJulianCalendar_is_leap_year_m1_3740 (Object_t * __this /* static, unused */, int32_t ___year, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCJulianCalendar::fixed_from_dmy(System.Int32,System.Int32,System.Int32)
extern "C" int32_t CCJulianCalendar_fixed_from_dmy_m1_3741 (Object_t * __this /* static, unused */, int32_t ___day, int32_t ___month, int32_t ___year, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCJulianCalendar::year_from_fixed(System.Int32)
extern "C" int32_t CCJulianCalendar_year_from_fixed_m1_3742 (Object_t * __this /* static, unused */, int32_t ___date, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.CCJulianCalendar::my_from_fixed(System.Int32&,System.Int32&,System.Int32)
extern "C" void CCJulianCalendar_my_from_fixed_m1_3743 (Object_t * __this /* static, unused */, int32_t* ___month, int32_t* ___year, int32_t ___date, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.CCJulianCalendar::dmy_from_fixed(System.Int32&,System.Int32&,System.Int32&,System.Int32)
extern "C" void CCJulianCalendar_dmy_from_fixed_m1_3744 (Object_t * __this /* static, unused */, int32_t* ___day, int32_t* ___month, int32_t* ___year, int32_t ___date, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCJulianCalendar::month_from_fixed(System.Int32)
extern "C" int32_t CCJulianCalendar_month_from_fixed_m1_3745 (Object_t * __this /* static, unused */, int32_t ___date, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCJulianCalendar::day_from_fixed(System.Int32)
extern "C" int32_t CCJulianCalendar_day_from_fixed_m1_3746 (Object_t * __this /* static, unused */, int32_t ___date, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCJulianCalendar::date_difference(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" int32_t CCJulianCalendar_date_difference_m1_3747 (Object_t * __this /* static, unused */, int32_t ___dayA, int32_t ___monthA, int32_t ___yearA, int32_t ___dayB, int32_t ___monthB, int32_t ___yearB, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCJulianCalendar::day_number(System.Int32,System.Int32,System.Int32)
extern "C" int32_t CCJulianCalendar_day_number_m1_3748 (Object_t * __this /* static, unused */, int32_t ___day, int32_t ___month, int32_t ___year, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCJulianCalendar::days_remaining(System.Int32,System.Int32,System.Int32)
extern "C" int32_t CCJulianCalendar_days_remaining_m1_3749 (Object_t * __this /* static, unused */, int32_t ___day, int32_t ___month, int32_t ___year, const MethodInfo* method) IL2CPP_METHOD_ATTR;
