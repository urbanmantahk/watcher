﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.InteropServices.IDispatchImplAttribute
struct IDispatchImplAttribute_t1_799;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_InteropServices_IDispatchImplType.h"

// System.Void System.Runtime.InteropServices.IDispatchImplAttribute::.ctor(System.Runtime.InteropServices.IDispatchImplType)
extern "C" void IDispatchImplAttribute__ctor_m1_7677 (IDispatchImplAttribute_t1_799 * __this, int32_t ___implType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.IDispatchImplAttribute::.ctor(System.Int16)
extern "C" void IDispatchImplAttribute__ctor_m1_7678 (IDispatchImplAttribute_t1_799 * __this, int16_t ___implType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.InteropServices.IDispatchImplType System.Runtime.InteropServices.IDispatchImplAttribute::get_Value()
extern "C" int32_t IDispatchImplAttribute_get_Value_m1_7679 (IDispatchImplAttribute_t1_799 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
