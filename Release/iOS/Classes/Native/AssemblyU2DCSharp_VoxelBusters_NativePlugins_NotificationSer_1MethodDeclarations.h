﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.NotificationService/<ProcessAppLaunchInfo>c__Iterator5
struct U3CProcessAppLaunchInfoU3Ec__Iterator5_t8_260;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.NotificationService/<ProcessAppLaunchInfo>c__Iterator5::.ctor()
extern "C" void U3CProcessAppLaunchInfoU3Ec__Iterator5__ctor_m8_1431 (U3CProcessAppLaunchInfoU3Ec__Iterator5_t8_260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VoxelBusters.NativePlugins.NotificationService/<ProcessAppLaunchInfo>c__Iterator5::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CProcessAppLaunchInfoU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m8_1432 (U3CProcessAppLaunchInfoU3Ec__Iterator5_t8_260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VoxelBusters.NativePlugins.NotificationService/<ProcessAppLaunchInfo>c__Iterator5::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CProcessAppLaunchInfoU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m8_1433 (U3CProcessAppLaunchInfoU3Ec__Iterator5_t8_260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.NativePlugins.NotificationService/<ProcessAppLaunchInfo>c__Iterator5::MoveNext()
extern "C" bool U3CProcessAppLaunchInfoU3Ec__Iterator5_MoveNext_m8_1434 (U3CProcessAppLaunchInfoU3Ec__Iterator5_t8_260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationService/<ProcessAppLaunchInfo>c__Iterator5::Dispose()
extern "C" void U3CProcessAppLaunchInfoU3Ec__Iterator5_Dispose_m8_1435 (U3CProcessAppLaunchInfoU3Ec__Iterator5_t8_260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationService/<ProcessAppLaunchInfo>c__Iterator5::Reset()
extern "C" void U3CProcessAppLaunchInfoU3Ec__Iterator5_Reset_m8_1436 (U3CProcessAppLaunchInfoU3Ec__Iterator5_t8_260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
