﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_67.h"
#include "mscorlib_System_Reflection_Emit_Label.h"

// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.Label>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_16245_gshared (InternalEnumerator_1_t1_2059 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1_16245(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1_2059 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m1_16245_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.Label>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_16246_gshared (InternalEnumerator_1_t1_2059 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_16246(__this, method) (( void (*) (InternalEnumerator_1_t1_2059 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_16246_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.Label>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_16247_gshared (InternalEnumerator_1_t1_2059 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_16247(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1_2059 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_16247_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.Label>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_16248_gshared (InternalEnumerator_1_t1_2059 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1_16248(__this, method) (( void (*) (InternalEnumerator_1_t1_2059 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1_16248_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.Label>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_16249_gshared (InternalEnumerator_1_t1_2059 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1_16249(__this, method) (( bool (*) (InternalEnumerator_1_t1_2059 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1_16249_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.Label>::get_Current()
extern "C" Label_t1_513  InternalEnumerator_1_get_Current_m1_16250_gshared (InternalEnumerator_1_t1_2059 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1_16250(__this, method) (( Label_t1_513  (*) (InternalEnumerator_1_t1_2059 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1_16250_gshared)(__this, method)
