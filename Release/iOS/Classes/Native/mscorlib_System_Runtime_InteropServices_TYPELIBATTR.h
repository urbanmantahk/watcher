﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType.h"
#include "mscorlib_System_Guid.h"
#include "mscorlib_System_Runtime_InteropServices_SYSKIND.h"
#include "mscorlib_System_Runtime_InteropServices_LIBFLAGS.h"

// System.Runtime.InteropServices.TYPELIBATTR
struct  TYPELIBATTR_t1_830 
{
	// System.Guid System.Runtime.InteropServices.TYPELIBATTR::guid
	Guid_t1_319  ___guid_0;
	// System.Int32 System.Runtime.InteropServices.TYPELIBATTR::lcid
	int32_t ___lcid_1;
	// System.Runtime.InteropServices.SYSKIND System.Runtime.InteropServices.TYPELIBATTR::syskind
	int32_t ___syskind_2;
	// System.Int16 System.Runtime.InteropServices.TYPELIBATTR::wMajorVerNum
	int16_t ___wMajorVerNum_3;
	// System.Int16 System.Runtime.InteropServices.TYPELIBATTR::wMinorVerNum
	int16_t ___wMinorVerNum_4;
	// System.Runtime.InteropServices.LIBFLAGS System.Runtime.InteropServices.TYPELIBATTR::wLibFlags
	int32_t ___wLibFlags_5;
};
// Native definition for marshalling of: System.Runtime.InteropServices.TYPELIBATTR
struct TYPELIBATTR_t1_830_marshaled
{
	Guid_t1_319  ___guid_0;
	int32_t ___lcid_1;
	int32_t ___syskind_2;
	int16_t ___wMajorVerNum_3;
	int16_t ___wMinorVerNum_4;
	int32_t ___wLibFlags_5;
};
