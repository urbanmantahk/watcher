﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Text.DecoderFallbackException
struct DecoderFallbackException_t1_1424;
// System.String
struct String_t;
// System.Exception
struct Exception_t1_33;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Text.DecoderFallbackException::.ctor()
extern "C" void DecoderFallbackException__ctor_m1_12231 (DecoderFallbackException_t1_1424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.DecoderFallbackException::.ctor(System.String)
extern "C" void DecoderFallbackException__ctor_m1_12232 (DecoderFallbackException_t1_1424 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.DecoderFallbackException::.ctor(System.String,System.Exception)
extern "C" void DecoderFallbackException__ctor_m1_12233 (DecoderFallbackException_t1_1424 * __this, String_t* ___message, Exception_t1_33 * ___innerException, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.DecoderFallbackException::.ctor(System.String,System.Byte[],System.Int32)
extern "C" void DecoderFallbackException__ctor_m1_12234 (DecoderFallbackException_t1_1424 * __this, String_t* ___message, ByteU5BU5D_t1_109* ___bytesUnknown, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Text.DecoderFallbackException::get_BytesUnknown()
extern "C" ByteU5BU5D_t1_109* DecoderFallbackException_get_BytesUnknown_m1_12235 (DecoderFallbackException_t1_1424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.DecoderFallbackException::get_Index()
extern "C" int32_t DecoderFallbackException_get_Index_m1_12236 (DecoderFallbackException_t1_1424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
