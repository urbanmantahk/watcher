﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.Emit.DynamicMethod
struct DynamicMethod_t1_496;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t1_31;
// System.Reflection.Module
struct Module_t1_495;
// System.Delegate
struct Delegate_t1_22;
// System.Object
struct Object_t;
// System.Reflection.Emit.ParameterBuilder
struct ParameterBuilder_t1_545;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Reflection.Emit.DynamicILInfo
struct DynamicILInfo_t1_493;
// System.Reflection.Emit.ILGenerator
struct ILGenerator_t1_480;
// System.Reflection.ParameterInfo[]
struct ParameterInfoU5BU5D_t1_1685;
// System.Reflection.Binder
struct Binder_t1_585;
// System.Globalization.CultureInfo
struct CultureInfo_t1_277;
// System.Reflection.ParameterInfo
struct ParameterInfo_t1_627;
// System.Reflection.ICustomAttributeProvider
struct ICustomAttributeProvider_t1_1686;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_MethodAttributes.h"
#include "mscorlib_System_Reflection_CallingConventions.h"
#include "mscorlib_System_Reflection_ParameterAttributes.h"
#include "mscorlib_System_Reflection_MethodImplAttributes.h"
#include "mscorlib_System_Reflection_BindingFlags.h"
#include "mscorlib_System_RuntimeMethodHandle.h"

// System.Void System.Reflection.Emit.DynamicMethod::.ctor(System.String,System.Type,System.Type[],System.Reflection.Module)
extern "C" void DynamicMethod__ctor_m1_5690 (DynamicMethod_t1_496 * __this, String_t* ___name, Type_t * ___returnType, TypeU5BU5D_t1_31* ___parameterTypes, Module_t1_495 * ___m, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.DynamicMethod::.ctor(System.String,System.Type,System.Type[],System.Type)
extern "C" void DynamicMethod__ctor_m1_5691 (DynamicMethod_t1_496 * __this, String_t* ___name, Type_t * ___returnType, TypeU5BU5D_t1_31* ___parameterTypes, Type_t * ___owner, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.DynamicMethod::.ctor(System.String,System.Type,System.Type[],System.Reflection.Module,System.Boolean)
extern "C" void DynamicMethod__ctor_m1_5692 (DynamicMethod_t1_496 * __this, String_t* ___name, Type_t * ___returnType, TypeU5BU5D_t1_31* ___parameterTypes, Module_t1_495 * ___m, bool ___skipVisibility, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.DynamicMethod::.ctor(System.String,System.Type,System.Type[],System.Type,System.Boolean)
extern "C" void DynamicMethod__ctor_m1_5693 (DynamicMethod_t1_496 * __this, String_t* ___name, Type_t * ___returnType, TypeU5BU5D_t1_31* ___parameterTypes, Type_t * ___owner, bool ___skipVisibility, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.DynamicMethod::.ctor(System.String,System.Reflection.MethodAttributes,System.Reflection.CallingConventions,System.Type,System.Type[],System.Type,System.Boolean)
extern "C" void DynamicMethod__ctor_m1_5694 (DynamicMethod_t1_496 * __this, String_t* ___name, int32_t ___attributes, int32_t ___callingConvention, Type_t * ___returnType, TypeU5BU5D_t1_31* ___parameterTypes, Type_t * ___owner, bool ___skipVisibility, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.DynamicMethod::.ctor(System.String,System.Reflection.MethodAttributes,System.Reflection.CallingConventions,System.Type,System.Type[],System.Reflection.Module,System.Boolean)
extern "C" void DynamicMethod__ctor_m1_5695 (DynamicMethod_t1_496 * __this, String_t* ___name, int32_t ___attributes, int32_t ___callingConvention, Type_t * ___returnType, TypeU5BU5D_t1_31* ___parameterTypes, Module_t1_495 * ___m, bool ___skipVisibility, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.DynamicMethod::.ctor(System.String,System.Type,System.Type[])
extern "C" void DynamicMethod__ctor_m1_5696 (DynamicMethod_t1_496 * __this, String_t* ___name, Type_t * ___returnType, TypeU5BU5D_t1_31* ___parameterTypes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.DynamicMethod::.ctor(System.String,System.Type,System.Type[],System.Boolean)
extern "C" void DynamicMethod__ctor_m1_5697 (DynamicMethod_t1_496 * __this, String_t* ___name, Type_t * ___returnType, TypeU5BU5D_t1_31* ___parameterTypes, bool ___restrictedSkipVisibility, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.DynamicMethod::.ctor(System.String,System.Reflection.MethodAttributes,System.Reflection.CallingConventions,System.Type,System.Type[],System.Type,System.Reflection.Module,System.Boolean,System.Boolean)
extern "C" void DynamicMethod__ctor_m1_5698 (DynamicMethod_t1_496 * __this, String_t* ___name, int32_t ___attributes, int32_t ___callingConvention, Type_t * ___returnType, TypeU5BU5D_t1_31* ___parameterTypes, Type_t * ___owner, Module_t1_495 * ___m, bool ___skipVisibility, bool ___anonHosted, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.DynamicMethod::create_dynamic_method(System.Reflection.Emit.DynamicMethod)
extern "C" void DynamicMethod_create_dynamic_method_m1_5699 (DynamicMethod_t1_496 * __this, DynamicMethod_t1_496 * ___m, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.DynamicMethod::destroy_dynamic_method(System.Reflection.Emit.DynamicMethod)
extern "C" void DynamicMethod_destroy_dynamic_method_m1_5700 (DynamicMethod_t1_496 * __this, DynamicMethod_t1_496 * ___m, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.DynamicMethod::CreateDynMethod()
extern "C" void DynamicMethod_CreateDynMethod_m1_5701 (DynamicMethod_t1_496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.DynamicMethod::Finalize()
extern "C" void DynamicMethod_Finalize_m1_5702 (DynamicMethod_t1_496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Reflection.Emit.DynamicMethod::CreateDelegate(System.Type)
extern "C" Delegate_t1_22 * DynamicMethod_CreateDelegate_m1_5703 (DynamicMethod_t1_496 * __this, Type_t * ___delegateType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Reflection.Emit.DynamicMethod::CreateDelegate(System.Type,System.Object)
extern "C" Delegate_t1_22 * DynamicMethod_CreateDelegate_m1_5704 (DynamicMethod_t1_496 * __this, Type_t * ___delegateType, Object_t * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.ParameterBuilder System.Reflection.Emit.DynamicMethod::DefineParameter(System.Int32,System.Reflection.ParameterAttributes,System.String)
extern "C" ParameterBuilder_t1_545 * DynamicMethod_DefineParameter_m1_5705 (DynamicMethod_t1_496 * __this, int32_t ___position, int32_t ___attributes, String_t* ___parameterName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Reflection.Emit.DynamicMethod::GetBaseDefinition()
extern "C" MethodInfo_t * DynamicMethod_GetBaseDefinition_m1_5706 (DynamicMethod_t1_496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Reflection.Emit.DynamicMethod::GetCustomAttributes(System.Boolean)
extern "C" ObjectU5BU5D_t1_272* DynamicMethod_GetCustomAttributes_m1_5707 (DynamicMethod_t1_496 * __this, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Reflection.Emit.DynamicMethod::GetCustomAttributes(System.Type,System.Boolean)
extern "C" ObjectU5BU5D_t1_272* DynamicMethod_GetCustomAttributes_m1_5708 (DynamicMethod_t1_496 * __this, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.DynamicILInfo System.Reflection.Emit.DynamicMethod::GetDynamicILInfo()
extern "C" DynamicILInfo_t1_493 * DynamicMethod_GetDynamicILInfo_m1_5709 (DynamicMethod_t1_496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.ILGenerator System.Reflection.Emit.DynamicMethod::GetILGenerator()
extern "C" ILGenerator_t1_480 * DynamicMethod_GetILGenerator_m1_5710 (DynamicMethod_t1_496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.ILGenerator System.Reflection.Emit.DynamicMethod::GetILGenerator(System.Int32)
extern "C" ILGenerator_t1_480 * DynamicMethod_GetILGenerator_m1_5711 (DynamicMethod_t1_496 * __this, int32_t ___streamSize, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodImplAttributes System.Reflection.Emit.DynamicMethod::GetMethodImplementationFlags()
extern "C" int32_t DynamicMethod_GetMethodImplementationFlags_m1_5712 (DynamicMethod_t1_496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ParameterInfo[] System.Reflection.Emit.DynamicMethod::GetParameters()
extern "C" ParameterInfoU5BU5D_t1_1685* DynamicMethod_GetParameters_m1_5713 (DynamicMethod_t1_496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Reflection.Emit.DynamicMethod::Invoke(System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo)
extern "C" Object_t * DynamicMethod_Invoke_m1_5714 (DynamicMethod_t1_496 * __this, Object_t * ___obj, int32_t ___invokeAttr, Binder_t1_585 * ___binder, ObjectU5BU5D_t1_272* ___parameters, CultureInfo_t1_277 * ___culture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.DynamicMethod::IsDefined(System.Type,System.Boolean)
extern "C" bool DynamicMethod_IsDefined_m1_5715 (DynamicMethod_t1_496 * __this, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Emit.DynamicMethod::ToString()
extern "C" String_t* DynamicMethod_ToString_m1_5716 (DynamicMethod_t1_496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodAttributes System.Reflection.Emit.DynamicMethod::get_Attributes()
extern "C" int32_t DynamicMethod_get_Attributes_m1_5717 (DynamicMethod_t1_496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.CallingConventions System.Reflection.Emit.DynamicMethod::get_CallingConvention()
extern "C" int32_t DynamicMethod_get_CallingConvention_m1_5718 (DynamicMethod_t1_496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.DynamicMethod::get_DeclaringType()
extern "C" Type_t * DynamicMethod_get_DeclaringType_m1_5719 (DynamicMethod_t1_496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.DynamicMethod::get_InitLocals()
extern "C" bool DynamicMethod_get_InitLocals_m1_5720 (DynamicMethod_t1_496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.DynamicMethod::set_InitLocals(System.Boolean)
extern "C" void DynamicMethod_set_InitLocals_m1_5721 (DynamicMethod_t1_496 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.RuntimeMethodHandle System.Reflection.Emit.DynamicMethod::get_MethodHandle()
extern "C" RuntimeMethodHandle_t1_479  DynamicMethod_get_MethodHandle_m1_5722 (DynamicMethod_t1_496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Module System.Reflection.Emit.DynamicMethod::get_Module()
extern "C" Module_t1_495 * DynamicMethod_get_Module_m1_5723 (DynamicMethod_t1_496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Emit.DynamicMethod::get_Name()
extern "C" String_t* DynamicMethod_get_Name_m1_5724 (DynamicMethod_t1_496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.DynamicMethod::get_ReflectedType()
extern "C" Type_t * DynamicMethod_get_ReflectedType_m1_5725 (DynamicMethod_t1_496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ParameterInfo System.Reflection.Emit.DynamicMethod::get_ReturnParameter()
extern "C" ParameterInfo_t1_627 * DynamicMethod_get_ReturnParameter_m1_5726 (DynamicMethod_t1_496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.DynamicMethod::get_ReturnType()
extern "C" Type_t * DynamicMethod_get_ReturnType_m1_5727 (DynamicMethod_t1_496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ICustomAttributeProvider System.Reflection.Emit.DynamicMethod::get_ReturnTypeCustomAttributes()
extern "C" Object_t * DynamicMethod_get_ReturnTypeCustomAttributes_m1_5728 (DynamicMethod_t1_496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.DynamicMethod::RejectIfCreated()
extern "C" void DynamicMethod_RejectIfCreated_m1_5729 (DynamicMethod_t1_496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.DynamicMethod::AddRef(System.Object)
extern "C" int32_t DynamicMethod_AddRef_m1_5730 (DynamicMethod_t1_496 * __this, Object_t * ___reference, const MethodInfo* method) IL2CPP_METHOD_ATTR;
