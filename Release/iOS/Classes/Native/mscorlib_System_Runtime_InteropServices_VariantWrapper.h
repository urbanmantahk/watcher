﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Object_t;

#include "mscorlib_System_Object.h"

// System.Runtime.InteropServices.VariantWrapper
struct  VariantWrapper_t1_849  : public Object_t
{
	// System.Object System.Runtime.InteropServices.VariantWrapper::_wrappedObject
	Object_t * ____wrappedObject_0;
};
