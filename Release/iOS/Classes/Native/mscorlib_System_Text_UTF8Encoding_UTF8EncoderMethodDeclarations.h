﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Text.UTF8Encoding/UTF8Encoder
struct UTF8Encoder_t1_1452;
// System.Char[]
struct CharU5BU5D_t1_16;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Text.UTF8Encoding/UTF8Encoder::.ctor(System.Boolean)
extern "C" void UTF8Encoder__ctor_m1_12543 (UTF8Encoder_t1_1452 * __this, bool ___emitIdentifier, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.UTF8Encoding/UTF8Encoder::GetByteCount(System.Char[],System.Int32,System.Int32,System.Boolean)
extern "C" int32_t UTF8Encoder_GetByteCount_m1_12544 (UTF8Encoder_t1_1452 * __this, CharU5BU5D_t1_16* ___chars, int32_t ___index, int32_t ___count, bool ___flush, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.UTF8Encoding/UTF8Encoder::GetBytes(System.Char[],System.Int32,System.Int32,System.Byte[],System.Int32,System.Boolean)
extern "C" int32_t UTF8Encoder_GetBytes_m1_12545 (UTF8Encoder_t1_1452 * __this, CharU5BU5D_t1_16* ___chars, int32_t ___charIndex, int32_t ___charCount, ByteU5BU5D_t1_109* ___bytes, int32_t ___byteIndex, bool ___flush, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.UTF8Encoding/UTF8Encoder::GetByteCount(System.Char*,System.Int32,System.Boolean)
extern "C" int32_t UTF8Encoder_GetByteCount_m1_12546 (UTF8Encoder_t1_1452 * __this, uint16_t* ___chars, int32_t ___count, bool ___flush, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.UTF8Encoding/UTF8Encoder::GetBytes(System.Char*,System.Int32,System.Byte*,System.Int32,System.Boolean)
extern "C" int32_t UTF8Encoder_GetBytes_m1_12547 (UTF8Encoder_t1_1452 * __this, uint16_t* ___chars, int32_t ___charCount, uint8_t* ___bytes, int32_t ___byteCount, bool ___flush, const MethodInfo* method) IL2CPP_METHOD_ATTR;
