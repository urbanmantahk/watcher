﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.CompilerServices.InternalsVisibleToAttribute
struct InternalsVisibleToAttribute_t1_58;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.CompilerServices.InternalsVisibleToAttribute::.ctor(System.String)
extern "C" void InternalsVisibleToAttribute__ctor_m1_1327 (InternalsVisibleToAttribute_t1_58 * __this, String_t* ___assemblyName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.CompilerServices.InternalsVisibleToAttribute::get_AssemblyName()
extern "C" String_t* InternalsVisibleToAttribute_get_AssemblyName_m1_1328 (InternalsVisibleToAttribute_t1_58 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.CompilerServices.InternalsVisibleToAttribute::get_AllInternalsVisible()
extern "C" bool InternalsVisibleToAttribute_get_AllInternalsVisible_m1_1329 (InternalsVisibleToAttribute_t1_58 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.CompilerServices.InternalsVisibleToAttribute::set_AllInternalsVisible(System.Boolean)
extern "C" void InternalsVisibleToAttribute_set_AllInternalsVisible_m1_1330 (InternalsVisibleToAttribute_t1_58 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
