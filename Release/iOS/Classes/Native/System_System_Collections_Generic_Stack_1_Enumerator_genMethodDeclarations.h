﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Stack`1<System.Object>
struct Stack_1_t3_250;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Stack_1_Enumerator_gen.h"

// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.Stack`1<T>)
extern "C" void Enumerator__ctor_m3_1836_gshared (Enumerator_t3_251 * __this, Stack_1_t3_250 * ___t, const MethodInfo* method);
#define Enumerator__ctor_m3_1836(__this, ___t, method) (( void (*) (Enumerator_t3_251 *, Stack_1_t3_250 *, const MethodInfo*))Enumerator__ctor_m3_1836_gshared)(__this, ___t, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Object>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m3_1837_gshared (Enumerator_t3_251 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3_1837(__this, method) (( void (*) (Enumerator_t3_251 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3_1837_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m3_1838_gshared (Enumerator_t3_251 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3_1838(__this, method) (( Object_t * (*) (Enumerator_t3_251 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3_1838_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m3_1839_gshared (Enumerator_t3_251 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3_1839(__this, method) (( void (*) (Enumerator_t3_251 *, const MethodInfo*))Enumerator_Dispose_m3_1839_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1/Enumerator<System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m3_1840_gshared (Enumerator_t3_251 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3_1840(__this, method) (( bool (*) (Enumerator_t3_251 *, const MethodInfo*))Enumerator_MoveNext_m3_1840_gshared)(__this, method)
// T System.Collections.Generic.Stack`1/Enumerator<System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m3_1841_gshared (Enumerator_t3_251 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3_1841(__this, method) (( Object_t * (*) (Enumerator_t3_251 *, const MethodInfo*))Enumerator_get_Current_m3_1841_gshared)(__this, method)
