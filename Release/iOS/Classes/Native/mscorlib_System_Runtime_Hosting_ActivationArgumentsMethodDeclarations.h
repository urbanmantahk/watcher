﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Hosting.ActivationArguments
struct ActivationArguments_t1_716;
// System.ActivationContext
struct ActivationContext_t1_717;
// System.ApplicationIdentity
struct ApplicationIdentity_t1_718;
// System.String[]
struct StringU5BU5D_t1_238;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Hosting.ActivationArguments::.ctor(System.ActivationContext)
extern "C" void ActivationArguments__ctor_m1_7564 (ActivationArguments_t1_716 * __this, ActivationContext_t1_717 * ___activationData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Hosting.ActivationArguments::.ctor(System.ApplicationIdentity)
extern "C" void ActivationArguments__ctor_m1_7565 (ActivationArguments_t1_716 * __this, ApplicationIdentity_t1_718 * ___applicationIdentity, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Hosting.ActivationArguments::.ctor(System.ActivationContext,System.String[])
extern "C" void ActivationArguments__ctor_m1_7566 (ActivationArguments_t1_716 * __this, ActivationContext_t1_717 * ___activationContext, StringU5BU5D_t1_238* ___activationData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Hosting.ActivationArguments::.ctor(System.ApplicationIdentity,System.String[])
extern "C" void ActivationArguments__ctor_m1_7567 (ActivationArguments_t1_716 * __this, ApplicationIdentity_t1_718 * ___applicationIdentity, StringU5BU5D_t1_238* ___activationData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ActivationContext System.Runtime.Hosting.ActivationArguments::get_ActivationContext()
extern "C" ActivationContext_t1_717 * ActivationArguments_get_ActivationContext_m1_7568 (ActivationArguments_t1_716 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.Runtime.Hosting.ActivationArguments::get_ActivationData()
extern "C" StringU5BU5D_t1_238* ActivationArguments_get_ActivationData_m1_7569 (ActivationArguments_t1_716 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ApplicationIdentity System.Runtime.Hosting.ActivationArguments::get_ApplicationIdentity()
extern "C" ApplicationIdentity_t1_718 * ActivationArguments_get_ApplicationIdentity_m1_7570 (ActivationArguments_t1_716 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
