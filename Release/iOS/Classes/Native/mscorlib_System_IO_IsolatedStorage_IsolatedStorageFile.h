﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Security.Policy.Evidence
struct Evidence_t1_398;
// System.Threading.Mutex
struct Mutex_t1_400;
// System.IO.DirectoryInfo
struct DirectoryInfo_t1_399;

#include "mscorlib_System_IO_IsolatedStorage_IsolatedStorage.h"

// System.IO.IsolatedStorage.IsolatedStorageFile
struct  IsolatedStorageFile_t1_397  : public IsolatedStorage_t1_394
{
	// System.Boolean System.IO.IsolatedStorage.IsolatedStorageFile::_resolved
	bool ____resolved_5;
	// System.UInt64 System.IO.IsolatedStorage.IsolatedStorageFile::_maxSize
	uint64_t ____maxSize_6;
	// System.Security.Policy.Evidence System.IO.IsolatedStorage.IsolatedStorageFile::_fullEvidences
	Evidence_t1_398 * ____fullEvidences_7;
	// System.IO.DirectoryInfo System.IO.IsolatedStorage.IsolatedStorageFile::directory
	DirectoryInfo_t1_399 * ___directory_9;
};
struct IsolatedStorageFile_t1_397_StaticFields{
	// System.Threading.Mutex System.IO.IsolatedStorage.IsolatedStorageFile::mutex
	Mutex_t1_400 * ___mutex_8;
};
