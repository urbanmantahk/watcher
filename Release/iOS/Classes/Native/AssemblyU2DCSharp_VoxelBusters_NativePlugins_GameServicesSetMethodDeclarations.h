﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.GameServicesSettings/AndroidSettings
struct AndroidSettings_t8_236;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1_238;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.GameServicesSettings/AndroidSettings::.ctor()
extern "C" void AndroidSettings__ctor_m8_1330 (AndroidSettings_t8_236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.GameServicesSettings/AndroidSettings::get_PlayServicesApplicationID()
extern "C" String_t* AndroidSettings_get_PlayServicesApplicationID_m8_1331 (AndroidSettings_t8_236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.GameServicesSettings/AndroidSettings::set_PlayServicesApplicationID(System.String)
extern "C" void AndroidSettings_set_PlayServicesApplicationID_m8_1332 (AndroidSettings_t8_236 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] VoxelBusters.NativePlugins.GameServicesSettings/AndroidSettings::get_AchievedDescriptionFormats()
extern "C" StringU5BU5D_t1_238* AndroidSettings_get_AchievedDescriptionFormats_m8_1333 (AndroidSettings_t8_236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.GameServicesSettings/AndroidSettings::set_AchievedDescriptionFormats(System.String[])
extern "C" void AndroidSettings_set_AchievedDescriptionFormats_m8_1334 (AndroidSettings_t8_236 * __this, StringU5BU5D_t1_238* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
