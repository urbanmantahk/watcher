﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Security_Permissions_ReflectionPermissionFla.h"

// System.Security.Permissions.ReflectionPermissionFlag
struct  ReflectionPermissionFlag_t1_1303 
{
	// System.Int32 System.Security.Permissions.ReflectionPermissionFlag::value__
	int32_t ___value___1;
};
