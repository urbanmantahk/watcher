﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "mscorlib_System_Array.h"

#pragma once
// Boomlagoon.JSON.JSONValue[]
// Boomlagoon.JSON.JSONValue[]
struct JSONValueU5BU5D_t8_370  : public Array_t { };
// VoxelBusters.AssetStoreProductUtility.Demo.DemoSubMenu[]
// VoxelBusters.AssetStoreProductUtility.Demo.DemoSubMenu[]
struct DemoSubMenuU5BU5D_t8_17  : public Array_t { };
// VoxelBusters.AssetStoreProductUtility.Demo.DemoGUIWindow[]
// VoxelBusters.AssetStoreProductUtility.Demo.DemoGUIWindow[]
struct DemoGUIWindowU5BU5D_t8_388  : public Array_t { };
// VoxelBusters.Utility.GUIModalWindow[]
// VoxelBusters.Utility.GUIModalWindow[]
struct GUIModalWindowU5BU5D_t8_389  : public Array_t { };
// VoxelBusters.DesignPatterns.IObserver[]
// VoxelBusters.DesignPatterns.IObserver[]
struct IObserverU5BU5D_t8_373  : public Array_t { };
// VoxelBusters.Utility.ShaderUtility/ShaderInfo[]
// VoxelBusters.Utility.ShaderUtility/ShaderInfo[]
struct ShaderInfoU5BU5D_t8_376  : public Array_t { };
// VoxelBusters.Utility.ShaderUtility/ShaderProperty[]
// VoxelBusters.Utility.ShaderUtility/ShaderProperty[]
struct ShaderPropertyU5BU5D_t8_377  : public Array_t { };
// ExifLibrary.MathEx/UFraction32[]
// ExifLibrary.MathEx/UFraction32[]
struct UFraction32U5BU5D_t8_122  : public Array_t { };
// ExifLibrary.MathEx/Fraction32[]
// ExifLibrary.MathEx/Fraction32[]
struct Fraction32U5BU5D_t8_129  : public Array_t { };
// ExifLibrary.ExifTag[]
// ExifLibrary.ExifTag[]
struct ExifTagU5BU5D_t8_379  : public Array_t { };
// ExifLibrary.ExifProperty[]
// ExifLibrary.ExifProperty[]
struct ExifPropertyU5BU5D_t8_380  : public Array_t { };
// ExifLibrary.IFD[]
// ExifLibrary.IFD[]
struct IFDU5BU5D_t8_387  : public Array_t { };
// ExifLibrary.JPEGSection[]
// ExifLibrary.JPEGSection[]
struct JPEGSectionU5BU5D_t8_381  : public Array_t { };
// VoxelBusters.Utility.UnityGUI.MENU.GUISubMenu[]
// VoxelBusters.Utility.UnityGUI.MENU.GUISubMenu[]
struct GUISubMenuU5BU5D_t8_142  : public Array_t { };
// VoxelBusters.Utility.UnityGUI.MENU.GUIMenuBase[]
// VoxelBusters.Utility.UnityGUI.MENU.GUIMenuBase[]
struct GUIMenuBaseU5BU5D_t8_390  : public Array_t { };
// VoxelBusters.DebugPRO.Internal.ConsoleLog[]
// VoxelBusters.DebugPRO.Internal.ConsoleLog[]
struct ConsoleLogU5BU5D_t8_382  : public Array_t { };
// VoxelBusters.DebugPRO.Internal.ConsoleTag[]
// VoxelBusters.DebugPRO.Internal.ConsoleTag[]
struct ConsoleTagU5BU5D_t8_383  : public Array_t { };
// VoxelBusters.NativePlugins.AddressBookContact[]
// VoxelBusters.NativePlugins.AddressBookContact[]
struct AddressBookContactU5BU5D_t8_177  : public Array_t { };
// VoxelBusters.NativePlugins.eShareOptions[]
// VoxelBusters.NativePlugins.eShareOptions[]
struct eShareOptionsU5BU5D_t8_186  : public Array_t { };
// VoxelBusters.NativePlugins.PlatformID[]
// VoxelBusters.NativePlugins.PlatformID[]
struct PlatformIDU5BU5D_t8_329  : public Array_t { };
// VoxelBusters.NativePlugins.BillingProduct[]
// VoxelBusters.NativePlugins.BillingProduct[]
struct BillingProductU5BU5D_t8_337  : public Array_t { };
// VoxelBusters.NativePlugins.Achievement[]
// VoxelBusters.NativePlugins.Achievement[]
struct AchievementU5BU5D_t8_219  : public Array_t { };
// VoxelBusters.NativePlugins.Internal.NPObject[]
// VoxelBusters.NativePlugins.Internal.NPObject[]
struct NPObjectU5BU5D_t8_386  : public Array_t { };
// VoxelBusters.NativePlugins.AchievementDescription[]
// VoxelBusters.NativePlugins.AchievementDescription[]
struct AchievementDescriptionU5BU5D_t8_224  : public Array_t { };
// VoxelBusters.NativePlugins.Score[]
// VoxelBusters.NativePlugins.Score[]
struct ScoreU5BU5D_t8_230  : public Array_t { };
// VoxelBusters.NativePlugins.User[]
// VoxelBusters.NativePlugins.User[]
struct UserU5BU5D_t8_234  : public Array_t { };
// VoxelBusters.NativePlugins.IDContainer[]
// VoxelBusters.NativePlugins.IDContainer[]
struct IDContainerU5BU5D_t8_239  : public Array_t { };
// VoxelBusters.NativePlugins.UI/AlertDialogCompletion[]
// VoxelBusters.NativePlugins.UI/AlertDialogCompletion[]
struct AlertDialogCompletionU5BU5D_t8_384  : public Array_t { };
// VoxelBusters.NativePlugins.WebView[]
// VoxelBusters.NativePlugins.WebView[]
struct WebViewU5BU5D_t8_385  : public Array_t { };
