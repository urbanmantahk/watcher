﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.CollectionDebuggerView`1<System.Object>
struct CollectionDebuggerView_1_t1_2011;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t1_2012;
// System.Object[]
struct ObjectU5BU5D_t1_272;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Generic.CollectionDebuggerView`1<System.Object>::.ctor(System.Collections.Generic.ICollection`1<T>)
extern "C" void CollectionDebuggerView_1__ctor_m1_15896_gshared (CollectionDebuggerView_1_t1_2011 * __this, Object_t* ___col, const MethodInfo* method);
#define CollectionDebuggerView_1__ctor_m1_15896(__this, ___col, method) (( void (*) (CollectionDebuggerView_1_t1_2011 *, Object_t*, const MethodInfo*))CollectionDebuggerView_1__ctor_m1_15896_gshared)(__this, ___col, method)
// T[] System.Collections.Generic.CollectionDebuggerView`1<System.Object>::get_Items()
extern "C" ObjectU5BU5D_t1_272* CollectionDebuggerView_1_get_Items_m1_15897_gshared (CollectionDebuggerView_1_t1_2011 * __this, const MethodInfo* method);
#define CollectionDebuggerView_1_get_Items_m1_15897(__this, method) (( ObjectU5BU5D_t1_272* (*) (CollectionDebuggerView_1_t1_2011 *, const MethodInfo*))CollectionDebuggerView_1_get_Items_m1_15897_gshared)(__this, method)
