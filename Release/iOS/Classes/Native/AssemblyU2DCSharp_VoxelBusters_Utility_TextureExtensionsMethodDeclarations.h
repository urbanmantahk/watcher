﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// UnityEngine.Texture2D
struct Texture2D_t6_33;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Action`1<UnityEngine.Texture2D>
struct Action_1_t1_1902;
// UnityEngine.Color32[]
struct Color32U5BU5D_t6_280;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_TextureExtensions_Enc.h"
#include "UnityEngine_UnityEngine_Color.h"
#include "UnityEngine_UnityEngine_Vector2.h"

// System.String VoxelBusters.Utility.TextureExtensions::ToString(UnityEngine.Texture2D,VoxelBusters.Utility.TextureExtensions/EncodeTo)
extern "C" String_t* TextureExtensions_ToString_m8_235 (Object_t * __this /* static, unused */, Texture2D_t6_33 * ____img, int32_t ____encodeTo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.Utility.TextureExtensions::Serialise(UnityEngine.Texture2D,VoxelBusters.Utility.TextureExtensions/EncodeTo)
extern "C" String_t* TextureExtensions_Serialise_m8_236 (Object_t * __this /* static, unused */, Texture2D_t6_33 * ____img, int32_t ____encodeTo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D VoxelBusters.Utility.TextureExtensions::Deserialise(System.String)
extern "C" Texture2D_t6_33 * TextureExtensions_Deserialise_m8_237 (Object_t * __this /* static, unused */, String_t* ____strImg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D VoxelBusters.Utility.TextureExtensions::CreateTexture(System.String,System.Int32,System.Int32)
extern "C" Texture2D_t6_33 * TextureExtensions_CreateTexture_m8_238 (Object_t * __this /* static, unused */, String_t* ____texDataB64, int32_t ____width, int32_t ____height, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator VoxelBusters.Utility.TextureExtensions::TakeScreenshot(System.Action`1<UnityEngine.Texture2D>)
extern "C" Object_t * TextureExtensions_TakeScreenshot_m8_239 (Object_t * __this /* static, unused */, Action_1_t1_1902 * ____onCompletionHandler, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D VoxelBusters.Utility.TextureExtensions::Rotate(UnityEngine.Texture2D,System.Single)
extern "C" Texture2D_t6_33 * TextureExtensions_Rotate_m8_240 (Object_t * __this /* static, unused */, Texture2D_t6_33 * ____inputTex, float ____angle, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color VoxelBusters.Utility.TextureExtensions::GetPixel(UnityEngine.Texture2D,UnityEngine.Color32[],UnityEngine.Vector2)
extern "C" Color_t6_40  TextureExtensions_GetPixel_m8_241 (Object_t * __this /* static, unused */, Texture2D_t6_33 * ____inputTex, Color32U5BU5D_t6_280* ____pixels, Vector2_t6_47  ____coordinate, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D VoxelBusters.Utility.TextureExtensions::MirrorTexture(UnityEngine.Texture2D,System.Boolean,System.Boolean)
extern "C" Texture2D_t6_33 * TextureExtensions_MirrorTexture_m8_242 (Object_t * __this /* static, unused */, Texture2D_t6_33 * ____inputTex, bool ____mirrorHorizontally, bool ____mirrorVertically, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D VoxelBusters.Utility.TextureExtensions::Scale(UnityEngine.Texture2D,System.Single)
extern "C" Texture2D_t6_33 * TextureExtensions_Scale_m8_243 (Object_t * __this /* static, unused */, Texture2D_t6_33 * ____inputTex, float ____scaleFactor, const MethodInfo* method) IL2CPP_METHOD_ATTR;
