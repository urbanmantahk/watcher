﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.MonoListItem
struct MonoListItem_t1_1576;
// System.Object
struct Object_t;

#include "mscorlib_System_Object.h"

// System.MonoListItem
struct  MonoListItem_t1_1576  : public Object_t
{
	// System.MonoListItem System.MonoListItem::next
	MonoListItem_t1_1576 * ___next_0;
	// System.Object System.MonoListItem::data
	Object_t * ___data_1;
};
