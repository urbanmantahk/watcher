﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_Microsoft_Win32_RegistryKeyPermissionCheck.h"

// Microsoft.Win32.RegistryKeyPermissionCheck
struct  RegistryKeyPermissionCheck_t1_95 
{
	// System.Int32 Microsoft.Win32.RegistryKeyPermissionCheck::value__
	int32_t ___value___1;
};
