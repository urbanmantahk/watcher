﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Globalization.RegionInfo
struct RegionInfo_t1_385;
// System.String
struct String_t;
// System.Globalization.CultureInfo
struct CultureInfo_t1_277;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Globalization.RegionInfo::.ctor(System.Int32)
extern "C" void RegionInfo__ctor_m1_4420 (RegionInfo_t1_385 * __this, int32_t ___culture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.RegionInfo::.ctor(System.String)
extern "C" void RegionInfo__ctor_m1_4421 (RegionInfo_t1_385 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.RegionInfo System.Globalization.RegionInfo::get_CurrentRegion()
extern "C" RegionInfo_t1_385 * RegionInfo_get_CurrentRegion_m1_4422 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.RegionInfo::GetByTerritory(System.Globalization.CultureInfo)
extern "C" bool RegionInfo_GetByTerritory_m1_4423 (RegionInfo_t1_385 * __this, CultureInfo_t1_277 * ___ci, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.RegionInfo::construct_internal_region_from_name(System.String)
extern "C" bool RegionInfo_construct_internal_region_from_name_m1_4424 (RegionInfo_t1_385 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.RegionInfo::get_CurrencyEnglishName()
extern "C" String_t* RegionInfo_get_CurrencyEnglishName_m1_4425 (RegionInfo_t1_385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.RegionInfo::get_CurrencySymbol()
extern "C" String_t* RegionInfo_get_CurrencySymbol_m1_4426 (RegionInfo_t1_385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.RegionInfo::get_DisplayName()
extern "C" String_t* RegionInfo_get_DisplayName_m1_4427 (RegionInfo_t1_385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.RegionInfo::get_EnglishName()
extern "C" String_t* RegionInfo_get_EnglishName_m1_4428 (RegionInfo_t1_385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.RegionInfo::get_GeoId()
extern "C" int32_t RegionInfo_get_GeoId_m1_4429 (RegionInfo_t1_385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.RegionInfo::get_IsMetric()
extern "C" bool RegionInfo_get_IsMetric_m1_4430 (RegionInfo_t1_385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.RegionInfo::get_ISOCurrencySymbol()
extern "C" String_t* RegionInfo_get_ISOCurrencySymbol_m1_4431 (RegionInfo_t1_385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.RegionInfo::get_NativeName()
extern "C" String_t* RegionInfo_get_NativeName_m1_4432 (RegionInfo_t1_385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.RegionInfo::get_CurrencyNativeName()
extern "C" String_t* RegionInfo_get_CurrencyNativeName_m1_4433 (RegionInfo_t1_385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.RegionInfo::get_Name()
extern "C" String_t* RegionInfo_get_Name_m1_4434 (RegionInfo_t1_385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.RegionInfo::get_ThreeLetterISORegionName()
extern "C" String_t* RegionInfo_get_ThreeLetterISORegionName_m1_4435 (RegionInfo_t1_385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.RegionInfo::get_ThreeLetterWindowsRegionName()
extern "C" String_t* RegionInfo_get_ThreeLetterWindowsRegionName_m1_4436 (RegionInfo_t1_385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.RegionInfo::get_TwoLetterISORegionName()
extern "C" String_t* RegionInfo_get_TwoLetterISORegionName_m1_4437 (RegionInfo_t1_385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.RegionInfo::Equals(System.Object)
extern "C" bool RegionInfo_Equals_m1_4438 (RegionInfo_t1_385 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.RegionInfo::GetHashCode()
extern "C" int32_t RegionInfo_GetHashCode_m1_4439 (RegionInfo_t1_385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.RegionInfo::ToString()
extern "C" String_t* RegionInfo_ToString_m1_4440 (RegionInfo_t1_385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
