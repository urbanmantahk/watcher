﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.Demo.NetworkConnectivityDemo
struct NetworkConnectivityDemo_t8_182;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.Demo.NetworkConnectivityDemo::.ctor()
extern "C" void NetworkConnectivityDemo__ctor_m8_1060 (NetworkConnectivityDemo_t8_182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
