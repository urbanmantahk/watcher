﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.Module
struct Module_t1_495;
// System.Reflection.Assembly
struct Assembly_t1_467;
// System.String
struct String_t;
// System.Type[]
struct TypeU5BU5D_t1_31;
// System.Reflection.TypeFilter
struct TypeFilter_t1_616;
// System.Object
struct Object_t;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Type
struct Type_t;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// System.Reflection.FieldInfo[]
struct FieldInfoU5BU5D_t1_1668;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Reflection.Binder
struct Binder_t1_585;
// System.Reflection.ParameterModifier[]
struct ParameterModifierU5BU5D_t1_1669;
// System.Reflection.MethodInfo[]
struct MethodInfoU5BU5D_t1_603;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;
// System.Exception
struct Exception_t1_33;
// System.IntPtr[]
struct IntPtrU5BU5D_t1_34;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Reflection.MethodBase
struct MethodBase_t1_335;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Guid.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_ModuleHandle.h"
#include "mscorlib_System_Reflection_BindingFlags.h"
#include "mscorlib_System_Reflection_CallingConventions.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Reflection_PortableExecutableKinds.h"
#include "mscorlib_System_Reflection_ImageFileMachine.h"
#include "mscorlib_System_Reflection_ResolveTokenError.h"

// System.Void System.Reflection.Module::.ctor()
extern "C" void Module__ctor_m1_6954 (Module_t1_495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Module::.cctor()
extern "C" void Module__cctor_m1_6955 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Module::System.Runtime.InteropServices._Module.GetIDsOfNames(System.Guid&,System.IntPtr,System.UInt32,System.UInt32,System.IntPtr)
extern "C" void Module_System_Runtime_InteropServices__Module_GetIDsOfNames_m1_6956 (Module_t1_495 * __this, Guid_t1_319 * ___riid, IntPtr_t ___rgszNames, uint32_t ___cNames, uint32_t ___lcid, IntPtr_t ___rgDispId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Module::System.Runtime.InteropServices._Module.GetTypeInfo(System.UInt32,System.UInt32,System.IntPtr)
extern "C" void Module_System_Runtime_InteropServices__Module_GetTypeInfo_m1_6957 (Module_t1_495 * __this, uint32_t ___iTInfo, uint32_t ___lcid, IntPtr_t ___ppTInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Module::System.Runtime.InteropServices._Module.GetTypeInfoCount(System.UInt32&)
extern "C" void Module_System_Runtime_InteropServices__Module_GetTypeInfoCount_m1_6958 (Module_t1_495 * __this, uint32_t* ___pcTInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Module::System.Runtime.InteropServices._Module.Invoke(System.UInt32,System.Guid&,System.UInt32,System.Int16,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr)
extern "C" void Module_System_Runtime_InteropServices__Module_Invoke_m1_6959 (Module_t1_495 * __this, uint32_t ___dispIdMember, Guid_t1_319 * ___riid, uint32_t ___lcid, int16_t ___wFlags, IntPtr_t ___pDispParams, IntPtr_t ___pVarResult, IntPtr_t ___pExcepInfo, IntPtr_t ___puArgErr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.Reflection.Module::get_Assembly()
extern "C" Assembly_t1_467 * Module_get_Assembly_m1_6960 (Module_t1_495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Module::get_FullyQualifiedName()
extern "C" String_t* Module_get_FullyQualifiedName_m1_6961 (Module_t1_495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Module::get_Name()
extern "C" String_t* Module_get_Name_m1_6962 (Module_t1_495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Module::get_ScopeName()
extern "C" String_t* Module_get_ScopeName_m1_6963 (Module_t1_495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ModuleHandle System.Reflection.Module::get_ModuleHandle()
extern "C" ModuleHandle_t1_1572  Module_get_ModuleHandle_m1_6964 (Module_t1_495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Module::get_MetadataToken()
extern "C" int32_t Module_get_MetadataToken_m1_6965 (Module_t1_495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Module::get_MDStreamVersion()
extern "C" int32_t Module_get_MDStreamVersion_m1_6966 (Module_t1_495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Module::GetMDStreamVersion(System.IntPtr)
extern "C" int32_t Module_GetMDStreamVersion_m1_6967 (Object_t * __this /* static, unused */, IntPtr_t ___module_handle, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type[] System.Reflection.Module::FindTypes(System.Reflection.TypeFilter,System.Object)
extern "C" TypeU5BU5D_t1_31* Module_FindTypes_m1_6968 (Module_t1_495 * __this, TypeFilter_t1_616 * ___filter, Object_t * ___filterCriteria, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Reflection.Module::GetCustomAttributes(System.Boolean)
extern "C" ObjectU5BU5D_t1_272* Module_GetCustomAttributes_m1_6969 (Module_t1_495 * __this, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Reflection.Module::GetCustomAttributes(System.Type,System.Boolean)
extern "C" ObjectU5BU5D_t1_272* Module_GetCustomAttributes_m1_6970 (Module_t1_495 * __this, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.FieldInfo System.Reflection.Module::GetField(System.String)
extern "C" FieldInfo_t * Module_GetField_m1_6971 (Module_t1_495 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.FieldInfo System.Reflection.Module::GetField(System.String,System.Reflection.BindingFlags)
extern "C" FieldInfo_t * Module_GetField_m1_6972 (Module_t1_495 * __this, String_t* ___name, int32_t ___bindingAttr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.FieldInfo[] System.Reflection.Module::GetFields()
extern "C" FieldInfoU5BU5D_t1_1668* Module_GetFields_m1_6973 (Module_t1_495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Reflection.Module::GetMethod(System.String)
extern "C" MethodInfo_t * Module_GetMethod_m1_6974 (Module_t1_495 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Reflection.Module::GetMethod(System.String,System.Type[])
extern "C" MethodInfo_t * Module_GetMethod_m1_6975 (Module_t1_495 * __this, String_t* ___name, TypeU5BU5D_t1_31* ___types, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Reflection.Module::GetMethod(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Reflection.CallingConventions,System.Type[],System.Reflection.ParameterModifier[])
extern "C" MethodInfo_t * Module_GetMethod_m1_6976 (Module_t1_495 * __this, String_t* ___name, int32_t ___bindingAttr, Binder_t1_585 * ___binder, int32_t ___callConvention, TypeU5BU5D_t1_31* ___types, ParameterModifierU5BU5D_t1_1669* ___modifiers, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Reflection.Module::GetMethodImpl(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Reflection.CallingConventions,System.Type[],System.Reflection.ParameterModifier[])
extern "C" MethodInfo_t * Module_GetMethodImpl_m1_6977 (Module_t1_495 * __this, String_t* ___name, int32_t ___bindingAttr, Binder_t1_585 * ___binder, int32_t ___callConvention, TypeU5BU5D_t1_31* ___types, ParameterModifierU5BU5D_t1_1669* ___modifiers, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo[] System.Reflection.Module::GetMethods()
extern "C" MethodInfoU5BU5D_t1_603* Module_GetMethods_m1_6978 (Module_t1_495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo[] System.Reflection.Module::GetMethods(System.Reflection.BindingFlags)
extern "C" MethodInfoU5BU5D_t1_603* Module_GetMethods_m1_6979 (Module_t1_495 * __this, int32_t ___bindingFlags, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.FieldInfo[] System.Reflection.Module::GetFields(System.Reflection.BindingFlags)
extern "C" FieldInfoU5BU5D_t1_1668* Module_GetFields_m1_6980 (Module_t1_495 * __this, int32_t ___bindingFlags, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Module::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Module_GetObjectData_m1_6981 (Module_t1_495 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Module::GetType(System.String)
extern "C" Type_t * Module_GetType_m1_6982 (Module_t1_495 * __this, String_t* ___className, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Module::GetType(System.String,System.Boolean)
extern "C" Type_t * Module_GetType_m1_6983 (Module_t1_495 * __this, String_t* ___className, bool ___ignoreCase, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Module::GetType(System.String,System.Boolean,System.Boolean)
extern "C" Type_t * Module_GetType_m1_6984 (Module_t1_495 * __this, String_t* ___className, bool ___throwOnError, bool ___ignoreCase, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type[] System.Reflection.Module::InternalGetTypes()
extern "C" TypeU5BU5D_t1_31* Module_InternalGetTypes_m1_6985 (Module_t1_495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type[] System.Reflection.Module::GetTypes()
extern "C" TypeU5BU5D_t1_31* Module_GetTypes_m1_6986 (Module_t1_495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Module::IsDefined(System.Type,System.Boolean)
extern "C" bool Module_IsDefined_m1_6987 (Module_t1_495 * __this, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Module::IsResource()
extern "C" bool Module_IsResource_m1_6988 (Module_t1_495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Module::ToString()
extern "C" String_t* Module_ToString_m1_6989 (Module_t1_495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Guid System.Reflection.Module::get_MvId()
extern "C" Guid_t1_319  Module_get_MvId_m1_6990 (Module_t1_495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Guid System.Reflection.Module::get_ModuleVersionId()
extern "C" Guid_t1_319  Module_get_ModuleVersionId_m1_6991 (Module_t1_495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Module::GetPEKind(System.Reflection.PortableExecutableKinds&,System.Reflection.ImageFileMachine&)
extern "C" void Module_GetPEKind_m1_6992 (Module_t1_495 * __this, int32_t* ___peKind, int32_t* ___machine, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception System.Reflection.Module::resolve_token_exception(System.Int32,System.Reflection.ResolveTokenError,System.String)
extern "C" Exception_t1_33 * Module_resolve_token_exception_m1_6993 (Module_t1_495 * __this, int32_t ___metadataToken, int32_t ___error, String_t* ___tokenType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr[] System.Reflection.Module::ptrs_from_types(System.Type[])
extern "C" IntPtrU5BU5D_t1_34* Module_ptrs_from_types_m1_6994 (Module_t1_495 * __this, TypeU5BU5D_t1_31* ___types, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.FieldInfo System.Reflection.Module::ResolveField(System.Int32)
extern "C" FieldInfo_t * Module_ResolveField_m1_6995 (Module_t1_495 * __this, int32_t ___metadataToken, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.FieldInfo System.Reflection.Module::ResolveField(System.Int32,System.Type[],System.Type[])
extern "C" FieldInfo_t * Module_ResolveField_m1_6996 (Module_t1_495 * __this, int32_t ___metadataToken, TypeU5BU5D_t1_31* ___genericTypeArguments, TypeU5BU5D_t1_31* ___genericMethodArguments, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MemberInfo System.Reflection.Module::ResolveMember(System.Int32)
extern "C" MemberInfo_t * Module_ResolveMember_m1_6997 (Module_t1_495 * __this, int32_t ___metadataToken, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MemberInfo System.Reflection.Module::ResolveMember(System.Int32,System.Type[],System.Type[])
extern "C" MemberInfo_t * Module_ResolveMember_m1_6998 (Module_t1_495 * __this, int32_t ___metadataToken, TypeU5BU5D_t1_31* ___genericTypeArguments, TypeU5BU5D_t1_31* ___genericMethodArguments, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodBase System.Reflection.Module::ResolveMethod(System.Int32)
extern "C" MethodBase_t1_335 * Module_ResolveMethod_m1_6999 (Module_t1_495 * __this, int32_t ___metadataToken, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodBase System.Reflection.Module::ResolveMethod(System.Int32,System.Type[],System.Type[])
extern "C" MethodBase_t1_335 * Module_ResolveMethod_m1_7000 (Module_t1_495 * __this, int32_t ___metadataToken, TypeU5BU5D_t1_31* ___genericTypeArguments, TypeU5BU5D_t1_31* ___genericMethodArguments, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Module::ResolveString(System.Int32)
extern "C" String_t* Module_ResolveString_m1_7001 (Module_t1_495 * __this, int32_t ___metadataToken, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Module::ResolveType(System.Int32)
extern "C" Type_t * Module_ResolveType_m1_7002 (Module_t1_495 * __this, int32_t ___metadataToken, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Module::ResolveType(System.Int32,System.Type[],System.Type[])
extern "C" Type_t * Module_ResolveType_m1_7003 (Module_t1_495 * __this, int32_t ___metadataToken, TypeU5BU5D_t1_31* ___genericTypeArguments, TypeU5BU5D_t1_31* ___genericMethodArguments, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Reflection.Module::ResolveSignature(System.Int32)
extern "C" ByteU5BU5D_t1_109* Module_ResolveSignature_m1_7004 (Module_t1_495 * __this, int32_t ___metadataToken, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Module::MonoDebugger_ResolveType(System.Reflection.Module,System.Int32)
extern "C" Type_t * Module_MonoDebugger_ResolveType_m1_7005 (Object_t * __this /* static, unused */, Module_t1_495 * ___module, int32_t ___token, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Guid System.Reflection.Module::Mono_GetGuid(System.Reflection.Module)
extern "C" Guid_t1_319  Module_Mono_GetGuid_m1_7006 (Object_t * __this /* static, unused */, Module_t1_495 * ___module, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Guid System.Reflection.Module::GetModuleVersionId()
extern "C" Guid_t1_319  Module_GetModuleVersionId_m1_7007 (Module_t1_495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Module::filter_by_type_name(System.Type,System.Object)
extern "C" bool Module_filter_by_type_name_m1_7008 (Object_t * __this /* static, unused */, Type_t * ___m, Object_t * ___filterCriteria, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Module::filter_by_type_name_ignore_case(System.Type,System.Object)
extern "C" bool Module_filter_by_type_name_ignore_case_m1_7009 (Object_t * __this /* static, unused */, Type_t * ___m, Object_t * ___filterCriteria, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Reflection.Module::GetHINSTANCE()
extern "C" IntPtr_t Module_GetHINSTANCE_m1_7010 (Module_t1_495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Module::GetGuidInternal()
extern "C" String_t* Module_GetGuidInternal_m1_7011 (Module_t1_495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Module::GetGlobalType()
extern "C" Type_t * Module_GetGlobalType_m1_7012 (Module_t1_495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Reflection.Module::ResolveTypeToken(System.IntPtr,System.Int32,System.IntPtr[],System.IntPtr[],System.Reflection.ResolveTokenError&)
extern "C" IntPtr_t Module_ResolveTypeToken_m1_7013 (Object_t * __this /* static, unused */, IntPtr_t ___module, int32_t ___token, IntPtrU5BU5D_t1_34* ___type_args, IntPtrU5BU5D_t1_34* ___method_args, int32_t* ___error, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Reflection.Module::ResolveMethodToken(System.IntPtr,System.Int32,System.IntPtr[],System.IntPtr[],System.Reflection.ResolveTokenError&)
extern "C" IntPtr_t Module_ResolveMethodToken_m1_7014 (Object_t * __this /* static, unused */, IntPtr_t ___module, int32_t ___token, IntPtrU5BU5D_t1_34* ___type_args, IntPtrU5BU5D_t1_34* ___method_args, int32_t* ___error, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Reflection.Module::ResolveFieldToken(System.IntPtr,System.Int32,System.IntPtr[],System.IntPtr[],System.Reflection.ResolveTokenError&)
extern "C" IntPtr_t Module_ResolveFieldToken_m1_7015 (Object_t * __this /* static, unused */, IntPtr_t ___module, int32_t ___token, IntPtrU5BU5D_t1_34* ___type_args, IntPtrU5BU5D_t1_34* ___method_args, int32_t* ___error, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Module::ResolveStringToken(System.IntPtr,System.Int32,System.Reflection.ResolveTokenError&)
extern "C" String_t* Module_ResolveStringToken_m1_7016 (Object_t * __this /* static, unused */, IntPtr_t ___module, int32_t ___token, int32_t* ___error, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MemberInfo System.Reflection.Module::ResolveMemberToken(System.IntPtr,System.Int32,System.IntPtr[],System.IntPtr[],System.Reflection.ResolveTokenError&)
extern "C" MemberInfo_t * Module_ResolveMemberToken_m1_7017 (Object_t * __this /* static, unused */, IntPtr_t ___module, int32_t ___token, IntPtrU5BU5D_t1_34* ___type_args, IntPtrU5BU5D_t1_34* ___method_args, int32_t* ___error, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Reflection.Module::ResolveSignature(System.IntPtr,System.Int32,System.Reflection.ResolveTokenError&)
extern "C" ByteU5BU5D_t1_109* Module_ResolveSignature_m1_7018 (Object_t * __this /* static, unused */, IntPtr_t ___module, int32_t ___metadataToken, int32_t* ___error, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Module::GetPEKind(System.IntPtr,System.Reflection.PortableExecutableKinds&,System.Reflection.ImageFileMachine&)
extern "C" void Module_GetPEKind_m1_7019 (Object_t * __this /* static, unused */, IntPtr_t ___module, int32_t* ___peKind, int32_t* ___machine, const MethodInfo* method) IL2CPP_METHOD_ATTR;
