﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_VoxelBusters_AssetStoreProductUtility_Demo_1.h"

// VoxelBusters.NativePlugins.Demo.NPDisabledFeatureDemo
struct  NPDisabledFeatureDemo_t8_175  : public DemoSubMenu_t8_18
{
	// System.String VoxelBusters.NativePlugins.Demo.NPDisabledFeatureDemo::m_enableFeatureInfoText
	String_t* ___m_enableFeatureInfoText_9;
};
