﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

extern "C" void NsDecl_t4_141_marshal(const NsDecl_t4_141& unmarshaled, NsDecl_t4_141_marshaled& marshaled);
extern "C" void NsDecl_t4_141_marshal_back(const NsDecl_t4_141_marshaled& marshaled, NsDecl_t4_141& unmarshaled);
extern "C" void NsDecl_t4_141_marshal_cleanup(NsDecl_t4_141_marshaled& marshaled);
