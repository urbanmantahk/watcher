﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Type
struct Type_t;

#include "UnityEngine_UnityEngine_PropertyAttribute.h"

// VoxelBusters.Utility.EnumMaskFieldAttribute
struct  EnumMaskFieldAttribute_t8_145  : public PropertyAttribute_t6_236
{
	// System.Type VoxelBusters.Utility.EnumMaskFieldAttribute::<TargetType>k__BackingField
	Type_t * ___U3CTargetTypeU3Ek__BackingField_0;
};
