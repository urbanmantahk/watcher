﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>
struct Dictionary_2_t1_2422;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_18.h"
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m1_21650_gshared (Enumerator_t1_2432 * __this, Dictionary_2_t1_2422 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m1_21650(__this, ___host, method) (( void (*) (Enumerator_t1_2432 *, Dictionary_2_t1_2422 *, const MethodInfo*))Enumerator__ctor_m1_21650_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m1_21651_gshared (Enumerator_t1_2432 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_21651(__this, method) (( Object_t * (*) (Enumerator_t1_2432 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_21651_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m1_21652_gshared (Enumerator_t1_2432 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1_21652(__this, method) (( void (*) (Enumerator_t1_2432 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_21652_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::Dispose()
extern "C" void Enumerator_Dispose_m1_21653_gshared (Enumerator_t1_2432 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1_21653(__this, method) (( void (*) (Enumerator_t1_2432 *, const MethodInfo*))Enumerator_Dispose_m1_21653_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::MoveNext()
extern "C" bool Enumerator_MoveNext_m1_21654_gshared (Enumerator_t1_2432 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1_21654(__this, method) (( bool (*) (Enumerator_t1_2432 *, const MethodInfo*))Enumerator_MoveNext_m1_21654_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Current()
extern "C" int32_t Enumerator_get_Current_m1_21655_gshared (Enumerator_t1_2432 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1_21655(__this, method) (( int32_t (*) (Enumerator_t1_2432 *, const MethodInfo*))Enumerator_get_Current_m1_21655_gshared)(__this, method)
