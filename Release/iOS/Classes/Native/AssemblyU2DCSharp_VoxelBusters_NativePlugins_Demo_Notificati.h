﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.ArrayList
struct ArrayList_t1_170;

#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Demo_NPDemoBase.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_NotificationTyp.h"

// VoxelBusters.NativePlugins.Demo.NotificationDemo
struct  NotificationDemo_t8_183  : public NPDemoBase_t8_174
{
	// VoxelBusters.NativePlugins.NotificationType VoxelBusters.NativePlugins.Demo.NotificationDemo::m_notificationType
	int32_t ___m_notificationType_13;
	// System.Collections.ArrayList VoxelBusters.NativePlugins.Demo.NotificationDemo::m_scheduledNotificationIDList
	ArrayList_t1_170 * ___m_scheduledNotificationIDList_14;
};
