﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_AndroidManifestGenera.h"

// System.Void VoxelBusters.Utility.AndroidManifestGenerator/Feature::.ctor(System.String,System.Boolean)
extern "C" void Feature__ctor_m8_159 (Feature_t8_24 * __this, String_t* ____name, bool ____required, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.Utility.AndroidManifestGenerator/Feature::get_Name()
extern "C" String_t* Feature_get_Name_m8_160 (Feature_t8_24 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.AndroidManifestGenerator/Feature::set_Name(System.String)
extern "C" void Feature_set_Name_m8_161 (Feature_t8_24 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.Utility.AndroidManifestGenerator/Feature::get_Required()
extern "C" bool Feature_get_Required_m8_162 (Feature_t8_24 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.AndroidManifestGenerator/Feature::set_Required(System.Boolean)
extern "C" void Feature_set_Required_m8_163 (Feature_t8_24 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void Feature_t8_24_marshal(const Feature_t8_24& unmarshaled, Feature_t8_24_marshaled& marshaled);
extern "C" void Feature_t8_24_marshal_back(const Feature_t8_24_marshaled& marshaled, Feature_t8_24& unmarshaled);
extern "C" void Feature_t8_24_marshal_cleanup(Feature_t8_24_marshaled& marshaled);
