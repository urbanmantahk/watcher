﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Text.EncoderFallback
struct EncoderFallback_t1_1419;
// System.Text.EncoderFallbackBuffer
struct EncoderFallbackBuffer_t1_1429;

#include "mscorlib_System_Object.h"

// System.Text.Encoder
struct  Encoder_t1_1428  : public Object_t
{
	// System.Text.EncoderFallback System.Text.Encoder::fallback
	EncoderFallback_t1_1419 * ___fallback_0;
	// System.Text.EncoderFallbackBuffer System.Text.Encoder::fallback_buffer
	EncoderFallbackBuffer_t1_1429 * ___fallback_buffer_1;
};
