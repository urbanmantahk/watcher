﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// UnityEngine.AssetBundleCreateRequest
struct AssetBundleCreateRequest_t6_1;
// UnityEngine.AssetBundle
struct AssetBundle_t6_4;
// UnityEngine.AssetBundleRequest
struct AssetBundleRequest_t6_3;
// UnityEngine.Object
struct Object_t6_5;
struct Object_t6_5_marshaled;
// UnityEngine.Object[]
struct ObjectU5BU5D_t6_272;
// System.String
struct String_t;
// System.Type
struct Type_t;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t6_10;
struct WaitForSeconds_t6_10_marshaled;
// UnityEngine.WaitForFixedUpdate
struct WaitForFixedUpdate_t6_12;
// UnityEngine.WaitForEndOfFrame
struct WaitForEndOfFrame_t6_13;
// UnityEngine.Coroutine
struct Coroutine_t6_14;
struct Coroutine_t6_14_marshaled;
// UnityEngine.ScriptableObject
struct ScriptableObject_t6_15;
struct ScriptableObject_t6_15_marshaled;
// UnityEngine.UnhandledExceptionHandler
struct UnhandledExceptionHandler_t6_16;
// System.Object
struct Object_t;
// System.UnhandledExceptionEventArgs
struct UnhandledExceptionEventArgs_t1_1614;
// System.Exception
struct Exception_t1_33;
// UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform
struct GameCenterPlatform_t6_17;
// UnityEngine.SocialPlatforms.ILocalUser
struct ILocalUser_t6_275;
// System.Action`1<System.Boolean>
struct Action_1_t1_1815;
// UnityEngine.Texture2D
struct Texture2D_t6_33;
// System.String[]
struct StringU5BU5D_t1_238;
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData[]
struct GcAchievementDataU5BU5D_t6_273;
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData[]
struct GcScoreDataU5BU5D_t6_274;
// System.Action`1<UnityEngine.SocialPlatforms.IAchievementDescription[]>
struct Action_1_t1_1816;
// System.Action`1<UnityEngine.SocialPlatforms.IAchievement[]>
struct Action_1_t1_1817;
// System.Action`1<UnityEngine.SocialPlatforms.IScore[]>
struct Action_1_t1_1818;
// UnityEngine.SocialPlatforms.ILeaderboard
struct ILeaderboard_t6_276;
// System.Action`1<UnityEngine.SocialPlatforms.IUserProfile[]>
struct Action_1_t1_1819;
// UnityEngine.SocialPlatforms.Impl.UserProfile[]
struct UserProfileU5BU5D_t6_19;
// UnityEngine.SocialPlatforms.IAchievement
struct IAchievement_t6_277;
// UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard
struct GcLeaderboard_t6_21;
// UnityEngine.SocialPlatforms.Impl.Leaderboard
struct Leaderboard_t6_22;
// UnityEngine.Mesh
struct Mesh_t6_23;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t6_161;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1_1834;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t6_278;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_t1_1835;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t6_279;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t1_1836;
// UnityEngine.Color32[]
struct Color32U5BU5D_t6_280;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t1_1837;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1_1838;
// System.Int32[]
struct Int32U5BU5D_t1_275;
// UnityEngine.Renderer
struct Renderer_t6_25;
// UnityEngine.Material
struct Material_t6_72;
// UnityEngine.GUILayer
struct GUILayer_t6_31;
// UnityEngine.GUIElement
struct GUIElement_t6_29;
// UnityEngine.Texture
struct Texture_t6_32;
// UnityEngine.Color[]
struct ColorU5BU5D_t6_281;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// UnityEngine.RenderTexture
struct RenderTexture_t6_34;
// UnityEngine.CullingGroup/StateChanged
struct StateChanged_t6_37;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;
// UnityEngine.CullingGroup
struct CullingGroup_t6_38;
// UnityEngine.Gradient
struct Gradient_t6_42;
struct Gradient_t6_42_marshaled;
// UnityEngine.TouchScreenKeyboard
struct TouchScreenKeyboard_t6_45;
// UnityEngine.RectTransform
struct RectTransform_t6_64;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t6_63;
// UnityEngine.ResourceRequest
struct ResourceRequest_t6_66;
// UnityEngine.TextAsset
struct TextAsset_t6_68;
// UnityEngine.SerializePrivateVariables
struct SerializePrivateVariables_t6_69;
// UnityEngine.SerializeField
struct SerializeField_t6_70;
// UnityEngine.Shader
struct Shader_t6_71;
// UnityEngine.Sprite
struct Sprite_t6_75;
// UnityEngine.WWW
struct WWW_t6_78;
// UnityEngine.WWWForm
struct WWWForm_t6_79;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t1_1839;
// System.Text.Encoding
struct Encoding_t1_406;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// UnityEngine.AsyncOperation
struct AsyncOperation_t6_2;
struct AsyncOperation_t6_2_marshaled;
// UnityEngine.Application/LogCallback
struct LogCallback_t6_83;
// UnityEngine.Behaviour
struct Behaviour_t6_30;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t6_85;
// UnityEngine.Camera
struct Camera_t6_86;
// UnityEngine.Camera[]
struct CameraU5BU5D_t6_232;
// UnityEngine.GameObject
struct GameObject_t6_97;
// UnityEngine.Display/DisplaysUpdatedDelegate
struct DisplaysUpdatedDelegate_t6_88;
// UnityEngine.Display
struct Display_t6_89;
// System.IntPtr[]
struct IntPtrU5BU5D_t1_34;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t6_91;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// UnityEngine.Touch[]
struct TouchU5BU5D_t6_282;
// UnityEngine.Component
struct Component_t6_26;
// UnityEngine.Transform
struct Transform_t6_65;
// System.Collections.Generic.List`1<UnityEngine.Component>
struct List_1_t1_1840;
// System.Array
struct Array_t;
// UnityEngine.Transform/Enumerator
struct Enumerator_t6_98;
// UnityEngine.YieldInstruction
struct YieldInstruction_t6_11;
struct YieldInstruction_t6_11_marshaled;
// UnityEngine.PlayerPrefsException
struct PlayerPrefsException_t6_101;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array.h"
#include "UnityEngine_U3CModuleU3E.h"
#include "UnityEngine_U3CModuleU3EMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AssetBundleCreateRequest.h"
#include "UnityEngine_UnityEngine_AssetBundleCreateRequestMethodDeclarations.h"
#include "mscorlib_System_Void.h"
#include "UnityEngine_UnityEngine_AsyncOperationMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AsyncOperation.h"
#include "UnityEngine_UnityEngine_AssetBundle.h"
#include "UnityEngine_UnityEngine_AssetBundleRequest.h"
#include "UnityEngine_UnityEngine_AssetBundleRequestMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object.h"
#include "UnityEngine_UnityEngine_AssetBundleMethodDeclarations.h"
#include "mscorlib_System_String.h"
#include "mscorlib_System_Type.h"
#include "UnityEngine_ArrayTypes.h"
#include "mscorlib_System_NullReferenceExceptionMethodDeclarations.h"
#include "mscorlib_System_StringMethodDeclarations.h"
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
#include "mscorlib_System_NullReferenceException.h"
#include "mscorlib_System_Int32.h"
#include "mscorlib_System_ArgumentException.h"
#include "UnityEngine_UnityEngine_SendMessageOptions.h"
#include "UnityEngine_UnityEngine_SendMessageOptionsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RuntimePlatform.h"
#include "UnityEngine_UnityEngine_RuntimePlatformMethodDeclarations.h"
#include "UnityEngine_UnityEngine_LogType.h"
#include "UnityEngine_UnityEngine_LogTypeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SystemInfo.h"
#include "UnityEngine_UnityEngine_SystemInfoMethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForSeconds.h"
#include "UnityEngine_UnityEngine_WaitForSecondsMethodDeclarations.h"
#include "mscorlib_System_Single.h"
#include "UnityEngine_UnityEngine_YieldInstructionMethodDeclarations.h"
#include "UnityEngine_UnityEngine_YieldInstruction.h"
#include "UnityEngine_UnityEngine_WaitForFixedUpdate.h"
#include "UnityEngine_UnityEngine_WaitForFixedUpdateMethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForEndOfFrame.h"
#include "UnityEngine_UnityEngine_WaitForEndOfFrameMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Coroutine.h"
#include "UnityEngine_UnityEngine_CoroutineMethodDeclarations.h"
#include "mscorlib_System_ObjectMethodDeclarations.h"
#include "mscorlib_System_Object.h"
#include "UnityEngine_UnityEngine_ScriptableObject.h"
#include "UnityEngine_UnityEngine_ScriptableObjectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_UnhandledExceptionHandler.h"
#include "UnityEngine_UnityEngine_UnhandledExceptionHandlerMethodDeclarations.h"
#include "mscorlib_System_AppDomainMethodDeclarations.h"
#include "mscorlib_System_UnhandledExceptionEventHandlerMethodDeclarations.h"
#include "mscorlib_System_AppDomain.h"
#include "mscorlib_System_UnhandledExceptionEventArgs.h"
#include "mscorlib_System_UnhandledExceptionEventHandler.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_UnhandledExceptionEventArgsMethodDeclarations.h"
#include "mscorlib_System_Exception.h"
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
#include "mscorlib_System_ExceptionMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GameCente.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GameCenteMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_6MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_AchievementDesc.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_UserProfile.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_6.h"
#include "mscorlib_System_Action_1_gen.h"
#include "mscorlib_System_Boolean.h"
#include "UnityEngine_UnityEngine_Texture2D.h"
#include "mscorlib_System_Double.h"
#include "mscorlib_System_Int64.h"
#include "mscorlib_ArrayTypes.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScope.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieveMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_AchievementDescMethodDeclarations.h"
#include "mscorlib_System_Action_1_gen_0MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen_0.h"
#include "mscorlib_System_Action_1_genMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcUserPro.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcUserProMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_LocalUserMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_LocalUser.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve_0.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve_0MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen_1MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achievement.h"
#include "mscorlib_System_Action_1_gen_1.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcScoreDa.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcScoreDaMethodDeclarations.h"
#include "mscorlib_System_Action_1_gen_2MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Score.h"
#include "mscorlib_System_Action_1_gen_2.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_UserProfileMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcLeaderbMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_LeaderboardMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Leaderboard.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcLeaderb.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Range.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScope.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_3MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_3.h"
#include "mscorlib_System_Action_1_gen_3MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen_3.h"
#include "UnityEngine_UnityEngine_Texture2DMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_AchievementMethodDeclarations.h"
#include "mscorlib_System_UInt32.h"
#include "UnityEngine_UnityEngine_Mesh.h"
#include "UnityEngine_UnityEngine_MeshMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector3.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_7.h"
#include "UnityEngine_UnityEngine_Vector4.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_8.h"
#include "UnityEngine_UnityEngine_Vector2.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_9.h"
#include "UnityEngine_UnityEngine_Color32.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_10.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_11.h"
#include "UnityEngine_UnityEngine_BoneWeight.h"
#include "UnityEngine_UnityEngine_BoneWeightMethodDeclarations.h"
#include "mscorlib_System_Int32MethodDeclarations.h"
#include "mscorlib_System_SingleMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector4MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Renderer.h"
#include "UnityEngine_UnityEngine_RendererMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Material.h"
#include "UnityEngine_UnityEngine_Screen.h"
#include "UnityEngine_UnityEngine_ScreenMethodDeclarations.h"
#include "UnityEngine_UnityEngine_MeshRenderer.h"
#include "UnityEngine_UnityEngine_MeshRendererMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIElement.h"
#include "UnityEngine_UnityEngine_GUIElementMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayer.h"
#include "UnityEngine_UnityEngine_GUILayerMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture.h"
#include "UnityEngine_UnityEngine_TextureMethodDeclarations.h"
#include "mscorlib_System_IntPtrMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextureFormat.h"
#include "UnityEngine_UnityEngine_Color.h"
#include "mscorlib_System_Byte.h"
#include "UnityEngine_UnityEngine_Rect.h"
#include "UnityEngine_UnityEngine_RenderTexture.h"
#include "UnityEngine_UnityEngine_RenderTextureMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ReflectionProbe.h"
#include "UnityEngine_UnityEngine_ReflectionProbeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_CullingGroupEvent.h"
#include "UnityEngine_UnityEngine_CullingGroupEventMethodDeclarations.h"
#include "UnityEngine_UnityEngine_CullingGroup_StateChanged.h"
#include "UnityEngine_UnityEngine_CullingGroup_StateChangedMethodDeclarations.h"
#include "mscorlib_System_AsyncCallback.h"
#include "UnityEngine_UnityEngine_CullingGroup.h"
#include "UnityEngine_UnityEngine_CullingGroupMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GradientColorKey.h"
#include "UnityEngine_UnityEngine_GradientColorKeyMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GradientAlphaKey.h"
#include "UnityEngine_UnityEngine_GradientAlphaKeyMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Gradient.h"
#include "UnityEngine_UnityEngine_GradientMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboard_InternalConstruc.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboard_InternalConstrucMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboardType.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboardTypeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboard.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboardMethodDeclarations.h"
#include "mscorlib_System_ConvertMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ApplicationMethodDeclarations.h"
#include "UnityEngine_UnityEngine_LayerMask.h"
#include "UnityEngine_UnityEngine_LayerMaskMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"
#include "mscorlib_System_IndexOutOfRangeExceptionMethodDeclarations.h"
#include "mscorlib_System_IndexOutOfRangeException.h"
#include "UnityEngine_UnityEngine_UnityStringMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector3MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MathfMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ColorMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color32MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion.h"
#include "UnityEngine_UnityEngine_QuaternionMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Matrix4x4.h"
#include "UnityEngine_UnityEngine_Matrix4x4MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Bounds.h"
#include "UnityEngine_UnityEngine_BoundsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Ray.h"
#include "UnityEngine_UnityEngine_RayMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Plane.h"
#include "UnityEngine_UnityEngine_PlaneMethodDeclarations.h"
#include "UnityEngine_UnityEngineInternal_MathfInternal.h"
#include "UnityEngine_UnityEngineInternal_MathfInternalMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mathf.h"
#include "mscorlib_System_MathMethodDeclarations.h"
#include "UnityEngine_UnityEngine_DrivenTransformProperties.h"
#include "UnityEngine_UnityEngine_DrivenTransformPropertiesMethodDeclarations.h"
#include "UnityEngine_UnityEngine_DrivenRectTransformTracker.h"
#include "UnityEngine_UnityEngine_DrivenRectTransformTrackerMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform.h"
#include "UnityEngine_UnityEngine_RectTransform_Edge.h"
#include "UnityEngine_UnityEngine_RectTransform_EdgeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform_Axis.h"
#include "UnityEngine_UnityEngine_RectTransform_AxisMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform_ReapplyDrivenPropertie.h"
#include "UnityEngine_UnityEngine_RectTransform_ReapplyDrivenPropertieMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransformMethodDeclarations.h"
#include "mscorlib_System_DelegateMethodDeclarations.h"
#include "mscorlib_System_Delegate.h"
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform.h"
#include "UnityEngine_UnityEngine_Component.h"
#include "UnityEngine_UnityEngine_ResourceRequest.h"
#include "UnityEngine_UnityEngine_ResourceRequestMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ResourcesMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Resources.h"
#include "mscorlib_System_TypeMethodDeclarations.h"
#include "mscorlib_System_RuntimeTypeHandle.h"
#include "UnityEngine_UnityEngine_TextAsset.h"
#include "UnityEngine_UnityEngine_TextAssetMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SerializePrivateVariables.h"
#include "UnityEngine_UnityEngine_SerializePrivateVariablesMethodDeclarations.h"
#include "mscorlib_System_AttributeMethodDeclarations.h"
#include "mscorlib_System_Attribute.h"
#include "UnityEngine_UnityEngine_SerializeField.h"
#include "UnityEngine_UnityEngine_SerializeFieldMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Shader.h"
#include "UnityEngine_UnityEngine_ShaderMethodDeclarations.h"
#include "UnityEngine_UnityEngine_MaterialMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SortingLayer.h"
#include "UnityEngine_UnityEngine_SortingLayerMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rendering_SphericalHarmonicsL2.h"
#include "UnityEngine_UnityEngine_Rendering_SphericalHarmonicsL2MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Sprite.h"
#include "UnityEngine_UnityEngine_SpriteMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SpriteRenderer.h"
#include "UnityEngine_UnityEngine_SpriteRendererMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Sprites_DataUtility.h"
#include "UnityEngine_UnityEngine_Sprites_DataUtilityMethodDeclarations.h"
#include "UnityEngine_UnityEngine_WWW.h"
#include "UnityEngine_UnityEngine_WWWMethodDeclarations.h"
#include "UnityEngine_UnityEngine_WWWForm.h"
#include "UnityEngine_UnityEngine_WWWFormMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_1.h"
#include "UnityEngine_UnityEngine_UnityExceptionMethodDeclarations.h"
#include "UnityEngine_UnityEngine_UnityException.h"
#include "mscorlib_System_Text_Encoding.h"
#include "mscorlib_System_Text_EncodingMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_1MethodDeclarations.h"
#include "mscorlib_System_StringComparison.h"
#include "mscorlib_System_Char.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator_MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_0MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_0.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator_.h"
#include "mscorlib_System_IO_StringReaderMethodDeclarations.h"
#include "mscorlib_System_IO_StringReader.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_12MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_genMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RandomMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_12.h"
#include "mscorlib_System_Collections_Generic_List_1_gen.h"
#include "mscorlib_System_IO_MemoryStreamMethodDeclarations.h"
#include "UnityEngine_UnityEngine_WWWTranscoderMethodDeclarations.h"
#include "mscorlib_System_IO_MemoryStream.h"
#include "UnityEngine_UnityEngine_WWWTranscoder.h"
#include "UnityEngine_UnityEngine_CacheIndex.h"
#include "UnityEngine_UnityEngine_CacheIndexMethodDeclarations.h"
#include "UnityEngine_UnityEngine_UnityString.h"
#include "UnityEngine_UnityEngine_Application_LogCallback.h"
#include "UnityEngine_UnityEngine_Application_LogCallbackMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Application.h"
#include "UnityEngine_UnityEngine_Behaviour.h"
#include "UnityEngine_UnityEngine_BehaviourMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera_CameraCallback.h"
#include "UnityEngine_UnityEngine_Camera_CameraCallbackMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera.h"
#include "UnityEngine_UnityEngine_CameraMethodDeclarations.h"
#include "UnityEngine_UnityEngine_CameraClearFlags.h"
#include "UnityEngine_UnityEngine_GameObject.h"
#include "UnityEngine_UnityEngine_QueryTriggerInteraction.h"
#include "UnityEngine_UnityEngine_Debug.h"
#include "UnityEngine_UnityEngine_Display_DisplaysUpdatedDelegate.h"
#include "UnityEngine_UnityEngine_Display_DisplaysUpdatedDelegateMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Display.h"
#include "UnityEngine_UnityEngine_DisplayMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RenderBuffer.h"
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TouchPhase.h"
#include "UnityEngine_UnityEngine_TouchPhaseMethodDeclarations.h"
#include "UnityEngine_UnityEngine_IMECompositionMode.h"
#include "UnityEngine_UnityEngine_IMECompositionModeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Touch.h"
#include "UnityEngine_UnityEngine_TouchMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Input.h"
#include "UnityEngine_UnityEngine_InputMethodDeclarations.h"
#include "UnityEngine_UnityEngine_HideFlags.h"
#include "UnityEngine_UnityEngine_HideFlagsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObjectMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_13.h"
#include "UnityEngine_UnityEngine_Transform_Enumerator.h"
#include "UnityEngine_UnityEngine_Transform_EnumeratorMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Time.h"
#include "UnityEngine_UnityEngine_TimeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Random.h"
#include "UnityEngine_UnityEngine_PlayerPrefsException.h"
#include "UnityEngine_UnityEngine_PlayerPrefsExceptionMethodDeclarations.h"
#include "UnityEngine_UnityEngine_PlayerPrefs.h"
#include "UnityEngine_UnityEngine_PlayerPrefsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_iOS_CalendarUnit.h"
#include "UnityEngine_UnityEngine_iOS_CalendarUnitMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Experimental_Director_DirectorPlayer.h"
#include "UnityEngine_UnityEngine_Experimental_Director_DirectorPlayerMethodDeclarations.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.AssetBundleCreateRequest::.ctor()
extern "C" void AssetBundleCreateRequest__ctor_m6_0 (AssetBundleCreateRequest_t6_1 * __this, const MethodInfo* method)
{
	{
		AsyncOperation__ctor_m6_574(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.AssetBundle UnityEngine.AssetBundleCreateRequest::get_assetBundle()
extern "C" AssetBundle_t6_4 * AssetBundleCreateRequest_get_assetBundle_m6_1 (AssetBundleCreateRequest_t6_1 * __this, const MethodInfo* method)
{
	typedef AssetBundle_t6_4 * (*AssetBundleCreateRequest_get_assetBundle_m6_1_ftn) (AssetBundleCreateRequest_t6_1 *);
	static AssetBundleCreateRequest_get_assetBundle_m6_1_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AssetBundleCreateRequest_get_assetBundle_m6_1_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AssetBundleCreateRequest::get_assetBundle()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AssetBundleCreateRequest::DisableCompatibilityChecks()
extern "C" void AssetBundleCreateRequest_DisableCompatibilityChecks_m6_2 (AssetBundleCreateRequest_t6_1 * __this, const MethodInfo* method)
{
	typedef void (*AssetBundleCreateRequest_DisableCompatibilityChecks_m6_2_ftn) (AssetBundleCreateRequest_t6_1 *);
	static AssetBundleCreateRequest_DisableCompatibilityChecks_m6_2_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AssetBundleCreateRequest_DisableCompatibilityChecks_m6_2_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AssetBundleCreateRequest::DisableCompatibilityChecks()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AssetBundleRequest::.ctor()
extern "C" void AssetBundleRequest__ctor_m6_3 (AssetBundleRequest_t6_3 * __this, const MethodInfo* method)
{
	{
		AsyncOperation__ctor_m6_574(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object UnityEngine.AssetBundleRequest::get_asset()
extern "C" Object_t6_5 * AssetBundleRequest_get_asset_m6_4 (AssetBundleRequest_t6_3 * __this, const MethodInfo* method)
{
	{
		AssetBundle_t6_4 * L_0 = (__this->___m_AssetBundle_1);
		String_t* L_1 = (__this->___m_Path_2);
		Type_t * L_2 = (__this->___m_Type_3);
		NullCheck(L_0);
		Object_t6_5 * L_3 = AssetBundle_LoadAsset_m6_6(L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Object[] UnityEngine.AssetBundleRequest::get_allAssets()
extern "C" ObjectU5BU5D_t6_272* AssetBundleRequest_get_allAssets_m6_5 (AssetBundleRequest_t6_3 * __this, const MethodInfo* method)
{
	{
		AssetBundle_t6_4 * L_0 = (__this->___m_AssetBundle_1);
		String_t* L_1 = (__this->___m_Path_2);
		Type_t * L_2 = (__this->___m_Type_3);
		NullCheck(L_0);
		ObjectU5BU5D_t6_272* L_3 = AssetBundle_LoadAssetWithSubAssets_Internal_m6_8(L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Object UnityEngine.AssetBundle::LoadAsset(System.String,System.Type)
extern TypeInfo* NullReferenceException_t1_1584_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4683;
extern Il2CppCodeGenString* _stringLiteral4684;
extern Il2CppCodeGenString* _stringLiteral4685;
extern "C" Object_t6_5 * AssetBundle_LoadAsset_m6_6 (AssetBundle_t6_4 * __this, String_t* ___name, Type_t * ___type, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NullReferenceException_t1_1584_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		_stringLiteral4683 = il2cpp_codegen_string_literal_from_index(4683);
		_stringLiteral4684 = il2cpp_codegen_string_literal_from_index(4684);
		_stringLiteral4685 = il2cpp_codegen_string_literal_from_index(4685);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___name;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		NullReferenceException_t1_1584 * L_1 = (NullReferenceException_t1_1584 *)il2cpp_codegen_object_new (NullReferenceException_t1_1584_il2cpp_TypeInfo_var);
		NullReferenceException__ctor_m1_14421(L_1, _stringLiteral4683, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		String_t* L_2 = ___name;
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m1_571(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0027;
		}
	}
	{
		ArgumentException_t1_1425 * L_4 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13260(L_4, _stringLiteral4684, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
	}

IL_0027:
	{
		Type_t * L_5 = ___type;
		if (L_5)
		{
			goto IL_0038;
		}
	}
	{
		NullReferenceException_t1_1584 * L_6 = (NullReferenceException_t1_1584 *)il2cpp_codegen_object_new (NullReferenceException_t1_1584_il2cpp_TypeInfo_var);
		NullReferenceException__ctor_m1_14421(L_6, _stringLiteral4685, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_6);
	}

IL_0038:
	{
		String_t* L_7 = ___name;
		Type_t * L_8 = ___type;
		Object_t6_5 * L_9 = AssetBundle_LoadAsset_Internal_m6_7(__this, L_7, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// UnityEngine.Object UnityEngine.AssetBundle::LoadAsset_Internal(System.String,System.Type)
extern "C" Object_t6_5 * AssetBundle_LoadAsset_Internal_m6_7 (AssetBundle_t6_4 * __this, String_t* ___name, Type_t * ___type, const MethodInfo* method)
{
	typedef Object_t6_5 * (*AssetBundle_LoadAsset_Internal_m6_7_ftn) (AssetBundle_t6_4 *, String_t*, Type_t *);
	static AssetBundle_LoadAsset_Internal_m6_7_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AssetBundle_LoadAsset_Internal_m6_7_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AssetBundle::LoadAsset_Internal(System.String,System.Type)");
	return _il2cpp_icall_func(__this, ___name, ___type);
}
// UnityEngine.Object[] UnityEngine.AssetBundle::LoadAssetWithSubAssets_Internal(System.String,System.Type)
extern "C" ObjectU5BU5D_t6_272* AssetBundle_LoadAssetWithSubAssets_Internal_m6_8 (AssetBundle_t6_4 * __this, String_t* ___name, Type_t * ___type, const MethodInfo* method)
{
	typedef ObjectU5BU5D_t6_272* (*AssetBundle_LoadAssetWithSubAssets_Internal_m6_8_ftn) (AssetBundle_t6_4 *, String_t*, Type_t *);
	static AssetBundle_LoadAssetWithSubAssets_Internal_m6_8_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AssetBundle_LoadAssetWithSubAssets_Internal_m6_8_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AssetBundle::LoadAssetWithSubAssets_Internal(System.String,System.Type)");
	return _il2cpp_icall_func(__this, ___name, ___type);
}
// System.String UnityEngine.SystemInfo::get_operatingSystem()
extern "C" String_t* SystemInfo_get_operatingSystem_m6_9 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*SystemInfo_get_operatingSystem_m6_9_ftn) ();
	static SystemInfo_get_operatingSystem_m6_9_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SystemInfo_get_operatingSystem_m6_9_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SystemInfo::get_operatingSystem()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
extern "C" void WaitForSeconds__ctor_m6_10 (WaitForSeconds_t6_10 * __this, float ___seconds, const MethodInfo* method)
{
	{
		YieldInstruction__ctor_m6_799(__this, /*hidden argument*/NULL);
		float L_0 = ___seconds;
		__this->___m_Seconds_0 = L_0;
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.WaitForSeconds
extern "C" void WaitForSeconds_t6_10_marshal(const WaitForSeconds_t6_10& unmarshaled, WaitForSeconds_t6_10_marshaled& marshaled)
{
	marshaled.___m_Seconds_0 = unmarshaled.___m_Seconds_0;
}
extern "C" void WaitForSeconds_t6_10_marshal_back(const WaitForSeconds_t6_10_marshaled& marshaled, WaitForSeconds_t6_10& unmarshaled)
{
	unmarshaled.___m_Seconds_0 = marshaled.___m_Seconds_0;
}
// Conversion method for clean up from marshalling of: UnityEngine.WaitForSeconds
extern "C" void WaitForSeconds_t6_10_marshal_cleanup(WaitForSeconds_t6_10_marshaled& marshaled)
{
}
// System.Void UnityEngine.WaitForFixedUpdate::.ctor()
extern "C" void WaitForFixedUpdate__ctor_m6_11 (WaitForFixedUpdate_t6_12 * __this, const MethodInfo* method)
{
	{
		YieldInstruction__ctor_m6_799(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WaitForEndOfFrame::.ctor()
extern "C" void WaitForEndOfFrame__ctor_m6_12 (WaitForEndOfFrame_t6_13 * __this, const MethodInfo* method)
{
	{
		YieldInstruction__ctor_m6_799(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Coroutine::.ctor()
extern "C" void Coroutine__ctor_m6_13 (Coroutine_t6_14 * __this, const MethodInfo* method)
{
	{
		YieldInstruction__ctor_m6_799(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Coroutine::ReleaseCoroutine()
extern "C" void Coroutine_ReleaseCoroutine_m6_14 (Coroutine_t6_14 * __this, const MethodInfo* method)
{
	typedef void (*Coroutine_ReleaseCoroutine_m6_14_ftn) (Coroutine_t6_14 *);
	static Coroutine_ReleaseCoroutine_m6_14_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Coroutine_ReleaseCoroutine_m6_14_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Coroutine::ReleaseCoroutine()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Coroutine::Finalize()
extern "C" void Coroutine_Finalize_m6_15 (Coroutine_t6_14 * __this, const MethodInfo* method)
{
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Coroutine_ReleaseCoroutine_m6_14(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m1_3(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0012:
	{
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.Coroutine
extern "C" void Coroutine_t6_14_marshal(const Coroutine_t6_14& unmarshaled, Coroutine_t6_14_marshaled& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.___m_Ptr_0).___m_value_0);
}
extern "C" void Coroutine_t6_14_marshal_back(const Coroutine_t6_14_marshaled& marshaled, Coroutine_t6_14& unmarshaled)
{
	(unmarshaled.___m_Ptr_0).___m_value_0 = reinterpret_cast<void*>(marshaled.___m_Ptr_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Coroutine
extern "C" void Coroutine_t6_14_marshal_cleanup(Coroutine_t6_14_marshaled& marshaled)
{
}
// System.Void UnityEngine.ScriptableObject::.ctor()
extern "C" void ScriptableObject__ctor_m6_16 (ScriptableObject_t6_15 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m6_701(__this, /*hidden argument*/NULL);
		ScriptableObject_Internal_CreateScriptableObject_m6_17(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ScriptableObject::Internal_CreateScriptableObject(UnityEngine.ScriptableObject)
extern "C" void ScriptableObject_Internal_CreateScriptableObject_m6_17 (Object_t * __this /* static, unused */, ScriptableObject_t6_15 * ___self, const MethodInfo* method)
{
	typedef void (*ScriptableObject_Internal_CreateScriptableObject_m6_17_ftn) (ScriptableObject_t6_15 *);
	static ScriptableObject_Internal_CreateScriptableObject_m6_17_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ScriptableObject_Internal_CreateScriptableObject_m6_17_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ScriptableObject::Internal_CreateScriptableObject(UnityEngine.ScriptableObject)");
	_il2cpp_icall_func(___self);
}
// UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstance(System.String)
extern "C" ScriptableObject_t6_15 * ScriptableObject_CreateInstance_m6_18 (Object_t * __this /* static, unused */, String_t* ___className, const MethodInfo* method)
{
	typedef ScriptableObject_t6_15 * (*ScriptableObject_CreateInstance_m6_18_ftn) (String_t*);
	static ScriptableObject_CreateInstance_m6_18_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ScriptableObject_CreateInstance_m6_18_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ScriptableObject::CreateInstance(System.String)");
	return _il2cpp_icall_func(___className);
}
// UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstance(System.Type)
extern "C" ScriptableObject_t6_15 * ScriptableObject_CreateInstance_m6_19 (Object_t * __this /* static, unused */, Type_t * ___type, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___type;
		ScriptableObject_t6_15 * L_1 = ScriptableObject_CreateInstanceFromType_m6_20(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstanceFromType(System.Type)
extern "C" ScriptableObject_t6_15 * ScriptableObject_CreateInstanceFromType_m6_20 (Object_t * __this /* static, unused */, Type_t * ___type, const MethodInfo* method)
{
	typedef ScriptableObject_t6_15 * (*ScriptableObject_CreateInstanceFromType_m6_20_ftn) (Type_t *);
	static ScriptableObject_CreateInstanceFromType_m6_20_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ScriptableObject_CreateInstanceFromType_m6_20_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ScriptableObject::CreateInstanceFromType(System.Type)");
	return _il2cpp_icall_func(___type);
}
// Conversion methods for marshalling of: UnityEngine.ScriptableObject
extern "C" void ScriptableObject_t6_15_marshal(const ScriptableObject_t6_15& unmarshaled, ScriptableObject_t6_15_marshaled& marshaled)
{
}
extern "C" void ScriptableObject_t6_15_marshal_back(const ScriptableObject_t6_15_marshaled& marshaled, ScriptableObject_t6_15& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: UnityEngine.ScriptableObject
extern "C" void ScriptableObject_t6_15_marshal_cleanup(ScriptableObject_t6_15_marshaled& marshaled)
{
}
// System.Void UnityEngine.UnhandledExceptionHandler::.ctor()
extern "C" void UnhandledExceptionHandler__ctor_m6_21 (UnhandledExceptionHandler_t6_16 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UnhandledExceptionHandler::RegisterUECatcher()
extern TypeInfo* UnhandledExceptionEventHandler_t1_1496_il2cpp_TypeInfo_var;
extern const MethodInfo* UnhandledExceptionHandler_HandleUnhandledException_m6_23_MethodInfo_var;
extern "C" void UnhandledExceptionHandler_RegisterUECatcher_m6_22 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnhandledExceptionEventHandler_t1_1496_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1064);
		UnhandledExceptionHandler_HandleUnhandledException_m6_23_MethodInfo_var = il2cpp_codegen_method_info_from_index(126);
		s_Il2CppMethodIntialized = true;
	}
	{
		AppDomain_t1_1403 * L_0 = AppDomain_get_CurrentDomain_m1_13075(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_1 = { (void*)UnhandledExceptionHandler_HandleUnhandledException_m6_23_MethodInfo_var };
		UnhandledExceptionEventHandler_t1_1496 * L_2 = (UnhandledExceptionEventHandler_t1_1496 *)il2cpp_codegen_object_new (UnhandledExceptionEventHandler_t1_1496_il2cpp_TypeInfo_var);
		UnhandledExceptionEventHandler__ctor_m1_14871(L_2, NULL, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		AppDomain_add_UnhandledException_m1_13058(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UnhandledExceptionHandler::HandleUnhandledException(System.Object,System.UnhandledExceptionEventArgs)
extern TypeInfo* Exception_t1_33_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4686;
extern "C" void UnhandledExceptionHandler_HandleUnhandledException_m6_23 (Object_t * __this /* static, unused */, Object_t * ___sender, UnhandledExceptionEventArgs_t1_1614 * ___args, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Exception_t1_33_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(49);
		_stringLiteral4686 = il2cpp_codegen_string_literal_from_index(4686);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1_33 * V_0 = {0};
	{
		UnhandledExceptionEventArgs_t1_1614 * L_0 = ___args;
		NullCheck(L_0);
		Object_t * L_1 = UnhandledExceptionEventArgs_get_ExceptionObject_m1_14710(L_0, /*hidden argument*/NULL);
		V_0 = ((Exception_t1_33 *)IsInstClass(L_1, Exception_t1_33_il2cpp_TypeInfo_var));
		Exception_t1_33 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_001d;
		}
	}
	{
		Exception_t1_33 * L_3 = V_0;
		UnhandledExceptionHandler_PrintException_m6_24(NULL /*static, unused*/, _stringLiteral4686, L_3, /*hidden argument*/NULL);
	}

IL_001d:
	{
		UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m6_25(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UnhandledExceptionHandler::PrintException(System.String,System.Exception)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4687;
extern "C" void UnhandledExceptionHandler_PrintException_m6_24 (Object_t * __this /* static, unused */, String_t* ___title, Exception_t1_33 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral4687 = il2cpp_codegen_string_literal_from_index(4687);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___title;
		Exception_t1_33 * L_1 = ___e;
		NullCheck(L_1);
		String_t* L_2 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Exception::ToString() */, L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m1_559(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		Debug_LogError_m6_632(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		Exception_t1_33 * L_4 = ___e;
		NullCheck(L_4);
		Exception_t1_33 * L_5 = (Exception_t1_33 *)VirtFuncInvoker0< Exception_t1_33 * >::Invoke(7 /* System.Exception System.Exception::get_InnerException() */, L_4);
		if (!L_5)
		{
			goto IL_002c;
		}
	}
	{
		Exception_t1_33 * L_6 = ___e;
		NullCheck(L_6);
		Exception_t1_33 * L_7 = (Exception_t1_33 *)VirtFuncInvoker0< Exception_t1_33 * >::Invoke(7 /* System.Exception System.Exception::get_InnerException() */, L_6);
		UnhandledExceptionHandler_PrintException_m6_24(NULL /*static, unused*/, _stringLiteral4687, L_7, /*hidden argument*/NULL);
	}

IL_002c:
	{
		return;
	}
}
// System.Void UnityEngine.UnhandledExceptionHandler::NativeUnhandledExceptionHandler()
extern "C" void UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m6_25 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m6_25_ftn) ();
	static UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m6_25_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m6_25_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.UnhandledExceptionHandler::NativeUnhandledExceptionHandler()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::.ctor()
extern "C" void GameCenterPlatform__ctor_m6_26 (GameCenterPlatform_t6_17 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::.cctor()
extern TypeInfo* AchievementDescriptionU5BU5D_t6_18_il2cpp_TypeInfo_var;
extern TypeInfo* GameCenterPlatform_t6_17_il2cpp_TypeInfo_var;
extern TypeInfo* UserProfileU5BU5D_t6_19_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t1_1820_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1_14935_MethodInfo_var;
extern "C" void GameCenterPlatform__cctor_m6_27 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AchievementDescriptionU5BU5D_t6_18_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1558);
		GameCenterPlatform_t6_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1560);
		UserProfileU5BU5D_t6_19_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1561);
		List_1_t1_1820_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1563);
		List_1__ctor_m1_14935_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483775);
		s_Il2CppMethodIntialized = true;
	}
	{
		((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___s_adCache_9 = ((AchievementDescriptionU5BU5D_t6_18*)SZArrayNew(AchievementDescriptionU5BU5D_t6_18_il2cpp_TypeInfo_var, 0));
		((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___s_friends_10 = ((UserProfileU5BU5D_t6_19*)SZArrayNew(UserProfileU5BU5D_t6_19_il2cpp_TypeInfo_var, 0));
		((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___s_users_11 = ((UserProfileU5BU5D_t6_19*)SZArrayNew(UserProfileU5BU5D_t6_19_il2cpp_TypeInfo_var, 0));
		List_1_t1_1820 * L_0 = (List_1_t1_1820 *)il2cpp_codegen_object_new (List_1_t1_1820_il2cpp_TypeInfo_var);
		List_1__ctor_m1_14935(L_0, /*hidden argument*/List_1__ctor_m1_14935_MethodInfo_var);
		((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___m_GcBoards_14 = L_0;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::UnityEngine.SocialPlatforms.ISocialPlatform.LoadFriends(UnityEngine.SocialPlatforms.ILocalUser,System.Action`1<System.Boolean>)
extern TypeInfo* GameCenterPlatform_t6_17_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_LoadFriends_m6_28 (GameCenterPlatform_t6_17 * __this, Object_t * ___user, Action_1_t1_1815 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1560);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t1_1815 * L_0 = ___callback;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_17_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___s_FriendsCallback_1 = L_0;
		GameCenterPlatform_Internal_LoadFriends_m6_36(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::UnityEngine.SocialPlatforms.ISocialPlatform.Authenticate(UnityEngine.SocialPlatforms.ILocalUser,System.Action`1<System.Boolean>)
extern TypeInfo* GameCenterPlatform_t6_17_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_Authenticate_m6_29 (GameCenterPlatform_t6_17 * __this, Object_t * ___user, Action_1_t1_1815 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1560);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t1_1815 * L_0 = ___callback;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_17_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___s_AuthenticateCallback_0 = L_0;
		GameCenterPlatform_Internal_Authenticate_m6_30(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Authenticate()
extern "C" void GameCenterPlatform_Internal_Authenticate_m6_30 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_Authenticate_m6_30_ftn) ();
	static GameCenterPlatform_Internal_Authenticate_m6_30_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_Authenticate_m6_30_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Authenticate()");
	_il2cpp_icall_func();
}
// System.Boolean UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Authenticated()
extern "C" bool GameCenterPlatform_Internal_Authenticated_m6_31 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*GameCenterPlatform_Internal_Authenticated_m6_31_ftn) ();
	static GameCenterPlatform_Internal_Authenticated_m6_31_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_Authenticated_m6_31_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Authenticated()");
	return _il2cpp_icall_func();
}
// System.String UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserName()
extern "C" String_t* GameCenterPlatform_Internal_UserName_m6_32 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*GameCenterPlatform_Internal_UserName_m6_32_ftn) ();
	static GameCenterPlatform_Internal_UserName_m6_32_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_UserName_m6_32_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserName()");
	return _il2cpp_icall_func();
}
// System.String UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserID()
extern "C" String_t* GameCenterPlatform_Internal_UserID_m6_33 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*GameCenterPlatform_Internal_UserID_m6_33_ftn) ();
	static GameCenterPlatform_Internal_UserID_m6_33_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_UserID_m6_33_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserID()");
	return _il2cpp_icall_func();
}
// System.Boolean UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Underage()
extern "C" bool GameCenterPlatform_Internal_Underage_m6_34 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*GameCenterPlatform_Internal_Underage_m6_34_ftn) ();
	static GameCenterPlatform_Internal_Underage_m6_34_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_Underage_m6_34_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Underage()");
	return _il2cpp_icall_func();
}
// UnityEngine.Texture2D UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserImage()
extern "C" Texture2D_t6_33 * GameCenterPlatform_Internal_UserImage_m6_35 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef Texture2D_t6_33 * (*GameCenterPlatform_Internal_UserImage_m6_35_ftn) ();
	static GameCenterPlatform_Internal_UserImage_m6_35_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_UserImage_m6_35_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserImage()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadFriends()
extern "C" void GameCenterPlatform_Internal_LoadFriends_m6_36 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_LoadFriends_m6_36_ftn) ();
	static GameCenterPlatform_Internal_LoadFriends_m6_36_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_LoadFriends_m6_36_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadFriends()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadAchievementDescriptions()
extern "C" void GameCenterPlatform_Internal_LoadAchievementDescriptions_m6_37 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_LoadAchievementDescriptions_m6_37_ftn) ();
	static GameCenterPlatform_Internal_LoadAchievementDescriptions_m6_37_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_LoadAchievementDescriptions_m6_37_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadAchievementDescriptions()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadAchievements()
extern "C" void GameCenterPlatform_Internal_LoadAchievements_m6_38 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_LoadAchievements_m6_38_ftn) ();
	static GameCenterPlatform_Internal_LoadAchievements_m6_38_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_LoadAchievements_m6_38_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadAchievements()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ReportProgress(System.String,System.Double)
extern "C" void GameCenterPlatform_Internal_ReportProgress_m6_39 (Object_t * __this /* static, unused */, String_t* ___id, double ___progress, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ReportProgress_m6_39_ftn) (String_t*, double);
	static GameCenterPlatform_Internal_ReportProgress_m6_39_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ReportProgress_m6_39_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ReportProgress(System.String,System.Double)");
	_il2cpp_icall_func(___id, ___progress);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ReportScore(System.Int64,System.String)
extern "C" void GameCenterPlatform_Internal_ReportScore_m6_40 (Object_t * __this /* static, unused */, int64_t ___score, String_t* ___category, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ReportScore_m6_40_ftn) (int64_t, String_t*);
	static GameCenterPlatform_Internal_ReportScore_m6_40_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ReportScore_m6_40_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ReportScore(System.Int64,System.String)");
	_il2cpp_icall_func(___score, ___category);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadScores(System.String)
extern "C" void GameCenterPlatform_Internal_LoadScores_m6_41 (Object_t * __this /* static, unused */, String_t* ___category, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_LoadScores_m6_41_ftn) (String_t*);
	static GameCenterPlatform_Internal_LoadScores_m6_41_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_LoadScores_m6_41_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadScores(System.String)");
	_il2cpp_icall_func(___category);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowAchievementsUI()
extern "C" void GameCenterPlatform_Internal_ShowAchievementsUI_m6_42 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ShowAchievementsUI_m6_42_ftn) ();
	static GameCenterPlatform_Internal_ShowAchievementsUI_m6_42_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ShowAchievementsUI_m6_42_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowAchievementsUI()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowLeaderboardUI()
extern "C" void GameCenterPlatform_Internal_ShowLeaderboardUI_m6_43 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ShowLeaderboardUI_m6_43_ftn) ();
	static GameCenterPlatform_Internal_ShowLeaderboardUI_m6_43_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ShowLeaderboardUI_m6_43_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowLeaderboardUI()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadUsers(System.String[])
extern "C" void GameCenterPlatform_Internal_LoadUsers_m6_44 (Object_t * __this /* static, unused */, StringU5BU5D_t1_238* ___userIds, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_LoadUsers_m6_44_ftn) (StringU5BU5D_t1_238*);
	static GameCenterPlatform_Internal_LoadUsers_m6_44_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_LoadUsers_m6_44_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadUsers(System.String[])");
	_il2cpp_icall_func(___userIds);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ResetAllAchievements()
extern "C" void GameCenterPlatform_Internal_ResetAllAchievements_m6_45 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ResetAllAchievements_m6_45_ftn) ();
	static GameCenterPlatform_Internal_ResetAllAchievements_m6_45_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ResetAllAchievements_m6_45_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ResetAllAchievements()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowDefaultAchievementBanner(System.Boolean)
extern "C" void GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m6_46 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m6_46_ftn) (bool);
	static GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m6_46_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m6_46_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowDefaultAchievementBanner(System.Boolean)");
	_il2cpp_icall_func(___value);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ResetAllAchievements(System.Action`1<System.Boolean>)
extern TypeInfo* GameCenterPlatform_t6_17_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_ResetAllAchievements_m6_47 (Object_t * __this /* static, unused */, Action_1_t1_1815 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1560);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t1_1815 * L_0 = ___callback;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_17_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___s_ResetAchievements_12 = L_0;
		GameCenterPlatform_Internal_ResetAllAchievements_m6_45(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ShowDefaultAchievementCompletionBanner(System.Boolean)
extern TypeInfo* GameCenterPlatform_t6_17_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_ShowDefaultAchievementCompletionBanner_m6_48 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1560);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_17_il2cpp_TypeInfo_var);
		GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m6_46(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ShowLeaderboardUI(System.String,UnityEngine.SocialPlatforms.TimeScope)
extern TypeInfo* GameCenterPlatform_t6_17_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_ShowLeaderboardUI_m6_49 (Object_t * __this /* static, unused */, String_t* ___leaderboardID, int32_t ___timeScope, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1560);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___leaderboardID;
		int32_t L_1 = ___timeScope;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_17_il2cpp_TypeInfo_var);
		GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m6_50(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowSpecificLeaderboardUI(System.String,System.Int32)
extern "C" void GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m6_50 (Object_t * __this /* static, unused */, String_t* ___leaderboardID, int32_t ___timeScope, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m6_50_ftn) (String_t*, int32_t);
	static GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m6_50_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m6_50_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowSpecificLeaderboardUI(System.String,System.Int32)");
	_il2cpp_icall_func(___leaderboardID, ___timeScope);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ClearAchievementDescriptions(System.Int32)
extern TypeInfo* GameCenterPlatform_t6_17_il2cpp_TypeInfo_var;
extern TypeInfo* AchievementDescriptionU5BU5D_t6_18_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_ClearAchievementDescriptions_m6_51 (Object_t * __this /* static, unused */, int32_t ___size, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1560);
		AchievementDescriptionU5BU5D_t6_18_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1558);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_17_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t6_18* L_0 = ((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___s_adCache_9;
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_17_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t6_18* L_1 = ((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___s_adCache_9;
		NullCheck(L_1);
		int32_t L_2 = ___size;
		if ((((int32_t)(((int32_t)((int32_t)(((Array_t *)L_1)->max_length))))) == ((int32_t)L_2)))
		{
			goto IL_0022;
		}
	}

IL_0017:
	{
		int32_t L_3 = ___size;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_17_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___s_adCache_9 = ((AchievementDescriptionU5BU5D_t6_18*)SZArrayNew(AchievementDescriptionU5BU5D_t6_18_il2cpp_TypeInfo_var, L_3));
	}

IL_0022:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetAchievementDescription(UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData,System.Int32)
extern TypeInfo* GameCenterPlatform_t6_17_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_SetAchievementDescription_m6_52 (Object_t * __this /* static, unused */, GcAchievementDescriptionData_t6_209  ___data, int32_t ___number, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1560);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_17_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t6_18* L_0 = ((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___s_adCache_9;
		int32_t L_1 = ___number;
		AchievementDescription_t6_224 * L_2 = GcAchievementDescriptionData_ToAchievementDescription_m6_1542((&___data), /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		ArrayElementTypeCheck (L_0, L_2);
		*((AchievementDescription_t6_224 **)(AchievementDescription_t6_224 **)SZArrayLdElema(L_0, L_1, sizeof(AchievementDescription_t6_224 *))) = (AchievementDescription_t6_224 *)L_2;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetAchievementDescriptionImage(UnityEngine.Texture2D,System.Int32)
extern TypeInfo* GameCenterPlatform_t6_17_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4688;
extern "C" void GameCenterPlatform_SetAchievementDescriptionImage_m6_53 (Object_t * __this /* static, unused */, Texture2D_t6_33 * ___texture, int32_t ___number, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1560);
		_stringLiteral4688 = il2cpp_codegen_string_literal_from_index(4688);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_17_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t6_18* L_0 = ((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___s_adCache_9;
		NullCheck(L_0);
		int32_t L_1 = ___number;
		if ((((int32_t)(((int32_t)((int32_t)(((Array_t *)L_0)->max_length))))) <= ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_2 = ___number;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_001f;
		}
	}

IL_0014:
	{
		Debug_Log_m6_631(NULL /*static, unused*/, _stringLiteral4688, /*hidden argument*/NULL);
		return;
	}

IL_001f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_17_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t6_18* L_3 = ((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___s_adCache_9;
		int32_t L_4 = ___number;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		Texture2D_t6_33 * L_6 = ___texture;
		NullCheck((*(AchievementDescription_t6_224 **)(AchievementDescription_t6_224 **)SZArrayLdElema(L_3, L_5, sizeof(AchievementDescription_t6_224 *))));
		AchievementDescription_SetImage_m6_1582((*(AchievementDescription_t6_224 **)(AchievementDescription_t6_224 **)SZArrayLdElema(L_3, L_5, sizeof(AchievementDescription_t6_224 *))), L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::TriggerAchievementDescriptionCallback()
extern TypeInfo* GameCenterPlatform_t6_17_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m1_14936_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4689;
extern "C" void GameCenterPlatform_TriggerAchievementDescriptionCallback_m6_54 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1560);
		Action_1_Invoke_m1_14936_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483776);
		_stringLiteral4689 = il2cpp_codegen_string_literal_from_index(4689);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_17_il2cpp_TypeInfo_var);
		Action_1_t1_1816 * L_0 = ((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___s_AchievementDescriptionLoaderCallback_2;
		if (!L_0)
		{
			goto IL_0039;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_17_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t6_18* L_1 = ((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___s_adCache_9;
		if (!L_1)
		{
			goto IL_0039;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_17_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t6_18* L_2 = ((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___s_adCache_9;
		NullCheck(L_2);
		if ((((int32_t)((int32_t)(((Array_t *)L_2)->max_length)))))
		{
			goto IL_002a;
		}
	}
	{
		Debug_Log_m6_631(NULL /*static, unused*/, _stringLiteral4689, /*hidden argument*/NULL);
	}

IL_002a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_17_il2cpp_TypeInfo_var);
		Action_1_t1_1816 * L_3 = ((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___s_AchievementDescriptionLoaderCallback_2;
		AchievementDescriptionU5BU5D_t6_18* L_4 = ((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___s_adCache_9;
		NullCheck(L_3);
		Action_1_Invoke_m1_14936(L_3, (IAchievementDescriptionU5BU5D_t6_291*)(IAchievementDescriptionU5BU5D_t6_291*)L_4, /*hidden argument*/Action_1_Invoke_m1_14936_MethodInfo_var);
	}

IL_0039:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::AuthenticateCallbackWrapper(System.Int32)
extern TypeInfo* GameCenterPlatform_t6_17_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m1_14937_MethodInfo_var;
extern "C" void GameCenterPlatform_AuthenticateCallbackWrapper_m6_55 (Object_t * __this /* static, unused */, int32_t ___result, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1560);
		Action_1_Invoke_m1_14937_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483777);
		s_Il2CppMethodIntialized = true;
	}
	Action_1_t1_1815 * G_B3_0 = {0};
	Action_1_t1_1815 * G_B2_0 = {0};
	int32_t G_B4_0 = 0;
	Action_1_t1_1815 * G_B4_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_17_il2cpp_TypeInfo_var);
		Action_1_t1_1815 * L_0 = ((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___s_AuthenticateCallback_0;
		if (!L_0)
		{
			goto IL_0027;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_17_il2cpp_TypeInfo_var);
		GameCenterPlatform_PopulateLocalUser_m6_65(NULL /*static, unused*/, /*hidden argument*/NULL);
		Action_1_t1_1815 * L_1 = ((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___s_AuthenticateCallback_0;
		int32_t L_2 = ___result;
		G_B2_0 = L_1;
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			G_B3_0 = L_1;
			goto IL_0021;
		}
	}
	{
		G_B4_0 = 1;
		G_B4_1 = G_B2_0;
		goto IL_0022;
	}

IL_0021:
	{
		G_B4_0 = 0;
		G_B4_1 = G_B3_0;
	}

IL_0022:
	{
		NullCheck(G_B4_1);
		Action_1_Invoke_m1_14937(G_B4_1, G_B4_0, /*hidden argument*/Action_1_Invoke_m1_14937_MethodInfo_var);
	}

IL_0027:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ClearFriends(System.Int32)
extern TypeInfo* GameCenterPlatform_t6_17_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_ClearFriends_m6_56 (Object_t * __this /* static, unused */, int32_t ___size, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1560);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_17_il2cpp_TypeInfo_var);
		int32_t L_0 = ___size;
		GameCenterPlatform_SafeClearArray_m6_83(NULL /*static, unused*/, (&((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___s_friends_10), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetFriends(UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData,System.Int32)
extern TypeInfo* GameCenterPlatform_t6_17_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_SetFriends_m6_57 (Object_t * __this /* static, unused */, GcUserProfileData_t6_208  ___data, int32_t ___number, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1560);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_17_il2cpp_TypeInfo_var);
		int32_t L_0 = ___number;
		GcUserProfileData_AddToArray_m6_1541((&___data), (&((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___s_friends_10), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetFriendImage(UnityEngine.Texture2D,System.Int32)
extern TypeInfo* GameCenterPlatform_t6_17_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_SetFriendImage_m6_58 (Object_t * __this /* static, unused */, Texture2D_t6_33 * ___texture, int32_t ___number, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1560);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_17_il2cpp_TypeInfo_var);
		Texture2D_t6_33 * L_0 = ___texture;
		int32_t L_1 = ___number;
		GameCenterPlatform_SafeSetUserImage_m6_82(NULL /*static, unused*/, (&((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___s_friends_10), L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::TriggerFriendsCallbackWrapper(System.Int32)
extern TypeInfo* GameCenterPlatform_t6_17_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m1_14937_MethodInfo_var;
extern "C" void GameCenterPlatform_TriggerFriendsCallbackWrapper_m6_59 (Object_t * __this /* static, unused */, int32_t ___result, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1560);
		Action_1_Invoke_m1_14937_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483777);
		s_Il2CppMethodIntialized = true;
	}
	Action_1_t1_1815 * G_B5_0 = {0};
	Action_1_t1_1815 * G_B4_0 = {0};
	int32_t G_B6_0 = 0;
	Action_1_t1_1815 * G_B6_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_17_il2cpp_TypeInfo_var);
		UserProfileU5BU5D_t6_19* L_0 = ((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___s_friends_10;
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_17_il2cpp_TypeInfo_var);
		LocalUser_t6_20 * L_1 = ((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___m_LocalUser_13;
		UserProfileU5BU5D_t6_19* L_2 = ((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___s_friends_10;
		NullCheck(L_1);
		LocalUser_SetFriends_m6_1555(L_1, (IUserProfileU5BU5D_t6_222*)(IUserProfileU5BU5D_t6_222*)L_2, /*hidden argument*/NULL);
	}

IL_0019:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_17_il2cpp_TypeInfo_var);
		Action_1_t1_1815 * L_3 = ((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___s_FriendsCallback_1;
		if (!L_3)
		{
			goto IL_003b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_17_il2cpp_TypeInfo_var);
		Action_1_t1_1815 * L_4 = ((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___s_FriendsCallback_1;
		int32_t L_5 = ___result;
		G_B4_0 = L_4;
		if ((!(((uint32_t)L_5) == ((uint32_t)1))))
		{
			G_B5_0 = L_4;
			goto IL_0035;
		}
	}
	{
		G_B6_0 = 1;
		G_B6_1 = G_B4_0;
		goto IL_0036;
	}

IL_0035:
	{
		G_B6_0 = 0;
		G_B6_1 = G_B5_0;
	}

IL_0036:
	{
		NullCheck(G_B6_1);
		Action_1_Invoke_m1_14937(G_B6_1, G_B6_0, /*hidden argument*/Action_1_Invoke_m1_14937_MethodInfo_var);
	}

IL_003b:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::AchievementCallbackWrapper(UnityEngine.SocialPlatforms.GameCenter.GcAchievementData[])
extern TypeInfo* GameCenterPlatform_t6_17_il2cpp_TypeInfo_var;
extern TypeInfo* AchievementU5BU5D_t6_292_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m1_14938_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4690;
extern "C" void GameCenterPlatform_AchievementCallbackWrapper_m6_60 (Object_t * __this /* static, unused */, GcAchievementDataU5BU5D_t6_273* ___result, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1560);
		AchievementU5BU5D_t6_292_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1568);
		Action_1_Invoke_m1_14938_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483778);
		_stringLiteral4690 = il2cpp_codegen_string_literal_from_index(4690);
		s_Il2CppMethodIntialized = true;
	}
	AchievementU5BU5D_t6_292* V_0 = {0};
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_17_il2cpp_TypeInfo_var);
		Action_1_t1_1817 * L_0 = ((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___s_AchievementLoaderCallback_3;
		if (!L_0)
		{
			goto IL_0053;
		}
	}
	{
		GcAchievementDataU5BU5D_t6_273* L_1 = ___result;
		NullCheck(L_1);
		if ((((int32_t)((int32_t)(((Array_t *)L_1)->max_length)))))
		{
			goto IL_001c;
		}
	}
	{
		Debug_Log_m6_631(NULL /*static, unused*/, _stringLiteral4690, /*hidden argument*/NULL);
	}

IL_001c:
	{
		GcAchievementDataU5BU5D_t6_273* L_2 = ___result;
		NullCheck(L_2);
		V_0 = ((AchievementU5BU5D_t6_292*)SZArrayNew(AchievementU5BU5D_t6_292_il2cpp_TypeInfo_var, (((int32_t)((int32_t)(((Array_t *)L_2)->max_length))))));
		V_1 = 0;
		goto IL_003f;
	}

IL_002c:
	{
		AchievementU5BU5D_t6_292* L_3 = V_0;
		int32_t L_4 = V_1;
		GcAchievementDataU5BU5D_t6_273* L_5 = ___result;
		int32_t L_6 = V_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		Achievement_t6_223 * L_7 = GcAchievementData_ToAchievement_m6_1543(((GcAchievementData_t6_210 *)(GcAchievementData_t6_210 *)SZArrayLdElema(L_5, L_6, sizeof(GcAchievementData_t6_210 ))), /*hidden argument*/NULL);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		ArrayElementTypeCheck (L_3, L_7);
		*((Achievement_t6_223 **)(Achievement_t6_223 **)SZArrayLdElema(L_3, L_4, sizeof(Achievement_t6_223 *))) = (Achievement_t6_223 *)L_7;
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_003f:
	{
		int32_t L_9 = V_1;
		GcAchievementDataU5BU5D_t6_273* L_10 = ___result;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_10)->max_length)))))))
		{
			goto IL_002c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_17_il2cpp_TypeInfo_var);
		Action_1_t1_1817 * L_11 = ((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___s_AchievementLoaderCallback_3;
		AchievementU5BU5D_t6_292* L_12 = V_0;
		NullCheck(L_11);
		Action_1_Invoke_m1_14938(L_11, (IAchievementU5BU5D_t6_293*)(IAchievementU5BU5D_t6_293*)L_12, /*hidden argument*/Action_1_Invoke_m1_14938_MethodInfo_var);
	}

IL_0053:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ProgressCallbackWrapper(System.Boolean)
extern TypeInfo* GameCenterPlatform_t6_17_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m1_14937_MethodInfo_var;
extern "C" void GameCenterPlatform_ProgressCallbackWrapper_m6_61 (Object_t * __this /* static, unused */, bool ___success, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1560);
		Action_1_Invoke_m1_14937_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483777);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_17_il2cpp_TypeInfo_var);
		Action_1_t1_1815 * L_0 = ((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___s_ProgressCallback_4;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_17_il2cpp_TypeInfo_var);
		Action_1_t1_1815 * L_1 = ((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___s_ProgressCallback_4;
		bool L_2 = ___success;
		NullCheck(L_1);
		Action_1_Invoke_m1_14937(L_1, L_2, /*hidden argument*/Action_1_Invoke_m1_14937_MethodInfo_var);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ScoreCallbackWrapper(System.Boolean)
extern TypeInfo* GameCenterPlatform_t6_17_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m1_14937_MethodInfo_var;
extern "C" void GameCenterPlatform_ScoreCallbackWrapper_m6_62 (Object_t * __this /* static, unused */, bool ___success, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1560);
		Action_1_Invoke_m1_14937_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483777);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_17_il2cpp_TypeInfo_var);
		Action_1_t1_1815 * L_0 = ((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___s_ScoreCallback_5;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_17_il2cpp_TypeInfo_var);
		Action_1_t1_1815 * L_1 = ((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___s_ScoreCallback_5;
		bool L_2 = ___success;
		NullCheck(L_1);
		Action_1_Invoke_m1_14937(L_1, L_2, /*hidden argument*/Action_1_Invoke_m1_14937_MethodInfo_var);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ScoreLoaderCallbackWrapper(UnityEngine.SocialPlatforms.GameCenter.GcScoreData[])
extern TypeInfo* GameCenterPlatform_t6_17_il2cpp_TypeInfo_var;
extern TypeInfo* ScoreU5BU5D_t6_294_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m1_14939_MethodInfo_var;
extern "C" void GameCenterPlatform_ScoreLoaderCallbackWrapper_m6_63 (Object_t * __this /* static, unused */, GcScoreDataU5BU5D_t6_274* ___result, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1560);
		ScoreU5BU5D_t6_294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1572);
		Action_1_Invoke_m1_14939_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483779);
		s_Il2CppMethodIntialized = true;
	}
	ScoreU5BU5D_t6_294* V_0 = {0};
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_17_il2cpp_TypeInfo_var);
		Action_1_t1_1818 * L_0 = ((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___s_ScoreLoaderCallback_6;
		if (!L_0)
		{
			goto IL_0041;
		}
	}
	{
		GcScoreDataU5BU5D_t6_274* L_1 = ___result;
		NullCheck(L_1);
		V_0 = ((ScoreU5BU5D_t6_294*)SZArrayNew(ScoreU5BU5D_t6_294_il2cpp_TypeInfo_var, (((int32_t)((int32_t)(((Array_t *)L_1)->max_length))))));
		V_1 = 0;
		goto IL_002d;
	}

IL_001a:
	{
		ScoreU5BU5D_t6_294* L_2 = V_0;
		int32_t L_3 = V_1;
		GcScoreDataU5BU5D_t6_274* L_4 = ___result;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		Score_t6_225 * L_6 = GcScoreData_ToScore_m6_1544(((GcScoreData_t6_211 *)(GcScoreData_t6_211 *)SZArrayLdElema(L_4, L_5, sizeof(GcScoreData_t6_211 ))), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		ArrayElementTypeCheck (L_2, L_6);
		*((Score_t6_225 **)(Score_t6_225 **)SZArrayLdElema(L_2, L_3, sizeof(Score_t6_225 *))) = (Score_t6_225 *)L_6;
		int32_t L_7 = V_1;
		V_1 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_002d:
	{
		int32_t L_8 = V_1;
		GcScoreDataU5BU5D_t6_274* L_9 = ___result;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_9)->max_length)))))))
		{
			goto IL_001a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_17_il2cpp_TypeInfo_var);
		Action_1_t1_1818 * L_10 = ((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___s_ScoreLoaderCallback_6;
		ScoreU5BU5D_t6_294* L_11 = V_0;
		NullCheck(L_10);
		Action_1_Invoke_m1_14939(L_10, (IScoreU5BU5D_t6_226*)(IScoreU5BU5D_t6_226*)L_11, /*hidden argument*/Action_1_Invoke_m1_14939_MethodInfo_var);
	}

IL_0041:
	{
		return;
	}
}
// UnityEngine.SocialPlatforms.ILocalUser UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::get_localUser()
extern TypeInfo* GameCenterPlatform_t6_17_il2cpp_TypeInfo_var;
extern TypeInfo* LocalUser_t6_20_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral166;
extern "C" Object_t * GameCenterPlatform_get_localUser_m6_64 (GameCenterPlatform_t6_17 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1560);
		LocalUser_t6_20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1574);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral166 = il2cpp_codegen_string_literal_from_index(166);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_17_il2cpp_TypeInfo_var);
		LocalUser_t6_20 * L_0 = ((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___m_LocalUser_13;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		LocalUser_t6_20 * L_1 = (LocalUser_t6_20 *)il2cpp_codegen_object_new (LocalUser_t6_20_il2cpp_TypeInfo_var);
		LocalUser__ctor_m6_1554(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_17_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___m_LocalUser_13 = L_1;
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_17_il2cpp_TypeInfo_var);
		bool L_2 = GameCenterPlatform_Internal_Authenticated_m6_31(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_003c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_17_il2cpp_TypeInfo_var);
		LocalUser_t6_20 * L_3 = ((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___m_LocalUser_13;
		NullCheck(L_3);
		String_t* L_4 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(5 /* System.String UnityEngine.SocialPlatforms.Impl.UserProfile::get_id() */, L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_op_Equality_m1_601(NULL /*static, unused*/, L_4, _stringLiteral166, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_003c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_17_il2cpp_TypeInfo_var);
		GameCenterPlatform_PopulateLocalUser_m6_65(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_003c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_17_il2cpp_TypeInfo_var);
		LocalUser_t6_20 * L_6 = ((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___m_LocalUser_13;
		return L_6;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::PopulateLocalUser()
extern TypeInfo* GameCenterPlatform_t6_17_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_PopulateLocalUser_m6_65 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1560);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_17_il2cpp_TypeInfo_var);
		LocalUser_t6_20 * L_0 = ((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___m_LocalUser_13;
		bool L_1 = GameCenterPlatform_Internal_Authenticated_m6_31(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		LocalUser_SetAuthenticated_m6_1556(L_0, L_1, /*hidden argument*/NULL);
		LocalUser_t6_20 * L_2 = ((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___m_LocalUser_13;
		String_t* L_3 = GameCenterPlatform_Internal_UserName_m6_32(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		UserProfile_SetUserName_m6_1562(L_2, L_3, /*hidden argument*/NULL);
		LocalUser_t6_20 * L_4 = ((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___m_LocalUser_13;
		String_t* L_5 = GameCenterPlatform_Internal_UserID_m6_33(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		UserProfile_SetUserID_m6_1563(L_4, L_5, /*hidden argument*/NULL);
		LocalUser_t6_20 * L_6 = ((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___m_LocalUser_13;
		bool L_7 = GameCenterPlatform_Internal_Underage_m6_34(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		LocalUser_SetUnderage_m6_1557(L_6, L_7, /*hidden argument*/NULL);
		LocalUser_t6_20 * L_8 = ((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___m_LocalUser_13;
		Texture2D_t6_33 * L_9 = GameCenterPlatform_Internal_UserImage_m6_35(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		UserProfile_SetImage_m6_1564(L_8, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LoadAchievementDescriptions(System.Action`1<UnityEngine.SocialPlatforms.IAchievementDescription[]>)
extern TypeInfo* AchievementDescriptionU5BU5D_t6_18_il2cpp_TypeInfo_var;
extern TypeInfo* GameCenterPlatform_t6_17_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m1_14936_MethodInfo_var;
extern "C" void GameCenterPlatform_LoadAchievementDescriptions_m6_66 (GameCenterPlatform_t6_17 * __this, Action_1_t1_1816 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AchievementDescriptionU5BU5D_t6_18_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1558);
		GameCenterPlatform_t6_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1560);
		Action_1_Invoke_m1_14936_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483776);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m6_74(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		Action_1_t1_1816 * L_1 = ___callback;
		NullCheck(L_1);
		Action_1_Invoke_m1_14936(L_1, (IAchievementDescriptionU5BU5D_t6_291*)(IAchievementDescriptionU5BU5D_t6_291*)((AchievementDescriptionU5BU5D_t6_18*)SZArrayNew(AchievementDescriptionU5BU5D_t6_18_il2cpp_TypeInfo_var, 0)), /*hidden argument*/Action_1_Invoke_m1_14936_MethodInfo_var);
		return;
	}

IL_0018:
	{
		Action_1_t1_1816 * L_2 = ___callback;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_17_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___s_AchievementDescriptionLoaderCallback_2 = L_2;
		GameCenterPlatform_Internal_LoadAchievementDescriptions_m6_37(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ReportProgress(System.String,System.Double,System.Action`1<System.Boolean>)
extern TypeInfo* GameCenterPlatform_t6_17_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m1_14937_MethodInfo_var;
extern "C" void GameCenterPlatform_ReportProgress_m6_67 (GameCenterPlatform_t6_17 * __this, String_t* ___id, double ___progress, Action_1_t1_1815 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1560);
		Action_1_Invoke_m1_14937_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483777);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m6_74(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		Action_1_t1_1815 * L_1 = ___callback;
		NullCheck(L_1);
		Action_1_Invoke_m1_14937(L_1, 0, /*hidden argument*/Action_1_Invoke_m1_14937_MethodInfo_var);
		return;
	}

IL_0013:
	{
		Action_1_t1_1815 * L_2 = ___callback;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_17_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___s_ProgressCallback_4 = L_2;
		String_t* L_3 = ___id;
		double L_4 = ___progress;
		GameCenterPlatform_Internal_ReportProgress_m6_39(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LoadAchievements(System.Action`1<UnityEngine.SocialPlatforms.IAchievement[]>)
extern TypeInfo* AchievementU5BU5D_t6_292_il2cpp_TypeInfo_var;
extern TypeInfo* GameCenterPlatform_t6_17_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m1_14938_MethodInfo_var;
extern "C" void GameCenterPlatform_LoadAchievements_m6_68 (GameCenterPlatform_t6_17 * __this, Action_1_t1_1817 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AchievementU5BU5D_t6_292_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1568);
		GameCenterPlatform_t6_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1560);
		Action_1_Invoke_m1_14938_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483778);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m6_74(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		Action_1_t1_1817 * L_1 = ___callback;
		NullCheck(L_1);
		Action_1_Invoke_m1_14938(L_1, (IAchievementU5BU5D_t6_293*)(IAchievementU5BU5D_t6_293*)((AchievementU5BU5D_t6_292*)SZArrayNew(AchievementU5BU5D_t6_292_il2cpp_TypeInfo_var, 0)), /*hidden argument*/Action_1_Invoke_m1_14938_MethodInfo_var);
		return;
	}

IL_0018:
	{
		Action_1_t1_1817 * L_2 = ___callback;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_17_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___s_AchievementLoaderCallback_3 = L_2;
		GameCenterPlatform_Internal_LoadAchievements_m6_38(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ReportScore(System.Int64,System.String,System.Action`1<System.Boolean>)
extern TypeInfo* GameCenterPlatform_t6_17_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m1_14937_MethodInfo_var;
extern "C" void GameCenterPlatform_ReportScore_m6_69 (GameCenterPlatform_t6_17 * __this, int64_t ___score, String_t* ___board, Action_1_t1_1815 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1560);
		Action_1_Invoke_m1_14937_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483777);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m6_74(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		Action_1_t1_1815 * L_1 = ___callback;
		NullCheck(L_1);
		Action_1_Invoke_m1_14937(L_1, 0, /*hidden argument*/Action_1_Invoke_m1_14937_MethodInfo_var);
		return;
	}

IL_0013:
	{
		Action_1_t1_1815 * L_2 = ___callback;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_17_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___s_ScoreCallback_5 = L_2;
		int64_t L_3 = ___score;
		String_t* L_4 = ___board;
		GameCenterPlatform_Internal_ReportScore_m6_40(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LoadScores(System.String,System.Action`1<UnityEngine.SocialPlatforms.IScore[]>)
extern TypeInfo* ScoreU5BU5D_t6_294_il2cpp_TypeInfo_var;
extern TypeInfo* GameCenterPlatform_t6_17_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m1_14939_MethodInfo_var;
extern "C" void GameCenterPlatform_LoadScores_m6_70 (GameCenterPlatform_t6_17 * __this, String_t* ___category, Action_1_t1_1818 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ScoreU5BU5D_t6_294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1572);
		GameCenterPlatform_t6_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1560);
		Action_1_Invoke_m1_14939_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483779);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m6_74(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		Action_1_t1_1818 * L_1 = ___callback;
		NullCheck(L_1);
		Action_1_Invoke_m1_14939(L_1, (IScoreU5BU5D_t6_226*)(IScoreU5BU5D_t6_226*)((ScoreU5BU5D_t6_294*)SZArrayNew(ScoreU5BU5D_t6_294_il2cpp_TypeInfo_var, 0)), /*hidden argument*/Action_1_Invoke_m1_14939_MethodInfo_var);
		return;
	}

IL_0018:
	{
		Action_1_t1_1818 * L_2 = ___callback;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_17_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___s_ScoreLoaderCallback_6 = L_2;
		String_t* L_3 = ___category;
		GameCenterPlatform_Internal_LoadScores_m6_41(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LoadScores(UnityEngine.SocialPlatforms.ILeaderboard,System.Action`1<System.Boolean>)
extern TypeInfo* GameCenterPlatform_t6_17_il2cpp_TypeInfo_var;
extern TypeInfo* Leaderboard_t6_22_il2cpp_TypeInfo_var;
extern TypeInfo* GcLeaderboard_t6_21_il2cpp_TypeInfo_var;
extern TypeInfo* ILeaderboard_t6_276_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m1_14937_MethodInfo_var;
extern "C" void GameCenterPlatform_LoadScores_m6_71 (GameCenterPlatform_t6_17 * __this, Object_t * ___board, Action_1_t1_1815 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1560);
		Leaderboard_t6_22_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1575);
		GcLeaderboard_t6_21_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1557);
		ILeaderboard_t6_276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1576);
		Action_1_Invoke_m1_14937_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483777);
		s_Il2CppMethodIntialized = true;
	}
	Leaderboard_t6_22 * V_0 = {0};
	GcLeaderboard_t6_21 * V_1 = {0};
	Range_t6_227  V_2 = {0};
	Range_t6_227  V_3 = {0};
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m6_74(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		Action_1_t1_1815 * L_1 = ___callback;
		NullCheck(L_1);
		Action_1_Invoke_m1_14937(L_1, 0, /*hidden argument*/Action_1_Invoke_m1_14937_MethodInfo_var);
		return;
	}

IL_0013:
	{
		Action_1_t1_1815 * L_2 = ___callback;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_17_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___s_LeaderboardCallback_7 = L_2;
		Object_t * L_3 = ___board;
		V_0 = ((Leaderboard_t6_22 *)CastclassClass(L_3, Leaderboard_t6_22_il2cpp_TypeInfo_var));
		Leaderboard_t6_22 * L_4 = V_0;
		GcLeaderboard_t6_21 * L_5 = (GcLeaderboard_t6_21 *)il2cpp_codegen_object_new (GcLeaderboard_t6_21_il2cpp_TypeInfo_var);
		GcLeaderboard__ctor_m6_87(L_5, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		List_1_t1_1820 * L_6 = ((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___m_GcBoards_14;
		GcLeaderboard_t6_21 * L_7 = V_1;
		NullCheck(L_6);
		VirtActionInvoker1< GcLeaderboard_t6_21 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::Add(!0) */, L_6, L_7);
		Leaderboard_t6_22 * L_8 = V_0;
		NullCheck(L_8);
		StringU5BU5D_t1_238* L_9 = Leaderboard_GetUserFilter_m6_1603(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		if ((((int32_t)(((int32_t)((int32_t)(((Array_t *)L_9)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_005d;
		}
	}
	{
		GcLeaderboard_t6_21 * L_10 = V_1;
		Object_t * L_11 = ___board;
		NullCheck(L_11);
		String_t* L_12 = (String_t*)InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String UnityEngine.SocialPlatforms.ILeaderboard::get_id() */, ILeaderboard_t6_276_il2cpp_TypeInfo_var, L_11);
		Object_t * L_13 = ___board;
		NullCheck(L_13);
		int32_t L_14 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(3 /* UnityEngine.SocialPlatforms.TimeScope UnityEngine.SocialPlatforms.ILeaderboard::get_timeScope() */, ILeaderboard_t6_276_il2cpp_TypeInfo_var, L_13);
		Leaderboard_t6_22 * L_15 = V_0;
		NullCheck(L_15);
		StringU5BU5D_t1_238* L_16 = Leaderboard_GetUserFilter_m6_1603(L_15, /*hidden argument*/NULL);
		NullCheck(L_10);
		GcLeaderboard_Internal_LoadScoresWithUsers_m6_95(L_10, L_12, L_14, L_16, /*hidden argument*/NULL);
		goto IL_0091;
	}

IL_005d:
	{
		GcLeaderboard_t6_21 * L_17 = V_1;
		Object_t * L_18 = ___board;
		NullCheck(L_18);
		String_t* L_19 = (String_t*)InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String UnityEngine.SocialPlatforms.ILeaderboard::get_id() */, ILeaderboard_t6_276_il2cpp_TypeInfo_var, L_18);
		Object_t * L_20 = ___board;
		NullCheck(L_20);
		Range_t6_227  L_21 = (Range_t6_227 )InterfaceFuncInvoker0< Range_t6_227  >::Invoke(2 /* UnityEngine.SocialPlatforms.Range UnityEngine.SocialPlatforms.ILeaderboard::get_range() */, ILeaderboard_t6_276_il2cpp_TypeInfo_var, L_20);
		V_2 = L_21;
		int32_t L_22 = ((&V_2)->___from_0);
		Object_t * L_23 = ___board;
		NullCheck(L_23);
		Range_t6_227  L_24 = (Range_t6_227 )InterfaceFuncInvoker0< Range_t6_227  >::Invoke(2 /* UnityEngine.SocialPlatforms.Range UnityEngine.SocialPlatforms.ILeaderboard::get_range() */, ILeaderboard_t6_276_il2cpp_TypeInfo_var, L_23);
		V_3 = L_24;
		int32_t L_25 = ((&V_3)->___count_1);
		Object_t * L_26 = ___board;
		NullCheck(L_26);
		int32_t L_27 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(1 /* UnityEngine.SocialPlatforms.UserScope UnityEngine.SocialPlatforms.ILeaderboard::get_userScope() */, ILeaderboard_t6_276_il2cpp_TypeInfo_var, L_26);
		Object_t * L_28 = ___board;
		NullCheck(L_28);
		int32_t L_29 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(3 /* UnityEngine.SocialPlatforms.TimeScope UnityEngine.SocialPlatforms.ILeaderboard::get_timeScope() */, ILeaderboard_t6_276_il2cpp_TypeInfo_var, L_28);
		NullCheck(L_17);
		GcLeaderboard_Internal_LoadScores_m6_94(L_17, L_19, L_22, L_25, L_27, L_29, /*hidden argument*/NULL);
	}

IL_0091:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LeaderboardCallbackWrapper(System.Boolean)
extern TypeInfo* GameCenterPlatform_t6_17_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m1_14937_MethodInfo_var;
extern "C" void GameCenterPlatform_LeaderboardCallbackWrapper_m6_72 (Object_t * __this /* static, unused */, bool ___success, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1560);
		Action_1_Invoke_m1_14937_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483777);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_17_il2cpp_TypeInfo_var);
		Action_1_t1_1815 * L_0 = ((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___s_LeaderboardCallback_7;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_17_il2cpp_TypeInfo_var);
		Action_1_t1_1815 * L_1 = ((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___s_LeaderboardCallback_7;
		bool L_2 = ___success;
		NullCheck(L_1);
		Action_1_Invoke_m1_14937(L_1, L_2, /*hidden argument*/Action_1_Invoke_m1_14937_MethodInfo_var);
	}

IL_0015:
	{
		return;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::GetLoading(UnityEngine.SocialPlatforms.ILeaderboard)
extern TypeInfo* GameCenterPlatform_t6_17_il2cpp_TypeInfo_var;
extern TypeInfo* Leaderboard_t6_22_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t1_1844_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m1_14940_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1_14941_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1_14942_MethodInfo_var;
extern "C" bool GameCenterPlatform_GetLoading_m6_73 (GameCenterPlatform_t6_17 * __this, Object_t * ___board, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1560);
		Leaderboard_t6_22_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1575);
		Enumerator_t1_1844_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1577);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		List_1_GetEnumerator_m1_14940_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483780);
		Enumerator_get_Current_m1_14941_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483781);
		Enumerator_MoveNext_m1_14942_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483782);
		s_Il2CppMethodIntialized = true;
	}
	GcLeaderboard_t6_21 * V_0 = {0};
	Enumerator_t1_1844  V_1 = {0};
	bool V_2 = false;
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m6_74(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_17_il2cpp_TypeInfo_var);
		List_1_t1_1820 * L_1 = ((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___m_GcBoards_14;
		NullCheck(L_1);
		Enumerator_t1_1844  L_2 = List_1_GetEnumerator_m1_14940(L_1, /*hidden argument*/List_1_GetEnumerator_m1_14940_MethodInfo_var);
		V_1 = L_2;
	}

IL_0018:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0042;
		}

IL_001d:
		{
			GcLeaderboard_t6_21 * L_3 = Enumerator_get_Current_m1_14941((&V_1), /*hidden argument*/Enumerator_get_Current_m1_14941_MethodInfo_var);
			V_0 = L_3;
			GcLeaderboard_t6_21 * L_4 = V_0;
			Object_t * L_5 = ___board;
			NullCheck(L_4);
			bool L_6 = GcLeaderboard_Contains_m6_89(L_4, ((Leaderboard_t6_22 *)CastclassClass(L_5, Leaderboard_t6_22_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
			if (!L_6)
			{
				goto IL_0042;
			}
		}

IL_0036:
		{
			GcLeaderboard_t6_21 * L_7 = V_0;
			NullCheck(L_7);
			bool L_8 = GcLeaderboard_Loading_m6_96(L_7, /*hidden argument*/NULL);
			V_2 = L_8;
			IL2CPP_LEAVE(0x61, FINALLY_0053);
		}

IL_0042:
		{
			bool L_9 = Enumerator_MoveNext_m1_14942((&V_1), /*hidden argument*/Enumerator_MoveNext_m1_14942_MethodInfo_var);
			if (L_9)
			{
				goto IL_001d;
			}
		}

IL_004e:
		{
			IL2CPP_LEAVE(0x5F, FINALLY_0053);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0053;
	}

FINALLY_0053:
	{ // begin finally (depth: 1)
		Enumerator_t1_1844  L_10 = V_1;
		Enumerator_t1_1844  L_11 = L_10;
		Object_t * L_12 = Box(Enumerator_t1_1844_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_12);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_12);
		IL2CPP_END_FINALLY(83)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(83)
	{
		IL2CPP_JUMP_TBL(0x61, IL_0061)
		IL2CPP_JUMP_TBL(0x5F, IL_005f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_005f:
	{
		return 0;
	}

IL_0061:
	{
		bool L_13 = V_2;
		return L_13;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::VerifyAuthentication()
extern TypeInfo* ILocalUser_t6_275_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4691;
extern "C" bool GameCenterPlatform_VerifyAuthentication_m6_74 (GameCenterPlatform_t6_17 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILocalUser_t6_275_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1578);
		_stringLiteral4691 = il2cpp_codegen_string_literal_from_index(4691);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = GameCenterPlatform_get_localUser_m6_64(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = (bool)InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean UnityEngine.SocialPlatforms.ILocalUser::get_authenticated() */, ILocalUser_t6_275_il2cpp_TypeInfo_var, L_0);
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		Debug_Log_m6_631(NULL /*static, unused*/, _stringLiteral4691, /*hidden argument*/NULL);
		return 0;
	}

IL_001c:
	{
		return 1;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ShowAchievementsUI()
extern TypeInfo* GameCenterPlatform_t6_17_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_ShowAchievementsUI_m6_75 (GameCenterPlatform_t6_17 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1560);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_17_il2cpp_TypeInfo_var);
		GameCenterPlatform_Internal_ShowAchievementsUI_m6_42(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ShowLeaderboardUI()
extern TypeInfo* GameCenterPlatform_t6_17_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_ShowLeaderboardUI_m6_76 (GameCenterPlatform_t6_17 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1560);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_17_il2cpp_TypeInfo_var);
		GameCenterPlatform_Internal_ShowLeaderboardUI_m6_43(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ClearUsers(System.Int32)
extern TypeInfo* GameCenterPlatform_t6_17_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_ClearUsers_m6_77 (Object_t * __this /* static, unused */, int32_t ___size, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1560);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_17_il2cpp_TypeInfo_var);
		int32_t L_0 = ___size;
		GameCenterPlatform_SafeClearArray_m6_83(NULL /*static, unused*/, (&((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___s_users_11), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetUser(UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData,System.Int32)
extern TypeInfo* GameCenterPlatform_t6_17_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_SetUser_m6_78 (Object_t * __this /* static, unused */, GcUserProfileData_t6_208  ___data, int32_t ___number, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1560);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_17_il2cpp_TypeInfo_var);
		int32_t L_0 = ___number;
		GcUserProfileData_AddToArray_m6_1541((&___data), (&((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___s_users_11), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetUserImage(UnityEngine.Texture2D,System.Int32)
extern TypeInfo* GameCenterPlatform_t6_17_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_SetUserImage_m6_79 (Object_t * __this /* static, unused */, Texture2D_t6_33 * ___texture, int32_t ___number, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1560);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_17_il2cpp_TypeInfo_var);
		Texture2D_t6_33 * L_0 = ___texture;
		int32_t L_1 = ___number;
		GameCenterPlatform_SafeSetUserImage_m6_82(NULL /*static, unused*/, (&((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___s_users_11), L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::TriggerUsersCallbackWrapper()
extern TypeInfo* GameCenterPlatform_t6_17_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m1_14943_MethodInfo_var;
extern "C" void GameCenterPlatform_TriggerUsersCallbackWrapper_m6_80 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1560);
		Action_1_Invoke_m1_14943_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483783);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_17_il2cpp_TypeInfo_var);
		Action_1_t1_1819 * L_0 = ((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___s_UsersCallback_8;
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_17_il2cpp_TypeInfo_var);
		Action_1_t1_1819 * L_1 = ((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___s_UsersCallback_8;
		UserProfileU5BU5D_t6_19* L_2 = ((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___s_users_11;
		NullCheck(L_1);
		Action_1_Invoke_m1_14943(L_1, (IUserProfileU5BU5D_t6_222*)(IUserProfileU5BU5D_t6_222*)L_2, /*hidden argument*/Action_1_Invoke_m1_14943_MethodInfo_var);
	}

IL_0019:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LoadUsers(System.String[],System.Action`1<UnityEngine.SocialPlatforms.IUserProfile[]>)
extern TypeInfo* UserProfileU5BU5D_t6_19_il2cpp_TypeInfo_var;
extern TypeInfo* GameCenterPlatform_t6_17_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m1_14943_MethodInfo_var;
extern "C" void GameCenterPlatform_LoadUsers_m6_81 (GameCenterPlatform_t6_17 * __this, StringU5BU5D_t1_238* ___userIds, Action_1_t1_1819 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UserProfileU5BU5D_t6_19_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1561);
		GameCenterPlatform_t6_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1560);
		Action_1_Invoke_m1_14943_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483783);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m6_74(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		Action_1_t1_1819 * L_1 = ___callback;
		NullCheck(L_1);
		Action_1_Invoke_m1_14943(L_1, (IUserProfileU5BU5D_t6_222*)(IUserProfileU5BU5D_t6_222*)((UserProfileU5BU5D_t6_19*)SZArrayNew(UserProfileU5BU5D_t6_19_il2cpp_TypeInfo_var, 0)), /*hidden argument*/Action_1_Invoke_m1_14943_MethodInfo_var);
		return;
	}

IL_0018:
	{
		Action_1_t1_1819 * L_2 = ___callback;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_17_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___s_UsersCallback_8 = L_2;
		StringU5BU5D_t1_238* L_3 = ___userIds;
		GameCenterPlatform_Internal_LoadUsers_m6_44(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SafeSetUserImage(UnityEngine.SocialPlatforms.Impl.UserProfile[]&,UnityEngine.Texture2D,System.Int32)
extern TypeInfo* Texture2D_t6_33_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4692;
extern Il2CppCodeGenString* _stringLiteral4693;
extern "C" void GameCenterPlatform_SafeSetUserImage_m6_82 (Object_t * __this /* static, unused */, UserProfileU5BU5D_t6_19** ___array, Texture2D_t6_33 * ___texture, int32_t ___number, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Texture2D_t6_33_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1581);
		_stringLiteral4692 = il2cpp_codegen_string_literal_from_index(4692);
		_stringLiteral4693 = il2cpp_codegen_string_literal_from_index(4693);
		s_Il2CppMethodIntialized = true;
	}
	{
		UserProfileU5BU5D_t6_19** L_0 = ___array;
		NullCheck((*((UserProfileU5BU5D_t6_19**)L_0)));
		int32_t L_1 = ___number;
		if ((((int32_t)(((int32_t)((int32_t)(((Array_t *)(*((UserProfileU5BU5D_t6_19**)L_0)))->max_length))))) <= ((int32_t)L_1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = ___number;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0026;
		}
	}

IL_0011:
	{
		Debug_Log_m6_631(NULL /*static, unused*/, _stringLiteral4692, /*hidden argument*/NULL);
		Texture2D_t6_33 * L_3 = (Texture2D_t6_33 *)il2cpp_codegen_object_new (Texture2D_t6_33_il2cpp_TypeInfo_var);
		Texture2D__ctor_m6_155(L_3, ((int32_t)76), ((int32_t)76), /*hidden argument*/NULL);
		___texture = L_3;
	}

IL_0026:
	{
		UserProfileU5BU5D_t6_19** L_4 = ___array;
		NullCheck((*((UserProfileU5BU5D_t6_19**)L_4)));
		int32_t L_5 = ___number;
		if ((((int32_t)(((int32_t)((int32_t)(((Array_t *)(*((UserProfileU5BU5D_t6_19**)L_4)))->max_length))))) <= ((int32_t)L_5)))
		{
			goto IL_0046;
		}
	}
	{
		int32_t L_6 = ___number;
		if ((((int32_t)L_6) < ((int32_t)0)))
		{
			goto IL_0046;
		}
	}
	{
		UserProfileU5BU5D_t6_19** L_7 = ___array;
		int32_t L_8 = ___number;
		NullCheck((*((UserProfileU5BU5D_t6_19**)L_7)));
		IL2CPP_ARRAY_BOUNDS_CHECK((*((UserProfileU5BU5D_t6_19**)L_7)), L_8);
		int32_t L_9 = L_8;
		Texture2D_t6_33 * L_10 = ___texture;
		NullCheck((*(UserProfile_t6_221 **)(UserProfile_t6_221 **)SZArrayLdElema((*((UserProfileU5BU5D_t6_19**)L_7)), L_9, sizeof(UserProfile_t6_221 *))));
		UserProfile_SetImage_m6_1564((*(UserProfile_t6_221 **)(UserProfile_t6_221 **)SZArrayLdElema((*((UserProfileU5BU5D_t6_19**)L_7)), L_9, sizeof(UserProfile_t6_221 *))), L_10, /*hidden argument*/NULL);
		goto IL_0050;
	}

IL_0046:
	{
		Debug_Log_m6_631(NULL /*static, unused*/, _stringLiteral4693, /*hidden argument*/NULL);
	}

IL_0050:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SafeClearArray(UnityEngine.SocialPlatforms.Impl.UserProfile[]&,System.Int32)
extern TypeInfo* UserProfileU5BU5D_t6_19_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_SafeClearArray_m6_83 (Object_t * __this /* static, unused */, UserProfileU5BU5D_t6_19** ___array, int32_t ___size, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UserProfileU5BU5D_t6_19_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1561);
		s_Il2CppMethodIntialized = true;
	}
	{
		UserProfileU5BU5D_t6_19** L_0 = ___array;
		if (!(*((UserProfileU5BU5D_t6_19**)L_0)))
		{
			goto IL_0011;
		}
	}
	{
		UserProfileU5BU5D_t6_19** L_1 = ___array;
		NullCheck((*((UserProfileU5BU5D_t6_19**)L_1)));
		int32_t L_2 = ___size;
		if ((((int32_t)(((int32_t)((int32_t)(((Array_t *)(*((UserProfileU5BU5D_t6_19**)L_1)))->max_length))))) == ((int32_t)L_2)))
		{
			goto IL_0019;
		}
	}

IL_0011:
	{
		UserProfileU5BU5D_t6_19** L_3 = ___array;
		int32_t L_4 = ___size;
		*((Object_t **)(L_3)) = (Object_t *)((UserProfileU5BU5D_t6_19*)SZArrayNew(UserProfileU5BU5D_t6_19_il2cpp_TypeInfo_var, L_4));
	}

IL_0019:
	{
		return;
	}
}
// UnityEngine.SocialPlatforms.ILeaderboard UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::CreateLeaderboard()
extern TypeInfo* Leaderboard_t6_22_il2cpp_TypeInfo_var;
extern "C" Object_t * GameCenterPlatform_CreateLeaderboard_m6_84 (GameCenterPlatform_t6_17 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Leaderboard_t6_22_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1575);
		s_Il2CppMethodIntialized = true;
	}
	Leaderboard_t6_22 * V_0 = {0};
	{
		Leaderboard_t6_22 * L_0 = (Leaderboard_t6_22 *)il2cpp_codegen_object_new (Leaderboard_t6_22_il2cpp_TypeInfo_var);
		Leaderboard__ctor_m6_1597(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Leaderboard_t6_22 * L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.SocialPlatforms.IAchievement UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::CreateAchievement()
extern TypeInfo* Achievement_t6_223_il2cpp_TypeInfo_var;
extern "C" Object_t * GameCenterPlatform_CreateAchievement_m6_85 (GameCenterPlatform_t6_17 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Achievement_t6_223_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1569);
		s_Il2CppMethodIntialized = true;
	}
	Achievement_t6_223 * V_0 = {0};
	{
		Achievement_t6_223 * L_0 = (Achievement_t6_223 *)il2cpp_codegen_object_new (Achievement_t6_223_il2cpp_TypeInfo_var);
		Achievement__ctor_m6_1571(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Achievement_t6_223 * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::TriggerResetAchievementCallback(System.Boolean)
extern TypeInfo* GameCenterPlatform_t6_17_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m1_14937_MethodInfo_var;
extern "C" void GameCenterPlatform_TriggerResetAchievementCallback_m6_86 (Object_t * __this /* static, unused */, bool ___result, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t6_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1560);
		Action_1_Invoke_m1_14937_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483777);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_17_il2cpp_TypeInfo_var);
		Action_1_t1_1815 * L_0 = ((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___s_ResetAchievements_12;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t6_17_il2cpp_TypeInfo_var);
		Action_1_t1_1815 * L_1 = ((GameCenterPlatform_t6_17_StaticFields*)GameCenterPlatform_t6_17_il2cpp_TypeInfo_var->static_fields)->___s_ResetAchievements_12;
		bool L_2 = ___result;
		NullCheck(L_1);
		Action_1_Invoke_m1_14937(L_1, L_2, /*hidden argument*/Action_1_Invoke_m1_14937_MethodInfo_var);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::.ctor(UnityEngine.SocialPlatforms.Impl.Leaderboard)
extern "C" void GcLeaderboard__ctor_m6_87 (GcLeaderboard_t6_21 * __this, Leaderboard_t6_22 * ___board, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		Leaderboard_t6_22 * L_0 = ___board;
		__this->___m_GenericLeaderboard_1 = L_0;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Finalize()
extern "C" void GcLeaderboard_Finalize_m6_88 (GcLeaderboard_t6_21 * __this, const MethodInfo* method)
{
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		GcLeaderboard_Dispose_m6_97(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m1_3(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0012:
	{
		return;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Contains(UnityEngine.SocialPlatforms.Impl.Leaderboard)
extern "C" bool GcLeaderboard_Contains_m6_89 (GcLeaderboard_t6_21 * __this, Leaderboard_t6_22 * ___board, const MethodInfo* method)
{
	{
		Leaderboard_t6_22 * L_0 = (__this->___m_GenericLeaderboard_1);
		Leaderboard_t6_22 * L_1 = ___board;
		return ((((Object_t*)(Leaderboard_t6_22 *)L_0) == ((Object_t*)(Leaderboard_t6_22 *)L_1))? 1 : 0);
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::SetScores(UnityEngine.SocialPlatforms.GameCenter.GcScoreData[])
extern TypeInfo* ScoreU5BU5D_t6_294_il2cpp_TypeInfo_var;
extern "C" void GcLeaderboard_SetScores_m6_90 (GcLeaderboard_t6_21 * __this, GcScoreDataU5BU5D_t6_274* ___scoreDatas, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ScoreU5BU5D_t6_294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1572);
		s_Il2CppMethodIntialized = true;
	}
	ScoreU5BU5D_t6_294* V_0 = {0};
	int32_t V_1 = 0;
	{
		Leaderboard_t6_22 * L_0 = (__this->___m_GenericLeaderboard_1);
		if (!L_0)
		{
			goto IL_0043;
		}
	}
	{
		GcScoreDataU5BU5D_t6_274* L_1 = ___scoreDatas;
		NullCheck(L_1);
		V_0 = ((ScoreU5BU5D_t6_294*)SZArrayNew(ScoreU5BU5D_t6_294_il2cpp_TypeInfo_var, (((int32_t)((int32_t)(((Array_t *)L_1)->max_length))))));
		V_1 = 0;
		goto IL_002e;
	}

IL_001b:
	{
		ScoreU5BU5D_t6_294* L_2 = V_0;
		int32_t L_3 = V_1;
		GcScoreDataU5BU5D_t6_274* L_4 = ___scoreDatas;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		Score_t6_225 * L_6 = GcScoreData_ToScore_m6_1544(((GcScoreData_t6_211 *)(GcScoreData_t6_211 *)SZArrayLdElema(L_4, L_5, sizeof(GcScoreData_t6_211 ))), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		ArrayElementTypeCheck (L_2, L_6);
		*((Score_t6_225 **)(Score_t6_225 **)SZArrayLdElema(L_2, L_3, sizeof(Score_t6_225 *))) = (Score_t6_225 *)L_6;
		int32_t L_7 = V_1;
		V_1 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_002e:
	{
		int32_t L_8 = V_1;
		GcScoreDataU5BU5D_t6_274* L_9 = ___scoreDatas;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_9)->max_length)))))))
		{
			goto IL_001b;
		}
	}
	{
		Leaderboard_t6_22 * L_10 = (__this->___m_GenericLeaderboard_1);
		ScoreU5BU5D_t6_294* L_11 = V_0;
		NullCheck(L_10);
		Leaderboard_SetScores_m6_1601(L_10, (IScoreU5BU5D_t6_226*)(IScoreU5BU5D_t6_226*)L_11, /*hidden argument*/NULL);
	}

IL_0043:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::SetLocalScore(UnityEngine.SocialPlatforms.GameCenter.GcScoreData)
extern "C" void GcLeaderboard_SetLocalScore_m6_91 (GcLeaderboard_t6_21 * __this, GcScoreData_t6_211  ___scoreData, const MethodInfo* method)
{
	{
		Leaderboard_t6_22 * L_0 = (__this->___m_GenericLeaderboard_1);
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Leaderboard_t6_22 * L_1 = (__this->___m_GenericLeaderboard_1);
		Score_t6_225 * L_2 = GcScoreData_ToScore_m6_1544((&___scoreData), /*hidden argument*/NULL);
		NullCheck(L_1);
		Leaderboard_SetLocalUserScore_m6_1599(L_1, L_2, /*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::SetMaxRange(System.UInt32)
extern "C" void GcLeaderboard_SetMaxRange_m6_92 (GcLeaderboard_t6_21 * __this, uint32_t ___maxRange, const MethodInfo* method)
{
	{
		Leaderboard_t6_22 * L_0 = (__this->___m_GenericLeaderboard_1);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Leaderboard_t6_22 * L_1 = (__this->___m_GenericLeaderboard_1);
		uint32_t L_2 = ___maxRange;
		NullCheck(L_1);
		Leaderboard_SetMaxRange_m6_1600(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::SetTitle(System.String)
extern "C" void GcLeaderboard_SetTitle_m6_93 (GcLeaderboard_t6_21 * __this, String_t* ___title, const MethodInfo* method)
{
	{
		Leaderboard_t6_22 * L_0 = (__this->___m_GenericLeaderboard_1);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Leaderboard_t6_22 * L_1 = (__this->___m_GenericLeaderboard_1);
		String_t* L_2 = ___title;
		NullCheck(L_1);
		Leaderboard_SetTitle_m6_1602(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Internal_LoadScores(System.String,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" void GcLeaderboard_Internal_LoadScores_m6_94 (GcLeaderboard_t6_21 * __this, String_t* ___category, int32_t ___from, int32_t ___count, int32_t ___playerScope, int32_t ___timeScope, const MethodInfo* method)
{
	typedef void (*GcLeaderboard_Internal_LoadScores_m6_94_ftn) (GcLeaderboard_t6_21 *, String_t*, int32_t, int32_t, int32_t, int32_t);
	static GcLeaderboard_Internal_LoadScores_m6_94_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GcLeaderboard_Internal_LoadScores_m6_94_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Internal_LoadScores(System.String,System.Int32,System.Int32,System.Int32,System.Int32)");
	_il2cpp_icall_func(__this, ___category, ___from, ___count, ___playerScope, ___timeScope);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Internal_LoadScoresWithUsers(System.String,System.Int32,System.String[])
extern "C" void GcLeaderboard_Internal_LoadScoresWithUsers_m6_95 (GcLeaderboard_t6_21 * __this, String_t* ___category, int32_t ___timeScope, StringU5BU5D_t1_238* ___userIDs, const MethodInfo* method)
{
	typedef void (*GcLeaderboard_Internal_LoadScoresWithUsers_m6_95_ftn) (GcLeaderboard_t6_21 *, String_t*, int32_t, StringU5BU5D_t1_238*);
	static GcLeaderboard_Internal_LoadScoresWithUsers_m6_95_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GcLeaderboard_Internal_LoadScoresWithUsers_m6_95_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Internal_LoadScoresWithUsers(System.String,System.Int32,System.String[])");
	_il2cpp_icall_func(__this, ___category, ___timeScope, ___userIDs);
}
// System.Boolean UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Loading()
extern "C" bool GcLeaderboard_Loading_m6_96 (GcLeaderboard_t6_21 * __this, const MethodInfo* method)
{
	typedef bool (*GcLeaderboard_Loading_m6_96_ftn) (GcLeaderboard_t6_21 *);
	static GcLeaderboard_Loading_m6_96_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GcLeaderboard_Loading_m6_96_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Loading()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Dispose()
extern "C" void GcLeaderboard_Dispose_m6_97 (GcLeaderboard_t6_21 * __this, const MethodInfo* method)
{
	typedef void (*GcLeaderboard_Dispose_m6_97_ftn) (GcLeaderboard_t6_21 *);
	static GcLeaderboard_Dispose_m6_97_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GcLeaderboard_Dispose_m6_97_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Dispose()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Mesh::.ctor()
extern "C" void Mesh__ctor_m6_98 (Mesh_t6_23 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m6_701(__this, /*hidden argument*/NULL);
		Mesh_Internal_Create_m6_99(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Mesh::Internal_Create(UnityEngine.Mesh)
extern "C" void Mesh_Internal_Create_m6_99 (Object_t * __this /* static, unused */, Mesh_t6_23 * ___mono, const MethodInfo* method)
{
	typedef void (*Mesh_Internal_Create_m6_99_ftn) (Mesh_t6_23 *);
	static Mesh_Internal_Create_m6_99_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_Internal_Create_m6_99_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::Internal_Create(UnityEngine.Mesh)");
	_il2cpp_icall_func(___mono);
}
// System.Void UnityEngine.Mesh::Clear(System.Boolean)
extern "C" void Mesh_Clear_m6_100 (Mesh_t6_23 * __this, bool ___keepVertexLayout, const MethodInfo* method)
{
	typedef void (*Mesh_Clear_m6_100_ftn) (Mesh_t6_23 *, bool);
	static Mesh_Clear_m6_100_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_Clear_m6_100_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::Clear(System.Boolean)");
	_il2cpp_icall_func(__this, ___keepVertexLayout);
}
// System.Void UnityEngine.Mesh::Clear()
extern "C" void Mesh_Clear_m6_101 (Mesh_t6_23 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = 1;
		bool L_0 = V_0;
		Mesh_Clear_m6_100(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3[] UnityEngine.Mesh::get_vertices()
extern "C" Vector3U5BU5D_t6_161* Mesh_get_vertices_m6_102 (Mesh_t6_23 * __this, const MethodInfo* method)
{
	typedef Vector3U5BU5D_t6_161* (*Mesh_get_vertices_m6_102_ftn) (Mesh_t6_23 *);
	static Mesh_get_vertices_m6_102_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_get_vertices_m6_102_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::get_vertices()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Mesh::SetVertices(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern "C" void Mesh_SetVertices_m6_103 (Mesh_t6_23 * __this, List_1_t1_1834 * ___inVertices, const MethodInfo* method)
{
	{
		List_1_t1_1834 * L_0 = ___inVertices;
		Mesh_SetVerticesInternal_m6_104(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Mesh::SetVerticesInternal(System.Object)
extern "C" void Mesh_SetVerticesInternal_m6_104 (Mesh_t6_23 * __this, Object_t * ___vertices, const MethodInfo* method)
{
	typedef void (*Mesh_SetVerticesInternal_m6_104_ftn) (Mesh_t6_23 *, Object_t *);
	static Mesh_SetVerticesInternal_m6_104_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_SetVerticesInternal_m6_104_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::SetVerticesInternal(System.Object)");
	_il2cpp_icall_func(__this, ___vertices);
}
// UnityEngine.Vector3[] UnityEngine.Mesh::get_normals()
extern "C" Vector3U5BU5D_t6_161* Mesh_get_normals_m6_105 (Mesh_t6_23 * __this, const MethodInfo* method)
{
	typedef Vector3U5BU5D_t6_161* (*Mesh_get_normals_m6_105_ftn) (Mesh_t6_23 *);
	static Mesh_get_normals_m6_105_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_get_normals_m6_105_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::get_normals()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Mesh::SetNormals(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern "C" void Mesh_SetNormals_m6_106 (Mesh_t6_23 * __this, List_1_t1_1834 * ___inNormals, const MethodInfo* method)
{
	{
		List_1_t1_1834 * L_0 = ___inNormals;
		Mesh_SetNormalsInternal_m6_107(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Mesh::SetNormalsInternal(System.Object)
extern "C" void Mesh_SetNormalsInternal_m6_107 (Mesh_t6_23 * __this, Object_t * ___normals, const MethodInfo* method)
{
	typedef void (*Mesh_SetNormalsInternal_m6_107_ftn) (Mesh_t6_23 *, Object_t *);
	static Mesh_SetNormalsInternal_m6_107_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_SetNormalsInternal_m6_107_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::SetNormalsInternal(System.Object)");
	_il2cpp_icall_func(__this, ___normals);
}
// UnityEngine.Vector4[] UnityEngine.Mesh::get_tangents()
extern "C" Vector4U5BU5D_t6_278* Mesh_get_tangents_m6_108 (Mesh_t6_23 * __this, const MethodInfo* method)
{
	typedef Vector4U5BU5D_t6_278* (*Mesh_get_tangents_m6_108_ftn) (Mesh_t6_23 *);
	static Mesh_get_tangents_m6_108_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_get_tangents_m6_108_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::get_tangents()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Mesh::SetTangents(System.Collections.Generic.List`1<UnityEngine.Vector4>)
extern "C" void Mesh_SetTangents_m6_109 (Mesh_t6_23 * __this, List_1_t1_1835 * ___inTangents, const MethodInfo* method)
{
	{
		List_1_t1_1835 * L_0 = ___inTangents;
		Mesh_SetTangentsInternal_m6_110(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Mesh::SetTangentsInternal(System.Object)
extern "C" void Mesh_SetTangentsInternal_m6_110 (Mesh_t6_23 * __this, Object_t * ___tangents, const MethodInfo* method)
{
	typedef void (*Mesh_SetTangentsInternal_m6_110_ftn) (Mesh_t6_23 *, Object_t *);
	static Mesh_SetTangentsInternal_m6_110_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_SetTangentsInternal_m6_110_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::SetTangentsInternal(System.Object)");
	_il2cpp_icall_func(__this, ___tangents);
}
// UnityEngine.Vector2[] UnityEngine.Mesh::get_uv()
extern "C" Vector2U5BU5D_t6_279* Mesh_get_uv_m6_111 (Mesh_t6_23 * __this, const MethodInfo* method)
{
	typedef Vector2U5BU5D_t6_279* (*Mesh_get_uv_m6_111_ftn) (Mesh_t6_23 *);
	static Mesh_get_uv_m6_111_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_get_uv_m6_111_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::get_uv()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Vector2[] UnityEngine.Mesh::get_uv2()
extern "C" Vector2U5BU5D_t6_279* Mesh_get_uv2_m6_112 (Mesh_t6_23 * __this, const MethodInfo* method)
{
	typedef Vector2U5BU5D_t6_279* (*Mesh_get_uv2_m6_112_ftn) (Mesh_t6_23 *);
	static Mesh_get_uv2_m6_112_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_get_uv2_m6_112_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::get_uv2()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Mesh::SetUVs(System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector2>)
extern "C" void Mesh_SetUVs_m6_113 (Mesh_t6_23 * __this, int32_t ___channel, List_1_t1_1836 * ___uvs, const MethodInfo* method)
{
	{
		List_1_t1_1836 * L_0 = ___uvs;
		int32_t L_1 = ___channel;
		Mesh_SetUVInternal_m6_114(__this, L_0, L_1, 2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Mesh::SetUVInternal(System.Object,System.Int32,System.Int32)
extern "C" void Mesh_SetUVInternal_m6_114 (Mesh_t6_23 * __this, Object_t * ___uvs, int32_t ___channel, int32_t ___dim, const MethodInfo* method)
{
	typedef void (*Mesh_SetUVInternal_m6_114_ftn) (Mesh_t6_23 *, Object_t *, int32_t, int32_t);
	static Mesh_SetUVInternal_m6_114_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_SetUVInternal_m6_114_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::SetUVInternal(System.Object,System.Int32,System.Int32)");
	_il2cpp_icall_func(__this, ___uvs, ___channel, ___dim);
}
// UnityEngine.Color32[] UnityEngine.Mesh::get_colors32()
extern "C" Color32U5BU5D_t6_280* Mesh_get_colors32_m6_115 (Mesh_t6_23 * __this, const MethodInfo* method)
{
	typedef Color32U5BU5D_t6_280* (*Mesh_get_colors32_m6_115_ftn) (Mesh_t6_23 *);
	static Mesh_get_colors32_m6_115_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_get_colors32_m6_115_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::get_colors32()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Mesh::SetColors(System.Collections.Generic.List`1<UnityEngine.Color32>)
extern "C" void Mesh_SetColors_m6_116 (Mesh_t6_23 * __this, List_1_t1_1837 * ___inColors, const MethodInfo* method)
{
	{
		List_1_t1_1837 * L_0 = ___inColors;
		Mesh_SetColors32Internal_m6_117(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Mesh::SetColors32Internal(System.Object)
extern "C" void Mesh_SetColors32Internal_m6_117 (Mesh_t6_23 * __this, Object_t * ___colors, const MethodInfo* method)
{
	typedef void (*Mesh_SetColors32Internal_m6_117_ftn) (Mesh_t6_23 *, Object_t *);
	static Mesh_SetColors32Internal_m6_117_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_SetColors32Internal_m6_117_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::SetColors32Internal(System.Object)");
	_il2cpp_icall_func(__this, ___colors);
}
// System.Void UnityEngine.Mesh::RecalculateBounds()
extern "C" void Mesh_RecalculateBounds_m6_118 (Mesh_t6_23 * __this, const MethodInfo* method)
{
	typedef void (*Mesh_RecalculateBounds_m6_118_ftn) (Mesh_t6_23 *);
	static Mesh_RecalculateBounds_m6_118_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_RecalculateBounds_m6_118_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::RecalculateBounds()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Mesh::SetTriangles(System.Collections.Generic.List`1<System.Int32>,System.Int32)
extern "C" void Mesh_SetTriangles_m6_119 (Mesh_t6_23 * __this, List_1_t1_1838 * ___inTriangles, int32_t ___submesh, const MethodInfo* method)
{
	{
		List_1_t1_1838 * L_0 = ___inTriangles;
		int32_t L_1 = ___submesh;
		Mesh_SetTrianglesInternal_m6_120(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Mesh::SetTrianglesInternal(System.Object,System.Int32)
extern "C" void Mesh_SetTrianglesInternal_m6_120 (Mesh_t6_23 * __this, Object_t * ___triangles, int32_t ___submesh, const MethodInfo* method)
{
	typedef void (*Mesh_SetTrianglesInternal_m6_120_ftn) (Mesh_t6_23 *, Object_t *, int32_t);
	static Mesh_SetTrianglesInternal_m6_120_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_SetTrianglesInternal_m6_120_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::SetTrianglesInternal(System.Object,System.Int32)");
	_il2cpp_icall_func(__this, ___triangles, ___submesh);
}
// System.Int32[] UnityEngine.Mesh::GetIndices(System.Int32)
extern "C" Int32U5BU5D_t1_275* Mesh_GetIndices_m6_121 (Mesh_t6_23 * __this, int32_t ___submesh, const MethodInfo* method)
{
	typedef Int32U5BU5D_t1_275* (*Mesh_GetIndices_m6_121_ftn) (Mesh_t6_23 *, int32_t);
	static Mesh_GetIndices_m6_121_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_GetIndices_m6_121_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::GetIndices(System.Int32)");
	return _il2cpp_icall_func(__this, ___submesh);
}
// System.Single UnityEngine.BoneWeight::get_weight0()
extern "C" float BoneWeight_get_weight0_m6_122 (BoneWeight_t6_24 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Weight0_0);
		return L_0;
	}
}
// System.Void UnityEngine.BoneWeight::set_weight0(System.Single)
extern "C" void BoneWeight_set_weight0_m6_123 (BoneWeight_t6_24 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_Weight0_0 = L_0;
		return;
	}
}
// System.Single UnityEngine.BoneWeight::get_weight1()
extern "C" float BoneWeight_get_weight1_m6_124 (BoneWeight_t6_24 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Weight1_1);
		return L_0;
	}
}
// System.Void UnityEngine.BoneWeight::set_weight1(System.Single)
extern "C" void BoneWeight_set_weight1_m6_125 (BoneWeight_t6_24 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_Weight1_1 = L_0;
		return;
	}
}
// System.Single UnityEngine.BoneWeight::get_weight2()
extern "C" float BoneWeight_get_weight2_m6_126 (BoneWeight_t6_24 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Weight2_2);
		return L_0;
	}
}
// System.Void UnityEngine.BoneWeight::set_weight2(System.Single)
extern "C" void BoneWeight_set_weight2_m6_127 (BoneWeight_t6_24 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_Weight2_2 = L_0;
		return;
	}
}
// System.Single UnityEngine.BoneWeight::get_weight3()
extern "C" float BoneWeight_get_weight3_m6_128 (BoneWeight_t6_24 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Weight3_3);
		return L_0;
	}
}
// System.Void UnityEngine.BoneWeight::set_weight3(System.Single)
extern "C" void BoneWeight_set_weight3_m6_129 (BoneWeight_t6_24 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_Weight3_3 = L_0;
		return;
	}
}
// System.Int32 UnityEngine.BoneWeight::get_boneIndex0()
extern "C" int32_t BoneWeight_get_boneIndex0_m6_130 (BoneWeight_t6_24 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_BoneIndex0_4);
		return L_0;
	}
}
// System.Void UnityEngine.BoneWeight::set_boneIndex0(System.Int32)
extern "C" void BoneWeight_set_boneIndex0_m6_131 (BoneWeight_t6_24 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_BoneIndex0_4 = L_0;
		return;
	}
}
// System.Int32 UnityEngine.BoneWeight::get_boneIndex1()
extern "C" int32_t BoneWeight_get_boneIndex1_m6_132 (BoneWeight_t6_24 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_BoneIndex1_5);
		return L_0;
	}
}
// System.Void UnityEngine.BoneWeight::set_boneIndex1(System.Int32)
extern "C" void BoneWeight_set_boneIndex1_m6_133 (BoneWeight_t6_24 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_BoneIndex1_5 = L_0;
		return;
	}
}
// System.Int32 UnityEngine.BoneWeight::get_boneIndex2()
extern "C" int32_t BoneWeight_get_boneIndex2_m6_134 (BoneWeight_t6_24 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_BoneIndex2_6);
		return L_0;
	}
}
// System.Void UnityEngine.BoneWeight::set_boneIndex2(System.Int32)
extern "C" void BoneWeight_set_boneIndex2_m6_135 (BoneWeight_t6_24 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_BoneIndex2_6 = L_0;
		return;
	}
}
// System.Int32 UnityEngine.BoneWeight::get_boneIndex3()
extern "C" int32_t BoneWeight_get_boneIndex3_m6_136 (BoneWeight_t6_24 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_BoneIndex3_7);
		return L_0;
	}
}
// System.Void UnityEngine.BoneWeight::set_boneIndex3(System.Int32)
extern "C" void BoneWeight_set_boneIndex3_m6_137 (BoneWeight_t6_24 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_BoneIndex3_7 = L_0;
		return;
	}
}
// System.Int32 UnityEngine.BoneWeight::GetHashCode()
extern "C" int32_t BoneWeight_GetHashCode_m6_138 (BoneWeight_t6_24 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	{
		int32_t L_0 = BoneWeight_get_boneIndex0_m6_130(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Int32_GetHashCode_m1_81((&V_0), /*hidden argument*/NULL);
		int32_t L_2 = BoneWeight_get_boneIndex1_m6_132(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = Int32_GetHashCode_m1_81((&V_1), /*hidden argument*/NULL);
		int32_t L_4 = BoneWeight_get_boneIndex2_m6_134(__this, /*hidden argument*/NULL);
		V_2 = L_4;
		int32_t L_5 = Int32_GetHashCode_m1_81((&V_2), /*hidden argument*/NULL);
		int32_t L_6 = BoneWeight_get_boneIndex3_m6_136(__this, /*hidden argument*/NULL);
		V_3 = L_6;
		int32_t L_7 = Int32_GetHashCode_m1_81((&V_3), /*hidden argument*/NULL);
		float L_8 = BoneWeight_get_weight0_m6_122(__this, /*hidden argument*/NULL);
		V_4 = L_8;
		int32_t L_9 = Single_GetHashCode_m1_622((&V_4), /*hidden argument*/NULL);
		float L_10 = BoneWeight_get_weight1_m6_124(__this, /*hidden argument*/NULL);
		V_5 = L_10;
		int32_t L_11 = Single_GetHashCode_m1_622((&V_5), /*hidden argument*/NULL);
		float L_12 = BoneWeight_get_weight2_m6_126(__this, /*hidden argument*/NULL);
		V_6 = L_12;
		int32_t L_13 = Single_GetHashCode_m1_622((&V_6), /*hidden argument*/NULL);
		float L_14 = BoneWeight_get_weight3_m6_128(__this, /*hidden argument*/NULL);
		V_7 = L_14;
		int32_t L_15 = Single_GetHashCode_m1_622((&V_7), /*hidden argument*/NULL);
		return ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)1))))^(int32_t)((int32_t)((int32_t)L_9<<(int32_t)5))))^(int32_t)((int32_t)((int32_t)L_11<<(int32_t)4))))^(int32_t)((int32_t)((int32_t)L_13>>(int32_t)4))))^(int32_t)((int32_t)((int32_t)L_15>>(int32_t)3))));
	}
}
// System.Boolean UnityEngine.BoneWeight::Equals(System.Object)
extern TypeInfo* BoneWeight_t6_24_il2cpp_TypeInfo_var;
extern TypeInfo* Vector4_t6_54_il2cpp_TypeInfo_var;
extern "C" bool BoneWeight_Equals_m6_139 (BoneWeight_t6_24 * __this, Object_t * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		BoneWeight_t6_24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1582);
		Vector4_t6_54_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1583);
		s_Il2CppMethodIntialized = true;
	}
	BoneWeight_t6_24  V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	Vector4_t6_54  V_5 = {0};
	int32_t G_B8_0 = 0;
	{
		Object_t * L_0 = ___other;
		if (((Object_t *)IsInstSealed(L_0, BoneWeight_t6_24_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		Object_t * L_1 = ___other;
		V_0 = ((*(BoneWeight_t6_24 *)((BoneWeight_t6_24 *)UnBox (L_1, BoneWeight_t6_24_il2cpp_TypeInfo_var))));
		int32_t L_2 = BoneWeight_get_boneIndex0_m6_130(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = BoneWeight_get_boneIndex0_m6_130((&V_0), /*hidden argument*/NULL);
		bool L_4 = Int32_Equals_m1_83((&V_1), L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_00cb;
		}
	}
	{
		int32_t L_5 = BoneWeight_get_boneIndex1_m6_132(__this, /*hidden argument*/NULL);
		V_2 = L_5;
		int32_t L_6 = BoneWeight_get_boneIndex1_m6_132((&V_0), /*hidden argument*/NULL);
		bool L_7 = Int32_Equals_m1_83((&V_2), L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_00cb;
		}
	}
	{
		int32_t L_8 = BoneWeight_get_boneIndex2_m6_134(__this, /*hidden argument*/NULL);
		V_3 = L_8;
		int32_t L_9 = BoneWeight_get_boneIndex2_m6_134((&V_0), /*hidden argument*/NULL);
		bool L_10 = Int32_Equals_m1_83((&V_3), L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_00cb;
		}
	}
	{
		int32_t L_11 = BoneWeight_get_boneIndex3_m6_136(__this, /*hidden argument*/NULL);
		V_4 = L_11;
		int32_t L_12 = BoneWeight_get_boneIndex3_m6_136((&V_0), /*hidden argument*/NULL);
		bool L_13 = Int32_Equals_m1_83((&V_4), L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_00cb;
		}
	}
	{
		float L_14 = BoneWeight_get_weight0_m6_122(__this, /*hidden argument*/NULL);
		float L_15 = BoneWeight_get_weight1_m6_124(__this, /*hidden argument*/NULL);
		float L_16 = BoneWeight_get_weight2_m6_126(__this, /*hidden argument*/NULL);
		float L_17 = BoneWeight_get_weight3_m6_128(__this, /*hidden argument*/NULL);
		Vector4__ctor_m6_399((&V_5), L_14, L_15, L_16, L_17, /*hidden argument*/NULL);
		float L_18 = BoneWeight_get_weight0_m6_122((&V_0), /*hidden argument*/NULL);
		float L_19 = BoneWeight_get_weight1_m6_124((&V_0), /*hidden argument*/NULL);
		float L_20 = BoneWeight_get_weight2_m6_126((&V_0), /*hidden argument*/NULL);
		float L_21 = BoneWeight_get_weight3_m6_128((&V_0), /*hidden argument*/NULL);
		Vector4_t6_54  L_22 = {0};
		Vector4__ctor_m6_399(&L_22, L_18, L_19, L_20, L_21, /*hidden argument*/NULL);
		Vector4_t6_54  L_23 = L_22;
		Object_t * L_24 = Box(Vector4_t6_54_il2cpp_TypeInfo_var, &L_23);
		bool L_25 = Vector4_Equals_m6_403((&V_5), L_24, /*hidden argument*/NULL);
		G_B8_0 = ((int32_t)(L_25));
		goto IL_00cc;
	}

IL_00cb:
	{
		G_B8_0 = 0;
	}

IL_00cc:
	{
		return G_B8_0;
	}
}
// System.Boolean UnityEngine.BoneWeight::op_Equality(UnityEngine.BoneWeight,UnityEngine.BoneWeight)
extern "C" bool BoneWeight_op_Equality_m6_140 (Object_t * __this /* static, unused */, BoneWeight_t6_24  ___lhs, BoneWeight_t6_24  ___rhs, const MethodInfo* method)
{
	int32_t G_B6_0 = 0;
	{
		int32_t L_0 = BoneWeight_get_boneIndex0_m6_130((&___lhs), /*hidden argument*/NULL);
		int32_t L_1 = BoneWeight_get_boneIndex0_m6_130((&___rhs), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0095;
		}
	}
	{
		int32_t L_2 = BoneWeight_get_boneIndex1_m6_132((&___lhs), /*hidden argument*/NULL);
		int32_t L_3 = BoneWeight_get_boneIndex1_m6_132((&___rhs), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) == ((uint32_t)L_3))))
		{
			goto IL_0095;
		}
	}
	{
		int32_t L_4 = BoneWeight_get_boneIndex2_m6_134((&___lhs), /*hidden argument*/NULL);
		int32_t L_5 = BoneWeight_get_boneIndex2_m6_134((&___rhs), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)L_5))))
		{
			goto IL_0095;
		}
	}
	{
		int32_t L_6 = BoneWeight_get_boneIndex3_m6_136((&___lhs), /*hidden argument*/NULL);
		int32_t L_7 = BoneWeight_get_boneIndex3_m6_136((&___rhs), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)L_7))))
		{
			goto IL_0095;
		}
	}
	{
		float L_8 = BoneWeight_get_weight0_m6_122((&___lhs), /*hidden argument*/NULL);
		float L_9 = BoneWeight_get_weight1_m6_124((&___lhs), /*hidden argument*/NULL);
		float L_10 = BoneWeight_get_weight2_m6_126((&___lhs), /*hidden argument*/NULL);
		float L_11 = BoneWeight_get_weight3_m6_128((&___lhs), /*hidden argument*/NULL);
		Vector4_t6_54  L_12 = {0};
		Vector4__ctor_m6_399(&L_12, L_8, L_9, L_10, L_11, /*hidden argument*/NULL);
		float L_13 = BoneWeight_get_weight0_m6_122((&___rhs), /*hidden argument*/NULL);
		float L_14 = BoneWeight_get_weight1_m6_124((&___rhs), /*hidden argument*/NULL);
		float L_15 = BoneWeight_get_weight2_m6_126((&___rhs), /*hidden argument*/NULL);
		float L_16 = BoneWeight_get_weight3_m6_128((&___rhs), /*hidden argument*/NULL);
		Vector4_t6_54  L_17 = {0};
		Vector4__ctor_m6_399(&L_17, L_13, L_14, L_15, L_16, /*hidden argument*/NULL);
		bool L_18 = Vector4_op_Equality_m6_411(NULL /*static, unused*/, L_12, L_17, /*hidden argument*/NULL);
		G_B6_0 = ((int32_t)(L_18));
		goto IL_0096;
	}

IL_0095:
	{
		G_B6_0 = 0;
	}

IL_0096:
	{
		return G_B6_0;
	}
}
// System.Boolean UnityEngine.BoneWeight::op_Inequality(UnityEngine.BoneWeight,UnityEngine.BoneWeight)
extern "C" bool BoneWeight_op_Inequality_m6_141 (Object_t * __this /* static, unused */, BoneWeight_t6_24  ___lhs, BoneWeight_t6_24  ___rhs, const MethodInfo* method)
{
	{
		BoneWeight_t6_24  L_0 = ___lhs;
		BoneWeight_t6_24  L_1 = ___rhs;
		bool L_2 = BoneWeight_op_Equality_m6_140(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return ((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
// UnityEngine.Material UnityEngine.Renderer::get_sharedMaterial()
extern "C" Material_t6_72 * Renderer_get_sharedMaterial_m6_142 (Renderer_t6_25 * __this, const MethodInfo* method)
{
	typedef Material_t6_72 * (*Renderer_get_sharedMaterial_m6_142_ftn) (Renderer_t6_25 *);
	static Renderer_get_sharedMaterial_m6_142_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_sharedMaterial_m6_142_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_sharedMaterial()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Renderer::get_sortingLayerID()
extern "C" int32_t Renderer_get_sortingLayerID_m6_143 (Renderer_t6_25 * __this, const MethodInfo* method)
{
	typedef int32_t (*Renderer_get_sortingLayerID_m6_143_ftn) (Renderer_t6_25 *);
	static Renderer_get_sortingLayerID_m6_143_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_sortingLayerID_m6_143_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_sortingLayerID()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Renderer::get_sortingOrder()
extern "C" int32_t Renderer_get_sortingOrder_m6_144 (Renderer_t6_25 * __this, const MethodInfo* method)
{
	typedef int32_t (*Renderer_get_sortingOrder_m6_144_ftn) (Renderer_t6_25 *);
	static Renderer_get_sortingOrder_m6_144_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_sortingOrder_m6_144_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_sortingOrder()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Screen::get_width()
extern "C" int32_t Screen_get_width_m6_145 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Screen_get_width_m6_145_ftn) ();
	static Screen_get_width_m6_145_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_width_m6_145_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_width()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.Screen::get_height()
extern "C" int32_t Screen_get_height_m6_146 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Screen_get_height_m6_146_ftn) ();
	static Screen_get_height_m6_146_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_height_m6_146_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_height()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.Screen::get_dpi()
extern "C" float Screen_get_dpi_m6_147 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Screen_get_dpi_m6_147_ftn) ();
	static Screen_get_dpi_m6_147_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_dpi_m6_147_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_dpi()");
	return _il2cpp_icall_func();
}
// UnityEngine.GUIElement UnityEngine.GUILayer::HitTest(UnityEngine.Vector3)
extern "C" GUIElement_t6_29 * GUILayer_HitTest_m6_148 (GUILayer_t6_31 * __this, Vector3_t6_48  ___screenPosition, const MethodInfo* method)
{
	{
		GUIElement_t6_29 * L_0 = GUILayer_INTERNAL_CALL_HitTest_m6_149(NULL /*static, unused*/, __this, (&___screenPosition), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.GUIElement UnityEngine.GUILayer::INTERNAL_CALL_HitTest(UnityEngine.GUILayer,UnityEngine.Vector3&)
extern "C" GUIElement_t6_29 * GUILayer_INTERNAL_CALL_HitTest_m6_149 (Object_t * __this /* static, unused */, GUILayer_t6_31 * ___self, Vector3_t6_48 * ___screenPosition, const MethodInfo* method)
{
	typedef GUIElement_t6_29 * (*GUILayer_INTERNAL_CALL_HitTest_m6_149_ftn) (GUILayer_t6_31 *, Vector3_t6_48 *);
	static GUILayer_INTERNAL_CALL_HitTest_m6_149_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUILayer_INTERNAL_CALL_HitTest_m6_149_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUILayer::INTERNAL_CALL_HitTest(UnityEngine.GUILayer,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___self, ___screenPosition);
}
// System.Void UnityEngine.Texture::.ctor()
extern "C" void Texture__ctor_m6_150 (Texture_t6_32 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m6_701(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.Texture::Internal_GetWidth(UnityEngine.Texture)
extern "C" int32_t Texture_Internal_GetWidth_m6_151 (Object_t * __this /* static, unused */, Texture_t6_32 * ___mono, const MethodInfo* method)
{
	typedef int32_t (*Texture_Internal_GetWidth_m6_151_ftn) (Texture_t6_32 *);
	static Texture_Internal_GetWidth_m6_151_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture_Internal_GetWidth_m6_151_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture::Internal_GetWidth(UnityEngine.Texture)");
	return _il2cpp_icall_func(___mono);
}
// System.Int32 UnityEngine.Texture::Internal_GetHeight(UnityEngine.Texture)
extern "C" int32_t Texture_Internal_GetHeight_m6_152 (Object_t * __this /* static, unused */, Texture_t6_32 * ___mono, const MethodInfo* method)
{
	typedef int32_t (*Texture_Internal_GetHeight_m6_152_ftn) (Texture_t6_32 *);
	static Texture_Internal_GetHeight_m6_152_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture_Internal_GetHeight_m6_152_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture::Internal_GetHeight(UnityEngine.Texture)");
	return _il2cpp_icall_func(___mono);
}
// System.Int32 UnityEngine.Texture::get_width()
extern "C" int32_t Texture_get_width_m6_153 (Texture_t6_32 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Texture_Internal_GetWidth_m6_151(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Int32 UnityEngine.Texture::get_height()
extern "C" int32_t Texture_get_height_m6_154 (Texture_t6_32 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Texture_Internal_GetHeight_m6_152(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" void Texture2D__ctor_m6_155 (Texture2D_t6_33 * __this, int32_t ___width, int32_t ___height, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		s_Il2CppMethodIntialized = true;
	}
	{
		Texture__ctor_m6_150(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___width;
		int32_t L_1 = ___height;
		IntPtr_t L_2 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		Texture2D_Internal_Create_m6_157(NULL /*static, unused*/, __this, L_0, L_1, 5, 1, 0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" void Texture2D__ctor_m6_156 (Texture2D_t6_33 * __this, int32_t ___width, int32_t ___height, int32_t ___format, bool ___mipmap, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		s_Il2CppMethodIntialized = true;
	}
	{
		Texture__ctor_m6_150(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___width;
		int32_t L_1 = ___height;
		int32_t L_2 = ___format;
		bool L_3 = ___mipmap;
		IntPtr_t L_4 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		Texture2D_Internal_Create_m6_157(NULL /*static, unused*/, __this, L_0, L_1, L_2, L_3, 0, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::Internal_Create(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean,System.IntPtr)
extern "C" void Texture2D_Internal_Create_m6_157 (Object_t * __this /* static, unused */, Texture2D_t6_33 * ___mono, int32_t ___width, int32_t ___height, int32_t ___format, bool ___mipmap, bool ___linear, IntPtr_t ___nativeTex, const MethodInfo* method)
{
	typedef void (*Texture2D_Internal_Create_m6_157_ftn) (Texture2D_t6_33 *, int32_t, int32_t, int32_t, bool, bool, IntPtr_t);
	static Texture2D_Internal_Create_m6_157_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_Internal_Create_m6_157_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::Internal_Create(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean,System.IntPtr)");
	_il2cpp_icall_func(___mono, ___width, ___height, ___format, ___mipmap, ___linear, ___nativeTex);
}
// UnityEngine.TextureFormat UnityEngine.Texture2D::get_format()
extern "C" int32_t Texture2D_get_format_m6_158 (Texture2D_t6_33 * __this, const MethodInfo* method)
{
	typedef int32_t (*Texture2D_get_format_m6_158_ftn) (Texture2D_t6_33 *);
	static Texture2D_get_format_m6_158_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_get_format_m6_158_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::get_format()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Texture2D UnityEngine.Texture2D::get_whiteTexture()
extern "C" Texture2D_t6_33 * Texture2D_get_whiteTexture_m6_159 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef Texture2D_t6_33 * (*Texture2D_get_whiteTexture_m6_159_ftn) ();
	static Texture2D_get_whiteTexture_m6_159_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_get_whiteTexture_m6_159_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::get_whiteTexture()");
	return _il2cpp_icall_func();
}
// UnityEngine.Texture2D UnityEngine.Texture2D::get_blackTexture()
extern "C" Texture2D_t6_33 * Texture2D_get_blackTexture_m6_160 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef Texture2D_t6_33 * (*Texture2D_get_blackTexture_m6_160_ftn) ();
	static Texture2D_get_blackTexture_m6_160_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_get_blackTexture_m6_160_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::get_blackTexture()");
	return _il2cpp_icall_func();
}
// UnityEngine.Color UnityEngine.Texture2D::GetPixelBilinear(System.Single,System.Single)
extern "C" Color_t6_40  Texture2D_GetPixelBilinear_m6_161 (Texture2D_t6_33 * __this, float ___u, float ___v, const MethodInfo* method)
{
	typedef Color_t6_40  (*Texture2D_GetPixelBilinear_m6_161_ftn) (Texture2D_t6_33 *, float, float);
	static Texture2D_GetPixelBilinear_m6_161_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_GetPixelBilinear_m6_161_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::GetPixelBilinear(System.Single,System.Single)");
	return _il2cpp_icall_func(__this, ___u, ___v);
}
// System.Void UnityEngine.Texture2D::SetPixels(UnityEngine.Color[],System.Int32)
extern "C" void Texture2D_SetPixels_m6_162 (Texture2D_t6_33 * __this, ColorU5BU5D_t6_281* ___colors, int32_t ___miplevel, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, __this);
		int32_t L_1 = ___miplevel;
		V_0 = ((int32_t)((int32_t)L_0>>(int32_t)((int32_t)((int32_t)L_1&(int32_t)((int32_t)31)))));
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) >= ((int32_t)1)))
		{
			goto IL_0015;
		}
	}
	{
		V_0 = 1;
	}

IL_0015:
	{
		int32_t L_3 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_height() */, __this);
		int32_t L_4 = ___miplevel;
		V_1 = ((int32_t)((int32_t)L_3>>(int32_t)((int32_t)((int32_t)L_4&(int32_t)((int32_t)31)))));
		int32_t L_5 = V_1;
		if ((((int32_t)L_5) >= ((int32_t)1)))
		{
			goto IL_002a;
		}
	}
	{
		V_1 = 1;
	}

IL_002a:
	{
		int32_t L_6 = V_0;
		int32_t L_7 = V_1;
		ColorU5BU5D_t6_281* L_8 = ___colors;
		int32_t L_9 = ___miplevel;
		Texture2D_SetPixels_m6_163(__this, 0, 0, L_6, L_7, L_8, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::SetPixels(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color[],System.Int32)
extern "C" void Texture2D_SetPixels_m6_163 (Texture2D_t6_33 * __this, int32_t ___x, int32_t ___y, int32_t ___blockWidth, int32_t ___blockHeight, ColorU5BU5D_t6_281* ___colors, int32_t ___miplevel, const MethodInfo* method)
{
	typedef void (*Texture2D_SetPixels_m6_163_ftn) (Texture2D_t6_33 *, int32_t, int32_t, int32_t, int32_t, ColorU5BU5D_t6_281*, int32_t);
	static Texture2D_SetPixels_m6_163_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_SetPixels_m6_163_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::SetPixels(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color[],System.Int32)");
	_il2cpp_icall_func(__this, ___x, ___y, ___blockWidth, ___blockHeight, ___colors, ___miplevel);
}
// System.Void UnityEngine.Texture2D::SetAllPixels32(UnityEngine.Color32[],System.Int32)
extern "C" void Texture2D_SetAllPixels32_m6_164 (Texture2D_t6_33 * __this, Color32U5BU5D_t6_280* ___colors, int32_t ___miplevel, const MethodInfo* method)
{
	typedef void (*Texture2D_SetAllPixels32_m6_164_ftn) (Texture2D_t6_33 *, Color32U5BU5D_t6_280*, int32_t);
	static Texture2D_SetAllPixels32_m6_164_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_SetAllPixels32_m6_164_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::SetAllPixels32(UnityEngine.Color32[],System.Int32)");
	_il2cpp_icall_func(__this, ___colors, ___miplevel);
}
// System.Void UnityEngine.Texture2D::SetPixels32(UnityEngine.Color32[],System.Int32)
extern "C" void Texture2D_SetPixels32_m6_165 (Texture2D_t6_33 * __this, Color32U5BU5D_t6_280* ___colors, int32_t ___miplevel, const MethodInfo* method)
{
	{
		Color32U5BU5D_t6_280* L_0 = ___colors;
		int32_t L_1 = ___miplevel;
		Texture2D_SetAllPixels32_m6_164(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Texture2D::LoadImage(System.Byte[])
extern "C" bool Texture2D_LoadImage_m6_166 (Texture2D_t6_33 * __this, ByteU5BU5D_t1_109* ___data, const MethodInfo* method)
{
	typedef bool (*Texture2D_LoadImage_m6_166_ftn) (Texture2D_t6_33 *, ByteU5BU5D_t1_109*);
	static Texture2D_LoadImage_m6_166_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_LoadImage_m6_166_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::LoadImage(System.Byte[])");
	return _il2cpp_icall_func(__this, ___data);
}
// UnityEngine.Color32[] UnityEngine.Texture2D::GetPixels32(System.Int32)
extern "C" Color32U5BU5D_t6_280* Texture2D_GetPixels32_m6_167 (Texture2D_t6_33 * __this, int32_t ___miplevel, const MethodInfo* method)
{
	typedef Color32U5BU5D_t6_280* (*Texture2D_GetPixels32_m6_167_ftn) (Texture2D_t6_33 *, int32_t);
	static Texture2D_GetPixels32_m6_167_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_GetPixels32_m6_167_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::GetPixels32(System.Int32)");
	return _il2cpp_icall_func(__this, ___miplevel);
}
// System.Void UnityEngine.Texture2D::Apply(System.Boolean,System.Boolean)
extern "C" void Texture2D_Apply_m6_168 (Texture2D_t6_33 * __this, bool ___updateMipmaps, bool ___makeNoLongerReadable, const MethodInfo* method)
{
	typedef void (*Texture2D_Apply_m6_168_ftn) (Texture2D_t6_33 *, bool, bool);
	static Texture2D_Apply_m6_168_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_Apply_m6_168_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::Apply(System.Boolean,System.Boolean)");
	_il2cpp_icall_func(__this, ___updateMipmaps, ___makeNoLongerReadable);
}
// System.Void UnityEngine.Texture2D::Apply()
extern "C" void Texture2D_Apply_m6_169 (Texture2D_t6_33 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	bool V_1 = false;
	{
		V_0 = 0;
		V_1 = 1;
		bool L_0 = V_1;
		bool L_1 = V_0;
		Texture2D_Apply_m6_168(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::ReadPixels(UnityEngine.Rect,System.Int32,System.Int32)
extern "C" void Texture2D_ReadPixels_m6_170 (Texture2D_t6_33 * __this, Rect_t6_51  ___source, int32_t ___destX, int32_t ___destY, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = 1;
		int32_t L_0 = ___destX;
		int32_t L_1 = ___destY;
		bool L_2 = V_0;
		Texture2D_INTERNAL_CALL_ReadPixels_m6_171(NULL /*static, unused*/, __this, (&___source), L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::INTERNAL_CALL_ReadPixels(UnityEngine.Texture2D,UnityEngine.Rect&,System.Int32,System.Int32,System.Boolean)
extern "C" void Texture2D_INTERNAL_CALL_ReadPixels_m6_171 (Object_t * __this /* static, unused */, Texture2D_t6_33 * ___self, Rect_t6_51 * ___source, int32_t ___destX, int32_t ___destY, bool ___recalculateMipMaps, const MethodInfo* method)
{
	typedef void (*Texture2D_INTERNAL_CALL_ReadPixels_m6_171_ftn) (Texture2D_t6_33 *, Rect_t6_51 *, int32_t, int32_t, bool);
	static Texture2D_INTERNAL_CALL_ReadPixels_m6_171_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_INTERNAL_CALL_ReadPixels_m6_171_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::INTERNAL_CALL_ReadPixels(UnityEngine.Texture2D,UnityEngine.Rect&,System.Int32,System.Int32,System.Boolean)");
	_il2cpp_icall_func(___self, ___source, ___destX, ___destY, ___recalculateMipMaps);
}
// System.Byte[] UnityEngine.Texture2D::EncodeToPNG()
extern "C" ByteU5BU5D_t1_109* Texture2D_EncodeToPNG_m6_172 (Texture2D_t6_33 * __this, const MethodInfo* method)
{
	typedef ByteU5BU5D_t1_109* (*Texture2D_EncodeToPNG_m6_172_ftn) (Texture2D_t6_33 *);
	static Texture2D_EncodeToPNG_m6_172_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_EncodeToPNG_m6_172_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::EncodeToPNG()");
	return _il2cpp_icall_func(__this);
}
// System.Byte[] UnityEngine.Texture2D::EncodeToJPG(System.Int32)
extern "C" ByteU5BU5D_t1_109* Texture2D_EncodeToJPG_m6_173 (Texture2D_t6_33 * __this, int32_t ___quality, const MethodInfo* method)
{
	typedef ByteU5BU5D_t1_109* (*Texture2D_EncodeToJPG_m6_173_ftn) (Texture2D_t6_33 *, int32_t);
	static Texture2D_EncodeToJPG_m6_173_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_EncodeToJPG_m6_173_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::EncodeToJPG(System.Int32)");
	return _il2cpp_icall_func(__this, ___quality);
}
// System.Byte[] UnityEngine.Texture2D::EncodeToJPG()
extern "C" ByteU5BU5D_t1_109* Texture2D_EncodeToJPG_m6_174 (Texture2D_t6_33 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t1_109* L_0 = Texture2D_EncodeToJPG_m6_173(__this, ((int32_t)75), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Int32 UnityEngine.RenderTexture::Internal_GetWidth(UnityEngine.RenderTexture)
extern "C" int32_t RenderTexture_Internal_GetWidth_m6_175 (Object_t * __this /* static, unused */, RenderTexture_t6_34 * ___mono, const MethodInfo* method)
{
	typedef int32_t (*RenderTexture_Internal_GetWidth_m6_175_ftn) (RenderTexture_t6_34 *);
	static RenderTexture_Internal_GetWidth_m6_175_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_GetWidth_m6_175_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_GetWidth(UnityEngine.RenderTexture)");
	return _il2cpp_icall_func(___mono);
}
// System.Int32 UnityEngine.RenderTexture::Internal_GetHeight(UnityEngine.RenderTexture)
extern "C" int32_t RenderTexture_Internal_GetHeight_m6_176 (Object_t * __this /* static, unused */, RenderTexture_t6_34 * ___mono, const MethodInfo* method)
{
	typedef int32_t (*RenderTexture_Internal_GetHeight_m6_176_ftn) (RenderTexture_t6_34 *);
	static RenderTexture_Internal_GetHeight_m6_176_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_GetHeight_m6_176_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_GetHeight(UnityEngine.RenderTexture)");
	return _il2cpp_icall_func(___mono);
}
// System.Int32 UnityEngine.RenderTexture::get_width()
extern "C" int32_t RenderTexture_get_width_m6_177 (RenderTexture_t6_34 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = RenderTexture_Internal_GetWidth_m6_175(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Int32 UnityEngine.RenderTexture::get_height()
extern "C" int32_t RenderTexture_get_height_m6_178 (RenderTexture_t6_34 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = RenderTexture_Internal_GetHeight_m6_176(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.CullingGroup/StateChanged::.ctor(System.Object,System.IntPtr)
extern "C" void StateChanged__ctor_m6_179 (StateChanged_t6_37 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.CullingGroup/StateChanged::Invoke(UnityEngine.CullingGroupEvent)
extern "C" void StateChanged_Invoke_m6_180 (StateChanged_t6_37 * __this, CullingGroupEvent_t6_36  ___sphere, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		StateChanged_Invoke_m6_180((StateChanged_t6_37 *)__this->___prev_9,___sphere, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, CullingGroupEvent_t6_36  ___sphere, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___sphere,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, CullingGroupEvent_t6_36  ___sphere, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___sphere,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_StateChanged_t6_37(Il2CppObject* delegate, CullingGroupEvent_t6_36  ___sphere)
{
	typedef void (STDCALL *native_function_ptr_type)(CullingGroupEvent_t6_36 );
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___sphere' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___sphere);

	// Marshaling cleanup of parameter '___sphere' native representation

}
// System.IAsyncResult UnityEngine.CullingGroup/StateChanged::BeginInvoke(UnityEngine.CullingGroupEvent,System.AsyncCallback,System.Object)
extern TypeInfo* CullingGroupEvent_t6_36_il2cpp_TypeInfo_var;
extern "C" Object_t * StateChanged_BeginInvoke_m6_181 (StateChanged_t6_37 * __this, CullingGroupEvent_t6_36  ___sphere, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CullingGroupEvent_t6_36_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1584);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(CullingGroupEvent_t6_36_il2cpp_TypeInfo_var, &___sphere);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.CullingGroup/StateChanged::EndInvoke(System.IAsyncResult)
extern "C" void StateChanged_EndInvoke_m6_182 (StateChanged_t6_37 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void UnityEngine.CullingGroup::Finalize()
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" void CullingGroup_Finalize_m6_183 (CullingGroup_t6_38 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = (__this->___m_Ptr_0);
			IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
			bool L_2 = IntPtr_op_Inequality_m1_841(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
			if (!L_2)
			{
				goto IL_001b;
			}
		}

IL_0015:
		{
			CullingGroup_FinalizerFailure_m6_186(__this, /*hidden argument*/NULL);
		}

IL_001b:
		{
			IL2CPP_LEAVE(0x27, FINALLY_0020);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0020;
	}

FINALLY_0020:
	{ // begin finally (depth: 1)
		Object_Finalize_m1_3(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(32)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(32)
	{
		IL2CPP_JUMP_TBL(0x27, IL_0027)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0027:
	{
		return;
	}
}
// System.Void UnityEngine.CullingGroup::Dispose()
extern "C" void CullingGroup_Dispose_m6_184 (CullingGroup_t6_38 * __this, const MethodInfo* method)
{
	typedef void (*CullingGroup_Dispose_m6_184_ftn) (CullingGroup_t6_38 *);
	static CullingGroup_Dispose_m6_184_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CullingGroup_Dispose_m6_184_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CullingGroup::Dispose()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.CullingGroup::SendEvents(UnityEngine.CullingGroup,System.IntPtr,System.Int32)
extern "C" void CullingGroup_SendEvents_m6_185 (Object_t * __this /* static, unused */, CullingGroup_t6_38 * ___cullingGroup, IntPtr_t ___eventsPtr, int32_t ___count, const MethodInfo* method)
{
	CullingGroupEvent_t6_36 * V_0 = {0};
	int32_t V_1 = 0;
	{
		void* L_0 = IntPtr_ToPointer_m1_837((&___eventsPtr), /*hidden argument*/NULL);
		V_0 = (CullingGroupEvent_t6_36 *)L_0;
		CullingGroup_t6_38 * L_1 = ___cullingGroup;
		NullCheck(L_1);
		StateChanged_t6_37 * L_2 = (L_1->___m_OnStateChanged_1);
		if (L_2)
		{
			goto IL_0014;
		}
	}
	{
		return;
	}

IL_0014:
	{
		V_1 = 0;
		goto IL_0039;
	}

IL_001b:
	{
		CullingGroup_t6_38 * L_3 = ___cullingGroup;
		NullCheck(L_3);
		StateChanged_t6_37 * L_4 = (L_3->___m_OnStateChanged_1);
		CullingGroupEvent_t6_36 * L_5 = V_0;
		int32_t L_6 = V_1;
		NullCheck(L_4);
		StateChanged_Invoke_m6_180(L_4, (*(CullingGroupEvent_t6_36 *)((CullingGroupEvent_t6_36 *)((intptr_t)L_5+(int32_t)((int32_t)((int32_t)L_6*(int32_t)sizeof(CullingGroupEvent_t6_36 )))))), /*hidden argument*/NULL);
		int32_t L_7 = V_1;
		V_1 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0039:
	{
		int32_t L_8 = V_1;
		int32_t L_9 = ___count;
		if ((((int32_t)L_8) < ((int32_t)L_9)))
		{
			goto IL_001b;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.CullingGroup::FinalizerFailure()
extern "C" void CullingGroup_FinalizerFailure_m6_186 (CullingGroup_t6_38 * __this, const MethodInfo* method)
{
	typedef void (*CullingGroup_FinalizerFailure_m6_186_ftn) (CullingGroup_t6_38 *);
	static CullingGroup_FinalizerFailure_m6_186_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CullingGroup_FinalizerFailure_m6_186_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CullingGroup::FinalizerFailure()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.GradientColorKey::.ctor(UnityEngine.Color,System.Single)
extern "C" void GradientColorKey__ctor_m6_187 (GradientColorKey_t6_39 * __this, Color_t6_40  ___col, float ___time, const MethodInfo* method)
{
	{
		Color_t6_40  L_0 = ___col;
		__this->___color_0 = L_0;
		float L_1 = ___time;
		__this->___time_1 = L_1;
		return;
	}
}
// System.Void UnityEngine.GradientAlphaKey::.ctor(System.Single,System.Single)
extern "C" void GradientAlphaKey__ctor_m6_188 (GradientAlphaKey_t6_41 * __this, float ___alpha, float ___time, const MethodInfo* method)
{
	{
		float L_0 = ___alpha;
		__this->___alpha_0 = L_0;
		float L_1 = ___time;
		__this->___time_1 = L_1;
		return;
	}
}
// System.Void UnityEngine.Gradient::.ctor()
extern "C" void Gradient__ctor_m6_189 (Gradient_t6_42 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		Gradient_Init_m6_190(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Gradient::Init()
extern "C" void Gradient_Init_m6_190 (Gradient_t6_42 * __this, const MethodInfo* method)
{
	typedef void (*Gradient_Init_m6_190_ftn) (Gradient_t6_42 *);
	static Gradient_Init_m6_190_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Gradient_Init_m6_190_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Gradient::Init()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Gradient::Cleanup()
extern "C" void Gradient_Cleanup_m6_191 (Gradient_t6_42 * __this, const MethodInfo* method)
{
	typedef void (*Gradient_Cleanup_m6_191_ftn) (Gradient_t6_42 *);
	static Gradient_Cleanup_m6_191_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Gradient_Cleanup_m6_191_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Gradient::Cleanup()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Gradient::Finalize()
extern "C" void Gradient_Finalize_m6_192 (Gradient_t6_42 * __this, const MethodInfo* method)
{
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Gradient_Cleanup_m6_191(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m1_3(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0012:
	{
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.Gradient
extern "C" void Gradient_t6_42_marshal(const Gradient_t6_42& unmarshaled, Gradient_t6_42_marshaled& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.___m_Ptr_0).___m_value_0);
}
extern "C" void Gradient_t6_42_marshal_back(const Gradient_t6_42_marshaled& marshaled, Gradient_t6_42& unmarshaled)
{
	(unmarshaled.___m_Ptr_0).___m_value_0 = reinterpret_cast<void*>(marshaled.___m_Ptr_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Gradient
extern "C" void Gradient_t6_42_marshal_cleanup(Gradient_t6_42_marshaled& marshaled)
{
}
// System.Void UnityEngine.TouchScreenKeyboard::.ctor(System.String,UnityEngine.TouchScreenKeyboardType,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.String)
extern TypeInfo* TouchScreenKeyboard_InternalConstructorHelperArguments_t6_43_il2cpp_TypeInfo_var;
extern TypeInfo* TouchScreenKeyboardType_t6_44_il2cpp_TypeInfo_var;
extern TypeInfo* Convert_t1_1522_il2cpp_TypeInfo_var;
extern "C" void TouchScreenKeyboard__ctor_m6_193 (TouchScreenKeyboard_t6_45 * __this, String_t* ___text, int32_t ___keyboardType, bool ___autocorrection, bool ___multiline, bool ___secure, bool ___alert, String_t* ___textPlaceholder, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TouchScreenKeyboard_InternalConstructorHelperArguments_t6_43_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1585);
		TouchScreenKeyboardType_t6_44_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1586);
		Convert_t1_1522_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		s_Il2CppMethodIntialized = true;
	}
	TouchScreenKeyboard_InternalConstructorHelperArguments_t6_43  V_0 = {0};
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		Initobj (TouchScreenKeyboard_InternalConstructorHelperArguments_t6_43_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_0 = ___keyboardType;
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(TouchScreenKeyboardType_t6_44_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1_1522_il2cpp_TypeInfo_var);
		uint32_t L_3 = Convert_ToUInt32_m1_13726(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		(&V_0)->___keyboardType_0 = L_3;
		bool L_4 = ___autocorrection;
		uint32_t L_5 = Convert_ToUInt32_m1_13709(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		(&V_0)->___autocorrection_1 = L_5;
		bool L_6 = ___multiline;
		uint32_t L_7 = Convert_ToUInt32_m1_13709(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		(&V_0)->___multiline_2 = L_7;
		bool L_8 = ___secure;
		uint32_t L_9 = Convert_ToUInt32_m1_13709(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		(&V_0)->___secure_3 = L_9;
		bool L_10 = ___alert;
		uint32_t L_11 = Convert_ToUInt32_m1_13709(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		(&V_0)->___alert_4 = L_11;
		String_t* L_12 = ___text;
		String_t* L_13 = ___textPlaceholder;
		TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m6_196(__this, (&V_0), L_12, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TouchScreenKeyboard::Destroy()
extern "C" void TouchScreenKeyboard_Destroy_m6_194 (TouchScreenKeyboard_t6_45 * __this, const MethodInfo* method)
{
	typedef void (*TouchScreenKeyboard_Destroy_m6_194_ftn) (TouchScreenKeyboard_t6_45 *);
	static TouchScreenKeyboard_Destroy_m6_194_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_Destroy_m6_194_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::Destroy()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TouchScreenKeyboard::Finalize()
extern "C" void TouchScreenKeyboard_Finalize_m6_195 (TouchScreenKeyboard_t6_45 * __this, const MethodInfo* method)
{
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		TouchScreenKeyboard_Destroy_m6_194(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m1_3(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0012:
	{
		return;
	}
}
// System.Void UnityEngine.TouchScreenKeyboard::TouchScreenKeyboard_InternalConstructorHelper(UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments&,System.String,System.String)
extern "C" void TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m6_196 (TouchScreenKeyboard_t6_45 * __this, TouchScreenKeyboard_InternalConstructorHelperArguments_t6_43 * ___arguments, String_t* ___text, String_t* ___textPlaceholder, const MethodInfo* method)
{
	typedef void (*TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m6_196_ftn) (TouchScreenKeyboard_t6_45 *, TouchScreenKeyboard_InternalConstructorHelperArguments_t6_43 *, String_t*, String_t*);
	static TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m6_196_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m6_196_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::TouchScreenKeyboard_InternalConstructorHelper(UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments&,System.String,System.String)");
	_il2cpp_icall_func(__this, ___arguments, ___text, ___textPlaceholder);
}
// System.Boolean UnityEngine.TouchScreenKeyboard::get_isSupported()
extern "C" bool TouchScreenKeyboard_get_isSupported_m6_197 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	int32_t V_0 = {0};
	int32_t V_1 = {0};
	{
		int32_t L_0 = Application_get_platform_m6_587(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		V_1 = L_1;
		int32_t L_2 = V_1;
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 0)
		{
			goto IL_0064;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 1)
		{
			goto IL_0064;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 2)
		{
			goto IL_0064;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 3)
		{
			goto IL_0062;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 4)
		{
			goto IL_0062;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 5)
		{
			goto IL_0062;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 6)
		{
			goto IL_0045;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 7)
		{
			goto IL_0045;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 8)
		{
			goto IL_0062;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 9)
		{
			goto IL_0045;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 10)
		{
			goto IL_0045;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 11)
		{
			goto IL_0045;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 12)
		{
			goto IL_0062;
		}
	}

IL_0045:
	{
		int32_t L_3 = V_1;
		if (((int32_t)((int32_t)L_3-(int32_t)8)) == 0)
		{
			goto IL_0062;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)8)) == 1)
		{
			goto IL_0066;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)8)) == 2)
		{
			goto IL_0066;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)8)) == 3)
		{
			goto IL_0062;
		}
	}
	{
		goto IL_0066;
	}

IL_0062:
	{
		return 1;
	}

IL_0064:
	{
		return 0;
	}

IL_0066:
	{
		return 0;
	}
}
// UnityEngine.TouchScreenKeyboard UnityEngine.TouchScreenKeyboard::Open(System.String,UnityEngine.TouchScreenKeyboardType,System.Boolean,System.Boolean,System.Boolean)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" TouchScreenKeyboard_t6_45 * TouchScreenKeyboard_Open_m6_198 (Object_t * __this /* static, unused */, String_t* ___text, int32_t ___keyboardType, bool ___autocorrection, bool ___multiline, bool ___secure, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	bool V_1 = false;
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_0 = L_0;
		V_1 = 0;
		String_t* L_1 = ___text;
		int32_t L_2 = ___keyboardType;
		bool L_3 = ___autocorrection;
		bool L_4 = ___multiline;
		bool L_5 = ___secure;
		bool L_6 = V_1;
		String_t* L_7 = V_0;
		TouchScreenKeyboard_t6_45 * L_8 = TouchScreenKeyboard_Open_m6_200(NULL /*static, unused*/, L_1, L_2, L_3, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// UnityEngine.TouchScreenKeyboard UnityEngine.TouchScreenKeyboard::Open(System.String,UnityEngine.TouchScreenKeyboardType,System.Boolean,System.Boolean)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" TouchScreenKeyboard_t6_45 * TouchScreenKeyboard_Open_m6_199 (Object_t * __this /* static, unused */, String_t* ___text, int32_t ___keyboardType, bool ___autocorrection, bool ___multiline, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	bool V_1 = false;
	bool V_2 = false;
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_0 = L_0;
		V_1 = 0;
		V_2 = 0;
		String_t* L_1 = ___text;
		int32_t L_2 = ___keyboardType;
		bool L_3 = ___autocorrection;
		bool L_4 = ___multiline;
		bool L_5 = V_2;
		bool L_6 = V_1;
		String_t* L_7 = V_0;
		TouchScreenKeyboard_t6_45 * L_8 = TouchScreenKeyboard_Open_m6_200(NULL /*static, unused*/, L_1, L_2, L_3, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// UnityEngine.TouchScreenKeyboard UnityEngine.TouchScreenKeyboard::Open(System.String,UnityEngine.TouchScreenKeyboardType,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.String)
extern TypeInfo* TouchScreenKeyboard_t6_45_il2cpp_TypeInfo_var;
extern "C" TouchScreenKeyboard_t6_45 * TouchScreenKeyboard_Open_m6_200 (Object_t * __this /* static, unused */, String_t* ___text, int32_t ___keyboardType, bool ___autocorrection, bool ___multiline, bool ___secure, bool ___alert, String_t* ___textPlaceholder, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TouchScreenKeyboard_t6_45_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1587);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___text;
		int32_t L_1 = ___keyboardType;
		bool L_2 = ___autocorrection;
		bool L_3 = ___multiline;
		bool L_4 = ___secure;
		bool L_5 = ___alert;
		String_t* L_6 = ___textPlaceholder;
		TouchScreenKeyboard_t6_45 * L_7 = (TouchScreenKeyboard_t6_45 *)il2cpp_codegen_object_new (TouchScreenKeyboard_t6_45_il2cpp_TypeInfo_var);
		TouchScreenKeyboard__ctor_m6_193(L_7, L_0, L_1, L_2, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.String UnityEngine.TouchScreenKeyboard::get_text()
extern "C" String_t* TouchScreenKeyboard_get_text_m6_201 (TouchScreenKeyboard_t6_45 * __this, const MethodInfo* method)
{
	typedef String_t* (*TouchScreenKeyboard_get_text_m6_201_ftn) (TouchScreenKeyboard_t6_45 *);
	static TouchScreenKeyboard_get_text_m6_201_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_get_text_m6_201_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::get_text()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TouchScreenKeyboard::set_text(System.String)
extern "C" void TouchScreenKeyboard_set_text_m6_202 (TouchScreenKeyboard_t6_45 * __this, String_t* ___value, const MethodInfo* method)
{
	typedef void (*TouchScreenKeyboard_set_text_m6_202_ftn) (TouchScreenKeyboard_t6_45 *, String_t*);
	static TouchScreenKeyboard_set_text_m6_202_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_set_text_m6_202_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::set_text(System.String)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.TouchScreenKeyboard::set_hideInput(System.Boolean)
extern "C" void TouchScreenKeyboard_set_hideInput_m6_203 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method)
{
	typedef void (*TouchScreenKeyboard_set_hideInput_m6_203_ftn) (bool);
	static TouchScreenKeyboard_set_hideInput_m6_203_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_set_hideInput_m6_203_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::set_hideInput(System.Boolean)");
	_il2cpp_icall_func(___value);
}
// System.Boolean UnityEngine.TouchScreenKeyboard::get_active()
extern "C" bool TouchScreenKeyboard_get_active_m6_204 (TouchScreenKeyboard_t6_45 * __this, const MethodInfo* method)
{
	typedef bool (*TouchScreenKeyboard_get_active_m6_204_ftn) (TouchScreenKeyboard_t6_45 *);
	static TouchScreenKeyboard_get_active_m6_204_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_get_active_m6_204_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::get_active()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TouchScreenKeyboard::set_active(System.Boolean)
extern "C" void TouchScreenKeyboard_set_active_m6_205 (TouchScreenKeyboard_t6_45 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*TouchScreenKeyboard_set_active_m6_205_ftn) (TouchScreenKeyboard_t6_45 *, bool);
	static TouchScreenKeyboard_set_active_m6_205_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_set_active_m6_205_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::set_active(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Boolean UnityEngine.TouchScreenKeyboard::get_done()
extern "C" bool TouchScreenKeyboard_get_done_m6_206 (TouchScreenKeyboard_t6_45 * __this, const MethodInfo* method)
{
	typedef bool (*TouchScreenKeyboard_get_done_m6_206_ftn) (TouchScreenKeyboard_t6_45 *);
	static TouchScreenKeyboard_get_done_m6_206_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_get_done_m6_206_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::get_done()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.TouchScreenKeyboard::get_wasCanceled()
extern "C" bool TouchScreenKeyboard_get_wasCanceled_m6_207 (TouchScreenKeyboard_t6_45 * __this, const MethodInfo* method)
{
	typedef bool (*TouchScreenKeyboard_get_wasCanceled_m6_207_ftn) (TouchScreenKeyboard_t6_45 *);
	static TouchScreenKeyboard_get_wasCanceled_m6_207_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_get_wasCanceled_m6_207_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::get_wasCanceled()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.LayerMask::get_value()
extern "C" int32_t LayerMask_get_value_m6_208 (LayerMask_t6_46 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Mask_0);
		return L_0;
	}
}
// System.Void UnityEngine.LayerMask::set_value(System.Int32)
extern "C" void LayerMask_set_value_m6_209 (LayerMask_t6_46 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_Mask_0 = L_0;
		return;
	}
}
// System.String UnityEngine.LayerMask::LayerToName(System.Int32)
extern "C" String_t* LayerMask_LayerToName_m6_210 (Object_t * __this /* static, unused */, int32_t ___layer, const MethodInfo* method)
{
	typedef String_t* (*LayerMask_LayerToName_m6_210_ftn) (int32_t);
	static LayerMask_LayerToName_m6_210_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LayerMask_LayerToName_m6_210_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.LayerMask::LayerToName(System.Int32)");
	return _il2cpp_icall_func(___layer);
}
// System.Int32 UnityEngine.LayerMask::NameToLayer(System.String)
extern "C" int32_t LayerMask_NameToLayer_m6_211 (Object_t * __this /* static, unused */, String_t* ___layerName, const MethodInfo* method)
{
	typedef int32_t (*LayerMask_NameToLayer_m6_211_ftn) (String_t*);
	static LayerMask_NameToLayer_m6_211_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LayerMask_NameToLayer_m6_211_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.LayerMask::NameToLayer(System.String)");
	return _il2cpp_icall_func(___layerName);
}
// System.Int32 UnityEngine.LayerMask::GetMask(System.String[])
extern "C" int32_t LayerMask_GetMask_m6_212 (Object_t * __this /* static, unused */, StringU5BU5D_t1_238* ___layerNames, const MethodInfo* method)
{
	int32_t V_0 = 0;
	String_t* V_1 = {0};
	StringU5BU5D_t1_238* V_2 = {0};
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		V_0 = 0;
		StringU5BU5D_t1_238* L_0 = ___layerNames;
		V_2 = L_0;
		V_3 = 0;
		goto IL_002f;
	}

IL_000b:
	{
		StringU5BU5D_t1_238* L_1 = V_2;
		int32_t L_2 = V_3;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		V_1 = (*(String_t**)(String_t**)SZArrayLdElema(L_1, L_3, sizeof(String_t*)));
		String_t* L_4 = V_1;
		int32_t L_5 = LayerMask_NameToLayer_m6_211(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_4 = L_5;
		int32_t L_6 = V_4;
		if (!L_6)
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_7 = V_0;
		int32_t L_8 = V_4;
		V_0 = ((int32_t)((int32_t)L_7|(int32_t)((int32_t)((int32_t)1<<(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_8&(int32_t)((int32_t)31)))&(int32_t)((int32_t)31)))))));
	}

IL_002b:
	{
		int32_t L_9 = V_3;
		V_3 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_002f:
	{
		int32_t L_10 = V_3;
		StringU5BU5D_t1_238* L_11 = V_2;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_11)->max_length)))))))
		{
			goto IL_000b;
		}
	}
	{
		int32_t L_12 = V_0;
		return L_12;
	}
}
// System.Int32 UnityEngine.LayerMask::op_Implicit(UnityEngine.LayerMask)
extern "C" int32_t LayerMask_op_Implicit_m6_213 (Object_t * __this /* static, unused */, LayerMask_t6_46  ___mask, const MethodInfo* method)
{
	{
		int32_t L_0 = ((&___mask)->___m_Mask_0);
		return L_0;
	}
}
// UnityEngine.LayerMask UnityEngine.LayerMask::op_Implicit(System.Int32)
extern "C" LayerMask_t6_46  LayerMask_op_Implicit_m6_214 (Object_t * __this /* static, unused */, int32_t ___intVal, const MethodInfo* method)
{
	LayerMask_t6_46  V_0 = {0};
	{
		int32_t L_0 = ___intVal;
		(&V_0)->___m_Mask_0 = L_0;
		LayerMask_t6_46  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C" void Vector2__ctor_m6_215 (Vector2_t6_47 * __this, float ___x, float ___y, const MethodInfo* method)
{
	{
		float L_0 = ___x;
		__this->___x_1 = L_0;
		float L_1 = ___y;
		__this->___y_2 = L_1;
		return;
	}
}
// System.Single UnityEngine.Vector2::get_Item(System.Int32)
extern TypeInfo* IndexOutOfRangeException_t1_1555_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4694;
extern "C" float Vector2_get_Item_m6_216 (Vector2_t6_47 * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IndexOutOfRangeException_t1_1555_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		_stringLiteral4694 = il2cpp_codegen_string_literal_from_index(4694);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_001b;
		}
	}
	{
		goto IL_0022;
	}

IL_0014:
	{
		float L_3 = (__this->___x_1);
		return L_3;
	}

IL_001b:
	{
		float L_4 = (__this->___y_2);
		return L_4;
	}

IL_0022:
	{
		IndexOutOfRangeException_t1_1555 * L_5 = (IndexOutOfRangeException_t1_1555 *)il2cpp_codegen_object_new (IndexOutOfRangeException_t1_1555_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m1_14159(L_5, _stringLiteral4694, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_5);
	}
}
// System.Void UnityEngine.Vector2::set_Item(System.Int32,System.Single)
extern TypeInfo* IndexOutOfRangeException_t1_1555_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4694;
extern "C" void Vector2_set_Item_m6_217 (Vector2_t6_47 * __this, int32_t ___index, float ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IndexOutOfRangeException_t1_1555_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		_stringLiteral4694 = il2cpp_codegen_string_literal_from_index(4694);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0020;
		}
	}
	{
		goto IL_002c;
	}

IL_0014:
	{
		float L_3 = ___value;
		__this->___x_1 = L_3;
		goto IL_0037;
	}

IL_0020:
	{
		float L_4 = ___value;
		__this->___y_2 = L_4;
		goto IL_0037;
	}

IL_002c:
	{
		IndexOutOfRangeException_t1_1555 * L_5 = (IndexOutOfRangeException_t1_1555 *)il2cpp_codegen_object_new (IndexOutOfRangeException_t1_1555_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m1_14159(L_5, _stringLiteral4694, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_5);
	}

IL_0037:
	{
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::Scale(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C" Vector2_t6_47  Vector2_Scale_m6_218 (Object_t * __this /* static, unused */, Vector2_t6_47  ___a, Vector2_t6_47  ___b, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ((&___b)->___x_1);
		float L_2 = ((&___a)->___y_2);
		float L_3 = ((&___b)->___y_2);
		Vector2_t6_47  L_4 = {0};
		Vector2__ctor_m6_215(&L_4, ((float)((float)L_0*(float)L_1)), ((float)((float)L_2*(float)L_3)), /*hidden argument*/NULL);
		return L_4;
	}
}
// System.String UnityEngine.Vector2::ToString()
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t1_17_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4695;
extern "C" String_t* Vector2_ToString_m6_219 (Vector2_t6_47 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Single_t1_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		_stringLiteral4695 = il2cpp_codegen_string_literal_from_index(4695);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_272* L_0 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 2));
		float L_1 = (__this->___x_1);
		float L_2 = L_1;
		Object_t * L_3 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_3;
		ObjectU5BU5D_t1_272* L_4 = L_0;
		float L_5 = (__this->___y_2);
		float L_6 = L_5;
		Object_t * L_7 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1, sizeof(Object_t *))) = (Object_t *)L_7;
		String_t* L_8 = UnityString_Format_m6_573(NULL /*static, unused*/, _stringLiteral4695, L_4, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Int32 UnityEngine.Vector2::GetHashCode()
extern "C" int32_t Vector2_GetHashCode_m6_220 (Vector2_t6_47 * __this, const MethodInfo* method)
{
	{
		float* L_0 = &(__this->___x_1);
		int32_t L_1 = Single_GetHashCode_m1_622(L_0, /*hidden argument*/NULL);
		float* L_2 = &(__this->___y_2);
		int32_t L_3 = Single_GetHashCode_m1_622(L_2, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))));
	}
}
// System.Boolean UnityEngine.Vector2::Equals(System.Object)
extern TypeInfo* Vector2_t6_47_il2cpp_TypeInfo_var;
extern "C" bool Vector2_Equals_m6_221 (Vector2_t6_47 * __this, Object_t * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Vector2_t6_47_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1588);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t6_47  V_0 = {0};
	int32_t G_B5_0 = 0;
	{
		Object_t * L_0 = ___other;
		if (((Object_t *)IsInstSealed(L_0, Vector2_t6_47_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		Object_t * L_1 = ___other;
		V_0 = ((*(Vector2_t6_47 *)((Vector2_t6_47 *)UnBox (L_1, Vector2_t6_47_il2cpp_TypeInfo_var))));
		float* L_2 = &(__this->___x_1);
		float L_3 = ((&V_0)->___x_1);
		bool L_4 = Single_Equals_m1_621(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_003f;
		}
	}
	{
		float* L_5 = &(__this->___y_2);
		float L_6 = ((&V_0)->___y_2);
		bool L_7 = Single_Equals_m1_621(L_5, L_6, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_7));
		goto IL_0040;
	}

IL_003f:
	{
		G_B5_0 = 0;
	}

IL_0040:
	{
		return G_B5_0;
	}
}
// System.Single UnityEngine.Vector2::Dot(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C" float Vector2_Dot_m6_222 (Object_t * __this /* static, unused */, Vector2_t6_47  ___lhs, Vector2_t6_47  ___rhs, const MethodInfo* method)
{
	{
		float L_0 = ((&___lhs)->___x_1);
		float L_1 = ((&___rhs)->___x_1);
		float L_2 = ((&___lhs)->___y_2);
		float L_3 = ((&___rhs)->___y_2);
		return ((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))));
	}
}
// System.Single UnityEngine.Vector2::get_sqrMagnitude()
extern "C" float Vector2_get_sqrMagnitude_m6_223 (Vector2_t6_47 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___x_1);
		float L_1 = (__this->___x_1);
		float L_2 = (__this->___y_2);
		float L_3 = (__this->___y_2);
		return ((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))));
	}
}
// System.Single UnityEngine.Vector2::SqrMagnitude(UnityEngine.Vector2)
extern "C" float Vector2_SqrMagnitude_m6_224 (Object_t * __this /* static, unused */, Vector2_t6_47  ___a, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ((&___a)->___x_1);
		float L_2 = ((&___a)->___y_2);
		float L_3 = ((&___a)->___y_2);
		return ((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))));
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::get_zero()
extern "C" Vector2_t6_47  Vector2_get_zero_m6_225 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector2_t6_47  L_0 = {0};
		Vector2__ctor_m6_215(&L_0, (0.0f), (0.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::get_one()
extern "C" Vector2_t6_47  Vector2_get_one_m6_226 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector2_t6_47  L_0 = {0};
		Vector2__ctor_m6_215(&L_0, (1.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::get_up()
extern "C" Vector2_t6_47  Vector2_get_up_m6_227 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector2_t6_47  L_0 = {0};
		Vector2__ctor_m6_215(&L_0, (0.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::get_right()
extern "C" Vector2_t6_47  Vector2_get_right_m6_228 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector2_t6_47  L_0 = {0};
		Vector2__ctor_m6_215(&L_0, (1.0f), (0.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_Addition(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C" Vector2_t6_47  Vector2_op_Addition_m6_229 (Object_t * __this /* static, unused */, Vector2_t6_47  ___a, Vector2_t6_47  ___b, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ((&___b)->___x_1);
		float L_2 = ((&___a)->___y_2);
		float L_3 = ((&___b)->___y_2);
		Vector2_t6_47  L_4 = {0};
		Vector2__ctor_m6_215(&L_4, ((float)((float)L_0+(float)L_1)), ((float)((float)L_2+(float)L_3)), /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_Subtraction(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C" Vector2_t6_47  Vector2_op_Subtraction_m6_230 (Object_t * __this /* static, unused */, Vector2_t6_47  ___a, Vector2_t6_47  ___b, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ((&___b)->___x_1);
		float L_2 = ((&___a)->___y_2);
		float L_3 = ((&___b)->___y_2);
		Vector2_t6_47  L_4 = {0};
		Vector2__ctor_m6_215(&L_4, ((float)((float)L_0-(float)L_1)), ((float)((float)L_2-(float)L_3)), /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_Multiply(UnityEngine.Vector2,System.Single)
extern "C" Vector2_t6_47  Vector2_op_Multiply_m6_231 (Object_t * __this /* static, unused */, Vector2_t6_47  ___a, float ___d, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ___d;
		float L_2 = ((&___a)->___y_2);
		float L_3 = ___d;
		Vector2_t6_47  L_4 = {0};
		Vector2__ctor_m6_215(&L_4, ((float)((float)L_0*(float)L_1)), ((float)((float)L_2*(float)L_3)), /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_Division(UnityEngine.Vector2,System.Single)
extern "C" Vector2_t6_47  Vector2_op_Division_m6_232 (Object_t * __this /* static, unused */, Vector2_t6_47  ___a, float ___d, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ___d;
		float L_2 = ((&___a)->___y_2);
		float L_3 = ___d;
		Vector2_t6_47  L_4 = {0};
		Vector2__ctor_m6_215(&L_4, ((float)((float)L_0/(float)L_1)), ((float)((float)L_2/(float)L_3)), /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean UnityEngine.Vector2::op_Equality(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C" bool Vector2_op_Equality_m6_233 (Object_t * __this /* static, unused */, Vector2_t6_47  ___lhs, Vector2_t6_47  ___rhs, const MethodInfo* method)
{
	{
		Vector2_t6_47  L_0 = ___lhs;
		Vector2_t6_47  L_1 = ___rhs;
		Vector2_t6_47  L_2 = Vector2_op_Subtraction_m6_230(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = Vector2_SqrMagnitude_m6_224(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return ((((float)L_3) < ((float)(9.99999944E-11f)))? 1 : 0);
	}
}
// System.Boolean UnityEngine.Vector2::op_Inequality(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C" bool Vector2_op_Inequality_m6_234 (Object_t * __this /* static, unused */, Vector2_t6_47  ___lhs, Vector2_t6_47  ___rhs, const MethodInfo* method)
{
	{
		Vector2_t6_47  L_0 = ___lhs;
		Vector2_t6_47  L_1 = ___rhs;
		Vector2_t6_47  L_2 = Vector2_op_Subtraction_m6_230(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = Vector2_SqrMagnitude_m6_224(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return ((((int32_t)((!(((float)L_3) >= ((float)(9.99999944E-11f))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
extern "C" Vector2_t6_47  Vector2_op_Implicit_m6_235 (Object_t * __this /* static, unused */, Vector3_t6_48  ___v, const MethodInfo* method)
{
	{
		float L_0 = ((&___v)->___x_1);
		float L_1 = ((&___v)->___y_2);
		Vector2_t6_47  L_2 = {0};
		Vector2__ctor_m6_215(&L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
extern "C" Vector3_t6_48  Vector2_op_Implicit_m6_236 (Object_t * __this /* static, unused */, Vector2_t6_47  ___v, const MethodInfo* method)
{
	{
		float L_0 = ((&___v)->___x_1);
		float L_1 = ((&___v)->___y_2);
		Vector3_t6_48  L_2 = {0};
		Vector3__ctor_m6_237(&L_2, L_0, L_1, (0.0f), /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C" void Vector3__ctor_m6_237 (Vector3_t6_48 * __this, float ___x, float ___y, float ___z, const MethodInfo* method)
{
	{
		float L_0 = ___x;
		__this->___x_1 = L_0;
		float L_1 = ___y;
		__this->___y_2 = L_1;
		float L_2 = ___z;
		__this->___z_3 = L_2;
		return;
	}
}
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single)
extern "C" void Vector3__ctor_m6_238 (Vector3_t6_48 * __this, float ___x, float ___y, const MethodInfo* method)
{
	{
		float L_0 = ___x;
		__this->___x_1 = L_0;
		float L_1 = ___y;
		__this->___y_2 = L_1;
		__this->___z_3 = (0.0f);
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::Lerp(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" Vector3_t6_48  Vector3_Lerp_m6_239 (Object_t * __this /* static, unused */, Vector3_t6_48  ___a, Vector3_t6_48  ___b, float ___t, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1589);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___t;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Clamp01_m6_441(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		___t = L_1;
		float L_2 = ((&___a)->___x_1);
		float L_3 = ((&___b)->___x_1);
		float L_4 = ((&___a)->___x_1);
		float L_5 = ___t;
		float L_6 = ((&___a)->___y_2);
		float L_7 = ((&___b)->___y_2);
		float L_8 = ((&___a)->___y_2);
		float L_9 = ___t;
		float L_10 = ((&___a)->___z_3);
		float L_11 = ((&___b)->___z_3);
		float L_12 = ((&___a)->___z_3);
		float L_13 = ___t;
		Vector3_t6_48  L_14 = {0};
		Vector3__ctor_m6_237(&L_14, ((float)((float)L_2+(float)((float)((float)((float)((float)L_3-(float)L_4))*(float)L_5)))), ((float)((float)L_6+(float)((float)((float)((float)((float)L_7-(float)L_8))*(float)L_9)))), ((float)((float)L_10+(float)((float)((float)((float)((float)L_11-(float)L_12))*(float)L_13)))), /*hidden argument*/NULL);
		return L_14;
	}
}
// System.Single UnityEngine.Vector3::get_Item(System.Int32)
extern TypeInfo* IndexOutOfRangeException_t1_1555_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4696;
extern "C" float Vector3_get_Item_m6_240 (Vector3_t6_48 * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IndexOutOfRangeException_t1_1555_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		_stringLiteral4696 = il2cpp_codegen_string_literal_from_index(4696);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0019;
		}
		if (L_1 == 1)
		{
			goto IL_0020;
		}
		if (L_1 == 2)
		{
			goto IL_0027;
		}
	}
	{
		goto IL_002e;
	}

IL_0019:
	{
		float L_2 = (__this->___x_1);
		return L_2;
	}

IL_0020:
	{
		float L_3 = (__this->___y_2);
		return L_3;
	}

IL_0027:
	{
		float L_4 = (__this->___z_3);
		return L_4;
	}

IL_002e:
	{
		IndexOutOfRangeException_t1_1555 * L_5 = (IndexOutOfRangeException_t1_1555 *)il2cpp_codegen_object_new (IndexOutOfRangeException_t1_1555_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m1_14159(L_5, _stringLiteral4696, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_5);
	}
}
// System.Void UnityEngine.Vector3::set_Item(System.Int32,System.Single)
extern TypeInfo* IndexOutOfRangeException_t1_1555_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4696;
extern "C" void Vector3_set_Item_m6_241 (Vector3_t6_48 * __this, int32_t ___index, float ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IndexOutOfRangeException_t1_1555_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		_stringLiteral4696 = il2cpp_codegen_string_literal_from_index(4696);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0019;
		}
		if (L_1 == 1)
		{
			goto IL_0025;
		}
		if (L_1 == 2)
		{
			goto IL_0031;
		}
	}
	{
		goto IL_003d;
	}

IL_0019:
	{
		float L_2 = ___value;
		__this->___x_1 = L_2;
		goto IL_0048;
	}

IL_0025:
	{
		float L_3 = ___value;
		__this->___y_2 = L_3;
		goto IL_0048;
	}

IL_0031:
	{
		float L_4 = ___value;
		__this->___z_3 = L_4;
		goto IL_0048;
	}

IL_003d:
	{
		IndexOutOfRangeException_t1_1555 * L_5 = (IndexOutOfRangeException_t1_1555 *)il2cpp_codegen_object_new (IndexOutOfRangeException_t1_1555_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m1_14159(L_5, _stringLiteral4696, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_5);
	}

IL_0048:
	{
		return;
	}
}
// System.Int32 UnityEngine.Vector3::GetHashCode()
extern "C" int32_t Vector3_GetHashCode_m6_242 (Vector3_t6_48 * __this, const MethodInfo* method)
{
	{
		float* L_0 = &(__this->___x_1);
		int32_t L_1 = Single_GetHashCode_m1_622(L_0, /*hidden argument*/NULL);
		float* L_2 = &(__this->___y_2);
		int32_t L_3 = Single_GetHashCode_m1_622(L_2, /*hidden argument*/NULL);
		float* L_4 = &(__this->___z_3);
		int32_t L_5 = Single_GetHashCode_m1_622(L_4, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))));
	}
}
// System.Boolean UnityEngine.Vector3::Equals(System.Object)
extern TypeInfo* Vector3_t6_48_il2cpp_TypeInfo_var;
extern "C" bool Vector3_Equals_m6_243 (Vector3_t6_48 * __this, Object_t * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Vector3_t6_48_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1590);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t6_48  V_0 = {0};
	int32_t G_B6_0 = 0;
	{
		Object_t * L_0 = ___other;
		if (((Object_t *)IsInstSealed(L_0, Vector3_t6_48_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		Object_t * L_1 = ___other;
		V_0 = ((*(Vector3_t6_48 *)((Vector3_t6_48 *)UnBox (L_1, Vector3_t6_48_il2cpp_TypeInfo_var))));
		float* L_2 = &(__this->___x_1);
		float L_3 = ((&V_0)->___x_1);
		bool L_4 = Single_Equals_m1_621(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0056;
		}
	}
	{
		float* L_5 = &(__this->___y_2);
		float L_6 = ((&V_0)->___y_2);
		bool L_7 = Single_Equals_m1_621(L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0056;
		}
	}
	{
		float* L_8 = &(__this->___z_3);
		float L_9 = ((&V_0)->___z_3);
		bool L_10 = Single_Equals_m1_621(L_8, L_9, /*hidden argument*/NULL);
		G_B6_0 = ((int32_t)(L_10));
		goto IL_0057;
	}

IL_0056:
	{
		G_B6_0 = 0;
	}

IL_0057:
	{
		return G_B6_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::Normalize(UnityEngine.Vector3)
extern "C" Vector3_t6_48  Vector3_Normalize_m6_244 (Object_t * __this /* static, unused */, Vector3_t6_48  ___value, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		Vector3_t6_48  L_0 = ___value;
		float L_1 = Vector3_Magnitude_m6_250(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = V_0;
		if ((!(((float)L_2) > ((float)(1.0E-05f)))))
		{
			goto IL_001a;
		}
	}
	{
		Vector3_t6_48  L_3 = ___value;
		float L_4 = V_0;
		Vector3_t6_48  L_5 = Vector3_op_Division_m6_266(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_001a:
	{
		Vector3_t6_48  L_6 = Vector3_get_zero_m6_255(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_normalized()
extern "C" Vector3_t6_48  Vector3_get_normalized_m6_245 (Vector3_t6_48 * __this, const MethodInfo* method)
{
	{
		Vector3_t6_48  L_0 = Vector3_Normalize_m6_244(NULL /*static, unused*/, (*(Vector3_t6_48 *)__this), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String UnityEngine.Vector3::ToString()
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t1_17_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4697;
extern "C" String_t* Vector3_ToString_m6_246 (Vector3_t6_48 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Single_t1_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		_stringLiteral4697 = il2cpp_codegen_string_literal_from_index(4697);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_272* L_0 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 3));
		float L_1 = (__this->___x_1);
		float L_2 = L_1;
		Object_t * L_3 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_3;
		ObjectU5BU5D_t1_272* L_4 = L_0;
		float L_5 = (__this->___y_2);
		float L_6 = L_5;
		Object_t * L_7 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1, sizeof(Object_t *))) = (Object_t *)L_7;
		ObjectU5BU5D_t1_272* L_8 = L_4;
		float L_9 = (__this->___z_3);
		float L_10 = L_9;
		Object_t * L_11 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 2, sizeof(Object_t *))) = (Object_t *)L_11;
		String_t* L_12 = UnityString_Format_m6_573(NULL /*static, unused*/, _stringLiteral4697, L_8, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.String UnityEngine.Vector3::ToString(System.String)
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4698;
extern "C" String_t* Vector3_ToString_m6_247 (Vector3_t6_48 * __this, String_t* ___format, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		_stringLiteral4698 = il2cpp_codegen_string_literal_from_index(4698);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_272* L_0 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 3));
		float* L_1 = &(__this->___x_1);
		String_t* L_2 = ___format;
		String_t* L_3 = Single_ToString_m1_635(L_1, L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_3;
		ObjectU5BU5D_t1_272* L_4 = L_0;
		float* L_5 = &(__this->___y_2);
		String_t* L_6 = ___format;
		String_t* L_7 = Single_ToString_m1_635(L_5, L_6, /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1, sizeof(Object_t *))) = (Object_t *)L_7;
		ObjectU5BU5D_t1_272* L_8 = L_4;
		float* L_9 = &(__this->___z_3);
		String_t* L_10 = ___format;
		String_t* L_11 = Single_ToString_m1_635(L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 2, sizeof(Object_t *))) = (Object_t *)L_11;
		String_t* L_12 = UnityString_Format_m6_573(NULL /*static, unused*/, _stringLiteral4698, L_8, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Single UnityEngine.Vector3::Dot(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" float Vector3_Dot_m6_248 (Object_t * __this /* static, unused */, Vector3_t6_48  ___lhs, Vector3_t6_48  ___rhs, const MethodInfo* method)
{
	{
		float L_0 = ((&___lhs)->___x_1);
		float L_1 = ((&___rhs)->___x_1);
		float L_2 = ((&___lhs)->___y_2);
		float L_3 = ((&___rhs)->___y_2);
		float L_4 = ((&___lhs)->___z_3);
		float L_5 = ((&___rhs)->___z_3);
		return ((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))));
	}
}
// System.Single UnityEngine.Vector3::Distance(UnityEngine.Vector3,UnityEngine.Vector3)
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" float Vector3_Distance_m6_249 (Object_t * __this /* static, unused */, Vector3_t6_48  ___a, Vector3_t6_48  ___b, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1589);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t6_48  V_0 = {0};
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ((&___b)->___x_1);
		float L_2 = ((&___a)->___y_2);
		float L_3 = ((&___b)->___y_2);
		float L_4 = ((&___a)->___z_3);
		float L_5 = ((&___b)->___z_3);
		Vector3__ctor_m6_237((&V_0), ((float)((float)L_0-(float)L_1)), ((float)((float)L_2-(float)L_3)), ((float)((float)L_4-(float)L_5)), /*hidden argument*/NULL);
		float L_6 = ((&V_0)->___x_1);
		float L_7 = ((&V_0)->___x_1);
		float L_8 = ((&V_0)->___y_2);
		float L_9 = ((&V_0)->___y_2);
		float L_10 = ((&V_0)->___z_3);
		float L_11 = ((&V_0)->___z_3);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_12 = sqrtf(((float)((float)((float)((float)((float)((float)L_6*(float)L_7))+(float)((float)((float)L_8*(float)L_9))))+(float)((float)((float)L_10*(float)L_11)))));
		return L_12;
	}
}
// System.Single UnityEngine.Vector3::Magnitude(UnityEngine.Vector3)
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" float Vector3_Magnitude_m6_250 (Object_t * __this /* static, unused */, Vector3_t6_48  ___a, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1589);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ((&___a)->___x_1);
		float L_2 = ((&___a)->___y_2);
		float L_3 = ((&___a)->___y_2);
		float L_4 = ((&___a)->___z_3);
		float L_5 = ((&___a)->___z_3);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_6 = sqrtf(((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5)))));
		return L_6;
	}
}
// System.Single UnityEngine.Vector3::SqrMagnitude(UnityEngine.Vector3)
extern "C" float Vector3_SqrMagnitude_m6_251 (Object_t * __this /* static, unused */, Vector3_t6_48  ___a, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ((&___a)->___x_1);
		float L_2 = ((&___a)->___y_2);
		float L_3 = ((&___a)->___y_2);
		float L_4 = ((&___a)->___z_3);
		float L_5 = ((&___a)->___z_3);
		return ((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))));
	}
}
// System.Single UnityEngine.Vector3::get_sqrMagnitude()
extern "C" float Vector3_get_sqrMagnitude_m6_252 (Vector3_t6_48 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___x_1);
		float L_1 = (__this->___x_1);
		float L_2 = (__this->___y_2);
		float L_3 = (__this->___y_2);
		float L_4 = (__this->___z_3);
		float L_5 = (__this->___z_3);
		return ((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))));
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::Min(UnityEngine.Vector3,UnityEngine.Vector3)
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" Vector3_t6_48  Vector3_Min_m6_253 (Object_t * __this /* static, unused */, Vector3_t6_48  ___lhs, Vector3_t6_48  ___rhs, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1589);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ((&___lhs)->___x_1);
		float L_1 = ((&___rhs)->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_2 = Mathf_Min_m6_427(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = ((&___lhs)->___y_2);
		float L_4 = ((&___rhs)->___y_2);
		float L_5 = Mathf_Min_m6_427(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		float L_6 = ((&___lhs)->___z_3);
		float L_7 = ((&___rhs)->___z_3);
		float L_8 = Mathf_Min_m6_427(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		Vector3_t6_48  L_9 = {0};
		Vector3__ctor_m6_237(&L_9, L_2, L_5, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::Max(UnityEngine.Vector3,UnityEngine.Vector3)
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" Vector3_t6_48  Vector3_Max_m6_254 (Object_t * __this /* static, unused */, Vector3_t6_48  ___lhs, Vector3_t6_48  ___rhs, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1589);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ((&___lhs)->___x_1);
		float L_1 = ((&___rhs)->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_2 = Mathf_Max_m6_429(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = ((&___lhs)->___y_2);
		float L_4 = ((&___rhs)->___y_2);
		float L_5 = Mathf_Max_m6_429(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		float L_6 = ((&___lhs)->___z_3);
		float L_7 = ((&___rhs)->___z_3);
		float L_8 = Mathf_Max_m6_429(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		Vector3_t6_48  L_9 = {0};
		Vector3__ctor_m6_237(&L_9, L_2, L_5, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
extern "C" Vector3_t6_48  Vector3_get_zero_m6_255 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector3_t6_48  L_0 = {0};
		Vector3__ctor_m6_237(&L_0, (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_one()
extern "C" Vector3_t6_48  Vector3_get_one_m6_256 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector3_t6_48  L_0 = {0};
		Vector3__ctor_m6_237(&L_0, (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_forward()
extern "C" Vector3_t6_48  Vector3_get_forward_m6_257 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector3_t6_48  L_0 = {0};
		Vector3__ctor_m6_237(&L_0, (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_back()
extern "C" Vector3_t6_48  Vector3_get_back_m6_258 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector3_t6_48  L_0 = {0};
		Vector3__ctor_m6_237(&L_0, (0.0f), (0.0f), (-1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_up()
extern "C" Vector3_t6_48  Vector3_get_up_m6_259 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector3_t6_48  L_0 = {0};
		Vector3__ctor_m6_237(&L_0, (0.0f), (1.0f), (0.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_down()
extern "C" Vector3_t6_48  Vector3_get_down_m6_260 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector3_t6_48  L_0 = {0};
		Vector3__ctor_m6_237(&L_0, (0.0f), (-1.0f), (0.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_left()
extern "C" Vector3_t6_48  Vector3_get_left_m6_261 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector3_t6_48  L_0 = {0};
		Vector3__ctor_m6_237(&L_0, (-1.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_right()
extern "C" Vector3_t6_48  Vector3_get_right_m6_262 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector3_t6_48  L_0 = {0};
		Vector3__ctor_m6_237(&L_0, (1.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" Vector3_t6_48  Vector3_op_Addition_m6_263 (Object_t * __this /* static, unused */, Vector3_t6_48  ___a, Vector3_t6_48  ___b, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ((&___b)->___x_1);
		float L_2 = ((&___a)->___y_2);
		float L_3 = ((&___b)->___y_2);
		float L_4 = ((&___a)->___z_3);
		float L_5 = ((&___b)->___z_3);
		Vector3_t6_48  L_6 = {0};
		Vector3__ctor_m6_237(&L_6, ((float)((float)L_0+(float)L_1)), ((float)((float)L_2+(float)L_3)), ((float)((float)L_4+(float)L_5)), /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" Vector3_t6_48  Vector3_op_Subtraction_m6_264 (Object_t * __this /* static, unused */, Vector3_t6_48  ___a, Vector3_t6_48  ___b, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ((&___b)->___x_1);
		float L_2 = ((&___a)->___y_2);
		float L_3 = ((&___b)->___y_2);
		float L_4 = ((&___a)->___z_3);
		float L_5 = ((&___b)->___z_3);
		Vector3_t6_48  L_6 = {0};
		Vector3__ctor_m6_237(&L_6, ((float)((float)L_0-(float)L_1)), ((float)((float)L_2-(float)L_3)), ((float)((float)L_4-(float)L_5)), /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
extern "C" Vector3_t6_48  Vector3_op_Multiply_m6_265 (Object_t * __this /* static, unused */, Vector3_t6_48  ___a, float ___d, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ___d;
		float L_2 = ((&___a)->___y_2);
		float L_3 = ___d;
		float L_4 = ((&___a)->___z_3);
		float L_5 = ___d;
		Vector3_t6_48  L_6 = {0};
		Vector3__ctor_m6_237(&L_6, ((float)((float)L_0*(float)L_1)), ((float)((float)L_2*(float)L_3)), ((float)((float)L_4*(float)L_5)), /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::op_Division(UnityEngine.Vector3,System.Single)
extern "C" Vector3_t6_48  Vector3_op_Division_m6_266 (Object_t * __this /* static, unused */, Vector3_t6_48  ___a, float ___d, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ___d;
		float L_2 = ((&___a)->___y_2);
		float L_3 = ___d;
		float L_4 = ((&___a)->___z_3);
		float L_5 = ___d;
		Vector3_t6_48  L_6 = {0};
		Vector3__ctor_m6_237(&L_6, ((float)((float)L_0/(float)L_1)), ((float)((float)L_2/(float)L_3)), ((float)((float)L_4/(float)L_5)), /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Boolean UnityEngine.Vector3::op_Equality(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" bool Vector3_op_Equality_m6_267 (Object_t * __this /* static, unused */, Vector3_t6_48  ___lhs, Vector3_t6_48  ___rhs, const MethodInfo* method)
{
	{
		Vector3_t6_48  L_0 = ___lhs;
		Vector3_t6_48  L_1 = ___rhs;
		Vector3_t6_48  L_2 = Vector3_op_Subtraction_m6_264(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = Vector3_SqrMagnitude_m6_251(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return ((((float)L_3) < ((float)(9.99999944E-11f)))? 1 : 0);
	}
}
// System.Boolean UnityEngine.Vector3::op_Inequality(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" bool Vector3_op_Inequality_m6_268 (Object_t * __this /* static, unused */, Vector3_t6_48  ___lhs, Vector3_t6_48  ___rhs, const MethodInfo* method)
{
	{
		Vector3_t6_48  L_0 = ___lhs;
		Vector3_t6_48  L_1 = ___rhs;
		Vector3_t6_48  L_2 = Vector3_op_Subtraction_m6_264(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = Vector3_SqrMagnitude_m6_251(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return ((((int32_t)((!(((float)L_3) >= ((float)(9.99999944E-11f))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C" void Color__ctor_m6_269 (Color_t6_40 * __this, float ___r, float ___g, float ___b, float ___a, const MethodInfo* method)
{
	{
		float L_0 = ___r;
		__this->___r_0 = L_0;
		float L_1 = ___g;
		__this->___g_1 = L_1;
		float L_2 = ___b;
		__this->___b_2 = L_2;
		float L_3 = ___a;
		__this->___a_3 = L_3;
		return;
	}
}
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single)
extern "C" void Color__ctor_m6_270 (Color_t6_40 * __this, float ___r, float ___g, float ___b, const MethodInfo* method)
{
	{
		float L_0 = ___r;
		__this->___r_0 = L_0;
		float L_1 = ___g;
		__this->___g_1 = L_1;
		float L_2 = ___b;
		__this->___b_2 = L_2;
		__this->___a_3 = (1.0f);
		return;
	}
}
// System.String UnityEngine.Color::ToString()
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t1_17_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4699;
extern "C" String_t* Color_ToString_m6_271 (Color_t6_40 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Single_t1_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		_stringLiteral4699 = il2cpp_codegen_string_literal_from_index(4699);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_272* L_0 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 4));
		float L_1 = (__this->___r_0);
		float L_2 = L_1;
		Object_t * L_3 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_3;
		ObjectU5BU5D_t1_272* L_4 = L_0;
		float L_5 = (__this->___g_1);
		float L_6 = L_5;
		Object_t * L_7 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1, sizeof(Object_t *))) = (Object_t *)L_7;
		ObjectU5BU5D_t1_272* L_8 = L_4;
		float L_9 = (__this->___b_2);
		float L_10 = L_9;
		Object_t * L_11 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 2, sizeof(Object_t *))) = (Object_t *)L_11;
		ObjectU5BU5D_t1_272* L_12 = L_8;
		float L_13 = (__this->___a_3);
		float L_14 = L_13;
		Object_t * L_15 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 3, sizeof(Object_t *))) = (Object_t *)L_15;
		String_t* L_16 = UnityString_Format_m6_573(NULL /*static, unused*/, _stringLiteral4699, L_12, /*hidden argument*/NULL);
		return L_16;
	}
}
// System.Int32 UnityEngine.Color::GetHashCode()
extern "C" int32_t Color_GetHashCode_m6_272 (Color_t6_40 * __this, const MethodInfo* method)
{
	Vector4_t6_54  V_0 = {0};
	{
		Vector4_t6_54  L_0 = Color_op_Implicit_m6_280(NULL /*static, unused*/, (*(Color_t6_40 *)__this), /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Vector4_GetHashCode_m6_402((&V_0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityEngine.Color::Equals(System.Object)
extern TypeInfo* Color_t6_40_il2cpp_TypeInfo_var;
extern "C" bool Color_Equals_m6_273 (Color_t6_40 * __this, Object_t * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Color_t6_40_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1591);
		s_Il2CppMethodIntialized = true;
	}
	Color_t6_40  V_0 = {0};
	int32_t G_B7_0 = 0;
	{
		Object_t * L_0 = ___other;
		if (((Object_t *)IsInstSealed(L_0, Color_t6_40_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		Object_t * L_1 = ___other;
		V_0 = ((*(Color_t6_40 *)((Color_t6_40 *)UnBox (L_1, Color_t6_40_il2cpp_TypeInfo_var))));
		float* L_2 = &(__this->___r_0);
		float L_3 = ((&V_0)->___r_0);
		bool L_4 = Single_Equals_m1_621(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_006d;
		}
	}
	{
		float* L_5 = &(__this->___g_1);
		float L_6 = ((&V_0)->___g_1);
		bool L_7 = Single_Equals_m1_621(L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_006d;
		}
	}
	{
		float* L_8 = &(__this->___b_2);
		float L_9 = ((&V_0)->___b_2);
		bool L_10 = Single_Equals_m1_621(L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_006d;
		}
	}
	{
		float* L_11 = &(__this->___a_3);
		float L_12 = ((&V_0)->___a_3);
		bool L_13 = Single_Equals_m1_621(L_11, L_12, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_13));
		goto IL_006e;
	}

IL_006d:
	{
		G_B7_0 = 0;
	}

IL_006e:
	{
		return G_B7_0;
	}
}
// UnityEngine.Color UnityEngine.Color::Lerp(UnityEngine.Color,UnityEngine.Color,System.Single)
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" Color_t6_40  Color_Lerp_m6_274 (Object_t * __this /* static, unused */, Color_t6_40  ___a, Color_t6_40  ___b, float ___t, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1589);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___t;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Clamp01_m6_441(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		___t = L_1;
		float L_2 = ((&___a)->___r_0);
		float L_3 = ((&___b)->___r_0);
		float L_4 = ((&___a)->___r_0);
		float L_5 = ___t;
		float L_6 = ((&___a)->___g_1);
		float L_7 = ((&___b)->___g_1);
		float L_8 = ((&___a)->___g_1);
		float L_9 = ___t;
		float L_10 = ((&___a)->___b_2);
		float L_11 = ((&___b)->___b_2);
		float L_12 = ((&___a)->___b_2);
		float L_13 = ___t;
		float L_14 = ((&___a)->___a_3);
		float L_15 = ((&___b)->___a_3);
		float L_16 = ((&___a)->___a_3);
		float L_17 = ___t;
		Color_t6_40  L_18 = {0};
		Color__ctor_m6_269(&L_18, ((float)((float)L_2+(float)((float)((float)((float)((float)L_3-(float)L_4))*(float)L_5)))), ((float)((float)L_6+(float)((float)((float)((float)((float)L_7-(float)L_8))*(float)L_9)))), ((float)((float)L_10+(float)((float)((float)((float)((float)L_11-(float)L_12))*(float)L_13)))), ((float)((float)L_14+(float)((float)((float)((float)((float)L_15-(float)L_16))*(float)L_17)))), /*hidden argument*/NULL);
		return L_18;
	}
}
// UnityEngine.Color UnityEngine.Color::get_red()
extern "C" Color_t6_40  Color_get_red_m6_275 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Color_t6_40  L_0 = {0};
		Color__ctor_m6_269(&L_0, (1.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Color UnityEngine.Color::get_white()
extern "C" Color_t6_40  Color_get_white_m6_276 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Color_t6_40  L_0 = {0};
		Color__ctor_m6_269(&L_0, (1.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Color UnityEngine.Color::get_black()
extern "C" Color_t6_40  Color_get_black_m6_277 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Color_t6_40  L_0 = {0};
		Color__ctor_m6_269(&L_0, (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Color UnityEngine.Color::get_clear()
extern "C" Color_t6_40  Color_get_clear_m6_278 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Color_t6_40  L_0 = {0};
		Color__ctor_m6_269(&L_0, (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Color UnityEngine.Color::op_Multiply(UnityEngine.Color,System.Single)
extern "C" Color_t6_40  Color_op_Multiply_m6_279 (Object_t * __this /* static, unused */, Color_t6_40  ___a, float ___b, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___r_0);
		float L_1 = ___b;
		float L_2 = ((&___a)->___g_1);
		float L_3 = ___b;
		float L_4 = ((&___a)->___b_2);
		float L_5 = ___b;
		float L_6 = ((&___a)->___a_3);
		float L_7 = ___b;
		Color_t6_40  L_8 = {0};
		Color__ctor_m6_269(&L_8, ((float)((float)L_0*(float)L_1)), ((float)((float)L_2*(float)L_3)), ((float)((float)L_4*(float)L_5)), ((float)((float)L_6*(float)L_7)), /*hidden argument*/NULL);
		return L_8;
	}
}
// UnityEngine.Vector4 UnityEngine.Color::op_Implicit(UnityEngine.Color)
extern "C" Vector4_t6_54  Color_op_Implicit_m6_280 (Object_t * __this /* static, unused */, Color_t6_40  ___c, const MethodInfo* method)
{
	{
		float L_0 = ((&___c)->___r_0);
		float L_1 = ((&___c)->___g_1);
		float L_2 = ((&___c)->___b_2);
		float L_3 = ((&___c)->___a_3);
		Vector4_t6_54  L_4 = {0};
		Vector4__ctor_m6_399(&L_4, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void UnityEngine.Color32::.ctor(System.Byte,System.Byte,System.Byte,System.Byte)
extern "C" void Color32__ctor_m6_281 (Color32_t6_49 * __this, uint8_t ___r, uint8_t ___g, uint8_t ___b, uint8_t ___a, const MethodInfo* method)
{
	{
		uint8_t L_0 = ___r;
		__this->___r_0 = L_0;
		uint8_t L_1 = ___g;
		__this->___g_1 = L_1;
		uint8_t L_2 = ___b;
		__this->___b_2 = L_2;
		uint8_t L_3 = ___a;
		__this->___a_3 = L_3;
		return;
	}
}
// System.String UnityEngine.Color32::ToString()
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4700;
extern "C" String_t* Color32_ToString_m6_282 (Color32_t6_49 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		_stringLiteral4700 = il2cpp_codegen_string_literal_from_index(4700);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_272* L_0 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 4));
		uint8_t L_1 = (__this->___r_0);
		uint8_t L_2 = L_1;
		Object_t * L_3 = Box(Byte_t1_11_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_3;
		ObjectU5BU5D_t1_272* L_4 = L_0;
		uint8_t L_5 = (__this->___g_1);
		uint8_t L_6 = L_5;
		Object_t * L_7 = Box(Byte_t1_11_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1, sizeof(Object_t *))) = (Object_t *)L_7;
		ObjectU5BU5D_t1_272* L_8 = L_4;
		uint8_t L_9 = (__this->___b_2);
		uint8_t L_10 = L_9;
		Object_t * L_11 = Box(Byte_t1_11_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 2, sizeof(Object_t *))) = (Object_t *)L_11;
		ObjectU5BU5D_t1_272* L_12 = L_8;
		uint8_t L_13 = (__this->___a_3);
		uint8_t L_14 = L_13;
		Object_t * L_15 = Box(Byte_t1_11_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 3, sizeof(Object_t *))) = (Object_t *)L_15;
		String_t* L_16 = UnityString_Format_m6_573(NULL /*static, unused*/, _stringLiteral4700, L_12, /*hidden argument*/NULL);
		return L_16;
	}
}
// UnityEngine.Color32 UnityEngine.Color32::op_Implicit(UnityEngine.Color)
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" Color32_t6_49  Color32_op_Implicit_m6_283 (Object_t * __this /* static, unused */, Color_t6_40  ___c, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1589);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ((&___c)->___r_0);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Clamp01_m6_441(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		float L_2 = ((&___c)->___g_1);
		float L_3 = Mathf_Clamp01_m6_441(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		float L_4 = ((&___c)->___b_2);
		float L_5 = Mathf_Clamp01_m6_441(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		float L_6 = ((&___c)->___a_3);
		float L_7 = Mathf_Clamp01_m6_441(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		Color32_t6_49  L_8 = {0};
		Color32__ctor_m6_281(&L_8, (((int32_t)((uint8_t)((float)((float)L_1*(float)(255.0f)))))), (((int32_t)((uint8_t)((float)((float)L_3*(float)(255.0f)))))), (((int32_t)((uint8_t)((float)((float)L_5*(float)(255.0f)))))), (((int32_t)((uint8_t)((float)((float)L_7*(float)(255.0f)))))), /*hidden argument*/NULL);
		return L_8;
	}
}
// UnityEngine.Color UnityEngine.Color32::op_Implicit(UnityEngine.Color32)
extern "C" Color_t6_40  Color32_op_Implicit_m6_284 (Object_t * __this /* static, unused */, Color32_t6_49  ___c, const MethodInfo* method)
{
	{
		uint8_t L_0 = ((&___c)->___r_0);
		uint8_t L_1 = ((&___c)->___g_1);
		uint8_t L_2 = ((&___c)->___b_2);
		uint8_t L_3 = ((&___c)->___a_3);
		Color_t6_40  L_4 = {0};
		Color__ctor_m6_269(&L_4, ((float)((float)(((float)((float)L_0)))/(float)(255.0f))), ((float)((float)(((float)((float)L_1)))/(float)(255.0f))), ((float)((float)(((float)((float)L_2)))/(float)(255.0f))), ((float)((float)(((float)((float)L_3)))/(float)(255.0f))), /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void UnityEngine.Quaternion::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C" void Quaternion__ctor_m6_285 (Quaternion_t6_50 * __this, float ___x, float ___y, float ___z, float ___w, const MethodInfo* method)
{
	{
		float L_0 = ___x;
		__this->___x_0 = L_0;
		float L_1 = ___y;
		__this->___y_1 = L_1;
		float L_2 = ___z;
		__this->___z_2 = L_2;
		float L_3 = ___w;
		__this->___w_3 = L_3;
		return;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
extern "C" Quaternion_t6_50  Quaternion_get_identity_m6_286 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Quaternion_t6_50  L_0 = {0};
		Quaternion__ctor_m6_285(&L_0, (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Single UnityEngine.Quaternion::Dot(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C" float Quaternion_Dot_m6_287 (Object_t * __this /* static, unused */, Quaternion_t6_50  ___a, Quaternion_t6_50  ___b, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_0);
		float L_1 = ((&___b)->___x_0);
		float L_2 = ((&___a)->___y_1);
		float L_3 = ((&___b)->___y_1);
		float L_4 = ((&___a)->___z_2);
		float L_5 = ((&___b)->___z_2);
		float L_6 = ((&___a)->___w_3);
		float L_7 = ((&___b)->___w_3);
		return ((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))+(float)((float)((float)L_6*(float)L_7))));
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Inverse(UnityEngine.Quaternion)
extern "C" Quaternion_t6_50  Quaternion_Inverse_m6_288 (Object_t * __this /* static, unused */, Quaternion_t6_50  ___rotation, const MethodInfo* method)
{
	{
		Quaternion_t6_50  L_0 = Quaternion_INTERNAL_CALL_Inverse_m6_289(NULL /*static, unused*/, (&___rotation), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::INTERNAL_CALL_Inverse(UnityEngine.Quaternion&)
extern "C" Quaternion_t6_50  Quaternion_INTERNAL_CALL_Inverse_m6_289 (Object_t * __this /* static, unused */, Quaternion_t6_50 * ___rotation, const MethodInfo* method)
{
	typedef Quaternion_t6_50  (*Quaternion_INTERNAL_CALL_Inverse_m6_289_ftn) (Quaternion_t6_50 *);
	static Quaternion_INTERNAL_CALL_Inverse_m6_289_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Inverse_m6_289_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Inverse(UnityEngine.Quaternion&)");
	return _il2cpp_icall_func(___rotation);
}
// System.String UnityEngine.Quaternion::ToString()
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t1_17_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4701;
extern "C" String_t* Quaternion_ToString_m6_290 (Quaternion_t6_50 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Single_t1_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		_stringLiteral4701 = il2cpp_codegen_string_literal_from_index(4701);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_272* L_0 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 4));
		float L_1 = (__this->___x_0);
		float L_2 = L_1;
		Object_t * L_3 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_3;
		ObjectU5BU5D_t1_272* L_4 = L_0;
		float L_5 = (__this->___y_1);
		float L_6 = L_5;
		Object_t * L_7 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1, sizeof(Object_t *))) = (Object_t *)L_7;
		ObjectU5BU5D_t1_272* L_8 = L_4;
		float L_9 = (__this->___z_2);
		float L_10 = L_9;
		Object_t * L_11 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 2, sizeof(Object_t *))) = (Object_t *)L_11;
		ObjectU5BU5D_t1_272* L_12 = L_8;
		float L_13 = (__this->___w_3);
		float L_14 = L_13;
		Object_t * L_15 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 3, sizeof(Object_t *))) = (Object_t *)L_15;
		String_t* L_16 = UnityString_Format_m6_573(NULL /*static, unused*/, _stringLiteral4701, L_12, /*hidden argument*/NULL);
		return L_16;
	}
}
// System.Int32 UnityEngine.Quaternion::GetHashCode()
extern "C" int32_t Quaternion_GetHashCode_m6_291 (Quaternion_t6_50 * __this, const MethodInfo* method)
{
	{
		float* L_0 = &(__this->___x_0);
		int32_t L_1 = Single_GetHashCode_m1_622(L_0, /*hidden argument*/NULL);
		float* L_2 = &(__this->___y_1);
		int32_t L_3 = Single_GetHashCode_m1_622(L_2, /*hidden argument*/NULL);
		float* L_4 = &(__this->___z_2);
		int32_t L_5 = Single_GetHashCode_m1_622(L_4, /*hidden argument*/NULL);
		float* L_6 = &(__this->___w_3);
		int32_t L_7 = Single_GetHashCode_m1_622(L_6, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)1))));
	}
}
// System.Boolean UnityEngine.Quaternion::Equals(System.Object)
extern TypeInfo* Quaternion_t6_50_il2cpp_TypeInfo_var;
extern "C" bool Quaternion_Equals_m6_292 (Quaternion_t6_50 * __this, Object_t * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Quaternion_t6_50_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1592);
		s_Il2CppMethodIntialized = true;
	}
	Quaternion_t6_50  V_0 = {0};
	int32_t G_B7_0 = 0;
	{
		Object_t * L_0 = ___other;
		if (((Object_t *)IsInstSealed(L_0, Quaternion_t6_50_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		Object_t * L_1 = ___other;
		V_0 = ((*(Quaternion_t6_50 *)((Quaternion_t6_50 *)UnBox (L_1, Quaternion_t6_50_il2cpp_TypeInfo_var))));
		float* L_2 = &(__this->___x_0);
		float L_3 = ((&V_0)->___x_0);
		bool L_4 = Single_Equals_m1_621(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_006d;
		}
	}
	{
		float* L_5 = &(__this->___y_1);
		float L_6 = ((&V_0)->___y_1);
		bool L_7 = Single_Equals_m1_621(L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_006d;
		}
	}
	{
		float* L_8 = &(__this->___z_2);
		float L_9 = ((&V_0)->___z_2);
		bool L_10 = Single_Equals_m1_621(L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_006d;
		}
	}
	{
		float* L_11 = &(__this->___w_3);
		float L_12 = ((&V_0)->___w_3);
		bool L_13 = Single_Equals_m1_621(L_11, L_12, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_13));
		goto IL_006e;
	}

IL_006d:
	{
		G_B7_0 = 0;
	}

IL_006e:
	{
		return G_B7_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C" Vector3_t6_48  Quaternion_op_Multiply_m6_293 (Object_t * __this /* static, unused */, Quaternion_t6_50  ___rotation, Vector3_t6_48  ___point, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	Vector3_t6_48  V_12 = {0};
	{
		float L_0 = ((&___rotation)->___x_0);
		V_0 = ((float)((float)L_0*(float)(2.0f)));
		float L_1 = ((&___rotation)->___y_1);
		V_1 = ((float)((float)L_1*(float)(2.0f)));
		float L_2 = ((&___rotation)->___z_2);
		V_2 = ((float)((float)L_2*(float)(2.0f)));
		float L_3 = ((&___rotation)->___x_0);
		float L_4 = V_0;
		V_3 = ((float)((float)L_3*(float)L_4));
		float L_5 = ((&___rotation)->___y_1);
		float L_6 = V_1;
		V_4 = ((float)((float)L_5*(float)L_6));
		float L_7 = ((&___rotation)->___z_2);
		float L_8 = V_2;
		V_5 = ((float)((float)L_7*(float)L_8));
		float L_9 = ((&___rotation)->___x_0);
		float L_10 = V_1;
		V_6 = ((float)((float)L_9*(float)L_10));
		float L_11 = ((&___rotation)->___x_0);
		float L_12 = V_2;
		V_7 = ((float)((float)L_11*(float)L_12));
		float L_13 = ((&___rotation)->___y_1);
		float L_14 = V_2;
		V_8 = ((float)((float)L_13*(float)L_14));
		float L_15 = ((&___rotation)->___w_3);
		float L_16 = V_0;
		V_9 = ((float)((float)L_15*(float)L_16));
		float L_17 = ((&___rotation)->___w_3);
		float L_18 = V_1;
		V_10 = ((float)((float)L_17*(float)L_18));
		float L_19 = ((&___rotation)->___w_3);
		float L_20 = V_2;
		V_11 = ((float)((float)L_19*(float)L_20));
		float L_21 = V_4;
		float L_22 = V_5;
		float L_23 = ((&___point)->___x_1);
		float L_24 = V_6;
		float L_25 = V_11;
		float L_26 = ((&___point)->___y_2);
		float L_27 = V_7;
		float L_28 = V_10;
		float L_29 = ((&___point)->___z_3);
		(&V_12)->___x_1 = ((float)((float)((float)((float)((float)((float)((float)((float)(1.0f)-(float)((float)((float)L_21+(float)L_22))))*(float)L_23))+(float)((float)((float)((float)((float)L_24-(float)L_25))*(float)L_26))))+(float)((float)((float)((float)((float)L_27+(float)L_28))*(float)L_29))));
		float L_30 = V_6;
		float L_31 = V_11;
		float L_32 = ((&___point)->___x_1);
		float L_33 = V_3;
		float L_34 = V_5;
		float L_35 = ((&___point)->___y_2);
		float L_36 = V_8;
		float L_37 = V_9;
		float L_38 = ((&___point)->___z_3);
		(&V_12)->___y_2 = ((float)((float)((float)((float)((float)((float)((float)((float)L_30+(float)L_31))*(float)L_32))+(float)((float)((float)((float)((float)(1.0f)-(float)((float)((float)L_33+(float)L_34))))*(float)L_35))))+(float)((float)((float)((float)((float)L_36-(float)L_37))*(float)L_38))));
		float L_39 = V_7;
		float L_40 = V_10;
		float L_41 = ((&___point)->___x_1);
		float L_42 = V_8;
		float L_43 = V_9;
		float L_44 = ((&___point)->___y_2);
		float L_45 = V_3;
		float L_46 = V_4;
		float L_47 = ((&___point)->___z_3);
		(&V_12)->___z_3 = ((float)((float)((float)((float)((float)((float)((float)((float)L_39-(float)L_40))*(float)L_41))+(float)((float)((float)((float)((float)L_42+(float)L_43))*(float)L_44))))+(float)((float)((float)((float)((float)(1.0f)-(float)((float)((float)L_45+(float)L_46))))*(float)L_47))));
		Vector3_t6_48  L_48 = V_12;
		return L_48;
	}
}
// System.Boolean UnityEngine.Quaternion::op_Inequality(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C" bool Quaternion_op_Inequality_m6_294 (Object_t * __this /* static, unused */, Quaternion_t6_50  ___lhs, Quaternion_t6_50  ___rhs, const MethodInfo* method)
{
	{
		Quaternion_t6_50  L_0 = ___lhs;
		Quaternion_t6_50  L_1 = ___rhs;
		float L_2 = Quaternion_Dot_m6_287(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return ((((int32_t)((!(((float)L_2) <= ((float)(0.999999f))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C" void Rect__ctor_m6_295 (Rect_t6_51 * __this, float ___x, float ___y, float ___width, float ___height, const MethodInfo* method)
{
	{
		float L_0 = ___x;
		__this->___m_XMin_0 = L_0;
		float L_1 = ___y;
		__this->___m_YMin_1 = L_1;
		float L_2 = ___width;
		__this->___m_Width_2 = L_2;
		float L_3 = ___height;
		__this->___m_Height_3 = L_3;
		return;
	}
}
// System.Void UnityEngine.Rect::.ctor(UnityEngine.Rect)
extern "C" void Rect__ctor_m6_296 (Rect_t6_51 * __this, Rect_t6_51  ___source, const MethodInfo* method)
{
	{
		float L_0 = ((&___source)->___m_XMin_0);
		__this->___m_XMin_0 = L_0;
		float L_1 = ((&___source)->___m_YMin_1);
		__this->___m_YMin_1 = L_1;
		float L_2 = ((&___source)->___m_Width_2);
		__this->___m_Width_2 = L_2;
		float L_3 = ((&___source)->___m_Height_3);
		__this->___m_Height_3 = L_3;
		return;
	}
}
// UnityEngine.Rect UnityEngine.Rect::MinMaxRect(System.Single,System.Single,System.Single,System.Single)
extern "C" Rect_t6_51  Rect_MinMaxRect_m6_297 (Object_t * __this /* static, unused */, float ___xmin, float ___ymin, float ___xmax, float ___ymax, const MethodInfo* method)
{
	{
		float L_0 = ___xmin;
		float L_1 = ___ymin;
		float L_2 = ___xmax;
		float L_3 = ___xmin;
		float L_4 = ___ymax;
		float L_5 = ___ymin;
		Rect_t6_51  L_6 = {0};
		Rect__ctor_m6_295(&L_6, L_0, L_1, ((float)((float)L_2-(float)L_3)), ((float)((float)L_4-(float)L_5)), /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Single UnityEngine.Rect::get_x()
extern "C" float Rect_get_x_m6_298 (Rect_t6_51 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_XMin_0);
		return L_0;
	}
}
// System.Void UnityEngine.Rect::set_x(System.Single)
extern "C" void Rect_set_x_m6_299 (Rect_t6_51 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_XMin_0 = L_0;
		return;
	}
}
// System.Single UnityEngine.Rect::get_y()
extern "C" float Rect_get_y_m6_300 (Rect_t6_51 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_YMin_1);
		return L_0;
	}
}
// System.Void UnityEngine.Rect::set_y(System.Single)
extern "C" void Rect_set_y_m6_301 (Rect_t6_51 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_YMin_1 = L_0;
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.Rect::get_position()
extern "C" Vector2_t6_47  Rect_get_position_m6_302 (Rect_t6_51 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_XMin_0);
		float L_1 = (__this->___m_YMin_1);
		Vector2_t6_47  L_2 = {0};
		Vector2__ctor_m6_215(&L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Vector2 UnityEngine.Rect::get_center()
extern "C" Vector2_t6_47  Rect_get_center_m6_303 (Rect_t6_51 * __this, const MethodInfo* method)
{
	{
		float L_0 = Rect_get_x_m6_298(__this, /*hidden argument*/NULL);
		float L_1 = (__this->___m_Width_2);
		float L_2 = Rect_get_y_m6_300(__this, /*hidden argument*/NULL);
		float L_3 = (__this->___m_Height_3);
		Vector2_t6_47  L_4 = {0};
		Vector2__ctor_m6_215(&L_4, ((float)((float)L_0+(float)((float)((float)L_1/(float)(2.0f))))), ((float)((float)L_2+(float)((float)((float)L_3/(float)(2.0f))))), /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Vector2 UnityEngine.Rect::get_min()
extern "C" Vector2_t6_47  Rect_get_min_m6_304 (Rect_t6_51 * __this, const MethodInfo* method)
{
	{
		float L_0 = Rect_get_xMin_m6_311(__this, /*hidden argument*/NULL);
		float L_1 = Rect_get_yMin_m6_313(__this, /*hidden argument*/NULL);
		Vector2_t6_47  L_2 = {0};
		Vector2__ctor_m6_215(&L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Vector2 UnityEngine.Rect::get_max()
extern "C" Vector2_t6_47  Rect_get_max_m6_305 (Rect_t6_51 * __this, const MethodInfo* method)
{
	{
		float L_0 = Rect_get_xMax_m6_315(__this, /*hidden argument*/NULL);
		float L_1 = Rect_get_yMax_m6_317(__this, /*hidden argument*/NULL);
		Vector2_t6_47  L_2 = {0};
		Vector2__ctor_m6_215(&L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Single UnityEngine.Rect::get_width()
extern "C" float Rect_get_width_m6_306 (Rect_t6_51 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Width_2);
		return L_0;
	}
}
// System.Void UnityEngine.Rect::set_width(System.Single)
extern "C" void Rect_set_width_m6_307 (Rect_t6_51 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_Width_2 = L_0;
		return;
	}
}
// System.Single UnityEngine.Rect::get_height()
extern "C" float Rect_get_height_m6_308 (Rect_t6_51 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Height_3);
		return L_0;
	}
}
// System.Void UnityEngine.Rect::set_height(System.Single)
extern "C" void Rect_set_height_m6_309 (Rect_t6_51 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_Height_3 = L_0;
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.Rect::get_size()
extern "C" Vector2_t6_47  Rect_get_size_m6_310 (Rect_t6_51 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Width_2);
		float L_1 = (__this->___m_Height_3);
		Vector2_t6_47  L_2 = {0};
		Vector2__ctor_m6_215(&L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Single UnityEngine.Rect::get_xMin()
extern "C" float Rect_get_xMin_m6_311 (Rect_t6_51 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_XMin_0);
		return L_0;
	}
}
// System.Void UnityEngine.Rect::set_xMin(System.Single)
extern "C" void Rect_set_xMin_m6_312 (Rect_t6_51 * __this, float ___value, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = Rect_get_xMax_m6_315(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = ___value;
		__this->___m_XMin_0 = L_1;
		float L_2 = V_0;
		float L_3 = (__this->___m_XMin_0);
		__this->___m_Width_2 = ((float)((float)L_2-(float)L_3));
		return;
	}
}
// System.Single UnityEngine.Rect::get_yMin()
extern "C" float Rect_get_yMin_m6_313 (Rect_t6_51 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_YMin_1);
		return L_0;
	}
}
// System.Void UnityEngine.Rect::set_yMin(System.Single)
extern "C" void Rect_set_yMin_m6_314 (Rect_t6_51 * __this, float ___value, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = Rect_get_yMax_m6_317(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = ___value;
		__this->___m_YMin_1 = L_1;
		float L_2 = V_0;
		float L_3 = (__this->___m_YMin_1);
		__this->___m_Height_3 = ((float)((float)L_2-(float)L_3));
		return;
	}
}
// System.Single UnityEngine.Rect::get_xMax()
extern "C" float Rect_get_xMax_m6_315 (Rect_t6_51 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Width_2);
		float L_1 = (__this->___m_XMin_0);
		return ((float)((float)L_0+(float)L_1));
	}
}
// System.Void UnityEngine.Rect::set_xMax(System.Single)
extern "C" void Rect_set_xMax_m6_316 (Rect_t6_51 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		float L_1 = (__this->___m_XMin_0);
		__this->___m_Width_2 = ((float)((float)L_0-(float)L_1));
		return;
	}
}
// System.Single UnityEngine.Rect::get_yMax()
extern "C" float Rect_get_yMax_m6_317 (Rect_t6_51 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Height_3);
		float L_1 = (__this->___m_YMin_1);
		return ((float)((float)L_0+(float)L_1));
	}
}
// System.Void UnityEngine.Rect::set_yMax(System.Single)
extern "C" void Rect_set_yMax_m6_318 (Rect_t6_51 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		float L_1 = (__this->___m_YMin_1);
		__this->___m_Height_3 = ((float)((float)L_0-(float)L_1));
		return;
	}
}
// System.String UnityEngine.Rect::ToString()
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t1_17_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4702;
extern "C" String_t* Rect_ToString_m6_319 (Rect_t6_51 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Single_t1_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		_stringLiteral4702 = il2cpp_codegen_string_literal_from_index(4702);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_272* L_0 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 4));
		float L_1 = Rect_get_x_m6_298(__this, /*hidden argument*/NULL);
		float L_2 = L_1;
		Object_t * L_3 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_3;
		ObjectU5BU5D_t1_272* L_4 = L_0;
		float L_5 = Rect_get_y_m6_300(__this, /*hidden argument*/NULL);
		float L_6 = L_5;
		Object_t * L_7 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1, sizeof(Object_t *))) = (Object_t *)L_7;
		ObjectU5BU5D_t1_272* L_8 = L_4;
		float L_9 = Rect_get_width_m6_306(__this, /*hidden argument*/NULL);
		float L_10 = L_9;
		Object_t * L_11 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 2, sizeof(Object_t *))) = (Object_t *)L_11;
		ObjectU5BU5D_t1_272* L_12 = L_8;
		float L_13 = Rect_get_height_m6_308(__this, /*hidden argument*/NULL);
		float L_14 = L_13;
		Object_t * L_15 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 3, sizeof(Object_t *))) = (Object_t *)L_15;
		String_t* L_16 = UnityString_Format_m6_573(NULL /*static, unused*/, _stringLiteral4702, L_12, /*hidden argument*/NULL);
		return L_16;
	}
}
// System.Boolean UnityEngine.Rect::Contains(UnityEngine.Vector2)
extern "C" bool Rect_Contains_m6_320 (Rect_t6_51 * __this, Vector2_t6_47  ___point, const MethodInfo* method)
{
	int32_t G_B5_0 = 0;
	{
		float L_0 = ((&___point)->___x_1);
		float L_1 = Rect_get_xMin_m6_311(__this, /*hidden argument*/NULL);
		if ((!(((float)L_0) >= ((float)L_1))))
		{
			goto IL_0047;
		}
	}
	{
		float L_2 = ((&___point)->___x_1);
		float L_3 = Rect_get_xMax_m6_315(__this, /*hidden argument*/NULL);
		if ((!(((float)L_2) < ((float)L_3))))
		{
			goto IL_0047;
		}
	}
	{
		float L_4 = ((&___point)->___y_2);
		float L_5 = Rect_get_yMin_m6_313(__this, /*hidden argument*/NULL);
		if ((!(((float)L_4) >= ((float)L_5))))
		{
			goto IL_0047;
		}
	}
	{
		float L_6 = ((&___point)->___y_2);
		float L_7 = Rect_get_yMax_m6_317(__this, /*hidden argument*/NULL);
		G_B5_0 = ((((float)L_6) < ((float)L_7))? 1 : 0);
		goto IL_0048;
	}

IL_0047:
	{
		G_B5_0 = 0;
	}

IL_0048:
	{
		return G_B5_0;
	}
}
// System.Boolean UnityEngine.Rect::Contains(UnityEngine.Vector3)
extern "C" bool Rect_Contains_m6_321 (Rect_t6_51 * __this, Vector3_t6_48  ___point, const MethodInfo* method)
{
	int32_t G_B5_0 = 0;
	{
		float L_0 = ((&___point)->___x_1);
		float L_1 = Rect_get_xMin_m6_311(__this, /*hidden argument*/NULL);
		if ((!(((float)L_0) >= ((float)L_1))))
		{
			goto IL_0047;
		}
	}
	{
		float L_2 = ((&___point)->___x_1);
		float L_3 = Rect_get_xMax_m6_315(__this, /*hidden argument*/NULL);
		if ((!(((float)L_2) < ((float)L_3))))
		{
			goto IL_0047;
		}
	}
	{
		float L_4 = ((&___point)->___y_2);
		float L_5 = Rect_get_yMin_m6_313(__this, /*hidden argument*/NULL);
		if ((!(((float)L_4) >= ((float)L_5))))
		{
			goto IL_0047;
		}
	}
	{
		float L_6 = ((&___point)->___y_2);
		float L_7 = Rect_get_yMax_m6_317(__this, /*hidden argument*/NULL);
		G_B5_0 = ((((float)L_6) < ((float)L_7))? 1 : 0);
		goto IL_0048;
	}

IL_0047:
	{
		G_B5_0 = 0;
	}

IL_0048:
	{
		return G_B5_0;
	}
}
// System.Boolean UnityEngine.Rect::Overlaps(UnityEngine.Rect)
extern "C" bool Rect_Overlaps_m6_322 (Rect_t6_51 * __this, Rect_t6_51  ___other, const MethodInfo* method)
{
	int32_t G_B5_0 = 0;
	{
		float L_0 = Rect_get_xMax_m6_315((&___other), /*hidden argument*/NULL);
		float L_1 = Rect_get_xMin_m6_311(__this, /*hidden argument*/NULL);
		if ((!(((float)L_0) > ((float)L_1))))
		{
			goto IL_0047;
		}
	}
	{
		float L_2 = Rect_get_xMin_m6_311((&___other), /*hidden argument*/NULL);
		float L_3 = Rect_get_xMax_m6_315(__this, /*hidden argument*/NULL);
		if ((!(((float)L_2) < ((float)L_3))))
		{
			goto IL_0047;
		}
	}
	{
		float L_4 = Rect_get_yMax_m6_317((&___other), /*hidden argument*/NULL);
		float L_5 = Rect_get_yMin_m6_313(__this, /*hidden argument*/NULL);
		if ((!(((float)L_4) > ((float)L_5))))
		{
			goto IL_0047;
		}
	}
	{
		float L_6 = Rect_get_yMin_m6_313((&___other), /*hidden argument*/NULL);
		float L_7 = Rect_get_yMax_m6_317(__this, /*hidden argument*/NULL);
		G_B5_0 = ((((float)L_6) < ((float)L_7))? 1 : 0);
		goto IL_0048;
	}

IL_0047:
	{
		G_B5_0 = 0;
	}

IL_0048:
	{
		return G_B5_0;
	}
}
// System.Int32 UnityEngine.Rect::GetHashCode()
extern "C" int32_t Rect_GetHashCode_m6_323 (Rect_t6_51 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	{
		float L_0 = Rect_get_x_m6_298(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Single_GetHashCode_m1_622((&V_0), /*hidden argument*/NULL);
		float L_2 = Rect_get_width_m6_306(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = Single_GetHashCode_m1_622((&V_1), /*hidden argument*/NULL);
		float L_4 = Rect_get_y_m6_300(__this, /*hidden argument*/NULL);
		V_2 = L_4;
		int32_t L_5 = Single_GetHashCode_m1_622((&V_2), /*hidden argument*/NULL);
		float L_6 = Rect_get_height_m6_308(__this, /*hidden argument*/NULL);
		V_3 = L_6;
		int32_t L_7 = Single_GetHashCode_m1_622((&V_3), /*hidden argument*/NULL);
		return ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)1))));
	}
}
// System.Boolean UnityEngine.Rect::Equals(System.Object)
extern TypeInfo* Rect_t6_51_il2cpp_TypeInfo_var;
extern "C" bool Rect_Equals_m6_324 (Rect_t6_51 * __this, Object_t * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Rect_t6_51_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1593);
		s_Il2CppMethodIntialized = true;
	}
	Rect_t6_51  V_0 = {0};
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	int32_t G_B7_0 = 0;
	{
		Object_t * L_0 = ___other;
		if (((Object_t *)IsInstSealed(L_0, Rect_t6_51_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		Object_t * L_1 = ___other;
		V_0 = ((*(Rect_t6_51 *)((Rect_t6_51 *)UnBox (L_1, Rect_t6_51_il2cpp_TypeInfo_var))));
		float L_2 = Rect_get_x_m6_298(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		float L_3 = Rect_get_x_m6_298((&V_0), /*hidden argument*/NULL);
		bool L_4 = Single_Equals_m1_621((&V_1), L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_007a;
		}
	}
	{
		float L_5 = Rect_get_y_m6_300(__this, /*hidden argument*/NULL);
		V_2 = L_5;
		float L_6 = Rect_get_y_m6_300((&V_0), /*hidden argument*/NULL);
		bool L_7 = Single_Equals_m1_621((&V_2), L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_007a;
		}
	}
	{
		float L_8 = Rect_get_width_m6_306(__this, /*hidden argument*/NULL);
		V_3 = L_8;
		float L_9 = Rect_get_width_m6_306((&V_0), /*hidden argument*/NULL);
		bool L_10 = Single_Equals_m1_621((&V_3), L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_007a;
		}
	}
	{
		float L_11 = Rect_get_height_m6_308(__this, /*hidden argument*/NULL);
		V_4 = L_11;
		float L_12 = Rect_get_height_m6_308((&V_0), /*hidden argument*/NULL);
		bool L_13 = Single_Equals_m1_621((&V_4), L_12, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_13));
		goto IL_007b;
	}

IL_007a:
	{
		G_B7_0 = 0;
	}

IL_007b:
	{
		return G_B7_0;
	}
}
// System.Boolean UnityEngine.Rect::op_Inequality(UnityEngine.Rect,UnityEngine.Rect)
extern "C" bool Rect_op_Inequality_m6_325 (Object_t * __this /* static, unused */, Rect_t6_51  ___lhs, Rect_t6_51  ___rhs, const MethodInfo* method)
{
	int32_t G_B5_0 = 0;
	{
		float L_0 = Rect_get_x_m6_298((&___lhs), /*hidden argument*/NULL);
		float L_1 = Rect_get_x_m6_298((&___rhs), /*hidden argument*/NULL);
		if ((!(((float)L_0) == ((float)L_1))))
		{
			goto IL_004e;
		}
	}
	{
		float L_2 = Rect_get_y_m6_300((&___lhs), /*hidden argument*/NULL);
		float L_3 = Rect_get_y_m6_300((&___rhs), /*hidden argument*/NULL);
		if ((!(((float)L_2) == ((float)L_3))))
		{
			goto IL_004e;
		}
	}
	{
		float L_4 = Rect_get_width_m6_306((&___lhs), /*hidden argument*/NULL);
		float L_5 = Rect_get_width_m6_306((&___rhs), /*hidden argument*/NULL);
		if ((!(((float)L_4) == ((float)L_5))))
		{
			goto IL_004e;
		}
	}
	{
		float L_6 = Rect_get_height_m6_308((&___lhs), /*hidden argument*/NULL);
		float L_7 = Rect_get_height_m6_308((&___rhs), /*hidden argument*/NULL);
		G_B5_0 = ((((int32_t)((((float)L_6) == ((float)L_7))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_004f;
	}

IL_004e:
	{
		G_B5_0 = 1;
	}

IL_004f:
	{
		return G_B5_0;
	}
}
// System.Boolean UnityEngine.Rect::op_Equality(UnityEngine.Rect,UnityEngine.Rect)
extern "C" bool Rect_op_Equality_m6_326 (Object_t * __this /* static, unused */, Rect_t6_51  ___lhs, Rect_t6_51  ___rhs, const MethodInfo* method)
{
	int32_t G_B5_0 = 0;
	{
		float L_0 = Rect_get_x_m6_298((&___lhs), /*hidden argument*/NULL);
		float L_1 = Rect_get_x_m6_298((&___rhs), /*hidden argument*/NULL);
		if ((!(((float)L_0) == ((float)L_1))))
		{
			goto IL_004b;
		}
	}
	{
		float L_2 = Rect_get_y_m6_300((&___lhs), /*hidden argument*/NULL);
		float L_3 = Rect_get_y_m6_300((&___rhs), /*hidden argument*/NULL);
		if ((!(((float)L_2) == ((float)L_3))))
		{
			goto IL_004b;
		}
	}
	{
		float L_4 = Rect_get_width_m6_306((&___lhs), /*hidden argument*/NULL);
		float L_5 = Rect_get_width_m6_306((&___rhs), /*hidden argument*/NULL);
		if ((!(((float)L_4) == ((float)L_5))))
		{
			goto IL_004b;
		}
	}
	{
		float L_6 = Rect_get_height_m6_308((&___lhs), /*hidden argument*/NULL);
		float L_7 = Rect_get_height_m6_308((&___rhs), /*hidden argument*/NULL);
		G_B5_0 = ((((float)L_6) == ((float)L_7))? 1 : 0);
		goto IL_004c;
	}

IL_004b:
	{
		G_B5_0 = 0;
	}

IL_004c:
	{
		return G_B5_0;
	}
}
// System.Single UnityEngine.Matrix4x4::get_Item(System.Int32,System.Int32)
extern "C" float Matrix4x4_get_Item_m6_327 (Matrix4x4_t6_52 * __this, int32_t ___row, int32_t ___column, const MethodInfo* method)
{
	{
		int32_t L_0 = ___row;
		int32_t L_1 = ___column;
		float L_2 = Matrix4x4_get_Item_m6_329(__this, ((int32_t)((int32_t)L_0+(int32_t)((int32_t)((int32_t)L_1*(int32_t)4)))), /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.Matrix4x4::set_Item(System.Int32,System.Int32,System.Single)
extern "C" void Matrix4x4_set_Item_m6_328 (Matrix4x4_t6_52 * __this, int32_t ___row, int32_t ___column, float ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___row;
		int32_t L_1 = ___column;
		float L_2 = ___value;
		Matrix4x4_set_Item_m6_330(__this, ((int32_t)((int32_t)L_0+(int32_t)((int32_t)((int32_t)L_1*(int32_t)4)))), L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.Matrix4x4::get_Item(System.Int32)
extern TypeInfo* IndexOutOfRangeException_t1_1555_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4703;
extern "C" float Matrix4x4_get_Item_m6_329 (Matrix4x4_t6_52 * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IndexOutOfRangeException_t1_1555_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		_stringLiteral4703 = il2cpp_codegen_string_literal_from_index(4703);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_004d;
		}
		if (L_1 == 1)
		{
			goto IL_0054;
		}
		if (L_1 == 2)
		{
			goto IL_005b;
		}
		if (L_1 == 3)
		{
			goto IL_0062;
		}
		if (L_1 == 4)
		{
			goto IL_0069;
		}
		if (L_1 == 5)
		{
			goto IL_0070;
		}
		if (L_1 == 6)
		{
			goto IL_0077;
		}
		if (L_1 == 7)
		{
			goto IL_007e;
		}
		if (L_1 == 8)
		{
			goto IL_0085;
		}
		if (L_1 == 9)
		{
			goto IL_008c;
		}
		if (L_1 == 10)
		{
			goto IL_0093;
		}
		if (L_1 == 11)
		{
			goto IL_009a;
		}
		if (L_1 == 12)
		{
			goto IL_00a1;
		}
		if (L_1 == 13)
		{
			goto IL_00a8;
		}
		if (L_1 == 14)
		{
			goto IL_00af;
		}
		if (L_1 == 15)
		{
			goto IL_00b6;
		}
	}
	{
		goto IL_00bd;
	}

IL_004d:
	{
		float L_2 = (__this->___m00_0);
		return L_2;
	}

IL_0054:
	{
		float L_3 = (__this->___m10_1);
		return L_3;
	}

IL_005b:
	{
		float L_4 = (__this->___m20_2);
		return L_4;
	}

IL_0062:
	{
		float L_5 = (__this->___m30_3);
		return L_5;
	}

IL_0069:
	{
		float L_6 = (__this->___m01_4);
		return L_6;
	}

IL_0070:
	{
		float L_7 = (__this->___m11_5);
		return L_7;
	}

IL_0077:
	{
		float L_8 = (__this->___m21_6);
		return L_8;
	}

IL_007e:
	{
		float L_9 = (__this->___m31_7);
		return L_9;
	}

IL_0085:
	{
		float L_10 = (__this->___m02_8);
		return L_10;
	}

IL_008c:
	{
		float L_11 = (__this->___m12_9);
		return L_11;
	}

IL_0093:
	{
		float L_12 = (__this->___m22_10);
		return L_12;
	}

IL_009a:
	{
		float L_13 = (__this->___m32_11);
		return L_13;
	}

IL_00a1:
	{
		float L_14 = (__this->___m03_12);
		return L_14;
	}

IL_00a8:
	{
		float L_15 = (__this->___m13_13);
		return L_15;
	}

IL_00af:
	{
		float L_16 = (__this->___m23_14);
		return L_16;
	}

IL_00b6:
	{
		float L_17 = (__this->___m33_15);
		return L_17;
	}

IL_00bd:
	{
		IndexOutOfRangeException_t1_1555 * L_18 = (IndexOutOfRangeException_t1_1555 *)il2cpp_codegen_object_new (IndexOutOfRangeException_t1_1555_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m1_14159(L_18, _stringLiteral4703, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_18);
	}
}
// System.Void UnityEngine.Matrix4x4::set_Item(System.Int32,System.Single)
extern TypeInfo* IndexOutOfRangeException_t1_1555_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4703;
extern "C" void Matrix4x4_set_Item_m6_330 (Matrix4x4_t6_52 * __this, int32_t ___index, float ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IndexOutOfRangeException_t1_1555_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		_stringLiteral4703 = il2cpp_codegen_string_literal_from_index(4703);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_004d;
		}
		if (L_1 == 1)
		{
			goto IL_0059;
		}
		if (L_1 == 2)
		{
			goto IL_0065;
		}
		if (L_1 == 3)
		{
			goto IL_0071;
		}
		if (L_1 == 4)
		{
			goto IL_007d;
		}
		if (L_1 == 5)
		{
			goto IL_0089;
		}
		if (L_1 == 6)
		{
			goto IL_0095;
		}
		if (L_1 == 7)
		{
			goto IL_00a1;
		}
		if (L_1 == 8)
		{
			goto IL_00ad;
		}
		if (L_1 == 9)
		{
			goto IL_00b9;
		}
		if (L_1 == 10)
		{
			goto IL_00c5;
		}
		if (L_1 == 11)
		{
			goto IL_00d1;
		}
		if (L_1 == 12)
		{
			goto IL_00dd;
		}
		if (L_1 == 13)
		{
			goto IL_00e9;
		}
		if (L_1 == 14)
		{
			goto IL_00f5;
		}
		if (L_1 == 15)
		{
			goto IL_0101;
		}
	}
	{
		goto IL_010d;
	}

IL_004d:
	{
		float L_2 = ___value;
		__this->___m00_0 = L_2;
		goto IL_0118;
	}

IL_0059:
	{
		float L_3 = ___value;
		__this->___m10_1 = L_3;
		goto IL_0118;
	}

IL_0065:
	{
		float L_4 = ___value;
		__this->___m20_2 = L_4;
		goto IL_0118;
	}

IL_0071:
	{
		float L_5 = ___value;
		__this->___m30_3 = L_5;
		goto IL_0118;
	}

IL_007d:
	{
		float L_6 = ___value;
		__this->___m01_4 = L_6;
		goto IL_0118;
	}

IL_0089:
	{
		float L_7 = ___value;
		__this->___m11_5 = L_7;
		goto IL_0118;
	}

IL_0095:
	{
		float L_8 = ___value;
		__this->___m21_6 = L_8;
		goto IL_0118;
	}

IL_00a1:
	{
		float L_9 = ___value;
		__this->___m31_7 = L_9;
		goto IL_0118;
	}

IL_00ad:
	{
		float L_10 = ___value;
		__this->___m02_8 = L_10;
		goto IL_0118;
	}

IL_00b9:
	{
		float L_11 = ___value;
		__this->___m12_9 = L_11;
		goto IL_0118;
	}

IL_00c5:
	{
		float L_12 = ___value;
		__this->___m22_10 = L_12;
		goto IL_0118;
	}

IL_00d1:
	{
		float L_13 = ___value;
		__this->___m32_11 = L_13;
		goto IL_0118;
	}

IL_00dd:
	{
		float L_14 = ___value;
		__this->___m03_12 = L_14;
		goto IL_0118;
	}

IL_00e9:
	{
		float L_15 = ___value;
		__this->___m13_13 = L_15;
		goto IL_0118;
	}

IL_00f5:
	{
		float L_16 = ___value;
		__this->___m23_14 = L_16;
		goto IL_0118;
	}

IL_0101:
	{
		float L_17 = ___value;
		__this->___m33_15 = L_17;
		goto IL_0118;
	}

IL_010d:
	{
		IndexOutOfRangeException_t1_1555 * L_18 = (IndexOutOfRangeException_t1_1555 *)il2cpp_codegen_object_new (IndexOutOfRangeException_t1_1555_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m1_14159(L_18, _stringLiteral4703, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_18);
	}

IL_0118:
	{
		return;
	}
}
// System.Int32 UnityEngine.Matrix4x4::GetHashCode()
extern "C" int32_t Matrix4x4_GetHashCode_m6_331 (Matrix4x4_t6_52 * __this, const MethodInfo* method)
{
	Vector4_t6_54  V_0 = {0};
	Vector4_t6_54  V_1 = {0};
	Vector4_t6_54  V_2 = {0};
	Vector4_t6_54  V_3 = {0};
	{
		Vector4_t6_54  L_0 = Matrix4x4_GetColumn_m6_342(__this, 0, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Vector4_GetHashCode_m6_402((&V_0), /*hidden argument*/NULL);
		Vector4_t6_54  L_2 = Matrix4x4_GetColumn_m6_342(__this, 1, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = Vector4_GetHashCode_m6_402((&V_1), /*hidden argument*/NULL);
		Vector4_t6_54  L_4 = Matrix4x4_GetColumn_m6_342(__this, 2, /*hidden argument*/NULL);
		V_2 = L_4;
		int32_t L_5 = Vector4_GetHashCode_m6_402((&V_2), /*hidden argument*/NULL);
		Vector4_t6_54  L_6 = Matrix4x4_GetColumn_m6_342(__this, 3, /*hidden argument*/NULL);
		V_3 = L_6;
		int32_t L_7 = Vector4_GetHashCode_m6_402((&V_3), /*hidden argument*/NULL);
		return ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)1))));
	}
}
// System.Boolean UnityEngine.Matrix4x4::Equals(System.Object)
extern TypeInfo* Matrix4x4_t6_52_il2cpp_TypeInfo_var;
extern TypeInfo* Vector4_t6_54_il2cpp_TypeInfo_var;
extern "C" bool Matrix4x4_Equals_m6_332 (Matrix4x4_t6_52 * __this, Object_t * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Matrix4x4_t6_52_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1594);
		Vector4_t6_54_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1583);
		s_Il2CppMethodIntialized = true;
	}
	Matrix4x4_t6_52  V_0 = {0};
	Vector4_t6_54  V_1 = {0};
	Vector4_t6_54  V_2 = {0};
	Vector4_t6_54  V_3 = {0};
	Vector4_t6_54  V_4 = {0};
	int32_t G_B7_0 = 0;
	{
		Object_t * L_0 = ___other;
		if (((Object_t *)IsInstSealed(L_0, Matrix4x4_t6_52_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		Object_t * L_1 = ___other;
		V_0 = ((*(Matrix4x4_t6_52 *)((Matrix4x4_t6_52 *)UnBox (L_1, Matrix4x4_t6_52_il2cpp_TypeInfo_var))));
		Vector4_t6_54  L_2 = Matrix4x4_GetColumn_m6_342(__this, 0, /*hidden argument*/NULL);
		V_1 = L_2;
		Vector4_t6_54  L_3 = Matrix4x4_GetColumn_m6_342((&V_0), 0, /*hidden argument*/NULL);
		Vector4_t6_54  L_4 = L_3;
		Object_t * L_5 = Box(Vector4_t6_54_il2cpp_TypeInfo_var, &L_4);
		bool L_6 = Vector4_Equals_m6_403((&V_1), L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0096;
		}
	}
	{
		Vector4_t6_54  L_7 = Matrix4x4_GetColumn_m6_342(__this, 1, /*hidden argument*/NULL);
		V_2 = L_7;
		Vector4_t6_54  L_8 = Matrix4x4_GetColumn_m6_342((&V_0), 1, /*hidden argument*/NULL);
		Vector4_t6_54  L_9 = L_8;
		Object_t * L_10 = Box(Vector4_t6_54_il2cpp_TypeInfo_var, &L_9);
		bool L_11 = Vector4_Equals_m6_403((&V_2), L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0096;
		}
	}
	{
		Vector4_t6_54  L_12 = Matrix4x4_GetColumn_m6_342(__this, 2, /*hidden argument*/NULL);
		V_3 = L_12;
		Vector4_t6_54  L_13 = Matrix4x4_GetColumn_m6_342((&V_0), 2, /*hidden argument*/NULL);
		Vector4_t6_54  L_14 = L_13;
		Object_t * L_15 = Box(Vector4_t6_54_il2cpp_TypeInfo_var, &L_14);
		bool L_16 = Vector4_Equals_m6_403((&V_3), L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_0096;
		}
	}
	{
		Vector4_t6_54  L_17 = Matrix4x4_GetColumn_m6_342(__this, 3, /*hidden argument*/NULL);
		V_4 = L_17;
		Vector4_t6_54  L_18 = Matrix4x4_GetColumn_m6_342((&V_0), 3, /*hidden argument*/NULL);
		Vector4_t6_54  L_19 = L_18;
		Object_t * L_20 = Box(Vector4_t6_54_il2cpp_TypeInfo_var, &L_19);
		bool L_21 = Vector4_Equals_m6_403((&V_4), L_20, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_21));
		goto IL_0097;
	}

IL_0096:
	{
		G_B7_0 = 0;
	}

IL_0097:
	{
		return G_B7_0;
	}
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::Inverse(UnityEngine.Matrix4x4)
extern "C" Matrix4x4_t6_52  Matrix4x4_Inverse_m6_333 (Object_t * __this /* static, unused */, Matrix4x4_t6_52  ___m, const MethodInfo* method)
{
	{
		Matrix4x4_t6_52  L_0 = Matrix4x4_INTERNAL_CALL_Inverse_m6_334(NULL /*static, unused*/, (&___m), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::INTERNAL_CALL_Inverse(UnityEngine.Matrix4x4&)
extern "C" Matrix4x4_t6_52  Matrix4x4_INTERNAL_CALL_Inverse_m6_334 (Object_t * __this /* static, unused */, Matrix4x4_t6_52 * ___m, const MethodInfo* method)
{
	typedef Matrix4x4_t6_52  (*Matrix4x4_INTERNAL_CALL_Inverse_m6_334_ftn) (Matrix4x4_t6_52 *);
	static Matrix4x4_INTERNAL_CALL_Inverse_m6_334_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Matrix4x4_INTERNAL_CALL_Inverse_m6_334_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Matrix4x4::INTERNAL_CALL_Inverse(UnityEngine.Matrix4x4&)");
	return _il2cpp_icall_func(___m);
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::Transpose(UnityEngine.Matrix4x4)
extern "C" Matrix4x4_t6_52  Matrix4x4_Transpose_m6_335 (Object_t * __this /* static, unused */, Matrix4x4_t6_52  ___m, const MethodInfo* method)
{
	{
		Matrix4x4_t6_52  L_0 = Matrix4x4_INTERNAL_CALL_Transpose_m6_336(NULL /*static, unused*/, (&___m), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::INTERNAL_CALL_Transpose(UnityEngine.Matrix4x4&)
extern "C" Matrix4x4_t6_52  Matrix4x4_INTERNAL_CALL_Transpose_m6_336 (Object_t * __this /* static, unused */, Matrix4x4_t6_52 * ___m, const MethodInfo* method)
{
	typedef Matrix4x4_t6_52  (*Matrix4x4_INTERNAL_CALL_Transpose_m6_336_ftn) (Matrix4x4_t6_52 *);
	static Matrix4x4_INTERNAL_CALL_Transpose_m6_336_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Matrix4x4_INTERNAL_CALL_Transpose_m6_336_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Matrix4x4::INTERNAL_CALL_Transpose(UnityEngine.Matrix4x4&)");
	return _il2cpp_icall_func(___m);
}
// System.Boolean UnityEngine.Matrix4x4::Invert(UnityEngine.Matrix4x4,UnityEngine.Matrix4x4&)
extern "C" bool Matrix4x4_Invert_m6_337 (Object_t * __this /* static, unused */, Matrix4x4_t6_52  ___inMatrix, Matrix4x4_t6_52 * ___dest, const MethodInfo* method)
{
	{
		Matrix4x4_t6_52 * L_0 = ___dest;
		bool L_1 = Matrix4x4_INTERNAL_CALL_Invert_m6_338(NULL /*static, unused*/, (&___inMatrix), L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityEngine.Matrix4x4::INTERNAL_CALL_Invert(UnityEngine.Matrix4x4&,UnityEngine.Matrix4x4&)
extern "C" bool Matrix4x4_INTERNAL_CALL_Invert_m6_338 (Object_t * __this /* static, unused */, Matrix4x4_t6_52 * ___inMatrix, Matrix4x4_t6_52 * ___dest, const MethodInfo* method)
{
	typedef bool (*Matrix4x4_INTERNAL_CALL_Invert_m6_338_ftn) (Matrix4x4_t6_52 *, Matrix4x4_t6_52 *);
	static Matrix4x4_INTERNAL_CALL_Invert_m6_338_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Matrix4x4_INTERNAL_CALL_Invert_m6_338_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Matrix4x4::INTERNAL_CALL_Invert(UnityEngine.Matrix4x4&,UnityEngine.Matrix4x4&)");
	return _il2cpp_icall_func(___inMatrix, ___dest);
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::get_inverse()
extern "C" Matrix4x4_t6_52  Matrix4x4_get_inverse_m6_339 (Matrix4x4_t6_52 * __this, const MethodInfo* method)
{
	{
		Matrix4x4_t6_52  L_0 = Matrix4x4_Inverse_m6_333(NULL /*static, unused*/, (*(Matrix4x4_t6_52 *)__this), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::get_transpose()
extern "C" Matrix4x4_t6_52  Matrix4x4_get_transpose_m6_340 (Matrix4x4_t6_52 * __this, const MethodInfo* method)
{
	{
		Matrix4x4_t6_52  L_0 = Matrix4x4_Transpose_m6_335(NULL /*static, unused*/, (*(Matrix4x4_t6_52 *)__this), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityEngine.Matrix4x4::get_isIdentity()
extern "C" bool Matrix4x4_get_isIdentity_m6_341 (Matrix4x4_t6_52 * __this, const MethodInfo* method)
{
	typedef bool (*Matrix4x4_get_isIdentity_m6_341_ftn) (Matrix4x4_t6_52 *);
	static Matrix4x4_get_isIdentity_m6_341_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Matrix4x4_get_isIdentity_m6_341_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Matrix4x4::get_isIdentity()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Vector4 UnityEngine.Matrix4x4::GetColumn(System.Int32)
extern "C" Vector4_t6_54  Matrix4x4_GetColumn_m6_342 (Matrix4x4_t6_52 * __this, int32_t ___i, const MethodInfo* method)
{
	{
		int32_t L_0 = ___i;
		float L_1 = Matrix4x4_get_Item_m6_327(__this, 0, L_0, /*hidden argument*/NULL);
		int32_t L_2 = ___i;
		float L_3 = Matrix4x4_get_Item_m6_327(__this, 1, L_2, /*hidden argument*/NULL);
		int32_t L_4 = ___i;
		float L_5 = Matrix4x4_get_Item_m6_327(__this, 2, L_4, /*hidden argument*/NULL);
		int32_t L_6 = ___i;
		float L_7 = Matrix4x4_get_Item_m6_327(__this, 3, L_6, /*hidden argument*/NULL);
		Vector4_t6_54  L_8 = {0};
		Vector4__ctor_m6_399(&L_8, L_1, L_3, L_5, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// UnityEngine.Vector4 UnityEngine.Matrix4x4::GetRow(System.Int32)
extern "C" Vector4_t6_54  Matrix4x4_GetRow_m6_343 (Matrix4x4_t6_52 * __this, int32_t ___i, const MethodInfo* method)
{
	{
		int32_t L_0 = ___i;
		float L_1 = Matrix4x4_get_Item_m6_327(__this, L_0, 0, /*hidden argument*/NULL);
		int32_t L_2 = ___i;
		float L_3 = Matrix4x4_get_Item_m6_327(__this, L_2, 1, /*hidden argument*/NULL);
		int32_t L_4 = ___i;
		float L_5 = Matrix4x4_get_Item_m6_327(__this, L_4, 2, /*hidden argument*/NULL);
		int32_t L_6 = ___i;
		float L_7 = Matrix4x4_get_Item_m6_327(__this, L_6, 3, /*hidden argument*/NULL);
		Vector4_t6_54  L_8 = {0};
		Vector4__ctor_m6_399(&L_8, L_1, L_3, L_5, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Void UnityEngine.Matrix4x4::SetColumn(System.Int32,UnityEngine.Vector4)
extern "C" void Matrix4x4_SetColumn_m6_344 (Matrix4x4_t6_52 * __this, int32_t ___i, Vector4_t6_54  ___v, const MethodInfo* method)
{
	{
		int32_t L_0 = ___i;
		float L_1 = ((&___v)->___x_1);
		Matrix4x4_set_Item_m6_328(__this, 0, L_0, L_1, /*hidden argument*/NULL);
		int32_t L_2 = ___i;
		float L_3 = ((&___v)->___y_2);
		Matrix4x4_set_Item_m6_328(__this, 1, L_2, L_3, /*hidden argument*/NULL);
		int32_t L_4 = ___i;
		float L_5 = ((&___v)->___z_3);
		Matrix4x4_set_Item_m6_328(__this, 2, L_4, L_5, /*hidden argument*/NULL);
		int32_t L_6 = ___i;
		float L_7 = ((&___v)->___w_4);
		Matrix4x4_set_Item_m6_328(__this, 3, L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Matrix4x4::SetRow(System.Int32,UnityEngine.Vector4)
extern "C" void Matrix4x4_SetRow_m6_345 (Matrix4x4_t6_52 * __this, int32_t ___i, Vector4_t6_54  ___v, const MethodInfo* method)
{
	{
		int32_t L_0 = ___i;
		float L_1 = ((&___v)->___x_1);
		Matrix4x4_set_Item_m6_328(__this, L_0, 0, L_1, /*hidden argument*/NULL);
		int32_t L_2 = ___i;
		float L_3 = ((&___v)->___y_2);
		Matrix4x4_set_Item_m6_328(__this, L_2, 1, L_3, /*hidden argument*/NULL);
		int32_t L_4 = ___i;
		float L_5 = ((&___v)->___z_3);
		Matrix4x4_set_Item_m6_328(__this, L_4, 2, L_5, /*hidden argument*/NULL);
		int32_t L_6 = ___i;
		float L_7 = ((&___v)->___w_4);
		Matrix4x4_set_Item_m6_328(__this, L_6, 3, L_7, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Matrix4x4::MultiplyPoint(UnityEngine.Vector3)
extern "C" Vector3_t6_48  Matrix4x4_MultiplyPoint_m6_346 (Matrix4x4_t6_52 * __this, Vector3_t6_48  ___v, const MethodInfo* method)
{
	Vector3_t6_48  V_0 = {0};
	float V_1 = 0.0f;
	{
		float L_0 = (__this->___m00_0);
		float L_1 = ((&___v)->___x_1);
		float L_2 = (__this->___m01_4);
		float L_3 = ((&___v)->___y_2);
		float L_4 = (__this->___m02_8);
		float L_5 = ((&___v)->___z_3);
		float L_6 = (__this->___m03_12);
		(&V_0)->___x_1 = ((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))+(float)L_6));
		float L_7 = (__this->___m10_1);
		float L_8 = ((&___v)->___x_1);
		float L_9 = (__this->___m11_5);
		float L_10 = ((&___v)->___y_2);
		float L_11 = (__this->___m12_9);
		float L_12 = ((&___v)->___z_3);
		float L_13 = (__this->___m13_13);
		(&V_0)->___y_2 = ((float)((float)((float)((float)((float)((float)((float)((float)L_7*(float)L_8))+(float)((float)((float)L_9*(float)L_10))))+(float)((float)((float)L_11*(float)L_12))))+(float)L_13));
		float L_14 = (__this->___m20_2);
		float L_15 = ((&___v)->___x_1);
		float L_16 = (__this->___m21_6);
		float L_17 = ((&___v)->___y_2);
		float L_18 = (__this->___m22_10);
		float L_19 = ((&___v)->___z_3);
		float L_20 = (__this->___m23_14);
		(&V_0)->___z_3 = ((float)((float)((float)((float)((float)((float)((float)((float)L_14*(float)L_15))+(float)((float)((float)L_16*(float)L_17))))+(float)((float)((float)L_18*(float)L_19))))+(float)L_20));
		float L_21 = (__this->___m30_3);
		float L_22 = ((&___v)->___x_1);
		float L_23 = (__this->___m31_7);
		float L_24 = ((&___v)->___y_2);
		float L_25 = (__this->___m32_11);
		float L_26 = ((&___v)->___z_3);
		float L_27 = (__this->___m33_15);
		V_1 = ((float)((float)((float)((float)((float)((float)((float)((float)L_21*(float)L_22))+(float)((float)((float)L_23*(float)L_24))))+(float)((float)((float)L_25*(float)L_26))))+(float)L_27));
		float L_28 = V_1;
		V_1 = ((float)((float)(1.0f)/(float)L_28));
		Vector3_t6_48 * L_29 = (&V_0);
		float L_30 = (L_29->___x_1);
		float L_31 = V_1;
		L_29->___x_1 = ((float)((float)L_30*(float)L_31));
		Vector3_t6_48 * L_32 = (&V_0);
		float L_33 = (L_32->___y_2);
		float L_34 = V_1;
		L_32->___y_2 = ((float)((float)L_33*(float)L_34));
		Vector3_t6_48 * L_35 = (&V_0);
		float L_36 = (L_35->___z_3);
		float L_37 = V_1;
		L_35->___z_3 = ((float)((float)L_36*(float)L_37));
		Vector3_t6_48  L_38 = V_0;
		return L_38;
	}
}
// UnityEngine.Vector3 UnityEngine.Matrix4x4::MultiplyPoint3x4(UnityEngine.Vector3)
extern "C" Vector3_t6_48  Matrix4x4_MultiplyPoint3x4_m6_347 (Matrix4x4_t6_52 * __this, Vector3_t6_48  ___v, const MethodInfo* method)
{
	Vector3_t6_48  V_0 = {0};
	{
		float L_0 = (__this->___m00_0);
		float L_1 = ((&___v)->___x_1);
		float L_2 = (__this->___m01_4);
		float L_3 = ((&___v)->___y_2);
		float L_4 = (__this->___m02_8);
		float L_5 = ((&___v)->___z_3);
		float L_6 = (__this->___m03_12);
		(&V_0)->___x_1 = ((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))+(float)L_6));
		float L_7 = (__this->___m10_1);
		float L_8 = ((&___v)->___x_1);
		float L_9 = (__this->___m11_5);
		float L_10 = ((&___v)->___y_2);
		float L_11 = (__this->___m12_9);
		float L_12 = ((&___v)->___z_3);
		float L_13 = (__this->___m13_13);
		(&V_0)->___y_2 = ((float)((float)((float)((float)((float)((float)((float)((float)L_7*(float)L_8))+(float)((float)((float)L_9*(float)L_10))))+(float)((float)((float)L_11*(float)L_12))))+(float)L_13));
		float L_14 = (__this->___m20_2);
		float L_15 = ((&___v)->___x_1);
		float L_16 = (__this->___m21_6);
		float L_17 = ((&___v)->___y_2);
		float L_18 = (__this->___m22_10);
		float L_19 = ((&___v)->___z_3);
		float L_20 = (__this->___m23_14);
		(&V_0)->___z_3 = ((float)((float)((float)((float)((float)((float)((float)((float)L_14*(float)L_15))+(float)((float)((float)L_16*(float)L_17))))+(float)((float)((float)L_18*(float)L_19))))+(float)L_20));
		Vector3_t6_48  L_21 = V_0;
		return L_21;
	}
}
// UnityEngine.Vector3 UnityEngine.Matrix4x4::MultiplyVector(UnityEngine.Vector3)
extern "C" Vector3_t6_48  Matrix4x4_MultiplyVector_m6_348 (Matrix4x4_t6_52 * __this, Vector3_t6_48  ___v, const MethodInfo* method)
{
	Vector3_t6_48  V_0 = {0};
	{
		float L_0 = (__this->___m00_0);
		float L_1 = ((&___v)->___x_1);
		float L_2 = (__this->___m01_4);
		float L_3 = ((&___v)->___y_2);
		float L_4 = (__this->___m02_8);
		float L_5 = ((&___v)->___z_3);
		(&V_0)->___x_1 = ((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))));
		float L_6 = (__this->___m10_1);
		float L_7 = ((&___v)->___x_1);
		float L_8 = (__this->___m11_5);
		float L_9 = ((&___v)->___y_2);
		float L_10 = (__this->___m12_9);
		float L_11 = ((&___v)->___z_3);
		(&V_0)->___y_2 = ((float)((float)((float)((float)((float)((float)L_6*(float)L_7))+(float)((float)((float)L_8*(float)L_9))))+(float)((float)((float)L_10*(float)L_11))));
		float L_12 = (__this->___m20_2);
		float L_13 = ((&___v)->___x_1);
		float L_14 = (__this->___m21_6);
		float L_15 = ((&___v)->___y_2);
		float L_16 = (__this->___m22_10);
		float L_17 = ((&___v)->___z_3);
		(&V_0)->___z_3 = ((float)((float)((float)((float)((float)((float)L_12*(float)L_13))+(float)((float)((float)L_14*(float)L_15))))+(float)((float)((float)L_16*(float)L_17))));
		Vector3_t6_48  L_18 = V_0;
		return L_18;
	}
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::Scale(UnityEngine.Vector3)
extern TypeInfo* Matrix4x4_t6_52_il2cpp_TypeInfo_var;
extern "C" Matrix4x4_t6_52  Matrix4x4_Scale_m6_349 (Object_t * __this /* static, unused */, Vector3_t6_48  ___v, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Matrix4x4_t6_52_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1594);
		s_Il2CppMethodIntialized = true;
	}
	Matrix4x4_t6_52  V_0 = {0};
	{
		Initobj (Matrix4x4_t6_52_il2cpp_TypeInfo_var, (&V_0));
		float L_0 = ((&___v)->___x_1);
		(&V_0)->___m00_0 = L_0;
		(&V_0)->___m01_4 = (0.0f);
		(&V_0)->___m02_8 = (0.0f);
		(&V_0)->___m03_12 = (0.0f);
		(&V_0)->___m10_1 = (0.0f);
		float L_1 = ((&___v)->___y_2);
		(&V_0)->___m11_5 = L_1;
		(&V_0)->___m12_9 = (0.0f);
		(&V_0)->___m13_13 = (0.0f);
		(&V_0)->___m20_2 = (0.0f);
		(&V_0)->___m21_6 = (0.0f);
		float L_2 = ((&___v)->___z_3);
		(&V_0)->___m22_10 = L_2;
		(&V_0)->___m23_14 = (0.0f);
		(&V_0)->___m30_3 = (0.0f);
		(&V_0)->___m31_7 = (0.0f);
		(&V_0)->___m32_11 = (0.0f);
		(&V_0)->___m33_15 = (1.0f);
		Matrix4x4_t6_52  L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::get_zero()
extern TypeInfo* Matrix4x4_t6_52_il2cpp_TypeInfo_var;
extern "C" Matrix4x4_t6_52  Matrix4x4_get_zero_m6_350 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Matrix4x4_t6_52_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1594);
		s_Il2CppMethodIntialized = true;
	}
	Matrix4x4_t6_52  V_0 = {0};
	{
		Initobj (Matrix4x4_t6_52_il2cpp_TypeInfo_var, (&V_0));
		(&V_0)->___m00_0 = (0.0f);
		(&V_0)->___m01_4 = (0.0f);
		(&V_0)->___m02_8 = (0.0f);
		(&V_0)->___m03_12 = (0.0f);
		(&V_0)->___m10_1 = (0.0f);
		(&V_0)->___m11_5 = (0.0f);
		(&V_0)->___m12_9 = (0.0f);
		(&V_0)->___m13_13 = (0.0f);
		(&V_0)->___m20_2 = (0.0f);
		(&V_0)->___m21_6 = (0.0f);
		(&V_0)->___m22_10 = (0.0f);
		(&V_0)->___m23_14 = (0.0f);
		(&V_0)->___m30_3 = (0.0f);
		(&V_0)->___m31_7 = (0.0f);
		(&V_0)->___m32_11 = (0.0f);
		(&V_0)->___m33_15 = (0.0f);
		Matrix4x4_t6_52  L_0 = V_0;
		return L_0;
	}
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::get_identity()
extern TypeInfo* Matrix4x4_t6_52_il2cpp_TypeInfo_var;
extern "C" Matrix4x4_t6_52  Matrix4x4_get_identity_m6_351 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Matrix4x4_t6_52_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1594);
		s_Il2CppMethodIntialized = true;
	}
	Matrix4x4_t6_52  V_0 = {0};
	{
		Initobj (Matrix4x4_t6_52_il2cpp_TypeInfo_var, (&V_0));
		(&V_0)->___m00_0 = (1.0f);
		(&V_0)->___m01_4 = (0.0f);
		(&V_0)->___m02_8 = (0.0f);
		(&V_0)->___m03_12 = (0.0f);
		(&V_0)->___m10_1 = (0.0f);
		(&V_0)->___m11_5 = (1.0f);
		(&V_0)->___m12_9 = (0.0f);
		(&V_0)->___m13_13 = (0.0f);
		(&V_0)->___m20_2 = (0.0f);
		(&V_0)->___m21_6 = (0.0f);
		(&V_0)->___m22_10 = (1.0f);
		(&V_0)->___m23_14 = (0.0f);
		(&V_0)->___m30_3 = (0.0f);
		(&V_0)->___m31_7 = (0.0f);
		(&V_0)->___m32_11 = (0.0f);
		(&V_0)->___m33_15 = (1.0f);
		Matrix4x4_t6_52  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Matrix4x4::SetTRS(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C" void Matrix4x4_SetTRS_m6_352 (Matrix4x4_t6_52 * __this, Vector3_t6_48  ___pos, Quaternion_t6_50  ___q, Vector3_t6_48  ___s, const MethodInfo* method)
{
	{
		Vector3_t6_48  L_0 = ___pos;
		Quaternion_t6_50  L_1 = ___q;
		Vector3_t6_48  L_2 = ___s;
		Matrix4x4_t6_52  L_3 = Matrix4x4_TRS_m6_353(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		(*(Matrix4x4_t6_52 *)__this) = L_3;
		return;
	}
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::TRS(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C" Matrix4x4_t6_52  Matrix4x4_TRS_m6_353 (Object_t * __this /* static, unused */, Vector3_t6_48  ___pos, Quaternion_t6_50  ___q, Vector3_t6_48  ___s, const MethodInfo* method)
{
	{
		Matrix4x4_t6_52  L_0 = Matrix4x4_INTERNAL_CALL_TRS_m6_354(NULL /*static, unused*/, (&___pos), (&___q), (&___s), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::INTERNAL_CALL_TRS(UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Vector3&)
extern "C" Matrix4x4_t6_52  Matrix4x4_INTERNAL_CALL_TRS_m6_354 (Object_t * __this /* static, unused */, Vector3_t6_48 * ___pos, Quaternion_t6_50 * ___q, Vector3_t6_48 * ___s, const MethodInfo* method)
{
	typedef Matrix4x4_t6_52  (*Matrix4x4_INTERNAL_CALL_TRS_m6_354_ftn) (Vector3_t6_48 *, Quaternion_t6_50 *, Vector3_t6_48 *);
	static Matrix4x4_INTERNAL_CALL_TRS_m6_354_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Matrix4x4_INTERNAL_CALL_TRS_m6_354_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Matrix4x4::INTERNAL_CALL_TRS(UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___pos, ___q, ___s);
}
// System.String UnityEngine.Matrix4x4::ToString()
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t1_17_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4704;
extern "C" String_t* Matrix4x4_ToString_m6_355 (Matrix4x4_t6_52 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Single_t1_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		_stringLiteral4704 = il2cpp_codegen_string_literal_from_index(4704);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_272* L_0 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, ((int32_t)16)));
		float L_1 = (__this->___m00_0);
		float L_2 = L_1;
		Object_t * L_3 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_3;
		ObjectU5BU5D_t1_272* L_4 = L_0;
		float L_5 = (__this->___m01_4);
		float L_6 = L_5;
		Object_t * L_7 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1, sizeof(Object_t *))) = (Object_t *)L_7;
		ObjectU5BU5D_t1_272* L_8 = L_4;
		float L_9 = (__this->___m02_8);
		float L_10 = L_9;
		Object_t * L_11 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 2, sizeof(Object_t *))) = (Object_t *)L_11;
		ObjectU5BU5D_t1_272* L_12 = L_8;
		float L_13 = (__this->___m03_12);
		float L_14 = L_13;
		Object_t * L_15 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 3, sizeof(Object_t *))) = (Object_t *)L_15;
		ObjectU5BU5D_t1_272* L_16 = L_12;
		float L_17 = (__this->___m10_1);
		float L_18 = L_17;
		Object_t * L_19 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_18);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 4);
		ArrayElementTypeCheck (L_16, L_19);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_16, 4, sizeof(Object_t *))) = (Object_t *)L_19;
		ObjectU5BU5D_t1_272* L_20 = L_16;
		float L_21 = (__this->___m11_5);
		float L_22 = L_21;
		Object_t * L_23 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_22);
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 5);
		ArrayElementTypeCheck (L_20, L_23);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_20, 5, sizeof(Object_t *))) = (Object_t *)L_23;
		ObjectU5BU5D_t1_272* L_24 = L_20;
		float L_25 = (__this->___m12_9);
		float L_26 = L_25;
		Object_t * L_27 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_26);
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 6);
		ArrayElementTypeCheck (L_24, L_27);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_24, 6, sizeof(Object_t *))) = (Object_t *)L_27;
		ObjectU5BU5D_t1_272* L_28 = L_24;
		float L_29 = (__this->___m13_13);
		float L_30 = L_29;
		Object_t * L_31 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_30);
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, 7);
		ArrayElementTypeCheck (L_28, L_31);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_28, 7, sizeof(Object_t *))) = (Object_t *)L_31;
		ObjectU5BU5D_t1_272* L_32 = L_28;
		float L_33 = (__this->___m20_2);
		float L_34 = L_33;
		Object_t * L_35 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_34);
		NullCheck(L_32);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_32, 8);
		ArrayElementTypeCheck (L_32, L_35);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_32, 8, sizeof(Object_t *))) = (Object_t *)L_35;
		ObjectU5BU5D_t1_272* L_36 = L_32;
		float L_37 = (__this->___m21_6);
		float L_38 = L_37;
		Object_t * L_39 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_38);
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, ((int32_t)9));
		ArrayElementTypeCheck (L_36, L_39);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_36, ((int32_t)9), sizeof(Object_t *))) = (Object_t *)L_39;
		ObjectU5BU5D_t1_272* L_40 = L_36;
		float L_41 = (__this->___m22_10);
		float L_42 = L_41;
		Object_t * L_43 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_42);
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, ((int32_t)10));
		ArrayElementTypeCheck (L_40, L_43);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_40, ((int32_t)10), sizeof(Object_t *))) = (Object_t *)L_43;
		ObjectU5BU5D_t1_272* L_44 = L_40;
		float L_45 = (__this->___m23_14);
		float L_46 = L_45;
		Object_t * L_47 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_46);
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, ((int32_t)11));
		ArrayElementTypeCheck (L_44, L_47);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_44, ((int32_t)11), sizeof(Object_t *))) = (Object_t *)L_47;
		ObjectU5BU5D_t1_272* L_48 = L_44;
		float L_49 = (__this->___m30_3);
		float L_50 = L_49;
		Object_t * L_51 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_50);
		NullCheck(L_48);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_48, ((int32_t)12));
		ArrayElementTypeCheck (L_48, L_51);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_48, ((int32_t)12), sizeof(Object_t *))) = (Object_t *)L_51;
		ObjectU5BU5D_t1_272* L_52 = L_48;
		float L_53 = (__this->___m31_7);
		float L_54 = L_53;
		Object_t * L_55 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_54);
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, ((int32_t)13));
		ArrayElementTypeCheck (L_52, L_55);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_52, ((int32_t)13), sizeof(Object_t *))) = (Object_t *)L_55;
		ObjectU5BU5D_t1_272* L_56 = L_52;
		float L_57 = (__this->___m32_11);
		float L_58 = L_57;
		Object_t * L_59 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_58);
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, ((int32_t)14));
		ArrayElementTypeCheck (L_56, L_59);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_56, ((int32_t)14), sizeof(Object_t *))) = (Object_t *)L_59;
		ObjectU5BU5D_t1_272* L_60 = L_56;
		float L_61 = (__this->___m33_15);
		float L_62 = L_61;
		Object_t * L_63 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_62);
		NullCheck(L_60);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_60, ((int32_t)15));
		ArrayElementTypeCheck (L_60, L_63);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_60, ((int32_t)15), sizeof(Object_t *))) = (Object_t *)L_63;
		String_t* L_64 = UnityString_Format_m6_573(NULL /*static, unused*/, _stringLiteral4704, L_60, /*hidden argument*/NULL);
		return L_64;
	}
}
// System.String UnityEngine.Matrix4x4::ToString(System.String)
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4705;
extern "C" String_t* Matrix4x4_ToString_m6_356 (Matrix4x4_t6_52 * __this, String_t* ___format, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		_stringLiteral4705 = il2cpp_codegen_string_literal_from_index(4705);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_272* L_0 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, ((int32_t)16)));
		float* L_1 = &(__this->___m00_0);
		String_t* L_2 = ___format;
		String_t* L_3 = Single_ToString_m1_635(L_1, L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_3;
		ObjectU5BU5D_t1_272* L_4 = L_0;
		float* L_5 = &(__this->___m01_4);
		String_t* L_6 = ___format;
		String_t* L_7 = Single_ToString_m1_635(L_5, L_6, /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1, sizeof(Object_t *))) = (Object_t *)L_7;
		ObjectU5BU5D_t1_272* L_8 = L_4;
		float* L_9 = &(__this->___m02_8);
		String_t* L_10 = ___format;
		String_t* L_11 = Single_ToString_m1_635(L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 2, sizeof(Object_t *))) = (Object_t *)L_11;
		ObjectU5BU5D_t1_272* L_12 = L_8;
		float* L_13 = &(__this->___m03_12);
		String_t* L_14 = ___format;
		String_t* L_15 = Single_ToString_m1_635(L_13, L_14, /*hidden argument*/NULL);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 3, sizeof(Object_t *))) = (Object_t *)L_15;
		ObjectU5BU5D_t1_272* L_16 = L_12;
		float* L_17 = &(__this->___m10_1);
		String_t* L_18 = ___format;
		String_t* L_19 = Single_ToString_m1_635(L_17, L_18, /*hidden argument*/NULL);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 4);
		ArrayElementTypeCheck (L_16, L_19);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_16, 4, sizeof(Object_t *))) = (Object_t *)L_19;
		ObjectU5BU5D_t1_272* L_20 = L_16;
		float* L_21 = &(__this->___m11_5);
		String_t* L_22 = ___format;
		String_t* L_23 = Single_ToString_m1_635(L_21, L_22, /*hidden argument*/NULL);
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 5);
		ArrayElementTypeCheck (L_20, L_23);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_20, 5, sizeof(Object_t *))) = (Object_t *)L_23;
		ObjectU5BU5D_t1_272* L_24 = L_20;
		float* L_25 = &(__this->___m12_9);
		String_t* L_26 = ___format;
		String_t* L_27 = Single_ToString_m1_635(L_25, L_26, /*hidden argument*/NULL);
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 6);
		ArrayElementTypeCheck (L_24, L_27);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_24, 6, sizeof(Object_t *))) = (Object_t *)L_27;
		ObjectU5BU5D_t1_272* L_28 = L_24;
		float* L_29 = &(__this->___m13_13);
		String_t* L_30 = ___format;
		String_t* L_31 = Single_ToString_m1_635(L_29, L_30, /*hidden argument*/NULL);
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, 7);
		ArrayElementTypeCheck (L_28, L_31);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_28, 7, sizeof(Object_t *))) = (Object_t *)L_31;
		ObjectU5BU5D_t1_272* L_32 = L_28;
		float* L_33 = &(__this->___m20_2);
		String_t* L_34 = ___format;
		String_t* L_35 = Single_ToString_m1_635(L_33, L_34, /*hidden argument*/NULL);
		NullCheck(L_32);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_32, 8);
		ArrayElementTypeCheck (L_32, L_35);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_32, 8, sizeof(Object_t *))) = (Object_t *)L_35;
		ObjectU5BU5D_t1_272* L_36 = L_32;
		float* L_37 = &(__this->___m21_6);
		String_t* L_38 = ___format;
		String_t* L_39 = Single_ToString_m1_635(L_37, L_38, /*hidden argument*/NULL);
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, ((int32_t)9));
		ArrayElementTypeCheck (L_36, L_39);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_36, ((int32_t)9), sizeof(Object_t *))) = (Object_t *)L_39;
		ObjectU5BU5D_t1_272* L_40 = L_36;
		float* L_41 = &(__this->___m22_10);
		String_t* L_42 = ___format;
		String_t* L_43 = Single_ToString_m1_635(L_41, L_42, /*hidden argument*/NULL);
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, ((int32_t)10));
		ArrayElementTypeCheck (L_40, L_43);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_40, ((int32_t)10), sizeof(Object_t *))) = (Object_t *)L_43;
		ObjectU5BU5D_t1_272* L_44 = L_40;
		float* L_45 = &(__this->___m23_14);
		String_t* L_46 = ___format;
		String_t* L_47 = Single_ToString_m1_635(L_45, L_46, /*hidden argument*/NULL);
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, ((int32_t)11));
		ArrayElementTypeCheck (L_44, L_47);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_44, ((int32_t)11), sizeof(Object_t *))) = (Object_t *)L_47;
		ObjectU5BU5D_t1_272* L_48 = L_44;
		float* L_49 = &(__this->___m30_3);
		String_t* L_50 = ___format;
		String_t* L_51 = Single_ToString_m1_635(L_49, L_50, /*hidden argument*/NULL);
		NullCheck(L_48);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_48, ((int32_t)12));
		ArrayElementTypeCheck (L_48, L_51);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_48, ((int32_t)12), sizeof(Object_t *))) = (Object_t *)L_51;
		ObjectU5BU5D_t1_272* L_52 = L_48;
		float* L_53 = &(__this->___m31_7);
		String_t* L_54 = ___format;
		String_t* L_55 = Single_ToString_m1_635(L_53, L_54, /*hidden argument*/NULL);
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, ((int32_t)13));
		ArrayElementTypeCheck (L_52, L_55);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_52, ((int32_t)13), sizeof(Object_t *))) = (Object_t *)L_55;
		ObjectU5BU5D_t1_272* L_56 = L_52;
		float* L_57 = &(__this->___m32_11);
		String_t* L_58 = ___format;
		String_t* L_59 = Single_ToString_m1_635(L_57, L_58, /*hidden argument*/NULL);
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, ((int32_t)14));
		ArrayElementTypeCheck (L_56, L_59);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_56, ((int32_t)14), sizeof(Object_t *))) = (Object_t *)L_59;
		ObjectU5BU5D_t1_272* L_60 = L_56;
		float* L_61 = &(__this->___m33_15);
		String_t* L_62 = ___format;
		String_t* L_63 = Single_ToString_m1_635(L_61, L_62, /*hidden argument*/NULL);
		NullCheck(L_60);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_60, ((int32_t)15));
		ArrayElementTypeCheck (L_60, L_63);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_60, ((int32_t)15), sizeof(Object_t *))) = (Object_t *)L_63;
		String_t* L_64 = UnityString_Format_m6_573(NULL /*static, unused*/, _stringLiteral4705, L_60, /*hidden argument*/NULL);
		return L_64;
	}
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::Ortho(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C" Matrix4x4_t6_52  Matrix4x4_Ortho_m6_357 (Object_t * __this /* static, unused */, float ___left, float ___right, float ___bottom, float ___top, float ___zNear, float ___zFar, const MethodInfo* method)
{
	typedef Matrix4x4_t6_52  (*Matrix4x4_Ortho_m6_357_ftn) (float, float, float, float, float, float);
	static Matrix4x4_Ortho_m6_357_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Matrix4x4_Ortho_m6_357_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Matrix4x4::Ortho(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)");
	return _il2cpp_icall_func(___left, ___right, ___bottom, ___top, ___zNear, ___zFar);
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::Perspective(System.Single,System.Single,System.Single,System.Single)
extern "C" Matrix4x4_t6_52  Matrix4x4_Perspective_m6_358 (Object_t * __this /* static, unused */, float ___fov, float ___aspect, float ___zNear, float ___zFar, const MethodInfo* method)
{
	typedef Matrix4x4_t6_52  (*Matrix4x4_Perspective_m6_358_ftn) (float, float, float, float);
	static Matrix4x4_Perspective_m6_358_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Matrix4x4_Perspective_m6_358_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Matrix4x4::Perspective(System.Single,System.Single,System.Single,System.Single)");
	return _il2cpp_icall_func(___fov, ___aspect, ___zNear, ___zFar);
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::op_Multiply(UnityEngine.Matrix4x4,UnityEngine.Matrix4x4)
extern TypeInfo* Matrix4x4_t6_52_il2cpp_TypeInfo_var;
extern "C" Matrix4x4_t6_52  Matrix4x4_op_Multiply_m6_359 (Object_t * __this /* static, unused */, Matrix4x4_t6_52  ___lhs, Matrix4x4_t6_52  ___rhs, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Matrix4x4_t6_52_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1594);
		s_Il2CppMethodIntialized = true;
	}
	Matrix4x4_t6_52  V_0 = {0};
	{
		Initobj (Matrix4x4_t6_52_il2cpp_TypeInfo_var, (&V_0));
		float L_0 = ((&___lhs)->___m00_0);
		float L_1 = ((&___rhs)->___m00_0);
		float L_2 = ((&___lhs)->___m01_4);
		float L_3 = ((&___rhs)->___m10_1);
		float L_4 = ((&___lhs)->___m02_8);
		float L_5 = ((&___rhs)->___m20_2);
		float L_6 = ((&___lhs)->___m03_12);
		float L_7 = ((&___rhs)->___m30_3);
		(&V_0)->___m00_0 = ((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))+(float)((float)((float)L_6*(float)L_7))));
		float L_8 = ((&___lhs)->___m00_0);
		float L_9 = ((&___rhs)->___m01_4);
		float L_10 = ((&___lhs)->___m01_4);
		float L_11 = ((&___rhs)->___m11_5);
		float L_12 = ((&___lhs)->___m02_8);
		float L_13 = ((&___rhs)->___m21_6);
		float L_14 = ((&___lhs)->___m03_12);
		float L_15 = ((&___rhs)->___m31_7);
		(&V_0)->___m01_4 = ((float)((float)((float)((float)((float)((float)((float)((float)L_8*(float)L_9))+(float)((float)((float)L_10*(float)L_11))))+(float)((float)((float)L_12*(float)L_13))))+(float)((float)((float)L_14*(float)L_15))));
		float L_16 = ((&___lhs)->___m00_0);
		float L_17 = ((&___rhs)->___m02_8);
		float L_18 = ((&___lhs)->___m01_4);
		float L_19 = ((&___rhs)->___m12_9);
		float L_20 = ((&___lhs)->___m02_8);
		float L_21 = ((&___rhs)->___m22_10);
		float L_22 = ((&___lhs)->___m03_12);
		float L_23 = ((&___rhs)->___m32_11);
		(&V_0)->___m02_8 = ((float)((float)((float)((float)((float)((float)((float)((float)L_16*(float)L_17))+(float)((float)((float)L_18*(float)L_19))))+(float)((float)((float)L_20*(float)L_21))))+(float)((float)((float)L_22*(float)L_23))));
		float L_24 = ((&___lhs)->___m00_0);
		float L_25 = ((&___rhs)->___m03_12);
		float L_26 = ((&___lhs)->___m01_4);
		float L_27 = ((&___rhs)->___m13_13);
		float L_28 = ((&___lhs)->___m02_8);
		float L_29 = ((&___rhs)->___m23_14);
		float L_30 = ((&___lhs)->___m03_12);
		float L_31 = ((&___rhs)->___m33_15);
		(&V_0)->___m03_12 = ((float)((float)((float)((float)((float)((float)((float)((float)L_24*(float)L_25))+(float)((float)((float)L_26*(float)L_27))))+(float)((float)((float)L_28*(float)L_29))))+(float)((float)((float)L_30*(float)L_31))));
		float L_32 = ((&___lhs)->___m10_1);
		float L_33 = ((&___rhs)->___m00_0);
		float L_34 = ((&___lhs)->___m11_5);
		float L_35 = ((&___rhs)->___m10_1);
		float L_36 = ((&___lhs)->___m12_9);
		float L_37 = ((&___rhs)->___m20_2);
		float L_38 = ((&___lhs)->___m13_13);
		float L_39 = ((&___rhs)->___m30_3);
		(&V_0)->___m10_1 = ((float)((float)((float)((float)((float)((float)((float)((float)L_32*(float)L_33))+(float)((float)((float)L_34*(float)L_35))))+(float)((float)((float)L_36*(float)L_37))))+(float)((float)((float)L_38*(float)L_39))));
		float L_40 = ((&___lhs)->___m10_1);
		float L_41 = ((&___rhs)->___m01_4);
		float L_42 = ((&___lhs)->___m11_5);
		float L_43 = ((&___rhs)->___m11_5);
		float L_44 = ((&___lhs)->___m12_9);
		float L_45 = ((&___rhs)->___m21_6);
		float L_46 = ((&___lhs)->___m13_13);
		float L_47 = ((&___rhs)->___m31_7);
		(&V_0)->___m11_5 = ((float)((float)((float)((float)((float)((float)((float)((float)L_40*(float)L_41))+(float)((float)((float)L_42*(float)L_43))))+(float)((float)((float)L_44*(float)L_45))))+(float)((float)((float)L_46*(float)L_47))));
		float L_48 = ((&___lhs)->___m10_1);
		float L_49 = ((&___rhs)->___m02_8);
		float L_50 = ((&___lhs)->___m11_5);
		float L_51 = ((&___rhs)->___m12_9);
		float L_52 = ((&___lhs)->___m12_9);
		float L_53 = ((&___rhs)->___m22_10);
		float L_54 = ((&___lhs)->___m13_13);
		float L_55 = ((&___rhs)->___m32_11);
		(&V_0)->___m12_9 = ((float)((float)((float)((float)((float)((float)((float)((float)L_48*(float)L_49))+(float)((float)((float)L_50*(float)L_51))))+(float)((float)((float)L_52*(float)L_53))))+(float)((float)((float)L_54*(float)L_55))));
		float L_56 = ((&___lhs)->___m10_1);
		float L_57 = ((&___rhs)->___m03_12);
		float L_58 = ((&___lhs)->___m11_5);
		float L_59 = ((&___rhs)->___m13_13);
		float L_60 = ((&___lhs)->___m12_9);
		float L_61 = ((&___rhs)->___m23_14);
		float L_62 = ((&___lhs)->___m13_13);
		float L_63 = ((&___rhs)->___m33_15);
		(&V_0)->___m13_13 = ((float)((float)((float)((float)((float)((float)((float)((float)L_56*(float)L_57))+(float)((float)((float)L_58*(float)L_59))))+(float)((float)((float)L_60*(float)L_61))))+(float)((float)((float)L_62*(float)L_63))));
		float L_64 = ((&___lhs)->___m20_2);
		float L_65 = ((&___rhs)->___m00_0);
		float L_66 = ((&___lhs)->___m21_6);
		float L_67 = ((&___rhs)->___m10_1);
		float L_68 = ((&___lhs)->___m22_10);
		float L_69 = ((&___rhs)->___m20_2);
		float L_70 = ((&___lhs)->___m23_14);
		float L_71 = ((&___rhs)->___m30_3);
		(&V_0)->___m20_2 = ((float)((float)((float)((float)((float)((float)((float)((float)L_64*(float)L_65))+(float)((float)((float)L_66*(float)L_67))))+(float)((float)((float)L_68*(float)L_69))))+(float)((float)((float)L_70*(float)L_71))));
		float L_72 = ((&___lhs)->___m20_2);
		float L_73 = ((&___rhs)->___m01_4);
		float L_74 = ((&___lhs)->___m21_6);
		float L_75 = ((&___rhs)->___m11_5);
		float L_76 = ((&___lhs)->___m22_10);
		float L_77 = ((&___rhs)->___m21_6);
		float L_78 = ((&___lhs)->___m23_14);
		float L_79 = ((&___rhs)->___m31_7);
		(&V_0)->___m21_6 = ((float)((float)((float)((float)((float)((float)((float)((float)L_72*(float)L_73))+(float)((float)((float)L_74*(float)L_75))))+(float)((float)((float)L_76*(float)L_77))))+(float)((float)((float)L_78*(float)L_79))));
		float L_80 = ((&___lhs)->___m20_2);
		float L_81 = ((&___rhs)->___m02_8);
		float L_82 = ((&___lhs)->___m21_6);
		float L_83 = ((&___rhs)->___m12_9);
		float L_84 = ((&___lhs)->___m22_10);
		float L_85 = ((&___rhs)->___m22_10);
		float L_86 = ((&___lhs)->___m23_14);
		float L_87 = ((&___rhs)->___m32_11);
		(&V_0)->___m22_10 = ((float)((float)((float)((float)((float)((float)((float)((float)L_80*(float)L_81))+(float)((float)((float)L_82*(float)L_83))))+(float)((float)((float)L_84*(float)L_85))))+(float)((float)((float)L_86*(float)L_87))));
		float L_88 = ((&___lhs)->___m20_2);
		float L_89 = ((&___rhs)->___m03_12);
		float L_90 = ((&___lhs)->___m21_6);
		float L_91 = ((&___rhs)->___m13_13);
		float L_92 = ((&___lhs)->___m22_10);
		float L_93 = ((&___rhs)->___m23_14);
		float L_94 = ((&___lhs)->___m23_14);
		float L_95 = ((&___rhs)->___m33_15);
		(&V_0)->___m23_14 = ((float)((float)((float)((float)((float)((float)((float)((float)L_88*(float)L_89))+(float)((float)((float)L_90*(float)L_91))))+(float)((float)((float)L_92*(float)L_93))))+(float)((float)((float)L_94*(float)L_95))));
		float L_96 = ((&___lhs)->___m30_3);
		float L_97 = ((&___rhs)->___m00_0);
		float L_98 = ((&___lhs)->___m31_7);
		float L_99 = ((&___rhs)->___m10_1);
		float L_100 = ((&___lhs)->___m32_11);
		float L_101 = ((&___rhs)->___m20_2);
		float L_102 = ((&___lhs)->___m33_15);
		float L_103 = ((&___rhs)->___m30_3);
		(&V_0)->___m30_3 = ((float)((float)((float)((float)((float)((float)((float)((float)L_96*(float)L_97))+(float)((float)((float)L_98*(float)L_99))))+(float)((float)((float)L_100*(float)L_101))))+(float)((float)((float)L_102*(float)L_103))));
		float L_104 = ((&___lhs)->___m30_3);
		float L_105 = ((&___rhs)->___m01_4);
		float L_106 = ((&___lhs)->___m31_7);
		float L_107 = ((&___rhs)->___m11_5);
		float L_108 = ((&___lhs)->___m32_11);
		float L_109 = ((&___rhs)->___m21_6);
		float L_110 = ((&___lhs)->___m33_15);
		float L_111 = ((&___rhs)->___m31_7);
		(&V_0)->___m31_7 = ((float)((float)((float)((float)((float)((float)((float)((float)L_104*(float)L_105))+(float)((float)((float)L_106*(float)L_107))))+(float)((float)((float)L_108*(float)L_109))))+(float)((float)((float)L_110*(float)L_111))));
		float L_112 = ((&___lhs)->___m30_3);
		float L_113 = ((&___rhs)->___m02_8);
		float L_114 = ((&___lhs)->___m31_7);
		float L_115 = ((&___rhs)->___m12_9);
		float L_116 = ((&___lhs)->___m32_11);
		float L_117 = ((&___rhs)->___m22_10);
		float L_118 = ((&___lhs)->___m33_15);
		float L_119 = ((&___rhs)->___m32_11);
		(&V_0)->___m32_11 = ((float)((float)((float)((float)((float)((float)((float)((float)L_112*(float)L_113))+(float)((float)((float)L_114*(float)L_115))))+(float)((float)((float)L_116*(float)L_117))))+(float)((float)((float)L_118*(float)L_119))));
		float L_120 = ((&___lhs)->___m30_3);
		float L_121 = ((&___rhs)->___m03_12);
		float L_122 = ((&___lhs)->___m31_7);
		float L_123 = ((&___rhs)->___m13_13);
		float L_124 = ((&___lhs)->___m32_11);
		float L_125 = ((&___rhs)->___m23_14);
		float L_126 = ((&___lhs)->___m33_15);
		float L_127 = ((&___rhs)->___m33_15);
		(&V_0)->___m33_15 = ((float)((float)((float)((float)((float)((float)((float)((float)L_120*(float)L_121))+(float)((float)((float)L_122*(float)L_123))))+(float)((float)((float)L_124*(float)L_125))))+(float)((float)((float)L_126*(float)L_127))));
		Matrix4x4_t6_52  L_128 = V_0;
		return L_128;
	}
}
// UnityEngine.Vector4 UnityEngine.Matrix4x4::op_Multiply(UnityEngine.Matrix4x4,UnityEngine.Vector4)
extern "C" Vector4_t6_54  Matrix4x4_op_Multiply_m6_360 (Object_t * __this /* static, unused */, Matrix4x4_t6_52  ___lhs, Vector4_t6_54  ___v, const MethodInfo* method)
{
	Vector4_t6_54  V_0 = {0};
	{
		float L_0 = ((&___lhs)->___m00_0);
		float L_1 = ((&___v)->___x_1);
		float L_2 = ((&___lhs)->___m01_4);
		float L_3 = ((&___v)->___y_2);
		float L_4 = ((&___lhs)->___m02_8);
		float L_5 = ((&___v)->___z_3);
		float L_6 = ((&___lhs)->___m03_12);
		float L_7 = ((&___v)->___w_4);
		(&V_0)->___x_1 = ((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))+(float)((float)((float)L_6*(float)L_7))));
		float L_8 = ((&___lhs)->___m10_1);
		float L_9 = ((&___v)->___x_1);
		float L_10 = ((&___lhs)->___m11_5);
		float L_11 = ((&___v)->___y_2);
		float L_12 = ((&___lhs)->___m12_9);
		float L_13 = ((&___v)->___z_3);
		float L_14 = ((&___lhs)->___m13_13);
		float L_15 = ((&___v)->___w_4);
		(&V_0)->___y_2 = ((float)((float)((float)((float)((float)((float)((float)((float)L_8*(float)L_9))+(float)((float)((float)L_10*(float)L_11))))+(float)((float)((float)L_12*(float)L_13))))+(float)((float)((float)L_14*(float)L_15))));
		float L_16 = ((&___lhs)->___m20_2);
		float L_17 = ((&___v)->___x_1);
		float L_18 = ((&___lhs)->___m21_6);
		float L_19 = ((&___v)->___y_2);
		float L_20 = ((&___lhs)->___m22_10);
		float L_21 = ((&___v)->___z_3);
		float L_22 = ((&___lhs)->___m23_14);
		float L_23 = ((&___v)->___w_4);
		(&V_0)->___z_3 = ((float)((float)((float)((float)((float)((float)((float)((float)L_16*(float)L_17))+(float)((float)((float)L_18*(float)L_19))))+(float)((float)((float)L_20*(float)L_21))))+(float)((float)((float)L_22*(float)L_23))));
		float L_24 = ((&___lhs)->___m30_3);
		float L_25 = ((&___v)->___x_1);
		float L_26 = ((&___lhs)->___m31_7);
		float L_27 = ((&___v)->___y_2);
		float L_28 = ((&___lhs)->___m32_11);
		float L_29 = ((&___v)->___z_3);
		float L_30 = ((&___lhs)->___m33_15);
		float L_31 = ((&___v)->___w_4);
		(&V_0)->___w_4 = ((float)((float)((float)((float)((float)((float)((float)((float)L_24*(float)L_25))+(float)((float)((float)L_26*(float)L_27))))+(float)((float)((float)L_28*(float)L_29))))+(float)((float)((float)L_30*(float)L_31))));
		Vector4_t6_54  L_32 = V_0;
		return L_32;
	}
}
// System.Boolean UnityEngine.Matrix4x4::op_Equality(UnityEngine.Matrix4x4,UnityEngine.Matrix4x4)
extern "C" bool Matrix4x4_op_Equality_m6_361 (Object_t * __this /* static, unused */, Matrix4x4_t6_52  ___lhs, Matrix4x4_t6_52  ___rhs, const MethodInfo* method)
{
	int32_t G_B5_0 = 0;
	{
		Vector4_t6_54  L_0 = Matrix4x4_GetColumn_m6_342((&___lhs), 0, /*hidden argument*/NULL);
		Vector4_t6_54  L_1 = Matrix4x4_GetColumn_m6_342((&___rhs), 0, /*hidden argument*/NULL);
		bool L_2 = Vector4_op_Equality_m6_411(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0065;
		}
	}
	{
		Vector4_t6_54  L_3 = Matrix4x4_GetColumn_m6_342((&___lhs), 1, /*hidden argument*/NULL);
		Vector4_t6_54  L_4 = Matrix4x4_GetColumn_m6_342((&___rhs), 1, /*hidden argument*/NULL);
		bool L_5 = Vector4_op_Equality_m6_411(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0065;
		}
	}
	{
		Vector4_t6_54  L_6 = Matrix4x4_GetColumn_m6_342((&___lhs), 2, /*hidden argument*/NULL);
		Vector4_t6_54  L_7 = Matrix4x4_GetColumn_m6_342((&___rhs), 2, /*hidden argument*/NULL);
		bool L_8 = Vector4_op_Equality_m6_411(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0065;
		}
	}
	{
		Vector4_t6_54  L_9 = Matrix4x4_GetColumn_m6_342((&___lhs), 3, /*hidden argument*/NULL);
		Vector4_t6_54  L_10 = Matrix4x4_GetColumn_m6_342((&___rhs), 3, /*hidden argument*/NULL);
		bool L_11 = Vector4_op_Equality_m6_411(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_11));
		goto IL_0066;
	}

IL_0065:
	{
		G_B5_0 = 0;
	}

IL_0066:
	{
		return G_B5_0;
	}
}
// System.Boolean UnityEngine.Matrix4x4::op_Inequality(UnityEngine.Matrix4x4,UnityEngine.Matrix4x4)
extern "C" bool Matrix4x4_op_Inequality_m6_362 (Object_t * __this /* static, unused */, Matrix4x4_t6_52  ___lhs, Matrix4x4_t6_52  ___rhs, const MethodInfo* method)
{
	{
		Matrix4x4_t6_52  L_0 = ___lhs;
		Matrix4x4_t6_52  L_1 = ___rhs;
		bool L_2 = Matrix4x4_op_Equality_m6_361(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return ((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void UnityEngine.Bounds::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" void Bounds__ctor_m6_363 (Bounds_t6_53 * __this, Vector3_t6_48  ___center, Vector3_t6_48  ___size, const MethodInfo* method)
{
	{
		Vector3_t6_48  L_0 = ___center;
		__this->___m_Center_0 = L_0;
		Vector3_t6_48  L_1 = ___size;
		Vector3_t6_48  L_2 = Vector3_op_Multiply_m6_265(NULL /*static, unused*/, L_1, (0.5f), /*hidden argument*/NULL);
		__this->___m_Extents_1 = L_2;
		return;
	}
}
// System.Int32 UnityEngine.Bounds::GetHashCode()
extern "C" int32_t Bounds_GetHashCode_m6_364 (Bounds_t6_53 * __this, const MethodInfo* method)
{
	Vector3_t6_48  V_0 = {0};
	Vector3_t6_48  V_1 = {0};
	{
		Vector3_t6_48  L_0 = Bounds_get_center_m6_366(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Vector3_GetHashCode_m6_242((&V_0), /*hidden argument*/NULL);
		Vector3_t6_48  L_2 = Bounds_get_extents_m6_370(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = Vector3_GetHashCode_m6_242((&V_1), /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))));
	}
}
// System.Boolean UnityEngine.Bounds::Equals(System.Object)
extern TypeInfo* Bounds_t6_53_il2cpp_TypeInfo_var;
extern TypeInfo* Vector3_t6_48_il2cpp_TypeInfo_var;
extern "C" bool Bounds_Equals_m6_365 (Bounds_t6_53 * __this, Object_t * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Bounds_t6_53_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1595);
		Vector3_t6_48_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1590);
		s_Il2CppMethodIntialized = true;
	}
	Bounds_t6_53  V_0 = {0};
	Vector3_t6_48  V_1 = {0};
	Vector3_t6_48  V_2 = {0};
	int32_t G_B5_0 = 0;
	{
		Object_t * L_0 = ___other;
		if (((Object_t *)IsInstSealed(L_0, Bounds_t6_53_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		Object_t * L_1 = ___other;
		V_0 = ((*(Bounds_t6_53 *)((Bounds_t6_53 *)UnBox (L_1, Bounds_t6_53_il2cpp_TypeInfo_var))));
		Vector3_t6_48  L_2 = Bounds_get_center_m6_366(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		Vector3_t6_48  L_3 = Bounds_get_center_m6_366((&V_0), /*hidden argument*/NULL);
		Vector3_t6_48  L_4 = L_3;
		Object_t * L_5 = Box(Vector3_t6_48_il2cpp_TypeInfo_var, &L_4);
		bool L_6 = Vector3_Equals_m6_243((&V_1), L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_004f;
		}
	}
	{
		Vector3_t6_48  L_7 = Bounds_get_extents_m6_370(__this, /*hidden argument*/NULL);
		V_2 = L_7;
		Vector3_t6_48  L_8 = Bounds_get_extents_m6_370((&V_0), /*hidden argument*/NULL);
		Vector3_t6_48  L_9 = L_8;
		Object_t * L_10 = Box(Vector3_t6_48_il2cpp_TypeInfo_var, &L_9);
		bool L_11 = Vector3_Equals_m6_243((&V_2), L_10, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_11));
		goto IL_0050;
	}

IL_004f:
	{
		G_B5_0 = 0;
	}

IL_0050:
	{
		return G_B5_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Bounds::get_center()
extern "C" Vector3_t6_48  Bounds_get_center_m6_366 (Bounds_t6_53 * __this, const MethodInfo* method)
{
	{
		Vector3_t6_48  L_0 = (__this->___m_Center_0);
		return L_0;
	}
}
// System.Void UnityEngine.Bounds::set_center(UnityEngine.Vector3)
extern "C" void Bounds_set_center_m6_367 (Bounds_t6_53 * __this, Vector3_t6_48  ___value, const MethodInfo* method)
{
	{
		Vector3_t6_48  L_0 = ___value;
		__this->___m_Center_0 = L_0;
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Bounds::get_size()
extern "C" Vector3_t6_48  Bounds_get_size_m6_368 (Bounds_t6_53 * __this, const MethodInfo* method)
{
	{
		Vector3_t6_48  L_0 = (__this->___m_Extents_1);
		Vector3_t6_48  L_1 = Vector3_op_Multiply_m6_265(NULL /*static, unused*/, L_0, (2.0f), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.Bounds::set_size(UnityEngine.Vector3)
extern "C" void Bounds_set_size_m6_369 (Bounds_t6_53 * __this, Vector3_t6_48  ___value, const MethodInfo* method)
{
	{
		Vector3_t6_48  L_0 = ___value;
		Vector3_t6_48  L_1 = Vector3_op_Multiply_m6_265(NULL /*static, unused*/, L_0, (0.5f), /*hidden argument*/NULL);
		__this->___m_Extents_1 = L_1;
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Bounds::get_extents()
extern "C" Vector3_t6_48  Bounds_get_extents_m6_370 (Bounds_t6_53 * __this, const MethodInfo* method)
{
	{
		Vector3_t6_48  L_0 = (__this->___m_Extents_1);
		return L_0;
	}
}
// System.Void UnityEngine.Bounds::set_extents(UnityEngine.Vector3)
extern "C" void Bounds_set_extents_m6_371 (Bounds_t6_53 * __this, Vector3_t6_48  ___value, const MethodInfo* method)
{
	{
		Vector3_t6_48  L_0 = ___value;
		__this->___m_Extents_1 = L_0;
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Bounds::get_min()
extern "C" Vector3_t6_48  Bounds_get_min_m6_372 (Bounds_t6_53 * __this, const MethodInfo* method)
{
	{
		Vector3_t6_48  L_0 = Bounds_get_center_m6_366(__this, /*hidden argument*/NULL);
		Vector3_t6_48  L_1 = Bounds_get_extents_m6_370(__this, /*hidden argument*/NULL);
		Vector3_t6_48  L_2 = Vector3_op_Subtraction_m6_264(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.Bounds::set_min(UnityEngine.Vector3)
extern "C" void Bounds_set_min_m6_373 (Bounds_t6_53 * __this, Vector3_t6_48  ___value, const MethodInfo* method)
{
	{
		Vector3_t6_48  L_0 = ___value;
		Vector3_t6_48  L_1 = Bounds_get_max_m6_374(__this, /*hidden argument*/NULL);
		Bounds_SetMinMax_m6_376(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Bounds::get_max()
extern "C" Vector3_t6_48  Bounds_get_max_m6_374 (Bounds_t6_53 * __this, const MethodInfo* method)
{
	{
		Vector3_t6_48  L_0 = Bounds_get_center_m6_366(__this, /*hidden argument*/NULL);
		Vector3_t6_48  L_1 = Bounds_get_extents_m6_370(__this, /*hidden argument*/NULL);
		Vector3_t6_48  L_2 = Vector3_op_Addition_m6_263(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.Bounds::set_max(UnityEngine.Vector3)
extern "C" void Bounds_set_max_m6_375 (Bounds_t6_53 * __this, Vector3_t6_48  ___value, const MethodInfo* method)
{
	{
		Vector3_t6_48  L_0 = Bounds_get_min_m6_372(__this, /*hidden argument*/NULL);
		Vector3_t6_48  L_1 = ___value;
		Bounds_SetMinMax_m6_376(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Bounds::SetMinMax(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" void Bounds_SetMinMax_m6_376 (Bounds_t6_53 * __this, Vector3_t6_48  ___min, Vector3_t6_48  ___max, const MethodInfo* method)
{
	{
		Vector3_t6_48  L_0 = ___max;
		Vector3_t6_48  L_1 = ___min;
		Vector3_t6_48  L_2 = Vector3_op_Subtraction_m6_264(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		Vector3_t6_48  L_3 = Vector3_op_Multiply_m6_265(NULL /*static, unused*/, L_2, (0.5f), /*hidden argument*/NULL);
		Bounds_set_extents_m6_371(__this, L_3, /*hidden argument*/NULL);
		Vector3_t6_48  L_4 = ___min;
		Vector3_t6_48  L_5 = Bounds_get_extents_m6_370(__this, /*hidden argument*/NULL);
		Vector3_t6_48  L_6 = Vector3_op_Addition_m6_263(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		Bounds_set_center_m6_367(__this, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Bounds::Encapsulate(UnityEngine.Vector3)
extern "C" void Bounds_Encapsulate_m6_377 (Bounds_t6_53 * __this, Vector3_t6_48  ___point, const MethodInfo* method)
{
	{
		Vector3_t6_48  L_0 = Bounds_get_min_m6_372(__this, /*hidden argument*/NULL);
		Vector3_t6_48  L_1 = ___point;
		Vector3_t6_48  L_2 = Vector3_Min_m6_253(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		Vector3_t6_48  L_3 = Bounds_get_max_m6_374(__this, /*hidden argument*/NULL);
		Vector3_t6_48  L_4 = ___point;
		Vector3_t6_48  L_5 = Vector3_Max_m6_254(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		Bounds_SetMinMax_m6_376(__this, L_2, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Bounds::Encapsulate(UnityEngine.Bounds)
extern "C" void Bounds_Encapsulate_m6_378 (Bounds_t6_53 * __this, Bounds_t6_53  ___bounds, const MethodInfo* method)
{
	{
		Vector3_t6_48  L_0 = Bounds_get_center_m6_366((&___bounds), /*hidden argument*/NULL);
		Vector3_t6_48  L_1 = Bounds_get_extents_m6_370((&___bounds), /*hidden argument*/NULL);
		Vector3_t6_48  L_2 = Vector3_op_Subtraction_m6_264(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		Bounds_Encapsulate_m6_377(__this, L_2, /*hidden argument*/NULL);
		Vector3_t6_48  L_3 = Bounds_get_center_m6_366((&___bounds), /*hidden argument*/NULL);
		Vector3_t6_48  L_4 = Bounds_get_extents_m6_370((&___bounds), /*hidden argument*/NULL);
		Vector3_t6_48  L_5 = Vector3_op_Addition_m6_263(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		Bounds_Encapsulate_m6_377(__this, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Bounds::Expand(System.Single)
extern "C" void Bounds_Expand_m6_379 (Bounds_t6_53 * __this, float ___amount, const MethodInfo* method)
{
	{
		float L_0 = ___amount;
		___amount = ((float)((float)L_0*(float)(0.5f)));
		Vector3_t6_48  L_1 = Bounds_get_extents_m6_370(__this, /*hidden argument*/NULL);
		float L_2 = ___amount;
		float L_3 = ___amount;
		float L_4 = ___amount;
		Vector3_t6_48  L_5 = {0};
		Vector3__ctor_m6_237(&L_5, L_2, L_3, L_4, /*hidden argument*/NULL);
		Vector3_t6_48  L_6 = Vector3_op_Addition_m6_263(NULL /*static, unused*/, L_1, L_5, /*hidden argument*/NULL);
		Bounds_set_extents_m6_371(__this, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Bounds::Expand(UnityEngine.Vector3)
extern "C" void Bounds_Expand_m6_380 (Bounds_t6_53 * __this, Vector3_t6_48  ___amount, const MethodInfo* method)
{
	{
		Vector3_t6_48  L_0 = Bounds_get_extents_m6_370(__this, /*hidden argument*/NULL);
		Vector3_t6_48  L_1 = ___amount;
		Vector3_t6_48  L_2 = Vector3_op_Multiply_m6_265(NULL /*static, unused*/, L_1, (0.5f), /*hidden argument*/NULL);
		Vector3_t6_48  L_3 = Vector3_op_Addition_m6_263(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		Bounds_set_extents_m6_371(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Bounds::Intersects(UnityEngine.Bounds)
extern "C" bool Bounds_Intersects_m6_381 (Bounds_t6_53 * __this, Bounds_t6_53  ___bounds, const MethodInfo* method)
{
	Vector3_t6_48  V_0 = {0};
	Vector3_t6_48  V_1 = {0};
	Vector3_t6_48  V_2 = {0};
	Vector3_t6_48  V_3 = {0};
	Vector3_t6_48  V_4 = {0};
	Vector3_t6_48  V_5 = {0};
	Vector3_t6_48  V_6 = {0};
	Vector3_t6_48  V_7 = {0};
	Vector3_t6_48  V_8 = {0};
	Vector3_t6_48  V_9 = {0};
	Vector3_t6_48  V_10 = {0};
	Vector3_t6_48  V_11 = {0};
	int32_t G_B7_0 = 0;
	{
		Vector3_t6_48  L_0 = Bounds_get_min_m6_372(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = ((&V_0)->___x_1);
		Vector3_t6_48  L_2 = Bounds_get_max_m6_374((&___bounds), /*hidden argument*/NULL);
		V_1 = L_2;
		float L_3 = ((&V_1)->___x_1);
		if ((!(((float)L_1) <= ((float)L_3))))
		{
			goto IL_00d6;
		}
	}
	{
		Vector3_t6_48  L_4 = Bounds_get_max_m6_374(__this, /*hidden argument*/NULL);
		V_2 = L_4;
		float L_5 = ((&V_2)->___x_1);
		Vector3_t6_48  L_6 = Bounds_get_min_m6_372((&___bounds), /*hidden argument*/NULL);
		V_3 = L_6;
		float L_7 = ((&V_3)->___x_1);
		if ((!(((float)L_5) >= ((float)L_7))))
		{
			goto IL_00d6;
		}
	}
	{
		Vector3_t6_48  L_8 = Bounds_get_min_m6_372(__this, /*hidden argument*/NULL);
		V_4 = L_8;
		float L_9 = ((&V_4)->___y_2);
		Vector3_t6_48  L_10 = Bounds_get_max_m6_374((&___bounds), /*hidden argument*/NULL);
		V_5 = L_10;
		float L_11 = ((&V_5)->___y_2);
		if ((!(((float)L_9) <= ((float)L_11))))
		{
			goto IL_00d6;
		}
	}
	{
		Vector3_t6_48  L_12 = Bounds_get_max_m6_374(__this, /*hidden argument*/NULL);
		V_6 = L_12;
		float L_13 = ((&V_6)->___y_2);
		Vector3_t6_48  L_14 = Bounds_get_min_m6_372((&___bounds), /*hidden argument*/NULL);
		V_7 = L_14;
		float L_15 = ((&V_7)->___y_2);
		if ((!(((float)L_13) >= ((float)L_15))))
		{
			goto IL_00d6;
		}
	}
	{
		Vector3_t6_48  L_16 = Bounds_get_min_m6_372(__this, /*hidden argument*/NULL);
		V_8 = L_16;
		float L_17 = ((&V_8)->___z_3);
		Vector3_t6_48  L_18 = Bounds_get_max_m6_374((&___bounds), /*hidden argument*/NULL);
		V_9 = L_18;
		float L_19 = ((&V_9)->___z_3);
		if ((!(((float)L_17) <= ((float)L_19))))
		{
			goto IL_00d6;
		}
	}
	{
		Vector3_t6_48  L_20 = Bounds_get_max_m6_374(__this, /*hidden argument*/NULL);
		V_10 = L_20;
		float L_21 = ((&V_10)->___z_3);
		Vector3_t6_48  L_22 = Bounds_get_min_m6_372((&___bounds), /*hidden argument*/NULL);
		V_11 = L_22;
		float L_23 = ((&V_11)->___z_3);
		G_B7_0 = ((((int32_t)((!(((float)L_21) >= ((float)L_23)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_00d7;
	}

IL_00d6:
	{
		G_B7_0 = 0;
	}

IL_00d7:
	{
		return G_B7_0;
	}
}
// System.Boolean UnityEngine.Bounds::Internal_Contains(UnityEngine.Bounds,UnityEngine.Vector3)
extern "C" bool Bounds_Internal_Contains_m6_382 (Object_t * __this /* static, unused */, Bounds_t6_53  ___m, Vector3_t6_48  ___point, const MethodInfo* method)
{
	{
		bool L_0 = Bounds_INTERNAL_CALL_Internal_Contains_m6_383(NULL /*static, unused*/, (&___m), (&___point), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityEngine.Bounds::INTERNAL_CALL_Internal_Contains(UnityEngine.Bounds&,UnityEngine.Vector3&)
extern "C" bool Bounds_INTERNAL_CALL_Internal_Contains_m6_383 (Object_t * __this /* static, unused */, Bounds_t6_53 * ___m, Vector3_t6_48 * ___point, const MethodInfo* method)
{
	typedef bool (*Bounds_INTERNAL_CALL_Internal_Contains_m6_383_ftn) (Bounds_t6_53 *, Vector3_t6_48 *);
	static Bounds_INTERNAL_CALL_Internal_Contains_m6_383_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Bounds_INTERNAL_CALL_Internal_Contains_m6_383_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Bounds::INTERNAL_CALL_Internal_Contains(UnityEngine.Bounds&,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___m, ___point);
}
// System.Boolean UnityEngine.Bounds::Contains(UnityEngine.Vector3)
extern "C" bool Bounds_Contains_m6_384 (Bounds_t6_53 * __this, Vector3_t6_48  ___point, const MethodInfo* method)
{
	{
		Vector3_t6_48  L_0 = ___point;
		bool L_1 = Bounds_Internal_Contains_m6_382(NULL /*static, unused*/, (*(Bounds_t6_53 *)__this), L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Single UnityEngine.Bounds::Internal_SqrDistance(UnityEngine.Bounds,UnityEngine.Vector3)
extern "C" float Bounds_Internal_SqrDistance_m6_385 (Object_t * __this /* static, unused */, Bounds_t6_53  ___m, Vector3_t6_48  ___point, const MethodInfo* method)
{
	{
		float L_0 = Bounds_INTERNAL_CALL_Internal_SqrDistance_m6_386(NULL /*static, unused*/, (&___m), (&___point), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Single UnityEngine.Bounds::INTERNAL_CALL_Internal_SqrDistance(UnityEngine.Bounds&,UnityEngine.Vector3&)
extern "C" float Bounds_INTERNAL_CALL_Internal_SqrDistance_m6_386 (Object_t * __this /* static, unused */, Bounds_t6_53 * ___m, Vector3_t6_48 * ___point, const MethodInfo* method)
{
	typedef float (*Bounds_INTERNAL_CALL_Internal_SqrDistance_m6_386_ftn) (Bounds_t6_53 *, Vector3_t6_48 *);
	static Bounds_INTERNAL_CALL_Internal_SqrDistance_m6_386_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Bounds_INTERNAL_CALL_Internal_SqrDistance_m6_386_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Bounds::INTERNAL_CALL_Internal_SqrDistance(UnityEngine.Bounds&,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___m, ___point);
}
// System.Single UnityEngine.Bounds::SqrDistance(UnityEngine.Vector3)
extern "C" float Bounds_SqrDistance_m6_387 (Bounds_t6_53 * __this, Vector3_t6_48  ___point, const MethodInfo* method)
{
	{
		Vector3_t6_48  L_0 = ___point;
		float L_1 = Bounds_Internal_SqrDistance_m6_385(NULL /*static, unused*/, (*(Bounds_t6_53 *)__this), L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityEngine.Bounds::Internal_IntersectRay(UnityEngine.Ray&,UnityEngine.Bounds&,System.Single&)
extern "C" bool Bounds_Internal_IntersectRay_m6_388 (Object_t * __this /* static, unused */, Ray_t6_55 * ___ray, Bounds_t6_53 * ___bounds, float* ___distance, const MethodInfo* method)
{
	{
		Ray_t6_55 * L_0 = ___ray;
		Bounds_t6_53 * L_1 = ___bounds;
		float* L_2 = ___distance;
		bool L_3 = Bounds_INTERNAL_CALL_Internal_IntersectRay_m6_389(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean UnityEngine.Bounds::INTERNAL_CALL_Internal_IntersectRay(UnityEngine.Ray&,UnityEngine.Bounds&,System.Single&)
extern "C" bool Bounds_INTERNAL_CALL_Internal_IntersectRay_m6_389 (Object_t * __this /* static, unused */, Ray_t6_55 * ___ray, Bounds_t6_53 * ___bounds, float* ___distance, const MethodInfo* method)
{
	typedef bool (*Bounds_INTERNAL_CALL_Internal_IntersectRay_m6_389_ftn) (Ray_t6_55 *, Bounds_t6_53 *, float*);
	static Bounds_INTERNAL_CALL_Internal_IntersectRay_m6_389_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Bounds_INTERNAL_CALL_Internal_IntersectRay_m6_389_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Bounds::INTERNAL_CALL_Internal_IntersectRay(UnityEngine.Ray&,UnityEngine.Bounds&,System.Single&)");
	return _il2cpp_icall_func(___ray, ___bounds, ___distance);
}
// System.Boolean UnityEngine.Bounds::IntersectRay(UnityEngine.Ray)
extern "C" bool Bounds_IntersectRay_m6_390 (Bounds_t6_53 * __this, Ray_t6_55  ___ray, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		bool L_0 = Bounds_Internal_IntersectRay_m6_388(NULL /*static, unused*/, (&___ray), __this, (&V_0), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityEngine.Bounds::IntersectRay(UnityEngine.Ray,System.Single&)
extern "C" bool Bounds_IntersectRay_m6_391 (Bounds_t6_53 * __this, Ray_t6_55  ___ray, float* ___distance, const MethodInfo* method)
{
	{
		float* L_0 = ___distance;
		bool L_1 = Bounds_Internal_IntersectRay_m6_388(NULL /*static, unused*/, (&___ray), __this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Vector3 UnityEngine.Bounds::Internal_GetClosestPoint(UnityEngine.Bounds&,UnityEngine.Vector3&)
extern "C" Vector3_t6_48  Bounds_Internal_GetClosestPoint_m6_392 (Object_t * __this /* static, unused */, Bounds_t6_53 * ___bounds, Vector3_t6_48 * ___point, const MethodInfo* method)
{
	{
		Bounds_t6_53 * L_0 = ___bounds;
		Vector3_t6_48 * L_1 = ___point;
		Vector3_t6_48  L_2 = Bounds_INTERNAL_CALL_Internal_GetClosestPoint_m6_393(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Vector3 UnityEngine.Bounds::INTERNAL_CALL_Internal_GetClosestPoint(UnityEngine.Bounds&,UnityEngine.Vector3&)
extern "C" Vector3_t6_48  Bounds_INTERNAL_CALL_Internal_GetClosestPoint_m6_393 (Object_t * __this /* static, unused */, Bounds_t6_53 * ___bounds, Vector3_t6_48 * ___point, const MethodInfo* method)
{
	typedef Vector3_t6_48  (*Bounds_INTERNAL_CALL_Internal_GetClosestPoint_m6_393_ftn) (Bounds_t6_53 *, Vector3_t6_48 *);
	static Bounds_INTERNAL_CALL_Internal_GetClosestPoint_m6_393_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Bounds_INTERNAL_CALL_Internal_GetClosestPoint_m6_393_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Bounds::INTERNAL_CALL_Internal_GetClosestPoint(UnityEngine.Bounds&,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___bounds, ___point);
}
// UnityEngine.Vector3 UnityEngine.Bounds::ClosestPoint(UnityEngine.Vector3)
extern "C" Vector3_t6_48  Bounds_ClosestPoint_m6_394 (Bounds_t6_53 * __this, Vector3_t6_48  ___point, const MethodInfo* method)
{
	{
		Vector3_t6_48  L_0 = Bounds_Internal_GetClosestPoint_m6_392(NULL /*static, unused*/, __this, (&___point), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String UnityEngine.Bounds::ToString()
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* Vector3_t6_48_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4706;
extern "C" String_t* Bounds_ToString_m6_395 (Bounds_t6_53 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Vector3_t6_48_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1590);
		_stringLiteral4706 = il2cpp_codegen_string_literal_from_index(4706);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_272* L_0 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 2));
		Vector3_t6_48  L_1 = (__this->___m_Center_0);
		Vector3_t6_48  L_2 = L_1;
		Object_t * L_3 = Box(Vector3_t6_48_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_3;
		ObjectU5BU5D_t1_272* L_4 = L_0;
		Vector3_t6_48  L_5 = (__this->___m_Extents_1);
		Vector3_t6_48  L_6 = L_5;
		Object_t * L_7 = Box(Vector3_t6_48_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1, sizeof(Object_t *))) = (Object_t *)L_7;
		String_t* L_8 = UnityString_Format_m6_573(NULL /*static, unused*/, _stringLiteral4706, L_4, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.String UnityEngine.Bounds::ToString(System.String)
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4706;
extern "C" String_t* Bounds_ToString_m6_396 (Bounds_t6_53 * __this, String_t* ___format, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		_stringLiteral4706 = il2cpp_codegen_string_literal_from_index(4706);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_272* L_0 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 2));
		Vector3_t6_48 * L_1 = &(__this->___m_Center_0);
		String_t* L_2 = ___format;
		String_t* L_3 = Vector3_ToString_m6_247(L_1, L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_3;
		ObjectU5BU5D_t1_272* L_4 = L_0;
		Vector3_t6_48 * L_5 = &(__this->___m_Extents_1);
		String_t* L_6 = ___format;
		String_t* L_7 = Vector3_ToString_m6_247(L_5, L_6, /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1, sizeof(Object_t *))) = (Object_t *)L_7;
		String_t* L_8 = UnityString_Format_m6_573(NULL /*static, unused*/, _stringLiteral4706, L_4, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Boolean UnityEngine.Bounds::op_Equality(UnityEngine.Bounds,UnityEngine.Bounds)
extern "C" bool Bounds_op_Equality_m6_397 (Object_t * __this /* static, unused */, Bounds_t6_53  ___lhs, Bounds_t6_53  ___rhs, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Vector3_t6_48  L_0 = Bounds_get_center_m6_366((&___lhs), /*hidden argument*/NULL);
		Vector3_t6_48  L_1 = Bounds_get_center_m6_366((&___rhs), /*hidden argument*/NULL);
		bool L_2 = Vector3_op_Equality_m6_267(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002d;
		}
	}
	{
		Vector3_t6_48  L_3 = Bounds_get_extents_m6_370((&___lhs), /*hidden argument*/NULL);
		Vector3_t6_48  L_4 = Bounds_get_extents_m6_370((&___rhs), /*hidden argument*/NULL);
		bool L_5 = Vector3_op_Equality_m6_267(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_5));
		goto IL_002e;
	}

IL_002d:
	{
		G_B3_0 = 0;
	}

IL_002e:
	{
		return G_B3_0;
	}
}
// System.Boolean UnityEngine.Bounds::op_Inequality(UnityEngine.Bounds,UnityEngine.Bounds)
extern "C" bool Bounds_op_Inequality_m6_398 (Object_t * __this /* static, unused */, Bounds_t6_53  ___lhs, Bounds_t6_53  ___rhs, const MethodInfo* method)
{
	{
		Bounds_t6_53  L_0 = ___lhs;
		Bounds_t6_53  L_1 = ___rhs;
		bool L_2 = Bounds_op_Equality_m6_397(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return ((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void UnityEngine.Vector4::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C" void Vector4__ctor_m6_399 (Vector4_t6_54 * __this, float ___x, float ___y, float ___z, float ___w, const MethodInfo* method)
{
	{
		float L_0 = ___x;
		__this->___x_1 = L_0;
		float L_1 = ___y;
		__this->___y_2 = L_1;
		float L_2 = ___z;
		__this->___z_3 = L_2;
		float L_3 = ___w;
		__this->___w_4 = L_3;
		return;
	}
}
// System.Single UnityEngine.Vector4::get_Item(System.Int32)
extern TypeInfo* IndexOutOfRangeException_t1_1555_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4707;
extern "C" float Vector4_get_Item_m6_400 (Vector4_t6_54 * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IndexOutOfRangeException_t1_1555_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		_stringLiteral4707 = il2cpp_codegen_string_literal_from_index(4707);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_001d;
		}
		if (L_1 == 1)
		{
			goto IL_0024;
		}
		if (L_1 == 2)
		{
			goto IL_002b;
		}
		if (L_1 == 3)
		{
			goto IL_0032;
		}
	}
	{
		goto IL_0039;
	}

IL_001d:
	{
		float L_2 = (__this->___x_1);
		return L_2;
	}

IL_0024:
	{
		float L_3 = (__this->___y_2);
		return L_3;
	}

IL_002b:
	{
		float L_4 = (__this->___z_3);
		return L_4;
	}

IL_0032:
	{
		float L_5 = (__this->___w_4);
		return L_5;
	}

IL_0039:
	{
		IndexOutOfRangeException_t1_1555 * L_6 = (IndexOutOfRangeException_t1_1555 *)il2cpp_codegen_object_new (IndexOutOfRangeException_t1_1555_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m1_14159(L_6, _stringLiteral4707, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_6);
	}
}
// System.Void UnityEngine.Vector4::set_Item(System.Int32,System.Single)
extern TypeInfo* IndexOutOfRangeException_t1_1555_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4707;
extern "C" void Vector4_set_Item_m6_401 (Vector4_t6_54 * __this, int32_t ___index, float ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IndexOutOfRangeException_t1_1555_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		_stringLiteral4707 = il2cpp_codegen_string_literal_from_index(4707);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_001d;
		}
		if (L_1 == 1)
		{
			goto IL_0029;
		}
		if (L_1 == 2)
		{
			goto IL_0035;
		}
		if (L_1 == 3)
		{
			goto IL_0041;
		}
	}
	{
		goto IL_004d;
	}

IL_001d:
	{
		float L_2 = ___value;
		__this->___x_1 = L_2;
		goto IL_0058;
	}

IL_0029:
	{
		float L_3 = ___value;
		__this->___y_2 = L_3;
		goto IL_0058;
	}

IL_0035:
	{
		float L_4 = ___value;
		__this->___z_3 = L_4;
		goto IL_0058;
	}

IL_0041:
	{
		float L_5 = ___value;
		__this->___w_4 = L_5;
		goto IL_0058;
	}

IL_004d:
	{
		IndexOutOfRangeException_t1_1555 * L_6 = (IndexOutOfRangeException_t1_1555 *)il2cpp_codegen_object_new (IndexOutOfRangeException_t1_1555_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m1_14159(L_6, _stringLiteral4707, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_6);
	}

IL_0058:
	{
		return;
	}
}
// System.Int32 UnityEngine.Vector4::GetHashCode()
extern "C" int32_t Vector4_GetHashCode_m6_402 (Vector4_t6_54 * __this, const MethodInfo* method)
{
	{
		float* L_0 = &(__this->___x_1);
		int32_t L_1 = Single_GetHashCode_m1_622(L_0, /*hidden argument*/NULL);
		float* L_2 = &(__this->___y_2);
		int32_t L_3 = Single_GetHashCode_m1_622(L_2, /*hidden argument*/NULL);
		float* L_4 = &(__this->___z_3);
		int32_t L_5 = Single_GetHashCode_m1_622(L_4, /*hidden argument*/NULL);
		float* L_6 = &(__this->___w_4);
		int32_t L_7 = Single_GetHashCode_m1_622(L_6, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)1))));
	}
}
// System.Boolean UnityEngine.Vector4::Equals(System.Object)
extern TypeInfo* Vector4_t6_54_il2cpp_TypeInfo_var;
extern "C" bool Vector4_Equals_m6_403 (Vector4_t6_54 * __this, Object_t * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Vector4_t6_54_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1583);
		s_Il2CppMethodIntialized = true;
	}
	Vector4_t6_54  V_0 = {0};
	int32_t G_B7_0 = 0;
	{
		Object_t * L_0 = ___other;
		if (((Object_t *)IsInstSealed(L_0, Vector4_t6_54_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		Object_t * L_1 = ___other;
		V_0 = ((*(Vector4_t6_54 *)((Vector4_t6_54 *)UnBox (L_1, Vector4_t6_54_il2cpp_TypeInfo_var))));
		float* L_2 = &(__this->___x_1);
		float L_3 = ((&V_0)->___x_1);
		bool L_4 = Single_Equals_m1_621(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_006d;
		}
	}
	{
		float* L_5 = &(__this->___y_2);
		float L_6 = ((&V_0)->___y_2);
		bool L_7 = Single_Equals_m1_621(L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_006d;
		}
	}
	{
		float* L_8 = &(__this->___z_3);
		float L_9 = ((&V_0)->___z_3);
		bool L_10 = Single_Equals_m1_621(L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_006d;
		}
	}
	{
		float* L_11 = &(__this->___w_4);
		float L_12 = ((&V_0)->___w_4);
		bool L_13 = Single_Equals_m1_621(L_11, L_12, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_13));
		goto IL_006e;
	}

IL_006d:
	{
		G_B7_0 = 0;
	}

IL_006e:
	{
		return G_B7_0;
	}
}
// System.String UnityEngine.Vector4::ToString()
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t1_17_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4701;
extern "C" String_t* Vector4_ToString_m6_404 (Vector4_t6_54 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Single_t1_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		_stringLiteral4701 = il2cpp_codegen_string_literal_from_index(4701);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_272* L_0 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 4));
		float L_1 = (__this->___x_1);
		float L_2 = L_1;
		Object_t * L_3 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_3;
		ObjectU5BU5D_t1_272* L_4 = L_0;
		float L_5 = (__this->___y_2);
		float L_6 = L_5;
		Object_t * L_7 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1, sizeof(Object_t *))) = (Object_t *)L_7;
		ObjectU5BU5D_t1_272* L_8 = L_4;
		float L_9 = (__this->___z_3);
		float L_10 = L_9;
		Object_t * L_11 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 2, sizeof(Object_t *))) = (Object_t *)L_11;
		ObjectU5BU5D_t1_272* L_12 = L_8;
		float L_13 = (__this->___w_4);
		float L_14 = L_13;
		Object_t * L_15 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 3, sizeof(Object_t *))) = (Object_t *)L_15;
		String_t* L_16 = UnityString_Format_m6_573(NULL /*static, unused*/, _stringLiteral4701, L_12, /*hidden argument*/NULL);
		return L_16;
	}
}
// System.Single UnityEngine.Vector4::Dot(UnityEngine.Vector4,UnityEngine.Vector4)
extern "C" float Vector4_Dot_m6_405 (Object_t * __this /* static, unused */, Vector4_t6_54  ___a, Vector4_t6_54  ___b, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ((&___b)->___x_1);
		float L_2 = ((&___a)->___y_2);
		float L_3 = ((&___b)->___y_2);
		float L_4 = ((&___a)->___z_3);
		float L_5 = ((&___b)->___z_3);
		float L_6 = ((&___a)->___w_4);
		float L_7 = ((&___b)->___w_4);
		return ((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))+(float)((float)((float)L_6*(float)L_7))));
	}
}
// System.Single UnityEngine.Vector4::SqrMagnitude(UnityEngine.Vector4)
extern "C" float Vector4_SqrMagnitude_m6_406 (Object_t * __this /* static, unused */, Vector4_t6_54  ___a, const MethodInfo* method)
{
	{
		Vector4_t6_54  L_0 = ___a;
		Vector4_t6_54  L_1 = ___a;
		float L_2 = Vector4_Dot_m6_405(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Single UnityEngine.Vector4::get_sqrMagnitude()
extern "C" float Vector4_get_sqrMagnitude_m6_407 (Vector4_t6_54 * __this, const MethodInfo* method)
{
	{
		float L_0 = Vector4_Dot_m6_405(NULL /*static, unused*/, (*(Vector4_t6_54 *)__this), (*(Vector4_t6_54 *)__this), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector4 UnityEngine.Vector4::get_zero()
extern "C" Vector4_t6_54  Vector4_get_zero_m6_408 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector4_t6_54  L_0 = {0};
		Vector4__ctor_m6_399(&L_0, (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector4 UnityEngine.Vector4::op_Subtraction(UnityEngine.Vector4,UnityEngine.Vector4)
extern "C" Vector4_t6_54  Vector4_op_Subtraction_m6_409 (Object_t * __this /* static, unused */, Vector4_t6_54  ___a, Vector4_t6_54  ___b, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ((&___b)->___x_1);
		float L_2 = ((&___a)->___y_2);
		float L_3 = ((&___b)->___y_2);
		float L_4 = ((&___a)->___z_3);
		float L_5 = ((&___b)->___z_3);
		float L_6 = ((&___a)->___w_4);
		float L_7 = ((&___b)->___w_4);
		Vector4_t6_54  L_8 = {0};
		Vector4__ctor_m6_399(&L_8, ((float)((float)L_0-(float)L_1)), ((float)((float)L_2-(float)L_3)), ((float)((float)L_4-(float)L_5)), ((float)((float)L_6-(float)L_7)), /*hidden argument*/NULL);
		return L_8;
	}
}
// UnityEngine.Vector4 UnityEngine.Vector4::op_Division(UnityEngine.Vector4,System.Single)
extern "C" Vector4_t6_54  Vector4_op_Division_m6_410 (Object_t * __this /* static, unused */, Vector4_t6_54  ___a, float ___d, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ___d;
		float L_2 = ((&___a)->___y_2);
		float L_3 = ___d;
		float L_4 = ((&___a)->___z_3);
		float L_5 = ___d;
		float L_6 = ((&___a)->___w_4);
		float L_7 = ___d;
		Vector4_t6_54  L_8 = {0};
		Vector4__ctor_m6_399(&L_8, ((float)((float)L_0/(float)L_1)), ((float)((float)L_2/(float)L_3)), ((float)((float)L_4/(float)L_5)), ((float)((float)L_6/(float)L_7)), /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Boolean UnityEngine.Vector4::op_Equality(UnityEngine.Vector4,UnityEngine.Vector4)
extern "C" bool Vector4_op_Equality_m6_411 (Object_t * __this /* static, unused */, Vector4_t6_54  ___lhs, Vector4_t6_54  ___rhs, const MethodInfo* method)
{
	{
		Vector4_t6_54  L_0 = ___lhs;
		Vector4_t6_54  L_1 = ___rhs;
		Vector4_t6_54  L_2 = Vector4_op_Subtraction_m6_409(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = Vector4_SqrMagnitude_m6_406(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return ((((float)L_3) < ((float)(9.99999944E-11f)))? 1 : 0);
	}
}
// System.Void UnityEngine.Ray::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" void Ray__ctor_m6_412 (Ray_t6_55 * __this, Vector3_t6_48  ___origin, Vector3_t6_48  ___direction, const MethodInfo* method)
{
	{
		Vector3_t6_48  L_0 = ___origin;
		__this->___m_Origin_0 = L_0;
		Vector3_t6_48  L_1 = Vector3_get_normalized_m6_245((&___direction), /*hidden argument*/NULL);
		__this->___m_Direction_1 = L_1;
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Ray::get_origin()
extern "C" Vector3_t6_48  Ray_get_origin_m6_413 (Ray_t6_55 * __this, const MethodInfo* method)
{
	{
		Vector3_t6_48  L_0 = (__this->___m_Origin_0);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Ray::get_direction()
extern "C" Vector3_t6_48  Ray_get_direction_m6_414 (Ray_t6_55 * __this, const MethodInfo* method)
{
	{
		Vector3_t6_48  L_0 = (__this->___m_Direction_1);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Ray::GetPoint(System.Single)
extern "C" Vector3_t6_48  Ray_GetPoint_m6_415 (Ray_t6_55 * __this, float ___distance, const MethodInfo* method)
{
	{
		Vector3_t6_48  L_0 = (__this->___m_Origin_0);
		Vector3_t6_48  L_1 = (__this->___m_Direction_1);
		float L_2 = ___distance;
		Vector3_t6_48  L_3 = Vector3_op_Multiply_m6_265(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Vector3_t6_48  L_4 = Vector3_op_Addition_m6_263(NULL /*static, unused*/, L_0, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.String UnityEngine.Ray::ToString()
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* Vector3_t6_48_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4708;
extern "C" String_t* Ray_ToString_m6_416 (Ray_t6_55 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Vector3_t6_48_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1590);
		_stringLiteral4708 = il2cpp_codegen_string_literal_from_index(4708);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_272* L_0 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 2));
		Vector3_t6_48  L_1 = (__this->___m_Origin_0);
		Vector3_t6_48  L_2 = L_1;
		Object_t * L_3 = Box(Vector3_t6_48_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_3;
		ObjectU5BU5D_t1_272* L_4 = L_0;
		Vector3_t6_48  L_5 = (__this->___m_Direction_1);
		Vector3_t6_48  L_6 = L_5;
		Object_t * L_7 = Box(Vector3_t6_48_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1, sizeof(Object_t *))) = (Object_t *)L_7;
		String_t* L_8 = UnityString_Format_m6_573(NULL /*static, unused*/, _stringLiteral4708, L_4, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Void UnityEngine.Plane::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" void Plane__ctor_m6_417 (Plane_t6_56 * __this, Vector3_t6_48  ___inNormal, Vector3_t6_48  ___inPoint, const MethodInfo* method)
{
	{
		Vector3_t6_48  L_0 = ___inNormal;
		Vector3_t6_48  L_1 = Vector3_Normalize_m6_244(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->___m_Normal_0 = L_1;
		Vector3_t6_48  L_2 = ___inNormal;
		Vector3_t6_48  L_3 = ___inPoint;
		float L_4 = Vector3_Dot_m6_248(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		__this->___m_Distance_1 = ((-L_4));
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Plane::get_normal()
extern "C" Vector3_t6_48  Plane_get_normal_m6_418 (Plane_t6_56 * __this, const MethodInfo* method)
{
	{
		Vector3_t6_48  L_0 = (__this->___m_Normal_0);
		return L_0;
	}
}
// System.Single UnityEngine.Plane::get_distance()
extern "C" float Plane_get_distance_m6_419 (Plane_t6_56 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Distance_1);
		return L_0;
	}
}
// System.Boolean UnityEngine.Plane::Raycast(UnityEngine.Ray,System.Single&)
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" bool Plane_Raycast_m6_420 (Plane_t6_56 * __this, Ray_t6_55  ___ray, float* ___enter, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1589);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		Vector3_t6_48  L_0 = Ray_get_direction_m6_414((&___ray), /*hidden argument*/NULL);
		Vector3_t6_48  L_1 = Plane_get_normal_m6_418(__this, /*hidden argument*/NULL);
		float L_2 = Vector3_Dot_m6_248(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Vector3_t6_48  L_3 = Ray_get_origin_m6_413((&___ray), /*hidden argument*/NULL);
		Vector3_t6_48  L_4 = Plane_get_normal_m6_418(__this, /*hidden argument*/NULL);
		float L_5 = Vector3_Dot_m6_248(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		float L_6 = Plane_get_distance_m6_419(__this, /*hidden argument*/NULL);
		V_1 = ((float)((float)((-L_5))-(float)L_6));
		float L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		bool L_8 = Mathf_Approximately_m6_443(NULL /*static, unused*/, L_7, (0.0f), /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0047;
		}
	}
	{
		float* L_9 = ___enter;
		*((float*)(L_9)) = (float)(0.0f);
		return 0;
	}

IL_0047:
	{
		float* L_10 = ___enter;
		float L_11 = V_1;
		float L_12 = V_0;
		*((float*)(L_10)) = (float)((float)((float)L_11/(float)L_12));
		float* L_13 = ___enter;
		return ((((float)(*((float*)L_13))) > ((float)(0.0f)))? 1 : 0);
	}
}
// System.Void UnityEngineInternal.MathfInternal::.cctor()
extern TypeInfo* MathfInternal_t6_57_il2cpp_TypeInfo_var;
extern "C" void MathfInternal__cctor_m6_421 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MathfInternal_t6_57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1596);
		s_Il2CppMethodIntialized = true;
	}
	{
		il2cpp_codegen_memory_barrier();
		((MathfInternal_t6_57_StaticFields*)MathfInternal_t6_57_il2cpp_TypeInfo_var->static_fields)->___FloatMinNormal_0 = (1.17549435E-38f);
		il2cpp_codegen_memory_barrier();
		((MathfInternal_t6_57_StaticFields*)MathfInternal_t6_57_il2cpp_TypeInfo_var->static_fields)->___FloatMinDenormal_1 = (1.401298E-45f);
		((MathfInternal_t6_57_StaticFields*)MathfInternal_t6_57_il2cpp_TypeInfo_var->static_fields)->___IsFlushToZeroEnabled_2 = 1;
		return;
	}
}
// System.Void UnityEngine.Mathf::.cctor()
extern TypeInfo* MathfInternal_t6_57_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" void Mathf__cctor_m6_422 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MathfInternal_t6_57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1596);
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1589);
		s_Il2CppMethodIntialized = true;
	}
	float G_B3_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(MathfInternal_t6_57_il2cpp_TypeInfo_var);
		bool L_0 = ((MathfInternal_t6_57_StaticFields*)MathfInternal_t6_57_il2cpp_TypeInfo_var->static_fields)->___IsFlushToZeroEnabled_2;
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MathfInternal_t6_57_il2cpp_TypeInfo_var);
		float L_1 = ((MathfInternal_t6_57_StaticFields*)MathfInternal_t6_57_il2cpp_TypeInfo_var->static_fields)->___FloatMinNormal_0;
		il2cpp_codegen_memory_barrier();
		G_B3_0 = L_1;
		goto IL_001d;
	}

IL_0016:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MathfInternal_t6_57_il2cpp_TypeInfo_var);
		float L_2 = ((MathfInternal_t6_57_StaticFields*)MathfInternal_t6_57_il2cpp_TypeInfo_var->static_fields)->___FloatMinDenormal_1;
		il2cpp_codegen_memory_barrier();
		G_B3_0 = L_2;
	}

IL_001d:
	{
		((Mathf_t6_58_StaticFields*)Mathf_t6_58_il2cpp_TypeInfo_var->static_fields)->___Epsilon_0 = G_B3_0;
		return;
	}
}
// System.Single UnityEngine.Mathf::Sin(System.Single)
extern "C" float Mathf_Sin_m6_423 (Object_t * __this /* static, unused */, float ___f, const MethodInfo* method)
{
	{
		float L_0 = ___f;
		double L_1 = sin((((double)((double)L_0))));
		return (((float)((float)L_1)));
	}
}
// System.Single UnityEngine.Mathf::Cos(System.Single)
extern "C" float Mathf_Cos_m6_424 (Object_t * __this /* static, unused */, float ___f, const MethodInfo* method)
{
	{
		float L_0 = ___f;
		double L_1 = cos((((double)((double)L_0))));
		return (((float)((float)L_1)));
	}
}
// System.Single UnityEngine.Mathf::Sqrt(System.Single)
extern "C" float Mathf_Sqrt_m6_425 (Object_t * __this /* static, unused */, float ___f, const MethodInfo* method)
{
	{
		float L_0 = ___f;
		double L_1 = sqrt((((double)((double)L_0))));
		return (((float)((float)L_1)));
	}
}
// System.Single UnityEngine.Mathf::Abs(System.Single)
extern "C" float Mathf_Abs_m6_426 (Object_t * __this /* static, unused */, float ___f, const MethodInfo* method)
{
	{
		float L_0 = ___f;
		float L_1 = fabsf(L_0);
		return (((float)((float)L_1)));
	}
}
// System.Single UnityEngine.Mathf::Min(System.Single,System.Single)
extern "C" float Mathf_Min_m6_427 (Object_t * __this /* static, unused */, float ___a, float ___b, const MethodInfo* method)
{
	float G_B3_0 = 0.0f;
	{
		float L_0 = ___a;
		float L_1 = ___b;
		if ((!(((float)L_0) < ((float)L_1))))
		{
			goto IL_000d;
		}
	}
	{
		float L_2 = ___a;
		G_B3_0 = L_2;
		goto IL_000e;
	}

IL_000d:
	{
		float L_3 = ___b;
		G_B3_0 = L_3;
	}

IL_000e:
	{
		return G_B3_0;
	}
}
// System.Int32 UnityEngine.Mathf::Min(System.Int32,System.Int32)
extern "C" int32_t Mathf_Min_m6_428 (Object_t * __this /* static, unused */, int32_t ___a, int32_t ___b, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = ___a;
		int32_t L_1 = ___b;
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_000d;
		}
	}
	{
		int32_t L_2 = ___a;
		G_B3_0 = L_2;
		goto IL_000e;
	}

IL_000d:
	{
		int32_t L_3 = ___b;
		G_B3_0 = L_3;
	}

IL_000e:
	{
		return G_B3_0;
	}
}
// System.Single UnityEngine.Mathf::Max(System.Single,System.Single)
extern "C" float Mathf_Max_m6_429 (Object_t * __this /* static, unused */, float ___a, float ___b, const MethodInfo* method)
{
	float G_B3_0 = 0.0f;
	{
		float L_0 = ___a;
		float L_1 = ___b;
		if ((!(((float)L_0) > ((float)L_1))))
		{
			goto IL_000d;
		}
	}
	{
		float L_2 = ___a;
		G_B3_0 = L_2;
		goto IL_000e;
	}

IL_000d:
	{
		float L_3 = ___b;
		G_B3_0 = L_3;
	}

IL_000e:
	{
		return G_B3_0;
	}
}
// System.Int32 UnityEngine.Mathf::Max(System.Int32,System.Int32)
extern "C" int32_t Mathf_Max_m6_430 (Object_t * __this /* static, unused */, int32_t ___a, int32_t ___b, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = ___a;
		int32_t L_1 = ___b;
		if ((((int32_t)L_0) <= ((int32_t)L_1)))
		{
			goto IL_000d;
		}
	}
	{
		int32_t L_2 = ___a;
		G_B3_0 = L_2;
		goto IL_000e;
	}

IL_000d:
	{
		int32_t L_3 = ___b;
		G_B3_0 = L_3;
	}

IL_000e:
	{
		return G_B3_0;
	}
}
// System.Single UnityEngine.Mathf::Pow(System.Single,System.Single)
extern "C" float Mathf_Pow_m6_431 (Object_t * __this /* static, unused */, float ___f, float ___p, const MethodInfo* method)
{
	{
		float L_0 = ___f;
		float L_1 = ___p;
		double L_2 = pow((((double)((double)L_0))), (((double)((double)L_1))));
		return (((float)((float)L_2)));
	}
}
// System.Single UnityEngine.Mathf::Log(System.Single,System.Single)
extern "C" float Mathf_Log_m6_432 (Object_t * __this /* static, unused */, float ___f, float ___p, const MethodInfo* method)
{
	{
		float L_0 = ___f;
		float L_1 = ___p;
		double L_2 = Math_Log_m1_14202(NULL /*static, unused*/, (((double)((double)L_0))), (((double)((double)L_1))), /*hidden argument*/NULL);
		return (((float)((float)L_2)));
	}
}
// System.Single UnityEngine.Mathf::Floor(System.Single)
extern "C" float Mathf_Floor_m6_433 (Object_t * __this /* static, unused */, float ___f, const MethodInfo* method)
{
	{
		float L_0 = ___f;
		double L_1 = floor((((double)((double)L_0))));
		return (((float)((float)L_1)));
	}
}
// System.Single UnityEngine.Mathf::Round(System.Single)
extern "C" float Mathf_Round_m6_434 (Object_t * __this /* static, unused */, float ___f, const MethodInfo* method)
{
	{
		float L_0 = ___f;
		double L_1 = bankers_round((((double)((double)L_0))));
		return (((float)((float)L_1)));
	}
}
// System.Int32 UnityEngine.Mathf::CeilToInt(System.Single)
extern "C" int32_t Mathf_CeilToInt_m6_435 (Object_t * __this /* static, unused */, float ___f, const MethodInfo* method)
{
	{
		float L_0 = ___f;
		double L_1 = ceil((((double)((double)L_0))));
		return (((int32_t)((int32_t)L_1)));
	}
}
// System.Int32 UnityEngine.Mathf::FloorToInt(System.Single)
extern "C" int32_t Mathf_FloorToInt_m6_436 (Object_t * __this /* static, unused */, float ___f, const MethodInfo* method)
{
	{
		float L_0 = ___f;
		double L_1 = floor((((double)((double)L_0))));
		return (((int32_t)((int32_t)L_1)));
	}
}
// System.Int32 UnityEngine.Mathf::RoundToInt(System.Single)
extern "C" int32_t Mathf_RoundToInt_m6_437 (Object_t * __this /* static, unused */, float ___f, const MethodInfo* method)
{
	{
		float L_0 = ___f;
		double L_1 = bankers_round((((double)((double)L_0))));
		return (((int32_t)((int32_t)L_1)));
	}
}
// System.Single UnityEngine.Mathf::Sign(System.Single)
extern "C" float Mathf_Sign_m6_438 (Object_t * __this /* static, unused */, float ___f, const MethodInfo* method)
{
	float G_B3_0 = 0.0f;
	{
		float L_0 = ___f;
		if ((!(((float)L_0) >= ((float)(0.0f)))))
		{
			goto IL_0015;
		}
	}
	{
		G_B3_0 = (1.0f);
		goto IL_001a;
	}

IL_0015:
	{
		G_B3_0 = (-1.0f);
	}

IL_001a:
	{
		return G_B3_0;
	}
}
// System.Single UnityEngine.Mathf::Clamp(System.Single,System.Single,System.Single)
extern "C" float Mathf_Clamp_m6_439 (Object_t * __this /* static, unused */, float ___value, float ___min, float ___max, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		float L_1 = ___min;
		if ((!(((float)L_0) < ((float)L_1))))
		{
			goto IL_000f;
		}
	}
	{
		float L_2 = ___min;
		___value = L_2;
		goto IL_0019;
	}

IL_000f:
	{
		float L_3 = ___value;
		float L_4 = ___max;
		if ((!(((float)L_3) > ((float)L_4))))
		{
			goto IL_0019;
		}
	}
	{
		float L_5 = ___max;
		___value = L_5;
	}

IL_0019:
	{
		float L_6 = ___value;
		return L_6;
	}
}
// System.Int32 UnityEngine.Mathf::Clamp(System.Int32,System.Int32,System.Int32)
extern "C" int32_t Mathf_Clamp_m6_440 (Object_t * __this /* static, unused */, int32_t ___value, int32_t ___min, int32_t ___max, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		int32_t L_1 = ___min;
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_000f;
		}
	}
	{
		int32_t L_2 = ___min;
		___value = L_2;
		goto IL_0019;
	}

IL_000f:
	{
		int32_t L_3 = ___value;
		int32_t L_4 = ___max;
		if ((((int32_t)L_3) <= ((int32_t)L_4)))
		{
			goto IL_0019;
		}
	}
	{
		int32_t L_5 = ___max;
		___value = L_5;
	}

IL_0019:
	{
		int32_t L_6 = ___value;
		return L_6;
	}
}
// System.Single UnityEngine.Mathf::Clamp01(System.Single)
extern "C" float Mathf_Clamp01_m6_441 (Object_t * __this /* static, unused */, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		if ((!(((float)L_0) < ((float)(0.0f)))))
		{
			goto IL_0011;
		}
	}
	{
		return (0.0f);
	}

IL_0011:
	{
		float L_1 = ___value;
		if ((!(((float)L_1) > ((float)(1.0f)))))
		{
			goto IL_0022;
		}
	}
	{
		return (1.0f);
	}

IL_0022:
	{
		float L_2 = ___value;
		return L_2;
	}
}
// System.Single UnityEngine.Mathf::Lerp(System.Single,System.Single,System.Single)
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" float Mathf_Lerp_m6_442 (Object_t * __this /* static, unused */, float ___a, float ___b, float ___t, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1589);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___a;
		float L_1 = ___b;
		float L_2 = ___a;
		float L_3 = ___t;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_4 = Mathf_Clamp01_m6_441(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return ((float)((float)L_0+(float)((float)((float)((float)((float)L_1-(float)L_2))*(float)L_4))));
	}
}
// System.Boolean UnityEngine.Mathf::Approximately(System.Single,System.Single)
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" bool Mathf_Approximately_m6_443 (Object_t * __this /* static, unused */, float ___a, float ___b, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1589);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___b;
		float L_1 = ___a;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_2 = fabsf(((float)((float)L_0-(float)L_1)));
		float L_3 = ___a;
		float L_4 = fabsf(L_3);
		float L_5 = ___b;
		float L_6 = fabsf(L_5);
		float L_7 = Mathf_Max_m6_429(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
		float L_8 = ((Mathf_t6_58_StaticFields*)Mathf_t6_58_il2cpp_TypeInfo_var->static_fields)->___Epsilon_0;
		float L_9 = Mathf_Max_m6_429(NULL /*static, unused*/, ((float)((float)(1.0E-06f)*(float)L_7)), ((float)((float)L_8*(float)(8.0f))), /*hidden argument*/NULL);
		return ((((float)L_2) < ((float)L_9))? 1 : 0);
	}
}
// System.Single UnityEngine.Mathf::SmoothDamp(System.Single,System.Single,System.Single&,System.Single,System.Single,System.Single)
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" float Mathf_SmoothDamp_m6_444 (Object_t * __this /* static, unused */, float ___current, float ___target, float* ___currentVelocity, float ___smoothTime, float ___maxSpeed, float ___deltaTime, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1589);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	{
		float L_0 = ___smoothTime;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Max_m6_429(NULL /*static, unused*/, (0.0001f), L_0, /*hidden argument*/NULL);
		___smoothTime = L_1;
		float L_2 = ___smoothTime;
		V_0 = ((float)((float)(2.0f)/(float)L_2));
		float L_3 = V_0;
		float L_4 = ___deltaTime;
		V_1 = ((float)((float)L_3*(float)L_4));
		float L_5 = V_1;
		float L_6 = V_1;
		float L_7 = V_1;
		float L_8 = V_1;
		float L_9 = V_1;
		float L_10 = V_1;
		V_2 = ((float)((float)(1.0f)/(float)((float)((float)((float)((float)((float)((float)(1.0f)+(float)L_5))+(float)((float)((float)((float)((float)(0.48f)*(float)L_6))*(float)L_7))))+(float)((float)((float)((float)((float)((float)((float)(0.235f)*(float)L_8))*(float)L_9))*(float)L_10))))));
		float L_11 = ___current;
		float L_12 = ___target;
		V_3 = ((float)((float)L_11-(float)L_12));
		float L_13 = ___target;
		V_4 = L_13;
		float L_14 = ___maxSpeed;
		float L_15 = ___smoothTime;
		V_5 = ((float)((float)L_14*(float)L_15));
		float L_16 = V_3;
		float L_17 = V_5;
		float L_18 = V_5;
		float L_19 = Mathf_Clamp_m6_439(NULL /*static, unused*/, L_16, ((-L_17)), L_18, /*hidden argument*/NULL);
		V_3 = L_19;
		float L_20 = ___current;
		float L_21 = V_3;
		___target = ((float)((float)L_20-(float)L_21));
		float* L_22 = ___currentVelocity;
		float L_23 = V_0;
		float L_24 = V_3;
		float L_25 = ___deltaTime;
		V_6 = ((float)((float)((float)((float)(*((float*)L_22))+(float)((float)((float)L_23*(float)L_24))))*(float)L_25));
		float* L_26 = ___currentVelocity;
		float* L_27 = ___currentVelocity;
		float L_28 = V_0;
		float L_29 = V_6;
		float L_30 = V_2;
		*((float*)(L_26)) = (float)((float)((float)((float)((float)(*((float*)L_27))-(float)((float)((float)L_28*(float)L_29))))*(float)L_30));
		float L_31 = ___target;
		float L_32 = V_3;
		float L_33 = V_6;
		float L_34 = V_2;
		V_7 = ((float)((float)L_31+(float)((float)((float)((float)((float)L_32+(float)L_33))*(float)L_34))));
		float L_35 = V_4;
		float L_36 = ___current;
		float L_37 = V_7;
		float L_38 = V_4;
		if ((!(((uint32_t)((((float)((float)((float)L_35-(float)L_36))) > ((float)(0.0f)))? 1 : 0)) == ((uint32_t)((((float)L_37) > ((float)L_38))? 1 : 0)))))
		{
			goto IL_00a0;
		}
	}
	{
		float L_39 = V_4;
		V_7 = L_39;
		float* L_40 = ___currentVelocity;
		float L_41 = V_7;
		float L_42 = V_4;
		float L_43 = ___deltaTime;
		*((float*)(L_40)) = (float)((float)((float)((float)((float)L_41-(float)L_42))/(float)L_43));
	}

IL_00a0:
	{
		float L_44 = V_7;
		return L_44;
	}
}
// System.Single UnityEngine.Mathf::Repeat(System.Single,System.Single)
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" float Mathf_Repeat_m6_445 (Object_t * __this /* static, unused */, float ___t, float ___length, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1589);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___t;
		float L_1 = ___t;
		float L_2 = ___length;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_3 = floorf(((float)((float)L_1/(float)L_2)));
		float L_4 = ___length;
		return ((float)((float)L_0-(float)((float)((float)L_3*(float)L_4))));
	}
}
// System.Single UnityEngine.Mathf::InverseLerp(System.Single,System.Single,System.Single)
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" float Mathf_InverseLerp_m6_446 (Object_t * __this /* static, unused */, float ___a, float ___b, float ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1589);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___a;
		float L_1 = ___b;
		if ((((float)L_0) == ((float)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		float L_2 = ___value;
		float L_3 = ___a;
		float L_4 = ___b;
		float L_5 = ___a;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_6 = Mathf_Clamp01_m6_441(NULL /*static, unused*/, ((float)((float)((float)((float)L_2-(float)L_3))/(float)((float)((float)L_4-(float)L_5)))), /*hidden argument*/NULL);
		return L_6;
	}

IL_0014:
	{
		return (0.0f);
	}
}
// System.Void UnityEngine.DrivenRectTransformTracker::Add(UnityEngine.Object,UnityEngine.RectTransform,UnityEngine.DrivenTransformProperties)
extern "C" void DrivenRectTransformTracker_Add_m6_447 (DrivenRectTransformTracker_t6_60 * __this, Object_t6_5 * ___driver, RectTransform_t6_64 * ___rectTransform, int32_t ___drivenProperties, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.DrivenRectTransformTracker::Clear()
extern "C" void DrivenRectTransformTracker_Clear_m6_448 (DrivenRectTransformTracker_t6_60 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::.ctor(System.Object,System.IntPtr)
extern "C" void ReapplyDrivenProperties__ctor_m6_449 (ReapplyDrivenProperties_t6_63 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::Invoke(UnityEngine.RectTransform)
extern "C" void ReapplyDrivenProperties_Invoke_m6_450 (ReapplyDrivenProperties_t6_63 * __this, RectTransform_t6_64 * ___driven, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		ReapplyDrivenProperties_Invoke_m6_450((ReapplyDrivenProperties_t6_63 *)__this->___prev_9,___driven, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, RectTransform_t6_64 * ___driven, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___driven,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, RectTransform_t6_64 * ___driven, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___driven,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(___driven,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_ReapplyDrivenProperties_t6_63(Il2CppObject* delegate, RectTransform_t6_64 * ___driven)
{
	// Marshaling of parameter '___driven' to native representation
	RectTransform_t6_64 * ____driven_marshaled = { 0 };
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'UnityEngine.RectTransform'."));
}
// System.IAsyncResult UnityEngine.RectTransform/ReapplyDrivenProperties::BeginInvoke(UnityEngine.RectTransform,System.AsyncCallback,System.Object)
extern "C" Object_t * ReapplyDrivenProperties_BeginInvoke_m6_451 (ReapplyDrivenProperties_t6_63 * __this, RectTransform_t6_64 * ___driven, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___driven;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::EndInvoke(System.IAsyncResult)
extern "C" void ReapplyDrivenProperties_EndInvoke_m6_452 (ReapplyDrivenProperties_t6_63 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void UnityEngine.RectTransform::add_reapplyDrivenProperties(UnityEngine.RectTransform/ReapplyDrivenProperties)
extern TypeInfo* RectTransform_t6_64_il2cpp_TypeInfo_var;
extern TypeInfo* ReapplyDrivenProperties_t6_63_il2cpp_TypeInfo_var;
extern "C" void RectTransform_add_reapplyDrivenProperties_m6_453 (Object_t * __this /* static, unused */, ReapplyDrivenProperties_t6_63 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t6_64_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1597);
		ReapplyDrivenProperties_t6_63_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1598);
		s_Il2CppMethodIntialized = true;
	}
	{
		ReapplyDrivenProperties_t6_63 * L_0 = ((RectTransform_t6_64_StaticFields*)RectTransform_t6_64_il2cpp_TypeInfo_var->static_fields)->___reapplyDrivenProperties_2;
		ReapplyDrivenProperties_t6_63 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Combine_m1_908(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((RectTransform_t6_64_StaticFields*)RectTransform_t6_64_il2cpp_TypeInfo_var->static_fields)->___reapplyDrivenProperties_2 = ((ReapplyDrivenProperties_t6_63 *)CastclassSealed(L_2, ReapplyDrivenProperties_t6_63_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.RectTransform::remove_reapplyDrivenProperties(UnityEngine.RectTransform/ReapplyDrivenProperties)
extern TypeInfo* RectTransform_t6_64_il2cpp_TypeInfo_var;
extern TypeInfo* ReapplyDrivenProperties_t6_63_il2cpp_TypeInfo_var;
extern "C" void RectTransform_remove_reapplyDrivenProperties_m6_454 (Object_t * __this /* static, unused */, ReapplyDrivenProperties_t6_63 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t6_64_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1597);
		ReapplyDrivenProperties_t6_63_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1598);
		s_Il2CppMethodIntialized = true;
	}
	{
		ReapplyDrivenProperties_t6_63 * L_0 = ((RectTransform_t6_64_StaticFields*)RectTransform_t6_64_il2cpp_TypeInfo_var->static_fields)->___reapplyDrivenProperties_2;
		ReapplyDrivenProperties_t6_63 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Remove_m1_911(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((RectTransform_t6_64_StaticFields*)RectTransform_t6_64_il2cpp_TypeInfo_var->static_fields)->___reapplyDrivenProperties_2 = ((ReapplyDrivenProperties_t6_63 *)CastclassSealed(L_2, ReapplyDrivenProperties_t6_63_il2cpp_TypeInfo_var));
		return;
	}
}
// UnityEngine.Rect UnityEngine.RectTransform::get_rect()
extern "C" Rect_t6_51  RectTransform_get_rect_m6_455 (RectTransform_t6_64 * __this, const MethodInfo* method)
{
	Rect_t6_51  V_0 = {0};
	{
		RectTransform_INTERNAL_get_rect_m6_456(__this, (&V_0), /*hidden argument*/NULL);
		Rect_t6_51  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_rect(UnityEngine.Rect&)
extern "C" void RectTransform_INTERNAL_get_rect_m6_456 (RectTransform_t6_64 * __this, Rect_t6_51 * ___value, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_rect_m6_456_ftn) (RectTransform_t6_64 *, Rect_t6_51 *);
	static RectTransform_INTERNAL_get_rect_m6_456_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_rect_m6_456_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_rect(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchorMin()
extern "C" Vector2_t6_47  RectTransform_get_anchorMin_m6_457 (RectTransform_t6_64 * __this, const MethodInfo* method)
{
	Vector2_t6_47  V_0 = {0};
	{
		RectTransform_INTERNAL_get_anchorMin_m6_459(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t6_47  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RectTransform::set_anchorMin(UnityEngine.Vector2)
extern "C" void RectTransform_set_anchorMin_m6_458 (RectTransform_t6_64 * __this, Vector2_t6_47  ___value, const MethodInfo* method)
{
	{
		RectTransform_INTERNAL_set_anchorMin_m6_460(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchorMin(UnityEngine.Vector2&)
extern "C" void RectTransform_INTERNAL_get_anchorMin_m6_459 (RectTransform_t6_64 * __this, Vector2_t6_47 * ___value, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_anchorMin_m6_459_ftn) (RectTransform_t6_64 *, Vector2_t6_47 *);
	static RectTransform_INTERNAL_get_anchorMin_m6_459_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_anchorMin_m6_459_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_anchorMin(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchorMin(UnityEngine.Vector2&)
extern "C" void RectTransform_INTERNAL_set_anchorMin_m6_460 (RectTransform_t6_64 * __this, Vector2_t6_47 * ___value, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_set_anchorMin_m6_460_ftn) (RectTransform_t6_64 *, Vector2_t6_47 *);
	static RectTransform_INTERNAL_set_anchorMin_m6_460_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_anchorMin_m6_460_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_anchorMin(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchorMax()
extern "C" Vector2_t6_47  RectTransform_get_anchorMax_m6_461 (RectTransform_t6_64 * __this, const MethodInfo* method)
{
	Vector2_t6_47  V_0 = {0};
	{
		RectTransform_INTERNAL_get_anchorMax_m6_463(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t6_47  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RectTransform::set_anchorMax(UnityEngine.Vector2)
extern "C" void RectTransform_set_anchorMax_m6_462 (RectTransform_t6_64 * __this, Vector2_t6_47  ___value, const MethodInfo* method)
{
	{
		RectTransform_INTERNAL_set_anchorMax_m6_464(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchorMax(UnityEngine.Vector2&)
extern "C" void RectTransform_INTERNAL_get_anchorMax_m6_463 (RectTransform_t6_64 * __this, Vector2_t6_47 * ___value, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_anchorMax_m6_463_ftn) (RectTransform_t6_64 *, Vector2_t6_47 *);
	static RectTransform_INTERNAL_get_anchorMax_m6_463_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_anchorMax_m6_463_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_anchorMax(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchorMax(UnityEngine.Vector2&)
extern "C" void RectTransform_INTERNAL_set_anchorMax_m6_464 (RectTransform_t6_64 * __this, Vector2_t6_47 * ___value, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_set_anchorMax_m6_464_ftn) (RectTransform_t6_64 *, Vector2_t6_47 *);
	static RectTransform_INTERNAL_set_anchorMax_m6_464_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_anchorMax_m6_464_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_anchorMax(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchoredPosition()
extern "C" Vector2_t6_47  RectTransform_get_anchoredPosition_m6_465 (RectTransform_t6_64 * __this, const MethodInfo* method)
{
	Vector2_t6_47  V_0 = {0};
	{
		RectTransform_INTERNAL_get_anchoredPosition_m6_467(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t6_47  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RectTransform::set_anchoredPosition(UnityEngine.Vector2)
extern "C" void RectTransform_set_anchoredPosition_m6_466 (RectTransform_t6_64 * __this, Vector2_t6_47  ___value, const MethodInfo* method)
{
	{
		RectTransform_INTERNAL_set_anchoredPosition_m6_468(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchoredPosition(UnityEngine.Vector2&)
extern "C" void RectTransform_INTERNAL_get_anchoredPosition_m6_467 (RectTransform_t6_64 * __this, Vector2_t6_47 * ___value, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_anchoredPosition_m6_467_ftn) (RectTransform_t6_64 *, Vector2_t6_47 *);
	static RectTransform_INTERNAL_get_anchoredPosition_m6_467_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_anchoredPosition_m6_467_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_anchoredPosition(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchoredPosition(UnityEngine.Vector2&)
extern "C" void RectTransform_INTERNAL_set_anchoredPosition_m6_468 (RectTransform_t6_64 * __this, Vector2_t6_47 * ___value, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_set_anchoredPosition_m6_468_ftn) (RectTransform_t6_64 *, Vector2_t6_47 *);
	static RectTransform_INTERNAL_set_anchoredPosition_m6_468_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_anchoredPosition_m6_468_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_anchoredPosition(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_sizeDelta()
extern "C" Vector2_t6_47  RectTransform_get_sizeDelta_m6_469 (RectTransform_t6_64 * __this, const MethodInfo* method)
{
	Vector2_t6_47  V_0 = {0};
	{
		RectTransform_INTERNAL_get_sizeDelta_m6_471(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t6_47  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RectTransform::set_sizeDelta(UnityEngine.Vector2)
extern "C" void RectTransform_set_sizeDelta_m6_470 (RectTransform_t6_64 * __this, Vector2_t6_47  ___value, const MethodInfo* method)
{
	{
		RectTransform_INTERNAL_set_sizeDelta_m6_472(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_sizeDelta(UnityEngine.Vector2&)
extern "C" void RectTransform_INTERNAL_get_sizeDelta_m6_471 (RectTransform_t6_64 * __this, Vector2_t6_47 * ___value, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_sizeDelta_m6_471_ftn) (RectTransform_t6_64 *, Vector2_t6_47 *);
	static RectTransform_INTERNAL_get_sizeDelta_m6_471_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_sizeDelta_m6_471_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_sizeDelta(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_sizeDelta(UnityEngine.Vector2&)
extern "C" void RectTransform_INTERNAL_set_sizeDelta_m6_472 (RectTransform_t6_64 * __this, Vector2_t6_47 * ___value, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_set_sizeDelta_m6_472_ftn) (RectTransform_t6_64 *, Vector2_t6_47 *);
	static RectTransform_INTERNAL_set_sizeDelta_m6_472_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_sizeDelta_m6_472_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_sizeDelta(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_pivot()
extern "C" Vector2_t6_47  RectTransform_get_pivot_m6_473 (RectTransform_t6_64 * __this, const MethodInfo* method)
{
	Vector2_t6_47  V_0 = {0};
	{
		RectTransform_INTERNAL_get_pivot_m6_475(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t6_47  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RectTransform::set_pivot(UnityEngine.Vector2)
extern "C" void RectTransform_set_pivot_m6_474 (RectTransform_t6_64 * __this, Vector2_t6_47  ___value, const MethodInfo* method)
{
	{
		RectTransform_INTERNAL_set_pivot_m6_476(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_pivot(UnityEngine.Vector2&)
extern "C" void RectTransform_INTERNAL_get_pivot_m6_475 (RectTransform_t6_64 * __this, Vector2_t6_47 * ___value, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_pivot_m6_475_ftn) (RectTransform_t6_64 *, Vector2_t6_47 *);
	static RectTransform_INTERNAL_get_pivot_m6_475_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_pivot_m6_475_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_pivot(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_pivot(UnityEngine.Vector2&)
extern "C" void RectTransform_INTERNAL_set_pivot_m6_476 (RectTransform_t6_64 * __this, Vector2_t6_47 * ___value, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_set_pivot_m6_476_ftn) (RectTransform_t6_64 *, Vector2_t6_47 *);
	static RectTransform_INTERNAL_set_pivot_m6_476_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_pivot_m6_476_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_pivot(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.RectTransform::SendReapplyDrivenProperties(UnityEngine.RectTransform)
extern TypeInfo* RectTransform_t6_64_il2cpp_TypeInfo_var;
extern "C" void RectTransform_SendReapplyDrivenProperties_m6_477 (Object_t * __this /* static, unused */, RectTransform_t6_64 * ___driven, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t6_64_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1597);
		s_Il2CppMethodIntialized = true;
	}
	{
		ReapplyDrivenProperties_t6_63 * L_0 = ((RectTransform_t6_64_StaticFields*)RectTransform_t6_64_il2cpp_TypeInfo_var->static_fields)->___reapplyDrivenProperties_2;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		ReapplyDrivenProperties_t6_63 * L_1 = ((RectTransform_t6_64_StaticFields*)RectTransform_t6_64_il2cpp_TypeInfo_var->static_fields)->___reapplyDrivenProperties_2;
		RectTransform_t6_64 * L_2 = ___driven;
		NullCheck(L_1);
		ReapplyDrivenProperties_Invoke_m6_450(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.RectTransform::GetLocalCorners(UnityEngine.Vector3[])
extern Il2CppCodeGenString* _stringLiteral4709;
extern "C" void RectTransform_GetLocalCorners_m6_478 (RectTransform_t6_64 * __this, Vector3U5BU5D_t6_161* ___fourCornersArray, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral4709 = il2cpp_codegen_string_literal_from_index(4709);
		s_Il2CppMethodIntialized = true;
	}
	Rect_t6_51  V_0 = {0};
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	{
		Vector3U5BU5D_t6_161* L_0 = ___fourCornersArray;
		if (!L_0)
		{
			goto IL_000f;
		}
	}
	{
		Vector3U5BU5D_t6_161* L_1 = ___fourCornersArray;
		NullCheck(L_1);
		if ((((int32_t)(((int32_t)((int32_t)(((Array_t *)L_1)->max_length))))) >= ((int32_t)4)))
		{
			goto IL_001a;
		}
	}

IL_000f:
	{
		Debug_LogError_m6_632(NULL /*static, unused*/, _stringLiteral4709, /*hidden argument*/NULL);
		return;
	}

IL_001a:
	{
		Rect_t6_51  L_2 = RectTransform_get_rect_m6_455(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Rect_get_x_m6_298((&V_0), /*hidden argument*/NULL);
		V_1 = L_3;
		float L_4 = Rect_get_y_m6_300((&V_0), /*hidden argument*/NULL);
		V_2 = L_4;
		float L_5 = Rect_get_xMax_m6_315((&V_0), /*hidden argument*/NULL);
		V_3 = L_5;
		float L_6 = Rect_get_yMax_m6_317((&V_0), /*hidden argument*/NULL);
		V_4 = L_6;
		Vector3U5BU5D_t6_161* L_7 = ___fourCornersArray;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		float L_8 = V_1;
		float L_9 = V_2;
		Vector3_t6_48  L_10 = {0};
		Vector3__ctor_m6_237(&L_10, L_8, L_9, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t6_48 *)((Vector3_t6_48 *)(Vector3_t6_48 *)SZArrayLdElema(L_7, 0, sizeof(Vector3_t6_48 )))) = L_10;
		Vector3U5BU5D_t6_161* L_11 = ___fourCornersArray;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 1);
		float L_12 = V_1;
		float L_13 = V_4;
		Vector3_t6_48  L_14 = {0};
		Vector3__ctor_m6_237(&L_14, L_12, L_13, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t6_48 *)((Vector3_t6_48 *)(Vector3_t6_48 *)SZArrayLdElema(L_11, 1, sizeof(Vector3_t6_48 )))) = L_14;
		Vector3U5BU5D_t6_161* L_15 = ___fourCornersArray;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 2);
		float L_16 = V_3;
		float L_17 = V_4;
		Vector3_t6_48  L_18 = {0};
		Vector3__ctor_m6_237(&L_18, L_16, L_17, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t6_48 *)((Vector3_t6_48 *)(Vector3_t6_48 *)SZArrayLdElema(L_15, 2, sizeof(Vector3_t6_48 )))) = L_18;
		Vector3U5BU5D_t6_161* L_19 = ___fourCornersArray;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 3);
		float L_20 = V_3;
		float L_21 = V_2;
		Vector3_t6_48  L_22 = {0};
		Vector3__ctor_m6_237(&L_22, L_20, L_21, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t6_48 *)((Vector3_t6_48 *)(Vector3_t6_48 *)SZArrayLdElema(L_19, 3, sizeof(Vector3_t6_48 )))) = L_22;
		return;
	}
}
// System.Void UnityEngine.RectTransform::GetWorldCorners(UnityEngine.Vector3[])
extern Il2CppCodeGenString* _stringLiteral4710;
extern "C" void RectTransform_GetWorldCorners_m6_479 (RectTransform_t6_64 * __this, Vector3U5BU5D_t6_161* ___fourCornersArray, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral4710 = il2cpp_codegen_string_literal_from_index(4710);
		s_Il2CppMethodIntialized = true;
	}
	Transform_t6_65 * V_0 = {0};
	int32_t V_1 = 0;
	{
		Vector3U5BU5D_t6_161* L_0 = ___fourCornersArray;
		if (!L_0)
		{
			goto IL_000f;
		}
	}
	{
		Vector3U5BU5D_t6_161* L_1 = ___fourCornersArray;
		NullCheck(L_1);
		if ((((int32_t)(((int32_t)((int32_t)(((Array_t *)L_1)->max_length))))) >= ((int32_t)4)))
		{
			goto IL_001a;
		}
	}

IL_000f:
	{
		Debug_LogError_m6_632(NULL /*static, unused*/, _stringLiteral4710, /*hidden argument*/NULL);
		return;
	}

IL_001a:
	{
		Vector3U5BU5D_t6_161* L_2 = ___fourCornersArray;
		RectTransform_GetLocalCorners_m6_478(__this, L_2, /*hidden argument*/NULL);
		Transform_t6_65 * L_3 = Component_get_transform_m6_724(__this, /*hidden argument*/NULL);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0051;
	}

IL_002f:
	{
		Vector3U5BU5D_t6_161* L_4 = ___fourCornersArray;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		Transform_t6_65 * L_6 = V_0;
		Vector3U5BU5D_t6_161* L_7 = ___fourCornersArray;
		int32_t L_8 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		NullCheck(L_6);
		Vector3_t6_48  L_9 = Transform_TransformPoint_m6_779(L_6, (*(Vector3_t6_48 *)((Vector3_t6_48 *)(Vector3_t6_48 *)SZArrayLdElema(L_7, L_8, sizeof(Vector3_t6_48 )))), /*hidden argument*/NULL);
		(*(Vector3_t6_48 *)((Vector3_t6_48 *)(Vector3_t6_48 *)SZArrayLdElema(L_4, L_5, sizeof(Vector3_t6_48 )))) = L_9;
		int32_t L_10 = V_1;
		V_1 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0051:
	{
		int32_t L_11 = V_1;
		if ((((int32_t)L_11) < ((int32_t)4)))
		{
			goto IL_002f;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.RectTransform::set_offsetMin(UnityEngine.Vector2)
extern "C" void RectTransform_set_offsetMin_m6_480 (RectTransform_t6_64 * __this, Vector2_t6_47  ___value, const MethodInfo* method)
{
	Vector2_t6_47  V_0 = {0};
	{
		Vector2_t6_47  L_0 = ___value;
		Vector2_t6_47  L_1 = RectTransform_get_anchoredPosition_m6_465(__this, /*hidden argument*/NULL);
		Vector2_t6_47  L_2 = RectTransform_get_sizeDelta_m6_469(__this, /*hidden argument*/NULL);
		Vector2_t6_47  L_3 = RectTransform_get_pivot_m6_473(__this, /*hidden argument*/NULL);
		Vector2_t6_47  L_4 = Vector2_Scale_m6_218(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		Vector2_t6_47  L_5 = Vector2_op_Subtraction_m6_230(NULL /*static, unused*/, L_1, L_4, /*hidden argument*/NULL);
		Vector2_t6_47  L_6 = Vector2_op_Subtraction_m6_230(NULL /*static, unused*/, L_0, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		Vector2_t6_47  L_7 = RectTransform_get_sizeDelta_m6_469(__this, /*hidden argument*/NULL);
		Vector2_t6_47  L_8 = V_0;
		Vector2_t6_47  L_9 = Vector2_op_Subtraction_m6_230(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		RectTransform_set_sizeDelta_m6_470(__this, L_9, /*hidden argument*/NULL);
		Vector2_t6_47  L_10 = RectTransform_get_anchoredPosition_m6_465(__this, /*hidden argument*/NULL);
		Vector2_t6_47  L_11 = V_0;
		Vector2_t6_47  L_12 = Vector2_get_one_m6_226(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t6_47  L_13 = RectTransform_get_pivot_m6_473(__this, /*hidden argument*/NULL);
		Vector2_t6_47  L_14 = Vector2_op_Subtraction_m6_230(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		Vector2_t6_47  L_15 = Vector2_Scale_m6_218(NULL /*static, unused*/, L_11, L_14, /*hidden argument*/NULL);
		Vector2_t6_47  L_16 = Vector2_op_Addition_m6_229(NULL /*static, unused*/, L_10, L_15, /*hidden argument*/NULL);
		RectTransform_set_anchoredPosition_m6_466(__this, L_16, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::set_offsetMax(UnityEngine.Vector2)
extern "C" void RectTransform_set_offsetMax_m6_481 (RectTransform_t6_64 * __this, Vector2_t6_47  ___value, const MethodInfo* method)
{
	Vector2_t6_47  V_0 = {0};
	{
		Vector2_t6_47  L_0 = ___value;
		Vector2_t6_47  L_1 = RectTransform_get_anchoredPosition_m6_465(__this, /*hidden argument*/NULL);
		Vector2_t6_47  L_2 = RectTransform_get_sizeDelta_m6_469(__this, /*hidden argument*/NULL);
		Vector2_t6_47  L_3 = Vector2_get_one_m6_226(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t6_47  L_4 = RectTransform_get_pivot_m6_473(__this, /*hidden argument*/NULL);
		Vector2_t6_47  L_5 = Vector2_op_Subtraction_m6_230(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		Vector2_t6_47  L_6 = Vector2_Scale_m6_218(NULL /*static, unused*/, L_2, L_5, /*hidden argument*/NULL);
		Vector2_t6_47  L_7 = Vector2_op_Addition_m6_229(NULL /*static, unused*/, L_1, L_6, /*hidden argument*/NULL);
		Vector2_t6_47  L_8 = Vector2_op_Subtraction_m6_230(NULL /*static, unused*/, L_0, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		Vector2_t6_47  L_9 = RectTransform_get_sizeDelta_m6_469(__this, /*hidden argument*/NULL);
		Vector2_t6_47  L_10 = V_0;
		Vector2_t6_47  L_11 = Vector2_op_Addition_m6_229(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		RectTransform_set_sizeDelta_m6_470(__this, L_11, /*hidden argument*/NULL);
		Vector2_t6_47  L_12 = RectTransform_get_anchoredPosition_m6_465(__this, /*hidden argument*/NULL);
		Vector2_t6_47  L_13 = V_0;
		Vector2_t6_47  L_14 = RectTransform_get_pivot_m6_473(__this, /*hidden argument*/NULL);
		Vector2_t6_47  L_15 = Vector2_Scale_m6_218(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		Vector2_t6_47  L_16 = Vector2_op_Addition_m6_229(NULL /*static, unused*/, L_12, L_15, /*hidden argument*/NULL);
		RectTransform_set_anchoredPosition_m6_466(__this, L_16, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::SetInsetAndSizeFromParentEdge(UnityEngine.RectTransform/Edge,System.Single,System.Single)
extern "C" void RectTransform_SetInsetAndSizeFromParentEdge_m6_482 (RectTransform_t6_64 * __this, int32_t ___edge, float ___inset, float ___size, const MethodInfo* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	float V_2 = 0.0f;
	Vector2_t6_47  V_3 = {0};
	Vector2_t6_47  V_4 = {0};
	Vector2_t6_47  V_5 = {0};
	Vector2_t6_47  V_6 = {0};
	Vector2_t6_47  V_7 = {0};
	int32_t G_B4_0 = 0;
	int32_t G_B7_0 = 0;
	int32_t G_B10_0 = 0;
	int32_t G_B12_0 = 0;
	Vector2_t6_47 * G_B12_1 = {0};
	int32_t G_B11_0 = 0;
	Vector2_t6_47 * G_B11_1 = {0};
	float G_B13_0 = 0.0f;
	int32_t G_B13_1 = 0;
	Vector2_t6_47 * G_B13_2 = {0};
	{
		int32_t L_0 = ___edge;
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_1 = ___edge;
		if ((!(((uint32_t)L_1) == ((uint32_t)3))))
		{
			goto IL_0014;
		}
	}

IL_000e:
	{
		G_B4_0 = 1;
		goto IL_0015;
	}

IL_0014:
	{
		G_B4_0 = 0;
	}

IL_0015:
	{
		V_0 = G_B4_0;
		int32_t L_2 = ___edge;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0023;
		}
	}
	{
		int32_t L_3 = ___edge;
		G_B7_0 = ((((int32_t)L_3) == ((int32_t)1))? 1 : 0);
		goto IL_0024;
	}

IL_0023:
	{
		G_B7_0 = 1;
	}

IL_0024:
	{
		V_1 = G_B7_0;
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_0031;
		}
	}
	{
		G_B10_0 = 1;
		goto IL_0032;
	}

IL_0031:
	{
		G_B10_0 = 0;
	}

IL_0032:
	{
		V_2 = (((float)((float)G_B10_0)));
		Vector2_t6_47  L_5 = RectTransform_get_anchorMin_m6_457(__this, /*hidden argument*/NULL);
		V_3 = L_5;
		int32_t L_6 = V_0;
		float L_7 = V_2;
		Vector2_set_Item_m6_217((&V_3), L_6, L_7, /*hidden argument*/NULL);
		Vector2_t6_47  L_8 = V_3;
		RectTransform_set_anchorMin_m6_458(__this, L_8, /*hidden argument*/NULL);
		Vector2_t6_47  L_9 = RectTransform_get_anchorMax_m6_461(__this, /*hidden argument*/NULL);
		V_3 = L_9;
		int32_t L_10 = V_0;
		float L_11 = V_2;
		Vector2_set_Item_m6_217((&V_3), L_10, L_11, /*hidden argument*/NULL);
		Vector2_t6_47  L_12 = V_3;
		RectTransform_set_anchorMax_m6_462(__this, L_12, /*hidden argument*/NULL);
		Vector2_t6_47  L_13 = RectTransform_get_sizeDelta_m6_469(__this, /*hidden argument*/NULL);
		V_4 = L_13;
		int32_t L_14 = V_0;
		float L_15 = ___size;
		Vector2_set_Item_m6_217((&V_4), L_14, L_15, /*hidden argument*/NULL);
		Vector2_t6_47  L_16 = V_4;
		RectTransform_set_sizeDelta_m6_470(__this, L_16, /*hidden argument*/NULL);
		Vector2_t6_47  L_17 = RectTransform_get_anchoredPosition_m6_465(__this, /*hidden argument*/NULL);
		V_5 = L_17;
		int32_t L_18 = V_0;
		bool L_19 = V_1;
		G_B11_0 = L_18;
		G_B11_1 = (&V_5);
		if (!L_19)
		{
			G_B12_0 = L_18;
			G_B12_1 = (&V_5);
			goto IL_00ac;
		}
	}
	{
		float L_20 = ___inset;
		float L_21 = ___size;
		Vector2_t6_47  L_22 = RectTransform_get_pivot_m6_473(__this, /*hidden argument*/NULL);
		V_6 = L_22;
		int32_t L_23 = V_0;
		float L_24 = Vector2_get_Item_m6_216((&V_6), L_23, /*hidden argument*/NULL);
		G_B13_0 = ((float)((float)((-L_20))-(float)((float)((float)L_21*(float)((float)((float)(1.0f)-(float)L_24))))));
		G_B13_1 = G_B11_0;
		G_B13_2 = G_B11_1;
		goto IL_00c0;
	}

IL_00ac:
	{
		float L_25 = ___inset;
		float L_26 = ___size;
		Vector2_t6_47  L_27 = RectTransform_get_pivot_m6_473(__this, /*hidden argument*/NULL);
		V_7 = L_27;
		int32_t L_28 = V_0;
		float L_29 = Vector2_get_Item_m6_216((&V_7), L_28, /*hidden argument*/NULL);
		G_B13_0 = ((float)((float)L_25+(float)((float)((float)L_26*(float)L_29))));
		G_B13_1 = G_B12_0;
		G_B13_2 = G_B12_1;
	}

IL_00c0:
	{
		Vector2_set_Item_m6_217(G_B13_2, G_B13_1, G_B13_0, /*hidden argument*/NULL);
		Vector2_t6_47  L_30 = V_5;
		RectTransform_set_anchoredPosition_m6_466(__this, L_30, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::SetSizeWithCurrentAnchors(UnityEngine.RectTransform/Axis,System.Single)
extern "C" void RectTransform_SetSizeWithCurrentAnchors_m6_483 (RectTransform_t6_64 * __this, int32_t ___axis, float ___size, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Vector2_t6_47  V_1 = {0};
	Vector2_t6_47  V_2 = {0};
	Vector2_t6_47  V_3 = {0};
	Vector2_t6_47  V_4 = {0};
	{
		int32_t L_0 = ___axis;
		V_0 = L_0;
		Vector2_t6_47  L_1 = RectTransform_get_sizeDelta_m6_469(__this, /*hidden argument*/NULL);
		V_1 = L_1;
		int32_t L_2 = V_0;
		float L_3 = ___size;
		Vector2_t6_47  L_4 = RectTransform_GetParentSize_m6_484(__this, /*hidden argument*/NULL);
		V_2 = L_4;
		int32_t L_5 = V_0;
		float L_6 = Vector2_get_Item_m6_216((&V_2), L_5, /*hidden argument*/NULL);
		Vector2_t6_47  L_7 = RectTransform_get_anchorMax_m6_461(__this, /*hidden argument*/NULL);
		V_3 = L_7;
		int32_t L_8 = V_0;
		float L_9 = Vector2_get_Item_m6_216((&V_3), L_8, /*hidden argument*/NULL);
		Vector2_t6_47  L_10 = RectTransform_get_anchorMin_m6_457(__this, /*hidden argument*/NULL);
		V_4 = L_10;
		int32_t L_11 = V_0;
		float L_12 = Vector2_get_Item_m6_216((&V_4), L_11, /*hidden argument*/NULL);
		Vector2_set_Item_m6_217((&V_1), L_2, ((float)((float)L_3-(float)((float)((float)L_6*(float)((float)((float)L_9-(float)L_12)))))), /*hidden argument*/NULL);
		Vector2_t6_47  L_13 = V_1;
		RectTransform_set_sizeDelta_m6_470(__this, L_13, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.RectTransform::GetParentSize()
extern TypeInfo* RectTransform_t6_64_il2cpp_TypeInfo_var;
extern "C" Vector2_t6_47  RectTransform_GetParentSize_m6_484 (RectTransform_t6_64 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t6_64_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1597);
		s_Il2CppMethodIntialized = true;
	}
	RectTransform_t6_64 * V_0 = {0};
	Rect_t6_51  V_1 = {0};
	{
		Transform_t6_65 * L_0 = Transform_get_parent_m6_771(__this, /*hidden argument*/NULL);
		V_0 = ((RectTransform_t6_64 *)IsInstSealed(L_0, RectTransform_t6_64_il2cpp_TypeInfo_var));
		RectTransform_t6_64 * L_1 = V_0;
		bool L_2 = Object_op_Implicit_m6_720(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_001d;
		}
	}
	{
		Vector2_t6_47  L_3 = Vector2_get_zero_m6_225(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_3;
	}

IL_001d:
	{
		RectTransform_t6_64 * L_4 = V_0;
		NullCheck(L_4);
		Rect_t6_51  L_5 = RectTransform_get_rect_m6_455(L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		Vector2_t6_47  L_6 = Rect_get_size_m6_310((&V_1), /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void UnityEngine.ResourceRequest::.ctor()
extern "C" void ResourceRequest__ctor_m6_485 (ResourceRequest_t6_66 * __this, const MethodInfo* method)
{
	{
		AsyncOperation__ctor_m6_574(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object UnityEngine.ResourceRequest::get_asset()
extern "C" Object_t6_5 * ResourceRequest_get_asset_m6_486 (ResourceRequest_t6_66 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_Path_1);
		Type_t * L_1 = (__this->___m_Type_2);
		Object_t6_5 * L_2 = Resources_Load_m6_488(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Object UnityEngine.Resources::Load(System.String)
extern const Il2CppType* Object_t6_5_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern "C" Object_t6_5 * Resources_Load_m6_487 (Object_t * __this /* static, unused */, String_t* ___path, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Object_t6_5_0_0_0_var = il2cpp_codegen_type_from_index(1599);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___path;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(Object_t6_5_0_0_0_var), /*hidden argument*/NULL);
		Object_t6_5 * L_2 = Resources_Load_m6_488(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Object UnityEngine.Resources::Load(System.String,System.Type)
extern "C" Object_t6_5 * Resources_Load_m6_488 (Object_t * __this /* static, unused */, String_t* ___path, Type_t * ___systemTypeInstance, const MethodInfo* method)
{
	typedef Object_t6_5 * (*Resources_Load_m6_488_ftn) (String_t*, Type_t *);
	static Resources_Load_m6_488_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Resources_Load_m6_488_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Resources::Load(System.String,System.Type)");
	return _il2cpp_icall_func(___path, ___systemTypeInstance);
}
// System.String UnityEngine.TextAsset::get_text()
extern "C" String_t* TextAsset_get_text_m6_489 (TextAsset_t6_68 * __this, const MethodInfo* method)
{
	typedef String_t* (*TextAsset_get_text_m6_489_ftn) (TextAsset_t6_68 *);
	static TextAsset_get_text_m6_489_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextAsset_get_text_m6_489_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextAsset::get_text()");
	return _il2cpp_icall_func(__this);
}
// System.String UnityEngine.TextAsset::ToString()
extern "C" String_t* TextAsset_ToString_m6_490 (TextAsset_t6_68 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = TextAsset_get_text_m6_489(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.SerializePrivateVariables::.ctor()
extern "C" void SerializePrivateVariables__ctor_m6_491 (SerializePrivateVariables_t6_69 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SerializeField::.ctor()
extern "C" void SerializeField__ctor_m6_492 (SerializeField_t6_70 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Shader UnityEngine.Shader::Find(System.String)
extern "C" Shader_t6_71 * Shader_Find_m6_493 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	typedef Shader_t6_71 * (*Shader_Find_m6_493_ftn) (String_t*);
	static Shader_Find_m6_493_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Shader_Find_m6_493_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Shader::Find(System.String)");
	return _il2cpp_icall_func(___name);
}
// System.Int32 UnityEngine.Shader::PropertyToID(System.String)
extern "C" int32_t Shader_PropertyToID_m6_494 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	typedef int32_t (*Shader_PropertyToID_m6_494_ftn) (String_t*);
	static Shader_PropertyToID_m6_494_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Shader_PropertyToID_m6_494_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Shader::PropertyToID(System.String)");
	return _il2cpp_icall_func(___name);
}
// System.Void UnityEngine.Material::.ctor(UnityEngine.Material)
extern "C" void Material__ctor_m6_495 (Material_t6_72 * __this, Material_t6_72 * ___source, const MethodInfo* method)
{
	{
		Object__ctor_m6_701(__this, /*hidden argument*/NULL);
		Material_t6_72 * L_0 = ___source;
		Material_Internal_CreateWithMaterial_m6_508(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Shader UnityEngine.Material::get_shader()
extern "C" Shader_t6_71 * Material_get_shader_m6_496 (Material_t6_72 * __this, const MethodInfo* method)
{
	typedef Shader_t6_71 * (*Material_get_shader_m6_496_ftn) (Material_t6_72 *);
	static Material_get_shader_m6_496_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_get_shader_m6_496_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::get_shader()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Texture UnityEngine.Material::get_mainTexture()
extern Il2CppCodeGenString* _stringLiteral4711;
extern "C" Texture_t6_32 * Material_get_mainTexture_m6_497 (Material_t6_72 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral4711 = il2cpp_codegen_string_literal_from_index(4711);
		s_Il2CppMethodIntialized = true;
	}
	{
		Texture_t6_32 * L_0 = Material_GetTexture_m6_501(__this, _stringLiteral4711, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.Material::set_mainTexture(UnityEngine.Texture)
extern Il2CppCodeGenString* _stringLiteral4711;
extern "C" void Material_set_mainTexture_m6_498 (Material_t6_72 * __this, Texture_t6_32 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral4711 = il2cpp_codegen_string_literal_from_index(4711);
		s_Il2CppMethodIntialized = true;
	}
	{
		Texture_t6_32 * L_0 = ___value;
		Material_SetTexture_m6_499(__this, _stringLiteral4711, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::SetTexture(System.String,UnityEngine.Texture)
extern "C" void Material_SetTexture_m6_499 (Material_t6_72 * __this, String_t* ___propertyName, Texture_t6_32 * ___texture, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName;
		int32_t L_1 = Shader_PropertyToID_m6_494(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Texture_t6_32 * L_2 = ___texture;
		Material_SetTexture_m6_500(__this, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::SetTexture(System.Int32,UnityEngine.Texture)
extern "C" void Material_SetTexture_m6_500 (Material_t6_72 * __this, int32_t ___nameID, Texture_t6_32 * ___texture, const MethodInfo* method)
{
	typedef void (*Material_SetTexture_m6_500_ftn) (Material_t6_72 *, int32_t, Texture_t6_32 *);
	static Material_SetTexture_m6_500_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_SetTexture_m6_500_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::SetTexture(System.Int32,UnityEngine.Texture)");
	_il2cpp_icall_func(__this, ___nameID, ___texture);
}
// UnityEngine.Texture UnityEngine.Material::GetTexture(System.String)
extern "C" Texture_t6_32 * Material_GetTexture_m6_501 (Material_t6_72 * __this, String_t* ___propertyName, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName;
		int32_t L_1 = Shader_PropertyToID_m6_494(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Texture_t6_32 * L_2 = Material_GetTexture_m6_502(__this, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Texture UnityEngine.Material::GetTexture(System.Int32)
extern "C" Texture_t6_32 * Material_GetTexture_m6_502 (Material_t6_72 * __this, int32_t ___nameID, const MethodInfo* method)
{
	typedef Texture_t6_32 * (*Material_GetTexture_m6_502_ftn) (Material_t6_72 *, int32_t);
	static Material_GetTexture_m6_502_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_GetTexture_m6_502_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::GetTexture(System.Int32)");
	return _il2cpp_icall_func(__this, ___nameID);
}
// System.Void UnityEngine.Material::SetFloat(System.String,System.Single)
extern "C" void Material_SetFloat_m6_503 (Material_t6_72 * __this, String_t* ___propertyName, float ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName;
		int32_t L_1 = Shader_PropertyToID_m6_494(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		float L_2 = ___value;
		Material_SetFloat_m6_504(__this, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::SetFloat(System.Int32,System.Single)
extern "C" void Material_SetFloat_m6_504 (Material_t6_72 * __this, int32_t ___nameID, float ___value, const MethodInfo* method)
{
	typedef void (*Material_SetFloat_m6_504_ftn) (Material_t6_72 *, int32_t, float);
	static Material_SetFloat_m6_504_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_SetFloat_m6_504_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::SetFloat(System.Int32,System.Single)");
	_il2cpp_icall_func(__this, ___nameID, ___value);
}
// System.Void UnityEngine.Material::SetInt(System.String,System.Int32)
extern "C" void Material_SetInt_m6_505 (Material_t6_72 * __this, String_t* ___propertyName, int32_t ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName;
		int32_t L_1 = ___value;
		Material_SetFloat_m6_503(__this, L_0, (((float)((float)L_1))), /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Material::HasProperty(System.String)
extern "C" bool Material_HasProperty_m6_506 (Material_t6_72 * __this, String_t* ___propertyName, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName;
		int32_t L_1 = Shader_PropertyToID_m6_494(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		bool L_2 = Material_HasProperty_m6_507(__this, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.Material::HasProperty(System.Int32)
extern "C" bool Material_HasProperty_m6_507 (Material_t6_72 * __this, int32_t ___nameID, const MethodInfo* method)
{
	typedef bool (*Material_HasProperty_m6_507_ftn) (Material_t6_72 *, int32_t);
	static Material_HasProperty_m6_507_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_HasProperty_m6_507_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::HasProperty(System.Int32)");
	return _il2cpp_icall_func(__this, ___nameID);
}
// System.Void UnityEngine.Material::Internal_CreateWithMaterial(UnityEngine.Material,UnityEngine.Material)
extern "C" void Material_Internal_CreateWithMaterial_m6_508 (Object_t * __this /* static, unused */, Material_t6_72 * ___mono, Material_t6_72 * ___source, const MethodInfo* method)
{
	typedef void (*Material_Internal_CreateWithMaterial_m6_508_ftn) (Material_t6_72 *, Material_t6_72 *);
	static Material_Internal_CreateWithMaterial_m6_508_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_Internal_CreateWithMaterial_m6_508_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::Internal_CreateWithMaterial(UnityEngine.Material,UnityEngine.Material)");
	_il2cpp_icall_func(___mono, ___source);
}
// System.Int32 UnityEngine.SortingLayer::GetLayerValueFromID(System.Int32)
extern "C" int32_t SortingLayer_GetLayerValueFromID_m6_509 (Object_t * __this /* static, unused */, int32_t ___id, const MethodInfo* method)
{
	typedef int32_t (*SortingLayer_GetLayerValueFromID_m6_509_ftn) (int32_t);
	static SortingLayer_GetLayerValueFromID_m6_509_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SortingLayer_GetLayerValueFromID_m6_509_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SortingLayer::GetLayerValueFromID(System.Int32)");
	return _il2cpp_icall_func(___id);
}
// System.Void UnityEngine.Rendering.SphericalHarmonicsL2::Clear()
extern "C" void SphericalHarmonicsL2_Clear_m6_510 (SphericalHarmonicsL2_t6_74 * __this, const MethodInfo* method)
{
	{
		SphericalHarmonicsL2_ClearInternal_m6_511(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rendering.SphericalHarmonicsL2::ClearInternal(UnityEngine.Rendering.SphericalHarmonicsL2&)
extern "C" void SphericalHarmonicsL2_ClearInternal_m6_511 (Object_t * __this /* static, unused */, SphericalHarmonicsL2_t6_74 * ___sh, const MethodInfo* method)
{
	{
		SphericalHarmonicsL2_t6_74 * L_0 = ___sh;
		SphericalHarmonicsL2_INTERNAL_CALL_ClearInternal_m6_512(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rendering.SphericalHarmonicsL2::INTERNAL_CALL_ClearInternal(UnityEngine.Rendering.SphericalHarmonicsL2&)
extern "C" void SphericalHarmonicsL2_INTERNAL_CALL_ClearInternal_m6_512 (Object_t * __this /* static, unused */, SphericalHarmonicsL2_t6_74 * ___sh, const MethodInfo* method)
{
	typedef void (*SphericalHarmonicsL2_INTERNAL_CALL_ClearInternal_m6_512_ftn) (SphericalHarmonicsL2_t6_74 *);
	static SphericalHarmonicsL2_INTERNAL_CALL_ClearInternal_m6_512_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SphericalHarmonicsL2_INTERNAL_CALL_ClearInternal_m6_512_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rendering.SphericalHarmonicsL2::INTERNAL_CALL_ClearInternal(UnityEngine.Rendering.SphericalHarmonicsL2&)");
	_il2cpp_icall_func(___sh);
}
// System.Void UnityEngine.Rendering.SphericalHarmonicsL2::AddAmbientLight(UnityEngine.Color)
extern "C" void SphericalHarmonicsL2_AddAmbientLight_m6_513 (SphericalHarmonicsL2_t6_74 * __this, Color_t6_40  ___color, const MethodInfo* method)
{
	{
		Color_t6_40  L_0 = ___color;
		SphericalHarmonicsL2_AddAmbientLightInternal_m6_514(NULL /*static, unused*/, L_0, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rendering.SphericalHarmonicsL2::AddAmbientLightInternal(UnityEngine.Color,UnityEngine.Rendering.SphericalHarmonicsL2&)
extern "C" void SphericalHarmonicsL2_AddAmbientLightInternal_m6_514 (Object_t * __this /* static, unused */, Color_t6_40  ___color, SphericalHarmonicsL2_t6_74 * ___sh, const MethodInfo* method)
{
	{
		SphericalHarmonicsL2_t6_74 * L_0 = ___sh;
		SphericalHarmonicsL2_INTERNAL_CALL_AddAmbientLightInternal_m6_515(NULL /*static, unused*/, (&___color), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rendering.SphericalHarmonicsL2::INTERNAL_CALL_AddAmbientLightInternal(UnityEngine.Color&,UnityEngine.Rendering.SphericalHarmonicsL2&)
extern "C" void SphericalHarmonicsL2_INTERNAL_CALL_AddAmbientLightInternal_m6_515 (Object_t * __this /* static, unused */, Color_t6_40 * ___color, SphericalHarmonicsL2_t6_74 * ___sh, const MethodInfo* method)
{
	typedef void (*SphericalHarmonicsL2_INTERNAL_CALL_AddAmbientLightInternal_m6_515_ftn) (Color_t6_40 *, SphericalHarmonicsL2_t6_74 *);
	static SphericalHarmonicsL2_INTERNAL_CALL_AddAmbientLightInternal_m6_515_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SphericalHarmonicsL2_INTERNAL_CALL_AddAmbientLightInternal_m6_515_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rendering.SphericalHarmonicsL2::INTERNAL_CALL_AddAmbientLightInternal(UnityEngine.Color&,UnityEngine.Rendering.SphericalHarmonicsL2&)");
	_il2cpp_icall_func(___color, ___sh);
}
// System.Void UnityEngine.Rendering.SphericalHarmonicsL2::AddDirectionalLight(UnityEngine.Vector3,UnityEngine.Color,System.Single)
extern "C" void SphericalHarmonicsL2_AddDirectionalLight_m6_516 (SphericalHarmonicsL2_t6_74 * __this, Vector3_t6_48  ___direction, Color_t6_40  ___color, float ___intensity, const MethodInfo* method)
{
	Color_t6_40  V_0 = {0};
	{
		Color_t6_40  L_0 = ___color;
		float L_1 = ___intensity;
		Color_t6_40  L_2 = Color_op_Multiply_m6_279(NULL /*static, unused*/, L_0, ((float)((float)(2.0f)*(float)L_1)), /*hidden argument*/NULL);
		V_0 = L_2;
		Vector3_t6_48  L_3 = ___direction;
		Color_t6_40  L_4 = V_0;
		SphericalHarmonicsL2_AddDirectionalLightInternal_m6_517(NULL /*static, unused*/, L_3, L_4, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rendering.SphericalHarmonicsL2::AddDirectionalLightInternal(UnityEngine.Vector3,UnityEngine.Color,UnityEngine.Rendering.SphericalHarmonicsL2&)
extern "C" void SphericalHarmonicsL2_AddDirectionalLightInternal_m6_517 (Object_t * __this /* static, unused */, Vector3_t6_48  ___direction, Color_t6_40  ___color, SphericalHarmonicsL2_t6_74 * ___sh, const MethodInfo* method)
{
	{
		SphericalHarmonicsL2_t6_74 * L_0 = ___sh;
		SphericalHarmonicsL2_INTERNAL_CALL_AddDirectionalLightInternal_m6_518(NULL /*static, unused*/, (&___direction), (&___color), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rendering.SphericalHarmonicsL2::INTERNAL_CALL_AddDirectionalLightInternal(UnityEngine.Vector3&,UnityEngine.Color&,UnityEngine.Rendering.SphericalHarmonicsL2&)
extern "C" void SphericalHarmonicsL2_INTERNAL_CALL_AddDirectionalLightInternal_m6_518 (Object_t * __this /* static, unused */, Vector3_t6_48 * ___direction, Color_t6_40 * ___color, SphericalHarmonicsL2_t6_74 * ___sh, const MethodInfo* method)
{
	typedef void (*SphericalHarmonicsL2_INTERNAL_CALL_AddDirectionalLightInternal_m6_518_ftn) (Vector3_t6_48 *, Color_t6_40 *, SphericalHarmonicsL2_t6_74 *);
	static SphericalHarmonicsL2_INTERNAL_CALL_AddDirectionalLightInternal_m6_518_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SphericalHarmonicsL2_INTERNAL_CALL_AddDirectionalLightInternal_m6_518_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rendering.SphericalHarmonicsL2::INTERNAL_CALL_AddDirectionalLightInternal(UnityEngine.Vector3&,UnityEngine.Color&,UnityEngine.Rendering.SphericalHarmonicsL2&)");
	_il2cpp_icall_func(___direction, ___color, ___sh);
}
// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::get_Item(System.Int32,System.Int32)
extern TypeInfo* IndexOutOfRangeException_t1_1555_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4712;
extern "C" float SphericalHarmonicsL2_get_Item_m6_519 (SphericalHarmonicsL2_t6_74 * __this, int32_t ___rgb, int32_t ___coefficient, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IndexOutOfRangeException_t1_1555_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		_stringLiteral4712 = il2cpp_codegen_string_literal_from_index(4712);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___rgb;
		int32_t L_1 = ___coefficient;
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_0*(int32_t)((int32_t)9)))+(int32_t)L_1));
		int32_t L_2 = V_0;
		V_1 = L_2;
		int32_t L_3 = V_1;
		if (L_3 == 0)
		{
			goto IL_0080;
		}
		if (L_3 == 1)
		{
			goto IL_0087;
		}
		if (L_3 == 2)
		{
			goto IL_008e;
		}
		if (L_3 == 3)
		{
			goto IL_0095;
		}
		if (L_3 == 4)
		{
			goto IL_009c;
		}
		if (L_3 == 5)
		{
			goto IL_00a3;
		}
		if (L_3 == 6)
		{
			goto IL_00aa;
		}
		if (L_3 == 7)
		{
			goto IL_00b1;
		}
		if (L_3 == 8)
		{
			goto IL_00b8;
		}
		if (L_3 == 9)
		{
			goto IL_00bf;
		}
		if (L_3 == 10)
		{
			goto IL_00c6;
		}
		if (L_3 == 11)
		{
			goto IL_00cd;
		}
		if (L_3 == 12)
		{
			goto IL_00d4;
		}
		if (L_3 == 13)
		{
			goto IL_00db;
		}
		if (L_3 == 14)
		{
			goto IL_00e2;
		}
		if (L_3 == 15)
		{
			goto IL_00e9;
		}
		if (L_3 == 16)
		{
			goto IL_00f0;
		}
		if (L_3 == 17)
		{
			goto IL_00f7;
		}
		if (L_3 == 18)
		{
			goto IL_00fe;
		}
		if (L_3 == 19)
		{
			goto IL_0105;
		}
		if (L_3 == 20)
		{
			goto IL_010c;
		}
		if (L_3 == 21)
		{
			goto IL_0113;
		}
		if (L_3 == 22)
		{
			goto IL_011a;
		}
		if (L_3 == 23)
		{
			goto IL_0121;
		}
		if (L_3 == 24)
		{
			goto IL_0128;
		}
		if (L_3 == 25)
		{
			goto IL_012f;
		}
		if (L_3 == 26)
		{
			goto IL_0136;
		}
	}
	{
		goto IL_013d;
	}

IL_0080:
	{
		float L_4 = (__this->___shr0_0);
		return L_4;
	}

IL_0087:
	{
		float L_5 = (__this->___shr1_1);
		return L_5;
	}

IL_008e:
	{
		float L_6 = (__this->___shr2_2);
		return L_6;
	}

IL_0095:
	{
		float L_7 = (__this->___shr3_3);
		return L_7;
	}

IL_009c:
	{
		float L_8 = (__this->___shr4_4);
		return L_8;
	}

IL_00a3:
	{
		float L_9 = (__this->___shr5_5);
		return L_9;
	}

IL_00aa:
	{
		float L_10 = (__this->___shr6_6);
		return L_10;
	}

IL_00b1:
	{
		float L_11 = (__this->___shr7_7);
		return L_11;
	}

IL_00b8:
	{
		float L_12 = (__this->___shr8_8);
		return L_12;
	}

IL_00bf:
	{
		float L_13 = (__this->___shg0_9);
		return L_13;
	}

IL_00c6:
	{
		float L_14 = (__this->___shg1_10);
		return L_14;
	}

IL_00cd:
	{
		float L_15 = (__this->___shg2_11);
		return L_15;
	}

IL_00d4:
	{
		float L_16 = (__this->___shg3_12);
		return L_16;
	}

IL_00db:
	{
		float L_17 = (__this->___shg4_13);
		return L_17;
	}

IL_00e2:
	{
		float L_18 = (__this->___shg5_14);
		return L_18;
	}

IL_00e9:
	{
		float L_19 = (__this->___shg6_15);
		return L_19;
	}

IL_00f0:
	{
		float L_20 = (__this->___shg7_16);
		return L_20;
	}

IL_00f7:
	{
		float L_21 = (__this->___shg8_17);
		return L_21;
	}

IL_00fe:
	{
		float L_22 = (__this->___shb0_18);
		return L_22;
	}

IL_0105:
	{
		float L_23 = (__this->___shb1_19);
		return L_23;
	}

IL_010c:
	{
		float L_24 = (__this->___shb2_20);
		return L_24;
	}

IL_0113:
	{
		float L_25 = (__this->___shb3_21);
		return L_25;
	}

IL_011a:
	{
		float L_26 = (__this->___shb4_22);
		return L_26;
	}

IL_0121:
	{
		float L_27 = (__this->___shb5_23);
		return L_27;
	}

IL_0128:
	{
		float L_28 = (__this->___shb6_24);
		return L_28;
	}

IL_012f:
	{
		float L_29 = (__this->___shb7_25);
		return L_29;
	}

IL_0136:
	{
		float L_30 = (__this->___shb8_26);
		return L_30;
	}

IL_013d:
	{
		IndexOutOfRangeException_t1_1555 * L_31 = (IndexOutOfRangeException_t1_1555 *)il2cpp_codegen_object_new (IndexOutOfRangeException_t1_1555_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m1_14159(L_31, _stringLiteral4712, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_31);
	}
}
// System.Void UnityEngine.Rendering.SphericalHarmonicsL2::set_Item(System.Int32,System.Int32,System.Single)
extern TypeInfo* IndexOutOfRangeException_t1_1555_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4712;
extern "C" void SphericalHarmonicsL2_set_Item_m6_520 (SphericalHarmonicsL2_t6_74 * __this, int32_t ___rgb, int32_t ___coefficient, float ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IndexOutOfRangeException_t1_1555_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		_stringLiteral4712 = il2cpp_codegen_string_literal_from_index(4712);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___rgb;
		int32_t L_1 = ___coefficient;
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_0*(int32_t)((int32_t)9)))+(int32_t)L_1));
		int32_t L_2 = V_0;
		V_1 = L_2;
		int32_t L_3 = V_1;
		if (L_3 == 0)
		{
			goto IL_0080;
		}
		if (L_3 == 1)
		{
			goto IL_008c;
		}
		if (L_3 == 2)
		{
			goto IL_0098;
		}
		if (L_3 == 3)
		{
			goto IL_00a4;
		}
		if (L_3 == 4)
		{
			goto IL_00b0;
		}
		if (L_3 == 5)
		{
			goto IL_00bc;
		}
		if (L_3 == 6)
		{
			goto IL_00c8;
		}
		if (L_3 == 7)
		{
			goto IL_00d4;
		}
		if (L_3 == 8)
		{
			goto IL_00e0;
		}
		if (L_3 == 9)
		{
			goto IL_00ec;
		}
		if (L_3 == 10)
		{
			goto IL_00f8;
		}
		if (L_3 == 11)
		{
			goto IL_0104;
		}
		if (L_3 == 12)
		{
			goto IL_0110;
		}
		if (L_3 == 13)
		{
			goto IL_011c;
		}
		if (L_3 == 14)
		{
			goto IL_0128;
		}
		if (L_3 == 15)
		{
			goto IL_0134;
		}
		if (L_3 == 16)
		{
			goto IL_0140;
		}
		if (L_3 == 17)
		{
			goto IL_014c;
		}
		if (L_3 == 18)
		{
			goto IL_0158;
		}
		if (L_3 == 19)
		{
			goto IL_0164;
		}
		if (L_3 == 20)
		{
			goto IL_0170;
		}
		if (L_3 == 21)
		{
			goto IL_017c;
		}
		if (L_3 == 22)
		{
			goto IL_0188;
		}
		if (L_3 == 23)
		{
			goto IL_0194;
		}
		if (L_3 == 24)
		{
			goto IL_01a0;
		}
		if (L_3 == 25)
		{
			goto IL_01ac;
		}
		if (L_3 == 26)
		{
			goto IL_01b8;
		}
	}
	{
		goto IL_01c4;
	}

IL_0080:
	{
		float L_4 = ___value;
		__this->___shr0_0 = L_4;
		goto IL_01cf;
	}

IL_008c:
	{
		float L_5 = ___value;
		__this->___shr1_1 = L_5;
		goto IL_01cf;
	}

IL_0098:
	{
		float L_6 = ___value;
		__this->___shr2_2 = L_6;
		goto IL_01cf;
	}

IL_00a4:
	{
		float L_7 = ___value;
		__this->___shr3_3 = L_7;
		goto IL_01cf;
	}

IL_00b0:
	{
		float L_8 = ___value;
		__this->___shr4_4 = L_8;
		goto IL_01cf;
	}

IL_00bc:
	{
		float L_9 = ___value;
		__this->___shr5_5 = L_9;
		goto IL_01cf;
	}

IL_00c8:
	{
		float L_10 = ___value;
		__this->___shr6_6 = L_10;
		goto IL_01cf;
	}

IL_00d4:
	{
		float L_11 = ___value;
		__this->___shr7_7 = L_11;
		goto IL_01cf;
	}

IL_00e0:
	{
		float L_12 = ___value;
		__this->___shr8_8 = L_12;
		goto IL_01cf;
	}

IL_00ec:
	{
		float L_13 = ___value;
		__this->___shg0_9 = L_13;
		goto IL_01cf;
	}

IL_00f8:
	{
		float L_14 = ___value;
		__this->___shg1_10 = L_14;
		goto IL_01cf;
	}

IL_0104:
	{
		float L_15 = ___value;
		__this->___shg2_11 = L_15;
		goto IL_01cf;
	}

IL_0110:
	{
		float L_16 = ___value;
		__this->___shg3_12 = L_16;
		goto IL_01cf;
	}

IL_011c:
	{
		float L_17 = ___value;
		__this->___shg4_13 = L_17;
		goto IL_01cf;
	}

IL_0128:
	{
		float L_18 = ___value;
		__this->___shg5_14 = L_18;
		goto IL_01cf;
	}

IL_0134:
	{
		float L_19 = ___value;
		__this->___shg6_15 = L_19;
		goto IL_01cf;
	}

IL_0140:
	{
		float L_20 = ___value;
		__this->___shg7_16 = L_20;
		goto IL_01cf;
	}

IL_014c:
	{
		float L_21 = ___value;
		__this->___shg8_17 = L_21;
		goto IL_01cf;
	}

IL_0158:
	{
		float L_22 = ___value;
		__this->___shb0_18 = L_22;
		goto IL_01cf;
	}

IL_0164:
	{
		float L_23 = ___value;
		__this->___shb1_19 = L_23;
		goto IL_01cf;
	}

IL_0170:
	{
		float L_24 = ___value;
		__this->___shb2_20 = L_24;
		goto IL_01cf;
	}

IL_017c:
	{
		float L_25 = ___value;
		__this->___shb3_21 = L_25;
		goto IL_01cf;
	}

IL_0188:
	{
		float L_26 = ___value;
		__this->___shb4_22 = L_26;
		goto IL_01cf;
	}

IL_0194:
	{
		float L_27 = ___value;
		__this->___shb5_23 = L_27;
		goto IL_01cf;
	}

IL_01a0:
	{
		float L_28 = ___value;
		__this->___shb6_24 = L_28;
		goto IL_01cf;
	}

IL_01ac:
	{
		float L_29 = ___value;
		__this->___shb7_25 = L_29;
		goto IL_01cf;
	}

IL_01b8:
	{
		float L_30 = ___value;
		__this->___shb8_26 = L_30;
		goto IL_01cf;
	}

IL_01c4:
	{
		IndexOutOfRangeException_t1_1555 * L_31 = (IndexOutOfRangeException_t1_1555 *)il2cpp_codegen_object_new (IndexOutOfRangeException_t1_1555_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m1_14159(L_31, _stringLiteral4712, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_31);
	}

IL_01cf:
	{
		return;
	}
}
// System.Int32 UnityEngine.Rendering.SphericalHarmonicsL2::GetHashCode()
extern "C" int32_t SphericalHarmonicsL2_GetHashCode_m6_521 (SphericalHarmonicsL2_t6_74 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = ((int32_t)17);
		int32_t L_0 = V_0;
		float* L_1 = &(__this->___shr0_0);
		int32_t L_2 = Single_GetHashCode_m1_622(L_1, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_0*(int32_t)((int32_t)23)))+(int32_t)L_2));
		int32_t L_3 = V_0;
		float* L_4 = &(__this->___shr1_1);
		int32_t L_5 = Single_GetHashCode_m1_622(L_4, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_3*(int32_t)((int32_t)23)))+(int32_t)L_5));
		int32_t L_6 = V_0;
		float* L_7 = &(__this->___shr2_2);
		int32_t L_8 = Single_GetHashCode_m1_622(L_7, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_6*(int32_t)((int32_t)23)))+(int32_t)L_8));
		int32_t L_9 = V_0;
		float* L_10 = &(__this->___shr3_3);
		int32_t L_11 = Single_GetHashCode_m1_622(L_10, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_9*(int32_t)((int32_t)23)))+(int32_t)L_11));
		int32_t L_12 = V_0;
		float* L_13 = &(__this->___shr4_4);
		int32_t L_14 = Single_GetHashCode_m1_622(L_13, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_12*(int32_t)((int32_t)23)))+(int32_t)L_14));
		int32_t L_15 = V_0;
		float* L_16 = &(__this->___shr5_5);
		int32_t L_17 = Single_GetHashCode_m1_622(L_16, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_15*(int32_t)((int32_t)23)))+(int32_t)L_17));
		int32_t L_18 = V_0;
		float* L_19 = &(__this->___shr6_6);
		int32_t L_20 = Single_GetHashCode_m1_622(L_19, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_18*(int32_t)((int32_t)23)))+(int32_t)L_20));
		int32_t L_21 = V_0;
		float* L_22 = &(__this->___shr7_7);
		int32_t L_23 = Single_GetHashCode_m1_622(L_22, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_21*(int32_t)((int32_t)23)))+(int32_t)L_23));
		int32_t L_24 = V_0;
		float* L_25 = &(__this->___shr8_8);
		int32_t L_26 = Single_GetHashCode_m1_622(L_25, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_24*(int32_t)((int32_t)23)))+(int32_t)L_26));
		int32_t L_27 = V_0;
		float* L_28 = &(__this->___shg0_9);
		int32_t L_29 = Single_GetHashCode_m1_622(L_28, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_27*(int32_t)((int32_t)23)))+(int32_t)L_29));
		int32_t L_30 = V_0;
		float* L_31 = &(__this->___shg1_10);
		int32_t L_32 = Single_GetHashCode_m1_622(L_31, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_30*(int32_t)((int32_t)23)))+(int32_t)L_32));
		int32_t L_33 = V_0;
		float* L_34 = &(__this->___shg2_11);
		int32_t L_35 = Single_GetHashCode_m1_622(L_34, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_33*(int32_t)((int32_t)23)))+(int32_t)L_35));
		int32_t L_36 = V_0;
		float* L_37 = &(__this->___shg3_12);
		int32_t L_38 = Single_GetHashCode_m1_622(L_37, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_36*(int32_t)((int32_t)23)))+(int32_t)L_38));
		int32_t L_39 = V_0;
		float* L_40 = &(__this->___shg4_13);
		int32_t L_41 = Single_GetHashCode_m1_622(L_40, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_39*(int32_t)((int32_t)23)))+(int32_t)L_41));
		int32_t L_42 = V_0;
		float* L_43 = &(__this->___shg5_14);
		int32_t L_44 = Single_GetHashCode_m1_622(L_43, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_42*(int32_t)((int32_t)23)))+(int32_t)L_44));
		int32_t L_45 = V_0;
		float* L_46 = &(__this->___shg6_15);
		int32_t L_47 = Single_GetHashCode_m1_622(L_46, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_45*(int32_t)((int32_t)23)))+(int32_t)L_47));
		int32_t L_48 = V_0;
		float* L_49 = &(__this->___shg7_16);
		int32_t L_50 = Single_GetHashCode_m1_622(L_49, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_48*(int32_t)((int32_t)23)))+(int32_t)L_50));
		int32_t L_51 = V_0;
		float* L_52 = &(__this->___shg8_17);
		int32_t L_53 = Single_GetHashCode_m1_622(L_52, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_51*(int32_t)((int32_t)23)))+(int32_t)L_53));
		int32_t L_54 = V_0;
		float* L_55 = &(__this->___shb0_18);
		int32_t L_56 = Single_GetHashCode_m1_622(L_55, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_54*(int32_t)((int32_t)23)))+(int32_t)L_56));
		int32_t L_57 = V_0;
		float* L_58 = &(__this->___shb1_19);
		int32_t L_59 = Single_GetHashCode_m1_622(L_58, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_57*(int32_t)((int32_t)23)))+(int32_t)L_59));
		int32_t L_60 = V_0;
		float* L_61 = &(__this->___shb2_20);
		int32_t L_62 = Single_GetHashCode_m1_622(L_61, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_60*(int32_t)((int32_t)23)))+(int32_t)L_62));
		int32_t L_63 = V_0;
		float* L_64 = &(__this->___shb3_21);
		int32_t L_65 = Single_GetHashCode_m1_622(L_64, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_63*(int32_t)((int32_t)23)))+(int32_t)L_65));
		int32_t L_66 = V_0;
		float* L_67 = &(__this->___shb4_22);
		int32_t L_68 = Single_GetHashCode_m1_622(L_67, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_66*(int32_t)((int32_t)23)))+(int32_t)L_68));
		int32_t L_69 = V_0;
		float* L_70 = &(__this->___shb5_23);
		int32_t L_71 = Single_GetHashCode_m1_622(L_70, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_69*(int32_t)((int32_t)23)))+(int32_t)L_71));
		int32_t L_72 = V_0;
		float* L_73 = &(__this->___shb6_24);
		int32_t L_74 = Single_GetHashCode_m1_622(L_73, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_72*(int32_t)((int32_t)23)))+(int32_t)L_74));
		int32_t L_75 = V_0;
		float* L_76 = &(__this->___shb7_25);
		int32_t L_77 = Single_GetHashCode_m1_622(L_76, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_75*(int32_t)((int32_t)23)))+(int32_t)L_77));
		int32_t L_78 = V_0;
		float* L_79 = &(__this->___shb8_26);
		int32_t L_80 = Single_GetHashCode_m1_622(L_79, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_78*(int32_t)((int32_t)23)))+(int32_t)L_80));
		int32_t L_81 = V_0;
		return L_81;
	}
}
// System.Boolean UnityEngine.Rendering.SphericalHarmonicsL2::Equals(System.Object)
extern TypeInfo* SphericalHarmonicsL2_t6_74_il2cpp_TypeInfo_var;
extern "C" bool SphericalHarmonicsL2_Equals_m6_522 (SphericalHarmonicsL2_t6_74 * __this, Object_t * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SphericalHarmonicsL2_t6_74_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1600);
		s_Il2CppMethodIntialized = true;
	}
	SphericalHarmonicsL2_t6_74  V_0 = {0};
	{
		Object_t * L_0 = ___other;
		if (((Object_t *)IsInstSealed(L_0, SphericalHarmonicsL2_t6_74_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		Object_t * L_1 = ___other;
		V_0 = ((*(SphericalHarmonicsL2_t6_74 *)((SphericalHarmonicsL2_t6_74 *)UnBox (L_1, SphericalHarmonicsL2_t6_74_il2cpp_TypeInfo_var))));
		SphericalHarmonicsL2_t6_74  L_2 = V_0;
		bool L_3 = SphericalHarmonicsL2_op_Equality_m6_526(NULL /*static, unused*/, (*(SphericalHarmonicsL2_t6_74 *)__this), L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Rendering.SphericalHarmonicsL2 UnityEngine.Rendering.SphericalHarmonicsL2::op_Multiply(UnityEngine.Rendering.SphericalHarmonicsL2,System.Single)
extern TypeInfo* SphericalHarmonicsL2_t6_74_il2cpp_TypeInfo_var;
extern "C" SphericalHarmonicsL2_t6_74  SphericalHarmonicsL2_op_Multiply_m6_523 (Object_t * __this /* static, unused */, SphericalHarmonicsL2_t6_74  ___lhs, float ___rhs, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SphericalHarmonicsL2_t6_74_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1600);
		s_Il2CppMethodIntialized = true;
	}
	SphericalHarmonicsL2_t6_74  V_0 = {0};
	{
		Initobj (SphericalHarmonicsL2_t6_74_il2cpp_TypeInfo_var, (&V_0));
		float L_0 = ((&___lhs)->___shr0_0);
		float L_1 = ___rhs;
		(&V_0)->___shr0_0 = ((float)((float)L_0*(float)L_1));
		float L_2 = ((&___lhs)->___shr1_1);
		float L_3 = ___rhs;
		(&V_0)->___shr1_1 = ((float)((float)L_2*(float)L_3));
		float L_4 = ((&___lhs)->___shr2_2);
		float L_5 = ___rhs;
		(&V_0)->___shr2_2 = ((float)((float)L_4*(float)L_5));
		float L_6 = ((&___lhs)->___shr3_3);
		float L_7 = ___rhs;
		(&V_0)->___shr3_3 = ((float)((float)L_6*(float)L_7));
		float L_8 = ((&___lhs)->___shr4_4);
		float L_9 = ___rhs;
		(&V_0)->___shr4_4 = ((float)((float)L_8*(float)L_9));
		float L_10 = ((&___lhs)->___shr5_5);
		float L_11 = ___rhs;
		(&V_0)->___shr5_5 = ((float)((float)L_10*(float)L_11));
		float L_12 = ((&___lhs)->___shr6_6);
		float L_13 = ___rhs;
		(&V_0)->___shr6_6 = ((float)((float)L_12*(float)L_13));
		float L_14 = ((&___lhs)->___shr7_7);
		float L_15 = ___rhs;
		(&V_0)->___shr7_7 = ((float)((float)L_14*(float)L_15));
		float L_16 = ((&___lhs)->___shr8_8);
		float L_17 = ___rhs;
		(&V_0)->___shr8_8 = ((float)((float)L_16*(float)L_17));
		float L_18 = ((&___lhs)->___shg0_9);
		float L_19 = ___rhs;
		(&V_0)->___shg0_9 = ((float)((float)L_18*(float)L_19));
		float L_20 = ((&___lhs)->___shg1_10);
		float L_21 = ___rhs;
		(&V_0)->___shg1_10 = ((float)((float)L_20*(float)L_21));
		float L_22 = ((&___lhs)->___shg2_11);
		float L_23 = ___rhs;
		(&V_0)->___shg2_11 = ((float)((float)L_22*(float)L_23));
		float L_24 = ((&___lhs)->___shg3_12);
		float L_25 = ___rhs;
		(&V_0)->___shg3_12 = ((float)((float)L_24*(float)L_25));
		float L_26 = ((&___lhs)->___shg4_13);
		float L_27 = ___rhs;
		(&V_0)->___shg4_13 = ((float)((float)L_26*(float)L_27));
		float L_28 = ((&___lhs)->___shg5_14);
		float L_29 = ___rhs;
		(&V_0)->___shg5_14 = ((float)((float)L_28*(float)L_29));
		float L_30 = ((&___lhs)->___shg6_15);
		float L_31 = ___rhs;
		(&V_0)->___shg6_15 = ((float)((float)L_30*(float)L_31));
		float L_32 = ((&___lhs)->___shg7_16);
		float L_33 = ___rhs;
		(&V_0)->___shg7_16 = ((float)((float)L_32*(float)L_33));
		float L_34 = ((&___lhs)->___shg8_17);
		float L_35 = ___rhs;
		(&V_0)->___shg8_17 = ((float)((float)L_34*(float)L_35));
		float L_36 = ((&___lhs)->___shb0_18);
		float L_37 = ___rhs;
		(&V_0)->___shb0_18 = ((float)((float)L_36*(float)L_37));
		float L_38 = ((&___lhs)->___shb1_19);
		float L_39 = ___rhs;
		(&V_0)->___shb1_19 = ((float)((float)L_38*(float)L_39));
		float L_40 = ((&___lhs)->___shb2_20);
		float L_41 = ___rhs;
		(&V_0)->___shb2_20 = ((float)((float)L_40*(float)L_41));
		float L_42 = ((&___lhs)->___shb3_21);
		float L_43 = ___rhs;
		(&V_0)->___shb3_21 = ((float)((float)L_42*(float)L_43));
		float L_44 = ((&___lhs)->___shb4_22);
		float L_45 = ___rhs;
		(&V_0)->___shb4_22 = ((float)((float)L_44*(float)L_45));
		float L_46 = ((&___lhs)->___shb5_23);
		float L_47 = ___rhs;
		(&V_0)->___shb5_23 = ((float)((float)L_46*(float)L_47));
		float L_48 = ((&___lhs)->___shb6_24);
		float L_49 = ___rhs;
		(&V_0)->___shb6_24 = ((float)((float)L_48*(float)L_49));
		float L_50 = ((&___lhs)->___shb7_25);
		float L_51 = ___rhs;
		(&V_0)->___shb7_25 = ((float)((float)L_50*(float)L_51));
		float L_52 = ((&___lhs)->___shb8_26);
		float L_53 = ___rhs;
		(&V_0)->___shb8_26 = ((float)((float)L_52*(float)L_53));
		SphericalHarmonicsL2_t6_74  L_54 = V_0;
		return L_54;
	}
}
// UnityEngine.Rendering.SphericalHarmonicsL2 UnityEngine.Rendering.SphericalHarmonicsL2::op_Multiply(System.Single,UnityEngine.Rendering.SphericalHarmonicsL2)
extern TypeInfo* SphericalHarmonicsL2_t6_74_il2cpp_TypeInfo_var;
extern "C" SphericalHarmonicsL2_t6_74  SphericalHarmonicsL2_op_Multiply_m6_524 (Object_t * __this /* static, unused */, float ___lhs, SphericalHarmonicsL2_t6_74  ___rhs, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SphericalHarmonicsL2_t6_74_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1600);
		s_Il2CppMethodIntialized = true;
	}
	SphericalHarmonicsL2_t6_74  V_0 = {0};
	{
		Initobj (SphericalHarmonicsL2_t6_74_il2cpp_TypeInfo_var, (&V_0));
		float L_0 = ((&___rhs)->___shr0_0);
		float L_1 = ___lhs;
		(&V_0)->___shr0_0 = ((float)((float)L_0*(float)L_1));
		float L_2 = ((&___rhs)->___shr1_1);
		float L_3 = ___lhs;
		(&V_0)->___shr1_1 = ((float)((float)L_2*(float)L_3));
		float L_4 = ((&___rhs)->___shr2_2);
		float L_5 = ___lhs;
		(&V_0)->___shr2_2 = ((float)((float)L_4*(float)L_5));
		float L_6 = ((&___rhs)->___shr3_3);
		float L_7 = ___lhs;
		(&V_0)->___shr3_3 = ((float)((float)L_6*(float)L_7));
		float L_8 = ((&___rhs)->___shr4_4);
		float L_9 = ___lhs;
		(&V_0)->___shr4_4 = ((float)((float)L_8*(float)L_9));
		float L_10 = ((&___rhs)->___shr5_5);
		float L_11 = ___lhs;
		(&V_0)->___shr5_5 = ((float)((float)L_10*(float)L_11));
		float L_12 = ((&___rhs)->___shr6_6);
		float L_13 = ___lhs;
		(&V_0)->___shr6_6 = ((float)((float)L_12*(float)L_13));
		float L_14 = ((&___rhs)->___shr7_7);
		float L_15 = ___lhs;
		(&V_0)->___shr7_7 = ((float)((float)L_14*(float)L_15));
		float L_16 = ((&___rhs)->___shr8_8);
		float L_17 = ___lhs;
		(&V_0)->___shr8_8 = ((float)((float)L_16*(float)L_17));
		float L_18 = ((&___rhs)->___shg0_9);
		float L_19 = ___lhs;
		(&V_0)->___shg0_9 = ((float)((float)L_18*(float)L_19));
		float L_20 = ((&___rhs)->___shg1_10);
		float L_21 = ___lhs;
		(&V_0)->___shg1_10 = ((float)((float)L_20*(float)L_21));
		float L_22 = ((&___rhs)->___shg2_11);
		float L_23 = ___lhs;
		(&V_0)->___shg2_11 = ((float)((float)L_22*(float)L_23));
		float L_24 = ((&___rhs)->___shg3_12);
		float L_25 = ___lhs;
		(&V_0)->___shg3_12 = ((float)((float)L_24*(float)L_25));
		float L_26 = ((&___rhs)->___shg4_13);
		float L_27 = ___lhs;
		(&V_0)->___shg4_13 = ((float)((float)L_26*(float)L_27));
		float L_28 = ((&___rhs)->___shg5_14);
		float L_29 = ___lhs;
		(&V_0)->___shg5_14 = ((float)((float)L_28*(float)L_29));
		float L_30 = ((&___rhs)->___shg6_15);
		float L_31 = ___lhs;
		(&V_0)->___shg6_15 = ((float)((float)L_30*(float)L_31));
		float L_32 = ((&___rhs)->___shg7_16);
		float L_33 = ___lhs;
		(&V_0)->___shg7_16 = ((float)((float)L_32*(float)L_33));
		float L_34 = ((&___rhs)->___shg8_17);
		float L_35 = ___lhs;
		(&V_0)->___shg8_17 = ((float)((float)L_34*(float)L_35));
		float L_36 = ((&___rhs)->___shb0_18);
		float L_37 = ___lhs;
		(&V_0)->___shb0_18 = ((float)((float)L_36*(float)L_37));
		float L_38 = ((&___rhs)->___shb1_19);
		float L_39 = ___lhs;
		(&V_0)->___shb1_19 = ((float)((float)L_38*(float)L_39));
		float L_40 = ((&___rhs)->___shb2_20);
		float L_41 = ___lhs;
		(&V_0)->___shb2_20 = ((float)((float)L_40*(float)L_41));
		float L_42 = ((&___rhs)->___shb3_21);
		float L_43 = ___lhs;
		(&V_0)->___shb3_21 = ((float)((float)L_42*(float)L_43));
		float L_44 = ((&___rhs)->___shb4_22);
		float L_45 = ___lhs;
		(&V_0)->___shb4_22 = ((float)((float)L_44*(float)L_45));
		float L_46 = ((&___rhs)->___shb5_23);
		float L_47 = ___lhs;
		(&V_0)->___shb5_23 = ((float)((float)L_46*(float)L_47));
		float L_48 = ((&___rhs)->___shb6_24);
		float L_49 = ___lhs;
		(&V_0)->___shb6_24 = ((float)((float)L_48*(float)L_49));
		float L_50 = ((&___rhs)->___shb7_25);
		float L_51 = ___lhs;
		(&V_0)->___shb7_25 = ((float)((float)L_50*(float)L_51));
		float L_52 = ((&___rhs)->___shb8_26);
		float L_53 = ___lhs;
		(&V_0)->___shb8_26 = ((float)((float)L_52*(float)L_53));
		SphericalHarmonicsL2_t6_74  L_54 = V_0;
		return L_54;
	}
}
// UnityEngine.Rendering.SphericalHarmonicsL2 UnityEngine.Rendering.SphericalHarmonicsL2::op_Addition(UnityEngine.Rendering.SphericalHarmonicsL2,UnityEngine.Rendering.SphericalHarmonicsL2)
extern TypeInfo* SphericalHarmonicsL2_t6_74_il2cpp_TypeInfo_var;
extern "C" SphericalHarmonicsL2_t6_74  SphericalHarmonicsL2_op_Addition_m6_525 (Object_t * __this /* static, unused */, SphericalHarmonicsL2_t6_74  ___lhs, SphericalHarmonicsL2_t6_74  ___rhs, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SphericalHarmonicsL2_t6_74_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1600);
		s_Il2CppMethodIntialized = true;
	}
	SphericalHarmonicsL2_t6_74  V_0 = {0};
	{
		Initobj (SphericalHarmonicsL2_t6_74_il2cpp_TypeInfo_var, (&V_0));
		float L_0 = ((&___lhs)->___shr0_0);
		float L_1 = ((&___rhs)->___shr0_0);
		(&V_0)->___shr0_0 = ((float)((float)L_0+(float)L_1));
		float L_2 = ((&___lhs)->___shr1_1);
		float L_3 = ((&___rhs)->___shr1_1);
		(&V_0)->___shr1_1 = ((float)((float)L_2+(float)L_3));
		float L_4 = ((&___lhs)->___shr2_2);
		float L_5 = ((&___rhs)->___shr2_2);
		(&V_0)->___shr2_2 = ((float)((float)L_4+(float)L_5));
		float L_6 = ((&___lhs)->___shr3_3);
		float L_7 = ((&___rhs)->___shr3_3);
		(&V_0)->___shr3_3 = ((float)((float)L_6+(float)L_7));
		float L_8 = ((&___lhs)->___shr4_4);
		float L_9 = ((&___rhs)->___shr4_4);
		(&V_0)->___shr4_4 = ((float)((float)L_8+(float)L_9));
		float L_10 = ((&___lhs)->___shr5_5);
		float L_11 = ((&___rhs)->___shr5_5);
		(&V_0)->___shr5_5 = ((float)((float)L_10+(float)L_11));
		float L_12 = ((&___lhs)->___shr6_6);
		float L_13 = ((&___rhs)->___shr6_6);
		(&V_0)->___shr6_6 = ((float)((float)L_12+(float)L_13));
		float L_14 = ((&___lhs)->___shr7_7);
		float L_15 = ((&___rhs)->___shr7_7);
		(&V_0)->___shr7_7 = ((float)((float)L_14+(float)L_15));
		float L_16 = ((&___lhs)->___shr8_8);
		float L_17 = ((&___rhs)->___shr8_8);
		(&V_0)->___shr8_8 = ((float)((float)L_16+(float)L_17));
		float L_18 = ((&___lhs)->___shg0_9);
		float L_19 = ((&___rhs)->___shg0_9);
		(&V_0)->___shg0_9 = ((float)((float)L_18+(float)L_19));
		float L_20 = ((&___lhs)->___shg1_10);
		float L_21 = ((&___rhs)->___shg1_10);
		(&V_0)->___shg1_10 = ((float)((float)L_20+(float)L_21));
		float L_22 = ((&___lhs)->___shg2_11);
		float L_23 = ((&___rhs)->___shg2_11);
		(&V_0)->___shg2_11 = ((float)((float)L_22+(float)L_23));
		float L_24 = ((&___lhs)->___shg3_12);
		float L_25 = ((&___rhs)->___shg3_12);
		(&V_0)->___shg3_12 = ((float)((float)L_24+(float)L_25));
		float L_26 = ((&___lhs)->___shg4_13);
		float L_27 = ((&___rhs)->___shg4_13);
		(&V_0)->___shg4_13 = ((float)((float)L_26+(float)L_27));
		float L_28 = ((&___lhs)->___shg5_14);
		float L_29 = ((&___rhs)->___shg5_14);
		(&V_0)->___shg5_14 = ((float)((float)L_28+(float)L_29));
		float L_30 = ((&___lhs)->___shg6_15);
		float L_31 = ((&___rhs)->___shg6_15);
		(&V_0)->___shg6_15 = ((float)((float)L_30+(float)L_31));
		float L_32 = ((&___lhs)->___shg7_16);
		float L_33 = ((&___rhs)->___shg7_16);
		(&V_0)->___shg7_16 = ((float)((float)L_32+(float)L_33));
		float L_34 = ((&___lhs)->___shg8_17);
		float L_35 = ((&___rhs)->___shg8_17);
		(&V_0)->___shg8_17 = ((float)((float)L_34+(float)L_35));
		float L_36 = ((&___lhs)->___shb0_18);
		float L_37 = ((&___rhs)->___shb0_18);
		(&V_0)->___shb0_18 = ((float)((float)L_36+(float)L_37));
		float L_38 = ((&___lhs)->___shb1_19);
		float L_39 = ((&___rhs)->___shb1_19);
		(&V_0)->___shb1_19 = ((float)((float)L_38+(float)L_39));
		float L_40 = ((&___lhs)->___shb2_20);
		float L_41 = ((&___rhs)->___shb2_20);
		(&V_0)->___shb2_20 = ((float)((float)L_40+(float)L_41));
		float L_42 = ((&___lhs)->___shb3_21);
		float L_43 = ((&___rhs)->___shb3_21);
		(&V_0)->___shb3_21 = ((float)((float)L_42+(float)L_43));
		float L_44 = ((&___lhs)->___shb4_22);
		float L_45 = ((&___rhs)->___shb4_22);
		(&V_0)->___shb4_22 = ((float)((float)L_44+(float)L_45));
		float L_46 = ((&___lhs)->___shb5_23);
		float L_47 = ((&___rhs)->___shb5_23);
		(&V_0)->___shb5_23 = ((float)((float)L_46+(float)L_47));
		float L_48 = ((&___lhs)->___shb6_24);
		float L_49 = ((&___rhs)->___shb6_24);
		(&V_0)->___shb6_24 = ((float)((float)L_48+(float)L_49));
		float L_50 = ((&___lhs)->___shb7_25);
		float L_51 = ((&___rhs)->___shb7_25);
		(&V_0)->___shb7_25 = ((float)((float)L_50+(float)L_51));
		float L_52 = ((&___lhs)->___shb8_26);
		float L_53 = ((&___rhs)->___shb8_26);
		(&V_0)->___shb8_26 = ((float)((float)L_52+(float)L_53));
		SphericalHarmonicsL2_t6_74  L_54 = V_0;
		return L_54;
	}
}
// System.Boolean UnityEngine.Rendering.SphericalHarmonicsL2::op_Equality(UnityEngine.Rendering.SphericalHarmonicsL2,UnityEngine.Rendering.SphericalHarmonicsL2)
extern "C" bool SphericalHarmonicsL2_op_Equality_m6_526 (Object_t * __this /* static, unused */, SphericalHarmonicsL2_t6_74  ___lhs, SphericalHarmonicsL2_t6_74  ___rhs, const MethodInfo* method)
{
	int32_t G_B28_0 = 0;
	{
		float L_0 = ((&___lhs)->___shr0_0);
		float L_1 = ((&___rhs)->___shr0_0);
		if ((!(((float)L_0) == ((float)L_1))))
		{
			goto IL_0200;
		}
	}
	{
		float L_2 = ((&___lhs)->___shr1_1);
		float L_3 = ((&___rhs)->___shr1_1);
		if ((!(((float)L_2) == ((float)L_3))))
		{
			goto IL_0200;
		}
	}
	{
		float L_4 = ((&___lhs)->___shr2_2);
		float L_5 = ((&___rhs)->___shr2_2);
		if ((!(((float)L_4) == ((float)L_5))))
		{
			goto IL_0200;
		}
	}
	{
		float L_6 = ((&___lhs)->___shr3_3);
		float L_7 = ((&___rhs)->___shr3_3);
		if ((!(((float)L_6) == ((float)L_7))))
		{
			goto IL_0200;
		}
	}
	{
		float L_8 = ((&___lhs)->___shr4_4);
		float L_9 = ((&___rhs)->___shr4_4);
		if ((!(((float)L_8) == ((float)L_9))))
		{
			goto IL_0200;
		}
	}
	{
		float L_10 = ((&___lhs)->___shr5_5);
		float L_11 = ((&___rhs)->___shr5_5);
		if ((!(((float)L_10) == ((float)L_11))))
		{
			goto IL_0200;
		}
	}
	{
		float L_12 = ((&___lhs)->___shr6_6);
		float L_13 = ((&___rhs)->___shr6_6);
		if ((!(((float)L_12) == ((float)L_13))))
		{
			goto IL_0200;
		}
	}
	{
		float L_14 = ((&___lhs)->___shr7_7);
		float L_15 = ((&___rhs)->___shr7_7);
		if ((!(((float)L_14) == ((float)L_15))))
		{
			goto IL_0200;
		}
	}
	{
		float L_16 = ((&___lhs)->___shr8_8);
		float L_17 = ((&___rhs)->___shr8_8);
		if ((!(((float)L_16) == ((float)L_17))))
		{
			goto IL_0200;
		}
	}
	{
		float L_18 = ((&___lhs)->___shg0_9);
		float L_19 = ((&___rhs)->___shg0_9);
		if ((!(((float)L_18) == ((float)L_19))))
		{
			goto IL_0200;
		}
	}
	{
		float L_20 = ((&___lhs)->___shg1_10);
		float L_21 = ((&___rhs)->___shg1_10);
		if ((!(((float)L_20) == ((float)L_21))))
		{
			goto IL_0200;
		}
	}
	{
		float L_22 = ((&___lhs)->___shg2_11);
		float L_23 = ((&___rhs)->___shg2_11);
		if ((!(((float)L_22) == ((float)L_23))))
		{
			goto IL_0200;
		}
	}
	{
		float L_24 = ((&___lhs)->___shg3_12);
		float L_25 = ((&___rhs)->___shg3_12);
		if ((!(((float)L_24) == ((float)L_25))))
		{
			goto IL_0200;
		}
	}
	{
		float L_26 = ((&___lhs)->___shg4_13);
		float L_27 = ((&___rhs)->___shg4_13);
		if ((!(((float)L_26) == ((float)L_27))))
		{
			goto IL_0200;
		}
	}
	{
		float L_28 = ((&___lhs)->___shg5_14);
		float L_29 = ((&___rhs)->___shg5_14);
		if ((!(((float)L_28) == ((float)L_29))))
		{
			goto IL_0200;
		}
	}
	{
		float L_30 = ((&___lhs)->___shg6_15);
		float L_31 = ((&___rhs)->___shg6_15);
		if ((!(((float)L_30) == ((float)L_31))))
		{
			goto IL_0200;
		}
	}
	{
		float L_32 = ((&___lhs)->___shg7_16);
		float L_33 = ((&___rhs)->___shg7_16);
		if ((!(((float)L_32) == ((float)L_33))))
		{
			goto IL_0200;
		}
	}
	{
		float L_34 = ((&___lhs)->___shg8_17);
		float L_35 = ((&___rhs)->___shg8_17);
		if ((!(((float)L_34) == ((float)L_35))))
		{
			goto IL_0200;
		}
	}
	{
		float L_36 = ((&___lhs)->___shb0_18);
		float L_37 = ((&___rhs)->___shb0_18);
		if ((!(((float)L_36) == ((float)L_37))))
		{
			goto IL_0200;
		}
	}
	{
		float L_38 = ((&___lhs)->___shb1_19);
		float L_39 = ((&___rhs)->___shb1_19);
		if ((!(((float)L_38) == ((float)L_39))))
		{
			goto IL_0200;
		}
	}
	{
		float L_40 = ((&___lhs)->___shb2_20);
		float L_41 = ((&___rhs)->___shb2_20);
		if ((!(((float)L_40) == ((float)L_41))))
		{
			goto IL_0200;
		}
	}
	{
		float L_42 = ((&___lhs)->___shb3_21);
		float L_43 = ((&___rhs)->___shb3_21);
		if ((!(((float)L_42) == ((float)L_43))))
		{
			goto IL_0200;
		}
	}
	{
		float L_44 = ((&___lhs)->___shb4_22);
		float L_45 = ((&___rhs)->___shb4_22);
		if ((!(((float)L_44) == ((float)L_45))))
		{
			goto IL_0200;
		}
	}
	{
		float L_46 = ((&___lhs)->___shb5_23);
		float L_47 = ((&___rhs)->___shb5_23);
		if ((!(((float)L_46) == ((float)L_47))))
		{
			goto IL_0200;
		}
	}
	{
		float L_48 = ((&___lhs)->___shb6_24);
		float L_49 = ((&___rhs)->___shb6_24);
		if ((!(((float)L_48) == ((float)L_49))))
		{
			goto IL_0200;
		}
	}
	{
		float L_50 = ((&___lhs)->___shb7_25);
		float L_51 = ((&___rhs)->___shb7_25);
		if ((!(((float)L_50) == ((float)L_51))))
		{
			goto IL_0200;
		}
	}
	{
		float L_52 = ((&___lhs)->___shb8_26);
		float L_53 = ((&___rhs)->___shb8_26);
		G_B28_0 = ((((float)L_52) == ((float)L_53))? 1 : 0);
		goto IL_0201;
	}

IL_0200:
	{
		G_B28_0 = 0;
	}

IL_0201:
	{
		return G_B28_0;
	}
}
// System.Boolean UnityEngine.Rendering.SphericalHarmonicsL2::op_Inequality(UnityEngine.Rendering.SphericalHarmonicsL2,UnityEngine.Rendering.SphericalHarmonicsL2)
extern "C" bool SphericalHarmonicsL2_op_Inequality_m6_527 (Object_t * __this /* static, unused */, SphericalHarmonicsL2_t6_74  ___lhs, SphericalHarmonicsL2_t6_74  ___rhs, const MethodInfo* method)
{
	{
		SphericalHarmonicsL2_t6_74  L_0 = ___lhs;
		SphericalHarmonicsL2_t6_74  L_1 = ___rhs;
		bool L_2 = SphericalHarmonicsL2_op_Equality_m6_526(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return ((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
// UnityEngine.Rect UnityEngine.Sprite::get_rect()
extern "C" Rect_t6_51  Sprite_get_rect_m6_528 (Sprite_t6_75 * __this, const MethodInfo* method)
{
	Rect_t6_51  V_0 = {0};
	{
		Sprite_INTERNAL_get_rect_m6_529(__this, (&V_0), /*hidden argument*/NULL);
		Rect_t6_51  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Sprite::INTERNAL_get_rect(UnityEngine.Rect&)
extern "C" void Sprite_INTERNAL_get_rect_m6_529 (Sprite_t6_75 * __this, Rect_t6_51 * ___value, const MethodInfo* method)
{
	typedef void (*Sprite_INTERNAL_get_rect_m6_529_ftn) (Sprite_t6_75 *, Rect_t6_51 *);
	static Sprite_INTERNAL_get_rect_m6_529_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Sprite_INTERNAL_get_rect_m6_529_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprite::INTERNAL_get_rect(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Single UnityEngine.Sprite::get_pixelsPerUnit()
extern "C" float Sprite_get_pixelsPerUnit_m6_530 (Sprite_t6_75 * __this, const MethodInfo* method)
{
	typedef float (*Sprite_get_pixelsPerUnit_m6_530_ftn) (Sprite_t6_75 *);
	static Sprite_get_pixelsPerUnit_m6_530_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Sprite_get_pixelsPerUnit_m6_530_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprite::get_pixelsPerUnit()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Texture2D UnityEngine.Sprite::get_texture()
extern "C" Texture2D_t6_33 * Sprite_get_texture_m6_531 (Sprite_t6_75 * __this, const MethodInfo* method)
{
	typedef Texture2D_t6_33 * (*Sprite_get_texture_m6_531_ftn) (Sprite_t6_75 *);
	static Sprite_get_texture_m6_531_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Sprite_get_texture_m6_531_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprite::get_texture()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Rect UnityEngine.Sprite::get_textureRect()
extern "C" Rect_t6_51  Sprite_get_textureRect_m6_532 (Sprite_t6_75 * __this, const MethodInfo* method)
{
	Rect_t6_51  V_0 = {0};
	{
		Sprite_INTERNAL_get_textureRect_m6_533(__this, (&V_0), /*hidden argument*/NULL);
		Rect_t6_51  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Sprite::INTERNAL_get_textureRect(UnityEngine.Rect&)
extern "C" void Sprite_INTERNAL_get_textureRect_m6_533 (Sprite_t6_75 * __this, Rect_t6_51 * ___value, const MethodInfo* method)
{
	typedef void (*Sprite_INTERNAL_get_textureRect_m6_533_ftn) (Sprite_t6_75 *, Rect_t6_51 *);
	static Sprite_INTERNAL_get_textureRect_m6_533_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Sprite_INTERNAL_get_textureRect_m6_533_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprite::INTERNAL_get_textureRect(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Vector4 UnityEngine.Sprite::get_border()
extern "C" Vector4_t6_54  Sprite_get_border_m6_534 (Sprite_t6_75 * __this, const MethodInfo* method)
{
	Vector4_t6_54  V_0 = {0};
	{
		Sprite_INTERNAL_get_border_m6_535(__this, (&V_0), /*hidden argument*/NULL);
		Vector4_t6_54  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Sprite::INTERNAL_get_border(UnityEngine.Vector4&)
extern "C" void Sprite_INTERNAL_get_border_m6_535 (Sprite_t6_75 * __this, Vector4_t6_54 * ___value, const MethodInfo* method)
{
	typedef void (*Sprite_INTERNAL_get_border_m6_535_ftn) (Sprite_t6_75 *, Vector4_t6_54 *);
	static Sprite_INTERNAL_get_border_m6_535_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Sprite_INTERNAL_get_border_m6_535_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprite::INTERNAL_get_border(UnityEngine.Vector4&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Vector4 UnityEngine.Sprites.DataUtility::GetInnerUV(UnityEngine.Sprite)
extern "C" Vector4_t6_54  DataUtility_GetInnerUV_m6_536 (Object_t * __this /* static, unused */, Sprite_t6_75 * ___sprite, const MethodInfo* method)
{
	typedef Vector4_t6_54  (*DataUtility_GetInnerUV_m6_536_ftn) (Sprite_t6_75 *);
	static DataUtility_GetInnerUV_m6_536_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DataUtility_GetInnerUV_m6_536_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprites.DataUtility::GetInnerUV(UnityEngine.Sprite)");
	return _il2cpp_icall_func(___sprite);
}
// UnityEngine.Vector4 UnityEngine.Sprites.DataUtility::GetOuterUV(UnityEngine.Sprite)
extern "C" Vector4_t6_54  DataUtility_GetOuterUV_m6_537 (Object_t * __this /* static, unused */, Sprite_t6_75 * ___sprite, const MethodInfo* method)
{
	typedef Vector4_t6_54  (*DataUtility_GetOuterUV_m6_537_ftn) (Sprite_t6_75 *);
	static DataUtility_GetOuterUV_m6_537_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DataUtility_GetOuterUV_m6_537_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprites.DataUtility::GetOuterUV(UnityEngine.Sprite)");
	return _il2cpp_icall_func(___sprite);
}
// UnityEngine.Vector4 UnityEngine.Sprites.DataUtility::GetPadding(UnityEngine.Sprite)
extern "C" Vector4_t6_54  DataUtility_GetPadding_m6_538 (Object_t * __this /* static, unused */, Sprite_t6_75 * ___sprite, const MethodInfo* method)
{
	typedef Vector4_t6_54  (*DataUtility_GetPadding_m6_538_ftn) (Sprite_t6_75 *);
	static DataUtility_GetPadding_m6_538_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DataUtility_GetPadding_m6_538_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprites.DataUtility::GetPadding(UnityEngine.Sprite)");
	return _il2cpp_icall_func(___sprite);
}
// UnityEngine.Vector2 UnityEngine.Sprites.DataUtility::GetMinSize(UnityEngine.Sprite)
extern "C" Vector2_t6_47  DataUtility_GetMinSize_m6_539 (Object_t * __this /* static, unused */, Sprite_t6_75 * ___sprite, const MethodInfo* method)
{
	Vector2_t6_47  V_0 = {0};
	{
		Sprite_t6_75 * L_0 = ___sprite;
		DataUtility_Internal_GetMinSize_m6_540(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		Vector2_t6_47  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Sprites.DataUtility::Internal_GetMinSize(UnityEngine.Sprite,UnityEngine.Vector2&)
extern "C" void DataUtility_Internal_GetMinSize_m6_540 (Object_t * __this /* static, unused */, Sprite_t6_75 * ___sprite, Vector2_t6_47 * ___output, const MethodInfo* method)
{
	typedef void (*DataUtility_Internal_GetMinSize_m6_540_ftn) (Sprite_t6_75 *, Vector2_t6_47 *);
	static DataUtility_Internal_GetMinSize_m6_540_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DataUtility_Internal_GetMinSize_m6_540_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprites.DataUtility::Internal_GetMinSize(UnityEngine.Sprite,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___sprite, ___output);
}
// System.Void UnityEngine.WWW::.ctor(System.String)
extern "C" void WWW__ctor_m6_541 (WWW_t6_78 * __this, String_t* ___url, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___url;
		WWW_InitWWW_m6_547(__this, L_0, (ByteU5BU5D_t1_109*)(ByteU5BU5D_t1_109*)NULL, (StringU5BU5D_t1_238*)(StringU5BU5D_t1_238*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WWW::.ctor(System.String,UnityEngine.WWWForm)
extern "C" void WWW__ctor_m6_542 (WWW_t6_78 * __this, String_t* ___url, WWWForm_t6_79 * ___form, const MethodInfo* method)
{
	StringU5BU5D_t1_238* V_0 = {0};
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		WWWForm_t6_79 * L_0 = ___form;
		NullCheck(L_0);
		Dictionary_2_t1_1839 * L_1 = WWWForm_get_headers_m6_563(L_0, /*hidden argument*/NULL);
		StringU5BU5D_t1_238* L_2 = WWW_FlattenedHeadersFrom_m6_558(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		String_t* L_3 = ___url;
		WWWForm_t6_79 * L_4 = ___form;
		NullCheck(L_4);
		ByteU5BU5D_t1_109* L_5 = WWWForm_get_data_m6_564(L_4, /*hidden argument*/NULL);
		StringU5BU5D_t1_238* L_6 = V_0;
		WWW_InitWWW_m6_547(__this, L_3, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WWW::.ctor(System.String,System.Byte[])
extern "C" void WWW__ctor_m6_543 (WWW_t6_78 * __this, String_t* ___url, ByteU5BU5D_t1_109* ___postData, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___url;
		ByteU5BU5D_t1_109* L_1 = ___postData;
		WWW_InitWWW_m6_547(__this, L_0, L_1, (StringU5BU5D_t1_238*)(StringU5BU5D_t1_238*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WWW::Dispose()
extern "C" void WWW_Dispose_m6_544 (WWW_t6_78 * __this, const MethodInfo* method)
{
	{
		WWW_DestroyWWW_m6_546(__this, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WWW::Finalize()
extern "C" void WWW_Finalize_m6_545 (WWW_t6_78 * __this, const MethodInfo* method)
{
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		WWW_DestroyWWW_m6_546(__this, 0, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m1_3(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0013:
	{
		return;
	}
}
// System.Void UnityEngine.WWW::DestroyWWW(System.Boolean)
extern "C" void WWW_DestroyWWW_m6_546 (WWW_t6_78 * __this, bool ___cancel, const MethodInfo* method)
{
	typedef void (*WWW_DestroyWWW_m6_546_ftn) (WWW_t6_78 *, bool);
	static WWW_DestroyWWW_m6_546_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WWW_DestroyWWW_m6_546_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WWW::DestroyWWW(System.Boolean)");
	_il2cpp_icall_func(__this, ___cancel);
}
// System.Void UnityEngine.WWW::InitWWW(System.String,System.Byte[],System.String[])
extern "C" void WWW_InitWWW_m6_547 (WWW_t6_78 * __this, String_t* ___url, ByteU5BU5D_t1_109* ___postData, StringU5BU5D_t1_238* ___iHeaders, const MethodInfo* method)
{
	typedef void (*WWW_InitWWW_m6_547_ftn) (WWW_t6_78 *, String_t*, ByteU5BU5D_t1_109*, StringU5BU5D_t1_238*);
	static WWW_InitWWW_m6_547_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WWW_InitWWW_m6_547_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WWW::InitWWW(System.String,System.Byte[],System.String[])");
	_il2cpp_icall_func(__this, ___url, ___postData, ___iHeaders);
}
// System.Collections.Generic.Dictionary`2<System.String,System.String> UnityEngine.WWW::get_responseHeaders()
extern TypeInfo* UnityException_t6_247_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4713;
extern "C" Dictionary_2_t1_1839 * WWW_get_responseHeaders_m6_548 (WWW_t6_78 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityException_t6_247_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1601);
		_stringLiteral4713 = il2cpp_codegen_string_literal_from_index(4713);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = WWW_get_isDone_m6_557(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		UnityException_t6_247 * L_1 = (UnityException_t6_247 *)il2cpp_codegen_object_new (UnityException_t6_247_il2cpp_TypeInfo_var);
		UnityException__ctor_m6_1667(L_1, _stringLiteral4713, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		String_t* L_2 = WWW_get_responseHeadersString_m6_549(__this, /*hidden argument*/NULL);
		Dictionary_2_t1_1839 * L_3 = WWW_ParseHTTPHeaderString_m6_559(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.String UnityEngine.WWW::get_responseHeadersString()
extern "C" String_t* WWW_get_responseHeadersString_m6_549 (WWW_t6_78 * __this, const MethodInfo* method)
{
	typedef String_t* (*WWW_get_responseHeadersString_m6_549_ftn) (WWW_t6_78 *);
	static WWW_get_responseHeadersString_m6_549_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WWW_get_responseHeadersString_m6_549_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WWW::get_responseHeadersString()");
	return _il2cpp_icall_func(__this);
}
// System.String UnityEngine.WWW::get_text()
extern TypeInfo* UnityException_t6_247_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4714;
extern "C" String_t* WWW_get_text_m6_550 (WWW_t6_78 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityException_t6_247_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1601);
		_stringLiteral4714 = il2cpp_codegen_string_literal_from_index(4714);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	{
		bool L_0 = WWW_get_isDone_m6_557(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		UnityException_t6_247 * L_1 = (UnityException_t6_247 *)il2cpp_codegen_object_new (UnityException_t6_247_il2cpp_TypeInfo_var);
		UnityException__ctor_m6_1667(L_1, _stringLiteral4714, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		ByteU5BU5D_t1_109* L_2 = WWW_get_bytes_m6_553(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		Encoding_t1_406 * L_3 = WWW_GetTextEncoder_m6_552(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_4 = V_0;
		ByteU5BU5D_t1_109* L_5 = V_0;
		NullCheck(L_5);
		NullCheck(L_3);
		String_t* L_6 = (String_t*)VirtFuncInvoker3< String_t*, ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(26 /* System.String System.Text.Encoding::GetString(System.Byte[],System.Int32,System.Int32) */, L_3, L_4, 0, (((int32_t)((int32_t)(((Array_t *)L_5)->max_length)))));
		return L_6;
	}
}
// System.Text.Encoding UnityEngine.WWW::get_DefaultEncoding()
extern TypeInfo* Encoding_t1_406_il2cpp_TypeInfo_var;
extern "C" Encoding_t1_406 * WWW_get_DefaultEncoding_m6_551 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Encoding_t1_406_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t1_406_il2cpp_TypeInfo_var);
		Encoding_t1_406 * L_0 = Encoding_get_ASCII_m1_12357(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Text.Encoding UnityEngine.WWW::GetTextEncoder()
extern TypeInfo* CharU5BU5D_t1_16_il2cpp_TypeInfo_var;
extern TypeInfo* Encoding_t1_406_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t1_33_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4715;
extern Il2CppCodeGenString* _stringLiteral4716;
extern Il2CppCodeGenString* _stringLiteral4717;
extern Il2CppCodeGenString* _stringLiteral224;
extern "C" Encoding_t1_406 * WWW_GetTextEncoder_m6_552 (WWW_t6_78 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CharU5BU5D_t1_16_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(15);
		Encoding_t1_406_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		Exception_t1_33_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(49);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral4715 = il2cpp_codegen_string_literal_from_index(4715);
		_stringLiteral4716 = il2cpp_codegen_string_literal_from_index(4716);
		_stringLiteral4717 = il2cpp_codegen_string_literal_from_index(4717);
		_stringLiteral224 = il2cpp_codegen_string_literal_from_index(224);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	String_t* V_3 = {0};
	int32_t V_4 = 0;
	Encoding_t1_406 * V_5 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (String_t*)NULL;
		Dictionary_2_t1_1839 * L_0 = WWW_get_responseHeaders_m6_548(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = (bool)VirtFuncInvoker2< bool, String_t*, String_t** >::Invoke(21 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String>::TryGetValue(!0,!1&) */, L_0, _stringLiteral4715, (&V_0));
		if (!L_1)
		{
			goto IL_00b0;
		}
	}
	{
		String_t* L_2 = V_0;
		NullCheck(L_2);
		int32_t L_3 = String_IndexOf_m1_488(L_2, _stringLiteral4716, 5, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = V_1;
		if ((((int32_t)L_4) <= ((int32_t)(-1))))
		{
			goto IL_00b0;
		}
	}
	{
		String_t* L_5 = V_0;
		int32_t L_6 = V_1;
		NullCheck(L_5);
		int32_t L_7 = String_IndexOf_m1_501(L_5, ((int32_t)61), L_6, /*hidden argument*/NULL);
		V_2 = L_7;
		int32_t L_8 = V_2;
		if ((((int32_t)L_8) <= ((int32_t)(-1))))
		{
			goto IL_00b0;
		}
	}
	{
		String_t* L_9 = V_0;
		int32_t L_10 = V_2;
		NullCheck(L_9);
		String_t* L_11 = String_Substring_m1_454(L_9, ((int32_t)((int32_t)L_10+(int32_t)1)), /*hidden argument*/NULL);
		NullCheck(L_11);
		String_t* L_12 = String_Trim_m1_457(L_11, /*hidden argument*/NULL);
		CharU5BU5D_t1_16* L_13 = ((CharU5BU5D_t1_16*)SZArrayNew(CharU5BU5D_t1_16_il2cpp_TypeInfo_var, 2));
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_13, 0, sizeof(uint16_t))) = (uint16_t)((int32_t)39);
		CharU5BU5D_t1_16* L_14 = L_13;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 1);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_14, 1, sizeof(uint16_t))) = (uint16_t)((int32_t)34);
		NullCheck(L_12);
		String_t* L_15 = String_Trim_m1_458(L_12, L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		String_t* L_16 = String_Trim_m1_457(L_15, /*hidden argument*/NULL);
		V_3 = L_16;
		String_t* L_17 = V_3;
		NullCheck(L_17);
		int32_t L_18 = String_IndexOf_m1_500(L_17, ((int32_t)59), /*hidden argument*/NULL);
		V_4 = L_18;
		int32_t L_19 = V_4;
		if ((((int32_t)L_19) <= ((int32_t)(-1))))
		{
			goto IL_0083;
		}
	}
	{
		String_t* L_20 = V_3;
		int32_t L_21 = V_4;
		NullCheck(L_20);
		String_t* L_22 = String_Substring_m1_455(L_20, 0, L_21, /*hidden argument*/NULL);
		V_3 = L_22;
	}

IL_0083:
	try
	{ // begin try (depth: 1)
		{
			String_t* L_23 = V_3;
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t1_406_il2cpp_TypeInfo_var);
			Encoding_t1_406 * L_24 = Encoding_GetEncoding_m1_12342(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
			V_5 = L_24;
			goto IL_00b6;
		}

IL_0090:
		{
			; // IL_0090: leave IL_00b0
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1_33 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1_33_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0095;
		throw e;
	}

CATCH_0095:
	{ // begin catch(System.Exception)
		String_t* L_25 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_26 = String_Concat_m1_560(NULL /*static, unused*/, _stringLiteral4717, L_25, _stringLiteral224, /*hidden argument*/NULL);
		Debug_Log_m6_631(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		goto IL_00b0;
	} // end catch (depth: 1)

IL_00b0:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t1_406_il2cpp_TypeInfo_var);
		Encoding_t1_406 * L_27 = Encoding_get_UTF8_m1_12363(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_27;
	}

IL_00b6:
	{
		Encoding_t1_406 * L_28 = V_5;
		return L_28;
	}
}
// System.Byte[] UnityEngine.WWW::get_bytes()
extern "C" ByteU5BU5D_t1_109* WWW_get_bytes_m6_553 (WWW_t6_78 * __this, const MethodInfo* method)
{
	typedef ByteU5BU5D_t1_109* (*WWW_get_bytes_m6_553_ftn) (WWW_t6_78 *);
	static WWW_get_bytes_m6_553_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WWW_get_bytes_m6_553_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WWW::get_bytes()");
	return _il2cpp_icall_func(__this);
}
// System.String UnityEngine.WWW::get_error()
extern "C" String_t* WWW_get_error_m6_554 (WWW_t6_78 * __this, const MethodInfo* method)
{
	typedef String_t* (*WWW_get_error_m6_554_ftn) (WWW_t6_78 *);
	static WWW_get_error_m6_554_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WWW_get_error_m6_554_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WWW::get_error()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Texture2D UnityEngine.WWW::GetTexture(System.Boolean)
extern "C" Texture2D_t6_33 * WWW_GetTexture_m6_555 (WWW_t6_78 * __this, bool ___markNonReadable, const MethodInfo* method)
{
	typedef Texture2D_t6_33 * (*WWW_GetTexture_m6_555_ftn) (WWW_t6_78 *, bool);
	static WWW_GetTexture_m6_555_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WWW_GetTexture_m6_555_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WWW::GetTexture(System.Boolean)");
	return _il2cpp_icall_func(__this, ___markNonReadable);
}
// UnityEngine.Texture2D UnityEngine.WWW::get_texture()
extern "C" Texture2D_t6_33 * WWW_get_texture_m6_556 (WWW_t6_78 * __this, const MethodInfo* method)
{
	{
		Texture2D_t6_33 * L_0 = WWW_GetTexture_m6_555(__this, 0, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityEngine.WWW::get_isDone()
extern "C" bool WWW_get_isDone_m6_557 (WWW_t6_78 * __this, const MethodInfo* method)
{
	typedef bool (*WWW_get_isDone_m6_557_ftn) (WWW_t6_78 *);
	static WWW_get_isDone_m6_557_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WWW_get_isDone_m6_557_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WWW::get_isDone()");
	return _il2cpp_icall_func(__this);
}
// System.String[] UnityEngine.WWW::FlattenedHeadersFrom(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern TypeInfo* StringU5BU5D_t1_238_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t1_1846_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m1_14944_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1_14945_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m1_14946_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m1_14947_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1_14948_MethodInfo_var;
extern "C" StringU5BU5D_t1_238* WWW_FlattenedHeadersFrom_m6_558 (Object_t * __this /* static, unused */, Dictionary_2_t1_1839 * ___headers, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringU5BU5D_t1_238_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(35);
		Enumerator_t1_1846_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1602);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		Dictionary_2_GetEnumerator_m1_14944_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483784);
		Enumerator_get_Current_m1_14945_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483785);
		KeyValuePair_2_get_Key_m1_14946_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483786);
		KeyValuePair_2_get_Value_m1_14947_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483787);
		Enumerator_MoveNext_m1_14948_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483788);
		s_Il2CppMethodIntialized = true;
	}
	StringU5BU5D_t1_238* V_0 = {0};
	int32_t V_1 = 0;
	KeyValuePair_2_t1_1845  V_2 = {0};
	Enumerator_t1_1846  V_3 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dictionary_2_t1_1839 * L_0 = ___headers;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (StringU5BU5D_t1_238*)NULL;
	}

IL_0008:
	{
		Dictionary_2_t1_1839 * L_1 = ___headers;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Generic.Dictionary`2<System.String,System.String>::get_Count() */, L_1);
		V_0 = ((StringU5BU5D_t1_238*)SZArrayNew(StringU5BU5D_t1_238_il2cpp_TypeInfo_var, ((int32_t)((int32_t)L_2*(int32_t)2))));
		V_1 = 0;
		Dictionary_2_t1_1839 * L_3 = ___headers;
		NullCheck(L_3);
		Enumerator_t1_1846  L_4 = Dictionary_2_GetEnumerator_m1_14944(L_3, /*hidden argument*/Dictionary_2_GetEnumerator_m1_14944_MethodInfo_var);
		V_3 = L_4;
	}

IL_001f:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0052;
		}

IL_0024:
		{
			KeyValuePair_2_t1_1845  L_5 = Enumerator_get_Current_m1_14945((&V_3), /*hidden argument*/Enumerator_get_Current_m1_14945_MethodInfo_var);
			V_2 = L_5;
			StringU5BU5D_t1_238* L_6 = V_0;
			int32_t L_7 = V_1;
			int32_t L_8 = L_7;
			V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
			String_t* L_9 = KeyValuePair_2_get_Key_m1_14946((&V_2), /*hidden argument*/KeyValuePair_2_get_Key_m1_14946_MethodInfo_var);
			NullCheck(L_9);
			String_t* L_10 = String_ToString_m1_546(L_9, /*hidden argument*/NULL);
			NullCheck(L_6);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_8);
			ArrayElementTypeCheck (L_6, L_10);
			*((String_t**)(String_t**)SZArrayLdElema(L_6, L_8, sizeof(String_t*))) = (String_t*)L_10;
			StringU5BU5D_t1_238* L_11 = V_0;
			int32_t L_12 = V_1;
			int32_t L_13 = L_12;
			V_1 = ((int32_t)((int32_t)L_13+(int32_t)1));
			String_t* L_14 = KeyValuePair_2_get_Value_m1_14947((&V_2), /*hidden argument*/KeyValuePair_2_get_Value_m1_14947_MethodInfo_var);
			NullCheck(L_14);
			String_t* L_15 = String_ToString_m1_546(L_14, /*hidden argument*/NULL);
			NullCheck(L_11);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_13);
			ArrayElementTypeCheck (L_11, L_15);
			*((String_t**)(String_t**)SZArrayLdElema(L_11, L_13, sizeof(String_t*))) = (String_t*)L_15;
		}

IL_0052:
		{
			bool L_16 = Enumerator_MoveNext_m1_14948((&V_3), /*hidden argument*/Enumerator_MoveNext_m1_14948_MethodInfo_var);
			if (L_16)
			{
				goto IL_0024;
			}
		}

IL_005e:
		{
			IL2CPP_LEAVE(0x6F, FINALLY_0063);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0063;
	}

FINALLY_0063:
	{ // begin finally (depth: 1)
		Enumerator_t1_1846  L_17 = V_3;
		Enumerator_t1_1846  L_18 = L_17;
		Object_t * L_19 = Box(Enumerator_t1_1846_il2cpp_TypeInfo_var, &L_18);
		NullCheck(L_19);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_19);
		IL2CPP_END_FINALLY(99)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(99)
	{
		IL2CPP_JUMP_TBL(0x6F, IL_006f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_006f:
	{
		StringU5BU5D_t1_238* L_20 = V_0;
		return L_20;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.String> UnityEngine.WWW::ParseHTTPHeaderString(System.String)
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t1_1839_il2cpp_TypeInfo_var;
extern TypeInfo* StringReader_t1_450_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_14949_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4718;
extern Il2CppCodeGenString* _stringLiteral4719;
extern Il2CppCodeGenString* _stringLiteral4720;
extern Il2CppCodeGenString* _stringLiteral259;
extern "C" Dictionary_2_t1_1839 * WWW_ParseHTTPHeaderString_m6_559 (Object_t * __this /* static, unused */, String_t* ___input, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		Dictionary_2_t1_1839_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1603);
		StringReader_t1_450_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(273);
		Dictionary_2__ctor_m1_14949_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483789);
		_stringLiteral4718 = il2cpp_codegen_string_literal_from_index(4718);
		_stringLiteral4719 = il2cpp_codegen_string_literal_from_index(4719);
		_stringLiteral4720 = il2cpp_codegen_string_literal_from_index(4720);
		_stringLiteral259 = il2cpp_codegen_string_literal_from_index(259);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t1_1839 * V_0 = {0};
	StringReader_t1_450 * V_1 = {0};
	int32_t V_2 = 0;
	String_t* V_3 = {0};
	int32_t V_4 = 0;
	String_t* V_5 = {0};
	String_t* V_6 = {0};
	{
		String_t* L_0 = ___input;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentException_t1_1425 * L_1 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13260(L_1, _stringLiteral4718, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		Dictionary_2_t1_1839 * L_2 = (Dictionary_2_t1_1839 *)il2cpp_codegen_object_new (Dictionary_2_t1_1839_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_14949(L_2, /*hidden argument*/Dictionary_2__ctor_m1_14949_MethodInfo_var);
		V_0 = L_2;
		String_t* L_3 = ___input;
		StringReader_t1_450 * L_4 = (StringReader_t1_450 *)il2cpp_codegen_object_new (StringReader_t1_450_il2cpp_TypeInfo_var);
		StringReader__ctor_m1_5262(L_4, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		V_2 = 0;
	}

IL_0020:
	{
		StringReader_t1_450 * L_5 = V_1;
		NullCheck(L_5);
		String_t* L_6 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(11 /* System.String System.IO.StringReader::ReadLine() */, L_5);
		V_3 = L_6;
		String_t* L_7 = V_3;
		if (L_7)
		{
			goto IL_0032;
		}
	}
	{
		goto IL_00a2;
	}

IL_0032:
	{
		int32_t L_8 = V_2;
		int32_t L_9 = L_8;
		V_2 = ((int32_t)((int32_t)L_9+(int32_t)1));
		if (L_9)
		{
			goto IL_005d;
		}
	}
	{
		String_t* L_10 = V_3;
		NullCheck(L_10);
		bool L_11 = String_StartsWith_m1_531(L_10, _stringLiteral4719, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_005d;
		}
	}
	{
		Dictionary_2_t1_1839 * L_12 = V_0;
		String_t* L_13 = V_3;
		NullCheck(L_12);
		VirtActionInvoker2< String_t*, String_t* >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::set_Item(!0,!1) */, L_12, _stringLiteral4720, L_13);
		goto IL_0020;
	}

IL_005d:
	{
		String_t* L_14 = V_3;
		NullCheck(L_14);
		int32_t L_15 = String_IndexOf_m1_505(L_14, _stringLiteral259, /*hidden argument*/NULL);
		V_4 = L_15;
		int32_t L_16 = V_4;
		if ((!(((uint32_t)L_16) == ((uint32_t)(-1)))))
		{
			goto IL_0077;
		}
	}
	{
		goto IL_0020;
	}

IL_0077:
	{
		String_t* L_17 = V_3;
		int32_t L_18 = V_4;
		NullCheck(L_17);
		String_t* L_19 = String_Substring_m1_455(L_17, 0, L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		String_t* L_20 = String_ToUpper_m1_543(L_19, /*hidden argument*/NULL);
		V_5 = L_20;
		String_t* L_21 = V_3;
		int32_t L_22 = V_4;
		NullCheck(L_21);
		String_t* L_23 = String_Substring_m1_454(L_21, ((int32_t)((int32_t)L_22+(int32_t)2)), /*hidden argument*/NULL);
		V_6 = L_23;
		Dictionary_2_t1_1839 * L_24 = V_0;
		String_t* L_25 = V_5;
		String_t* L_26 = V_6;
		NullCheck(L_24);
		VirtActionInvoker2< String_t*, String_t* >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::set_Item(!0,!1) */, L_24, L_25, L_26);
		goto IL_0020;
	}

IL_00a2:
	{
		Dictionary_2_t1_1839 * L_27 = V_0;
		return L_27;
	}
}
// System.Void UnityEngine.WWWForm::.ctor()
extern TypeInfo* List_1_t1_1821_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t1_1742_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1_14950_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m1_14876_MethodInfo_var;
extern "C" void WWWForm__ctor_m6_560 (WWWForm_t6_79 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t1_1821_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1604);
		List_1_t1_1742_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(37);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		List_1__ctor_m1_14950_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483790);
		List_1__ctor_m1_14876_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483649);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		List_1_t1_1821 * L_0 = (List_1_t1_1821 *)il2cpp_codegen_object_new (List_1_t1_1821_il2cpp_TypeInfo_var);
		List_1__ctor_m1_14950(L_0, /*hidden argument*/List_1__ctor_m1_14950_MethodInfo_var);
		__this->___formData_0 = L_0;
		List_1_t1_1742 * L_1 = (List_1_t1_1742 *)il2cpp_codegen_object_new (List_1_t1_1742_il2cpp_TypeInfo_var);
		List_1__ctor_m1_14876(L_1, /*hidden argument*/List_1__ctor_m1_14876_MethodInfo_var);
		__this->___fieldNames_1 = L_1;
		List_1_t1_1742 * L_2 = (List_1_t1_1742 *)il2cpp_codegen_object_new (List_1_t1_1742_il2cpp_TypeInfo_var);
		List_1__ctor_m1_14876(L_2, /*hidden argument*/List_1__ctor_m1_14876_MethodInfo_var);
		__this->___fileNames_2 = L_2;
		List_1_t1_1742 * L_3 = (List_1_t1_1742 *)il2cpp_codegen_object_new (List_1_t1_1742_il2cpp_TypeInfo_var);
		List_1__ctor_m1_14876(L_3, /*hidden argument*/List_1__ctor_m1_14876_MethodInfo_var);
		__this->___types_3 = L_3;
		__this->___boundary_4 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, ((int32_t)40)));
		V_0 = 0;
		goto IL_0076;
	}

IL_0046:
	{
		int32_t L_4 = Random_Range_m6_797(NULL /*static, unused*/, ((int32_t)48), ((int32_t)110), /*hidden argument*/NULL);
		V_1 = L_4;
		int32_t L_5 = V_1;
		if ((((int32_t)L_5) <= ((int32_t)((int32_t)57))))
		{
			goto IL_005c;
		}
	}
	{
		int32_t L_6 = V_1;
		V_1 = ((int32_t)((int32_t)L_6+(int32_t)7));
	}

IL_005c:
	{
		int32_t L_7 = V_1;
		if ((((int32_t)L_7) <= ((int32_t)((int32_t)90))))
		{
			goto IL_0068;
		}
	}
	{
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)6));
	}

IL_0068:
	{
		ByteU5BU5D_t1_109* L_9 = (__this->___boundary_4);
		int32_t L_10 = V_0;
		int32_t L_11 = V_1;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_9, L_10, sizeof(uint8_t))) = (uint8_t)(((int32_t)((uint8_t)L_11)));
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0076:
	{
		int32_t L_13 = V_0;
		if ((((int32_t)L_13) < ((int32_t)((int32_t)40))))
		{
			goto IL_0046;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.WWWForm::AddField(System.String,System.String)
extern TypeInfo* Encoding_t1_406_il2cpp_TypeInfo_var;
extern "C" void WWWForm_AddField_m6_561 (WWWForm_t6_79 * __this, String_t* ___fieldName, String_t* ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Encoding_t1_406_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		s_Il2CppMethodIntialized = true;
	}
	Encoding_t1_406 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t1_406_il2cpp_TypeInfo_var);
		Encoding_t1_406 * L_0 = Encoding_get_UTF8_m1_12363(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = ___fieldName;
		String_t* L_2 = ___value;
		Encoding_t1_406 * L_3 = V_0;
		WWWForm_AddField_m6_562(__this, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WWWForm::AddField(System.String,System.String,System.Text.Encoding)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4721;
extern Il2CppCodeGenString* _stringLiteral583;
extern "C" void WWWForm_AddField_m6_562 (WWWForm_t6_79 * __this, String_t* ___fieldName, String_t* ___value, Encoding_t1_406 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral4721 = il2cpp_codegen_string_literal_from_index(4721);
		_stringLiteral583 = il2cpp_codegen_string_literal_from_index(583);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1_1742 * L_0 = (__this->___fieldNames_1);
		String_t* L_1 = ___fieldName;
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.String>::Add(!0) */, L_0, L_1);
		List_1_t1_1742 * L_2 = (__this->___fileNames_2);
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.String>::Add(!0) */, L_2, (String_t*)NULL);
		List_1_t1_1821 * L_3 = (__this->___formData_0);
		Encoding_t1_406 * L_4 = ___e;
		String_t* L_5 = ___value;
		NullCheck(L_4);
		ByteU5BU5D_t1_109* L_6 = (ByteU5BU5D_t1_109*)VirtFuncInvoker1< ByteU5BU5D_t1_109*, String_t* >::Invoke(11 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_4, L_5);
		NullCheck(L_3);
		VirtActionInvoker1< ByteU5BU5D_t1_109* >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Byte[]>::Add(!0) */, L_3, L_6);
		List_1_t1_1742 * L_7 = (__this->___types_3);
		Encoding_t1_406 * L_8 = ___e;
		NullCheck(L_8);
		String_t* L_9 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(36 /* System.String System.Text.Encoding::get_WebName() */, L_8);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m1_560(NULL /*static, unused*/, _stringLiteral4721, L_9, _stringLiteral583, /*hidden argument*/NULL);
		NullCheck(L_7);
		VirtActionInvoker1< String_t* >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.String>::Add(!0) */, L_7, L_10);
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.String> UnityEngine.WWWForm::get_headers()
extern TypeInfo* Dictionary_2_t1_1839_il2cpp_TypeInfo_var;
extern TypeInfo* Encoding_t1_406_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_14949_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4278;
extern Il2CppCodeGenString* _stringLiteral4722;
extern Il2CppCodeGenString* _stringLiteral583;
extern Il2CppCodeGenString* _stringLiteral4723;
extern "C" Dictionary_2_t1_1839 * WWWForm_get_headers_m6_563 (WWWForm_t6_79 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t1_1839_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1603);
		Encoding_t1_406_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Dictionary_2__ctor_m1_14949_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483789);
		_stringLiteral4278 = il2cpp_codegen_string_literal_from_index(4278);
		_stringLiteral4722 = il2cpp_codegen_string_literal_from_index(4722);
		_stringLiteral583 = il2cpp_codegen_string_literal_from_index(583);
		_stringLiteral4723 = il2cpp_codegen_string_literal_from_index(4723);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t1_1839 * V_0 = {0};
	{
		Dictionary_2_t1_1839 * L_0 = (Dictionary_2_t1_1839 *)il2cpp_codegen_object_new (Dictionary_2_t1_1839_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_14949(L_0, /*hidden argument*/Dictionary_2__ctor_m1_14949_MethodInfo_var);
		V_0 = L_0;
		bool L_1 = (__this->___containsFiles_5);
		if (!L_1)
		{
			goto IL_0049;
		}
	}
	{
		Dictionary_2_t1_1839 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t1_406_il2cpp_TypeInfo_var);
		Encoding_t1_406 * L_3 = Encoding_get_UTF8_m1_12363(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_4 = (__this->___boundary_4);
		ByteU5BU5D_t1_109* L_5 = (__this->___boundary_4);
		NullCheck(L_5);
		NullCheck(L_3);
		String_t* L_6 = (String_t*)VirtFuncInvoker3< String_t*, ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(26 /* System.String System.Text.Encoding::GetString(System.Byte[],System.Int32,System.Int32) */, L_3, L_4, 0, (((int32_t)((int32_t)(((Array_t *)L_5)->max_length)))));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m1_560(NULL /*static, unused*/, _stringLiteral4722, L_6, _stringLiteral583, /*hidden argument*/NULL);
		NullCheck(L_2);
		VirtActionInvoker2< String_t*, String_t* >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::set_Item(!0,!1) */, L_2, _stringLiteral4278, L_7);
		goto IL_0059;
	}

IL_0049:
	{
		Dictionary_2_t1_1839 * L_8 = V_0;
		NullCheck(L_8);
		VirtActionInvoker2< String_t*, String_t* >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::set_Item(!0,!1) */, L_8, _stringLiteral4278, _stringLiteral4723);
	}

IL_0059:
	{
		Dictionary_2_t1_1839 * L_9 = V_0;
		return L_9;
	}
}
// System.Byte[] UnityEngine.WWWForm::get_data()
extern TypeInfo* MemoryStream_t1_433_il2cpp_TypeInfo_var;
extern TypeInfo* Encoding_t1_406_il2cpp_TypeInfo_var;
extern TypeInfo* WWWTranscoder_t6_80_il2cpp_TypeInfo_var;
extern TypeInfo* StringU5BU5D_t1_238_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4048;
extern Il2CppCodeGenString* _stringLiteral4335;
extern Il2CppCodeGenString* _stringLiteral4724;
extern Il2CppCodeGenString* _stringLiteral4725;
extern Il2CppCodeGenString* _stringLiteral583;
extern Il2CppCodeGenString* _stringLiteral4726;
extern Il2CppCodeGenString* _stringLiteral4727;
extern Il2CppCodeGenString* _stringLiteral4728;
extern Il2CppCodeGenString* _stringLiteral4729;
extern Il2CppCodeGenString* _stringLiteral1537;
extern Il2CppCodeGenString* _stringLiteral582;
extern "C" ByteU5BU5D_t1_109* WWWForm_get_data_m6_564 (WWWForm_t6_79 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MemoryStream_t1_433_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(390);
		Encoding_t1_406_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		WWWTranscoder_t6_80_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1605);
		StringU5BU5D_t1_238_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(35);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		_stringLiteral4048 = il2cpp_codegen_string_literal_from_index(4048);
		_stringLiteral4335 = il2cpp_codegen_string_literal_from_index(4335);
		_stringLiteral4724 = il2cpp_codegen_string_literal_from_index(4724);
		_stringLiteral4725 = il2cpp_codegen_string_literal_from_index(4725);
		_stringLiteral583 = il2cpp_codegen_string_literal_from_index(583);
		_stringLiteral4726 = il2cpp_codegen_string_literal_from_index(4726);
		_stringLiteral4727 = il2cpp_codegen_string_literal_from_index(4727);
		_stringLiteral4728 = il2cpp_codegen_string_literal_from_index(4728);
		_stringLiteral4729 = il2cpp_codegen_string_literal_from_index(4729);
		_stringLiteral1537 = il2cpp_codegen_string_literal_from_index(1537);
		_stringLiteral582 = il2cpp_codegen_string_literal_from_index(582);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	ByteU5BU5D_t1_109* V_1 = {0};
	ByteU5BU5D_t1_109* V_2 = {0};
	ByteU5BU5D_t1_109* V_3 = {0};
	ByteU5BU5D_t1_109* V_4 = {0};
	ByteU5BU5D_t1_109* V_5 = {0};
	MemoryStream_t1_433 * V_6 = {0};
	int32_t V_7 = 0;
	ByteU5BU5D_t1_109* V_8 = {0};
	String_t* V_9 = {0};
	String_t* V_10 = {0};
	ByteU5BU5D_t1_109* V_11 = {0};
	String_t* V_12 = {0};
	ByteU5BU5D_t1_109* V_13 = {0};
	ByteU5BU5D_t1_109* V_14 = {0};
	ByteU5BU5D_t1_109* V_15 = {0};
	ByteU5BU5D_t1_109* V_16 = {0};
	MemoryStream_t1_433 * V_17 = {0};
	int32_t V_18 = 0;
	ByteU5BU5D_t1_109* V_19 = {0};
	ByteU5BU5D_t1_109* V_20 = {0};
	ByteU5BU5D_t1_109* V_21 = {0};
	ByteU5BU5D_t1_109* V_22 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = (__this->___containsFiles_5);
		if (!L_0)
		{
			goto IL_0311;
		}
	}
	{
		Encoding_t1_406 * L_1 = WWW_get_DefaultEncoding_m6_551(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		ByteU5BU5D_t1_109* L_2 = (ByteU5BU5D_t1_109*)VirtFuncInvoker1< ByteU5BU5D_t1_109*, String_t* >::Invoke(11 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_1, _stringLiteral4048);
		V_0 = L_2;
		Encoding_t1_406 * L_3 = WWW_get_DefaultEncoding_m6_551(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		ByteU5BU5D_t1_109* L_4 = (ByteU5BU5D_t1_109*)VirtFuncInvoker1< ByteU5BU5D_t1_109*, String_t* >::Invoke(11 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_3, _stringLiteral4335);
		V_1 = L_4;
		Encoding_t1_406 * L_5 = WWW_get_DefaultEncoding_m6_551(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		ByteU5BU5D_t1_109* L_6 = (ByteU5BU5D_t1_109*)VirtFuncInvoker1< ByteU5BU5D_t1_109*, String_t* >::Invoke(11 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_5, _stringLiteral4724);
		V_2 = L_6;
		Encoding_t1_406 * L_7 = WWW_get_DefaultEncoding_m6_551(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		ByteU5BU5D_t1_109* L_8 = (ByteU5BU5D_t1_109*)VirtFuncInvoker1< ByteU5BU5D_t1_109*, String_t* >::Invoke(11 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_7, _stringLiteral4725);
		V_3 = L_8;
		Encoding_t1_406 * L_9 = WWW_get_DefaultEncoding_m6_551(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_9);
		ByteU5BU5D_t1_109* L_10 = (ByteU5BU5D_t1_109*)VirtFuncInvoker1< ByteU5BU5D_t1_109*, String_t* >::Invoke(11 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_9, _stringLiteral583);
		V_4 = L_10;
		Encoding_t1_406 * L_11 = WWW_get_DefaultEncoding_m6_551(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_11);
		ByteU5BU5D_t1_109* L_12 = (ByteU5BU5D_t1_109*)VirtFuncInvoker1< ByteU5BU5D_t1_109*, String_t* >::Invoke(11 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_11, _stringLiteral4726);
		V_5 = L_12;
		MemoryStream_t1_433 * L_13 = (MemoryStream_t1_433 *)il2cpp_codegen_object_new (MemoryStream_t1_433_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m1_5045(L_13, ((int32_t)1024), /*hidden argument*/NULL);
		V_6 = L_13;
	}

IL_0079:
	try
	{ // begin try (depth: 1)
		{
			V_7 = 0;
			goto IL_0297;
		}

IL_0081:
		{
			MemoryStream_t1_433 * L_14 = V_6;
			ByteU5BU5D_t1_109* L_15 = V_1;
			ByteU5BU5D_t1_109* L_16 = V_1;
			NullCheck(L_16);
			NullCheck(L_14);
			VirtActionInvoker3< ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(24 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_14, L_15, 0, (((int32_t)((int32_t)(((Array_t *)L_16)->max_length)))));
			MemoryStream_t1_433 * L_17 = V_6;
			ByteU5BU5D_t1_109* L_18 = V_0;
			ByteU5BU5D_t1_109* L_19 = V_0;
			NullCheck(L_19);
			NullCheck(L_17);
			VirtActionInvoker3< ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(24 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_17, L_18, 0, (((int32_t)((int32_t)(((Array_t *)L_19)->max_length)))));
			MemoryStream_t1_433 * L_20 = V_6;
			ByteU5BU5D_t1_109* L_21 = (__this->___boundary_4);
			ByteU5BU5D_t1_109* L_22 = (__this->___boundary_4);
			NullCheck(L_22);
			NullCheck(L_20);
			VirtActionInvoker3< ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(24 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_20, L_21, 0, (((int32_t)((int32_t)(((Array_t *)L_22)->max_length)))));
			MemoryStream_t1_433 * L_23 = V_6;
			ByteU5BU5D_t1_109* L_24 = V_1;
			ByteU5BU5D_t1_109* L_25 = V_1;
			NullCheck(L_25);
			NullCheck(L_23);
			VirtActionInvoker3< ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(24 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_23, L_24, 0, (((int32_t)((int32_t)(((Array_t *)L_25)->max_length)))));
			MemoryStream_t1_433 * L_26 = V_6;
			ByteU5BU5D_t1_109* L_27 = V_2;
			ByteU5BU5D_t1_109* L_28 = V_2;
			NullCheck(L_28);
			NullCheck(L_26);
			VirtActionInvoker3< ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(24 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_26, L_27, 0, (((int32_t)((int32_t)(((Array_t *)L_28)->max_length)))));
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t1_406_il2cpp_TypeInfo_var);
			Encoding_t1_406 * L_29 = Encoding_get_UTF8_m1_12363(NULL /*static, unused*/, /*hidden argument*/NULL);
			List_1_t1_1742 * L_30 = (__this->___types_3);
			int32_t L_31 = V_7;
			NullCheck(L_30);
			String_t* L_32 = (String_t*)VirtFuncInvoker1< String_t*, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.String>::get_Item(System.Int32) */, L_30, L_31);
			NullCheck(L_29);
			ByteU5BU5D_t1_109* L_33 = (ByteU5BU5D_t1_109*)VirtFuncInvoker1< ByteU5BU5D_t1_109*, String_t* >::Invoke(11 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_29, L_32);
			V_8 = L_33;
			MemoryStream_t1_433 * L_34 = V_6;
			ByteU5BU5D_t1_109* L_35 = V_8;
			ByteU5BU5D_t1_109* L_36 = V_8;
			NullCheck(L_36);
			NullCheck(L_34);
			VirtActionInvoker3< ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(24 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_34, L_35, 0, (((int32_t)((int32_t)(((Array_t *)L_36)->max_length)))));
			MemoryStream_t1_433 * L_37 = V_6;
			ByteU5BU5D_t1_109* L_38 = V_1;
			ByteU5BU5D_t1_109* L_39 = V_1;
			NullCheck(L_39);
			NullCheck(L_37);
			VirtActionInvoker3< ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(24 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_37, L_38, 0, (((int32_t)((int32_t)(((Array_t *)L_39)->max_length)))));
			MemoryStream_t1_433 * L_40 = V_6;
			ByteU5BU5D_t1_109* L_41 = V_3;
			ByteU5BU5D_t1_109* L_42 = V_3;
			NullCheck(L_42);
			NullCheck(L_40);
			VirtActionInvoker3< ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(24 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_40, L_41, 0, (((int32_t)((int32_t)(((Array_t *)L_42)->max_length)))));
			Encoding_t1_406 * L_43 = Encoding_get_UTF8_m1_12363(NULL /*static, unused*/, /*hidden argument*/NULL);
			NullCheck(L_43);
			String_t* L_44 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(31 /* System.String System.Text.Encoding::get_HeaderName() */, L_43);
			V_9 = L_44;
			List_1_t1_1742 * L_45 = (__this->___fieldNames_1);
			int32_t L_46 = V_7;
			NullCheck(L_45);
			String_t* L_47 = (String_t*)VirtFuncInvoker1< String_t*, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.String>::get_Item(System.Int32) */, L_45, L_46);
			V_10 = L_47;
			String_t* L_48 = V_10;
			Encoding_t1_406 * L_49 = Encoding_get_UTF8_m1_12363(NULL /*static, unused*/, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t6_80_il2cpp_TypeInfo_var);
			bool L_50 = WWWTranscoder_SevenBitClean_m6_571(NULL /*static, unused*/, L_48, L_49, /*hidden argument*/NULL);
			if (!L_50)
			{
				goto IL_0144;
			}
		}

IL_0132:
		{
			String_t* L_51 = V_10;
			NullCheck(L_51);
			int32_t L_52 = String_IndexOf_m1_505(L_51, _stringLiteral4727, /*hidden argument*/NULL);
			if ((((int32_t)L_52) <= ((int32_t)(-1))))
			{
				goto IL_017d;
			}
		}

IL_0144:
		{
			StringU5BU5D_t1_238* L_53 = ((StringU5BU5D_t1_238*)SZArrayNew(StringU5BU5D_t1_238_il2cpp_TypeInfo_var, 5));
			NullCheck(L_53);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_53, 0);
			ArrayElementTypeCheck (L_53, _stringLiteral4727);
			*((String_t**)(String_t**)SZArrayLdElema(L_53, 0, sizeof(String_t*))) = (String_t*)_stringLiteral4727;
			StringU5BU5D_t1_238* L_54 = L_53;
			String_t* L_55 = V_9;
			NullCheck(L_54);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_54, 1);
			ArrayElementTypeCheck (L_54, L_55);
			*((String_t**)(String_t**)SZArrayLdElema(L_54, 1, sizeof(String_t*))) = (String_t*)L_55;
			StringU5BU5D_t1_238* L_56 = L_54;
			NullCheck(L_56);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_56, 2);
			ArrayElementTypeCheck (L_56, _stringLiteral4728);
			*((String_t**)(String_t**)SZArrayLdElema(L_56, 2, sizeof(String_t*))) = (String_t*)_stringLiteral4728;
			StringU5BU5D_t1_238* L_57 = L_56;
			String_t* L_58 = V_10;
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t1_406_il2cpp_TypeInfo_var);
			Encoding_t1_406 * L_59 = Encoding_get_UTF8_m1_12363(NULL /*static, unused*/, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t6_80_il2cpp_TypeInfo_var);
			String_t* L_60 = WWWTranscoder_QPEncode_m6_568(NULL /*static, unused*/, L_58, L_59, /*hidden argument*/NULL);
			NullCheck(L_57);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_57, 3);
			ArrayElementTypeCheck (L_57, L_60);
			*((String_t**)(String_t**)SZArrayLdElema(L_57, 3, sizeof(String_t*))) = (String_t*)L_60;
			StringU5BU5D_t1_238* L_61 = L_57;
			NullCheck(L_61);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_61, 4);
			ArrayElementTypeCheck (L_61, _stringLiteral4729);
			*((String_t**)(String_t**)SZArrayLdElema(L_61, 4, sizeof(String_t*))) = (String_t*)_stringLiteral4729;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_62 = String_Concat_m1_563(NULL /*static, unused*/, L_61, /*hidden argument*/NULL);
			V_10 = L_62;
		}

IL_017d:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t1_406_il2cpp_TypeInfo_var);
			Encoding_t1_406 * L_63 = Encoding_get_UTF8_m1_12363(NULL /*static, unused*/, /*hidden argument*/NULL);
			String_t* L_64 = V_10;
			NullCheck(L_63);
			ByteU5BU5D_t1_109* L_65 = (ByteU5BU5D_t1_109*)VirtFuncInvoker1< ByteU5BU5D_t1_109*, String_t* >::Invoke(11 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_63, L_64);
			V_11 = L_65;
			MemoryStream_t1_433 * L_66 = V_6;
			ByteU5BU5D_t1_109* L_67 = V_11;
			ByteU5BU5D_t1_109* L_68 = V_11;
			NullCheck(L_68);
			NullCheck(L_66);
			VirtActionInvoker3< ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(24 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_66, L_67, 0, (((int32_t)((int32_t)(((Array_t *)L_68)->max_length)))));
			MemoryStream_t1_433 * L_69 = V_6;
			ByteU5BU5D_t1_109* L_70 = V_4;
			ByteU5BU5D_t1_109* L_71 = V_4;
			NullCheck(L_71);
			NullCheck(L_69);
			VirtActionInvoker3< ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(24 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_69, L_70, 0, (((int32_t)((int32_t)(((Array_t *)L_71)->max_length)))));
			List_1_t1_1742 * L_72 = (__this->___fileNames_2);
			int32_t L_73 = V_7;
			NullCheck(L_72);
			String_t* L_74 = (String_t*)VirtFuncInvoker1< String_t*, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.String>::get_Item(System.Int32) */, L_72, L_73);
			if (!L_74)
			{
				goto IL_025c;
			}
		}

IL_01b9:
		{
			List_1_t1_1742 * L_75 = (__this->___fileNames_2);
			int32_t L_76 = V_7;
			NullCheck(L_75);
			String_t* L_77 = (String_t*)VirtFuncInvoker1< String_t*, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.String>::get_Item(System.Int32) */, L_75, L_76);
			V_12 = L_77;
			String_t* L_78 = V_12;
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t1_406_il2cpp_TypeInfo_var);
			Encoding_t1_406 * L_79 = Encoding_get_UTF8_m1_12363(NULL /*static, unused*/, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t6_80_il2cpp_TypeInfo_var);
			bool L_80 = WWWTranscoder_SevenBitClean_m6_571(NULL /*static, unused*/, L_78, L_79, /*hidden argument*/NULL);
			if (!L_80)
			{
				goto IL_01eb;
			}
		}

IL_01d9:
		{
			String_t* L_81 = V_12;
			NullCheck(L_81);
			int32_t L_82 = String_IndexOf_m1_505(L_81, _stringLiteral4727, /*hidden argument*/NULL);
			if ((((int32_t)L_82) <= ((int32_t)(-1))))
			{
				goto IL_0224;
			}
		}

IL_01eb:
		{
			StringU5BU5D_t1_238* L_83 = ((StringU5BU5D_t1_238*)SZArrayNew(StringU5BU5D_t1_238_il2cpp_TypeInfo_var, 5));
			NullCheck(L_83);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_83, 0);
			ArrayElementTypeCheck (L_83, _stringLiteral4727);
			*((String_t**)(String_t**)SZArrayLdElema(L_83, 0, sizeof(String_t*))) = (String_t*)_stringLiteral4727;
			StringU5BU5D_t1_238* L_84 = L_83;
			String_t* L_85 = V_9;
			NullCheck(L_84);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_84, 1);
			ArrayElementTypeCheck (L_84, L_85);
			*((String_t**)(String_t**)SZArrayLdElema(L_84, 1, sizeof(String_t*))) = (String_t*)L_85;
			StringU5BU5D_t1_238* L_86 = L_84;
			NullCheck(L_86);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_86, 2);
			ArrayElementTypeCheck (L_86, _stringLiteral4728);
			*((String_t**)(String_t**)SZArrayLdElema(L_86, 2, sizeof(String_t*))) = (String_t*)_stringLiteral4728;
			StringU5BU5D_t1_238* L_87 = L_86;
			String_t* L_88 = V_12;
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t1_406_il2cpp_TypeInfo_var);
			Encoding_t1_406 * L_89 = Encoding_get_UTF8_m1_12363(NULL /*static, unused*/, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t6_80_il2cpp_TypeInfo_var);
			String_t* L_90 = WWWTranscoder_QPEncode_m6_568(NULL /*static, unused*/, L_88, L_89, /*hidden argument*/NULL);
			NullCheck(L_87);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_87, 3);
			ArrayElementTypeCheck (L_87, L_90);
			*((String_t**)(String_t**)SZArrayLdElema(L_87, 3, sizeof(String_t*))) = (String_t*)L_90;
			StringU5BU5D_t1_238* L_91 = L_87;
			NullCheck(L_91);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_91, 4);
			ArrayElementTypeCheck (L_91, _stringLiteral4729);
			*((String_t**)(String_t**)SZArrayLdElema(L_91, 4, sizeof(String_t*))) = (String_t*)_stringLiteral4729;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_92 = String_Concat_m1_563(NULL /*static, unused*/, L_91, /*hidden argument*/NULL);
			V_12 = L_92;
		}

IL_0224:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t1_406_il2cpp_TypeInfo_var);
			Encoding_t1_406 * L_93 = Encoding_get_UTF8_m1_12363(NULL /*static, unused*/, /*hidden argument*/NULL);
			String_t* L_94 = V_12;
			NullCheck(L_93);
			ByteU5BU5D_t1_109* L_95 = (ByteU5BU5D_t1_109*)VirtFuncInvoker1< ByteU5BU5D_t1_109*, String_t* >::Invoke(11 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_93, L_94);
			V_13 = L_95;
			MemoryStream_t1_433 * L_96 = V_6;
			ByteU5BU5D_t1_109* L_97 = V_5;
			ByteU5BU5D_t1_109* L_98 = V_5;
			NullCheck(L_98);
			NullCheck(L_96);
			VirtActionInvoker3< ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(24 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_96, L_97, 0, (((int32_t)((int32_t)(((Array_t *)L_98)->max_length)))));
			MemoryStream_t1_433 * L_99 = V_6;
			ByteU5BU5D_t1_109* L_100 = V_13;
			ByteU5BU5D_t1_109* L_101 = V_13;
			NullCheck(L_101);
			NullCheck(L_99);
			VirtActionInvoker3< ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(24 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_99, L_100, 0, (((int32_t)((int32_t)(((Array_t *)L_101)->max_length)))));
			MemoryStream_t1_433 * L_102 = V_6;
			ByteU5BU5D_t1_109* L_103 = V_4;
			ByteU5BU5D_t1_109* L_104 = V_4;
			NullCheck(L_104);
			NullCheck(L_102);
			VirtActionInvoker3< ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(24 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_102, L_103, 0, (((int32_t)((int32_t)(((Array_t *)L_104)->max_length)))));
		}

IL_025c:
		{
			MemoryStream_t1_433 * L_105 = V_6;
			ByteU5BU5D_t1_109* L_106 = V_1;
			ByteU5BU5D_t1_109* L_107 = V_1;
			NullCheck(L_107);
			NullCheck(L_105);
			VirtActionInvoker3< ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(24 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_105, L_106, 0, (((int32_t)((int32_t)(((Array_t *)L_107)->max_length)))));
			MemoryStream_t1_433 * L_108 = V_6;
			ByteU5BU5D_t1_109* L_109 = V_1;
			ByteU5BU5D_t1_109* L_110 = V_1;
			NullCheck(L_110);
			NullCheck(L_108);
			VirtActionInvoker3< ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(24 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_108, L_109, 0, (((int32_t)((int32_t)(((Array_t *)L_110)->max_length)))));
			List_1_t1_1821 * L_111 = (__this->___formData_0);
			int32_t L_112 = V_7;
			NullCheck(L_111);
			ByteU5BU5D_t1_109* L_113 = (ByteU5BU5D_t1_109*)VirtFuncInvoker1< ByteU5BU5D_t1_109*, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.Byte[]>::get_Item(System.Int32) */, L_111, L_112);
			V_14 = L_113;
			MemoryStream_t1_433 * L_114 = V_6;
			ByteU5BU5D_t1_109* L_115 = V_14;
			ByteU5BU5D_t1_109* L_116 = V_14;
			NullCheck(L_116);
			NullCheck(L_114);
			VirtActionInvoker3< ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(24 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_114, L_115, 0, (((int32_t)((int32_t)(((Array_t *)L_116)->max_length)))));
			int32_t L_117 = V_7;
			V_7 = ((int32_t)((int32_t)L_117+(int32_t)1));
		}

IL_0297:
		{
			int32_t L_118 = V_7;
			List_1_t1_1821 * L_119 = (__this->___formData_0);
			NullCheck(L_119);
			int32_t L_120 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.Byte[]>::get_Count() */, L_119);
			if ((((int32_t)L_118) < ((int32_t)L_120)))
			{
				goto IL_0081;
			}
		}

IL_02a9:
		{
			MemoryStream_t1_433 * L_121 = V_6;
			ByteU5BU5D_t1_109* L_122 = V_1;
			ByteU5BU5D_t1_109* L_123 = V_1;
			NullCheck(L_123);
			NullCheck(L_121);
			VirtActionInvoker3< ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(24 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_121, L_122, 0, (((int32_t)((int32_t)(((Array_t *)L_123)->max_length)))));
			MemoryStream_t1_433 * L_124 = V_6;
			ByteU5BU5D_t1_109* L_125 = V_0;
			ByteU5BU5D_t1_109* L_126 = V_0;
			NullCheck(L_126);
			NullCheck(L_124);
			VirtActionInvoker3< ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(24 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_124, L_125, 0, (((int32_t)((int32_t)(((Array_t *)L_126)->max_length)))));
			MemoryStream_t1_433 * L_127 = V_6;
			ByteU5BU5D_t1_109* L_128 = (__this->___boundary_4);
			ByteU5BU5D_t1_109* L_129 = (__this->___boundary_4);
			NullCheck(L_129);
			NullCheck(L_127);
			VirtActionInvoker3< ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(24 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_127, L_128, 0, (((int32_t)((int32_t)(((Array_t *)L_129)->max_length)))));
			MemoryStream_t1_433 * L_130 = V_6;
			ByteU5BU5D_t1_109* L_131 = V_0;
			ByteU5BU5D_t1_109* L_132 = V_0;
			NullCheck(L_132);
			NullCheck(L_130);
			VirtActionInvoker3< ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(24 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_130, L_131, 0, (((int32_t)((int32_t)(((Array_t *)L_132)->max_length)))));
			MemoryStream_t1_433 * L_133 = V_6;
			ByteU5BU5D_t1_109* L_134 = V_1;
			ByteU5BU5D_t1_109* L_135 = V_1;
			NullCheck(L_135);
			NullCheck(L_133);
			VirtActionInvoker3< ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(24 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_133, L_134, 0, (((int32_t)((int32_t)(((Array_t *)L_135)->max_length)))));
			MemoryStream_t1_433 * L_136 = V_6;
			NullCheck(L_136);
			ByteU5BU5D_t1_109* L_137 = (ByteU5BU5D_t1_109*)VirtFuncInvoker0< ByteU5BU5D_t1_109* >::Invoke(33 /* System.Byte[] System.IO.MemoryStream::ToArray() */, L_136);
			V_22 = L_137;
			IL2CPP_LEAVE(0x3F7, FINALLY_0302);
		}

IL_02fd:
		{
			; // IL_02fd: leave IL_0311
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0302;
	}

FINALLY_0302:
	{ // begin finally (depth: 1)
		{
			MemoryStream_t1_433 * L_138 = V_6;
			if (!L_138)
			{
				goto IL_0310;
			}
		}

IL_0309:
		{
			MemoryStream_t1_433 * L_139 = V_6;
			NullCheck(L_139);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_139);
		}

IL_0310:
		{
			IL2CPP_END_FINALLY(770)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(770)
	{
		IL2CPP_JUMP_TBL(0x3F7, IL_03f7)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0311:
	{
		Encoding_t1_406 * L_140 = WWW_get_DefaultEncoding_m6_551(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_140);
		ByteU5BU5D_t1_109* L_141 = (ByteU5BU5D_t1_109*)VirtFuncInvoker1< ByteU5BU5D_t1_109*, String_t* >::Invoke(11 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_140, _stringLiteral1537);
		V_15 = L_141;
		Encoding_t1_406 * L_142 = WWW_get_DefaultEncoding_m6_551(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_142);
		ByteU5BU5D_t1_109* L_143 = (ByteU5BU5D_t1_109*)VirtFuncInvoker1< ByteU5BU5D_t1_109*, String_t* >::Invoke(11 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_142, _stringLiteral582);
		V_16 = L_143;
		MemoryStream_t1_433 * L_144 = (MemoryStream_t1_433 *)il2cpp_codegen_object_new (MemoryStream_t1_433_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m1_5045(L_144, ((int32_t)1024), /*hidden argument*/NULL);
		V_17 = L_144;
	}

IL_033f:
	try
	{ // begin try (depth: 1)
		{
			V_18 = 0;
			goto IL_03c3;
		}

IL_0347:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t1_406_il2cpp_TypeInfo_var);
			Encoding_t1_406 * L_145 = Encoding_get_UTF8_m1_12363(NULL /*static, unused*/, /*hidden argument*/NULL);
			List_1_t1_1742 * L_146 = (__this->___fieldNames_1);
			int32_t L_147 = V_18;
			NullCheck(L_146);
			String_t* L_148 = (String_t*)VirtFuncInvoker1< String_t*, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.String>::get_Item(System.Int32) */, L_146, L_147);
			NullCheck(L_145);
			ByteU5BU5D_t1_109* L_149 = (ByteU5BU5D_t1_109*)VirtFuncInvoker1< ByteU5BU5D_t1_109*, String_t* >::Invoke(11 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_145, L_148);
			IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t6_80_il2cpp_TypeInfo_var);
			ByteU5BU5D_t1_109* L_150 = WWWTranscoder_URLEncode_m6_567(NULL /*static, unused*/, L_149, /*hidden argument*/NULL);
			V_19 = L_150;
			List_1_t1_1821 * L_151 = (__this->___formData_0);
			int32_t L_152 = V_18;
			NullCheck(L_151);
			ByteU5BU5D_t1_109* L_153 = (ByteU5BU5D_t1_109*)VirtFuncInvoker1< ByteU5BU5D_t1_109*, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.Byte[]>::get_Item(System.Int32) */, L_151, L_152);
			V_20 = L_153;
			ByteU5BU5D_t1_109* L_154 = V_20;
			ByteU5BU5D_t1_109* L_155 = WWWTranscoder_URLEncode_m6_567(NULL /*static, unused*/, L_154, /*hidden argument*/NULL);
			V_21 = L_155;
			int32_t L_156 = V_18;
			if ((((int32_t)L_156) <= ((int32_t)0)))
			{
				goto IL_0393;
			}
		}

IL_0385:
		{
			MemoryStream_t1_433 * L_157 = V_17;
			ByteU5BU5D_t1_109* L_158 = V_15;
			ByteU5BU5D_t1_109* L_159 = V_15;
			NullCheck(L_159);
			NullCheck(L_157);
			VirtActionInvoker3< ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(24 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_157, L_158, 0, (((int32_t)((int32_t)(((Array_t *)L_159)->max_length)))));
		}

IL_0393:
		{
			MemoryStream_t1_433 * L_160 = V_17;
			ByteU5BU5D_t1_109* L_161 = V_19;
			ByteU5BU5D_t1_109* L_162 = V_19;
			NullCheck(L_162);
			NullCheck(L_160);
			VirtActionInvoker3< ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(24 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_160, L_161, 0, (((int32_t)((int32_t)(((Array_t *)L_162)->max_length)))));
			MemoryStream_t1_433 * L_163 = V_17;
			ByteU5BU5D_t1_109* L_164 = V_16;
			ByteU5BU5D_t1_109* L_165 = V_16;
			NullCheck(L_165);
			NullCheck(L_163);
			VirtActionInvoker3< ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(24 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_163, L_164, 0, (((int32_t)((int32_t)(((Array_t *)L_165)->max_length)))));
			MemoryStream_t1_433 * L_166 = V_17;
			ByteU5BU5D_t1_109* L_167 = V_21;
			ByteU5BU5D_t1_109* L_168 = V_21;
			NullCheck(L_168);
			NullCheck(L_166);
			VirtActionInvoker3< ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(24 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_166, L_167, 0, (((int32_t)((int32_t)(((Array_t *)L_168)->max_length)))));
			int32_t L_169 = V_18;
			V_18 = ((int32_t)((int32_t)L_169+(int32_t)1));
		}

IL_03c3:
		{
			int32_t L_170 = V_18;
			List_1_t1_1821 * L_171 = (__this->___formData_0);
			NullCheck(L_171);
			int32_t L_172 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.Byte[]>::get_Count() */, L_171);
			if ((((int32_t)L_170) < ((int32_t)L_172)))
			{
				goto IL_0347;
			}
		}

IL_03d5:
		{
			MemoryStream_t1_433 * L_173 = V_17;
			NullCheck(L_173);
			ByteU5BU5D_t1_109* L_174 = (ByteU5BU5D_t1_109*)VirtFuncInvoker0< ByteU5BU5D_t1_109* >::Invoke(33 /* System.Byte[] System.IO.MemoryStream::ToArray() */, L_173);
			V_22 = L_174;
			IL2CPP_LEAVE(0x3F7, FINALLY_03e8);
		}

IL_03e3:
		{
			; // IL_03e3: leave IL_03f7
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_03e8;
	}

FINALLY_03e8:
	{ // begin finally (depth: 1)
		{
			MemoryStream_t1_433 * L_175 = V_17;
			if (!L_175)
			{
				goto IL_03f6;
			}
		}

IL_03ef:
		{
			MemoryStream_t1_433 * L_176 = V_17;
			NullCheck(L_176);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_176);
		}

IL_03f6:
		{
			IL2CPP_END_FINALLY(1000)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(1000)
	{
		IL2CPP_JUMP_TBL(0x3F7, IL_03f7)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_03f7:
	{
		ByteU5BU5D_t1_109* L_177 = V_22;
		return L_177;
	}
}
// System.Void UnityEngine.WWWTranscoder::.cctor()
extern TypeInfo* WWWTranscoder_t6_80_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral727;
extern Il2CppCodeGenString* _stringLiteral4730;
extern Il2CppCodeGenString* _stringLiteral4731;
extern Il2CppCodeGenString* _stringLiteral4732;
extern "C" void WWWTranscoder__cctor_m6_565 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WWWTranscoder_t6_80_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1605);
		_stringLiteral727 = il2cpp_codegen_string_literal_from_index(727);
		_stringLiteral4730 = il2cpp_codegen_string_literal_from_index(4730);
		_stringLiteral4731 = il2cpp_codegen_string_literal_from_index(4731);
		_stringLiteral4732 = il2cpp_codegen_string_literal_from_index(4732);
		s_Il2CppMethodIntialized = true;
	}
	{
		Encoding_t1_406 * L_0 = WWW_get_DefaultEncoding_m6_551(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		ByteU5BU5D_t1_109* L_1 = (ByteU5BU5D_t1_109*)VirtFuncInvoker1< ByteU5BU5D_t1_109*, String_t* >::Invoke(11 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_0, _stringLiteral727);
		((WWWTranscoder_t6_80_StaticFields*)WWWTranscoder_t6_80_il2cpp_TypeInfo_var->static_fields)->___ucHexChars_0 = L_1;
		Encoding_t1_406 * L_2 = WWW_get_DefaultEncoding_m6_551(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		ByteU5BU5D_t1_109* L_3 = (ByteU5BU5D_t1_109*)VirtFuncInvoker1< ByteU5BU5D_t1_109*, String_t* >::Invoke(11 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_2, _stringLiteral4730);
		((WWWTranscoder_t6_80_StaticFields*)WWWTranscoder_t6_80_il2cpp_TypeInfo_var->static_fields)->___lcHexChars_1 = L_3;
		((WWWTranscoder_t6_80_StaticFields*)WWWTranscoder_t6_80_il2cpp_TypeInfo_var->static_fields)->___urlEscapeChar_2 = ((int32_t)37);
		((WWWTranscoder_t6_80_StaticFields*)WWWTranscoder_t6_80_il2cpp_TypeInfo_var->static_fields)->___urlSpace_3 = ((int32_t)43);
		Encoding_t1_406 * L_4 = WWW_get_DefaultEncoding_m6_551(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		ByteU5BU5D_t1_109* L_5 = (ByteU5BU5D_t1_109*)VirtFuncInvoker1< ByteU5BU5D_t1_109*, String_t* >::Invoke(11 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_4, _stringLiteral4731);
		((WWWTranscoder_t6_80_StaticFields*)WWWTranscoder_t6_80_il2cpp_TypeInfo_var->static_fields)->___urlForbidden_4 = L_5;
		((WWWTranscoder_t6_80_StaticFields*)WWWTranscoder_t6_80_il2cpp_TypeInfo_var->static_fields)->___qpEscapeChar_5 = ((int32_t)61);
		((WWWTranscoder_t6_80_StaticFields*)WWWTranscoder_t6_80_il2cpp_TypeInfo_var->static_fields)->___qpSpace_6 = ((int32_t)95);
		Encoding_t1_406 * L_6 = WWW_get_DefaultEncoding_m6_551(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		ByteU5BU5D_t1_109* L_7 = (ByteU5BU5D_t1_109*)VirtFuncInvoker1< ByteU5BU5D_t1_109*, String_t* >::Invoke(11 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_6, _stringLiteral4732);
		((WWWTranscoder_t6_80_StaticFields*)WWWTranscoder_t6_80_il2cpp_TypeInfo_var->static_fields)->___qpForbidden_7 = L_7;
		return;
	}
}
// System.Byte[] UnityEngine.WWWTranscoder::Byte2Hex(System.Byte,System.Byte[])
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern "C" ByteU5BU5D_t1_109* WWWTranscoder_Byte2Hex_m6_566 (Object_t * __this /* static, unused */, uint8_t ___b, ByteU5BU5D_t1_109* ___hexChars, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	{
		V_0 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 2));
		ByteU5BU5D_t1_109* L_0 = V_0;
		ByteU5BU5D_t1_109* L_1 = ___hexChars;
		uint8_t L_2 = ___b;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, ((int32_t)((int32_t)L_2>>(int32_t)4)));
		int32_t L_3 = ((int32_t)((int32_t)L_2>>(int32_t)4));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_0, 0, sizeof(uint8_t))) = (uint8_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_1, L_3, sizeof(uint8_t)));
		ByteU5BU5D_t1_109* L_4 = V_0;
		ByteU5BU5D_t1_109* L_5 = ___hexChars;
		uint8_t L_6 = ___b;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, ((int32_t)((int32_t)L_6&(int32_t)((int32_t)15))));
		int32_t L_7 = ((int32_t)((int32_t)L_6&(int32_t)((int32_t)15)));
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_4, 1, sizeof(uint8_t))) = (uint8_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_5, L_7, sizeof(uint8_t)));
		ByteU5BU5D_t1_109* L_8 = V_0;
		return L_8;
	}
}
// System.Byte[] UnityEngine.WWWTranscoder::URLEncode(System.Byte[])
extern TypeInfo* WWWTranscoder_t6_80_il2cpp_TypeInfo_var;
extern "C" ByteU5BU5D_t1_109* WWWTranscoder_URLEncode_m6_567 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___toEncode, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WWWTranscoder_t6_80_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1605);
		s_Il2CppMethodIntialized = true;
	}
	{
		ByteU5BU5D_t1_109* L_0 = ___toEncode;
		IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t6_80_il2cpp_TypeInfo_var);
		uint8_t L_1 = ((WWWTranscoder_t6_80_StaticFields*)WWWTranscoder_t6_80_il2cpp_TypeInfo_var->static_fields)->___urlEscapeChar_2;
		uint8_t L_2 = ((WWWTranscoder_t6_80_StaticFields*)WWWTranscoder_t6_80_il2cpp_TypeInfo_var->static_fields)->___urlSpace_3;
		ByteU5BU5D_t1_109* L_3 = ((WWWTranscoder_t6_80_StaticFields*)WWWTranscoder_t6_80_il2cpp_TypeInfo_var->static_fields)->___urlForbidden_4;
		ByteU5BU5D_t1_109* L_4 = WWWTranscoder_Encode_m6_569(NULL /*static, unused*/, L_0, L_1, L_2, L_3, 0, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.String UnityEngine.WWWTranscoder::QPEncode(System.String,System.Text.Encoding)
extern TypeInfo* WWWTranscoder_t6_80_il2cpp_TypeInfo_var;
extern "C" String_t* WWWTranscoder_QPEncode_m6_568 (Object_t * __this /* static, unused */, String_t* ___toEncode, Encoding_t1_406 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WWWTranscoder_t6_80_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1605);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	{
		Encoding_t1_406 * L_0 = ___e;
		String_t* L_1 = ___toEncode;
		NullCheck(L_0);
		ByteU5BU5D_t1_109* L_2 = (ByteU5BU5D_t1_109*)VirtFuncInvoker1< ByteU5BU5D_t1_109*, String_t* >::Invoke(11 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_0, L_1);
		IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t6_80_il2cpp_TypeInfo_var);
		uint8_t L_3 = ((WWWTranscoder_t6_80_StaticFields*)WWWTranscoder_t6_80_il2cpp_TypeInfo_var->static_fields)->___qpEscapeChar_5;
		uint8_t L_4 = ((WWWTranscoder_t6_80_StaticFields*)WWWTranscoder_t6_80_il2cpp_TypeInfo_var->static_fields)->___qpSpace_6;
		ByteU5BU5D_t1_109* L_5 = ((WWWTranscoder_t6_80_StaticFields*)WWWTranscoder_t6_80_il2cpp_TypeInfo_var->static_fields)->___qpForbidden_7;
		ByteU5BU5D_t1_109* L_6 = WWWTranscoder_Encode_m6_569(NULL /*static, unused*/, L_2, L_3, L_4, L_5, 1, /*hidden argument*/NULL);
		V_0 = L_6;
		Encoding_t1_406 * L_7 = WWW_get_DefaultEncoding_m6_551(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_8 = V_0;
		ByteU5BU5D_t1_109* L_9 = V_0;
		NullCheck(L_9);
		NullCheck(L_7);
		String_t* L_10 = (String_t*)VirtFuncInvoker3< String_t*, ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(26 /* System.String System.Text.Encoding::GetString(System.Byte[],System.Int32,System.Int32) */, L_7, L_8, 0, (((int32_t)((int32_t)(((Array_t *)L_9)->max_length)))));
		return L_10;
	}
}
// System.Byte[] UnityEngine.WWWTranscoder::Encode(System.Byte[],System.Byte,System.Byte,System.Byte[],System.Boolean)
extern TypeInfo* MemoryStream_t1_433_il2cpp_TypeInfo_var;
extern TypeInfo* WWWTranscoder_t6_80_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern "C" ByteU5BU5D_t1_109* WWWTranscoder_Encode_m6_569 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___input, uint8_t ___escapeChar, uint8_t ___space, ByteU5BU5D_t1_109* ___forbidden, bool ___uppercase, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MemoryStream_t1_433_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(390);
		WWWTranscoder_t6_80_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1605);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		s_Il2CppMethodIntialized = true;
	}
	MemoryStream_t1_433 * V_0 = {0};
	int32_t V_1 = 0;
	ByteU5BU5D_t1_109* V_2 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B9_0 = 0;
	MemoryStream_t1_433 * G_B9_1 = {0};
	int32_t G_B8_0 = 0;
	MemoryStream_t1_433 * G_B8_1 = {0};
	ByteU5BU5D_t1_109* G_B10_0 = {0};
	int32_t G_B10_1 = 0;
	MemoryStream_t1_433 * G_B10_2 = {0};
	{
		ByteU5BU5D_t1_109* L_0 = ___input;
		NullCheck(L_0);
		MemoryStream_t1_433 * L_1 = (MemoryStream_t1_433 *)il2cpp_codegen_object_new (MemoryStream_t1_433_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m1_5045(L_1, ((int32_t)((int32_t)(((int32_t)((int32_t)(((Array_t *)L_0)->max_length))))*(int32_t)2)), /*hidden argument*/NULL);
		V_0 = L_1;
	}

IL_000b:
	try
	{ // begin try (depth: 1)
		{
			V_1 = 0;
			goto IL_0089;
		}

IL_0012:
		{
			ByteU5BU5D_t1_109* L_2 = ___input;
			int32_t L_3 = V_1;
			NullCheck(L_2);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
			int32_t L_4 = L_3;
			if ((!(((uint32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_2, L_4, sizeof(uint8_t)))) == ((uint32_t)((int32_t)32)))))
			{
				goto IL_0028;
			}
		}

IL_001c:
		{
			MemoryStream_t1_433 * L_5 = V_0;
			uint8_t L_6 = ___space;
			NullCheck(L_5);
			VirtActionInvoker1< uint8_t >::Invoke(25 /* System.Void System.IO.MemoryStream::WriteByte(System.Byte) */, L_5, L_6);
			goto IL_0085;
		}

IL_0028:
		{
			ByteU5BU5D_t1_109* L_7 = ___input;
			int32_t L_8 = V_1;
			NullCheck(L_7);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
			int32_t L_9 = L_8;
			if ((((int32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_7, L_9, sizeof(uint8_t)))) < ((int32_t)((int32_t)32))))
			{
				goto IL_004a;
			}
		}

IL_0032:
		{
			ByteU5BU5D_t1_109* L_10 = ___input;
			int32_t L_11 = V_1;
			NullCheck(L_10);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
			int32_t L_12 = L_11;
			if ((((int32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_10, L_12, sizeof(uint8_t)))) > ((int32_t)((int32_t)126))))
			{
				goto IL_004a;
			}
		}

IL_003c:
		{
			ByteU5BU5D_t1_109* L_13 = ___forbidden;
			ByteU5BU5D_t1_109* L_14 = ___input;
			int32_t L_15 = V_1;
			NullCheck(L_14);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
			int32_t L_16 = L_15;
			IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t6_80_il2cpp_TypeInfo_var);
			bool L_17 = WWWTranscoder_ByteArrayContains_m6_570(NULL /*static, unused*/, L_13, (*(uint8_t*)(uint8_t*)SZArrayLdElema(L_14, L_16, sizeof(uint8_t))), /*hidden argument*/NULL);
			if (!L_17)
			{
				goto IL_007c;
			}
		}

IL_004a:
		{
			MemoryStream_t1_433 * L_18 = V_0;
			uint8_t L_19 = ___escapeChar;
			NullCheck(L_18);
			VirtActionInvoker1< uint8_t >::Invoke(25 /* System.Void System.IO.MemoryStream::WriteByte(System.Byte) */, L_18, L_19);
			MemoryStream_t1_433 * L_20 = V_0;
			ByteU5BU5D_t1_109* L_21 = ___input;
			int32_t L_22 = V_1;
			NullCheck(L_21);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_21, L_22);
			int32_t L_23 = L_22;
			bool L_24 = ___uppercase;
			G_B8_0 = ((int32_t)((*(uint8_t*)(uint8_t*)SZArrayLdElema(L_21, L_23, sizeof(uint8_t)))));
			G_B8_1 = L_20;
			if (!L_24)
			{
				G_B9_0 = ((int32_t)((*(uint8_t*)(uint8_t*)SZArrayLdElema(L_21, L_23, sizeof(uint8_t)))));
				G_B9_1 = L_20;
				goto IL_0066;
			}
		}

IL_005c:
		{
			IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t6_80_il2cpp_TypeInfo_var);
			ByteU5BU5D_t1_109* L_25 = ((WWWTranscoder_t6_80_StaticFields*)WWWTranscoder_t6_80_il2cpp_TypeInfo_var->static_fields)->___ucHexChars_0;
			G_B10_0 = L_25;
			G_B10_1 = G_B8_0;
			G_B10_2 = G_B8_1;
			goto IL_006b;
		}

IL_0066:
		{
			IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t6_80_il2cpp_TypeInfo_var);
			ByteU5BU5D_t1_109* L_26 = ((WWWTranscoder_t6_80_StaticFields*)WWWTranscoder_t6_80_il2cpp_TypeInfo_var->static_fields)->___lcHexChars_1;
			G_B10_0 = L_26;
			G_B10_1 = G_B9_0;
			G_B10_2 = G_B9_1;
		}

IL_006b:
		{
			IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t6_80_il2cpp_TypeInfo_var);
			ByteU5BU5D_t1_109* L_27 = WWWTranscoder_Byte2Hex_m6_566(NULL /*static, unused*/, G_B10_1, G_B10_0, /*hidden argument*/NULL);
			NullCheck(G_B10_2);
			VirtActionInvoker3< ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(24 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, G_B10_2, L_27, 0, 2);
			goto IL_0085;
		}

IL_007c:
		{
			MemoryStream_t1_433 * L_28 = V_0;
			ByteU5BU5D_t1_109* L_29 = ___input;
			int32_t L_30 = V_1;
			NullCheck(L_29);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_29, L_30);
			int32_t L_31 = L_30;
			NullCheck(L_28);
			VirtActionInvoker1< uint8_t >::Invoke(25 /* System.Void System.IO.MemoryStream::WriteByte(System.Byte) */, L_28, (*(uint8_t*)(uint8_t*)SZArrayLdElema(L_29, L_31, sizeof(uint8_t))));
		}

IL_0085:
		{
			int32_t L_32 = V_1;
			V_1 = ((int32_t)((int32_t)L_32+(int32_t)1));
		}

IL_0089:
		{
			int32_t L_33 = V_1;
			ByteU5BU5D_t1_109* L_34 = ___input;
			NullCheck(L_34);
			if ((((int32_t)L_33) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_34)->max_length)))))))
			{
				goto IL_0012;
			}
		}

IL_0092:
		{
			MemoryStream_t1_433 * L_35 = V_0;
			NullCheck(L_35);
			ByteU5BU5D_t1_109* L_36 = (ByteU5BU5D_t1_109*)VirtFuncInvoker0< ByteU5BU5D_t1_109* >::Invoke(33 /* System.Byte[] System.IO.MemoryStream::ToArray() */, L_35);
			V_2 = L_36;
			IL2CPP_LEAVE(0xB0, FINALLY_00a3);
		}

IL_009e:
		{
			; // IL_009e: leave IL_00b0
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_00a3;
	}

FINALLY_00a3:
	{ // begin finally (depth: 1)
		{
			MemoryStream_t1_433 * L_37 = V_0;
			if (!L_37)
			{
				goto IL_00af;
			}
		}

IL_00a9:
		{
			MemoryStream_t1_433 * L_38 = V_0;
			NullCheck(L_38);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_38);
		}

IL_00af:
		{
			IL2CPP_END_FINALLY(163)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(163)
	{
		IL2CPP_JUMP_TBL(0xB0, IL_00b0)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_00b0:
	{
		ByteU5BU5D_t1_109* L_39 = V_2;
		return L_39;
	}
}
// System.Boolean UnityEngine.WWWTranscoder::ByteArrayContains(System.Byte[],System.Byte)
extern "C" bool WWWTranscoder_ByteArrayContains_m6_570 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___array, uint8_t ___b, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		ByteU5BU5D_t1_109* L_0 = ___array;
		NullCheck(L_0);
		V_0 = (((int32_t)((int32_t)(((Array_t *)L_0)->max_length))));
		V_1 = 0;
		goto IL_001a;
	}

IL_000b:
	{
		ByteU5BU5D_t1_109* L_1 = ___array;
		int32_t L_2 = V_1;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		uint8_t L_4 = ___b;
		if ((!(((uint32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_1, L_3, sizeof(uint8_t)))) == ((uint32_t)L_4))))
		{
			goto IL_0016;
		}
	}
	{
		return 1;
	}

IL_0016:
	{
		int32_t L_5 = V_1;
		V_1 = ((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_001a:
	{
		int32_t L_6 = V_1;
		int32_t L_7 = V_0;
		if ((((int32_t)L_6) < ((int32_t)L_7)))
		{
			goto IL_000b;
		}
	}
	{
		return 0;
	}
}
// System.Boolean UnityEngine.WWWTranscoder::SevenBitClean(System.String,System.Text.Encoding)
extern TypeInfo* WWWTranscoder_t6_80_il2cpp_TypeInfo_var;
extern "C" bool WWWTranscoder_SevenBitClean_m6_571 (Object_t * __this /* static, unused */, String_t* ___s, Encoding_t1_406 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WWWTranscoder_t6_80_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1605);
		s_Il2CppMethodIntialized = true;
	}
	{
		Encoding_t1_406 * L_0 = ___e;
		String_t* L_1 = ___s;
		NullCheck(L_0);
		ByteU5BU5D_t1_109* L_2 = (ByteU5BU5D_t1_109*)VirtFuncInvoker1< ByteU5BU5D_t1_109*, String_t* >::Invoke(11 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_0, L_1);
		IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t6_80_il2cpp_TypeInfo_var);
		bool L_3 = WWWTranscoder_SevenBitClean_m6_572(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean UnityEngine.WWWTranscoder::SevenBitClean(System.Byte[])
extern "C" bool WWWTranscoder_SevenBitClean_m6_572 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___input, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0021;
	}

IL_0007:
	{
		ByteU5BU5D_t1_109* L_0 = ___input;
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		if ((((int32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_0, L_2, sizeof(uint8_t)))) < ((int32_t)((int32_t)32))))
		{
			goto IL_001b;
		}
	}
	{
		ByteU5BU5D_t1_109* L_3 = ___input;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		if ((((int32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_3, L_5, sizeof(uint8_t)))) <= ((int32_t)((int32_t)126))))
		{
			goto IL_001d;
		}
	}

IL_001b:
	{
		return 0;
	}

IL_001d:
	{
		int32_t L_6 = V_0;
		V_0 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0021:
	{
		int32_t L_7 = V_0;
		ByteU5BU5D_t1_109* L_8 = ___input;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_8)->max_length)))))))
		{
			goto IL_0007;
		}
	}
	{
		return 1;
	}
}
// Conversion methods for marshalling of: UnityEngine.CacheIndex
extern "C" void CacheIndex_t6_81_marshal(const CacheIndex_t6_81& unmarshaled, CacheIndex_t6_81_marshaled& marshaled)
{
	marshaled.___name_0 = il2cpp_codegen_marshal_string(unmarshaled.___name_0);
	marshaled.___bytesUsed_1 = unmarshaled.___bytesUsed_1;
	marshaled.___expires_2 = unmarshaled.___expires_2;
}
extern "C" void CacheIndex_t6_81_marshal_back(const CacheIndex_t6_81_marshaled& marshaled, CacheIndex_t6_81& unmarshaled)
{
	unmarshaled.___name_0 = il2cpp_codegen_marshal_string_result(marshaled.___name_0);
	unmarshaled.___bytesUsed_1 = marshaled.___bytesUsed_1;
	unmarshaled.___expires_2 = marshaled.___expires_2;
}
// Conversion method for clean up from marshalling of: UnityEngine.CacheIndex
extern "C" void CacheIndex_t6_81_marshal_cleanup(CacheIndex_t6_81_marshaled& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___name_0);
	marshaled.___name_0 = NULL;
}
// System.String UnityEngine.UnityString::Format(System.String,System.Object[])
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" String_t* UnityString_Format_m6_573 (Object_t * __this /* static, unused */, String_t* ___fmt, ObjectU5BU5D_t1_272* ___args, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___fmt;
		ObjectU5BU5D_t1_272* L_1 = ___args;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Format_m1_551(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.AsyncOperation::.ctor()
extern "C" void AsyncOperation__ctor_m6_574 (AsyncOperation_t6_2 * __this, const MethodInfo* method)
{
	{
		YieldInstruction__ctor_m6_799(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AsyncOperation::InternalDestroy()
extern "C" void AsyncOperation_InternalDestroy_m6_575 (AsyncOperation_t6_2 * __this, const MethodInfo* method)
{
	typedef void (*AsyncOperation_InternalDestroy_m6_575_ftn) (AsyncOperation_t6_2 *);
	static AsyncOperation_InternalDestroy_m6_575_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AsyncOperation_InternalDestroy_m6_575_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AsyncOperation::InternalDestroy()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AsyncOperation::Finalize()
extern "C" void AsyncOperation_Finalize_m6_576 (AsyncOperation_t6_2 * __this, const MethodInfo* method)
{
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		AsyncOperation_InternalDestroy_m6_575(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m1_3(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0012:
	{
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.AsyncOperation
extern "C" void AsyncOperation_t6_2_marshal(const AsyncOperation_t6_2& unmarshaled, AsyncOperation_t6_2_marshaled& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.___m_Ptr_0).___m_value_0);
}
extern "C" void AsyncOperation_t6_2_marshal_back(const AsyncOperation_t6_2_marshaled& marshaled, AsyncOperation_t6_2& unmarshaled)
{
	(unmarshaled.___m_Ptr_0).___m_value_0 = reinterpret_cast<void*>(marshaled.___m_Ptr_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.AsyncOperation
extern "C" void AsyncOperation_t6_2_marshal_cleanup(AsyncOperation_t6_2_marshaled& marshaled)
{
}
// System.Void UnityEngine.Application/LogCallback::.ctor(System.Object,System.IntPtr)
extern "C" void LogCallback__ctor_m6_577 (LogCallback_t6_83 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.Application/LogCallback::Invoke(System.String,System.String,UnityEngine.LogType)
extern "C" void LogCallback_Invoke_m6_578 (LogCallback_t6_83 * __this, String_t* ___condition, String_t* ___stackTrace, int32_t ___type, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		LogCallback_Invoke_m6_578((LogCallback_t6_83 *)__this->___prev_9,___condition, ___stackTrace, ___type, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, String_t* ___condition, String_t* ___stackTrace, int32_t ___type, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___condition, ___stackTrace, ___type,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, String_t* ___condition, String_t* ___stackTrace, int32_t ___type, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___condition, ___stackTrace, ___type,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, String_t* ___stackTrace, int32_t ___type, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(___condition, ___stackTrace, ___type,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_LogCallback_t6_83(Il2CppObject* delegate, String_t* ___condition, String_t* ___stackTrace, int32_t ___type)
{
	typedef void (STDCALL *native_function_ptr_type)(char*, char*, int32_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___condition' to native representation
	char* ____condition_marshaled = { 0 };
	____condition_marshaled = il2cpp_codegen_marshal_string(___condition);

	// Marshaling of parameter '___stackTrace' to native representation
	char* ____stackTrace_marshaled = { 0 };
	____stackTrace_marshaled = il2cpp_codegen_marshal_string(___stackTrace);

	// Marshaling of parameter '___type' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____condition_marshaled, ____stackTrace_marshaled, ___type);

	// Marshaling cleanup of parameter '___condition' native representation
	il2cpp_codegen_marshal_free(____condition_marshaled);
	____condition_marshaled = NULL;

	// Marshaling cleanup of parameter '___stackTrace' native representation
	il2cpp_codegen_marshal_free(____stackTrace_marshaled);
	____stackTrace_marshaled = NULL;

	// Marshaling cleanup of parameter '___type' native representation

}
// System.IAsyncResult UnityEngine.Application/LogCallback::BeginInvoke(System.String,System.String,UnityEngine.LogType,System.AsyncCallback,System.Object)
extern TypeInfo* LogType_t6_8_il2cpp_TypeInfo_var;
extern "C" Object_t * LogCallback_BeginInvoke_m6_579 (LogCallback_t6_83 * __this, String_t* ___condition, String_t* ___stackTrace, int32_t ___type, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LogType_t6_8_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1606);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = ___condition;
	__d_args[1] = ___stackTrace;
	__d_args[2] = Box(LogType_t6_8_il2cpp_TypeInfo_var, &___type);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.Application/LogCallback::EndInvoke(System.IAsyncResult)
extern "C" void LogCallback_EndInvoke_m6_580 (LogCallback_t6_83 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void UnityEngine.Application::add_logMessageReceived(UnityEngine.Application/LogCallback)
extern TypeInfo* Application_t6_84_il2cpp_TypeInfo_var;
extern TypeInfo* LogCallback_t6_83_il2cpp_TypeInfo_var;
extern "C" void Application_add_logMessageReceived_m6_581 (Object_t * __this /* static, unused */, LogCallback_t6_83 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Application_t6_84_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1607);
		LogCallback_t6_83_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1608);
		s_Il2CppMethodIntialized = true;
	}
	{
		LogCallback_t6_83 * L_0 = ((Application_t6_84_StaticFields*)Application_t6_84_il2cpp_TypeInfo_var->static_fields)->___s_LogCallbackHandler_0;
		LogCallback_t6_83 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Combine_m1_908(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((Application_t6_84_StaticFields*)Application_t6_84_il2cpp_TypeInfo_var->static_fields)->___s_LogCallbackHandler_0 = ((LogCallback_t6_83 *)CastclassSealed(L_2, LogCallback_t6_83_il2cpp_TypeInfo_var));
		LogCallback_t6_83 * L_3 = ((Application_t6_84_StaticFields*)Application_t6_84_il2cpp_TypeInfo_var->static_fields)->___s_LogCallbackHandlerThreaded_1;
		Application_SetLogCallbackDefined_m6_594(NULL /*static, unused*/, 1, ((((int32_t)((((Object_t*)(LogCallback_t6_83 *)L_3) == ((Object_t*)(Object_t *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Application::remove_logMessageReceived(UnityEngine.Application/LogCallback)
extern TypeInfo* Application_t6_84_il2cpp_TypeInfo_var;
extern TypeInfo* LogCallback_t6_83_il2cpp_TypeInfo_var;
extern "C" void Application_remove_logMessageReceived_m6_582 (Object_t * __this /* static, unused */, LogCallback_t6_83 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Application_t6_84_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1607);
		LogCallback_t6_83_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1608);
		s_Il2CppMethodIntialized = true;
	}
	{
		LogCallback_t6_83 * L_0 = ((Application_t6_84_StaticFields*)Application_t6_84_il2cpp_TypeInfo_var->static_fields)->___s_LogCallbackHandler_0;
		LogCallback_t6_83 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Remove_m1_911(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((Application_t6_84_StaticFields*)Application_t6_84_il2cpp_TypeInfo_var->static_fields)->___s_LogCallbackHandler_0 = ((LogCallback_t6_83 *)CastclassSealed(L_2, LogCallback_t6_83_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Application::LoadLevel(System.String)
extern "C" void Application_LoadLevel_m6_583 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		Application_LoadLevelAsync_m6_584(NULL /*static, unused*/, L_0, (-1), 0, 1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.AsyncOperation UnityEngine.Application::LoadLevelAsync(System.String,System.Int32,System.Boolean,System.Boolean)
extern "C" AsyncOperation_t6_2 * Application_LoadLevelAsync_m6_584 (Object_t * __this /* static, unused */, String_t* ___monoLevelName, int32_t ___index, bool ___additive, bool ___mustCompleteNextFrame, const MethodInfo* method)
{
	typedef AsyncOperation_t6_2 * (*Application_LoadLevelAsync_m6_584_ftn) (String_t*, int32_t, bool, bool);
	static Application_LoadLevelAsync_m6_584_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_LoadLevelAsync_m6_584_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::LoadLevelAsync(System.String,System.Int32,System.Boolean,System.Boolean)");
	return _il2cpp_icall_func(___monoLevelName, ___index, ___additive, ___mustCompleteNextFrame);
}
// System.Boolean UnityEngine.Application::get_isPlaying()
extern "C" bool Application_get_isPlaying_m6_585 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Application_get_isPlaying_m6_585_ftn) ();
	static Application_get_isPlaying_m6_585_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_isPlaying_m6_585_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_isPlaying()");
	return _il2cpp_icall_func();
}
// System.Boolean UnityEngine.Application::get_isEditor()
extern "C" bool Application_get_isEditor_m6_586 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Application_get_isEditor_m6_586_ftn) ();
	static Application_get_isEditor_m6_586_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_isEditor_m6_586_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_isEditor()");
	return _il2cpp_icall_func();
}
// UnityEngine.RuntimePlatform UnityEngine.Application::get_platform()
extern "C" int32_t Application_get_platform_m6_587 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Application_get_platform_m6_587_ftn) ();
	static Application_get_platform_m6_587_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_platform_m6_587_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_platform()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Application::CaptureScreenshot(System.String,System.Int32)
extern "C" void Application_CaptureScreenshot_m6_588 (Object_t * __this /* static, unused */, String_t* ___filename, int32_t ___superSize, const MethodInfo* method)
{
	typedef void (*Application_CaptureScreenshot_m6_588_ftn) (String_t*, int32_t);
	static Application_CaptureScreenshot_m6_588_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_CaptureScreenshot_m6_588_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::CaptureScreenshot(System.String,System.Int32)");
	_il2cpp_icall_func(___filename, ___superSize);
}
// System.Void UnityEngine.Application::CaptureScreenshot(System.String)
extern "C" void Application_CaptureScreenshot_m6_589 (Object_t * __this /* static, unused */, String_t* ___filename, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		String_t* L_0 = ___filename;
		int32_t L_1 = V_0;
		Application_CaptureScreenshot_m6_588(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.Application::get_dataPath()
extern "C" String_t* Application_get_dataPath_m6_590 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*Application_get_dataPath_m6_590_ftn) ();
	static Application_get_dataPath_m6_590_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_dataPath_m6_590_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_dataPath()");
	return _il2cpp_icall_func();
}
// System.String UnityEngine.Application::get_persistentDataPath()
extern "C" String_t* Application_get_persistentDataPath_m6_591 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*Application_get_persistentDataPath_m6_591_ftn) ();
	static Application_get_persistentDataPath_m6_591_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_persistentDataPath_m6_591_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_persistentDataPath()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Application::OpenURL(System.String)
extern "C" void Application_OpenURL_m6_592 (Object_t * __this /* static, unused */, String_t* ___url, const MethodInfo* method)
{
	typedef void (*Application_OpenURL_m6_592_ftn) (String_t*);
	static Application_OpenURL_m6_592_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_OpenURL_m6_592_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::OpenURL(System.String)");
	_il2cpp_icall_func(___url);
}
// System.Void UnityEngine.Application::CallLogCallback(System.String,System.String,UnityEngine.LogType,System.Boolean)
extern TypeInfo* Application_t6_84_il2cpp_TypeInfo_var;
extern "C" void Application_CallLogCallback_m6_593 (Object_t * __this /* static, unused */, String_t* ___logString, String_t* ___stackTrace, int32_t ___type, bool ___invokedOnMainThread, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Application_t6_84_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1607);
		s_Il2CppMethodIntialized = true;
	}
	LogCallback_t6_83 * V_0 = {0};
	LogCallback_t6_83 * V_1 = {0};
	{
		bool L_0 = ___invokedOnMainThread;
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		LogCallback_t6_83 * L_1 = ((Application_t6_84_StaticFields*)Application_t6_84_il2cpp_TypeInfo_var->static_fields)->___s_LogCallbackHandler_0;
		V_0 = L_1;
		LogCallback_t6_83 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_001b;
		}
	}
	{
		LogCallback_t6_83 * L_3 = V_0;
		String_t* L_4 = ___logString;
		String_t* L_5 = ___stackTrace;
		int32_t L_6 = ___type;
		NullCheck(L_3);
		LogCallback_Invoke_m6_578(L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
	}

IL_001b:
	{
		LogCallback_t6_83 * L_7 = ((Application_t6_84_StaticFields*)Application_t6_84_il2cpp_TypeInfo_var->static_fields)->___s_LogCallbackHandlerThreaded_1;
		V_1 = L_7;
		LogCallback_t6_83 * L_8 = V_1;
		if (!L_8)
		{
			goto IL_0030;
		}
	}
	{
		LogCallback_t6_83 * L_9 = V_1;
		String_t* L_10 = ___logString;
		String_t* L_11 = ___stackTrace;
		int32_t L_12 = ___type;
		NullCheck(L_9);
		LogCallback_Invoke_m6_578(L_9, L_10, L_11, L_12, /*hidden argument*/NULL);
	}

IL_0030:
	{
		return;
	}
}
// System.Void UnityEngine.Application::SetLogCallbackDefined(System.Boolean,System.Boolean)
extern "C" void Application_SetLogCallbackDefined_m6_594 (Object_t * __this /* static, unused */, bool ___defined, bool ___threaded, const MethodInfo* method)
{
	typedef void (*Application_SetLogCallbackDefined_m6_594_ftn) (bool, bool);
	static Application_SetLogCallbackDefined_m6_594_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_SetLogCallbackDefined_m6_594_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::SetLogCallbackDefined(System.Boolean,System.Boolean)");
	_il2cpp_icall_func(___defined, ___threaded);
}
// System.Void UnityEngine.Behaviour::.ctor()
extern "C" void Behaviour__ctor_m6_595 (Behaviour_t6_30 * __this, const MethodInfo* method)
{
	{
		Component__ctor_m6_723(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Behaviour::get_enabled()
extern "C" bool Behaviour_get_enabled_m6_596 (Behaviour_t6_30 * __this, const MethodInfo* method)
{
	typedef bool (*Behaviour_get_enabled_m6_596_ftn) (Behaviour_t6_30 *);
	static Behaviour_get_enabled_m6_596_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Behaviour_get_enabled_m6_596_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Behaviour::get_enabled()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
extern "C" void Behaviour_set_enabled_m6_597 (Behaviour_t6_30 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*Behaviour_set_enabled_m6_597_ftn) (Behaviour_t6_30 *, bool);
	static Behaviour_set_enabled_m6_597_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Behaviour_set_enabled_m6_597_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Behaviour::set_enabled(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Boolean UnityEngine.Behaviour::get_isActiveAndEnabled()
extern "C" bool Behaviour_get_isActiveAndEnabled_m6_598 (Behaviour_t6_30 * __this, const MethodInfo* method)
{
	typedef bool (*Behaviour_get_isActiveAndEnabled_m6_598_ftn) (Behaviour_t6_30 *);
	static Behaviour_get_isActiveAndEnabled_m6_598_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Behaviour_get_isActiveAndEnabled_m6_598_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Behaviour::get_isActiveAndEnabled()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Camera/CameraCallback::.ctor(System.Object,System.IntPtr)
extern "C" void CameraCallback__ctor_m6_599 (CameraCallback_t6_85 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.Camera/CameraCallback::Invoke(UnityEngine.Camera)
extern "C" void CameraCallback_Invoke_m6_600 (CameraCallback_t6_85 * __this, Camera_t6_86 * ___cam, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		CameraCallback_Invoke_m6_600((CameraCallback_t6_85 *)__this->___prev_9,___cam, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, Camera_t6_86 * ___cam, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___cam,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, Camera_t6_86 * ___cam, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___cam,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(___cam,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_CameraCallback_t6_85(Il2CppObject* delegate, Camera_t6_86 * ___cam)
{
	// Marshaling of parameter '___cam' to native representation
	Camera_t6_86 * ____cam_marshaled = { 0 };
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'UnityEngine.Camera'."));
}
// System.IAsyncResult UnityEngine.Camera/CameraCallback::BeginInvoke(UnityEngine.Camera,System.AsyncCallback,System.Object)
extern "C" Object_t * CameraCallback_BeginInvoke_m6_601 (CameraCallback_t6_85 * __this, Camera_t6_86 * ___cam, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___cam;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.Camera/CameraCallback::EndInvoke(System.IAsyncResult)
extern "C" void CameraCallback_EndInvoke_m6_602 (CameraCallback_t6_85 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Single UnityEngine.Camera::get_nearClipPlane()
extern "C" float Camera_get_nearClipPlane_m6_603 (Camera_t6_86 * __this, const MethodInfo* method)
{
	typedef float (*Camera_get_nearClipPlane_m6_603_ftn) (Camera_t6_86 *);
	static Camera_get_nearClipPlane_m6_603_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_nearClipPlane_m6_603_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_nearClipPlane()");
	return _il2cpp_icall_func(__this);
}
// System.Single UnityEngine.Camera::get_farClipPlane()
extern "C" float Camera_get_farClipPlane_m6_604 (Camera_t6_86 * __this, const MethodInfo* method)
{
	typedef float (*Camera_get_farClipPlane_m6_604_ftn) (Camera_t6_86 *);
	static Camera_get_farClipPlane_m6_604_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_farClipPlane_m6_604_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_farClipPlane()");
	return _il2cpp_icall_func(__this);
}
// System.Single UnityEngine.Camera::get_depth()
extern "C" float Camera_get_depth_m6_605 (Camera_t6_86 * __this, const MethodInfo* method)
{
	typedef float (*Camera_get_depth_m6_605_ftn) (Camera_t6_86 *);
	static Camera_get_depth_m6_605_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_depth_m6_605_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_depth()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Camera::get_cullingMask()
extern "C" int32_t Camera_get_cullingMask_m6_606 (Camera_t6_86 * __this, const MethodInfo* method)
{
	typedef int32_t (*Camera_get_cullingMask_m6_606_ftn) (Camera_t6_86 *);
	static Camera_get_cullingMask_m6_606_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_cullingMask_m6_606_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_cullingMask()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Camera::get_eventMask()
extern "C" int32_t Camera_get_eventMask_m6_607 (Camera_t6_86 * __this, const MethodInfo* method)
{
	typedef int32_t (*Camera_get_eventMask_m6_607_ftn) (Camera_t6_86 *);
	static Camera_get_eventMask_m6_607_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_eventMask_m6_607_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_eventMask()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Rect UnityEngine.Camera::get_pixelRect()
extern "C" Rect_t6_51  Camera_get_pixelRect_m6_608 (Camera_t6_86 * __this, const MethodInfo* method)
{
	Rect_t6_51  V_0 = {0};
	{
		Camera_INTERNAL_get_pixelRect_m6_609(__this, (&V_0), /*hidden argument*/NULL);
		Rect_t6_51  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_get_pixelRect(UnityEngine.Rect&)
extern "C" void Camera_INTERNAL_get_pixelRect_m6_609 (Camera_t6_86 * __this, Rect_t6_51 * ___value, const MethodInfo* method)
{
	typedef void (*Camera_INTERNAL_get_pixelRect_m6_609_ftn) (Camera_t6_86 *, Rect_t6_51 *);
	static Camera_INTERNAL_get_pixelRect_m6_609_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_get_pixelRect_m6_609_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_get_pixelRect(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.RenderTexture UnityEngine.Camera::get_targetTexture()
extern "C" RenderTexture_t6_34 * Camera_get_targetTexture_m6_610 (Camera_t6_86 * __this, const MethodInfo* method)
{
	typedef RenderTexture_t6_34 * (*Camera_get_targetTexture_m6_610_ftn) (Camera_t6_86 *);
	static Camera_get_targetTexture_m6_610_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_targetTexture_m6_610_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_targetTexture()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.CameraClearFlags UnityEngine.Camera::get_clearFlags()
extern "C" int32_t Camera_get_clearFlags_m6_611 (Camera_t6_86 * __this, const MethodInfo* method)
{
	typedef int32_t (*Camera_get_clearFlags_m6_611_ftn) (Camera_t6_86 *);
	static Camera_get_clearFlags_m6_611_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_clearFlags_m6_611_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_clearFlags()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Vector3 UnityEngine.Camera::ScreenToViewportPoint(UnityEngine.Vector3)
extern "C" Vector3_t6_48  Camera_ScreenToViewportPoint_m6_612 (Camera_t6_86 * __this, Vector3_t6_48  ___position, const MethodInfo* method)
{
	{
		Vector3_t6_48  L_0 = Camera_INTERNAL_CALL_ScreenToViewportPoint_m6_613(NULL /*static, unused*/, __this, (&___position), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Camera::INTERNAL_CALL_ScreenToViewportPoint(UnityEngine.Camera,UnityEngine.Vector3&)
extern "C" Vector3_t6_48  Camera_INTERNAL_CALL_ScreenToViewportPoint_m6_613 (Object_t * __this /* static, unused */, Camera_t6_86 * ___self, Vector3_t6_48 * ___position, const MethodInfo* method)
{
	typedef Vector3_t6_48  (*Camera_INTERNAL_CALL_ScreenToViewportPoint_m6_613_ftn) (Camera_t6_86 *, Vector3_t6_48 *);
	static Camera_INTERNAL_CALL_ScreenToViewportPoint_m6_613_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_ScreenToViewportPoint_m6_613_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_ScreenToViewportPoint(UnityEngine.Camera,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___self, ___position);
}
// UnityEngine.Ray UnityEngine.Camera::ScreenPointToRay(UnityEngine.Vector3)
extern "C" Ray_t6_55  Camera_ScreenPointToRay_m6_614 (Camera_t6_86 * __this, Vector3_t6_48  ___position, const MethodInfo* method)
{
	{
		Ray_t6_55  L_0 = Camera_INTERNAL_CALL_ScreenPointToRay_m6_615(NULL /*static, unused*/, __this, (&___position), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Ray UnityEngine.Camera::INTERNAL_CALL_ScreenPointToRay(UnityEngine.Camera,UnityEngine.Vector3&)
extern "C" Ray_t6_55  Camera_INTERNAL_CALL_ScreenPointToRay_m6_615 (Object_t * __this /* static, unused */, Camera_t6_86 * ___self, Vector3_t6_48 * ___position, const MethodInfo* method)
{
	typedef Ray_t6_55  (*Camera_INTERNAL_CALL_ScreenPointToRay_m6_615_ftn) (Camera_t6_86 *, Vector3_t6_48 *);
	static Camera_INTERNAL_CALL_ScreenPointToRay_m6_615_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_ScreenPointToRay_m6_615_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_ScreenPointToRay(UnityEngine.Camera,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___self, ___position);
}
// UnityEngine.Camera UnityEngine.Camera::get_main()
extern "C" Camera_t6_86 * Camera_get_main_m6_616 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef Camera_t6_86 * (*Camera_get_main_m6_616_ftn) ();
	static Camera_get_main_m6_616_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_main_m6_616_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_main()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.Camera::get_allCamerasCount()
extern "C" int32_t Camera_get_allCamerasCount_m6_617 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Camera_get_allCamerasCount_m6_617_ftn) ();
	static Camera_get_allCamerasCount_m6_617_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_allCamerasCount_m6_617_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_allCamerasCount()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.Camera::GetAllCameras(UnityEngine.Camera[])
extern "C" int32_t Camera_GetAllCameras_m6_618 (Object_t * __this /* static, unused */, CameraU5BU5D_t6_232* ___cameras, const MethodInfo* method)
{
	typedef int32_t (*Camera_GetAllCameras_m6_618_ftn) (CameraU5BU5D_t6_232*);
	static Camera_GetAllCameras_m6_618_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_GetAllCameras_m6_618_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::GetAllCameras(UnityEngine.Camera[])");
	return _il2cpp_icall_func(___cameras);
}
// System.Void UnityEngine.Camera::FireOnPreCull(UnityEngine.Camera)
extern TypeInfo* Camera_t6_86_il2cpp_TypeInfo_var;
extern "C" void Camera_FireOnPreCull_m6_619 (Object_t * __this /* static, unused */, Camera_t6_86 * ___cam, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Camera_t6_86_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1609);
		s_Il2CppMethodIntialized = true;
	}
	{
		CameraCallback_t6_85 * L_0 = ((Camera_t6_86_StaticFields*)Camera_t6_86_il2cpp_TypeInfo_var->static_fields)->___onPreCull_2;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		CameraCallback_t6_85 * L_1 = ((Camera_t6_86_StaticFields*)Camera_t6_86_il2cpp_TypeInfo_var->static_fields)->___onPreCull_2;
		Camera_t6_86 * L_2 = ___cam;
		NullCheck(L_1);
		CameraCallback_Invoke_m6_600(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.Camera::FireOnPreRender(UnityEngine.Camera)
extern TypeInfo* Camera_t6_86_il2cpp_TypeInfo_var;
extern "C" void Camera_FireOnPreRender_m6_620 (Object_t * __this /* static, unused */, Camera_t6_86 * ___cam, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Camera_t6_86_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1609);
		s_Il2CppMethodIntialized = true;
	}
	{
		CameraCallback_t6_85 * L_0 = ((Camera_t6_86_StaticFields*)Camera_t6_86_il2cpp_TypeInfo_var->static_fields)->___onPreRender_3;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		CameraCallback_t6_85 * L_1 = ((Camera_t6_86_StaticFields*)Camera_t6_86_il2cpp_TypeInfo_var->static_fields)->___onPreRender_3;
		Camera_t6_86 * L_2 = ___cam;
		NullCheck(L_1);
		CameraCallback_Invoke_m6_600(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.Camera::FireOnPostRender(UnityEngine.Camera)
extern TypeInfo* Camera_t6_86_il2cpp_TypeInfo_var;
extern "C" void Camera_FireOnPostRender_m6_621 (Object_t * __this /* static, unused */, Camera_t6_86 * ___cam, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Camera_t6_86_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1609);
		s_Il2CppMethodIntialized = true;
	}
	{
		CameraCallback_t6_85 * L_0 = ((Camera_t6_86_StaticFields*)Camera_t6_86_il2cpp_TypeInfo_var->static_fields)->___onPostRender_4;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		CameraCallback_t6_85 * L_1 = ((Camera_t6_86_StaticFields*)Camera_t6_86_il2cpp_TypeInfo_var->static_fields)->___onPostRender_4;
		Camera_t6_86 * L_2 = ___cam;
		NullCheck(L_1);
		CameraCallback_Invoke_m6_600(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0015:
	{
		return;
	}
}
// UnityEngine.GameObject UnityEngine.Camera::RaycastTry(UnityEngine.Ray,System.Single,System.Int32)
extern "C" GameObject_t6_97 * Camera_RaycastTry_m6_622 (Camera_t6_86 * __this, Ray_t6_55  ___ray, float ___distance, int32_t ___layerMask, const MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		V_0 = 0;
		float L_0 = ___distance;
		int32_t L_1 = ___layerMask;
		int32_t L_2 = V_0;
		GameObject_t6_97 * L_3 = Camera_INTERNAL_CALL_RaycastTry_m6_623(NULL /*static, unused*/, __this, (&___ray), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.GameObject UnityEngine.Camera::INTERNAL_CALL_RaycastTry(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C" GameObject_t6_97 * Camera_INTERNAL_CALL_RaycastTry_m6_623 (Object_t * __this /* static, unused */, Camera_t6_86 * ___self, Ray_t6_55 * ___ray, float ___distance, int32_t ___layerMask, int32_t ___queryTriggerInteraction, const MethodInfo* method)
{
	typedef GameObject_t6_97 * (*Camera_INTERNAL_CALL_RaycastTry_m6_623_ftn) (Camera_t6_86 *, Ray_t6_55 *, float, int32_t, int32_t);
	static Camera_INTERNAL_CALL_RaycastTry_m6_623_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_RaycastTry_m6_623_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_RaycastTry(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)");
	return _il2cpp_icall_func(___self, ___ray, ___distance, ___layerMask, ___queryTriggerInteraction);
}
// UnityEngine.GameObject UnityEngine.Camera::RaycastTry2D(UnityEngine.Ray,System.Single,System.Int32)
extern "C" GameObject_t6_97 * Camera_RaycastTry2D_m6_624 (Camera_t6_86 * __this, Ray_t6_55  ___ray, float ___distance, int32_t ___layerMask, const MethodInfo* method)
{
	{
		float L_0 = ___distance;
		int32_t L_1 = ___layerMask;
		GameObject_t6_97 * L_2 = Camera_INTERNAL_CALL_RaycastTry2D_m6_625(NULL /*static, unused*/, __this, (&___ray), L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.GameObject UnityEngine.Camera::INTERNAL_CALL_RaycastTry2D(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)
extern "C" GameObject_t6_97 * Camera_INTERNAL_CALL_RaycastTry2D_m6_625 (Object_t * __this /* static, unused */, Camera_t6_86 * ___self, Ray_t6_55 * ___ray, float ___distance, int32_t ___layerMask, const MethodInfo* method)
{
	typedef GameObject_t6_97 * (*Camera_INTERNAL_CALL_RaycastTry2D_m6_625_ftn) (Camera_t6_86 *, Ray_t6_55 *, float, int32_t);
	static Camera_INTERNAL_CALL_RaycastTry2D_m6_625_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_RaycastTry2D_m6_625_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_RaycastTry2D(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)");
	return _il2cpp_icall_func(___self, ___ray, ___distance, ___layerMask);
}
// System.Void UnityEngine.Debug::DrawLine(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Color,System.Single,System.Boolean)
extern "C" void Debug_DrawLine_m6_626 (Object_t * __this /* static, unused */, Vector3_t6_48  ___start, Vector3_t6_48  ___end, Color_t6_40  ___color, float ___duration, bool ___depthTest, const MethodInfo* method)
{
	{
		float L_0 = ___duration;
		bool L_1 = ___depthTest;
		Debug_INTERNAL_CALL_DrawLine_m6_627(NULL /*static, unused*/, (&___start), (&___end), (&___color), L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Debug::INTERNAL_CALL_DrawLine(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Color&,System.Single,System.Boolean)
extern "C" void Debug_INTERNAL_CALL_DrawLine_m6_627 (Object_t * __this /* static, unused */, Vector3_t6_48 * ___start, Vector3_t6_48 * ___end, Color_t6_40 * ___color, float ___duration, bool ___depthTest, const MethodInfo* method)
{
	typedef void (*Debug_INTERNAL_CALL_DrawLine_m6_627_ftn) (Vector3_t6_48 *, Vector3_t6_48 *, Color_t6_40 *, float, bool);
	static Debug_INTERNAL_CALL_DrawLine_m6_627_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Debug_INTERNAL_CALL_DrawLine_m6_627_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Debug::INTERNAL_CALL_DrawLine(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Color&,System.Single,System.Boolean)");
	_il2cpp_icall_func(___start, ___end, ___color, ___duration, ___depthTest);
}
// System.Void UnityEngine.Debug::DrawRay(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Color,System.Single,System.Boolean)
extern "C" void Debug_DrawRay_m6_628 (Object_t * __this /* static, unused */, Vector3_t6_48  ___start, Vector3_t6_48  ___dir, Color_t6_40  ___color, float ___duration, bool ___depthTest, const MethodInfo* method)
{
	{
		Vector3_t6_48  L_0 = ___start;
		Vector3_t6_48  L_1 = ___start;
		Vector3_t6_48  L_2 = ___dir;
		Vector3_t6_48  L_3 = Vector3_op_Addition_m6_263(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Color_t6_40  L_4 = ___color;
		float L_5 = ___duration;
		bool L_6 = ___depthTest;
		Debug_DrawLine_m6_626(NULL /*static, unused*/, L_0, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Debug::Internal_Log(System.Int32,System.String,UnityEngine.Object)
extern "C" void Debug_Internal_Log_m6_629 (Object_t * __this /* static, unused */, int32_t ___level, String_t* ___msg, Object_t6_5 * ___obj, const MethodInfo* method)
{
	typedef void (*Debug_Internal_Log_m6_629_ftn) (int32_t, String_t*, Object_t6_5 *);
	static Debug_Internal_Log_m6_629_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Debug_Internal_Log_m6_629_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Debug::Internal_Log(System.Int32,System.String,UnityEngine.Object)");
	_il2cpp_icall_func(___level, ___msg, ___obj);
}
// System.Void UnityEngine.Debug::Internal_LogException(System.Exception,UnityEngine.Object)
extern "C" void Debug_Internal_LogException_m6_630 (Object_t * __this /* static, unused */, Exception_t1_33 * ___exception, Object_t6_5 * ___obj, const MethodInfo* method)
{
	typedef void (*Debug_Internal_LogException_m6_630_ftn) (Exception_t1_33 *, Object_t6_5 *);
	static Debug_Internal_LogException_m6_630_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Debug_Internal_LogException_m6_630_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Debug::Internal_LogException(System.Exception,UnityEngine.Object)");
	_il2cpp_icall_func(___exception, ___obj);
}
// System.Void UnityEngine.Debug::Log(System.Object)
extern Il2CppCodeGenString* _stringLiteral4733;
extern "C" void Debug_Log_m6_631 (Object_t * __this /* static, unused */, Object_t * ___message, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral4733 = il2cpp_codegen_string_literal_from_index(4733);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B2_0 = 0;
	int32_t G_B1_0 = 0;
	String_t* G_B3_0 = {0};
	int32_t G_B3_1 = 0;
	{
		Object_t * L_0 = ___message;
		G_B1_0 = 0;
		if (!L_0)
		{
			G_B2_0 = 0;
			goto IL_0012;
		}
	}
	{
		Object_t * L_1 = ___message;
		NullCheck(L_1);
		String_t* L_2 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		goto IL_0017;
	}

IL_0012:
	{
		G_B3_0 = _stringLiteral4733;
		G_B3_1 = G_B2_0;
	}

IL_0017:
	{
		Debug_Internal_Log_m6_629(NULL /*static, unused*/, G_B3_1, G_B3_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Debug::LogError(System.Object)
extern Il2CppCodeGenString* _stringLiteral4733;
extern "C" void Debug_LogError_m6_632 (Object_t * __this /* static, unused */, Object_t * ___message, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral4733 = il2cpp_codegen_string_literal_from_index(4733);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B2_0 = 0;
	int32_t G_B1_0 = 0;
	String_t* G_B3_0 = {0};
	int32_t G_B3_1 = 0;
	{
		Object_t * L_0 = ___message;
		G_B1_0 = 2;
		if (!L_0)
		{
			G_B2_0 = 2;
			goto IL_0012;
		}
	}
	{
		Object_t * L_1 = ___message;
		NullCheck(L_1);
		String_t* L_2 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		goto IL_0017;
	}

IL_0012:
	{
		G_B3_0 = _stringLiteral4733;
		G_B3_1 = G_B2_0;
	}

IL_0017:
	{
		Debug_Internal_Log_m6_629(NULL /*static, unused*/, G_B3_1, G_B3_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Debug::LogError(System.Object,UnityEngine.Object)
extern "C" void Debug_LogError_m6_633 (Object_t * __this /* static, unused */, Object_t * ___message, Object_t6_5 * ___context, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___message;
		NullCheck(L_0);
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		Object_t6_5 * L_2 = ___context;
		Debug_Internal_Log_m6_629(NULL /*static, unused*/, 2, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Debug::LogException(System.Exception)
extern "C" void Debug_LogException_m6_634 (Object_t * __this /* static, unused */, Exception_t1_33 * ___exception, const MethodInfo* method)
{
	{
		Exception_t1_33 * L_0 = ___exception;
		Debug_Internal_LogException_m6_630(NULL /*static, unused*/, L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Debug::LogException(System.Exception,UnityEngine.Object)
extern "C" void Debug_LogException_m6_635 (Object_t * __this /* static, unused */, Exception_t1_33 * ___exception, Object_t6_5 * ___context, const MethodInfo* method)
{
	{
		Exception_t1_33 * L_0 = ___exception;
		Object_t6_5 * L_1 = ___context;
		Debug_Internal_LogException_m6_630(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Debug::LogWarning(System.Object)
extern "C" void Debug_LogWarning_m6_636 (Object_t * __this /* static, unused */, Object_t * ___message, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___message;
		NullCheck(L_0);
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		Debug_Internal_Log_m6_629(NULL /*static, unused*/, 1, L_1, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Debug::LogWarning(System.Object,UnityEngine.Object)
extern "C" void Debug_LogWarning_m6_637 (Object_t * __this /* static, unused */, Object_t * ___message, Object_t6_5 * ___context, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___message;
		NullCheck(L_0);
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		Object_t6_5 * L_2 = ___context;
		Debug_Internal_Log_m6_629(NULL /*static, unused*/, 1, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Debug::get_isDebugBuild()
extern "C" bool Debug_get_isDebugBuild_m6_638 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Debug_get_isDebugBuild_m6_638_ftn) ();
	static Debug_get_isDebugBuild_m6_638_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Debug_get_isDebugBuild_m6_638_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Debug::get_isDebugBuild()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Display/DisplaysUpdatedDelegate::.ctor(System.Object,System.IntPtr)
extern "C" void DisplaysUpdatedDelegate__ctor_m6_639 (DisplaysUpdatedDelegate_t6_88 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.Display/DisplaysUpdatedDelegate::Invoke()
extern "C" void DisplaysUpdatedDelegate_Invoke_m6_640 (DisplaysUpdatedDelegate_t6_88 * __this, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		DisplaysUpdatedDelegate_Invoke_m6_640((DisplaysUpdatedDelegate_t6_88 *)__this->___prev_9, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if ((__this->___m_target_2 != NULL || MethodHasParameters((MethodInfo*)(__this->___method_3.___m_value_0))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_DisplaysUpdatedDelegate_t6_88(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.IAsyncResult UnityEngine.Display/DisplaysUpdatedDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * DisplaysUpdatedDelegate_BeginInvoke_m6_641 (DisplaysUpdatedDelegate_t6_88 * __this, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.Display/DisplaysUpdatedDelegate::EndInvoke(System.IAsyncResult)
extern "C" void DisplaysUpdatedDelegate_EndInvoke_m6_642 (DisplaysUpdatedDelegate_t6_88 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void UnityEngine.Display::.ctor()
extern "C" void Display__ctor_m6_643 (Display_t6_89 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		IntPtr_t L_0 = {0};
		IntPtr__ctor_m1_827(&L_0, 0, /*hidden argument*/NULL);
		__this->___nativeDisplay_0 = L_0;
		return;
	}
}
// System.Void UnityEngine.Display::.ctor(System.IntPtr)
extern "C" void Display__ctor_m6_644 (Display_t6_89 * __this, IntPtr_t ___nativeDisplay, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		IntPtr_t L_0 = ___nativeDisplay;
		__this->___nativeDisplay_0 = L_0;
		return;
	}
}
// System.Void UnityEngine.Display::.cctor()
extern TypeInfo* DisplayU5BU5D_t6_90_il2cpp_TypeInfo_var;
extern TypeInfo* Display_t6_89_il2cpp_TypeInfo_var;
extern "C" void Display__cctor_m6_645 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DisplayU5BU5D_t6_90_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1610);
		Display_t6_89_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1611);
		s_Il2CppMethodIntialized = true;
	}
	{
		DisplayU5BU5D_t6_90* L_0 = ((DisplayU5BU5D_t6_90*)SZArrayNew(DisplayU5BU5D_t6_90_il2cpp_TypeInfo_var, 1));
		Display_t6_89 * L_1 = (Display_t6_89 *)il2cpp_codegen_object_new (Display_t6_89_il2cpp_TypeInfo_var);
		Display__ctor_m6_643(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Display_t6_89 **)(Display_t6_89 **)SZArrayLdElema(L_0, 0, sizeof(Display_t6_89 *))) = (Display_t6_89 *)L_1;
		((Display_t6_89_StaticFields*)Display_t6_89_il2cpp_TypeInfo_var->static_fields)->___displays_1 = L_0;
		DisplayU5BU5D_t6_90* L_2 = ((Display_t6_89_StaticFields*)Display_t6_89_il2cpp_TypeInfo_var->static_fields)->___displays_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		int32_t L_3 = 0;
		((Display_t6_89_StaticFields*)Display_t6_89_il2cpp_TypeInfo_var->static_fields)->____mainDisplay_2 = (*(Display_t6_89 **)(Display_t6_89 **)SZArrayLdElema(L_2, L_3, sizeof(Display_t6_89 *)));
		((Display_t6_89_StaticFields*)Display_t6_89_il2cpp_TypeInfo_var->static_fields)->___onDisplaysUpdated_3 = (DisplaysUpdatedDelegate_t6_88 *)NULL;
		return;
	}
}
// System.Void UnityEngine.Display::add_onDisplaysUpdated(UnityEngine.Display/DisplaysUpdatedDelegate)
extern TypeInfo* Display_t6_89_il2cpp_TypeInfo_var;
extern TypeInfo* DisplaysUpdatedDelegate_t6_88_il2cpp_TypeInfo_var;
extern "C" void Display_add_onDisplaysUpdated_m6_646 (Object_t * __this /* static, unused */, DisplaysUpdatedDelegate_t6_88 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t6_89_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1611);
		DisplaysUpdatedDelegate_t6_88_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1612);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t6_89_il2cpp_TypeInfo_var);
		DisplaysUpdatedDelegate_t6_88 * L_0 = ((Display_t6_89_StaticFields*)Display_t6_89_il2cpp_TypeInfo_var->static_fields)->___onDisplaysUpdated_3;
		DisplaysUpdatedDelegate_t6_88 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Combine_m1_908(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((Display_t6_89_StaticFields*)Display_t6_89_il2cpp_TypeInfo_var->static_fields)->___onDisplaysUpdated_3 = ((DisplaysUpdatedDelegate_t6_88 *)CastclassSealed(L_2, DisplaysUpdatedDelegate_t6_88_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Display::remove_onDisplaysUpdated(UnityEngine.Display/DisplaysUpdatedDelegate)
extern TypeInfo* Display_t6_89_il2cpp_TypeInfo_var;
extern TypeInfo* DisplaysUpdatedDelegate_t6_88_il2cpp_TypeInfo_var;
extern "C" void Display_remove_onDisplaysUpdated_m6_647 (Object_t * __this /* static, unused */, DisplaysUpdatedDelegate_t6_88 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t6_89_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1611);
		DisplaysUpdatedDelegate_t6_88_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1612);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t6_89_il2cpp_TypeInfo_var);
		DisplaysUpdatedDelegate_t6_88 * L_0 = ((Display_t6_89_StaticFields*)Display_t6_89_il2cpp_TypeInfo_var->static_fields)->___onDisplaysUpdated_3;
		DisplaysUpdatedDelegate_t6_88 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Remove_m1_911(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((Display_t6_89_StaticFields*)Display_t6_89_il2cpp_TypeInfo_var->static_fields)->___onDisplaysUpdated_3 = ((DisplaysUpdatedDelegate_t6_88 *)CastclassSealed(L_2, DisplaysUpdatedDelegate_t6_88_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Int32 UnityEngine.Display::get_renderingWidth()
extern TypeInfo* Display_t6_89_il2cpp_TypeInfo_var;
extern "C" int32_t Display_get_renderingWidth_m6_648 (Display_t6_89 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t6_89_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1611);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		V_1 = 0;
		IntPtr_t L_0 = (__this->___nativeDisplay_0);
		IL2CPP_RUNTIME_CLASS_INIT(Display_t6_89_il2cpp_TypeInfo_var);
		Display_GetRenderingExtImpl_m6_664(NULL /*static, unused*/, L_0, (&V_0), (&V_1), /*hidden argument*/NULL);
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Int32 UnityEngine.Display::get_renderingHeight()
extern TypeInfo* Display_t6_89_il2cpp_TypeInfo_var;
extern "C" int32_t Display_get_renderingHeight_m6_649 (Display_t6_89 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t6_89_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1611);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		V_1 = 0;
		IntPtr_t L_0 = (__this->___nativeDisplay_0);
		IL2CPP_RUNTIME_CLASS_INIT(Display_t6_89_il2cpp_TypeInfo_var);
		Display_GetRenderingExtImpl_m6_664(NULL /*static, unused*/, L_0, (&V_0), (&V_1), /*hidden argument*/NULL);
		int32_t L_1 = V_1;
		return L_1;
	}
}
// System.Int32 UnityEngine.Display::get_systemWidth()
extern TypeInfo* Display_t6_89_il2cpp_TypeInfo_var;
extern "C" int32_t Display_get_systemWidth_m6_650 (Display_t6_89 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t6_89_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1611);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		V_1 = 0;
		IntPtr_t L_0 = (__this->___nativeDisplay_0);
		IL2CPP_RUNTIME_CLASS_INIT(Display_t6_89_il2cpp_TypeInfo_var);
		Display_GetSystemExtImpl_m6_663(NULL /*static, unused*/, L_0, (&V_0), (&V_1), /*hidden argument*/NULL);
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Int32 UnityEngine.Display::get_systemHeight()
extern TypeInfo* Display_t6_89_il2cpp_TypeInfo_var;
extern "C" int32_t Display_get_systemHeight_m6_651 (Display_t6_89 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t6_89_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1611);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		V_1 = 0;
		IntPtr_t L_0 = (__this->___nativeDisplay_0);
		IL2CPP_RUNTIME_CLASS_INIT(Display_t6_89_il2cpp_TypeInfo_var);
		Display_GetSystemExtImpl_m6_663(NULL /*static, unused*/, L_0, (&V_0), (&V_1), /*hidden argument*/NULL);
		int32_t L_1 = V_1;
		return L_1;
	}
}
// UnityEngine.RenderBuffer UnityEngine.Display::get_colorBuffer()
extern TypeInfo* Display_t6_89_il2cpp_TypeInfo_var;
extern "C" RenderBuffer_t6_213  Display_get_colorBuffer_m6_652 (Display_t6_89 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t6_89_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1611);
		s_Il2CppMethodIntialized = true;
	}
	RenderBuffer_t6_213  V_0 = {0};
	RenderBuffer_t6_213  V_1 = {0};
	{
		IntPtr_t L_0 = (__this->___nativeDisplay_0);
		IL2CPP_RUNTIME_CLASS_INIT(Display_t6_89_il2cpp_TypeInfo_var);
		Display_GetRenderingBuffersImpl_m6_665(NULL /*static, unused*/, L_0, (&V_0), (&V_1), /*hidden argument*/NULL);
		RenderBuffer_t6_213  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.RenderBuffer UnityEngine.Display::get_depthBuffer()
extern TypeInfo* Display_t6_89_il2cpp_TypeInfo_var;
extern "C" RenderBuffer_t6_213  Display_get_depthBuffer_m6_653 (Display_t6_89 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t6_89_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1611);
		s_Il2CppMethodIntialized = true;
	}
	RenderBuffer_t6_213  V_0 = {0};
	RenderBuffer_t6_213  V_1 = {0};
	{
		IntPtr_t L_0 = (__this->___nativeDisplay_0);
		IL2CPP_RUNTIME_CLASS_INIT(Display_t6_89_il2cpp_TypeInfo_var);
		Display_GetRenderingBuffersImpl_m6_665(NULL /*static, unused*/, L_0, (&V_0), (&V_1), /*hidden argument*/NULL);
		RenderBuffer_t6_213  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Display::Activate()
extern TypeInfo* Display_t6_89_il2cpp_TypeInfo_var;
extern "C" void Display_Activate_m6_654 (Display_t6_89 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t6_89_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1611);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = (__this->___nativeDisplay_0);
		IL2CPP_RUNTIME_CLASS_INIT(Display_t6_89_il2cpp_TypeInfo_var);
		Display_ActivateDisplayImpl_m6_667(NULL /*static, unused*/, L_0, 0, 0, ((int32_t)60), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Display::Activate(System.Int32,System.Int32,System.Int32)
extern TypeInfo* Display_t6_89_il2cpp_TypeInfo_var;
extern "C" void Display_Activate_m6_655 (Display_t6_89 * __this, int32_t ___width, int32_t ___height, int32_t ___refreshRate, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t6_89_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1611);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = (__this->___nativeDisplay_0);
		int32_t L_1 = ___width;
		int32_t L_2 = ___height;
		int32_t L_3 = ___refreshRate;
		IL2CPP_RUNTIME_CLASS_INIT(Display_t6_89_il2cpp_TypeInfo_var);
		Display_ActivateDisplayImpl_m6_667(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Display::SetParams(System.Int32,System.Int32,System.Int32,System.Int32)
extern TypeInfo* Display_t6_89_il2cpp_TypeInfo_var;
extern "C" void Display_SetParams_m6_656 (Display_t6_89 * __this, int32_t ___width, int32_t ___height, int32_t ___x, int32_t ___y, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t6_89_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1611);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = (__this->___nativeDisplay_0);
		int32_t L_1 = ___width;
		int32_t L_2 = ___height;
		int32_t L_3 = ___x;
		int32_t L_4 = ___y;
		IL2CPP_RUNTIME_CLASS_INIT(Display_t6_89_il2cpp_TypeInfo_var);
		Display_SetParamsImpl_m6_668(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Display::SetRenderingResolution(System.Int32,System.Int32)
extern TypeInfo* Display_t6_89_il2cpp_TypeInfo_var;
extern "C" void Display_SetRenderingResolution_m6_657 (Display_t6_89 * __this, int32_t ___w, int32_t ___h, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t6_89_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1611);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = (__this->___nativeDisplay_0);
		int32_t L_1 = ___w;
		int32_t L_2 = ___h;
		IL2CPP_RUNTIME_CLASS_INIT(Display_t6_89_il2cpp_TypeInfo_var);
		Display_SetRenderingResolutionImpl_m6_666(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Display::MultiDisplayLicense()
extern TypeInfo* Display_t6_89_il2cpp_TypeInfo_var;
extern "C" bool Display_MultiDisplayLicense_m6_658 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t6_89_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1611);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t6_89_il2cpp_TypeInfo_var);
		bool L_0 = Display_MultiDisplayLicenseImpl_m6_669(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Display::RelativeMouseAt(UnityEngine.Vector3)
extern TypeInfo* Display_t6_89_il2cpp_TypeInfo_var;
extern "C" Vector3_t6_48  Display_RelativeMouseAt_m6_659 (Object_t * __this /* static, unused */, Vector3_t6_48  ___inputMouseCoordinates, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t6_89_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1611);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t6_48  V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		V_1 = 0;
		V_2 = 0;
		float L_0 = ((&___inputMouseCoordinates)->___x_1);
		V_3 = (((int32_t)((int32_t)L_0)));
		float L_1 = ((&___inputMouseCoordinates)->___y_2);
		V_4 = (((int32_t)((int32_t)L_1)));
		int32_t L_2 = V_3;
		int32_t L_3 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Display_t6_89_il2cpp_TypeInfo_var);
		int32_t L_4 = Display_RelativeMouseAtImpl_m6_670(NULL /*static, unused*/, L_2, L_3, (&V_1), (&V_2), /*hidden argument*/NULL);
		(&V_0)->___z_3 = (((float)((float)L_4)));
		int32_t L_5 = V_1;
		(&V_0)->___x_1 = (((float)((float)L_5)));
		int32_t L_6 = V_2;
		(&V_0)->___y_2 = (((float)((float)L_6)));
		Vector3_t6_48  L_7 = V_0;
		return L_7;
	}
}
// UnityEngine.Display UnityEngine.Display::get_main()
extern TypeInfo* Display_t6_89_il2cpp_TypeInfo_var;
extern "C" Display_t6_89 * Display_get_main_m6_660 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t6_89_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1611);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t6_89_il2cpp_TypeInfo_var);
		Display_t6_89 * L_0 = ((Display_t6_89_StaticFields*)Display_t6_89_il2cpp_TypeInfo_var->static_fields)->____mainDisplay_2;
		return L_0;
	}
}
// System.Void UnityEngine.Display::RecreateDisplayList(System.IntPtr[])
extern TypeInfo* DisplayU5BU5D_t6_90_il2cpp_TypeInfo_var;
extern TypeInfo* Display_t6_89_il2cpp_TypeInfo_var;
extern "C" void Display_RecreateDisplayList_m6_661 (Object_t * __this /* static, unused */, IntPtrU5BU5D_t1_34* ___nativeDisplay, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DisplayU5BU5D_t6_90_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1610);
		Display_t6_89_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1611);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		IntPtrU5BU5D_t1_34* L_0 = ___nativeDisplay;
		NullCheck(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Display_t6_89_il2cpp_TypeInfo_var);
		((Display_t6_89_StaticFields*)Display_t6_89_il2cpp_TypeInfo_var->static_fields)->___displays_1 = ((DisplayU5BU5D_t6_90*)SZArrayNew(DisplayU5BU5D_t6_90_il2cpp_TypeInfo_var, (((int32_t)((int32_t)(((Array_t *)L_0)->max_length))))));
		V_0 = 0;
		goto IL_0027;
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t6_89_il2cpp_TypeInfo_var);
		DisplayU5BU5D_t6_90* L_1 = ((Display_t6_89_StaticFields*)Display_t6_89_il2cpp_TypeInfo_var->static_fields)->___displays_1;
		int32_t L_2 = V_0;
		IntPtrU5BU5D_t1_34* L_3 = ___nativeDisplay;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		Display_t6_89 * L_6 = (Display_t6_89 *)il2cpp_codegen_object_new (Display_t6_89_il2cpp_TypeInfo_var);
		Display__ctor_m6_644(L_6, (*(IntPtr_t*)(IntPtr_t*)SZArrayLdElema(L_3, L_5, sizeof(IntPtr_t))), /*hidden argument*/NULL);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		ArrayElementTypeCheck (L_1, L_6);
		*((Display_t6_89 **)(Display_t6_89 **)SZArrayLdElema(L_1, L_2, sizeof(Display_t6_89 *))) = (Display_t6_89 *)L_6;
		int32_t L_7 = V_0;
		V_0 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0027:
	{
		int32_t L_8 = V_0;
		IntPtrU5BU5D_t1_34* L_9 = ___nativeDisplay;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_9)->max_length)))))))
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t6_89_il2cpp_TypeInfo_var);
		DisplayU5BU5D_t6_90* L_10 = ((Display_t6_89_StaticFields*)Display_t6_89_il2cpp_TypeInfo_var->static_fields)->___displays_1;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		int32_t L_11 = 0;
		((Display_t6_89_StaticFields*)Display_t6_89_il2cpp_TypeInfo_var->static_fields)->____mainDisplay_2 = (*(Display_t6_89 **)(Display_t6_89 **)SZArrayLdElema(L_10, L_11, sizeof(Display_t6_89 *)));
		return;
	}
}
// System.Void UnityEngine.Display::FireDisplaysUpdated()
extern TypeInfo* Display_t6_89_il2cpp_TypeInfo_var;
extern "C" void Display_FireDisplaysUpdated_m6_662 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t6_89_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1611);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t6_89_il2cpp_TypeInfo_var);
		DisplaysUpdatedDelegate_t6_88 * L_0 = ((Display_t6_89_StaticFields*)Display_t6_89_il2cpp_TypeInfo_var->static_fields)->___onDisplaysUpdated_3;
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t6_89_il2cpp_TypeInfo_var);
		DisplaysUpdatedDelegate_t6_88 * L_1 = ((Display_t6_89_StaticFields*)Display_t6_89_il2cpp_TypeInfo_var->static_fields)->___onDisplaysUpdated_3;
		NullCheck(L_1);
		DisplaysUpdatedDelegate_Invoke_m6_640(L_1, /*hidden argument*/NULL);
	}

IL_0014:
	{
		return;
	}
}
// System.Void UnityEngine.Display::GetSystemExtImpl(System.IntPtr,System.Int32&,System.Int32&)
extern "C" void Display_GetSystemExtImpl_m6_663 (Object_t * __this /* static, unused */, IntPtr_t ___nativeDisplay, int32_t* ___w, int32_t* ___h, const MethodInfo* method)
{
	typedef void (*Display_GetSystemExtImpl_m6_663_ftn) (IntPtr_t, int32_t*, int32_t*);
	static Display_GetSystemExtImpl_m6_663_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Display_GetSystemExtImpl_m6_663_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Display::GetSystemExtImpl(System.IntPtr,System.Int32&,System.Int32&)");
	_il2cpp_icall_func(___nativeDisplay, ___w, ___h);
}
// System.Void UnityEngine.Display::GetRenderingExtImpl(System.IntPtr,System.Int32&,System.Int32&)
extern "C" void Display_GetRenderingExtImpl_m6_664 (Object_t * __this /* static, unused */, IntPtr_t ___nativeDisplay, int32_t* ___w, int32_t* ___h, const MethodInfo* method)
{
	typedef void (*Display_GetRenderingExtImpl_m6_664_ftn) (IntPtr_t, int32_t*, int32_t*);
	static Display_GetRenderingExtImpl_m6_664_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Display_GetRenderingExtImpl_m6_664_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Display::GetRenderingExtImpl(System.IntPtr,System.Int32&,System.Int32&)");
	_il2cpp_icall_func(___nativeDisplay, ___w, ___h);
}
// System.Void UnityEngine.Display::GetRenderingBuffersImpl(System.IntPtr,UnityEngine.RenderBuffer&,UnityEngine.RenderBuffer&)
extern "C" void Display_GetRenderingBuffersImpl_m6_665 (Object_t * __this /* static, unused */, IntPtr_t ___nativeDisplay, RenderBuffer_t6_213 * ___color, RenderBuffer_t6_213 * ___depth, const MethodInfo* method)
{
	typedef void (*Display_GetRenderingBuffersImpl_m6_665_ftn) (IntPtr_t, RenderBuffer_t6_213 *, RenderBuffer_t6_213 *);
	static Display_GetRenderingBuffersImpl_m6_665_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Display_GetRenderingBuffersImpl_m6_665_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Display::GetRenderingBuffersImpl(System.IntPtr,UnityEngine.RenderBuffer&,UnityEngine.RenderBuffer&)");
	_il2cpp_icall_func(___nativeDisplay, ___color, ___depth);
}
// System.Void UnityEngine.Display::SetRenderingResolutionImpl(System.IntPtr,System.Int32,System.Int32)
extern "C" void Display_SetRenderingResolutionImpl_m6_666 (Object_t * __this /* static, unused */, IntPtr_t ___nativeDisplay, int32_t ___w, int32_t ___h, const MethodInfo* method)
{
	typedef void (*Display_SetRenderingResolutionImpl_m6_666_ftn) (IntPtr_t, int32_t, int32_t);
	static Display_SetRenderingResolutionImpl_m6_666_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Display_SetRenderingResolutionImpl_m6_666_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Display::SetRenderingResolutionImpl(System.IntPtr,System.Int32,System.Int32)");
	_il2cpp_icall_func(___nativeDisplay, ___w, ___h);
}
// System.Void UnityEngine.Display::ActivateDisplayImpl(System.IntPtr,System.Int32,System.Int32,System.Int32)
extern "C" void Display_ActivateDisplayImpl_m6_667 (Object_t * __this /* static, unused */, IntPtr_t ___nativeDisplay, int32_t ___width, int32_t ___height, int32_t ___refreshRate, const MethodInfo* method)
{
	typedef void (*Display_ActivateDisplayImpl_m6_667_ftn) (IntPtr_t, int32_t, int32_t, int32_t);
	static Display_ActivateDisplayImpl_m6_667_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Display_ActivateDisplayImpl_m6_667_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Display::ActivateDisplayImpl(System.IntPtr,System.Int32,System.Int32,System.Int32)");
	_il2cpp_icall_func(___nativeDisplay, ___width, ___height, ___refreshRate);
}
// System.Void UnityEngine.Display::SetParamsImpl(System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" void Display_SetParamsImpl_m6_668 (Object_t * __this /* static, unused */, IntPtr_t ___nativeDisplay, int32_t ___width, int32_t ___height, int32_t ___x, int32_t ___y, const MethodInfo* method)
{
	typedef void (*Display_SetParamsImpl_m6_668_ftn) (IntPtr_t, int32_t, int32_t, int32_t, int32_t);
	static Display_SetParamsImpl_m6_668_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Display_SetParamsImpl_m6_668_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Display::SetParamsImpl(System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32)");
	_il2cpp_icall_func(___nativeDisplay, ___width, ___height, ___x, ___y);
}
// System.Boolean UnityEngine.Display::MultiDisplayLicenseImpl()
extern "C" bool Display_MultiDisplayLicenseImpl_m6_669 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Display_MultiDisplayLicenseImpl_m6_669_ftn) ();
	static Display_MultiDisplayLicenseImpl_m6_669_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Display_MultiDisplayLicenseImpl_m6_669_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Display::MultiDisplayLicenseImpl()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.Display::RelativeMouseAtImpl(System.Int32,System.Int32,System.Int32&,System.Int32&)
extern "C" int32_t Display_RelativeMouseAtImpl_m6_670 (Object_t * __this /* static, unused */, int32_t ___x, int32_t ___y, int32_t* ___rx, int32_t* ___ry, const MethodInfo* method)
{
	typedef int32_t (*Display_RelativeMouseAtImpl_m6_670_ftn) (int32_t, int32_t, int32_t*, int32_t*);
	static Display_RelativeMouseAtImpl_m6_670_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Display_RelativeMouseAtImpl_m6_670_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Display::RelativeMouseAtImpl(System.Int32,System.Int32,System.Int32&,System.Int32&)");
	return _il2cpp_icall_func(___x, ___y, ___rx, ___ry);
}
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C" void MonoBehaviour__ctor_m6_671 (MonoBehaviour_t6_91 * __this, const MethodInfo* method)
{
	{
		Behaviour__ctor_m6_595(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
extern "C" Coroutine_t6_14 * MonoBehaviour_StartCoroutine_m6_672 (MonoBehaviour_t6_91 * __this, Object_t * ___routine, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___routine;
		Coroutine_t6_14 * L_1 = MonoBehaviour_StartCoroutine_Auto_m6_673(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine_Auto(System.Collections.IEnumerator)
extern "C" Coroutine_t6_14 * MonoBehaviour_StartCoroutine_Auto_m6_673 (MonoBehaviour_t6_91 * __this, Object_t * ___routine, const MethodInfo* method)
{
	typedef Coroutine_t6_14 * (*MonoBehaviour_StartCoroutine_Auto_m6_673_ftn) (MonoBehaviour_t6_91 *, Object_t *);
	static MonoBehaviour_StartCoroutine_Auto_m6_673_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StartCoroutine_Auto_m6_673_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StartCoroutine_Auto(System.Collections.IEnumerator)");
	return _il2cpp_icall_func(__this, ___routine);
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutine(System.Collections.IEnumerator)
extern "C" void MonoBehaviour_StopCoroutine_m6_674 (MonoBehaviour_t6_91 * __this, Object_t * ___routine, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___routine;
		MonoBehaviour_StopCoroutineViaEnumerator_Auto_m6_676(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutine(UnityEngine.Coroutine)
extern "C" void MonoBehaviour_StopCoroutine_m6_675 (MonoBehaviour_t6_91 * __this, Coroutine_t6_14 * ___routine, const MethodInfo* method)
{
	{
		Coroutine_t6_14 * L_0 = ___routine;
		MonoBehaviour_StopCoroutine_Auto_m6_677(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutineViaEnumerator_Auto(System.Collections.IEnumerator)
extern "C" void MonoBehaviour_StopCoroutineViaEnumerator_Auto_m6_676 (MonoBehaviour_t6_91 * __this, Object_t * ___routine, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_StopCoroutineViaEnumerator_Auto_m6_676_ftn) (MonoBehaviour_t6_91 *, Object_t *);
	static MonoBehaviour_StopCoroutineViaEnumerator_Auto_m6_676_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StopCoroutineViaEnumerator_Auto_m6_676_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StopCoroutineViaEnumerator_Auto(System.Collections.IEnumerator)");
	_il2cpp_icall_func(__this, ___routine);
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutine_Auto(UnityEngine.Coroutine)
extern "C" void MonoBehaviour_StopCoroutine_Auto_m6_677 (MonoBehaviour_t6_91 * __this, Coroutine_t6_14 * ___routine, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_StopCoroutine_Auto_m6_677_ftn) (MonoBehaviour_t6_91 *, Coroutine_t6_14 *);
	static MonoBehaviour_StopCoroutine_Auto_m6_677_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StopCoroutine_Auto_m6_677_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StopCoroutine_Auto(UnityEngine.Coroutine)");
	_il2cpp_icall_func(__this, ___routine);
}
// System.Int32 UnityEngine.Touch::get_fingerId()
extern "C" int32_t Touch_get_fingerId_m6_678 (Touch_t6_94 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_FingerId_0);
		return L_0;
	}
}
// UnityEngine.Vector2 UnityEngine.Touch::get_position()
extern "C" Vector2_t6_47  Touch_get_position_m6_679 (Touch_t6_94 * __this, const MethodInfo* method)
{
	{
		Vector2_t6_47  L_0 = (__this->___m_Position_1);
		return L_0;
	}
}
// UnityEngine.Vector2 UnityEngine.Touch::get_deltaPosition()
extern "C" Vector2_t6_47  Touch_get_deltaPosition_m6_680 (Touch_t6_94 * __this, const MethodInfo* method)
{
	{
		Vector2_t6_47  L_0 = (__this->___m_PositionDelta_3);
		return L_0;
	}
}
// UnityEngine.TouchPhase UnityEngine.Touch::get_phase()
extern "C" int32_t Touch_get_phase_m6_681 (Touch_t6_94 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Phase_6);
		return L_0;
	}
}
// Conversion methods for marshalling of: UnityEngine.Touch
extern "C" void Touch_t6_94_marshal(const Touch_t6_94& unmarshaled, Touch_t6_94_marshaled& marshaled)
{
	marshaled.___m_FingerId_0 = unmarshaled.___m_FingerId_0;
	marshaled.___m_Position_1 = unmarshaled.___m_Position_1;
	marshaled.___m_RawPosition_2 = unmarshaled.___m_RawPosition_2;
	marshaled.___m_PositionDelta_3 = unmarshaled.___m_PositionDelta_3;
	marshaled.___m_TimeDelta_4 = unmarshaled.___m_TimeDelta_4;
	marshaled.___m_TapCount_5 = unmarshaled.___m_TapCount_5;
	marshaled.___m_Phase_6 = unmarshaled.___m_Phase_6;
	marshaled.___m_Pressure_7 = unmarshaled.___m_Pressure_7;
	marshaled.___m_maximumPossiblePressure_8 = unmarshaled.___m_maximumPossiblePressure_8;
}
extern "C" void Touch_t6_94_marshal_back(const Touch_t6_94_marshaled& marshaled, Touch_t6_94& unmarshaled)
{
	unmarshaled.___m_FingerId_0 = marshaled.___m_FingerId_0;
	unmarshaled.___m_Position_1 = marshaled.___m_Position_1;
	unmarshaled.___m_RawPosition_2 = marshaled.___m_RawPosition_2;
	unmarshaled.___m_PositionDelta_3 = marshaled.___m_PositionDelta_3;
	unmarshaled.___m_TimeDelta_4 = marshaled.___m_TimeDelta_4;
	unmarshaled.___m_TapCount_5 = marshaled.___m_TapCount_5;
	unmarshaled.___m_Phase_6 = marshaled.___m_Phase_6;
	unmarshaled.___m_Pressure_7 = marshaled.___m_Pressure_7;
	unmarshaled.___m_maximumPossiblePressure_8 = marshaled.___m_maximumPossiblePressure_8;
}
// Conversion method for clean up from marshalling of: UnityEngine.Touch
extern "C" void Touch_t6_94_marshal_cleanup(Touch_t6_94_marshaled& marshaled)
{
}
// System.Void UnityEngine.Input::.cctor()
extern "C" void Input__cctor_m6_682 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Single UnityEngine.Input::GetAxisRaw(System.String)
extern "C" float Input_GetAxisRaw_m6_683 (Object_t * __this /* static, unused */, String_t* ___axisName, const MethodInfo* method)
{
	typedef float (*Input_GetAxisRaw_m6_683_ftn) (String_t*);
	static Input_GetAxisRaw_m6_683_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetAxisRaw_m6_683_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetAxisRaw(System.String)");
	return _il2cpp_icall_func(___axisName);
}
// System.Boolean UnityEngine.Input::GetButtonDown(System.String)
extern "C" bool Input_GetButtonDown_m6_684 (Object_t * __this /* static, unused */, String_t* ___buttonName, const MethodInfo* method)
{
	typedef bool (*Input_GetButtonDown_m6_684_ftn) (String_t*);
	static Input_GetButtonDown_m6_684_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetButtonDown_m6_684_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetButtonDown(System.String)");
	return _il2cpp_icall_func(___buttonName);
}
// System.Boolean UnityEngine.Input::GetMouseButton(System.Int32)
extern "C" bool Input_GetMouseButton_m6_685 (Object_t * __this /* static, unused */, int32_t ___button, const MethodInfo* method)
{
	typedef bool (*Input_GetMouseButton_m6_685_ftn) (int32_t);
	static Input_GetMouseButton_m6_685_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetMouseButton_m6_685_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetMouseButton(System.Int32)");
	return _il2cpp_icall_func(___button);
}
// System.Boolean UnityEngine.Input::GetMouseButtonDown(System.Int32)
extern "C" bool Input_GetMouseButtonDown_m6_686 (Object_t * __this /* static, unused */, int32_t ___button, const MethodInfo* method)
{
	typedef bool (*Input_GetMouseButtonDown_m6_686_ftn) (int32_t);
	static Input_GetMouseButtonDown_m6_686_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetMouseButtonDown_m6_686_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetMouseButtonDown(System.Int32)");
	return _il2cpp_icall_func(___button);
}
// System.Boolean UnityEngine.Input::GetMouseButtonUp(System.Int32)
extern "C" bool Input_GetMouseButtonUp_m6_687 (Object_t * __this /* static, unused */, int32_t ___button, const MethodInfo* method)
{
	typedef bool (*Input_GetMouseButtonUp_m6_687_ftn) (int32_t);
	static Input_GetMouseButtonUp_m6_687_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetMouseButtonUp_m6_687_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetMouseButtonUp(System.Int32)");
	return _il2cpp_icall_func(___button);
}
// UnityEngine.Vector3 UnityEngine.Input::get_mousePosition()
extern TypeInfo* Input_t6_95_il2cpp_TypeInfo_var;
extern "C" Vector3_t6_48  Input_get_mousePosition_m6_688 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t6_95_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1613);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t6_48  V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t6_95_il2cpp_TypeInfo_var);
		Input_INTERNAL_get_mousePosition_m6_689(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Vector3_t6_48  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Input::INTERNAL_get_mousePosition(UnityEngine.Vector3&)
extern "C" void Input_INTERNAL_get_mousePosition_m6_689 (Object_t * __this /* static, unused */, Vector3_t6_48 * ___value, const MethodInfo* method)
{
	typedef void (*Input_INTERNAL_get_mousePosition_m6_689_ftn) (Vector3_t6_48 *);
	static Input_INTERNAL_get_mousePosition_m6_689_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_INTERNAL_get_mousePosition_m6_689_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::INTERNAL_get_mousePosition(UnityEngine.Vector3&)");
	_il2cpp_icall_func(___value);
}
// UnityEngine.Vector2 UnityEngine.Input::get_mouseScrollDelta()
extern TypeInfo* Input_t6_95_il2cpp_TypeInfo_var;
extern "C" Vector2_t6_47  Input_get_mouseScrollDelta_m6_690 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t6_95_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1613);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t6_47  V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t6_95_il2cpp_TypeInfo_var);
		Input_INTERNAL_get_mouseScrollDelta_m6_691(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Vector2_t6_47  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Input::INTERNAL_get_mouseScrollDelta(UnityEngine.Vector2&)
extern "C" void Input_INTERNAL_get_mouseScrollDelta_m6_691 (Object_t * __this /* static, unused */, Vector2_t6_47 * ___value, const MethodInfo* method)
{
	typedef void (*Input_INTERNAL_get_mouseScrollDelta_m6_691_ftn) (Vector2_t6_47 *);
	static Input_INTERNAL_get_mouseScrollDelta_m6_691_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_INTERNAL_get_mouseScrollDelta_m6_691_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::INTERNAL_get_mouseScrollDelta(UnityEngine.Vector2&)");
	_il2cpp_icall_func(___value);
}
// System.Boolean UnityEngine.Input::get_mousePresent()
extern "C" bool Input_get_mousePresent_m6_692 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Input_get_mousePresent_m6_692_ftn) ();
	static Input_get_mousePresent_m6_692_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_get_mousePresent_m6_692_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::get_mousePresent()");
	return _il2cpp_icall_func();
}
// UnityEngine.Touch[] UnityEngine.Input::get_touches()
extern TypeInfo* Input_t6_95_il2cpp_TypeInfo_var;
extern TypeInfo* TouchU5BU5D_t6_282_il2cpp_TypeInfo_var;
extern "C" TouchU5BU5D_t6_282* Input_get_touches_m6_693 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t6_95_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1613);
		TouchU5BU5D_t6_282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1614);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	TouchU5BU5D_t6_282* V_1 = {0};
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t6_95_il2cpp_TypeInfo_var);
		int32_t L_0 = Input_get_touchCount_m6_695(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		V_1 = ((TouchU5BU5D_t6_282*)SZArrayNew(TouchU5BU5D_t6_282_il2cpp_TypeInfo_var, L_1));
		V_2 = 0;
		goto IL_002a;
	}

IL_0014:
	{
		TouchU5BU5D_t6_282* L_2 = V_1;
		int32_t L_3 = V_2;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t6_95_il2cpp_TypeInfo_var);
		Touch_t6_94  L_5 = Input_GetTouch_m6_694(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		(*(Touch_t6_94 *)((Touch_t6_94 *)(Touch_t6_94 *)SZArrayLdElema(L_2, L_3, sizeof(Touch_t6_94 )))) = L_5;
		int32_t L_6 = V_2;
		V_2 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_002a:
	{
		int32_t L_7 = V_2;
		int32_t L_8 = V_0;
		if ((((int32_t)L_7) < ((int32_t)L_8)))
		{
			goto IL_0014;
		}
	}
	{
		TouchU5BU5D_t6_282* L_9 = V_1;
		return L_9;
	}
}
// UnityEngine.Touch UnityEngine.Input::GetTouch(System.Int32)
extern "C" Touch_t6_94  Input_GetTouch_m6_694 (Object_t * __this /* static, unused */, int32_t ___index, const MethodInfo* method)
{
	typedef Touch_t6_94  (*Input_GetTouch_m6_694_ftn) (int32_t);
	static Input_GetTouch_m6_694_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetTouch_m6_694_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetTouch(System.Int32)");
	return _il2cpp_icall_func(___index);
}
// System.Int32 UnityEngine.Input::get_touchCount()
extern "C" int32_t Input_get_touchCount_m6_695 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Input_get_touchCount_m6_695_ftn) ();
	static Input_get_touchCount_m6_695_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_get_touchCount_m6_695_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::get_touchCount()");
	return _il2cpp_icall_func();
}
// System.Boolean UnityEngine.Input::get_touchSupported()
extern "C" bool Input_get_touchSupported_m6_696 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		return 1;
	}
}
// System.Void UnityEngine.Input::set_imeCompositionMode(UnityEngine.IMECompositionMode)
extern "C" void Input_set_imeCompositionMode_m6_697 (Object_t * __this /* static, unused */, int32_t ___value, const MethodInfo* method)
{
	typedef void (*Input_set_imeCompositionMode_m6_697_ftn) (int32_t);
	static Input_set_imeCompositionMode_m6_697_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_set_imeCompositionMode_m6_697_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::set_imeCompositionMode(UnityEngine.IMECompositionMode)");
	_il2cpp_icall_func(___value);
}
// System.String UnityEngine.Input::get_compositionString()
extern "C" String_t* Input_get_compositionString_m6_698 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*Input_get_compositionString_m6_698_ftn) ();
	static Input_get_compositionString_m6_698_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_get_compositionString_m6_698_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::get_compositionString()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Input::set_compositionCursorPos(UnityEngine.Vector2)
extern TypeInfo* Input_t6_95_il2cpp_TypeInfo_var;
extern "C" void Input_set_compositionCursorPos_m6_699 (Object_t * __this /* static, unused */, Vector2_t6_47  ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t6_95_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1613);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t6_95_il2cpp_TypeInfo_var);
		Input_INTERNAL_set_compositionCursorPos_m6_700(NULL /*static, unused*/, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Input::INTERNAL_set_compositionCursorPos(UnityEngine.Vector2&)
extern "C" void Input_INTERNAL_set_compositionCursorPos_m6_700 (Object_t * __this /* static, unused */, Vector2_t6_47 * ___value, const MethodInfo* method)
{
	typedef void (*Input_INTERNAL_set_compositionCursorPos_m6_700_ftn) (Vector2_t6_47 *);
	static Input_INTERNAL_set_compositionCursorPos_m6_700_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_INTERNAL_set_compositionCursorPos_m6_700_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::INTERNAL_set_compositionCursorPos(UnityEngine.Vector2&)");
	_il2cpp_icall_func(___value);
}
// System.Void UnityEngine.Object::.ctor()
extern "C" void Object__ctor_m6_701 (Object_t6_5 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object UnityEngine.Object::Internal_CloneSingle(UnityEngine.Object)
extern "C" Object_t6_5 * Object_Internal_CloneSingle_m6_702 (Object_t * __this /* static, unused */, Object_t6_5 * ___data, const MethodInfo* method)
{
	typedef Object_t6_5 * (*Object_Internal_CloneSingle_m6_702_ftn) (Object_t6_5 *);
	static Object_Internal_CloneSingle_m6_702_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_Internal_CloneSingle_m6_702_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::Internal_CloneSingle(UnityEngine.Object)");
	return _il2cpp_icall_func(___data);
}
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)
extern "C" void Object_Destroy_m6_703 (Object_t * __this /* static, unused */, Object_t6_5 * ___obj, float ___t, const MethodInfo* method)
{
	typedef void (*Object_Destroy_m6_703_ftn) (Object_t6_5 *, float);
	static Object_Destroy_m6_703_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_Destroy_m6_703_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)");
	_il2cpp_icall_func(___obj, ___t);
}
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C" void Object_Destroy_m6_704 (Object_t * __this /* static, unused */, Object_t6_5 * ___obj, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		V_0 = (0.0f);
		Object_t6_5 * L_0 = ___obj;
		float L_1 = V_0;
		Object_Destroy_m6_703(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object,System.Boolean)
extern "C" void Object_DestroyImmediate_m6_705 (Object_t * __this /* static, unused */, Object_t6_5 * ___obj, bool ___allowDestroyingAssets, const MethodInfo* method)
{
	typedef void (*Object_DestroyImmediate_m6_705_ftn) (Object_t6_5 *, bool);
	static Object_DestroyImmediate_m6_705_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_DestroyImmediate_m6_705_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::DestroyImmediate(UnityEngine.Object,System.Boolean)");
	_il2cpp_icall_func(___obj, ___allowDestroyingAssets);
}
// System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object)
extern "C" void Object_DestroyImmediate_m6_706 (Object_t * __this /* static, unused */, Object_t6_5 * ___obj, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = 0;
		Object_t6_5 * L_0 = ___obj;
		bool L_1 = V_0;
		Object_DestroyImmediate_m6_705(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object[] UnityEngine.Object::FindObjectsOfType(System.Type)
extern "C" ObjectU5BU5D_t6_272* Object_FindObjectsOfType_m6_707 (Object_t * __this /* static, unused */, Type_t * ___type, const MethodInfo* method)
{
	typedef ObjectU5BU5D_t6_272* (*Object_FindObjectsOfType_m6_707_ftn) (Type_t *);
	static Object_FindObjectsOfType_m6_707_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_FindObjectsOfType_m6_707_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::FindObjectsOfType(System.Type)");
	return _il2cpp_icall_func(___type);
}
// System.String UnityEngine.Object::get_name()
extern "C" String_t* Object_get_name_m6_708 (Object_t6_5 * __this, const MethodInfo* method)
{
	typedef String_t* (*Object_get_name_m6_708_ftn) (Object_t6_5 *);
	static Object_get_name_m6_708_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_get_name_m6_708_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::get_name()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Object::set_name(System.String)
extern "C" void Object_set_name_m6_709 (Object_t6_5 * __this, String_t* ___value, const MethodInfo* method)
{
	typedef void (*Object_set_name_m6_709_ftn) (Object_t6_5 *, String_t*);
	static Object_set_name_m6_709_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_set_name_m6_709_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::set_name(System.String)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
extern "C" void Object_DontDestroyOnLoad_m6_710 (Object_t * __this /* static, unused */, Object_t6_5 * ___target, const MethodInfo* method)
{
	typedef void (*Object_DontDestroyOnLoad_m6_710_ftn) (Object_t6_5 *);
	static Object_DontDestroyOnLoad_m6_710_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_DontDestroyOnLoad_m6_710_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)");
	_il2cpp_icall_func(___target);
}
// System.Void UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)
extern "C" void Object_set_hideFlags_m6_711 (Object_t6_5 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*Object_set_hideFlags_m6_711_ftn) (Object_t6_5 *, int32_t);
	static Object_set_hideFlags_m6_711_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_set_hideFlags_m6_711_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)");
	_il2cpp_icall_func(__this, ___value);
}
// System.String UnityEngine.Object::ToString()
extern "C" String_t* Object_ToString_m6_712 (Object_t6_5 * __this, const MethodInfo* method)
{
	typedef String_t* (*Object_ToString_m6_712_ftn) (Object_t6_5 *);
	static Object_ToString_m6_712_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_ToString_m6_712_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::ToString()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.Object::Equals(System.Object)
extern TypeInfo* Object_t6_5_il2cpp_TypeInfo_var;
extern "C" bool Object_Equals_m6_713 (Object_t6_5 * __this, Object_t * ___o, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Object_t6_5_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1599);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___o;
		bool L_1 = Object_CompareBaseObjects_m6_715(NULL /*static, unused*/, __this, ((Object_t6_5 *)IsInstClass(L_0, Object_t6_5_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 UnityEngine.Object::GetHashCode()
extern "C" int32_t Object_GetHashCode_m6_714 (Object_t6_5 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Object_GetInstanceID_m6_717(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityEngine.Object::CompareBaseObjects(UnityEngine.Object,UnityEngine.Object)
extern "C" bool Object_CompareBaseObjects_m6_715 (Object_t * __this /* static, unused */, Object_t6_5 * ___lhs, Object_t6_5 * ___rhs, const MethodInfo* method)
{
	bool V_0 = false;
	bool V_1 = false;
	{
		Object_t6_5 * L_0 = ___lhs;
		V_0 = ((((Object_t*)(Object_t6_5 *)L_0) == ((Object_t*)(Object_t *)NULL))? 1 : 0);
		Object_t6_5 * L_1 = ___rhs;
		V_1 = ((((Object_t*)(Object_t6_5 *)L_1) == ((Object_t*)(Object_t *)NULL))? 1 : 0);
		bool L_2 = V_1;
		if (!L_2)
		{
			goto IL_0018;
		}
	}
	{
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_0018;
		}
	}
	{
		return 1;
	}

IL_0018:
	{
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_0028;
		}
	}
	{
		Object_t6_5 * L_5 = ___lhs;
		bool L_6 = Object_IsNativeObjectAlive_m6_716(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return ((((int32_t)L_6) == ((int32_t)0))? 1 : 0);
	}

IL_0028:
	{
		bool L_7 = V_0;
		if (!L_7)
		{
			goto IL_0038;
		}
	}
	{
		Object_t6_5 * L_8 = ___rhs;
		bool L_9 = Object_IsNativeObjectAlive_m6_716(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return ((((int32_t)L_9) == ((int32_t)0))? 1 : 0);
	}

IL_0038:
	{
		Object_t6_5 * L_10 = ___lhs;
		NullCheck(L_10);
		int32_t L_11 = (L_10->___m_InstanceID_0);
		Object_t6_5 * L_12 = ___rhs;
		NullCheck(L_12);
		int32_t L_13 = (L_12->___m_InstanceID_0);
		return ((((int32_t)L_11) == ((int32_t)L_13))? 1 : 0);
	}
}
// System.Boolean UnityEngine.Object::IsNativeObjectAlive(UnityEngine.Object)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" bool Object_IsNativeObjectAlive_m6_716 (Object_t * __this /* static, unused */, Object_t6_5 * ___o, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t6_5 * L_0 = ___o;
		NullCheck(L_0);
		IntPtr_t L_1 = Object_GetCachedPtr_m6_718(L_0, /*hidden argument*/NULL);
		IntPtr_t L_2 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		bool L_3 = IntPtr_op_Inequality_m1_841(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Int32 UnityEngine.Object::GetInstanceID()
extern "C" int32_t Object_GetInstanceID_m6_717 (Object_t6_5 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_InstanceID_0);
		return L_0;
	}
}
// System.IntPtr UnityEngine.Object::GetCachedPtr()
extern "C" IntPtr_t Object_GetCachedPtr_m6_718 (Object_t6_5 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = (__this->___m_CachedPtr_1);
		return L_0;
	}
}
// System.Void UnityEngine.Object::CheckNullArgument(System.Object,System.String)
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern "C" void Object_CheckNullArgument_m6_719 (Object_t * __this /* static, unused */, Object_t * ___arg, String_t* ___message, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___arg;
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		String_t* L_1 = ___message;
		ArgumentException_t1_1425 * L_2 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13260(L_2, L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_000d:
	{
		return;
	}
}
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C" bool Object_op_Implicit_m6_720 (Object_t * __this /* static, unused */, Object_t6_5 * ___exists, const MethodInfo* method)
{
	{
		Object_t6_5 * L_0 = ___exists;
		bool L_1 = Object_CompareBaseObjects_m6_715(NULL /*static, unused*/, L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		return ((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C" bool Object_op_Equality_m6_721 (Object_t * __this /* static, unused */, Object_t6_5 * ___x, Object_t6_5 * ___y, const MethodInfo* method)
{
	{
		Object_t6_5 * L_0 = ___x;
		Object_t6_5 * L_1 = ___y;
		bool L_2 = Object_CompareBaseObjects_m6_715(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C" bool Object_op_Inequality_m6_722 (Object_t * __this /* static, unused */, Object_t6_5 * ___x, Object_t6_5 * ___y, const MethodInfo* method)
{
	{
		Object_t6_5 * L_0 = ___x;
		Object_t6_5 * L_1 = ___y;
		bool L_2 = Object_CompareBaseObjects_m6_715(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return ((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
// Conversion methods for marshalling of: UnityEngine.Object
extern "C" void Object_t6_5_marshal(const Object_t6_5& unmarshaled, Object_t6_5_marshaled& marshaled)
{
	marshaled.___m_InstanceID_0 = unmarshaled.___m_InstanceID_0;
	marshaled.___m_CachedPtr_1 = reinterpret_cast<intptr_t>((unmarshaled.___m_CachedPtr_1).___m_value_0);
}
extern "C" void Object_t6_5_marshal_back(const Object_t6_5_marshaled& marshaled, Object_t6_5& unmarshaled)
{
	unmarshaled.___m_InstanceID_0 = marshaled.___m_InstanceID_0;
	(unmarshaled.___m_CachedPtr_1).___m_value_0 = reinterpret_cast<void*>(marshaled.___m_CachedPtr_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.Object
extern "C" void Object_t6_5_marshal_cleanup(Object_t6_5_marshaled& marshaled)
{
}
// System.Void UnityEngine.Component::.ctor()
extern "C" void Component__ctor_m6_723 (Component_t6_26 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m6_701(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C" Transform_t6_65 * Component_get_transform_m6_724 (Component_t6_26 * __this, const MethodInfo* method)
{
	typedef Transform_t6_65 * (*Component_get_transform_m6_724_ftn) (Component_t6_26 *);
	static Component_get_transform_m6_724_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_get_transform_m6_724_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::get_transform()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C" GameObject_t6_97 * Component_get_gameObject_m6_725 (Component_t6_26 * __this, const MethodInfo* method)
{
	typedef GameObject_t6_97 * (*Component_get_gameObject_m6_725_ftn) (Component_t6_26 *);
	static Component_get_gameObject_m6_725_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_get_gameObject_m6_725_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::get_gameObject()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Component UnityEngine.Component::GetComponent(System.Type)
extern "C" Component_t6_26 * Component_GetComponent_m6_726 (Component_t6_26 * __this, Type_t * ___type, const MethodInfo* method)
{
	{
		GameObject_t6_97 * L_0 = Component_get_gameObject_m6_725(__this, /*hidden argument*/NULL);
		Type_t * L_1 = ___type;
		NullCheck(L_0);
		Component_t6_26 * L_2 = GameObject_GetComponent_m6_734(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.Component::GetComponentFastPath(System.Type,System.IntPtr)
extern "C" void Component_GetComponentFastPath_m6_727 (Component_t6_26 * __this, Type_t * ___type, IntPtr_t ___oneFurtherThanResultValue, const MethodInfo* method)
{
	typedef void (*Component_GetComponentFastPath_m6_727_ftn) (Component_t6_26 *, Type_t *, IntPtr_t);
	static Component_GetComponentFastPath_m6_727_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_GetComponentFastPath_m6_727_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::GetComponentFastPath(System.Type,System.IntPtr)");
	_il2cpp_icall_func(__this, ___type, ___oneFurtherThanResultValue);
}
// UnityEngine.Component UnityEngine.Component::GetComponentInChildren(System.Type)
extern "C" Component_t6_26 * Component_GetComponentInChildren_m6_728 (Component_t6_26 * __this, Type_t * ___t, const MethodInfo* method)
{
	{
		GameObject_t6_97 * L_0 = Component_get_gameObject_m6_725(__this, /*hidden argument*/NULL);
		Type_t * L_1 = ___t;
		NullCheck(L_0);
		Component_t6_26 * L_2 = GameObject_GetComponentInChildren_m6_736(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Component UnityEngine.Component::GetComponentInParent(System.Type)
extern "C" Component_t6_26 * Component_GetComponentInParent_m6_729 (Component_t6_26 * __this, Type_t * ___t, const MethodInfo* method)
{
	{
		GameObject_t6_97 * L_0 = Component_get_gameObject_m6_725(__this, /*hidden argument*/NULL);
		Type_t * L_1 = ___t;
		NullCheck(L_0);
		Component_t6_26 * L_2 = GameObject_GetComponentInParent_m6_737(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.Component::GetComponentsForListInternal(System.Type,System.Object)
extern "C" void Component_GetComponentsForListInternal_m6_730 (Component_t6_26 * __this, Type_t * ___searchType, Object_t * ___resultList, const MethodInfo* method)
{
	typedef void (*Component_GetComponentsForListInternal_m6_730_ftn) (Component_t6_26 *, Type_t *, Object_t *);
	static Component_GetComponentsForListInternal_m6_730_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_GetComponentsForListInternal_m6_730_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::GetComponentsForListInternal(System.Type,System.Object)");
	_il2cpp_icall_func(__this, ___searchType, ___resultList);
}
// System.Void UnityEngine.Component::GetComponents(System.Type,System.Collections.Generic.List`1<UnityEngine.Component>)
extern "C" void Component_GetComponents_m6_731 (Component_t6_26 * __this, Type_t * ___type, List_1_t1_1840 * ___results, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___type;
		List_1_t1_1840 * L_1 = ___results;
		Component_GetComponentsForListInternal_m6_730(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GameObject::.ctor(System.String)
extern "C" void GameObject__ctor_m6_732 (GameObject_t6_97 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		Object__ctor_m6_701(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name;
		GameObject_Internal_CreateGameObject_m6_748(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GameObject::.ctor()
extern "C" void GameObject__ctor_m6_733 (GameObject_t6_97 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m6_701(__this, /*hidden argument*/NULL);
		GameObject_Internal_CreateGameObject_m6_748(NULL /*static, unused*/, __this, (String_t*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Component UnityEngine.GameObject::GetComponent(System.Type)
extern "C" Component_t6_26 * GameObject_GetComponent_m6_734 (GameObject_t6_97 * __this, Type_t * ___type, const MethodInfo* method)
{
	typedef Component_t6_26 * (*GameObject_GetComponent_m6_734_ftn) (GameObject_t6_97 *, Type_t *);
	static GameObject_GetComponent_m6_734_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_GetComponent_m6_734_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::GetComponent(System.Type)");
	return _il2cpp_icall_func(__this, ___type);
}
// System.Void UnityEngine.GameObject::GetComponentFastPath(System.Type,System.IntPtr)
extern "C" void GameObject_GetComponentFastPath_m6_735 (GameObject_t6_97 * __this, Type_t * ___type, IntPtr_t ___oneFurtherThanResultValue, const MethodInfo* method)
{
	typedef void (*GameObject_GetComponentFastPath_m6_735_ftn) (GameObject_t6_97 *, Type_t *, IntPtr_t);
	static GameObject_GetComponentFastPath_m6_735_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_GetComponentFastPath_m6_735_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::GetComponentFastPath(System.Type,System.IntPtr)");
	_il2cpp_icall_func(__this, ___type, ___oneFurtherThanResultValue);
}
// UnityEngine.Component UnityEngine.GameObject::GetComponentInChildren(System.Type)
extern TypeInfo* IEnumerator_t1_193_il2cpp_TypeInfo_var;
extern TypeInfo* Transform_t6_65_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern "C" Component_t6_26 * GameObject_GetComponentInChildren_m6_736 (GameObject_t6_97 * __this, Type_t * ___type, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IEnumerator_t1_193_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(144);
		Transform_t6_65_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1616);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		s_Il2CppMethodIntialized = true;
	}
	Component_t6_26 * V_0 = {0};
	Transform_t6_65 * V_1 = {0};
	Transform_t6_65 * V_2 = {0};
	Object_t * V_3 = {0};
	Component_t6_26 * V_4 = {0};
	Component_t6_26 * V_5 = {0};
	Object_t * V_6 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = GameObject_get_activeInHierarchy_m6_744(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		Type_t * L_1 = ___type;
		Component_t6_26 * L_2 = GameObject_GetComponent_m6_734(__this, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Component_t6_26 * L_3 = V_0;
		bool L_4 = Object_op_Inequality_m6_722(NULL /*static, unused*/, L_3, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0021;
		}
	}
	{
		Component_t6_26 * L_5 = V_0;
		return L_5;
	}

IL_0021:
	{
		Transform_t6_65 * L_6 = GameObject_get_transform_m6_739(__this, /*hidden argument*/NULL);
		V_1 = L_6;
		Transform_t6_65 * L_7 = V_1;
		bool L_8 = Object_op_Inequality_m6_722(NULL /*static, unused*/, L_7, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0095;
		}
	}
	{
		Transform_t6_65 * L_9 = V_1;
		NullCheck(L_9);
		Object_t * L_10 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(4 /* System.Collections.IEnumerator UnityEngine.Transform::GetEnumerator() */, L_9);
		V_3 = L_10;
	}

IL_003b:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0070;
		}

IL_0040:
		{
			Object_t * L_11 = V_3;
			NullCheck(L_11);
			Object_t * L_12 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_11);
			V_2 = ((Transform_t6_65 *)CastclassClass(L_12, Transform_t6_65_il2cpp_TypeInfo_var));
			Transform_t6_65 * L_13 = V_2;
			NullCheck(L_13);
			GameObject_t6_97 * L_14 = Component_get_gameObject_m6_725(L_13, /*hidden argument*/NULL);
			Type_t * L_15 = ___type;
			NullCheck(L_14);
			Component_t6_26 * L_16 = GameObject_GetComponentInChildren_m6_736(L_14, L_15, /*hidden argument*/NULL);
			V_4 = L_16;
			Component_t6_26 * L_17 = V_4;
			bool L_18 = Object_op_Inequality_m6_722(NULL /*static, unused*/, L_17, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
			if (!L_18)
			{
				goto IL_0070;
			}
		}

IL_0067:
		{
			Component_t6_26 * L_19 = V_4;
			V_5 = L_19;
			IL2CPP_LEAVE(0x97, FINALLY_0080);
		}

IL_0070:
		{
			Object_t * L_20 = V_3;
			NullCheck(L_20);
			bool L_21 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_20);
			if (L_21)
			{
				goto IL_0040;
			}
		}

IL_007b:
		{
			IL2CPP_LEAVE(0x95, FINALLY_0080);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0080;
	}

FINALLY_0080:
	{ // begin finally (depth: 1)
		{
			Object_t * L_22 = V_3;
			V_6 = ((Object_t *)IsInst(L_22, IDisposable_t1_1035_il2cpp_TypeInfo_var));
			Object_t * L_23 = V_6;
			if (L_23)
			{
				goto IL_008d;
			}
		}

IL_008c:
		{
			IL2CPP_END_FINALLY(128)
		}

IL_008d:
		{
			Object_t * L_24 = V_6;
			NullCheck(L_24);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_24);
			IL2CPP_END_FINALLY(128)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(128)
	{
		IL2CPP_JUMP_TBL(0x97, IL_0097)
		IL2CPP_JUMP_TBL(0x95, IL_0095)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0095:
	{
		return (Component_t6_26 *)NULL;
	}

IL_0097:
	{
		Component_t6_26 * L_25 = V_5;
		return L_25;
	}
}
// UnityEngine.Component UnityEngine.GameObject::GetComponentInParent(System.Type)
extern "C" Component_t6_26 * GameObject_GetComponentInParent_m6_737 (GameObject_t6_97 * __this, Type_t * ___type, const MethodInfo* method)
{
	Component_t6_26 * V_0 = {0};
	Transform_t6_65 * V_1 = {0};
	Component_t6_26 * V_2 = {0};
	{
		bool L_0 = GameObject_get_activeInHierarchy_m6_744(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		Type_t * L_1 = ___type;
		Component_t6_26 * L_2 = GameObject_GetComponent_m6_734(__this, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Component_t6_26 * L_3 = V_0;
		bool L_4 = Object_op_Inequality_m6_722(NULL /*static, unused*/, L_3, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0021;
		}
	}
	{
		Component_t6_26 * L_5 = V_0;
		return L_5;
	}

IL_0021:
	{
		Transform_t6_65 * L_6 = GameObject_get_transform_m6_739(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_t6_65 * L_7 = Transform_get_parent_m6_771(L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		Transform_t6_65 * L_8 = V_1;
		bool L_9 = Object_op_Inequality_m6_722(NULL /*static, unused*/, L_8, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_007c;
		}
	}
	{
		goto IL_0070;
	}

IL_003e:
	{
		Transform_t6_65 * L_10 = V_1;
		NullCheck(L_10);
		GameObject_t6_97 * L_11 = Component_get_gameObject_m6_725(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		bool L_12 = GameObject_get_activeInHierarchy_m6_744(L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0069;
		}
	}
	{
		Transform_t6_65 * L_13 = V_1;
		NullCheck(L_13);
		GameObject_t6_97 * L_14 = Component_get_gameObject_m6_725(L_13, /*hidden argument*/NULL);
		Type_t * L_15 = ___type;
		NullCheck(L_14);
		Component_t6_26 * L_16 = GameObject_GetComponent_m6_734(L_14, L_15, /*hidden argument*/NULL);
		V_2 = L_16;
		Component_t6_26 * L_17 = V_2;
		bool L_18 = Object_op_Inequality_m6_722(NULL /*static, unused*/, L_17, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_0069;
		}
	}
	{
		Component_t6_26 * L_19 = V_2;
		return L_19;
	}

IL_0069:
	{
		Transform_t6_65 * L_20 = V_1;
		NullCheck(L_20);
		Transform_t6_65 * L_21 = Transform_get_parent_m6_771(L_20, /*hidden argument*/NULL);
		V_1 = L_21;
	}

IL_0070:
	{
		Transform_t6_65 * L_22 = V_1;
		bool L_23 = Object_op_Inequality_m6_722(NULL /*static, unused*/, L_22, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (L_23)
		{
			goto IL_003e;
		}
	}

IL_007c:
	{
		return (Component_t6_26 *)NULL;
	}
}
// System.Array UnityEngine.GameObject::GetComponentsInternal(System.Type,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Object)
extern "C" Array_t * GameObject_GetComponentsInternal_m6_738 (GameObject_t6_97 * __this, Type_t * ___type, bool ___useSearchTypeAsArrayReturnType, bool ___recursive, bool ___includeInactive, bool ___reverse, Object_t * ___resultList, const MethodInfo* method)
{
	typedef Array_t * (*GameObject_GetComponentsInternal_m6_738_ftn) (GameObject_t6_97 *, Type_t *, bool, bool, bool, bool, Object_t *);
	static GameObject_GetComponentsInternal_m6_738_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_GetComponentsInternal_m6_738_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::GetComponentsInternal(System.Type,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Object)");
	return _il2cpp_icall_func(__this, ___type, ___useSearchTypeAsArrayReturnType, ___recursive, ___includeInactive, ___reverse, ___resultList);
}
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C" Transform_t6_65 * GameObject_get_transform_m6_739 (GameObject_t6_97 * __this, const MethodInfo* method)
{
	typedef Transform_t6_65 * (*GameObject_get_transform_m6_739_ftn) (GameObject_t6_97 *);
	static GameObject_get_transform_m6_739_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_get_transform_m6_739_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::get_transform()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.GameObject::get_layer()
extern "C" int32_t GameObject_get_layer_m6_740 (GameObject_t6_97 * __this, const MethodInfo* method)
{
	typedef int32_t (*GameObject_get_layer_m6_740_ftn) (GameObject_t6_97 *);
	static GameObject_get_layer_m6_740_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_get_layer_m6_740_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::get_layer()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.GameObject::set_layer(System.Int32)
extern "C" void GameObject_set_layer_m6_741 (GameObject_t6_97 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*GameObject_set_layer_m6_741_ftn) (GameObject_t6_97 *, int32_t);
	static GameObject_set_layer_m6_741_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_set_layer_m6_741_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::set_layer(System.Int32)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
extern "C" void GameObject_SetActive_m6_742 (GameObject_t6_97 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*GameObject_SetActive_m6_742_ftn) (GameObject_t6_97 *, bool);
	static GameObject_SetActive_m6_742_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_SetActive_m6_742_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::SetActive(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Boolean UnityEngine.GameObject::get_activeSelf()
extern "C" bool GameObject_get_activeSelf_m6_743 (GameObject_t6_97 * __this, const MethodInfo* method)
{
	typedef bool (*GameObject_get_activeSelf_m6_743_ftn) (GameObject_t6_97 *);
	static GameObject_get_activeSelf_m6_743_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_get_activeSelf_m6_743_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::get_activeSelf()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.GameObject::get_activeInHierarchy()
extern "C" bool GameObject_get_activeInHierarchy_m6_744 (GameObject_t6_97 * __this, const MethodInfo* method)
{
	typedef bool (*GameObject_get_activeInHierarchy_m6_744_ftn) (GameObject_t6_97 *);
	static GameObject_get_activeInHierarchy_m6_744_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_get_activeInHierarchy_m6_744_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::get_activeInHierarchy()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.GameObject::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
extern "C" void GameObject_SendMessage_m6_745 (GameObject_t6_97 * __this, String_t* ___methodName, Object_t * ___value, int32_t ___options, const MethodInfo* method)
{
	typedef void (*GameObject_SendMessage_m6_745_ftn) (GameObject_t6_97 *, String_t*, Object_t *, int32_t);
	static GameObject_SendMessage_m6_745_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_SendMessage_m6_745_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)");
	_il2cpp_icall_func(__this, ___methodName, ___value, ___options);
}
// UnityEngine.Component UnityEngine.GameObject::Internal_AddComponentWithType(System.Type)
extern "C" Component_t6_26 * GameObject_Internal_AddComponentWithType_m6_746 (GameObject_t6_97 * __this, Type_t * ___componentType, const MethodInfo* method)
{
	typedef Component_t6_26 * (*GameObject_Internal_AddComponentWithType_m6_746_ftn) (GameObject_t6_97 *, Type_t *);
	static GameObject_Internal_AddComponentWithType_m6_746_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_Internal_AddComponentWithType_m6_746_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::Internal_AddComponentWithType(System.Type)");
	return _il2cpp_icall_func(__this, ___componentType);
}
// UnityEngine.Component UnityEngine.GameObject::AddComponent(System.Type)
extern "C" Component_t6_26 * GameObject_AddComponent_m6_747 (GameObject_t6_97 * __this, Type_t * ___componentType, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___componentType;
		Component_t6_26 * L_1 = GameObject_Internal_AddComponentWithType_m6_746(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.GameObject::Internal_CreateGameObject(UnityEngine.GameObject,System.String)
extern "C" void GameObject_Internal_CreateGameObject_m6_748 (Object_t * __this /* static, unused */, GameObject_t6_97 * ___mono, String_t* ___name, const MethodInfo* method)
{
	typedef void (*GameObject_Internal_CreateGameObject_m6_748_ftn) (GameObject_t6_97 *, String_t*);
	static GameObject_Internal_CreateGameObject_m6_748_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_Internal_CreateGameObject_m6_748_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::Internal_CreateGameObject(UnityEngine.GameObject,System.String)");
	_il2cpp_icall_func(___mono, ___name);
}
// UnityEngine.GameObject UnityEngine.GameObject::Find(System.String)
extern "C" GameObject_t6_97 * GameObject_Find_m6_749 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	typedef GameObject_t6_97 * (*GameObject_Find_m6_749_ftn) (String_t*);
	static GameObject_Find_m6_749_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_Find_m6_749_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::Find(System.String)");
	return _il2cpp_icall_func(___name);
}
// System.Void UnityEngine.Transform/Enumerator::.ctor(UnityEngine.Transform)
extern "C" void Enumerator__ctor_m6_750 (Enumerator_t6_98 * __this, Transform_t6_65 * ___outer, const MethodInfo* method)
{
	{
		__this->___currentIndex_1 = (-1);
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		Transform_t6_65 * L_0 = ___outer;
		__this->___outer_0 = L_0;
		return;
	}
}
// System.Object UnityEngine.Transform/Enumerator::get_Current()
extern "C" Object_t * Enumerator_get_Current_m6_751 (Enumerator_t6_98 * __this, const MethodInfo* method)
{
	{
		Transform_t6_65 * L_0 = (__this->___outer_0);
		int32_t L_1 = (__this->___currentIndex_1);
		NullCheck(L_0);
		Transform_t6_65 * L_2 = Transform_GetChild_m6_789(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.Transform/Enumerator::MoveNext()
extern "C" bool Enumerator_MoveNext_m6_752 (Enumerator_t6_98 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Transform_t6_65 * L_0 = (__this->___outer_0);
		NullCheck(L_0);
		int32_t L_1 = Transform_get_childCount_m6_783(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = (__this->___currentIndex_1);
		int32_t L_3 = ((int32_t)((int32_t)L_2+(int32_t)1));
		V_1 = L_3;
		__this->___currentIndex_1 = L_3;
		int32_t L_4 = V_1;
		int32_t L_5 = V_0;
		return ((((int32_t)L_4) < ((int32_t)L_5))? 1 : 0);
	}
}
// System.Void UnityEngine.Transform/Enumerator::Reset()
extern "C" void Enumerator_Reset_m6_753 (Enumerator_t6_98 * __this, const MethodInfo* method)
{
	{
		__this->___currentIndex_1 = (-1);
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C" Vector3_t6_48  Transform_get_position_m6_754 (Transform_t6_65 * __this, const MethodInfo* method)
{
	Vector3_t6_48  V_0 = {0};
	{
		Transform_INTERNAL_get_position_m6_755(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t6_48  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_position(UnityEngine.Vector3&)
extern "C" void Transform_INTERNAL_get_position_m6_755 (Transform_t6_65 * __this, Vector3_t6_48 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_position_m6_755_ftn) (Transform_t6_65 *, Vector3_t6_48 *);
	static Transform_INTERNAL_get_position_m6_755_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_position_m6_755_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_position(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_localPosition()
extern "C" Vector3_t6_48  Transform_get_localPosition_m6_756 (Transform_t6_65 * __this, const MethodInfo* method)
{
	Vector3_t6_48  V_0 = {0};
	{
		Transform_INTERNAL_get_localPosition_m6_758(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t6_48  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
extern "C" void Transform_set_localPosition_m6_757 (Transform_t6_65 * __this, Vector3_t6_48  ___value, const MethodInfo* method)
{
	{
		Transform_INTERNAL_set_localPosition_m6_759(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_localPosition(UnityEngine.Vector3&)
extern "C" void Transform_INTERNAL_get_localPosition_m6_758 (Transform_t6_65 * __this, Vector3_t6_48 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_localPosition_m6_758_ftn) (Transform_t6_65 *, Vector3_t6_48 *);
	static Transform_INTERNAL_get_localPosition_m6_758_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_localPosition_m6_758_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_localPosition(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Transform::INTERNAL_set_localPosition(UnityEngine.Vector3&)
extern "C" void Transform_INTERNAL_set_localPosition_m6_759 (Transform_t6_65 * __this, Vector3_t6_48 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_set_localPosition_m6_759_ftn) (Transform_t6_65 *, Vector3_t6_48 *);
	static Transform_INTERNAL_set_localPosition_m6_759_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_localPosition_m6_759_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_localPosition(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_forward()
extern "C" Vector3_t6_48  Transform_get_forward_m6_760 (Transform_t6_65 * __this, const MethodInfo* method)
{
	{
		Quaternion_t6_50  L_0 = Transform_get_rotation_m6_761(__this, /*hidden argument*/NULL);
		Vector3_t6_48  L_1 = Vector3_get_forward_m6_257(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t6_48  L_2 = Quaternion_op_Multiply_m6_293(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
extern "C" Quaternion_t6_50  Transform_get_rotation_m6_761 (Transform_t6_65 * __this, const MethodInfo* method)
{
	Quaternion_t6_50  V_0 = {0};
	{
		Transform_INTERNAL_get_rotation_m6_762(__this, (&V_0), /*hidden argument*/NULL);
		Quaternion_t6_50  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_rotation(UnityEngine.Quaternion&)
extern "C" void Transform_INTERNAL_get_rotation_m6_762 (Transform_t6_65 * __this, Quaternion_t6_50 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_rotation_m6_762_ftn) (Transform_t6_65 *, Quaternion_t6_50 *);
	static Transform_INTERNAL_get_rotation_m6_762_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_rotation_m6_762_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_rotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Quaternion UnityEngine.Transform::get_localRotation()
extern "C" Quaternion_t6_50  Transform_get_localRotation_m6_763 (Transform_t6_65 * __this, const MethodInfo* method)
{
	Quaternion_t6_50  V_0 = {0};
	{
		Transform_INTERNAL_get_localRotation_m6_765(__this, (&V_0), /*hidden argument*/NULL);
		Quaternion_t6_50  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_localRotation(UnityEngine.Quaternion)
extern "C" void Transform_set_localRotation_m6_764 (Transform_t6_65 * __this, Quaternion_t6_50  ___value, const MethodInfo* method)
{
	{
		Transform_INTERNAL_set_localRotation_m6_766(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_localRotation(UnityEngine.Quaternion&)
extern "C" void Transform_INTERNAL_get_localRotation_m6_765 (Transform_t6_65 * __this, Quaternion_t6_50 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_localRotation_m6_765_ftn) (Transform_t6_65 *, Quaternion_t6_50 *);
	static Transform_INTERNAL_get_localRotation_m6_765_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_localRotation_m6_765_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_localRotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Transform::INTERNAL_set_localRotation(UnityEngine.Quaternion&)
extern "C" void Transform_INTERNAL_set_localRotation_m6_766 (Transform_t6_65 * __this, Quaternion_t6_50 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_set_localRotation_m6_766_ftn) (Transform_t6_65 *, Quaternion_t6_50 *);
	static Transform_INTERNAL_set_localRotation_m6_766_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_localRotation_m6_766_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_localRotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_localScale()
extern "C" Vector3_t6_48  Transform_get_localScale_m6_767 (Transform_t6_65 * __this, const MethodInfo* method)
{
	Vector3_t6_48  V_0 = {0};
	{
		Transform_INTERNAL_get_localScale_m6_769(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t6_48  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
extern "C" void Transform_set_localScale_m6_768 (Transform_t6_65 * __this, Vector3_t6_48  ___value, const MethodInfo* method)
{
	{
		Transform_INTERNAL_set_localScale_m6_770(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_localScale(UnityEngine.Vector3&)
extern "C" void Transform_INTERNAL_get_localScale_m6_769 (Transform_t6_65 * __this, Vector3_t6_48 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_localScale_m6_769_ftn) (Transform_t6_65 *, Vector3_t6_48 *);
	static Transform_INTERNAL_get_localScale_m6_769_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_localScale_m6_769_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_localScale(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Transform::INTERNAL_set_localScale(UnityEngine.Vector3&)
extern "C" void Transform_INTERNAL_set_localScale_m6_770 (Transform_t6_65 * __this, Vector3_t6_48 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_set_localScale_m6_770_ftn) (Transform_t6_65 *, Vector3_t6_48 *);
	static Transform_INTERNAL_set_localScale_m6_770_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_localScale_m6_770_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_localScale(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Transform UnityEngine.Transform::get_parent()
extern "C" Transform_t6_65 * Transform_get_parent_m6_771 (Transform_t6_65 * __this, const MethodInfo* method)
{
	{
		Transform_t6_65 * L_0 = Transform_get_parentInternal_m6_773(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_parent(UnityEngine.Transform)
extern TypeInfo* RectTransform_t6_64_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4735;
extern "C" void Transform_set_parent_m6_772 (Transform_t6_65 * __this, Transform_t6_65 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t6_64_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1597);
		_stringLiteral4735 = il2cpp_codegen_string_literal_from_index(4735);
		s_Il2CppMethodIntialized = true;
	}
	{
		if (!((RectTransform_t6_64 *)IsInstSealed(__this, RectTransform_t6_64_il2cpp_TypeInfo_var)))
		{
			goto IL_0016;
		}
	}
	{
		Debug_LogWarning_m6_637(NULL /*static, unused*/, _stringLiteral4735, __this, /*hidden argument*/NULL);
	}

IL_0016:
	{
		Transform_t6_65 * L_0 = ___value;
		Transform_set_parentInternal_m6_774(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Transform UnityEngine.Transform::get_parentInternal()
extern "C" Transform_t6_65 * Transform_get_parentInternal_m6_773 (Transform_t6_65 * __this, const MethodInfo* method)
{
	typedef Transform_t6_65 * (*Transform_get_parentInternal_m6_773_ftn) (Transform_t6_65 *);
	static Transform_get_parentInternal_m6_773_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_get_parentInternal_m6_773_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::get_parentInternal()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Transform::set_parentInternal(UnityEngine.Transform)
extern "C" void Transform_set_parentInternal_m6_774 (Transform_t6_65 * __this, Transform_t6_65 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_set_parentInternal_m6_774_ftn) (Transform_t6_65 *, Transform_t6_65 *);
	static Transform_set_parentInternal_m6_774_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_set_parentInternal_m6_774_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::set_parentInternal(UnityEngine.Transform)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform)
extern "C" void Transform_SetParent_m6_775 (Transform_t6_65 * __this, Transform_t6_65 * ___parent, const MethodInfo* method)
{
	{
		Transform_t6_65 * L_0 = ___parent;
		Transform_SetParent_m6_776(__this, L_0, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform,System.Boolean)
extern "C" void Transform_SetParent_m6_776 (Transform_t6_65 * __this, Transform_t6_65 * ___parent, bool ___worldPositionStays, const MethodInfo* method)
{
	typedef void (*Transform_SetParent_m6_776_ftn) (Transform_t6_65 *, Transform_t6_65 *, bool);
	static Transform_SetParent_m6_776_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_SetParent_m6_776_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::SetParent(UnityEngine.Transform,System.Boolean)");
	_il2cpp_icall_func(__this, ___parent, ___worldPositionStays);
}
// UnityEngine.Matrix4x4 UnityEngine.Transform::get_worldToLocalMatrix()
extern "C" Matrix4x4_t6_52  Transform_get_worldToLocalMatrix_m6_777 (Transform_t6_65 * __this, const MethodInfo* method)
{
	Matrix4x4_t6_52  V_0 = {0};
	{
		Transform_INTERNAL_get_worldToLocalMatrix_m6_778(__this, (&V_0), /*hidden argument*/NULL);
		Matrix4x4_t6_52  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_worldToLocalMatrix(UnityEngine.Matrix4x4&)
extern "C" void Transform_INTERNAL_get_worldToLocalMatrix_m6_778 (Transform_t6_65 * __this, Matrix4x4_t6_52 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_worldToLocalMatrix_m6_778_ftn) (Transform_t6_65 *, Matrix4x4_t6_52 *);
	static Transform_INTERNAL_get_worldToLocalMatrix_m6_778_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_worldToLocalMatrix_m6_778_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_worldToLocalMatrix(UnityEngine.Matrix4x4&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Vector3 UnityEngine.Transform::TransformPoint(UnityEngine.Vector3)
extern "C" Vector3_t6_48  Transform_TransformPoint_m6_779 (Transform_t6_65 * __this, Vector3_t6_48  ___position, const MethodInfo* method)
{
	{
		Vector3_t6_48  L_0 = Transform_INTERNAL_CALL_TransformPoint_m6_780(NULL /*static, unused*/, __this, (&___position), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Transform::INTERNAL_CALL_TransformPoint(UnityEngine.Transform,UnityEngine.Vector3&)
extern "C" Vector3_t6_48  Transform_INTERNAL_CALL_TransformPoint_m6_780 (Object_t * __this /* static, unused */, Transform_t6_65 * ___self, Vector3_t6_48 * ___position, const MethodInfo* method)
{
	typedef Vector3_t6_48  (*Transform_INTERNAL_CALL_TransformPoint_m6_780_ftn) (Transform_t6_65 *, Vector3_t6_48 *);
	static Transform_INTERNAL_CALL_TransformPoint_m6_780_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_CALL_TransformPoint_m6_780_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_CALL_TransformPoint(UnityEngine.Transform,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___self, ___position);
}
// UnityEngine.Vector3 UnityEngine.Transform::InverseTransformPoint(UnityEngine.Vector3)
extern "C" Vector3_t6_48  Transform_InverseTransformPoint_m6_781 (Transform_t6_65 * __this, Vector3_t6_48  ___position, const MethodInfo* method)
{
	{
		Vector3_t6_48  L_0 = Transform_INTERNAL_CALL_InverseTransformPoint_m6_782(NULL /*static, unused*/, __this, (&___position), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Transform::INTERNAL_CALL_InverseTransformPoint(UnityEngine.Transform,UnityEngine.Vector3&)
extern "C" Vector3_t6_48  Transform_INTERNAL_CALL_InverseTransformPoint_m6_782 (Object_t * __this /* static, unused */, Transform_t6_65 * ___self, Vector3_t6_48 * ___position, const MethodInfo* method)
{
	typedef Vector3_t6_48  (*Transform_INTERNAL_CALL_InverseTransformPoint_m6_782_ftn) (Transform_t6_65 *, Vector3_t6_48 *);
	static Transform_INTERNAL_CALL_InverseTransformPoint_m6_782_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_CALL_InverseTransformPoint_m6_782_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_CALL_InverseTransformPoint(UnityEngine.Transform,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___self, ___position);
}
// System.Int32 UnityEngine.Transform::get_childCount()
extern "C" int32_t Transform_get_childCount_m6_783 (Transform_t6_65 * __this, const MethodInfo* method)
{
	typedef int32_t (*Transform_get_childCount_m6_783_ftn) (Transform_t6_65 *);
	static Transform_get_childCount_m6_783_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_get_childCount_m6_783_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::get_childCount()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Transform::SetAsFirstSibling()
extern "C" void Transform_SetAsFirstSibling_m6_784 (Transform_t6_65 * __this, const MethodInfo* method)
{
	typedef void (*Transform_SetAsFirstSibling_m6_784_ftn) (Transform_t6_65 *);
	static Transform_SetAsFirstSibling_m6_784_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_SetAsFirstSibling_m6_784_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::SetAsFirstSibling()");
	_il2cpp_icall_func(__this);
}
// UnityEngine.Transform UnityEngine.Transform::Find(System.String)
extern "C" Transform_t6_65 * Transform_Find_m6_785 (Transform_t6_65 * __this, String_t* ___name, const MethodInfo* method)
{
	typedef Transform_t6_65 * (*Transform_Find_m6_785_ftn) (Transform_t6_65 *, String_t*);
	static Transform_Find_m6_785_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_Find_m6_785_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::Find(System.String)");
	return _il2cpp_icall_func(__this, ___name);
}
// System.Boolean UnityEngine.Transform::IsChildOf(UnityEngine.Transform)
extern "C" bool Transform_IsChildOf_m6_786 (Transform_t6_65 * __this, Transform_t6_65 * ___parent, const MethodInfo* method)
{
	typedef bool (*Transform_IsChildOf_m6_786_ftn) (Transform_t6_65 *, Transform_t6_65 *);
	static Transform_IsChildOf_m6_786_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_IsChildOf_m6_786_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::IsChildOf(UnityEngine.Transform)");
	return _il2cpp_icall_func(__this, ___parent);
}
// UnityEngine.Transform UnityEngine.Transform::FindChild(System.String)
extern "C" Transform_t6_65 * Transform_FindChild_m6_787 (Transform_t6_65 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		Transform_t6_65 * L_1 = Transform_Find_m6_785(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Collections.IEnumerator UnityEngine.Transform::GetEnumerator()
extern TypeInfo* Enumerator_t6_98_il2cpp_TypeInfo_var;
extern "C" Object_t * Transform_GetEnumerator_m6_788 (Transform_t6_65 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Enumerator_t6_98_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1617);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t6_98 * L_0 = (Enumerator_t6_98 *)il2cpp_codegen_object_new (Enumerator_t6_98_il2cpp_TypeInfo_var);
		Enumerator__ctor_m6_750(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
extern "C" Transform_t6_65 * Transform_GetChild_m6_789 (Transform_t6_65 * __this, int32_t ___index, const MethodInfo* method)
{
	typedef Transform_t6_65 * (*Transform_GetChild_m6_789_ftn) (Transform_t6_65 *, int32_t);
	static Transform_GetChild_m6_789_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_GetChild_m6_789_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::GetChild(System.Int32)");
	return _il2cpp_icall_func(__this, ___index);
}
// System.Single UnityEngine.Time::get_time()
extern "C" float Time_get_time_m6_790 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Time_get_time_m6_790_ftn) ();
	static Time_get_time_m6_790_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_time_m6_790_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_time()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.Time::get_deltaTime()
extern "C" float Time_get_deltaTime_m6_791 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Time_get_deltaTime_m6_791_ftn) ();
	static Time_get_deltaTime_m6_791_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_deltaTime_m6_791_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_deltaTime()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.Time::get_unscaledTime()
extern "C" float Time_get_unscaledTime_m6_792 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Time_get_unscaledTime_m6_792_ftn) ();
	static Time_get_unscaledTime_m6_792_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_unscaledTime_m6_792_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_unscaledTime()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.Time::get_unscaledDeltaTime()
extern "C" float Time_get_unscaledDeltaTime_m6_793 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Time_get_unscaledDeltaTime_m6_793_ftn) ();
	static Time_get_unscaledDeltaTime_m6_793_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_unscaledDeltaTime_m6_793_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_unscaledDeltaTime()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.Time::get_timeScale()
extern "C" float Time_get_timeScale_m6_794 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Time_get_timeScale_m6_794_ftn) ();
	static Time_get_timeScale_m6_794_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_timeScale_m6_794_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_timeScale()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Time::set_timeScale(System.Single)
extern "C" void Time_set_timeScale_m6_795 (Object_t * __this /* static, unused */, float ___value, const MethodInfo* method)
{
	typedef void (*Time_set_timeScale_m6_795_ftn) (float);
	static Time_set_timeScale_m6_795_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_set_timeScale_m6_795_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::set_timeScale(System.Single)");
	_il2cpp_icall_func(___value);
}
// System.Single UnityEngine.Time::get_realtimeSinceStartup()
extern "C" float Time_get_realtimeSinceStartup_m6_796 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Time_get_realtimeSinceStartup_m6_796_ftn) ();
	static Time_get_realtimeSinceStartup_m6_796_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_realtimeSinceStartup_m6_796_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_realtimeSinceStartup()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
extern "C" int32_t Random_Range_m6_797 (Object_t * __this /* static, unused */, int32_t ___min, int32_t ___max, const MethodInfo* method)
{
	{
		int32_t L_0 = ___min;
		int32_t L_1 = ___max;
		int32_t L_2 = Random_RandomRangeInt_m6_798(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 UnityEngine.Random::RandomRangeInt(System.Int32,System.Int32)
extern "C" int32_t Random_RandomRangeInt_m6_798 (Object_t * __this /* static, unused */, int32_t ___min, int32_t ___max, const MethodInfo* method)
{
	typedef int32_t (*Random_RandomRangeInt_m6_798_ftn) (int32_t, int32_t);
	static Random_RandomRangeInt_m6_798_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Random_RandomRangeInt_m6_798_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Random::RandomRangeInt(System.Int32,System.Int32)");
	return _il2cpp_icall_func(___min, ___max);
}
// System.Void UnityEngine.YieldInstruction::.ctor()
extern "C" void YieldInstruction__ctor_m6_799 (YieldInstruction_t6_11 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.YieldInstruction
extern "C" void YieldInstruction_t6_11_marshal(const YieldInstruction_t6_11& unmarshaled, YieldInstruction_t6_11_marshaled& marshaled)
{
}
extern "C" void YieldInstruction_t6_11_marshal_back(const YieldInstruction_t6_11_marshaled& marshaled, YieldInstruction_t6_11& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: UnityEngine.YieldInstruction
extern "C" void YieldInstruction_t6_11_marshal_cleanup(YieldInstruction_t6_11_marshaled& marshaled)
{
}
// System.Void UnityEngine.PlayerPrefsException::.ctor(System.String)
extern "C" void PlayerPrefsException__ctor_m6_800 (PlayerPrefsException_t6_101 * __this, String_t* ___error, const MethodInfo* method)
{
	{
		String_t* L_0 = ___error;
		Exception__ctor_m1_1238(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.PlayerPrefs::TrySetInt(System.String,System.Int32)
extern "C" bool PlayerPrefs_TrySetInt_m6_801 (Object_t * __this /* static, unused */, String_t* ___key, int32_t ___value, const MethodInfo* method)
{
	typedef bool (*PlayerPrefs_TrySetInt_m6_801_ftn) (String_t*, int32_t);
	static PlayerPrefs_TrySetInt_m6_801_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_TrySetInt_m6_801_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::TrySetInt(System.String,System.Int32)");
	return _il2cpp_icall_func(___key, ___value);
}
// System.Boolean UnityEngine.PlayerPrefs::TrySetSetString(System.String,System.String)
extern "C" bool PlayerPrefs_TrySetSetString_m6_802 (Object_t * __this /* static, unused */, String_t* ___key, String_t* ___value, const MethodInfo* method)
{
	typedef bool (*PlayerPrefs_TrySetSetString_m6_802_ftn) (String_t*, String_t*);
	static PlayerPrefs_TrySetSetString_m6_802_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_TrySetSetString_m6_802_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::TrySetSetString(System.String,System.String)");
	return _il2cpp_icall_func(___key, ___value);
}
// System.Void UnityEngine.PlayerPrefs::SetInt(System.String,System.Int32)
extern TypeInfo* PlayerPrefsException_t6_101_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4736;
extern "C" void PlayerPrefs_SetInt_m6_803 (Object_t * __this /* static, unused */, String_t* ___key, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PlayerPrefsException_t6_101_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1618);
		_stringLiteral4736 = il2cpp_codegen_string_literal_from_index(4736);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___key;
		int32_t L_1 = ___value;
		bool L_2 = PlayerPrefs_TrySetInt_m6_801(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0017;
		}
	}
	{
		PlayerPrefsException_t6_101 * L_3 = (PlayerPrefsException_t6_101 *)il2cpp_codegen_object_new (PlayerPrefsException_t6_101_il2cpp_TypeInfo_var);
		PlayerPrefsException__ctor_m6_800(L_3, _stringLiteral4736, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0017:
	{
		return;
	}
}
// System.Int32 UnityEngine.PlayerPrefs::GetInt(System.String,System.Int32)
extern "C" int32_t PlayerPrefs_GetInt_m6_804 (Object_t * __this /* static, unused */, String_t* ___key, int32_t ___defaultValue, const MethodInfo* method)
{
	typedef int32_t (*PlayerPrefs_GetInt_m6_804_ftn) (String_t*, int32_t);
	static PlayerPrefs_GetInt_m6_804_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_GetInt_m6_804_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::GetInt(System.String,System.Int32)");
	return _il2cpp_icall_func(___key, ___defaultValue);
}
// System.Void UnityEngine.PlayerPrefs::SetString(System.String,System.String)
extern TypeInfo* PlayerPrefsException_t6_101_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4736;
extern "C" void PlayerPrefs_SetString_m6_805 (Object_t * __this /* static, unused */, String_t* ___key, String_t* ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PlayerPrefsException_t6_101_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1618);
		_stringLiteral4736 = il2cpp_codegen_string_literal_from_index(4736);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___key;
		String_t* L_1 = ___value;
		bool L_2 = PlayerPrefs_TrySetSetString_m6_802(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0017;
		}
	}
	{
		PlayerPrefsException_t6_101 * L_3 = (PlayerPrefsException_t6_101 *)il2cpp_codegen_object_new (PlayerPrefsException_t6_101_il2cpp_TypeInfo_var);
		PlayerPrefsException__ctor_m6_800(L_3, _stringLiteral4736, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0017:
	{
		return;
	}
}
// System.String UnityEngine.PlayerPrefs::GetString(System.String,System.String)
extern "C" String_t* PlayerPrefs_GetString_m6_806 (Object_t * __this /* static, unused */, String_t* ___key, String_t* ___defaultValue, const MethodInfo* method)
{
	typedef String_t* (*PlayerPrefs_GetString_m6_806_ftn) (String_t*, String_t*);
	static PlayerPrefs_GetString_m6_806_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_GetString_m6_806_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::GetString(System.String,System.String)");
	return _il2cpp_icall_func(___key, ___defaultValue);
}
// System.String UnityEngine.PlayerPrefs::GetString(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" String_t* PlayerPrefs_GetString_m6_807 (Object_t * __this /* static, unused */, String_t* ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_0 = L_0;
		String_t* L_1 = ___key;
		String_t* L_2 = V_0;
		String_t* L_3 = PlayerPrefs_GetString_m6_806(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void UnityEngine.PlayerPrefs::DeleteAll()
extern "C" void PlayerPrefs_DeleteAll_m6_808 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*PlayerPrefs_DeleteAll_m6_808_ftn) ();
	static PlayerPrefs_DeleteAll_m6_808_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_DeleteAll_m6_808_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::DeleteAll()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.PlayerPrefs::Save()
extern "C" void PlayerPrefs_Save_m6_809 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*PlayerPrefs_Save_m6_809_ftn) ();
	static PlayerPrefs_Save_m6_809_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_Save_m6_809_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::Save()");
	_il2cpp_icall_func();
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
