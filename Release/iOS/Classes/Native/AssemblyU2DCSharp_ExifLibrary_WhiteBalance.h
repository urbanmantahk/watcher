﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "AssemblyU2DCSharp_ExifLibrary_WhiteBalance.h"

// ExifLibrary.WhiteBalance
struct  WhiteBalance_t8_79 
{
	// System.UInt16 ExifLibrary.WhiteBalance::value__
	uint16_t ___value___1;
};
