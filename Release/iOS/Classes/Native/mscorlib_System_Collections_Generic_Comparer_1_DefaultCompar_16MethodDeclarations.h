﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Int32,ExifLibrary.IFD>>
struct DefaultComparer_t1_2686;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_21.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Int32,ExifLibrary.IFD>>::.ctor()
extern "C" void DefaultComparer__ctor_m1_26602_gshared (DefaultComparer_t1_2686 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1_26602(__this, method) (( void (*) (DefaultComparer_t1_2686 *, const MethodInfo*))DefaultComparer__ctor_m1_26602_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Int32,ExifLibrary.IFD>>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m1_26603_gshared (DefaultComparer_t1_2686 * __this, KeyValuePair_2_t1_2681  ___x, KeyValuePair_2_t1_2681  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m1_26603(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t1_2686 *, KeyValuePair_2_t1_2681 , KeyValuePair_2_t1_2681 , const MethodInfo*))DefaultComparer_Compare_m1_26603_gshared)(__this, ___x, ___y, method)
