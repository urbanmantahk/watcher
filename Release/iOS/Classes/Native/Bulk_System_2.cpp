﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.Text.RegularExpressions.Syntax.NonBacktrackingGroup
struct NonBacktrackingGroup_t3_219;
// System.Text.RegularExpressions.ICompiler
struct ICompiler_t3_246;
// System.Text.RegularExpressions.Syntax.Repetition
struct Repetition_t3_220;
// System.Text.RegularExpressions.Syntax.Expression
struct Expression_t3_213;
// System.Text.RegularExpressions.Syntax.AnchorInfo
struct AnchorInfo_t3_230;
// System.Text.RegularExpressions.Syntax.Assertion
struct Assertion_t3_221;
// System.Text.RegularExpressions.Syntax.CaptureAssertion
struct CaptureAssertion_t3_222;
// System.Text.RegularExpressions.Syntax.Literal
struct Literal_t3_224;
// System.Text.RegularExpressions.Syntax.CapturingGroup
struct CapturingGroup_t3_217;
// System.Text.RegularExpressions.Syntax.ExpressionAssertion
struct ExpressionAssertion_t3_223;
// System.Text.RegularExpressions.Syntax.Alternation
struct Alternation_t3_225;
// System.Text.RegularExpressions.Syntax.ExpressionCollection
struct ExpressionCollection_t3_212;
// System.String
struct String_t;
// System.Text.RegularExpressions.Syntax.PositionAssertion
struct PositionAssertion_t3_226;
// System.Text.RegularExpressions.Syntax.Reference
struct Reference_t3_227;
// System.Text.RegularExpressions.Syntax.BackslashNumber
struct BackslashNumber_t3_228;
// System.Collections.Hashtable
struct Hashtable_t1_100;
// System.Text.RegularExpressions.Syntax.CharacterClass
struct CharacterClass_t3_229;
// System.DefaultUriParser
struct DefaultUriParser_t3_231;
// System.Uri
struct Uri_t3_3;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;
// System.String[]
struct StringU5BU5D_t1_238;
// System.Object
struct Object_t;
// System.UriParser
struct UriParser_t3_232;
// System.Char[]
struct CharU5BU5D_t1_16;
// System.IO.MemoryStream
struct MemoryStream_t1_433;
// System.Text.Encoding
struct Encoding_t1_406;
// System.UriFormatException
struct UriFormatException_t3_19;
// System.Net.Security.LocalCertificateSelectionCallback
struct LocalCertificateSelectionCallback_t3_50;
// System.Security.Cryptography.X509Certificates.X509Certificate
struct X509Certificate_t1_1179;
// System.Security.Cryptography.X509Certificates.X509CertificateCollection
struct X509CertificateCollection_t3_1;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;
// System.Net.Security.RemoteCertificateValidationCallback
struct RemoteCertificateValidationCallback_t3_5;
// System.Security.Cryptography.X509Certificates.X509Chain
struct X509Chain_t3_7;
// System.Net.BindIPEndPoint
struct BindIPEndPoint_t3_125;
// System.Net.IPEndPoint
struct IPEndPoint_t3_110;
// System.Net.ServicePoint
struct ServicePoint_t3_4;
// System.Net.HttpContinueDelegate
struct HttpContinueDelegate_t3_115;
// System.Net.WebHeaderCollection
struct WebHeaderCollection_t3_80;
// System.Text.RegularExpressions.MatchEvaluator
struct MatchEvaluator_t3_240;
// System.Text.RegularExpressions.Match
struct Match_t3_13;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array.h"
#include "System_System_Text_RegularExpressions_Syntax_NonBacktracking.h"
#include "System_System_Text_RegularExpressions_Syntax_NonBacktrackingMethodDeclarations.h"
#include "mscorlib_System_Void.h"
#include "System_System_Text_RegularExpressions_Syntax_GroupMethodDeclarations.h"
#include "System_System_Text_RegularExpressions_Syntax_Group.h"
#include "mscorlib_System_Boolean.h"
#include "System_System_Text_RegularExpressions_LinkRef.h"
#include "System_System_Text_RegularExpressions_Syntax_Repetition.h"
#include "System_System_Text_RegularExpressions_Syntax_RepetitionMethodDeclarations.h"
#include "mscorlib_System_Int32.h"
#include "System_System_Text_RegularExpressions_Syntax_CompositeExpresMethodDeclarations.h"
#include "System_System_Text_RegularExpressions_Syntax_ExpressionColleMethodDeclarations.h"
#include "System_System_Text_RegularExpressions_Syntax_CompositeExpres.h"
#include "System_System_Text_RegularExpressions_Syntax_ExpressionColle.h"
#include "System_System_Text_RegularExpressions_Syntax_Expression.h"
#include "System_System_Text_RegularExpressions_Syntax_ExpressionMethodDeclarations.h"
#include "System_System_Text_RegularExpressions_Syntax_AnchorInfo.h"
#include "System_System_Text_RegularExpressions_Syntax_AnchorInfoMethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilderMethodDeclarations.h"
#include "mscorlib_System_String.h"
#include "mscorlib_System_Text_StringBuilder.h"
#include "System_System_Text_RegularExpressions_Position.h"
#include "System_System_Text_RegularExpressions_Syntax_Assertion.h"
#include "System_System_Text_RegularExpressions_Syntax_AssertionMethodDeclarations.h"
#include "System_System_Text_RegularExpressions_Syntax_CaptureAssertio.h"
#include "System_System_Text_RegularExpressions_Syntax_CaptureAssertioMethodDeclarations.h"
#include "System_System_Text_RegularExpressions_Syntax_Literal.h"
#include "System_System_Text_RegularExpressions_Syntax_CapturingGroup.h"
#include "System_System_Text_RegularExpressions_Syntax_CapturingGroupMethodDeclarations.h"
#include "System_System_Text_RegularExpressions_Syntax_ExpressionAsser.h"
#include "System_System_Text_RegularExpressions_Syntax_ExpressionAsserMethodDeclarations.h"
#include "System_System_Text_RegularExpressions_Syntax_Alternation.h"
#include "System_System_Text_RegularExpressions_Syntax_AlternationMethodDeclarations.h"
#include "mscorlib_System_Collections_CollectionBase.h"
#include "mscorlib_System_Collections_CollectionBaseMethodDeclarations.h"
#include "mscorlib_System_Object.h"
#include "System_System_Text_RegularExpressions_Syntax_LiteralMethodDeclarations.h"
#include "mscorlib_System_StringMethodDeclarations.h"
#include "mscorlib_System_Char.h"
#include "System_System_Text_RegularExpressions_Syntax_PositionAsserti.h"
#include "System_System_Text_RegularExpressions_Syntax_PositionAssertiMethodDeclarations.h"
#include "System_System_Text_RegularExpressions_Syntax_Reference.h"
#include "System_System_Text_RegularExpressions_Syntax_ReferenceMethodDeclarations.h"
#include "System_System_Text_RegularExpressions_Syntax_BackslashNumber.h"
#include "System_System_Text_RegularExpressions_Syntax_BackslashNumberMethodDeclarations.h"
#include "mscorlib_System_Collections_Hashtable.h"
#include "System_System_Text_RegularExpressions_Syntax_ParserMethodDeclarations.h"
#include "mscorlib_System_Collections_HashtableMethodDeclarations.h"
#include "System_System_Text_RegularExpressions_Syntax_CharacterClass.h"
#include "System_System_Text_RegularExpressions_Syntax_CharacterClassMethodDeclarations.h"
#include "System_System_Text_RegularExpressions_IntervalCollectionMethodDeclarations.h"
#include "mscorlib_System_Collections_BitArrayMethodDeclarations.h"
#include "System_System_Text_RegularExpressions_IntervalCollection.h"
#include "mscorlib_System_Collections_BitArray.h"
#include "System_System_Text_RegularExpressions_Category.h"
#include "System_System_Text_RegularExpressions_IntervalMethodDeclarations.h"
#include "System_System_Text_RegularExpressions_Interval.h"
#include "System_System_Text_RegularExpressions_IntervalCollection_CosMethodDeclarations.h"
#include "mscorlib_System_Double.h"
#include "System_System_Text_RegularExpressions_IntervalCollection_Cos.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_ObjectMethodDeclarations.h"
#include "System_System_DefaultUriParser.h"
#include "System_System_DefaultUriParserMethodDeclarations.h"
#include "System_System_UriParserMethodDeclarations.h"
#include "System_System_UriParser.h"
#include "System_System_GenericUriParser.h"
#include "System_System_GenericUriParserMethodDeclarations.h"
#include "System_System_Uri_UriScheme.h"
#include "System_System_Uri_UriSchemeMethodDeclarations.h"
#include "System_System_Uri.h"
#include "System_System_UriMethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Runtime_Serialization_SerializationInfo.h"
#include "mscorlib_System_Runtime_Serialization_SerializationInfoMethodDeclarations.h"
#include "System_System_UriKind.h"
#include "System_System_UriFormatExceptionMethodDeclarations.h"
#include "System_LocaleMethodDeclarations.h"
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
#include "System_System_UriFormatException.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_ArgumentException.h"
#include "System_ArrayTypes.h"
#include "mscorlib_System_ArgumentNullExceptionMethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeExceptionMethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException.h"
#include "mscorlib_System_ArgumentOutOfRangeException.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_genMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen.h"
#include "System_System_UriPartial.h"
#include "System_System_UriHostNameType.h"
#include "System_System_Net_IPAddressMethodDeclarations.h"
#include "System_System_Net_IPv6AddressMethodDeclarations.h"
#include "System_System_Net_IPAddress.h"
#include "System_System_Net_IPv6Address.h"
#include "mscorlib_System_CharMethodDeclarations.h"
#include "mscorlib_System_IO_Path.h"
#include "mscorlib_System_IO_PathMethodDeclarations.h"
#include "mscorlib_System_ArrayMethodDeclarations.h"
#include "mscorlib_System_UInt32MethodDeclarations.h"
#include "mscorlib_System_UInt32.h"
#include "mscorlib_System_Globalization_CultureInfoMethodDeclarations.h"
#include "mscorlib_System_Globalization_CultureInfo.h"
#include "mscorlib_System_MathMethodDeclarations.h"
#include "mscorlib_System_Text_EncodingMethodDeclarations.h"
#include "mscorlib_System_Byte.h"
#include "mscorlib_System_Text_Encoding.h"
#include "mscorlib_System_Int32MethodDeclarations.h"
#include "mscorlib_System_Globalization_NumberStyles.h"
#include "mscorlib_System_Exception.h"
#include "mscorlib_System_ExceptionMethodDeclarations.h"
#include "mscorlib_System_Collections_ArrayListMethodDeclarations.h"
#include "mscorlib_System_Collections_ArrayList.h"
#include "mscorlib_System_IO_MemoryStreamMethodDeclarations.h"
#include "mscorlib_System_Int64.h"
#include "mscorlib_System_IO_MemoryStream.h"
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException.h"
#include "mscorlib_System_FormatExceptionMethodDeclarations.h"
#include "mscorlib_System_FormatException.h"
#include "System_System_UriHostNameTypeMethodDeclarations.h"
#include "System_System_UriKindMethodDeclarations.h"
#include "System_System_Text_RegularExpressions_RegexMethodDeclarations.h"
#include "System_System_Text_RegularExpressions_Regex.h"
#include "mscorlib_System_Threading_MonitorMethodDeclarations.h"
#include "System_System_UriPartialMethodDeclarations.h"
#include "System_System_UriTypeConverter.h"
#include "System_System_UriTypeConverterMethodDeclarations.h"
#include "System_System_Net_Security_LocalCertificateSelectionCallback.h"
#include "System_System_Net_Security_LocalCertificateSelectionCallbackMethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_X509Certificates_X509C_0.h"
#include "System_System_Security_Cryptography_X509Certificates_X509Cer.h"
#include "mscorlib_System_AsyncCallback.h"
#include "System_System_Net_Security_RemoteCertificateValidationCallba.h"
#include "System_System_Net_Security_RemoteCertificateValidationCallbaMethodDeclarations.h"
#include "System_System_Net_Security_SslPolicyErrors.h"
#include "System_System_Security_Cryptography_X509Certificates_X509Cha.h"
#include "System_System_Net_BindIPEndPoint.h"
#include "System_System_Net_BindIPEndPointMethodDeclarations.h"
#include "System_System_Net_IPEndPoint.h"
#include "System_System_Net_ServicePoint.h"
#include "System_System_Net_HttpContinueDelegate.h"
#include "System_System_Net_HttpContinueDelegateMethodDeclarations.h"
#include "System_System_Net_WebHeaderCollection.h"
#include "System_System_Text_RegularExpressions_MatchEvaluator.h"
#include "System_System_Text_RegularExpressions_MatchEvaluatorMethodDeclarations.h"
#include "System_System_Text_RegularExpressions_Match.h"
#include "System_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU2416.h"
#include "System_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU2416MethodDeclarations.h"
#include "System_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU24128.h"
#include "System_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU24128MethodDeclarations.h"
#include "System_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU2412.h"
#include "System_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU2412MethodDeclarations.h"
#include "System_U3CPrivateImplementationDetailsU3E.h"
#include "System_U3CPrivateImplementationDetailsU3EMethodDeclarations.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Text.RegularExpressions.Syntax.NonBacktrackingGroup::.ctor()
extern "C" void NonBacktrackingGroup__ctor_m3_1637 (NonBacktrackingGroup_t3_219 * __this, const MethodInfo* method)
{
	{
		Group__ctor_m3_1617(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Text.RegularExpressions.Syntax.NonBacktrackingGroup::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
extern TypeInfo* ICompiler_t3_246_il2cpp_TypeInfo_var;
extern "C" void NonBacktrackingGroup_Compile_m3_1638 (NonBacktrackingGroup_t3_219 * __this, Object_t * ___cmp, bool ___reverse, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ICompiler_t3_246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1515);
		s_Il2CppMethodIntialized = true;
	}
	LinkRef_t3_193 * V_0 = {0};
	{
		Object_t * L_0 = ___cmp;
		NullCheck(L_0);
		LinkRef_t3_193 * L_1 = (LinkRef_t3_193 *)InterfaceFuncInvoker0< LinkRef_t3_193 * >::Invoke(28 /* System.Text.RegularExpressions.LinkRef System.Text.RegularExpressions.ICompiler::NewLink() */, ICompiler_t3_246_il2cpp_TypeInfo_var, L_0);
		V_0 = L_1;
		Object_t * L_2 = ___cmp;
		LinkRef_t3_193 * L_3 = V_0;
		NullCheck(L_2);
		InterfaceActionInvoker1< LinkRef_t3_193 * >::Invoke(16 /* System.Void System.Text.RegularExpressions.ICompiler::EmitSub(System.Text.RegularExpressions.LinkRef) */, ICompiler_t3_246_il2cpp_TypeInfo_var, L_2, L_3);
		Object_t * L_4 = ___cmp;
		bool L_5 = ___reverse;
		Group_Compile_m3_1619(__this, L_4, L_5, /*hidden argument*/NULL);
		Object_t * L_6 = ___cmp;
		NullCheck(L_6);
		InterfaceActionInvoker0::Invoke(2 /* System.Void System.Text.RegularExpressions.ICompiler::EmitTrue() */, ICompiler_t3_246_il2cpp_TypeInfo_var, L_6);
		Object_t * L_7 = ___cmp;
		LinkRef_t3_193 * L_8 = V_0;
		NullCheck(L_7);
		InterfaceActionInvoker1< LinkRef_t3_193 * >::Invoke(29 /* System.Void System.Text.RegularExpressions.ICompiler::ResolveLink(System.Text.RegularExpressions.LinkRef) */, ICompiler_t3_246_il2cpp_TypeInfo_var, L_7, L_8);
		return;
	}
}
// System.Boolean System.Text.RegularExpressions.Syntax.NonBacktrackingGroup::IsComplex()
extern "C" bool NonBacktrackingGroup_IsComplex_m3_1639 (NonBacktrackingGroup_t3_219 * __this, const MethodInfo* method)
{
	{
		return 1;
	}
}
// System.Void System.Text.RegularExpressions.Syntax.Repetition::.ctor(System.Int32,System.Int32,System.Boolean)
extern "C" void Repetition__ctor_m3_1640 (Repetition_t3_220 * __this, int32_t ___min, int32_t ___max, bool ___lazy, const MethodInfo* method)
{
	{
		CompositeExpression__ctor_m3_1613(__this, /*hidden argument*/NULL);
		ExpressionCollection_t3_212 * L_0 = CompositeExpression_get_Expressions_m3_1614(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		ExpressionCollection_Add_m3_1606(L_0, (Expression_t3_213 *)NULL, /*hidden argument*/NULL);
		int32_t L_1 = ___min;
		__this->___min_1 = L_1;
		int32_t L_2 = ___max;
		__this->___max_2 = L_2;
		bool L_3 = ___lazy;
		__this->___lazy_3 = L_3;
		return;
	}
}
// System.Text.RegularExpressions.Syntax.Expression System.Text.RegularExpressions.Syntax.Repetition::get_Expression()
extern "C" Expression_t3_213 * Repetition_get_Expression_m3_1641 (Repetition_t3_220 * __this, const MethodInfo* method)
{
	{
		ExpressionCollection_t3_212 * L_0 = CompositeExpression_get_Expressions_m3_1614(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Expression_t3_213 * L_1 = ExpressionCollection_get_Item_m3_1607(L_0, 0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void System.Text.RegularExpressions.Syntax.Repetition::set_Expression(System.Text.RegularExpressions.Syntax.Expression)
extern "C" void Repetition_set_Expression_m3_1642 (Repetition_t3_220 * __this, Expression_t3_213 * ___value, const MethodInfo* method)
{
	{
		ExpressionCollection_t3_212 * L_0 = CompositeExpression_get_Expressions_m3_1614(__this, /*hidden argument*/NULL);
		Expression_t3_213 * L_1 = ___value;
		NullCheck(L_0);
		ExpressionCollection_set_Item_m3_1608(L_0, 0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 System.Text.RegularExpressions.Syntax.Repetition::get_Minimum()
extern "C" int32_t Repetition_get_Minimum_m3_1643 (Repetition_t3_220 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___min_1);
		return L_0;
	}
}
// System.Void System.Text.RegularExpressions.Syntax.Repetition::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
extern TypeInfo* ICompiler_t3_246_il2cpp_TypeInfo_var;
extern "C" void Repetition_Compile_m3_1644 (Repetition_t3_220 * __this, Object_t * ___cmp, bool ___reverse, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ICompiler_t3_246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1515);
		s_Il2CppMethodIntialized = true;
	}
	LinkRef_t3_193 * V_0 = {0};
	LinkRef_t3_193 * V_1 = {0};
	{
		Expression_t3_213 * L_0 = Repetition_get_Expression_m3_1641(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Text.RegularExpressions.Syntax.Expression::IsComplex() */, L_0);
		if (!L_1)
		{
			goto IL_0049;
		}
	}
	{
		Object_t * L_2 = ___cmp;
		NullCheck(L_2);
		LinkRef_t3_193 * L_3 = (LinkRef_t3_193 *)InterfaceFuncInvoker0< LinkRef_t3_193 * >::Invoke(28 /* System.Text.RegularExpressions.LinkRef System.Text.RegularExpressions.ICompiler::NewLink() */, ICompiler_t3_246_il2cpp_TypeInfo_var, L_2);
		V_0 = L_3;
		Object_t * L_4 = ___cmp;
		int32_t L_5 = (__this->___min_1);
		int32_t L_6 = (__this->___max_2);
		bool L_7 = (__this->___lazy_3);
		LinkRef_t3_193 * L_8 = V_0;
		NullCheck(L_4);
		InterfaceActionInvoker4< int32_t, int32_t, bool, LinkRef_t3_193 * >::Invoke(20 /* System.Void System.Text.RegularExpressions.ICompiler::EmitRepeat(System.Int32,System.Int32,System.Boolean,System.Text.RegularExpressions.LinkRef) */, ICompiler_t3_246_il2cpp_TypeInfo_var, L_4, L_5, L_6, L_7, L_8);
		Expression_t3_213 * L_9 = Repetition_get_Expression_m3_1641(__this, /*hidden argument*/NULL);
		Object_t * L_10 = ___cmp;
		bool L_11 = ___reverse;
		NullCheck(L_9);
		VirtActionInvoker2< Object_t *, bool >::Invoke(4 /* System.Void System.Text.RegularExpressions.Syntax.Expression::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean) */, L_9, L_10, L_11);
		Object_t * L_12 = ___cmp;
		LinkRef_t3_193 * L_13 = V_0;
		NullCheck(L_12);
		InterfaceActionInvoker1< LinkRef_t3_193 * >::Invoke(21 /* System.Void System.Text.RegularExpressions.ICompiler::EmitUntil(System.Text.RegularExpressions.LinkRef) */, ICompiler_t3_246_il2cpp_TypeInfo_var, L_12, L_13);
		goto IL_0083;
	}

IL_0049:
	{
		Object_t * L_14 = ___cmp;
		NullCheck(L_14);
		LinkRef_t3_193 * L_15 = (LinkRef_t3_193 *)InterfaceFuncInvoker0< LinkRef_t3_193 * >::Invoke(28 /* System.Text.RegularExpressions.LinkRef System.Text.RegularExpressions.ICompiler::NewLink() */, ICompiler_t3_246_il2cpp_TypeInfo_var, L_14);
		V_1 = L_15;
		Object_t * L_16 = ___cmp;
		int32_t L_17 = (__this->___min_1);
		int32_t L_18 = (__this->___max_2);
		bool L_19 = (__this->___lazy_3);
		LinkRef_t3_193 * L_20 = V_1;
		NullCheck(L_16);
		InterfaceActionInvoker4< int32_t, int32_t, bool, LinkRef_t3_193 * >::Invoke(24 /* System.Void System.Text.RegularExpressions.ICompiler::EmitFastRepeat(System.Int32,System.Int32,System.Boolean,System.Text.RegularExpressions.LinkRef) */, ICompiler_t3_246_il2cpp_TypeInfo_var, L_16, L_17, L_18, L_19, L_20);
		Expression_t3_213 * L_21 = Repetition_get_Expression_m3_1641(__this, /*hidden argument*/NULL);
		Object_t * L_22 = ___cmp;
		bool L_23 = ___reverse;
		NullCheck(L_21);
		VirtActionInvoker2< Object_t *, bool >::Invoke(4 /* System.Void System.Text.RegularExpressions.Syntax.Expression::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean) */, L_21, L_22, L_23);
		Object_t * L_24 = ___cmp;
		NullCheck(L_24);
		InterfaceActionInvoker0::Invoke(2 /* System.Void System.Text.RegularExpressions.ICompiler::EmitTrue() */, ICompiler_t3_246_il2cpp_TypeInfo_var, L_24);
		Object_t * L_25 = ___cmp;
		LinkRef_t3_193 * L_26 = V_1;
		NullCheck(L_25);
		InterfaceActionInvoker1< LinkRef_t3_193 * >::Invoke(29 /* System.Void System.Text.RegularExpressions.ICompiler::ResolveLink(System.Text.RegularExpressions.LinkRef) */, ICompiler_t3_246_il2cpp_TypeInfo_var, L_25, L_26);
	}

IL_0083:
	{
		return;
	}
}
// System.Void System.Text.RegularExpressions.Syntax.Repetition::GetWidth(System.Int32&,System.Int32&)
extern "C" void Repetition_GetWidth_m3_1645 (Repetition_t3_220 * __this, int32_t* ___min, int32_t* ___max, const MethodInfo* method)
{
	{
		Expression_t3_213 * L_0 = Repetition_get_Expression_m3_1641(__this, /*hidden argument*/NULL);
		int32_t* L_1 = ___min;
		int32_t* L_2 = ___max;
		NullCheck(L_0);
		VirtActionInvoker2< int32_t*, int32_t* >::Invoke(5 /* System.Void System.Text.RegularExpressions.Syntax.Expression::GetWidth(System.Int32&,System.Int32&) */, L_0, L_1, L_2);
		int32_t* L_3 = ___min;
		int32_t* L_4 = ___min;
		int32_t L_5 = (__this->___min_1);
		*((int32_t*)(L_3)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_4))*(int32_t)L_5));
		int32_t* L_6 = ___max;
		if ((((int32_t)(*((int32_t*)L_6))) == ((int32_t)((int32_t)2147483647))))
		{
			goto IL_0034;
		}
	}
	{
		int32_t L_7 = (__this->___max_2);
		if ((!(((uint32_t)L_7) == ((uint32_t)((int32_t)65535)))))
		{
			goto IL_0040;
		}
	}

IL_0034:
	{
		int32_t* L_8 = ___max;
		*((int32_t*)(L_8)) = (int32_t)((int32_t)2147483647);
		goto IL_004b;
	}

IL_0040:
	{
		int32_t* L_9 = ___max;
		int32_t* L_10 = ___max;
		int32_t L_11 = (__this->___max_2);
		*((int32_t*)(L_9)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_10))*(int32_t)L_11));
	}

IL_004b:
	{
		return;
	}
}
// System.Text.RegularExpressions.Syntax.AnchorInfo System.Text.RegularExpressions.Syntax.Repetition::GetAnchorInfo(System.Boolean)
extern TypeInfo* AnchorInfo_t3_230_il2cpp_TypeInfo_var;
extern TypeInfo* StringBuilder_t1_247_il2cpp_TypeInfo_var;
extern "C" AnchorInfo_t3_230 * Repetition_GetAnchorInfo_m3_1646 (Repetition_t3_220 * __this, bool ___reverse, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AnchorInfo_t3_230_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1548);
		StringBuilder_t1_247_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	AnchorInfo_t3_230 * V_1 = {0};
	String_t* V_2 = {0};
	StringBuilder_t1_247 * V_3 = {0};
	int32_t V_4 = 0;
	{
		int32_t L_0 = Expression_GetFixedWidth_m3_1611(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Repetition_get_Minimum_m3_1643(__this, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_2 = V_0;
		AnchorInfo_t3_230 * L_3 = (AnchorInfo_t3_230 *)il2cpp_codegen_object_new (AnchorInfo_t3_230_il2cpp_TypeInfo_var);
		AnchorInfo__ctor_m3_1701(L_3, __this, L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_001a:
	{
		Expression_t3_213 * L_4 = Repetition_get_Expression_m3_1641(__this, /*hidden argument*/NULL);
		bool L_5 = ___reverse;
		NullCheck(L_4);
		AnchorInfo_t3_230 * L_6 = (AnchorInfo_t3_230 *)VirtFuncInvoker1< AnchorInfo_t3_230 *, bool >::Invoke(6 /* System.Text.RegularExpressions.Syntax.AnchorInfo System.Text.RegularExpressions.Syntax.Expression::GetAnchorInfo(System.Boolean) */, L_4, L_5);
		V_1 = L_6;
		AnchorInfo_t3_230 * L_7 = V_1;
		NullCheck(L_7);
		bool L_8 = AnchorInfo_get_IsPosition_m3_1713(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0046;
		}
	}
	{
		AnchorInfo_t3_230 * L_9 = V_1;
		NullCheck(L_9);
		int32_t L_10 = AnchorInfo_get_Offset_m3_1704(L_9, /*hidden argument*/NULL);
		int32_t L_11 = V_0;
		AnchorInfo_t3_230 * L_12 = V_1;
		NullCheck(L_12);
		uint16_t L_13 = AnchorInfo_get_Position_m3_1711(L_12, /*hidden argument*/NULL);
		AnchorInfo_t3_230 * L_14 = (AnchorInfo_t3_230 *)il2cpp_codegen_object_new (AnchorInfo_t3_230_il2cpp_TypeInfo_var);
		AnchorInfo__ctor_m3_1703(L_14, __this, L_10, L_11, L_13, /*hidden argument*/NULL);
		return L_14;
	}

IL_0046:
	{
		AnchorInfo_t3_230 * L_15 = V_1;
		NullCheck(L_15);
		bool L_16 = AnchorInfo_get_IsSubstring_m3_1712(L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00bc;
		}
	}
	{
		AnchorInfo_t3_230 * L_17 = V_1;
		NullCheck(L_17);
		bool L_18 = AnchorInfo_get_IsComplete_m3_1708(L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00a2;
		}
	}
	{
		AnchorInfo_t3_230 * L_19 = V_1;
		NullCheck(L_19);
		String_t* L_20 = AnchorInfo_get_Substring_m3_1709(L_19, /*hidden argument*/NULL);
		V_2 = L_20;
		String_t* L_21 = V_2;
		StringBuilder_t1_247 * L_22 = (StringBuilder_t1_247 *)il2cpp_codegen_object_new (StringBuilder_t1_247_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1_12417(L_22, L_21, /*hidden argument*/NULL);
		V_3 = L_22;
		V_4 = 1;
		goto IL_0080;
	}

IL_0072:
	{
		StringBuilder_t1_247 * L_23 = V_3;
		String_t* L_24 = V_2;
		NullCheck(L_23);
		StringBuilder_Append_m1_12438(L_23, L_24, /*hidden argument*/NULL);
		int32_t L_25 = V_4;
		V_4 = ((int32_t)((int32_t)L_25+(int32_t)1));
	}

IL_0080:
	{
		int32_t L_26 = V_4;
		int32_t L_27 = Repetition_get_Minimum_m3_1643(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_26) < ((int32_t)L_27)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_28 = V_0;
		StringBuilder_t1_247 * L_29 = V_3;
		NullCheck(L_29);
		String_t* L_30 = StringBuilder_ToString_m1_12428(L_29, /*hidden argument*/NULL);
		AnchorInfo_t3_230 * L_31 = V_1;
		NullCheck(L_31);
		bool L_32 = AnchorInfo_get_IgnoreCase_m3_1710(L_31, /*hidden argument*/NULL);
		AnchorInfo_t3_230 * L_33 = (AnchorInfo_t3_230 *)il2cpp_codegen_object_new (AnchorInfo_t3_230_il2cpp_TypeInfo_var);
		AnchorInfo__ctor_m3_1702(L_33, __this, 0, L_28, L_30, L_32, /*hidden argument*/NULL);
		return L_33;
	}

IL_00a2:
	{
		AnchorInfo_t3_230 * L_34 = V_1;
		NullCheck(L_34);
		int32_t L_35 = AnchorInfo_get_Offset_m3_1704(L_34, /*hidden argument*/NULL);
		int32_t L_36 = V_0;
		AnchorInfo_t3_230 * L_37 = V_1;
		NullCheck(L_37);
		String_t* L_38 = AnchorInfo_get_Substring_m3_1709(L_37, /*hidden argument*/NULL);
		AnchorInfo_t3_230 * L_39 = V_1;
		NullCheck(L_39);
		bool L_40 = AnchorInfo_get_IgnoreCase_m3_1710(L_39, /*hidden argument*/NULL);
		AnchorInfo_t3_230 * L_41 = (AnchorInfo_t3_230 *)il2cpp_codegen_object_new (AnchorInfo_t3_230_il2cpp_TypeInfo_var);
		AnchorInfo__ctor_m3_1702(L_41, __this, L_35, L_36, L_38, L_40, /*hidden argument*/NULL);
		return L_41;
	}

IL_00bc:
	{
		int32_t L_42 = V_0;
		AnchorInfo_t3_230 * L_43 = (AnchorInfo_t3_230 *)il2cpp_codegen_object_new (AnchorInfo_t3_230_il2cpp_TypeInfo_var);
		AnchorInfo__ctor_m3_1701(L_43, __this, L_42, /*hidden argument*/NULL);
		return L_43;
	}
}
// System.Void System.Text.RegularExpressions.Syntax.Assertion::.ctor()
extern "C" void Assertion__ctor_m3_1647 (Assertion_t3_221 * __this, const MethodInfo* method)
{
	{
		CompositeExpression__ctor_m3_1613(__this, /*hidden argument*/NULL);
		ExpressionCollection_t3_212 * L_0 = CompositeExpression_get_Expressions_m3_1614(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		ExpressionCollection_Add_m3_1606(L_0, (Expression_t3_213 *)NULL, /*hidden argument*/NULL);
		ExpressionCollection_t3_212 * L_1 = CompositeExpression_get_Expressions_m3_1614(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		ExpressionCollection_Add_m3_1606(L_1, (Expression_t3_213 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Text.RegularExpressions.Syntax.Expression System.Text.RegularExpressions.Syntax.Assertion::get_TrueExpression()
extern "C" Expression_t3_213 * Assertion_get_TrueExpression_m3_1648 (Assertion_t3_221 * __this, const MethodInfo* method)
{
	{
		ExpressionCollection_t3_212 * L_0 = CompositeExpression_get_Expressions_m3_1614(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Expression_t3_213 * L_1 = ExpressionCollection_get_Item_m3_1607(L_0, 0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void System.Text.RegularExpressions.Syntax.Assertion::set_TrueExpression(System.Text.RegularExpressions.Syntax.Expression)
extern "C" void Assertion_set_TrueExpression_m3_1649 (Assertion_t3_221 * __this, Expression_t3_213 * ___value, const MethodInfo* method)
{
	{
		ExpressionCollection_t3_212 * L_0 = CompositeExpression_get_Expressions_m3_1614(__this, /*hidden argument*/NULL);
		Expression_t3_213 * L_1 = ___value;
		NullCheck(L_0);
		ExpressionCollection_set_Item_m3_1608(L_0, 0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Text.RegularExpressions.Syntax.Expression System.Text.RegularExpressions.Syntax.Assertion::get_FalseExpression()
extern "C" Expression_t3_213 * Assertion_get_FalseExpression_m3_1650 (Assertion_t3_221 * __this, const MethodInfo* method)
{
	{
		ExpressionCollection_t3_212 * L_0 = CompositeExpression_get_Expressions_m3_1614(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Expression_t3_213 * L_1 = ExpressionCollection_get_Item_m3_1607(L_0, 1, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void System.Text.RegularExpressions.Syntax.Assertion::set_FalseExpression(System.Text.RegularExpressions.Syntax.Expression)
extern "C" void Assertion_set_FalseExpression_m3_1651 (Assertion_t3_221 * __this, Expression_t3_213 * ___value, const MethodInfo* method)
{
	{
		ExpressionCollection_t3_212 * L_0 = CompositeExpression_get_Expressions_m3_1614(__this, /*hidden argument*/NULL);
		Expression_t3_213 * L_1 = ___value;
		NullCheck(L_0);
		ExpressionCollection_set_Item_m3_1608(L_0, 1, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Text.RegularExpressions.Syntax.Assertion::GetWidth(System.Int32&,System.Int32&)
extern "C" void Assertion_GetWidth_m3_1652 (Assertion_t3_221 * __this, int32_t* ___min, int32_t* ___max, const MethodInfo* method)
{
	{
		int32_t* L_0 = ___min;
		int32_t* L_1 = ___max;
		CompositeExpression_GetWidth_m3_1615(__this, L_0, L_1, 2, /*hidden argument*/NULL);
		Expression_t3_213 * L_2 = Assertion_get_TrueExpression_m3_1648(__this, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001f;
		}
	}
	{
		Expression_t3_213 * L_3 = Assertion_get_FalseExpression_m3_1650(__this, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0022;
		}
	}

IL_001f:
	{
		int32_t* L_4 = ___min;
		*((int32_t*)(L_4)) = (int32_t)0;
	}

IL_0022:
	{
		return;
	}
}
// System.Void System.Text.RegularExpressions.Syntax.CaptureAssertion::.ctor(System.Text.RegularExpressions.Syntax.Literal)
extern "C" void CaptureAssertion__ctor_m3_1653 (CaptureAssertion_t3_222 * __this, Literal_t3_224 * ___l, const MethodInfo* method)
{
	{
		Assertion__ctor_m3_1647(__this, /*hidden argument*/NULL);
		Literal_t3_224 * L_0 = ___l;
		__this->___literal_3 = L_0;
		return;
	}
}
// System.Void System.Text.RegularExpressions.Syntax.CaptureAssertion::set_CapturingGroup(System.Text.RegularExpressions.Syntax.CapturingGroup)
extern "C" void CaptureAssertion_set_CapturingGroup_m3_1654 (CaptureAssertion_t3_222 * __this, CapturingGroup_t3_217 * ___value, const MethodInfo* method)
{
	{
		CapturingGroup_t3_217 * L_0 = ___value;
		__this->___group_2 = L_0;
		return;
	}
}
// System.Void System.Text.RegularExpressions.Syntax.CaptureAssertion::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
extern TypeInfo* ICompiler_t3_246_il2cpp_TypeInfo_var;
extern "C" void CaptureAssertion_Compile_m3_1655 (CaptureAssertion_t3_222 * __this, Object_t * ___cmp, bool ___reverse, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ICompiler_t3_246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1515);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	LinkRef_t3_193 * V_1 = {0};
	LinkRef_t3_193 * V_2 = {0};
	{
		CapturingGroup_t3_217 * L_0 = (__this->___group_2);
		if (L_0)
		{
			goto IL_0019;
		}
	}
	{
		ExpressionAssertion_t3_223 * L_1 = CaptureAssertion_get_Alternate_m3_1657(__this, /*hidden argument*/NULL);
		Object_t * L_2 = ___cmp;
		bool L_3 = ___reverse;
		NullCheck(L_1);
		VirtActionInvoker2< Object_t *, bool >::Invoke(4 /* System.Void System.Text.RegularExpressions.Syntax.ExpressionAssertion::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean) */, L_1, L_2, L_3);
		return;
	}

IL_0019:
	{
		CapturingGroup_t3_217 * L_4 = (__this->___group_2);
		NullCheck(L_4);
		int32_t L_5 = CapturingGroup_get_Index_m3_1626(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		Object_t * L_6 = ___cmp;
		NullCheck(L_6);
		LinkRef_t3_193 * L_7 = (LinkRef_t3_193 *)InterfaceFuncInvoker0< LinkRef_t3_193 * >::Invoke(28 /* System.Text.RegularExpressions.LinkRef System.Text.RegularExpressions.ICompiler::NewLink() */, ICompiler_t3_246_il2cpp_TypeInfo_var, L_6);
		V_1 = L_7;
		Expression_t3_213 * L_8 = Assertion_get_FalseExpression_m3_1650(__this, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0051;
		}
	}
	{
		Object_t * L_9 = ___cmp;
		int32_t L_10 = V_0;
		LinkRef_t3_193 * L_11 = V_1;
		NullCheck(L_9);
		InterfaceActionInvoker2< int32_t, LinkRef_t3_193 * >::Invoke(15 /* System.Void System.Text.RegularExpressions.ICompiler::EmitIfDefined(System.Int32,System.Text.RegularExpressions.LinkRef) */, ICompiler_t3_246_il2cpp_TypeInfo_var, L_9, L_10, L_11);
		Expression_t3_213 * L_12 = Assertion_get_TrueExpression_m3_1648(__this, /*hidden argument*/NULL);
		Object_t * L_13 = ___cmp;
		bool L_14 = ___reverse;
		NullCheck(L_12);
		VirtActionInvoker2< Object_t *, bool >::Invoke(4 /* System.Void System.Text.RegularExpressions.Syntax.Expression::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean) */, L_12, L_13, L_14);
		goto IL_0088;
	}

IL_0051:
	{
		Object_t * L_15 = ___cmp;
		NullCheck(L_15);
		LinkRef_t3_193 * L_16 = (LinkRef_t3_193 *)InterfaceFuncInvoker0< LinkRef_t3_193 * >::Invoke(28 /* System.Text.RegularExpressions.LinkRef System.Text.RegularExpressions.ICompiler::NewLink() */, ICompiler_t3_246_il2cpp_TypeInfo_var, L_15);
		V_2 = L_16;
		Object_t * L_17 = ___cmp;
		int32_t L_18 = V_0;
		LinkRef_t3_193 * L_19 = V_2;
		NullCheck(L_17);
		InterfaceActionInvoker2< int32_t, LinkRef_t3_193 * >::Invoke(15 /* System.Void System.Text.RegularExpressions.ICompiler::EmitIfDefined(System.Int32,System.Text.RegularExpressions.LinkRef) */, ICompiler_t3_246_il2cpp_TypeInfo_var, L_17, L_18, L_19);
		Expression_t3_213 * L_20 = Assertion_get_TrueExpression_m3_1648(__this, /*hidden argument*/NULL);
		Object_t * L_21 = ___cmp;
		bool L_22 = ___reverse;
		NullCheck(L_20);
		VirtActionInvoker2< Object_t *, bool >::Invoke(4 /* System.Void System.Text.RegularExpressions.Syntax.Expression::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean) */, L_20, L_21, L_22);
		Object_t * L_23 = ___cmp;
		LinkRef_t3_193 * L_24 = V_1;
		NullCheck(L_23);
		InterfaceActionInvoker1< LinkRef_t3_193 * >::Invoke(19 /* System.Void System.Text.RegularExpressions.ICompiler::EmitJump(System.Text.RegularExpressions.LinkRef) */, ICompiler_t3_246_il2cpp_TypeInfo_var, L_23, L_24);
		Object_t * L_25 = ___cmp;
		LinkRef_t3_193 * L_26 = V_2;
		NullCheck(L_25);
		InterfaceActionInvoker1< LinkRef_t3_193 * >::Invoke(29 /* System.Void System.Text.RegularExpressions.ICompiler::ResolveLink(System.Text.RegularExpressions.LinkRef) */, ICompiler_t3_246_il2cpp_TypeInfo_var, L_25, L_26);
		Expression_t3_213 * L_27 = Assertion_get_FalseExpression_m3_1650(__this, /*hidden argument*/NULL);
		Object_t * L_28 = ___cmp;
		bool L_29 = ___reverse;
		NullCheck(L_27);
		VirtActionInvoker2< Object_t *, bool >::Invoke(4 /* System.Void System.Text.RegularExpressions.Syntax.Expression::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean) */, L_27, L_28, L_29);
	}

IL_0088:
	{
		Object_t * L_30 = ___cmp;
		LinkRef_t3_193 * L_31 = V_1;
		NullCheck(L_30);
		InterfaceActionInvoker1< LinkRef_t3_193 * >::Invoke(29 /* System.Void System.Text.RegularExpressions.ICompiler::ResolveLink(System.Text.RegularExpressions.LinkRef) */, ICompiler_t3_246_il2cpp_TypeInfo_var, L_30, L_31);
		return;
	}
}
// System.Boolean System.Text.RegularExpressions.Syntax.CaptureAssertion::IsComplex()
extern "C" bool CaptureAssertion_IsComplex_m3_1656 (CaptureAssertion_t3_222 * __this, const MethodInfo* method)
{
	{
		CapturingGroup_t3_217 * L_0 = (__this->___group_2);
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ExpressionAssertion_t3_223 * L_1 = CaptureAssertion_get_Alternate_m3_1657(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_2 = (bool)VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Text.RegularExpressions.Syntax.ExpressionAssertion::IsComplex() */, L_1);
		return L_2;
	}

IL_0017:
	{
		Expression_t3_213 * L_3 = Assertion_get_TrueExpression_m3_1648(__this, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0034;
		}
	}
	{
		Expression_t3_213 * L_4 = Assertion_get_TrueExpression_m3_1648(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		bool L_5 = (bool)VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Text.RegularExpressions.Syntax.Expression::IsComplex() */, L_4);
		if (!L_5)
		{
			goto IL_0034;
		}
	}
	{
		return 1;
	}

IL_0034:
	{
		Expression_t3_213 * L_6 = Assertion_get_FalseExpression_m3_1650(__this, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0051;
		}
	}
	{
		Expression_t3_213 * L_7 = Assertion_get_FalseExpression_m3_1650(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		bool L_8 = (bool)VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Text.RegularExpressions.Syntax.Expression::IsComplex() */, L_7);
		if (!L_8)
		{
			goto IL_0051;
		}
	}
	{
		return 1;
	}

IL_0051:
	{
		int32_t L_9 = Expression_GetFixedWidth_m3_1611(__this, /*hidden argument*/NULL);
		return ((((int32_t)((((int32_t)L_9) > ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Text.RegularExpressions.Syntax.ExpressionAssertion System.Text.RegularExpressions.Syntax.CaptureAssertion::get_Alternate()
extern TypeInfo* ExpressionAssertion_t3_223_il2cpp_TypeInfo_var;
extern "C" ExpressionAssertion_t3_223 * CaptureAssertion_get_Alternate_m3_1657 (CaptureAssertion_t3_222 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExpressionAssertion_t3_223_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1542);
		s_Il2CppMethodIntialized = true;
	}
	{
		ExpressionAssertion_t3_223 * L_0 = (__this->___alternate_1);
		if (L_0)
		{
			goto IL_0049;
		}
	}
	{
		ExpressionAssertion_t3_223 * L_1 = (ExpressionAssertion_t3_223 *)il2cpp_codegen_object_new (ExpressionAssertion_t3_223_il2cpp_TypeInfo_var);
		ExpressionAssertion__ctor_m3_1658(L_1, /*hidden argument*/NULL);
		__this->___alternate_1 = L_1;
		ExpressionAssertion_t3_223 * L_2 = (__this->___alternate_1);
		Expression_t3_213 * L_3 = Assertion_get_TrueExpression_m3_1648(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Assertion_set_TrueExpression_m3_1649(L_2, L_3, /*hidden argument*/NULL);
		ExpressionAssertion_t3_223 * L_4 = (__this->___alternate_1);
		Expression_t3_213 * L_5 = Assertion_get_FalseExpression_m3_1650(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Assertion_set_FalseExpression_m3_1651(L_4, L_5, /*hidden argument*/NULL);
		ExpressionAssertion_t3_223 * L_6 = (__this->___alternate_1);
		Literal_t3_224 * L_7 = (__this->___literal_3);
		NullCheck(L_6);
		ExpressionAssertion_set_TestExpression_m3_1662(L_6, L_7, /*hidden argument*/NULL);
	}

IL_0049:
	{
		ExpressionAssertion_t3_223 * L_8 = (__this->___alternate_1);
		return L_8;
	}
}
// System.Void System.Text.RegularExpressions.Syntax.ExpressionAssertion::.ctor()
extern "C" void ExpressionAssertion__ctor_m3_1658 (ExpressionAssertion_t3_223 * __this, const MethodInfo* method)
{
	{
		Assertion__ctor_m3_1647(__this, /*hidden argument*/NULL);
		ExpressionCollection_t3_212 * L_0 = CompositeExpression_get_Expressions_m3_1614(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		ExpressionCollection_Add_m3_1606(L_0, (Expression_t3_213 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Text.RegularExpressions.Syntax.ExpressionAssertion::set_Reverse(System.Boolean)
extern "C" void ExpressionAssertion_set_Reverse_m3_1659 (ExpressionAssertion_t3_223 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___reverse_1 = L_0;
		return;
	}
}
// System.Void System.Text.RegularExpressions.Syntax.ExpressionAssertion::set_Negate(System.Boolean)
extern "C" void ExpressionAssertion_set_Negate_m3_1660 (ExpressionAssertion_t3_223 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___negate_2 = L_0;
		return;
	}
}
// System.Text.RegularExpressions.Syntax.Expression System.Text.RegularExpressions.Syntax.ExpressionAssertion::get_TestExpression()
extern "C" Expression_t3_213 * ExpressionAssertion_get_TestExpression_m3_1661 (ExpressionAssertion_t3_223 * __this, const MethodInfo* method)
{
	{
		ExpressionCollection_t3_212 * L_0 = CompositeExpression_get_Expressions_m3_1614(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Expression_t3_213 * L_1 = ExpressionCollection_get_Item_m3_1607(L_0, 2, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void System.Text.RegularExpressions.Syntax.ExpressionAssertion::set_TestExpression(System.Text.RegularExpressions.Syntax.Expression)
extern "C" void ExpressionAssertion_set_TestExpression_m3_1662 (ExpressionAssertion_t3_223 * __this, Expression_t3_213 * ___value, const MethodInfo* method)
{
	{
		ExpressionCollection_t3_212 * L_0 = CompositeExpression_get_Expressions_m3_1614(__this, /*hidden argument*/NULL);
		Expression_t3_213 * L_1 = ___value;
		NullCheck(L_0);
		ExpressionCollection_set_Item_m3_1608(L_0, 2, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Text.RegularExpressions.Syntax.ExpressionAssertion::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
extern TypeInfo* ICompiler_t3_246_il2cpp_TypeInfo_var;
extern "C" void ExpressionAssertion_Compile_m3_1663 (ExpressionAssertion_t3_223 * __this, Object_t * ___cmp, bool ___reverse, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ICompiler_t3_246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1515);
		s_Il2CppMethodIntialized = true;
	}
	LinkRef_t3_193 * V_0 = {0};
	LinkRef_t3_193 * V_1 = {0};
	LinkRef_t3_193 * V_2 = {0};
	{
		Object_t * L_0 = ___cmp;
		NullCheck(L_0);
		LinkRef_t3_193 * L_1 = (LinkRef_t3_193 *)InterfaceFuncInvoker0< LinkRef_t3_193 * >::Invoke(28 /* System.Text.RegularExpressions.LinkRef System.Text.RegularExpressions.ICompiler::NewLink() */, ICompiler_t3_246_il2cpp_TypeInfo_var, L_0);
		V_0 = L_1;
		Object_t * L_2 = ___cmp;
		NullCheck(L_2);
		LinkRef_t3_193 * L_3 = (LinkRef_t3_193 *)InterfaceFuncInvoker0< LinkRef_t3_193 * >::Invoke(28 /* System.Text.RegularExpressions.LinkRef System.Text.RegularExpressions.ICompiler::NewLink() */, ICompiler_t3_246_il2cpp_TypeInfo_var, L_2);
		V_1 = L_3;
		bool L_4 = (__this->___negate_2);
		if (L_4)
		{
			goto IL_0026;
		}
	}
	{
		Object_t * L_5 = ___cmp;
		LinkRef_t3_193 * L_6 = V_0;
		LinkRef_t3_193 * L_7 = V_1;
		NullCheck(L_5);
		InterfaceActionInvoker2< LinkRef_t3_193 *, LinkRef_t3_193 * >::Invoke(17 /* System.Void System.Text.RegularExpressions.ICompiler::EmitTest(System.Text.RegularExpressions.LinkRef,System.Text.RegularExpressions.LinkRef) */, ICompiler_t3_246_il2cpp_TypeInfo_var, L_5, L_6, L_7);
		goto IL_002e;
	}

IL_0026:
	{
		Object_t * L_8 = ___cmp;
		LinkRef_t3_193 * L_9 = V_1;
		LinkRef_t3_193 * L_10 = V_0;
		NullCheck(L_8);
		InterfaceActionInvoker2< LinkRef_t3_193 *, LinkRef_t3_193 * >::Invoke(17 /* System.Void System.Text.RegularExpressions.ICompiler::EmitTest(System.Text.RegularExpressions.LinkRef,System.Text.RegularExpressions.LinkRef) */, ICompiler_t3_246_il2cpp_TypeInfo_var, L_8, L_9, L_10);
	}

IL_002e:
	{
		Expression_t3_213 * L_11 = ExpressionAssertion_get_TestExpression_m3_1661(__this, /*hidden argument*/NULL);
		Object_t * L_12 = ___cmp;
		bool L_13 = (__this->___reverse_1);
		NullCheck(L_11);
		VirtActionInvoker2< Object_t *, bool >::Invoke(4 /* System.Void System.Text.RegularExpressions.Syntax.Expression::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean) */, L_11, L_12, L_13);
		Object_t * L_14 = ___cmp;
		NullCheck(L_14);
		InterfaceActionInvoker0::Invoke(2 /* System.Void System.Text.RegularExpressions.ICompiler::EmitTrue() */, ICompiler_t3_246_il2cpp_TypeInfo_var, L_14);
		Expression_t3_213 * L_15 = Assertion_get_TrueExpression_m3_1648(__this, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_006a;
		}
	}
	{
		Object_t * L_16 = ___cmp;
		LinkRef_t3_193 * L_17 = V_1;
		NullCheck(L_16);
		InterfaceActionInvoker1< LinkRef_t3_193 * >::Invoke(29 /* System.Void System.Text.RegularExpressions.ICompiler::ResolveLink(System.Text.RegularExpressions.LinkRef) */, ICompiler_t3_246_il2cpp_TypeInfo_var, L_16, L_17);
		Object_t * L_18 = ___cmp;
		NullCheck(L_18);
		InterfaceActionInvoker0::Invoke(1 /* System.Void System.Text.RegularExpressions.ICompiler::EmitFalse() */, ICompiler_t3_246_il2cpp_TypeInfo_var, L_18);
		Object_t * L_19 = ___cmp;
		LinkRef_t3_193 * L_20 = V_0;
		NullCheck(L_19);
		InterfaceActionInvoker1< LinkRef_t3_193 * >::Invoke(29 /* System.Void System.Text.RegularExpressions.ICompiler::ResolveLink(System.Text.RegularExpressions.LinkRef) */, ICompiler_t3_246_il2cpp_TypeInfo_var, L_19, L_20);
		goto IL_00be;
	}

IL_006a:
	{
		Object_t * L_21 = ___cmp;
		LinkRef_t3_193 * L_22 = V_0;
		NullCheck(L_21);
		InterfaceActionInvoker1< LinkRef_t3_193 * >::Invoke(29 /* System.Void System.Text.RegularExpressions.ICompiler::ResolveLink(System.Text.RegularExpressions.LinkRef) */, ICompiler_t3_246_il2cpp_TypeInfo_var, L_21, L_22);
		Expression_t3_213 * L_23 = Assertion_get_TrueExpression_m3_1648(__this, /*hidden argument*/NULL);
		Object_t * L_24 = ___cmp;
		bool L_25 = ___reverse;
		NullCheck(L_23);
		VirtActionInvoker2< Object_t *, bool >::Invoke(4 /* System.Void System.Text.RegularExpressions.Syntax.Expression::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean) */, L_23, L_24, L_25);
		Expression_t3_213 * L_26 = Assertion_get_FalseExpression_m3_1650(__this, /*hidden argument*/NULL);
		if (L_26)
		{
			goto IL_0095;
		}
	}
	{
		Object_t * L_27 = ___cmp;
		LinkRef_t3_193 * L_28 = V_1;
		NullCheck(L_27);
		InterfaceActionInvoker1< LinkRef_t3_193 * >::Invoke(29 /* System.Void System.Text.RegularExpressions.ICompiler::ResolveLink(System.Text.RegularExpressions.LinkRef) */, ICompiler_t3_246_il2cpp_TypeInfo_var, L_27, L_28);
		goto IL_00be;
	}

IL_0095:
	{
		Object_t * L_29 = ___cmp;
		NullCheck(L_29);
		LinkRef_t3_193 * L_30 = (LinkRef_t3_193 *)InterfaceFuncInvoker0< LinkRef_t3_193 * >::Invoke(28 /* System.Text.RegularExpressions.LinkRef System.Text.RegularExpressions.ICompiler::NewLink() */, ICompiler_t3_246_il2cpp_TypeInfo_var, L_29);
		V_2 = L_30;
		Object_t * L_31 = ___cmp;
		LinkRef_t3_193 * L_32 = V_2;
		NullCheck(L_31);
		InterfaceActionInvoker1< LinkRef_t3_193 * >::Invoke(19 /* System.Void System.Text.RegularExpressions.ICompiler::EmitJump(System.Text.RegularExpressions.LinkRef) */, ICompiler_t3_246_il2cpp_TypeInfo_var, L_31, L_32);
		Object_t * L_33 = ___cmp;
		LinkRef_t3_193 * L_34 = V_1;
		NullCheck(L_33);
		InterfaceActionInvoker1< LinkRef_t3_193 * >::Invoke(29 /* System.Void System.Text.RegularExpressions.ICompiler::ResolveLink(System.Text.RegularExpressions.LinkRef) */, ICompiler_t3_246_il2cpp_TypeInfo_var, L_33, L_34);
		Expression_t3_213 * L_35 = Assertion_get_FalseExpression_m3_1650(__this, /*hidden argument*/NULL);
		Object_t * L_36 = ___cmp;
		bool L_37 = ___reverse;
		NullCheck(L_35);
		VirtActionInvoker2< Object_t *, bool >::Invoke(4 /* System.Void System.Text.RegularExpressions.Syntax.Expression::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean) */, L_35, L_36, L_37);
		Object_t * L_38 = ___cmp;
		LinkRef_t3_193 * L_39 = V_2;
		NullCheck(L_38);
		InterfaceActionInvoker1< LinkRef_t3_193 * >::Invoke(29 /* System.Void System.Text.RegularExpressions.ICompiler::ResolveLink(System.Text.RegularExpressions.LinkRef) */, ICompiler_t3_246_il2cpp_TypeInfo_var, L_38, L_39);
	}

IL_00be:
	{
		return;
	}
}
// System.Boolean System.Text.RegularExpressions.Syntax.ExpressionAssertion::IsComplex()
extern "C" bool ExpressionAssertion_IsComplex_m3_1664 (ExpressionAssertion_t3_223 * __this, const MethodInfo* method)
{
	{
		return 1;
	}
}
// System.Void System.Text.RegularExpressions.Syntax.Alternation::.ctor()
extern "C" void Alternation__ctor_m3_1665 (Alternation_t3_225 * __this, const MethodInfo* method)
{
	{
		CompositeExpression__ctor_m3_1613(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Text.RegularExpressions.Syntax.ExpressionCollection System.Text.RegularExpressions.Syntax.Alternation::get_Alternatives()
extern "C" ExpressionCollection_t3_212 * Alternation_get_Alternatives_m3_1666 (Alternation_t3_225 * __this, const MethodInfo* method)
{
	{
		ExpressionCollection_t3_212 * L_0 = CompositeExpression_get_Expressions_m3_1614(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Text.RegularExpressions.Syntax.Alternation::AddAlternative(System.Text.RegularExpressions.Syntax.Expression)
extern "C" void Alternation_AddAlternative_m3_1667 (Alternation_t3_225 * __this, Expression_t3_213 * ___e, const MethodInfo* method)
{
	{
		ExpressionCollection_t3_212 * L_0 = Alternation_get_Alternatives_m3_1666(__this, /*hidden argument*/NULL);
		Expression_t3_213 * L_1 = ___e;
		NullCheck(L_0);
		ExpressionCollection_Add_m3_1606(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Text.RegularExpressions.Syntax.Alternation::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
extern TypeInfo* ICompiler_t3_246_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t1_193_il2cpp_TypeInfo_var;
extern TypeInfo* Expression_t3_213_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern "C" void Alternation_Compile_m3_1668 (Alternation_t3_225 * __this, Object_t * ___cmp, bool ___reverse, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ICompiler_t3_246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1515);
		IEnumerator_t1_193_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(144);
		Expression_t3_213_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1547);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		s_Il2CppMethodIntialized = true;
	}
	LinkRef_t3_193 * V_0 = {0};
	Expression_t3_213 * V_1 = {0};
	Object_t * V_2 = {0};
	LinkRef_t3_193 * V_3 = {0};
	Object_t * V_4 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Object_t * L_0 = ___cmp;
		NullCheck(L_0);
		LinkRef_t3_193 * L_1 = (LinkRef_t3_193 *)InterfaceFuncInvoker0< LinkRef_t3_193 * >::Invoke(28 /* System.Text.RegularExpressions.LinkRef System.Text.RegularExpressions.ICompiler::NewLink() */, ICompiler_t3_246_il2cpp_TypeInfo_var, L_0);
		V_0 = L_1;
		ExpressionCollection_t3_212 * L_2 = Alternation_get_Alternatives_m3_1666(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Object_t * L_3 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(4 /* System.Collections.IEnumerator System.Collections.CollectionBase::GetEnumerator() */, L_2);
		V_2 = L_3;
	}

IL_0013:
	try
	{ // begin try (depth: 1)
		{
			goto IL_004e;
		}

IL_0018:
		{
			Object_t * L_4 = V_2;
			NullCheck(L_4);
			Object_t * L_5 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_4);
			V_1 = ((Expression_t3_213 *)CastclassClass(L_5, Expression_t3_213_il2cpp_TypeInfo_var));
			Object_t * L_6 = ___cmp;
			NullCheck(L_6);
			LinkRef_t3_193 * L_7 = (LinkRef_t3_193 *)InterfaceFuncInvoker0< LinkRef_t3_193 * >::Invoke(28 /* System.Text.RegularExpressions.LinkRef System.Text.RegularExpressions.ICompiler::NewLink() */, ICompiler_t3_246_il2cpp_TypeInfo_var, L_6);
			V_3 = L_7;
			Object_t * L_8 = ___cmp;
			LinkRef_t3_193 * L_9 = V_3;
			NullCheck(L_8);
			InterfaceActionInvoker1< LinkRef_t3_193 * >::Invoke(18 /* System.Void System.Text.RegularExpressions.ICompiler::EmitBranch(System.Text.RegularExpressions.LinkRef) */, ICompiler_t3_246_il2cpp_TypeInfo_var, L_8, L_9);
			Expression_t3_213 * L_10 = V_1;
			Object_t * L_11 = ___cmp;
			bool L_12 = ___reverse;
			NullCheck(L_10);
			VirtActionInvoker2< Object_t *, bool >::Invoke(4 /* System.Void System.Text.RegularExpressions.Syntax.Expression::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean) */, L_10, L_11, L_12);
			Object_t * L_13 = ___cmp;
			LinkRef_t3_193 * L_14 = V_0;
			NullCheck(L_13);
			InterfaceActionInvoker1< LinkRef_t3_193 * >::Invoke(19 /* System.Void System.Text.RegularExpressions.ICompiler::EmitJump(System.Text.RegularExpressions.LinkRef) */, ICompiler_t3_246_il2cpp_TypeInfo_var, L_13, L_14);
			Object_t * L_15 = ___cmp;
			LinkRef_t3_193 * L_16 = V_3;
			NullCheck(L_15);
			InterfaceActionInvoker1< LinkRef_t3_193 * >::Invoke(29 /* System.Void System.Text.RegularExpressions.ICompiler::ResolveLink(System.Text.RegularExpressions.LinkRef) */, ICompiler_t3_246_il2cpp_TypeInfo_var, L_15, L_16);
			Object_t * L_17 = ___cmp;
			NullCheck(L_17);
			InterfaceActionInvoker0::Invoke(26 /* System.Void System.Text.RegularExpressions.ICompiler::EmitBranchEnd() */, ICompiler_t3_246_il2cpp_TypeInfo_var, L_17);
		}

IL_004e:
		{
			Object_t * L_18 = V_2;
			NullCheck(L_18);
			bool L_19 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_18);
			if (L_19)
			{
				goto IL_0018;
			}
		}

IL_0059:
		{
			IL2CPP_LEAVE(0x73, FINALLY_005e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_005e;
	}

FINALLY_005e:
	{ // begin finally (depth: 1)
		{
			Object_t * L_20 = V_2;
			V_4 = ((Object_t *)IsInst(L_20, IDisposable_t1_1035_il2cpp_TypeInfo_var));
			Object_t * L_21 = V_4;
			if (L_21)
			{
				goto IL_006b;
			}
		}

IL_006a:
		{
			IL2CPP_END_FINALLY(94)
		}

IL_006b:
		{
			Object_t * L_22 = V_4;
			NullCheck(L_22);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_22);
			IL2CPP_END_FINALLY(94)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(94)
	{
		IL2CPP_JUMP_TBL(0x73, IL_0073)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0073:
	{
		Object_t * L_23 = ___cmp;
		NullCheck(L_23);
		InterfaceActionInvoker0::Invoke(1 /* System.Void System.Text.RegularExpressions.ICompiler::EmitFalse() */, ICompiler_t3_246_il2cpp_TypeInfo_var, L_23);
		Object_t * L_24 = ___cmp;
		LinkRef_t3_193 * L_25 = V_0;
		NullCheck(L_24);
		InterfaceActionInvoker1< LinkRef_t3_193 * >::Invoke(29 /* System.Void System.Text.RegularExpressions.ICompiler::ResolveLink(System.Text.RegularExpressions.LinkRef) */, ICompiler_t3_246_il2cpp_TypeInfo_var, L_24, L_25);
		Object_t * L_26 = ___cmp;
		NullCheck(L_26);
		InterfaceActionInvoker0::Invoke(27 /* System.Void System.Text.RegularExpressions.ICompiler::EmitAlternationEnd() */, ICompiler_t3_246_il2cpp_TypeInfo_var, L_26);
		return;
	}
}
// System.Void System.Text.RegularExpressions.Syntax.Alternation::GetWidth(System.Int32&,System.Int32&)
extern "C" void Alternation_GetWidth_m3_1669 (Alternation_t3_225 * __this, int32_t* ___min, int32_t* ___max, const MethodInfo* method)
{
	{
		int32_t* L_0 = ___min;
		int32_t* L_1 = ___max;
		ExpressionCollection_t3_212 * L_2 = Alternation_get_Alternatives_m3_1666(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 System.Collections.CollectionBase::get_Count() */, L_2);
		CompositeExpression_GetWidth_m3_1615(__this, L_0, L_1, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Text.RegularExpressions.Syntax.Literal::.ctor(System.String,System.Boolean)
extern "C" void Literal__ctor_m3_1670 (Literal_t3_224 * __this, String_t* ___str, bool ___ignore, const MethodInfo* method)
{
	{
		Expression__ctor_m3_1610(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___str;
		__this->___str_0 = L_0;
		bool L_1 = ___ignore;
		__this->___ignore_1 = L_1;
		return;
	}
}
// System.Void System.Text.RegularExpressions.Syntax.Literal::CompileLiteral(System.String,System.Text.RegularExpressions.ICompiler,System.Boolean,System.Boolean)
extern TypeInfo* ICompiler_t3_246_il2cpp_TypeInfo_var;
extern "C" void Literal_CompileLiteral_m3_1671 (Object_t * __this /* static, unused */, String_t* ___str, Object_t * ___cmp, bool ___ignore, bool ___reverse, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ICompiler_t3_246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1515);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___str;
		NullCheck(L_0);
		int32_t L_1 = String_get_Length_m1_571(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		String_t* L_2 = ___str;
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m1_571(L_2, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)1))))
		{
			goto IL_002d;
		}
	}
	{
		Object_t * L_4 = ___cmp;
		String_t* L_5 = ___str;
		NullCheck(L_5);
		uint16_t L_6 = String_get_Chars_m1_442(L_5, 0, /*hidden argument*/NULL);
		bool L_7 = ___ignore;
		bool L_8 = ___reverse;
		NullCheck(L_4);
		InterfaceActionInvoker4< uint16_t, bool, bool, bool >::Invoke(3 /* System.Void System.Text.RegularExpressions.ICompiler::EmitCharacter(System.Char,System.Boolean,System.Boolean,System.Boolean) */, ICompiler_t3_246_il2cpp_TypeInfo_var, L_4, L_6, 0, L_7, L_8);
		goto IL_0036;
	}

IL_002d:
	{
		Object_t * L_9 = ___cmp;
		String_t* L_10 = ___str;
		bool L_11 = ___ignore;
		bool L_12 = ___reverse;
		NullCheck(L_9);
		InterfaceActionInvoker3< String_t*, bool, bool >::Invoke(8 /* System.Void System.Text.RegularExpressions.ICompiler::EmitString(System.String,System.Boolean,System.Boolean) */, ICompiler_t3_246_il2cpp_TypeInfo_var, L_9, L_10, L_11, L_12);
	}

IL_0036:
	{
		return;
	}
}
// System.Void System.Text.RegularExpressions.Syntax.Literal::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
extern "C" void Literal_Compile_m3_1672 (Literal_t3_224 * __this, Object_t * ___cmp, bool ___reverse, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___str_0);
		Object_t * L_1 = ___cmp;
		bool L_2 = (__this->___ignore_1);
		bool L_3 = ___reverse;
		Literal_CompileLiteral_m3_1671(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Text.RegularExpressions.Syntax.Literal::GetWidth(System.Int32&,System.Int32&)
extern "C" void Literal_GetWidth_m3_1673 (Literal_t3_224 * __this, int32_t* ___min, int32_t* ___max, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t* L_0 = ___min;
		int32_t* L_1 = ___max;
		String_t* L_2 = (__this->___str_0);
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m1_571(L_2, /*hidden argument*/NULL);
		int32_t L_4 = L_3;
		V_0 = L_4;
		*((int32_t*)(L_1)) = (int32_t)L_4;
		int32_t L_5 = V_0;
		*((int32_t*)(L_0)) = (int32_t)L_5;
		return;
	}
}
// System.Text.RegularExpressions.Syntax.AnchorInfo System.Text.RegularExpressions.Syntax.Literal::GetAnchorInfo(System.Boolean)
extern TypeInfo* AnchorInfo_t3_230_il2cpp_TypeInfo_var;
extern "C" AnchorInfo_t3_230 * Literal_GetAnchorInfo_m3_1674 (Literal_t3_224 * __this, bool ___reverse, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AnchorInfo_t3_230_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1548);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = (__this->___str_0);
		NullCheck(L_0);
		int32_t L_1 = String_get_Length_m1_571(L_0, /*hidden argument*/NULL);
		String_t* L_2 = (__this->___str_0);
		bool L_3 = (__this->___ignore_1);
		AnchorInfo_t3_230 * L_4 = (AnchorInfo_t3_230 *)il2cpp_codegen_object_new (AnchorInfo_t3_230_il2cpp_TypeInfo_var);
		AnchorInfo__ctor_m3_1702(L_4, __this, 0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean System.Text.RegularExpressions.Syntax.Literal::IsComplex()
extern "C" bool Literal_IsComplex_m3_1675 (Literal_t3_224 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Void System.Text.RegularExpressions.Syntax.PositionAssertion::.ctor(System.Text.RegularExpressions.Position)
extern "C" void PositionAssertion__ctor_m3_1676 (PositionAssertion_t3_226 * __this, uint16_t ___pos, const MethodInfo* method)
{
	{
		Expression__ctor_m3_1610(__this, /*hidden argument*/NULL);
		uint16_t L_0 = ___pos;
		__this->___pos_0 = L_0;
		return;
	}
}
// System.Void System.Text.RegularExpressions.Syntax.PositionAssertion::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
extern TypeInfo* ICompiler_t3_246_il2cpp_TypeInfo_var;
extern "C" void PositionAssertion_Compile_m3_1677 (PositionAssertion_t3_226 * __this, Object_t * ___cmp, bool ___reverse, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ICompiler_t3_246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1515);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___cmp;
		uint16_t L_1 = (__this->___pos_0);
		NullCheck(L_0);
		InterfaceActionInvoker1< uint16_t >::Invoke(9 /* System.Void System.Text.RegularExpressions.ICompiler::EmitPosition(System.Text.RegularExpressions.Position) */, ICompiler_t3_246_il2cpp_TypeInfo_var, L_0, L_1);
		return;
	}
}
// System.Void System.Text.RegularExpressions.Syntax.PositionAssertion::GetWidth(System.Int32&,System.Int32&)
extern "C" void PositionAssertion_GetWidth_m3_1678 (PositionAssertion_t3_226 * __this, int32_t* ___min, int32_t* ___max, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t* L_0 = ___min;
		int32_t* L_1 = ___max;
		int32_t L_2 = 0;
		V_0 = L_2;
		*((int32_t*)(L_1)) = (int32_t)L_2;
		int32_t L_3 = V_0;
		*((int32_t*)(L_0)) = (int32_t)L_3;
		return;
	}
}
// System.Boolean System.Text.RegularExpressions.Syntax.PositionAssertion::IsComplex()
extern "C" bool PositionAssertion_IsComplex_m3_1679 (PositionAssertion_t3_226 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Text.RegularExpressions.Syntax.AnchorInfo System.Text.RegularExpressions.Syntax.PositionAssertion::GetAnchorInfo(System.Boolean)
extern TypeInfo* AnchorInfo_t3_230_il2cpp_TypeInfo_var;
extern "C" AnchorInfo_t3_230 * PositionAssertion_GetAnchorInfo_m3_1680 (PositionAssertion_t3_226 * __this, bool ___revers, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AnchorInfo_t3_230_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1548);
		s_Il2CppMethodIntialized = true;
	}
	uint16_t V_0 = {0};
	{
		uint16_t L_0 = (__this->___pos_0);
		V_0 = L_0;
		uint16_t L_1 = V_0;
		if (((int32_t)((int32_t)L_1-(int32_t)2)) == 0)
		{
			goto IL_0020;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)2)) == 1)
		{
			goto IL_0020;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)2)) == 2)
		{
			goto IL_0020;
		}
	}
	{
		goto IL_002f;
	}

IL_0020:
	{
		uint16_t L_2 = (__this->___pos_0);
		AnchorInfo_t3_230 * L_3 = (AnchorInfo_t3_230 *)il2cpp_codegen_object_new (AnchorInfo_t3_230_il2cpp_TypeInfo_var);
		AnchorInfo__ctor_m3_1703(L_3, __this, 0, 0, L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_002f:
	{
		AnchorInfo_t3_230 * L_4 = (AnchorInfo_t3_230 *)il2cpp_codegen_object_new (AnchorInfo_t3_230_il2cpp_TypeInfo_var);
		AnchorInfo__ctor_m3_1701(L_4, __this, 0, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void System.Text.RegularExpressions.Syntax.Reference::.ctor(System.Boolean)
extern "C" void Reference__ctor_m3_1681 (Reference_t3_227 * __this, bool ___ignore, const MethodInfo* method)
{
	{
		Expression__ctor_m3_1610(__this, /*hidden argument*/NULL);
		bool L_0 = ___ignore;
		__this->___ignore_1 = L_0;
		return;
	}
}
// System.Text.RegularExpressions.Syntax.CapturingGroup System.Text.RegularExpressions.Syntax.Reference::get_CapturingGroup()
extern "C" CapturingGroup_t3_217 * Reference_get_CapturingGroup_m3_1682 (Reference_t3_227 * __this, const MethodInfo* method)
{
	{
		CapturingGroup_t3_217 * L_0 = (__this->___group_0);
		return L_0;
	}
}
// System.Void System.Text.RegularExpressions.Syntax.Reference::set_CapturingGroup(System.Text.RegularExpressions.Syntax.CapturingGroup)
extern "C" void Reference_set_CapturingGroup_m3_1683 (Reference_t3_227 * __this, CapturingGroup_t3_217 * ___value, const MethodInfo* method)
{
	{
		CapturingGroup_t3_217 * L_0 = ___value;
		__this->___group_0 = L_0;
		return;
	}
}
// System.Boolean System.Text.RegularExpressions.Syntax.Reference::get_IgnoreCase()
extern "C" bool Reference_get_IgnoreCase_m3_1684 (Reference_t3_227 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___ignore_1);
		return L_0;
	}
}
// System.Void System.Text.RegularExpressions.Syntax.Reference::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
extern TypeInfo* ICompiler_t3_246_il2cpp_TypeInfo_var;
extern "C" void Reference_Compile_m3_1685 (Reference_t3_227 * __this, Object_t * ___cmp, bool ___reverse, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ICompiler_t3_246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1515);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___cmp;
		CapturingGroup_t3_217 * L_1 = (__this->___group_0);
		NullCheck(L_1);
		int32_t L_2 = CapturingGroup_get_Index_m3_1626(L_1, /*hidden argument*/NULL);
		bool L_3 = (__this->___ignore_1);
		bool L_4 = ___reverse;
		NullCheck(L_0);
		InterfaceActionInvoker3< int32_t, bool, bool >::Invoke(14 /* System.Void System.Text.RegularExpressions.ICompiler::EmitReference(System.Int32,System.Boolean,System.Boolean) */, ICompiler_t3_246_il2cpp_TypeInfo_var, L_0, L_2, L_3, L_4);
		return;
	}
}
// System.Void System.Text.RegularExpressions.Syntax.Reference::GetWidth(System.Int32&,System.Int32&)
extern "C" void Reference_GetWidth_m3_1686 (Reference_t3_227 * __this, int32_t* ___min, int32_t* ___max, const MethodInfo* method)
{
	{
		int32_t* L_0 = ___min;
		*((int32_t*)(L_0)) = (int32_t)0;
		int32_t* L_1 = ___max;
		*((int32_t*)(L_1)) = (int32_t)((int32_t)2147483647);
		return;
	}
}
// System.Boolean System.Text.RegularExpressions.Syntax.Reference::IsComplex()
extern "C" bool Reference_IsComplex_m3_1687 (Reference_t3_227 * __this, const MethodInfo* method)
{
	{
		return 1;
	}
}
// System.Void System.Text.RegularExpressions.Syntax.BackslashNumber::.ctor(System.Boolean,System.Boolean)
extern "C" void BackslashNumber__ctor_m3_1688 (BackslashNumber_t3_228 * __this, bool ___ignore, bool ___ecma, const MethodInfo* method)
{
	{
		bool L_0 = ___ignore;
		Reference__ctor_m3_1681(__this, L_0, /*hidden argument*/NULL);
		bool L_1 = ___ecma;
		__this->___ecma_3 = L_1;
		return;
	}
}
// System.Boolean System.Text.RegularExpressions.Syntax.BackslashNumber::ResolveReference(System.String,System.Collections.Hashtable)
extern TypeInfo* CapturingGroup_t3_217_il2cpp_TypeInfo_var;
extern TypeInfo* Char_t1_15_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" bool BackslashNumber_ResolveReference_m3_1689 (BackslashNumber_t3_228 * __this, String_t* ___num_str, Hashtable_t1_100 * ___groups, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CapturingGroup_t3_217_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1534);
		Char_t1_15_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(12);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		bool L_0 = (__this->___ecma_3);
		if (!L_0)
		{
			goto IL_006c;
		}
	}
	{
		V_0 = 0;
		V_1 = 1;
		goto IL_002d;
	}

IL_0014:
	{
		Hashtable_t1_100 * L_1 = ___groups;
		String_t* L_2 = ___num_str;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		String_t* L_4 = String_Substring_m1_455(L_2, 0, L_3, /*hidden argument*/NULL);
		NullCheck(L_1);
		Object_t * L_5 = (Object_t *)VirtFuncInvoker1< Object_t *, Object_t * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_1, L_4);
		if (!L_5)
		{
			goto IL_0029;
		}
	}
	{
		int32_t L_6 = V_1;
		V_0 = L_6;
	}

IL_0029:
	{
		int32_t L_7 = V_1;
		V_1 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_002d:
	{
		int32_t L_8 = V_1;
		String_t* L_9 = ___num_str;
		NullCheck(L_9);
		int32_t L_10 = String_get_Length_m1_571(L_9, /*hidden argument*/NULL);
		if ((((int32_t)L_8) < ((int32_t)L_10)))
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_11 = V_0;
		if (!L_11)
		{
			goto IL_0067;
		}
	}
	{
		Hashtable_t1_100 * L_12 = ___groups;
		String_t* L_13 = ___num_str;
		int32_t L_14 = V_0;
		NullCheck(L_13);
		String_t* L_15 = String_Substring_m1_455(L_13, 0, L_14, /*hidden argument*/NULL);
		NullCheck(L_12);
		Object_t * L_16 = (Object_t *)VirtFuncInvoker1< Object_t *, Object_t * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_12, L_15);
		Reference_set_CapturingGroup_m3_1683(__this, ((CapturingGroup_t3_217 *)CastclassClass(L_16, CapturingGroup_t3_217_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		String_t* L_17 = ___num_str;
		int32_t L_18 = V_0;
		NullCheck(L_17);
		String_t* L_19 = String_Substring_m1_454(L_17, L_18, /*hidden argument*/NULL);
		__this->___literal_2 = L_19;
		return 1;
	}

IL_0067:
	{
		goto IL_007a;
	}

IL_006c:
	{
		String_t* L_20 = ___num_str;
		NullCheck(L_20);
		int32_t L_21 = String_get_Length_m1_571(L_20, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_21) == ((uint32_t)1))))
		{
			goto IL_007a;
		}
	}
	{
		return 0;
	}

IL_007a:
	{
		V_2 = 0;
		String_t* L_22 = ___num_str;
		int32_t L_23 = Parser_ParseOctal_m3_1560(NULL /*static, unused*/, L_22, (&V_2), /*hidden argument*/NULL);
		V_3 = L_23;
		int32_t L_24 = V_3;
		if ((!(((uint32_t)L_24) == ((uint32_t)(-1)))))
		{
			goto IL_008e;
		}
	}
	{
		return 0;
	}

IL_008e:
	{
		int32_t L_25 = V_3;
		if ((((int32_t)L_25) <= ((int32_t)((int32_t)255))))
		{
			goto IL_00ac;
		}
	}
	{
		bool L_26 = (__this->___ecma_3);
		if (!L_26)
		{
			goto IL_00ac;
		}
	}
	{
		int32_t L_27 = V_3;
		V_3 = ((int32_t)((int32_t)L_27/(int32_t)8));
		int32_t L_28 = V_2;
		V_2 = ((int32_t)((int32_t)L_28-(int32_t)1));
	}

IL_00ac:
	{
		int32_t L_29 = V_3;
		V_3 = ((int32_t)((int32_t)L_29&(int32_t)((int32_t)255)));
		int32_t L_30 = V_3;
		uint16_t L_31 = (((int32_t)((uint16_t)L_30)));
		Object_t * L_32 = Box(Char_t1_15_il2cpp_TypeInfo_var, &L_31);
		String_t* L_33 = ___num_str;
		int32_t L_34 = V_2;
		NullCheck(L_33);
		String_t* L_35 = String_Substring_m1_454(L_33, L_34, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_36 = String_Concat_m1_556(NULL /*static, unused*/, L_32, L_35, /*hidden argument*/NULL);
		__this->___literal_2 = L_36;
		return 1;
	}
}
// System.Void System.Text.RegularExpressions.Syntax.BackslashNumber::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
extern "C" void BackslashNumber_Compile_m3_1690 (BackslashNumber_t3_228 * __this, Object_t * ___cmp, bool ___reverse, const MethodInfo* method)
{
	{
		CapturingGroup_t3_217 * L_0 = Reference_get_CapturingGroup_m3_1682(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		Object_t * L_1 = ___cmp;
		bool L_2 = ___reverse;
		Reference_Compile_m3_1685(__this, L_1, L_2, /*hidden argument*/NULL);
	}

IL_0013:
	{
		String_t* L_3 = (__this->___literal_2);
		if (!L_3)
		{
			goto IL_0031;
		}
	}
	{
		String_t* L_4 = (__this->___literal_2);
		Object_t * L_5 = ___cmp;
		bool L_6 = Reference_get_IgnoreCase_m3_1684(__this, /*hidden argument*/NULL);
		bool L_7 = ___reverse;
		Literal_CompileLiteral_m3_1671(NULL /*static, unused*/, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void System.Text.RegularExpressions.Syntax.CharacterClass::.ctor(System.Boolean,System.Boolean)
extern TypeInfo* IntervalCollection_t3_209_il2cpp_TypeInfo_var;
extern TypeInfo* BitArray_t1_274_il2cpp_TypeInfo_var;
extern "C" void CharacterClass__ctor_m3_1691 (CharacterClass_t3_229 * __this, bool ___negate, bool ___ignore, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntervalCollection_t3_209_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1531);
		BitArray_t1_274_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(294);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		Expression__ctor_m3_1610(__this, /*hidden argument*/NULL);
		bool L_0 = ___negate;
		__this->___negate_1 = L_0;
		bool L_1 = ___ignore;
		__this->___ignore_2 = L_1;
		IntervalCollection_t3_209 * L_2 = (IntervalCollection_t3_209 *)il2cpp_codegen_object_new (IntervalCollection_t3_209_il2cpp_TypeInfo_var);
		IntervalCollection__ctor_m3_1547(L_2, /*hidden argument*/NULL);
		__this->___intervals_5 = L_2;
		V_0 = ((int32_t)144);
		int32_t L_3 = V_0;
		BitArray_t1_274 * L_4 = (BitArray_t1_274 *)il2cpp_codegen_object_new (BitArray_t1_274_il2cpp_TypeInfo_var);
		BitArray__ctor_m1_3121(L_4, L_3, /*hidden argument*/NULL);
		__this->___pos_cats_3 = L_4;
		int32_t L_5 = V_0;
		BitArray_t1_274 * L_6 = (BitArray_t1_274 *)il2cpp_codegen_object_new (BitArray_t1_274_il2cpp_TypeInfo_var);
		BitArray__ctor_m1_3121(L_6, L_5, /*hidden argument*/NULL);
		__this->___neg_cats_4 = L_6;
		return;
	}
}
// System.Void System.Text.RegularExpressions.Syntax.CharacterClass::.ctor(System.Text.RegularExpressions.Category,System.Boolean)
extern "C" void CharacterClass__ctor_m3_1692 (CharacterClass_t3_229 * __this, uint16_t ___cat, bool ___negate, const MethodInfo* method)
{
	{
		CharacterClass__ctor_m3_1691(__this, 0, 0, /*hidden argument*/NULL);
		uint16_t L_0 = ___cat;
		bool L_1 = ___negate;
		CharacterClass_AddCategory_m3_1694(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Text.RegularExpressions.Syntax.CharacterClass::.cctor()
extern TypeInfo* CharacterClass_t3_229_il2cpp_TypeInfo_var;
extern "C" void CharacterClass__cctor_m3_1693 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CharacterClass_t3_229_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1537);
		s_Il2CppMethodIntialized = true;
	}
	{
		Interval_t3_206  L_0 = {0};
		Interval__ctor_m3_1526(&L_0, ((int32_t)65), ((int32_t)90), /*hidden argument*/NULL);
		((CharacterClass_t3_229_StaticFields*)CharacterClass_t3_229_il2cpp_TypeInfo_var->static_fields)->___upper_case_characters_0 = L_0;
		return;
	}
}
// System.Void System.Text.RegularExpressions.Syntax.CharacterClass::AddCategory(System.Text.RegularExpressions.Category,System.Boolean)
extern "C" void CharacterClass_AddCategory_m3_1694 (CharacterClass_t3_229 * __this, uint16_t ___cat, bool ___negate, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		uint16_t L_0 = ___cat;
		V_0 = L_0;
		bool L_1 = ___negate;
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		BitArray_t1_274 * L_2 = (__this->___neg_cats_4);
		int32_t L_3 = V_0;
		NullCheck(L_2);
		BitArray_set_Item_m1_3131(L_2, L_3, 1, /*hidden argument*/NULL);
		goto IL_0027;
	}

IL_001a:
	{
		BitArray_t1_274 * L_4 = (__this->___pos_cats_3);
		int32_t L_5 = V_0;
		NullCheck(L_4);
		BitArray_set_Item_m1_3131(L_4, L_5, 1, /*hidden argument*/NULL);
	}

IL_0027:
	{
		return;
	}
}
// System.Void System.Text.RegularExpressions.Syntax.CharacterClass::AddCharacter(System.Char)
extern "C" void CharacterClass_AddCharacter_m3_1695 (CharacterClass_t3_229 * __this, uint16_t ___c, const MethodInfo* method)
{
	{
		uint16_t L_0 = ___c;
		uint16_t L_1 = ___c;
		CharacterClass_AddRange_m3_1696(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Text.RegularExpressions.Syntax.CharacterClass::AddRange(System.Char,System.Char)
extern TypeInfo* CharacterClass_t3_229_il2cpp_TypeInfo_var;
extern "C" void CharacterClass_AddRange_m3_1696 (CharacterClass_t3_229 * __this, uint16_t ___lo, uint16_t ___hi, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CharacterClass_t3_229_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1537);
		s_Il2CppMethodIntialized = true;
	}
	Interval_t3_206  V_0 = {0};
	Interval_t3_206  V_1 = {0};
	{
		uint16_t L_0 = ___lo;
		uint16_t L_1 = ___hi;
		Interval__ctor_m3_1526((&V_0), L_0, L_1, /*hidden argument*/NULL);
		bool L_2 = (__this->___ignore_2);
		if (!L_2)
		{
			goto IL_00e2;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CharacterClass_t3_229_il2cpp_TypeInfo_var);
		Interval_t3_206  L_3 = V_0;
		bool L_4 = Interval_Intersects_m3_1536((&((CharacterClass_t3_229_StaticFields*)CharacterClass_t3_229_il2cpp_TypeInfo_var->static_fields)->___upper_case_characters_0), L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_00b2;
		}
	}
	{
		int32_t L_5 = ((&V_0)->___low_0);
		IL2CPP_RUNTIME_CLASS_INIT(CharacterClass_t3_229_il2cpp_TypeInfo_var);
		int32_t L_6 = ((&((CharacterClass_t3_229_StaticFields*)CharacterClass_t3_229_il2cpp_TypeInfo_var->static_fields)->___upper_case_characters_0)->___low_0);
		if ((((int32_t)L_5) >= ((int32_t)L_6)))
		{
			goto IL_0070;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CharacterClass_t3_229_il2cpp_TypeInfo_var);
		int32_t L_7 = ((&((CharacterClass_t3_229_StaticFields*)CharacterClass_t3_229_il2cpp_TypeInfo_var->static_fields)->___upper_case_characters_0)->___low_0);
		int32_t L_8 = ((&V_0)->___high_1);
		Interval__ctor_m3_1526((&V_1), ((int32_t)((int32_t)L_7+(int32_t)((int32_t)32))), ((int32_t)((int32_t)L_8+(int32_t)((int32_t)32))), /*hidden argument*/NULL);
		int32_t L_9 = ((&((CharacterClass_t3_229_StaticFields*)CharacterClass_t3_229_il2cpp_TypeInfo_var->static_fields)->___upper_case_characters_0)->___low_0);
		(&V_0)->___high_1 = ((int32_t)((int32_t)L_9-(int32_t)1));
		goto IL_00a1;
	}

IL_0070:
	{
		int32_t L_10 = ((&V_0)->___low_0);
		IL2CPP_RUNTIME_CLASS_INIT(CharacterClass_t3_229_il2cpp_TypeInfo_var);
		int32_t L_11 = ((&((CharacterClass_t3_229_StaticFields*)CharacterClass_t3_229_il2cpp_TypeInfo_var->static_fields)->___upper_case_characters_0)->___high_1);
		Interval__ctor_m3_1526((&V_1), ((int32_t)((int32_t)L_10+(int32_t)((int32_t)32))), ((int32_t)((int32_t)L_11+(int32_t)((int32_t)32))), /*hidden argument*/NULL);
		int32_t L_12 = ((&((CharacterClass_t3_229_StaticFields*)CharacterClass_t3_229_il2cpp_TypeInfo_var->static_fields)->___upper_case_characters_0)->___high_1);
		(&V_0)->___low_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_00a1:
	{
		IntervalCollection_t3_209 * L_13 = (__this->___intervals_5);
		Interval_t3_206  L_14 = V_1;
		NullCheck(L_13);
		IntervalCollection_Add_m3_1549(L_13, L_14, /*hidden argument*/NULL);
		goto IL_00e2;
	}

IL_00b2:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CharacterClass_t3_229_il2cpp_TypeInfo_var);
		Interval_t3_206  L_15 = V_0;
		bool L_16 = Interval_Contains_m3_1534((&((CharacterClass_t3_229_StaticFields*)CharacterClass_t3_229_il2cpp_TypeInfo_var->static_fields)->___upper_case_characters_0), L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00e2;
		}
	}
	{
		Interval_t3_206 * L_17 = (&V_0);
		int32_t L_18 = (L_17->___high_1);
		L_17->___high_1 = ((int32_t)((int32_t)L_18+(int32_t)((int32_t)32)));
		Interval_t3_206 * L_19 = (&V_0);
		int32_t L_20 = (L_19->___low_0);
		L_19->___low_0 = ((int32_t)((int32_t)L_20+(int32_t)((int32_t)32)));
	}

IL_00e2:
	{
		IntervalCollection_t3_209 * L_21 = (__this->___intervals_5);
		Interval_t3_206  L_22 = V_0;
		NullCheck(L_21);
		IntervalCollection_Add_m3_1549(L_21, L_22, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Text.RegularExpressions.Syntax.CharacterClass::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
extern TypeInfo* CostDelegate_t3_208_il2cpp_TypeInfo_var;
extern TypeInfo* ICompiler_t3_246_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t1_193_il2cpp_TypeInfo_var;
extern TypeInfo* Interval_t3_206_il2cpp_TypeInfo_var;
extern TypeInfo* BitArray_t1_274_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern const MethodInfo* CharacterClass_GetIntervalCost_m3_1700_MethodInfo_var;
extern "C" void CharacterClass_Compile_m3_1697 (CharacterClass_t3_229 * __this, Object_t * ___cmp, bool ___reverse, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CostDelegate_t3_208_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1550);
		ICompiler_t3_246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1515);
		IEnumerator_t1_193_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(144);
		Interval_t3_206_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1530);
		BitArray_t1_274_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(294);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		CharacterClass_GetIntervalCost_m3_1700_MethodInfo_var = il2cpp_codegen_method_info_from_index(125);
		s_Il2CppMethodIntialized = true;
	}
	IntervalCollection_t3_209 * V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	LinkRef_t3_193 * V_3 = {0};
	Interval_t3_206  V_4 = {0};
	Object_t * V_5 = {0};
	BitArray_t1_274 * V_6 = {0};
	Interval_t3_206  V_7 = {0};
	Object_t * V_8 = {0};
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	Object_t * V_11 = {0};
	Object_t * V_12 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IntervalCollection_t3_209 * L_0 = (__this->___intervals_5);
		IntPtr_t L_1 = { (void*)CharacterClass_GetIntervalCost_m3_1700_MethodInfo_var };
		CostDelegate_t3_208 * L_2 = (CostDelegate_t3_208 *)il2cpp_codegen_object_new (CostDelegate_t3_208_il2cpp_TypeInfo_var);
		CostDelegate__ctor_m3_1543(L_2, NULL, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		IntervalCollection_t3_209 * L_3 = IntervalCollection_GetMetaCollection_m3_1551(L_0, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		IntervalCollection_t3_209 * L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 System.Text.RegularExpressions.IntervalCollection::get_Count() */, L_4);
		V_1 = L_5;
		V_2 = 0;
		goto IL_0050;
	}

IL_0026:
	{
		BitArray_t1_274 * L_6 = (__this->___pos_cats_3);
		int32_t L_7 = V_2;
		NullCheck(L_6);
		bool L_8 = BitArray_get_Item_m1_3130(L_6, L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0048;
		}
	}
	{
		BitArray_t1_274 * L_9 = (__this->___neg_cats_4);
		int32_t L_10 = V_2;
		NullCheck(L_9);
		bool L_11 = BitArray_get_Item_m1_3130(L_9, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_004c;
		}
	}

IL_0048:
	{
		int32_t L_12 = V_1;
		V_1 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_004c:
	{
		int32_t L_13 = V_2;
		V_2 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0050:
	{
		int32_t L_14 = V_2;
		BitArray_t1_274 * L_15 = (__this->___pos_cats_3);
		NullCheck(L_15);
		int32_t L_16 = BitArray_get_Length_m1_3132(L_15, /*hidden argument*/NULL);
		if ((((int32_t)L_14) < ((int32_t)L_16)))
		{
			goto IL_0026;
		}
	}
	{
		int32_t L_17 = V_1;
		if (L_17)
		{
			goto IL_0068;
		}
	}
	{
		return;
	}

IL_0068:
	{
		Object_t * L_18 = ___cmp;
		NullCheck(L_18);
		LinkRef_t3_193 * L_19 = (LinkRef_t3_193 *)InterfaceFuncInvoker0< LinkRef_t3_193 * >::Invoke(28 /* System.Text.RegularExpressions.LinkRef System.Text.RegularExpressions.ICompiler::NewLink() */, ICompiler_t3_246_il2cpp_TypeInfo_var, L_18);
		V_3 = L_19;
		int32_t L_20 = V_1;
		if ((((int32_t)L_20) <= ((int32_t)1)))
		{
			goto IL_007d;
		}
	}
	{
		Object_t * L_21 = ___cmp;
		LinkRef_t3_193 * L_22 = V_3;
		NullCheck(L_21);
		InterfaceActionInvoker1< LinkRef_t3_193 * >::Invoke(22 /* System.Void System.Text.RegularExpressions.ICompiler::EmitIn(System.Text.RegularExpressions.LinkRef) */, ICompiler_t3_246_il2cpp_TypeInfo_var, L_21, L_22);
	}

IL_007d:
	{
		IntervalCollection_t3_209 * L_23 = V_0;
		NullCheck(L_23);
		Object_t * L_24 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(8 /* System.Collections.IEnumerator System.Text.RegularExpressions.IntervalCollection::GetEnumerator() */, L_23);
		V_5 = L_24;
	}

IL_0085:
	try
	{ // begin try (depth: 1)
		{
			goto IL_01ac;
		}

IL_008a:
		{
			Object_t * L_25 = V_5;
			NullCheck(L_25);
			Object_t * L_26 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_25);
			V_4 = ((*(Interval_t3_206 *)((Interval_t3_206 *)UnBox (L_26, Interval_t3_206_il2cpp_TypeInfo_var))));
			bool L_27 = Interval_get_IsDiscontiguous_m3_1528((&V_4), /*hidden argument*/NULL);
			if (!L_27)
			{
				goto IL_015d;
			}
		}

IL_00a4:
		{
			int32_t L_28 = Interval_get_Size_m3_1531((&V_4), /*hidden argument*/NULL);
			BitArray_t1_274 * L_29 = (BitArray_t1_274 *)il2cpp_codegen_object_new (BitArray_t1_274_il2cpp_TypeInfo_var);
			BitArray__ctor_m1_3121(L_29, L_28, /*hidden argument*/NULL);
			V_6 = L_29;
			IntervalCollection_t3_209 * L_30 = (__this->___intervals_5);
			NullCheck(L_30);
			Object_t * L_31 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(8 /* System.Collections.IEnumerator System.Text.RegularExpressions.IntervalCollection::GetEnumerator() */, L_30);
			V_8 = L_31;
		}

IL_00bf:
		try
		{ // begin try (depth: 2)
			{
				goto IL_0114;
			}

IL_00c4:
			{
				Object_t * L_32 = V_8;
				NullCheck(L_32);
				Object_t * L_33 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_32);
				V_7 = ((*(Interval_t3_206 *)((Interval_t3_206 *)UnBox (L_33, Interval_t3_206_il2cpp_TypeInfo_var))));
				Interval_t3_206  L_34 = V_7;
				bool L_35 = Interval_Contains_m3_1534((&V_4), L_34, /*hidden argument*/NULL);
				if (!L_35)
				{
					goto IL_0114;
				}
			}

IL_00e0:
			{
				int32_t L_36 = ((&V_7)->___low_0);
				V_9 = L_36;
				goto IL_0106;
			}

IL_00ee:
			{
				BitArray_t1_274 * L_37 = V_6;
				int32_t L_38 = V_9;
				int32_t L_39 = ((&V_4)->___low_0);
				NullCheck(L_37);
				BitArray_set_Item_m1_3131(L_37, ((int32_t)((int32_t)L_38-(int32_t)L_39)), 1, /*hidden argument*/NULL);
				int32_t L_40 = V_9;
				V_9 = ((int32_t)((int32_t)L_40+(int32_t)1));
			}

IL_0106:
			{
				int32_t L_41 = V_9;
				int32_t L_42 = ((&V_7)->___high_1);
				if ((((int32_t)L_41) <= ((int32_t)L_42)))
				{
					goto IL_00ee;
				}
			}

IL_0114:
			{
				Object_t * L_43 = V_8;
				NullCheck(L_43);
				bool L_44 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_43);
				if (L_44)
				{
					goto IL_00c4;
				}
			}

IL_0120:
			{
				IL2CPP_LEAVE(0x13B, FINALLY_0125);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1_33 *)e.ex;
			goto FINALLY_0125;
		}

FINALLY_0125:
		{ // begin finally (depth: 2)
			{
				Object_t * L_45 = V_8;
				V_11 = ((Object_t *)IsInst(L_45, IDisposable_t1_1035_il2cpp_TypeInfo_var));
				Object_t * L_46 = V_11;
				if (L_46)
				{
					goto IL_0133;
				}
			}

IL_0132:
			{
				IL2CPP_END_FINALLY(293)
			}

IL_0133:
			{
				Object_t * L_47 = V_11;
				NullCheck(L_47);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_47);
				IL2CPP_END_FINALLY(293)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(293)
		{
			IL2CPP_JUMP_TBL(0x13B, IL_013b)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
		}

IL_013b:
		{
			Object_t * L_48 = ___cmp;
			int32_t L_49 = ((&V_4)->___low_0);
			BitArray_t1_274 * L_50 = V_6;
			bool L_51 = (__this->___negate_1);
			bool L_52 = (__this->___ignore_2);
			bool L_53 = ___reverse;
			NullCheck(L_48);
			InterfaceActionInvoker5< uint16_t, BitArray_t1_274 *, bool, bool, bool >::Invoke(7 /* System.Void System.Text.RegularExpressions.ICompiler::EmitSet(System.Char,System.Collections.BitArray,System.Boolean,System.Boolean,System.Boolean) */, ICompiler_t3_246_il2cpp_TypeInfo_var, L_48, (((int32_t)((uint16_t)L_49))), L_50, L_51, L_52, L_53);
			goto IL_01ac;
		}

IL_015d:
		{
			bool L_54 = Interval_get_IsSingleton_m3_1529((&V_4), /*hidden argument*/NULL);
			if (!L_54)
			{
				goto IL_0189;
			}
		}

IL_0169:
		{
			Object_t * L_55 = ___cmp;
			int32_t L_56 = ((&V_4)->___low_0);
			bool L_57 = (__this->___negate_1);
			bool L_58 = (__this->___ignore_2);
			bool L_59 = ___reverse;
			NullCheck(L_55);
			InterfaceActionInvoker4< uint16_t, bool, bool, bool >::Invoke(3 /* System.Void System.Text.RegularExpressions.ICompiler::EmitCharacter(System.Char,System.Boolean,System.Boolean,System.Boolean) */, ICompiler_t3_246_il2cpp_TypeInfo_var, L_55, (((int32_t)((uint16_t)L_56))), L_57, L_58, L_59);
			goto IL_01ac;
		}

IL_0189:
		{
			Object_t * L_60 = ___cmp;
			int32_t L_61 = ((&V_4)->___low_0);
			int32_t L_62 = ((&V_4)->___high_1);
			bool L_63 = (__this->___negate_1);
			bool L_64 = (__this->___ignore_2);
			bool L_65 = ___reverse;
			NullCheck(L_60);
			InterfaceActionInvoker5< uint16_t, uint16_t, bool, bool, bool >::Invoke(6 /* System.Void System.Text.RegularExpressions.ICompiler::EmitRange(System.Char,System.Char,System.Boolean,System.Boolean,System.Boolean) */, ICompiler_t3_246_il2cpp_TypeInfo_var, L_60, (((int32_t)((uint16_t)L_61))), (((int32_t)((uint16_t)L_62))), L_63, L_64, L_65);
		}

IL_01ac:
		{
			Object_t * L_66 = V_5;
			NullCheck(L_66);
			bool L_67 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_66);
			if (L_67)
			{
				goto IL_008a;
			}
		}

IL_01b8:
		{
			IL2CPP_LEAVE(0x1D3, FINALLY_01bd);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_01bd;
	}

FINALLY_01bd:
	{ // begin finally (depth: 1)
		{
			Object_t * L_68 = V_5;
			V_12 = ((Object_t *)IsInst(L_68, IDisposable_t1_1035_il2cpp_TypeInfo_var));
			Object_t * L_69 = V_12;
			if (L_69)
			{
				goto IL_01cb;
			}
		}

IL_01ca:
		{
			IL2CPP_END_FINALLY(445)
		}

IL_01cb:
		{
			Object_t * L_70 = V_12;
			NullCheck(L_70);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_70);
			IL2CPP_END_FINALLY(445)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(445)
	{
		IL2CPP_JUMP_TBL(0x1D3, IL_01d3)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_01d3:
	{
		V_10 = 0;
		goto IL_024f;
	}

IL_01db:
	{
		BitArray_t1_274 * L_71 = (__this->___pos_cats_3);
		int32_t L_72 = V_10;
		NullCheck(L_71);
		bool L_73 = BitArray_get_Item_m1_3130(L_71, L_72, /*hidden argument*/NULL);
		if (!L_73)
		{
			goto IL_0227;
		}
	}
	{
		BitArray_t1_274 * L_74 = (__this->___neg_cats_4);
		int32_t L_75 = V_10;
		NullCheck(L_74);
		bool L_76 = BitArray_get_Item_m1_3130(L_74, L_75, /*hidden argument*/NULL);
		if (!L_76)
		{
			goto IL_0212;
		}
	}
	{
		Object_t * L_77 = ___cmp;
		bool L_78 = (__this->___negate_1);
		bool L_79 = ___reverse;
		NullCheck(L_77);
		InterfaceActionInvoker3< uint16_t, bool, bool >::Invoke(4 /* System.Void System.Text.RegularExpressions.ICompiler::EmitCategory(System.Text.RegularExpressions.Category,System.Boolean,System.Boolean) */, ICompiler_t3_246_il2cpp_TypeInfo_var, L_77, 2, L_78, L_79);
		goto IL_0222;
	}

IL_0212:
	{
		Object_t * L_80 = ___cmp;
		int32_t L_81 = V_10;
		bool L_82 = (__this->___negate_1);
		bool L_83 = ___reverse;
		NullCheck(L_80);
		InterfaceActionInvoker3< uint16_t, bool, bool >::Invoke(4 /* System.Void System.Text.RegularExpressions.ICompiler::EmitCategory(System.Text.RegularExpressions.Category,System.Boolean,System.Boolean) */, ICompiler_t3_246_il2cpp_TypeInfo_var, L_80, (((int32_t)((uint16_t)L_81))), L_82, L_83);
	}

IL_0222:
	{
		goto IL_0249;
	}

IL_0227:
	{
		BitArray_t1_274 * L_84 = (__this->___neg_cats_4);
		int32_t L_85 = V_10;
		NullCheck(L_84);
		bool L_86 = BitArray_get_Item_m1_3130(L_84, L_85, /*hidden argument*/NULL);
		if (!L_86)
		{
			goto IL_0249;
		}
	}
	{
		Object_t * L_87 = ___cmp;
		int32_t L_88 = V_10;
		bool L_89 = (__this->___negate_1);
		bool L_90 = ___reverse;
		NullCheck(L_87);
		InterfaceActionInvoker3< uint16_t, bool, bool >::Invoke(5 /* System.Void System.Text.RegularExpressions.ICompiler::EmitNotCategory(System.Text.RegularExpressions.Category,System.Boolean,System.Boolean) */, ICompiler_t3_246_il2cpp_TypeInfo_var, L_87, (((int32_t)((uint16_t)L_88))), L_89, L_90);
	}

IL_0249:
	{
		int32_t L_91 = V_10;
		V_10 = ((int32_t)((int32_t)L_91+(int32_t)1));
	}

IL_024f:
	{
		int32_t L_92 = V_10;
		BitArray_t1_274 * L_93 = (__this->___pos_cats_3);
		NullCheck(L_93);
		int32_t L_94 = BitArray_get_Length_m1_3132(L_93, /*hidden argument*/NULL);
		if ((((int32_t)L_92) < ((int32_t)L_94)))
		{
			goto IL_01db;
		}
	}
	{
		int32_t L_95 = V_1;
		if ((((int32_t)L_95) <= ((int32_t)1)))
		{
			goto IL_028b;
		}
	}
	{
		bool L_96 = (__this->___negate_1);
		if (!L_96)
		{
			goto IL_027e;
		}
	}
	{
		Object_t * L_97 = ___cmp;
		NullCheck(L_97);
		InterfaceActionInvoker0::Invoke(2 /* System.Void System.Text.RegularExpressions.ICompiler::EmitTrue() */, ICompiler_t3_246_il2cpp_TypeInfo_var, L_97);
		goto IL_0284;
	}

IL_027e:
	{
		Object_t * L_98 = ___cmp;
		NullCheck(L_98);
		InterfaceActionInvoker0::Invoke(1 /* System.Void System.Text.RegularExpressions.ICompiler::EmitFalse() */, ICompiler_t3_246_il2cpp_TypeInfo_var, L_98);
	}

IL_0284:
	{
		Object_t * L_99 = ___cmp;
		LinkRef_t3_193 * L_100 = V_3;
		NullCheck(L_99);
		InterfaceActionInvoker1< LinkRef_t3_193 * >::Invoke(29 /* System.Void System.Text.RegularExpressions.ICompiler::ResolveLink(System.Text.RegularExpressions.LinkRef) */, ICompiler_t3_246_il2cpp_TypeInfo_var, L_99, L_100);
	}

IL_028b:
	{
		return;
	}
}
// System.Void System.Text.RegularExpressions.Syntax.CharacterClass::GetWidth(System.Int32&,System.Int32&)
extern "C" void CharacterClass_GetWidth_m3_1698 (CharacterClass_t3_229 * __this, int32_t* ___min, int32_t* ___max, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t* L_0 = ___min;
		int32_t* L_1 = ___max;
		int32_t L_2 = 1;
		V_0 = L_2;
		*((int32_t*)(L_1)) = (int32_t)L_2;
		int32_t L_3 = V_0;
		*((int32_t*)(L_0)) = (int32_t)L_3;
		return;
	}
}
// System.Boolean System.Text.RegularExpressions.Syntax.CharacterClass::IsComplex()
extern "C" bool CharacterClass_IsComplex_m3_1699 (CharacterClass_t3_229 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Double System.Text.RegularExpressions.Syntax.CharacterClass::GetIntervalCost(System.Text.RegularExpressions.Interval)
extern "C" double CharacterClass_GetIntervalCost_m3_1700 (Object_t * __this /* static, unused */, Interval_t3_206  ___i, const MethodInfo* method)
{
	{
		bool L_0 = Interval_get_IsDiscontiguous_m3_1528((&___i), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = Interval_get_Size_m3_1531((&___i), /*hidden argument*/NULL);
		return (((double)((double)((int32_t)((int32_t)3+(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1+(int32_t)((int32_t)15)))>>(int32_t)4)))))));
	}

IL_001c:
	{
		bool L_2 = Interval_get_IsSingleton_m3_1529((&___i), /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0032;
		}
	}
	{
		return (2.0);
	}

IL_0032:
	{
		return (3.0);
	}
}
// System.Void System.Text.RegularExpressions.Syntax.AnchorInfo::.ctor(System.Text.RegularExpressions.Syntax.Expression,System.Int32)
extern "C" void AnchorInfo__ctor_m3_1701 (AnchorInfo_t3_230 * __this, Expression_t3_213 * ___expr, int32_t ___width, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		Expression_t3_213 * L_0 = ___expr;
		__this->___expr_0 = L_0;
		__this->___offset_2 = 0;
		int32_t L_1 = ___width;
		__this->___width_4 = L_1;
		__this->___str_3 = (String_t*)NULL;
		__this->___ignore_5 = 0;
		__this->___pos_1 = 0;
		return;
	}
}
// System.Void System.Text.RegularExpressions.Syntax.AnchorInfo::.ctor(System.Text.RegularExpressions.Syntax.Expression,System.Int32,System.Int32,System.String,System.Boolean)
extern "C" void AnchorInfo__ctor_m3_1702 (AnchorInfo_t3_230 * __this, Expression_t3_213 * ___expr, int32_t ___offset, int32_t ___width, String_t* ___str, bool ___ignore, const MethodInfo* method)
{
	AnchorInfo_t3_230 * G_B2_0 = {0};
	AnchorInfo_t3_230 * G_B1_0 = {0};
	String_t* G_B3_0 = {0};
	AnchorInfo_t3_230 * G_B3_1 = {0};
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		Expression_t3_213 * L_0 = ___expr;
		__this->___expr_0 = L_0;
		int32_t L_1 = ___offset;
		__this->___offset_2 = L_1;
		int32_t L_2 = ___width;
		__this->___width_4 = L_2;
		bool L_3 = ___ignore;
		G_B1_0 = __this;
		if (!L_3)
		{
			G_B2_0 = __this;
			goto IL_002f;
		}
	}
	{
		String_t* L_4 = ___str;
		NullCheck(L_4);
		String_t* L_5 = String_ToLower_m1_540(L_4, /*hidden argument*/NULL);
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		goto IL_0031;
	}

IL_002f:
	{
		String_t* L_6 = ___str;
		G_B3_0 = L_6;
		G_B3_1 = G_B2_0;
	}

IL_0031:
	{
		NullCheck(G_B3_1);
		G_B3_1->___str_3 = G_B3_0;
		bool L_7 = ___ignore;
		__this->___ignore_5 = L_7;
		__this->___pos_1 = 0;
		return;
	}
}
// System.Void System.Text.RegularExpressions.Syntax.AnchorInfo::.ctor(System.Text.RegularExpressions.Syntax.Expression,System.Int32,System.Int32,System.Text.RegularExpressions.Position)
extern "C" void AnchorInfo__ctor_m3_1703 (AnchorInfo_t3_230 * __this, Expression_t3_213 * ___expr, int32_t ___offset, int32_t ___width, uint16_t ___pos, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		Expression_t3_213 * L_0 = ___expr;
		__this->___expr_0 = L_0;
		int32_t L_1 = ___offset;
		__this->___offset_2 = L_1;
		int32_t L_2 = ___width;
		__this->___width_4 = L_2;
		uint16_t L_3 = ___pos;
		__this->___pos_1 = L_3;
		__this->___str_3 = (String_t*)NULL;
		__this->___ignore_5 = 0;
		return;
	}
}
// System.Int32 System.Text.RegularExpressions.Syntax.AnchorInfo::get_Offset()
extern "C" int32_t AnchorInfo_get_Offset_m3_1704 (AnchorInfo_t3_230 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___offset_2);
		return L_0;
	}
}
// System.Int32 System.Text.RegularExpressions.Syntax.AnchorInfo::get_Width()
extern "C" int32_t AnchorInfo_get_Width_m3_1705 (AnchorInfo_t3_230 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___width_4);
		return L_0;
	}
}
// System.Int32 System.Text.RegularExpressions.Syntax.AnchorInfo::get_Length()
extern "C" int32_t AnchorInfo_get_Length_m3_1706 (AnchorInfo_t3_230 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		String_t* L_0 = (__this->___str_3);
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		String_t* L_1 = (__this->___str_3);
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m1_571(L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		goto IL_001c;
	}

IL_001b:
	{
		G_B3_0 = 0;
	}

IL_001c:
	{
		return G_B3_0;
	}
}
// System.Boolean System.Text.RegularExpressions.Syntax.AnchorInfo::get_IsUnknownWidth()
extern "C" bool AnchorInfo_get_IsUnknownWidth_m3_1707 (AnchorInfo_t3_230 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___width_4);
		return ((((int32_t)L_0) < ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean System.Text.RegularExpressions.Syntax.AnchorInfo::get_IsComplete()
extern "C" bool AnchorInfo_get_IsComplete_m3_1708 (AnchorInfo_t3_230 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = AnchorInfo_get_Length_m3_1706(__this, /*hidden argument*/NULL);
		int32_t L_1 = AnchorInfo_get_Width_m3_1705(__this, /*hidden argument*/NULL);
		return ((((int32_t)L_0) == ((int32_t)L_1))? 1 : 0);
	}
}
// System.String System.Text.RegularExpressions.Syntax.AnchorInfo::get_Substring()
extern "C" String_t* AnchorInfo_get_Substring_m3_1709 (AnchorInfo_t3_230 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___str_3);
		return L_0;
	}
}
// System.Boolean System.Text.RegularExpressions.Syntax.AnchorInfo::get_IgnoreCase()
extern "C" bool AnchorInfo_get_IgnoreCase_m3_1710 (AnchorInfo_t3_230 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___ignore_5);
		return L_0;
	}
}
// System.Text.RegularExpressions.Position System.Text.RegularExpressions.Syntax.AnchorInfo::get_Position()
extern "C" uint16_t AnchorInfo_get_Position_m3_1711 (AnchorInfo_t3_230 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = (__this->___pos_1);
		return L_0;
	}
}
// System.Boolean System.Text.RegularExpressions.Syntax.AnchorInfo::get_IsSubstring()
extern "C" bool AnchorInfo_get_IsSubstring_m3_1712 (AnchorInfo_t3_230 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___str_3);
		return ((((int32_t)((((Object_t*)(String_t*)L_0) == ((Object_t*)(Object_t *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean System.Text.RegularExpressions.Syntax.AnchorInfo::get_IsPosition()
extern "C" bool AnchorInfo_get_IsPosition_m3_1713 (AnchorInfo_t3_230 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = (__this->___pos_1);
		return ((((int32_t)((((int32_t)L_0) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Text.RegularExpressions.Interval System.Text.RegularExpressions.Syntax.AnchorInfo::GetInterval(System.Int32)
extern "C" Interval_t3_206  AnchorInfo_GetInterval_m3_1714 (AnchorInfo_t3_230 * __this, int32_t ___start, const MethodInfo* method)
{
	{
		bool L_0 = AnchorInfo_get_IsSubstring_m3_1712(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		Interval_t3_206  L_1 = Interval_get_Empty_m3_1527(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_1;
	}

IL_0011:
	{
		int32_t L_2 = ___start;
		int32_t L_3 = AnchorInfo_get_Offset_m3_1704(__this, /*hidden argument*/NULL);
		int32_t L_4 = ___start;
		int32_t L_5 = AnchorInfo_get_Offset_m3_1704(__this, /*hidden argument*/NULL);
		int32_t L_6 = AnchorInfo_get_Length_m3_1706(__this, /*hidden argument*/NULL);
		Interval_t3_206  L_7 = {0};
		Interval__ctor_m3_1526(&L_7, ((int32_t)((int32_t)L_2+(int32_t)L_3)), ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_4+(int32_t)L_5))+(int32_t)L_6))-(int32_t)1)), /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void System.DefaultUriParser::.ctor()
extern TypeInfo* UriParser_t3_232_il2cpp_TypeInfo_var;
extern "C" void DefaultUriParser__ctor_m3_1715 (DefaultUriParser_t3_231 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UriParser_t3_232_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1551);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UriParser_t3_232_il2cpp_TypeInfo_var);
		UriParser__ctor_m3_1777(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.DefaultUriParser::.ctor(System.String)
extern TypeInfo* UriParser_t3_232_il2cpp_TypeInfo_var;
extern "C" void DefaultUriParser__ctor_m3_1716 (DefaultUriParser_t3_231 * __this, String_t* ___scheme, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UriParser_t3_232_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1551);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UriParser_t3_232_il2cpp_TypeInfo_var);
		UriParser__ctor_m3_1777(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___scheme;
		((UriParser_t3_232 *)__this)->___scheme_name_2 = L_0;
		return;
	}
}
// System.Void System.Uri/UriScheme::.ctor(System.String,System.String,System.Int32)
extern "C" void UriScheme__ctor_m3_1717 (UriScheme_t3_234 * __this, String_t* ___s, String_t* ___d, int32_t ___p, const MethodInfo* method)
{
	{
		String_t* L_0 = ___s;
		__this->___scheme_0 = L_0;
		String_t* L_1 = ___d;
		__this->___delimiter_1 = L_1;
		int32_t L_2 = ___p;
		__this->___defaultPort_2 = L_2;
		return;
	}
}
// Conversion methods for marshalling of: System.Uri/UriScheme
extern "C" void UriScheme_t3_234_marshal(const UriScheme_t3_234& unmarshaled, UriScheme_t3_234_marshaled& marshaled)
{
	marshaled.___scheme_0 = il2cpp_codegen_marshal_string(unmarshaled.___scheme_0);
	marshaled.___delimiter_1 = il2cpp_codegen_marshal_string(unmarshaled.___delimiter_1);
	marshaled.___defaultPort_2 = unmarshaled.___defaultPort_2;
}
extern "C" void UriScheme_t3_234_marshal_back(const UriScheme_t3_234_marshaled& marshaled, UriScheme_t3_234& unmarshaled)
{
	unmarshaled.___scheme_0 = il2cpp_codegen_marshal_string_result(marshaled.___scheme_0);
	unmarshaled.___delimiter_1 = il2cpp_codegen_marshal_string_result(marshaled.___delimiter_1);
	unmarshaled.___defaultPort_2 = marshaled.___defaultPort_2;
}
// Conversion method for clean up from marshalling of: System.Uri/UriScheme
extern "C" void UriScheme_t3_234_marshal_cleanup(UriScheme_t3_234_marshaled& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___scheme_0);
	marshaled.___scheme_0 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___delimiter_1);
	marshaled.___delimiter_1 = NULL;
}
// System.Void System.Uri::.ctor(System.String)
extern "C" void Uri__ctor_m3_23 (Uri_t3_3 * __this, String_t* ___uriString, const MethodInfo* method)
{
	{
		String_t* L_0 = ___uriString;
		Uri__ctor_m3_1719(__this, L_0, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Uri::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern Il2CppCodeGenString* _stringLiteral4661;
extern "C" void Uri__ctor_m3_1718 (Uri_t3_3 * __this, SerializationInfo_t1_293 * ___serializationInfo, StreamingContext_t1_1050  ___streamingContext, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral4661 = il2cpp_codegen_string_literal_from_index(4661);
		s_Il2CppMethodIntialized = true;
	}
	{
		SerializationInfo_t1_293 * L_0 = ___serializationInfo;
		NullCheck(L_0);
		String_t* L_1 = SerializationInfo_GetString_m1_9654(L_0, _stringLiteral4661, /*hidden argument*/NULL);
		Uri__ctor_m3_1719(__this, L_1, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Uri::.ctor(System.String,System.UriKind)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* UriFormatException_t3_19_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* UriKind_t3_237_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4662;
extern Il2CppCodeGenString* _stringLiteral4663;
extern Il2CppCodeGenString* _stringLiteral4664;
extern "C" void Uri__ctor_m3_27 (Uri_t3_3 * __this, String_t* ___uriString, int32_t ___uriKind, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		UriFormatException_t3_19_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1299);
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		UriKind_t3_237_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1552);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		_stringLiteral4662 = il2cpp_codegen_string_literal_from_index(4662);
		_stringLiteral4663 = il2cpp_codegen_string_literal_from_index(4663);
		_stringLiteral4664 = il2cpp_codegen_string_literal_from_index(4664);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	int32_t V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___scheme_2 = L_0;
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___host_3 = L_1;
		__this->___port_4 = (-1);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___path_5 = L_2;
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___query_6 = L_3;
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___fragment_7 = L_4;
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___userinfo_8 = L_5;
		__this->___isAbsoluteUri_11 = 1;
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		String_t* L_6 = ___uriString;
		__this->___source_1 = L_6;
		int32_t L_7 = ___uriKind;
		Uri_ParseUri_m3_1752(__this, L_7, /*hidden argument*/NULL);
		int32_t L_8 = ___uriKind;
		V_1 = L_8;
		int32_t L_9 = V_1;
		if (L_9 == 0)
		{
			goto IL_00b3;
		}
		if (L_9 == 1)
		{
			goto IL_007d;
		}
		if (L_9 == 2)
		{
			goto IL_0098;
		}
	}
	{
		goto IL_00b8;
	}

IL_007d:
	{
		bool L_10 = Uri_get_IsAbsoluteUri_m3_32(__this, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_0093;
		}
	}
	{
		UriFormatException_t3_19 * L_11 = (UriFormatException_t3_19 *)il2cpp_codegen_object_new (UriFormatException_t3_19_il2cpp_TypeInfo_var);
		UriFormatException__ctor_m3_1774(L_11, _stringLiteral4662, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_11);
	}

IL_0093:
	{
		goto IL_00d9;
	}

IL_0098:
	{
		bool L_12 = Uri_get_IsAbsoluteUri_m3_32(__this, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_00ae;
		}
	}
	{
		UriFormatException_t3_19 * L_13 = (UriFormatException_t3_19 *)il2cpp_codegen_object_new (UriFormatException_t3_19_il2cpp_TypeInfo_var);
		UriFormatException__ctor_m3_1774(L_13, _stringLiteral4663, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_13);
	}

IL_00ae:
	{
		goto IL_00d9;
	}

IL_00b3:
	{
		goto IL_00d9;
	}

IL_00b8:
	{
		ObjectU5BU5D_t1_272* L_14 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 1));
		int32_t L_15 = ___uriKind;
		int32_t L_16 = L_15;
		Object_t * L_17 = Box(UriKind_t3_237_il2cpp_TypeInfo_var, &L_16);
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 0);
		ArrayElementTypeCheck (L_14, L_17);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_14, 0, sizeof(Object_t *))) = (Object_t *)L_17;
		String_t* L_18 = Locale_GetText_m3_38(NULL /*static, unused*/, _stringLiteral4664, L_14, /*hidden argument*/NULL);
		V_0 = L_18;
		String_t* L_19 = V_0;
		ArgumentException_t1_1425 * L_20 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13260(L_20, L_19, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_20);
	}

IL_00d9:
	{
		return;
	}
}
// System.Void System.Uri::.ctor(System.String,System.Boolean)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* UriFormatException_t3_19_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4665;
extern "C" void Uri__ctor_m3_1719 (Uri_t3_3 * __this, String_t* ___uriString, bool ___dontEscape, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		UriFormatException_t3_19_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1299);
		_stringLiteral4665 = il2cpp_codegen_string_literal_from_index(4665);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___scheme_2 = L_0;
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___host_3 = L_1;
		__this->___port_4 = (-1);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___path_5 = L_2;
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___query_6 = L_3;
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___fragment_7 = L_4;
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___userinfo_8 = L_5;
		__this->___isAbsoluteUri_11 = 1;
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		bool L_6 = ___dontEscape;
		__this->___userEscaped_13 = L_6;
		String_t* L_7 = ___uriString;
		__this->___source_1 = L_7;
		Uri_ParseUri_m3_1752(__this, 1, /*hidden argument*/NULL);
		bool L_8 = (__this->___isAbsoluteUri_11);
		if (L_8)
		{
			goto IL_0087;
		}
	}
	{
		String_t* L_9 = ___uriString;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m1_559(NULL /*static, unused*/, _stringLiteral4665, L_9, /*hidden argument*/NULL);
		UriFormatException_t3_19 * L_11 = (UriFormatException_t3_19 *)il2cpp_codegen_object_new (UriFormatException_t3_19_il2cpp_TypeInfo_var);
		UriFormatException__ctor_m3_1774(L_11, L_10, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_11);
	}

IL_0087:
	{
		return;
	}
}
// System.Void System.Uri::.ctor(System.Uri,System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void Uri__ctor_m3_25 (Uri_t3_3 * __this, Uri_t3_3 * ___baseUri, String_t* ___relativeUri, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___scheme_2 = L_0;
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___host_3 = L_1;
		__this->___port_4 = (-1);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___path_5 = L_2;
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___query_6 = L_3;
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___fragment_7 = L_4;
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___userinfo_8 = L_5;
		__this->___isAbsoluteUri_11 = 1;
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		Uri_t3_3 * L_6 = ___baseUri;
		String_t* L_7 = ___relativeUri;
		Uri_Merge_m3_1722(__this, L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Uri::.cctor()
extern TypeInfo* Uri_t3_3_il2cpp_TypeInfo_var;
extern TypeInfo* UriSchemeU5BU5D_t3_235_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral727;
extern Il2CppCodeGenString* _stringLiteral728;
extern Il2CppCodeGenString* _stringLiteral729;
extern Il2CppCodeGenString* _stringLiteral730;
extern Il2CppCodeGenString* _stringLiteral731;
extern Il2CppCodeGenString* _stringLiteral732;
extern Il2CppCodeGenString* _stringLiteral733;
extern Il2CppCodeGenString* _stringLiteral734;
extern Il2CppCodeGenString* _stringLiteral735;
extern Il2CppCodeGenString* _stringLiteral736;
extern Il2CppCodeGenString* _stringLiteral4666;
extern Il2CppCodeGenString* _stringLiteral4667;
extern Il2CppCodeGenString* _stringLiteral737;
extern "C" void Uri__cctor_m3_1720 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Uri_t3_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		UriSchemeU5BU5D_t3_235_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1553);
		_stringLiteral727 = il2cpp_codegen_string_literal_from_index(727);
		_stringLiteral728 = il2cpp_codegen_string_literal_from_index(728);
		_stringLiteral729 = il2cpp_codegen_string_literal_from_index(729);
		_stringLiteral730 = il2cpp_codegen_string_literal_from_index(730);
		_stringLiteral731 = il2cpp_codegen_string_literal_from_index(731);
		_stringLiteral732 = il2cpp_codegen_string_literal_from_index(732);
		_stringLiteral733 = il2cpp_codegen_string_literal_from_index(733);
		_stringLiteral734 = il2cpp_codegen_string_literal_from_index(734);
		_stringLiteral735 = il2cpp_codegen_string_literal_from_index(735);
		_stringLiteral736 = il2cpp_codegen_string_literal_from_index(736);
		_stringLiteral4666 = il2cpp_codegen_string_literal_from_index(4666);
		_stringLiteral4667 = il2cpp_codegen_string_literal_from_index(4667);
		_stringLiteral737 = il2cpp_codegen_string_literal_from_index(737);
		s_Il2CppMethodIntialized = true;
	}
	{
		((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___hexUpperChars_18 = _stringLiteral727;
		((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___SchemeDelimiter_19 = _stringLiteral728;
		((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___UriSchemeFile_20 = _stringLiteral729;
		((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___UriSchemeFtp_21 = _stringLiteral730;
		((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___UriSchemeGopher_22 = _stringLiteral731;
		((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___UriSchemeHttp_23 = _stringLiteral732;
		((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___UriSchemeHttps_24 = _stringLiteral733;
		((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___UriSchemeMailto_25 = _stringLiteral734;
		((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___UriSchemeNews_26 = _stringLiteral735;
		((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___UriSchemeNntp_27 = _stringLiteral736;
		((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___UriSchemeNetPipe_28 = _stringLiteral4666;
		((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___UriSchemeNetTcp_29 = _stringLiteral4667;
		UriSchemeU5BU5D_t3_235* L_0 = ((UriSchemeU5BU5D_t3_235*)SZArrayNew(UriSchemeU5BU5D_t3_235_il2cpp_TypeInfo_var, 8));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		String_t* L_1 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___UriSchemeHttp_23;
		String_t* L_2 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___SchemeDelimiter_19;
		UriScheme_t3_234  L_3 = {0};
		UriScheme__ctor_m3_1717(&L_3, L_1, L_2, ((int32_t)80), /*hidden argument*/NULL);
		(*(UriScheme_t3_234 *)((UriScheme_t3_234 *)(UriScheme_t3_234 *)SZArrayLdElema(L_0, 0, sizeof(UriScheme_t3_234 )))) = L_3;
		UriSchemeU5BU5D_t3_235* L_4 = L_0;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		String_t* L_5 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___UriSchemeHttps_24;
		String_t* L_6 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___SchemeDelimiter_19;
		UriScheme_t3_234  L_7 = {0};
		UriScheme__ctor_m3_1717(&L_7, L_5, L_6, ((int32_t)443), /*hidden argument*/NULL);
		(*(UriScheme_t3_234 *)((UriScheme_t3_234 *)(UriScheme_t3_234 *)SZArrayLdElema(L_4, 1, sizeof(UriScheme_t3_234 )))) = L_7;
		UriSchemeU5BU5D_t3_235* L_8 = L_4;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		String_t* L_9 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___UriSchemeFtp_21;
		String_t* L_10 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___SchemeDelimiter_19;
		UriScheme_t3_234  L_11 = {0};
		UriScheme__ctor_m3_1717(&L_11, L_9, L_10, ((int32_t)21), /*hidden argument*/NULL);
		(*(UriScheme_t3_234 *)((UriScheme_t3_234 *)(UriScheme_t3_234 *)SZArrayLdElema(L_8, 2, sizeof(UriScheme_t3_234 )))) = L_11;
		UriSchemeU5BU5D_t3_235* L_12 = L_8;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		String_t* L_13 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___UriSchemeFile_20;
		String_t* L_14 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___SchemeDelimiter_19;
		UriScheme_t3_234  L_15 = {0};
		UriScheme__ctor_m3_1717(&L_15, L_13, L_14, (-1), /*hidden argument*/NULL);
		(*(UriScheme_t3_234 *)((UriScheme_t3_234 *)(UriScheme_t3_234 *)SZArrayLdElema(L_12, 3, sizeof(UriScheme_t3_234 )))) = L_15;
		UriSchemeU5BU5D_t3_235* L_16 = L_12;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 4);
		String_t* L_17 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___UriSchemeMailto_25;
		UriScheme_t3_234  L_18 = {0};
		UriScheme__ctor_m3_1717(&L_18, L_17, _stringLiteral737, ((int32_t)25), /*hidden argument*/NULL);
		(*(UriScheme_t3_234 *)((UriScheme_t3_234 *)(UriScheme_t3_234 *)SZArrayLdElema(L_16, 4, sizeof(UriScheme_t3_234 )))) = L_18;
		UriSchemeU5BU5D_t3_235* L_19 = L_16;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 5);
		String_t* L_20 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___UriSchemeNews_26;
		UriScheme_t3_234  L_21 = {0};
		UriScheme__ctor_m3_1717(&L_21, L_20, _stringLiteral737, ((int32_t)119), /*hidden argument*/NULL);
		(*(UriScheme_t3_234 *)((UriScheme_t3_234 *)(UriScheme_t3_234 *)SZArrayLdElema(L_19, 5, sizeof(UriScheme_t3_234 )))) = L_21;
		UriSchemeU5BU5D_t3_235* L_22 = L_19;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, 6);
		String_t* L_23 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___UriSchemeNntp_27;
		String_t* L_24 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___SchemeDelimiter_19;
		UriScheme_t3_234  L_25 = {0};
		UriScheme__ctor_m3_1717(&L_25, L_23, L_24, ((int32_t)119), /*hidden argument*/NULL);
		(*(UriScheme_t3_234 *)((UriScheme_t3_234 *)(UriScheme_t3_234 *)SZArrayLdElema(L_22, 6, sizeof(UriScheme_t3_234 )))) = L_25;
		UriSchemeU5BU5D_t3_235* L_26 = L_22;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, 7);
		String_t* L_27 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___UriSchemeGopher_22;
		String_t* L_28 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___SchemeDelimiter_19;
		UriScheme_t3_234  L_29 = {0};
		UriScheme__ctor_m3_1717(&L_29, L_27, L_28, ((int32_t)70), /*hidden argument*/NULL);
		(*(UriScheme_t3_234 *)((UriScheme_t3_234 *)(UriScheme_t3_234 *)SZArrayLdElema(L_26, 7, sizeof(UriScheme_t3_234 )))) = L_29;
		((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___schemes_30 = L_26;
		return;
	}
}
// System.Void System.Uri::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern Il2CppCodeGenString* _stringLiteral4661;
extern "C" void Uri_System_Runtime_Serialization_ISerializable_GetObjectData_m3_1721 (Uri_t3_3 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral4661 = il2cpp_codegen_string_literal_from_index(4661);
		s_Il2CppMethodIntialized = true;
	}
	{
		SerializationInfo_t1_293 * L_0 = ___info;
		String_t* L_1 = Uri_get_AbsoluteUri_m3_1723(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		SerializationInfo_AddValue_m1_9642(L_0, _stringLiteral4661, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Uri::Merge(System.Uri,System.String)
extern TypeInfo* Uri_t3_3_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t1_16_il2cpp_TypeInfo_var;
extern TypeInfo* Char_t1_15_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral721;
extern Il2CppCodeGenString* _stringLiteral1111;
extern Il2CppCodeGenString* _stringLiteral724;
extern Il2CppCodeGenString* _stringLiteral725;
extern Il2CppCodeGenString* _stringLiteral321;
extern Il2CppCodeGenString* _stringLiteral726;
extern "C" void Uri_Merge_m3_1722 (Uri_t3_3 * __this, Uri_t3_3 * ___baseUri, String_t* ___relativeUri, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Uri_t3_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		CharU5BU5D_t1_16_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(15);
		Char_t1_15_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(12);
		_stringLiteral721 = il2cpp_codegen_string_literal_from_index(721);
		_stringLiteral1111 = il2cpp_codegen_string_literal_from_index(1111);
		_stringLiteral724 = il2cpp_codegen_string_literal_from_index(724);
		_stringLiteral725 = il2cpp_codegen_string_literal_from_index(725);
		_stringLiteral321 = il2cpp_codegen_string_literal_from_index(321);
		_stringLiteral726 = il2cpp_codegen_string_literal_from_index(726);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		Uri_t3_3 * L_0 = ___baseUri;
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		bool L_1 = Uri_op_Equality_m3_26(NULL /*static, unused*/, L_0, (Uri_t3_3 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_2 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_2, _stringLiteral721, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		Uri_t3_3 * L_3 = ___baseUri;
		NullCheck(L_3);
		bool L_4 = Uri_get_IsAbsoluteUri_m3_32(L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_002d;
		}
	}
	{
		ArgumentOutOfRangeException_t1_1501 * L_5 = (ArgumentOutOfRangeException_t1_1501 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1_13274(L_5, _stringLiteral721, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_5);
	}

IL_002d:
	{
		String_t* L_6 = ___relativeUri;
		if (L_6)
		{
			goto IL_003a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		___relativeUri = L_7;
	}

IL_003a:
	{
		String_t* L_8 = ___relativeUri;
		NullCheck(L_8);
		int32_t L_9 = String_get_Length_m1_571(L_8, /*hidden argument*/NULL);
		if ((((int32_t)L_9) < ((int32_t)2)))
		{
			goto IL_0071;
		}
	}
	{
		String_t* L_10 = ___relativeUri;
		NullCheck(L_10);
		uint16_t L_11 = String_get_Chars_m1_442(L_10, 0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_11) == ((uint32_t)((int32_t)92)))))
		{
			goto IL_0071;
		}
	}
	{
		String_t* L_12 = ___relativeUri;
		NullCheck(L_12);
		uint16_t L_13 = String_get_Chars_m1_442(L_12, 1, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_13) == ((uint32_t)((int32_t)92)))))
		{
			goto IL_0071;
		}
	}
	{
		String_t* L_14 = ___relativeUri;
		__this->___source_1 = L_14;
		Uri_ParseUri_m3_1752(__this, 1, /*hidden argument*/NULL);
		return;
	}

IL_0071:
	{
		String_t* L_15 = ___relativeUri;
		NullCheck(L_15);
		int32_t L_16 = String_IndexOf_m1_500(L_15, ((int32_t)58), /*hidden argument*/NULL);
		V_0 = L_16;
		int32_t L_17 = V_0;
		if ((((int32_t)L_17) == ((int32_t)(-1))))
		{
			goto IL_0107;
		}
	}
	{
		String_t* L_18 = ___relativeUri;
		CharU5BU5D_t1_16* L_19 = ((CharU5BU5D_t1_16*)SZArrayNew(CharU5BU5D_t1_16_il2cpp_TypeInfo_var, 3));
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_19, 0, sizeof(uint16_t))) = (uint16_t)((int32_t)47);
		CharU5BU5D_t1_16* L_20 = L_19;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 1);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_20, 1, sizeof(uint16_t))) = (uint16_t)((int32_t)92);
		CharU5BU5D_t1_16* L_21 = L_20;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 2);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_21, 2, sizeof(uint16_t))) = (uint16_t)((int32_t)63);
		NullCheck(L_18);
		int32_t L_22 = String_IndexOfAny_m1_484(L_18, L_21, /*hidden argument*/NULL);
		V_1 = L_22;
		int32_t L_23 = V_1;
		int32_t L_24 = V_0;
		if ((((int32_t)L_23) > ((int32_t)L_24)))
		{
			goto IL_00ab;
		}
	}
	{
		int32_t L_25 = V_1;
		if ((((int32_t)L_25) >= ((int32_t)0)))
		{
			goto IL_0107;
		}
	}

IL_00ab:
	{
		Uri_t3_3 * L_26 = ___baseUri;
		NullCheck(L_26);
		String_t* L_27 = Uri_get_Scheme_m3_33(L_26, /*hidden argument*/NULL);
		String_t* L_28 = ___relativeUri;
		int32_t L_29 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		int32_t L_30 = String_CompareOrdinal_m1_478(NULL /*static, unused*/, L_27, 0, L_28, 0, L_29, /*hidden argument*/NULL);
		if (L_30)
		{
			goto IL_00ed;
		}
	}
	{
		Uri_t3_3 * L_31 = ___baseUri;
		NullCheck(L_31);
		String_t* L_32 = Uri_get_Scheme_m3_33(L_31, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		bool L_33 = Uri_IsPredefinedScheme_m3_1766(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_00ed;
		}
	}
	{
		String_t* L_34 = ___relativeUri;
		NullCheck(L_34);
		int32_t L_35 = String_get_Length_m1_571(L_34, /*hidden argument*/NULL);
		int32_t L_36 = V_0;
		if ((((int32_t)L_35) <= ((int32_t)((int32_t)((int32_t)L_36+(int32_t)1)))))
		{
			goto IL_00fc;
		}
	}
	{
		String_t* L_37 = ___relativeUri;
		int32_t L_38 = V_0;
		NullCheck(L_37);
		uint16_t L_39 = String_get_Chars_m1_442(L_37, ((int32_t)((int32_t)L_38+(int32_t)1)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_39) == ((uint32_t)((int32_t)47)))))
		{
			goto IL_00fc;
		}
	}

IL_00ed:
	{
		String_t* L_40 = ___relativeUri;
		__this->___source_1 = L_40;
		Uri_ParseUri_m3_1752(__this, 1, /*hidden argument*/NULL);
		return;
	}

IL_00fc:
	{
		String_t* L_41 = ___relativeUri;
		int32_t L_42 = V_0;
		NullCheck(L_41);
		String_t* L_43 = String_Substring_m1_454(L_41, ((int32_t)((int32_t)L_42+(int32_t)1)), /*hidden argument*/NULL);
		___relativeUri = L_43;
	}

IL_0107:
	{
		Uri_t3_3 * L_44 = ___baseUri;
		NullCheck(L_44);
		String_t* L_45 = (L_44->___scheme_2);
		__this->___scheme_2 = L_45;
		Uri_t3_3 * L_46 = ___baseUri;
		NullCheck(L_46);
		String_t* L_47 = (L_46->___host_3);
		__this->___host_3 = L_47;
		Uri_t3_3 * L_48 = ___baseUri;
		NullCheck(L_48);
		int32_t L_49 = (L_48->___port_4);
		__this->___port_4 = L_49;
		Uri_t3_3 * L_50 = ___baseUri;
		NullCheck(L_50);
		String_t* L_51 = (L_50->___userinfo_8);
		__this->___userinfo_8 = L_51;
		Uri_t3_3 * L_52 = ___baseUri;
		NullCheck(L_52);
		bool L_53 = (L_52->___isUnc_9);
		__this->___isUnc_9 = L_53;
		Uri_t3_3 * L_54 = ___baseUri;
		NullCheck(L_54);
		bool L_55 = (L_54->___isUnixFilePath_0);
		__this->___isUnixFilePath_0 = L_55;
		Uri_t3_3 * L_56 = ___baseUri;
		NullCheck(L_56);
		bool L_57 = (L_56->___isOpaquePart_10);
		__this->___isOpaquePart_10 = L_57;
		String_t* L_58 = ___relativeUri;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_59 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		bool L_60 = String_op_Equality_m1_601(NULL /*static, unused*/, L_58, L_59, /*hidden argument*/NULL);
		if (!L_60)
		{
			goto IL_0190;
		}
	}
	{
		Uri_t3_3 * L_61 = ___baseUri;
		NullCheck(L_61);
		String_t* L_62 = (L_61->___path_5);
		__this->___path_5 = L_62;
		Uri_t3_3 * L_63 = ___baseUri;
		NullCheck(L_63);
		String_t* L_64 = (L_63->___query_6);
		__this->___query_6 = L_64;
		Uri_t3_3 * L_65 = ___baseUri;
		NullCheck(L_65);
		String_t* L_66 = (L_65->___fragment_7);
		__this->___fragment_7 = L_66;
		return;
	}

IL_0190:
	{
		String_t* L_67 = ___relativeUri;
		NullCheck(L_67);
		int32_t L_68 = String_IndexOf_m1_500(L_67, ((int32_t)35), /*hidden argument*/NULL);
		V_0 = L_68;
		int32_t L_69 = V_0;
		if ((((int32_t)L_69) == ((int32_t)(-1))))
		{
			goto IL_01e5;
		}
	}
	{
		bool L_70 = (__this->___userEscaped_13);
		if (!L_70)
		{
			goto IL_01bd;
		}
	}
	{
		String_t* L_71 = ___relativeUri;
		int32_t L_72 = V_0;
		NullCheck(L_71);
		String_t* L_73 = String_Substring_m1_454(L_71, L_72, /*hidden argument*/NULL);
		__this->___fragment_7 = L_73;
		goto IL_01db;
	}

IL_01bd:
	{
		String_t* L_74 = ___relativeUri;
		int32_t L_75 = V_0;
		NullCheck(L_74);
		String_t* L_76 = String_Substring_m1_454(L_74, ((int32_t)((int32_t)L_75+(int32_t)1)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		String_t* L_77 = Uri_EscapeString_m3_1750(NULL /*static, unused*/, L_76, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_78 = String_Concat_m1_559(NULL /*static, unused*/, _stringLiteral1111, L_77, /*hidden argument*/NULL);
		__this->___fragment_7 = L_78;
	}

IL_01db:
	{
		String_t* L_79 = ___relativeUri;
		int32_t L_80 = V_0;
		NullCheck(L_79);
		String_t* L_81 = String_Substring_m1_455(L_79, 0, L_80, /*hidden argument*/NULL);
		___relativeUri = L_81;
	}

IL_01e5:
	{
		String_t* L_82 = ___relativeUri;
		NullCheck(L_82);
		int32_t L_83 = String_IndexOf_m1_500(L_82, ((int32_t)63), /*hidden argument*/NULL);
		V_0 = L_83;
		int32_t L_84 = V_0;
		if ((((int32_t)L_84) == ((int32_t)(-1))))
		{
			goto IL_0228;
		}
	}
	{
		String_t* L_85 = ___relativeUri;
		int32_t L_86 = V_0;
		NullCheck(L_85);
		String_t* L_87 = String_Substring_m1_454(L_85, L_86, /*hidden argument*/NULL);
		__this->___query_6 = L_87;
		bool L_88 = (__this->___userEscaped_13);
		if (L_88)
		{
			goto IL_021e;
		}
	}
	{
		String_t* L_89 = (__this->___query_6);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		String_t* L_90 = Uri_EscapeString_m3_1750(NULL /*static, unused*/, L_89, /*hidden argument*/NULL);
		__this->___query_6 = L_90;
	}

IL_021e:
	{
		String_t* L_91 = ___relativeUri;
		int32_t L_92 = V_0;
		NullCheck(L_91);
		String_t* L_93 = String_Substring_m1_455(L_91, 0, L_92, /*hidden argument*/NULL);
		___relativeUri = L_93;
	}

IL_0228:
	{
		String_t* L_94 = ___relativeUri;
		NullCheck(L_94);
		int32_t L_95 = String_get_Length_m1_571(L_94, /*hidden argument*/NULL);
		if ((((int32_t)L_95) <= ((int32_t)0)))
		{
			goto IL_02a1;
		}
	}
	{
		String_t* L_96 = ___relativeUri;
		NullCheck(L_96);
		uint16_t L_97 = String_get_Chars_m1_442(L_96, 0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_97) == ((uint32_t)((int32_t)47)))))
		{
			goto IL_02a1;
		}
	}
	{
		String_t* L_98 = ___relativeUri;
		NullCheck(L_98);
		int32_t L_99 = String_get_Length_m1_571(L_98, /*hidden argument*/NULL);
		if ((((int32_t)L_99) <= ((int32_t)1)))
		{
			goto IL_027d;
		}
	}
	{
		String_t* L_100 = ___relativeUri;
		NullCheck(L_100);
		uint16_t L_101 = String_get_Chars_m1_442(L_100, 1, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_101) == ((uint32_t)((int32_t)47)))))
		{
			goto IL_027d;
		}
	}
	{
		String_t* L_102 = (__this->___scheme_2);
		uint16_t L_103 = ((int32_t)58);
		Object_t * L_104 = Box(Char_t1_15_il2cpp_TypeInfo_var, &L_103);
		String_t* L_105 = ___relativeUri;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_106 = String_Concat_m1_557(NULL /*static, unused*/, L_102, L_104, L_105, /*hidden argument*/NULL);
		__this->___source_1 = L_106;
		Uri_ParseUri_m3_1752(__this, 1, /*hidden argument*/NULL);
		return;
	}

IL_027d:
	{
		String_t* L_107 = ___relativeUri;
		__this->___path_5 = L_107;
		bool L_108 = (__this->___userEscaped_13);
		if (L_108)
		{
			goto IL_02a0;
		}
	}
	{
		String_t* L_109 = (__this->___path_5);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		String_t* L_110 = Uri_EscapeString_m3_1750(NULL /*static, unused*/, L_109, /*hidden argument*/NULL);
		__this->___path_5 = L_110;
	}

IL_02a0:
	{
		return;
	}

IL_02a1:
	{
		Uri_t3_3 * L_111 = ___baseUri;
		NullCheck(L_111);
		String_t* L_112 = (L_111->___path_5);
		__this->___path_5 = L_112;
		String_t* L_113 = ___relativeUri;
		NullCheck(L_113);
		int32_t L_114 = String_get_Length_m1_571(L_113, /*hidden argument*/NULL);
		if ((((int32_t)L_114) > ((int32_t)0)))
		{
			goto IL_02ca;
		}
	}
	{
		String_t* L_115 = (__this->___query_6);
		NullCheck(L_115);
		int32_t L_116 = String_get_Length_m1_571(L_115, /*hidden argument*/NULL);
		if ((((int32_t)L_116) <= ((int32_t)0)))
		{
			goto IL_02f4;
		}
	}

IL_02ca:
	{
		String_t* L_117 = (__this->___path_5);
		NullCheck(L_117);
		int32_t L_118 = String_LastIndexOf_m1_512(L_117, ((int32_t)47), /*hidden argument*/NULL);
		V_0 = L_118;
		int32_t L_119 = V_0;
		if ((((int32_t)L_119) < ((int32_t)0)))
		{
			goto IL_02f4;
		}
	}
	{
		String_t* L_120 = (__this->___path_5);
		int32_t L_121 = V_0;
		NullCheck(L_120);
		String_t* L_122 = String_Substring_m1_455(L_120, 0, ((int32_t)((int32_t)L_121+(int32_t)1)), /*hidden argument*/NULL);
		__this->___path_5 = L_122;
	}

IL_02f4:
	{
		String_t* L_123 = ___relativeUri;
		NullCheck(L_123);
		int32_t L_124 = String_get_Length_m1_571(L_123, /*hidden argument*/NULL);
		if (L_124)
		{
			goto IL_0300;
		}
	}
	{
		return;
	}

IL_0300:
	{
		String_t* L_125 = (__this->___path_5);
		String_t* L_126 = ___relativeUri;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_127 = String_Concat_m1_559(NULL /*static, unused*/, L_125, L_126, /*hidden argument*/NULL);
		__this->___path_5 = L_127;
		V_2 = 0;
	}

IL_0314:
	{
		String_t* L_128 = (__this->___path_5);
		int32_t L_129 = V_2;
		NullCheck(L_128);
		int32_t L_130 = String_IndexOf_m1_506(L_128, _stringLiteral724, L_129, /*hidden argument*/NULL);
		V_0 = L_130;
		int32_t L_131 = V_0;
		if ((!(((uint32_t)L_131) == ((uint32_t)(-1)))))
		{
			goto IL_0332;
		}
	}
	{
		goto IL_0386;
	}

IL_0332:
	{
		int32_t L_132 = V_0;
		if (L_132)
		{
			goto IL_0350;
		}
	}
	{
		String_t* L_133 = (__this->___path_5);
		NullCheck(L_133);
		String_t* L_134 = String_Remove_m1_539(L_133, 0, 2, /*hidden argument*/NULL);
		__this->___path_5 = L_134;
		goto IL_0381;
	}

IL_0350:
	{
		String_t* L_135 = (__this->___path_5);
		int32_t L_136 = V_0;
		NullCheck(L_135);
		uint16_t L_137 = String_get_Chars_m1_442(L_135, ((int32_t)((int32_t)L_136-(int32_t)1)), /*hidden argument*/NULL);
		if ((((int32_t)L_137) == ((int32_t)((int32_t)46))))
		{
			goto IL_037d;
		}
	}
	{
		String_t* L_138 = (__this->___path_5);
		int32_t L_139 = V_0;
		NullCheck(L_138);
		String_t* L_140 = String_Remove_m1_539(L_138, L_139, 2, /*hidden argument*/NULL);
		__this->___path_5 = L_140;
		goto IL_0381;
	}

IL_037d:
	{
		int32_t L_141 = V_0;
		V_2 = ((int32_t)((int32_t)L_141+(int32_t)1));
	}

IL_0381:
	{
		goto IL_0314;
	}

IL_0386:
	{
		String_t* L_142 = (__this->___path_5);
		NullCheck(L_142);
		int32_t L_143 = String_get_Length_m1_571(L_142, /*hidden argument*/NULL);
		if ((((int32_t)L_143) <= ((int32_t)1)))
		{
			goto IL_03f4;
		}
	}
	{
		String_t* L_144 = (__this->___path_5);
		String_t* L_145 = (__this->___path_5);
		NullCheck(L_145);
		int32_t L_146 = String_get_Length_m1_571(L_145, /*hidden argument*/NULL);
		NullCheck(L_144);
		uint16_t L_147 = String_get_Chars_m1_442(L_144, ((int32_t)((int32_t)L_146-(int32_t)1)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_147) == ((uint32_t)((int32_t)46)))))
		{
			goto IL_03f4;
		}
	}
	{
		String_t* L_148 = (__this->___path_5);
		String_t* L_149 = (__this->___path_5);
		NullCheck(L_149);
		int32_t L_150 = String_get_Length_m1_571(L_149, /*hidden argument*/NULL);
		NullCheck(L_148);
		uint16_t L_151 = String_get_Chars_m1_442(L_148, ((int32_t)((int32_t)L_150-(int32_t)2)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_151) == ((uint32_t)((int32_t)47)))))
		{
			goto IL_03f4;
		}
	}
	{
		String_t* L_152 = (__this->___path_5);
		String_t* L_153 = (__this->___path_5);
		NullCheck(L_153);
		int32_t L_154 = String_get_Length_m1_571(L_153, /*hidden argument*/NULL);
		NullCheck(L_152);
		String_t* L_155 = String_Remove_m1_539(L_152, ((int32_t)((int32_t)L_154-(int32_t)1)), 1, /*hidden argument*/NULL);
		__this->___path_5 = L_155;
	}

IL_03f4:
	{
		V_2 = 0;
	}

IL_03f6:
	{
		String_t* L_156 = (__this->___path_5);
		int32_t L_157 = V_2;
		NullCheck(L_156);
		int32_t L_158 = String_IndexOf_m1_506(L_156, _stringLiteral725, L_157, /*hidden argument*/NULL);
		V_0 = L_158;
		int32_t L_159 = V_0;
		if ((!(((uint32_t)L_159) == ((uint32_t)(-1)))))
		{
			goto IL_0414;
		}
	}
	{
		goto IL_048b;
	}

IL_0414:
	{
		int32_t L_160 = V_0;
		if (L_160)
		{
			goto IL_0421;
		}
	}
	{
		V_2 = 3;
		goto IL_03f6;
	}

IL_0421:
	{
		String_t* L_161 = (__this->___path_5);
		int32_t L_162 = V_0;
		NullCheck(L_161);
		int32_t L_163 = String_LastIndexOf_m1_513(L_161, ((int32_t)47), ((int32_t)((int32_t)L_162-(int32_t)1)), /*hidden argument*/NULL);
		V_3 = L_163;
		int32_t L_164 = V_3;
		if ((!(((uint32_t)L_164) == ((uint32_t)(-1)))))
		{
			goto IL_0442;
		}
	}
	{
		int32_t L_165 = V_0;
		V_2 = ((int32_t)((int32_t)L_165+(int32_t)1));
		goto IL_0486;
	}

IL_0442:
	{
		String_t* L_166 = (__this->___path_5);
		int32_t L_167 = V_3;
		int32_t L_168 = V_0;
		int32_t L_169 = V_3;
		NullCheck(L_166);
		String_t* L_170 = String_Substring_m1_455(L_166, ((int32_t)((int32_t)L_167+(int32_t)1)), ((int32_t)((int32_t)((int32_t)((int32_t)L_168-(int32_t)L_169))-(int32_t)1)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_171 = String_op_Inequality_m1_602(NULL /*static, unused*/, L_170, _stringLiteral321, /*hidden argument*/NULL);
		if (!L_171)
		{
			goto IL_0482;
		}
	}
	{
		String_t* L_172 = (__this->___path_5);
		int32_t L_173 = V_3;
		int32_t L_174 = V_0;
		int32_t L_175 = V_3;
		NullCheck(L_172);
		String_t* L_176 = String_Remove_m1_539(L_172, ((int32_t)((int32_t)L_173+(int32_t)1)), ((int32_t)((int32_t)((int32_t)((int32_t)L_174-(int32_t)L_175))+(int32_t)3)), /*hidden argument*/NULL);
		__this->___path_5 = L_176;
		goto IL_0486;
	}

IL_0482:
	{
		int32_t L_177 = V_0;
		V_2 = ((int32_t)((int32_t)L_177+(int32_t)1));
	}

IL_0486:
	{
		goto IL_03f6;
	}

IL_048b:
	{
		String_t* L_178 = (__this->___path_5);
		NullCheck(L_178);
		int32_t L_179 = String_get_Length_m1_571(L_178, /*hidden argument*/NULL);
		if ((((int32_t)L_179) <= ((int32_t)3)))
		{
			goto IL_0522;
		}
	}
	{
		String_t* L_180 = (__this->___path_5);
		NullCheck(L_180);
		bool L_181 = String_EndsWith_m1_482(L_180, _stringLiteral726, /*hidden argument*/NULL);
		if (!L_181)
		{
			goto IL_0522;
		}
	}
	{
		String_t* L_182 = (__this->___path_5);
		String_t* L_183 = (__this->___path_5);
		NullCheck(L_183);
		int32_t L_184 = String_get_Length_m1_571(L_183, /*hidden argument*/NULL);
		NullCheck(L_182);
		int32_t L_185 = String_LastIndexOf_m1_513(L_182, ((int32_t)47), ((int32_t)((int32_t)L_184-(int32_t)4)), /*hidden argument*/NULL);
		V_0 = L_185;
		int32_t L_186 = V_0;
		if ((((int32_t)L_186) == ((int32_t)(-1))))
		{
			goto IL_0522;
		}
	}
	{
		String_t* L_187 = (__this->___path_5);
		int32_t L_188 = V_0;
		String_t* L_189 = (__this->___path_5);
		NullCheck(L_189);
		int32_t L_190 = String_get_Length_m1_571(L_189, /*hidden argument*/NULL);
		int32_t L_191 = V_0;
		NullCheck(L_187);
		String_t* L_192 = String_Substring_m1_455(L_187, ((int32_t)((int32_t)L_188+(int32_t)1)), ((int32_t)((int32_t)((int32_t)((int32_t)L_190-(int32_t)L_191))-(int32_t)4)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_193 = String_op_Inequality_m1_602(NULL /*static, unused*/, L_192, _stringLiteral321, /*hidden argument*/NULL);
		if (!L_193)
		{
			goto IL_0522;
		}
	}
	{
		String_t* L_194 = (__this->___path_5);
		int32_t L_195 = V_0;
		String_t* L_196 = (__this->___path_5);
		NullCheck(L_196);
		int32_t L_197 = String_get_Length_m1_571(L_196, /*hidden argument*/NULL);
		int32_t L_198 = V_0;
		NullCheck(L_194);
		String_t* L_199 = String_Remove_m1_539(L_194, ((int32_t)((int32_t)L_195+(int32_t)1)), ((int32_t)((int32_t)((int32_t)((int32_t)L_197-(int32_t)L_198))-(int32_t)1)), /*hidden argument*/NULL);
		__this->___path_5 = L_199;
	}

IL_0522:
	{
		bool L_200 = (__this->___userEscaped_13);
		if (L_200)
		{
			goto IL_053e;
		}
	}
	{
		String_t* L_201 = (__this->___path_5);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		String_t* L_202 = Uri_EscapeString_m3_1750(NULL /*static, unused*/, L_201, /*hidden argument*/NULL);
		__this->___path_5 = L_202;
	}

IL_053e:
	{
		return;
	}
}
// System.String System.Uri::get_AbsolutePath()
extern TypeInfo* Uri_t3_3_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t1_92_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_14878_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral734;
extern Il2CppCodeGenString* _stringLiteral729;
extern Il2CppCodeGenString* _stringLiteral740;
extern "C" String_t* Uri_get_AbsolutePath_m3_34 (Uri_t3_3 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Uri_t3_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		Dictionary_2_t1_92_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(129);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Dictionary_2__ctor_m1_14878_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483658);
		_stringLiteral734 = il2cpp_codegen_string_literal_from_index(734);
		_stringLiteral729 = il2cpp_codegen_string_literal_from_index(729);
		_stringLiteral740 = il2cpp_codegen_string_literal_from_index(740);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	String_t* V_1 = {0};
	Dictionary_2_t1_92 * V_2 = {0};
	int32_t V_3 = 0;
	{
		Uri_EnsureAbsoluteUri_m3_1772(__this, /*hidden argument*/NULL);
		String_t* L_0 = Uri_get_Scheme_m3_33(__this, /*hidden argument*/NULL);
		V_1 = L_0;
		String_t* L_1 = V_1;
		if (!L_1)
		{
			goto IL_0066;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		Dictionary_2_t1_92 * L_2 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__switchU24map12_32;
		if (L_2)
		{
			goto IL_0042;
		}
	}
	{
		Dictionary_2_t1_92 * L_3 = (Dictionary_2_t1_92 *)il2cpp_codegen_object_new (Dictionary_2_t1_92_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_14878(L_3, 2, /*hidden argument*/Dictionary_2__ctor_m1_14878_MethodInfo_var);
		V_2 = L_3;
		Dictionary_2_t1_92 * L_4 = V_2;
		NullCheck(L_4);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_4, _stringLiteral734, 0);
		Dictionary_2_t1_92 * L_5 = V_2;
		NullCheck(L_5);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_5, _stringLiteral729, 0);
		Dictionary_2_t1_92 * L_6 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__switchU24map12_32 = L_6;
	}

IL_0042:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		Dictionary_2_t1_92 * L_7 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__switchU24map12_32;
		String_t* L_8 = V_1;
		NullCheck(L_7);
		bool L_9 = (bool)VirtFuncInvoker2< bool, String_t*, int32_t* >::Invoke(21 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int32>::TryGetValue(!0,!1&) */, L_7, L_8, (&V_3));
		if (!L_9)
		{
			goto IL_0066;
		}
	}
	{
		int32_t L_10 = V_3;
		if (!L_10)
		{
			goto IL_005f;
		}
	}
	{
		goto IL_0066;
	}

IL_005f:
	{
		String_t* L_11 = (__this->___path_5);
		return L_11;
	}

IL_0066:
	{
		String_t* L_12 = (__this->___path_5);
		NullCheck(L_12);
		int32_t L_13 = String_get_Length_m1_571(L_12, /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_00a4;
		}
	}
	{
		String_t* L_14 = Uri_get_Scheme_m3_33(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		String_t* L_15 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___SchemeDelimiter_19;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m1_559(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
		V_0 = L_16;
		String_t* L_17 = (__this->___path_5);
		String_t* L_18 = V_0;
		NullCheck(L_17);
		bool L_19 = String_StartsWith_m1_531(L_17, L_18, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_009e;
		}
	}
	{
		return _stringLiteral740;
	}

IL_009e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		return L_20;
	}

IL_00a4:
	{
		String_t* L_21 = (__this->___path_5);
		return L_21;
	}
}
// System.String System.Uri::get_AbsoluteUri()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" String_t* Uri_get_AbsoluteUri_m3_1723 (Uri_t3_3 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	{
		Uri_EnsureAbsoluteUri_m3_1772(__this, /*hidden argument*/NULL);
		String_t* L_0 = (__this->___cachedAbsoluteUri_14);
		if (L_0)
		{
			goto IL_006e;
		}
	}
	{
		String_t* L_1 = Uri_GetLeftPart_m3_1742(__this, 2, /*hidden argument*/NULL);
		__this->___cachedAbsoluteUri_14 = L_1;
		String_t* L_2 = (__this->___query_6);
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m1_571(L_2, /*hidden argument*/NULL);
		if ((((int32_t)L_3) <= ((int32_t)0)))
		{
			goto IL_0046;
		}
	}
	{
		String_t* L_4 = (__this->___cachedAbsoluteUri_14);
		String_t* L_5 = (__this->___query_6);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m1_559(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		__this->___cachedAbsoluteUri_14 = L_6;
	}

IL_0046:
	{
		String_t* L_7 = (__this->___fragment_7);
		NullCheck(L_7);
		int32_t L_8 = String_get_Length_m1_571(L_7, /*hidden argument*/NULL);
		if ((((int32_t)L_8) <= ((int32_t)0)))
		{
			goto IL_006e;
		}
	}
	{
		String_t* L_9 = (__this->___cachedAbsoluteUri_14);
		String_t* L_10 = (__this->___fragment_7);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Concat_m1_559(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		__this->___cachedAbsoluteUri_14 = L_11;
	}

IL_006e:
	{
		String_t* L_12 = (__this->___cachedAbsoluteUri_14);
		return L_12;
	}
}
// System.String System.Uri::get_Authority()
extern TypeInfo* Uri_t3_3_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral737;
extern "C" String_t* Uri_get_Authority_m3_1724 (Uri_t3_3 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Uri_t3_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral737 = il2cpp_codegen_string_literal_from_index(737);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B3_0 = {0};
	{
		Uri_EnsureAbsoluteUri_m3_1772(__this, /*hidden argument*/NULL);
		String_t* L_0 = Uri_get_Scheme_m3_33(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		int32_t L_1 = Uri_GetDefaultPort_m3_1764(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		int32_t L_2 = (__this->___port_4);
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_0027;
		}
	}
	{
		String_t* L_3 = (__this->___host_3);
		G_B3_0 = L_3;
		goto IL_0042;
	}

IL_0027:
	{
		String_t* L_4 = (__this->___host_3);
		int32_t L_5 = (__this->___port_4);
		int32_t L_6 = L_5;
		Object_t * L_7 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_6);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m1_557(NULL /*static, unused*/, L_4, _stringLiteral737, L_7, /*hidden argument*/NULL);
		G_B3_0 = L_8;
	}

IL_0042:
	{
		return G_B3_0;
	}
}
// System.String System.Uri::get_Host()
extern "C" String_t* Uri_get_Host_m3_2 (Uri_t3_3 * __this, const MethodInfo* method)
{
	{
		Uri_EnsureAbsoluteUri_m3_1772(__this, /*hidden argument*/NULL);
		String_t* L_0 = (__this->___host_3);
		return L_0;
	}
}
// System.UriHostNameType System.Uri::get_HostNameType()
extern TypeInfo* Uri_t3_3_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t1_92_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_14878_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral734;
extern "C" int32_t Uri_get_HostNameType_m3_1725 (Uri_t3_3 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Uri_t3_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		Dictionary_2_t1_92_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(129);
		Dictionary_2__ctor_m1_14878_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483658);
		_stringLiteral734 = il2cpp_codegen_string_literal_from_index(734);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	String_t* V_1 = {0};
	Dictionary_2_t1_92 * V_2 = {0};
	int32_t V_3 = 0;
	int32_t G_B12_0 = 0;
	{
		Uri_EnsureAbsoluteUri_m3_1772(__this, /*hidden argument*/NULL);
		String_t* L_0 = Uri_get_Host_m3_2(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		int32_t L_1 = Uri_CheckHostName_m3_1734(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_3 = V_0;
		return L_3;
	}

IL_001a:
	{
		String_t* L_4 = Uri_get_Scheme_m3_33(__this, /*hidden argument*/NULL);
		V_1 = L_4;
		String_t* L_5 = V_1;
		if (!L_5)
		{
			goto IL_0069;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		Dictionary_2_t1_92 * L_6 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__switchU24map13_33;
		if (L_6)
		{
			goto IL_004a;
		}
	}
	{
		Dictionary_2_t1_92 * L_7 = (Dictionary_2_t1_92 *)il2cpp_codegen_object_new (Dictionary_2_t1_92_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_14878(L_7, 1, /*hidden argument*/Dictionary_2__ctor_m1_14878_MethodInfo_var);
		V_2 = L_7;
		Dictionary_2_t1_92 * L_8 = V_2;
		NullCheck(L_8);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_8, _stringLiteral734, 0);
		Dictionary_2_t1_92 * L_9 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__switchU24map13_33 = L_9;
	}

IL_004a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		Dictionary_2_t1_92 * L_10 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__switchU24map13_33;
		String_t* L_11 = V_1;
		NullCheck(L_10);
		bool L_12 = (bool)VirtFuncInvoker2< bool, String_t*, int32_t* >::Invoke(21 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int32>::TryGetValue(!0,!1&) */, L_10, L_11, (&V_3));
		if (!L_12)
		{
			goto IL_0069;
		}
	}
	{
		int32_t L_13 = V_3;
		if (!L_13)
		{
			goto IL_0067;
		}
	}
	{
		goto IL_0069;
	}

IL_0067:
	{
		return (int32_t)(1);
	}

IL_0069:
	{
		bool L_14 = Uri_get_IsFile_m3_1727(__this, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_007a;
		}
	}
	{
		G_B12_0 = 1;
		goto IL_007b;
	}

IL_007a:
	{
		int32_t L_15 = V_0;
		G_B12_0 = ((int32_t)(L_15));
	}

IL_007b:
	{
		return (int32_t)(G_B12_0);
	}
}
// System.Boolean System.Uri::get_IsDefaultPort()
extern TypeInfo* Uri_t3_3_il2cpp_TypeInfo_var;
extern "C" bool Uri_get_IsDefaultPort_m3_1726 (Uri_t3_3 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Uri_t3_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		s_Il2CppMethodIntialized = true;
	}
	{
		Uri_EnsureAbsoluteUri_m3_1772(__this, /*hidden argument*/NULL);
		String_t* L_0 = Uri_get_Scheme_m3_33(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		int32_t L_1 = Uri_GetDefaultPort_m3_1764(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		int32_t L_2 = (__this->___port_4);
		return ((((int32_t)L_1) == ((int32_t)L_2))? 1 : 0);
	}
}
// System.Boolean System.Uri::get_IsFile()
extern TypeInfo* Uri_t3_3_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" bool Uri_get_IsFile_m3_1727 (Uri_t3_3 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Uri_t3_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	{
		Uri_EnsureAbsoluteUri_m3_1772(__this, /*hidden argument*/NULL);
		String_t* L_0 = Uri_get_Scheme_m3_33(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		String_t* L_1 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___UriSchemeFile_20;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Equality_m1_601(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean System.Uri::get_IsLoopback()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* IPAddress_t3_121_il2cpp_TypeInfo_var;
extern TypeInfo* IPv6Address_t3_122_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral738;
extern Il2CppCodeGenString* _stringLiteral739;
extern "C" bool Uri_get_IsLoopback_m3_1728 (Uri_t3_3 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		IPAddress_t3_121_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1401);
		IPv6Address_t3_122_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1454);
		_stringLiteral738 = il2cpp_codegen_string_literal_from_index(738);
		_stringLiteral739 = il2cpp_codegen_string_literal_from_index(739);
		s_Il2CppMethodIntialized = true;
	}
	IPAddress_t3_121 * V_0 = {0};
	IPv6Address_t3_122 * V_1 = {0};
	{
		Uri_EnsureAbsoluteUri_m3_1772(__this, /*hidden argument*/NULL);
		String_t* L_0 = Uri_get_Host_m3_2(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = String_get_Length_m1_571(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001d;
		}
	}
	{
		bool L_2 = Uri_get_IsFile_m3_1727(__this, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		String_t* L_3 = (__this->___host_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_op_Equality_m1_601(NULL /*static, unused*/, L_3, _stringLiteral738, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0047;
		}
	}
	{
		String_t* L_5 = (__this->___host_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_6 = String_op_Equality_m1_601(NULL /*static, unused*/, L_5, _stringLiteral739, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0049;
		}
	}

IL_0047:
	{
		return 1;
	}

IL_0049:
	{
		String_t* L_7 = (__this->___host_3);
		IL2CPP_RUNTIME_CLASS_INIT(IPAddress_t3_121_il2cpp_TypeInfo_var);
		bool L_8 = IPAddress_TryParse_m3_773(NULL /*static, unused*/, L_7, (&V_0), /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_006d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IPAddress_t3_121_il2cpp_TypeInfo_var);
		IPAddress_t3_121 * L_9 = ((IPAddress_t3_121_StaticFields*)IPAddress_t3_121_il2cpp_TypeInfo_var->static_fields)->___Loopback_6;
		IPAddress_t3_121 * L_10 = V_0;
		NullCheck(L_9);
		bool L_11 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Net.IPAddress::Equals(System.Object) */, L_9, L_10);
		if (!L_11)
		{
			goto IL_006d;
		}
	}
	{
		return 1;
	}

IL_006d:
	{
		String_t* L_12 = (__this->___host_3);
		IL2CPP_RUNTIME_CLASS_INIT(IPv6Address_t3_122_il2cpp_TypeInfo_var);
		bool L_13 = IPv6Address_TryParse_m3_810(NULL /*static, unused*/, L_12, (&V_1), /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_008c;
		}
	}
	{
		IPv6Address_t3_122 * L_14 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(IPv6Address_t3_122_il2cpp_TypeInfo_var);
		bool L_15 = IPv6Address_IsLoopback_m3_814(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_008c;
		}
	}
	{
		return 1;
	}

IL_008c:
	{
		return 0;
	}
}
// System.Boolean System.Uri::get_IsUnc()
extern "C" bool Uri_get_IsUnc_m3_1729 (Uri_t3_3 * __this, const MethodInfo* method)
{
	{
		Uri_EnsureAbsoluteUri_m3_1772(__this, /*hidden argument*/NULL);
		bool L_0 = (__this->___isUnc_9);
		return L_0;
	}
}
// System.String System.Uri::get_LocalPath()
extern TypeInfo* Path_t1_437_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral723;
extern "C" String_t* Uri_get_LocalPath_m3_35 (Uri_t3_3 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Path_t1_437_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(131);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral723 = il2cpp_codegen_string_literal_from_index(723);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	String_t* V_1 = {0};
	bool V_2 = false;
	String_t* V_3 = {0};
	uint16_t V_4 = 0x0;
	int32_t G_B9_0 = 0;
	int32_t G_B11_0 = 0;
	{
		Uri_EnsureAbsoluteUri_m3_1772(__this, /*hidden argument*/NULL);
		String_t* L_0 = (__this->___cachedLocalPath_16);
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		String_t* L_1 = (__this->___cachedLocalPath_16);
		return L_1;
	}

IL_0018:
	{
		bool L_2 = Uri_get_IsFile_m3_1727(__this, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_002a;
		}
	}
	{
		String_t* L_3 = Uri_get_AbsolutePath_m3_34(__this, /*hidden argument*/NULL);
		return L_3;
	}

IL_002a:
	{
		String_t* L_4 = (__this->___path_5);
		NullCheck(L_4);
		int32_t L_5 = String_get_Length_m1_571(L_4, /*hidden argument*/NULL);
		if ((((int32_t)L_5) <= ((int32_t)3)))
		{
			goto IL_0076;
		}
	}
	{
		String_t* L_6 = (__this->___path_5);
		NullCheck(L_6);
		uint16_t L_7 = String_get_Chars_m1_442(L_6, 1, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_7) == ((uint32_t)((int32_t)58)))))
		{
			goto IL_0076;
		}
	}
	{
		String_t* L_8 = (__this->___path_5);
		NullCheck(L_8);
		uint16_t L_9 = String_get_Chars_m1_442(L_8, 2, /*hidden argument*/NULL);
		if ((((int32_t)L_9) == ((int32_t)((int32_t)92))))
		{
			goto IL_0073;
		}
	}
	{
		String_t* L_10 = (__this->___path_5);
		NullCheck(L_10);
		uint16_t L_11 = String_get_Chars_m1_442(L_10, 2, /*hidden argument*/NULL);
		G_B9_0 = ((((int32_t)L_11) == ((int32_t)((int32_t)47)))? 1 : 0);
		goto IL_0074;
	}

IL_0073:
	{
		G_B9_0 = 1;
	}

IL_0074:
	{
		G_B11_0 = G_B9_0;
		goto IL_0077;
	}

IL_0076:
	{
		G_B11_0 = 0;
	}

IL_0077:
	{
		V_0 = G_B11_0;
		bool L_12 = Uri_get_IsUnc_m3_1729(__this, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_00b9;
		}
	}
	{
		String_t* L_13 = (__this->___path_5);
		String_t* L_14 = (String_t*)VirtFuncInvoker1< String_t*, String_t* >::Invoke(5 /* System.String System.Uri::Unescape(System.String) */, __this, L_13);
		V_1 = L_14;
		bool L_15 = V_0;
		V_2 = L_15;
		bool L_16 = V_2;
		if (!L_16)
		{
			goto IL_00ad;
		}
	}
	{
		String_t* L_17 = V_1;
		NullCheck(L_17);
		String_t* L_18 = String_Replace_m1_535(L_17, ((int32_t)47), ((int32_t)92), /*hidden argument*/NULL);
		__this->___cachedLocalPath_16 = L_18;
		goto IL_00b4;
	}

IL_00ad:
	{
		String_t* L_19 = V_1;
		__this->___cachedLocalPath_16 = L_19;
	}

IL_00b4:
	{
		goto IL_018f;
	}

IL_00b9:
	{
		String_t* L_20 = (__this->___path_5);
		NullCheck(L_20);
		int32_t L_21 = String_get_Length_m1_571(L_20, /*hidden argument*/NULL);
		if ((((int32_t)L_21) <= ((int32_t)1)))
		{
			goto IL_0103;
		}
	}
	{
		String_t* L_22 = (__this->___path_5);
		NullCheck(L_22);
		uint16_t L_23 = String_get_Chars_m1_442(L_22, 1, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_23) == ((uint32_t)((int32_t)58)))))
		{
			goto IL_0103;
		}
	}
	{
		String_t* L_24 = (__this->___path_5);
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1_437_il2cpp_TypeInfo_var);
		uint16_t L_25 = ((Path_t1_437_StaticFields*)Path_t1_437_il2cpp_TypeInfo_var->static_fields)->___AltDirectorySeparatorChar_1;
		uint16_t L_26 = ((Path_t1_437_StaticFields*)Path_t1_437_il2cpp_TypeInfo_var->static_fields)->___DirectorySeparatorChar_2;
		NullCheck(L_24);
		String_t* L_27 = String_Replace_m1_535(L_24, L_25, L_26, /*hidden argument*/NULL);
		String_t* L_28 = (String_t*)VirtFuncInvoker1< String_t*, String_t* >::Invoke(5 /* System.String System.Uri::Unescape(System.String) */, __this, L_27);
		__this->___cachedLocalPath_16 = L_28;
		goto IL_018f;
	}

IL_0103:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1_437_il2cpp_TypeInfo_var);
		uint16_t L_29 = ((Path_t1_437_StaticFields*)Path_t1_437_il2cpp_TypeInfo_var->static_fields)->___DirectorySeparatorChar_2;
		if ((!(((uint32_t)L_29) == ((uint32_t)((int32_t)92)))))
		{
			goto IL_017d;
		}
	}
	{
		String_t* L_30 = (__this->___host_3);
		V_3 = L_30;
		String_t* L_31 = (__this->___path_5);
		NullCheck(L_31);
		int32_t L_32 = String_get_Length_m1_571(L_31, /*hidden argument*/NULL);
		if ((((int32_t)L_32) <= ((int32_t)0)))
		{
			goto IL_0161;
		}
	}
	{
		String_t* L_33 = (__this->___path_5);
		NullCheck(L_33);
		int32_t L_34 = String_get_Length_m1_571(L_33, /*hidden argument*/NULL);
		if ((((int32_t)L_34) > ((int32_t)1)))
		{
			goto IL_014b;
		}
	}
	{
		String_t* L_35 = (__this->___path_5);
		NullCheck(L_35);
		uint16_t L_36 = String_get_Chars_m1_442(L_35, 0, /*hidden argument*/NULL);
		if ((((int32_t)L_36) == ((int32_t)((int32_t)47))))
		{
			goto IL_0161;
		}
	}

IL_014b:
	{
		String_t* L_37 = V_3;
		String_t* L_38 = (__this->___path_5);
		NullCheck(L_38);
		String_t* L_39 = String_Replace_m1_535(L_38, ((int32_t)47), ((int32_t)92), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_40 = String_Concat_m1_559(NULL /*static, unused*/, L_37, L_39, /*hidden argument*/NULL);
		V_3 = L_40;
	}

IL_0161:
	{
		String_t* L_41 = V_3;
		String_t* L_42 = (String_t*)VirtFuncInvoker1< String_t*, String_t* >::Invoke(5 /* System.String System.Uri::Unescape(System.String) */, __this, L_41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_43 = String_Concat_m1_559(NULL /*static, unused*/, _stringLiteral723, L_42, /*hidden argument*/NULL);
		__this->___cachedLocalPath_16 = L_43;
		goto IL_018f;
	}

IL_017d:
	{
		String_t* L_44 = (__this->___path_5);
		String_t* L_45 = (String_t*)VirtFuncInvoker1< String_t*, String_t* >::Invoke(5 /* System.String System.Uri::Unescape(System.String) */, __this, L_44);
		__this->___cachedLocalPath_16 = L_45;
	}

IL_018f:
	{
		String_t* L_46 = (__this->___cachedLocalPath_16);
		NullCheck(L_46);
		int32_t L_47 = String_get_Length_m1_571(L_46, /*hidden argument*/NULL);
		if (L_47)
		{
			goto IL_01b3;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1_437_il2cpp_TypeInfo_var);
		uint16_t L_48 = ((Path_t1_437_StaticFields*)Path_t1_437_il2cpp_TypeInfo_var->static_fields)->___DirectorySeparatorChar_2;
		V_4 = L_48;
		String_t* L_49 = Char_ToString_m1_409((&V_4), /*hidden argument*/NULL);
		__this->___cachedLocalPath_16 = L_49;
	}

IL_01b3:
	{
		String_t* L_50 = (__this->___cachedLocalPath_16);
		return L_50;
	}
}
// System.String System.Uri::get_PathAndQuery()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" String_t* Uri_get_PathAndQuery_m3_1730 (Uri_t3_3 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	{
		Uri_EnsureAbsoluteUri_m3_1772(__this, /*hidden argument*/NULL);
		String_t* L_0 = (__this->___path_5);
		String_t* L_1 = Uri_get_Query_m3_1732(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m1_559(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 System.Uri::get_Port()
extern "C" int32_t Uri_get_Port_m3_1731 (Uri_t3_3 * __this, const MethodInfo* method)
{
	{
		Uri_EnsureAbsoluteUri_m3_1772(__this, /*hidden argument*/NULL);
		int32_t L_0 = (__this->___port_4);
		return L_0;
	}
}
// System.String System.Uri::get_Query()
extern "C" String_t* Uri_get_Query_m3_1732 (Uri_t3_3 * __this, const MethodInfo* method)
{
	{
		Uri_EnsureAbsoluteUri_m3_1772(__this, /*hidden argument*/NULL);
		String_t* L_0 = (__this->___query_6);
		return L_0;
	}
}
// System.String System.Uri::get_Scheme()
extern "C" String_t* Uri_get_Scheme_m3_33 (Uri_t3_3 * __this, const MethodInfo* method)
{
	{
		Uri_EnsureAbsoluteUri_m3_1772(__this, /*hidden argument*/NULL);
		String_t* L_0 = (__this->___scheme_2);
		return L_0;
	}
}
// System.String[] System.Uri::get_Segments()
extern TypeInfo* StringU5BU5D_t1_238_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t1_16_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Char_t1_15_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral740;
extern "C" StringU5BU5D_t1_238* Uri_get_Segments_m3_1733 (Uri_t3_3 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringU5BU5D_t1_238_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(35);
		CharU5BU5D_t1_16_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(15);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Char_t1_15_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(12);
		_stringLiteral740 = il2cpp_codegen_string_literal_from_index(740);
		s_Il2CppMethodIntialized = true;
	}
	StringU5BU5D_t1_238* V_0 = {0};
	bool V_1 = false;
	StringU5BU5D_t1_238* V_2 = {0};
	int32_t V_3 = 0;
	StringU5BU5D_t1_238* V_4 = {0};
	int32_t V_5 = 0;
	{
		Uri_EnsureAbsoluteUri_m3_1772(__this, /*hidden argument*/NULL);
		StringU5BU5D_t1_238* L_0 = (__this->___segments_12);
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		StringU5BU5D_t1_238* L_1 = (__this->___segments_12);
		return L_1;
	}

IL_0018:
	{
		String_t* L_2 = (__this->___path_5);
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m1_571(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_003b;
		}
	}
	{
		__this->___segments_12 = ((StringU5BU5D_t1_238*)SZArrayNew(StringU5BU5D_t1_238_il2cpp_TypeInfo_var, 0));
		StringU5BU5D_t1_238* L_4 = (__this->___segments_12);
		return L_4;
	}

IL_003b:
	{
		String_t* L_5 = (__this->___path_5);
		CharU5BU5D_t1_16* L_6 = ((CharU5BU5D_t1_16*)SZArrayNew(CharU5BU5D_t1_16_il2cpp_TypeInfo_var, 1));
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_6, 0, sizeof(uint16_t))) = (uint16_t)((int32_t)47);
		NullCheck(L_5);
		StringU5BU5D_t1_238* L_7 = String_Split_m1_448(L_5, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		StringU5BU5D_t1_238* L_8 = V_0;
		__this->___segments_12 = L_8;
		String_t* L_9 = (__this->___path_5);
		NullCheck(L_9);
		bool L_10 = String_EndsWith_m1_482(L_9, _stringLiteral740, /*hidden argument*/NULL);
		V_1 = L_10;
		StringU5BU5D_t1_238* L_11 = V_0;
		NullCheck(L_11);
		if ((((int32_t)(((int32_t)((int32_t)(((Array_t *)L_11)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_0094;
		}
	}
	{
		bool L_12 = V_1;
		if (!L_12)
		{
			goto IL_0094;
		}
	}
	{
		StringU5BU5D_t1_238* L_13 = V_0;
		NullCheck(L_13);
		V_2 = ((StringU5BU5D_t1_238*)SZArrayNew(StringU5BU5D_t1_238_il2cpp_TypeInfo_var, ((int32_t)((int32_t)(((int32_t)((int32_t)(((Array_t *)L_13)->max_length))))-(int32_t)1))));
		StringU5BU5D_t1_238* L_14 = V_0;
		StringU5BU5D_t1_238* L_15 = V_2;
		StringU5BU5D_t1_238* L_16 = V_0;
		NullCheck(L_16);
		Array_Copy_m1_1041(NULL /*static, unused*/, (Array_t *)(Array_t *)L_14, 0, (Array_t *)(Array_t *)L_15, 0, ((int32_t)((int32_t)(((int32_t)((int32_t)(((Array_t *)L_16)->max_length))))-(int32_t)1)), /*hidden argument*/NULL);
		StringU5BU5D_t1_238* L_17 = V_2;
		V_0 = L_17;
	}

IL_0094:
	{
		V_3 = 0;
		bool L_18 = Uri_get_IsFile_m3_1727(__this, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00ff;
		}
	}
	{
		String_t* L_19 = (__this->___path_5);
		NullCheck(L_19);
		int32_t L_20 = String_get_Length_m1_571(L_19, /*hidden argument*/NULL);
		if ((((int32_t)L_20) <= ((int32_t)1)))
		{
			goto IL_00ff;
		}
	}
	{
		String_t* L_21 = (__this->___path_5);
		NullCheck(L_21);
		uint16_t L_22 = String_get_Chars_m1_442(L_21, 1, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_22) == ((uint32_t)((int32_t)58)))))
		{
			goto IL_00ff;
		}
	}
	{
		StringU5BU5D_t1_238* L_23 = V_0;
		NullCheck(L_23);
		V_4 = ((StringU5BU5D_t1_238*)SZArrayNew(StringU5BU5D_t1_238_il2cpp_TypeInfo_var, ((int32_t)((int32_t)(((int32_t)((int32_t)(((Array_t *)L_23)->max_length))))+(int32_t)1))));
		StringU5BU5D_t1_238* L_24 = V_0;
		StringU5BU5D_t1_238* L_25 = V_4;
		StringU5BU5D_t1_238* L_26 = V_0;
		NullCheck(L_26);
		Array_Copy_m1_1041(NULL /*static, unused*/, (Array_t *)(Array_t *)L_24, 1, (Array_t *)(Array_t *)L_25, 2, ((int32_t)((int32_t)(((int32_t)((int32_t)(((Array_t *)L_26)->max_length))))-(int32_t)1)), /*hidden argument*/NULL);
		StringU5BU5D_t1_238* L_27 = V_4;
		V_0 = L_27;
		StringU5BU5D_t1_238* L_28 = V_0;
		String_t* L_29 = (__this->___path_5);
		NullCheck(L_29);
		String_t* L_30 = String_Substring_m1_455(L_29, 0, 2, /*hidden argument*/NULL);
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, 0);
		ArrayElementTypeCheck (L_28, L_30);
		*((String_t**)(String_t**)SZArrayLdElema(L_28, 0, sizeof(String_t*))) = (String_t*)L_30;
		StringU5BU5D_t1_238* L_31 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, 1);
		ArrayElementTypeCheck (L_31, L_32);
		*((String_t**)(String_t**)SZArrayLdElema(L_31, 1, sizeof(String_t*))) = (String_t*)L_32;
		int32_t L_33 = V_3;
		V_3 = ((int32_t)((int32_t)L_33+(int32_t)1));
	}

IL_00ff:
	{
		StringU5BU5D_t1_238* L_34 = V_0;
		NullCheck(L_34);
		V_5 = (((int32_t)((int32_t)(((Array_t *)L_34)->max_length))));
		goto IL_0133;
	}

IL_0109:
	{
		int32_t L_35 = V_3;
		int32_t L_36 = V_5;
		if ((!(((uint32_t)L_35) == ((uint32_t)((int32_t)((int32_t)L_36-(int32_t)1))))))
		{
			goto IL_0119;
		}
	}
	{
		bool L_37 = V_1;
		if (!L_37)
		{
			goto IL_012f;
		}
	}

IL_0119:
	{
		StringU5BU5D_t1_238* L_38 = V_0;
		int32_t L_39 = V_3;
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, L_39);
		String_t** L_40 = ((String_t**)(String_t**)SZArrayLdElema(L_38, L_39, sizeof(String_t*)));
		uint16_t L_41 = ((int32_t)47);
		Object_t * L_42 = Box(Char_t1_15_il2cpp_TypeInfo_var, &L_41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_43 = String_Concat_m1_556(NULL /*static, unused*/, (*((String_t**)L_40)), L_42, /*hidden argument*/NULL);
		*((Object_t **)(L_40)) = (Object_t *)L_43;
	}

IL_012f:
	{
		int32_t L_44 = V_3;
		V_3 = ((int32_t)((int32_t)L_44+(int32_t)1));
	}

IL_0133:
	{
		int32_t L_45 = V_3;
		int32_t L_46 = V_5;
		if ((((int32_t)L_45) < ((int32_t)L_46)))
		{
			goto IL_0109;
		}
	}
	{
		StringU5BU5D_t1_238* L_47 = V_0;
		__this->___segments_12 = L_47;
		StringU5BU5D_t1_238* L_48 = (__this->___segments_12);
		return L_48;
	}
}
// System.Boolean System.Uri::get_IsAbsoluteUri()
extern "C" bool Uri_get_IsAbsoluteUri_m3_32 (Uri_t3_3 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___isAbsoluteUri_11);
		return L_0;
	}
}
// System.UriHostNameType System.Uri::CheckHostName(System.String)
extern TypeInfo* Uri_t3_3_il2cpp_TypeInfo_var;
extern TypeInfo* IPv6Address_t3_122_il2cpp_TypeInfo_var;
extern "C" int32_t Uri_CheckHostName_m3_1734 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Uri_t3_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		IPv6Address_t3_122_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1454);
		s_Il2CppMethodIntialized = true;
	}
	IPv6Address_t3_122 * V_0 = {0};
	{
		String_t* L_0 = ___name;
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		String_t* L_1 = ___name;
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m1_571(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0013;
		}
	}

IL_0011:
	{
		return (int32_t)(0);
	}

IL_0013:
	{
		String_t* L_3 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		bool L_4 = Uri_IsIPv4Address_m3_1735(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0020;
		}
	}
	{
		return (int32_t)(3);
	}

IL_0020:
	{
		String_t* L_5 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		bool L_6 = Uri_IsDomainAddress_m3_1736(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_002d;
		}
	}
	{
		return (int32_t)(2);
	}

IL_002d:
	{
		String_t* L_7 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(IPv6Address_t3_122_il2cpp_TypeInfo_var);
		bool L_8 = IPv6Address_TryParse_m3_810(NULL /*static, unused*/, L_7, (&V_0), /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_003c;
		}
	}
	{
		return (int32_t)(4);
	}

IL_003c:
	{
		return (int32_t)(0);
	}
}
// System.Boolean System.Uri::IsIPv4Address(System.String)
extern TypeInfo* CharU5BU5D_t1_16_il2cpp_TypeInfo_var;
extern "C" bool Uri_IsIPv4Address_m3_1735 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CharU5BU5D_t1_16_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(15);
		s_Il2CppMethodIntialized = true;
	}
	StringU5BU5D_t1_238* V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	uint32_t V_3 = 0;
	{
		String_t* L_0 = ___name;
		CharU5BU5D_t1_16* L_1 = ((CharU5BU5D_t1_16*)SZArrayNew(CharU5BU5D_t1_16_il2cpp_TypeInfo_var, 1));
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_1, 0, sizeof(uint16_t))) = (uint16_t)((int32_t)46);
		NullCheck(L_0);
		StringU5BU5D_t1_238* L_2 = String_Split_m1_448(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		StringU5BU5D_t1_238* L_3 = V_0;
		NullCheck(L_3);
		if ((((int32_t)(((int32_t)((int32_t)(((Array_t *)L_3)->max_length))))) == ((int32_t)4)))
		{
			goto IL_001d;
		}
	}
	{
		return 0;
	}

IL_001d:
	{
		V_1 = 0;
		goto IL_0057;
	}

IL_0024:
	{
		StringU5BU5D_t1_238* L_4 = V_0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		NullCheck((*(String_t**)(String_t**)SZArrayLdElema(L_4, L_6, sizeof(String_t*))));
		int32_t L_7 = String_get_Length_m1_571((*(String_t**)(String_t**)SZArrayLdElema(L_4, L_6, sizeof(String_t*))), /*hidden argument*/NULL);
		V_2 = L_7;
		int32_t L_8 = V_2;
		if (L_8)
		{
			goto IL_0035;
		}
	}
	{
		return 0;
	}

IL_0035:
	{
		StringU5BU5D_t1_238* L_9 = V_0;
		int32_t L_10 = V_1;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		bool L_12 = UInt32_TryParse_m1_174(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_9, L_11, sizeof(String_t*))), (&V_3), /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_0046;
		}
	}
	{
		return 0;
	}

IL_0046:
	{
		uint32_t L_13 = V_3;
		if ((!(((uint32_t)L_13) > ((uint32_t)((int32_t)255)))))
		{
			goto IL_0053;
		}
	}
	{
		return 0;
	}

IL_0053:
	{
		int32_t L_14 = V_1;
		V_1 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_0057:
	{
		int32_t L_15 = V_1;
		if ((((int32_t)L_15) < ((int32_t)4)))
		{
			goto IL_0024;
		}
	}
	{
		return 1;
	}
}
// System.Boolean System.Uri::IsDomainAddress(System.String)
extern TypeInfo* Char_t1_15_il2cpp_TypeInfo_var;
extern "C" bool Uri_IsDomainAddress_m3_1736 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Char_t1_15_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(12);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	uint16_t V_3 = 0x0;
	{
		String_t* L_0 = ___name;
		NullCheck(L_0);
		int32_t L_1 = String_get_Length_m1_571(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		V_1 = 0;
		V_2 = 0;
		goto IL_006e;
	}

IL_0010:
	{
		String_t* L_2 = ___name;
		int32_t L_3 = V_2;
		NullCheck(L_2);
		uint16_t L_4 = String_get_Chars_m1_442(L_2, L_3, /*hidden argument*/NULL);
		V_3 = L_4;
		int32_t L_5 = V_1;
		if (L_5)
		{
			goto IL_0030;
		}
	}
	{
		uint16_t L_6 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Char_t1_15_il2cpp_TypeInfo_var);
		bool L_7 = Char_IsLetterOrDigit_m1_380(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_002b;
		}
	}
	{
		return 0;
	}

IL_002b:
	{
		goto IL_005c;
	}

IL_0030:
	{
		uint16_t L_8 = V_3;
		if ((!(((uint32_t)L_8) == ((uint32_t)((int32_t)46)))))
		{
			goto IL_003f;
		}
	}
	{
		V_1 = 0;
		goto IL_005c;
	}

IL_003f:
	{
		uint16_t L_9 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Char_t1_15_il2cpp_TypeInfo_var);
		bool L_10 = Char_IsLetterOrDigit_m1_380(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_005c;
		}
	}
	{
		uint16_t L_11 = V_3;
		if ((((int32_t)L_11) == ((int32_t)((int32_t)45))))
		{
			goto IL_005c;
		}
	}
	{
		uint16_t L_12 = V_3;
		if ((((int32_t)L_12) == ((int32_t)((int32_t)95))))
		{
			goto IL_005c;
		}
	}
	{
		return 0;
	}

IL_005c:
	{
		int32_t L_13 = V_1;
		int32_t L_14 = ((int32_t)((int32_t)L_13+(int32_t)1));
		V_1 = L_14;
		if ((!(((uint32_t)L_14) == ((uint32_t)((int32_t)64)))))
		{
			goto IL_006a;
		}
	}
	{
		return 0;
	}

IL_006a:
	{
		int32_t L_15 = V_2;
		V_2 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_006e:
	{
		int32_t L_16 = V_2;
		int32_t L_17 = V_0;
		if ((((int32_t)L_16) < ((int32_t)L_17)))
		{
			goto IL_0010;
		}
	}
	{
		return 1;
	}
}
// System.Boolean System.Uri::CheckSchemeName(System.String)
extern TypeInfo* Uri_t3_3_il2cpp_TypeInfo_var;
extern TypeInfo* Char_t1_15_il2cpp_TypeInfo_var;
extern "C" bool Uri_CheckSchemeName_m3_1737 (Object_t * __this /* static, unused */, String_t* ___schemeName, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Uri_t3_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		Char_t1_15_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(12);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	uint16_t V_2 = 0x0;
	{
		String_t* L_0 = ___schemeName;
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		String_t* L_1 = ___schemeName;
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m1_571(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0013;
		}
	}

IL_0011:
	{
		return 0;
	}

IL_0013:
	{
		String_t* L_3 = ___schemeName;
		NullCheck(L_3);
		uint16_t L_4 = String_get_Chars_m1_442(L_3, 0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		bool L_5 = Uri_IsAlpha_m3_1738(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0026;
		}
	}
	{
		return 0;
	}

IL_0026:
	{
		String_t* L_6 = ___schemeName;
		NullCheck(L_6);
		int32_t L_7 = String_get_Length_m1_571(L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		V_1 = 1;
		goto IL_0070;
	}

IL_0034:
	{
		String_t* L_8 = ___schemeName;
		int32_t L_9 = V_1;
		NullCheck(L_8);
		uint16_t L_10 = String_get_Chars_m1_442(L_8, L_9, /*hidden argument*/NULL);
		V_2 = L_10;
		uint16_t L_11 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Char_t1_15_il2cpp_TypeInfo_var);
		bool L_12 = Char_IsDigit_m1_374(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_006c;
		}
	}
	{
		uint16_t L_13 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		bool L_14 = Uri_IsAlpha_m3_1738(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_006c;
		}
	}
	{
		uint16_t L_15 = V_2;
		if ((((int32_t)L_15) == ((int32_t)((int32_t)46))))
		{
			goto IL_006c;
		}
	}
	{
		uint16_t L_16 = V_2;
		if ((((int32_t)L_16) == ((int32_t)((int32_t)43))))
		{
			goto IL_006c;
		}
	}
	{
		uint16_t L_17 = V_2;
		if ((((int32_t)L_17) == ((int32_t)((int32_t)45))))
		{
			goto IL_006c;
		}
	}
	{
		return 0;
	}

IL_006c:
	{
		int32_t L_18 = V_1;
		V_1 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0070:
	{
		int32_t L_19 = V_1;
		int32_t L_20 = V_0;
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0034;
		}
	}
	{
		return 1;
	}
}
// System.Boolean System.Uri::IsAlpha(System.Char)
extern "C" bool Uri_IsAlpha_m3_1738 (Object_t * __this /* static, unused */, uint16_t ___c, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	int32_t G_B7_0 = 0;
	{
		uint16_t L_0 = ___c;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) < ((int32_t)((int32_t)65))))
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) <= ((int32_t)((int32_t)90))))
		{
			goto IL_0027;
		}
	}

IL_0012:
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) < ((int32_t)((int32_t)97))))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_4 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_4) > ((int32_t)((int32_t)122)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0025;
	}

IL_0024:
	{
		G_B5_0 = 0;
	}

IL_0025:
	{
		G_B7_0 = G_B5_0;
		goto IL_0028;
	}

IL_0027:
	{
		G_B7_0 = 1;
	}

IL_0028:
	{
		return G_B7_0;
	}
}
// System.Boolean System.Uri::Equals(System.Object)
extern TypeInfo* Uri_t3_3_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" bool Uri_Equals_m3_1739 (Uri_t3_3 * __this, Object_t * ___comparant, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Uri_t3_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	Uri_t3_3 * V_0 = {0};
	String_t* V_1 = {0};
	{
		Object_t * L_0 = ___comparant;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return 0;
	}

IL_0008:
	{
		Object_t * L_1 = ___comparant;
		V_0 = ((Uri_t3_3 *)IsInstClass(L_1, Uri_t3_3_il2cpp_TypeInfo_var));
		Uri_t3_3 * L_2 = V_0;
		if (L_2)
		{
			goto IL_002b;
		}
	}
	{
		Object_t * L_3 = ___comparant;
		V_1 = ((String_t*)IsInstSealed(L_3, String_t_il2cpp_TypeInfo_var));
		String_t* L_4 = V_1;
		if (L_4)
		{
			goto IL_0024;
		}
	}
	{
		return 0;
	}

IL_0024:
	{
		String_t* L_5 = V_1;
		Uri_t3_3 * L_6 = (Uri_t3_3 *)il2cpp_codegen_object_new (Uri_t3_3_il2cpp_TypeInfo_var);
		Uri__ctor_m3_23(L_6, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
	}

IL_002b:
	{
		Uri_t3_3 * L_7 = V_0;
		bool L_8 = Uri_InternalEquals_m3_1740(__this, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Boolean System.Uri::InternalEquals(System.Uri)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* CultureInfo_t1_277_il2cpp_TypeInfo_var;
extern "C" bool Uri_InternalEquals_m3_1740 (Uri_t3_3 * __this, Uri_t3_3 * ___uri, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		CultureInfo_t1_277_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(31);
		s_Il2CppMethodIntialized = true;
	}
	CultureInfo_t1_277 * V_0 = {0};
	int32_t G_B10_0 = 0;
	{
		bool L_0 = (__this->___isAbsoluteUri_11);
		Uri_t3_3 * L_1 = ___uri;
		NullCheck(L_1);
		bool L_2 = (L_1->___isAbsoluteUri_11);
		if ((((int32_t)L_0) == ((int32_t)L_2)))
		{
			goto IL_0013;
		}
	}
	{
		return 0;
	}

IL_0013:
	{
		bool L_3 = (__this->___isAbsoluteUri_11);
		if (L_3)
		{
			goto IL_0030;
		}
	}
	{
		String_t* L_4 = (__this->___source_1);
		Uri_t3_3 * L_5 = ___uri;
		NullCheck(L_5);
		String_t* L_6 = (L_5->___source_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_7 = String_op_Equality_m1_601(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
		return L_7;
	}

IL_0030:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t1_277_il2cpp_TypeInfo_var);
		CultureInfo_t1_277 * L_8 = CultureInfo_get_InvariantCulture_m1_3911(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_8;
		String_t* L_9 = (__this->___scheme_2);
		CultureInfo_t1_277 * L_10 = V_0;
		NullCheck(L_9);
		String_t* L_11 = String_ToLower_m1_541(L_9, L_10, /*hidden argument*/NULL);
		Uri_t3_3 * L_12 = ___uri;
		NullCheck(L_12);
		String_t* L_13 = (L_12->___scheme_2);
		CultureInfo_t1_277 * L_14 = V_0;
		NullCheck(L_13);
		String_t* L_15 = String_ToLower_m1_541(L_13, L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_16 = String_op_Equality_m1_601(NULL /*static, unused*/, L_11, L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00b4;
		}
	}
	{
		String_t* L_17 = (__this->___host_3);
		CultureInfo_t1_277 * L_18 = V_0;
		NullCheck(L_17);
		String_t* L_19 = String_ToLower_m1_541(L_17, L_18, /*hidden argument*/NULL);
		Uri_t3_3 * L_20 = ___uri;
		NullCheck(L_20);
		String_t* L_21 = (L_20->___host_3);
		CultureInfo_t1_277 * L_22 = V_0;
		NullCheck(L_21);
		String_t* L_23 = String_ToLower_m1_541(L_21, L_22, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_24 = String_op_Equality_m1_601(NULL /*static, unused*/, L_19, L_23, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_00b4;
		}
	}
	{
		int32_t L_25 = (__this->___port_4);
		Uri_t3_3 * L_26 = ___uri;
		NullCheck(L_26);
		int32_t L_27 = (L_26->___port_4);
		if ((!(((uint32_t)L_25) == ((uint32_t)L_27))))
		{
			goto IL_00b4;
		}
	}
	{
		String_t* L_28 = (__this->___query_6);
		Uri_t3_3 * L_29 = ___uri;
		NullCheck(L_29);
		String_t* L_30 = (L_29->___query_6);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_31 = String_op_Equality_m1_601(NULL /*static, unused*/, L_28, L_30, /*hidden argument*/NULL);
		if (!L_31)
		{
			goto IL_00b4;
		}
	}
	{
		String_t* L_32 = (__this->___path_5);
		Uri_t3_3 * L_33 = ___uri;
		NullCheck(L_33);
		String_t* L_34 = (L_33->___path_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_35 = String_op_Equality_m1_601(NULL /*static, unused*/, L_32, L_34, /*hidden argument*/NULL);
		G_B10_0 = ((int32_t)(L_35));
		goto IL_00b5;
	}

IL_00b4:
	{
		G_B10_0 = 0;
	}

IL_00b5:
	{
		return G_B10_0;
	}
}
// System.Int32 System.Uri::GetHashCode()
extern TypeInfo* CultureInfo_t1_277_il2cpp_TypeInfo_var;
extern "C" int32_t Uri_GetHashCode_m3_1741 (Uri_t3_3 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CultureInfo_t1_277_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(31);
		s_Il2CppMethodIntialized = true;
	}
	CultureInfo_t1_277 * V_0 = {0};
	{
		int32_t L_0 = (__this->___cachedHashCode_17);
		if (L_0)
		{
			goto IL_007a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t1_277_il2cpp_TypeInfo_var);
		CultureInfo_t1_277 * L_1 = CultureInfo_get_InvariantCulture_m1_3911(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = (__this->___isAbsoluteUri_11);
		if (!L_2)
		{
			goto IL_0069;
		}
	}
	{
		String_t* L_3 = (__this->___scheme_2);
		CultureInfo_t1_277 * L_4 = V_0;
		NullCheck(L_3);
		String_t* L_5 = String_ToLower_m1_541(L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		int32_t L_6 = String_GetHashCode_m1_577(L_5, /*hidden argument*/NULL);
		String_t* L_7 = (__this->___host_3);
		CultureInfo_t1_277 * L_8 = V_0;
		NullCheck(L_7);
		String_t* L_9 = String_ToLower_m1_541(L_7, L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		int32_t L_10 = String_GetHashCode_m1_577(L_9, /*hidden argument*/NULL);
		int32_t L_11 = (__this->___port_4);
		String_t* L_12 = (__this->___query_6);
		NullCheck(L_12);
		int32_t L_13 = String_GetHashCode_m1_577(L_12, /*hidden argument*/NULL);
		String_t* L_14 = (__this->___path_5);
		NullCheck(L_14);
		int32_t L_15 = String_GetHashCode_m1_577(L_14, /*hidden argument*/NULL);
		__this->___cachedHashCode_17 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6^(int32_t)L_10))^(int32_t)L_11))^(int32_t)L_13))^(int32_t)L_15));
		goto IL_007a;
	}

IL_0069:
	{
		String_t* L_16 = (__this->___source_1);
		NullCheck(L_16);
		int32_t L_17 = String_GetHashCode_m1_577(L_16, /*hidden argument*/NULL);
		__this->___cachedHashCode_17 = L_17;
	}

IL_007a:
	{
		int32_t L_18 = (__this->___cachedHashCode_17);
		return L_18;
	}
}
// System.String System.Uri::GetLeftPart(System.UriPartial)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Uri_t3_3_il2cpp_TypeInfo_var;
extern TypeInfo* StringBuilder_t1_247_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t1_92_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_14878_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral734;
extern Il2CppCodeGenString* _stringLiteral735;
extern "C" String_t* Uri_GetLeftPart_m3_1742 (Uri_t3_3 * __this, int32_t ___part, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Uri_t3_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		StringBuilder_t1_247_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		Dictionary_2_t1_92_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(129);
		Dictionary_2__ctor_m1_14878_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483658);
		_stringLiteral734 = il2cpp_codegen_string_literal_from_index(734);
		_stringLiteral735 = il2cpp_codegen_string_literal_from_index(735);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	StringBuilder_t1_247 * V_1 = {0};
	StringBuilder_t1_247 * V_2 = {0};
	int32_t V_3 = {0};
	String_t* V_4 = {0};
	Dictionary_2_t1_92 * V_5 = {0};
	int32_t V_6 = 0;
	{
		Uri_EnsureAbsoluteUri_m3_1772(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___part;
		V_3 = L_0;
		int32_t L_1 = V_3;
		if (L_1 == 0)
		{
			goto IL_001f;
		}
		if (L_1 == 1)
		{
			goto IL_0031;
		}
		if (L_1 == 2)
		{
			goto IL_0134;
		}
	}
	{
		goto IL_02ad;
	}

IL_001f:
	{
		String_t* L_2 = (__this->___scheme_2);
		String_t* L_3 = Uri_GetOpaqueWiseSchemeDelimiter_m3_1765(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m1_559(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_0031:
	{
		String_t* L_5 = (__this->___scheme_2);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		String_t* L_6 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___UriSchemeMailto_25;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_7 = String_op_Equality_m1_601(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_005b;
		}
	}
	{
		String_t* L_8 = (__this->___scheme_2);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		String_t* L_9 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___UriSchemeNews_26;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_10 = String_op_Equality_m1_601(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_005b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		return L_11;
	}

IL_0061:
	{
		StringBuilder_t1_247 * L_12 = (StringBuilder_t1_247 *)il2cpp_codegen_object_new (StringBuilder_t1_247_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1_12414(L_12, /*hidden argument*/NULL);
		V_1 = L_12;
		StringBuilder_t1_247 * L_13 = V_1;
		String_t* L_14 = (__this->___scheme_2);
		NullCheck(L_13);
		StringBuilder_Append_m1_12438(L_13, L_14, /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_15 = V_1;
		String_t* L_16 = Uri_GetOpaqueWiseSchemeDelimiter_m3_1765(__this, /*hidden argument*/NULL);
		NullCheck(L_15);
		StringBuilder_Append_m1_12438(L_15, L_16, /*hidden argument*/NULL);
		String_t* L_17 = (__this->___path_5);
		NullCheck(L_17);
		int32_t L_18 = String_get_Length_m1_571(L_17, /*hidden argument*/NULL);
		if ((((int32_t)L_18) <= ((int32_t)1)))
		{
			goto IL_00c3;
		}
	}
	{
		String_t* L_19 = (__this->___path_5);
		NullCheck(L_19);
		uint16_t L_20 = String_get_Chars_m1_442(L_19, 1, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_20) == ((uint32_t)((int32_t)58)))))
		{
			goto IL_00c3;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		String_t* L_21 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___UriSchemeFile_20;
		String_t* L_22 = (__this->___scheme_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_23 = String_op_Equality_m1_601(NULL /*static, unused*/, L_21, L_22, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_00c3;
		}
	}
	{
		StringBuilder_t1_247 * L_24 = V_1;
		NullCheck(L_24);
		StringBuilder_Append_m1_12452(L_24, ((int32_t)47), /*hidden argument*/NULL);
	}

IL_00c3:
	{
		String_t* L_25 = (__this->___userinfo_8);
		NullCheck(L_25);
		int32_t L_26 = String_get_Length_m1_571(L_25, /*hidden argument*/NULL);
		if ((((int32_t)L_26) <= ((int32_t)0)))
		{
			goto IL_00e8;
		}
	}
	{
		StringBuilder_t1_247 * L_27 = V_1;
		String_t* L_28 = (__this->___userinfo_8);
		NullCheck(L_27);
		StringBuilder_t1_247 * L_29 = StringBuilder_Append_m1_12438(L_27, L_28, /*hidden argument*/NULL);
		NullCheck(L_29);
		StringBuilder_Append_m1_12452(L_29, ((int32_t)64), /*hidden argument*/NULL);
	}

IL_00e8:
	{
		StringBuilder_t1_247 * L_30 = V_1;
		String_t* L_31 = (__this->___host_3);
		NullCheck(L_30);
		StringBuilder_Append_m1_12438(L_30, L_31, /*hidden argument*/NULL);
		String_t* L_32 = (__this->___scheme_2);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		int32_t L_33 = Uri_GetDefaultPort_m3_1764(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		V_0 = L_33;
		int32_t L_34 = (__this->___port_4);
		if ((((int32_t)L_34) == ((int32_t)(-1))))
		{
			goto IL_012d;
		}
	}
	{
		int32_t L_35 = (__this->___port_4);
		int32_t L_36 = V_0;
		if ((((int32_t)L_35) == ((int32_t)L_36)))
		{
			goto IL_012d;
		}
	}
	{
		StringBuilder_t1_247 * L_37 = V_1;
		NullCheck(L_37);
		StringBuilder_t1_247 * L_38 = StringBuilder_Append_m1_12452(L_37, ((int32_t)58), /*hidden argument*/NULL);
		int32_t L_39 = (__this->___port_4);
		NullCheck(L_38);
		StringBuilder_Append_m1_12444(L_38, L_39, /*hidden argument*/NULL);
	}

IL_012d:
	{
		StringBuilder_t1_247 * L_40 = V_1;
		NullCheck(L_40);
		String_t* L_41 = StringBuilder_ToString_m1_12428(L_40, /*hidden argument*/NULL);
		return L_41;
	}

IL_0134:
	{
		StringBuilder_t1_247 * L_42 = (StringBuilder_t1_247 *)il2cpp_codegen_object_new (StringBuilder_t1_247_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1_12414(L_42, /*hidden argument*/NULL);
		V_2 = L_42;
		StringBuilder_t1_247 * L_43 = V_2;
		String_t* L_44 = (__this->___scheme_2);
		NullCheck(L_43);
		StringBuilder_Append_m1_12438(L_43, L_44, /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_45 = V_2;
		String_t* L_46 = Uri_GetOpaqueWiseSchemeDelimiter_m3_1765(__this, /*hidden argument*/NULL);
		NullCheck(L_45);
		StringBuilder_Append_m1_12438(L_45, L_46, /*hidden argument*/NULL);
		String_t* L_47 = (__this->___path_5);
		NullCheck(L_47);
		int32_t L_48 = String_get_Length_m1_571(L_47, /*hidden argument*/NULL);
		if ((((int32_t)L_48) <= ((int32_t)1)))
		{
			goto IL_0196;
		}
	}
	{
		String_t* L_49 = (__this->___path_5);
		NullCheck(L_49);
		uint16_t L_50 = String_get_Chars_m1_442(L_49, 1, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_50) == ((uint32_t)((int32_t)58)))))
		{
			goto IL_0196;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		String_t* L_51 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___UriSchemeFile_20;
		String_t* L_52 = (__this->___scheme_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_53 = String_op_Equality_m1_601(NULL /*static, unused*/, L_51, L_52, /*hidden argument*/NULL);
		if (!L_53)
		{
			goto IL_0196;
		}
	}
	{
		StringBuilder_t1_247 * L_54 = V_2;
		NullCheck(L_54);
		StringBuilder_Append_m1_12452(L_54, ((int32_t)47), /*hidden argument*/NULL);
	}

IL_0196:
	{
		String_t* L_55 = (__this->___userinfo_8);
		NullCheck(L_55);
		int32_t L_56 = String_get_Length_m1_571(L_55, /*hidden argument*/NULL);
		if ((((int32_t)L_56) <= ((int32_t)0)))
		{
			goto IL_01bb;
		}
	}
	{
		StringBuilder_t1_247 * L_57 = V_2;
		String_t* L_58 = (__this->___userinfo_8);
		NullCheck(L_57);
		StringBuilder_t1_247 * L_59 = StringBuilder_Append_m1_12438(L_57, L_58, /*hidden argument*/NULL);
		NullCheck(L_59);
		StringBuilder_Append_m1_12452(L_59, ((int32_t)64), /*hidden argument*/NULL);
	}

IL_01bb:
	{
		StringBuilder_t1_247 * L_60 = V_2;
		String_t* L_61 = (__this->___host_3);
		NullCheck(L_60);
		StringBuilder_Append_m1_12438(L_60, L_61, /*hidden argument*/NULL);
		String_t* L_62 = (__this->___scheme_2);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		int32_t L_63 = Uri_GetDefaultPort_m3_1764(NULL /*static, unused*/, L_62, /*hidden argument*/NULL);
		V_0 = L_63;
		int32_t L_64 = (__this->___port_4);
		if ((((int32_t)L_64) == ((int32_t)(-1))))
		{
			goto IL_0200;
		}
	}
	{
		int32_t L_65 = (__this->___port_4);
		int32_t L_66 = V_0;
		if ((((int32_t)L_65) == ((int32_t)L_66)))
		{
			goto IL_0200;
		}
	}
	{
		StringBuilder_t1_247 * L_67 = V_2;
		NullCheck(L_67);
		StringBuilder_t1_247 * L_68 = StringBuilder_Append_m1_12452(L_67, ((int32_t)58), /*hidden argument*/NULL);
		int32_t L_69 = (__this->___port_4);
		NullCheck(L_68);
		StringBuilder_Append_m1_12444(L_68, L_69, /*hidden argument*/NULL);
	}

IL_0200:
	{
		String_t* L_70 = (__this->___path_5);
		NullCheck(L_70);
		int32_t L_71 = String_get_Length_m1_571(L_70, /*hidden argument*/NULL);
		if ((((int32_t)L_71) <= ((int32_t)0)))
		{
			goto IL_02a6;
		}
	}
	{
		String_t* L_72 = Uri_get_Scheme_m3_33(__this, /*hidden argument*/NULL);
		V_4 = L_72;
		String_t* L_73 = V_4;
		if (!L_73)
		{
			goto IL_0284;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		Dictionary_2_t1_92 * L_74 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__switchU24map14_34;
		if (L_74)
		{
			goto IL_0253;
		}
	}
	{
		Dictionary_2_t1_92 * L_75 = (Dictionary_2_t1_92 *)il2cpp_codegen_object_new (Dictionary_2_t1_92_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_14878(L_75, 2, /*hidden argument*/Dictionary_2__ctor_m1_14878_MethodInfo_var);
		V_5 = L_75;
		Dictionary_2_t1_92 * L_76 = V_5;
		NullCheck(L_76);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_76, _stringLiteral734, 0);
		Dictionary_2_t1_92 * L_77 = V_5;
		NullCheck(L_77);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_77, _stringLiteral735, 0);
		Dictionary_2_t1_92 * L_78 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__switchU24map14_34 = L_78;
	}

IL_0253:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		Dictionary_2_t1_92 * L_79 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__switchU24map14_34;
		String_t* L_80 = V_4;
		NullCheck(L_79);
		bool L_81 = (bool)VirtFuncInvoker2< bool, String_t*, int32_t* >::Invoke(21 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int32>::TryGetValue(!0,!1&) */, L_79, L_80, (&V_6));
		if (!L_81)
		{
			goto IL_0284;
		}
	}
	{
		int32_t L_82 = V_6;
		if (!L_82)
		{
			goto IL_0272;
		}
	}
	{
		goto IL_0284;
	}

IL_0272:
	{
		StringBuilder_t1_247 * L_83 = V_2;
		String_t* L_84 = (__this->___path_5);
		NullCheck(L_83);
		StringBuilder_Append_m1_12438(L_83, L_84, /*hidden argument*/NULL);
		goto IL_02a6;
	}

IL_0284:
	{
		StringBuilder_t1_247 * L_85 = V_2;
		String_t* L_86 = (__this->___path_5);
		String_t* L_87 = Uri_get_Scheme_m3_33(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		bool L_88 = Uri_CompactEscaped_m3_1760(NULL /*static, unused*/, L_87, /*hidden argument*/NULL);
		String_t* L_89 = Uri_Reduce_m3_1761(NULL /*static, unused*/, L_86, L_88, /*hidden argument*/NULL);
		NullCheck(L_85);
		StringBuilder_Append_m1_12438(L_85, L_89, /*hidden argument*/NULL);
		goto IL_02a6;
	}

IL_02a6:
	{
		StringBuilder_t1_247 * L_90 = V_2;
		NullCheck(L_90);
		String_t* L_91 = StringBuilder_ToString_m1_12428(L_90, /*hidden argument*/NULL);
		return L_91;
	}

IL_02ad:
	{
		return (String_t*)NULL;
	}
}
// System.Int32 System.Uri::FromHex(System.Char)
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral741;
extern "C" int32_t Uri_FromHex_m3_1743 (Object_t * __this /* static, unused */, uint16_t ___digit, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		_stringLiteral741 = il2cpp_codegen_string_literal_from_index(741);
		s_Il2CppMethodIntialized = true;
	}
	{
		uint16_t L_0 = ___digit;
		if ((((int32_t)((int32_t)48)) > ((int32_t)L_0)))
		{
			goto IL_0015;
		}
	}
	{
		uint16_t L_1 = ___digit;
		if ((((int32_t)L_1) > ((int32_t)((int32_t)57))))
		{
			goto IL_0015;
		}
	}
	{
		uint16_t L_2 = ___digit;
		return ((int32_t)((int32_t)L_2-(int32_t)((int32_t)48)));
	}

IL_0015:
	{
		uint16_t L_3 = ___digit;
		if ((((int32_t)((int32_t)97)) > ((int32_t)L_3)))
		{
			goto IL_002d;
		}
	}
	{
		uint16_t L_4 = ___digit;
		if ((((int32_t)L_4) > ((int32_t)((int32_t)102))))
		{
			goto IL_002d;
		}
	}
	{
		uint16_t L_5 = ___digit;
		return ((int32_t)((int32_t)((int32_t)((int32_t)L_5-(int32_t)((int32_t)97)))+(int32_t)((int32_t)10)));
	}

IL_002d:
	{
		uint16_t L_6 = ___digit;
		if ((((int32_t)((int32_t)65)) > ((int32_t)L_6)))
		{
			goto IL_0045;
		}
	}
	{
		uint16_t L_7 = ___digit;
		if ((((int32_t)L_7) > ((int32_t)((int32_t)70))))
		{
			goto IL_0045;
		}
	}
	{
		uint16_t L_8 = ___digit;
		return ((int32_t)((int32_t)((int32_t)((int32_t)L_8-(int32_t)((int32_t)65)))+(int32_t)((int32_t)10)));
	}

IL_0045:
	{
		ArgumentException_t1_1425 * L_9 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13260(L_9, _stringLiteral741, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_9);
	}
}
// System.String System.Uri::HexEscape(System.Char)
extern TypeInfo* ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var;
extern TypeInfo* Uri_t3_3_il2cpp_TypeInfo_var;
extern TypeInfo* Char_t1_15_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral742;
extern Il2CppCodeGenString* _stringLiteral743;
extern "C" String_t* Uri_HexEscape_m3_1744 (Object_t * __this /* static, unused */, uint16_t ___character, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		Uri_t3_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		Char_t1_15_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(12);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral742 = il2cpp_codegen_string_literal_from_index(742);
		_stringLiteral743 = il2cpp_codegen_string_literal_from_index(743);
		s_Il2CppMethodIntialized = true;
	}
	{
		uint16_t L_0 = ___character;
		if ((((int32_t)L_0) <= ((int32_t)((int32_t)255))))
		{
			goto IL_0016;
		}
	}
	{
		ArgumentOutOfRangeException_t1_1501 * L_1 = (ArgumentOutOfRangeException_t1_1501 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1_13274(L_1, _stringLiteral742, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		String_t* L_2 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___hexUpperChars_18;
		uint16_t L_3 = ___character;
		NullCheck(L_2);
		uint16_t L_4 = String_get_Chars_m1_442(L_2, ((int32_t)((int32_t)((int32_t)((int32_t)L_3&(int32_t)((int32_t)240)))>>(int32_t)4)), /*hidden argument*/NULL);
		uint16_t L_5 = L_4;
		Object_t * L_6 = Box(Char_t1_15_il2cpp_TypeInfo_var, &L_5);
		String_t* L_7 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___hexUpperChars_18;
		uint16_t L_8 = ___character;
		NullCheck(L_7);
		uint16_t L_9 = String_get_Chars_m1_442(L_7, ((int32_t)((int32_t)L_8&(int32_t)((int32_t)15))), /*hidden argument*/NULL);
		uint16_t L_10 = L_9;
		Object_t * L_11 = Box(Char_t1_15_il2cpp_TypeInfo_var, &L_10);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Concat_m1_557(NULL /*static, unused*/, _stringLiteral743, L_6, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Boolean System.Uri::IsHexDigit(System.Char)
extern "C" bool Uri_IsHexDigit_m3_1745 (Object_t * __this /* static, unused */, uint16_t ___digit, const MethodInfo* method)
{
	int32_t G_B7_0 = 0;
	int32_t G_B9_0 = 0;
	{
		uint16_t L_0 = ___digit;
		if ((((int32_t)((int32_t)48)) > ((int32_t)L_0)))
		{
			goto IL_0010;
		}
	}
	{
		uint16_t L_1 = ___digit;
		if ((((int32_t)L_1) <= ((int32_t)((int32_t)57))))
		{
			goto IL_0035;
		}
	}

IL_0010:
	{
		uint16_t L_2 = ___digit;
		if ((((int32_t)((int32_t)97)) > ((int32_t)L_2)))
		{
			goto IL_0020;
		}
	}
	{
		uint16_t L_3 = ___digit;
		if ((((int32_t)L_3) <= ((int32_t)((int32_t)102))))
		{
			goto IL_0035;
		}
	}

IL_0020:
	{
		uint16_t L_4 = ___digit;
		if ((((int32_t)((int32_t)65)) > ((int32_t)L_4)))
		{
			goto IL_0032;
		}
	}
	{
		uint16_t L_5 = ___digit;
		G_B7_0 = ((((int32_t)((((int32_t)L_5) > ((int32_t)((int32_t)70)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0033;
	}

IL_0032:
	{
		G_B7_0 = 0;
	}

IL_0033:
	{
		G_B9_0 = G_B7_0;
		goto IL_0036;
	}

IL_0035:
	{
		G_B9_0 = 1;
	}

IL_0036:
	{
		return G_B9_0;
	}
}
// System.Boolean System.Uri::IsHexEncoding(System.String,System.Int32)
extern TypeInfo* Uri_t3_3_il2cpp_TypeInfo_var;
extern "C" bool Uri_IsHexEncoding_m3_1746 (Object_t * __this /* static, unused */, String_t* ___pattern, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Uri_t3_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B6_0 = 0;
	{
		int32_t L_0 = ___index;
		String_t* L_1 = ___pattern;
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m1_571(L_1, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_0+(int32_t)3))) <= ((int32_t)L_2)))
		{
			goto IL_0010;
		}
	}
	{
		return 0;
	}

IL_0010:
	{
		String_t* L_3 = ___pattern;
		int32_t L_4 = ___index;
		int32_t L_5 = L_4;
		___index = ((int32_t)((int32_t)L_5+(int32_t)1));
		NullCheck(L_3);
		uint16_t L_6 = String_get_Chars_m1_442(L_3, L_5, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)((int32_t)37)))))
		{
			goto IL_0047;
		}
	}
	{
		String_t* L_7 = ___pattern;
		int32_t L_8 = ___index;
		int32_t L_9 = L_8;
		___index = ((int32_t)((int32_t)L_9+(int32_t)1));
		NullCheck(L_7);
		uint16_t L_10 = String_get_Chars_m1_442(L_7, L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		bool L_11 = Uri_IsHexDigit_m3_1745(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0047;
		}
	}
	{
		String_t* L_12 = ___pattern;
		int32_t L_13 = ___index;
		NullCheck(L_12);
		uint16_t L_14 = String_get_Chars_m1_442(L_12, L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		bool L_15 = Uri_IsHexDigit_m3_1745(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		G_B6_0 = ((int32_t)(L_15));
		goto IL_0048;
	}

IL_0047:
	{
		G_B6_0 = 0;
	}

IL_0048:
	{
		return G_B6_0;
	}
}
// System.Uri System.Uri::MakeRelativeUri(System.Uri)
extern TypeInfo* Uri_t3_3_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2335;
extern Il2CppCodeGenString* _stringLiteral745;
extern "C" Uri_t3_3 * Uri_MakeRelativeUri_m3_1747 (Uri_t3_3 * __this, Uri_t3_3 * ___uri, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Uri_t3_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral2335 = il2cpp_codegen_string_literal_from_index(2335);
		_stringLiteral745 = il2cpp_codegen_string_literal_from_index(745);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	StringU5BU5D_t1_238* V_1 = {0};
	StringU5BU5D_t1_238* V_2 = {0};
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	{
		Uri_t3_3 * L_0 = ___uri;
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		bool L_1 = Uri_op_Equality_m3_26(NULL /*static, unused*/, L_0, (Uri_t3_3 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_2 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_2, _stringLiteral2335, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		String_t* L_3 = Uri_get_Host_m3_2(__this, /*hidden argument*/NULL);
		Uri_t3_3 * L_4 = ___uri;
		NullCheck(L_4);
		String_t* L_5 = Uri_get_Host_m3_2(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_6 = String_op_Inequality_m1_602(NULL /*static, unused*/, L_3, L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0043;
		}
	}
	{
		String_t* L_7 = Uri_get_Scheme_m3_33(__this, /*hidden argument*/NULL);
		Uri_t3_3 * L_8 = ___uri;
		NullCheck(L_8);
		String_t* L_9 = Uri_get_Scheme_m3_33(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_10 = String_op_Inequality_m1_602(NULL /*static, unused*/, L_7, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0045;
		}
	}

IL_0043:
	{
		Uri_t3_3 * L_11 = ___uri;
		return L_11;
	}

IL_0045:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_0 = L_12;
		String_t* L_13 = (__this->___path_5);
		Uri_t3_3 * L_14 = ___uri;
		NullCheck(L_14);
		String_t* L_15 = (L_14->___path_5);
		bool L_16 = String_op_Inequality_m1_602(NULL /*static, unused*/, L_13, L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00ed;
		}
	}
	{
		StringU5BU5D_t1_238* L_17 = Uri_get_Segments_m3_1733(__this, /*hidden argument*/NULL);
		V_1 = L_17;
		Uri_t3_3 * L_18 = ___uri;
		NullCheck(L_18);
		StringU5BU5D_t1_238* L_19 = Uri_get_Segments_m3_1733(L_18, /*hidden argument*/NULL);
		V_2 = L_19;
		V_3 = 0;
		StringU5BU5D_t1_238* L_20 = V_1;
		NullCheck(L_20);
		StringU5BU5D_t1_238* L_21 = V_2;
		NullCheck(L_21);
		int32_t L_22 = Math_Min_m1_14218(NULL /*static, unused*/, (((int32_t)((int32_t)(((Array_t *)L_20)->max_length)))), (((int32_t)((int32_t)(((Array_t *)L_21)->max_length)))), /*hidden argument*/NULL);
		V_4 = L_22;
		goto IL_009c;
	}

IL_0083:
	{
		StringU5BU5D_t1_238* L_23 = V_1;
		int32_t L_24 = V_3;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_24);
		int32_t L_25 = L_24;
		StringU5BU5D_t1_238* L_26 = V_2;
		int32_t L_27 = V_3;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, L_27);
		int32_t L_28 = L_27;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_29 = String_op_Inequality_m1_602(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_23, L_25, sizeof(String_t*))), (*(String_t**)(String_t**)SZArrayLdElema(L_26, L_28, sizeof(String_t*))), /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_0098;
		}
	}
	{
		goto IL_00a4;
	}

IL_0098:
	{
		int32_t L_30 = V_3;
		V_3 = ((int32_t)((int32_t)L_30+(int32_t)1));
	}

IL_009c:
	{
		int32_t L_31 = V_3;
		int32_t L_32 = V_4;
		if ((((int32_t)L_31) < ((int32_t)L_32)))
		{
			goto IL_0083;
		}
	}

IL_00a4:
	{
		int32_t L_33 = V_3;
		V_5 = ((int32_t)((int32_t)L_33+(int32_t)1));
		goto IL_00c0;
	}

IL_00ae:
	{
		String_t* L_34 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_35 = String_Concat_m1_559(NULL /*static, unused*/, L_34, _stringLiteral745, /*hidden argument*/NULL);
		V_0 = L_35;
		int32_t L_36 = V_5;
		V_5 = ((int32_t)((int32_t)L_36+(int32_t)1));
	}

IL_00c0:
	{
		int32_t L_37 = V_5;
		StringU5BU5D_t1_238* L_38 = V_1;
		NullCheck(L_38);
		if ((((int32_t)L_37) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_38)->max_length)))))))
		{
			goto IL_00ae;
		}
	}
	{
		int32_t L_39 = V_3;
		V_6 = L_39;
		goto IL_00e3;
	}

IL_00d2:
	{
		String_t* L_40 = V_0;
		StringU5BU5D_t1_238* L_41 = V_2;
		int32_t L_42 = V_6;
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, L_42);
		int32_t L_43 = L_42;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_44 = String_Concat_m1_559(NULL /*static, unused*/, L_40, (*(String_t**)(String_t**)SZArrayLdElema(L_41, L_43, sizeof(String_t*))), /*hidden argument*/NULL);
		V_0 = L_44;
		int32_t L_45 = V_6;
		V_6 = ((int32_t)((int32_t)L_45+(int32_t)1));
	}

IL_00e3:
	{
		int32_t L_46 = V_6;
		StringU5BU5D_t1_238* L_47 = V_2;
		NullCheck(L_47);
		if ((((int32_t)L_46) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_47)->max_length)))))))
		{
			goto IL_00d2;
		}
	}

IL_00ed:
	{
		Uri_t3_3 * L_48 = ___uri;
		NullCheck(L_48);
		Uri_AppendQueryAndFragment_m3_1748(L_48, (&V_0), /*hidden argument*/NULL);
		String_t* L_49 = V_0;
		Uri_t3_3 * L_50 = (Uri_t3_3 *)il2cpp_codegen_object_new (Uri_t3_3_il2cpp_TypeInfo_var);
		Uri__ctor_m3_27(L_50, L_49, 2, /*hidden argument*/NULL);
		return L_50;
	}
}
// System.Void System.Uri::AppendQueryAndFragment(System.String&)
extern TypeInfo* Char_t1_15_il2cpp_TypeInfo_var;
extern TypeInfo* Uri_t3_3_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void Uri_AppendQueryAndFragment_m3_1748 (Uri_t3_3 * __this, String_t** ___result, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Char_t1_15_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(12);
		Uri_t3_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	String_t* G_B4_0 = {0};
	{
		String_t* L_0 = (__this->___query_6);
		NullCheck(L_0);
		int32_t L_1 = String_get_Length_m1_571(L_0, /*hidden argument*/NULL);
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_005e;
		}
	}
	{
		String_t* L_2 = (__this->___query_6);
		NullCheck(L_2);
		uint16_t L_3 = String_get_Chars_m1_442(L_2, 0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)((int32_t)63)))))
		{
			goto IL_0047;
		}
	}
	{
		uint16_t L_4 = ((int32_t)63);
		Object_t * L_5 = Box(Char_t1_15_il2cpp_TypeInfo_var, &L_4);
		String_t* L_6 = (__this->___query_6);
		NullCheck(L_6);
		String_t* L_7 = String_Substring_m1_454(L_6, 1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		String_t* L_8 = Uri_Unescape_m3_1754(NULL /*static, unused*/, L_7, 0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m1_556(NULL /*static, unused*/, L_5, L_8, /*hidden argument*/NULL);
		G_B4_0 = L_9;
		goto IL_0053;
	}

IL_0047:
	{
		String_t* L_10 = (__this->___query_6);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		String_t* L_11 = Uri_Unescape_m3_1754(NULL /*static, unused*/, L_10, 0, /*hidden argument*/NULL);
		G_B4_0 = L_11;
	}

IL_0053:
	{
		V_0 = G_B4_0;
		String_t** L_12 = ___result;
		String_t** L_13 = ___result;
		String_t* L_14 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Concat_m1_559(NULL /*static, unused*/, (*((String_t**)L_13)), L_14, /*hidden argument*/NULL);
		*((Object_t **)(L_12)) = (Object_t *)L_15;
	}

IL_005e:
	{
		String_t* L_16 = (__this->___fragment_7);
		NullCheck(L_16);
		int32_t L_17 = String_get_Length_m1_571(L_16, /*hidden argument*/NULL);
		if ((((int32_t)L_17) <= ((int32_t)0)))
		{
			goto IL_007e;
		}
	}
	{
		String_t** L_18 = ___result;
		String_t** L_19 = ___result;
		String_t* L_20 = (__this->___fragment_7);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = String_Concat_m1_559(NULL /*static, unused*/, (*((String_t**)L_19)), L_20, /*hidden argument*/NULL);
		*((Object_t **)(L_18)) = (Object_t *)L_21;
	}

IL_007e:
	{
		return;
	}
}
// System.String System.Uri::ToString()
extern TypeInfo* Uri_t3_3_il2cpp_TypeInfo_var;
extern "C" String_t* Uri_ToString_m3_1749 (Uri_t3_3 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Uri_t3_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = (__this->___cachedToString_15);
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		String_t* L_1 = (__this->___cachedToString_15);
		return L_1;
	}

IL_0012:
	{
		bool L_2 = (__this->___isAbsoluteUri_11);
		if (!L_2)
		{
			goto IL_0035;
		}
	}
	{
		String_t* L_3 = Uri_GetLeftPart_m3_1742(__this, 2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		String_t* L_4 = Uri_Unescape_m3_1754(NULL /*static, unused*/, L_3, 1, /*hidden argument*/NULL);
		__this->___cachedToString_15 = L_4;
		goto IL_0047;
	}

IL_0035:
	{
		String_t* L_5 = (__this->___path_5);
		String_t* L_6 = (String_t*)VirtFuncInvoker1< String_t*, String_t* >::Invoke(5 /* System.String System.Uri::Unescape(System.String) */, __this, L_5);
		__this->___cachedToString_15 = L_6;
	}

IL_0047:
	{
		String_t** L_7 = &(__this->___cachedToString_15);
		Uri_AppendQueryAndFragment_m3_1748(__this, L_7, /*hidden argument*/NULL);
		String_t* L_8 = (__this->___cachedToString_15);
		return L_8;
	}
}
// System.String System.Uri::EscapeString(System.String)
extern TypeInfo* Uri_t3_3_il2cpp_TypeInfo_var;
extern "C" String_t* Uri_EscapeString_m3_1750 (Object_t * __this /* static, unused */, String_t* ___str, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Uri_t3_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___str;
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		String_t* L_1 = Uri_EscapeString_m3_1751(NULL /*static, unused*/, L_0, 0, 1, 1, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String System.Uri::EscapeString(System.String,System.Boolean,System.Boolean,System.Boolean)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* StringBuilder_t1_247_il2cpp_TypeInfo_var;
extern TypeInfo* Uri_t3_3_il2cpp_TypeInfo_var;
extern TypeInfo* Encoding_t1_406_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t1_16_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral747;
extern Il2CppCodeGenString* _stringLiteral748;
extern "C" String_t* Uri_EscapeString_m3_1751 (Object_t * __this /* static, unused */, String_t* ___str, bool ___escapeReserved, bool ___escapeHex, bool ___escapeBrackets, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		StringBuilder_t1_247_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		Uri_t3_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		Encoding_t1_406_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		CharU5BU5D_t1_16_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(15);
		_stringLiteral747 = il2cpp_codegen_string_literal_from_index(747);
		_stringLiteral748 = il2cpp_codegen_string_literal_from_index(748);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t1_247 * V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	ByteU5BU5D_t1_109* V_3 = {0};
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	uint16_t V_6 = 0x0;
	{
		String_t* L_0 = ___str;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		return L_1;
	}

IL_000c:
	{
		StringBuilder_t1_247 * L_2 = (StringBuilder_t1_247 *)il2cpp_codegen_object_new (StringBuilder_t1_247_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1_12414(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		String_t* L_3 = ___str;
		NullCheck(L_3);
		int32_t L_4 = String_get_Length_m1_571(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		V_2 = 0;
		goto IL_0105;
	}

IL_0020:
	{
		String_t* L_5 = ___str;
		int32_t L_6 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		bool L_7 = Uri_IsHexEncoding_m3_1746(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0044;
		}
	}
	{
		StringBuilder_t1_247 * L_8 = V_0;
		String_t* L_9 = ___str;
		int32_t L_10 = V_2;
		NullCheck(L_9);
		String_t* L_11 = String_Substring_m1_455(L_9, L_10, 3, /*hidden argument*/NULL);
		NullCheck(L_8);
		StringBuilder_Append_m1_12438(L_8, L_11, /*hidden argument*/NULL);
		int32_t L_12 = V_2;
		V_2 = ((int32_t)((int32_t)L_12+(int32_t)2));
		goto IL_0101;
	}

IL_0044:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t1_406_il2cpp_TypeInfo_var);
		Encoding_t1_406 * L_13 = Encoding_get_UTF8_m1_12363(NULL /*static, unused*/, /*hidden argument*/NULL);
		CharU5BU5D_t1_16* L_14 = ((CharU5BU5D_t1_16*)SZArrayNew(CharU5BU5D_t1_16_il2cpp_TypeInfo_var, 1));
		String_t* L_15 = ___str;
		int32_t L_16 = V_2;
		NullCheck(L_15);
		uint16_t L_17 = String_get_Chars_m1_442(L_15, L_16, /*hidden argument*/NULL);
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_14, 0, sizeof(uint16_t))) = (uint16_t)L_17;
		NullCheck(L_13);
		ByteU5BU5D_t1_109* L_18 = (ByteU5BU5D_t1_109*)VirtFuncInvoker1< ByteU5BU5D_t1_109*, CharU5BU5D_t1_16* >::Invoke(13 /* System.Byte[] System.Text.Encoding::GetBytes(System.Char[]) */, L_13, L_14);
		V_3 = L_18;
		ByteU5BU5D_t1_109* L_19 = V_3;
		NullCheck(L_19);
		V_4 = (((int32_t)((int32_t)(((Array_t *)L_19)->max_length))));
		V_5 = 0;
		goto IL_00f8;
	}

IL_006c:
	{
		ByteU5BU5D_t1_109* L_20 = V_3;
		int32_t L_21 = V_5;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		int32_t L_22 = L_21;
		V_6 = (((int32_t)((uint16_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_20, L_22, sizeof(uint8_t))))));
		uint16_t L_23 = V_6;
		if ((((int32_t)L_23) <= ((int32_t)((int32_t)32))))
		{
			goto IL_00d6;
		}
	}
	{
		uint16_t L_24 = V_6;
		if ((((int32_t)L_24) >= ((int32_t)((int32_t)127))))
		{
			goto IL_00d6;
		}
	}
	{
		uint16_t L_25 = V_6;
		NullCheck(_stringLiteral747);
		int32_t L_26 = String_IndexOf_m1_500(_stringLiteral747, L_25, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_26) == ((uint32_t)(-1)))))
		{
			goto IL_00d6;
		}
	}
	{
		bool L_27 = ___escapeHex;
		if (!L_27)
		{
			goto IL_00a6;
		}
	}
	{
		uint16_t L_28 = V_6;
		if ((((int32_t)L_28) == ((int32_t)((int32_t)35))))
		{
			goto IL_00d6;
		}
	}

IL_00a6:
	{
		bool L_29 = ___escapeBrackets;
		if (!L_29)
		{
			goto IL_00be;
		}
	}
	{
		uint16_t L_30 = V_6;
		if ((((int32_t)L_30) == ((int32_t)((int32_t)91))))
		{
			goto IL_00d6;
		}
	}
	{
		uint16_t L_31 = V_6;
		if ((((int32_t)L_31) == ((int32_t)((int32_t)93))))
		{
			goto IL_00d6;
		}
	}

IL_00be:
	{
		bool L_32 = ___escapeReserved;
		if (!L_32)
		{
			goto IL_00e9;
		}
	}
	{
		uint16_t L_33 = V_6;
		NullCheck(_stringLiteral748);
		int32_t L_34 = String_IndexOf_m1_500(_stringLiteral748, L_33, /*hidden argument*/NULL);
		if ((((int32_t)L_34) == ((int32_t)(-1))))
		{
			goto IL_00e9;
		}
	}

IL_00d6:
	{
		StringBuilder_t1_247 * L_35 = V_0;
		uint16_t L_36 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		String_t* L_37 = Uri_HexEscape_m3_1744(NULL /*static, unused*/, L_36, /*hidden argument*/NULL);
		NullCheck(L_35);
		StringBuilder_Append_m1_12438(L_35, L_37, /*hidden argument*/NULL);
		goto IL_00f2;
	}

IL_00e9:
	{
		StringBuilder_t1_247 * L_38 = V_0;
		uint16_t L_39 = V_6;
		NullCheck(L_38);
		StringBuilder_Append_m1_12452(L_38, L_39, /*hidden argument*/NULL);
	}

IL_00f2:
	{
		int32_t L_40 = V_5;
		V_5 = ((int32_t)((int32_t)L_40+(int32_t)1));
	}

IL_00f8:
	{
		int32_t L_41 = V_5;
		int32_t L_42 = V_4;
		if ((((int32_t)L_41) < ((int32_t)L_42)))
		{
			goto IL_006c;
		}
	}

IL_0101:
	{
		int32_t L_43 = V_2;
		V_2 = ((int32_t)((int32_t)L_43+(int32_t)1));
	}

IL_0105:
	{
		int32_t L_44 = V_2;
		int32_t L_45 = V_1;
		if ((((int32_t)L_44) < ((int32_t)L_45)))
		{
			goto IL_0020;
		}
	}
	{
		StringBuilder_t1_247 * L_46 = V_0;
		NullCheck(L_46);
		String_t* L_47 = StringBuilder_ToString_m1_12428(L_46, /*hidden argument*/NULL);
		return L_47;
	}
}
// System.Void System.Uri::ParseUri(System.UriKind)
extern TypeInfo* Uri_t3_3_il2cpp_TypeInfo_var;
extern TypeInfo* CultureInfo_t1_277_il2cpp_TypeInfo_var;
extern "C" void Uri_ParseUri_m3_1752 (Uri_t3_3 * __this, int32_t ___kind, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Uri_t3_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		CultureInfo_t1_277_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(31);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___kind;
		String_t* L_1 = (__this->___source_1);
		Uri_Parse_m3_1758(__this, L_0, L_1, /*hidden argument*/NULL);
		bool L_2 = (__this->___userEscaped_13);
		if (!L_2)
		{
			goto IL_0019;
		}
	}
	{
		return;
	}

IL_0019:
	{
		String_t* L_3 = (__this->___host_3);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		String_t* L_4 = Uri_EscapeString_m3_1751(NULL /*static, unused*/, L_3, 0, 1, 0, /*hidden argument*/NULL);
		__this->___host_3 = L_4;
		String_t* L_5 = (__this->___host_3);
		NullCheck(L_5);
		int32_t L_6 = String_get_Length_m1_571(L_5, /*hidden argument*/NULL);
		if ((((int32_t)L_6) <= ((int32_t)1)))
		{
			goto IL_0086;
		}
	}
	{
		String_t* L_7 = (__this->___host_3);
		NullCheck(L_7);
		uint16_t L_8 = String_get_Chars_m1_442(L_7, 0, /*hidden argument*/NULL);
		if ((((int32_t)L_8) == ((int32_t)((int32_t)91))))
		{
			goto IL_0086;
		}
	}
	{
		String_t* L_9 = (__this->___host_3);
		String_t* L_10 = (__this->___host_3);
		NullCheck(L_10);
		int32_t L_11 = String_get_Length_m1_571(L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		uint16_t L_12 = String_get_Chars_m1_442(L_9, ((int32_t)((int32_t)L_11-(int32_t)1)), /*hidden argument*/NULL);
		if ((((int32_t)L_12) == ((int32_t)((int32_t)93))))
		{
			goto IL_0086;
		}
	}
	{
		String_t* L_13 = (__this->___host_3);
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t1_277_il2cpp_TypeInfo_var);
		CultureInfo_t1_277 * L_14 = CultureInfo_get_InvariantCulture_m1_3911(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_13);
		String_t* L_15 = String_ToLower_m1_541(L_13, L_14, /*hidden argument*/NULL);
		__this->___host_3 = L_15;
	}

IL_0086:
	{
		String_t* L_16 = (__this->___path_5);
		NullCheck(L_16);
		int32_t L_17 = String_get_Length_m1_571(L_16, /*hidden argument*/NULL);
		if ((((int32_t)L_17) <= ((int32_t)0)))
		{
			goto IL_00a8;
		}
	}
	{
		String_t* L_18 = (__this->___path_5);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		String_t* L_19 = Uri_EscapeString_m3_1750(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		__this->___path_5 = L_19;
	}

IL_00a8:
	{
		return;
	}
}
// System.String System.Uri::Unescape(System.String)
extern TypeInfo* Uri_t3_3_il2cpp_TypeInfo_var;
extern "C" String_t* Uri_Unescape_m3_1753 (Uri_t3_3 * __this, String_t* ___str, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Uri_t3_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___str;
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		String_t* L_1 = Uri_Unescape_m3_1754(NULL /*static, unused*/, L_0, 0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String System.Uri::Unescape(System.String,System.Boolean)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* StringBuilder_t1_247_il2cpp_TypeInfo_var;
extern TypeInfo* Uri_t3_3_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral749;
extern Il2CppCodeGenString* _stringLiteral4002;
extern Il2CppCodeGenString* _stringLiteral4668;
extern "C" String_t* Uri_Unescape_m3_1754 (Object_t * __this /* static, unused */, String_t* ___str, bool ___excludeSpecial, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		StringBuilder_t1_247_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		Uri_t3_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		_stringLiteral749 = il2cpp_codegen_string_literal_from_index(749);
		_stringLiteral4002 = il2cpp_codegen_string_literal_from_index(4002);
		_stringLiteral4668 = il2cpp_codegen_string_literal_from_index(4668);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t1_247 * V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	uint16_t V_3 = 0x0;
	uint16_t V_4 = 0x0;
	uint16_t V_5 = 0x0;
	{
		String_t* L_0 = ___str;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		return L_1;
	}

IL_000c:
	{
		StringBuilder_t1_247 * L_2 = (StringBuilder_t1_247 *)il2cpp_codegen_object_new (StringBuilder_t1_247_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1_12414(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		String_t* L_3 = ___str;
		NullCheck(L_3);
		int32_t L_4 = String_get_Length_m1_571(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		V_2 = 0;
		goto IL_00ca;
	}

IL_0020:
	{
		String_t* L_5 = ___str;
		int32_t L_6 = V_2;
		NullCheck(L_5);
		uint16_t L_7 = String_get_Chars_m1_442(L_5, L_6, /*hidden argument*/NULL);
		V_3 = L_7;
		uint16_t L_8 = V_3;
		if ((!(((uint32_t)L_8) == ((uint32_t)((int32_t)37)))))
		{
			goto IL_00be;
		}
	}
	{
		String_t* L_9 = ___str;
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		uint16_t L_10 = Uri_HexUnescapeMultiByte_m3_1762(NULL /*static, unused*/, L_9, (&V_2), (&V_4), /*hidden argument*/NULL);
		V_5 = L_10;
		bool L_11 = ___excludeSpecial;
		if (!L_11)
		{
			goto IL_005c;
		}
	}
	{
		uint16_t L_12 = V_5;
		if ((!(((uint32_t)L_12) == ((uint32_t)((int32_t)35)))))
		{
			goto IL_005c;
		}
	}
	{
		StringBuilder_t1_247 * L_13 = V_0;
		NullCheck(L_13);
		StringBuilder_Append_m1_12438(L_13, _stringLiteral749, /*hidden argument*/NULL);
		goto IL_00b5;
	}

IL_005c:
	{
		bool L_14 = ___excludeSpecial;
		if (!L_14)
		{
			goto IL_007c;
		}
	}
	{
		uint16_t L_15 = V_5;
		if ((!(((uint32_t)L_15) == ((uint32_t)((int32_t)37)))))
		{
			goto IL_007c;
		}
	}
	{
		StringBuilder_t1_247 * L_16 = V_0;
		NullCheck(L_16);
		StringBuilder_Append_m1_12438(L_16, _stringLiteral4002, /*hidden argument*/NULL);
		goto IL_00b5;
	}

IL_007c:
	{
		bool L_17 = ___excludeSpecial;
		if (!L_17)
		{
			goto IL_009c;
		}
	}
	{
		uint16_t L_18 = V_5;
		if ((!(((uint32_t)L_18) == ((uint32_t)((int32_t)63)))))
		{
			goto IL_009c;
		}
	}
	{
		StringBuilder_t1_247 * L_19 = V_0;
		NullCheck(L_19);
		StringBuilder_Append_m1_12438(L_19, _stringLiteral4668, /*hidden argument*/NULL);
		goto IL_00b5;
	}

IL_009c:
	{
		StringBuilder_t1_247 * L_20 = V_0;
		uint16_t L_21 = V_5;
		NullCheck(L_20);
		StringBuilder_Append_m1_12452(L_20, L_21, /*hidden argument*/NULL);
		uint16_t L_22 = V_4;
		if (!L_22)
		{
			goto IL_00b5;
		}
	}
	{
		StringBuilder_t1_247 * L_23 = V_0;
		uint16_t L_24 = V_4;
		NullCheck(L_23);
		StringBuilder_Append_m1_12452(L_23, L_24, /*hidden argument*/NULL);
	}

IL_00b5:
	{
		int32_t L_25 = V_2;
		V_2 = ((int32_t)((int32_t)L_25-(int32_t)1));
		goto IL_00c6;
	}

IL_00be:
	{
		StringBuilder_t1_247 * L_26 = V_0;
		uint16_t L_27 = V_3;
		NullCheck(L_26);
		StringBuilder_Append_m1_12452(L_26, L_27, /*hidden argument*/NULL);
	}

IL_00c6:
	{
		int32_t L_28 = V_2;
		V_2 = ((int32_t)((int32_t)L_28+(int32_t)1));
	}

IL_00ca:
	{
		int32_t L_29 = V_2;
		int32_t L_30 = V_1;
		if ((((int32_t)L_29) < ((int32_t)L_30)))
		{
			goto IL_0020;
		}
	}
	{
		StringBuilder_t1_247 * L_31 = V_0;
		NullCheck(L_31);
		String_t* L_32 = StringBuilder_ToString_m1_12428(L_31, /*hidden argument*/NULL);
		return L_32;
	}
}
// System.Void System.Uri::ParseAsWindowsUNC(System.String)
extern TypeInfo* Uri_t3_3_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t1_16_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral313;
extern Il2CppCodeGenString* _stringLiteral740;
extern "C" void Uri_ParseAsWindowsUNC_m3_1755 (Uri_t3_3 * __this, String_t* ___uriString, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Uri_t3_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		CharU5BU5D_t1_16_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(15);
		_stringLiteral313 = il2cpp_codegen_string_literal_from_index(313);
		_stringLiteral740 = il2cpp_codegen_string_literal_from_index(740);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		String_t* L_0 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___UriSchemeFile_20;
		__this->___scheme_2 = L_0;
		__this->___port_4 = (-1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___fragment_7 = L_1;
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___query_6 = L_2;
		__this->___isUnc_9 = 1;
		String_t* L_3 = ___uriString;
		CharU5BU5D_t1_16* L_4 = ((CharU5BU5D_t1_16*)SZArrayNew(CharU5BU5D_t1_16_il2cpp_TypeInfo_var, 1));
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_4, 0, sizeof(uint16_t))) = (uint16_t)((int32_t)92);
		NullCheck(L_3);
		String_t* L_5 = String_TrimStart_m1_459(L_3, L_4, /*hidden argument*/NULL);
		___uriString = L_5;
		String_t* L_6 = ___uriString;
		NullCheck(L_6);
		int32_t L_7 = String_IndexOf_m1_500(L_6, ((int32_t)92), /*hidden argument*/NULL);
		V_0 = L_7;
		int32_t L_8 = V_0;
		if ((((int32_t)L_8) <= ((int32_t)0)))
		{
			goto IL_0072;
		}
	}
	{
		String_t* L_9 = ___uriString;
		int32_t L_10 = V_0;
		NullCheck(L_9);
		String_t* L_11 = String_Substring_m1_454(L_9, L_10, /*hidden argument*/NULL);
		__this->___path_5 = L_11;
		String_t* L_12 = ___uriString;
		int32_t L_13 = V_0;
		NullCheck(L_12);
		String_t* L_14 = String_Substring_m1_455(L_12, 0, L_13, /*hidden argument*/NULL);
		__this->___host_3 = L_14;
		goto IL_0084;
	}

IL_0072:
	{
		String_t* L_15 = ___uriString;
		__this->___host_3 = L_15;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___path_5 = L_16;
	}

IL_0084:
	{
		String_t* L_17 = (__this->___path_5);
		NullCheck(L_17);
		String_t* L_18 = String_Replace_m1_536(L_17, _stringLiteral313, _stringLiteral740, /*hidden argument*/NULL);
		__this->___path_5 = L_18;
		return;
	}
}
// System.String System.Uri::ParseAsWindowsAbsoluteFilePath(System.String)
extern TypeInfo* Uri_t3_3_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral750;
extern Il2CppCodeGenString* _stringLiteral313;
extern Il2CppCodeGenString* _stringLiteral740;
extern "C" String_t* Uri_ParseAsWindowsAbsoluteFilePath_m3_1756 (Uri_t3_3 * __this, String_t* ___uriString, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Uri_t3_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral750 = il2cpp_codegen_string_literal_from_index(750);
		_stringLiteral313 = il2cpp_codegen_string_literal_from_index(313);
		_stringLiteral740 = il2cpp_codegen_string_literal_from_index(740);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___uriString;
		NullCheck(L_0);
		int32_t L_1 = String_get_Length_m1_571(L_0, /*hidden argument*/NULL);
		if ((((int32_t)L_1) <= ((int32_t)2)))
		{
			goto IL_002e;
		}
	}
	{
		String_t* L_2 = ___uriString;
		NullCheck(L_2);
		uint16_t L_3 = String_get_Chars_m1_442(L_2, 2, /*hidden argument*/NULL);
		if ((((int32_t)L_3) == ((int32_t)((int32_t)92))))
		{
			goto IL_002e;
		}
	}
	{
		String_t* L_4 = ___uriString;
		NullCheck(L_4);
		uint16_t L_5 = String_get_Chars_m1_442(L_4, 2, /*hidden argument*/NULL);
		if ((((int32_t)L_5) == ((int32_t)((int32_t)47))))
		{
			goto IL_002e;
		}
	}
	{
		return _stringLiteral750;
	}

IL_002e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		String_t* L_6 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___UriSchemeFile_20;
		__this->___scheme_2 = L_6;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___host_3 = L_7;
		__this->___port_4 = (-1);
		String_t* L_8 = ___uriString;
		NullCheck(L_8);
		String_t* L_9 = String_Replace_m1_536(L_8, _stringLiteral313, _stringLiteral740, /*hidden argument*/NULL);
		__this->___path_5 = L_9;
		String_t* L_10 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___fragment_7 = L_10;
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___query_6 = L_11;
		return (String_t*)NULL;
	}
}
// System.Void System.Uri::ParseAsUnixAbsoluteFilePath(System.String)
extern TypeInfo* Uri_t3_3_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t1_16_il2cpp_TypeInfo_var;
extern TypeInfo* Char_t1_15_il2cpp_TypeInfo_var;
extern "C" void Uri_ParseAsUnixAbsoluteFilePath_m3_1757 (Uri_t3_3 * __this, String_t* ___uriString, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Uri_t3_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		CharU5BU5D_t1_16_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(15);
		Char_t1_15_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(12);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___isUnixFilePath_0 = 1;
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		String_t* L_0 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___UriSchemeFile_20;
		__this->___scheme_2 = L_0;
		__this->___port_4 = (-1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___fragment_7 = L_1;
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___query_6 = L_2;
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___host_3 = L_3;
		__this->___path_5 = (String_t*)NULL;
		String_t* L_4 = ___uriString;
		NullCheck(L_4);
		int32_t L_5 = String_get_Length_m1_571(L_4, /*hidden argument*/NULL);
		if ((((int32_t)L_5) < ((int32_t)2)))
		{
			goto IL_008f;
		}
	}
	{
		String_t* L_6 = ___uriString;
		NullCheck(L_6);
		uint16_t L_7 = String_get_Chars_m1_442(L_6, 0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_7) == ((uint32_t)((int32_t)47)))))
		{
			goto IL_008f;
		}
	}
	{
		String_t* L_8 = ___uriString;
		NullCheck(L_8);
		uint16_t L_9 = String_get_Chars_m1_442(L_8, 1, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_9) == ((uint32_t)((int32_t)47)))))
		{
			goto IL_008f;
		}
	}
	{
		String_t* L_10 = ___uriString;
		CharU5BU5D_t1_16* L_11 = ((CharU5BU5D_t1_16*)SZArrayNew(CharU5BU5D_t1_16_il2cpp_TypeInfo_var, 1));
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_11, 0, sizeof(uint16_t))) = (uint16_t)((int32_t)47);
		NullCheck(L_10);
		String_t* L_12 = String_TrimStart_m1_459(L_10, L_11, /*hidden argument*/NULL);
		___uriString = L_12;
		uint16_t L_13 = ((int32_t)47);
		Object_t * L_14 = Box(Char_t1_15_il2cpp_TypeInfo_var, &L_13);
		String_t* L_15 = ___uriString;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m1_556(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
		__this->___path_5 = L_16;
	}

IL_008f:
	{
		String_t* L_17 = (__this->___path_5);
		if (L_17)
		{
			goto IL_00a1;
		}
	}
	{
		String_t* L_18 = ___uriString;
		__this->___path_5 = L_18;
	}

IL_00a1:
	{
		return;
	}
}
// System.Void System.Uri::Parse(System.UriKind,System.String)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* UriFormatException_t3_19_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral752;
extern "C" void Uri_Parse_m3_1758 (Uri_t3_3 * __this, int32_t ___kind, String_t* ___uriString, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		UriFormatException_t3_19_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1299);
		_stringLiteral752 = il2cpp_codegen_string_literal_from_index(752);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	{
		String_t* L_0 = ___uriString;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, _stringLiteral752, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		int32_t L_2 = ___kind;
		String_t* L_3 = ___uriString;
		String_t* L_4 = Uri_ParseNoExceptions_m3_1759(__this, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		String_t* L_5 = V_0;
		if (!L_5)
		{
			goto IL_0027;
		}
	}
	{
		String_t* L_6 = V_0;
		UriFormatException_t3_19 * L_7 = (UriFormatException_t3_19 *)il2cpp_codegen_object_new (UriFormatException_t3_19_il2cpp_TypeInfo_var);
		UriFormatException__ctor_m3_1774(L_7, L_6, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_7);
	}

IL_0027:
	{
		return;
	}
}
// System.String System.Uri::ParseNoExceptions(System.UriKind,System.String)
extern TypeInfo* Path_t1_437_il2cpp_TypeInfo_var;
extern TypeInfo* Uri_t3_3_il2cpp_TypeInfo_var;
extern TypeInfo* CultureInfo_t1_277_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Char_t1_15_il2cpp_TypeInfo_var;
extern TypeInfo* IPv6Address_t3_122_il2cpp_TypeInfo_var;
extern TypeInfo* DefaultUriParser_t3_231_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4669;
extern Il2CppCodeGenString* _stringLiteral4662;
extern Il2CppCodeGenString* _stringLiteral4670;
extern Il2CppCodeGenString* _stringLiteral4671;
extern Il2CppCodeGenString* _stringLiteral1111;
extern Il2CppCodeGenString* _stringLiteral4672;
extern Il2CppCodeGenString* _stringLiteral4673;
extern Il2CppCodeGenString* _stringLiteral740;
extern Il2CppCodeGenString* _stringLiteral4674;
extern Il2CppCodeGenString* _stringLiteral751;
extern Il2CppCodeGenString* _stringLiteral760;
extern Il2CppCodeGenString* _stringLiteral264;
extern Il2CppCodeGenString* _stringLiteral266;
extern Il2CppCodeGenString* _stringLiteral4675;
extern Il2CppCodeGenString* _stringLiteral106;
extern "C" String_t* Uri_ParseNoExceptions_m3_1759 (Uri_t3_3 * __this, int32_t ___kind, String_t* ___uriString, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Path_t1_437_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(131);
		Uri_t3_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		CultureInfo_t1_277_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(31);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Char_t1_15_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(12);
		IPv6Address_t3_122_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1454);
		DefaultUriParser_t3_231_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1555);
		_stringLiteral4669 = il2cpp_codegen_string_literal_from_index(4669);
		_stringLiteral4662 = il2cpp_codegen_string_literal_from_index(4662);
		_stringLiteral4670 = il2cpp_codegen_string_literal_from_index(4670);
		_stringLiteral4671 = il2cpp_codegen_string_literal_from_index(4671);
		_stringLiteral1111 = il2cpp_codegen_string_literal_from_index(1111);
		_stringLiteral4672 = il2cpp_codegen_string_literal_from_index(4672);
		_stringLiteral4673 = il2cpp_codegen_string_literal_from_index(4673);
		_stringLiteral740 = il2cpp_codegen_string_literal_from_index(740);
		_stringLiteral4674 = il2cpp_codegen_string_literal_from_index(4674);
		_stringLiteral751 = il2cpp_codegen_string_literal_from_index(751);
		_stringLiteral760 = il2cpp_codegen_string_literal_from_index(760);
		_stringLiteral264 = il2cpp_codegen_string_literal_from_index(264);
		_stringLiteral266 = il2cpp_codegen_string_literal_from_index(266);
		_stringLiteral4675 = il2cpp_codegen_string_literal_from_index(4675);
		_stringLiteral106 = il2cpp_codegen_string_literal_from_index(106);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	String_t* V_2 = {0};
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	bool V_5 = false;
	bool V_6 = false;
	bool V_7 = false;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	String_t* V_10 = {0};
	bool V_11 = false;
	IPv6Address_t3_122 * V_12 = {0};
	UriFormatException_t3_19 * V_13 = {0};
	int32_t G_B50_0 = 0;
	int32_t G_B55_0 = 0;
	int32_t G_B57_0 = 0;
	int32_t G_B139_0 = 0;
	{
		String_t* L_0 = ___uriString;
		NullCheck(L_0);
		String_t* L_1 = String_Trim_m1_457(L_0, /*hidden argument*/NULL);
		___uriString = L_1;
		String_t* L_2 = ___uriString;
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m1_571(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		if (L_4)
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_5 = ___kind;
		if ((((int32_t)L_5) == ((int32_t)2)))
		{
			goto IL_0022;
		}
	}
	{
		int32_t L_6 = ___kind;
		if (L_6)
		{
			goto IL_002b;
		}
	}

IL_0022:
	{
		__this->___isAbsoluteUri_11 = 0;
		return (String_t*)NULL;
	}

IL_002b:
	{
		int32_t L_7 = V_0;
		if ((((int32_t)L_7) > ((int32_t)1)))
		{
			goto IL_003f;
		}
	}
	{
		int32_t L_8 = ___kind;
		if ((((int32_t)L_8) == ((int32_t)2)))
		{
			goto IL_003f;
		}
	}
	{
		return _stringLiteral4669;
	}

IL_003f:
	{
		V_1 = 0;
		String_t* L_9 = ___uriString;
		NullCheck(L_9);
		int32_t L_10 = String_IndexOf_m1_500(L_9, ((int32_t)58), /*hidden argument*/NULL);
		V_1 = L_10;
		int32_t L_11 = V_1;
		if (L_11)
		{
			goto IL_0056;
		}
	}
	{
		return _stringLiteral4662;
	}

IL_0056:
	{
		int32_t L_12 = V_1;
		if ((((int32_t)L_12) >= ((int32_t)0)))
		{
			goto IL_00d5;
		}
	}
	{
		String_t* L_13 = ___uriString;
		NullCheck(L_13);
		uint16_t L_14 = String_get_Chars_m1_442(L_13, 0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_14) == ((uint32_t)((int32_t)47)))))
		{
			goto IL_0091;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1_437_il2cpp_TypeInfo_var);
		uint16_t L_15 = ((Path_t1_437_StaticFields*)Path_t1_437_il2cpp_TypeInfo_var->static_fields)->___DirectorySeparatorChar_2;
		if ((!(((uint32_t)L_15) == ((uint32_t)((int32_t)47)))))
		{
			goto IL_0091;
		}
	}
	{
		String_t* L_16 = ___uriString;
		Uri_ParseAsUnixAbsoluteFilePath_m3_1757(__this, L_16, /*hidden argument*/NULL);
		int32_t L_17 = ___kind;
		if ((!(((uint32_t)L_17) == ((uint32_t)2))))
		{
			goto IL_008c;
		}
	}
	{
		__this->___isAbsoluteUri_11 = 0;
	}

IL_008c:
	{
		goto IL_00d3;
	}

IL_0091:
	{
		String_t* L_18 = ___uriString;
		NullCheck(L_18);
		int32_t L_19 = String_get_Length_m1_571(L_18, /*hidden argument*/NULL);
		if ((((int32_t)L_19) < ((int32_t)2)))
		{
			goto IL_00c5;
		}
	}
	{
		String_t* L_20 = ___uriString;
		NullCheck(L_20);
		uint16_t L_21 = String_get_Chars_m1_442(L_20, 0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_21) == ((uint32_t)((int32_t)92)))))
		{
			goto IL_00c5;
		}
	}
	{
		String_t* L_22 = ___uriString;
		NullCheck(L_22);
		uint16_t L_23 = String_get_Chars_m1_442(L_22, 1, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_23) == ((uint32_t)((int32_t)92)))))
		{
			goto IL_00c5;
		}
	}
	{
		String_t* L_24 = ___uriString;
		Uri_ParseAsWindowsUNC_m3_1755(__this, L_24, /*hidden argument*/NULL);
		goto IL_00d3;
	}

IL_00c5:
	{
		__this->___isAbsoluteUri_11 = 0;
		String_t* L_25 = ___uriString;
		__this->___path_5 = L_25;
	}

IL_00d3:
	{
		return (String_t*)NULL;
	}

IL_00d5:
	{
		int32_t L_26 = V_1;
		if ((!(((uint32_t)L_26) == ((uint32_t)1))))
		{
			goto IL_0105;
		}
	}
	{
		String_t* L_27 = ___uriString;
		NullCheck(L_27);
		uint16_t L_28 = String_get_Chars_m1_442(L_27, 0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		bool L_29 = Uri_IsAlpha_m3_1738(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		if (L_29)
		{
			goto IL_00f3;
		}
	}
	{
		return _stringLiteral4670;
	}

IL_00f3:
	{
		String_t* L_30 = ___uriString;
		String_t* L_31 = Uri_ParseAsWindowsAbsoluteFilePath_m3_1756(__this, L_30, /*hidden argument*/NULL);
		V_2 = L_31;
		String_t* L_32 = V_2;
		if (!L_32)
		{
			goto IL_0103;
		}
	}
	{
		String_t* L_33 = V_2;
		return L_33;
	}

IL_0103:
	{
		return (String_t*)NULL;
	}

IL_0105:
	{
		String_t* L_34 = ___uriString;
		int32_t L_35 = V_1;
		NullCheck(L_34);
		String_t* L_36 = String_Substring_m1_455(L_34, 0, L_35, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t1_277_il2cpp_TypeInfo_var);
		CultureInfo_t1_277 * L_37 = CultureInfo_get_InvariantCulture_m1_3911(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_36);
		String_t* L_38 = String_ToLower_m1_541(L_36, L_37, /*hidden argument*/NULL);
		__this->___scheme_2 = L_38;
		String_t* L_39 = (__this->___scheme_2);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		bool L_40 = Uri_CheckSchemeName_m3_1737(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
		if (L_40)
		{
			goto IL_0138;
		}
	}
	{
		String_t* L_41 = Locale_GetText_m3_37(NULL /*static, unused*/, _stringLiteral4671, /*hidden argument*/NULL);
		return L_41;
	}

IL_0138:
	{
		int32_t L_42 = V_1;
		V_3 = ((int32_t)((int32_t)L_42+(int32_t)1));
		String_t* L_43 = ___uriString;
		NullCheck(L_43);
		int32_t L_44 = String_get_Length_m1_571(L_43, /*hidden argument*/NULL);
		V_4 = L_44;
		String_t* L_45 = ___uriString;
		int32_t L_46 = V_3;
		NullCheck(L_45);
		int32_t L_47 = String_IndexOf_m1_501(L_45, ((int32_t)35), L_46, /*hidden argument*/NULL);
		V_1 = L_47;
		bool L_48 = Uri_get_IsUnc_m3_1729(__this, /*hidden argument*/NULL);
		if (L_48)
		{
			goto IL_019e;
		}
	}
	{
		int32_t L_49 = V_1;
		if ((((int32_t)L_49) == ((int32_t)(-1))))
		{
			goto IL_019e;
		}
	}
	{
		bool L_50 = (__this->___userEscaped_13);
		if (!L_50)
		{
			goto IL_017d;
		}
	}
	{
		String_t* L_51 = ___uriString;
		int32_t L_52 = V_1;
		NullCheck(L_51);
		String_t* L_53 = String_Substring_m1_454(L_51, L_52, /*hidden argument*/NULL);
		__this->___fragment_7 = L_53;
		goto IL_019b;
	}

IL_017d:
	{
		String_t* L_54 = ___uriString;
		int32_t L_55 = V_1;
		NullCheck(L_54);
		String_t* L_56 = String_Substring_m1_454(L_54, ((int32_t)((int32_t)L_55+(int32_t)1)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		String_t* L_57 = Uri_EscapeString_m3_1750(NULL /*static, unused*/, L_56, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_58 = String_Concat_m1_559(NULL /*static, unused*/, _stringLiteral1111, L_57, /*hidden argument*/NULL);
		__this->___fragment_7 = L_58;
	}

IL_019b:
	{
		int32_t L_59 = V_1;
		V_4 = L_59;
	}

IL_019e:
	{
		String_t* L_60 = ___uriString;
		int32_t L_61 = V_3;
		int32_t L_62 = V_4;
		int32_t L_63 = V_3;
		NullCheck(L_60);
		int32_t L_64 = String_IndexOf_m1_502(L_60, ((int32_t)63), L_61, ((int32_t)((int32_t)L_62-(int32_t)L_63)), /*hidden argument*/NULL);
		V_1 = L_64;
		int32_t L_65 = V_1;
		if ((((int32_t)L_65) == ((int32_t)(-1))))
		{
			goto IL_01e3;
		}
	}
	{
		String_t* L_66 = ___uriString;
		int32_t L_67 = V_1;
		int32_t L_68 = V_4;
		int32_t L_69 = V_1;
		NullCheck(L_66);
		String_t* L_70 = String_Substring_m1_455(L_66, L_67, ((int32_t)((int32_t)L_68-(int32_t)L_69)), /*hidden argument*/NULL);
		__this->___query_6 = L_70;
		int32_t L_71 = V_1;
		V_4 = L_71;
		bool L_72 = (__this->___userEscaped_13);
		if (L_72)
		{
			goto IL_01e3;
		}
	}
	{
		String_t* L_73 = (__this->___query_6);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		String_t* L_74 = Uri_EscapeString_m3_1750(NULL /*static, unused*/, L_73, /*hidden argument*/NULL);
		__this->___query_6 = L_74;
	}

IL_01e3:
	{
		String_t* L_75 = (__this->___scheme_2);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		bool L_76 = Uri_IsPredefinedScheme_m3_1766(NULL /*static, unused*/, L_75, /*hidden argument*/NULL);
		if (!L_76)
		{
			goto IL_0255;
		}
	}
	{
		String_t* L_77 = (__this->___scheme_2);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		String_t* L_78 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___UriSchemeMailto_25;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_79 = String_op_Inequality_m1_602(NULL /*static, unused*/, L_77, L_78, /*hidden argument*/NULL);
		if (!L_79)
		{
			goto IL_0255;
		}
	}
	{
		String_t* L_80 = (__this->___scheme_2);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		String_t* L_81 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___UriSchemeNews_26;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_82 = String_op_Inequality_m1_602(NULL /*static, unused*/, L_80, L_81, /*hidden argument*/NULL);
		if (!L_82)
		{
			goto IL_0255;
		}
	}
	{
		int32_t L_83 = V_4;
		int32_t L_84 = V_3;
		if ((((int32_t)((int32_t)((int32_t)L_83-(int32_t)L_84))) < ((int32_t)2)))
		{
			goto IL_024f;
		}
	}
	{
		int32_t L_85 = V_4;
		int32_t L_86 = V_3;
		if ((((int32_t)((int32_t)((int32_t)L_85-(int32_t)L_86))) < ((int32_t)2)))
		{
			goto IL_0255;
		}
	}
	{
		String_t* L_87 = ___uriString;
		int32_t L_88 = V_3;
		NullCheck(L_87);
		uint16_t L_89 = String_get_Chars_m1_442(L_87, L_88, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_89) == ((uint32_t)((int32_t)47)))))
		{
			goto IL_0255;
		}
	}
	{
		String_t* L_90 = ___uriString;
		int32_t L_91 = V_3;
		NullCheck(L_90);
		uint16_t L_92 = String_get_Chars_m1_442(L_90, ((int32_t)((int32_t)L_91+(int32_t)1)), /*hidden argument*/NULL);
		if ((((int32_t)L_92) == ((int32_t)((int32_t)47))))
		{
			goto IL_0255;
		}
	}

IL_024f:
	{
		return _stringLiteral4672;
	}

IL_0255:
	{
		int32_t L_93 = V_4;
		int32_t L_94 = V_3;
		if ((((int32_t)((int32_t)((int32_t)L_93-(int32_t)L_94))) < ((int32_t)2)))
		{
			goto IL_027c;
		}
	}
	{
		String_t* L_95 = ___uriString;
		int32_t L_96 = V_3;
		NullCheck(L_95);
		uint16_t L_97 = String_get_Chars_m1_442(L_95, L_96, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_97) == ((uint32_t)((int32_t)47)))))
		{
			goto IL_027c;
		}
	}
	{
		String_t* L_98 = ___uriString;
		int32_t L_99 = V_3;
		NullCheck(L_98);
		uint16_t L_100 = String_get_Chars_m1_442(L_98, ((int32_t)((int32_t)L_99+(int32_t)1)), /*hidden argument*/NULL);
		G_B50_0 = ((((int32_t)L_100) == ((int32_t)((int32_t)47)))? 1 : 0);
		goto IL_027d;
	}

IL_027c:
	{
		G_B50_0 = 0;
	}

IL_027d:
	{
		V_5 = G_B50_0;
		String_t* L_101 = (__this->___scheme_2);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		String_t* L_102 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___UriSchemeFile_20;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_103 = String_op_Equality_m1_601(NULL /*static, unused*/, L_101, L_102, /*hidden argument*/NULL);
		if (!L_103)
		{
			goto IL_02b7;
		}
	}
	{
		bool L_104 = V_5;
		if (!L_104)
		{
			goto IL_02b7;
		}
	}
	{
		int32_t L_105 = V_4;
		int32_t L_106 = V_3;
		if ((((int32_t)((int32_t)((int32_t)L_105-(int32_t)L_106))) == ((int32_t)2)))
		{
			goto IL_02b4;
		}
	}
	{
		String_t* L_107 = ___uriString;
		int32_t L_108 = V_3;
		NullCheck(L_107);
		uint16_t L_109 = String_get_Chars_m1_442(L_107, ((int32_t)((int32_t)L_108+(int32_t)2)), /*hidden argument*/NULL);
		G_B55_0 = ((((int32_t)L_109) == ((int32_t)((int32_t)47)))? 1 : 0);
		goto IL_02b5;
	}

IL_02b4:
	{
		G_B55_0 = 1;
	}

IL_02b5:
	{
		G_B57_0 = G_B55_0;
		goto IL_02b8;
	}

IL_02b7:
	{
		G_B57_0 = 0;
	}

IL_02b8:
	{
		V_6 = G_B57_0;
		V_7 = 0;
		bool L_110 = V_5;
		if (!L_110)
		{
			goto IL_03a8;
		}
	}
	{
		int32_t L_111 = ___kind;
		if ((!(((uint32_t)L_111) == ((uint32_t)2))))
		{
			goto IL_02d1;
		}
	}
	{
		return _stringLiteral4673;
	}

IL_02d1:
	{
		String_t* L_112 = (__this->___scheme_2);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		String_t* L_113 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___UriSchemeMailto_25;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_114 = String_op_Inequality_m1_602(NULL /*static, unused*/, L_112, L_113, /*hidden argument*/NULL);
		if (!L_114)
		{
			goto IL_02ff;
		}
	}
	{
		String_t* L_115 = (__this->___scheme_2);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		String_t* L_116 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___UriSchemeNews_26;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_117 = String_op_Inequality_m1_602(NULL /*static, unused*/, L_115, L_116, /*hidden argument*/NULL);
		if (!L_117)
		{
			goto IL_02ff;
		}
	}
	{
		int32_t L_118 = V_3;
		V_3 = ((int32_t)((int32_t)L_118+(int32_t)2));
	}

IL_02ff:
	{
		String_t* L_119 = (__this->___scheme_2);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		String_t* L_120 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___UriSchemeFile_20;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_121 = String_op_Equality_m1_601(NULL /*static, unused*/, L_119, L_120, /*hidden argument*/NULL);
		if (!L_121)
		{
			goto IL_0383;
		}
	}
	{
		V_8 = 2;
		int32_t L_122 = V_3;
		V_9 = L_122;
		goto IL_033f;
	}

IL_031f:
	{
		String_t* L_123 = ___uriString;
		int32_t L_124 = V_9;
		NullCheck(L_123);
		uint16_t L_125 = String_get_Chars_m1_442(L_123, L_124, /*hidden argument*/NULL);
		if ((((int32_t)L_125) == ((int32_t)((int32_t)47))))
		{
			goto IL_0333;
		}
	}
	{
		goto IL_0348;
	}

IL_0333:
	{
		int32_t L_126 = V_8;
		V_8 = ((int32_t)((int32_t)L_126+(int32_t)1));
		int32_t L_127 = V_9;
		V_9 = ((int32_t)((int32_t)L_127+(int32_t)1));
	}

IL_033f:
	{
		int32_t L_128 = V_9;
		int32_t L_129 = V_4;
		if ((((int32_t)L_128) < ((int32_t)L_129)))
		{
			goto IL_031f;
		}
	}

IL_0348:
	{
		int32_t L_130 = V_8;
		if ((((int32_t)L_130) < ((int32_t)4)))
		{
			goto IL_0377;
		}
	}
	{
		V_6 = 0;
		goto IL_035c;
	}

IL_0358:
	{
		int32_t L_131 = V_3;
		V_3 = ((int32_t)((int32_t)L_131+(int32_t)1));
	}

IL_035c:
	{
		int32_t L_132 = V_3;
		int32_t L_133 = V_4;
		if ((((int32_t)L_132) >= ((int32_t)L_133)))
		{
			goto IL_0372;
		}
	}
	{
		String_t* L_134 = ___uriString;
		int32_t L_135 = V_3;
		NullCheck(L_134);
		uint16_t L_136 = String_get_Chars_m1_442(L_134, L_135, /*hidden argument*/NULL);
		if ((((int32_t)L_136) == ((int32_t)((int32_t)47))))
		{
			goto IL_0358;
		}
	}

IL_0372:
	{
		goto IL_0383;
	}

IL_0377:
	{
		int32_t L_137 = V_8;
		if ((((int32_t)L_137) < ((int32_t)3)))
		{
			goto IL_0383;
		}
	}
	{
		int32_t L_138 = V_3;
		V_3 = ((int32_t)((int32_t)L_138+(int32_t)1));
	}

IL_0383:
	{
		int32_t L_139 = V_4;
		int32_t L_140 = V_3;
		if ((((int32_t)((int32_t)((int32_t)L_139-(int32_t)L_140))) <= ((int32_t)1)))
		{
			goto IL_03a3;
		}
	}
	{
		String_t* L_141 = ___uriString;
		int32_t L_142 = V_3;
		NullCheck(L_141);
		uint16_t L_143 = String_get_Chars_m1_442(L_141, ((int32_t)((int32_t)L_142+(int32_t)1)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_143) == ((uint32_t)((int32_t)58)))))
		{
			goto IL_03a3;
		}
	}
	{
		V_6 = 0;
		V_7 = 1;
	}

IL_03a3:
	{
		goto IL_03d2;
	}

IL_03a8:
	{
		String_t* L_144 = (__this->___scheme_2);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		bool L_145 = Uri_IsPredefinedScheme_m3_1766(NULL /*static, unused*/, L_144, /*hidden argument*/NULL);
		if (L_145)
		{
			goto IL_03d2;
		}
	}
	{
		String_t* L_146 = ___uriString;
		int32_t L_147 = V_3;
		int32_t L_148 = V_4;
		int32_t L_149 = V_3;
		NullCheck(L_146);
		String_t* L_150 = String_Substring_m1_455(L_146, L_147, ((int32_t)((int32_t)L_148-(int32_t)L_149)), /*hidden argument*/NULL);
		__this->___path_5 = L_150;
		__this->___isOpaquePart_10 = 1;
		return (String_t*)NULL;
	}

IL_03d2:
	{
		bool L_151 = V_6;
		if (!L_151)
		{
			goto IL_03e0;
		}
	}
	{
		V_1 = (-1);
		goto IL_040a;
	}

IL_03e0:
	{
		String_t* L_152 = ___uriString;
		int32_t L_153 = V_3;
		int32_t L_154 = V_4;
		int32_t L_155 = V_3;
		NullCheck(L_152);
		int32_t L_156 = String_IndexOf_m1_502(L_152, ((int32_t)47), L_153, ((int32_t)((int32_t)L_154-(int32_t)L_155)), /*hidden argument*/NULL);
		V_1 = L_156;
		int32_t L_157 = V_1;
		if ((!(((uint32_t)L_157) == ((uint32_t)(-1)))))
		{
			goto IL_040a;
		}
	}
	{
		bool L_158 = V_7;
		if (!L_158)
		{
			goto IL_040a;
		}
	}
	{
		String_t* L_159 = ___uriString;
		int32_t L_160 = V_3;
		int32_t L_161 = V_4;
		int32_t L_162 = V_3;
		NullCheck(L_159);
		int32_t L_163 = String_IndexOf_m1_502(L_159, ((int32_t)92), L_160, ((int32_t)((int32_t)L_161-(int32_t)L_162)), /*hidden argument*/NULL);
		V_1 = L_163;
	}

IL_040a:
	{
		int32_t L_164 = V_1;
		if ((!(((uint32_t)L_164) == ((uint32_t)(-1)))))
		{
			goto IL_044b;
		}
	}
	{
		String_t* L_165 = (__this->___scheme_2);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		String_t* L_166 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___UriSchemeMailto_25;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_167 = String_op_Inequality_m1_602(NULL /*static, unused*/, L_165, L_166, /*hidden argument*/NULL);
		if (!L_167)
		{
			goto IL_0446;
		}
	}
	{
		String_t* L_168 = (__this->___scheme_2);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		String_t* L_169 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___UriSchemeNews_26;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_170 = String_op_Inequality_m1_602(NULL /*static, unused*/, L_168, L_169, /*hidden argument*/NULL);
		if (!L_170)
		{
			goto IL_0446;
		}
	}
	{
		__this->___path_5 = _stringLiteral740;
	}

IL_0446:
	{
		goto IL_045f;
	}

IL_044b:
	{
		String_t* L_171 = ___uriString;
		int32_t L_172 = V_1;
		int32_t L_173 = V_4;
		int32_t L_174 = V_1;
		NullCheck(L_171);
		String_t* L_175 = String_Substring_m1_455(L_171, L_172, ((int32_t)((int32_t)L_173-(int32_t)L_174)), /*hidden argument*/NULL);
		__this->___path_5 = L_175;
		int32_t L_176 = V_1;
		V_4 = L_176;
	}

IL_045f:
	{
		bool L_177 = V_6;
		if (!L_177)
		{
			goto IL_046d;
		}
	}
	{
		V_1 = (-1);
		goto IL_047b;
	}

IL_046d:
	{
		String_t* L_178 = ___uriString;
		int32_t L_179 = V_3;
		int32_t L_180 = V_4;
		int32_t L_181 = V_3;
		NullCheck(L_178);
		int32_t L_182 = String_IndexOf_m1_502(L_178, ((int32_t)64), L_179, ((int32_t)((int32_t)L_180-(int32_t)L_181)), /*hidden argument*/NULL);
		V_1 = L_182;
	}

IL_047b:
	{
		int32_t L_183 = V_1;
		if ((((int32_t)L_183) == ((int32_t)(-1))))
		{
			goto IL_0496;
		}
	}
	{
		String_t* L_184 = ___uriString;
		int32_t L_185 = V_3;
		int32_t L_186 = V_1;
		int32_t L_187 = V_3;
		NullCheck(L_184);
		String_t* L_188 = String_Substring_m1_455(L_184, L_185, ((int32_t)((int32_t)L_186-(int32_t)L_187)), /*hidden argument*/NULL);
		__this->___userinfo_8 = L_188;
		int32_t L_189 = V_1;
		V_3 = ((int32_t)((int32_t)L_189+(int32_t)1));
	}

IL_0496:
	{
		__this->___port_4 = (-1);
		bool L_190 = V_6;
		if (!L_190)
		{
			goto IL_04ab;
		}
	}
	{
		V_1 = (-1);
		goto IL_04bc;
	}

IL_04ab:
	{
		String_t* L_191 = ___uriString;
		int32_t L_192 = V_4;
		int32_t L_193 = V_4;
		int32_t L_194 = V_3;
		NullCheck(L_191);
		int32_t L_195 = String_LastIndexOf_m1_514(L_191, ((int32_t)58), ((int32_t)((int32_t)L_192-(int32_t)1)), ((int32_t)((int32_t)L_193-(int32_t)L_194)), /*hidden argument*/NULL);
		V_1 = L_195;
	}

IL_04bc:
	{
		int32_t L_196 = V_1;
		if ((((int32_t)L_196) == ((int32_t)(-1))))
		{
			goto IL_0566;
		}
	}
	{
		int32_t L_197 = V_1;
		int32_t L_198 = V_4;
		if ((((int32_t)L_197) == ((int32_t)((int32_t)((int32_t)L_198-(int32_t)1)))))
		{
			goto IL_0566;
		}
	}
	{
		String_t* L_199 = ___uriString;
		int32_t L_200 = V_1;
		int32_t L_201 = V_4;
		int32_t L_202 = V_1;
		NullCheck(L_199);
		String_t* L_203 = String_Substring_m1_455(L_199, ((int32_t)((int32_t)L_200+(int32_t)1)), ((int32_t)((int32_t)L_201-(int32_t)((int32_t)((int32_t)L_202+(int32_t)1)))), /*hidden argument*/NULL);
		V_10 = L_203;
		String_t* L_204 = V_10;
		NullCheck(L_204);
		int32_t L_205 = String_get_Length_m1_571(L_204, /*hidden argument*/NULL);
		if ((((int32_t)L_205) <= ((int32_t)0)))
		{
			goto IL_0544;
		}
	}
	{
		String_t* L_206 = V_10;
		String_t* L_207 = V_10;
		NullCheck(L_207);
		int32_t L_208 = String_get_Length_m1_571(L_207, /*hidden argument*/NULL);
		NullCheck(L_206);
		uint16_t L_209 = String_get_Chars_m1_442(L_206, ((int32_t)((int32_t)L_208-(int32_t)1)), /*hidden argument*/NULL);
		if ((((int32_t)L_209) == ((int32_t)((int32_t)93))))
		{
			goto IL_0544;
		}
	}
	{
		String_t* L_210 = V_10;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t1_277_il2cpp_TypeInfo_var);
		CultureInfo_t1_277 * L_211 = CultureInfo_get_InvariantCulture_m1_3911(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t* L_212 = &(__this->___port_4);
		bool L_213 = Int32_TryParse_m1_100(NULL /*static, unused*/, L_210, 7, L_211, L_212, /*hidden argument*/NULL);
		if (!L_213)
		{
			goto IL_0536;
		}
	}
	{
		int32_t L_214 = (__this->___port_4);
		if ((((int32_t)L_214) < ((int32_t)0)))
		{
			goto IL_0536;
		}
	}
	{
		int32_t L_215 = (__this->___port_4);
		if ((((int32_t)L_215) <= ((int32_t)((int32_t)65535))))
		{
			goto IL_053c;
		}
	}

IL_0536:
	{
		return _stringLiteral4674;
	}

IL_053c:
	{
		int32_t L_216 = V_1;
		V_4 = L_216;
		goto IL_0561;
	}

IL_0544:
	{
		int32_t L_217 = (__this->___port_4);
		if ((!(((uint32_t)L_217) == ((uint32_t)(-1)))))
		{
			goto IL_0561;
		}
	}
	{
		String_t* L_218 = (__this->___scheme_2);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		int32_t L_219 = Uri_GetDefaultPort_m3_1764(NULL /*static, unused*/, L_218, /*hidden argument*/NULL);
		__this->___port_4 = L_219;
	}

IL_0561:
	{
		goto IL_0583;
	}

IL_0566:
	{
		int32_t L_220 = (__this->___port_4);
		if ((!(((uint32_t)L_220) == ((uint32_t)(-1)))))
		{
			goto IL_0583;
		}
	}
	{
		String_t* L_221 = (__this->___scheme_2);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		int32_t L_222 = Uri_GetDefaultPort_m3_1764(NULL /*static, unused*/, L_221, /*hidden argument*/NULL);
		__this->___port_4 = L_222;
	}

IL_0583:
	{
		String_t* L_223 = ___uriString;
		int32_t L_224 = V_3;
		int32_t L_225 = V_4;
		int32_t L_226 = V_3;
		NullCheck(L_223);
		String_t* L_227 = String_Substring_m1_455(L_223, L_224, ((int32_t)((int32_t)L_225-(int32_t)L_226)), /*hidden argument*/NULL);
		___uriString = L_227;
		String_t* L_228 = ___uriString;
		__this->___host_3 = L_228;
		bool L_229 = V_6;
		if (!L_229)
		{
			goto IL_05c7;
		}
	}
	{
		uint16_t L_230 = ((int32_t)47);
		Object_t * L_231 = Box(Char_t1_15_il2cpp_TypeInfo_var, &L_230);
		String_t* L_232 = ___uriString;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_233 = String_Concat_m1_556(NULL /*static, unused*/, L_231, L_232, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		String_t* L_234 = Uri_Reduce_m3_1761(NULL /*static, unused*/, L_233, 1, /*hidden argument*/NULL);
		__this->___path_5 = L_234;
		String_t* L_235 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___host_3 = L_235;
		goto IL_071c;
	}

IL_05c7:
	{
		String_t* L_236 = (__this->___host_3);
		NullCheck(L_236);
		int32_t L_237 = String_get_Length_m1_571(L_236, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_237) == ((uint32_t)2))))
		{
			goto IL_0612;
		}
	}
	{
		String_t* L_238 = (__this->___host_3);
		NullCheck(L_238);
		uint16_t L_239 = String_get_Chars_m1_442(L_238, 1, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_239) == ((uint32_t)((int32_t)58)))))
		{
			goto IL_0612;
		}
	}
	{
		String_t* L_240 = (__this->___host_3);
		String_t* L_241 = (__this->___path_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_242 = String_Concat_m1_559(NULL /*static, unused*/, L_240, L_241, /*hidden argument*/NULL);
		__this->___path_5 = L_242;
		String_t* L_243 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___host_3 = L_243;
		goto IL_071c;
	}

IL_0612:
	{
		bool L_244 = (__this->___isUnixFilePath_0);
		if (!L_244)
		{
			goto IL_063a;
		}
	}
	{
		String_t* L_245 = ___uriString;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_246 = String_Concat_m1_559(NULL /*static, unused*/, _stringLiteral751, L_245, /*hidden argument*/NULL);
		___uriString = L_246;
		String_t* L_247 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___host_3 = L_247;
		goto IL_071c;
	}

IL_063a:
	{
		String_t* L_248 = (__this->___scheme_2);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		String_t* L_249 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___UriSchemeFile_20;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_250 = String_op_Equality_m1_601(NULL /*static, unused*/, L_248, L_249, /*hidden argument*/NULL);
		if (!L_250)
		{
			goto IL_065b;
		}
	}
	{
		__this->___isUnc_9 = 1;
		goto IL_071c;
	}

IL_065b:
	{
		String_t* L_251 = (__this->___scheme_2);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		String_t* L_252 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___UriSchemeNews_26;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_253 = String_op_Equality_m1_601(NULL /*static, unused*/, L_251, L_252, /*hidden argument*/NULL);
		if (!L_253)
		{
			goto IL_069d;
		}
	}
	{
		String_t* L_254 = (__this->___host_3);
		NullCheck(L_254);
		int32_t L_255 = String_get_Length_m1_571(L_254, /*hidden argument*/NULL);
		if ((((int32_t)L_255) <= ((int32_t)0)))
		{
			goto IL_0698;
		}
	}
	{
		String_t* L_256 = (__this->___host_3);
		__this->___path_5 = L_256;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_257 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___host_3 = L_257;
	}

IL_0698:
	{
		goto IL_071c;
	}

IL_069d:
	{
		String_t* L_258 = (__this->___host_3);
		NullCheck(L_258);
		int32_t L_259 = String_get_Length_m1_571(L_258, /*hidden argument*/NULL);
		if (L_259)
		{
			goto IL_071c;
		}
	}
	{
		String_t* L_260 = (__this->___scheme_2);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		String_t* L_261 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___UriSchemeHttp_23;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_262 = String_op_Equality_m1_601(NULL /*static, unused*/, L_260, L_261, /*hidden argument*/NULL);
		if (L_262)
		{
			goto IL_0716;
		}
	}
	{
		String_t* L_263 = (__this->___scheme_2);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		String_t* L_264 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___UriSchemeGopher_22;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_265 = String_op_Equality_m1_601(NULL /*static, unused*/, L_263, L_264, /*hidden argument*/NULL);
		if (L_265)
		{
			goto IL_0716;
		}
	}
	{
		String_t* L_266 = (__this->___scheme_2);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		String_t* L_267 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___UriSchemeNntp_27;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_268 = String_op_Equality_m1_601(NULL /*static, unused*/, L_266, L_267, /*hidden argument*/NULL);
		if (L_268)
		{
			goto IL_0716;
		}
	}
	{
		String_t* L_269 = (__this->___scheme_2);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		String_t* L_270 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___UriSchemeHttps_24;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_271 = String_op_Equality_m1_601(NULL /*static, unused*/, L_269, L_270, /*hidden argument*/NULL);
		if (L_271)
		{
			goto IL_0716;
		}
	}
	{
		String_t* L_272 = (__this->___scheme_2);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		String_t* L_273 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___UriSchemeFtp_21;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_274 = String_op_Equality_m1_601(NULL /*static, unused*/, L_272, L_273, /*hidden argument*/NULL);
		if (!L_274)
		{
			goto IL_071c;
		}
	}

IL_0716:
	{
		return _stringLiteral760;
	}

IL_071c:
	{
		String_t* L_275 = (__this->___host_3);
		NullCheck(L_275);
		int32_t L_276 = String_get_Length_m1_571(L_275, /*hidden argument*/NULL);
		if ((((int32_t)L_276) <= ((int32_t)0)))
		{
			goto IL_073d;
		}
	}
	{
		String_t* L_277 = (__this->___host_3);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		int32_t L_278 = Uri_CheckHostName_m3_1734(NULL /*static, unused*/, L_277, /*hidden argument*/NULL);
		G_B139_0 = ((((int32_t)L_278) == ((int32_t)0))? 1 : 0);
		goto IL_073e;
	}

IL_073d:
	{
		G_B139_0 = 0;
	}

IL_073e:
	{
		V_11 = G_B139_0;
		bool L_279 = V_11;
		if (L_279)
		{
			goto IL_07c1;
		}
	}
	{
		String_t* L_280 = (__this->___host_3);
		NullCheck(L_280);
		int32_t L_281 = String_get_Length_m1_571(L_280, /*hidden argument*/NULL);
		if ((((int32_t)L_281) <= ((int32_t)1)))
		{
			goto IL_07c1;
		}
	}
	{
		String_t* L_282 = (__this->___host_3);
		NullCheck(L_282);
		uint16_t L_283 = String_get_Chars_m1_442(L_282, 0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_283) == ((uint32_t)((int32_t)91)))))
		{
			goto IL_07c1;
		}
	}
	{
		String_t* L_284 = (__this->___host_3);
		String_t* L_285 = (__this->___host_3);
		NullCheck(L_285);
		int32_t L_286 = String_get_Length_m1_571(L_285, /*hidden argument*/NULL);
		NullCheck(L_284);
		uint16_t L_287 = String_get_Chars_m1_442(L_284, ((int32_t)((int32_t)L_286-(int32_t)1)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_287) == ((uint32_t)((int32_t)93)))))
		{
			goto IL_07c1;
		}
	}
	{
		String_t* L_288 = (__this->___host_3);
		IL2CPP_RUNTIME_CLASS_INIT(IPv6Address_t3_122_il2cpp_TypeInfo_var);
		bool L_289 = IPv6Address_TryParse_m3_810(NULL /*static, unused*/, L_288, (&V_12), /*hidden argument*/NULL);
		if (!L_289)
		{
			goto IL_07be;
		}
	}
	{
		IPv6Address_t3_122 * L_290 = V_12;
		NullCheck(L_290);
		String_t* L_291 = IPv6Address_ToString_m3_820(L_290, 1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_292 = String_Concat_m1_560(NULL /*static, unused*/, _stringLiteral264, L_291, _stringLiteral266, /*hidden argument*/NULL);
		__this->___host_3 = L_292;
		goto IL_07c1;
	}

IL_07be:
	{
		V_11 = 1;
	}

IL_07c1:
	{
		bool L_293 = V_11;
		if (!L_293)
		{
			goto IL_07fe;
		}
	}
	{
		UriParser_t3_232 * L_294 = Uri_get_Parser_m3_1767(__this, /*hidden argument*/NULL);
		if (((DefaultUriParser_t3_231 *)IsInstClass(L_294, DefaultUriParser_t3_231_il2cpp_TypeInfo_var)))
		{
			goto IL_07e3;
		}
	}
	{
		UriParser_t3_232 * L_295 = Uri_get_Parser_m3_1767(__this, /*hidden argument*/NULL);
		if (L_295)
		{
			goto IL_07fe;
		}
	}

IL_07e3:
	{
		String_t* L_296 = (__this->___host_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_297 = String_Concat_m1_560(NULL /*static, unused*/, _stringLiteral4675, L_296, _stringLiteral106, /*hidden argument*/NULL);
		String_t* L_298 = Locale_GetText_m3_37(NULL /*static, unused*/, L_297, /*hidden argument*/NULL);
		return L_298;
	}

IL_07fe:
	{
		V_13 = (UriFormatException_t3_19 *)NULL;
		UriParser_t3_232 * L_299 = Uri_get_Parser_m3_1767(__this, /*hidden argument*/NULL);
		if (!L_299)
		{
			goto IL_081a;
		}
	}
	{
		UriParser_t3_232 * L_300 = Uri_get_Parser_m3_1767(__this, /*hidden argument*/NULL);
		NullCheck(L_300);
		VirtActionInvoker2< Uri_t3_3 *, UriFormatException_t3_19 ** >::Invoke(4 /* System.Void System.UriParser::InitializeAndValidate(System.Uri,System.UriFormatException&) */, L_300, __this, (&V_13));
	}

IL_081a:
	{
		UriFormatException_t3_19 * L_301 = V_13;
		if (!L_301)
		{
			goto IL_0829;
		}
	}
	{
		UriFormatException_t3_19 * L_302 = V_13;
		NullCheck(L_302);
		String_t* L_303 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(21 /* System.String System.Exception::get_Message() */, L_302);
		return L_303;
	}

IL_0829:
	{
		String_t* L_304 = (__this->___scheme_2);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		String_t* L_305 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___UriSchemeMailto_25;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_306 = String_op_Inequality_m1_602(NULL /*static, unused*/, L_304, L_305, /*hidden argument*/NULL);
		if (!L_306)
		{
			goto IL_0884;
		}
	}
	{
		String_t* L_307 = (__this->___scheme_2);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		String_t* L_308 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___UriSchemeNews_26;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_309 = String_op_Inequality_m1_602(NULL /*static, unused*/, L_307, L_308, /*hidden argument*/NULL);
		if (!L_309)
		{
			goto IL_0884;
		}
	}
	{
		String_t* L_310 = (__this->___scheme_2);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		String_t* L_311 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___UriSchemeFile_20;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_312 = String_op_Inequality_m1_602(NULL /*static, unused*/, L_310, L_311, /*hidden argument*/NULL);
		if (!L_312)
		{
			goto IL_0884;
		}
	}
	{
		String_t* L_313 = (__this->___path_5);
		String_t* L_314 = (__this->___scheme_2);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		bool L_315 = Uri_CompactEscaped_m3_1760(NULL /*static, unused*/, L_314, /*hidden argument*/NULL);
		String_t* L_316 = Uri_Reduce_m3_1761(NULL /*static, unused*/, L_313, L_315, /*hidden argument*/NULL);
		__this->___path_5 = L_316;
	}

IL_0884:
	{
		return (String_t*)NULL;
	}
}
// System.Boolean System.Uri::CompactEscaped(System.String)
extern TypeInfo* Uri_t3_3_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t1_92_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_14878_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral729;
extern Il2CppCodeGenString* _stringLiteral732;
extern Il2CppCodeGenString* _stringLiteral733;
extern Il2CppCodeGenString* _stringLiteral4666;
extern Il2CppCodeGenString* _stringLiteral4667;
extern "C" bool Uri_CompactEscaped_m3_1760 (Object_t * __this /* static, unused */, String_t* ___scheme, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Uri_t3_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		Dictionary_2_t1_92_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(129);
		Dictionary_2__ctor_m1_14878_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483658);
		_stringLiteral729 = il2cpp_codegen_string_literal_from_index(729);
		_stringLiteral732 = il2cpp_codegen_string_literal_from_index(732);
		_stringLiteral733 = il2cpp_codegen_string_literal_from_index(733);
		_stringLiteral4666 = il2cpp_codegen_string_literal_from_index(4666);
		_stringLiteral4667 = il2cpp_codegen_string_literal_from_index(4667);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	Dictionary_2_t1_92 * V_1 = {0};
	int32_t V_2 = 0;
	{
		String_t* L_0 = ___scheme;
		V_0 = L_0;
		String_t* L_1 = V_0;
		if (!L_1)
		{
			goto IL_007a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		Dictionary_2_t1_92 * L_2 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__switchU24map15_35;
		if (L_2)
		{
			goto IL_005b;
		}
	}
	{
		Dictionary_2_t1_92 * L_3 = (Dictionary_2_t1_92 *)il2cpp_codegen_object_new (Dictionary_2_t1_92_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_14878(L_3, 5, /*hidden argument*/Dictionary_2__ctor_m1_14878_MethodInfo_var);
		V_1 = L_3;
		Dictionary_2_t1_92 * L_4 = V_1;
		NullCheck(L_4);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_4, _stringLiteral729, 0);
		Dictionary_2_t1_92 * L_5 = V_1;
		NullCheck(L_5);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_5, _stringLiteral732, 0);
		Dictionary_2_t1_92 * L_6 = V_1;
		NullCheck(L_6);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_6, _stringLiteral733, 0);
		Dictionary_2_t1_92 * L_7 = V_1;
		NullCheck(L_7);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_7, _stringLiteral4666, 0);
		Dictionary_2_t1_92 * L_8 = V_1;
		NullCheck(L_8);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_8, _stringLiteral4667, 0);
		Dictionary_2_t1_92 * L_9 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__switchU24map15_35 = L_9;
	}

IL_005b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		Dictionary_2_t1_92 * L_10 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__switchU24map15_35;
		String_t* L_11 = V_0;
		NullCheck(L_10);
		bool L_12 = (bool)VirtFuncInvoker2< bool, String_t*, int32_t* >::Invoke(21 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int32>::TryGetValue(!0,!1&) */, L_10, L_11, (&V_2));
		if (!L_12)
		{
			goto IL_007a;
		}
	}
	{
		int32_t L_13 = V_2;
		if (!L_13)
		{
			goto IL_0078;
		}
	}
	{
		goto IL_007a;
	}

IL_0078:
	{
		return 1;
	}

IL_007a:
	{
		return 0;
	}
}
// System.String System.Uri::Reduce(System.String,System.Boolean)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* StringBuilder_t1_247_il2cpp_TypeInfo_var;
extern TypeInfo* Char_t1_15_il2cpp_TypeInfo_var;
extern TypeInfo* ArrayList_t1_170_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t1_193_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral740;
extern Il2CppCodeGenString* _stringLiteral56;
extern Il2CppCodeGenString* _stringLiteral321;
extern "C" String_t* Uri_Reduce_m3_1761 (Object_t * __this /* static, unused */, String_t* ___path, bool ___compact_escaped, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		StringBuilder_t1_247_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		Char_t1_15_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(12);
		ArrayList_t1_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(60);
		IEnumerator_t1_193_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(144);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		_stringLiteral740 = il2cpp_codegen_string_literal_from_index(740);
		_stringLiteral56 = il2cpp_codegen_string_literal_from_index(56);
		_stringLiteral321 = il2cpp_codegen_string_literal_from_index(321);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t1_247 * V_0 = {0};
	int32_t V_1 = 0;
	uint16_t V_2 = 0x0;
	uint16_t V_3 = 0x0;
	uint16_t V_4 = 0x0;
	ArrayList_t1_170 * V_5 = {0};
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	String_t* V_8 = {0};
	int32_t V_9 = 0;
	bool V_10 = false;
	String_t* V_11 = {0};
	Object_t * V_12 = {0};
	uint16_t V_13 = 0x0;
	Object_t * V_14 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___path;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_op_Equality_m1_601(NULL /*static, unused*/, L_0, _stringLiteral740, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		String_t* L_2 = ___path;
		return L_2;
	}

IL_0012:
	{
		StringBuilder_t1_247 * L_3 = (StringBuilder_t1_247 *)il2cpp_codegen_object_new (StringBuilder_t1_247_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1_12414(L_3, /*hidden argument*/NULL);
		V_0 = L_3;
		bool L_4 = ___compact_escaped;
		if (!L_4)
		{
			goto IL_00f5;
		}
	}
	{
		V_1 = 0;
		goto IL_00dc;
	}

IL_0025:
	{
		String_t* L_5 = ___path;
		int32_t L_6 = V_1;
		NullCheck(L_5);
		uint16_t L_7 = String_get_Chars_m1_442(L_5, L_6, /*hidden argument*/NULL);
		V_2 = L_7;
		uint16_t L_8 = V_2;
		V_13 = L_8;
		uint16_t L_9 = V_13;
		if ((((int32_t)L_9) == ((int32_t)((int32_t)37))))
		{
			goto IL_0055;
		}
	}
	{
		uint16_t L_10 = V_13;
		if ((((int32_t)L_10) == ((int32_t)((int32_t)92))))
		{
			goto IL_0047;
		}
	}
	{
		goto IL_00cb;
	}

IL_0047:
	{
		StringBuilder_t1_247 * L_11 = V_0;
		NullCheck(L_11);
		StringBuilder_Append_m1_12452(L_11, ((int32_t)47), /*hidden argument*/NULL);
		goto IL_00d8;
	}

IL_0055:
	{
		int32_t L_12 = V_1;
		String_t* L_13 = ___path;
		NullCheck(L_13);
		int32_t L_14 = String_get_Length_m1_571(L_13, /*hidden argument*/NULL);
		if ((((int32_t)L_12) >= ((int32_t)((int32_t)((int32_t)L_14-(int32_t)2)))))
		{
			goto IL_00be;
		}
	}
	{
		String_t* L_15 = ___path;
		int32_t L_16 = V_1;
		NullCheck(L_15);
		uint16_t L_17 = String_get_Chars_m1_442(L_15, ((int32_t)((int32_t)L_16+(int32_t)1)), /*hidden argument*/NULL);
		V_3 = L_17;
		String_t* L_18 = ___path;
		int32_t L_19 = V_1;
		NullCheck(L_18);
		uint16_t L_20 = String_get_Chars_m1_442(L_18, ((int32_t)((int32_t)L_19+(int32_t)2)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Char_t1_15_il2cpp_TypeInfo_var);
		uint16_t L_21 = Char_ToUpper_m1_406(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		V_4 = L_21;
		uint16_t L_22 = V_3;
		if ((!(((uint32_t)L_22) == ((uint32_t)((int32_t)50)))))
		{
			goto IL_008e;
		}
	}
	{
		uint16_t L_23 = V_4;
		if ((((int32_t)L_23) == ((int32_t)((int32_t)70))))
		{
			goto IL_009f;
		}
	}

IL_008e:
	{
		uint16_t L_24 = V_3;
		if ((!(((uint32_t)L_24) == ((uint32_t)((int32_t)53)))))
		{
			goto IL_00b1;
		}
	}
	{
		uint16_t L_25 = V_4;
		if ((!(((uint32_t)L_25) == ((uint32_t)((int32_t)67)))))
		{
			goto IL_00b1;
		}
	}

IL_009f:
	{
		StringBuilder_t1_247 * L_26 = V_0;
		NullCheck(L_26);
		StringBuilder_Append_m1_12452(L_26, ((int32_t)47), /*hidden argument*/NULL);
		int32_t L_27 = V_1;
		V_1 = ((int32_t)((int32_t)L_27+(int32_t)2));
		goto IL_00b9;
	}

IL_00b1:
	{
		StringBuilder_t1_247 * L_28 = V_0;
		uint16_t L_29 = V_2;
		NullCheck(L_28);
		StringBuilder_Append_m1_12452(L_28, L_29, /*hidden argument*/NULL);
	}

IL_00b9:
	{
		goto IL_00c6;
	}

IL_00be:
	{
		StringBuilder_t1_247 * L_30 = V_0;
		uint16_t L_31 = V_2;
		NullCheck(L_30);
		StringBuilder_Append_m1_12452(L_30, L_31, /*hidden argument*/NULL);
	}

IL_00c6:
	{
		goto IL_00d8;
	}

IL_00cb:
	{
		StringBuilder_t1_247 * L_32 = V_0;
		uint16_t L_33 = V_2;
		NullCheck(L_32);
		StringBuilder_Append_m1_12452(L_32, L_33, /*hidden argument*/NULL);
		goto IL_00d8;
	}

IL_00d8:
	{
		int32_t L_34 = V_1;
		V_1 = ((int32_t)((int32_t)L_34+(int32_t)1));
	}

IL_00dc:
	{
		int32_t L_35 = V_1;
		String_t* L_36 = ___path;
		NullCheck(L_36);
		int32_t L_37 = String_get_Length_m1_571(L_36, /*hidden argument*/NULL);
		if ((((int32_t)L_35) < ((int32_t)L_37)))
		{
			goto IL_0025;
		}
	}
	{
		StringBuilder_t1_247 * L_38 = V_0;
		NullCheck(L_38);
		String_t* L_39 = StringBuilder_ToString_m1_12428(L_38, /*hidden argument*/NULL);
		___path = L_39;
		goto IL_0101;
	}

IL_00f5:
	{
		String_t* L_40 = ___path;
		NullCheck(L_40);
		String_t* L_41 = String_Replace_m1_535(L_40, ((int32_t)92), ((int32_t)47), /*hidden argument*/NULL);
		___path = L_41;
	}

IL_0101:
	{
		ArrayList_t1_170 * L_42 = (ArrayList_t1_170 *)il2cpp_codegen_object_new (ArrayList_t1_170_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1_3049(L_42, /*hidden argument*/NULL);
		V_5 = L_42;
		V_6 = 0;
		goto IL_01a3;
	}

IL_0110:
	{
		String_t* L_43 = ___path;
		int32_t L_44 = V_6;
		NullCheck(L_43);
		int32_t L_45 = String_IndexOf_m1_501(L_43, ((int32_t)47), L_44, /*hidden argument*/NULL);
		V_7 = L_45;
		int32_t L_46 = V_7;
		if ((!(((uint32_t)L_46) == ((uint32_t)(-1)))))
		{
			goto IL_012c;
		}
	}
	{
		String_t* L_47 = ___path;
		NullCheck(L_47);
		int32_t L_48 = String_get_Length_m1_571(L_47, /*hidden argument*/NULL);
		V_7 = L_48;
	}

IL_012c:
	{
		String_t* L_49 = ___path;
		int32_t L_50 = V_6;
		int32_t L_51 = V_7;
		int32_t L_52 = V_6;
		NullCheck(L_49);
		String_t* L_53 = String_Substring_m1_455(L_49, L_50, ((int32_t)((int32_t)L_51-(int32_t)L_52)), /*hidden argument*/NULL);
		V_8 = L_53;
		int32_t L_54 = V_7;
		V_6 = ((int32_t)((int32_t)L_54+(int32_t)1));
		String_t* L_55 = V_8;
		NullCheck(L_55);
		int32_t L_56 = String_get_Length_m1_571(L_55, /*hidden argument*/NULL);
		if (!L_56)
		{
			goto IL_015e;
		}
	}
	{
		String_t* L_57 = V_8;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_58 = String_op_Equality_m1_601(NULL /*static, unused*/, L_57, _stringLiteral56, /*hidden argument*/NULL);
		if (!L_58)
		{
			goto IL_0163;
		}
	}

IL_015e:
	{
		goto IL_01a3;
	}

IL_0163:
	{
		String_t* L_59 = V_8;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_60 = String_op_Equality_m1_601(NULL /*static, unused*/, L_59, _stringLiteral321, /*hidden argument*/NULL);
		if (!L_60)
		{
			goto IL_0199;
		}
	}
	{
		ArrayList_t1_170 * L_61 = V_5;
		NullCheck(L_61);
		int32_t L_62 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_61);
		V_9 = L_62;
		int32_t L_63 = V_9;
		if (L_63)
		{
			goto IL_0189;
		}
	}
	{
		goto IL_01a3;
	}

IL_0189:
	{
		ArrayList_t1_170 * L_64 = V_5;
		int32_t L_65 = V_9;
		NullCheck(L_64);
		VirtActionInvoker1< int32_t >::Invoke(43 /* System.Void System.Collections.ArrayList::RemoveAt(System.Int32) */, L_64, ((int32_t)((int32_t)L_65-(int32_t)1)));
		goto IL_01a3;
	}

IL_0199:
	{
		ArrayList_t1_170 * L_66 = V_5;
		String_t* L_67 = V_8;
		NullCheck(L_66);
		VirtFuncInvoker1< int32_t, Object_t * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_66, L_67);
	}

IL_01a3:
	{
		int32_t L_68 = V_6;
		String_t* L_69 = ___path;
		NullCheck(L_69);
		int32_t L_70 = String_get_Length_m1_571(L_69, /*hidden argument*/NULL);
		if ((((int32_t)L_68) < ((int32_t)L_70)))
		{
			goto IL_0110;
		}
	}
	{
		ArrayList_t1_170 * L_71 = V_5;
		NullCheck(L_71);
		int32_t L_72 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_71);
		if (L_72)
		{
			goto IL_01c2;
		}
	}
	{
		return _stringLiteral740;
	}

IL_01c2:
	{
		StringBuilder_t1_247 * L_73 = V_0;
		NullCheck(L_73);
		StringBuilder_set_Length_m1_12425(L_73, 0, /*hidden argument*/NULL);
		String_t* L_74 = ___path;
		NullCheck(L_74);
		uint16_t L_75 = String_get_Chars_m1_442(L_74, 0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_75) == ((uint32_t)((int32_t)47)))))
		{
			goto IL_01e0;
		}
	}
	{
		StringBuilder_t1_247 * L_76 = V_0;
		NullCheck(L_76);
		StringBuilder_Append_m1_12452(L_76, ((int32_t)47), /*hidden argument*/NULL);
	}

IL_01e0:
	{
		V_10 = 1;
		ArrayList_t1_170 * L_77 = V_5;
		NullCheck(L_77);
		Object_t * L_78 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(50 /* System.Collections.IEnumerator System.Collections.ArrayList::GetEnumerator() */, L_77);
		V_12 = L_78;
	}

IL_01ec:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0220;
		}

IL_01f1:
		{
			Object_t * L_79 = V_12;
			NullCheck(L_79);
			Object_t * L_80 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_79);
			V_11 = ((String_t*)CastclassSealed(L_80, String_t_il2cpp_TypeInfo_var));
			bool L_81 = V_10;
			if (!L_81)
			{
				goto IL_020e;
			}
		}

IL_0206:
		{
			V_10 = 0;
			goto IL_0217;
		}

IL_020e:
		{
			StringBuilder_t1_247 * L_82 = V_0;
			NullCheck(L_82);
			StringBuilder_Append_m1_12452(L_82, ((int32_t)47), /*hidden argument*/NULL);
		}

IL_0217:
		{
			StringBuilder_t1_247 * L_83 = V_0;
			String_t* L_84 = V_11;
			NullCheck(L_83);
			StringBuilder_Append_m1_12438(L_83, L_84, /*hidden argument*/NULL);
		}

IL_0220:
		{
			Object_t * L_85 = V_12;
			NullCheck(L_85);
			bool L_86 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_85);
			if (L_86)
			{
				goto IL_01f1;
			}
		}

IL_022c:
		{
			IL2CPP_LEAVE(0x247, FINALLY_0231);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0231;
	}

FINALLY_0231:
	{ // begin finally (depth: 1)
		{
			Object_t * L_87 = V_12;
			V_14 = ((Object_t *)IsInst(L_87, IDisposable_t1_1035_il2cpp_TypeInfo_var));
			Object_t * L_88 = V_14;
			if (L_88)
			{
				goto IL_023f;
			}
		}

IL_023e:
		{
			IL2CPP_END_FINALLY(561)
		}

IL_023f:
		{
			Object_t * L_89 = V_14;
			NullCheck(L_89);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_89);
			IL2CPP_END_FINALLY(561)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(561)
	{
		IL2CPP_JUMP_TBL(0x247, IL_0247)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0247:
	{
		String_t* L_90 = ___path;
		NullCheck(L_90);
		bool L_91 = String_EndsWith_m1_482(L_90, _stringLiteral740, /*hidden argument*/NULL);
		if (!L_91)
		{
			goto IL_0260;
		}
	}
	{
		StringBuilder_t1_247 * L_92 = V_0;
		NullCheck(L_92);
		StringBuilder_Append_m1_12452(L_92, ((int32_t)47), /*hidden argument*/NULL);
	}

IL_0260:
	{
		StringBuilder_t1_247 * L_93 = V_0;
		NullCheck(L_93);
		String_t* L_94 = StringBuilder_ToString_m1_12428(L_93, /*hidden argument*/NULL);
		return L_94;
	}
}
// System.Char System.Uri::HexUnescapeMultiByte(System.String,System.Int32&,System.Char&)
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var;
extern TypeInfo* Uri_t3_3_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral744;
extern Il2CppCodeGenString* _stringLiteral170;
extern "C" uint16_t Uri_HexUnescapeMultiByte_m3_1762 (Object_t * __this /* static, unused */, String_t* ___pattern, int32_t* ___index, uint16_t* ___surrogate, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		Uri_t3_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		_stringLiteral744 = il2cpp_codegen_string_literal_from_index(744);
		_stringLiteral170 = il2cpp_codegen_string_literal_from_index(170);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	ByteU5BU5D_t1_109* V_5 = {0};
	bool V_6 = false;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	uint8_t V_10 = 0x0;
	int32_t V_11 = 0;
	int32_t V_12 = 0;
	int32_t V_13 = 0;
	{
		uint16_t* L_0 = ___surrogate;
		*((int16_t*)(L_0)) = (int16_t)0;
		String_t* L_1 = ___pattern;
		if (L_1)
		{
			goto IL_0014;
		}
	}
	{
		ArgumentException_t1_1425 * L_2 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13260(L_2, _stringLiteral744, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0014:
	{
		int32_t* L_3 = ___index;
		if ((((int32_t)(*((int32_t*)L_3))) < ((int32_t)0)))
		{
			goto IL_0029;
		}
	}
	{
		int32_t* L_4 = ___index;
		String_t* L_5 = ___pattern;
		NullCheck(L_5);
		int32_t L_6 = String_get_Length_m1_571(L_5, /*hidden argument*/NULL);
		if ((((int32_t)(*((int32_t*)L_4))) < ((int32_t)L_6)))
		{
			goto IL_0034;
		}
	}

IL_0029:
	{
		ArgumentOutOfRangeException_t1_1501 * L_7 = (ArgumentOutOfRangeException_t1_1501 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1_13274(L_7, _stringLiteral170, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_7);
	}

IL_0034:
	{
		String_t* L_8 = ___pattern;
		int32_t* L_9 = ___index;
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		bool L_10 = Uri_IsHexEncoding_m3_1746(NULL /*static, unused*/, L_8, (*((int32_t*)L_9)), /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_0053;
		}
	}
	{
		String_t* L_11 = ___pattern;
		int32_t* L_12 = ___index;
		int32_t* L_13 = ___index;
		int32_t L_14 = (*((int32_t*)L_13));
		V_13 = L_14;
		*((int32_t*)(L_12)) = (int32_t)((int32_t)((int32_t)L_14+(int32_t)1));
		int32_t L_15 = V_13;
		NullCheck(L_11);
		uint16_t L_16 = String_get_Chars_m1_442(L_11, L_15, /*hidden argument*/NULL);
		return L_16;
	}

IL_0053:
	{
		int32_t* L_17 = ___index;
		int32_t* L_18 = ___index;
		int32_t L_19 = (*((int32_t*)L_18));
		V_13 = L_19;
		*((int32_t*)(L_17)) = (int32_t)((int32_t)((int32_t)L_19+(int32_t)1));
		int32_t L_20 = V_13;
		V_0 = L_20;
		String_t* L_21 = ___pattern;
		int32_t* L_22 = ___index;
		int32_t* L_23 = ___index;
		int32_t L_24 = (*((int32_t*)L_23));
		V_13 = L_24;
		*((int32_t*)(L_22)) = (int32_t)((int32_t)((int32_t)L_24+(int32_t)1));
		int32_t L_25 = V_13;
		NullCheck(L_21);
		uint16_t L_26 = String_get_Chars_m1_442(L_21, L_25, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		int32_t L_27 = Uri_FromHex_m3_1743(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		V_1 = L_27;
		String_t* L_28 = ___pattern;
		int32_t* L_29 = ___index;
		int32_t* L_30 = ___index;
		int32_t L_31 = (*((int32_t*)L_30));
		V_13 = L_31;
		*((int32_t*)(L_29)) = (int32_t)((int32_t)((int32_t)L_31+(int32_t)1));
		int32_t L_32 = V_13;
		NullCheck(L_28);
		uint16_t L_33 = String_get_Chars_m1_442(L_28, L_32, /*hidden argument*/NULL);
		int32_t L_34 = Uri_FromHex_m3_1743(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
		V_2 = L_34;
		int32_t L_35 = V_1;
		V_3 = L_35;
		V_4 = 0;
		goto IL_00a1;
	}

IL_0097:
	{
		int32_t L_36 = V_4;
		V_4 = ((int32_t)((int32_t)L_36+(int32_t)1));
		int32_t L_37 = V_3;
		V_3 = ((int32_t)((int32_t)L_37<<(int32_t)1));
	}

IL_00a1:
	{
		int32_t L_38 = V_3;
		if ((((int32_t)((int32_t)((int32_t)L_38&(int32_t)8))) == ((int32_t)8)))
		{
			goto IL_0097;
		}
	}
	{
		int32_t L_39 = V_4;
		if ((((int32_t)L_39) > ((int32_t)1)))
		{
			goto IL_00b9;
		}
	}
	{
		int32_t L_40 = V_1;
		int32_t L_41 = V_2;
		return (((int32_t)((uint16_t)((int32_t)((int32_t)((int32_t)((int32_t)L_40<<(int32_t)4))|(int32_t)L_41)))));
	}

IL_00b9:
	{
		int32_t L_42 = V_4;
		V_5 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, L_42));
		V_6 = 0;
		ByteU5BU5D_t1_109* L_43 = V_5;
		int32_t L_44 = V_1;
		int32_t L_45 = V_2;
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_43, 0, sizeof(uint8_t))) = (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((int32_t)((int32_t)L_44<<(int32_t)4))|(int32_t)L_45)))));
		V_7 = 1;
		goto IL_014b;
	}

IL_00d7:
	{
		String_t* L_46 = ___pattern;
		int32_t* L_47 = ___index;
		int32_t* L_48 = ___index;
		int32_t L_49 = (*((int32_t*)L_48));
		V_13 = L_49;
		*((int32_t*)(L_47)) = (int32_t)((int32_t)((int32_t)L_49+(int32_t)1));
		int32_t L_50 = V_13;
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		bool L_51 = Uri_IsHexEncoding_m3_1746(NULL /*static, unused*/, L_46, L_50, /*hidden argument*/NULL);
		if (L_51)
		{
			goto IL_00f5;
		}
	}
	{
		V_6 = 1;
		goto IL_0154;
	}

IL_00f5:
	{
		String_t* L_52 = ___pattern;
		int32_t* L_53 = ___index;
		int32_t* L_54 = ___index;
		int32_t L_55 = (*((int32_t*)L_54));
		V_13 = L_55;
		*((int32_t*)(L_53)) = (int32_t)((int32_t)((int32_t)L_55+(int32_t)1));
		int32_t L_56 = V_13;
		NullCheck(L_52);
		uint16_t L_57 = String_get_Chars_m1_442(L_52, L_56, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		int32_t L_58 = Uri_FromHex_m3_1743(NULL /*static, unused*/, L_57, /*hidden argument*/NULL);
		V_8 = L_58;
		int32_t L_59 = V_8;
		if ((((int32_t)((int32_t)((int32_t)L_59&(int32_t)((int32_t)12)))) == ((int32_t)8)))
		{
			goto IL_0120;
		}
	}
	{
		V_6 = 1;
		goto IL_0154;
	}

IL_0120:
	{
		String_t* L_60 = ___pattern;
		int32_t* L_61 = ___index;
		int32_t* L_62 = ___index;
		int32_t L_63 = (*((int32_t*)L_62));
		V_13 = L_63;
		*((int32_t*)(L_61)) = (int32_t)((int32_t)((int32_t)L_63+(int32_t)1));
		int32_t L_64 = V_13;
		NullCheck(L_60);
		uint16_t L_65 = String_get_Chars_m1_442(L_60, L_64, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		int32_t L_66 = Uri_FromHex_m3_1743(NULL /*static, unused*/, L_65, /*hidden argument*/NULL);
		V_9 = L_66;
		ByteU5BU5D_t1_109* L_67 = V_5;
		int32_t L_68 = V_7;
		int32_t L_69 = V_8;
		int32_t L_70 = V_9;
		NullCheck(L_67);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_67, L_68);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_67, L_68, sizeof(uint8_t))) = (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((int32_t)((int32_t)L_69<<(int32_t)4))|(int32_t)L_70)))));
		int32_t L_71 = V_7;
		V_7 = ((int32_t)((int32_t)L_71+(int32_t)1));
	}

IL_014b:
	{
		int32_t L_72 = V_7;
		int32_t L_73 = V_4;
		if ((((int32_t)L_72) < ((int32_t)L_73)))
		{
			goto IL_00d7;
		}
	}

IL_0154:
	{
		bool L_74 = V_6;
		if (!L_74)
		{
			goto IL_0166;
		}
	}
	{
		int32_t* L_75 = ___index;
		int32_t L_76 = V_0;
		*((int32_t*)(L_75)) = (int32_t)((int32_t)((int32_t)L_76+(int32_t)3));
		ByteU5BU5D_t1_109* L_77 = V_5;
		NullCheck(L_77);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_77, 0);
		int32_t L_78 = 0;
		return (((int32_t)((uint16_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_77, L_78, sizeof(uint8_t))))));
	}

IL_0166:
	{
		V_10 = ((int32_t)255);
		uint8_t L_79 = V_10;
		int32_t L_80 = V_4;
		V_10 = (((int32_t)((uint8_t)((int32_t)((int32_t)L_79>>(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_80+(int32_t)1))&(int32_t)((int32_t)31))))))));
		ByteU5BU5D_t1_109* L_81 = V_5;
		NullCheck(L_81);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_81, 0);
		int32_t L_82 = 0;
		uint8_t L_83 = V_10;
		V_11 = ((int32_t)((int32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_81, L_82, sizeof(uint8_t)))&(int32_t)L_83));
		V_12 = 1;
		goto IL_01a4;
	}

IL_018b:
	{
		int32_t L_84 = V_11;
		V_11 = ((int32_t)((int32_t)L_84<<(int32_t)6));
		int32_t L_85 = V_11;
		ByteU5BU5D_t1_109* L_86 = V_5;
		int32_t L_87 = V_12;
		NullCheck(L_86);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_86, L_87);
		int32_t L_88 = L_87;
		V_11 = ((int32_t)((int32_t)L_85|(int32_t)((int32_t)((int32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_86, L_88, sizeof(uint8_t)))&(int32_t)((int32_t)63)))));
		int32_t L_89 = V_12;
		V_12 = ((int32_t)((int32_t)L_89+(int32_t)1));
	}

IL_01a4:
	{
		int32_t L_90 = V_12;
		int32_t L_91 = V_4;
		if ((((int32_t)L_90) < ((int32_t)L_91)))
		{
			goto IL_018b;
		}
	}
	{
		int32_t L_92 = V_11;
		if ((((int32_t)L_92) > ((int32_t)((int32_t)65535))))
		{
			goto IL_01bd;
		}
	}
	{
		int32_t L_93 = V_11;
		return (((int32_t)((uint16_t)L_93)));
	}

IL_01bd:
	{
		int32_t L_94 = V_11;
		V_11 = ((int32_t)((int32_t)L_94-(int32_t)((int32_t)65536)));
		uint16_t* L_95 = ___surrogate;
		int32_t L_96 = V_11;
		*((int16_t*)(L_95)) = (int16_t)(((int32_t)((uint16_t)((int32_t)((int32_t)((int32_t)((int32_t)L_96&(int32_t)((int32_t)1023)))|(int32_t)((int32_t)56320))))));
		int32_t L_97 = V_11;
		return (((int32_t)((uint16_t)((int32_t)((int32_t)((int32_t)((int32_t)L_97>>(int32_t)((int32_t)10)))|(int32_t)((int32_t)55296))))));
	}
}
// System.String System.Uri::GetSchemeDelimiter(System.String)
extern TypeInfo* Uri_t3_3_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" String_t* Uri_GetSchemeDelimiter_m3_1763 (Object_t * __this /* static, unused */, String_t* ___scheme, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Uri_t3_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0037;
	}

IL_0007:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		UriSchemeU5BU5D_t3_235* L_0 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___schemes_30;
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		String_t* L_2 = (((UriScheme_t3_234 *)(UriScheme_t3_234 *)SZArrayLdElema(L_0, L_1, sizeof(UriScheme_t3_234 )))->___scheme_0);
		String_t* L_3 = ___scheme;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_op_Equality_m1_601(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0033;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		UriSchemeU5BU5D_t3_235* L_5 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___schemes_30;
		int32_t L_6 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		String_t* L_7 = (((UriScheme_t3_234 *)(UriScheme_t3_234 *)SZArrayLdElema(L_5, L_6, sizeof(UriScheme_t3_234 )))->___delimiter_1);
		return L_7;
	}

IL_0033:
	{
		int32_t L_8 = V_0;
		V_0 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0037:
	{
		int32_t L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		UriSchemeU5BU5D_t3_235* L_10 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___schemes_30;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_10)->max_length)))))))
		{
			goto IL_0007;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		String_t* L_11 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___SchemeDelimiter_19;
		return L_11;
	}
}
// System.Int32 System.Uri::GetDefaultPort(System.String)
extern TypeInfo* UriParser_t3_232_il2cpp_TypeInfo_var;
extern "C" int32_t Uri_GetDefaultPort_m3_1764 (Object_t * __this /* static, unused */, String_t* ___scheme, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UriParser_t3_232_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1551);
		s_Il2CppMethodIntialized = true;
	}
	UriParser_t3_232 * V_0 = {0};
	{
		String_t* L_0 = ___scheme;
		IL2CPP_RUNTIME_CLASS_INIT(UriParser_t3_232_il2cpp_TypeInfo_var);
		UriParser_t3_232 * L_1 = UriParser_GetParser_m3_1786(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		UriParser_t3_232 * L_2 = V_0;
		if (L_2)
		{
			goto IL_000f;
		}
	}
	{
		return (-1);
	}

IL_000f:
	{
		UriParser_t3_232 * L_3 = V_0;
		NullCheck(L_3);
		int32_t L_4 = UriParser_get_DefaultPort_m3_1782(L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.String System.Uri::GetOpaqueWiseSchemeDelimiter()
extern TypeInfo* Uri_t3_3_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral737;
extern "C" String_t* Uri_GetOpaqueWiseSchemeDelimiter_m3_1765 (Uri_t3_3 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Uri_t3_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		_stringLiteral737 = il2cpp_codegen_string_literal_from_index(737);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (__this->___isOpaquePart_10);
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		return _stringLiteral737;
	}

IL_0011:
	{
		String_t* L_1 = (__this->___scheme_2);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		String_t* L_2 = Uri_GetSchemeDelimiter_m3_1763(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean System.Uri::IsPredefinedScheme(System.String)
extern TypeInfo* Uri_t3_3_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t1_92_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_14878_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral732;
extern Il2CppCodeGenString* _stringLiteral733;
extern Il2CppCodeGenString* _stringLiteral729;
extern Il2CppCodeGenString* _stringLiteral730;
extern Il2CppCodeGenString* _stringLiteral736;
extern Il2CppCodeGenString* _stringLiteral731;
extern Il2CppCodeGenString* _stringLiteral734;
extern Il2CppCodeGenString* _stringLiteral735;
extern Il2CppCodeGenString* _stringLiteral4666;
extern Il2CppCodeGenString* _stringLiteral4667;
extern "C" bool Uri_IsPredefinedScheme_m3_1766 (Object_t * __this /* static, unused */, String_t* ___scheme, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Uri_t3_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		Dictionary_2_t1_92_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(129);
		Dictionary_2__ctor_m1_14878_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483658);
		_stringLiteral732 = il2cpp_codegen_string_literal_from_index(732);
		_stringLiteral733 = il2cpp_codegen_string_literal_from_index(733);
		_stringLiteral729 = il2cpp_codegen_string_literal_from_index(729);
		_stringLiteral730 = il2cpp_codegen_string_literal_from_index(730);
		_stringLiteral736 = il2cpp_codegen_string_literal_from_index(736);
		_stringLiteral731 = il2cpp_codegen_string_literal_from_index(731);
		_stringLiteral734 = il2cpp_codegen_string_literal_from_index(734);
		_stringLiteral735 = il2cpp_codegen_string_literal_from_index(735);
		_stringLiteral4666 = il2cpp_codegen_string_literal_from_index(4666);
		_stringLiteral4667 = il2cpp_codegen_string_literal_from_index(4667);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	Dictionary_2_t1_92 * V_1 = {0};
	int32_t V_2 = 0;
	{
		String_t* L_0 = ___scheme;
		V_0 = L_0;
		String_t* L_1 = V_0;
		if (!L_1)
		{
			goto IL_00b7;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		Dictionary_2_t1_92 * L_2 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__switchU24map16_36;
		if (L_2)
		{
			goto IL_0098;
		}
	}
	{
		Dictionary_2_t1_92 * L_3 = (Dictionary_2_t1_92 *)il2cpp_codegen_object_new (Dictionary_2_t1_92_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_14878(L_3, ((int32_t)10), /*hidden argument*/Dictionary_2__ctor_m1_14878_MethodInfo_var);
		V_1 = L_3;
		Dictionary_2_t1_92 * L_4 = V_1;
		NullCheck(L_4);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_4, _stringLiteral732, 0);
		Dictionary_2_t1_92 * L_5 = V_1;
		NullCheck(L_5);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_5, _stringLiteral733, 0);
		Dictionary_2_t1_92 * L_6 = V_1;
		NullCheck(L_6);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_6, _stringLiteral729, 0);
		Dictionary_2_t1_92 * L_7 = V_1;
		NullCheck(L_7);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_7, _stringLiteral730, 0);
		Dictionary_2_t1_92 * L_8 = V_1;
		NullCheck(L_8);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_8, _stringLiteral736, 0);
		Dictionary_2_t1_92 * L_9 = V_1;
		NullCheck(L_9);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_9, _stringLiteral731, 0);
		Dictionary_2_t1_92 * L_10 = V_1;
		NullCheck(L_10);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_10, _stringLiteral734, 0);
		Dictionary_2_t1_92 * L_11 = V_1;
		NullCheck(L_11);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_11, _stringLiteral735, 0);
		Dictionary_2_t1_92 * L_12 = V_1;
		NullCheck(L_12);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_12, _stringLiteral4666, 0);
		Dictionary_2_t1_92 * L_13 = V_1;
		NullCheck(L_13);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_13, _stringLiteral4667, 0);
		Dictionary_2_t1_92 * L_14 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__switchU24map16_36 = L_14;
	}

IL_0098:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		Dictionary_2_t1_92 * L_15 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__switchU24map16_36;
		String_t* L_16 = V_0;
		NullCheck(L_15);
		bool L_17 = (bool)VirtFuncInvoker2< bool, String_t*, int32_t* >::Invoke(21 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int32>::TryGetValue(!0,!1&) */, L_15, L_16, (&V_2));
		if (!L_17)
		{
			goto IL_00b7;
		}
	}
	{
		int32_t L_18 = V_2;
		if (!L_18)
		{
			goto IL_00b5;
		}
	}
	{
		goto IL_00b7;
	}

IL_00b5:
	{
		return 1;
	}

IL_00b7:
	{
		return 0;
	}
}
// System.UriParser System.Uri::get_Parser()
extern TypeInfo* UriParser_t3_232_il2cpp_TypeInfo_var;
extern TypeInfo* DefaultUriParser_t3_231_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral715;
extern "C" UriParser_t3_232 * Uri_get_Parser_m3_1767 (Uri_t3_3 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UriParser_t3_232_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1551);
		DefaultUriParser_t3_231_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1555);
		_stringLiteral715 = il2cpp_codegen_string_literal_from_index(715);
		s_Il2CppMethodIntialized = true;
	}
	{
		UriParser_t3_232 * L_0 = (__this->___parser_31);
		if (L_0)
		{
			goto IL_0037;
		}
	}
	{
		String_t* L_1 = Uri_get_Scheme_m3_33(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UriParser_t3_232_il2cpp_TypeInfo_var);
		UriParser_t3_232 * L_2 = UriParser_GetParser_m3_1786(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		__this->___parser_31 = L_2;
		UriParser_t3_232 * L_3 = (__this->___parser_31);
		if (L_3)
		{
			goto IL_0037;
		}
	}
	{
		DefaultUriParser_t3_231 * L_4 = (DefaultUriParser_t3_231 *)il2cpp_codegen_object_new (DefaultUriParser_t3_231_il2cpp_TypeInfo_var);
		DefaultUriParser__ctor_m3_1716(L_4, _stringLiteral715, /*hidden argument*/NULL);
		__this->___parser_31 = L_4;
	}

IL_0037:
	{
		UriParser_t3_232 * L_5 = (__this->___parser_31);
		return L_5;
	}
}
// System.String System.Uri::UnescapeDataString(System.String)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* StringBuilder_t1_247_il2cpp_TypeInfo_var;
extern TypeInfo* MemoryStream_t1_433_il2cpp_TypeInfo_var;
extern TypeInfo* Encoding_t1_406_il2cpp_TypeInfo_var;
extern TypeInfo* Uri_t3_3_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4676;
extern "C" String_t* Uri_UnescapeDataString_m3_1768 (Object_t * __this /* static, unused */, String_t* ___stringToUnescape, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		StringBuilder_t1_247_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		MemoryStream_t1_433_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(390);
		Encoding_t1_406_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		Uri_t3_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		_stringLiteral4676 = il2cpp_codegen_string_literal_from_index(4676);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t1_247 * V_0 = {0};
	int64_t V_1 = 0;
	MemoryStream_t1_433 * V_2 = {0};
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		String_t* L_0 = ___stringToUnescape;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, _stringLiteral4676, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		String_t* L_2 = ___stringToUnescape;
		NullCheck(L_2);
		int32_t L_3 = String_IndexOf_m1_500(L_2, ((int32_t)37), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		String_t* L_4 = ___stringToUnescape;
		NullCheck(L_4);
		int32_t L_5 = String_IndexOf_m1_500(L_4, ((int32_t)43), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_5) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		String_t* L_6 = ___stringToUnescape;
		return L_6;
	}

IL_002f:
	{
		StringBuilder_t1_247 * L_7 = (StringBuilder_t1_247 *)il2cpp_codegen_object_new (StringBuilder_t1_247_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1_12414(L_7, /*hidden argument*/NULL);
		V_0 = L_7;
		String_t* L_8 = ___stringToUnescape;
		NullCheck(L_8);
		int32_t L_9 = String_get_Length_m1_571(L_8, /*hidden argument*/NULL);
		V_1 = (((int64_t)((int64_t)L_9)));
		MemoryStream_t1_433 * L_10 = (MemoryStream_t1_433 *)il2cpp_codegen_object_new (MemoryStream_t1_433_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m1_5044(L_10, /*hidden argument*/NULL);
		V_2 = L_10;
		V_4 = 0;
		goto IL_015e;
	}

IL_004b:
	{
		String_t* L_11 = ___stringToUnescape;
		int32_t L_12 = V_4;
		NullCheck(L_11);
		uint16_t L_13 = String_get_Chars_m1_442(L_11, L_12, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_13) == ((uint32_t)((int32_t)37)))))
		{
			goto IL_0122;
		}
	}
	{
		int32_t L_14 = V_4;
		int64_t L_15 = V_1;
		if ((((int64_t)(((int64_t)((int64_t)((int32_t)((int32_t)L_14+(int32_t)2)))))) >= ((int64_t)L_15)))
		{
			goto IL_0122;
		}
	}
	{
		String_t* L_16 = ___stringToUnescape;
		int32_t L_17 = V_4;
		NullCheck(L_16);
		uint16_t L_18 = String_get_Chars_m1_442(L_16, ((int32_t)((int32_t)L_17+(int32_t)1)), /*hidden argument*/NULL);
		if ((((int32_t)L_18) == ((int32_t)((int32_t)37))))
		{
			goto IL_0122;
		}
	}
	{
		String_t* L_19 = ___stringToUnescape;
		int32_t L_20 = V_4;
		NullCheck(L_19);
		uint16_t L_21 = String_get_Chars_m1_442(L_19, ((int32_t)((int32_t)L_20+(int32_t)1)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_21) == ((uint32_t)((int32_t)117)))))
		{
			goto IL_00ee;
		}
	}
	{
		int32_t L_22 = V_4;
		int64_t L_23 = V_1;
		if ((((int64_t)(((int64_t)((int64_t)((int32_t)((int32_t)L_22+(int32_t)5)))))) >= ((int64_t)L_23)))
		{
			goto IL_00ee;
		}
	}
	{
		MemoryStream_t1_433 * L_24 = V_2;
		NullCheck(L_24);
		int64_t L_25 = (int64_t)VirtFuncInvoker0< int64_t >::Invoke(9 /* System.Int64 System.IO.MemoryStream::get_Length() */, L_24);
		if ((((int64_t)L_25) <= ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_00b9;
		}
	}
	{
		StringBuilder_t1_247 * L_26 = V_0;
		MemoryStream_t1_433 * L_27 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t1_406_il2cpp_TypeInfo_var);
		Encoding_t1_406 * L_28 = Encoding_get_UTF8_m1_12363(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		CharU5BU5D_t1_16* L_29 = Uri_GetChars_m3_1771(NULL /*static, unused*/, L_27, L_28, /*hidden argument*/NULL);
		NullCheck(L_26);
		StringBuilder_Append_m1_12437(L_26, L_29, /*hidden argument*/NULL);
		MemoryStream_t1_433 * L_30 = V_2;
		NullCheck(L_30);
		VirtActionInvoker1< int64_t >::Invoke(23 /* System.Void System.IO.MemoryStream::SetLength(System.Int64) */, L_30, (((int64_t)((int64_t)0))));
	}

IL_00b9:
	{
		String_t* L_31 = ___stringToUnescape;
		int32_t L_32 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		int32_t L_33 = Uri_GetChar_m3_1770(NULL /*static, unused*/, L_31, ((int32_t)((int32_t)L_32+(int32_t)2)), 4, /*hidden argument*/NULL);
		V_3 = L_33;
		int32_t L_34 = V_3;
		if ((((int32_t)L_34) == ((int32_t)(-1))))
		{
			goto IL_00e0;
		}
	}
	{
		StringBuilder_t1_247 * L_35 = V_0;
		int32_t L_36 = V_3;
		NullCheck(L_35);
		StringBuilder_Append_m1_12452(L_35, (((int32_t)((uint16_t)L_36))), /*hidden argument*/NULL);
		int32_t L_37 = V_4;
		V_4 = ((int32_t)((int32_t)L_37+(int32_t)5));
		goto IL_00e9;
	}

IL_00e0:
	{
		StringBuilder_t1_247 * L_38 = V_0;
		NullCheck(L_38);
		StringBuilder_Append_m1_12452(L_38, ((int32_t)37), /*hidden argument*/NULL);
	}

IL_00e9:
	{
		goto IL_011d;
	}

IL_00ee:
	{
		String_t* L_39 = ___stringToUnescape;
		int32_t L_40 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		int32_t L_41 = Uri_GetChar_m3_1770(NULL /*static, unused*/, L_39, ((int32_t)((int32_t)L_40+(int32_t)1)), 2, /*hidden argument*/NULL);
		int32_t L_42 = L_41;
		V_3 = L_42;
		if ((((int32_t)L_42) == ((int32_t)(-1))))
		{
			goto IL_0114;
		}
	}
	{
		MemoryStream_t1_433 * L_43 = V_2;
		int32_t L_44 = V_3;
		NullCheck(L_43);
		VirtActionInvoker1< uint8_t >::Invoke(25 /* System.Void System.IO.MemoryStream::WriteByte(System.Byte) */, L_43, (((int32_t)((uint8_t)L_44))));
		int32_t L_45 = V_4;
		V_4 = ((int32_t)((int32_t)L_45+(int32_t)2));
		goto IL_011d;
	}

IL_0114:
	{
		StringBuilder_t1_247 * L_46 = V_0;
		NullCheck(L_46);
		StringBuilder_Append_m1_12452(L_46, ((int32_t)37), /*hidden argument*/NULL);
	}

IL_011d:
	{
		goto IL_0158;
	}

IL_0122:
	{
		MemoryStream_t1_433 * L_47 = V_2;
		NullCheck(L_47);
		int64_t L_48 = (int64_t)VirtFuncInvoker0< int64_t >::Invoke(9 /* System.Int64 System.IO.MemoryStream::get_Length() */, L_47);
		if ((((int64_t)L_48) <= ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_0149;
		}
	}
	{
		StringBuilder_t1_247 * L_49 = V_0;
		MemoryStream_t1_433 * L_50 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t1_406_il2cpp_TypeInfo_var);
		Encoding_t1_406 * L_51 = Encoding_get_UTF8_m1_12363(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		CharU5BU5D_t1_16* L_52 = Uri_GetChars_m3_1771(NULL /*static, unused*/, L_50, L_51, /*hidden argument*/NULL);
		NullCheck(L_49);
		StringBuilder_Append_m1_12437(L_49, L_52, /*hidden argument*/NULL);
		MemoryStream_t1_433 * L_53 = V_2;
		NullCheck(L_53);
		VirtActionInvoker1< int64_t >::Invoke(23 /* System.Void System.IO.MemoryStream::SetLength(System.Int64) */, L_53, (((int64_t)((int64_t)0))));
	}

IL_0149:
	{
		StringBuilder_t1_247 * L_54 = V_0;
		String_t* L_55 = ___stringToUnescape;
		int32_t L_56 = V_4;
		NullCheck(L_55);
		uint16_t L_57 = String_get_Chars_m1_442(L_55, L_56, /*hidden argument*/NULL);
		NullCheck(L_54);
		StringBuilder_Append_m1_12452(L_54, L_57, /*hidden argument*/NULL);
	}

IL_0158:
	{
		int32_t L_58 = V_4;
		V_4 = ((int32_t)((int32_t)L_58+(int32_t)1));
	}

IL_015e:
	{
		int32_t L_59 = V_4;
		int64_t L_60 = V_1;
		if ((((int64_t)(((int64_t)((int64_t)L_59)))) < ((int64_t)L_60)))
		{
			goto IL_004b;
		}
	}
	{
		MemoryStream_t1_433 * L_61 = V_2;
		NullCheck(L_61);
		int64_t L_62 = (int64_t)VirtFuncInvoker0< int64_t >::Invoke(9 /* System.Int64 System.IO.MemoryStream::get_Length() */, L_61);
		if ((((int64_t)L_62) <= ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_0186;
		}
	}
	{
		StringBuilder_t1_247 * L_63 = V_0;
		MemoryStream_t1_433 * L_64 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t1_406_il2cpp_TypeInfo_var);
		Encoding_t1_406 * L_65 = Encoding_get_UTF8_m1_12363(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		CharU5BU5D_t1_16* L_66 = Uri_GetChars_m3_1771(NULL /*static, unused*/, L_64, L_65, /*hidden argument*/NULL);
		NullCheck(L_63);
		StringBuilder_Append_m1_12437(L_63, L_66, /*hidden argument*/NULL);
	}

IL_0186:
	{
		V_2 = (MemoryStream_t1_433 *)NULL;
		StringBuilder_t1_247 * L_67 = V_0;
		NullCheck(L_67);
		String_t* L_68 = StringBuilder_ToString_m1_12428(L_67, /*hidden argument*/NULL);
		return L_68;
	}
}
// System.Int32 System.Uri::GetInt(System.Byte)
extern "C" int32_t Uri_GetInt_m3_1769 (Object_t * __this /* static, unused */, uint8_t ___b, const MethodInfo* method)
{
	uint16_t V_0 = 0x0;
	{
		uint8_t L_0 = ___b;
		V_0 = (((int32_t)((uint16_t)L_0)));
		uint16_t L_1 = V_0;
		if ((((int32_t)L_1) < ((int32_t)((int32_t)48))))
		{
			goto IL_0018;
		}
	}
	{
		uint16_t L_2 = V_0;
		if ((((int32_t)L_2) > ((int32_t)((int32_t)57))))
		{
			goto IL_0018;
		}
	}
	{
		uint16_t L_3 = V_0;
		return ((int32_t)((int32_t)L_3-(int32_t)((int32_t)48)));
	}

IL_0018:
	{
		uint16_t L_4 = V_0;
		if ((((int32_t)L_4) < ((int32_t)((int32_t)97))))
		{
			goto IL_0030;
		}
	}
	{
		uint16_t L_5 = V_0;
		if ((((int32_t)L_5) > ((int32_t)((int32_t)102))))
		{
			goto IL_0030;
		}
	}
	{
		uint16_t L_6 = V_0;
		return ((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)((int32_t)97)))+(int32_t)((int32_t)10)));
	}

IL_0030:
	{
		uint16_t L_7 = V_0;
		if ((((int32_t)L_7) < ((int32_t)((int32_t)65))))
		{
			goto IL_0048;
		}
	}
	{
		uint16_t L_8 = V_0;
		if ((((int32_t)L_8) > ((int32_t)((int32_t)70))))
		{
			goto IL_0048;
		}
	}
	{
		uint16_t L_9 = V_0;
		return ((int32_t)((int32_t)((int32_t)((int32_t)L_9-(int32_t)((int32_t)65)))+(int32_t)((int32_t)10)));
	}

IL_0048:
	{
		return (-1);
	}
}
// System.Int32 System.Uri::GetChar(System.String,System.Int32,System.Int32)
extern TypeInfo* Uri_t3_3_il2cpp_TypeInfo_var;
extern "C" int32_t Uri_GetChar_m3_1770 (Object_t * __this /* static, unused */, String_t* ___str, int32_t ___offset, int32_t ___length, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Uri_t3_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	uint16_t V_3 = 0x0;
	int32_t V_4 = 0;
	{
		V_0 = 0;
		int32_t L_0 = ___length;
		int32_t L_1 = ___offset;
		V_1 = ((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = ___offset;
		V_2 = L_2;
		goto IL_003d;
	}

IL_000d:
	{
		String_t* L_3 = ___str;
		int32_t L_4 = V_2;
		NullCheck(L_3);
		uint16_t L_5 = String_get_Chars_m1_442(L_3, L_4, /*hidden argument*/NULL);
		V_3 = L_5;
		uint16_t L_6 = V_3;
		if ((((int32_t)L_6) <= ((int32_t)((int32_t)127))))
		{
			goto IL_001f;
		}
	}
	{
		return (-1);
	}

IL_001f:
	{
		uint16_t L_7 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		int32_t L_8 = Uri_GetInt_m3_1769(NULL /*static, unused*/, (((int32_t)((uint8_t)L_7))), /*hidden argument*/NULL);
		V_4 = L_8;
		int32_t L_9 = V_4;
		if ((!(((uint32_t)L_9) == ((uint32_t)(-1)))))
		{
			goto IL_0032;
		}
	}
	{
		return (-1);
	}

IL_0032:
	{
		int32_t L_10 = V_0;
		int32_t L_11 = V_4;
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_10<<(int32_t)4))+(int32_t)L_11));
		int32_t L_12 = V_2;
		V_2 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_003d:
	{
		int32_t L_13 = V_2;
		int32_t L_14 = V_1;
		if ((((int32_t)L_13) < ((int32_t)L_14)))
		{
			goto IL_000d;
		}
	}
	{
		int32_t L_15 = V_0;
		return L_15;
	}
}
// System.Char[] System.Uri::GetChars(System.IO.MemoryStream,System.Text.Encoding)
extern "C" CharU5BU5D_t1_16* Uri_GetChars_m3_1771 (Object_t * __this /* static, unused */, MemoryStream_t1_433 * ___b, Encoding_t1_406 * ___e, const MethodInfo* method)
{
	{
		Encoding_t1_406 * L_0 = ___e;
		MemoryStream_t1_433 * L_1 = ___b;
		NullCheck(L_1);
		ByteU5BU5D_t1_109* L_2 = (ByteU5BU5D_t1_109*)VirtFuncInvoker0< ByteU5BU5D_t1_109* >::Invoke(32 /* System.Byte[] System.IO.MemoryStream::GetBuffer() */, L_1);
		MemoryStream_t1_433 * L_3 = ___b;
		NullCheck(L_3);
		int64_t L_4 = (int64_t)VirtFuncInvoker0< int64_t >::Invoke(9 /* System.Int64 System.IO.MemoryStream::get_Length() */, L_3);
		NullCheck(L_0);
		CharU5BU5D_t1_16* L_5 = (CharU5BU5D_t1_16*)VirtFuncInvoker3< CharU5BU5D_t1_16*, ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(17 /* System.Char[] System.Text.Encoding::GetChars(System.Byte[],System.Int32,System.Int32) */, L_0, L_2, 0, (((int32_t)((int32_t)L_4))));
		return L_5;
	}
}
// System.Void System.Uri::EnsureAbsoluteUri()
extern TypeInfo* InvalidOperationException_t1_1559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4677;
extern "C" void Uri_EnsureAbsoluteUri_m3_1772 (Uri_t3_3 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvalidOperationException_t1_1559_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(78);
		_stringLiteral4677 = il2cpp_codegen_string_literal_from_index(4677);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Uri_get_IsAbsoluteUri_m3_32(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t1_1559 * L_1 = (InvalidOperationException_t1_1559 *)il2cpp_codegen_object_new (InvalidOperationException_t1_1559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1_14171(L_1, _stringLiteral4677, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		return;
	}
}
// System.Boolean System.Uri::op_Equality(System.Uri,System.Uri)
extern "C" bool Uri_op_Equality_m3_26 (Object_t * __this /* static, unused */, Uri_t3_3 * ___u1, Uri_t3_3 * ___u2, const MethodInfo* method)
{
	{
		Uri_t3_3 * L_0 = ___u1;
		Uri_t3_3 * L_1 = ___u2;
		bool L_2 = Object_Equals_m1_2(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean System.Uri::op_Inequality(System.Uri,System.Uri)
extern TypeInfo* Uri_t3_3_il2cpp_TypeInfo_var;
extern "C" bool Uri_op_Inequality_m3_24 (Object_t * __this /* static, unused */, Uri_t3_3 * ___u1, Uri_t3_3 * ___u2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Uri_t3_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		s_Il2CppMethodIntialized = true;
	}
	{
		Uri_t3_3 * L_0 = ___u1;
		Uri_t3_3 * L_1 = ___u2;
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		bool L_2 = Uri_op_Equality_m3_26(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return ((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.UriFormatException::.ctor()
extern Il2CppCodeGenString* _stringLiteral4678;
extern "C" void UriFormatException__ctor_m3_1773 (UriFormatException_t3_19 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral4678 = il2cpp_codegen_string_literal_from_index(4678);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = Locale_GetText_m3_37(NULL /*static, unused*/, _stringLiteral4678, /*hidden argument*/NULL);
		FormatException__ctor_m1_14100(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.UriFormatException::.ctor(System.String)
extern "C" void UriFormatException__ctor_m3_1774 (UriFormatException_t3_19 * __this, String_t* ___message, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message;
		FormatException__ctor_m1_14100(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.UriFormatException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void UriFormatException__ctor_m3_1775 (UriFormatException_t3_19 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method)
{
	{
		SerializationInfo_t1_293 * L_0 = ___info;
		StreamingContext_t1_1050  L_1 = ___context;
		FormatException__ctor_m1_14102(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.UriFormatException::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void UriFormatException_System_Runtime_Serialization_ISerializable_GetObjectData_m3_1776 (UriFormatException_t3_19 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method)
{
	{
		SerializationInfo_t1_293 * L_0 = ___info;
		StreamingContext_t1_1050  L_1 = ___context;
		Exception_GetObjectData_m1_1256(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.UriParser::.ctor()
extern "C" void UriParser__ctor_m3_1777 (UriParser_t3_232 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.UriParser::.cctor()
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern TypeInfo* UriParser_t3_232_il2cpp_TypeInfo_var;
extern TypeInfo* Regex_t3_11_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4679;
extern Il2CppCodeGenString* _stringLiteral4680;
extern "C" void UriParser__cctor_m3_1778 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		UriParser_t3_232_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1551);
		Regex_t3_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1211);
		_stringLiteral4679 = il2cpp_codegen_string_literal_from_index(4679);
		_stringLiteral4680 = il2cpp_codegen_string_literal_from_index(4680);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = (Object_t *)il2cpp_codegen_object_new (Object_t_il2cpp_TypeInfo_var);
		Object__ctor_m1_0(L_0, /*hidden argument*/NULL);
		((UriParser_t3_232_StaticFields*)UriParser_t3_232_il2cpp_TypeInfo_var->static_fields)->___lock_object_0 = L_0;
		Regex_t3_11 * L_1 = (Regex_t3_11 *)il2cpp_codegen_object_new (Regex_t3_11_il2cpp_TypeInfo_var);
		Regex__ctor_m3_17(L_1, _stringLiteral4679, /*hidden argument*/NULL);
		((UriParser_t3_232_StaticFields*)UriParser_t3_232_il2cpp_TypeInfo_var->static_fields)->___uri_regex_4 = L_1;
		Regex_t3_11 * L_2 = (Regex_t3_11 *)il2cpp_codegen_object_new (Regex_t3_11_il2cpp_TypeInfo_var);
		Regex__ctor_m3_17(L_2, _stringLiteral4680, /*hidden argument*/NULL);
		((UriParser_t3_232_StaticFields*)UriParser_t3_232_il2cpp_TypeInfo_var->static_fields)->___auth_regex_5 = L_2;
		return;
	}
}
// System.Void System.UriParser::InitializeAndValidate(System.Uri,System.UriFormatException&)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* UriFormatException_t3_19_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral715;
extern Il2CppCodeGenString* _stringLiteral4681;
extern "C" void UriParser_InitializeAndValidate_m3_1779 (UriParser_t3_232 * __this, Uri_t3_3 * ___uri, UriFormatException_t3_19 ** ___parsingError, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		UriFormatException_t3_19_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1299);
		_stringLiteral715 = il2cpp_codegen_string_literal_from_index(715);
		_stringLiteral4681 = il2cpp_codegen_string_literal_from_index(4681);
		s_Il2CppMethodIntialized = true;
	}
	{
		Uri_t3_3 * L_0 = ___uri;
		NullCheck(L_0);
		String_t* L_1 = Uri_get_Scheme_m3_33(L_0, /*hidden argument*/NULL);
		String_t* L_2 = (__this->___scheme_name_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Inequality_m1_602(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003c;
		}
	}
	{
		String_t* L_4 = (__this->___scheme_name_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_op_Inequality_m1_602(NULL /*static, unused*/, L_4, _stringLiteral715, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_003c;
		}
	}
	{
		UriFormatException_t3_19 ** L_6 = ___parsingError;
		UriFormatException_t3_19 * L_7 = (UriFormatException_t3_19 *)il2cpp_codegen_object_new (UriFormatException_t3_19_il2cpp_TypeInfo_var);
		UriFormatException__ctor_m3_1774(L_7, _stringLiteral4681, /*hidden argument*/NULL);
		*((Object_t **)(L_6)) = (Object_t *)L_7;
		goto IL_003f;
	}

IL_003c:
	{
		UriFormatException_t3_19 ** L_8 = ___parsingError;
		*((Object_t **)(L_8)) = (Object_t *)NULL;
	}

IL_003f:
	{
		return;
	}
}
// System.Void System.UriParser::OnRegister(System.String,System.Int32)
extern "C" void UriParser_OnRegister_m3_1780 (UriParser_t3_232 * __this, String_t* ___schemeName, int32_t ___defaultPort, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void System.UriParser::set_SchemeName(System.String)
extern "C" void UriParser_set_SchemeName_m3_1781 (UriParser_t3_232 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___scheme_name_2 = L_0;
		return;
	}
}
// System.Int32 System.UriParser::get_DefaultPort()
extern "C" int32_t UriParser_get_DefaultPort_m3_1782 (UriParser_t3_232 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___default_port_3);
		return L_0;
	}
}
// System.Void System.UriParser::set_DefaultPort(System.Int32)
extern "C" void UriParser_set_DefaultPort_m3_1783 (UriParser_t3_232 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___default_port_3 = L_0;
		return;
	}
}
// System.Void System.UriParser::CreateDefaults()
extern TypeInfo* UriParser_t3_232_il2cpp_TypeInfo_var;
extern TypeInfo* Hashtable_t1_100_il2cpp_TypeInfo_var;
extern TypeInfo* DefaultUriParser_t3_231_il2cpp_TypeInfo_var;
extern TypeInfo* Uri_t3_3_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4682;
extern "C" void UriParser_CreateDefaults_m3_1784 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UriParser_t3_232_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1551);
		Hashtable_t1_100_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(119);
		DefaultUriParser_t3_231_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1555);
		Uri_t3_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		_stringLiteral4682 = il2cpp_codegen_string_literal_from_index(4682);
		s_Il2CppMethodIntialized = true;
	}
	Hashtable_t1_100 * V_0 = {0};
	Object_t * V_1 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(UriParser_t3_232_il2cpp_TypeInfo_var);
		Hashtable_t1_100 * L_0 = ((UriParser_t3_232_StaticFields*)UriParser_t3_232_il2cpp_TypeInfo_var->static_fields)->___table_1;
		if (!L_0)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		Hashtable_t1_100 * L_1 = (Hashtable_t1_100 *)il2cpp_codegen_object_new (Hashtable_t1_100_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1_3279(L_1, /*hidden argument*/NULL);
		V_0 = L_1;
		Hashtable_t1_100 * L_2 = V_0;
		DefaultUriParser_t3_231 * L_3 = (DefaultUriParser_t3_231 *)il2cpp_codegen_object_new (DefaultUriParser_t3_231_il2cpp_TypeInfo_var);
		DefaultUriParser__ctor_m3_1715(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		String_t* L_4 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___UriSchemeFile_20;
		IL2CPP_RUNTIME_CLASS_INIT(UriParser_t3_232_il2cpp_TypeInfo_var);
		UriParser_InternalRegister_m3_1785(NULL /*static, unused*/, L_2, L_3, L_4, (-1), /*hidden argument*/NULL);
		Hashtable_t1_100 * L_5 = V_0;
		DefaultUriParser_t3_231 * L_6 = (DefaultUriParser_t3_231 *)il2cpp_codegen_object_new (DefaultUriParser_t3_231_il2cpp_TypeInfo_var);
		DefaultUriParser__ctor_m3_1715(L_6, /*hidden argument*/NULL);
		String_t* L_7 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___UriSchemeFtp_21;
		UriParser_InternalRegister_m3_1785(NULL /*static, unused*/, L_5, L_6, L_7, ((int32_t)21), /*hidden argument*/NULL);
		Hashtable_t1_100 * L_8 = V_0;
		DefaultUriParser_t3_231 * L_9 = (DefaultUriParser_t3_231 *)il2cpp_codegen_object_new (DefaultUriParser_t3_231_il2cpp_TypeInfo_var);
		DefaultUriParser__ctor_m3_1715(L_9, /*hidden argument*/NULL);
		String_t* L_10 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___UriSchemeGopher_22;
		UriParser_InternalRegister_m3_1785(NULL /*static, unused*/, L_8, L_9, L_10, ((int32_t)70), /*hidden argument*/NULL);
		Hashtable_t1_100 * L_11 = V_0;
		DefaultUriParser_t3_231 * L_12 = (DefaultUriParser_t3_231 *)il2cpp_codegen_object_new (DefaultUriParser_t3_231_il2cpp_TypeInfo_var);
		DefaultUriParser__ctor_m3_1715(L_12, /*hidden argument*/NULL);
		String_t* L_13 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___UriSchemeHttp_23;
		UriParser_InternalRegister_m3_1785(NULL /*static, unused*/, L_11, L_12, L_13, ((int32_t)80), /*hidden argument*/NULL);
		Hashtable_t1_100 * L_14 = V_0;
		DefaultUriParser_t3_231 * L_15 = (DefaultUriParser_t3_231 *)il2cpp_codegen_object_new (DefaultUriParser_t3_231_il2cpp_TypeInfo_var);
		DefaultUriParser__ctor_m3_1715(L_15, /*hidden argument*/NULL);
		String_t* L_16 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___UriSchemeHttps_24;
		UriParser_InternalRegister_m3_1785(NULL /*static, unused*/, L_14, L_15, L_16, ((int32_t)443), /*hidden argument*/NULL);
		Hashtable_t1_100 * L_17 = V_0;
		DefaultUriParser_t3_231 * L_18 = (DefaultUriParser_t3_231 *)il2cpp_codegen_object_new (DefaultUriParser_t3_231_il2cpp_TypeInfo_var);
		DefaultUriParser__ctor_m3_1715(L_18, /*hidden argument*/NULL);
		String_t* L_19 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___UriSchemeMailto_25;
		UriParser_InternalRegister_m3_1785(NULL /*static, unused*/, L_17, L_18, L_19, ((int32_t)25), /*hidden argument*/NULL);
		Hashtable_t1_100 * L_20 = V_0;
		DefaultUriParser_t3_231 * L_21 = (DefaultUriParser_t3_231 *)il2cpp_codegen_object_new (DefaultUriParser_t3_231_il2cpp_TypeInfo_var);
		DefaultUriParser__ctor_m3_1715(L_21, /*hidden argument*/NULL);
		String_t* L_22 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___UriSchemeNetPipe_28;
		UriParser_InternalRegister_m3_1785(NULL /*static, unused*/, L_20, L_21, L_22, (-1), /*hidden argument*/NULL);
		Hashtable_t1_100 * L_23 = V_0;
		DefaultUriParser_t3_231 * L_24 = (DefaultUriParser_t3_231 *)il2cpp_codegen_object_new (DefaultUriParser_t3_231_il2cpp_TypeInfo_var);
		DefaultUriParser__ctor_m3_1715(L_24, /*hidden argument*/NULL);
		String_t* L_25 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___UriSchemeNetTcp_29;
		UriParser_InternalRegister_m3_1785(NULL /*static, unused*/, L_23, L_24, L_25, (-1), /*hidden argument*/NULL);
		Hashtable_t1_100 * L_26 = V_0;
		DefaultUriParser_t3_231 * L_27 = (DefaultUriParser_t3_231 *)il2cpp_codegen_object_new (DefaultUriParser_t3_231_il2cpp_TypeInfo_var);
		DefaultUriParser__ctor_m3_1715(L_27, /*hidden argument*/NULL);
		String_t* L_28 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___UriSchemeNews_26;
		UriParser_InternalRegister_m3_1785(NULL /*static, unused*/, L_26, L_27, L_28, ((int32_t)119), /*hidden argument*/NULL);
		Hashtable_t1_100 * L_29 = V_0;
		DefaultUriParser_t3_231 * L_30 = (DefaultUriParser_t3_231 *)il2cpp_codegen_object_new (DefaultUriParser_t3_231_il2cpp_TypeInfo_var);
		DefaultUriParser__ctor_m3_1715(L_30, /*hidden argument*/NULL);
		String_t* L_31 = ((Uri_t3_3_StaticFields*)Uri_t3_3_il2cpp_TypeInfo_var->static_fields)->___UriSchemeNntp_27;
		UriParser_InternalRegister_m3_1785(NULL /*static, unused*/, L_29, L_30, L_31, ((int32_t)119), /*hidden argument*/NULL);
		Hashtable_t1_100 * L_32 = V_0;
		DefaultUriParser_t3_231 * L_33 = (DefaultUriParser_t3_231 *)il2cpp_codegen_object_new (DefaultUriParser_t3_231_il2cpp_TypeInfo_var);
		DefaultUriParser__ctor_m3_1715(L_33, /*hidden argument*/NULL);
		UriParser_InternalRegister_m3_1785(NULL /*static, unused*/, L_32, L_33, _stringLiteral4682, ((int32_t)389), /*hidden argument*/NULL);
		Object_t * L_34 = ((UriParser_t3_232_StaticFields*)UriParser_t3_232_il2cpp_TypeInfo_var->static_fields)->___lock_object_0;
		V_1 = L_34;
		Object_t * L_35 = V_1;
		Monitor_Enter_m1_12697(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
	}

IL_00e6:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(UriParser_t3_232_il2cpp_TypeInfo_var);
			Hashtable_t1_100 * L_36 = ((UriParser_t3_232_StaticFields*)UriParser_t3_232_il2cpp_TypeInfo_var->static_fields)->___table_1;
			if (L_36)
			{
				goto IL_00fb;
			}
		}

IL_00f0:
		{
			Hashtable_t1_100 * L_37 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(UriParser_t3_232_il2cpp_TypeInfo_var);
			((UriParser_t3_232_StaticFields*)UriParser_t3_232_il2cpp_TypeInfo_var->static_fields)->___table_1 = L_37;
			goto IL_00fd;
		}

IL_00fb:
		{
			V_0 = (Hashtable_t1_100 *)NULL;
		}

IL_00fd:
		{
			IL2CPP_LEAVE(0x109, FINALLY_0102);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0102;
	}

FINALLY_0102:
	{ // begin finally (depth: 1)
		Object_t * L_38 = V_1;
		Monitor_Exit_m1_12698(NULL /*static, unused*/, L_38, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(258)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(258)
	{
		IL2CPP_JUMP_TBL(0x109, IL_0109)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0109:
	{
		return;
	}
}
// System.Void System.UriParser::InternalRegister(System.Collections.Hashtable,System.UriParser,System.String,System.Int32)
extern TypeInfo* GenericUriParser_t3_233_il2cpp_TypeInfo_var;
extern TypeInfo* DefaultUriParser_t3_231_il2cpp_TypeInfo_var;
extern "C" void UriParser_InternalRegister_m3_1785 (Object_t * __this /* static, unused */, Hashtable_t1_100 * ___table, UriParser_t3_232 * ___uriParser, String_t* ___schemeName, int32_t ___defaultPort, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GenericUriParser_t3_233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1556);
		DefaultUriParser_t3_231_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1555);
		s_Il2CppMethodIntialized = true;
	}
	DefaultUriParser_t3_231 * V_0 = {0};
	{
		UriParser_t3_232 * L_0 = ___uriParser;
		String_t* L_1 = ___schemeName;
		NullCheck(L_0);
		UriParser_set_SchemeName_m3_1781(L_0, L_1, /*hidden argument*/NULL);
		UriParser_t3_232 * L_2 = ___uriParser;
		int32_t L_3 = ___defaultPort;
		NullCheck(L_2);
		UriParser_set_DefaultPort_m3_1783(L_2, L_3, /*hidden argument*/NULL);
		UriParser_t3_232 * L_4 = ___uriParser;
		if (!((GenericUriParser_t3_233 *)IsInstClass(L_4, GenericUriParser_t3_233_il2cpp_TypeInfo_var)))
		{
			goto IL_0026;
		}
	}
	{
		Hashtable_t1_100 * L_5 = ___table;
		String_t* L_6 = ___schemeName;
		UriParser_t3_232 * L_7 = ___uriParser;
		NullCheck(L_5);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_5, L_6, L_7);
		goto IL_0042;
	}

IL_0026:
	{
		DefaultUriParser_t3_231 * L_8 = (DefaultUriParser_t3_231 *)il2cpp_codegen_object_new (DefaultUriParser_t3_231_il2cpp_TypeInfo_var);
		DefaultUriParser__ctor_m3_1715(L_8, /*hidden argument*/NULL);
		V_0 = L_8;
		DefaultUriParser_t3_231 * L_9 = V_0;
		String_t* L_10 = ___schemeName;
		NullCheck(L_9);
		UriParser_set_SchemeName_m3_1781(L_9, L_10, /*hidden argument*/NULL);
		DefaultUriParser_t3_231 * L_11 = V_0;
		int32_t L_12 = ___defaultPort;
		NullCheck(L_11);
		UriParser_set_DefaultPort_m3_1783(L_11, L_12, /*hidden argument*/NULL);
		Hashtable_t1_100 * L_13 = ___table;
		String_t* L_14 = ___schemeName;
		DefaultUriParser_t3_231 * L_15 = V_0;
		NullCheck(L_13);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_13, L_14, L_15);
	}

IL_0042:
	{
		UriParser_t3_232 * L_16 = ___uriParser;
		String_t* L_17 = ___schemeName;
		int32_t L_18 = ___defaultPort;
		NullCheck(L_16);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(5 /* System.Void System.UriParser::OnRegister(System.String,System.Int32) */, L_16, L_17, L_18);
		return;
	}
}
// System.UriParser System.UriParser::GetParser(System.String)
extern TypeInfo* UriParser_t3_232_il2cpp_TypeInfo_var;
extern TypeInfo* CultureInfo_t1_277_il2cpp_TypeInfo_var;
extern "C" UriParser_t3_232 * UriParser_GetParser_m3_1786 (Object_t * __this /* static, unused */, String_t* ___schemeName, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UriParser_t3_232_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1551);
		CultureInfo_t1_277_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(31);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	{
		String_t* L_0 = ___schemeName;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (UriParser_t3_232 *)NULL;
	}

IL_0008:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UriParser_t3_232_il2cpp_TypeInfo_var);
		UriParser_CreateDefaults_m3_1784(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = ___schemeName;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t1_277_il2cpp_TypeInfo_var);
		CultureInfo_t1_277 * L_2 = CultureInfo_get_InvariantCulture_m1_3911(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_3 = String_ToLower_m1_541(L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Hashtable_t1_100 * L_4 = ((UriParser_t3_232_StaticFields*)UriParser_t3_232_il2cpp_TypeInfo_var->static_fields)->___table_1;
		String_t* L_5 = V_0;
		NullCheck(L_4);
		Object_t * L_6 = (Object_t *)VirtFuncInvoker1< Object_t *, Object_t * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_4, L_5);
		return ((UriParser_t3_232 *)CastclassClass(L_6, UriParser_t3_232_il2cpp_TypeInfo_var));
	}
}
// System.Void System.Net.Security.LocalCertificateSelectionCallback::.ctor(System.Object,System.IntPtr)
extern "C" void LocalCertificateSelectionCallback__ctor_m3_1787 (LocalCertificateSelectionCallback_t3_50 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Security.Cryptography.X509Certificates.X509Certificate System.Net.Security.LocalCertificateSelectionCallback::Invoke(System.Object,System.String,System.Security.Cryptography.X509Certificates.X509CertificateCollection,System.Security.Cryptography.X509Certificates.X509Certificate,System.String[])
extern "C" X509Certificate_t1_1179 * LocalCertificateSelectionCallback_Invoke_m3_1788 (LocalCertificateSelectionCallback_t3_50 * __this, Object_t * ___sender, String_t* ___targetHost, X509CertificateCollection_t3_1 * ___localCertificates, X509Certificate_t1_1179 * ___remoteCertificate, StringU5BU5D_t1_238* ___acceptableIssuers, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		LocalCertificateSelectionCallback_Invoke_m3_1788((LocalCertificateSelectionCallback_t3_50 *)__this->___prev_9,___sender, ___targetHost, ___localCertificates, ___remoteCertificate, ___acceptableIssuers, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef X509Certificate_t1_1179 * (*FunctionPointerType) (Object_t *, Object_t * __this, Object_t * ___sender, String_t* ___targetHost, X509CertificateCollection_t3_1 * ___localCertificates, X509Certificate_t1_1179 * ___remoteCertificate, StringU5BU5D_t1_238* ___acceptableIssuers, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___sender, ___targetHost, ___localCertificates, ___remoteCertificate, ___acceptableIssuers,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef X509Certificate_t1_1179 * (*FunctionPointerType) (Object_t * __this, Object_t * ___sender, String_t* ___targetHost, X509CertificateCollection_t3_1 * ___localCertificates, X509Certificate_t1_1179 * ___remoteCertificate, StringU5BU5D_t1_238* ___acceptableIssuers, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___sender, ___targetHost, ___localCertificates, ___remoteCertificate, ___acceptableIssuers,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef X509Certificate_t1_1179 * (*FunctionPointerType) (Object_t * __this, String_t* ___targetHost, X509CertificateCollection_t3_1 * ___localCertificates, X509Certificate_t1_1179 * ___remoteCertificate, StringU5BU5D_t1_238* ___acceptableIssuers, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(___sender, ___targetHost, ___localCertificates, ___remoteCertificate, ___acceptableIssuers,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" X509Certificate_t1_1179 * pinvoke_delegate_wrapper_LocalCertificateSelectionCallback_t3_50(Il2CppObject* delegate, Object_t * ___sender, String_t* ___targetHost, X509CertificateCollection_t3_1 * ___localCertificates, X509Certificate_t1_1179 * ___remoteCertificate, StringU5BU5D_t1_238* ___acceptableIssuers)
{
	// Marshaling of parameter '___sender' to native representation
	Object_t * ____sender_marshaled = { 0 };
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Object'."));
}
// System.IAsyncResult System.Net.Security.LocalCertificateSelectionCallback::BeginInvoke(System.Object,System.String,System.Security.Cryptography.X509Certificates.X509CertificateCollection,System.Security.Cryptography.X509Certificates.X509Certificate,System.String[],System.AsyncCallback,System.Object)
extern "C" Object_t * LocalCertificateSelectionCallback_BeginInvoke_m3_1789 (LocalCertificateSelectionCallback_t3_50 * __this, Object_t * ___sender, String_t* ___targetHost, X509CertificateCollection_t3_1 * ___localCertificates, X509Certificate_t1_1179 * ___remoteCertificate, StringU5BU5D_t1_238* ___acceptableIssuers, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[6] = {0};
	__d_args[0] = ___sender;
	__d_args[1] = ___targetHost;
	__d_args[2] = ___localCertificates;
	__d_args[3] = ___remoteCertificate;
	__d_args[4] = ___acceptableIssuers;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Security.Cryptography.X509Certificates.X509Certificate System.Net.Security.LocalCertificateSelectionCallback::EndInvoke(System.IAsyncResult)
extern "C" X509Certificate_t1_1179 * LocalCertificateSelectionCallback_EndInvoke_m3_1790 (LocalCertificateSelectionCallback_t3_50 * __this, Object_t * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return (X509Certificate_t1_1179 *)__result;
}
// System.Void System.Net.Security.RemoteCertificateValidationCallback::.ctor(System.Object,System.IntPtr)
extern "C" void RemoteCertificateValidationCallback__ctor_m3_1791 (RemoteCertificateValidationCallback_t3_5 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Boolean System.Net.Security.RemoteCertificateValidationCallback::Invoke(System.Object,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain,System.Net.Security.SslPolicyErrors)
extern "C" bool RemoteCertificateValidationCallback_Invoke_m3_10 (RemoteCertificateValidationCallback_t3_5 * __this, Object_t * ___sender, X509Certificate_t1_1179 * ___certificate, X509Chain_t3_7 * ___chain, int32_t ___sslPolicyErrors, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		RemoteCertificateValidationCallback_Invoke_m3_10((RemoteCertificateValidationCallback_t3_5 *)__this->___prev_9,___sender, ___certificate, ___chain, ___sslPolicyErrors, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Object_t *, Object_t * __this, Object_t * ___sender, X509Certificate_t1_1179 * ___certificate, X509Chain_t3_7 * ___chain, int32_t ___sslPolicyErrors, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___sender, ___certificate, ___chain, ___sslPolicyErrors,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Object_t * __this, Object_t * ___sender, X509Certificate_t1_1179 * ___certificate, X509Chain_t3_7 * ___chain, int32_t ___sslPolicyErrors, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___sender, ___certificate, ___chain, ___sslPolicyErrors,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef bool (*FunctionPointerType) (Object_t * __this, X509Certificate_t1_1179 * ___certificate, X509Chain_t3_7 * ___chain, int32_t ___sslPolicyErrors, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(___sender, ___certificate, ___chain, ___sslPolicyErrors,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" bool pinvoke_delegate_wrapper_RemoteCertificateValidationCallback_t3_5(Il2CppObject* delegate, Object_t * ___sender, X509Certificate_t1_1179 * ___certificate, X509Chain_t3_7 * ___chain, int32_t ___sslPolicyErrors)
{
	// Marshaling of parameter '___sender' to native representation
	Object_t * ____sender_marshaled = { 0 };
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Object'."));
}
// System.IAsyncResult System.Net.Security.RemoteCertificateValidationCallback::BeginInvoke(System.Object,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain,System.Net.Security.SslPolicyErrors,System.AsyncCallback,System.Object)
extern TypeInfo* SslPolicyErrors_t3_47_il2cpp_TypeInfo_var;
extern "C" Object_t * RemoteCertificateValidationCallback_BeginInvoke_m3_1792 (RemoteCertificateValidationCallback_t3_5 * __this, Object_t * ___sender, X509Certificate_t1_1179 * ___certificate, X509Chain_t3_7 * ___chain, int32_t ___sslPolicyErrors, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SslPolicyErrors_t3_47_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1445);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[5] = {0};
	__d_args[0] = ___sender;
	__d_args[1] = ___certificate;
	__d_args[2] = ___chain;
	__d_args[3] = Box(SslPolicyErrors_t3_47_il2cpp_TypeInfo_var, &___sslPolicyErrors);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Boolean System.Net.Security.RemoteCertificateValidationCallback::EndInvoke(System.IAsyncResult)
extern "C" bool RemoteCertificateValidationCallback_EndInvoke_m3_1793 (RemoteCertificateValidationCallback_t3_5 * __this, Object_t * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Net.BindIPEndPoint::.ctor(System.Object,System.IntPtr)
extern "C" void BindIPEndPoint__ctor_m3_1794 (BindIPEndPoint_t3_125 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Net.IPEndPoint System.Net.BindIPEndPoint::Invoke(System.Net.ServicePoint,System.Net.IPEndPoint,System.Int32)
extern "C" IPEndPoint_t3_110 * BindIPEndPoint_Invoke_m3_1795 (BindIPEndPoint_t3_125 * __this, ServicePoint_t3_4 * ___servicePoint, IPEndPoint_t3_110 * ___remoteEndPoint, int32_t ___retryCount, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		BindIPEndPoint_Invoke_m3_1795((BindIPEndPoint_t3_125 *)__this->___prev_9,___servicePoint, ___remoteEndPoint, ___retryCount, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef IPEndPoint_t3_110 * (*FunctionPointerType) (Object_t *, Object_t * __this, ServicePoint_t3_4 * ___servicePoint, IPEndPoint_t3_110 * ___remoteEndPoint, int32_t ___retryCount, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___servicePoint, ___remoteEndPoint, ___retryCount,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef IPEndPoint_t3_110 * (*FunctionPointerType) (Object_t * __this, ServicePoint_t3_4 * ___servicePoint, IPEndPoint_t3_110 * ___remoteEndPoint, int32_t ___retryCount, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___servicePoint, ___remoteEndPoint, ___retryCount,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef IPEndPoint_t3_110 * (*FunctionPointerType) (Object_t * __this, IPEndPoint_t3_110 * ___remoteEndPoint, int32_t ___retryCount, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(___servicePoint, ___remoteEndPoint, ___retryCount,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" IPEndPoint_t3_110 * pinvoke_delegate_wrapper_BindIPEndPoint_t3_125(Il2CppObject* delegate, ServicePoint_t3_4 * ___servicePoint, IPEndPoint_t3_110 * ___remoteEndPoint, int32_t ___retryCount)
{
	// Marshaling of parameter '___servicePoint' to native representation
	ServicePoint_t3_4 * ____servicePoint_marshaled = { 0 };
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Net.ServicePoint'."));
}
// System.IAsyncResult System.Net.BindIPEndPoint::BeginInvoke(System.Net.ServicePoint,System.Net.IPEndPoint,System.Int32,System.AsyncCallback,System.Object)
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern "C" Object_t * BindIPEndPoint_BeginInvoke_m3_1796 (BindIPEndPoint_t3_125 * __this, ServicePoint_t3_4 * ___servicePoint, IPEndPoint_t3_110 * ___remoteEndPoint, int32_t ___retryCount, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = ___servicePoint;
	__d_args[1] = ___remoteEndPoint;
	__d_args[2] = Box(Int32_t1_3_il2cpp_TypeInfo_var, &___retryCount);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Net.IPEndPoint System.Net.BindIPEndPoint::EndInvoke(System.IAsyncResult)
extern "C" IPEndPoint_t3_110 * BindIPEndPoint_EndInvoke_m3_1797 (BindIPEndPoint_t3_125 * __this, Object_t * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return (IPEndPoint_t3_110 *)__result;
}
// System.Void System.Net.HttpContinueDelegate::.ctor(System.Object,System.IntPtr)
extern "C" void HttpContinueDelegate__ctor_m3_1798 (HttpContinueDelegate_t3_115 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void System.Net.HttpContinueDelegate::Invoke(System.Int32,System.Net.WebHeaderCollection)
extern "C" void HttpContinueDelegate_Invoke_m3_1799 (HttpContinueDelegate_t3_115 * __this, int32_t ___StatusCode, WebHeaderCollection_t3_80 * ___httpHeaders, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		HttpContinueDelegate_Invoke_m3_1799((HttpContinueDelegate_t3_115 *)__this->___prev_9,___StatusCode, ___httpHeaders, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, int32_t ___StatusCode, WebHeaderCollection_t3_80 * ___httpHeaders, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___StatusCode, ___httpHeaders,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, int32_t ___StatusCode, WebHeaderCollection_t3_80 * ___httpHeaders, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___StatusCode, ___httpHeaders,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_HttpContinueDelegate_t3_115(Il2CppObject* delegate, int32_t ___StatusCode, WebHeaderCollection_t3_80 * ___httpHeaders)
{
	// Marshaling of parameter '___httpHeaders' to native representation
	WebHeaderCollection_t3_80 * ____httpHeaders_marshaled = { 0 };
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Net.WebHeaderCollection'."));
}
// System.IAsyncResult System.Net.HttpContinueDelegate::BeginInvoke(System.Int32,System.Net.WebHeaderCollection,System.AsyncCallback,System.Object)
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern "C" Object_t * HttpContinueDelegate_BeginInvoke_m3_1800 (HttpContinueDelegate_t3_115 * __this, int32_t ___StatusCode, WebHeaderCollection_t3_80 * ___httpHeaders, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t1_3_il2cpp_TypeInfo_var, &___StatusCode);
	__d_args[1] = ___httpHeaders;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void System.Net.HttpContinueDelegate::EndInvoke(System.IAsyncResult)
extern "C" void HttpContinueDelegate_EndInvoke_m3_1801 (HttpContinueDelegate_t3_115 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void System.Text.RegularExpressions.MatchEvaluator::.ctor(System.Object,System.IntPtr)
extern "C" void MatchEvaluator__ctor_m3_1802 (MatchEvaluator_t3_240 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.String System.Text.RegularExpressions.MatchEvaluator::Invoke(System.Text.RegularExpressions.Match)
extern "C" String_t* MatchEvaluator_Invoke_m3_1803 (MatchEvaluator_t3_240 * __this, Match_t3_13 * ___match, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		MatchEvaluator_Invoke_m3_1803((MatchEvaluator_t3_240 *)__this->___prev_9,___match, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef String_t* (*FunctionPointerType) (Object_t *, Object_t * __this, Match_t3_13 * ___match, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___match,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef String_t* (*FunctionPointerType) (Object_t * __this, Match_t3_13 * ___match, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___match,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef String_t* (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(___match,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" String_t* pinvoke_delegate_wrapper_MatchEvaluator_t3_240(Il2CppObject* delegate, Match_t3_13 * ___match)
{
	// Marshaling of parameter '___match' to native representation
	Match_t3_13 * ____match_marshaled = { 0 };
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Text.RegularExpressions.Match'."));
}
// System.IAsyncResult System.Text.RegularExpressions.MatchEvaluator::BeginInvoke(System.Text.RegularExpressions.Match,System.AsyncCallback,System.Object)
extern "C" Object_t * MatchEvaluator_BeginInvoke_m3_1804 (MatchEvaluator_t3_240 * __this, Match_t3_13 * ___match, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___match;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.String System.Text.RegularExpressions.MatchEvaluator::EndInvoke(System.IAsyncResult)
extern "C" String_t* MatchEvaluator_EndInvoke_m3_1805 (MatchEvaluator_t3_240 * __this, Object_t * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return (String_t*)__result;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
