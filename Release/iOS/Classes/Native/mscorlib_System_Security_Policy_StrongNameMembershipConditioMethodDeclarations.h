﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Policy.StrongNameMembershipCondition
struct StrongNameMembershipCondition_t1_1364;
// System.Security.Permissions.StrongNamePublicKeyBlob
struct StrongNamePublicKeyBlob_t1_1314;
// System.String
struct String_t;
// System.Version
struct Version_t1_578;
// System.Security.SecurityElement
struct SecurityElement_t1_242;
// System.Security.Policy.Evidence
struct Evidence_t1_398;
// System.Security.Policy.IMembershipCondition
struct IMembershipCondition_t1_1340;
// System.Object
struct Object_t;
// System.Security.Policy.PolicyLevel
struct PolicyLevel_t1_1357;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Policy.StrongNameMembershipCondition::.ctor(System.Security.Permissions.StrongNamePublicKeyBlob,System.String,System.Version)
extern "C" void StrongNameMembershipCondition__ctor_m1_11695 (StrongNameMembershipCondition_t1_1364 * __this, StrongNamePublicKeyBlob_t1_1314 * ___blob, String_t* ___name, Version_t1_578 * ___version, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.StrongNameMembershipCondition::.ctor(System.Security.SecurityElement)
extern "C" void StrongNameMembershipCondition__ctor_m1_11696 (StrongNameMembershipCondition_t1_1364 * __this, SecurityElement_t1_242 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.StrongNameMembershipCondition::.ctor()
extern "C" void StrongNameMembershipCondition__ctor_m1_11697 (StrongNameMembershipCondition_t1_1364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Policy.StrongNameMembershipCondition::get_Name()
extern "C" String_t* StrongNameMembershipCondition_get_Name_m1_11698 (StrongNameMembershipCondition_t1_1364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.StrongNameMembershipCondition::set_Name(System.String)
extern "C" void StrongNameMembershipCondition_set_Name_m1_11699 (StrongNameMembershipCondition_t1_1364 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Version System.Security.Policy.StrongNameMembershipCondition::get_Version()
extern "C" Version_t1_578 * StrongNameMembershipCondition_get_Version_m1_11700 (StrongNameMembershipCondition_t1_1364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.StrongNameMembershipCondition::set_Version(System.Version)
extern "C" void StrongNameMembershipCondition_set_Version_m1_11701 (StrongNameMembershipCondition_t1_1364 * __this, Version_t1_578 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Permissions.StrongNamePublicKeyBlob System.Security.Policy.StrongNameMembershipCondition::get_PublicKey()
extern "C" StrongNamePublicKeyBlob_t1_1314 * StrongNameMembershipCondition_get_PublicKey_m1_11702 (StrongNameMembershipCondition_t1_1364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.StrongNameMembershipCondition::set_PublicKey(System.Security.Permissions.StrongNamePublicKeyBlob)
extern "C" void StrongNameMembershipCondition_set_PublicKey_m1_11703 (StrongNameMembershipCondition_t1_1364 * __this, StrongNamePublicKeyBlob_t1_1314 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Policy.StrongNameMembershipCondition::Check(System.Security.Policy.Evidence)
extern "C" bool StrongNameMembershipCondition_Check_m1_11704 (StrongNameMembershipCondition_t1_1364 * __this, Evidence_t1_398 * ___evidence, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.IMembershipCondition System.Security.Policy.StrongNameMembershipCondition::Copy()
extern "C" Object_t * StrongNameMembershipCondition_Copy_m1_11705 (StrongNameMembershipCondition_t1_1364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Policy.StrongNameMembershipCondition::Equals(System.Object)
extern "C" bool StrongNameMembershipCondition_Equals_m1_11706 (StrongNameMembershipCondition_t1_1364 * __this, Object_t * ___o, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Policy.StrongNameMembershipCondition::GetHashCode()
extern "C" int32_t StrongNameMembershipCondition_GetHashCode_m1_11707 (StrongNameMembershipCondition_t1_1364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.StrongNameMembershipCondition::FromXml(System.Security.SecurityElement)
extern "C" void StrongNameMembershipCondition_FromXml_m1_11708 (StrongNameMembershipCondition_t1_1364 * __this, SecurityElement_t1_242 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.StrongNameMembershipCondition::FromXml(System.Security.SecurityElement,System.Security.Policy.PolicyLevel)
extern "C" void StrongNameMembershipCondition_FromXml_m1_11709 (StrongNameMembershipCondition_t1_1364 * __this, SecurityElement_t1_242 * ___e, PolicyLevel_t1_1357 * ___level, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Policy.StrongNameMembershipCondition::ToString()
extern "C" String_t* StrongNameMembershipCondition_ToString_m1_11710 (StrongNameMembershipCondition_t1_1364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityElement System.Security.Policy.StrongNameMembershipCondition::ToXml()
extern "C" SecurityElement_t1_242 * StrongNameMembershipCondition_ToXml_m1_11711 (StrongNameMembershipCondition_t1_1364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityElement System.Security.Policy.StrongNameMembershipCondition::ToXml(System.Security.Policy.PolicyLevel)
extern "C" SecurityElement_t1_242 * StrongNameMembershipCondition_ToXml_m1_11712 (StrongNameMembershipCondition_t1_1364 * __this, PolicyLevel_t1_1357 * ___level, const MethodInfo* method) IL2CPP_METHOD_ATTR;
