﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Security_AccessControl_AuditRule.h"
#include "mscorlib_System_Security_AccessControl_FileSystemRights.h"

// System.Security.AccessControl.FileSystemAuditRule
struct  FileSystemAuditRule_t1_1155  : public AuditRule_t1_1119
{
	// System.Security.AccessControl.FileSystemRights System.Security.AccessControl.FileSystemAuditRule::rights
	int32_t ___rights_6;
};
