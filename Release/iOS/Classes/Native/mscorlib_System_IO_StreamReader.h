﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Char[]
struct CharU5BU5D_t1_16;
// System.Text.Encoding
struct Encoding_t1_406;
// System.Text.Decoder
struct Decoder_t1_407;
// System.IO.Stream
struct Stream_t1_405;
// System.Text.StringBuilder
struct StringBuilder_t1_247;
// System.IO.StreamReader
struct StreamReader_t1_447;

#include "mscorlib_System_IO_TextReader.h"

// System.IO.StreamReader
struct  StreamReader_t1_447  : public TextReader_t1_246
{
	// System.Byte[] System.IO.StreamReader::input_buffer
	ByteU5BU5D_t1_109* ___input_buffer_4;
	// System.Char[] System.IO.StreamReader::decoded_buffer
	CharU5BU5D_t1_16* ___decoded_buffer_5;
	// System.Int32 System.IO.StreamReader::decoded_count
	int32_t ___decoded_count_6;
	// System.Int32 System.IO.StreamReader::pos
	int32_t ___pos_7;
	// System.Int32 System.IO.StreamReader::buffer_size
	int32_t ___buffer_size_8;
	// System.Int32 System.IO.StreamReader::do_checks
	int32_t ___do_checks_9;
	// System.Text.Encoding System.IO.StreamReader::encoding
	Encoding_t1_406 * ___encoding_10;
	// System.Text.Decoder System.IO.StreamReader::decoder
	Decoder_t1_407 * ___decoder_11;
	// System.IO.Stream System.IO.StreamReader::base_stream
	Stream_t1_405 * ___base_stream_12;
	// System.Boolean System.IO.StreamReader::mayBlock
	bool ___mayBlock_13;
	// System.Text.StringBuilder System.IO.StreamReader::line_builder
	StringBuilder_t1_247 * ___line_builder_14;
	// System.Boolean System.IO.StreamReader::foundCR
	bool ___foundCR_16;
};
struct StreamReader_t1_447_StaticFields{
	// System.IO.StreamReader System.IO.StreamReader::Null
	StreamReader_t1_447 * ___Null_15;
};
