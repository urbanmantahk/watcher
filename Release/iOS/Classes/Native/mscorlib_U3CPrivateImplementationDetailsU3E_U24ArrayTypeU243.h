﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType.h"

// <PrivateImplementationDetails>/$ArrayType$36
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU2436_t1_1636 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU2436_t1_1636__padding[36];
	};
};
#pragma pack(pop, tp)
