﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t7_63;
// UnityEngine.UI.Slider
struct Slider_t7_125;
// UnityEngine.GameObject
struct GameObject_t6_97;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// TimerSetting
struct  TimerSetting_t8_13  : public MonoBehaviour_t6_91
{
	// UnityEngine.UI.Text TimerSetting::am_hour
	Text_t7_63 * ___am_hour_2;
	// UnityEngine.UI.Text TimerSetting::am_mins
	Text_t7_63 * ___am_mins_3;
	// UnityEngine.UI.Text TimerSetting::pm_hour
	Text_t7_63 * ___pm_hour_4;
	// UnityEngine.UI.Text TimerSetting::pm_mins
	Text_t7_63 * ___pm_mins_5;
	// UnityEngine.UI.Slider TimerSetting::am_hour_slider
	Slider_t7_125 * ___am_hour_slider_6;
	// UnityEngine.UI.Slider TimerSetting::am_mins_slider
	Slider_t7_125 * ___am_mins_slider_7;
	// UnityEngine.UI.Slider TimerSetting::pm_hour_slider
	Slider_t7_125 * ___pm_hour_slider_8;
	// UnityEngine.UI.Slider TimerSetting::pm_mins_slider
	Slider_t7_125 * ___pm_mins_slider_9;
	// UnityEngine.GameObject TimerSetting::dialog_error
	GameObject_t6_97 * ___dialog_error_10;
	// UnityEngine.UI.Text TimerSetting::error_msg
	Text_t7_63 * ___error_msg_11;
	// System.Boolean TimerSetting::updatedServer
	bool ___updatedServer_12;
	// System.String TimerSetting::url_dev
	String_t* ___url_dev_13;
	// System.Single TimerSetting::startHour
	float ___startHour_14;
	// System.Single TimerSetting::alarmInterval
	float ___alarmInterval_15;
};
