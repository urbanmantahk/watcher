﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.Text.Encoding
struct Encoding_t1_406;
// System.IO.StreamWriter
struct StreamWriter_t1_448;
// System.IO.FileStream
struct FileStream_t1_146;
// System.IO.StreamReader
struct StreamReader_t1_447;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.String[]
struct StringU5BU5D_t1_238;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_FileAttributes.h"
#include "mscorlib_System_DateTime.h"
#include "mscorlib_System_IO_FileMode.h"
#include "mscorlib_System_IO_FileAccess.h"
#include "mscorlib_System_IO_FileShare.h"

// System.Void System.IO.File::AppendAllText(System.String,System.String)
extern "C" void File_AppendAllText_m1_4848 (Object_t * __this /* static, unused */, String_t* ___path, String_t* ___contents, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.File::AppendAllText(System.String,System.String,System.Text.Encoding)
extern "C" void File_AppendAllText_m1_4849 (Object_t * __this /* static, unused */, String_t* ___path, String_t* ___contents, Encoding_t1_406 * ___encoding, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.StreamWriter System.IO.File::AppendText(System.String)
extern "C" StreamWriter_t1_448 * File_AppendText_m1_4850 (Object_t * __this /* static, unused */, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.File::Copy(System.String,System.String)
extern "C" void File_Copy_m1_4851 (Object_t * __this /* static, unused */, String_t* ___sourceFileName, String_t* ___destFileName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.File::Copy(System.String,System.String,System.Boolean)
extern "C" void File_Copy_m1_4852 (Object_t * __this /* static, unused */, String_t* ___sourceFileName, String_t* ___destFileName, bool ___overwrite, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.FileStream System.IO.File::Create(System.String)
extern "C" FileStream_t1_146 * File_Create_m1_4853 (Object_t * __this /* static, unused */, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.FileStream System.IO.File::Create(System.String,System.Int32)
extern "C" FileStream_t1_146 * File_Create_m1_4854 (Object_t * __this /* static, unused */, String_t* ___path, int32_t ___bufferSize, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.StreamWriter System.IO.File::CreateText(System.String)
extern "C" StreamWriter_t1_448 * File_CreateText_m1_4855 (Object_t * __this /* static, unused */, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.File::Delete(System.String)
extern "C" void File_Delete_m1_4856 (Object_t * __this /* static, unused */, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IO.File::Exists(System.String)
extern "C" bool File_Exists_m1_4857 (Object_t * __this /* static, unused */, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.FileAttributes System.IO.File::GetAttributes(System.String)
extern "C" int32_t File_GetAttributes_m1_4858 (Object_t * __this /* static, unused */, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.IO.File::GetCreationTime(System.String)
extern "C" DateTime_t1_150  File_GetCreationTime_m1_4859 (Object_t * __this /* static, unused */, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.IO.File::GetCreationTimeUtc(System.String)
extern "C" DateTime_t1_150  File_GetCreationTimeUtc_m1_4860 (Object_t * __this /* static, unused */, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.IO.File::GetLastAccessTime(System.String)
extern "C" DateTime_t1_150  File_GetLastAccessTime_m1_4861 (Object_t * __this /* static, unused */, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.IO.File::GetLastAccessTimeUtc(System.String)
extern "C" DateTime_t1_150  File_GetLastAccessTimeUtc_m1_4862 (Object_t * __this /* static, unused */, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.IO.File::GetLastWriteTime(System.String)
extern "C" DateTime_t1_150  File_GetLastWriteTime_m1_4863 (Object_t * __this /* static, unused */, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.IO.File::GetLastWriteTimeUtc(System.String)
extern "C" DateTime_t1_150  File_GetLastWriteTimeUtc_m1_4864 (Object_t * __this /* static, unused */, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.File::Move(System.String,System.String)
extern "C" void File_Move_m1_4865 (Object_t * __this /* static, unused */, String_t* ___sourceFileName, String_t* ___destFileName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.FileStream System.IO.File::Open(System.String,System.IO.FileMode)
extern "C" FileStream_t1_146 * File_Open_m1_4866 (Object_t * __this /* static, unused */, String_t* ___path, int32_t ___mode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.FileStream System.IO.File::Open(System.String,System.IO.FileMode,System.IO.FileAccess)
extern "C" FileStream_t1_146 * File_Open_m1_4867 (Object_t * __this /* static, unused */, String_t* ___path, int32_t ___mode, int32_t ___access, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.FileStream System.IO.File::Open(System.String,System.IO.FileMode,System.IO.FileAccess,System.IO.FileShare)
extern "C" FileStream_t1_146 * File_Open_m1_4868 (Object_t * __this /* static, unused */, String_t* ___path, int32_t ___mode, int32_t ___access, int32_t ___share, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.FileStream System.IO.File::OpenRead(System.String)
extern "C" FileStream_t1_146 * File_OpenRead_m1_4869 (Object_t * __this /* static, unused */, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.StreamReader System.IO.File::OpenText(System.String)
extern "C" StreamReader_t1_447 * File_OpenText_m1_4870 (Object_t * __this /* static, unused */, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.FileStream System.IO.File::OpenWrite(System.String)
extern "C" FileStream_t1_146 * File_OpenWrite_m1_4871 (Object_t * __this /* static, unused */, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.File::Replace(System.String,System.String,System.String)
extern "C" void File_Replace_m1_4872 (Object_t * __this /* static, unused */, String_t* ___sourceFileName, String_t* ___destinationFileName, String_t* ___destinationBackupFileName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.File::Replace(System.String,System.String,System.String,System.Boolean)
extern "C" void File_Replace_m1_4873 (Object_t * __this /* static, unused */, String_t* ___sourceFileName, String_t* ___destinationFileName, String_t* ___destinationBackupFileName, bool ___ignoreMetadataErrors, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.File::SetAttributes(System.String,System.IO.FileAttributes)
extern "C" void File_SetAttributes_m1_4874 (Object_t * __this /* static, unused */, String_t* ___path, int32_t ___fileAttributes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.File::SetCreationTime(System.String,System.DateTime)
extern "C" void File_SetCreationTime_m1_4875 (Object_t * __this /* static, unused */, String_t* ___path, DateTime_t1_150  ___creationTime, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.File::SetCreationTimeUtc(System.String,System.DateTime)
extern "C" void File_SetCreationTimeUtc_m1_4876 (Object_t * __this /* static, unused */, String_t* ___path, DateTime_t1_150  ___creationTimeUtc, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.File::SetLastAccessTime(System.String,System.DateTime)
extern "C" void File_SetLastAccessTime_m1_4877 (Object_t * __this /* static, unused */, String_t* ___path, DateTime_t1_150  ___lastAccessTime, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.File::SetLastAccessTimeUtc(System.String,System.DateTime)
extern "C" void File_SetLastAccessTimeUtc_m1_4878 (Object_t * __this /* static, unused */, String_t* ___path, DateTime_t1_150  ___lastAccessTimeUtc, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.File::SetLastWriteTime(System.String,System.DateTime)
extern "C" void File_SetLastWriteTime_m1_4879 (Object_t * __this /* static, unused */, String_t* ___path, DateTime_t1_150  ___lastWriteTime, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.File::SetLastWriteTimeUtc(System.String,System.DateTime)
extern "C" void File_SetLastWriteTimeUtc_m1_4880 (Object_t * __this /* static, unused */, String_t* ___path, DateTime_t1_150  ___lastWriteTimeUtc, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.File::CheckPathExceptions(System.String)
extern "C" void File_CheckPathExceptions_m1_4881 (Object_t * __this /* static, unused */, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.IO.File::ReadAllBytes(System.String)
extern "C" ByteU5BU5D_t1_109* File_ReadAllBytes_m1_4882 (Object_t * __this /* static, unused */, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.IO.File::ReadAllLines(System.String)
extern "C" StringU5BU5D_t1_238* File_ReadAllLines_m1_4883 (Object_t * __this /* static, unused */, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.IO.File::ReadAllLines(System.String,System.Text.Encoding)
extern "C" StringU5BU5D_t1_238* File_ReadAllLines_m1_4884 (Object_t * __this /* static, unused */, String_t* ___path, Encoding_t1_406 * ___encoding, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.IO.File::ReadAllLines(System.IO.StreamReader)
extern "C" StringU5BU5D_t1_238* File_ReadAllLines_m1_4885 (Object_t * __this /* static, unused */, StreamReader_t1_447 * ___reader, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.IO.File::ReadAllText(System.String)
extern "C" String_t* File_ReadAllText_m1_4886 (Object_t * __this /* static, unused */, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.IO.File::ReadAllText(System.String,System.Text.Encoding)
extern "C" String_t* File_ReadAllText_m1_4887 (Object_t * __this /* static, unused */, String_t* ___path, Encoding_t1_406 * ___encoding, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.File::WriteAllBytes(System.String,System.Byte[])
extern "C" void File_WriteAllBytes_m1_4888 (Object_t * __this /* static, unused */, String_t* ___path, ByteU5BU5D_t1_109* ___bytes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.File::WriteAllLines(System.String,System.String[])
extern "C" void File_WriteAllLines_m1_4889 (Object_t * __this /* static, unused */, String_t* ___path, StringU5BU5D_t1_238* ___contents, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.File::WriteAllLines(System.String,System.String[],System.Text.Encoding)
extern "C" void File_WriteAllLines_m1_4890 (Object_t * __this /* static, unused */, String_t* ___path, StringU5BU5D_t1_238* ___contents, Encoding_t1_406 * ___encoding, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.File::WriteAllLines(System.IO.StreamWriter,System.String[])
extern "C" void File_WriteAllLines_m1_4891 (Object_t * __this /* static, unused */, StreamWriter_t1_448 * ___writer, StringU5BU5D_t1_238* ___contents, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.File::WriteAllText(System.String,System.String)
extern "C" void File_WriteAllText_m1_4892 (Object_t * __this /* static, unused */, String_t* ___path, String_t* ___contents, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.File::WriteAllText(System.String,System.String,System.Text.Encoding)
extern "C" void File_WriteAllText_m1_4893 (Object_t * __this /* static, unused */, String_t* ___path, String_t* ___contents, Encoding_t1_406 * ___encoding, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.IO.File::get_DefaultLocalFileTime()
extern "C" DateTime_t1_150  File_get_DefaultLocalFileTime_m1_4894 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.File::Encrypt(System.String)
extern "C" void File_Encrypt_m1_4895 (Object_t * __this /* static, unused */, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.File::Decrypt(System.String)
extern "C" void File_Decrypt_m1_4896 (Object_t * __this /* static, unused */, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
