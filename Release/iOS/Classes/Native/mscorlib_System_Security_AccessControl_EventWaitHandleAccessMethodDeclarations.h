﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.AccessControl.EventWaitHandleAccessRule
struct EventWaitHandleAccessRule_t1_1149;
// System.Security.Principal.IdentityReference
struct IdentityReference_t1_1120;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_AccessControl_EventWaitHandleRights.h"
#include "mscorlib_System_Security_AccessControl_AccessControlType.h"

// System.Void System.Security.AccessControl.EventWaitHandleAccessRule::.ctor(System.Security.Principal.IdentityReference,System.Security.AccessControl.EventWaitHandleRights,System.Security.AccessControl.AccessControlType)
extern "C" void EventWaitHandleAccessRule__ctor_m1_9846 (EventWaitHandleAccessRule_t1_1149 * __this, IdentityReference_t1_1120 * ___identity, int32_t ___eventRights, int32_t ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.EventWaitHandleAccessRule::.ctor(System.String,System.Security.AccessControl.EventWaitHandleRights,System.Security.AccessControl.AccessControlType)
extern "C" void EventWaitHandleAccessRule__ctor_m1_9847 (EventWaitHandleAccessRule_t1_1149 * __this, String_t* ___identity, int32_t ___eventRights, int32_t ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.AccessControl.EventWaitHandleRights System.Security.AccessControl.EventWaitHandleAccessRule::get_EventWaitHandleRights()
extern "C" int32_t EventWaitHandleAccessRule_get_EventWaitHandleRights_m1_9848 (EventWaitHandleAccessRule_t1_1149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
