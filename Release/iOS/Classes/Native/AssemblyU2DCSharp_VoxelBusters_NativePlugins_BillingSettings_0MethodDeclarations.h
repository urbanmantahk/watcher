﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.BillingSettings/iOSSettings
struct iOSSettings_t8_216;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.BillingSettings/iOSSettings::.ctor()
extern "C" void iOSSettings__ctor_m8_1230 (iOSSettings_t8_216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.NativePlugins.BillingSettings/iOSSettings::get_SupportsReceiptValidation()
extern "C" bool iOSSettings_get_SupportsReceiptValidation_m8_1231 (iOSSettings_t8_216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.BillingSettings/iOSSettings::set_SupportsReceiptValidation(System.Boolean)
extern "C" void iOSSettings_set_SupportsReceiptValidation_m8_1232 (iOSSettings_t8_216 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.BillingSettings/iOSSettings::get_ValidateUsingServerURL()
extern "C" String_t* iOSSettings_get_ValidateUsingServerURL_m8_1233 (iOSSettings_t8_216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.BillingSettings/iOSSettings::set_ValidateUsingServerURL(System.String)
extern "C" void iOSSettings_set_ValidateUsingServerURL_m8_1234 (iOSSettings_t8_216 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
