﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.Utility.GUIScrollView
struct GUIScrollView_t8_19;
// UnityEngine.GUIStyle
struct GUIStyle_t6_176;
// UnityEngine.GUILayoutOption[]
struct GUILayoutOptionU5BU5D_t6_290;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.Utility.GUIScrollView::.ctor()
extern "C" void GUIScrollView__ctor_m8_807 (GUIScrollView_t8_19 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.GUIScrollView::BeginScrollView(UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C" void GUIScrollView_BeginScrollView_m8_808 (GUIScrollView_t8_19 * __this, GUIStyle_t6_176 * ____style, GUILayoutOptionU5BU5D_t6_290* ____options, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.GUIScrollView::BeginScrollView(UnityEngine.GUILayoutOption[])
extern "C" void GUIScrollView_BeginScrollView_m8_809 (GUIScrollView_t8_19 * __this, GUILayoutOptionU5BU5D_t6_290* ____options, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.GUIScrollView::EndScrollView()
extern "C" void GUIScrollView_EndScrollView_m8_810 (GUIScrollView_t8_19 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.GUIScrollView::Reset()
extern "C" void GUIScrollView_Reset_m8_811 (GUIScrollView_t8_19 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.GUIScrollView::Update()
extern "C" void GUIScrollView_Update_m8_812 (GUIScrollView_t8_19 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.GUIScrollView::UpdateScroll()
extern "C" void GUIScrollView_UpdateScroll_m8_813 (GUIScrollView_t8_19 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
