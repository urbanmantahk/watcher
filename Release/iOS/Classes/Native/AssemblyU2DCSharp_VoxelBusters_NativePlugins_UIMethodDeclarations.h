﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.UI
struct UI_t8_303;
// System.String
struct String_t;
// System.Collections.IDictionary
struct IDictionary_t1_35;
// VoxelBusters.NativePlugins.UI/AlertDialogCompletion
struct AlertDialogCompletion_t8_300;
// System.String[]
struct StringU5BU5D_t1_238;
// VoxelBusters.NativePlugins.UI/SingleFieldPromptCompletion
struct SingleFieldPromptCompletion_t8_301;
// VoxelBusters.NativePlugins.UI/LoginPromptCompletion
struct LoginPromptCompletion_t8_302;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eToastMessageLe.h"
#include "UnityEngine_UnityEngine_Vector2.h"

// System.Void VoxelBusters.NativePlugins.UI::.ctor()
extern "C" void UI__ctor_m8_1760 (UI_t8_303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.UI::AlertDialogClosed(System.String)
extern "C" void UI_AlertDialogClosed_m8_1761 (UI_t8_303 * __this, String_t* ____jsonStr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.UI::ParseAlertDialogDismissedData(System.Collections.IDictionary,System.String&,System.String&)
extern "C" void UI_ParseAlertDialogDismissedData_m8_1762 (UI_t8_303 * __this, Object_t * ____dataDict, String_t** ____buttonPressed, String_t** ____callerTag, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.UI::CacheAlertDialogCallback(VoxelBusters.NativePlugins.UI/AlertDialogCompletion)
extern "C" String_t* UI_CacheAlertDialogCallback_m8_1763 (UI_t8_303 * __this, AlertDialogCompletion_t8_300 * ____newCallback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.NativePlugins.UI/AlertDialogCompletion VoxelBusters.NativePlugins.UI::GetAlertDialogCallback(System.String)
extern "C" AlertDialogCompletion_t8_300 * UI_GetAlertDialogCallback_m8_1764 (UI_t8_303 * __this, String_t* ____tag, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.UI::ShowAlertDialogWithSingleButton(System.String,System.String,System.String,VoxelBusters.NativePlugins.UI/AlertDialogCompletion)
extern "C" void UI_ShowAlertDialogWithSingleButton_m8_1765 (UI_t8_303 * __this, String_t* ____title, String_t* ____message, String_t* ____button, AlertDialogCompletion_t8_300 * ____onCompletion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.UI::ShowAlertDialogWithMultipleButtons(System.String,System.String,System.String[],VoxelBusters.NativePlugins.UI/AlertDialogCompletion)
extern "C" void UI_ShowAlertDialogWithMultipleButtons_m8_1766 (UI_t8_303 * __this, String_t* ____title, String_t* ____message, StringU5BU5D_t1_238* ____buttonsList, AlertDialogCompletion_t8_300 * ____onCompletion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.UI::ShowAlertDialogWithMultipleButtons(System.String,System.String,System.String[],System.String)
extern "C" void UI_ShowAlertDialogWithMultipleButtons_m8_1767 (UI_t8_303 * __this, String_t* ____title, String_t* ____message, StringU5BU5D_t1_238* ____buttonsList, String_t* ____callbackTag, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.UI::SingleFieldPromptDialogClosed(System.String)
extern "C" void UI_SingleFieldPromptDialogClosed_m8_1768 (UI_t8_303 * __this, String_t* ____jsonStr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.UI::LoginPromptDialogClosed(System.String)
extern "C" void UI_LoginPromptDialogClosed_m8_1769 (UI_t8_303 * __this, String_t* ____jsonStr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.UI::ParseSingleFieldPromptClosedData(System.Collections.IDictionary,System.String&,System.String&)
extern "C" void UI_ParseSingleFieldPromptClosedData_m8_1770 (UI_t8_303 * __this, Object_t * ____dataDict, String_t** ____buttonPressed, String_t** ____inputText, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.UI::ParseLoginPromptClosedData(System.Collections.IDictionary,System.String&,System.String&,System.String&)
extern "C" void UI_ParseLoginPromptClosedData_m8_1771 (UI_t8_303 * __this, Object_t * ____dataDict, String_t** ____buttonPressed, String_t** ____usernameText, String_t** ____passwordText, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.UI::ShowSingleFieldPromptDialogWithPlainText(System.String,System.String,System.String,System.String[],VoxelBusters.NativePlugins.UI/SingleFieldPromptCompletion)
extern "C" void UI_ShowSingleFieldPromptDialogWithPlainText_m8_1772 (UI_t8_303 * __this, String_t* ____title, String_t* ____message, String_t* ____placeholder, StringU5BU5D_t1_238* ____buttonsList, SingleFieldPromptCompletion_t8_301 * ____onCompletion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.UI::ShowSingleFieldPromptDialogWithSecuredText(System.String,System.String,System.String,System.String[],VoxelBusters.NativePlugins.UI/SingleFieldPromptCompletion)
extern "C" void UI_ShowSingleFieldPromptDialogWithSecuredText_m8_1773 (UI_t8_303 * __this, String_t* ____title, String_t* ____message, String_t* ____placeholder, StringU5BU5D_t1_238* ____buttonsList, SingleFieldPromptCompletion_t8_301 * ____onCompletion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.UI::ShowSingleFieldPromptDialog(System.String,System.String,System.String,System.Boolean,System.String[],VoxelBusters.NativePlugins.UI/SingleFieldPromptCompletion)
extern "C" void UI_ShowSingleFieldPromptDialog_m8_1774 (UI_t8_303 * __this, String_t* ____title, String_t* ____message, String_t* ____placeholder, bool ____useSecureText, StringU5BU5D_t1_238* ____buttonsList, SingleFieldPromptCompletion_t8_301 * ____onCompletion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.UI::ShowLoginPromptDialog(System.String,System.String,System.String,System.String,System.String[],VoxelBusters.NativePlugins.UI/LoginPromptCompletion)
extern "C" void UI_ShowLoginPromptDialog_m8_1775 (UI_t8_303 * __this, String_t* ____title, String_t* ____message, String_t* ____usernamePlaceHolder, String_t* ____passwordPlaceHolder, StringU5BU5D_t1_238* ____buttonsList, LoginPromptCompletion_t8_302 * ____onCompletion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.UI::ShowToast(System.String,VoxelBusters.NativePlugins.eToastMessageLength)
extern "C" void UI_ShowToast_m8_1776 (UI_t8_303 * __this, String_t* ____message, int32_t ____length, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.UI::SetPopoverPoint(UnityEngine.Vector2)
extern "C" void UI_SetPopoverPoint_m8_1777 (UI_t8_303 * __this, Vector2_t6_47  ____position, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.UI::SetPopoverPointAtLastTouchPosition()
extern "C" void UI_SetPopoverPointAtLastTouchPosition_m8_1778 (UI_t8_303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
