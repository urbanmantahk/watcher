﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32>
struct ReadOnlyCollection_1_t1_2313;
// System.Collections.Generic.IList`1<System.Int32>
struct IList_1_t1_1922;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Object
struct Object_t;
// System.Int32[]
struct Int32U5BU5D_t1_275;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t1_2773;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C" void ReadOnlyCollection_1__ctor_m1_19594_gshared (ReadOnlyCollection_1_t1_2313 * __this, Object_t* ___list, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m1_19594(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t1_2313 *, Object_t*, const MethodInfo*))ReadOnlyCollection_1__ctor_m1_19594_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1_19595_gshared (ReadOnlyCollection_1_t1_2313 * __this, int32_t ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1_19595(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t1_2313 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1_19595_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32>::System.Collections.Generic.ICollection<T>.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1_19596_gshared (ReadOnlyCollection_1_t1_2313 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1_19596(__this, method) (( void (*) (ReadOnlyCollection_1_t1_2313 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1_19596_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1_19597_gshared (ReadOnlyCollection_1_t1_2313 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1_19597(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t1_2313 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1_19597_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1_19598_gshared (ReadOnlyCollection_1_t1_2313 * __this, int32_t ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1_19598(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t1_2313 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1_19598_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1_19599_gshared (ReadOnlyCollection_1_t1_2313 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1_19599(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t1_2313 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1_19599_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1_19600_gshared (ReadOnlyCollection_1_t1_2313 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1_19600(__this, ___index, method) (( int32_t (*) (ReadOnlyCollection_1_t1_2313 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1_19600_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1_19601_gshared (ReadOnlyCollection_1_t1_2313 * __this, int32_t ___index, int32_t ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1_19601(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t1_2313 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1_19601_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_19602_gshared (ReadOnlyCollection_1_t1_2313 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_19602(__this, method) (( bool (*) (ReadOnlyCollection_1_t1_2313 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_19602_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1_19603_gshared (ReadOnlyCollection_1_t1_2313 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1_19603(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t1_2313 *, Array_t *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1_19603_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1_19604_gshared (ReadOnlyCollection_1_t1_2313 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1_19604(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t1_2313 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1_19604_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32>::System.Collections.IList.Add(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m1_19605_gshared (ReadOnlyCollection_1_t1_2313 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m1_19605(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t1_2313 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m1_19605_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32>::System.Collections.IList.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m1_19606_gshared (ReadOnlyCollection_1_t1_2313 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m1_19606(__this, method) (( void (*) (ReadOnlyCollection_1_t1_2313 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m1_19606_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32>::System.Collections.IList.Contains(System.Object)
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_Contains_m1_19607_gshared (ReadOnlyCollection_1_t1_2313 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m1_19607(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t1_2313 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m1_19607_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1_19608_gshared (ReadOnlyCollection_1_t1_2313 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1_19608(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t1_2313 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1_19608_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m1_19609_gshared (ReadOnlyCollection_1_t1_2313 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m1_19609(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t1_2313 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m1_19609_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32>::System.Collections.IList.Remove(System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m1_19610_gshared (ReadOnlyCollection_1_t1_2313 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m1_19610(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t1_2313 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m1_19610_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32>::System.Collections.IList.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1_19611_gshared (ReadOnlyCollection_1_t1_2313 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1_19611(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t1_2313 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1_19611_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1_19612_gshared (ReadOnlyCollection_1_t1_2313 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1_19612(__this, method) (( bool (*) (ReadOnlyCollection_1_t1_2313 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1_19612_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1_19613_gshared (ReadOnlyCollection_1_t1_2313 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1_19613(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t1_2313 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1_19613_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32>::System.Collections.IList.get_IsFixedSize()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1_19614_gshared (ReadOnlyCollection_1_t1_2313 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1_19614(__this, method) (( bool (*) (ReadOnlyCollection_1_t1_2313 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1_19614_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32>::System.Collections.IList.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1_19615_gshared (ReadOnlyCollection_1_t1_2313 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1_19615(__this, method) (( bool (*) (ReadOnlyCollection_1_t1_2313 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1_19615_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IList_get_Item_m1_19616_gshared (ReadOnlyCollection_1_t1_2313 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m1_19616(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t1_2313 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m1_19616_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m1_19617_gshared (ReadOnlyCollection_1_t1_2313 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m1_19617(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t1_2313 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m1_19617_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32>::Contains(T)
extern "C" bool ReadOnlyCollection_1_Contains_m1_19618_gshared (ReadOnlyCollection_1_t1_2313 * __this, int32_t ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m1_19618(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t1_2313 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_Contains_m1_19618_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32>::CopyTo(T[],System.Int32)
extern "C" void ReadOnlyCollection_1_CopyTo_m1_19619_gshared (ReadOnlyCollection_1_t1_2313 * __this, Int32U5BU5D_t1_275* ___array, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m1_19619(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t1_2313 *, Int32U5BU5D_t1_275*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m1_19619_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32>::GetEnumerator()
extern "C" Object_t* ReadOnlyCollection_1_GetEnumerator_m1_19620_gshared (ReadOnlyCollection_1_t1_2313 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m1_19620(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t1_2313 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m1_19620_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32>::IndexOf(T)
extern "C" int32_t ReadOnlyCollection_1_IndexOf_m1_19621_gshared (ReadOnlyCollection_1_t1_2313 * __this, int32_t ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m1_19621(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t1_2313 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m1_19621_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32>::get_Count()
extern "C" int32_t ReadOnlyCollection_1_get_Count_m1_19622_gshared (ReadOnlyCollection_1_t1_2313 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m1_19622(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t1_2313 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m1_19622_gshared)(__this, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32>::get_Items()
extern "C" Object_t* ReadOnlyCollection_1_get_Items_m1_19623_gshared (ReadOnlyCollection_1_t1_2313 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Items_m1_19623(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t1_2313 *, const MethodInfo*))ReadOnlyCollection_1_get_Items_m1_19623_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32>::get_Item(System.Int32)
extern "C" int32_t ReadOnlyCollection_1_get_Item_m1_19624_gshared (ReadOnlyCollection_1_t1_2313 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m1_19624(__this, ___index, method) (( int32_t (*) (ReadOnlyCollection_1_t1_2313 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m1_19624_gshared)(__this, ___index, method)
