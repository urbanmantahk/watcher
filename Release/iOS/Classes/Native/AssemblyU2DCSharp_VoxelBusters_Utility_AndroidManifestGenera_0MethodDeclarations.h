﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.Utility.AndroidManifestGenerator
struct AndroidManifestGenerator_t8_25;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.Utility.AndroidManifestGenerator::.ctor()
extern "C" void AndroidManifestGenerator__ctor_m8_164 (AndroidManifestGenerator_t8_25 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
