﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.CompilerServices.ScopelessEnumAttribute
struct ScopelessEnumAttribute_t1_707;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.CompilerServices.ScopelessEnumAttribute::.ctor()
extern "C" void ScopelessEnumAttribute__ctor_m1_7554 (ScopelessEnumAttribute_t1_707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
