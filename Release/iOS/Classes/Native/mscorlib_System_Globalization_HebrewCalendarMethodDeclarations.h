﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Globalization.HebrewCalendar
struct HebrewCalendar_t1_373;
// System.Int32[]
struct Int32U5BU5D_t1_275;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime.h"
#include "mscorlib_System_DayOfWeek.h"

// System.Void System.Globalization.HebrewCalendar::.ctor()
extern "C" void HebrewCalendar__ctor_m1_4118 (HebrewCalendar_t1_373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.HebrewCalendar::.cctor()
extern "C" void HebrewCalendar__cctor_m1_4119 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.HebrewCalendar::get_M_MaxYear()
extern "C" int32_t HebrewCalendar_get_M_MaxYear_m1_4120 (HebrewCalendar_t1_373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] System.Globalization.HebrewCalendar::get_Eras()
extern "C" Int32U5BU5D_t1_275* HebrewCalendar_get_Eras_m1_4121 (HebrewCalendar_t1_373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.HebrewCalendar::get_TwoDigitYearMax()
extern "C" int32_t HebrewCalendar_get_TwoDigitYearMax_m1_4122 (HebrewCalendar_t1_373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.HebrewCalendar::set_TwoDigitYearMax(System.Int32)
extern "C" void HebrewCalendar_set_TwoDigitYearMax_m1_4123 (HebrewCalendar_t1_373 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.HebrewCalendar::M_CheckDateTime(System.DateTime)
extern "C" void HebrewCalendar_M_CheckDateTime_m1_4124 (HebrewCalendar_t1_373 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.HebrewCalendar::M_CheckEra(System.Int32&)
extern "C" void HebrewCalendar_M_CheckEra_m1_4125 (HebrewCalendar_t1_373 * __this, int32_t* ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.HebrewCalendar::M_CheckYE(System.Int32,System.Int32&)
extern "C" void HebrewCalendar_M_CheckYE_m1_4126 (HebrewCalendar_t1_373 * __this, int32_t ___year, int32_t* ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.HebrewCalendar::M_CheckYME(System.Int32,System.Int32,System.Int32&)
extern "C" void HebrewCalendar_M_CheckYME_m1_4127 (HebrewCalendar_t1_373 * __this, int32_t ___year, int32_t ___month, int32_t* ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.HebrewCalendar::M_CheckYMDE(System.Int32,System.Int32,System.Int32,System.Int32&)
extern "C" void HebrewCalendar_M_CheckYMDE_m1_4128 (HebrewCalendar_t1_373 * __this, int32_t ___year, int32_t ___month, int32_t ___day, int32_t* ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.HebrewCalendar::AddMonths(System.DateTime,System.Int32)
extern "C" DateTime_t1_150  HebrewCalendar_AddMonths_m1_4129 (HebrewCalendar_t1_373 * __this, DateTime_t1_150  ___time, int32_t ___months, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.HebrewCalendar::AddYears(System.DateTime,System.Int32)
extern "C" DateTime_t1_150  HebrewCalendar_AddYears_m1_4130 (HebrewCalendar_t1_373 * __this, DateTime_t1_150  ___time, int32_t ___years, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.HebrewCalendar::GetDayOfMonth(System.DateTime)
extern "C" int32_t HebrewCalendar_GetDayOfMonth_m1_4131 (HebrewCalendar_t1_373 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DayOfWeek System.Globalization.HebrewCalendar::GetDayOfWeek(System.DateTime)
extern "C" int32_t HebrewCalendar_GetDayOfWeek_m1_4132 (HebrewCalendar_t1_373 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.HebrewCalendar::GetDayOfYear(System.DateTime)
extern "C" int32_t HebrewCalendar_GetDayOfYear_m1_4133 (HebrewCalendar_t1_373 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.HebrewCalendar::M_CCMonth(System.Int32,System.Int32)
extern "C" int32_t HebrewCalendar_M_CCMonth_m1_4134 (HebrewCalendar_t1_373 * __this, int32_t ___month, int32_t ___year, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.HebrewCalendar::M_Month(System.Int32,System.Int32)
extern "C" int32_t HebrewCalendar_M_Month_m1_4135 (HebrewCalendar_t1_373 * __this, int32_t ___ccmonth, int32_t ___year, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.HebrewCalendar::GetDaysInMonth(System.Int32,System.Int32,System.Int32)
extern "C" int32_t HebrewCalendar_GetDaysInMonth_m1_4136 (HebrewCalendar_t1_373 * __this, int32_t ___year, int32_t ___month, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.HebrewCalendar::GetDaysInYear(System.Int32,System.Int32)
extern "C" int32_t HebrewCalendar_GetDaysInYear_m1_4137 (HebrewCalendar_t1_373 * __this, int32_t ___year, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.HebrewCalendar::GetEra(System.DateTime)
extern "C" int32_t HebrewCalendar_GetEra_m1_4138 (HebrewCalendar_t1_373 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.HebrewCalendar::GetLeapMonth(System.Int32,System.Int32)
extern "C" int32_t HebrewCalendar_GetLeapMonth_m1_4139 (HebrewCalendar_t1_373 * __this, int32_t ___year, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.HebrewCalendar::GetMonth(System.DateTime)
extern "C" int32_t HebrewCalendar_GetMonth_m1_4140 (HebrewCalendar_t1_373 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.HebrewCalendar::GetMonthsInYear(System.Int32,System.Int32)
extern "C" int32_t HebrewCalendar_GetMonthsInYear_m1_4141 (HebrewCalendar_t1_373 * __this, int32_t ___year, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.HebrewCalendar::GetYear(System.DateTime)
extern "C" int32_t HebrewCalendar_GetYear_m1_4142 (HebrewCalendar_t1_373 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.HebrewCalendar::IsLeapDay(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" bool HebrewCalendar_IsLeapDay_m1_4143 (HebrewCalendar_t1_373 * __this, int32_t ___year, int32_t ___month, int32_t ___day, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.HebrewCalendar::IsLeapMonth(System.Int32,System.Int32,System.Int32)
extern "C" bool HebrewCalendar_IsLeapMonth_m1_4144 (HebrewCalendar_t1_373 * __this, int32_t ___year, int32_t ___month, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.HebrewCalendar::IsLeapYear(System.Int32,System.Int32)
extern "C" bool HebrewCalendar_IsLeapYear_m1_4145 (HebrewCalendar_t1_373 * __this, int32_t ___year, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.HebrewCalendar::ToDateTime(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" DateTime_t1_150  HebrewCalendar_ToDateTime_m1_4146 (HebrewCalendar_t1_373 * __this, int32_t ___year, int32_t ___month, int32_t ___day, int32_t ___hour, int32_t ___minute, int32_t ___second, int32_t ___millisecond, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.HebrewCalendar::ToFourDigitYear(System.Int32)
extern "C" int32_t HebrewCalendar_ToFourDigitYear_m1_4147 (HebrewCalendar_t1_373 * __this, int32_t ___year, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.HebrewCalendar::get_MinSupportedDateTime()
extern "C" DateTime_t1_150  HebrewCalendar_get_MinSupportedDateTime_m1_4148 (HebrewCalendar_t1_373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.HebrewCalendar::get_MaxSupportedDateTime()
extern "C" DateTime_t1_150  HebrewCalendar_get_MaxSupportedDateTime_m1_4149 (HebrewCalendar_t1_373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
