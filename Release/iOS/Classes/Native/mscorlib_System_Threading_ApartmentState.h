﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Threading_ApartmentState.h"

// System.Threading.ApartmentState
struct  ApartmentState_t1_1457 
{
	// System.Int32 System.Threading.ApartmentState::value__
	int32_t ___value___1;
};
