﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.AssemblyFileVersionAttribute
struct AssemblyFileVersionAttribute_t1_571;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Reflection.AssemblyFileVersionAttribute::.ctor(System.String)
extern "C" void AssemblyFileVersionAttribute__ctor_m1_6682 (AssemblyFileVersionAttribute_t1_571 * __this, String_t* ___version, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.AssemblyFileVersionAttribute::get_Version()
extern "C" String_t* AssemblyFileVersionAttribute_get_Version_m1_6683 (AssemblyFileVersionAttribute_t1_571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
