﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GUISkin
struct GUISkin_t6_169;
// VoxelBusters.Utility.GUIScrollView
struct GUIScrollView_t8_19;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "UnityEngine_UnityEngine_Rect.h"

// VoxelBusters.Utility.GUIModalWindow
struct  GUIModalWindow_t8_15  : public MonoBehaviour_t6_91
{
	// UnityEngine.GUISkin VoxelBusters.Utility.GUIModalWindow::m_uiSkin
	GUISkin_t6_169 * ___m_uiSkin_2;
	// UnityEngine.GUISkin VoxelBusters.Utility.GUIModalWindow::m_oldSkin
	GUISkin_t6_169 * ___m_oldSkin_3;
	// VoxelBusters.Utility.GUIScrollView VoxelBusters.Utility.GUIModalWindow::m_rootScrollView
	GUIScrollView_t8_19 * ___m_rootScrollView_4;
	// UnityEngine.Rect VoxelBusters.Utility.GUIModalWindow::m_windowRect
	Rect_t6_51  ___m_windowRect_5;
};
