﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_10.h"
#include "mscorlib_System_Reflection_ParameterModifier.h"

// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_15450_gshared (InternalEnumerator_1_t1_1958 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1_15450(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1_1958 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m1_15450_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_15451_gshared (InternalEnumerator_1_t1_1958 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_15451(__this, method) (( void (*) (InternalEnumerator_1_t1_1958 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_15451_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_15452_gshared (InternalEnumerator_1_t1_1958 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_15452(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1_1958 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_15452_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_15453_gshared (InternalEnumerator_1_t1_1958 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1_15453(__this, method) (( void (*) (InternalEnumerator_1_t1_1958 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1_15453_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_15454_gshared (InternalEnumerator_1_t1_1958 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1_15454(__this, method) (( bool (*) (InternalEnumerator_1_t1_1958 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1_15454_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::get_Current()
extern "C" ParameterModifier_t1_628  InternalEnumerator_1_get_Current_m1_15455_gshared (InternalEnumerator_1_t1_1958 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1_15455(__this, method) (( ParameterModifier_t1_628  (*) (InternalEnumerator_1_t1_1958 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1_15455_gshared)(__this, method)
