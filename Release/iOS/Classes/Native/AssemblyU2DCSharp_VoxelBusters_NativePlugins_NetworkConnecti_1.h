﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// VoxelBusters.NativePlugins.NetworkConnectivitySettings/EditorSettings
struct EditorSettings_t8_254;
// VoxelBusters.NativePlugins.NetworkConnectivitySettings/AndroidSettings
struct AndroidSettings_t8_255;

#include "mscorlib_System_Object.h"

// VoxelBusters.NativePlugins.NetworkConnectivitySettings
struct  NetworkConnectivitySettings_t8_256  : public Object_t
{
	// System.String VoxelBusters.NativePlugins.NetworkConnectivitySettings::m_ipAddress
	String_t* ___m_ipAddress_0;
	// VoxelBusters.NativePlugins.NetworkConnectivitySettings/EditorSettings VoxelBusters.NativePlugins.NetworkConnectivitySettings::m_editor
	EditorSettings_t8_254 * ___m_editor_1;
	// VoxelBusters.NativePlugins.NetworkConnectivitySettings/AndroidSettings VoxelBusters.NativePlugins.NetworkConnectivitySettings::m_android
	AndroidSettings_t8_255 * ___m_android_2;
};
