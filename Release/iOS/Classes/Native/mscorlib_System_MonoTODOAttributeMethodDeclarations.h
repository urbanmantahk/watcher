﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.MonoTODOAttribute
struct MonoTODOAttribute_t1_76;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.MonoTODOAttribute::.ctor()
extern "C" void MonoTODOAttribute__ctor_m1_1406 (MonoTODOAttribute_t1_76 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.MonoTODOAttribute::.ctor(System.String)
extern "C" void MonoTODOAttribute__ctor_m1_1407 (MonoTODOAttribute_t1_76 * __this, String_t* ___comment, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.MonoTODOAttribute::get_Comment()
extern "C" String_t* MonoTODOAttribute_get_Comment_m1_1408 (MonoTODOAttribute_t1_76 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
