﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_ExifLibrary_ExifURationalArray.h"

// ExifLibrary.GPSLatitudeLongitude
struct  GPSLatitudeLongitude_t8_106  : public ExifURationalArray_t8_107
{
};
