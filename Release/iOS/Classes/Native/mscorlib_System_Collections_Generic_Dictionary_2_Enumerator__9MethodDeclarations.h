﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__4MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1_21293(__this, ___dictionary, method) (( void (*) (Enumerator_t1_2409 *, Dictionary_2_t1_1829 *, const MethodInfo*))Enumerator__ctor_m1_15990_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_21294(__this, method) (( Object_t * (*) (Enumerator_t1_2409 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_15991_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1_21295(__this, method) (( void (*) (Enumerator_t1_2409 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_15992_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_21296(__this, method) (( DictionaryEntry_t1_284  (*) (Enumerator_t1_2409 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_15993_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_21297(__this, method) (( Object_t * (*) (Enumerator_t1_2409 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_15994_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_21298(__this, method) (( Object_t * (*) (Enumerator_t1_2409 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_15995_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::MoveNext()
#define Enumerator_MoveNext_m1_21299(__this, method) (( bool (*) (Enumerator_t1_2409 *, const MethodInfo*))Enumerator_MoveNext_m1_15996_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::get_Current()
#define Enumerator_get_Current_m1_21300(__this, method) (( KeyValuePair_2_t1_2407  (*) (Enumerator_t1_2409 *, const MethodInfo*))Enumerator_get_Current_m1_15997_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1_21301(__this, method) (( String_t* (*) (Enumerator_t1_2409 *, const MethodInfo*))Enumerator_get_CurrentKey_m1_15998_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1_21302(__this, method) (( GUIStyle_t6_176 * (*) (Enumerator_t1_2409 *, const MethodInfo*))Enumerator_get_CurrentValue_m1_15999_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::Reset()
#define Enumerator_Reset_m1_21303(__this, method) (( void (*) (Enumerator_t1_2409 *, const MethodInfo*))Enumerator_Reset_m1_16000_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::VerifyState()
#define Enumerator_VerifyState_m1_21304(__this, method) (( void (*) (Enumerator_t1_2409 *, const MethodInfo*))Enumerator_VerifyState_m1_16001_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1_21305(__this, method) (( void (*) (Enumerator_t1_2409 *, const MethodInfo*))Enumerator_VerifyCurrent_m1_16002_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::Dispose()
#define Enumerator_Dispose_m1_21306(__this, method) (( void (*) (Enumerator_t1_2409 *, const MethodInfo*))Enumerator_Dispose_m1_16003_gshared)(__this, method)
