﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.InteropServices.BestFitMappingAttribute
struct BestFitMappingAttribute_t1_758;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.InteropServices.BestFitMappingAttribute::.ctor(System.Boolean)
extern "C" void BestFitMappingAttribute__ctor_m1_7588 (BestFitMappingAttribute_t1_758 * __this, bool ___BestFitMapping, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.InteropServices.BestFitMappingAttribute::get_BestFitMapping()
extern "C" bool BestFitMappingAttribute_get_BestFitMapping_m1_7589 (BestFitMappingAttribute_t1_758 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
