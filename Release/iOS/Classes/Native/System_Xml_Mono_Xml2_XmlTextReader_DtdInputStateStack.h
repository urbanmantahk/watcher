﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Stack
struct Stack_t1_243;

#include "mscorlib_System_Object.h"

// Mono.Xml2.XmlTextReader/DtdInputStateStack
struct  DtdInputStateStack_t4_173  : public Object_t
{
	// System.Collections.Stack Mono.Xml2.XmlTextReader/DtdInputStateStack::intern
	Stack_t1_243 * ___intern_0;
};
