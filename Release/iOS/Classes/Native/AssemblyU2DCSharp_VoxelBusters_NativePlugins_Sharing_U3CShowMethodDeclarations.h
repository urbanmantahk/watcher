﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.Sharing/<ShowViewCoroutine>c__Iterator6
struct U3CShowViewCoroutineU3Ec__Iterator6_t8_272;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.Sharing/<ShowViewCoroutine>c__Iterator6::.ctor()
extern "C" void U3CShowViewCoroutineU3Ec__Iterator6__ctor_m8_1572 (U3CShowViewCoroutineU3Ec__Iterator6_t8_272 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VoxelBusters.NativePlugins.Sharing/<ShowViewCoroutine>c__Iterator6::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CShowViewCoroutineU3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m8_1573 (U3CShowViewCoroutineU3Ec__Iterator6_t8_272 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VoxelBusters.NativePlugins.Sharing/<ShowViewCoroutine>c__Iterator6::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CShowViewCoroutineU3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m8_1574 (U3CShowViewCoroutineU3Ec__Iterator6_t8_272 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.NativePlugins.Sharing/<ShowViewCoroutine>c__Iterator6::MoveNext()
extern "C" bool U3CShowViewCoroutineU3Ec__Iterator6_MoveNext_m8_1575 (U3CShowViewCoroutineU3Ec__Iterator6_t8_272 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Sharing/<ShowViewCoroutine>c__Iterator6::Dispose()
extern "C" void U3CShowViewCoroutineU3Ec__Iterator6_Dispose_m8_1576 (U3CShowViewCoroutineU3Ec__Iterator6_t8_272 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Sharing/<ShowViewCoroutine>c__Iterator6::Reset()
extern "C" void U3CShowViewCoroutineU3Ec__Iterator6_Reset_m8_1577 (U3CShowViewCoroutineU3Ec__Iterator6_t8_272 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
