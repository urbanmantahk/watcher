﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_ShaderUtility_eShader.h"

// VoxelBusters.Utility.ShaderUtility/eShaderPropertyType
struct  eShaderPropertyType_t8_28 
{
	// System.Int32 VoxelBusters.Utility.ShaderUtility/eShaderPropertyType::value__
	int32_t ___value___1;
};
