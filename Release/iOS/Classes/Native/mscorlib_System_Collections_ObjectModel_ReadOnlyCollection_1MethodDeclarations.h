﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>
struct ReadOnlyCollection_1_t1_1750;
// System.Collections.Generic.IList`1<System.Reflection.CustomAttributeTypedArgument>
struct IList_1_t1_589;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Object
struct Object_t;
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t1_1748;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeTypedArgument>
struct IEnumerator_1_t1_2778;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArgument.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C" void ReadOnlyCollection_1__ctor_m1_14892_gshared (ReadOnlyCollection_1_t1_1750 * __this, Object_t* ___list, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m1_14892(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t1_1750 *, Object_t*, const MethodInfo*))ReadOnlyCollection_1__ctor_m1_14892_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1_16311_gshared (ReadOnlyCollection_1_t1_1750 * __this, CustomAttributeTypedArgument_t1_594  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1_16311(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t1_1750 *, CustomAttributeTypedArgument_t1_594 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1_16311_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.ICollection<T>.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1_16312_gshared (ReadOnlyCollection_1_t1_1750 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1_16312(__this, method) (( void (*) (ReadOnlyCollection_1_t1_1750 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1_16312_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1_16313_gshared (ReadOnlyCollection_1_t1_1750 * __this, int32_t ___index, CustomAttributeTypedArgument_t1_594  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1_16313(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t1_1750 *, int32_t, CustomAttributeTypedArgument_t1_594 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1_16313_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1_16314_gshared (ReadOnlyCollection_1_t1_1750 * __this, CustomAttributeTypedArgument_t1_594  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1_16314(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t1_1750 *, CustomAttributeTypedArgument_t1_594 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1_16314_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1_16315_gshared (ReadOnlyCollection_1_t1_1750 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1_16315(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t1_1750 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1_16315_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C" CustomAttributeTypedArgument_t1_594  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1_16316_gshared (ReadOnlyCollection_1_t1_1750 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1_16316(__this, ___index, method) (( CustomAttributeTypedArgument_t1_594  (*) (ReadOnlyCollection_1_t1_1750 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1_16316_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1_16317_gshared (ReadOnlyCollection_1_t1_1750 * __this, int32_t ___index, CustomAttributeTypedArgument_t1_594  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1_16317(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t1_1750 *, int32_t, CustomAttributeTypedArgument_t1_594 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1_16317_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_16318_gshared (ReadOnlyCollection_1_t1_1750 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_16318(__this, method) (( bool (*) (ReadOnlyCollection_1_t1_1750 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_16318_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1_16319_gshared (ReadOnlyCollection_1_t1_1750 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1_16319(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t1_1750 *, Array_t *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1_16319_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1_16320_gshared (ReadOnlyCollection_1_t1_1750 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1_16320(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t1_1750 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1_16320_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Add(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m1_16321_gshared (ReadOnlyCollection_1_t1_1750 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m1_16321(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t1_1750 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m1_16321_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m1_16322_gshared (ReadOnlyCollection_1_t1_1750 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m1_16322(__this, method) (( void (*) (ReadOnlyCollection_1_t1_1750 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m1_16322_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Contains(System.Object)
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_Contains_m1_16323_gshared (ReadOnlyCollection_1_t1_1750 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m1_16323(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t1_1750 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m1_16323_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1_16324_gshared (ReadOnlyCollection_1_t1_1750 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1_16324(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t1_1750 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1_16324_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m1_16325_gshared (ReadOnlyCollection_1_t1_1750 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m1_16325(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t1_1750 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m1_16325_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Remove(System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m1_16326_gshared (ReadOnlyCollection_1_t1_1750 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m1_16326(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t1_1750 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m1_16326_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1_16327_gshared (ReadOnlyCollection_1_t1_1750 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1_16327(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t1_1750 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1_16327_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1_16328_gshared (ReadOnlyCollection_1_t1_1750 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1_16328(__this, method) (( bool (*) (ReadOnlyCollection_1_t1_1750 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1_16328_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1_16329_gshared (ReadOnlyCollection_1_t1_1750 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1_16329(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t1_1750 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1_16329_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.get_IsFixedSize()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1_16330_gshared (ReadOnlyCollection_1_t1_1750 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1_16330(__this, method) (( bool (*) (ReadOnlyCollection_1_t1_1750 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1_16330_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1_16331_gshared (ReadOnlyCollection_1_t1_1750 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1_16331(__this, method) (( bool (*) (ReadOnlyCollection_1_t1_1750 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1_16331_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IList_get_Item_m1_16332_gshared (ReadOnlyCollection_1_t1_1750 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m1_16332(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t1_1750 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m1_16332_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m1_16333_gshared (ReadOnlyCollection_1_t1_1750 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m1_16333(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t1_1750 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m1_16333_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::Contains(T)
extern "C" bool ReadOnlyCollection_1_Contains_m1_16334_gshared (ReadOnlyCollection_1_t1_1750 * __this, CustomAttributeTypedArgument_t1_594  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m1_16334(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t1_1750 *, CustomAttributeTypedArgument_t1_594 , const MethodInfo*))ReadOnlyCollection_1_Contains_m1_16334_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::CopyTo(T[],System.Int32)
extern "C" void ReadOnlyCollection_1_CopyTo_m1_16335_gshared (ReadOnlyCollection_1_t1_1750 * __this, CustomAttributeTypedArgumentU5BU5D_t1_1748* ___array, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m1_16335(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t1_1750 *, CustomAttributeTypedArgumentU5BU5D_t1_1748*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m1_16335_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::GetEnumerator()
extern "C" Object_t* ReadOnlyCollection_1_GetEnumerator_m1_16336_gshared (ReadOnlyCollection_1_t1_1750 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m1_16336(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t1_1750 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m1_16336_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T)
extern "C" int32_t ReadOnlyCollection_1_IndexOf_m1_16337_gshared (ReadOnlyCollection_1_t1_1750 * __this, CustomAttributeTypedArgument_t1_594  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m1_16337(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t1_1750 *, CustomAttributeTypedArgument_t1_594 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m1_16337_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::get_Count()
extern "C" int32_t ReadOnlyCollection_1_get_Count_m1_16338_gshared (ReadOnlyCollection_1_t1_1750 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m1_16338(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t1_1750 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m1_16338_gshared)(__this, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::get_Items()
extern "C" Object_t* ReadOnlyCollection_1_get_Items_m1_16339_gshared (ReadOnlyCollection_1_t1_1750 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Items_m1_16339(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t1_1750 *, const MethodInfo*))ReadOnlyCollection_1_get_Items_m1_16339_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::get_Item(System.Int32)
extern "C" CustomAttributeTypedArgument_t1_594  ReadOnlyCollection_1_get_Item_m1_16340_gshared (ReadOnlyCollection_1_t1_1750 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m1_16340(__this, ___index, method) (( CustomAttributeTypedArgument_t1_594  (*) (ReadOnlyCollection_1_t1_1750 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m1_16340_gshared)(__this, ___index, method)
