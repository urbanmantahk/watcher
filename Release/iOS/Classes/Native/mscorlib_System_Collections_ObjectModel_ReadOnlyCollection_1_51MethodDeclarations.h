﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCollection_1_4MethodDeclarations.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::.ctor(System.Collections.Generic.IList`1<T>)
#define ReadOnlyCollection_1__ctor_m1_26001(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t1_2639 *, Object_t*, const MethodInfo*))ReadOnlyCollection_1__ctor_m1_15265_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1_26002(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t1_2639 *, ShaderInfo_t8_26 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1_15266_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1_26003(__this, method) (( void (*) (ReadOnlyCollection_1_t1_2639 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1_15267_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1_26004(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t1_2639 *, int32_t, ShaderInfo_t8_26 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1_15268_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1_26005(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t1_2639 *, ShaderInfo_t8_26 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1_15269_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1_26006(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t1_2639 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1_15270_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1_26007(__this, ___index, method) (( ShaderInfo_t8_26 * (*) (ReadOnlyCollection_1_t1_2639 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1_15271_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1_26008(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t1_2639 *, int32_t, ShaderInfo_t8_26 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1_15272_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_26009(__this, method) (( bool (*) (ReadOnlyCollection_1_t1_2639 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_15273_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1_26010(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t1_2639 *, Array_t *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1_15274_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1_26011(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t1_2639 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1_15275_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m1_26012(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t1_2639 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m1_15276_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m1_26013(__this, method) (( void (*) (ReadOnlyCollection_1_t1_2639 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m1_15277_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m1_26014(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t1_2639 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m1_15278_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1_26015(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t1_2639 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1_15279_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m1_26016(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t1_2639 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m1_15280_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m1_26017(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t1_2639 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m1_15281_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1_26018(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t1_2639 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1_15282_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1_26019(__this, method) (( bool (*) (ReadOnlyCollection_1_t1_2639 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1_15283_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1_26020(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t1_2639 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1_15284_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1_26021(__this, method) (( bool (*) (ReadOnlyCollection_1_t1_2639 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1_15285_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1_26022(__this, method) (( bool (*) (ReadOnlyCollection_1_t1_2639 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1_15286_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m1_26023(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t1_2639 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m1_15287_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m1_26024(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t1_2639 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m1_15288_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::Contains(T)
#define ReadOnlyCollection_1_Contains_m1_26025(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t1_2639 *, ShaderInfo_t8_26 *, const MethodInfo*))ReadOnlyCollection_1_Contains_m1_15289_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m1_26026(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t1_2639 *, ShaderInfoU5BU5D_t8_376*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m1_15290_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m1_26027(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t1_2639 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m1_15291_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m1_26028(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t1_2639 *, ShaderInfo_t8_26 *, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m1_15292_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::get_Count()
#define ReadOnlyCollection_1_get_Count_m1_26029(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t1_2639 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m1_15293_gshared)(__this, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::get_Items()
#define ReadOnlyCollection_1_get_Items_m1_26030(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t1_2639 *, const MethodInfo*))ReadOnlyCollection_1_get_Items_m1_15294_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m1_26031(__this, ___index, method) (( ShaderInfo_t8_26 * (*) (ReadOnlyCollection_1_t1_2639 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m1_15295_gshared)(__this, ___index, method)
