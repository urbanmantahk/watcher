﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_23MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m1_26538(__this, ___dictionary, method) (( void (*) (KeyCollection_t1_2679 *, Dictionary_2_t1_1904 *, const MethodInfo*))KeyCollection__ctor_m1_26444_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1_26539(__this, ___item, method) (( void (*) (KeyCollection_t1_2679 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1_26445_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1_26540(__this, method) (( void (*) (KeyCollection_t1_2679 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1_26446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1_26541(__this, ___item, method) (( bool (*) (KeyCollection_t1_2679 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1_26447_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1_26542(__this, ___item, method) (( bool (*) (KeyCollection_t1_2679 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1_26448_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1_26543(__this, method) (( Object_t* (*) (KeyCollection_t1_2679 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1_26449_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m1_26544(__this, ___array, ___index, method) (( void (*) (KeyCollection_t1_2679 *, Array_t *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m1_26450_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1_26545(__this, method) (( Object_t * (*) (KeyCollection_t1_2679 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1_26451_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1_26546(__this, method) (( bool (*) (KeyCollection_t1_2679 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1_26452_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1_26547(__this, method) (( bool (*) (KeyCollection_t1_2679 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1_26453_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m1_26548(__this, method) (( Object_t * (*) (KeyCollection_t1_2679 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m1_26454_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m1_26549(__this, ___array, ___index, method) (( void (*) (KeyCollection_t1_2679 *, ExifTagU5BU5D_t8_379*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m1_26455_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::GetEnumerator()
#define KeyCollection_GetEnumerator_m1_26550(__this, method) (( Enumerator_t1_2879  (*) (KeyCollection_t1_2679 *, const MethodInfo*))KeyCollection_GetEnumerator_m1_26456_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::get_Count()
#define KeyCollection_get_Count_m1_26551(__this, method) (( int32_t (*) (KeyCollection_t1_2679 *, const MethodInfo*))KeyCollection_get_Count_m1_26457_gshared)(__this, method)
