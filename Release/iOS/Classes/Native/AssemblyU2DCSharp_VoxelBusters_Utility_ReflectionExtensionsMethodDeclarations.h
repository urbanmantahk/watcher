﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Type[]
struct TypeU5BU5D_t1_31;
// System.Reflection.FieldInfo
struct FieldInfo_t;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.Utility.ReflectionExtensions::SetFieldValue(System.Object,System.String,System.Object)
extern "C" void ReflectionExtensions_SetFieldValue_m8_215 (Object_t * __this /* static, unused */, Object_t * ____object, String_t* ____name, Object_t * ____value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.ReflectionExtensions::SetFieldValue(System.Object,System.Type,System.String,System.Object)
extern "C" void ReflectionExtensions_SetFieldValue_m8_216 (Object_t * __this /* static, unused */, Object_t * ____object, Type_t * ____objectType, String_t* ____name, Object_t * ____value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VoxelBusters.Utility.ReflectionExtensions::GetStaticValue(System.Type,System.String)
extern "C" Object_t * ReflectionExtensions_GetStaticValue_m8_217 (Object_t * __this /* static, unused */, Type_t * ____objectType, String_t* ____name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.ReflectionExtensions::SetStaticValue(System.Type,System.String,System.Object)
extern "C" void ReflectionExtensions_SetStaticValue_m8_218 (Object_t * __this /* static, unused */, Type_t * ____objectType, String_t* ____name, Object_t * ____value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.ReflectionExtensions::InvokeMethod(System.Object,System.String,System.Object)
extern "C" void ReflectionExtensions_InvokeMethod_m8_219 (Object_t * __this /* static, unused */, Object_t * ____object, String_t* ____method, Object_t * ____value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.ReflectionExtensions::InvokeMethod(System.Object,System.String,System.Object[],System.Type[])
extern "C" void ReflectionExtensions_InvokeMethod_m8_220 (Object_t * __this /* static, unused */, Object_t * ____object, String_t* ____method, ObjectU5BU5D_t1_272* ____argValues, TypeU5BU5D_t1_31* ____argTypes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.ReflectionExtensions::InvokeMethod(System.Object,System.Type,System.String,System.Object[],System.Type[])
extern "C" void ReflectionExtensions_InvokeMethod_m8_221 (Object_t * __this /* static, unused */, Object_t * ____object, Type_t * ____objectType, String_t* ____method, ObjectU5BU5D_t1_272* ____argValues, TypeU5BU5D_t1_31* ____argTypes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.ReflectionExtensions::InvokeStaticMethod(System.Type,System.String,System.Object[],System.Type[])
extern "C" void ReflectionExtensions_InvokeStaticMethod_m8_222 (Object_t * __this /* static, unused */, Type_t * ____objectType, String_t* ____method, ObjectU5BU5D_t1_272* ____argValues, TypeU5BU5D_t1_31* ____argTypes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.FieldInfo VoxelBusters.Utility.ReflectionExtensions::GetFieldWithName(System.Type,System.String,System.Boolean)
extern "C" FieldInfo_t * ReflectionExtensions_GetFieldWithName_m8_223 (Object_t * __this /* static, unused */, Type_t * ____type, String_t* ____name, bool ____isPublic, const MethodInfo* method) IL2CPP_METHOD_ATTR;
