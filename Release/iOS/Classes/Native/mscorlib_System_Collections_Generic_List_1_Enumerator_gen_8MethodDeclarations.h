﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Object>
struct List_1_t1_1894;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_8.h"

// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m1_15258_gshared (Enumerator_t1_1936 * __this, List_1_t1_1894 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m1_15258(__this, ___l, method) (( void (*) (Enumerator_t1_1936 *, List_1_t1_1894 *, const MethodInfo*))Enumerator__ctor_m1_15258_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m1_15259_gshared (Enumerator_t1_1936 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1_15259(__this, method) (( void (*) (Enumerator_t1_1936 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_15259_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m1_15260_gshared (Enumerator_t1_1936 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_15260(__this, method) (( Object_t * (*) (Enumerator_t1_1936 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_15260_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m1_15261_gshared (Enumerator_t1_1936 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1_15261(__this, method) (( void (*) (Enumerator_t1_1936 *, const MethodInfo*))Enumerator_Dispose_m1_15261_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::VerifyState()
extern "C" void Enumerator_VerifyState_m1_15262_gshared (Enumerator_t1_1936 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m1_15262(__this, method) (( void (*) (Enumerator_t1_1936 *, const MethodInfo*))Enumerator_VerifyState_m1_15262_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m1_15263_gshared (Enumerator_t1_1936 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1_15263(__this, method) (( bool (*) (Enumerator_t1_1936 *, const MethodInfo*))Enumerator_MoveNext_m1_15263_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m1_15264_gshared (Enumerator_t1_1936 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1_15264(__this, method) (( Object_t * (*) (Enumerator_t1_1936 *, const MethodInfo*))Enumerator_get_Current_m1_15264_gshared)(__this, method)
