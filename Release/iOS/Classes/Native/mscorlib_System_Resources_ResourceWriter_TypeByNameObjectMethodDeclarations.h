﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Resources.ResourceWriter/TypeByNameObject
struct TypeByNameObject_t1_657;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Resources.ResourceWriter/TypeByNameObject::.ctor(System.String,System.Byte[])
extern "C" void TypeByNameObject__ctor_m1_7442 (TypeByNameObject_t1_657 * __this, String_t* ___typeName, ByteU5BU5D_t1_109* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
