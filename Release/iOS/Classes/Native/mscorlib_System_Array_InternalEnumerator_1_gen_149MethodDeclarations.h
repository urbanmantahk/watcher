﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_149.h"
#include "UnityEngine_UnityEngine_Vector2.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector2>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_18752_gshared (InternalEnumerator_1_t1_2266 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1_18752(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1_2266 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m1_18752_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector2>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_18753_gshared (InternalEnumerator_1_t1_2266 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_18753(__this, method) (( void (*) (InternalEnumerator_1_t1_2266 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_18753_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Vector2>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_18754_gshared (InternalEnumerator_1_t1_2266 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_18754(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1_2266 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_18754_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector2>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_18755_gshared (InternalEnumerator_1_t1_2266 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1_18755(__this, method) (( void (*) (InternalEnumerator_1_t1_2266 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1_18755_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Vector2>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_18756_gshared (InternalEnumerator_1_t1_2266 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1_18756(__this, method) (( bool (*) (InternalEnumerator_1_t1_2266 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1_18756_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Vector2>::get_Current()
extern "C" Vector2_t6_47  InternalEnumerator_1_get_Current_m1_18757_gshared (InternalEnumerator_1_t1_2266 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1_18757(__this, method) (( Vector2_t6_47  (*) (InternalEnumerator_1_t1_2266 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1_18757_gshared)(__this, method)
