﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaDerivationMethod.h"

// System.Xml.Schema.XmlSchemaUtil
struct  XmlSchemaUtil_t4_74  : public Object_t
{
};
struct XmlSchemaUtil_t4_74_StaticFields{
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchemaUtil::FinalAllowed
	int32_t ___FinalAllowed_0;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchemaUtil::ElementBlockAllowed
	int32_t ___ElementBlockAllowed_1;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchemaUtil::ComplexTypeBlockAllowed
	int32_t ___ComplexTypeBlockAllowed_2;
	// System.Boolean System.Xml.Schema.XmlSchemaUtil::StrictMsCompliant
	bool ___StrictMsCompliant_3;
};
