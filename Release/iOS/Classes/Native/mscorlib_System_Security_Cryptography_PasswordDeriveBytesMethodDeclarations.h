﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.PasswordDeriveBytes
struct PasswordDeriveBytes_t1_1227;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Security.Cryptography.CspParameters
struct CspParameters_t1_164;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Cryptography.PasswordDeriveBytes::.ctor(System.String,System.Byte[])
extern "C" void PasswordDeriveBytes__ctor_m1_10436 (PasswordDeriveBytes_t1_1227 * __this, String_t* ___strPassword, ByteU5BU5D_t1_109* ___rgbSalt, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.PasswordDeriveBytes::.ctor(System.String,System.Byte[],System.Security.Cryptography.CspParameters)
extern "C" void PasswordDeriveBytes__ctor_m1_10437 (PasswordDeriveBytes_t1_1227 * __this, String_t* ___strPassword, ByteU5BU5D_t1_109* ___rgbSalt, CspParameters_t1_164 * ___cspParams, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.PasswordDeriveBytes::.ctor(System.String,System.Byte[],System.String,System.Int32)
extern "C" void PasswordDeriveBytes__ctor_m1_10438 (PasswordDeriveBytes_t1_1227 * __this, String_t* ___strPassword, ByteU5BU5D_t1_109* ___rgbSalt, String_t* ___strHashName, int32_t ___iterations, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.PasswordDeriveBytes::.ctor(System.String,System.Byte[],System.String,System.Int32,System.Security.Cryptography.CspParameters)
extern "C" void PasswordDeriveBytes__ctor_m1_10439 (PasswordDeriveBytes_t1_1227 * __this, String_t* ___strPassword, ByteU5BU5D_t1_109* ___rgbSalt, String_t* ___strHashName, int32_t ___iterations, CspParameters_t1_164 * ___cspParams, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.PasswordDeriveBytes::.ctor(System.Byte[],System.Byte[])
extern "C" void PasswordDeriveBytes__ctor_m1_10440 (PasswordDeriveBytes_t1_1227 * __this, ByteU5BU5D_t1_109* ___password, ByteU5BU5D_t1_109* ___salt, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.PasswordDeriveBytes::.ctor(System.Byte[],System.Byte[],System.Security.Cryptography.CspParameters)
extern "C" void PasswordDeriveBytes__ctor_m1_10441 (PasswordDeriveBytes_t1_1227 * __this, ByteU5BU5D_t1_109* ___password, ByteU5BU5D_t1_109* ___salt, CspParameters_t1_164 * ___cspParams, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.PasswordDeriveBytes::.ctor(System.Byte[],System.Byte[],System.String,System.Int32)
extern "C" void PasswordDeriveBytes__ctor_m1_10442 (PasswordDeriveBytes_t1_1227 * __this, ByteU5BU5D_t1_109* ___password, ByteU5BU5D_t1_109* ___salt, String_t* ___hashName, int32_t ___iterations, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.PasswordDeriveBytes::.ctor(System.Byte[],System.Byte[],System.String,System.Int32,System.Security.Cryptography.CspParameters)
extern "C" void PasswordDeriveBytes__ctor_m1_10443 (PasswordDeriveBytes_t1_1227 * __this, ByteU5BU5D_t1_109* ___password, ByteU5BU5D_t1_109* ___salt, String_t* ___hashName, int32_t ___iterations, CspParameters_t1_164 * ___cspParams, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.PasswordDeriveBytes::Finalize()
extern "C" void PasswordDeriveBytes_Finalize_m1_10444 (PasswordDeriveBytes_t1_1227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.PasswordDeriveBytes::Prepare(System.String,System.Byte[],System.String,System.Int32)
extern "C" void PasswordDeriveBytes_Prepare_m1_10445 (PasswordDeriveBytes_t1_1227 * __this, String_t* ___strPassword, ByteU5BU5D_t1_109* ___rgbSalt, String_t* ___strHashName, int32_t ___iterations, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.PasswordDeriveBytes::Prepare(System.Byte[],System.Byte[],System.String,System.Int32)
extern "C" void PasswordDeriveBytes_Prepare_m1_10446 (PasswordDeriveBytes_t1_1227 * __this, ByteU5BU5D_t1_109* ___password, ByteU5BU5D_t1_109* ___rgbSalt, String_t* ___strHashName, int32_t ___iterations, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Cryptography.PasswordDeriveBytes::get_HashName()
extern "C" String_t* PasswordDeriveBytes_get_HashName_m1_10447 (PasswordDeriveBytes_t1_1227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.PasswordDeriveBytes::set_HashName(System.String)
extern "C" void PasswordDeriveBytes_set_HashName_m1_10448 (PasswordDeriveBytes_t1_1227 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Cryptography.PasswordDeriveBytes::get_IterationCount()
extern "C" int32_t PasswordDeriveBytes_get_IterationCount_m1_10449 (PasswordDeriveBytes_t1_1227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.PasswordDeriveBytes::set_IterationCount(System.Int32)
extern "C" void PasswordDeriveBytes_set_IterationCount_m1_10450 (PasswordDeriveBytes_t1_1227 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.PasswordDeriveBytes::get_Salt()
extern "C" ByteU5BU5D_t1_109* PasswordDeriveBytes_get_Salt_m1_10451 (PasswordDeriveBytes_t1_1227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.PasswordDeriveBytes::set_Salt(System.Byte[])
extern "C" void PasswordDeriveBytes_set_Salt_m1_10452 (PasswordDeriveBytes_t1_1227 * __this, ByteU5BU5D_t1_109* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.PasswordDeriveBytes::CryptDeriveKey(System.String,System.String,System.Int32,System.Byte[])
extern "C" ByteU5BU5D_t1_109* PasswordDeriveBytes_CryptDeriveKey_m1_10453 (PasswordDeriveBytes_t1_1227 * __this, String_t* ___algname, String_t* ___alghashname, int32_t ___keySize, ByteU5BU5D_t1_109* ___rgbIV, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.PasswordDeriveBytes::GetBytes(System.Int32)
extern "C" ByteU5BU5D_t1_109* PasswordDeriveBytes_GetBytes_m1_10454 (PasswordDeriveBytes_t1_1227 * __this, int32_t ___cb, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.PasswordDeriveBytes::Reset()
extern "C" void PasswordDeriveBytes_Reset_m1_10455 (PasswordDeriveBytes_t1_1227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
