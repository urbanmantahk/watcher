﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Runtime.Remoting.Proxies.RealProxy
struct RealProxy_t1_130;

#include "mscorlib_System_Object.h"
#include "mscorlib_System_IntPtr.h"

// System.Runtime.Remoting.Proxies.TransparentProxy
struct  TransparentProxy_t1_1005  : public Object_t
{
	// System.Runtime.Remoting.Proxies.RealProxy System.Runtime.Remoting.Proxies.TransparentProxy::_rp
	RealProxy_t1_130 * ____rp_0;
	// System.IntPtr System.Runtime.Remoting.Proxies.TransparentProxy::_class
	IntPtr_t ____class_1;
	// System.Boolean System.Runtime.Remoting.Proxies.TransparentProxy::_custom_type_info
	bool ____custom_type_info_2;
};
