﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Queue`1<System.Object>
struct Queue_1_t3_253;
// System.Array
struct Array_t;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1_2210;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Object[]
struct ObjectU5BU5D_t1_272;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Queue_1_Enumerator_gen.h"

// System.Void System.Collections.Generic.Queue`1<System.Object>::.ctor()
extern "C" void Queue_1__ctor_m3_1848_gshared (Queue_1_t3_253 * __this, const MethodInfo* method);
#define Queue_1__ctor_m3_1848(__this, method) (( void (*) (Queue_1_t3_253 *, const MethodInfo*))Queue_1__ctor_m3_1848_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Queue_1_System_Collections_ICollection_CopyTo_m3_1849_gshared (Queue_1_t3_253 * __this, Array_t * ___array, int32_t ___idx, const MethodInfo* method);
#define Queue_1_System_Collections_ICollection_CopyTo_m3_1849(__this, ___array, ___idx, method) (( void (*) (Queue_1_t3_253 *, Array_t *, int32_t, const MethodInfo*))Queue_1_System_Collections_ICollection_CopyTo_m3_1849_gshared)(__this, ___array, ___idx, method)
// System.Boolean System.Collections.Generic.Queue`1<System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Queue_1_System_Collections_ICollection_get_IsSynchronized_m3_1850_gshared (Queue_1_t3_253 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_ICollection_get_IsSynchronized_m3_1850(__this, method) (( bool (*) (Queue_1_t3_253 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_IsSynchronized_m3_1850_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Queue_1_System_Collections_ICollection_get_SyncRoot_m3_1851_gshared (Queue_1_t3_253 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_ICollection_get_SyncRoot_m3_1851(__this, method) (( Object_t * (*) (Queue_1_t3_253 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_SyncRoot_m3_1851_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Queue`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3_1852_gshared (Queue_1_t3_253 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3_1852(__this, method) (( Object_t* (*) (Queue_1_t3_253 *, const MethodInfo*))Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3_1852_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Queue`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Queue_1_System_Collections_IEnumerable_GetEnumerator_m3_1853_gshared (Queue_1_t3_253 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_IEnumerable_GetEnumerator_m3_1853(__this, method) (( Object_t * (*) (Queue_1_t3_253 *, const MethodInfo*))Queue_1_System_Collections_IEnumerable_GetEnumerator_m3_1853_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void Queue_1_CopyTo_m3_1854_gshared (Queue_1_t3_253 * __this, ObjectU5BU5D_t1_272* ___array, int32_t ___idx, const MethodInfo* method);
#define Queue_1_CopyTo_m3_1854(__this, ___array, ___idx, method) (( void (*) (Queue_1_t3_253 *, ObjectU5BU5D_t1_272*, int32_t, const MethodInfo*))Queue_1_CopyTo_m3_1854_gshared)(__this, ___array, ___idx, method)
// T System.Collections.Generic.Queue`1<System.Object>::Dequeue()
extern "C" Object_t * Queue_1_Dequeue_m3_1855_gshared (Queue_1_t3_253 * __this, const MethodInfo* method);
#define Queue_1_Dequeue_m3_1855(__this, method) (( Object_t * (*) (Queue_1_t3_253 *, const MethodInfo*))Queue_1_Dequeue_m3_1855_gshared)(__this, method)
// T System.Collections.Generic.Queue`1<System.Object>::Peek()
extern "C" Object_t * Queue_1_Peek_m3_1856_gshared (Queue_1_t3_253 * __this, const MethodInfo* method);
#define Queue_1_Peek_m3_1856(__this, method) (( Object_t * (*) (Queue_1_t3_253 *, const MethodInfo*))Queue_1_Peek_m3_1856_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<System.Object>::Enqueue(T)
extern "C" void Queue_1_Enqueue_m3_1857_gshared (Queue_1_t3_253 * __this, Object_t * ___item, const MethodInfo* method);
#define Queue_1_Enqueue_m3_1857(__this, ___item, method) (( void (*) (Queue_1_t3_253 *, Object_t *, const MethodInfo*))Queue_1_Enqueue_m3_1857_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Queue`1<System.Object>::SetCapacity(System.Int32)
extern "C" void Queue_1_SetCapacity_m3_1858_gshared (Queue_1_t3_253 * __this, int32_t ___new_size, const MethodInfo* method);
#define Queue_1_SetCapacity_m3_1858(__this, ___new_size, method) (( void (*) (Queue_1_t3_253 *, int32_t, const MethodInfo*))Queue_1_SetCapacity_m3_1858_gshared)(__this, ___new_size, method)
// System.Int32 System.Collections.Generic.Queue`1<System.Object>::get_Count()
extern "C" int32_t Queue_1_get_Count_m3_1859_gshared (Queue_1_t3_253 * __this, const MethodInfo* method);
#define Queue_1_get_Count_m3_1859(__this, method) (( int32_t (*) (Queue_1_t3_253 *, const MethodInfo*))Queue_1_get_Count_m3_1859_gshared)(__this, method)
// System.Collections.Generic.Queue`1/Enumerator<T> System.Collections.Generic.Queue`1<System.Object>::GetEnumerator()
extern "C" Enumerator_t3_254  Queue_1_GetEnumerator_m3_1860_gshared (Queue_1_t3_253 * __this, const MethodInfo* method);
#define Queue_1_GetEnumerator_m3_1860(__this, method) (( Enumerator_t3_254  (*) (Queue_1_t3_253 *, const MethodInfo*))Queue_1_GetEnumerator_m3_1860_gshared)(__this, method)
