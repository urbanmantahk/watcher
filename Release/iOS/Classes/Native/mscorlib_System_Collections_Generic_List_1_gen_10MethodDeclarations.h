﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t1_1837;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Color32>
struct IEnumerable_1_t1_2806;
// UnityEngine.Color32[]
struct Color32U5BU5D_t6_280;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Color32>
struct IEnumerator_1_t1_2807;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<UnityEngine.Color32>
struct ICollection_1_t1_2808;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>
struct ReadOnlyCollection_1_t1_2302;
// System.Collections.Generic.IComparer`1<UnityEngine.Color32>
struct IComparer_1_t1_2809;
// System.Predicate`1<UnityEngine.Color32>
struct Predicate_1_t1_2309;
// System.Action`1<UnityEngine.Color32>
struct Action_1_t1_2310;
// System.Comparison`1<UnityEngine.Color32>
struct Comparison_1_t1_2311;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color32.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_18.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::.ctor()
extern "C" void List_1__ctor_m1_19319_gshared (List_1_t1_1837 * __this, const MethodInfo* method);
#define List_1__ctor_m1_19319(__this, method) (( void (*) (List_1_t1_1837 *, const MethodInfo*))List_1__ctor_m1_19319_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m1_19320_gshared (List_1_t1_1837 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m1_19320(__this, ___collection, method) (( void (*) (List_1_t1_1837 *, Object_t*, const MethodInfo*))List_1__ctor_m1_19320_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::.ctor(System.Int32)
extern "C" void List_1__ctor_m1_19321_gshared (List_1_t1_1837 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m1_19321(__this, ___capacity, method) (( void (*) (List_1_t1_1837 *, int32_t, const MethodInfo*))List_1__ctor_m1_19321_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::.ctor(T[],System.Int32)
extern "C" void List_1__ctor_m1_19322_gshared (List_1_t1_1837 * __this, Color32U5BU5D_t6_280* ___data, int32_t ___size, const MethodInfo* method);
#define List_1__ctor_m1_19322(__this, ___data, ___size, method) (( void (*) (List_1_t1_1837 *, Color32U5BU5D_t6_280*, int32_t, const MethodInfo*))List_1__ctor_m1_19322_gshared)(__this, ___data, ___size, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::.cctor()
extern "C" void List_1__cctor_m1_19323_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m1_19323(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m1_19323_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_19324_gshared (List_1_t1_1837 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_19324(__this, method) (( Object_t* (*) (List_1_t1_1837 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_19324_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m1_19325_gshared (List_1_t1_1837 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m1_19325(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_1837 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m1_19325_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m1_19326_gshared (List_1_t1_1837 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1_19326(__this, method) (( Object_t * (*) (List_1_t1_1837 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1_19326_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m1_19327_gshared (List_1_t1_1837 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m1_19327(__this, ___item, method) (( int32_t (*) (List_1_t1_1837 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m1_19327_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m1_19328_gshared (List_1_t1_1837 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m1_19328(__this, ___item, method) (( bool (*) (List_1_t1_1837 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1_19328_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m1_19329_gshared (List_1_t1_1837 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m1_19329(__this, ___item, method) (( int32_t (*) (List_1_t1_1837 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m1_19329_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m1_19330_gshared (List_1_t1_1837 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m1_19330(__this, ___index, ___item, method) (( void (*) (List_1_t1_1837 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m1_19330_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m1_19331_gshared (List_1_t1_1837 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m1_19331(__this, ___item, method) (( void (*) (List_1_t1_1837 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1_19331_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_19332_gshared (List_1_t1_1837 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_19332(__this, method) (( bool (*) (List_1_t1_1837 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_19332_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m1_19333_gshared (List_1_t1_1837 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m1_19333(__this, method) (( bool (*) (List_1_t1_1837 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m1_19333_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m1_19334_gshared (List_1_t1_1837 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m1_19334(__this, method) (( Object_t * (*) (List_1_t1_1837 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1_19334_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m1_19335_gshared (List_1_t1_1837 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m1_19335(__this, method) (( bool (*) (List_1_t1_1837 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m1_19335_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m1_19336_gshared (List_1_t1_1837 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m1_19336(__this, method) (( bool (*) (List_1_t1_1837 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m1_19336_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m1_19337_gshared (List_1_t1_1837 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m1_19337(__this, ___index, method) (( Object_t * (*) (List_1_t1_1837 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m1_19337_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m1_19338_gshared (List_1_t1_1837 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m1_19338(__this, ___index, ___value, method) (( void (*) (List_1_t1_1837 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m1_19338_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::Add(T)
extern "C" void List_1_Add_m1_19339_gshared (List_1_t1_1837 * __this, Color32_t6_49  ___item, const MethodInfo* method);
#define List_1_Add_m1_19339(__this, ___item, method) (( void (*) (List_1_t1_1837 *, Color32_t6_49 , const MethodInfo*))List_1_Add_m1_19339_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m1_19340_gshared (List_1_t1_1837 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m1_19340(__this, ___newCount, method) (( void (*) (List_1_t1_1837 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m1_19340_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::CheckRange(System.Int32,System.Int32)
extern "C" void List_1_CheckRange_m1_19341_gshared (List_1_t1_1837 * __this, int32_t ___idx, int32_t ___count, const MethodInfo* method);
#define List_1_CheckRange_m1_19341(__this, ___idx, ___count, method) (( void (*) (List_1_t1_1837 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m1_19341_gshared)(__this, ___idx, ___count, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m1_19342_gshared (List_1_t1_1837 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m1_19342(__this, ___collection, method) (( void (*) (List_1_t1_1837 *, Object_t*, const MethodInfo*))List_1_AddCollection_m1_19342_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m1_19343_gshared (List_1_t1_1837 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m1_19343(__this, ___enumerable, method) (( void (*) (List_1_t1_1837 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m1_19343_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m1_15020_gshared (List_1_t1_1837 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m1_15020(__this, ___collection, method) (( void (*) (List_1_t1_1837 *, Object_t*, const MethodInfo*))List_1_AddRange_m1_15020_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.Color32>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t1_2302 * List_1_AsReadOnly_m1_19344_gshared (List_1_t1_1837 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m1_19344(__this, method) (( ReadOnlyCollection_1_t1_2302 * (*) (List_1_t1_1837 *, const MethodInfo*))List_1_AsReadOnly_m1_19344_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Color32>::BinarySearch(T)
extern "C" int32_t List_1_BinarySearch_m1_19345_gshared (List_1_t1_1837 * __this, Color32_t6_49  ___item, const MethodInfo* method);
#define List_1_BinarySearch_m1_19345(__this, ___item, method) (( int32_t (*) (List_1_t1_1837 *, Color32_t6_49 , const MethodInfo*))List_1_BinarySearch_m1_19345_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Color32>::BinarySearch(T,System.Collections.Generic.IComparer`1<T>)
extern "C" int32_t List_1_BinarySearch_m1_19346_gshared (List_1_t1_1837 * __this, Color32_t6_49  ___item, Object_t* ___comparer, const MethodInfo* method);
#define List_1_BinarySearch_m1_19346(__this, ___item, ___comparer, method) (( int32_t (*) (List_1_t1_1837 *, Color32_t6_49 , Object_t*, const MethodInfo*))List_1_BinarySearch_m1_19346_gshared)(__this, ___item, ___comparer, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Color32>::BinarySearch(System.Int32,System.Int32,T,System.Collections.Generic.IComparer`1<T>)
extern "C" int32_t List_1_BinarySearch_m1_19347_gshared (List_1_t1_1837 * __this, int32_t ___index, int32_t ___count, Color32_t6_49  ___item, Object_t* ___comparer, const MethodInfo* method);
#define List_1_BinarySearch_m1_19347(__this, ___index, ___count, ___item, ___comparer, method) (( int32_t (*) (List_1_t1_1837 *, int32_t, int32_t, Color32_t6_49 , Object_t*, const MethodInfo*))List_1_BinarySearch_m1_19347_gshared)(__this, ___index, ___count, ___item, ___comparer, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::Clear()
extern "C" void List_1_Clear_m1_19348_gshared (List_1_t1_1837 * __this, const MethodInfo* method);
#define List_1_Clear_m1_19348(__this, method) (( void (*) (List_1_t1_1837 *, const MethodInfo*))List_1_Clear_m1_19348_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Color32>::Contains(T)
extern "C" bool List_1_Contains_m1_19349_gshared (List_1_t1_1837 * __this, Color32_t6_49  ___item, const MethodInfo* method);
#define List_1_Contains_m1_19349(__this, ___item, method) (( bool (*) (List_1_t1_1837 *, Color32_t6_49 , const MethodInfo*))List_1_Contains_m1_19349_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::CopyTo(T[])
extern "C" void List_1_CopyTo_m1_19350_gshared (List_1_t1_1837 * __this, Color32U5BU5D_t6_280* ___array, const MethodInfo* method);
#define List_1_CopyTo_m1_19350(__this, ___array, method) (( void (*) (List_1_t1_1837 *, Color32U5BU5D_t6_280*, const MethodInfo*))List_1_CopyTo_m1_19350_gshared)(__this, ___array, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m1_19351_gshared (List_1_t1_1837 * __this, Color32U5BU5D_t6_280* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m1_19351(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_1837 *, Color32U5BU5D_t6_280*, int32_t, const MethodInfo*))List_1_CopyTo_m1_19351_gshared)(__this, ___array, ___arrayIndex, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::CopyTo(System.Int32,T[],System.Int32,System.Int32)
extern "C" void List_1_CopyTo_m1_19352_gshared (List_1_t1_1837 * __this, int32_t ___index, Color32U5BU5D_t6_280* ___array, int32_t ___arrayIndex, int32_t ___count, const MethodInfo* method);
#define List_1_CopyTo_m1_19352(__this, ___index, ___array, ___arrayIndex, ___count, method) (( void (*) (List_1_t1_1837 *, int32_t, Color32U5BU5D_t6_280*, int32_t, int32_t, const MethodInfo*))List_1_CopyTo_m1_19352_gshared)(__this, ___index, ___array, ___arrayIndex, ___count, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Color32>::Exists(System.Predicate`1<T>)
extern "C" bool List_1_Exists_m1_19353_gshared (List_1_t1_1837 * __this, Predicate_1_t1_2309 * ___match, const MethodInfo* method);
#define List_1_Exists_m1_19353(__this, ___match, method) (( bool (*) (List_1_t1_1837 *, Predicate_1_t1_2309 *, const MethodInfo*))List_1_Exists_m1_19353_gshared)(__this, ___match, method)
// T System.Collections.Generic.List`1<UnityEngine.Color32>::Find(System.Predicate`1<T>)
extern "C" Color32_t6_49  List_1_Find_m1_19354_gshared (List_1_t1_1837 * __this, Predicate_1_t1_2309 * ___match, const MethodInfo* method);
#define List_1_Find_m1_19354(__this, ___match, method) (( Color32_t6_49  (*) (List_1_t1_1837 *, Predicate_1_t1_2309 *, const MethodInfo*))List_1_Find_m1_19354_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m1_19355_gshared (Object_t * __this /* static, unused */, Predicate_1_t1_2309 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m1_19355(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t1_2309 *, const MethodInfo*))List_1_CheckMatch_m1_19355_gshared)(__this /* static, unused */, ___match, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<UnityEngine.Color32>::FindAll(System.Predicate`1<T>)
extern "C" List_1_t1_1837 * List_1_FindAll_m1_19356_gshared (List_1_t1_1837 * __this, Predicate_1_t1_2309 * ___match, const MethodInfo* method);
#define List_1_FindAll_m1_19356(__this, ___match, method) (( List_1_t1_1837 * (*) (List_1_t1_1837 *, Predicate_1_t1_2309 *, const MethodInfo*))List_1_FindAll_m1_19356_gshared)(__this, ___match, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<UnityEngine.Color32>::FindAllStackBits(System.Predicate`1<T>)
extern "C" List_1_t1_1837 * List_1_FindAllStackBits_m1_19357_gshared (List_1_t1_1837 * __this, Predicate_1_t1_2309 * ___match, const MethodInfo* method);
#define List_1_FindAllStackBits_m1_19357(__this, ___match, method) (( List_1_t1_1837 * (*) (List_1_t1_1837 *, Predicate_1_t1_2309 *, const MethodInfo*))List_1_FindAllStackBits_m1_19357_gshared)(__this, ___match, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<UnityEngine.Color32>::FindAllList(System.Predicate`1<T>)
extern "C" List_1_t1_1837 * List_1_FindAllList_m1_19358_gshared (List_1_t1_1837 * __this, Predicate_1_t1_2309 * ___match, const MethodInfo* method);
#define List_1_FindAllList_m1_19358(__this, ___match, method) (( List_1_t1_1837 * (*) (List_1_t1_1837 *, Predicate_1_t1_2309 *, const MethodInfo*))List_1_FindAllList_m1_19358_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Color32>::FindIndex(System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m1_19359_gshared (List_1_t1_1837 * __this, Predicate_1_t1_2309 * ___match, const MethodInfo* method);
#define List_1_FindIndex_m1_19359(__this, ___match, method) (( int32_t (*) (List_1_t1_1837 *, Predicate_1_t1_2309 *, const MethodInfo*))List_1_FindIndex_m1_19359_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Color32>::FindIndex(System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m1_19360_gshared (List_1_t1_1837 * __this, int32_t ___startIndex, Predicate_1_t1_2309 * ___match, const MethodInfo* method);
#define List_1_FindIndex_m1_19360(__this, ___startIndex, ___match, method) (( int32_t (*) (List_1_t1_1837 *, int32_t, Predicate_1_t1_2309 *, const MethodInfo*))List_1_FindIndex_m1_19360_gshared)(__this, ___startIndex, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Color32>::FindIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m1_19361_gshared (List_1_t1_1837 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_2309 * ___match, const MethodInfo* method);
#define List_1_FindIndex_m1_19361(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_1837 *, int32_t, int32_t, Predicate_1_t1_2309 *, const MethodInfo*))List_1_FindIndex_m1_19361_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Color32>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m1_19362_gshared (List_1_t1_1837 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_2309 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m1_19362(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_1837 *, int32_t, int32_t, Predicate_1_t1_2309 *, const MethodInfo*))List_1_GetIndex_m1_19362_gshared)(__this, ___startIndex, ___count, ___match, method)
// T System.Collections.Generic.List`1<UnityEngine.Color32>::FindLast(System.Predicate`1<T>)
extern "C" Color32_t6_49  List_1_FindLast_m1_19363_gshared (List_1_t1_1837 * __this, Predicate_1_t1_2309 * ___match, const MethodInfo* method);
#define List_1_FindLast_m1_19363(__this, ___match, method) (( Color32_t6_49  (*) (List_1_t1_1837 *, Predicate_1_t1_2309 *, const MethodInfo*))List_1_FindLast_m1_19363_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Color32>::FindLastIndex(System.Predicate`1<T>)
extern "C" int32_t List_1_FindLastIndex_m1_19364_gshared (List_1_t1_1837 * __this, Predicate_1_t1_2309 * ___match, const MethodInfo* method);
#define List_1_FindLastIndex_m1_19364(__this, ___match, method) (( int32_t (*) (List_1_t1_1837 *, Predicate_1_t1_2309 *, const MethodInfo*))List_1_FindLastIndex_m1_19364_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Color32>::FindLastIndex(System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindLastIndex_m1_19365_gshared (List_1_t1_1837 * __this, int32_t ___startIndex, Predicate_1_t1_2309 * ___match, const MethodInfo* method);
#define List_1_FindLastIndex_m1_19365(__this, ___startIndex, ___match, method) (( int32_t (*) (List_1_t1_1837 *, int32_t, Predicate_1_t1_2309 *, const MethodInfo*))List_1_FindLastIndex_m1_19365_gshared)(__this, ___startIndex, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Color32>::FindLastIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindLastIndex_m1_19366_gshared (List_1_t1_1837 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_2309 * ___match, const MethodInfo* method);
#define List_1_FindLastIndex_m1_19366(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_1837 *, int32_t, int32_t, Predicate_1_t1_2309 *, const MethodInfo*))List_1_FindLastIndex_m1_19366_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Color32>::GetLastIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetLastIndex_m1_19367_gshared (List_1_t1_1837 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_2309 * ___match, const MethodInfo* method);
#define List_1_GetLastIndex_m1_19367(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_1837 *, int32_t, int32_t, Predicate_1_t1_2309 *, const MethodInfo*))List_1_GetLastIndex_m1_19367_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::ForEach(System.Action`1<T>)
extern "C" void List_1_ForEach_m1_19368_gshared (List_1_t1_1837 * __this, Action_1_t1_2310 * ___action, const MethodInfo* method);
#define List_1_ForEach_m1_19368(__this, ___action, method) (( void (*) (List_1_t1_1837 *, Action_1_t1_2310 *, const MethodInfo*))List_1_ForEach_m1_19368_gshared)(__this, ___action, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Color32>::GetEnumerator()
extern "C" Enumerator_t1_2301  List_1_GetEnumerator_m1_19369_gshared (List_1_t1_1837 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m1_19369(__this, method) (( Enumerator_t1_2301  (*) (List_1_t1_1837 *, const MethodInfo*))List_1_GetEnumerator_m1_19369_gshared)(__this, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<UnityEngine.Color32>::GetRange(System.Int32,System.Int32)
extern "C" List_1_t1_1837 * List_1_GetRange_m1_19370_gshared (List_1_t1_1837 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_GetRange_m1_19370(__this, ___index, ___count, method) (( List_1_t1_1837 * (*) (List_1_t1_1837 *, int32_t, int32_t, const MethodInfo*))List_1_GetRange_m1_19370_gshared)(__this, ___index, ___count, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Color32>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m1_19371_gshared (List_1_t1_1837 * __this, Color32_t6_49  ___item, const MethodInfo* method);
#define List_1_IndexOf_m1_19371(__this, ___item, method) (( int32_t (*) (List_1_t1_1837 *, Color32_t6_49 , const MethodInfo*))List_1_IndexOf_m1_19371_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Color32>::IndexOf(T,System.Int32)
extern "C" int32_t List_1_IndexOf_m1_19372_gshared (List_1_t1_1837 * __this, Color32_t6_49  ___item, int32_t ___index, const MethodInfo* method);
#define List_1_IndexOf_m1_19372(__this, ___item, ___index, method) (( int32_t (*) (List_1_t1_1837 *, Color32_t6_49 , int32_t, const MethodInfo*))List_1_IndexOf_m1_19372_gshared)(__this, ___item, ___index, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Color32>::IndexOf(T,System.Int32,System.Int32)
extern "C" int32_t List_1_IndexOf_m1_19373_gshared (List_1_t1_1837 * __this, Color32_t6_49  ___item, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_IndexOf_m1_19373(__this, ___item, ___index, ___count, method) (( int32_t (*) (List_1_t1_1837 *, Color32_t6_49 , int32_t, int32_t, const MethodInfo*))List_1_IndexOf_m1_19373_gshared)(__this, ___item, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m1_19374_gshared (List_1_t1_1837 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m1_19374(__this, ___start, ___delta, method) (( void (*) (List_1_t1_1837 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m1_19374_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m1_19375_gshared (List_1_t1_1837 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m1_19375(__this, ___index, method) (( void (*) (List_1_t1_1837 *, int32_t, const MethodInfo*))List_1_CheckIndex_m1_19375_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m1_19376_gshared (List_1_t1_1837 * __this, int32_t ___index, Color32_t6_49  ___item, const MethodInfo* method);
#define List_1_Insert_m1_19376(__this, ___index, ___item, method) (( void (*) (List_1_t1_1837 *, int32_t, Color32_t6_49 , const MethodInfo*))List_1_Insert_m1_19376_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m1_19377_gshared (List_1_t1_1837 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m1_19377(__this, ___collection, method) (( void (*) (List_1_t1_1837 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m1_19377_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::InsertRange(System.Int32,System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_InsertRange_m1_19378_gshared (List_1_t1_1837 * __this, int32_t ___index, Object_t* ___collection, const MethodInfo* method);
#define List_1_InsertRange_m1_19378(__this, ___index, ___collection, method) (( void (*) (List_1_t1_1837 *, int32_t, Object_t*, const MethodInfo*))List_1_InsertRange_m1_19378_gshared)(__this, ___index, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::InsertCollection(System.Int32,System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_InsertCollection_m1_19379_gshared (List_1_t1_1837 * __this, int32_t ___index, Object_t* ___collection, const MethodInfo* method);
#define List_1_InsertCollection_m1_19379(__this, ___index, ___collection, method) (( void (*) (List_1_t1_1837 *, int32_t, Object_t*, const MethodInfo*))List_1_InsertCollection_m1_19379_gshared)(__this, ___index, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::InsertEnumeration(System.Int32,System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_InsertEnumeration_m1_19380_gshared (List_1_t1_1837 * __this, int32_t ___index, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_InsertEnumeration_m1_19380(__this, ___index, ___enumerable, method) (( void (*) (List_1_t1_1837 *, int32_t, Object_t*, const MethodInfo*))List_1_InsertEnumeration_m1_19380_gshared)(__this, ___index, ___enumerable, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Color32>::LastIndexOf(T)
extern "C" int32_t List_1_LastIndexOf_m1_19381_gshared (List_1_t1_1837 * __this, Color32_t6_49  ___item, const MethodInfo* method);
#define List_1_LastIndexOf_m1_19381(__this, ___item, method) (( int32_t (*) (List_1_t1_1837 *, Color32_t6_49 , const MethodInfo*))List_1_LastIndexOf_m1_19381_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Color32>::LastIndexOf(T,System.Int32)
extern "C" int32_t List_1_LastIndexOf_m1_19382_gshared (List_1_t1_1837 * __this, Color32_t6_49  ___item, int32_t ___index, const MethodInfo* method);
#define List_1_LastIndexOf_m1_19382(__this, ___item, ___index, method) (( int32_t (*) (List_1_t1_1837 *, Color32_t6_49 , int32_t, const MethodInfo*))List_1_LastIndexOf_m1_19382_gshared)(__this, ___item, ___index, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Color32>::LastIndexOf(T,System.Int32,System.Int32)
extern "C" int32_t List_1_LastIndexOf_m1_19383_gshared (List_1_t1_1837 * __this, Color32_t6_49  ___item, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_LastIndexOf_m1_19383(__this, ___item, ___index, ___count, method) (( int32_t (*) (List_1_t1_1837 *, Color32_t6_49 , int32_t, int32_t, const MethodInfo*))List_1_LastIndexOf_m1_19383_gshared)(__this, ___item, ___index, ___count, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Color32>::Remove(T)
extern "C" bool List_1_Remove_m1_19384_gshared (List_1_t1_1837 * __this, Color32_t6_49  ___item, const MethodInfo* method);
#define List_1_Remove_m1_19384(__this, ___item, method) (( bool (*) (List_1_t1_1837 *, Color32_t6_49 , const MethodInfo*))List_1_Remove_m1_19384_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Color32>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m1_19385_gshared (List_1_t1_1837 * __this, Predicate_1_t1_2309 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m1_19385(__this, ___match, method) (( int32_t (*) (List_1_t1_1837 *, Predicate_1_t1_2309 *, const MethodInfo*))List_1_RemoveAll_m1_19385_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m1_19386_gshared (List_1_t1_1837 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m1_19386(__this, ___index, method) (( void (*) (List_1_t1_1837 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1_19386_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::RemoveRange(System.Int32,System.Int32)
extern "C" void List_1_RemoveRange_m1_19387_gshared (List_1_t1_1837 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_RemoveRange_m1_19387(__this, ___index, ___count, method) (( void (*) (List_1_t1_1837 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m1_19387_gshared)(__this, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::Reverse()
extern "C" void List_1_Reverse_m1_19388_gshared (List_1_t1_1837 * __this, const MethodInfo* method);
#define List_1_Reverse_m1_19388(__this, method) (( void (*) (List_1_t1_1837 *, const MethodInfo*))List_1_Reverse_m1_19388_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::Reverse(System.Int32,System.Int32)
extern "C" void List_1_Reverse_m1_19389_gshared (List_1_t1_1837 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_Reverse_m1_19389(__this, ___index, ___count, method) (( void (*) (List_1_t1_1837 *, int32_t, int32_t, const MethodInfo*))List_1_Reverse_m1_19389_gshared)(__this, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::Sort()
extern "C" void List_1_Sort_m1_19390_gshared (List_1_t1_1837 * __this, const MethodInfo* method);
#define List_1_Sort_m1_19390(__this, method) (( void (*) (List_1_t1_1837 *, const MethodInfo*))List_1_Sort_m1_19390_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C" void List_1_Sort_m1_19391_gshared (List_1_t1_1837 * __this, Object_t* ___comparer, const MethodInfo* method);
#define List_1_Sort_m1_19391(__this, ___comparer, method) (( void (*) (List_1_t1_1837 *, Object_t*, const MethodInfo*))List_1_Sort_m1_19391_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m1_19392_gshared (List_1_t1_1837 * __this, Comparison_1_t1_2311 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m1_19392(__this, ___comparison, method) (( void (*) (List_1_t1_1837 *, Comparison_1_t1_2311 *, const MethodInfo*))List_1_Sort_m1_19392_gshared)(__this, ___comparison, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::Sort(System.Int32,System.Int32,System.Collections.Generic.IComparer`1<T>)
extern "C" void List_1_Sort_m1_19393_gshared (List_1_t1_1837 * __this, int32_t ___index, int32_t ___count, Object_t* ___comparer, const MethodInfo* method);
#define List_1_Sort_m1_19393(__this, ___index, ___count, ___comparer, method) (( void (*) (List_1_t1_1837 *, int32_t, int32_t, Object_t*, const MethodInfo*))List_1_Sort_m1_19393_gshared)(__this, ___index, ___count, ___comparer, method)
// T[] System.Collections.Generic.List`1<UnityEngine.Color32>::ToArray()
extern "C" Color32U5BU5D_t6_280* List_1_ToArray_m1_19394_gshared (List_1_t1_1837 * __this, const MethodInfo* method);
#define List_1_ToArray_m1_19394(__this, method) (( Color32U5BU5D_t6_280* (*) (List_1_t1_1837 *, const MethodInfo*))List_1_ToArray_m1_19394_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::TrimExcess()
extern "C" void List_1_TrimExcess_m1_19395_gshared (List_1_t1_1837 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m1_19395(__this, method) (( void (*) (List_1_t1_1837 *, const MethodInfo*))List_1_TrimExcess_m1_19395_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Color32>::TrueForAll(System.Predicate`1<T>)
extern "C" bool List_1_TrueForAll_m1_19396_gshared (List_1_t1_1837 * __this, Predicate_1_t1_2309 * ___match, const MethodInfo* method);
#define List_1_TrueForAll_m1_19396(__this, ___match, method) (( bool (*) (List_1_t1_1837 *, Predicate_1_t1_2309 *, const MethodInfo*))List_1_TrueForAll_m1_19396_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Color32>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m1_19397_gshared (List_1_t1_1837 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m1_19397(__this, method) (( int32_t (*) (List_1_t1_1837 *, const MethodInfo*))List_1_get_Capacity_m1_19397_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m1_19398_gshared (List_1_t1_1837 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m1_19398(__this, ___value, method) (( void (*) (List_1_t1_1837 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1_19398_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Color32>::get_Count()
extern "C" int32_t List_1_get_Count_m1_19399_gshared (List_1_t1_1837 * __this, const MethodInfo* method);
#define List_1_get_Count_m1_19399(__this, method) (( int32_t (*) (List_1_t1_1837 *, const MethodInfo*))List_1_get_Count_m1_19399_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.Color32>::get_Item(System.Int32)
extern "C" Color32_t6_49  List_1_get_Item_m1_19400_gshared (List_1_t1_1837 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m1_19400(__this, ___index, method) (( Color32_t6_49  (*) (List_1_t1_1837 *, int32_t, const MethodInfo*))List_1_get_Item_m1_19400_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m1_19401_gshared (List_1_t1_1837 * __this, int32_t ___index, Color32_t6_49  ___value, const MethodInfo* method);
#define List_1_set_Item_m1_19401(__this, ___index, ___value, method) (( void (*) (List_1_t1_1837 *, int32_t, Color32_t6_49 , const MethodInfo*))List_1_set_Item_m1_19401_gshared)(__this, ___index, ___value, method)
