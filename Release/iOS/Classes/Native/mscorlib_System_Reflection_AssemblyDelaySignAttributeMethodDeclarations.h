﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.AssemblyDelaySignAttribute
struct AssemblyDelaySignAttribute_t1_569;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Reflection.AssemblyDelaySignAttribute::.ctor(System.Boolean)
extern "C" void AssemblyDelaySignAttribute__ctor_m1_6678 (AssemblyDelaySignAttribute_t1_569 * __this, bool ___delaySign, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.AssemblyDelaySignAttribute::get_DelaySign()
extern "C" bool AssemblyDelaySignAttribute_get_DelaySign_m1_6679 (AssemblyDelaySignAttribute_t1_569 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
