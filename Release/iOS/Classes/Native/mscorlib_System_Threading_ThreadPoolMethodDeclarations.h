﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.InteropServices.SafeHandle
struct SafeHandle_t1_88;
// System.Threading.WaitCallback
struct WaitCallback_t1_1629;
// System.Object
struct Object_t;
// System.Threading.RegisteredWaitHandle
struct RegisteredWaitHandle_t1_1473;
// System.Threading.WaitHandle
struct WaitHandle_t1_917;
// System.Threading.WaitOrTimerCallback
struct WaitOrTimerCallback_t1_1474;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_TimeSpan.h"

// System.Boolean System.Threading.ThreadPool::BindHandle(System.IntPtr)
extern "C" bool ThreadPool_BindHandle_m1_12919 (Object_t * __this /* static, unused */, IntPtr_t ___osHandle, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.ThreadPool::BindHandle(System.Runtime.InteropServices.SafeHandle)
extern "C" bool ThreadPool_BindHandle_m1_12920 (Object_t * __this /* static, unused */, SafeHandle_t1_88 * ___osHandle, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.ThreadPool::GetMaxThreads(System.Int32&,System.Int32&)
extern "C" void ThreadPool_GetMaxThreads_m1_12921 (Object_t * __this /* static, unused */, int32_t* ___workerThreads, int32_t* ___completionPortThreads, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.ThreadPool::GetMinThreads(System.Int32&,System.Int32&)
extern "C" void ThreadPool_GetMinThreads_m1_12922 (Object_t * __this /* static, unused */, int32_t* ___workerThreads, int32_t* ___completionPortThreads, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.ThreadPool::SetMinThreads(System.Int32,System.Int32)
extern "C" bool ThreadPool_SetMinThreads_m1_12923 (Object_t * __this /* static, unused */, int32_t ___workerThreads, int32_t ___completionPortThreads, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.ThreadPool::SetMaxThreads(System.Int32,System.Int32)
extern "C" bool ThreadPool_SetMaxThreads_m1_12924 (Object_t * __this /* static, unused */, int32_t ___workerThreads, int32_t ___completionPortThreads, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.ThreadPool::QueueUserWorkItem(System.Threading.WaitCallback)
extern "C" bool ThreadPool_QueueUserWorkItem_m1_12925 (Object_t * __this /* static, unused */, WaitCallback_t1_1629 * ___callBack, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.ThreadPool::QueueUserWorkItem(System.Threading.WaitCallback,System.Object)
extern "C" bool ThreadPool_QueueUserWorkItem_m1_12926 (Object_t * __this /* static, unused */, WaitCallback_t1_1629 * ___callBack, Object_t * ___state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.RegisteredWaitHandle System.Threading.ThreadPool::RegisterWaitForSingleObject(System.Threading.WaitHandle,System.Threading.WaitOrTimerCallback,System.Object,System.Int32,System.Boolean)
extern "C" RegisteredWaitHandle_t1_1473 * ThreadPool_RegisterWaitForSingleObject_m1_12927 (Object_t * __this /* static, unused */, WaitHandle_t1_917 * ___waitObject, WaitOrTimerCallback_t1_1474 * ___callBack, Object_t * ___state, int32_t ___millisecondsTimeOutInterval, bool ___executeOnlyOnce, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.RegisteredWaitHandle System.Threading.ThreadPool::RegisterWaitForSingleObject(System.Threading.WaitHandle,System.Threading.WaitOrTimerCallback,System.Object,System.Int64,System.Boolean)
extern "C" RegisteredWaitHandle_t1_1473 * ThreadPool_RegisterWaitForSingleObject_m1_12928 (Object_t * __this /* static, unused */, WaitHandle_t1_917 * ___waitObject, WaitOrTimerCallback_t1_1474 * ___callBack, Object_t * ___state, int64_t ___millisecondsTimeOutInterval, bool ___executeOnlyOnce, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.RegisteredWaitHandle System.Threading.ThreadPool::RegisterWaitForSingleObject(System.Threading.WaitHandle,System.Threading.WaitOrTimerCallback,System.Object,System.TimeSpan,System.Boolean)
extern "C" RegisteredWaitHandle_t1_1473 * ThreadPool_RegisterWaitForSingleObject_m1_12929 (Object_t * __this /* static, unused */, WaitHandle_t1_917 * ___waitObject, WaitOrTimerCallback_t1_1474 * ___callBack, Object_t * ___state, TimeSpan_t1_368  ___timeout, bool ___executeOnlyOnce, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.RegisteredWaitHandle System.Threading.ThreadPool::RegisterWaitForSingleObject(System.Threading.WaitHandle,System.Threading.WaitOrTimerCallback,System.Object,System.UInt32,System.Boolean)
extern "C" RegisteredWaitHandle_t1_1473 * ThreadPool_RegisterWaitForSingleObject_m1_12930 (Object_t * __this /* static, unused */, WaitHandle_t1_917 * ___waitObject, WaitOrTimerCallback_t1_1474 * ___callBack, Object_t * ___state, uint32_t ___millisecondsTimeOutInterval, bool ___executeOnlyOnce, const MethodInfo* method) IL2CPP_METHOD_ATTR;
