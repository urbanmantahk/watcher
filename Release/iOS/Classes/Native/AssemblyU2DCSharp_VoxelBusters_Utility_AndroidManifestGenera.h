﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_ValueType.h"

// VoxelBusters.Utility.AndroidManifestGenerator/Feature
struct  Feature_t8_24 
{
	union
	{
		struct
		{
			// System.String VoxelBusters.Utility.AndroidManifestGenerator/Feature::<Name>k__BackingField
			String_t* ___U3CNameU3Ek__BackingField_0;
			// System.Boolean VoxelBusters.Utility.AndroidManifestGenerator/Feature::<Required>k__BackingField
			bool ___U3CRequiredU3Ek__BackingField_1;
		};
		uint8_t Feature_t8_24__padding[1];
	};
};
// Native definition for marshalling of: VoxelBusters.Utility.AndroidManifestGenerator/Feature
struct Feature_t8_24_marshaled
{
	union
	{
		struct
		{
			char* ___U3CNameU3Ek__BackingField_0;
			int32_t ___U3CRequiredU3Ek__BackingField_1;
		};
		uint8_t Feature_t8_24__padding[1];
	};
};
