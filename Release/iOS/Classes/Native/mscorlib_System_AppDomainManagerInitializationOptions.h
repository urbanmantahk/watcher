﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_AppDomainManagerInitializationOptions.h"

// System.AppDomainManagerInitializationOptions
struct  AppDomainManagerInitializationOptions_t1_1535 
{
	// System.Int32 System.AppDomainManagerInitializationOptions::value__
	int32_t ___value___1;
};
