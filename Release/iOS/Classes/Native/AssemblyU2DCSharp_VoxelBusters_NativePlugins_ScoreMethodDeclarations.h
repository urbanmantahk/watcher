﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.Score
struct Score_t8_231;
// System.String
struct String_t;
// VoxelBusters.NativePlugins.User
struct User_t8_235;
// VoxelBusters.NativePlugins.Score/ReportScoreCompletion
struct ReportScoreCompletion_t8_232;
// System.Collections.IDictionary
struct IDictionary_t1_35;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.Score::.ctor()
extern "C" void Score__ctor_m8_1304 (Score_t8_231 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Score::.ctor(System.String,System.String,VoxelBusters.NativePlugins.User,System.Int64)
extern "C" void Score__ctor_m8_1305 (Score_t8_231 * __this, String_t* ____leaderboardGlobalID, String_t* ____leaderboardID, User_t8_235 * ____user, int64_t ____scoreValue, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Score::add_ReportScoreFinishedEvent(VoxelBusters.NativePlugins.Score/ReportScoreCompletion)
extern "C" void Score_add_ReportScoreFinishedEvent_m8_1306 (Score_t8_231 * __this, ReportScoreCompletion_t8_232 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Score::remove_ReportScoreFinishedEvent(VoxelBusters.NativePlugins.Score/ReportScoreCompletion)
extern "C" void Score_remove_ReportScoreFinishedEvent_m8_1307 (Score_t8_231 * __this, ReportScoreCompletion_t8_232 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.Score::get_LeaderboardGlobalID()
extern "C" String_t* Score_get_LeaderboardGlobalID_m8_1308 (Score_t8_231 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Score::set_LeaderboardGlobalID(System.String)
extern "C" void Score_set_LeaderboardGlobalID_m8_1309 (Score_t8_231 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.Score::get_FormattedValue()
extern "C" String_t* Score_get_FormattedValue_m8_1310 (Score_t8_231 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Score::ReportScore(VoxelBusters.NativePlugins.Score/ReportScoreCompletion)
extern "C" void Score_ReportScore_m8_1311 (Score_t8_231 * __this, ReportScoreCompletion_t8_232 * ____onCompletion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.Score::ToString()
extern "C" String_t* Score_ToString_m8_1312 (Score_t8_231 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Score::ReportScoreFinished(System.Collections.IDictionary)
extern "C" void Score_ReportScoreFinished_m8_1313 (Score_t8_231 * __this, Object_t * ____dataDict, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Score::ReportScoreFinished(System.Boolean,System.String)
extern "C" void Score_ReportScoreFinished_m8_1314 (Score_t8_231 * __this, bool ____success, String_t* ____error, const MethodInfo* method) IL2CPP_METHOD_ATTR;
