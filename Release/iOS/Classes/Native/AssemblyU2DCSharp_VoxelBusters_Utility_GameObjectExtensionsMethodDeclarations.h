﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.GameObject
struct GameObject_t6_97;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1_238;
// UnityEngine.Transform
struct Transform_t6_65;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector3.h"
#include "UnityEngine_UnityEngine_Quaternion.h"

// UnityEngine.GameObject VoxelBusters.Utility.GameObjectExtensions::AddChild(UnityEngine.GameObject,System.String)
extern "C" GameObject_t6_97 * GameObjectExtensions_AddChild_m8_191 (Object_t * __this /* static, unused */, GameObject_t6_97 * ____parentGO, String_t* ____childName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject VoxelBusters.Utility.GameObjectExtensions::AddChild(UnityEngine.GameObject,System.String,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C" GameObject_t6_97 * GameObjectExtensions_AddChild_m8_192 (Object_t * __this /* static, unused */, GameObject_t6_97 * ____parentGO, String_t* ____childName, Vector3_t6_48  ____localPosition, Quaternion_t6_50  ____localRotation, Vector3_t6_48  ____localScale, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject VoxelBusters.Utility.GameObjectExtensions::AddChild(UnityEngine.GameObject,UnityEngine.GameObject)
extern "C" GameObject_t6_97 * GameObjectExtensions_AddChild_m8_193 (Object_t * __this /* static, unused */, GameObject_t6_97 * ____parentGO, GameObject_t6_97 * ____childGO, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject VoxelBusters.Utility.GameObjectExtensions::AddChild(UnityEngine.GameObject,UnityEngine.GameObject,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C" GameObject_t6_97 * GameObjectExtensions_AddChild_m8_194 (Object_t * __this /* static, unused */, GameObject_t6_97 * ____parentGO, GameObject_t6_97 * ____childGO, Vector3_t6_48  ____localPosition, Quaternion_t6_50  ____localRotation, Vector3_t6_48  ____localScale, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.Utility.GameObjectExtensions::GetPath(UnityEngine.GameObject)
extern "C" String_t* GameObjectExtensions_GetPath_m8_195 (Object_t * __this /* static, unused */, GameObject_t6_97 * ____gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject VoxelBusters.Utility.GameObjectExtensions::CreateGameObjectAtPath(System.String)
extern "C" GameObject_t6_97 * GameObjectExtensions_CreateGameObjectAtPath_m8_196 (Object_t * __this /* static, unused */, String_t* ____path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject VoxelBusters.Utility.GameObjectExtensions::CreateGameObject(System.String[],System.Int32&,System.Int32,UnityEngine.Transform)
extern "C" GameObject_t6_97 * GameObjectExtensions_CreateGameObject_m8_197 (Object_t * __this /* static, unused */, StringU5BU5D_t1_238* ____pathComponents, int32_t* ____index, int32_t ____count, Transform_t6_65 * ____parentTransform, const MethodInfo* method) IL2CPP_METHOD_ATTR;
