﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.InteropServices.CriticalHandle
struct CriticalHandle_t1_83;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"

// System.Void System.Runtime.InteropServices.CriticalHandle::.ctor(System.IntPtr)
extern "C" void CriticalHandle__ctor_m1_7620 (CriticalHandle_t1_83 * __this, IntPtr_t ___invalidHandleValue, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.CriticalHandle::Finalize()
extern "C" void CriticalHandle_Finalize_m1_7621 (CriticalHandle_t1_83 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.CriticalHandle::Close()
extern "C" void CriticalHandle_Close_m1_7622 (CriticalHandle_t1_83 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.CriticalHandle::Dispose()
extern "C" void CriticalHandle_Dispose_m1_7623 (CriticalHandle_t1_83 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.CriticalHandle::Dispose(System.Boolean)
extern "C" void CriticalHandle_Dispose_m1_7624 (CriticalHandle_t1_83 * __this, bool ___disposing, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.CriticalHandle::SetHandle(System.IntPtr)
extern "C" void CriticalHandle_SetHandle_m1_7625 (CriticalHandle_t1_83 * __this, IntPtr_t ___handle, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.CriticalHandle::SetHandleAsInvalid()
extern "C" void CriticalHandle_SetHandleAsInvalid_m1_7626 (CriticalHandle_t1_83 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.InteropServices.CriticalHandle::get_IsClosed()
extern "C" bool CriticalHandle_get_IsClosed_m1_7627 (CriticalHandle_t1_83 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
