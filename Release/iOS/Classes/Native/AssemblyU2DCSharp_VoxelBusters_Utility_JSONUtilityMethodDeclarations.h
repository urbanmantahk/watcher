﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.String VoxelBusters.Utility.JSONUtility::ToJSON(System.Object)
extern "C" String_t* JSONUtility_ToJSON_m8_296 (Object_t * __this /* static, unused */, Object_t * ____object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.Utility.JSONUtility::IsNull(System.String)
extern "C" bool JSONUtility_IsNull_m8_297 (Object_t * __this /* static, unused */, String_t* ____jsonStr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VoxelBusters.Utility.JSONUtility::FromJSON(System.String)
extern "C" Object_t * JSONUtility_FromJSON_m8_298 (Object_t * __this /* static, unused */, String_t* ____inputJSONString, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VoxelBusters.Utility.JSONUtility::FromJSON(System.String,System.Int32&)
extern "C" Object_t * JSONUtility_FromJSON_m8_299 (Object_t * __this /* static, unused */, String_t* ____inputJSONString, int32_t* ____errorIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
