﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Permissions.KeyContainerPermissionAccessEntryCollection
struct KeyContainerPermissionAccessEntryCollection_t1_1289;
// System.Security.Permissions.KeyContainerPermissionAccessEntry[]
struct KeyContainerPermissionAccessEntryU5BU5D_t1_1730;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Security.Permissions.KeyContainerPermissionAccessEntry
struct KeyContainerPermissionAccessEntry_t1_1290;
// System.Object
struct Object_t;
// System.Security.Permissions.KeyContainerPermissionAccessEntryEnumerator
struct KeyContainerPermissionAccessEntryEnumerator_t1_1291;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Permissions.KeyContainerPermissionAccessEntryCollection::.ctor()
extern "C" void KeyContainerPermissionAccessEntryCollection__ctor_m1_10984 (KeyContainerPermissionAccessEntryCollection_t1_1289 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.KeyContainerPermissionAccessEntryCollection::.ctor(System.Security.Permissions.KeyContainerPermissionAccessEntry[])
extern "C" void KeyContainerPermissionAccessEntryCollection__ctor_m1_10985 (KeyContainerPermissionAccessEntryCollection_t1_1289 * __this, KeyContainerPermissionAccessEntryU5BU5D_t1_1730* ___entries, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.KeyContainerPermissionAccessEntryCollection::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void KeyContainerPermissionAccessEntryCollection_System_Collections_ICollection_CopyTo_m1_10986 (KeyContainerPermissionAccessEntryCollection_t1_1289 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Security.Permissions.KeyContainerPermissionAccessEntryCollection::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * KeyContainerPermissionAccessEntryCollection_System_Collections_IEnumerable_GetEnumerator_m1_10987 (KeyContainerPermissionAccessEntryCollection_t1_1289 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Permissions.KeyContainerPermissionAccessEntryCollection::get_Count()
extern "C" int32_t KeyContainerPermissionAccessEntryCollection_get_Count_m1_10988 (KeyContainerPermissionAccessEntryCollection_t1_1289 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.KeyContainerPermissionAccessEntryCollection::get_IsSynchronized()
extern "C" bool KeyContainerPermissionAccessEntryCollection_get_IsSynchronized_m1_10989 (KeyContainerPermissionAccessEntryCollection_t1_1289 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Permissions.KeyContainerPermissionAccessEntry System.Security.Permissions.KeyContainerPermissionAccessEntryCollection::get_Item(System.Int32)
extern "C" KeyContainerPermissionAccessEntry_t1_1290 * KeyContainerPermissionAccessEntryCollection_get_Item_m1_10990 (KeyContainerPermissionAccessEntryCollection_t1_1289 * __this, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Security.Permissions.KeyContainerPermissionAccessEntryCollection::get_SyncRoot()
extern "C" Object_t * KeyContainerPermissionAccessEntryCollection_get_SyncRoot_m1_10991 (KeyContainerPermissionAccessEntryCollection_t1_1289 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Permissions.KeyContainerPermissionAccessEntryCollection::Add(System.Security.Permissions.KeyContainerPermissionAccessEntry)
extern "C" int32_t KeyContainerPermissionAccessEntryCollection_Add_m1_10992 (KeyContainerPermissionAccessEntryCollection_t1_1289 * __this, KeyContainerPermissionAccessEntry_t1_1290 * ___accessEntry, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.KeyContainerPermissionAccessEntryCollection::Clear()
extern "C" void KeyContainerPermissionAccessEntryCollection_Clear_m1_10993 (KeyContainerPermissionAccessEntryCollection_t1_1289 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.KeyContainerPermissionAccessEntryCollection::CopyTo(System.Security.Permissions.KeyContainerPermissionAccessEntry[],System.Int32)
extern "C" void KeyContainerPermissionAccessEntryCollection_CopyTo_m1_10994 (KeyContainerPermissionAccessEntryCollection_t1_1289 * __this, KeyContainerPermissionAccessEntryU5BU5D_t1_1730* ___array, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Permissions.KeyContainerPermissionAccessEntryEnumerator System.Security.Permissions.KeyContainerPermissionAccessEntryCollection::GetEnumerator()
extern "C" KeyContainerPermissionAccessEntryEnumerator_t1_1291 * KeyContainerPermissionAccessEntryCollection_GetEnumerator_m1_10995 (KeyContainerPermissionAccessEntryCollection_t1_1289 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Permissions.KeyContainerPermissionAccessEntryCollection::IndexOf(System.Security.Permissions.KeyContainerPermissionAccessEntry)
extern "C" int32_t KeyContainerPermissionAccessEntryCollection_IndexOf_m1_10996 (KeyContainerPermissionAccessEntryCollection_t1_1289 * __this, KeyContainerPermissionAccessEntry_t1_1290 * ___accessEntry, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.KeyContainerPermissionAccessEntryCollection::Remove(System.Security.Permissions.KeyContainerPermissionAccessEntry)
extern "C" void KeyContainerPermissionAccessEntryCollection_Remove_m1_10997 (KeyContainerPermissionAccessEntryCollection_t1_1289 * __this, KeyContainerPermissionAccessEntry_t1_1290 * ___accessEntry, const MethodInfo* method) IL2CPP_METHOD_ATTR;
