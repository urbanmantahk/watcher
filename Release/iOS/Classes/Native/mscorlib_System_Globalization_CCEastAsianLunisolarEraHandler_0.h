﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.SortedList
struct SortedList_t1_304;

#include "mscorlib_System_Object.h"

// System.Globalization.CCEastAsianLunisolarEraHandler
struct  CCEastAsianLunisolarEraHandler_t1_355  : public Object_t
{
	// System.Collections.SortedList System.Globalization.CCEastAsianLunisolarEraHandler::_Eras
	SortedList_t1_304 * ____Eras_0;
};
