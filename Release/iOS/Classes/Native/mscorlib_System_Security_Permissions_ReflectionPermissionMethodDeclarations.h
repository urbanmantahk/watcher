﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Permissions.ReflectionPermission
struct ReflectionPermission_t1_1301;
// System.Security.IPermission
struct IPermission_t1_1400;
// System.Security.SecurityElement
struct SecurityElement_t1_242;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Permissions_PermissionState.h"
#include "mscorlib_System_Security_Permissions_ReflectionPermissionFla.h"

// System.Void System.Security.Permissions.ReflectionPermission::.ctor(System.Security.Permissions.PermissionState)
extern "C" void ReflectionPermission__ctor_m1_11083 (ReflectionPermission_t1_1301 * __this, int32_t ___state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.ReflectionPermission::.ctor(System.Security.Permissions.ReflectionPermissionFlag)
extern "C" void ReflectionPermission__ctor_m1_11084 (ReflectionPermission_t1_1301 * __this, int32_t ___flag, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Permissions.ReflectionPermission::System.Security.Permissions.IBuiltInPermission.GetTokenIndex()
extern "C" int32_t ReflectionPermission_System_Security_Permissions_IBuiltInPermission_GetTokenIndex_m1_11085 (ReflectionPermission_t1_1301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Permissions.ReflectionPermissionFlag System.Security.Permissions.ReflectionPermission::get_Flags()
extern "C" int32_t ReflectionPermission_get_Flags_m1_11086 (ReflectionPermission_t1_1301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.ReflectionPermission::set_Flags(System.Security.Permissions.ReflectionPermissionFlag)
extern "C" void ReflectionPermission_set_Flags_m1_11087 (ReflectionPermission_t1_1301 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.ReflectionPermission::Copy()
extern "C" Object_t * ReflectionPermission_Copy_m1_11088 (ReflectionPermission_t1_1301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.ReflectionPermission::FromXml(System.Security.SecurityElement)
extern "C" void ReflectionPermission_FromXml_m1_11089 (ReflectionPermission_t1_1301 * __this, SecurityElement_t1_242 * ___esd, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.ReflectionPermission::Intersect(System.Security.IPermission)
extern "C" Object_t * ReflectionPermission_Intersect_m1_11090 (ReflectionPermission_t1_1301 * __this, Object_t * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.ReflectionPermission::IsSubsetOf(System.Security.IPermission)
extern "C" bool ReflectionPermission_IsSubsetOf_m1_11091 (ReflectionPermission_t1_1301 * __this, Object_t * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.ReflectionPermission::IsUnrestricted()
extern "C" bool ReflectionPermission_IsUnrestricted_m1_11092 (ReflectionPermission_t1_1301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityElement System.Security.Permissions.ReflectionPermission::ToXml()
extern "C" SecurityElement_t1_242 * ReflectionPermission_ToXml_m1_11093 (ReflectionPermission_t1_1301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.ReflectionPermission::Union(System.Security.IPermission)
extern "C" Object_t * ReflectionPermission_Union_m1_11094 (ReflectionPermission_t1_1301 * __this, Object_t * ___other, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Permissions.ReflectionPermission System.Security.Permissions.ReflectionPermission::Cast(System.Security.IPermission)
extern "C" ReflectionPermission_t1_1301 * ReflectionPermission_Cast_m1_11095 (ReflectionPermission_t1_1301 * __this, Object_t * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
