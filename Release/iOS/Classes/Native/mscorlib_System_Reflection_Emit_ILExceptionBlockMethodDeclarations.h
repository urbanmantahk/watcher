﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_Emit_ILExceptionBlock.h"

// System.Void System.Reflection.Emit.ILExceptionBlock::Debug()
extern "C" void ILExceptionBlock_Debug_m1_5953 (ILExceptionBlock_t1_510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
