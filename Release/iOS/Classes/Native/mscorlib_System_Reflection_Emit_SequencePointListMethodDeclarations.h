﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.Emit.SequencePointList
struct SequencePointList_t1_522;
// System.Diagnostics.SymbolStore.ISymbolDocumentWriter
struct ISymbolDocumentWriter_t1_525;
// System.Int32[]
struct Int32U5BU5D_t1_275;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Reflection.Emit.SequencePointList::.ctor(System.Diagnostics.SymbolStore.ISymbolDocumentWriter)
extern "C" void SequencePointList__ctor_m1_6020 (SequencePointList_t1_522 * __this, Object_t * ___doc, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Diagnostics.SymbolStore.ISymbolDocumentWriter System.Reflection.Emit.SequencePointList::get_Document()
extern "C" Object_t * SequencePointList_get_Document_m1_6021 (SequencePointList_t1_522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] System.Reflection.Emit.SequencePointList::GetOffsets()
extern "C" Int32U5BU5D_t1_275* SequencePointList_GetOffsets_m1_6022 (SequencePointList_t1_522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] System.Reflection.Emit.SequencePointList::GetLines()
extern "C" Int32U5BU5D_t1_275* SequencePointList_GetLines_m1_6023 (SequencePointList_t1_522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] System.Reflection.Emit.SequencePointList::GetColumns()
extern "C" Int32U5BU5D_t1_275* SequencePointList_GetColumns_m1_6024 (SequencePointList_t1_522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] System.Reflection.Emit.SequencePointList::GetEndLines()
extern "C" Int32U5BU5D_t1_275* SequencePointList_GetEndLines_m1_6025 (SequencePointList_t1_522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] System.Reflection.Emit.SequencePointList::GetEndColumns()
extern "C" Int32U5BU5D_t1_275* SequencePointList_GetEndColumns_m1_6026 (SequencePointList_t1_522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.SequencePointList::get_StartLine()
extern "C" int32_t SequencePointList_get_StartLine_m1_6027 (SequencePointList_t1_522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.SequencePointList::get_EndLine()
extern "C" int32_t SequencePointList_get_EndLine_m1_6028 (SequencePointList_t1_522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.SequencePointList::get_StartColumn()
extern "C" int32_t SequencePointList_get_StartColumn_m1_6029 (SequencePointList_t1_522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.SequencePointList::get_EndColumn()
extern "C" int32_t SequencePointList_get_EndColumn_m1_6030 (SequencePointList_t1_522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.SequencePointList::AddSequencePoint(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" void SequencePointList_AddSequencePoint_m1_6031 (SequencePointList_t1_522 * __this, int32_t ___offset, int32_t ___line, int32_t ___col, int32_t ___endLine, int32_t ___endCol, const MethodInfo* method) IL2CPP_METHOD_ATTR;
