﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Globalization.Bootstring
struct Bootstring_t1_377;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Globalization.Bootstring::.ctor(System.Char,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" void Bootstring__ctor_m1_4207 (Bootstring_t1_377 * __this, uint16_t ___delimiter, int32_t ___baseNum, int32_t ___tmin, int32_t ___tmax, int32_t ___skew, int32_t ___damp, int32_t ___initialBias, int32_t ___initialN, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.Bootstring::Encode(System.String,System.Int32)
extern "C" String_t* Bootstring_Encode_m1_4208 (Bootstring_t1_377 * __this, String_t* ___s, int32_t ___offset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char System.Globalization.Bootstring::EncodeDigit(System.Int32)
extern "C" uint16_t Bootstring_EncodeDigit_m1_4209 (Bootstring_t1_377 * __this, int32_t ___d, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.Bootstring::DecodeDigit(System.Char)
extern "C" int32_t Bootstring_DecodeDigit_m1_4210 (Bootstring_t1_377 * __this, uint16_t ___c, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.Bootstring::Adapt(System.Int32,System.Int32,System.Boolean)
extern "C" int32_t Bootstring_Adapt_m1_4211 (Bootstring_t1_377 * __this, int32_t ___delta, int32_t ___numPoints, bool ___firstTime, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.Bootstring::Decode(System.String,System.Int32)
extern "C" String_t* Bootstring_Decode_m1_4212 (Bootstring_t1_377 * __this, String_t* ___s, int32_t ___offset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
