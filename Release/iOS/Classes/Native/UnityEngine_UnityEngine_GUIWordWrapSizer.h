﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GUIContent
struct GUIContent_t6_171;

#include "UnityEngine_UnityEngine_GUILayoutEntry.h"

// UnityEngine.GUIWordWrapSizer
struct  GUIWordWrapSizer_t6_180  : public GUILayoutEntry_t6_177
{
	// UnityEngine.GUIContent UnityEngine.GUIWordWrapSizer::m_Content
	GUIContent_t6_171 * ___m_Content_10;
	// System.Single UnityEngine.GUIWordWrapSizer::m_ForcedMinHeight
	float ___m_ForcedMinHeight_11;
	// System.Single UnityEngine.GUIWordWrapSizer::m_ForcedMaxHeight
	float ___m_ForcedMaxHeight_12;
};
