﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Permissions.HostProtectionAttribute
struct HostProtectionAttribute_t1_1279;
// System.Security.IPermission
struct IPermission_t1_1400;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Permissions_SecurityAction.h"
#include "mscorlib_System_Security_Permissions_HostProtectionResource.h"

// System.Void System.Security.Permissions.HostProtectionAttribute::.ctor()
extern "C" void HostProtectionAttribute__ctor_m1_10892 (HostProtectionAttribute_t1_1279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.HostProtectionAttribute::.ctor(System.Security.Permissions.SecurityAction)
extern "C" void HostProtectionAttribute__ctor_m1_10893 (HostProtectionAttribute_t1_1279 * __this, int32_t ___action, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.HostProtectionAttribute::get_ExternalProcessMgmt()
extern "C" bool HostProtectionAttribute_get_ExternalProcessMgmt_m1_10894 (HostProtectionAttribute_t1_1279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.HostProtectionAttribute::set_ExternalProcessMgmt(System.Boolean)
extern "C" void HostProtectionAttribute_set_ExternalProcessMgmt_m1_10895 (HostProtectionAttribute_t1_1279 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.HostProtectionAttribute::get_ExternalThreading()
extern "C" bool HostProtectionAttribute_get_ExternalThreading_m1_10896 (HostProtectionAttribute_t1_1279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.HostProtectionAttribute::set_ExternalThreading(System.Boolean)
extern "C" void HostProtectionAttribute_set_ExternalThreading_m1_10897 (HostProtectionAttribute_t1_1279 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.HostProtectionAttribute::get_MayLeakOnAbort()
extern "C" bool HostProtectionAttribute_get_MayLeakOnAbort_m1_10898 (HostProtectionAttribute_t1_1279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.HostProtectionAttribute::set_MayLeakOnAbort(System.Boolean)
extern "C" void HostProtectionAttribute_set_MayLeakOnAbort_m1_10899 (HostProtectionAttribute_t1_1279 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.HostProtectionAttribute::get_SecurityInfrastructure()
extern "C" bool HostProtectionAttribute_get_SecurityInfrastructure_m1_10900 (HostProtectionAttribute_t1_1279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.HostProtectionAttribute::set_SecurityInfrastructure(System.Boolean)
extern "C" void HostProtectionAttribute_set_SecurityInfrastructure_m1_10901 (HostProtectionAttribute_t1_1279 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.HostProtectionAttribute::get_SelfAffectingProcessMgmt()
extern "C" bool HostProtectionAttribute_get_SelfAffectingProcessMgmt_m1_10902 (HostProtectionAttribute_t1_1279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.HostProtectionAttribute::set_SelfAffectingProcessMgmt(System.Boolean)
extern "C" void HostProtectionAttribute_set_SelfAffectingProcessMgmt_m1_10903 (HostProtectionAttribute_t1_1279 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.HostProtectionAttribute::get_SelfAffectingThreading()
extern "C" bool HostProtectionAttribute_get_SelfAffectingThreading_m1_10904 (HostProtectionAttribute_t1_1279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.HostProtectionAttribute::set_SelfAffectingThreading(System.Boolean)
extern "C" void HostProtectionAttribute_set_SelfAffectingThreading_m1_10905 (HostProtectionAttribute_t1_1279 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.HostProtectionAttribute::get_SharedState()
extern "C" bool HostProtectionAttribute_get_SharedState_m1_10906 (HostProtectionAttribute_t1_1279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.HostProtectionAttribute::set_SharedState(System.Boolean)
extern "C" void HostProtectionAttribute_set_SharedState_m1_10907 (HostProtectionAttribute_t1_1279 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.HostProtectionAttribute::get_Synchronization()
extern "C" bool HostProtectionAttribute_get_Synchronization_m1_10908 (HostProtectionAttribute_t1_1279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.HostProtectionAttribute::set_Synchronization(System.Boolean)
extern "C" void HostProtectionAttribute_set_Synchronization_m1_10909 (HostProtectionAttribute_t1_1279 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.HostProtectionAttribute::get_UI()
extern "C" bool HostProtectionAttribute_get_UI_m1_10910 (HostProtectionAttribute_t1_1279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.HostProtectionAttribute::set_UI(System.Boolean)
extern "C" void HostProtectionAttribute_set_UI_m1_10911 (HostProtectionAttribute_t1_1279 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Permissions.HostProtectionResource System.Security.Permissions.HostProtectionAttribute::get_Resources()
extern "C" int32_t HostProtectionAttribute_get_Resources_m1_10912 (HostProtectionAttribute_t1_1279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.HostProtectionAttribute::set_Resources(System.Security.Permissions.HostProtectionResource)
extern "C" void HostProtectionAttribute_set_Resources_m1_10913 (HostProtectionAttribute_t1_1279 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.HostProtectionAttribute::CreatePermission()
extern "C" Object_t * HostProtectionAttribute_CreatePermission_m1_10914 (HostProtectionAttribute_t1_1279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
