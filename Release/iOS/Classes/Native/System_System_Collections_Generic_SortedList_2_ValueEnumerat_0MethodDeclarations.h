﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>
struct SortedList_2_t3_248;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_SortedList_2_ValueEnumerat_0.h"
#include "AssemblyU2DCSharp_ExifLibrary_IFD.h"

// System.Void System.Collections.Generic.SortedList`2/ValueEnumerator<System.Int32,ExifLibrary.IFD>::.ctor(System.Collections.Generic.SortedList`2<TKey,TValue>)
extern "C" void ValueEnumerator__ctor_m3_2221_gshared (ValueEnumerator_t3_284 * __this, SortedList_2_t3_248 * ___l, const MethodInfo* method);
#define ValueEnumerator__ctor_m3_2221(__this, ___l, method) (( void (*) (ValueEnumerator_t3_284 *, SortedList_2_t3_248 *, const MethodInfo*))ValueEnumerator__ctor_m3_2221_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.SortedList`2/ValueEnumerator<System.Int32,ExifLibrary.IFD>::System.Collections.IEnumerator.Reset()
extern "C" void ValueEnumerator_System_Collections_IEnumerator_Reset_m3_2222_gshared (ValueEnumerator_t3_284 * __this, const MethodInfo* method);
#define ValueEnumerator_System_Collections_IEnumerator_Reset_m3_2222(__this, method) (( void (*) (ValueEnumerator_t3_284 *, const MethodInfo*))ValueEnumerator_System_Collections_IEnumerator_Reset_m3_2222_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2/ValueEnumerator<System.Int32,ExifLibrary.IFD>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * ValueEnumerator_System_Collections_IEnumerator_get_Current_m3_2223_gshared (ValueEnumerator_t3_284 * __this, const MethodInfo* method);
#define ValueEnumerator_System_Collections_IEnumerator_get_Current_m3_2223(__this, method) (( Object_t * (*) (ValueEnumerator_t3_284 *, const MethodInfo*))ValueEnumerator_System_Collections_IEnumerator_get_Current_m3_2223_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/ValueEnumerator<System.Int32,ExifLibrary.IFD>::Dispose()
extern "C" void ValueEnumerator_Dispose_m3_2224_gshared (ValueEnumerator_t3_284 * __this, const MethodInfo* method);
#define ValueEnumerator_Dispose_m3_2224(__this, method) (( void (*) (ValueEnumerator_t3_284 *, const MethodInfo*))ValueEnumerator_Dispose_m3_2224_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedList`2/ValueEnumerator<System.Int32,ExifLibrary.IFD>::MoveNext()
extern "C" bool ValueEnumerator_MoveNext_m3_2225_gshared (ValueEnumerator_t3_284 * __this, const MethodInfo* method);
#define ValueEnumerator_MoveNext_m3_2225(__this, method) (( bool (*) (ValueEnumerator_t3_284 *, const MethodInfo*))ValueEnumerator_MoveNext_m3_2225_gshared)(__this, method)
// TValue System.Collections.Generic.SortedList`2/ValueEnumerator<System.Int32,ExifLibrary.IFD>::get_Current()
extern "C" int32_t ValueEnumerator_get_Current_m3_2226_gshared (ValueEnumerator_t3_284 * __this, const MethodInfo* method);
#define ValueEnumerator_get_Current_m3_2226(__this, method) (( int32_t (*) (ValueEnumerator_t3_284 *, const MethodInfo*))ValueEnumerator_get_Current_m3_2226_gshared)(__this, method)
