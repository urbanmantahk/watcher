﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_Emit_Label.h"

// System.Void System.Reflection.Emit.Label::.ctor(System.Int32)
extern "C" void Label__ctor_m1_6032 (Label_t1_513 * __this, int32_t ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.Label::Equals(System.Object)
extern "C" bool Label_Equals_m1_6033 (Label_t1_513 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.Label::Equals(System.Reflection.Emit.Label)
extern "C" bool Label_Equals_m1_6034 (Label_t1_513 * __this, Label_t1_513  ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.Label::GetHashCode()
extern "C" int32_t Label_GetHashCode_m1_6035 (Label_t1_513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.Label::op_Equality(System.Reflection.Emit.Label,System.Reflection.Emit.Label)
extern "C" bool Label_op_Equality_m1_6036 (Object_t * __this /* static, unused */, Label_t1_513  ___a, Label_t1_513  ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.Label::op_Inequality(System.Reflection.Emit.Label,System.Reflection.Emit.Label)
extern "C" bool Label_op_Inequality_m1_6037 (Object_t * __this /* static, unused */, Label_t1_513  ___a, Label_t1_513  ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
