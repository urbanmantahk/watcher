﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Reflection_Emit_OpCodeType.h"

// System.Reflection.Emit.OpCodeType
struct  OpCodeType_t1_540 
{
	// System.Int32 System.Reflection.Emit.OpCodeType::value__
	int32_t ___value___1;
};
