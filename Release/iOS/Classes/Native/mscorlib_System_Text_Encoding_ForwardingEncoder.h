﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Text.Encoding
struct Encoding_t1_406;

#include "mscorlib_System_Text_Encoder.h"

// System.Text.Encoding/ForwardingEncoder
struct  ForwardingEncoder_t1_1436  : public Encoder_t1_1428
{
	// System.Text.Encoding System.Text.Encoding/ForwardingEncoder::encoding
	Encoding_t1_406 * ___encoding_2;
};
