﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Principal.WindowsImpersonationContext
struct WindowsImpersonationContext_t1_1385;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"

// System.Void System.Security.Principal.WindowsImpersonationContext::.ctor(System.IntPtr)
extern "C" void WindowsImpersonationContext__ctor_m1_11881 (WindowsImpersonationContext_t1_1385 * __this, IntPtr_t ___token, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Principal.WindowsImpersonationContext::Dispose()
extern "C" void WindowsImpersonationContext_Dispose_m1_11882 (WindowsImpersonationContext_t1_1385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Principal.WindowsImpersonationContext::Dispose(System.Boolean)
extern "C" void WindowsImpersonationContext_Dispose_m1_11883 (WindowsImpersonationContext_t1_1385 * __this, bool ___disposing, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Principal.WindowsImpersonationContext::Undo()
extern "C" void WindowsImpersonationContext_Undo_m1_11884 (WindowsImpersonationContext_t1_1385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Principal.WindowsImpersonationContext::CloseToken(System.IntPtr)
extern "C" bool WindowsImpersonationContext_CloseToken_m1_11885 (Object_t * __this /* static, unused */, IntPtr_t ___token, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Security.Principal.WindowsImpersonationContext::DuplicateToken(System.IntPtr)
extern "C" IntPtr_t WindowsImpersonationContext_DuplicateToken_m1_11886 (Object_t * __this /* static, unused */, IntPtr_t ___token, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Principal.WindowsImpersonationContext::SetCurrentToken(System.IntPtr)
extern "C" bool WindowsImpersonationContext_SetCurrentToken_m1_11887 (Object_t * __this /* static, unused */, IntPtr_t ___token, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Principal.WindowsImpersonationContext::RevertToSelf()
extern "C" bool WindowsImpersonationContext_RevertToSelf_m1_11888 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
