﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Xml.XmlNameEntry
struct XmlNameEntry_t4_117;
// System.Xml.XmlLinkedNode
struct XmlLinkedNode_t4_118;
// System.Xml.Schema.IXmlSchemaInfo
struct IXmlSchemaInfo_t4_119;

#include "System_Xml_System_Xml_XmlNode.h"

// System.Xml.XmlAttribute
struct  XmlAttribute_t4_115  : public XmlNode_t4_116
{
	// System.Xml.XmlNameEntry System.Xml.XmlAttribute::name
	XmlNameEntry_t4_117 * ___name_5;
	// System.Boolean System.Xml.XmlAttribute::isDefault
	bool ___isDefault_6;
	// System.Xml.XmlLinkedNode System.Xml.XmlAttribute::lastLinkedChild
	XmlLinkedNode_t4_118 * ___lastLinkedChild_7;
	// System.Xml.Schema.IXmlSchemaInfo System.Xml.XmlAttribute::schemaInfo
	Object_t * ___schemaInfo_8;
};
