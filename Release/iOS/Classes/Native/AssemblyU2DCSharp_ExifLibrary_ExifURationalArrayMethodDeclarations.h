﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExifLibrary.ExifURationalArray
struct ExifURationalArray_t8_107;
// ExifLibrary.MathEx/UFraction32[]
struct UFraction32U5BU5D_t8_122;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Single[]
struct SingleU5BU5D_t1_1695;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTag.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifInterOperability.h"

// System.Void ExifLibrary.ExifURationalArray::.ctor(ExifLibrary.ExifTag,ExifLibrary.MathEx/UFraction32[])
extern "C" void ExifURationalArray__ctor_m8_576 (ExifURationalArray_t8_107 * __this, int32_t ___tag, UFraction32U5BU5D_t8_122* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ExifLibrary.ExifURationalArray::get__Value()
extern "C" Object_t * ExifURationalArray_get__Value_m8_577 (ExifURationalArray_t8_107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.ExifURationalArray::set__Value(System.Object)
extern "C" void ExifURationalArray_set__Value_m8_578 (ExifURationalArray_t8_107 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/UFraction32[] ExifLibrary.ExifURationalArray::get_Value()
extern "C" UFraction32U5BU5D_t8_122* ExifURationalArray_get_Value_m8_579 (ExifURationalArray_t8_107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.ExifURationalArray::set_Value(ExifLibrary.MathEx/UFraction32[])
extern "C" void ExifURationalArray_set_Value_m8_580 (ExifURationalArray_t8_107 * __this, UFraction32U5BU5D_t8_122* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExifLibrary.ExifURationalArray::ToString()
extern "C" String_t* ExifURationalArray_ToString_m8_581 (ExifURationalArray_t8_107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.ExifInterOperability ExifLibrary.ExifURationalArray::get_Interoperability()
extern "C" ExifInterOperability_t8_113  ExifURationalArray_get_Interoperability_m8_582 (ExifURationalArray_t8_107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single[] ExifLibrary.ExifURationalArray::op_Explicit(ExifLibrary.ExifURationalArray)
extern "C" SingleU5BU5D_t1_1695* ExifURationalArray_op_Explicit_m8_583 (Object_t * __this /* static, unused */, ExifURationalArray_t8_107 * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
