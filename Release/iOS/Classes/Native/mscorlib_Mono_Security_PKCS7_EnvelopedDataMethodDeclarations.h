﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.PKCS7/EnvelopedData
struct EnvelopedData_t1_224;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// Mono.Security.ASN1
struct ASN1_t1_149;
// System.Collections.ArrayList
struct ArrayList_t1_170;
// Mono.Security.PKCS7/ContentInfo
struct ContentInfo_t1_222;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Security.PKCS7/EnvelopedData::.ctor()
extern "C" void EnvelopedData__ctor_m1_2496 (EnvelopedData_t1_224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.PKCS7/EnvelopedData::.ctor(System.Byte[])
extern "C" void EnvelopedData__ctor_m1_2497 (EnvelopedData_t1_224 * __this, ByteU5BU5D_t1_109* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.PKCS7/EnvelopedData::.ctor(Mono.Security.ASN1)
extern "C" void EnvelopedData__ctor_m1_2498 (EnvelopedData_t1_224 * __this, ASN1_t1_149 * ___asn1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList Mono.Security.PKCS7/EnvelopedData::get_RecipientInfos()
extern "C" ArrayList_t1_170 * EnvelopedData_get_RecipientInfos_m1_2499 (EnvelopedData_t1_224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.ASN1 Mono.Security.PKCS7/EnvelopedData::get_ASN1()
extern "C" ASN1_t1_149 * EnvelopedData_get_ASN1_m1_2500 (EnvelopedData_t1_224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.PKCS7/ContentInfo Mono.Security.PKCS7/EnvelopedData::get_ContentInfo()
extern "C" ContentInfo_t1_222 * EnvelopedData_get_ContentInfo_m1_2501 (EnvelopedData_t1_224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.PKCS7/ContentInfo Mono.Security.PKCS7/EnvelopedData::get_EncryptionAlgorithm()
extern "C" ContentInfo_t1_222 * EnvelopedData_get_EncryptionAlgorithm_m1_2502 (EnvelopedData_t1_224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.PKCS7/EnvelopedData::get_EncryptedContent()
extern "C" ByteU5BU5D_t1_109* EnvelopedData_get_EncryptedContent_m1_2503 (EnvelopedData_t1_224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte Mono.Security.PKCS7/EnvelopedData::get_Version()
extern "C" uint8_t EnvelopedData_get_Version_m1_2504 (EnvelopedData_t1_224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.PKCS7/EnvelopedData::set_Version(System.Byte)
extern "C" void EnvelopedData_set_Version_m1_2505 (EnvelopedData_t1_224 * __this, uint8_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.ASN1 Mono.Security.PKCS7/EnvelopedData::GetASN1()
extern "C" ASN1_t1_149 * EnvelopedData_GetASN1_m1_2506 (EnvelopedData_t1_224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.PKCS7/EnvelopedData::GetBytes()
extern "C" ByteU5BU5D_t1_109* EnvelopedData_GetBytes_m1_2507 (EnvelopedData_t1_224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
