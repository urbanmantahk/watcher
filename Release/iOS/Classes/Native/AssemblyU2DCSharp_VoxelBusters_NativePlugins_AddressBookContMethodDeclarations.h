﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.AddressBookContact/<GetImageAsync>c__AnonStoreyA
struct U3CGetImageAsyncU3Ec__AnonStoreyA_t8_197;
// UnityEngine.Texture2D
struct Texture2D_t6_33;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.AddressBookContact/<GetImageAsync>c__AnonStoreyA::.ctor()
extern "C" void U3CGetImageAsyncU3Ec__AnonStoreyA__ctor_m8_1138 (U3CGetImageAsyncU3Ec__AnonStoreyA_t8_197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.AddressBookContact/<GetImageAsync>c__AnonStoreyA::<>m__9(UnityEngine.Texture2D,System.String)
extern "C" void U3CGetImageAsyncU3Ec__AnonStoreyA_U3CU3Em__9_m8_1139 (U3CGetImageAsyncU3Ec__AnonStoreyA_t8_197 * __this, Texture2D_t6_33 * ____newTexture, String_t* ____error, const MethodInfo* method) IL2CPP_METHOD_ATTR;
