﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType.h"
#include "mscorlib_System_Reflection_Emit_TypeToken.h"

// System.Reflection.Emit.TypeToken
struct  TypeToken_t1_558 
{
	// System.Int32 System.Reflection.Emit.TypeToken::tokValue
	int32_t ___tokValue_0;
};
struct TypeToken_t1_558_StaticFields{
	// System.Reflection.Emit.TypeToken System.Reflection.Emit.TypeToken::Empty
	TypeToken_t1_558  ___Empty_1;
};
