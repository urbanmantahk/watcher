﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Security_Permissions_CodeAccessSecurityAttri.h"

// System.Security.Permissions.RegistryPermissionAttribute
struct  RegistryPermissionAttribute_t1_1306  : public CodeAccessSecurityAttribute_t1_1266
{
	// System.String System.Security.Permissions.RegistryPermissionAttribute::create
	String_t* ___create_2;
	// System.String System.Security.Permissions.RegistryPermissionAttribute::read
	String_t* ___read_3;
	// System.String System.Security.Permissions.RegistryPermissionAttribute::write
	String_t* ___write_4;
	// System.String System.Security.Permissions.RegistryPermissionAttribute::changeAccessControl
	String_t* ___changeAccessControl_5;
	// System.String System.Security.Permissions.RegistryPermissionAttribute::viewAccessControl
	String_t* ___viewAccessControl_6;
};
