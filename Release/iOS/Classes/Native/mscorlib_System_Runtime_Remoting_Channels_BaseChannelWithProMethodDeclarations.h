﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Channels.BaseChannelWithProperties
struct BaseChannelWithProperties_t1_866;
// System.Collections.IDictionary
struct IDictionary_t1_35;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Channels.BaseChannelWithProperties::.ctor()
extern "C" void BaseChannelWithProperties__ctor_m1_8026 (BaseChannelWithProperties_t1_866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IDictionary System.Runtime.Remoting.Channels.BaseChannelWithProperties::get_Properties()
extern "C" Object_t * BaseChannelWithProperties_get_Properties_m1_8027 (BaseChannelWithProperties_t1_866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
