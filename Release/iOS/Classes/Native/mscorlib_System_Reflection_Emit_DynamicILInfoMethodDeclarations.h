﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.Emit.DynamicILInfo
struct DynamicILInfo_t1_493;
// System.Reflection.Emit.DynamicMethod
struct DynamicMethod_t1_496;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_RuntimeFieldHandle.h"
#include "mscorlib_System_RuntimeMethodHandle.h"
#include "mscorlib_System_RuntimeTypeHandle.h"

// System.Void System.Reflection.Emit.DynamicILInfo::.ctor()
extern "C" void DynamicILInfo__ctor_m1_5673 (DynamicILInfo_t1_493 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.DynamicMethod System.Reflection.Emit.DynamicILInfo::get_DynamicMethod()
extern "C" DynamicMethod_t1_496 * DynamicILInfo_get_DynamicMethod_m1_5674 (DynamicILInfo_t1_493 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.DynamicILInfo::GetTokenFor(System.Byte[])
extern "C" int32_t DynamicILInfo_GetTokenFor_m1_5675 (DynamicILInfo_t1_493 * __this, ByteU5BU5D_t1_109* ___signature, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.DynamicILInfo::GetTokenFor(System.Reflection.Emit.DynamicMethod)
extern "C" int32_t DynamicILInfo_GetTokenFor_m1_5676 (DynamicILInfo_t1_493 * __this, DynamicMethod_t1_496 * ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.DynamicILInfo::GetTokenFor(System.RuntimeFieldHandle)
extern "C" int32_t DynamicILInfo_GetTokenFor_m1_5677 (DynamicILInfo_t1_493 * __this, RuntimeFieldHandle_t1_36  ___field, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.DynamicILInfo::GetTokenFor(System.RuntimeMethodHandle)
extern "C" int32_t DynamicILInfo_GetTokenFor_m1_5678 (DynamicILInfo_t1_493 * __this, RuntimeMethodHandle_t1_479  ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.DynamicILInfo::GetTokenFor(System.RuntimeTypeHandle)
extern "C" int32_t DynamicILInfo_GetTokenFor_m1_5679 (DynamicILInfo_t1_493 * __this, RuntimeTypeHandle_t1_30  ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.DynamicILInfo::GetTokenFor(System.String)
extern "C" int32_t DynamicILInfo_GetTokenFor_m1_5680 (DynamicILInfo_t1_493 * __this, String_t* ___literal, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.DynamicILInfo::GetTokenFor(System.RuntimeMethodHandle,System.RuntimeTypeHandle)
extern "C" int32_t DynamicILInfo_GetTokenFor_m1_5681 (DynamicILInfo_t1_493 * __this, RuntimeMethodHandle_t1_479  ___method, RuntimeTypeHandle_t1_30  ___contextType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.DynamicILInfo::SetCode(System.Byte[],System.Int32)
extern "C" void DynamicILInfo_SetCode_m1_5682 (DynamicILInfo_t1_493 * __this, ByteU5BU5D_t1_109* ___code, int32_t ___maxStackSize, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.DynamicILInfo::SetCode(System.Byte*,System.Int32,System.Int32)
extern "C" void DynamicILInfo_SetCode_m1_5683 (DynamicILInfo_t1_493 * __this, uint8_t* ___code, int32_t ___codeSize, int32_t ___maxStackSize, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.DynamicILInfo::SetExceptions(System.Byte[])
extern "C" void DynamicILInfo_SetExceptions_m1_5684 (DynamicILInfo_t1_493 * __this, ByteU5BU5D_t1_109* ___exceptions, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.DynamicILInfo::SetExceptions(System.Byte*,System.Int32)
extern "C" void DynamicILInfo_SetExceptions_m1_5685 (DynamicILInfo_t1_493 * __this, uint8_t* ___exceptions, int32_t ___exceptionsSize, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.DynamicILInfo::SetLocalSignature(System.Byte[])
extern "C" void DynamicILInfo_SetLocalSignature_m1_5686 (DynamicILInfo_t1_493 * __this, ByteU5BU5D_t1_109* ___localSignature, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.DynamicILInfo::SetLocalSignature(System.Byte*,System.Int32)
extern "C" void DynamicILInfo_SetLocalSignature_m1_5687 (DynamicILInfo_t1_493 * __this, uint8_t* ___localSignature, int32_t ___signatureSize, const MethodInfo* method) IL2CPP_METHOD_ATTR;
