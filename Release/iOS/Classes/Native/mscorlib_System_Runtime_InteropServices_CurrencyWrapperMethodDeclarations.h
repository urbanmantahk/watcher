﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.InteropServices.CurrencyWrapper
struct CurrencyWrapper_t1_777;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Decimal.h"

// System.Void System.Runtime.InteropServices.CurrencyWrapper::.ctor(System.Decimal)
extern "C" void CurrencyWrapper__ctor_m1_7628 (CurrencyWrapper_t1_777 * __this, Decimal_t1_19  ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.CurrencyWrapper::.ctor(System.Object)
extern "C" void CurrencyWrapper__ctor_m1_7629 (CurrencyWrapper_t1_777 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Runtime.InteropServices.CurrencyWrapper::get_WrappedObject()
extern "C" Decimal_t1_19  CurrencyWrapper_get_WrappedObject_m1_7630 (CurrencyWrapper_t1_777 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
