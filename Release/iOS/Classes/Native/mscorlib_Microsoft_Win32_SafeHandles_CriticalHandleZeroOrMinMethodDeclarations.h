﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Microsoft.Win32.SafeHandles.CriticalHandleZeroOrMinusOneIsInvalid
struct CriticalHandleZeroOrMinusOneIsInvalid_t1_84;

#include "codegen/il2cpp-codegen.h"

// System.Void Microsoft.Win32.SafeHandles.CriticalHandleZeroOrMinusOneIsInvalid::.ctor()
extern "C" void CriticalHandleZeroOrMinusOneIsInvalid__ctor_m1_1416 (CriticalHandleZeroOrMinusOneIsInvalid_t1_84 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Microsoft.Win32.SafeHandles.CriticalHandleZeroOrMinusOneIsInvalid::get_IsInvalid()
extern "C" bool CriticalHandleZeroOrMinusOneIsInvalid_get_IsInvalid_m1_1417 (CriticalHandleZeroOrMinusOneIsInvalid_t1_84 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
