﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<VoxelBusters.DebugPRO.Internal.ConsoleLog>
struct DefaultComparer_t1_2726;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VoxelBusters_DebugPRO_Internal_ConsoleLog.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<VoxelBusters.DebugPRO.Internal.ConsoleLog>::.ctor()
extern "C" void DefaultComparer__ctor_m1_27278_gshared (DefaultComparer_t1_2726 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1_27278(__this, method) (( void (*) (DefaultComparer_t1_2726 *, const MethodInfo*))DefaultComparer__ctor_m1_27278_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m1_27279_gshared (DefaultComparer_t1_2726 * __this, ConsoleLog_t8_170  ___x, ConsoleLog_t8_170  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m1_27279(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t1_2726 *, ConsoleLog_t8_170 , ConsoleLog_t8_170 , const MethodInfo*))DefaultComparer_Compare_m1_27279_gshared)(__this, ___x, ___y, method)
