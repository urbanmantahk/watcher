﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ArrayList/ListWrapper
struct ListWrapper_t1_269;
// System.Collections.IList
struct IList_t1_262;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.ArrayList/ListWrapper::.ctor(System.Collections.IList)
extern "C" void ListWrapper__ctor_m1_3032 (ListWrapper_t1_269 * __this, Object_t * ___innerList, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.ArrayList/ListWrapper::get_Item(System.Int32)
extern "C" Object_t * ListWrapper_get_Item_m1_3033 (ListWrapper_t1_269 * __this, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/ListWrapper::set_Item(System.Int32,System.Object)
extern "C" void ListWrapper_set_Item_m1_3034 (ListWrapper_t1_269 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.ArrayList/ListWrapper::get_Count()
extern "C" int32_t ListWrapper_get_Count_m1_3035 (ListWrapper_t1_269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.ArrayList/ListWrapper::get_IsSynchronized()
extern "C" bool ListWrapper_get_IsSynchronized_m1_3036 (ListWrapper_t1_269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.ArrayList/ListWrapper::get_SyncRoot()
extern "C" Object_t * ListWrapper_get_SyncRoot_m1_3037 (ListWrapper_t1_269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.ArrayList/ListWrapper::get_IsFixedSize()
extern "C" bool ListWrapper_get_IsFixedSize_m1_3038 (ListWrapper_t1_269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.ArrayList/ListWrapper::get_IsReadOnly()
extern "C" bool ListWrapper_get_IsReadOnly_m1_3039 (ListWrapper_t1_269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.ArrayList/ListWrapper::Add(System.Object)
extern "C" int32_t ListWrapper_Add_m1_3040 (ListWrapper_t1_269 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/ListWrapper::Clear()
extern "C" void ListWrapper_Clear_m1_3041 (ListWrapper_t1_269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.ArrayList/ListWrapper::Contains(System.Object)
extern "C" bool ListWrapper_Contains_m1_3042 (ListWrapper_t1_269 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.ArrayList/ListWrapper::IndexOf(System.Object)
extern "C" int32_t ListWrapper_IndexOf_m1_3043 (ListWrapper_t1_269 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/ListWrapper::Insert(System.Int32,System.Object)
extern "C" void ListWrapper_Insert_m1_3044 (ListWrapper_t1_269 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/ListWrapper::Remove(System.Object)
extern "C" void ListWrapper_Remove_m1_3045 (ListWrapper_t1_269 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/ListWrapper::RemoveAt(System.Int32)
extern "C" void ListWrapper_RemoveAt_m1_3046 (ListWrapper_t1_269 * __this, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/ListWrapper::CopyTo(System.Array,System.Int32)
extern "C" void ListWrapper_CopyTo_m1_3047 (ListWrapper_t1_269 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Collections.ArrayList/ListWrapper::GetEnumerator()
extern "C" Object_t * ListWrapper_GetEnumerator_m1_3048 (ListWrapper_t1_269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
