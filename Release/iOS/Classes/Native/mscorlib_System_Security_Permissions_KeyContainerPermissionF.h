﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Security_Permissions_KeyContainerPermissionF.h"

// System.Security.Permissions.KeyContainerPermissionFlags
struct  KeyContainerPermissionFlags_t1_1293 
{
	// System.Int32 System.Security.Permissions.KeyContainerPermissionFlags::value__
	int32_t ___value___1;
};
