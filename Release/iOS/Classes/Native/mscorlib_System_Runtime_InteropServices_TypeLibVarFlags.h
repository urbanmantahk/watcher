﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Runtime_InteropServices_TypeLibVarFlags.h"

// System.Runtime.InteropServices.TypeLibVarFlags
struct  TypeLibVarFlags_t1_840 
{
	// System.Int32 System.Runtime.InteropServices.TypeLibVarFlags::value__
	int32_t ___value___1;
};
