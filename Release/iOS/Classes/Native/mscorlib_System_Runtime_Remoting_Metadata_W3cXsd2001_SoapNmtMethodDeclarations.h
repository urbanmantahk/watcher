﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNmtoken
struct SoapNmtoken_t1_985;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNmtoken::.ctor()
extern "C" void SoapNmtoken__ctor_m1_8824 (SoapNmtoken_t1_985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNmtoken::.ctor(System.String)
extern "C" void SoapNmtoken__ctor_m1_8825 (SoapNmtoken_t1_985 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNmtoken::get_Value()
extern "C" String_t* SoapNmtoken_get_Value_m1_8826 (SoapNmtoken_t1_985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNmtoken::set_Value(System.String)
extern "C" void SoapNmtoken_set_Value_m1_8827 (SoapNmtoken_t1_985 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNmtoken::get_XsdType()
extern "C" String_t* SoapNmtoken_get_XsdType_m1_8828 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNmtoken::GetXsdType()
extern "C" String_t* SoapNmtoken_GetXsdType_m1_8829 (SoapNmtoken_t1_985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNmtoken System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNmtoken::Parse(System.String)
extern "C" SoapNmtoken_t1_985 * SoapNmtoken_Parse_m1_8830 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNmtoken::ToString()
extern "C" String_t* SoapNmtoken_ToString_m1_8831 (SoapNmtoken_t1_985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
