﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VoxelBusters.NativePlugins.Leaderboard
struct Leaderboard_t8_180;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1_238;

#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Demo_NPDisabled.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eLeaderboardTim.h"

// VoxelBusters.NativePlugins.Demo.GameServicesDemo
struct  GameServicesDemo_t8_179  : public NPDisabledFeatureDemo_t8_175
{
	// VoxelBusters.NativePlugins.eLeaderboardTimeScope VoxelBusters.NativePlugins.Demo.GameServicesDemo::m_timeScope
	int32_t ___m_timeScope_10;
	// System.Int32 VoxelBusters.NativePlugins.Demo.GameServicesDemo::m_maxScoreResults
	int32_t ___m_maxScoreResults_11;
	// VoxelBusters.NativePlugins.Leaderboard VoxelBusters.NativePlugins.Demo.GameServicesDemo::m_curLeaderboard
	Leaderboard_t8_180 * ___m_curLeaderboard_12;
	// System.String VoxelBusters.NativePlugins.Demo.GameServicesDemo::m_curLeaderboardGID
	String_t* ___m_curLeaderboardGID_13;
	// System.Int32 VoxelBusters.NativePlugins.Demo.GameServicesDemo::m_curLeaderboardGIDIndex
	int32_t ___m_curLeaderboardGIDIndex_14;
	// System.String[] VoxelBusters.NativePlugins.Demo.GameServicesDemo::m_leaderboardGIDList
	StringU5BU5D_t1_238* ___m_leaderboardGIDList_15;
	// System.String VoxelBusters.NativePlugins.Demo.GameServicesDemo::m_curAchievementGID
	String_t* ___m_curAchievementGID_16;
	// System.Int32 VoxelBusters.NativePlugins.Demo.GameServicesDemo::m_curAchievementGIDIndex
	int32_t ___m_curAchievementGIDIndex_17;
	// System.String[] VoxelBusters.NativePlugins.Demo.GameServicesDemo::m_achievementGIDList
	StringU5BU5D_t1_238* ___m_achievementGIDList_18;
};
