﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExifLibrary.ExifSInt
struct ExifSInt_t8_124;
// System.Object
struct Object_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTag.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifInterOperability.h"

// System.Void ExifLibrary.ExifSInt::.ctor(ExifLibrary.ExifTag,System.Int32)
extern "C" void ExifSInt__ctor_m8_592 (ExifSInt_t8_124 * __this, int32_t ___tag, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ExifLibrary.ExifSInt::get__Value()
extern "C" Object_t * ExifSInt_get__Value_m8_593 (ExifSInt_t8_124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.ExifSInt::set__Value(System.Object)
extern "C" void ExifSInt_set__Value_m8_594 (ExifSInt_t8_124 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExifLibrary.ExifSInt::get_Value()
extern "C" int32_t ExifSInt_get_Value_m8_595 (ExifSInt_t8_124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.ExifSInt::set_Value(System.Int32)
extern "C" void ExifSInt_set_Value_m8_596 (ExifSInt_t8_124 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExifLibrary.ExifSInt::ToString()
extern "C" String_t* ExifSInt_ToString_m8_597 (ExifSInt_t8_124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.ExifInterOperability ExifLibrary.ExifSInt::get_Interoperability()
extern "C" ExifInterOperability_t8_113  ExifSInt_get_Interoperability_m8_598 (ExifSInt_t8_124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExifLibrary.ExifSInt::op_Implicit(ExifLibrary.ExifSInt)
extern "C" int32_t ExifSInt_op_Implicit_m8_599 (Object_t * __this /* static, unused */, ExifSInt_t8_124 * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
