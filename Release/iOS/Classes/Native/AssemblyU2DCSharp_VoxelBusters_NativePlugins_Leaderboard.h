﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VoxelBusters.NativePlugins.Leaderboard/LoadScoreCompletion
struct LoadScoreCompletion_t8_229;
// System.String
struct String_t;

#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_NPObje.h"

// VoxelBusters.NativePlugins.Leaderboard
struct  Leaderboard_t8_180  : public NPObject_t8_222
{
	// System.Int32 VoxelBusters.NativePlugins.Leaderboard::m_maxResults
	int32_t ___m_maxResults_3;
	// VoxelBusters.NativePlugins.Leaderboard/LoadScoreCompletion VoxelBusters.NativePlugins.Leaderboard::LoadScoreFinishedEvent
	LoadScoreCompletion_t8_229 * ___LoadScoreFinishedEvent_4;
	// System.String VoxelBusters.NativePlugins.Leaderboard::<GlobalIdentifier>k__BackingField
	String_t* ___U3CGlobalIdentifierU3Ek__BackingField_5;
};
