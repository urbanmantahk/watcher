﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VoxelBusters.Utility.EditorScheduler/CallbackFunction
struct CallbackFunction_t8_151;

#include "mscorlib_System_Object.h"

// VoxelBusters.Utility.EditorScheduler
struct  EditorScheduler_t8_152  : public Object_t
{
};
struct EditorScheduler_t8_152_StaticFields{
	// VoxelBusters.Utility.EditorScheduler/CallbackFunction VoxelBusters.Utility.EditorScheduler::ScheduleUpdate
	CallbackFunction_t8_151 * ___ScheduleUpdate_0;
};
