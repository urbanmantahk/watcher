﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_ParameterModifier.h"

// System.Void System.Reflection.ParameterModifier::.ctor(System.Int32)
extern "C" void ParameterModifier__ctor_m1_7251 (ParameterModifier_t1_628 * __this, int32_t ___parameterCount, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.ParameterModifier::get_Item(System.Int32)
extern "C" bool ParameterModifier_get_Item_m1_7252 (ParameterModifier_t1_628 * __this, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.ParameterModifier::set_Item(System.Int32,System.Boolean)
extern "C" void ParameterModifier_set_Item_m1_7253 (ParameterModifier_t1_628 * __this, int32_t ___index, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void ParameterModifier_t1_628_marshal(const ParameterModifier_t1_628& unmarshaled, ParameterModifier_t1_628_marshaled& marshaled);
extern "C" void ParameterModifier_t1_628_marshal_back(const ParameterModifier_t1_628_marshaled& marshaled, ParameterModifier_t1_628& unmarshaled);
extern "C" void ParameterModifier_t1_628_marshal_cleanup(ParameterModifier_t1_628_marshaled& marshaled);
