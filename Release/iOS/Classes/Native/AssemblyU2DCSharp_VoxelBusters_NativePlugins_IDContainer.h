﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// VoxelBusters.NativePlugins.PlatformID[]
struct PlatformIDU5BU5D_t8_329;

#include "mscorlib_System_Object.h"

// VoxelBusters.NativePlugins.IDContainer
struct  IDContainer_t8_328  : public Object_t
{
	// System.String VoxelBusters.NativePlugins.IDContainer::m_globalID
	String_t* ___m_globalID_0;
	// VoxelBusters.NativePlugins.PlatformID[] VoxelBusters.NativePlugins.IDContainer::m_platformIDs
	PlatformIDU5BU5D_t8_329* ___m_platformIDs_1;
};
