﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.CompilerServices.TypeForwardedToAttribute
struct TypeForwardedToAttribute_t1_55;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.CompilerServices.TypeForwardedToAttribute::.ctor(System.Type)
extern "C" void TypeForwardedToAttribute__ctor_m1_1321 (TypeForwardedToAttribute_t1_55 * __this, Type_t * ___destination, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Runtime.CompilerServices.TypeForwardedToAttribute::get_Destination()
extern "C" Type_t * TypeForwardedToAttribute_get_Destination_m1_1322 (TypeForwardedToAttribute_t1_55 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
