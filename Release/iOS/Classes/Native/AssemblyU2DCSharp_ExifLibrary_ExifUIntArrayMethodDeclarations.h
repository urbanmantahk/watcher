﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExifLibrary.ExifUIntArray
struct ExifUIntArray_t8_119;
// System.UInt32[]
struct UInt32U5BU5D_t1_142;
// System.Object
struct Object_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTag.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifInterOperability.h"

// System.Void ExifLibrary.ExifUIntArray::.ctor(ExifLibrary.ExifTag,System.UInt32[])
extern "C" void ExifUIntArray__ctor_m8_557 (ExifUIntArray_t8_119 * __this, int32_t ___tag, UInt32U5BU5D_t1_142* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ExifLibrary.ExifUIntArray::get__Value()
extern "C" Object_t * ExifUIntArray_get__Value_m8_558 (ExifUIntArray_t8_119 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.ExifUIntArray::set__Value(System.Object)
extern "C" void ExifUIntArray_set__Value_m8_559 (ExifUIntArray_t8_119 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32[] ExifLibrary.ExifUIntArray::get_Value()
extern "C" UInt32U5BU5D_t1_142* ExifUIntArray_get_Value_m8_560 (ExifUIntArray_t8_119 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.ExifUIntArray::set_Value(System.UInt32[])
extern "C" void ExifUIntArray_set_Value_m8_561 (ExifUIntArray_t8_119 * __this, UInt32U5BU5D_t1_142* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExifLibrary.ExifUIntArray::ToString()
extern "C" String_t* ExifUIntArray_ToString_m8_562 (ExifUIntArray_t8_119 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.ExifInterOperability ExifLibrary.ExifUIntArray::get_Interoperability()
extern "C" ExifInterOperability_t8_113  ExifUIntArray_get_Interoperability_m8_563 (ExifUIntArray_t8_119 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32[] ExifLibrary.ExifUIntArray::op_Implicit(ExifLibrary.ExifUIntArray)
extern "C" UInt32U5BU5D_t1_142* ExifUIntArray_op_Implicit_m8_564 (Object_t * __this /* static, unused */, ExifUIntArray_t8_119 * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
