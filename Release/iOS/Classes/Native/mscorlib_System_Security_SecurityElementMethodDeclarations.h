﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.SecurityElement
struct SecurityElement_t1_242;
// System.String
struct String_t;
// System.Collections.Hashtable
struct Hashtable_t1_100;
// System.Collections.ArrayList
struct ArrayList_t1_170;
// System.Text.StringBuilder
struct StringBuilder_t1_247;
// System.Security.SecurityElement/SecurityAttribute
struct SecurityAttribute_t1_1398;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.SecurityElement::.ctor(System.String)
extern "C" void SecurityElement__ctor_m1_12033 (SecurityElement_t1_242 * __this, String_t* ___tag, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityElement::.ctor(System.String,System.String)
extern "C" void SecurityElement__ctor_m1_12034 (SecurityElement_t1_242 * __this, String_t* ___tag, String_t* ___text, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityElement::.ctor(System.Security.SecurityElement)
extern "C" void SecurityElement__ctor_m1_12035 (SecurityElement_t1_242 * __this, SecurityElement_t1_242 * ___se, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityElement::.cctor()
extern "C" void SecurityElement__cctor_m1_12036 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Hashtable System.Security.SecurityElement::get_Attributes()
extern "C" Hashtable_t1_100 * SecurityElement_get_Attributes_m1_12037 (SecurityElement_t1_242 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityElement::set_Attributes(System.Collections.Hashtable)
extern "C" void SecurityElement_set_Attributes_m1_12038 (SecurityElement_t1_242 * __this, Hashtable_t1_100 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList System.Security.SecurityElement::get_Children()
extern "C" ArrayList_t1_170 * SecurityElement_get_Children_m1_12039 (SecurityElement_t1_242 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityElement::set_Children(System.Collections.ArrayList)
extern "C" void SecurityElement_set_Children_m1_12040 (SecurityElement_t1_242 * __this, ArrayList_t1_170 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.SecurityElement::get_Tag()
extern "C" String_t* SecurityElement_get_Tag_m1_12041 (SecurityElement_t1_242 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityElement::set_Tag(System.String)
extern "C" void SecurityElement_set_Tag_m1_12042 (SecurityElement_t1_242 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.SecurityElement::get_Text()
extern "C" String_t* SecurityElement_get_Text_m1_12043 (SecurityElement_t1_242 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityElement::set_Text(System.String)
extern "C" void SecurityElement_set_Text_m1_12044 (SecurityElement_t1_242 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityElement::AddAttribute(System.String,System.String)
extern "C" void SecurityElement_AddAttribute_m1_12045 (SecurityElement_t1_242 * __this, String_t* ___name, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityElement::AddChild(System.Security.SecurityElement)
extern "C" void SecurityElement_AddChild_m1_12046 (SecurityElement_t1_242 * __this, SecurityElement_t1_242 * ___child, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.SecurityElement::Attribute(System.String)
extern "C" String_t* SecurityElement_Attribute_m1_12047 (SecurityElement_t1_242 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityElement System.Security.SecurityElement::Copy()
extern "C" SecurityElement_t1_242 * SecurityElement_Copy_m1_12048 (SecurityElement_t1_242 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.SecurityElement::Equal(System.Security.SecurityElement)
extern "C" bool SecurityElement_Equal_m1_12049 (SecurityElement_t1_242 * __this, SecurityElement_t1_242 * ___other, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.SecurityElement::Escape(System.String)
extern "C" String_t* SecurityElement_Escape_m1_12050 (Object_t * __this /* static, unused */, String_t* ___str, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.SecurityElement::Unescape(System.String)
extern "C" String_t* SecurityElement_Unescape_m1_12051 (Object_t * __this /* static, unused */, String_t* ___str, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityElement System.Security.SecurityElement::FromString(System.String)
extern "C" SecurityElement_t1_242 * SecurityElement_FromString_m1_12052 (Object_t * __this /* static, unused */, String_t* ___xml, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.SecurityElement::IsValidAttributeName(System.String)
extern "C" bool SecurityElement_IsValidAttributeName_m1_12053 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.SecurityElement::IsValidAttributeValue(System.String)
extern "C" bool SecurityElement_IsValidAttributeValue_m1_12054 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.SecurityElement::IsValidTag(System.String)
extern "C" bool SecurityElement_IsValidTag_m1_12055 (Object_t * __this /* static, unused */, String_t* ___tag, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.SecurityElement::IsValidText(System.String)
extern "C" bool SecurityElement_IsValidText_m1_12056 (Object_t * __this /* static, unused */, String_t* ___text, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityElement System.Security.SecurityElement::SearchForChildByTag(System.String)
extern "C" SecurityElement_t1_242 * SecurityElement_SearchForChildByTag_m1_12057 (SecurityElement_t1_242 * __this, String_t* ___tag, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.SecurityElement::SearchForTextOfTag(System.String)
extern "C" String_t* SecurityElement_SearchForTextOfTag_m1_12058 (SecurityElement_t1_242 * __this, String_t* ___tag, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.SecurityElement::ToString()
extern "C" String_t* SecurityElement_ToString_m1_12059 (SecurityElement_t1_242 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityElement::ToXml(System.Text.StringBuilder&,System.Int32)
extern "C" void SecurityElement_ToXml_m1_12060 (SecurityElement_t1_242 * __this, StringBuilder_t1_247 ** ___s, int32_t ___level, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityElement/SecurityAttribute System.Security.SecurityElement::GetAttribute(System.String)
extern "C" SecurityAttribute_t1_1398 * SecurityElement_GetAttribute_m1_12061 (SecurityElement_t1_242 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
