﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_ShareI.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_eSocia.h"

// VoxelBusters.NativePlugins.SocialShareComposerBase
struct  SocialShareComposerBase_t8_283  : public ShareImageUtility_t8_280
{
	// VoxelBusters.NativePlugins.Internal.eSocialServiceType VoxelBusters.NativePlugins.SocialShareComposerBase::<ServiceType>k__BackingField
	int32_t ___U3CServiceTypeU3Ek__BackingField_3;
	// System.String VoxelBusters.NativePlugins.SocialShareComposerBase::<Text>k__BackingField
	String_t* ___U3CTextU3Ek__BackingField_4;
	// System.String VoxelBusters.NativePlugins.SocialShareComposerBase::<URL>k__BackingField
	String_t* ___U3CURLU3Ek__BackingField_5;
	// System.Byte[] VoxelBusters.NativePlugins.SocialShareComposerBase::<ImageData>k__BackingField
	ByteU5BU5D_t1_109* ___U3CImageDataU3Ek__BackingField_6;
};
