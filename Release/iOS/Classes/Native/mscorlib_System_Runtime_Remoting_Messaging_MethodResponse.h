﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Reflection.MethodBase
struct MethodBase_t1_335;
// System.Object
struct Object_t;
// System.Exception
struct Exception_t1_33;
// System.Type[]
struct TypeU5BU5D_t1_31;
// System.Runtime.Remoting.Messaging.ArgInfo
struct ArgInfo_t1_915;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Runtime.Remoting.Messaging.IMethodCallMessage
struct IMethodCallMessage_t1_949;
// System.Runtime.Remoting.Messaging.LogicalCallContext
struct LogicalCallContext_t1_941;
// System.Runtime.Remoting.Identity
struct Identity_t1_943;
// System.Collections.IDictionary
struct IDictionary_t1_35;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t1_92;

#include "mscorlib_System_Object.h"

// System.Runtime.Remoting.Messaging.MethodResponse
struct  MethodResponse_t1_936  : public Object_t
{
	// System.String System.Runtime.Remoting.Messaging.MethodResponse::_methodName
	String_t* ____methodName_0;
	// System.String System.Runtime.Remoting.Messaging.MethodResponse::_uri
	String_t* ____uri_1;
	// System.String System.Runtime.Remoting.Messaging.MethodResponse::_typeName
	String_t* ____typeName_2;
	// System.Reflection.MethodBase System.Runtime.Remoting.Messaging.MethodResponse::_methodBase
	MethodBase_t1_335 * ____methodBase_3;
	// System.Object System.Runtime.Remoting.Messaging.MethodResponse::_returnValue
	Object_t * ____returnValue_4;
	// System.Exception System.Runtime.Remoting.Messaging.MethodResponse::_exception
	Exception_t1_33 * ____exception_5;
	// System.Type[] System.Runtime.Remoting.Messaging.MethodResponse::_methodSignature
	TypeU5BU5D_t1_31* ____methodSignature_6;
	// System.Runtime.Remoting.Messaging.ArgInfo System.Runtime.Remoting.Messaging.MethodResponse::_inArgInfo
	ArgInfo_t1_915 * ____inArgInfo_7;
	// System.Object[] System.Runtime.Remoting.Messaging.MethodResponse::_args
	ObjectU5BU5D_t1_272* ____args_8;
	// System.Object[] System.Runtime.Remoting.Messaging.MethodResponse::_outArgs
	ObjectU5BU5D_t1_272* ____outArgs_9;
	// System.Runtime.Remoting.Messaging.IMethodCallMessage System.Runtime.Remoting.Messaging.MethodResponse::_callMsg
	Object_t * ____callMsg_10;
	// System.Runtime.Remoting.Messaging.LogicalCallContext System.Runtime.Remoting.Messaging.MethodResponse::_callContext
	LogicalCallContext_t1_941 * ____callContext_11;
	// System.Runtime.Remoting.Identity System.Runtime.Remoting.Messaging.MethodResponse::_targetIdentity
	Identity_t1_943 * ____targetIdentity_12;
	// System.Collections.IDictionary System.Runtime.Remoting.Messaging.MethodResponse::ExternalProperties
	Object_t * ___ExternalProperties_13;
	// System.Collections.IDictionary System.Runtime.Remoting.Messaging.MethodResponse::InternalProperties
	Object_t * ___InternalProperties_14;
};
struct MethodResponse_t1_936_StaticFields{
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Runtime.Remoting.Messaging.MethodResponse::<>f__switch$map25
	Dictionary_2_t1_92 * ___U3CU3Ef__switchU24map25_15;
};
