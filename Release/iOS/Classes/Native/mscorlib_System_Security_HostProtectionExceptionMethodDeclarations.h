﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.HostProtectionException
struct HostProtectionException_t1_1388;
// System.String
struct String_t;
// System.Exception
struct Exception_t1_33;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Permissions_HostProtectionResource.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.Security.HostProtectionException::.ctor()
extern "C" void HostProtectionException__ctor_m1_11925 (HostProtectionException_t1_1388 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.HostProtectionException::.ctor(System.String)
extern "C" void HostProtectionException__ctor_m1_11926 (HostProtectionException_t1_1388 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.HostProtectionException::.ctor(System.String,System.Exception)
extern "C" void HostProtectionException__ctor_m1_11927 (HostProtectionException_t1_1388 * __this, String_t* ___message, Exception_t1_33 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.HostProtectionException::.ctor(System.String,System.Security.Permissions.HostProtectionResource,System.Security.Permissions.HostProtectionResource)
extern "C" void HostProtectionException__ctor_m1_11928 (HostProtectionException_t1_1388 * __this, String_t* ___message, int32_t ___protectedResources, int32_t ___demandedResources, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.HostProtectionException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void HostProtectionException__ctor_m1_11929 (HostProtectionException_t1_1388 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Permissions.HostProtectionResource System.Security.HostProtectionException::get_DemandedResources()
extern "C" int32_t HostProtectionException_get_DemandedResources_m1_11930 (HostProtectionException_t1_1388 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Permissions.HostProtectionResource System.Security.HostProtectionException::get_ProtectedResources()
extern "C" int32_t HostProtectionException_get_ProtectedResources_m1_11931 (HostProtectionException_t1_1388 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.HostProtectionException::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void HostProtectionException_GetObjectData_m1_11932 (HostProtectionException_t1_1388 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.HostProtectionException::ToString()
extern "C" String_t* HostProtectionException_ToString_m1_11933 (HostProtectionException_t1_1388 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
