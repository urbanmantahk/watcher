﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.InteropServices.ComConversionLossAttribute
struct ComConversionLossAttribute_t1_769;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.InteropServices.ComConversionLossAttribute::.ctor()
extern "C" void ComConversionLossAttribute__ctor_m1_7606 (ComConversionLossAttribute_t1_769 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
