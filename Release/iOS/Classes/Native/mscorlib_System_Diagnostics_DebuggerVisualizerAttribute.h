﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Type
struct Type_t;

#include "mscorlib_System_Attribute.h"

// System.Diagnostics.DebuggerVisualizerAttribute
struct  DebuggerVisualizerAttribute_t1_333  : public Attribute_t1_2
{
	// System.String System.Diagnostics.DebuggerVisualizerAttribute::description
	String_t* ___description_0;
	// System.String System.Diagnostics.DebuggerVisualizerAttribute::visualizerSourceName
	String_t* ___visualizerSourceName_1;
	// System.String System.Diagnostics.DebuggerVisualizerAttribute::visualizerName
	String_t* ___visualizerName_2;
	// System.String System.Diagnostics.DebuggerVisualizerAttribute::targetTypeName
	String_t* ___targetTypeName_3;
	// System.Type System.Diagnostics.DebuggerVisualizerAttribute::target
	Type_t * ___target_4;
};
