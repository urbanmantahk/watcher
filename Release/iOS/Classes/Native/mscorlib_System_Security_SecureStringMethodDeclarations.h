﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.SecureString
struct SecureString_t1_1197;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.SecureString::.ctor()
extern "C" void SecureString__ctor_m1_11994 (SecureString_t1_1197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecureString::.ctor(System.Char*,System.Int32)
extern "C" void SecureString__ctor_m1_11995 (SecureString_t1_1197 * __this, uint16_t* ___value, int32_t ___length, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecureString::.cctor()
extern "C" void SecureString__cctor_m1_11996 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.SecureString::get_Length()
extern "C" int32_t SecureString_get_Length_m1_11997 (SecureString_t1_1197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecureString::AppendChar(System.Char)
extern "C" void SecureString_AppendChar_m1_11998 (SecureString_t1_1197 * __this, uint16_t ___c, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecureString::Clear()
extern "C" void SecureString_Clear_m1_11999 (SecureString_t1_1197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecureString System.Security.SecureString::Copy()
extern "C" SecureString_t1_1197 * SecureString_Copy_m1_12000 (SecureString_t1_1197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecureString::Dispose()
extern "C" void SecureString_Dispose_m1_12001 (SecureString_t1_1197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecureString::InsertAt(System.Int32,System.Char)
extern "C" void SecureString_InsertAt_m1_12002 (SecureString_t1_1197 * __this, int32_t ___index, uint16_t ___c, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.SecureString::IsReadOnly()
extern "C" bool SecureString_IsReadOnly_m1_12003 (SecureString_t1_1197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecureString::MakeReadOnly()
extern "C" void SecureString_MakeReadOnly_m1_12004 (SecureString_t1_1197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecureString::RemoveAt(System.Int32)
extern "C" void SecureString_RemoveAt_m1_12005 (SecureString_t1_1197 * __this, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecureString::SetAt(System.Int32,System.Char)
extern "C" void SecureString_SetAt_m1_12006 (SecureString_t1_1197 * __this, int32_t ___index, uint16_t ___c, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecureString::Encrypt()
extern "C" void SecureString_Encrypt_m1_12007 (SecureString_t1_1197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecureString::Decrypt()
extern "C" void SecureString_Decrypt_m1_12008 (SecureString_t1_1197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecureString::Alloc(System.Int32,System.Boolean)
extern "C" void SecureString_Alloc_m1_12009 (SecureString_t1_1197 * __this, int32_t ___length, bool ___realloc, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.SecureString::GetBuffer()
extern "C" ByteU5BU5D_t1_109* SecureString_GetBuffer_m1_12010 (SecureString_t1_1197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
