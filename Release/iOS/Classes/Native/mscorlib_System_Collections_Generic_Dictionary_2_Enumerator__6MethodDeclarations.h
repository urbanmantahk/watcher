﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__5MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1_18483(__this, ___dictionary, method) (( void (*) (Enumerator_t1_2240 *, Dictionary_2_t1_1812 *, const MethodInfo*))Enumerator__ctor_m1_18376_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_18484(__this, method) (( Object_t * (*) (Enumerator_t1_2240 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_18377_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1_18485(__this, method) (( void (*) (Enumerator_t1_2240 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_18378_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_18486(__this, method) (( DictionaryEntry_t1_284  (*) (Enumerator_t1_2240 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_18379_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_18487(__this, method) (( Object_t * (*) (Enumerator_t1_2240 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_18380_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_18488(__this, method) (( Object_t * (*) (Enumerator_t1_2240 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_18381_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::MoveNext()
#define Enumerator_MoveNext_m1_18489(__this, method) (( bool (*) (Enumerator_t1_2240 *, const MethodInfo*))Enumerator_MoveNext_m1_18382_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::get_Current()
#define Enumerator_get_Current_m1_18490(__this, method) (( KeyValuePair_2_t1_2237  (*) (Enumerator_t1_2240 *, const MethodInfo*))Enumerator_get_Current_m1_18383_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1_18491(__this, method) (( String_t* (*) (Enumerator_t1_2240 *, const MethodInfo*))Enumerator_get_CurrentKey_m1_18384_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1_18492(__this, method) (( bool (*) (Enumerator_t1_2240 *, const MethodInfo*))Enumerator_get_CurrentValue_m1_18385_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::Reset()
#define Enumerator_Reset_m1_18493(__this, method) (( void (*) (Enumerator_t1_2240 *, const MethodInfo*))Enumerator_Reset_m1_18386_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::VerifyState()
#define Enumerator_VerifyState_m1_18494(__this, method) (( void (*) (Enumerator_t1_2240 *, const MethodInfo*))Enumerator_VerifyState_m1_18387_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1_18495(__this, method) (( void (*) (Enumerator_t1_2240 *, const MethodInfo*))Enumerator_VerifyCurrent_m1_18388_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::Dispose()
#define Enumerator_Dispose_m1_18496(__this, method) (( void (*) (Enumerator_t1_2240 *, const MethodInfo*))Enumerator_Dispose_m1_18389_gshared)(__this, method)
