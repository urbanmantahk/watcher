﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.ContextStaticAttribute
struct ContextStaticAttribute_t1_1520;

#include "codegen/il2cpp-codegen.h"

// System.Void System.ContextStaticAttribute::.ctor()
extern "C" void ContextStaticAttribute__ctor_m1_13435 (ContextStaticAttribute_t1_1520 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
