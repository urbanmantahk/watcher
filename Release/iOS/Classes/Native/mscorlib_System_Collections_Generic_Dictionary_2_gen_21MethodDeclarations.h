﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t1_2018;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t1_1980;
// System.Collections.Generic.IDictionary`2<System.Object,System.Object>
struct IDictionary_2_t1_2776;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t1_2012;
// System.Collections.ICollection
struct ICollection_t1_280;
// System.Object
struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t1_2186;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t1_2777;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1_863;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>
struct KeyCollection_t1_2020;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>
struct ValueCollection_t1_2024;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__4.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor()
extern "C" void Dictionary_2__ctor_m1_15914_gshared (Dictionary_2_t1_2018 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m1_15914(__this, method) (( void (*) (Dictionary_2_t1_2018 *, const MethodInfo*))Dictionary_2__ctor_m1_15914_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m1_15915_gshared (Dictionary_2_t1_2018 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m1_15915(__this, ___comparer, method) (( void (*) (Dictionary_2_t1_2018 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m1_15915_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C" void Dictionary_2__ctor_m1_15916_gshared (Dictionary_2_t1_2018 * __this, Object_t* ___dictionary, const MethodInfo* method);
#define Dictionary_2__ctor_m1_15916(__this, ___dictionary, method) (( void (*) (Dictionary_2_t1_2018 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m1_15916_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m1_15917_gshared (Dictionary_2_t1_2018 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m1_15917(__this, ___capacity, method) (( void (*) (Dictionary_2_t1_2018 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m1_15917_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m1_15918_gshared (Dictionary_2_t1_2018 * __this, Object_t* ___dictionary, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m1_15918(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t1_2018 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m1_15918_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m1_15919_gshared (Dictionary_2_t1_2018 * __this, int32_t ___capacity, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m1_15919(__this, ___capacity, ___comparer, method) (( void (*) (Dictionary_2_t1_2018 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2__ctor_m1_15919_gshared)(__this, ___capacity, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m1_15920_gshared (Dictionary_2_t1_2018 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m1_15920(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1_2018 *, SerializationInfo_t1_293 *, StreamingContext_t1_1050 , const MethodInfo*))Dictionary_2__ctor_m1_15920_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1_15921_gshared (Dictionary_2_t1_2018 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1_15921(__this, method) (( Object_t* (*) (Dictionary_2_t1_2018 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1_15921_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1_15922_gshared (Dictionary_2_t1_2018 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1_15922(__this, method) (( Object_t* (*) (Dictionary_2_t1_2018 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1_15922_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Keys_m1_15923_gshared (Dictionary_2_t1_2018 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m1_15923(__this, method) (( Object_t * (*) (Dictionary_2_t1_2018 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m1_15923_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.IDictionary.get_Values()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Values_m1_15924_gshared (Dictionary_2_t1_2018 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m1_15924(__this, method) (( Object_t * (*) (Dictionary_2_t1_2018 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m1_15924_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.IDictionary.get_IsFixedSize()
extern "C" bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1_15925_gshared (Dictionary_2_t1_2018 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1_15925(__this, method) (( bool (*) (Dictionary_2_t1_2018 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1_15925_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.IDictionary.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1_15926_gshared (Dictionary_2_t1_2018 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1_15926(__this, method) (( bool (*) (Dictionary_2_t1_2018 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1_15926_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m1_15927_gshared (Dictionary_2_t1_2018 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m1_15927(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t1_2018 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m1_15927_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m1_15928_gshared (Dictionary_2_t1_2018 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m1_15928(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1_2018 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m1_15928_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m1_15929_gshared (Dictionary_2_t1_2018 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m1_15929(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1_2018 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m1_15929_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m1_15930_gshared (Dictionary_2_t1_2018 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m1_15930(__this, ___key, method) (( bool (*) (Dictionary_2_t1_2018 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m1_15930_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m1_15931_gshared (Dictionary_2_t1_2018 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m1_15931(__this, ___key, method) (( void (*) (Dictionary_2_t1_2018 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m1_15931_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1_15932_gshared (Dictionary_2_t1_2018 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1_15932(__this, method) (( bool (*) (Dictionary_2_t1_2018 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1_15932_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1_15933_gshared (Dictionary_2_t1_2018 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1_15933(__this, method) (( Object_t * (*) (Dictionary_2_t1_2018 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1_15933_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1_15934_gshared (Dictionary_2_t1_2018 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1_15934(__this, method) (( bool (*) (Dictionary_2_t1_2018 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1_15934_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1_15935_gshared (Dictionary_2_t1_2018 * __this, KeyValuePair_2_t1_2015  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1_15935(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t1_2018 *, KeyValuePair_2_t1_2015 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1_15935_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1_15936_gshared (Dictionary_2_t1_2018 * __this, KeyValuePair_2_t1_2015  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1_15936(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1_2018 *, KeyValuePair_2_t1_2015 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1_15936_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1_15937_gshared (Dictionary_2_t1_2018 * __this, KeyValuePair_2U5BU5D_t1_2186* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1_15937(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1_2018 *, KeyValuePair_2U5BU5D_t1_2186*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1_15937_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1_15938_gshared (Dictionary_2_t1_2018 * __this, KeyValuePair_2_t1_2015  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1_15938(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1_2018 *, KeyValuePair_2_t1_2015 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1_15938_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m1_15939_gshared (Dictionary_2_t1_2018 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m1_15939(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1_2018 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m1_15939_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1_15940_gshared (Dictionary_2_t1_2018 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1_15940(__this, method) (( Object_t * (*) (Dictionary_2_t1_2018 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1_15940_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1_15941_gshared (Dictionary_2_t1_2018 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1_15941(__this, method) (( Object_t* (*) (Dictionary_2_t1_2018 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1_15941_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1_15942_gshared (Dictionary_2_t1_2018 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1_15942(__this, method) (( Object_t * (*) (Dictionary_2_t1_2018 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1_15942_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m1_15943_gshared (Dictionary_2_t1_2018 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m1_15943(__this, method) (( int32_t (*) (Dictionary_2_t1_2018 *, const MethodInfo*))Dictionary_2_get_Count_m1_15943_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Item(TKey)
extern "C" Object_t * Dictionary_2_get_Item_m1_15944_gshared (Dictionary_2_t1_2018 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m1_15944(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t1_2018 *, Object_t *, const MethodInfo*))Dictionary_2_get_Item_m1_15944_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m1_15945_gshared (Dictionary_2_t1_2018 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m1_15945(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1_2018 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_set_Item_m1_15945_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m1_15946_gshared (Dictionary_2_t1_2018 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m1_15946(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t1_2018 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m1_15946_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m1_15947_gshared (Dictionary_2_t1_2018 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m1_15947(__this, ___size, method) (( void (*) (Dictionary_2_t1_2018 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m1_15947_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m1_15948_gshared (Dictionary_2_t1_2018 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m1_15948(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1_2018 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m1_15948_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Object>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t1_2015  Dictionary_2_make_pair_m1_15949_gshared (Object_t * __this /* static, unused */, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m1_15949(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t1_2015  (*) (Object_t * /* static, unused */, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_make_pair_m1_15949_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Object>::pick_key(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_key_m1_15950_gshared (Object_t * __this /* static, unused */, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m1_15950(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_pick_key_m1_15950_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Object>::pick_value(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_value_m1_15951_gshared (Object_t * __this /* static, unused */, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m1_15951(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_pick_value_m1_15951_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m1_15952_gshared (Dictionary_2_t1_2018 * __this, KeyValuePair_2U5BU5D_t1_2186* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m1_15952(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1_2018 *, KeyValuePair_2U5BU5D_t1_2186*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m1_15952_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Resize()
extern "C" void Dictionary_2_Resize_m1_15953_gshared (Dictionary_2_t1_2018 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m1_15953(__this, method) (( void (*) (Dictionary_2_t1_2018 *, const MethodInfo*))Dictionary_2_Resize_m1_15953_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m1_15954_gshared (Dictionary_2_t1_2018 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_Add_m1_15954(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1_2018 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_Add_m1_15954_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Comparer()
extern "C" Object_t* Dictionary_2_get_Comparer_m1_15955_gshared (Dictionary_2_t1_2018 * __this, const MethodInfo* method);
#define Dictionary_2_get_Comparer_m1_15955(__this, method) (( Object_t* (*) (Dictionary_2_t1_2018 *, const MethodInfo*))Dictionary_2_get_Comparer_m1_15955_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Clear()
extern "C" void Dictionary_2_Clear_m1_15956_gshared (Dictionary_2_t1_2018 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m1_15956(__this, method) (( void (*) (Dictionary_2_t1_2018 *, const MethodInfo*))Dictionary_2_Clear_m1_15956_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m1_15957_gshared (Dictionary_2_t1_2018 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m1_15957(__this, ___key, method) (( bool (*) (Dictionary_2_t1_2018 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsKey_m1_15957_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m1_15958_gshared (Dictionary_2_t1_2018 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m1_15958(__this, ___value, method) (( bool (*) (Dictionary_2_t1_2018 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsValue_m1_15958_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m1_15959_gshared (Dictionary_2_t1_2018 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m1_15959(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1_2018 *, SerializationInfo_t1_293 *, StreamingContext_t1_1050 , const MethodInfo*))Dictionary_2_GetObjectData_m1_15959_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m1_15960_gshared (Dictionary_2_t1_2018 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m1_15960(__this, ___sender, method) (( void (*) (Dictionary_2_t1_2018 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m1_15960_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m1_15961_gshared (Dictionary_2_t1_2018 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m1_15961(__this, ___key, method) (( bool (*) (Dictionary_2_t1_2018 *, Object_t *, const MethodInfo*))Dictionary_2_Remove_m1_15961_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m1_15962_gshared (Dictionary_2_t1_2018 * __this, Object_t * ___key, Object_t ** ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m1_15962(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t1_2018 *, Object_t *, Object_t **, const MethodInfo*))Dictionary_2_TryGetValue_m1_15962_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Keys()
extern "C" KeyCollection_t1_2020 * Dictionary_2_get_Keys_m1_15963_gshared (Dictionary_2_t1_2018 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m1_15963(__this, method) (( KeyCollection_t1_2020 * (*) (Dictionary_2_t1_2018 *, const MethodInfo*))Dictionary_2_get_Keys_m1_15963_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Values()
extern "C" ValueCollection_t1_2024 * Dictionary_2_get_Values_m1_15964_gshared (Dictionary_2_t1_2018 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m1_15964(__this, method) (( ValueCollection_t1_2024 * (*) (Dictionary_2_t1_2018 *, const MethodInfo*))Dictionary_2_get_Values_m1_15964_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ToTKey(System.Object)
extern "C" Object_t * Dictionary_2_ToTKey_m1_15965_gshared (Dictionary_2_t1_2018 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m1_15965(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t1_2018 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m1_15965_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ToTValue(System.Object)
extern "C" Object_t * Dictionary_2_ToTValue_m1_15966_gshared (Dictionary_2_t1_2018 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m1_15966(__this, ___value, method) (( Object_t * (*) (Dictionary_2_t1_2018 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m1_15966_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m1_15967_gshared (Dictionary_2_t1_2018 * __this, KeyValuePair_2_t1_2015  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m1_15967(__this, ___pair, method) (( bool (*) (Dictionary_2_t1_2018 *, KeyValuePair_2_t1_2015 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m1_15967_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Object>::GetEnumerator()
extern "C" Enumerator_t1_2022  Dictionary_2_GetEnumerator_m1_15968_gshared (Dictionary_2_t1_2018 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m1_15968(__this, method) (( Enumerator_t1_2022  (*) (Dictionary_2_t1_2018 *, const MethodInfo*))Dictionary_2_GetEnumerator_m1_15968_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,System.Object>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t1_284  Dictionary_2_U3CCopyToU3Em__0_m1_15969_gshared (Object_t * __this /* static, unused */, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m1_15969(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t1_284  (*) (Object_t * /* static, unused */, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m1_15969_gshared)(__this /* static, unused */, ___key, ___value, method)
