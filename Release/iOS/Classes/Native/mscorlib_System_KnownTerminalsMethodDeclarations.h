﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "codegen/il2cpp-codegen.h"

// System.Byte[] System.KnownTerminals::get_linux()
extern "C" ByteU5BU5D_t1_109* KnownTerminals_get_linux_m1_14178 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.KnownTerminals::get_xterm()
extern "C" ByteU5BU5D_t1_109* KnownTerminals_get_xterm_m1_14179 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.KnownTerminals::get_ansi()
extern "C" ByteU5BU5D_t1_109* KnownTerminals_get_ansi_m1_14180 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
