﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.WebViewMessage
struct WebViewMessage_t8_314;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t1_1839;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.WebViewMessage::.ctor()
extern "C" void WebViewMessage__ctor_m8_1852 (WebViewMessage_t8_314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.WebViewMessage::get_SchemeName()
extern "C" String_t* WebViewMessage_get_SchemeName_m8_1853 (WebViewMessage_t8_314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.WebViewMessage::set_SchemeName(System.String)
extern "C" void WebViewMessage_set_SchemeName_m8_1854 (WebViewMessage_t8_314 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.WebViewMessage::get_Host()
extern "C" String_t* WebViewMessage_get_Host_m8_1855 (WebViewMessage_t8_314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.WebViewMessage::set_Host(System.String)
extern "C" void WebViewMessage_set_Host_m8_1856 (WebViewMessage_t8_314 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> VoxelBusters.NativePlugins.WebViewMessage::get_Arguments()
extern "C" Dictionary_2_t1_1839 * WebViewMessage_get_Arguments_m8_1857 (WebViewMessage_t8_314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.WebViewMessage::set_Arguments(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C" void WebViewMessage_set_Arguments_m8_1858 (WebViewMessage_t8_314 * __this, Dictionary_2_t1_1839 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.WebViewMessage::ToString()
extern "C" String_t* WebViewMessage_ToString_m8_1859 (WebViewMessage_t8_314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
