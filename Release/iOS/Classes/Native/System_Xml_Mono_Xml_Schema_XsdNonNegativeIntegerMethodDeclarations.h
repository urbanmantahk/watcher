﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Xml.Schema.XsdNonNegativeInteger
struct XsdNonNegativeInteger_t4_27;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Xml.Schema.XsdNonNegativeInteger::.ctor()
extern "C" void XsdNonNegativeInteger__ctor_m4_42 (XsdNonNegativeInteger_t4_27 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
