﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Resources.ResourceManager
struct ResourceManager_t1_645;
// System.Type
struct Type_t;
// System.String
struct String_t;
// System.Reflection.Assembly
struct Assembly_t1_467;
// System.Collections.Hashtable
struct Hashtable_t1_100;
// System.Object
struct Object_t;
// System.Globalization.CultureInfo
struct CultureInfo_t1_277;
// System.Resources.ResourceSet
struct ResourceSet_t1_655;
// System.IO.Stream
struct Stream_t1_405;
// System.IO.UnmanagedMemoryStream
struct UnmanagedMemoryStream_t1_460;
// System.Version
struct Version_t1_578;
// System.Resources.MissingManifestResourceException
struct MissingManifestResourceException_t1_642;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Resources_UltimateResourceFallbackLocation.h"

// System.Void System.Resources.ResourceManager::.ctor()
extern "C" void ResourceManager__ctor_m1_7360 (ResourceManager_t1_645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.ResourceManager::.ctor(System.Type)
extern "C" void ResourceManager__ctor_m1_7361 (ResourceManager_t1_645 * __this, Type_t * ___resourceSource, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.ResourceManager::.ctor(System.String,System.Reflection.Assembly)
extern "C" void ResourceManager__ctor_m1_7362 (ResourceManager_t1_645 * __this, String_t* ___baseName, Assembly_t1_467 * ___assembly, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.ResourceManager::.ctor(System.String,System.Reflection.Assembly,System.Type)
extern "C" void ResourceManager__ctor_m1_7363 (ResourceManager_t1_645 * __this, String_t* ___baseName, Assembly_t1_467 * ___assembly, Type_t * ___usingResourceSet, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.ResourceManager::.ctor(System.String,System.String,System.Type)
extern "C" void ResourceManager__ctor_m1_7364 (ResourceManager_t1_645 * __this, String_t* ___baseName, String_t* ___resourceDir, Type_t * ___usingResourceSet, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.ResourceManager::.cctor()
extern "C" void ResourceManager__cctor_m1_7365 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Hashtable System.Resources.ResourceManager::GetResourceSets(System.Reflection.Assembly,System.String)
extern "C" Hashtable_t1_100 * ResourceManager_GetResourceSets_m1_7366 (Object_t * __this /* static, unused */, Assembly_t1_467 * ___assembly, String_t* ___basename, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Resources.ResourceManager::CheckResourceSetType(System.Type,System.Boolean)
extern "C" Type_t * ResourceManager_CheckResourceSetType_m1_7367 (ResourceManager_t1_645 * __this, Type_t * ___usingResourceSet, bool ___verifyType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Resources.ResourceManager System.Resources.ResourceManager::CreateFileBasedResourceManager(System.String,System.String,System.Type)
extern "C" ResourceManager_t1_645 * ResourceManager_CreateFileBasedResourceManager_m1_7368 (Object_t * __this /* static, unused */, String_t* ___baseName, String_t* ___resourceDir, Type_t * ___usingResourceSet, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Resources.ResourceManager::get_BaseName()
extern "C" String_t* ResourceManager_get_BaseName_m1_7369 (ResourceManager_t1_645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Resources.ResourceManager::get_IgnoreCase()
extern "C" bool ResourceManager_get_IgnoreCase_m1_7370 (ResourceManager_t1_645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.ResourceManager::set_IgnoreCase(System.Boolean)
extern "C" void ResourceManager_set_IgnoreCase_m1_7371 (ResourceManager_t1_645 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Resources.ResourceManager::get_ResourceSetType()
extern "C" Type_t * ResourceManager_get_ResourceSetType_m1_7372 (ResourceManager_t1_645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Resources.ResourceManager::GetObject(System.String)
extern "C" Object_t * ResourceManager_GetObject_m1_7373 (ResourceManager_t1_645 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Resources.ResourceManager::GetObject(System.String,System.Globalization.CultureInfo)
extern "C" Object_t * ResourceManager_GetObject_m1_7374 (ResourceManager_t1_645 * __this, String_t* ___name, CultureInfo_t1_277 * ___culture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Resources.ResourceSet System.Resources.ResourceManager::GetResourceSet(System.Globalization.CultureInfo,System.Boolean,System.Boolean)
extern "C" ResourceSet_t1_655 * ResourceManager_GetResourceSet_m1_7375 (ResourceManager_t1_645 * __this, CultureInfo_t1_277 * ___culture, bool ___createIfNotExists, bool ___tryParents, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Resources.ResourceManager::GetString(System.String)
extern "C" String_t* ResourceManager_GetString_m1_7376 (ResourceManager_t1_645 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Resources.ResourceManager::GetString(System.String,System.Globalization.CultureInfo)
extern "C" String_t* ResourceManager_GetString_m1_7377 (ResourceManager_t1_645 * __this, String_t* ___name, CultureInfo_t1_277 * ___culture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Resources.ResourceManager::GetResourceFileName(System.Globalization.CultureInfo)
extern "C" String_t* ResourceManager_GetResourceFileName_m1_7378 (ResourceManager_t1_645 * __this, CultureInfo_t1_277 * ___culture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Resources.ResourceManager::GetResourceFilePath(System.Globalization.CultureInfo)
extern "C" String_t* ResourceManager_GetResourceFilePath_m1_7379 (ResourceManager_t1_645 * __this, CultureInfo_t1_277 * ___culture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream System.Resources.ResourceManager::GetManifestResourceStreamNoCase(System.Reflection.Assembly,System.String)
extern "C" Stream_t1_405 * ResourceManager_GetManifestResourceStreamNoCase_m1_7380 (ResourceManager_t1_645 * __this, Assembly_t1_467 * ___ass, String_t* ___fn, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.UnmanagedMemoryStream System.Resources.ResourceManager::GetStream(System.String)
extern "C" UnmanagedMemoryStream_t1_460 * ResourceManager_GetStream_m1_7381 (ResourceManager_t1_645 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.UnmanagedMemoryStream System.Resources.ResourceManager::GetStream(System.String,System.Globalization.CultureInfo)
extern "C" UnmanagedMemoryStream_t1_460 * ResourceManager_GetStream_m1_7382 (ResourceManager_t1_645 * __this, String_t* ___name, CultureInfo_t1_277 * ___culture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Resources.ResourceSet System.Resources.ResourceManager::InternalGetResourceSet(System.Globalization.CultureInfo,System.Boolean,System.Boolean)
extern "C" ResourceSet_t1_655 * ResourceManager_InternalGetResourceSet_m1_7383 (ResourceManager_t1_645 * __this, CultureInfo_t1_277 * ___culture, bool ___createIfNotExists, bool ___tryParents, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.ResourceManager::ReleaseAllResources()
extern "C" void ResourceManager_ReleaseAllResources_m1_7384 (ResourceManager_t1_645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.CultureInfo System.Resources.ResourceManager::GetNeutralResourcesLanguage(System.Reflection.Assembly)
extern "C" CultureInfo_t1_277 * ResourceManager_GetNeutralResourcesLanguage_m1_7385 (Object_t * __this /* static, unused */, Assembly_t1_467 * ___a, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Version System.Resources.ResourceManager::GetSatelliteContractVersion(System.Reflection.Assembly)
extern "C" Version_t1_578 * ResourceManager_GetSatelliteContractVersion_m1_7386 (Object_t * __this /* static, unused */, Assembly_t1_467 * ___a, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Resources.UltimateResourceFallbackLocation System.Resources.ResourceManager::get_FallbackLocation()
extern "C" int32_t ResourceManager_get_FallbackLocation_m1_7387 (ResourceManager_t1_645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.ResourceManager::set_FallbackLocation(System.Resources.UltimateResourceFallbackLocation)
extern "C" void ResourceManager_set_FallbackLocation_m1_7388 (ResourceManager_t1_645 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Resources.MissingManifestResourceException System.Resources.ResourceManager::AssemblyResourceMissing(System.String)
extern "C" MissingManifestResourceException_t1_642 * ResourceManager_AssemblyResourceMissing_m1_7389 (ResourceManager_t1_645 * __this, String_t* ___fileName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Resources.ResourceManager::GetManifestResourceName(System.String)
extern "C" String_t* ResourceManager_GetManifestResourceName_m1_7390 (ResourceManager_t1_645 * __this, String_t* ___fn, const MethodInfo* method) IL2CPP_METHOD_ATTR;
