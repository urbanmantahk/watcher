﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_AdvancedScriptableObj_1MethodDeclarations.h"

// System.Void VoxelBusters.Utility.AdvancedScriptableObject`1<VoxelBusters.NativePlugins.NPSettings>::.ctor()
#define AdvancedScriptableObject_1__ctor_m8_2023(__this, method) (( void (*) (AdvancedScriptableObject_1_t8_336 *, const MethodInfo*))AdvancedScriptableObject_1__ctor_m8_2047_gshared)(__this, method)
// System.Void VoxelBusters.Utility.AdvancedScriptableObject`1<VoxelBusters.NativePlugins.NPSettings>::.cctor()
#define AdvancedScriptableObject_1__cctor_m8_2371(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))AdvancedScriptableObject_1__cctor_m8_2049_gshared)(__this /* static, unused */, method)
// T VoxelBusters.Utility.AdvancedScriptableObject`1<VoxelBusters.NativePlugins.NPSettings>::get_Instance()
#define AdvancedScriptableObject_1_get_Instance_m8_2024(__this /* static, unused */, method) (( NPSettings_t8_335 * (*) (Object_t * /* static, unused */, const MethodInfo*))AdvancedScriptableObject_1_get_Instance_m8_2051_gshared)(__this /* static, unused */, method)
// System.Void VoxelBusters.Utility.AdvancedScriptableObject`1<VoxelBusters.NativePlugins.NPSettings>::Reset()
#define AdvancedScriptableObject_1_Reset_m8_2025(__this, method) (( void (*) (AdvancedScriptableObject_1_t8_336 *, const MethodInfo*))AdvancedScriptableObject_1_Reset_m8_2053_gshared)(__this, method)
// System.Void VoxelBusters.Utility.AdvancedScriptableObject`1<VoxelBusters.NativePlugins.NPSettings>::OnEnable()
#define AdvancedScriptableObject_1_OnEnable_m8_2026(__this, method) (( void (*) (AdvancedScriptableObject_1_t8_336 *, const MethodInfo*))AdvancedScriptableObject_1_OnEnable_m8_2054_gshared)(__this, method)
// System.Void VoxelBusters.Utility.AdvancedScriptableObject`1<VoxelBusters.NativePlugins.NPSettings>::OnDisable()
#define AdvancedScriptableObject_1_OnDisable_m8_2372(__this, method) (( void (*) (AdvancedScriptableObject_1_t8_336 *, const MethodInfo*))AdvancedScriptableObject_1_OnDisable_m8_2056_gshared)(__this, method)
// System.Void VoxelBusters.Utility.AdvancedScriptableObject`1<VoxelBusters.NativePlugins.NPSettings>::OnDestroy()
#define AdvancedScriptableObject_1_OnDestroy_m8_2373(__this, method) (( void (*) (AdvancedScriptableObject_1_t8_336 *, const MethodInfo*))AdvancedScriptableObject_1_OnDestroy_m8_2058_gshared)(__this, method)
// System.Void VoxelBusters.Utility.AdvancedScriptableObject`1<VoxelBusters.NativePlugins.NPSettings>::Save()
#define AdvancedScriptableObject_1_Save_m8_2374(__this, method) (( void (*) (AdvancedScriptableObject_1_t8_336 *, const MethodInfo*))AdvancedScriptableObject_1_Save_m8_2060_gshared)(__this, method)
// T VoxelBusters.Utility.AdvancedScriptableObject`1<VoxelBusters.NativePlugins.NPSettings>::GetAsset(System.String)
#define AdvancedScriptableObject_1_GetAsset_m8_2375(__this /* static, unused */, ____assetName, method) (( NPSettings_t8_335 * (*) (Object_t * /* static, unused */, String_t*, const MethodInfo*))AdvancedScriptableObject_1_GetAsset_m8_2062_gshared)(__this /* static, unused */, ____assetName, method)
