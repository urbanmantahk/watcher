﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.CompilerServices.RuntimeWrappedException
struct RuntimeWrappedException_t1_706;
// System.Object
struct Object_t;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.Runtime.CompilerServices.RuntimeWrappedException::.ctor()
extern "C" void RuntimeWrappedException__ctor_m1_7551 (RuntimeWrappedException_t1_706 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.CompilerServices.RuntimeWrappedException::get_WrappedException()
extern "C" Object_t * RuntimeWrappedException_get_WrappedException_m1_7552 (RuntimeWrappedException_t1_706 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.CompilerServices.RuntimeWrappedException::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void RuntimeWrappedException_GetObjectData_m1_7553 (RuntimeWrappedException_t1_706 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
