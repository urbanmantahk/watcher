﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_SystemException.h"

// System.StackOverflowException
struct  StackOverflowException_t1_1597  : public SystemException_t1_250
{
};
