﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Events.UnityEvent`1<UnityEngine.Color>
struct UnityEvent_1_t6_302;
// UnityEngine.Events.UnityAction`1<UnityEngine.Color>
struct UnityAction_1_t6_310;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Object
struct Object_t;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t6_257;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color.h"

// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::.ctor()
extern "C" void UnityEvent_1__ctor_m6_1848_gshared (UnityEvent_1_t6_302 * __this, const MethodInfo* method);
#define UnityEvent_1__ctor_m6_1848(__this, method) (( void (*) (UnityEvent_1_t6_302 *, const MethodInfo*))UnityEvent_1__ctor_m6_1848_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C" void UnityEvent_1_AddListener_m6_1850_gshared (UnityEvent_1_t6_302 * __this, UnityAction_1_t6_310 * ___call, const MethodInfo* method);
#define UnityEvent_1_AddListener_m6_1850(__this, ___call, method) (( void (*) (UnityEvent_1_t6_302 *, UnityAction_1_t6_310 *, const MethodInfo*))UnityEvent_1_AddListener_m6_1850_gshared)(__this, ___call, method)
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C" void UnityEvent_1_RemoveListener_m6_2039_gshared (UnityEvent_1_t6_302 * __this, UnityAction_1_t6_310 * ___call, const MethodInfo* method);
#define UnityEvent_1_RemoveListener_m6_2039(__this, ___call, method) (( void (*) (UnityEvent_1_t6_302 *, UnityAction_1_t6_310 *, const MethodInfo*))UnityEvent_1_RemoveListener_m6_2039_gshared)(__this, ___call, method)
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::FindMethod_Impl(System.String,System.Object)
extern "C" MethodInfo_t * UnityEvent_1_FindMethod_Impl_m6_2040_gshared (UnityEvent_1_t6_302 * __this, String_t* ___name, Object_t * ___targetObj, const MethodInfo* method);
#define UnityEvent_1_FindMethod_Impl_m6_2040(__this, ___name, ___targetObj, method) (( MethodInfo_t * (*) (UnityEvent_1_t6_302 *, String_t*, Object_t *, const MethodInfo*))UnityEvent_1_FindMethod_Impl_m6_2040_gshared)(__this, ___name, ___targetObj, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C" BaseInvokableCall_t6_257 * UnityEvent_1_GetDelegate_m6_2041_gshared (UnityEvent_1_t6_302 * __this, Object_t * ___target, MethodInfo_t * ___theFunction, const MethodInfo* method);
#define UnityEvent_1_GetDelegate_m6_2041(__this, ___target, ___theFunction, method) (( BaseInvokableCall_t6_257 * (*) (UnityEvent_1_t6_302 *, Object_t *, MethodInfo_t *, const MethodInfo*))UnityEvent_1_GetDelegate_m6_2041_gshared)(__this, ___target, ___theFunction, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
extern "C" BaseInvokableCall_t6_257 * UnityEvent_1_GetDelegate_m6_2042_gshared (Object_t * __this /* static, unused */, UnityAction_1_t6_310 * ___action, const MethodInfo* method);
#define UnityEvent_1_GetDelegate_m6_2042(__this /* static, unused */, ___action, method) (( BaseInvokableCall_t6_257 * (*) (Object_t * /* static, unused */, UnityAction_1_t6_310 *, const MethodInfo*))UnityEvent_1_GetDelegate_m6_2042_gshared)(__this /* static, unused */, ___action, method)
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::Invoke(T0)
extern "C" void UnityEvent_1_Invoke_m6_1849_gshared (UnityEvent_1_t6_302 * __this, Color_t6_40  ___arg0, const MethodInfo* method);
#define UnityEvent_1_Invoke_m6_1849(__this, ___arg0, method) (( void (*) (UnityEvent_1_t6_302 *, Color_t6_40 , const MethodInfo*))UnityEvent_1_Invoke_m6_1849_gshared)(__this, ___arg0, method)
