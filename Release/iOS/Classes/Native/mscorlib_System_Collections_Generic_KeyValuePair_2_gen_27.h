﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// VoxelBusters.NativePlugins.Internal.NPObject
struct NPObject_t8_222;

#include "mscorlib_System_ValueType.h"

// System.Collections.Generic.KeyValuePair`2<System.String,VoxelBusters.NativePlugins.Internal.NPObject>
struct  KeyValuePair_2_t1_2764 
{
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	NPObject_t8_222 * ___value_1;
};
