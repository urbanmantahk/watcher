﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Attribute.h"

// System.Reflection.ObfuscateAssemblyAttribute
struct  ObfuscateAssemblyAttribute_t1_624  : public Attribute_t1_2
{
	// System.Boolean System.Reflection.ObfuscateAssemblyAttribute::is_private
	bool ___is_private_0;
	// System.Boolean System.Reflection.ObfuscateAssemblyAttribute::strip
	bool ___strip_1;
};
