﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Policy.GacInstalled
struct GacInstalled_t1_1348;
// System.Char[]
struct CharU5BU5D_t1_16;
// System.Object
struct Object_t;
// System.Security.IPermission
struct IPermission_t1_1400;
// System.Security.Policy.Evidence
struct Evidence_t1_398;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Policy.GacInstalled::.ctor()
extern "C" void GacInstalled__ctor_m1_11497 (GacInstalled_t1_1348 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Policy.GacInstalled::System.Security.Policy.IBuiltInEvidence.GetRequiredSize(System.Boolean)
extern "C" int32_t GacInstalled_System_Security_Policy_IBuiltInEvidence_GetRequiredSize_m1_11498 (GacInstalled_t1_1348 * __this, bool ___verbose, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Policy.GacInstalled::System.Security.Policy.IBuiltInEvidence.InitFromBuffer(System.Char[],System.Int32)
extern "C" int32_t GacInstalled_System_Security_Policy_IBuiltInEvidence_InitFromBuffer_m1_11499 (GacInstalled_t1_1348 * __this, CharU5BU5D_t1_16* ___buffer, int32_t ___position, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Policy.GacInstalled::System.Security.Policy.IBuiltInEvidence.OutputToBuffer(System.Char[],System.Int32,System.Boolean)
extern "C" int32_t GacInstalled_System_Security_Policy_IBuiltInEvidence_OutputToBuffer_m1_11500 (GacInstalled_t1_1348 * __this, CharU5BU5D_t1_16* ___buffer, int32_t ___position, bool ___verbose, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Security.Policy.GacInstalled::Copy()
extern "C" Object_t * GacInstalled_Copy_m1_11501 (GacInstalled_t1_1348 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Policy.GacInstalled::CreateIdentityPermission(System.Security.Policy.Evidence)
extern "C" Object_t * GacInstalled_CreateIdentityPermission_m1_11502 (GacInstalled_t1_1348 * __this, Evidence_t1_398 * ___evidence, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Policy.GacInstalled::Equals(System.Object)
extern "C" bool GacInstalled_Equals_m1_11503 (GacInstalled_t1_1348 * __this, Object_t * ___o, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Policy.GacInstalled::GetHashCode()
extern "C" int32_t GacInstalled_GetHashCode_m1_11504 (GacInstalled_t1_1348 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Policy.GacInstalled::ToString()
extern "C" String_t* GacInstalled_ToString_m1_11505 (GacInstalled_t1_1348 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
