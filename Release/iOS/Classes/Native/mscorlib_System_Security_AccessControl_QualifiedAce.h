﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "mscorlib_System_Security_AccessControl_KnownAce.h"
#include "mscorlib_System_Security_AccessControl_AceQualifier.h"

// System.Security.AccessControl.QualifiedAce
struct  QualifiedAce_t1_1123  : public KnownAce_t1_1136
{
	// System.Security.AccessControl.AceQualifier System.Security.AccessControl.QualifiedAce::ace_qualifier
	int32_t ___ace_qualifier_6;
	// System.Boolean System.Security.AccessControl.QualifiedAce::is_callback
	bool ___is_callback_7;
	// System.Byte[] System.Security.AccessControl.QualifiedAce::opaque
	ByteU5BU5D_t1_109* ___opaque_8;
};
