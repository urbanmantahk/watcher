﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Proxies.RemotingProxy
struct RemotingProxy_t1_932;
// System.Type
struct Type_t;
// System.Runtime.Remoting.ClientIdentity
struct ClientIdentity_t1_1013;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Runtime.Remoting.Messaging.IMessage
struct IMessage_t1_875;
// System.Runtime.Remoting.Identity
struct Identity_t1_943;
// System.Runtime.Remoting.Messaging.IMethodMessage
struct IMethodMessage_t1_948;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Proxies.RemotingProxy::.ctor(System.Type,System.Runtime.Remoting.ClientIdentity)
extern "C" void RemotingProxy__ctor_m1_9014 (RemotingProxy_t1_932 * __this, Type_t * ___type, ClientIdentity_t1_1013 * ___identity, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Proxies.RemotingProxy::.ctor(System.Type,System.String,System.Object[])
extern "C" void RemotingProxy__ctor_m1_9015 (RemotingProxy_t1_932 * __this, Type_t * ___type, String_t* ___activationUrl, ObjectU5BU5D_t1_272* ___activationAttributes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Proxies.RemotingProxy::.cctor()
extern "C" void RemotingProxy__cctor_m1_9016 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Messaging.IMessage System.Runtime.Remoting.Proxies.RemotingProxy::Invoke(System.Runtime.Remoting.Messaging.IMessage)
extern "C" Object_t * RemotingProxy_Invoke_m1_9017 (RemotingProxy_t1_932 * __this, Object_t * ___request, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Proxies.RemotingProxy::AttachIdentity(System.Runtime.Remoting.Identity)
extern "C" void RemotingProxy_AttachIdentity_m1_9018 (RemotingProxy_t1_932 * __this, Identity_t1_943 * ___identity, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Messaging.IMessage System.Runtime.Remoting.Proxies.RemotingProxy::ActivateRemoteObject(System.Runtime.Remoting.Messaging.IMethodMessage)
extern "C" Object_t * RemotingProxy_ActivateRemoteObject_m1_9019 (RemotingProxy_t1_932 * __this, Object_t * ___request, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Proxies.RemotingProxy::get_TypeName()
extern "C" String_t* RemotingProxy_get_TypeName_m1_9020 (RemotingProxy_t1_932 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Proxies.RemotingProxy::set_TypeName(System.String)
extern "C" void RemotingProxy_set_TypeName_m1_9021 (RemotingProxy_t1_932 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Remoting.Proxies.RemotingProxy::CanCastTo(System.Type,System.Object)
extern "C" bool RemotingProxy_CanCastTo_m1_9022 (RemotingProxy_t1_932 * __this, Type_t * ___fromType, Object_t * ___o, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Proxies.RemotingProxy::Finalize()
extern "C" void RemotingProxy_Finalize_m1_9023 (RemotingProxy_t1_932 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
