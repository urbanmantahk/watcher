﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Diagnostics_SymbolStore_SymAddressKind.h"

// System.Diagnostics.SymbolStore.SymAddressKind
struct  SymAddressKind_t1_317 
{
	// System.Int32 System.Diagnostics.SymbolStore.SymAddressKind::value__
	int32_t ___value___1;
};
