﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Globalization_Calendar.h"
#include "mscorlib_System_DateTime.h"

// System.Globalization.PersianCalendar
struct  PersianCalendar_t1_384  : public Calendar_t1_338
{
};
struct PersianCalendar_t1_384_StaticFields{
	// System.Int32 System.Globalization.PersianCalendar::PersianEra
	int32_t ___PersianEra_10;
	// System.DateTime System.Globalization.PersianCalendar::PersianMin
	DateTime_t1_150  ___PersianMin_11;
	// System.DateTime System.Globalization.PersianCalendar::PersianMax
	DateTime_t1_150  ___PersianMax_12;
};
