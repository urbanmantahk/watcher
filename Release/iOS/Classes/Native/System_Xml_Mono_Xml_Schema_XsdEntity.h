﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "System_Xml_Mono_Xml_Schema_XsdName.h"

// Mono.Xml.Schema.XsdEntity
struct  XsdEntity_t4_18  : public XsdName_t4_13
{
};
