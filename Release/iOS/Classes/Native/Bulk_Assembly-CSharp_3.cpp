﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// VoxelBusters.NativePlugins.ApplicationSettings/AndroidSettings
struct AndroidSettings_t8_317;
// System.String
struct String_t;
// VoxelBusters.NativePlugins.ApplicationSettings/Features
struct Features_t8_318;
// VoxelBusters.NativePlugins.ApplicationSettings/iOSSettings
struct iOSSettings_t8_319;
// VoxelBusters.NativePlugins.ApplicationSettings
struct ApplicationSettings_t8_320;
// VoxelBusters.NativePlugins.Internal.NotifyNPSettingsOnValueChangeAttribute
struct NotifyNPSettingsOnValueChangeAttribute_t8_321;
// VoxelBusters.NativePlugins.Internal.GameServicesIDHandler
struct GameServicesIDHandler_t8_322;
// VoxelBusters.NativePlugins.IDContainer[]
struct IDContainerU5BU5D_t8_239;
// VoxelBusters.NativePlugins.Internal.Constants
struct Constants_t8_323;
// VoxelBusters.NativePlugins.Internal.InstanceIDGenerator
struct InstanceIDGenerator_t8_324;
// VoxelBusters.NativePlugins.Internal.NPObject
struct NPObject_t8_222;
// VoxelBusters.NativePlugins.Internal.NPObjectManager
struct NPObjectManager_t8_326;
// VoxelBusters.NativePlugins.AchievementHandler
struct AchievementHandler_t8_327;
// VoxelBusters.NativePlugins.AchievementDescription[]
struct AchievementDescriptionU5BU5D_t8_224;
// VoxelBusters.NativePlugins.AchievementDescription
struct AchievementDescription_t8_225;
// VoxelBusters.NativePlugins.IDContainer
struct IDContainer_t8_328;
// VoxelBusters.NativePlugins.PlatformID[]
struct PlatformIDU5BU5D_t8_329;
// VoxelBusters.NativePlugins.PlatformID
struct PlatformID_t8_331;
// VoxelBusters.NativePlugins.MIMEType
struct MIMEType_t8_332;
// NPBinding
struct NPBinding_t8_333;
// VoxelBusters.NativePlugins.NotificationService
struct NotificationService_t8_261;
// System.Object
struct Object_t;
// VoxelBusters.NativePlugins.UI
struct UI_t8_303;
// VoxelBusters.NativePlugins.Utility
struct Utility_t8_306;
// VoxelBusters.NativePlugins.NPSettings
struct NPSettings_t8_335;
// VoxelBusters.NativePlugins.UtilitySettings
struct UtilitySettings_t8_309;
// VoxelBusters.NativePlugins.NotificationServiceSettings
struct NotificationServiceSettings_t8_270;
// VoxelBusters.AssetStoreProductUtility.AssetStoreProduct
struct AssetStoreProduct_t8_22;
// VoxelBusters.NativePlugins.RateMyApp/Settings
struct Settings_t8_310;
// VoxelBusters.NativePlugins.RateMyApp
struct RateMyApp_t8_307;
// VoxelBusters.NativePlugins.WebView
struct WebView_t8_191;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_ApplicationSett.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_ApplicationSettMethodDeclarations.h"
#include "mscorlib_System_Void.h"
#include "mscorlib_System_ObjectMethodDeclarations.h"
#include "mscorlib_System_Object.h"
#include "mscorlib_System_String.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_ApplicationSett_0.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_ApplicationSett_0MethodDeclarations.h"
#include "mscorlib_System_Boolean.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_ApplicationSett_1.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_ApplicationSett_1MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_ApplicationSett_2.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_ApplicationSett_2MethodDeclarations.h"
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_Notify.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_NotifyMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_ExecuteOnValueChangeAMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_ExecuteOnValueChangeA.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_GameSe.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_GameSeMethodDeclarations.h"
#include "Assembly-CSharp_ArrayTypes.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_IDContainer.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_IDContainerMethodDeclarations.h"
#include "mscorlib_System_StringMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_DebugPRO_ConsoleMethodDeclarations.h"
#include "mscorlib_System_Int32.h"
#include "UnityEngine_UnityEngine_Object.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_Consta.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_ConstaMethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_Instan.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_InstanMethodDeclarations.h"
#include "mscorlib_System_GuidMethodDeclarations.h"
#include "mscorlib_System_ConvertMethodDeclarations.h"
#include "mscorlib_System_Guid.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Byte.h"
#include "mscorlib_System_Char.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_NPObje.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_NPObjeMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_NPObje_0.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_NPObje_1MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_NPObje_0MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_NPObje_1.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_19MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_19.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_AchievementHand.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_AchievementHandMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_AchievementDesc_0.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_AchievementDesc_0MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_PlatformID.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_PlatformIDMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_PlatformID_ePla.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_PlatformID_ePlaMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_MIMEType.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_MIMETypeMethodDeclarations.h"
#include "AssemblyU2DCSharp_NPBinding.h"
#include "AssemblyU2DCSharp_NPBindingMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_DesignPatterns_SingletonPatteMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_DesignPatterns_SingletonPatte.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_NotificationSer_2.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_NPSettingsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObjectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_UI.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Utility.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_NPSettings.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_UtilitySettingsMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_NotificationSer_6MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_AdvancedScriptableObj_0MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_UtilitySettings.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_NotificationSer_6.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_AdvancedScriptableObj_0.h"
#include "AssemblyU2DCSharp_VoxelBusters_AssetStoreProductUtility_Asse.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_RateMyApp_Setti.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_RateMyApp_SettiMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_RateMyApp.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_RateMyAppMethodDeclarations.h"
#include "UnityEngine_UnityEngine_PlayerPrefsMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_PlayerSettingsMethodDeclarations.h"
#include "mscorlib_System_DateTimeMethodDeclarations.h"
#include "mscorlib_System_TimeSpanMethodDeclarations.h"
#include "mscorlib_System_Single.h"
#include "mscorlib_System_DateTime.h"
#include "mscorlib_System_TimeSpan.h"
#include "mscorlib_System_Double.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_UI_AlertDialogCMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_UIMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_UI_AlertDialogC.h"
#include "mscorlib_System_IntPtr.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_UtilityMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_WebView.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_WebViewMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ColorMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect.h"
#include "UnityEngine_UnityEngine_Color.h"

// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C" Object_t * GameObject_GetComponent_TisObject_t_m6_1905_gshared (GameObject_t6_97 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisObject_t_m6_1905(__this, method) (( Object_t * (*) (GameObject_t6_97 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m6_1905_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<VoxelBusters.NativePlugins.NotificationService>()
#define GameObject_GetComponent_TisNotificationService_t8_261_m6_1928(__this, method) (( NotificationService_t8_261 * (*) (GameObject_t6_97 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m6_1905_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<VoxelBusters.NativePlugins.UI>()
#define GameObject_GetComponent_TisUI_t8_303_m6_1929(__this, method) (( UI_t8_303 * (*) (GameObject_t6_97 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m6_1905_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<VoxelBusters.NativePlugins.Utility>()
#define GameObject_GetComponent_TisUtility_t8_306_m6_1930(__this, method) (( Utility_t8_306 * (*) (GameObject_t6_97 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m6_1905_gshared)(__this, method)
// !!0 NPBinding::AddComponentBasedOnPlatform<System.Object>()
extern "C" Object_t * NPBinding_AddComponentBasedOnPlatform_TisObject_t_m8_2027_gshared (NPBinding_t8_333 * __this, const MethodInfo* method);
#define NPBinding_AddComponentBasedOnPlatform_TisObject_t_m8_2027(__this, method) (( Object_t * (*) (NPBinding_t8_333 *, const MethodInfo*))NPBinding_AddComponentBasedOnPlatform_TisObject_t_m8_2027_gshared)(__this, method)
// !!0 NPBinding::AddComponentBasedOnPlatform<VoxelBusters.NativePlugins.NotificationService>()
#define NPBinding_AddComponentBasedOnPlatform_TisNotificationService_t8_261_m8_2020(__this, method) (( NotificationService_t8_261 * (*) (NPBinding_t8_333 *, const MethodInfo*))NPBinding_AddComponentBasedOnPlatform_TisObject_t_m8_2027_gshared)(__this, method)
// !!0 NPBinding::AddComponentBasedOnPlatform<VoxelBusters.NativePlugins.UI>()
#define NPBinding_AddComponentBasedOnPlatform_TisUI_t8_303_m8_2021(__this, method) (( UI_t8_303 * (*) (NPBinding_t8_333 *, const MethodInfo*))NPBinding_AddComponentBasedOnPlatform_TisObject_t_m8_2027_gshared)(__this, method)
// !!0 NPBinding::AddComponentBasedOnPlatform<VoxelBusters.NativePlugins.Utility>()
#define NPBinding_AddComponentBasedOnPlatform_TisUtility_t8_306_m8_2022(__this, method) (( Utility_t8_306 * (*) (NPBinding_t8_333 *, const MethodInfo*))NPBinding_AddComponentBasedOnPlatform_TisObject_t_m8_2027_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void VoxelBusters.NativePlugins.ApplicationSettings/AndroidSettings::.ctor()
extern "C" void AndroidSettings__ctor_m8_1863 (AndroidSettings_t8_317 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String VoxelBusters.NativePlugins.ApplicationSettings/AndroidSettings::get_StoreIdentifier()
extern "C" String_t* AndroidSettings_get_StoreIdentifier_m8_1864 (AndroidSettings_t8_317 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_storeIdentifier_0);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.ApplicationSettings/AndroidSettings::set_StoreIdentifier(System.String)
extern "C" void AndroidSettings_set_StoreIdentifier_m8_1865 (AndroidSettings_t8_317 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_storeIdentifier_0 = L_0;
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.ApplicationSettings/Features::.ctor()
extern "C" void Features__ctor_m8_1866 (Features_t8_318 * __this, const MethodInfo* method)
{
	{
		__this->___m_usesAddressBook_0 = 1;
		__this->___m_usesBilling_1 = 1;
		__this->___m_usesGameServices_2 = 1;
		__this->___m_usesMediaLibrary_3 = 1;
		__this->___m_usesNetworkConnectivity_4 = 1;
		__this->___m_usesNotificationService_5 = 1;
		__this->___m_usesSharing_6 = 1;
		__this->___m_usesTwitter_7 = 1;
		__this->___m_usesWebView_8 = 1;
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean VoxelBusters.NativePlugins.ApplicationSettings/Features::get_UsesAddressBook()
extern "C" bool Features_get_UsesAddressBook_m8_1867 (Features_t8_318 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_usesAddressBook_0);
		return L_0;
	}
}
// System.Boolean VoxelBusters.NativePlugins.ApplicationSettings/Features::get_UsesBilling()
extern "C" bool Features_get_UsesBilling_m8_1868 (Features_t8_318 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_usesBilling_1);
		return L_0;
	}
}
// System.Boolean VoxelBusters.NativePlugins.ApplicationSettings/Features::get_UsesGameServices()
extern "C" bool Features_get_UsesGameServices_m8_1869 (Features_t8_318 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_usesGameServices_2);
		return L_0;
	}
}
// System.Boolean VoxelBusters.NativePlugins.ApplicationSettings/Features::get_UsesMediaLibrary()
extern "C" bool Features_get_UsesMediaLibrary_m8_1870 (Features_t8_318 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_usesMediaLibrary_3);
		return L_0;
	}
}
// System.Boolean VoxelBusters.NativePlugins.ApplicationSettings/Features::get_UsesNetworkConnectivity()
extern "C" bool Features_get_UsesNetworkConnectivity_m8_1871 (Features_t8_318 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_usesNetworkConnectivity_4);
		return L_0;
	}
}
// System.Boolean VoxelBusters.NativePlugins.ApplicationSettings/Features::get_UsesNotificationService()
extern "C" bool Features_get_UsesNotificationService_m8_1872 (Features_t8_318 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_usesNotificationService_5);
		return L_0;
	}
}
// System.Boolean VoxelBusters.NativePlugins.ApplicationSettings/Features::get_UsesSharing()
extern "C" bool Features_get_UsesSharing_m8_1873 (Features_t8_318 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_usesSharing_6);
		return L_0;
	}
}
// System.Boolean VoxelBusters.NativePlugins.ApplicationSettings/Features::get_UsesTwitter()
extern "C" bool Features_get_UsesTwitter_m8_1874 (Features_t8_318 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_usesTwitter_7);
		return L_0;
	}
}
// System.Boolean VoxelBusters.NativePlugins.ApplicationSettings/Features::get_UsesWebView()
extern "C" bool Features_get_UsesWebView_m8_1875 (Features_t8_318 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_usesWebView_8);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.ApplicationSettings/iOSSettings::.ctor()
extern "C" void iOSSettings__ctor_m8_1876 (iOSSettings_t8_319 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String VoxelBusters.NativePlugins.ApplicationSettings/iOSSettings::get_StoreIdentifier()
extern "C" String_t* iOSSettings_get_StoreIdentifier_m8_1877 (iOSSettings_t8_319 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_storeIdentifier_0);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.ApplicationSettings/iOSSettings::set_StoreIdentifier(System.String)
extern "C" void iOSSettings_set_StoreIdentifier_m8_1878 (iOSSettings_t8_319 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_storeIdentifier_0 = L_0;
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.ApplicationSettings::.ctor()
extern "C" void ApplicationSettings__ctor_m8_1879 (ApplicationSettings_t8_320 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean VoxelBusters.NativePlugins.ApplicationSettings::get_IsDebugMode()
extern "C" bool ApplicationSettings_get_IsDebugMode_m8_1880 (ApplicationSettings_t8_320 * __this, const MethodInfo* method)
{
	{
		bool L_0 = Debug_get_isDebugBuild_m6_638(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// VoxelBusters.NativePlugins.ApplicationSettings/iOSSettings VoxelBusters.NativePlugins.ApplicationSettings::get_IOS()
extern "C" iOSSettings_t8_319 * ApplicationSettings_get_IOS_m8_1881 (ApplicationSettings_t8_320 * __this, const MethodInfo* method)
{
	{
		iOSSettings_t8_319 * L_0 = (__this->___m_iOS_0);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.ApplicationSettings::set_IOS(VoxelBusters.NativePlugins.ApplicationSettings/iOSSettings)
extern "C" void ApplicationSettings_set_IOS_m8_1882 (ApplicationSettings_t8_320 * __this, iOSSettings_t8_319 * ___value, const MethodInfo* method)
{
	{
		iOSSettings_t8_319 * L_0 = ___value;
		__this->___m_iOS_0 = L_0;
		return;
	}
}
// VoxelBusters.NativePlugins.ApplicationSettings/AndroidSettings VoxelBusters.NativePlugins.ApplicationSettings::get_Android()
extern "C" AndroidSettings_t8_317 * ApplicationSettings_get_Android_m8_1883 (ApplicationSettings_t8_320 * __this, const MethodInfo* method)
{
	{
		AndroidSettings_t8_317 * L_0 = (__this->___m_android_1);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.ApplicationSettings::set_Android(VoxelBusters.NativePlugins.ApplicationSettings/AndroidSettings)
extern "C" void ApplicationSettings_set_Android_m8_1884 (ApplicationSettings_t8_320 * __this, AndroidSettings_t8_317 * ___value, const MethodInfo* method)
{
	{
		AndroidSettings_t8_317 * L_0 = ___value;
		__this->___m_android_1 = L_0;
		return;
	}
}
// VoxelBusters.NativePlugins.ApplicationSettings/Features VoxelBusters.NativePlugins.ApplicationSettings::get_SupportedFeatures()
extern "C" Features_t8_318 * ApplicationSettings_get_SupportedFeatures_m8_1885 (ApplicationSettings_t8_320 * __this, const MethodInfo* method)
{
	{
		Features_t8_318 * L_0 = (__this->___m_supportedFeatures_2);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.ApplicationSettings::set_SupportedFeatures(VoxelBusters.NativePlugins.ApplicationSettings/Features)
extern "C" void ApplicationSettings_set_SupportedFeatures_m8_1886 (ApplicationSettings_t8_320 * __this, Features_t8_318 * ___value, const MethodInfo* method)
{
	{
		Features_t8_318 * L_0 = ___value;
		__this->___m_supportedFeatures_2 = L_0;
		return;
	}
}
// System.String VoxelBusters.NativePlugins.ApplicationSettings::get_StoreIdentifier()
extern "C" String_t* ApplicationSettings_get_StoreIdentifier_m8_1887 (ApplicationSettings_t8_320 * __this, const MethodInfo* method)
{
	{
		iOSSettings_t8_319 * L_0 = (__this->___m_iOS_0);
		NullCheck(L_0);
		String_t* L_1 = iOSSettings_get_StoreIdentifier_m8_1877(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.NotifyNPSettingsOnValueChangeAttribute::.ctor()
extern Il2CppCodeGenString* _stringLiteral5539;
extern "C" void NotifyNPSettingsOnValueChangeAttribute__ctor_m8_1888 (NotifyNPSettingsOnValueChangeAttribute_t8_321 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral5539 = il2cpp_codegen_string_literal_from_index(5539);
		s_Il2CppMethodIntialized = true;
	}
	{
		ExecuteOnValueChangeAttribute__ctor_m8_866(__this, _stringLiteral5539, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.GameServicesIDHandler::.ctor()
extern "C" void GameServicesIDHandler__ctor_m8_1889 (GameServicesIDHandler_t8_322 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.GameServicesIDHandler::.cctor()
extern TypeInfo* IDContainerU5BU5D_t8_239_il2cpp_TypeInfo_var;
extern TypeInfo* GameServicesIDHandler_t8_322_il2cpp_TypeInfo_var;
extern "C" void GameServicesIDHandler__cctor_m8_1890 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDContainerU5BU5D_t8_239_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2130);
		GameServicesIDHandler_t8_322_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2171);
		s_Il2CppMethodIntialized = true;
	}
	{
		((GameServicesIDHandler_t8_322_StaticFields*)GameServicesIDHandler_t8_322_il2cpp_TypeInfo_var->static_fields)->___leaderboardIDCollection_0 = ((IDContainerU5BU5D_t8_239*)SZArrayNew(IDContainerU5BU5D_t8_239_il2cpp_TypeInfo_var, 0));
		((GameServicesIDHandler_t8_322_StaticFields*)GameServicesIDHandler_t8_322_il2cpp_TypeInfo_var->static_fields)->___achievementIDCollection_1 = ((IDContainerU5BU5D_t8_239*)SZArrayNew(IDContainerU5BU5D_t8_239_il2cpp_TypeInfo_var, 0));
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.GameServicesIDHandler::SetLeaderboardIDCollection(VoxelBusters.NativePlugins.IDContainer[])
extern TypeInfo* GameServicesIDHandler_t8_322_il2cpp_TypeInfo_var;
extern "C" void GameServicesIDHandler_SetLeaderboardIDCollection_m8_1891 (Object_t * __this /* static, unused */, IDContainerU5BU5D_t8_239* ____newCollection, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameServicesIDHandler_t8_322_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2171);
		s_Il2CppMethodIntialized = true;
	}
	{
		IDContainerU5BU5D_t8_239* L_0 = ____newCollection;
		IL2CPP_RUNTIME_CLASS_INIT(GameServicesIDHandler_t8_322_il2cpp_TypeInfo_var);
		((GameServicesIDHandler_t8_322_StaticFields*)GameServicesIDHandler_t8_322_il2cpp_TypeInfo_var->static_fields)->___leaderboardIDCollection_0 = L_0;
		return;
	}
}
// System.String VoxelBusters.NativePlugins.Internal.GameServicesIDHandler::GetLeaderboardID(System.String)
extern TypeInfo* GameServicesIDHandler_t8_322_il2cpp_TypeInfo_var;
extern "C" String_t* GameServicesIDHandler_GetLeaderboardID_m8_1892 (Object_t * __this /* static, unused */, String_t* ____globalID, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameServicesIDHandler_t8_322_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2171);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameServicesIDHandler_t8_322_il2cpp_TypeInfo_var);
		IDContainerU5BU5D_t8_239* L_0 = ((GameServicesIDHandler_t8_322_StaticFields*)GameServicesIDHandler_t8_322_il2cpp_TypeInfo_var->static_fields)->___leaderboardIDCollection_0;
		String_t* L_1 = ____globalID;
		String_t* L_2 = GameServicesIDHandler_FindCurrentPlatformIDFromCollection_m8_1897(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String VoxelBusters.NativePlugins.Internal.GameServicesIDHandler::GetLeaderboardGID(System.String)
extern TypeInfo* GameServicesIDHandler_t8_322_il2cpp_TypeInfo_var;
extern "C" String_t* GameServicesIDHandler_GetLeaderboardGID_m8_1893 (Object_t * __this /* static, unused */, String_t* ____platformID, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameServicesIDHandler_t8_322_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2171);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameServicesIDHandler_t8_322_il2cpp_TypeInfo_var);
		IDContainerU5BU5D_t8_239* L_0 = ((GameServicesIDHandler_t8_322_StaticFields*)GameServicesIDHandler_t8_322_il2cpp_TypeInfo_var->static_fields)->___leaderboardIDCollection_0;
		String_t* L_1 = ____platformID;
		String_t* L_2 = GameServicesIDHandler_FindGlobalIDFromCollection_m8_1898(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.GameServicesIDHandler::SetAchievementIDCollection(VoxelBusters.NativePlugins.IDContainer[])
extern TypeInfo* GameServicesIDHandler_t8_322_il2cpp_TypeInfo_var;
extern "C" void GameServicesIDHandler_SetAchievementIDCollection_m8_1894 (Object_t * __this /* static, unused */, IDContainerU5BU5D_t8_239* ____newCollection, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameServicesIDHandler_t8_322_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2171);
		s_Il2CppMethodIntialized = true;
	}
	{
		IDContainerU5BU5D_t8_239* L_0 = ____newCollection;
		IL2CPP_RUNTIME_CLASS_INIT(GameServicesIDHandler_t8_322_il2cpp_TypeInfo_var);
		((GameServicesIDHandler_t8_322_StaticFields*)GameServicesIDHandler_t8_322_il2cpp_TypeInfo_var->static_fields)->___achievementIDCollection_1 = L_0;
		return;
	}
}
// System.String VoxelBusters.NativePlugins.Internal.GameServicesIDHandler::GetAchievementID(System.String)
extern TypeInfo* GameServicesIDHandler_t8_322_il2cpp_TypeInfo_var;
extern "C" String_t* GameServicesIDHandler_GetAchievementID_m8_1895 (Object_t * __this /* static, unused */, String_t* ____globalID, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameServicesIDHandler_t8_322_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2171);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameServicesIDHandler_t8_322_il2cpp_TypeInfo_var);
		IDContainerU5BU5D_t8_239* L_0 = ((GameServicesIDHandler_t8_322_StaticFields*)GameServicesIDHandler_t8_322_il2cpp_TypeInfo_var->static_fields)->___achievementIDCollection_1;
		String_t* L_1 = ____globalID;
		String_t* L_2 = GameServicesIDHandler_FindCurrentPlatformIDFromCollection_m8_1897(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String VoxelBusters.NativePlugins.Internal.GameServicesIDHandler::GetAchievementGID(System.String)
extern TypeInfo* GameServicesIDHandler_t8_322_il2cpp_TypeInfo_var;
extern "C" String_t* GameServicesIDHandler_GetAchievementGID_m8_1896 (Object_t * __this /* static, unused */, String_t* ____platformID, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameServicesIDHandler_t8_322_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2171);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameServicesIDHandler_t8_322_il2cpp_TypeInfo_var);
		IDContainerU5BU5D_t8_239* L_0 = ((GameServicesIDHandler_t8_322_StaticFields*)GameServicesIDHandler_t8_322_il2cpp_TypeInfo_var->static_fields)->___achievementIDCollection_1;
		String_t* L_1 = ____platformID;
		String_t* L_2 = GameServicesIDHandler_FindGlobalIDFromCollection_m8_1898(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String VoxelBusters.NativePlugins.Internal.GameServicesIDHandler::FindCurrentPlatformIDFromCollection(VoxelBusters.NativePlugins.IDContainer[],System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5540;
extern "C" String_t* GameServicesIDHandler_FindCurrentPlatformIDFromCollection_m8_1897 (Object_t * __this /* static, unused */, IDContainerU5BU5D_t8_239* ____collection, String_t* ____globalID, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5540 = il2cpp_codegen_string_literal_from_index(5540);
		s_Il2CppMethodIntialized = true;
	}
	IDContainer_t8_328 * V_0 = {0};
	IDContainerU5BU5D_t8_239* V_1 = {0};
	int32_t V_2 = 0;
	{
		String_t* L_0 = ____globalID;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (String_t*)NULL;
	}

IL_0008:
	{
		IDContainerU5BU5D_t8_239* L_1 = ____collection;
		V_1 = L_1;
		V_2 = 0;
		goto IL_002c;
	}

IL_0011:
	{
		IDContainerU5BU5D_t8_239* L_2 = V_1;
		int32_t L_3 = V_2;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		V_0 = (*(IDContainer_t8_328 **)(IDContainer_t8_328 **)SZArrayLdElema(L_2, L_4, sizeof(IDContainer_t8_328 *)));
		IDContainer_t8_328 * L_5 = V_0;
		String_t* L_6 = ____globalID;
		NullCheck(L_5);
		bool L_7 = IDContainer_CompareGlobalID_m8_1914(L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0028;
		}
	}
	{
		IDContainer_t8_328 * L_8 = V_0;
		NullCheck(L_8);
		String_t* L_9 = IDContainer_GetCurrentPlatformID_m8_1917(L_8, /*hidden argument*/NULL);
		return L_9;
	}

IL_0028:
	{
		int32_t L_10 = V_2;
		V_2 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_002c:
	{
		int32_t L_11 = V_2;
		IDContainerU5BU5D_t8_239* L_12 = V_1;
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_12)->max_length)))))))
		{
			goto IL_0011;
		}
	}
	{
		String_t* L_13 = ____globalID;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Format_m1_548(NULL /*static, unused*/, _stringLiteral5540, L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_LogError_m8_1009(NULL /*static, unused*/, _stringLiteral5368, L_14, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		return (String_t*)NULL;
	}
}
// System.String VoxelBusters.NativePlugins.Internal.GameServicesIDHandler::FindGlobalIDFromCollection(VoxelBusters.NativePlugins.IDContainer[],System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5541;
extern "C" String_t* GameServicesIDHandler_FindGlobalIDFromCollection_m8_1898 (Object_t * __this /* static, unused */, IDContainerU5BU5D_t8_239* ____collection, String_t* ____platformID, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5541 = il2cpp_codegen_string_literal_from_index(5541);
		s_Il2CppMethodIntialized = true;
	}
	IDContainer_t8_328 * V_0 = {0};
	IDContainerU5BU5D_t8_239* V_1 = {0};
	int32_t V_2 = 0;
	{
		String_t* L_0 = ____platformID;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (String_t*)NULL;
	}

IL_0008:
	{
		IDContainerU5BU5D_t8_239* L_1 = ____collection;
		V_1 = L_1;
		V_2 = 0;
		goto IL_002c;
	}

IL_0011:
	{
		IDContainerU5BU5D_t8_239* L_2 = V_1;
		int32_t L_3 = V_2;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		V_0 = (*(IDContainer_t8_328 **)(IDContainer_t8_328 **)SZArrayLdElema(L_2, L_4, sizeof(IDContainer_t8_328 *)));
		IDContainer_t8_328 * L_5 = V_0;
		String_t* L_6 = ____platformID;
		NullCheck(L_5);
		bool L_7 = IDContainer_CompareCurrentPlatformID_m8_1916(L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0028;
		}
	}
	{
		IDContainer_t8_328 * L_8 = V_0;
		NullCheck(L_8);
		String_t* L_9 = IDContainer_GetGlobalID_m8_1915(L_8, /*hidden argument*/NULL);
		return L_9;
	}

IL_0028:
	{
		int32_t L_10 = V_2;
		V_2 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_002c:
	{
		int32_t L_11 = V_2;
		IDContainerU5BU5D_t8_239* L_12 = V_1;
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_12)->max_length)))))))
		{
			goto IL_0011;
		}
	}
	{
		String_t* L_13 = ____platformID;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Format_m1_548(NULL /*static, unused*/, _stringLiteral5541, L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_LogError_m8_1009(NULL /*static, unused*/, _stringLiteral5368, L_14, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		String_t* L_15 = ____platformID;
		return L_15;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.Constants::.ctor()
extern "C" void Constants__ctor_m8_1899 (Constants_t8_323 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m6_671(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.InstanceIDGenerator::.ctor()
extern "C" void InstanceIDGenerator__ctor_m8_1900 (InstanceIDGenerator_t8_324 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String VoxelBusters.NativePlugins.Internal.InstanceIDGenerator::Create()
extern TypeInfo* Guid_t1_319_il2cpp_TypeInfo_var;
extern TypeInfo* Convert_t1_1522_il2cpp_TypeInfo_var;
extern "C" String_t* InstanceIDGenerator_Create_m8_1901 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Guid_t1_319_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(208);
		Convert_t1_1522_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	Guid_t1_319  V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Guid_t1_319_il2cpp_TypeInfo_var);
		Guid_t1_319  L_0 = Guid_NewGuid_m1_14146(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_0;
		ByteU5BU5D_t1_109* L_1 = Guid_ToByteArray_m1_14148((&V_1), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1_1522_il2cpp_TypeInfo_var);
		String_t* L_2 = Convert_ToBase64String_m1_13445(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		String_t* L_3 = V_0;
		NullCheck(L_3);
		String_t* L_4 = String_Replace_m1_535(L_3, ((int32_t)47), ((int32_t)95), /*hidden argument*/NULL);
		V_0 = L_4;
		String_t* L_5 = V_0;
		NullCheck(L_5);
		String_t* L_6 = String_Substring_m1_455(L_5, 0, ((int32_t)22), /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.NPObject::.ctor()
extern "C" void NPObject__ctor_m8_1902 (NPObject_t8_222 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.NPObject::.ctor(VoxelBusters.NativePlugins.Internal.NPObjectManager/eCollectionType)
extern TypeInfo* NPObjectManager_t8_326_il2cpp_TypeInfo_var;
extern "C" void NPObject__ctor_m8_1903 (NPObject_t8_222 * __this, int32_t ____collectionType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NPObjectManager_t8_326_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2172);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		String_t* L_0 = InstanceIDGenerator_Create_m8_1901(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___m_instaceID_0 = L_0;
		int32_t L_1 = ____collectionType;
		IL2CPP_RUNTIME_CLASS_INIT(NPObjectManager_t8_326_il2cpp_TypeInfo_var);
		NPObjectManager_AddNewObjectToCollection_m8_1907(NULL /*static, unused*/, __this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String VoxelBusters.NativePlugins.Internal.NPObject::GetInstanceID()
extern "C" String_t* NPObject_GetInstanceID_m8_1904 (NPObject_t8_222 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_instaceID_0);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.NPObjectManager::.ctor()
extern "C" void NPObjectManager__ctor_m8_1905 (NPObjectManager_t8_326 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.NPObjectManager::.cctor()
extern TypeInfo* Dictionary_2_t1_1913_il2cpp_TypeInfo_var;
extern TypeInfo* NPObjectManager_t8_326_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_15069_MethodInfo_var;
extern "C" void NPObjectManager__cctor_m8_1906 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t1_1913_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2174);
		NPObjectManager_t8_326_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2172);
		Dictionary_2__ctor_m1_15069_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484248);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t1_1913 * L_0 = (Dictionary_2_t1_1913 *)il2cpp_codegen_object_new (Dictionary_2_t1_1913_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_15069(L_0, /*hidden argument*/Dictionary_2__ctor_m1_15069_MethodInfo_var);
		((NPObjectManager_t8_326_StaticFields*)NPObjectManager_t8_326_il2cpp_TypeInfo_var->static_fields)->___gameServicesObjectCollection_0 = L_0;
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.NPObjectManager::AddNewObjectToCollection(VoxelBusters.NativePlugins.Internal.NPObject,VoxelBusters.NativePlugins.Internal.NPObjectManager/eCollectionType)
extern TypeInfo* NPObjectManager_t8_326_il2cpp_TypeInfo_var;
extern "C" void NPObjectManager_AddNewObjectToCollection_m8_1907 (Object_t * __this /* static, unused */, NPObject_t8_222 * ____newObject, int32_t ____collectionType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NPObjectManager_t8_326_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2172);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t1_1913 * V_0 = {0};
	int32_t V_1 = {0};
	{
		V_0 = (Dictionary_2_t1_1913 *)NULL;
		int32_t L_0 = ____collectionType;
		V_1 = L_0;
		int32_t L_1 = V_1;
		if (!L_1)
		{
			goto IL_000f;
		}
	}
	{
		goto IL_001a;
	}

IL_000f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(NPObjectManager_t8_326_il2cpp_TypeInfo_var);
		Dictionary_2_t1_1913 * L_2 = ((NPObjectManager_t8_326_StaticFields*)NPObjectManager_t8_326_il2cpp_TypeInfo_var->static_fields)->___gameServicesObjectCollection_0;
		V_0 = L_2;
		goto IL_001a;
	}

IL_001a:
	{
		Dictionary_2_t1_1913 * L_3 = V_0;
		NPObject_t8_222 * L_4 = ____newObject;
		NullCheck(L_4);
		String_t* L_5 = NPObject_GetInstanceID_m8_1904(L_4, /*hidden argument*/NULL);
		NPObject_t8_222 * L_6 = ____newObject;
		NullCheck(L_3);
		VirtActionInvoker2< String_t*, NPObject_t8_222 * >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,VoxelBusters.NativePlugins.Internal.NPObject>::Add(!0,!1) */, L_3, L_5, L_6);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.AchievementHandler::.ctor()
extern "C" void AchievementHandler__ctor_m8_1908 (AchievementHandler_t8_327 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.AchievementHandler::.cctor()
extern "C" void AchievementHandler__cctor_m8_1909 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.AchievementHandler::SetAchievementDescriptionList(VoxelBusters.NativePlugins.AchievementDescription[])
extern TypeInfo* AchievementHandler_t8_327_il2cpp_TypeInfo_var;
extern "C" void AchievementHandler_SetAchievementDescriptionList_m8_1910 (Object_t * __this /* static, unused */, AchievementDescriptionU5BU5D_t8_224* ____descriptionList, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AchievementHandler_t8_327_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2125);
		s_Il2CppMethodIntialized = true;
	}
	{
		AchievementDescriptionU5BU5D_t8_224* L_0 = ____descriptionList;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AchievementHandler_t8_327_il2cpp_TypeInfo_var);
		((AchievementHandler_t8_327_StaticFields*)AchievementHandler_t8_327_il2cpp_TypeInfo_var->static_fields)->___cachedAchievementDescriptionList_0 = (AchievementDescriptionU5BU5D_t8_224*)NULL;
		((AchievementHandler_t8_327_StaticFields*)AchievementHandler_t8_327_il2cpp_TypeInfo_var->static_fields)->___cachedAchievementDescriptionCount_1 = 0;
		goto IL_0025;
	}

IL_0017:
	{
		AchievementDescriptionU5BU5D_t8_224* L_1 = ____descriptionList;
		IL2CPP_RUNTIME_CLASS_INIT(AchievementHandler_t8_327_il2cpp_TypeInfo_var);
		((AchievementHandler_t8_327_StaticFields*)AchievementHandler_t8_327_il2cpp_TypeInfo_var->static_fields)->___cachedAchievementDescriptionList_0 = L_1;
		AchievementDescriptionU5BU5D_t8_224* L_2 = ____descriptionList;
		NullCheck(L_2);
		((AchievementHandler_t8_327_StaticFields*)AchievementHandler_t8_327_il2cpp_TypeInfo_var->static_fields)->___cachedAchievementDescriptionCount_1 = (((int32_t)((int32_t)(((Array_t *)L_2)->max_length))));
	}

IL_0025:
	{
		return;
	}
}
// VoxelBusters.NativePlugins.AchievementDescription VoxelBusters.NativePlugins.AchievementHandler::GetAchievementDescription(System.String)
extern TypeInfo* AchievementHandler_t8_327_il2cpp_TypeInfo_var;
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5542;
extern Il2CppCodeGenString* _stringLiteral5543;
extern "C" AchievementDescription_t8_225 * AchievementHandler_GetAchievementDescription_m8_1911 (Object_t * __this /* static, unused */, String_t* ____achievementID, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AchievementHandler_t8_327_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2125);
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5542 = il2cpp_codegen_string_literal_from_index(5542);
		_stringLiteral5543 = il2cpp_codegen_string_literal_from_index(5543);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	AchievementDescription_t8_225 * V_1 = {0};
	String_t* V_2 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(AchievementHandler_t8_327_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t8_224* L_0 = ((AchievementHandler_t8_327_StaticFields*)AchievementHandler_t8_327_il2cpp_TypeInfo_var->static_fields)->___cachedAchievementDescriptionList_0;
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_LogError_m8_1009(NULL /*static, unused*/, _stringLiteral5368, _stringLiteral5542, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		return (AchievementDescription_t8_225 *)NULL;
	}

IL_001c:
	{
		V_0 = 0;
		goto IL_0044;
	}

IL_0023:
	{
		IL2CPP_RUNTIME_CLASS_INIT(AchievementHandler_t8_327_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t8_224* L_1 = ((AchievementHandler_t8_327_StaticFields*)AchievementHandler_t8_327_il2cpp_TypeInfo_var->static_fields)->___cachedAchievementDescriptionList_0;
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		V_1 = (*(AchievementDescription_t8_225 **)(AchievementDescription_t8_225 **)SZArrayLdElema(L_1, L_3, sizeof(AchievementDescription_t8_225 *)));
		AchievementDescription_t8_225 * L_4 = V_1;
		NullCheck(L_4);
		String_t* L_5 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String VoxelBusters.NativePlugins.AchievementDescription::get_Identifier() */, L_4);
		V_2 = L_5;
		String_t* L_6 = V_2;
		String_t* L_7 = ____achievementID;
		NullCheck(L_6);
		bool L_8 = String_Equals_m1_441(L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0040;
		}
	}
	{
		AchievementDescription_t8_225 * L_9 = V_1;
		return L_9;
	}

IL_0040:
	{
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0044:
	{
		int32_t L_11 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(AchievementHandler_t8_327_il2cpp_TypeInfo_var);
		int32_t L_12 = ((AchievementHandler_t8_327_StaticFields*)AchievementHandler_t8_327_il2cpp_TypeInfo_var->static_fields)->___cachedAchievementDescriptionCount_1;
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_0023;
		}
	}
	{
		String_t* L_13 = ____achievementID;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Format_m1_548(NULL /*static, unused*/, _stringLiteral5543, L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_LogError_m8_1009(NULL /*static, unused*/, _stringLiteral5368, L_14, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		return (AchievementDescription_t8_225 *)NULL;
	}
}
// System.Void VoxelBusters.NativePlugins.IDContainer::.ctor()
extern "C" void IDContainer__ctor_m8_1912 (IDContainer_t8_328 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.IDContainer::.ctor(System.String,VoxelBusters.NativePlugins.PlatformID[])
extern "C" void IDContainer__ctor_m8_1913 (IDContainer_t8_328 * __this, String_t* ____globalID, PlatformIDU5BU5D_t8_329* ____platformIDs, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ____globalID;
		__this->___m_globalID_0 = L_0;
		PlatformIDU5BU5D_t8_329* L_1 = ____platformIDs;
		__this->___m_platformIDs_1 = L_1;
		return;
	}
}
// System.Boolean VoxelBusters.NativePlugins.IDContainer::CompareGlobalID(System.String)
extern "C" bool IDContainer_CompareGlobalID_m8_1914 (IDContainer_t8_328 * __this, String_t* ____identifier, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_globalID_0);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		String_t* L_1 = (__this->___m_globalID_0);
		String_t* L_2 = ____identifier;
		NullCheck(L_1);
		bool L_3 = String_Equals_m1_441(L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.String VoxelBusters.NativePlugins.IDContainer::GetGlobalID()
extern "C" String_t* IDContainer_GetGlobalID_m8_1915 (IDContainer_t8_328 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_globalID_0);
		return L_0;
	}
}
// System.Boolean VoxelBusters.NativePlugins.IDContainer::CompareCurrentPlatformID(System.String)
extern "C" bool IDContainer_CompareCurrentPlatformID_m8_1916 (IDContainer_t8_328 * __this, String_t* ____identifier, const MethodInfo* method)
{
	String_t* V_0 = {0};
	{
		String_t* L_0 = IDContainer_GetCurrentPlatformID_m8_1917(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = V_0;
		if (L_1)
		{
			goto IL_000f;
		}
	}
	{
		return 0;
	}

IL_000f:
	{
		String_t* L_2 = V_0;
		String_t* L_3 = ____identifier;
		NullCheck(L_2);
		bool L_4 = String_Equals_m1_441(L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.String VoxelBusters.NativePlugins.IDContainer::GetCurrentPlatformID()
extern "C" String_t* IDContainer_GetCurrentPlatformID_m8_1917 (IDContainer_t8_328 * __this, const MethodInfo* method)
{
	PlatformID_t8_331 * V_0 = {0};
	PlatformIDU5BU5D_t8_329* V_1 = {0};
	int32_t V_2 = 0;
	{
		PlatformIDU5BU5D_t8_329* L_0 = (__this->___m_platformIDs_1);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (String_t*)NULL;
	}

IL_000d:
	{
		PlatformIDU5BU5D_t8_329* L_1 = (__this->___m_platformIDs_1);
		V_1 = L_1;
		V_2 = 0;
		goto IL_0035;
	}

IL_001b:
	{
		PlatformIDU5BU5D_t8_329* L_2 = V_1;
		int32_t L_3 = V_2;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		V_0 = (*(PlatformID_t8_331 **)(PlatformID_t8_331 **)SZArrayLdElema(L_2, L_4, sizeof(PlatformID_t8_331 *)));
		PlatformID_t8_331 * L_5 = V_0;
		NullCheck(L_5);
		int32_t L_6 = PlatformID_get_Platform_m8_1920(L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0031;
		}
	}
	{
		PlatformID_t8_331 * L_7 = V_0;
		NullCheck(L_7);
		String_t* L_8 = PlatformID_get_Value_m8_1922(L_7, /*hidden argument*/NULL);
		return L_8;
	}

IL_0031:
	{
		int32_t L_9 = V_2;
		V_2 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0035:
	{
		int32_t L_10 = V_2;
		PlatformIDU5BU5D_t8_329* L_11 = V_1;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_11)->max_length)))))))
		{
			goto IL_001b;
		}
	}
	{
		return (String_t*)NULL;
	}
}
// System.Void VoxelBusters.NativePlugins.PlatformID::.ctor()
extern "C" void PlatformID__ctor_m8_1918 (PlatformID_t8_331 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.PlatformID::.ctor(VoxelBusters.NativePlugins.PlatformID/ePlatform,System.String)
extern "C" void PlatformID__ctor_m8_1919 (PlatformID_t8_331 * __this, int32_t ____platform, String_t* ____identifier, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		int32_t L_0 = ____platform;
		PlatformID_set_Platform_m8_1921(__this, L_0, /*hidden argument*/NULL);
		String_t* L_1 = ____identifier;
		PlatformID_set_Value_m8_1923(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// VoxelBusters.NativePlugins.PlatformID/ePlatform VoxelBusters.NativePlugins.PlatformID::get_Platform()
extern "C" int32_t PlatformID_get_Platform_m8_1920 (PlatformID_t8_331 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_platform_0);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.PlatformID::set_Platform(VoxelBusters.NativePlugins.PlatformID/ePlatform)
extern "C" void PlatformID_set_Platform_m8_1921 (PlatformID_t8_331 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_platform_0 = L_0;
		return;
	}
}
// System.String VoxelBusters.NativePlugins.PlatformID::get_Value()
extern "C" String_t* PlatformID_get_Value_m8_1922 (PlatformID_t8_331 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_value_1);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.PlatformID::set_Value(System.String)
extern "C" void PlatformID_set_Value_m8_1923 (PlatformID_t8_331 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_value_1 = L_0;
		return;
	}
}
// VoxelBusters.NativePlugins.PlatformID VoxelBusters.NativePlugins.PlatformID::IOS(System.String)
extern TypeInfo* PlatformID_t8_331_il2cpp_TypeInfo_var;
extern "C" PlatformID_t8_331 * PlatformID_IOS_m8_1924 (Object_t * __this /* static, unused */, String_t* ____identifier, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PlatformID_t8_331_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2175);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ____identifier;
		PlatformID_t8_331 * L_1 = (PlatformID_t8_331 *)il2cpp_codegen_object_new (PlatformID_t8_331_il2cpp_TypeInfo_var);
		PlatformID__ctor_m8_1919(L_1, 0, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// VoxelBusters.NativePlugins.PlatformID VoxelBusters.NativePlugins.PlatformID::Android(System.String)
extern TypeInfo* PlatformID_t8_331_il2cpp_TypeInfo_var;
extern "C" PlatformID_t8_331 * PlatformID_Android_m8_1925 (Object_t * __this /* static, unused */, String_t* ____identifier, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PlatformID_t8_331_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2175);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ____identifier;
		PlatformID_t8_331 * L_1 = (PlatformID_t8_331 *)il2cpp_codegen_object_new (PlatformID_t8_331_il2cpp_TypeInfo_var);
		PlatformID__ctor_m8_1919(L_1, 1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// VoxelBusters.NativePlugins.PlatformID VoxelBusters.NativePlugins.PlatformID::Editor(System.String)
extern TypeInfo* PlatformID_t8_331_il2cpp_TypeInfo_var;
extern "C" PlatformID_t8_331 * PlatformID_Editor_m8_1926 (Object_t * __this /* static, unused */, String_t* ____identifier, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PlatformID_t8_331_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2175);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ____identifier;
		PlatformID_t8_331 * L_1 = (PlatformID_t8_331 *)il2cpp_codegen_object_new (PlatformID_t8_331_il2cpp_TypeInfo_var);
		PlatformID__ctor_m8_1919(L_1, 2, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void VoxelBusters.NativePlugins.MIMEType::.ctor()
extern "C" void MIMEType__ctor_m8_1927 (MIMEType_t8_332 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m6_671(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NPBinding::.ctor()
extern TypeInfo* SingletonPattern_1_t8_334_il2cpp_TypeInfo_var;
extern const MethodInfo* SingletonPattern_1__ctor_m8_2017_MethodInfo_var;
extern "C" void NPBinding__ctor_m8_1928 (NPBinding_t8_333 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SingletonPattern_1_t8_334_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2165);
		SingletonPattern_1__ctor_m8_2017_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484249);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SingletonPattern_1_t8_334_il2cpp_TypeInfo_var);
		SingletonPattern_1__ctor_m8_2017(__this, /*hidden argument*/SingletonPattern_1__ctor_m8_2017_MethodInfo_var);
		return;
	}
}
// VoxelBusters.NativePlugins.NotificationService NPBinding::get_NotificationService()
extern TypeInfo* SingletonPattern_1_t8_334_il2cpp_TypeInfo_var;
extern TypeInfo* NPBinding_t8_333_il2cpp_TypeInfo_var;
extern const MethodInfo* SingletonPattern_1_get_Instance_m8_2016_MethodInfo_var;
extern const MethodInfo* SingletonPattern_1_get_CachedGameObject_m8_2018_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisNotificationService_t8_261_m6_1928_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral5544;
extern "C" NotificationService_t8_261 * NPBinding_get_NotificationService_m8_1929 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SingletonPattern_1_t8_334_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2165);
		NPBinding_t8_333_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2164);
		SingletonPattern_1_get_Instance_m8_2016_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484243);
		SingletonPattern_1_get_CachedGameObject_m8_2018_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484250);
		GameObject_GetComponent_TisNotificationService_t8_261_m6_1928_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484251);
		_stringLiteral5544 = il2cpp_codegen_string_literal_from_index(5544);
		s_Il2CppMethodIntialized = true;
	}
	{
		ApplicationSettings_t8_320 * L_0 = NPSettings_get_Application_m8_1934(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Features_t8_318 * L_1 = ApplicationSettings_get_SupportedFeatures_m8_1885(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_2 = Features_get_UsesNotificationService_m8_1872(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0020;
		}
	}
	{
		Debug_LogError_m6_632(NULL /*static, unused*/, _stringLiteral5544, /*hidden argument*/NULL);
		return (NotificationService_t8_261 *)NULL;
	}

IL_0020:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SingletonPattern_1_t8_334_il2cpp_TypeInfo_var);
		NPBinding_t8_333 * L_3 = SingletonPattern_1_get_Instance_m8_2016(NULL /*static, unused*/, /*hidden argument*/SingletonPattern_1_get_Instance_m8_2016_MethodInfo_var);
		bool L_4 = Object_op_Equality_m6_721(NULL /*static, unused*/, L_3, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0032;
		}
	}
	{
		return (NotificationService_t8_261 *)NULL;
	}

IL_0032:
	{
		NotificationService_t8_261 * L_5 = ((NPBinding_t8_333_StaticFields*)NPBinding_t8_333_il2cpp_TypeInfo_var->static_fields)->___notificationService_9;
		bool L_6 = Object_op_Equality_m6_721(NULL /*static, unused*/, L_5, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0056;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SingletonPattern_1_t8_334_il2cpp_TypeInfo_var);
		NPBinding_t8_333 * L_7 = SingletonPattern_1_get_Instance_m8_2016(NULL /*static, unused*/, /*hidden argument*/SingletonPattern_1_get_Instance_m8_2016_MethodInfo_var);
		NullCheck(L_7);
		GameObject_t6_97 * L_8 = SingletonPattern_1_get_CachedGameObject_m8_2018(L_7, /*hidden argument*/SingletonPattern_1_get_CachedGameObject_m8_2018_MethodInfo_var);
		NullCheck(L_8);
		NotificationService_t8_261 * L_9 = GameObject_GetComponent_TisNotificationService_t8_261_m6_1928(L_8, /*hidden argument*/GameObject_GetComponent_TisNotificationService_t8_261_m6_1928_MethodInfo_var);
		((NPBinding_t8_333_StaticFields*)NPBinding_t8_333_il2cpp_TypeInfo_var->static_fields)->___notificationService_9 = L_9;
	}

IL_0056:
	{
		NotificationService_t8_261 * L_10 = ((NPBinding_t8_333_StaticFields*)NPBinding_t8_333_il2cpp_TypeInfo_var->static_fields)->___notificationService_9;
		return L_10;
	}
}
// VoxelBusters.NativePlugins.UI NPBinding::get_UI()
extern TypeInfo* SingletonPattern_1_t8_334_il2cpp_TypeInfo_var;
extern TypeInfo* NPBinding_t8_333_il2cpp_TypeInfo_var;
extern const MethodInfo* SingletonPattern_1_get_Instance_m8_2016_MethodInfo_var;
extern const MethodInfo* SingletonPattern_1_get_CachedGameObject_m8_2018_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisUI_t8_303_m6_1929_MethodInfo_var;
extern "C" UI_t8_303 * NPBinding_get_UI_m8_1930 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SingletonPattern_1_t8_334_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2165);
		NPBinding_t8_333_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2164);
		SingletonPattern_1_get_Instance_m8_2016_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484243);
		SingletonPattern_1_get_CachedGameObject_m8_2018_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484250);
		GameObject_GetComponent_TisUI_t8_303_m6_1929_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484252);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SingletonPattern_1_t8_334_il2cpp_TypeInfo_var);
		NPBinding_t8_333 * L_0 = SingletonPattern_1_get_Instance_m8_2016(NULL /*static, unused*/, /*hidden argument*/SingletonPattern_1_get_Instance_m8_2016_MethodInfo_var);
		bool L_1 = Object_op_Equality_m6_721(NULL /*static, unused*/, L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return (UI_t8_303 *)NULL;
	}

IL_0012:
	{
		UI_t8_303 * L_2 = ((NPBinding_t8_333_StaticFields*)NPBinding_t8_333_il2cpp_TypeInfo_var->static_fields)->___userInterface_10;
		bool L_3 = Object_op_Equality_m6_721(NULL /*static, unused*/, L_2, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0036;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SingletonPattern_1_t8_334_il2cpp_TypeInfo_var);
		NPBinding_t8_333 * L_4 = SingletonPattern_1_get_Instance_m8_2016(NULL /*static, unused*/, /*hidden argument*/SingletonPattern_1_get_Instance_m8_2016_MethodInfo_var);
		NullCheck(L_4);
		GameObject_t6_97 * L_5 = SingletonPattern_1_get_CachedGameObject_m8_2018(L_4, /*hidden argument*/SingletonPattern_1_get_CachedGameObject_m8_2018_MethodInfo_var);
		NullCheck(L_5);
		UI_t8_303 * L_6 = GameObject_GetComponent_TisUI_t8_303_m6_1929(L_5, /*hidden argument*/GameObject_GetComponent_TisUI_t8_303_m6_1929_MethodInfo_var);
		((NPBinding_t8_333_StaticFields*)NPBinding_t8_333_il2cpp_TypeInfo_var->static_fields)->___userInterface_10 = L_6;
	}

IL_0036:
	{
		UI_t8_303 * L_7 = ((NPBinding_t8_333_StaticFields*)NPBinding_t8_333_il2cpp_TypeInfo_var->static_fields)->___userInterface_10;
		return L_7;
	}
}
// VoxelBusters.NativePlugins.Utility NPBinding::get_Utility()
extern TypeInfo* SingletonPattern_1_t8_334_il2cpp_TypeInfo_var;
extern TypeInfo* NPBinding_t8_333_il2cpp_TypeInfo_var;
extern const MethodInfo* SingletonPattern_1_get_Instance_m8_2016_MethodInfo_var;
extern const MethodInfo* SingletonPattern_1_get_CachedGameObject_m8_2018_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisUtility_t8_306_m6_1930_MethodInfo_var;
extern "C" Utility_t8_306 * NPBinding_get_Utility_m8_1931 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SingletonPattern_1_t8_334_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2165);
		NPBinding_t8_333_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2164);
		SingletonPattern_1_get_Instance_m8_2016_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484243);
		SingletonPattern_1_get_CachedGameObject_m8_2018_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484250);
		GameObject_GetComponent_TisUtility_t8_306_m6_1930_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484253);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SingletonPattern_1_t8_334_il2cpp_TypeInfo_var);
		NPBinding_t8_333 * L_0 = SingletonPattern_1_get_Instance_m8_2016(NULL /*static, unused*/, /*hidden argument*/SingletonPattern_1_get_Instance_m8_2016_MethodInfo_var);
		bool L_1 = Object_op_Equality_m6_721(NULL /*static, unused*/, L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return (Utility_t8_306 *)NULL;
	}

IL_0012:
	{
		Utility_t8_306 * L_2 = ((NPBinding_t8_333_StaticFields*)NPBinding_t8_333_il2cpp_TypeInfo_var->static_fields)->___utility_11;
		bool L_3 = Object_op_Equality_m6_721(NULL /*static, unused*/, L_2, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0036;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SingletonPattern_1_t8_334_il2cpp_TypeInfo_var);
		NPBinding_t8_333 * L_4 = SingletonPattern_1_get_Instance_m8_2016(NULL /*static, unused*/, /*hidden argument*/SingletonPattern_1_get_Instance_m8_2016_MethodInfo_var);
		NullCheck(L_4);
		GameObject_t6_97 * L_5 = SingletonPattern_1_get_CachedGameObject_m8_2018(L_4, /*hidden argument*/SingletonPattern_1_get_CachedGameObject_m8_2018_MethodInfo_var);
		NullCheck(L_5);
		Utility_t8_306 * L_6 = GameObject_GetComponent_TisUtility_t8_306_m6_1930(L_5, /*hidden argument*/GameObject_GetComponent_TisUtility_t8_306_m6_1930_MethodInfo_var);
		((NPBinding_t8_333_StaticFields*)NPBinding_t8_333_il2cpp_TypeInfo_var->static_fields)->___utility_11 = L_6;
	}

IL_0036:
	{
		Utility_t8_306 * L_7 = ((NPBinding_t8_333_StaticFields*)NPBinding_t8_333_il2cpp_TypeInfo_var->static_fields)->___utility_11;
		return L_7;
	}
}
// System.Void NPBinding::Init()
extern TypeInfo* SingletonPattern_1_t8_334_il2cpp_TypeInfo_var;
extern TypeInfo* NPBinding_t8_333_il2cpp_TypeInfo_var;
extern const MethodInfo* SingletonPattern_1_Init_m8_2019_MethodInfo_var;
extern const MethodInfo* NPBinding_AddComponentBasedOnPlatform_TisNotificationService_t8_261_m8_2020_MethodInfo_var;
extern const MethodInfo* NPBinding_AddComponentBasedOnPlatform_TisUI_t8_303_m8_2021_MethodInfo_var;
extern const MethodInfo* NPBinding_AddComponentBasedOnPlatform_TisUtility_t8_306_m8_2022_MethodInfo_var;
extern "C" void NPBinding_Init_m8_1932 (NPBinding_t8_333 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SingletonPattern_1_t8_334_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2165);
		NPBinding_t8_333_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2164);
		SingletonPattern_1_Init_m8_2019_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484254);
		NPBinding_AddComponentBasedOnPlatform_TisNotificationService_t8_261_m8_2020_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484255);
		NPBinding_AddComponentBasedOnPlatform_TisUI_t8_303_m8_2021_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484256);
		NPBinding_AddComponentBasedOnPlatform_TisUtility_t8_306_m8_2022_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484257);
		s_Il2CppMethodIntialized = true;
	}
	{
		SingletonPattern_1_Init_m8_2019(__this, /*hidden argument*/SingletonPattern_1_Init_m8_2019_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(SingletonPattern_1_t8_334_il2cpp_TypeInfo_var);
		NPBinding_t8_333 * L_0 = ((SingletonPattern_1_t8_334_StaticFields*)SingletonPattern_1_t8_334_il2cpp_TypeInfo_var->static_fields)->___instance_2;
		bool L_1 = Object_op_Inequality_m6_722(NULL /*static, unused*/, L_0, __this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		return;
	}

IL_0017:
	{
		NotificationService_t8_261 * L_2 = NPBinding_AddComponentBasedOnPlatform_TisNotificationService_t8_261_m8_2020(__this, /*hidden argument*/NPBinding_AddComponentBasedOnPlatform_TisNotificationService_t8_261_m8_2020_MethodInfo_var);
		((NPBinding_t8_333_StaticFields*)NPBinding_t8_333_il2cpp_TypeInfo_var->static_fields)->___notificationService_9 = L_2;
		UI_t8_303 * L_3 = NPBinding_AddComponentBasedOnPlatform_TisUI_t8_303_m8_2021(__this, /*hidden argument*/NPBinding_AddComponentBasedOnPlatform_TisUI_t8_303_m8_2021_MethodInfo_var);
		((NPBinding_t8_333_StaticFields*)NPBinding_t8_333_il2cpp_TypeInfo_var->static_fields)->___userInterface_10 = L_3;
		Utility_t8_306 * L_4 = NPBinding_AddComponentBasedOnPlatform_TisUtility_t8_306_m8_2022(__this, /*hidden argument*/NPBinding_AddComponentBasedOnPlatform_TisUtility_t8_306_m8_2022_MethodInfo_var);
		((NPBinding_t8_333_StaticFields*)NPBinding_t8_333_il2cpp_TypeInfo_var->static_fields)->___utility_11 = L_4;
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.NPSettings::.ctor()
extern TypeInfo* CharU5BU5D_t1_16_il2cpp_TypeInfo_var;
extern TypeInfo* ApplicationSettings_t8_320_il2cpp_TypeInfo_var;
extern TypeInfo* UtilitySettings_t8_309_il2cpp_TypeInfo_var;
extern TypeInfo* NotificationServiceSettings_t8_270_il2cpp_TypeInfo_var;
extern TypeInfo* AdvancedScriptableObject_1_t8_336_il2cpp_TypeInfo_var;
extern const MethodInfo* AdvancedScriptableObject_1__ctor_m8_2023_MethodInfo_var;
extern "C" void NPSettings__ctor_m8_1933 (NPSettings_t8_335 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CharU5BU5D_t1_16_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(15);
		ApplicationSettings_t8_320_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2179);
		UtilitySettings_t8_309_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2180);
		NotificationServiceSettings_t8_270_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2181);
		AdvancedScriptableObject_1_t8_336_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2182);
		AdvancedScriptableObject_1__ctor_m8_2023_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484258);
		s_Il2CppMethodIntialized = true;
	}
	{
		CharU5BU5D_t1_16* L_0 = ((CharU5BU5D_t1_16*)SZArrayNew(CharU5BU5D_t1_16_il2cpp_TypeInfo_var, 3));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_0, 0, sizeof(uint16_t))) = (uint16_t)((int32_t)59);
		CharU5BU5D_t1_16* L_1 = L_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_1, 1, sizeof(uint16_t))) = (uint16_t)((int32_t)44);
		CharU5BU5D_t1_16* L_2 = L_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 2);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_2, 2, sizeof(uint16_t))) = (uint16_t)((int32_t)32);
		__this->___defineSeperators_23 = L_2;
		ApplicationSettings_t8_320 * L_3 = (ApplicationSettings_t8_320 *)il2cpp_codegen_object_new (ApplicationSettings_t8_320_il2cpp_TypeInfo_var);
		ApplicationSettings__ctor_m8_1879(L_3, /*hidden argument*/NULL);
		__this->___m_applicationSettings_25 = L_3;
		UtilitySettings_t8_309 * L_4 = (UtilitySettings_t8_309 *)il2cpp_codegen_object_new (UtilitySettings_t8_309_il2cpp_TypeInfo_var);
		UtilitySettings__ctor_m8_1805(L_4, /*hidden argument*/NULL);
		__this->___m_utilitySettings_26 = L_4;
		NotificationServiceSettings_t8_270 * L_5 = (NotificationServiceSettings_t8_270 *)il2cpp_codegen_object_new (NotificationServiceSettings_t8_270_il2cpp_TypeInfo_var);
		NotificationServiceSettings__ctor_m8_1563(L_5, /*hidden argument*/NULL);
		__this->___m_notificationSettings_27 = L_5;
		IL2CPP_RUNTIME_CLASS_INIT(AdvancedScriptableObject_1_t8_336_il2cpp_TypeInfo_var);
		AdvancedScriptableObject_1__ctor_m8_2023(__this, /*hidden argument*/AdvancedScriptableObject_1__ctor_m8_2023_MethodInfo_var);
		return;
	}
}
// VoxelBusters.NativePlugins.ApplicationSettings VoxelBusters.NativePlugins.NPSettings::get_Application()
extern TypeInfo* AdvancedScriptableObject_1_t8_336_il2cpp_TypeInfo_var;
extern const MethodInfo* AdvancedScriptableObject_1_get_Instance_m8_2024_MethodInfo_var;
extern "C" ApplicationSettings_t8_320 * NPSettings_get_Application_m8_1934 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AdvancedScriptableObject_1_t8_336_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2182);
		AdvancedScriptableObject_1_get_Instance_m8_2024_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484259);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AdvancedScriptableObject_1_t8_336_il2cpp_TypeInfo_var);
		NPSettings_t8_335 * L_0 = AdvancedScriptableObject_1_get_Instance_m8_2024(NULL /*static, unused*/, /*hidden argument*/AdvancedScriptableObject_1_get_Instance_m8_2024_MethodInfo_var);
		NullCheck(L_0);
		ApplicationSettings_t8_320 * L_1 = (L_0->___m_applicationSettings_25);
		return L_1;
	}
}
// VoxelBusters.NativePlugins.UtilitySettings VoxelBusters.NativePlugins.NPSettings::get_Utility()
extern TypeInfo* AdvancedScriptableObject_1_t8_336_il2cpp_TypeInfo_var;
extern const MethodInfo* AdvancedScriptableObject_1_get_Instance_m8_2024_MethodInfo_var;
extern "C" UtilitySettings_t8_309 * NPSettings_get_Utility_m8_1935 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AdvancedScriptableObject_1_t8_336_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2182);
		AdvancedScriptableObject_1_get_Instance_m8_2024_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484259);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AdvancedScriptableObject_1_t8_336_il2cpp_TypeInfo_var);
		NPSettings_t8_335 * L_0 = AdvancedScriptableObject_1_get_Instance_m8_2024(NULL /*static, unused*/, /*hidden argument*/AdvancedScriptableObject_1_get_Instance_m8_2024_MethodInfo_var);
		NullCheck(L_0);
		UtilitySettings_t8_309 * L_1 = (L_0->___m_utilitySettings_26);
		return L_1;
	}
}
// VoxelBusters.NativePlugins.NotificationServiceSettings VoxelBusters.NativePlugins.NPSettings::get_Notification()
extern TypeInfo* AdvancedScriptableObject_1_t8_336_il2cpp_TypeInfo_var;
extern const MethodInfo* AdvancedScriptableObject_1_get_Instance_m8_2024_MethodInfo_var;
extern "C" NotificationServiceSettings_t8_270 * NPSettings_get_Notification_m8_1936 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AdvancedScriptableObject_1_t8_336_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2182);
		AdvancedScriptableObject_1_get_Instance_m8_2024_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484259);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AdvancedScriptableObject_1_t8_336_il2cpp_TypeInfo_var);
		NPSettings_t8_335 * L_0 = AdvancedScriptableObject_1_get_Instance_m8_2024(NULL /*static, unused*/, /*hidden argument*/AdvancedScriptableObject_1_get_Instance_m8_2024_MethodInfo_var);
		NullCheck(L_0);
		NotificationServiceSettings_t8_270 * L_1 = (L_0->___m_notificationSettings_27);
		return L_1;
	}
}
// VoxelBusters.AssetStoreProductUtility.AssetStoreProduct VoxelBusters.NativePlugins.NPSettings::get_AssetStoreProduct()
extern "C" AssetStoreProduct_t8_22 * NPSettings_get_AssetStoreProduct_m8_1937 (NPSettings_t8_335 * __this, const MethodInfo* method)
{
	{
		AssetStoreProduct_t8_22 * L_0 = (__this->___m_assetStoreProduct_24);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.NPSettings::Reset()
extern const MethodInfo* AdvancedScriptableObject_1_Reset_m8_2025_MethodInfo_var;
extern "C" void NPSettings_Reset_m8_1938 (NPSettings_t8_335 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AdvancedScriptableObject_1_Reset_m8_2025_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484260);
		s_Il2CppMethodIntialized = true;
	}
	{
		AdvancedScriptableObject_1_Reset_m8_2025(__this, /*hidden argument*/AdvancedScriptableObject_1_Reset_m8_2025_MethodInfo_var);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.NPSettings::OnEnable()
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern const MethodInfo* AdvancedScriptableObject_1_OnEnable_m8_2026_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern "C" void NPSettings_OnEnable_m8_1939 (NPSettings_t8_335 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		AdvancedScriptableObject_1_OnEnable_m8_2026_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484261);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		s_Il2CppMethodIntialized = true;
	}
	{
		AdvancedScriptableObject_1_OnEnable_m8_2026(__this, /*hidden argument*/AdvancedScriptableObject_1_OnEnable_m8_2026_MethodInfo_var);
		ApplicationSettings_t8_320 * L_0 = (__this->___m_applicationSettings_25);
		NullCheck(L_0);
		bool L_1 = ApplicationSettings_get_IsDebugMode_m8_1880(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0025;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_RemoveIgnoreTag_m8_1004(NULL /*static, unused*/, _stringLiteral5368, /*hidden argument*/NULL);
		goto IL_002f;
	}

IL_0025:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_AddIgnoreTag_m8_1003(NULL /*static, unused*/, _stringLiteral5368, /*hidden argument*/NULL);
	}

IL_002f:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.RateMyApp/Settings::.ctor()
extern Il2CppCodeGenString* _stringLiteral5551;
extern Il2CppCodeGenString* _stringLiteral5552;
extern Il2CppCodeGenString* _stringLiteral5553;
extern Il2CppCodeGenString* _stringLiteral5554;
extern Il2CppCodeGenString* _stringLiteral5555;
extern "C" void Settings__ctor_m8_1940 (Settings_t8_310 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral5551 = il2cpp_codegen_string_literal_from_index(5551);
		_stringLiteral5552 = il2cpp_codegen_string_literal_from_index(5552);
		_stringLiteral5553 = il2cpp_codegen_string_literal_from_index(5553);
		_stringLiteral5554 = il2cpp_codegen_string_literal_from_index(5554);
		_stringLiteral5555 = il2cpp_codegen_string_literal_from_index(5555);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___m_title_1 = _stringLiteral5551;
		__this->___m_message_2 = _stringLiteral5552;
		__this->___m_showFirstPromptAfterHours_3 = 2;
		__this->___m_successivePromptAfterHours_4 = 6;
		__this->___m_successivePromptAfterLaunches_5 = 5;
		__this->___m_remindMeLaterButtonText_6 = _stringLiteral5553;
		__this->___m_rateItButtonText_7 = _stringLiteral5554;
		__this->___m_dontAskButtonText_8 = _stringLiteral5555;
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean VoxelBusters.NativePlugins.RateMyApp/Settings::get_IsEnabled()
extern "C" bool Settings_get_IsEnabled_m8_1941 (Settings_t8_310 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_isEnabled_0);
		return L_0;
	}
}
// System.String VoxelBusters.NativePlugins.RateMyApp/Settings::get_Title()
extern "C" String_t* Settings_get_Title_m8_1942 (Settings_t8_310 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_title_1);
		return L_0;
	}
}
// System.String VoxelBusters.NativePlugins.RateMyApp/Settings::get_Message()
extern "C" String_t* Settings_get_Message_m8_1943 (Settings_t8_310 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_message_2);
		return L_0;
	}
}
// System.Int32 VoxelBusters.NativePlugins.RateMyApp/Settings::get_ShowFirstPromptAfterHours()
extern "C" int32_t Settings_get_ShowFirstPromptAfterHours_m8_1944 (Settings_t8_310 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_showFirstPromptAfterHours_3);
		return L_0;
	}
}
// System.Int32 VoxelBusters.NativePlugins.RateMyApp/Settings::get_SuccessivePromptAfterHours()
extern "C" int32_t Settings_get_SuccessivePromptAfterHours_m8_1945 (Settings_t8_310 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_successivePromptAfterHours_4);
		return L_0;
	}
}
// System.Int32 VoxelBusters.NativePlugins.RateMyApp/Settings::get_SuccessivePromptAfterLaunches()
extern "C" int32_t Settings_get_SuccessivePromptAfterLaunches_m8_1946 (Settings_t8_310 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_successivePromptAfterLaunches_5);
		return L_0;
	}
}
// System.String VoxelBusters.NativePlugins.RateMyApp/Settings::get_RemindMeLaterButtonText()
extern "C" String_t* Settings_get_RemindMeLaterButtonText_m8_1947 (Settings_t8_310 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_remindMeLaterButtonText_6);
		return L_0;
	}
}
// System.String VoxelBusters.NativePlugins.RateMyApp/Settings::get_RateItButtonText()
extern "C" String_t* Settings_get_RateItButtonText_m8_1948 (Settings_t8_310 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_rateItButtonText_7);
		return L_0;
	}
}
// System.String VoxelBusters.NativePlugins.RateMyApp/Settings::get_DontAskButtonText()
extern "C" String_t* Settings_get_DontAskButtonText_m8_1949 (Settings_t8_310 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_dontAskButtonText_8);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.RateMyApp::.ctor()
extern "C" void RateMyApp__ctor_m8_1950 (RateMyApp_t8_307 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m6_671(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.RateMyApp::Awake()
extern TypeInfo* StringU5BU5D_t1_238_il2cpp_TypeInfo_var;
extern "C" void RateMyApp_Awake_m8_1951 (RateMyApp_t8_307 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringU5BU5D_t1_238_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(35);
		s_Il2CppMethodIntialized = true;
	}
	{
		UtilitySettings_t8_309 * L_0 = NPSettings_get_Utility_m8_1935(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Settings_t8_310 * L_1 = UtilitySettings_get_RateMyApp_m8_1806(L_0, /*hidden argument*/NULL);
		__this->___m_rateMyAppSettings_8 = L_1;
		StringU5BU5D_t1_238* L_2 = ((StringU5BU5D_t1_238*)SZArrayNew(StringU5BU5D_t1_238_il2cpp_TypeInfo_var, 3));
		Settings_t8_310 * L_3 = (__this->___m_rateMyAppSettings_8);
		NullCheck(L_3);
		String_t* L_4 = Settings_get_RateItButtonText_m8_1948(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_4);
		*((String_t**)(String_t**)SZArrayLdElema(L_2, 0, sizeof(String_t*))) = (String_t*)L_4;
		StringU5BU5D_t1_238* L_5 = L_2;
		Settings_t8_310 * L_6 = (__this->___m_rateMyAppSettings_8);
		NullCheck(L_6);
		String_t* L_7 = Settings_get_RemindMeLaterButtonText_m8_1947(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 1);
		ArrayElementTypeCheck (L_5, L_7);
		*((String_t**)(String_t**)SZArrayLdElema(L_5, 1, sizeof(String_t*))) = (String_t*)L_7;
		StringU5BU5D_t1_238* L_8 = L_5;
		Settings_t8_310 * L_9 = (__this->___m_rateMyAppSettings_8);
		NullCheck(L_9);
		String_t* L_10 = Settings_get_DontAskButtonText_m8_1949(L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_10);
		*((String_t**)(String_t**)SZArrayLdElema(L_8, 2, sizeof(String_t*))) = (String_t*)L_10;
		__this->___m_buttonList_7 = L_8;
		RateMyApp_TrackApplicationUsage_m8_1954(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.RateMyApp::Start()
extern "C" void RateMyApp_Start_m8_1952 (RateMyApp_t8_307 * __this, const MethodInfo* method)
{
	{
		RateMyApp_AskForReview_m8_1956(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.RateMyApp::OnApplicationPause(System.Boolean)
extern "C" void RateMyApp_OnApplicationPause_m8_1953 (RateMyApp_t8_307 * __this, bool ____pauseStatus, const MethodInfo* method)
{
	{
		bool L_0 = ____pauseStatus;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		RateMyApp_AskForReview_m8_1956(__this, /*hidden argument*/NULL);
	}

IL_000c:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.RateMyApp::TrackApplicationUsage()
extern Il2CppCodeGenString* _stringLiteral5546;
extern "C" void RateMyApp_TrackApplicationUsage_m8_1954 (RateMyApp_t8_307 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral5546 = il2cpp_codegen_string_literal_from_index(5546);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = PlayerPrefs_GetInt_m6_804(NULL /*static, unused*/, _stringLiteral5546, 0, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_0+(int32_t)1));
		int32_t L_1 = V_0;
		PlayerPrefs_SetInt_m6_803(NULL /*static, unused*/, _stringLiteral5546, L_1, /*hidden argument*/NULL);
		PlayerPrefs_Save_m6_809(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 VoxelBusters.NativePlugins.RateMyApp::GetAppUsageCount()
extern Il2CppCodeGenString* _stringLiteral5546;
extern "C" int32_t RateMyApp_GetAppUsageCount_m8_1955 (RateMyApp_t8_307 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral5546 = il2cpp_codegen_string_literal_from_index(5546);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = PlayerPrefs_GetInt_m6_804(NULL /*static, unused*/, _stringLiteral5546, 0, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.RateMyApp::AskForReview()
extern "C" void RateMyApp_AskForReview_m8_1956 (RateMyApp_t8_307 * __this, const MethodInfo* method)
{
	{
		bool L_0 = RateMyApp_CanAskForReview_m8_1958(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		int32_t L_1 = RateMyApp_GetAppUsageCount_m8_1955(__this, /*hidden argument*/NULL);
		bool L_2 = RateMyApp_IsEligibleToShowPrompt_m8_1959(__this, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_001e;
		}
	}
	{
		return;
	}

IL_001e:
	{
		RateMyApp_ShowRateMeDialog_m8_1960(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.RateMyApp::AskForReviewNow()
extern "C" void RateMyApp_AskForReviewNow_m8_1957 (RateMyApp_t8_307 * __this, const MethodInfo* method)
{
	{
		RateMyApp_ShowRateMeDialog_m8_1960(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean VoxelBusters.NativePlugins.RateMyApp::CanAskForReview()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5547;
extern Il2CppCodeGenString* _stringLiteral5548;
extern "C" bool RateMyApp_CanAskForReview_m8_1958 (RateMyApp_t8_307 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5547 = il2cpp_codegen_string_literal_from_index(5547);
		_stringLiteral5548 = il2cpp_codegen_string_literal_from_index(5548);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	String_t* V_1 = {0};
	{
		int32_t L_0 = PlayerPrefs_GetInt_m6_804(NULL /*static, unused*/, _stringLiteral5547, 0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0013;
		}
	}
	{
		return 0;
	}

IL_0013:
	{
		String_t* L_1 = PlayerPrefs_GetString_m6_807(NULL /*static, unused*/, _stringLiteral5548, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m1_521(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_003e;
		}
	}
	{
		String_t* L_4 = PlayerSettings_GetBundleVersion_m8_213(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_4;
		String_t* L_5 = V_1;
		String_t* L_6 = V_0;
		NullCheck(L_5);
		int32_t L_7 = String_CompareTo_m1_476(L_5, L_6, /*hidden argument*/NULL);
		if ((((int32_t)L_7) > ((int32_t)0)))
		{
			goto IL_003e;
		}
	}
	{
		return 0;
	}

IL_003e:
	{
		return 1;
	}
}
// System.Boolean VoxelBusters.NativePlugins.RateMyApp::IsEligibleToShowPrompt(System.Int32)
extern TypeInfo* DateTime_t1_150_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5549;
extern Il2CppCodeGenString* _stringLiteral5550;
extern Il2CppCodeGenString* _stringLiteral5546;
extern "C" bool RateMyApp_IsEligibleToShowPrompt_m8_1959 (RateMyApp_t8_307 * __this, int32_t ____appUsageCountUntilNow, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DateTime_t1_150_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		_stringLiteral5549 = il2cpp_codegen_string_literal_from_index(5549);
		_stringLiteral5550 = il2cpp_codegen_string_literal_from_index(5550);
		_stringLiteral5546 = il2cpp_codegen_string_literal_from_index(5546);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	DateTime_t1_150  V_1 = {0};
	String_t* V_2 = {0};
	DateTime_t1_150  V_3 = {0};
	float V_4 = 0.0f;
	TimeSpan_t1_368  V_5 = {0};
	{
		int32_t L_0 = PlayerPrefs_GetInt_m6_804(NULL /*static, unused*/, _stringLiteral5549, (-1), /*hidden argument*/NULL);
		V_0 = (((float)((float)L_0)));
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		DateTime_t1_150  L_1 = DateTime_get_UtcNow_m1_13829(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_1;
		float L_2 = V_0;
		if ((!(((float)L_2) == ((float)(-1.0f)))))
		{
			goto IL_0046;
		}
	}
	{
		Settings_t8_310 * L_3 = (__this->___m_rateMyAppSettings_8);
		NullCheck(L_3);
		int32_t L_4 = Settings_get_ShowFirstPromptAfterHours_m8_1944(L_3, /*hidden argument*/NULL);
		PlayerPrefs_SetInt_m6_803(NULL /*static, unused*/, _stringLiteral5549, L_4, /*hidden argument*/NULL);
		String_t* L_5 = DateTime_ToString_m1_13896((&V_1), /*hidden argument*/NULL);
		PlayerPrefs_SetString_m6_805(NULL /*static, unused*/, _stringLiteral5550, L_5, /*hidden argument*/NULL);
		return 0;
	}

IL_0046:
	{
		String_t* L_6 = PlayerPrefs_GetString_m6_807(NULL /*static, unused*/, _stringLiteral5550, /*hidden argument*/NULL);
		V_2 = L_6;
		String_t* L_7 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		DateTime_t1_150  L_8 = DateTime_Parse_m1_13865(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		V_3 = L_8;
		DateTime_t1_150  L_9 = V_1;
		DateTime_t1_150  L_10 = V_3;
		TimeSpan_t1_368  L_11 = DateTime_op_Subtraction_m1_13909(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		V_5 = L_11;
		double L_12 = TimeSpan_get_TotalHours_m1_14631((&V_5), /*hidden argument*/NULL);
		V_4 = (((float)((float)L_12)));
		float L_13 = V_0;
		float L_14 = V_4;
		if ((!(((float)L_13) > ((float)L_14))))
		{
			goto IL_0075;
		}
	}
	{
		return 0;
	}

IL_0075:
	{
		int32_t L_15 = ____appUsageCountUntilNow;
		Settings_t8_310 * L_16 = (__this->___m_rateMyAppSettings_8);
		NullCheck(L_16);
		int32_t L_17 = Settings_get_SuccessivePromptAfterLaunches_m8_1946(L_16, /*hidden argument*/NULL);
		if ((((int32_t)L_15) > ((int32_t)L_17)))
		{
			goto IL_0088;
		}
	}
	{
		return 0;
	}

IL_0088:
	{
		PlayerPrefs_SetInt_m6_803(NULL /*static, unused*/, _stringLiteral5546, 0, /*hidden argument*/NULL);
		String_t* L_18 = DateTime_ToString_m1_13896((&V_1), /*hidden argument*/NULL);
		PlayerPrefs_SetString_m6_805(NULL /*static, unused*/, _stringLiteral5550, L_18, /*hidden argument*/NULL);
		return 1;
	}
}
// System.Void VoxelBusters.NativePlugins.RateMyApp::ShowRateMeDialog()
extern TypeInfo* AlertDialogCompletion_t8_300_il2cpp_TypeInfo_var;
extern const MethodInfo* RateMyApp_U3CShowRateMeDialogU3Em__15_m8_1964_MethodInfo_var;
extern "C" void RateMyApp_ShowRateMeDialog_m8_1960 (RateMyApp_t8_307 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AlertDialogCompletion_t8_300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2109);
		RateMyApp_U3CShowRateMeDialogU3Em__15_m8_1964_MethodInfo_var = il2cpp_codegen_method_info_from_index(614);
		s_Il2CppMethodIntialized = true;
	}
	{
		UI_t8_303 * L_0 = NPBinding_get_UI_m8_1930(NULL /*static, unused*/, /*hidden argument*/NULL);
		Settings_t8_310 * L_1 = (__this->___m_rateMyAppSettings_8);
		NullCheck(L_1);
		String_t* L_2 = Settings_get_Title_m8_1942(L_1, /*hidden argument*/NULL);
		Settings_t8_310 * L_3 = (__this->___m_rateMyAppSettings_8);
		NullCheck(L_3);
		String_t* L_4 = Settings_get_Message_m8_1943(L_3, /*hidden argument*/NULL);
		StringU5BU5D_t1_238* L_5 = (__this->___m_buttonList_7);
		IntPtr_t L_6 = { (void*)RateMyApp_U3CShowRateMeDialogU3Em__15_m8_1964_MethodInfo_var };
		AlertDialogCompletion_t8_300 * L_7 = (AlertDialogCompletion_t8_300 *)il2cpp_codegen_object_new (AlertDialogCompletion_t8_300_il2cpp_TypeInfo_var);
		AlertDialogCompletion__ctor_m8_1748(L_7, __this, L_6, /*hidden argument*/NULL);
		NullCheck(L_0);
		UI_ShowAlertDialogWithMultipleButtons_m8_1766(L_0, L_2, L_4, L_5, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.RateMyApp::OnPressingRemingMeLater()
extern Il2CppCodeGenString* _stringLiteral5549;
extern "C" void RateMyApp_OnPressingRemingMeLater_m8_1961 (RateMyApp_t8_307 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral5549 = il2cpp_codegen_string_literal_from_index(5549);
		s_Il2CppMethodIntialized = true;
	}
	{
		Settings_t8_310 * L_0 = (__this->___m_rateMyAppSettings_8);
		NullCheck(L_0);
		int32_t L_1 = Settings_get_SuccessivePromptAfterHours_m8_1945(L_0, /*hidden argument*/NULL);
		PlayerPrefs_SetInt_m6_803(NULL /*static, unused*/, _stringLiteral5549, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.RateMyApp::OnPressingRateMyApp()
extern Il2CppCodeGenString* _stringLiteral5548;
extern "C" void RateMyApp_OnPressingRateMyApp_m8_1962 (RateMyApp_t8_307 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral5548 = il2cpp_codegen_string_literal_from_index(5548);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	{
		String_t* L_0 = PlayerSettings_GetBundleVersion_m8_213(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = V_0;
		PlayerPrefs_SetString_m6_805(NULL /*static, unused*/, _stringLiteral5548, L_1, /*hidden argument*/NULL);
		Utility_t8_306 * L_2 = NPBinding_get_Utility_m8_1931(NULL /*static, unused*/, /*hidden argument*/NULL);
		ApplicationSettings_t8_320 * L_3 = NPSettings_get_Application_m8_1934(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		String_t* L_4 = ApplicationSettings_get_StoreIdentifier_m8_1887(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(6 /* System.Void VoxelBusters.NativePlugins.Utility::OpenStoreLink(System.String) */, L_2, L_4);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.RateMyApp::OnPressingDontShow()
extern Il2CppCodeGenString* _stringLiteral5547;
extern "C" void RateMyApp_OnPressingDontShow_m8_1963 (RateMyApp_t8_307 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral5547 = il2cpp_codegen_string_literal_from_index(5547);
		s_Il2CppMethodIntialized = true;
	}
	{
		PlayerPrefs_SetInt_m6_803(NULL /*static, unused*/, _stringLiteral5547, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.RateMyApp::<ShowRateMeDialog>m__15(System.String)
extern "C" void RateMyApp_U3CShowRateMeDialogU3Em__15_m8_1964 (RateMyApp_t8_307 * __this, String_t* ____buttonName, const MethodInfo* method)
{
	{
		String_t* L_0 = ____buttonName;
		Settings_t8_310 * L_1 = (__this->___m_rateMyAppSettings_8);
		NullCheck(L_1);
		String_t* L_2 = Settings_get_RemindMeLaterButtonText_m8_1947(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_3 = String_Equals_m1_441(L_0, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0021;
		}
	}
	{
		VirtActionInvoker0::Invoke(4 /* System.Void VoxelBusters.NativePlugins.RateMyApp::OnPressingRemingMeLater() */, __this);
		goto IL_0048;
	}

IL_0021:
	{
		String_t* L_4 = ____buttonName;
		Settings_t8_310 * L_5 = (__this->___m_rateMyAppSettings_8);
		NullCheck(L_5);
		String_t* L_6 = Settings_get_RateItButtonText_m8_1948(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		bool L_7 = String_Equals_m1_441(L_4, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0042;
		}
	}
	{
		VirtActionInvoker0::Invoke(5 /* System.Void VoxelBusters.NativePlugins.RateMyApp::OnPressingRateMyApp() */, __this);
		goto IL_0048;
	}

IL_0042:
	{
		VirtActionInvoker0::Invoke(6 /* System.Void VoxelBusters.NativePlugins.RateMyApp::OnPressingDontShow() */, __this);
	}

IL_0048:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.WebView::.ctor()
extern "C" void WebView__ctor_m8_1965 (WebView_t8_191 * __this, const MethodInfo* method)
{
	{
		__this->___m_canHide_2 = 1;
		__this->___m_canBounce_3 = 1;
		__this->___m_autoShowOnLoadFinish_6 = 1;
		__this->___m_scalesPageToFit_7 = 1;
		Rect_t6_51  L_0 = {0};
		Rect__ctor_m6_295(&L_0, (0.0f), (0.0f), (-1.0f), (-1.0f), /*hidden argument*/NULL);
		__this->___m_frame_8 = L_0;
		Color_t6_40  L_1 = Color_get_white_m6_276(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___m_backgroundColor_9 = L_1;
		MonoBehaviour__ctor_m6_671(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String VoxelBusters.NativePlugins.WebView::get_UniqueID()
extern "C" String_t* WebView_get_UniqueID_m8_1966 (WebView_t8_191 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CUniqueIDU3Ek__BackingField_10);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.WebView::set_UniqueID(System.String)
extern "C" void WebView_set_UniqueID_m8_1967 (WebView_t8_191 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CUniqueIDU3Ek__BackingField_10 = L_0;
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
