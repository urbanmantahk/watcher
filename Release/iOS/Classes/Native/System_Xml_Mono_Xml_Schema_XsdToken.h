﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "System_Xml_Mono_Xml_Schema_XsdNormalizedString.h"

// Mono.Xml.Schema.XsdToken
struct  XsdToken_t4_9  : public XsdNormalizedString_t4_8
{
};
