﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Predicate_1_gen_4MethodDeclarations.h"

// System.Void System.Predicate`1<UnityEngine.Canvas>::.ctor(System.Object,System.IntPtr)
#define Predicate_1__ctor_m1_23751(__this, ___object, ___method, method) (( void (*) (Predicate_1_t1_2527 *, Object_t *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m1_15354_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<UnityEngine.Canvas>::Invoke(T)
#define Predicate_1_Invoke_m1_23752(__this, ___obj, method) (( bool (*) (Predicate_1_t1_2527 *, Canvas_t6_156 *, const MethodInfo*))Predicate_1_Invoke_m1_15355_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<UnityEngine.Canvas>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m1_23753(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t1_2527 *, Canvas_t6_156 *, AsyncCallback_t1_28 *, Object_t *, const MethodInfo*))Predicate_1_BeginInvoke_m1_15356_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<UnityEngine.Canvas>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m1_23754(__this, ___result, method) (( bool (*) (Predicate_1_t1_2527 *, Object_t *, const MethodInfo*))Predicate_1_EndInvoke_m1_15357_gshared)(__this, ___result, method)
