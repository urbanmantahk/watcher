﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Reflection.Emit.ModuleBuilder[]
struct ModuleBuilderU5BU5D_t1_468;
// System.String
struct String_t;
// System.Reflection.Emit.CustomAttributeBuilder[]
struct CustomAttributeBuilderU5BU5D_t1_469;
// System.Reflection.Emit.MonoResource[]
struct MonoResourceU5BU5D_t1_470;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Reflection.Module[]
struct ModuleU5BU5D_t1_471;
// System.Reflection.Emit.MonoWin32Resource[]
struct MonoWin32ResourceU5BU5D_t1_472;
// System.Reflection.Emit.RefEmitPermissionSet[]
struct RefEmitPermissionSetU5BU5D_t1_473;
// System.Type[]
struct TypeU5BU5D_t1_31;
// System.Type
struct Type_t;
// System.Collections.ArrayList
struct ArrayList_t1_170;
// System.Resources.Win32VersionResource
struct Win32VersionResource_t1_474;
// Mono.Security.StrongName
struct StrongName_t1_232;
// System.Reflection.Emit.ModuleBuilder
struct ModuleBuilder_t1_475;

#include "mscorlib_System_Reflection_Assembly.h"
#include "mscorlib_System_Reflection_Emit_AssemblyBuilderAccess.h"
#include "mscorlib_System_UIntPtr.h"
#include "mscorlib_System_Reflection_Emit_PEFileKinds.h"
#include "mscorlib_System_Reflection_PortableExecutableKinds.h"
#include "mscorlib_System_Reflection_ImageFileMachine.h"
#include "mscorlib_System_Reflection_Emit_NativeResourceType.h"

// System.Reflection.Emit.AssemblyBuilder
struct  AssemblyBuilder_t1_466  : public Assembly_t1_467
{
	// System.UIntPtr System.Reflection.Emit.AssemblyBuilder::dynamic_assembly
	UIntPtr_t  ___dynamic_assembly_11;
	// System.Reflection.MethodInfo System.Reflection.Emit.AssemblyBuilder::entry_point
	MethodInfo_t * ___entry_point_12;
	// System.Reflection.Emit.ModuleBuilder[] System.Reflection.Emit.AssemblyBuilder::modules
	ModuleBuilderU5BU5D_t1_468* ___modules_13;
	// System.String System.Reflection.Emit.AssemblyBuilder::name
	String_t* ___name_14;
	// System.String System.Reflection.Emit.AssemblyBuilder::dir
	String_t* ___dir_15;
	// System.Reflection.Emit.CustomAttributeBuilder[] System.Reflection.Emit.AssemblyBuilder::cattrs
	CustomAttributeBuilderU5BU5D_t1_469* ___cattrs_16;
	// System.Reflection.Emit.MonoResource[] System.Reflection.Emit.AssemblyBuilder::resources
	MonoResourceU5BU5D_t1_470* ___resources_17;
	// System.Byte[] System.Reflection.Emit.AssemblyBuilder::public_key
	ByteU5BU5D_t1_109* ___public_key_18;
	// System.String System.Reflection.Emit.AssemblyBuilder::version
	String_t* ___version_19;
	// System.String System.Reflection.Emit.AssemblyBuilder::culture
	String_t* ___culture_20;
	// System.UInt32 System.Reflection.Emit.AssemblyBuilder::algid
	uint32_t ___algid_21;
	// System.UInt32 System.Reflection.Emit.AssemblyBuilder::flags
	uint32_t ___flags_22;
	// System.Reflection.Emit.PEFileKinds System.Reflection.Emit.AssemblyBuilder::pekind
	int32_t ___pekind_23;
	// System.Boolean System.Reflection.Emit.AssemblyBuilder::delay_sign
	bool ___delay_sign_24;
	// System.UInt32 System.Reflection.Emit.AssemblyBuilder::access
	uint32_t ___access_25;
	// System.Reflection.Module[] System.Reflection.Emit.AssemblyBuilder::loaded_modules
	ModuleU5BU5D_t1_471* ___loaded_modules_26;
	// System.Reflection.Emit.MonoWin32Resource[] System.Reflection.Emit.AssemblyBuilder::win32_resources
	MonoWin32ResourceU5BU5D_t1_472* ___win32_resources_27;
	// System.Reflection.Emit.RefEmitPermissionSet[] System.Reflection.Emit.AssemblyBuilder::permissions_minimum
	RefEmitPermissionSetU5BU5D_t1_473* ___permissions_minimum_28;
	// System.Reflection.Emit.RefEmitPermissionSet[] System.Reflection.Emit.AssemblyBuilder::permissions_optional
	RefEmitPermissionSetU5BU5D_t1_473* ___permissions_optional_29;
	// System.Reflection.Emit.RefEmitPermissionSet[] System.Reflection.Emit.AssemblyBuilder::permissions_refused
	RefEmitPermissionSetU5BU5D_t1_473* ___permissions_refused_30;
	// System.Reflection.PortableExecutableKinds System.Reflection.Emit.AssemblyBuilder::peKind
	int32_t ___peKind_31;
	// System.Reflection.ImageFileMachine System.Reflection.Emit.AssemblyBuilder::machine
	int32_t ___machine_32;
	// System.Boolean System.Reflection.Emit.AssemblyBuilder::corlib_internal
	bool ___corlib_internal_33;
	// System.Type[] System.Reflection.Emit.AssemblyBuilder::type_forwarders
	TypeU5BU5D_t1_31* ___type_forwarders_34;
	// System.Byte[] System.Reflection.Emit.AssemblyBuilder::pktoken
	ByteU5BU5D_t1_109* ___pktoken_35;
	// System.Type System.Reflection.Emit.AssemblyBuilder::corlib_object_type
	Type_t * ___corlib_object_type_36;
	// System.Type System.Reflection.Emit.AssemblyBuilder::corlib_value_type
	Type_t * ___corlib_value_type_37;
	// System.Type System.Reflection.Emit.AssemblyBuilder::corlib_enum_type
	Type_t * ___corlib_enum_type_38;
	// System.Type System.Reflection.Emit.AssemblyBuilder::corlib_void_type
	Type_t * ___corlib_void_type_39;
	// System.Collections.ArrayList System.Reflection.Emit.AssemblyBuilder::resource_writers
	ArrayList_t1_170 * ___resource_writers_40;
	// System.Resources.Win32VersionResource System.Reflection.Emit.AssemblyBuilder::version_res
	Win32VersionResource_t1_474 * ___version_res_41;
	// System.Boolean System.Reflection.Emit.AssemblyBuilder::created
	bool ___created_42;
	// System.Boolean System.Reflection.Emit.AssemblyBuilder::is_module_only
	bool ___is_module_only_43;
	// Mono.Security.StrongName System.Reflection.Emit.AssemblyBuilder::sn
	StrongName_t1_232 * ___sn_44;
	// System.Reflection.Emit.NativeResourceType System.Reflection.Emit.AssemblyBuilder::native_resource
	int32_t ___native_resource_45;
	// System.Boolean System.Reflection.Emit.AssemblyBuilder::is_compiler_context
	bool ___is_compiler_context_46;
	// System.String System.Reflection.Emit.AssemblyBuilder::versioninfo_culture
	String_t* ___versioninfo_culture_47;
	// System.Reflection.Emit.ModuleBuilder System.Reflection.Emit.AssemblyBuilder::manifest_module
	ModuleBuilder_t1_475 * ___manifest_module_48;
};
