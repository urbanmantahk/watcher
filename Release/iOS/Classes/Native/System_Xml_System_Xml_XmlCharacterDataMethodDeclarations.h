﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XmlCharacterData
struct XmlCharacterData_t4_125;
// System.String
struct String_t;
// System.Xml.XmlDocument
struct XmlDocument_t4_123;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Xml.XmlCharacterData::.ctor(System.String,System.Xml.XmlDocument)
extern "C" void XmlCharacterData__ctor_m4_369 (XmlCharacterData_t4_125 * __this, String_t* ___data, XmlDocument_t4_123 * ___doc, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XmlCharacterData::get_Data()
extern "C" String_t* XmlCharacterData_get_Data_m4_370 (XmlCharacterData_t4_125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XmlCharacterData::set_Data(System.String)
extern "C" void XmlCharacterData_set_Data_m4_371 (XmlCharacterData_t4_125 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XmlCharacterData::get_InnerText()
extern "C" String_t* XmlCharacterData_get_InnerText_m4_372 (XmlCharacterData_t4_125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XmlCharacterData::get_Value()
extern "C" String_t* XmlCharacterData_get_Value_m4_373 (XmlCharacterData_t4_125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XmlCharacterData::set_Value(System.String)
extern "C" void XmlCharacterData_set_Value_m4_374 (XmlCharacterData_t4_125 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
