﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.Emit.LocalBuilder
struct LocalBuilder_t1_527;
// System.Type
struct Type_t;
// System.Reflection.Emit.ILGenerator
struct ILGenerator_t1_480;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Guid.h"
#include "mscorlib_System_IntPtr.h"

// System.Void System.Reflection.Emit.LocalBuilder::.ctor(System.Type,System.Reflection.Emit.ILGenerator)
extern "C" void LocalBuilder__ctor_m1_6038 (LocalBuilder_t1_527 * __this, Type_t * ___t, ILGenerator_t1_480 * ___ilgen, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.LocalBuilder::System.Runtime.InteropServices._LocalBuilder.GetIDsOfNames(System.Guid&,System.IntPtr,System.UInt32,System.UInt32,System.IntPtr)
extern "C" void LocalBuilder_System_Runtime_InteropServices__LocalBuilder_GetIDsOfNames_m1_6039 (LocalBuilder_t1_527 * __this, Guid_t1_319 * ___riid, IntPtr_t ___rgszNames, uint32_t ___cNames, uint32_t ___lcid, IntPtr_t ___rgDispId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.LocalBuilder::System.Runtime.InteropServices._LocalBuilder.GetTypeInfo(System.UInt32,System.UInt32,System.IntPtr)
extern "C" void LocalBuilder_System_Runtime_InteropServices__LocalBuilder_GetTypeInfo_m1_6040 (LocalBuilder_t1_527 * __this, uint32_t ___iTInfo, uint32_t ___lcid, IntPtr_t ___ppTInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.LocalBuilder::System.Runtime.InteropServices._LocalBuilder.GetTypeInfoCount(System.UInt32&)
extern "C" void LocalBuilder_System_Runtime_InteropServices__LocalBuilder_GetTypeInfoCount_m1_6041 (LocalBuilder_t1_527 * __this, uint32_t* ___pcTInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.LocalBuilder::System.Runtime.InteropServices._LocalBuilder.Invoke(System.UInt32,System.Guid&,System.UInt32,System.Int16,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr)
extern "C" void LocalBuilder_System_Runtime_InteropServices__LocalBuilder_Invoke_m1_6042 (LocalBuilder_t1_527 * __this, uint32_t ___dispIdMember, Guid_t1_319 * ___riid, uint32_t ___lcid, int16_t ___wFlags, IntPtr_t ___pDispParams, IntPtr_t ___pVarResult, IntPtr_t ___pExcepInfo, IntPtr_t ___puArgErr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.LocalBuilder::SetLocalSymInfo(System.String,System.Int32,System.Int32)
extern "C" void LocalBuilder_SetLocalSymInfo_m1_6043 (LocalBuilder_t1_527 * __this, String_t* ___name, int32_t ___startOffset, int32_t ___endOffset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.LocalBuilder::SetLocalSymInfo(System.String)
extern "C" void LocalBuilder_SetLocalSymInfo_m1_6044 (LocalBuilder_t1_527 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.LocalBuilder::get_LocalType()
extern "C" Type_t * LocalBuilder_get_LocalType_m1_6045 (LocalBuilder_t1_527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.LocalBuilder::get_IsPinned()
extern "C" bool LocalBuilder_get_IsPinned_m1_6046 (LocalBuilder_t1_527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.LocalBuilder::get_LocalIndex()
extern "C" int32_t LocalBuilder_get_LocalIndex_m1_6047 (LocalBuilder_t1_527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.LocalBuilder::Mono_GetLocalIndex(System.Reflection.Emit.LocalBuilder)
extern "C" int32_t LocalBuilder_Mono_GetLocalIndex_m1_6048 (Object_t * __this /* static, unused */, LocalBuilder_t1_527 * ___builder, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Emit.LocalBuilder::get_Name()
extern "C" String_t* LocalBuilder_get_Name_m1_6049 (LocalBuilder_t1_527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.LocalBuilder::get_StartOffset()
extern "C" int32_t LocalBuilder_get_StartOffset_m1_6050 (LocalBuilder_t1_527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.LocalBuilder::get_EndOffset()
extern "C" int32_t LocalBuilder_get_EndOffset_m1_6051 (LocalBuilder_t1_527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
