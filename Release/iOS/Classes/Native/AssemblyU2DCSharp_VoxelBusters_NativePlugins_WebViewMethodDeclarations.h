﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.WebView
struct WebView_t8_191;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.WebView::.ctor()
extern "C" void WebView__ctor_m8_1965 (WebView_t8_191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.WebView::get_UniqueID()
extern "C" String_t* WebView_get_UniqueID_m8_1966 (WebView_t8_191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.WebView::set_UniqueID(System.String)
extern "C" void WebView_set_UniqueID_m8_1967 (WebView_t8_191 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
