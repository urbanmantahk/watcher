﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.SocialNetworkSettings
struct SocialNetworkSettings_t8_291;
// VoxelBusters.NativePlugins.TwitterSettings
struct TwitterSettings_t8_292;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.SocialNetworkSettings::.ctor()
extern "C" void SocialNetworkSettings__ctor_m8_1715 (SocialNetworkSettings_t8_291 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.NativePlugins.TwitterSettings VoxelBusters.NativePlugins.SocialNetworkSettings::get_TwitterSettings()
extern "C" TwitterSettings_t8_292 * SocialNetworkSettings_get_TwitterSettings_m8_1716 (SocialNetworkSettings_t8_291 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
