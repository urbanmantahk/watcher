﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Queue`1<ExifLibrary.ExifProperty>
struct Queue_1_t3_249;

#include "mscorlib_System_ValueType.h"

// System.Collections.Generic.Queue`1/Enumerator<ExifLibrary.ExifProperty>
struct  Enumerator_t3_289 
{
	// System.Collections.Generic.Queue`1<T> System.Collections.Generic.Queue`1/Enumerator::q
	Queue_1_t3_249 * ___q_0;
	// System.Int32 System.Collections.Generic.Queue`1/Enumerator::idx
	int32_t ___idx_1;
	// System.Int32 System.Collections.Generic.Queue`1/Enumerator::ver
	int32_t ___ver_2;
};
