﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t1_2018;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__4.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m1_15990_gshared (Enumerator_t1_2022 * __this, Dictionary_2_t1_2018 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m1_15990(__this, ___dictionary, method) (( void (*) (Enumerator_t1_2022 *, Dictionary_2_t1_2018 *, const MethodInfo*))Enumerator__ctor_m1_15990_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m1_15991_gshared (Enumerator_t1_2022 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_15991(__this, method) (( Object_t * (*) (Enumerator_t1_2022 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_15991_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m1_15992_gshared (Enumerator_t1_2022 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1_15992(__this, method) (( void (*) (Enumerator_t1_2022 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_15992_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t1_284  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_15993_gshared (Enumerator_t1_2022 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_15993(__this, method) (( DictionaryEntry_t1_284  (*) (Enumerator_t1_2022 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_15993_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_15994_gshared (Enumerator_t1_2022 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_15994(__this, method) (( Object_t * (*) (Enumerator_t1_2022 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_15994_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_15995_gshared (Enumerator_t1_2022 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_15995(__this, method) (( Object_t * (*) (Enumerator_t1_2022 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_15995_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m1_15996_gshared (Enumerator_t1_2022 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1_15996(__this, method) (( bool (*) (Enumerator_t1_2022 *, const MethodInfo*))Enumerator_MoveNext_m1_15996_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_Current()
extern "C" KeyValuePair_2_t1_2015  Enumerator_get_Current_m1_15997_gshared (Enumerator_t1_2022 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1_15997(__this, method) (( KeyValuePair_2_t1_2015  (*) (Enumerator_t1_2022 *, const MethodInfo*))Enumerator_get_Current_m1_15997_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m1_15998_gshared (Enumerator_t1_2022 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m1_15998(__this, method) (( Object_t * (*) (Enumerator_t1_2022 *, const MethodInfo*))Enumerator_get_CurrentKey_m1_15998_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_CurrentValue()
extern "C" Object_t * Enumerator_get_CurrentValue_m1_15999_gshared (Enumerator_t1_2022 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m1_15999(__this, method) (( Object_t * (*) (Enumerator_t1_2022 *, const MethodInfo*))Enumerator_get_CurrentValue_m1_15999_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::Reset()
extern "C" void Enumerator_Reset_m1_16000_gshared (Enumerator_t1_2022 * __this, const MethodInfo* method);
#define Enumerator_Reset_m1_16000(__this, method) (( void (*) (Enumerator_t1_2022 *, const MethodInfo*))Enumerator_Reset_m1_16000_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::VerifyState()
extern "C" void Enumerator_VerifyState_m1_16001_gshared (Enumerator_t1_2022 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m1_16001(__this, method) (( void (*) (Enumerator_t1_2022 *, const MethodInfo*))Enumerator_VerifyState_m1_16001_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m1_16002_gshared (Enumerator_t1_2022 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m1_16002(__this, method) (( void (*) (Enumerator_t1_2022 *, const MethodInfo*))Enumerator_VerifyCurrent_m1_16002_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m1_16003_gshared (Enumerator_t1_2022 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1_16003(__this, method) (( void (*) (Enumerator_t1_2022 *, const MethodInfo*))Enumerator_Dispose_m1_16003_gshared)(__this, method)
