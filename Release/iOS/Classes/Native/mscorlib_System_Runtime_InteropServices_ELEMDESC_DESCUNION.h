﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType.h"
#include "mscorlib_System_Runtime_InteropServices_IDLDESC.h"
#include "mscorlib_System_Runtime_InteropServices_PARAMDESC.h"

// System.Runtime.InteropServices.ELEMDESC/DESCUNION
#pragma pack(push, tp, 1)
struct  DESCUNION_t1_782 
{
	union
	{
		// System.Runtime.InteropServices.IDLDESC System.Runtime.InteropServices.ELEMDESC/DESCUNION::idldesc
		struct
		{
			IDLDESC_t1_783  ___idldesc_0;
		};
		// System.Runtime.InteropServices.PARAMDESC System.Runtime.InteropServices.ELEMDESC/DESCUNION::paramdesc
		struct
		{
			PARAMDESC_t1_784  ___paramdesc_1;
		};
	};
};
#pragma pack(pop, tp)
// Native definition for marshalling of: System.Runtime.InteropServices.ELEMDESC/DESCUNION
#pragma pack(push, tp, 1)
struct DESCUNION_t1_782_marshaled
{
	union
	{
		struct
		{
			IDLDESC_t1_783_marshaled ___idldesc_0;
		};
		struct
		{
			PARAMDESC_t1_784_marshaled ___paramdesc_1;
		};
	};
};
#pragma pack(pop, tp)
