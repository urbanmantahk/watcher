﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType.h"
#include "mscorlib_System_Reflection_Emit_EventToken.h"

// System.Reflection.Emit.EventToken
struct  EventToken_t1_504 
{
	// System.Int32 System.Reflection.Emit.EventToken::tokValue
	int32_t ___tokValue_0;
};
struct EventToken_t1_504_StaticFields{
	// System.Reflection.Emit.EventToken System.Reflection.Emit.EventToken::Empty
	EventToken_t1_504  ___Empty_1;
};
