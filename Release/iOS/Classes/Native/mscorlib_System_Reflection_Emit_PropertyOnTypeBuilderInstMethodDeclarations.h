﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.Emit.PropertyOnTypeBuilderInst
struct PropertyOnTypeBuilderInst_t1_548;
// System.Reflection.MonoGenericClass
struct MonoGenericClass_t1_485;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// System.Type
struct Type_t;
// System.String
struct String_t;
// System.Reflection.MethodInfo[]
struct MethodInfoU5BU5D_t1_603;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Reflection.ParameterInfo[]
struct ParameterInfoU5BU5D_t1_1685;
// System.Object
struct Object_t;
// System.Reflection.Binder
struct Binder_t1_585;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Globalization.CultureInfo
struct CultureInfo_t1_277;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_PropertyAttributes.h"
#include "mscorlib_System_Reflection_BindingFlags.h"

// System.Void System.Reflection.Emit.PropertyOnTypeBuilderInst::.ctor(System.Reflection.MonoGenericClass,System.Reflection.PropertyInfo)
extern "C" void PropertyOnTypeBuilderInst__ctor_m1_6330 (PropertyOnTypeBuilderInst_t1_548 * __this, MonoGenericClass_t1_485 * ___instantiation, PropertyInfo_t * ___prop, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.PropertyAttributes System.Reflection.Emit.PropertyOnTypeBuilderInst::get_Attributes()
extern "C" int32_t PropertyOnTypeBuilderInst_get_Attributes_m1_6331 (PropertyOnTypeBuilderInst_t1_548 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.PropertyOnTypeBuilderInst::get_CanRead()
extern "C" bool PropertyOnTypeBuilderInst_get_CanRead_m1_6332 (PropertyOnTypeBuilderInst_t1_548 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.PropertyOnTypeBuilderInst::get_CanWrite()
extern "C" bool PropertyOnTypeBuilderInst_get_CanWrite_m1_6333 (PropertyOnTypeBuilderInst_t1_548 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.PropertyOnTypeBuilderInst::get_PropertyType()
extern "C" Type_t * PropertyOnTypeBuilderInst_get_PropertyType_m1_6334 (PropertyOnTypeBuilderInst_t1_548 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.PropertyOnTypeBuilderInst::get_DeclaringType()
extern "C" Type_t * PropertyOnTypeBuilderInst_get_DeclaringType_m1_6335 (PropertyOnTypeBuilderInst_t1_548 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.PropertyOnTypeBuilderInst::get_ReflectedType()
extern "C" Type_t * PropertyOnTypeBuilderInst_get_ReflectedType_m1_6336 (PropertyOnTypeBuilderInst_t1_548 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Emit.PropertyOnTypeBuilderInst::get_Name()
extern "C" String_t* PropertyOnTypeBuilderInst_get_Name_m1_6337 (PropertyOnTypeBuilderInst_t1_548 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo[] System.Reflection.Emit.PropertyOnTypeBuilderInst::GetAccessors(System.Boolean)
extern "C" MethodInfoU5BU5D_t1_603* PropertyOnTypeBuilderInst_GetAccessors_m1_6338 (PropertyOnTypeBuilderInst_t1_548 * __this, bool ___nonPublic, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Reflection.Emit.PropertyOnTypeBuilderInst::GetGetMethod(System.Boolean)
extern "C" MethodInfo_t * PropertyOnTypeBuilderInst_GetGetMethod_m1_6339 (PropertyOnTypeBuilderInst_t1_548 * __this, bool ___nonPublic, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ParameterInfo[] System.Reflection.Emit.PropertyOnTypeBuilderInst::GetIndexParameters()
extern "C" ParameterInfoU5BU5D_t1_1685* PropertyOnTypeBuilderInst_GetIndexParameters_m1_6340 (PropertyOnTypeBuilderInst_t1_548 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Reflection.Emit.PropertyOnTypeBuilderInst::GetSetMethod(System.Boolean)
extern "C" MethodInfo_t * PropertyOnTypeBuilderInst_GetSetMethod_m1_6341 (PropertyOnTypeBuilderInst_t1_548 * __this, bool ___nonPublic, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Emit.PropertyOnTypeBuilderInst::ToString()
extern "C" String_t* PropertyOnTypeBuilderInst_ToString_m1_6342 (PropertyOnTypeBuilderInst_t1_548 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Reflection.Emit.PropertyOnTypeBuilderInst::GetValue(System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo)
extern "C" Object_t * PropertyOnTypeBuilderInst_GetValue_m1_6343 (PropertyOnTypeBuilderInst_t1_548 * __this, Object_t * ___obj, int32_t ___invokeAttr, Binder_t1_585 * ___binder, ObjectU5BU5D_t1_272* ___index, CultureInfo_t1_277 * ___culture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.PropertyOnTypeBuilderInst::SetValue(System.Object,System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo)
extern "C" void PropertyOnTypeBuilderInst_SetValue_m1_6344 (PropertyOnTypeBuilderInst_t1_548 * __this, Object_t * ___obj, Object_t * ___value, int32_t ___invokeAttr, Binder_t1_585 * ___binder, ObjectU5BU5D_t1_272* ___index, CultureInfo_t1_277 * ___culture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.PropertyOnTypeBuilderInst::IsDefined(System.Type,System.Boolean)
extern "C" bool PropertyOnTypeBuilderInst_IsDefined_m1_6345 (PropertyOnTypeBuilderInst_t1_548 * __this, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Reflection.Emit.PropertyOnTypeBuilderInst::GetCustomAttributes(System.Boolean)
extern "C" ObjectU5BU5D_t1_272* PropertyOnTypeBuilderInst_GetCustomAttributes_m1_6346 (PropertyOnTypeBuilderInst_t1_548 * __this, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Reflection.Emit.PropertyOnTypeBuilderInst::GetCustomAttributes(System.Type,System.Boolean)
extern "C" ObjectU5BU5D_t1_272* PropertyOnTypeBuilderInst_GetCustomAttributes_m1_6347 (PropertyOnTypeBuilderInst_t1_548 * __this, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
