﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AlertControl/<CheckInServer>c__Iterator0
struct U3CCheckInServerU3Ec__Iterator0_t8_8;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void AlertControl/<CheckInServer>c__Iterator0::.ctor()
extern "C" void U3CCheckInServerU3Ec__Iterator0__ctor_m8_67 (U3CCheckInServerU3Ec__Iterator0_t8_8 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AlertControl/<CheckInServer>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CCheckInServerU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m8_68 (U3CCheckInServerU3Ec__Iterator0_t8_8 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AlertControl/<CheckInServer>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CCheckInServerU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m8_69 (U3CCheckInServerU3Ec__Iterator0_t8_8 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AlertControl/<CheckInServer>c__Iterator0::MoveNext()
extern "C" bool U3CCheckInServerU3Ec__Iterator0_MoveNext_m8_70 (U3CCheckInServerU3Ec__Iterator0_t8_8 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AlertControl/<CheckInServer>c__Iterator0::Dispose()
extern "C" void U3CCheckInServerU3Ec__Iterator0_Dispose_m8_71 (U3CCheckInServerU3Ec__Iterator0_t8_8 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AlertControl/<CheckInServer>c__Iterator0::Reset()
extern "C" void U3CCheckInServerU3Ec__Iterator0_Reset_m8_72 (U3CCheckInServerU3Ec__Iterator0_t8_8 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
