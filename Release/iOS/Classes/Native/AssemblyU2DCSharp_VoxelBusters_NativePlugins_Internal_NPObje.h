﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object.h"

// VoxelBusters.NativePlugins.Internal.NPObject
struct  NPObject_t8_222  : public Object_t
{
	// System.String VoxelBusters.NativePlugins.Internal.NPObject::m_instaceID
	String_t* ___m_instaceID_0;
};
