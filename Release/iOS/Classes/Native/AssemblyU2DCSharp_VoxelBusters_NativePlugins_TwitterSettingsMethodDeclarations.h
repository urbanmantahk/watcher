﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.TwitterSettings
struct TwitterSettings_t8_292;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.TwitterSettings::.ctor()
extern "C" void TwitterSettings__ctor_m8_1743 (TwitterSettings_t8_292 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.TwitterSettings::get_ConsumerKey()
extern "C" String_t* TwitterSettings_get_ConsumerKey_m8_1744 (TwitterSettings_t8_292 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.TwitterSettings::set_ConsumerKey(System.String)
extern "C" void TwitterSettings_set_ConsumerKey_m8_1745 (TwitterSettings_t8_292 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.TwitterSettings::get_ConsumerSecret()
extern "C" String_t* TwitterSettings_get_ConsumerSecret_m8_1746 (TwitterSettings_t8_292 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.TwitterSettings::set_ConsumerSecret(System.String)
extern "C" void TwitterSettings_set_ConsumerSecret_m8_1747 (TwitterSettings_t8_292 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
