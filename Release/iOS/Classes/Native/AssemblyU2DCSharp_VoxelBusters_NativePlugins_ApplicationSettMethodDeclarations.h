﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.ApplicationSettings/AndroidSettings
struct AndroidSettings_t8_317;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.ApplicationSettings/AndroidSettings::.ctor()
extern "C" void AndroidSettings__ctor_m8_1863 (AndroidSettings_t8_317 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.ApplicationSettings/AndroidSettings::get_StoreIdentifier()
extern "C" String_t* AndroidSettings_get_StoreIdentifier_m8_1864 (AndroidSettings_t8_317 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.ApplicationSettings/AndroidSettings::set_StoreIdentifier(System.String)
extern "C" void AndroidSettings_set_StoreIdentifier_m8_1865 (AndroidSettings_t8_317 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
