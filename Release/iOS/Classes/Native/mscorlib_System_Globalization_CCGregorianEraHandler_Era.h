﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType.h"

// System.Globalization.CCGregorianEraHandler/Era
struct  Era_t1_352 
{
	// System.Int32 System.Globalization.CCGregorianEraHandler/Era::_nr
	int32_t ____nr_0;
	// System.Int32 System.Globalization.CCGregorianEraHandler/Era::_start
	int32_t ____start_1;
	// System.Int32 System.Globalization.CCGregorianEraHandler/Era::_gregorianYearStart
	int32_t ____gregorianYearStart_2;
	// System.Int32 System.Globalization.CCGregorianEraHandler/Era::_end
	int32_t ____end_3;
	// System.Int32 System.Globalization.CCGregorianEraHandler/Era::_maxYear
	int32_t ____maxYear_4;
};
