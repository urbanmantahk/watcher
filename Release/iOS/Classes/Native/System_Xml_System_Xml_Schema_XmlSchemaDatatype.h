﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Char[]
struct CharU5BU5D_t1_16;
// System.Text.StringBuilder
struct StringBuilder_t1_247;
// Mono.Xml.Schema.XsdAnySimpleType
struct XsdAnySimpleType_t4_3;
// Mono.Xml.Schema.XsdString
struct XsdString_t4_7;
// Mono.Xml.Schema.XsdNormalizedString
struct XsdNormalizedString_t4_8;
// Mono.Xml.Schema.XsdToken
struct XsdToken_t4_9;
// Mono.Xml.Schema.XsdLanguage
struct XsdLanguage_t4_10;
// Mono.Xml.Schema.XsdNMToken
struct XsdNMToken_t4_11;
// Mono.Xml.Schema.XsdNMTokens
struct XsdNMTokens_t4_12;
// Mono.Xml.Schema.XsdName
struct XsdName_t4_13;
// Mono.Xml.Schema.XsdNCName
struct XsdNCName_t4_14;
// Mono.Xml.Schema.XsdID
struct XsdID_t4_15;
// Mono.Xml.Schema.XsdIDRef
struct XsdIDRef_t4_16;
// Mono.Xml.Schema.XsdIDRefs
struct XsdIDRefs_t4_17;
// Mono.Xml.Schema.XsdEntity
struct XsdEntity_t4_18;
// Mono.Xml.Schema.XsdEntities
struct XsdEntities_t4_19;
// Mono.Xml.Schema.XsdNotation
struct XsdNotation_t4_20;
// Mono.Xml.Schema.XsdDecimal
struct XsdDecimal_t4_21;
// Mono.Xml.Schema.XsdInteger
struct XsdInteger_t4_22;
// Mono.Xml.Schema.XsdLong
struct XsdLong_t4_23;
// Mono.Xml.Schema.XsdInt
struct XsdInt_t4_24;
// Mono.Xml.Schema.XsdShort
struct XsdShort_t4_25;
// Mono.Xml.Schema.XsdByte
struct XsdByte_t4_26;
// Mono.Xml.Schema.XsdNonNegativeInteger
struct XsdNonNegativeInteger_t4_27;
// Mono.Xml.Schema.XsdPositiveInteger
struct XsdPositiveInteger_t4_32;
// Mono.Xml.Schema.XsdUnsignedLong
struct XsdUnsignedLong_t4_28;
// Mono.Xml.Schema.XsdUnsignedInt
struct XsdUnsignedInt_t4_29;
// Mono.Xml.Schema.XsdUnsignedShort
struct XsdUnsignedShort_t4_30;
// Mono.Xml.Schema.XsdUnsignedByte
struct XsdUnsignedByte_t4_31;
// Mono.Xml.Schema.XsdNonPositiveInteger
struct XsdNonPositiveInteger_t4_33;
// Mono.Xml.Schema.XsdNegativeInteger
struct XsdNegativeInteger_t4_34;
// Mono.Xml.Schema.XsdFloat
struct XsdFloat_t4_35;
// Mono.Xml.Schema.XsdDouble
struct XsdDouble_t4_36;
// Mono.Xml.Schema.XsdBase64Binary
struct XsdBase64Binary_t4_37;
// Mono.Xml.Schema.XsdBoolean
struct XsdBoolean_t4_40;
// Mono.Xml.Schema.XsdAnyURI
struct XsdAnyURI_t4_41;
// Mono.Xml.Schema.XsdDuration
struct XsdDuration_t4_42;
// Mono.Xml.Schema.XsdDateTime
struct XsdDateTime_t4_45;
// Mono.Xml.Schema.XsdDate
struct XsdDate_t4_46;
// Mono.Xml.Schema.XsdTime
struct XsdTime_t4_47;
// Mono.Xml.Schema.XsdHexBinary
struct XsdHexBinary_t4_38;
// Mono.Xml.Schema.XsdQName
struct XsdQName_t4_39;
// Mono.Xml.Schema.XsdGYearMonth
struct XsdGYearMonth_t4_48;
// Mono.Xml.Schema.XsdGMonthDay
struct XsdGMonthDay_t4_49;
// Mono.Xml.Schema.XsdGYear
struct XsdGYear_t4_50;
// Mono.Xml.Schema.XsdGMonth
struct XsdGMonth_t4_51;
// Mono.Xml.Schema.XsdGDay
struct XsdGDay_t4_52;
// Mono.Xml.Schema.XdtAnyAtomicType
struct XdtAnyAtomicType_t4_5;
// Mono.Xml.Schema.XdtUntypedAtomic
struct XdtUntypedAtomic_t4_6;
// Mono.Xml.Schema.XdtDayTimeDuration
struct XdtDayTimeDuration_t4_43;
// Mono.Xml.Schema.XdtYearMonthDuration
struct XdtYearMonthDuration_t4_44;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t1_92;

#include "mscorlib_System_Object.h"
#include "System_Xml_Mono_Xml_Schema_XsdWhitespaceFacet.h"

// System.Xml.Schema.XmlSchemaDatatype
struct  XmlSchemaDatatype_t4_4  : public Object_t
{
	// Mono.Xml.Schema.XsdWhitespaceFacet System.Xml.Schema.XmlSchemaDatatype::WhitespaceValue
	int32_t ___WhitespaceValue_0;
	// System.Text.StringBuilder System.Xml.Schema.XmlSchemaDatatype::sb
	StringBuilder_t1_247 * ___sb_2;
};
struct XmlSchemaDatatype_t4_4_StaticFields{
	// System.Char[] System.Xml.Schema.XmlSchemaDatatype::wsChars
	CharU5BU5D_t1_16* ___wsChars_1;
	// Mono.Xml.Schema.XsdAnySimpleType System.Xml.Schema.XmlSchemaDatatype::datatypeAnySimpleType
	XsdAnySimpleType_t4_3 * ___datatypeAnySimpleType_3;
	// Mono.Xml.Schema.XsdString System.Xml.Schema.XmlSchemaDatatype::datatypeString
	XsdString_t4_7 * ___datatypeString_4;
	// Mono.Xml.Schema.XsdNormalizedString System.Xml.Schema.XmlSchemaDatatype::datatypeNormalizedString
	XsdNormalizedString_t4_8 * ___datatypeNormalizedString_5;
	// Mono.Xml.Schema.XsdToken System.Xml.Schema.XmlSchemaDatatype::datatypeToken
	XsdToken_t4_9 * ___datatypeToken_6;
	// Mono.Xml.Schema.XsdLanguage System.Xml.Schema.XmlSchemaDatatype::datatypeLanguage
	XsdLanguage_t4_10 * ___datatypeLanguage_7;
	// Mono.Xml.Schema.XsdNMToken System.Xml.Schema.XmlSchemaDatatype::datatypeNMToken
	XsdNMToken_t4_11 * ___datatypeNMToken_8;
	// Mono.Xml.Schema.XsdNMTokens System.Xml.Schema.XmlSchemaDatatype::datatypeNMTokens
	XsdNMTokens_t4_12 * ___datatypeNMTokens_9;
	// Mono.Xml.Schema.XsdName System.Xml.Schema.XmlSchemaDatatype::datatypeName
	XsdName_t4_13 * ___datatypeName_10;
	// Mono.Xml.Schema.XsdNCName System.Xml.Schema.XmlSchemaDatatype::datatypeNCName
	XsdNCName_t4_14 * ___datatypeNCName_11;
	// Mono.Xml.Schema.XsdID System.Xml.Schema.XmlSchemaDatatype::datatypeID
	XsdID_t4_15 * ___datatypeID_12;
	// Mono.Xml.Schema.XsdIDRef System.Xml.Schema.XmlSchemaDatatype::datatypeIDRef
	XsdIDRef_t4_16 * ___datatypeIDRef_13;
	// Mono.Xml.Schema.XsdIDRefs System.Xml.Schema.XmlSchemaDatatype::datatypeIDRefs
	XsdIDRefs_t4_17 * ___datatypeIDRefs_14;
	// Mono.Xml.Schema.XsdEntity System.Xml.Schema.XmlSchemaDatatype::datatypeEntity
	XsdEntity_t4_18 * ___datatypeEntity_15;
	// Mono.Xml.Schema.XsdEntities System.Xml.Schema.XmlSchemaDatatype::datatypeEntities
	XsdEntities_t4_19 * ___datatypeEntities_16;
	// Mono.Xml.Schema.XsdNotation System.Xml.Schema.XmlSchemaDatatype::datatypeNotation
	XsdNotation_t4_20 * ___datatypeNotation_17;
	// Mono.Xml.Schema.XsdDecimal System.Xml.Schema.XmlSchemaDatatype::datatypeDecimal
	XsdDecimal_t4_21 * ___datatypeDecimal_18;
	// Mono.Xml.Schema.XsdInteger System.Xml.Schema.XmlSchemaDatatype::datatypeInteger
	XsdInteger_t4_22 * ___datatypeInteger_19;
	// Mono.Xml.Schema.XsdLong System.Xml.Schema.XmlSchemaDatatype::datatypeLong
	XsdLong_t4_23 * ___datatypeLong_20;
	// Mono.Xml.Schema.XsdInt System.Xml.Schema.XmlSchemaDatatype::datatypeInt
	XsdInt_t4_24 * ___datatypeInt_21;
	// Mono.Xml.Schema.XsdShort System.Xml.Schema.XmlSchemaDatatype::datatypeShort
	XsdShort_t4_25 * ___datatypeShort_22;
	// Mono.Xml.Schema.XsdByte System.Xml.Schema.XmlSchemaDatatype::datatypeByte
	XsdByte_t4_26 * ___datatypeByte_23;
	// Mono.Xml.Schema.XsdNonNegativeInteger System.Xml.Schema.XmlSchemaDatatype::datatypeNonNegativeInteger
	XsdNonNegativeInteger_t4_27 * ___datatypeNonNegativeInteger_24;
	// Mono.Xml.Schema.XsdPositiveInteger System.Xml.Schema.XmlSchemaDatatype::datatypePositiveInteger
	XsdPositiveInteger_t4_32 * ___datatypePositiveInteger_25;
	// Mono.Xml.Schema.XsdUnsignedLong System.Xml.Schema.XmlSchemaDatatype::datatypeUnsignedLong
	XsdUnsignedLong_t4_28 * ___datatypeUnsignedLong_26;
	// Mono.Xml.Schema.XsdUnsignedInt System.Xml.Schema.XmlSchemaDatatype::datatypeUnsignedInt
	XsdUnsignedInt_t4_29 * ___datatypeUnsignedInt_27;
	// Mono.Xml.Schema.XsdUnsignedShort System.Xml.Schema.XmlSchemaDatatype::datatypeUnsignedShort
	XsdUnsignedShort_t4_30 * ___datatypeUnsignedShort_28;
	// Mono.Xml.Schema.XsdUnsignedByte System.Xml.Schema.XmlSchemaDatatype::datatypeUnsignedByte
	XsdUnsignedByte_t4_31 * ___datatypeUnsignedByte_29;
	// Mono.Xml.Schema.XsdNonPositiveInteger System.Xml.Schema.XmlSchemaDatatype::datatypeNonPositiveInteger
	XsdNonPositiveInteger_t4_33 * ___datatypeNonPositiveInteger_30;
	// Mono.Xml.Schema.XsdNegativeInteger System.Xml.Schema.XmlSchemaDatatype::datatypeNegativeInteger
	XsdNegativeInteger_t4_34 * ___datatypeNegativeInteger_31;
	// Mono.Xml.Schema.XsdFloat System.Xml.Schema.XmlSchemaDatatype::datatypeFloat
	XsdFloat_t4_35 * ___datatypeFloat_32;
	// Mono.Xml.Schema.XsdDouble System.Xml.Schema.XmlSchemaDatatype::datatypeDouble
	XsdDouble_t4_36 * ___datatypeDouble_33;
	// Mono.Xml.Schema.XsdBase64Binary System.Xml.Schema.XmlSchemaDatatype::datatypeBase64Binary
	XsdBase64Binary_t4_37 * ___datatypeBase64Binary_34;
	// Mono.Xml.Schema.XsdBoolean System.Xml.Schema.XmlSchemaDatatype::datatypeBoolean
	XsdBoolean_t4_40 * ___datatypeBoolean_35;
	// Mono.Xml.Schema.XsdAnyURI System.Xml.Schema.XmlSchemaDatatype::datatypeAnyURI
	XsdAnyURI_t4_41 * ___datatypeAnyURI_36;
	// Mono.Xml.Schema.XsdDuration System.Xml.Schema.XmlSchemaDatatype::datatypeDuration
	XsdDuration_t4_42 * ___datatypeDuration_37;
	// Mono.Xml.Schema.XsdDateTime System.Xml.Schema.XmlSchemaDatatype::datatypeDateTime
	XsdDateTime_t4_45 * ___datatypeDateTime_38;
	// Mono.Xml.Schema.XsdDate System.Xml.Schema.XmlSchemaDatatype::datatypeDate
	XsdDate_t4_46 * ___datatypeDate_39;
	// Mono.Xml.Schema.XsdTime System.Xml.Schema.XmlSchemaDatatype::datatypeTime
	XsdTime_t4_47 * ___datatypeTime_40;
	// Mono.Xml.Schema.XsdHexBinary System.Xml.Schema.XmlSchemaDatatype::datatypeHexBinary
	XsdHexBinary_t4_38 * ___datatypeHexBinary_41;
	// Mono.Xml.Schema.XsdQName System.Xml.Schema.XmlSchemaDatatype::datatypeQName
	XsdQName_t4_39 * ___datatypeQName_42;
	// Mono.Xml.Schema.XsdGYearMonth System.Xml.Schema.XmlSchemaDatatype::datatypeGYearMonth
	XsdGYearMonth_t4_48 * ___datatypeGYearMonth_43;
	// Mono.Xml.Schema.XsdGMonthDay System.Xml.Schema.XmlSchemaDatatype::datatypeGMonthDay
	XsdGMonthDay_t4_49 * ___datatypeGMonthDay_44;
	// Mono.Xml.Schema.XsdGYear System.Xml.Schema.XmlSchemaDatatype::datatypeGYear
	XsdGYear_t4_50 * ___datatypeGYear_45;
	// Mono.Xml.Schema.XsdGMonth System.Xml.Schema.XmlSchemaDatatype::datatypeGMonth
	XsdGMonth_t4_51 * ___datatypeGMonth_46;
	// Mono.Xml.Schema.XsdGDay System.Xml.Schema.XmlSchemaDatatype::datatypeGDay
	XsdGDay_t4_52 * ___datatypeGDay_47;
	// Mono.Xml.Schema.XdtAnyAtomicType System.Xml.Schema.XmlSchemaDatatype::datatypeAnyAtomicType
	XdtAnyAtomicType_t4_5 * ___datatypeAnyAtomicType_48;
	// Mono.Xml.Schema.XdtUntypedAtomic System.Xml.Schema.XmlSchemaDatatype::datatypeUntypedAtomic
	XdtUntypedAtomic_t4_6 * ___datatypeUntypedAtomic_49;
	// Mono.Xml.Schema.XdtDayTimeDuration System.Xml.Schema.XmlSchemaDatatype::datatypeDayTimeDuration
	XdtDayTimeDuration_t4_43 * ___datatypeDayTimeDuration_50;
	// Mono.Xml.Schema.XdtYearMonthDuration System.Xml.Schema.XmlSchemaDatatype::datatypeYearMonthDuration
	XdtYearMonthDuration_t4_44 * ___datatypeYearMonthDuration_51;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.Schema.XmlSchemaDatatype::<>f__switch$map2A
	Dictionary_2_t1_92 * ___U3CU3Ef__switchU24map2A_52;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.Schema.XmlSchemaDatatype::<>f__switch$map2B
	Dictionary_2_t1_92 * ___U3CU3Ef__switchU24map2B_53;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.Schema.XmlSchemaDatatype::<>f__switch$map2C
	Dictionary_2_t1_92 * ___U3CU3Ef__switchU24map2C_54;
};
