﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "System_Xml_System_Xml_WriteState.h"

// System.Xml.WriteState
struct  WriteState_t4_114 
{
	// System.Int32 System.Xml.WriteState::value__
	int32_t ___value___1;
};
