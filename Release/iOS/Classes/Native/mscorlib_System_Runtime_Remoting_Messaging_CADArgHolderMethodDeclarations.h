﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Messaging.CADArgHolder
struct CADArgHolder_t1_921;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Messaging.CADArgHolder::.ctor(System.Int32)
extern "C" void CADArgHolder__ctor_m1_8293 (CADArgHolder_t1_921 * __this, int32_t ___i, const MethodInfo* method) IL2CPP_METHOD_ATTR;
