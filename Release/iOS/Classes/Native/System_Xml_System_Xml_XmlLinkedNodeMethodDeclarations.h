﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XmlLinkedNode
struct XmlLinkedNode_t4_118;
// System.Xml.XmlDocument
struct XmlDocument_t4_123;
// System.Xml.XmlNode
struct XmlNode_t4_116;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Xml.XmlLinkedNode::.ctor(System.Xml.XmlDocument)
extern "C" void XmlLinkedNode__ctor_m4_571 (XmlLinkedNode_t4_118 * __this, XmlDocument_t4_123 * ___doc, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XmlLinkedNode::get_IsRooted()
extern "C" bool XmlLinkedNode_get_IsRooted_m4_572 (XmlLinkedNode_t4_118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNode System.Xml.XmlLinkedNode::get_NextSibling()
extern "C" XmlNode_t4_116 * XmlLinkedNode_get_NextSibling_m4_573 (XmlLinkedNode_t4_118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlLinkedNode System.Xml.XmlLinkedNode::get_NextLinkedSibling()
extern "C" XmlLinkedNode_t4_118 * XmlLinkedNode_get_NextLinkedSibling_m4_574 (XmlLinkedNode_t4_118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XmlLinkedNode::set_NextLinkedSibling(System.Xml.XmlLinkedNode)
extern "C" void XmlLinkedNode_set_NextLinkedSibling_m4_575 (XmlLinkedNode_t4_118 * __this, XmlLinkedNode_t4_118 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNode System.Xml.XmlLinkedNode::get_PreviousSibling()
extern "C" XmlNode_t4_116 * XmlLinkedNode_get_PreviousSibling_m4_576 (XmlLinkedNode_t4_118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
