﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.X509.X509Crl
struct X509Crl_t1_191;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Collections.ArrayList
struct ArrayList_t1_170;
// Mono.Security.X509.X509Crl/X509CrlEntry
struct X509CrlEntry_t1_189;
// Mono.Security.X509.X509ExtensionCollection
struct X509ExtensionCollection_t1_190;
// System.String
struct String_t;
// Mono.Security.X509.X509Certificate
struct X509Certificate_t1_151;
// System.Security.Cryptography.DSA
struct DSA_t1_160;
// System.Security.Cryptography.RSA
struct RSA_t1_175;
// System.Security.Cryptography.AsymmetricAlgorithm
struct AsymmetricAlgorithm_t1_228;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime.h"

// System.Void Mono.Security.X509.X509Crl::.ctor(System.Byte[])
extern "C" void X509Crl__ctor_m1_2216 (X509Crl_t1_191 * __this, ByteU5BU5D_t1_109* ___crl, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509Crl::Parse(System.Byte[])
extern "C" void X509Crl_Parse_m1_2217 (X509Crl_t1_191 * __this, ByteU5BU5D_t1_109* ___crl, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList Mono.Security.X509.X509Crl::get_Entries()
extern "C" ArrayList_t1_170 * X509Crl_get_Entries_m1_2218 (X509Crl_t1_191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Crl/X509CrlEntry Mono.Security.X509.X509Crl::get_Item(System.Int32)
extern "C" X509CrlEntry_t1_189 * X509Crl_get_Item_m1_2219 (X509Crl_t1_191 * __this, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Crl/X509CrlEntry Mono.Security.X509.X509Crl::get_Item(System.Byte[])
extern "C" X509CrlEntry_t1_189 * X509Crl_get_Item_m1_2220 (X509Crl_t1_191 * __this, ByteU5BU5D_t1_109* ___serialNumber, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509ExtensionCollection Mono.Security.X509.X509Crl::get_Extensions()
extern "C" X509ExtensionCollection_t1_190 * X509Crl_get_Extensions_m1_2221 (X509Crl_t1_191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.X509.X509Crl::get_Hash()
extern "C" ByteU5BU5D_t1_109* X509Crl_get_Hash_m1_2222 (X509Crl_t1_191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.X509.X509Crl::get_IssuerName()
extern "C" String_t* X509Crl_get_IssuerName_m1_2223 (X509Crl_t1_191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Mono.Security.X509.X509Crl::get_NextUpdate()
extern "C" DateTime_t1_150  X509Crl_get_NextUpdate_m1_2224 (X509Crl_t1_191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Mono.Security.X509.X509Crl::get_ThisUpdate()
extern "C" DateTime_t1_150  X509Crl_get_ThisUpdate_m1_2225 (X509Crl_t1_191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.X509.X509Crl::get_SignatureAlgorithm()
extern "C" String_t* X509Crl_get_SignatureAlgorithm_m1_2226 (X509Crl_t1_191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.X509.X509Crl::get_Signature()
extern "C" ByteU5BU5D_t1_109* X509Crl_get_Signature_m1_2227 (X509Crl_t1_191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.X509.X509Crl::get_RawData()
extern "C" ByteU5BU5D_t1_109* X509Crl_get_RawData_m1_2228 (X509Crl_t1_191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte Mono.Security.X509.X509Crl::get_Version()
extern "C" uint8_t X509Crl_get_Version_m1_2229 (X509Crl_t1_191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509Crl::get_IsCurrent()
extern "C" bool X509Crl_get_IsCurrent_m1_2230 (X509Crl_t1_191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509Crl::WasCurrent(System.DateTime)
extern "C" bool X509Crl_WasCurrent_m1_2231 (X509Crl_t1_191 * __this, DateTime_t1_150  ___instant, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.X509.X509Crl::GetBytes()
extern "C" ByteU5BU5D_t1_109* X509Crl_GetBytes_m1_2232 (X509Crl_t1_191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509Crl::Compare(System.Byte[],System.Byte[])
extern "C" bool X509Crl_Compare_m1_2233 (X509Crl_t1_191 * __this, ByteU5BU5D_t1_109* ___array1, ByteU5BU5D_t1_109* ___array2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Crl/X509CrlEntry Mono.Security.X509.X509Crl::GetCrlEntry(Mono.Security.X509.X509Certificate)
extern "C" X509CrlEntry_t1_189 * X509Crl_GetCrlEntry_m1_2234 (X509Crl_t1_191 * __this, X509Certificate_t1_151 * ___x509, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Crl/X509CrlEntry Mono.Security.X509.X509Crl::GetCrlEntry(System.Byte[])
extern "C" X509CrlEntry_t1_189 * X509Crl_GetCrlEntry_m1_2235 (X509Crl_t1_191 * __this, ByteU5BU5D_t1_109* ___serialNumber, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509Crl::VerifySignature(Mono.Security.X509.X509Certificate)
extern "C" bool X509Crl_VerifySignature_m1_2236 (X509Crl_t1_191 * __this, X509Certificate_t1_151 * ___x509, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.X509.X509Crl::GetHashName()
extern "C" String_t* X509Crl_GetHashName_m1_2237 (X509Crl_t1_191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509Crl::VerifySignature(System.Security.Cryptography.DSA)
extern "C" bool X509Crl_VerifySignature_m1_2238 (X509Crl_t1_191 * __this, DSA_t1_160 * ___dsa, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509Crl::VerifySignature(System.Security.Cryptography.RSA)
extern "C" bool X509Crl_VerifySignature_m1_2239 (X509Crl_t1_191 * __this, RSA_t1_175 * ___rsa, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509Crl::VerifySignature(System.Security.Cryptography.AsymmetricAlgorithm)
extern "C" bool X509Crl_VerifySignature_m1_2240 (X509Crl_t1_191 * __this, AsymmetricAlgorithm_t1_228 * ___aa, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Crl Mono.Security.X509.X509Crl::CreateFromFile(System.String)
extern "C" X509Crl_t1_191 * X509Crl_CreateFromFile_m1_2241 (Object_t * __this /* static, unused */, String_t* ___filename, const MethodInfo* method) IL2CPP_METHOD_ATTR;
