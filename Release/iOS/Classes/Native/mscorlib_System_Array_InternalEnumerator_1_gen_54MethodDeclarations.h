﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_54.h"
#include "mscorlib_System_Reflection_Emit_RefEmitPermissionSet.h"

// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.RefEmitPermissionSet>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_16167_gshared (InternalEnumerator_1_t1_2046 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1_16167(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1_2046 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m1_16167_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.RefEmitPermissionSet>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_16168_gshared (InternalEnumerator_1_t1_2046 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_16168(__this, method) (( void (*) (InternalEnumerator_1_t1_2046 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_16168_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.RefEmitPermissionSet>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_16169_gshared (InternalEnumerator_1_t1_2046 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_16169(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1_2046 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_16169_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.RefEmitPermissionSet>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_16170_gshared (InternalEnumerator_1_t1_2046 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1_16170(__this, method) (( void (*) (InternalEnumerator_1_t1_2046 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1_16170_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.RefEmitPermissionSet>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_16171_gshared (InternalEnumerator_1_t1_2046 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1_16171(__this, method) (( bool (*) (InternalEnumerator_1_t1_2046 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1_16171_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.RefEmitPermissionSet>::get_Current()
extern "C" RefEmitPermissionSet_t1_463  InternalEnumerator_1_get_Current_m1_16172_gshared (InternalEnumerator_1_t1_2046 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1_16172(__this, method) (( RefEmitPermissionSet_t1_463  (*) (InternalEnumerator_1_t1_2046 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1_16172_gshared)(__this, method)
