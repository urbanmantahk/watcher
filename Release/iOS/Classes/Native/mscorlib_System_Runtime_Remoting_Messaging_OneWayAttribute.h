﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Attribute.h"

// System.Runtime.Remoting.Messaging.OneWayAttribute
struct  OneWayAttribute_t1_954  : public Attribute_t1_2
{
};
