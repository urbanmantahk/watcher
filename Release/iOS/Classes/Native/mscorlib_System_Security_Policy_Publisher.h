﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Security.Cryptography.X509Certificates.X509Certificate
struct X509Certificate_t1_1179;

#include "mscorlib_System_Object.h"

// System.Security.Policy.Publisher
struct  Publisher_t1_1359  : public Object_t
{
	// System.Security.Cryptography.X509Certificates.X509Certificate System.Security.Policy.Publisher::m_cert
	X509Certificate_t1_1179 * ___m_cert_0;
};
