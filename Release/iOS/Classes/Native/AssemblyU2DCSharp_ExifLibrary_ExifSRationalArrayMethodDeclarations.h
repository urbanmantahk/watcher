﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExifLibrary.ExifSRationalArray
struct ExifSRationalArray_t8_128;
// ExifLibrary.MathEx/Fraction32[]
struct Fraction32U5BU5D_t8_129;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Single[]
struct SingleU5BU5D_t1_1695;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTag.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifInterOperability.h"

// System.Void ExifLibrary.ExifSRationalArray::.ctor(ExifLibrary.ExifTag,ExifLibrary.MathEx/Fraction32[])
extern "C" void ExifSRationalArray__ctor_m8_619 (ExifSRationalArray_t8_128 * __this, int32_t ___tag, Fraction32U5BU5D_t8_129* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ExifLibrary.ExifSRationalArray::get__Value()
extern "C" Object_t * ExifSRationalArray_get__Value_m8_620 (ExifSRationalArray_t8_128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.ExifSRationalArray::set__Value(System.Object)
extern "C" void ExifSRationalArray_set__Value_m8_621 (ExifSRationalArray_t8_128 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/Fraction32[] ExifLibrary.ExifSRationalArray::get_Value()
extern "C" Fraction32U5BU5D_t8_129* ExifSRationalArray_get_Value_m8_622 (ExifSRationalArray_t8_128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.ExifSRationalArray::set_Value(ExifLibrary.MathEx/Fraction32[])
extern "C" void ExifSRationalArray_set_Value_m8_623 (ExifSRationalArray_t8_128 * __this, Fraction32U5BU5D_t8_129* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExifLibrary.ExifSRationalArray::ToString()
extern "C" String_t* ExifSRationalArray_ToString_m8_624 (ExifSRationalArray_t8_128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.ExifInterOperability ExifLibrary.ExifSRationalArray::get_Interoperability()
extern "C" ExifInterOperability_t8_113  ExifSRationalArray_get_Interoperability_m8_625 (ExifSRationalArray_t8_128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single[] ExifLibrary.ExifSRationalArray::op_Explicit(ExifLibrary.ExifSRationalArray)
extern "C" SingleU5BU5D_t1_1695* ExifSRationalArray_op_Explicit_m8_626 (Object_t * __this /* static, unused */, ExifSRationalArray_t8_128 * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
