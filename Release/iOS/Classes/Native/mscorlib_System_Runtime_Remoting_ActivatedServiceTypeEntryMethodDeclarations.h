﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.ActivatedServiceTypeEntry
struct ActivatedServiceTypeEntry_t1_1010;
// System.Type
struct Type_t;
// System.String
struct String_t;
// System.Runtime.Remoting.Contexts.IContextAttribute[]
struct IContextAttributeU5BU5D_t1_1722;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.ActivatedServiceTypeEntry::.ctor(System.Type)
extern "C" void ActivatedServiceTypeEntry__ctor_m1_9043 (ActivatedServiceTypeEntry_t1_1010 * __this, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.ActivatedServiceTypeEntry::.ctor(System.String,System.String)
extern "C" void ActivatedServiceTypeEntry__ctor_m1_9044 (ActivatedServiceTypeEntry_t1_1010 * __this, String_t* ___typeName, String_t* ___assemblyName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Contexts.IContextAttribute[] System.Runtime.Remoting.ActivatedServiceTypeEntry::get_ContextAttributes()
extern "C" IContextAttributeU5BU5D_t1_1722* ActivatedServiceTypeEntry_get_ContextAttributes_m1_9045 (ActivatedServiceTypeEntry_t1_1010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.ActivatedServiceTypeEntry::set_ContextAttributes(System.Runtime.Remoting.Contexts.IContextAttribute[])
extern "C" void ActivatedServiceTypeEntry_set_ContextAttributes_m1_9046 (ActivatedServiceTypeEntry_t1_1010 * __this, IContextAttributeU5BU5D_t1_1722* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Runtime.Remoting.ActivatedServiceTypeEntry::get_ObjectType()
extern "C" Type_t * ActivatedServiceTypeEntry_get_ObjectType_m1_9047 (ActivatedServiceTypeEntry_t1_1010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.ActivatedServiceTypeEntry::ToString()
extern "C" String_t* ActivatedServiceTypeEntry_ToString_m1_9048 (ActivatedServiceTypeEntry_t1_1010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
