﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_37.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_15900_gshared (InternalEnumerator_1_t1_2016 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1_15900(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1_2016 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m1_15900_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_15901_gshared (InternalEnumerator_1_t1_2016 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_15901(__this, method) (( void (*) (InternalEnumerator_1_t1_2016 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_15901_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_15902_gshared (InternalEnumerator_1_t1_2016 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_15902(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1_2016 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_15902_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_15903_gshared (InternalEnumerator_1_t1_2016 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1_15903(__this, method) (( void (*) (InternalEnumerator_1_t1_2016 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1_15903_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_15904_gshared (InternalEnumerator_1_t1_2016 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1_15904(__this, method) (( bool (*) (InternalEnumerator_1_t1_2016 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1_15904_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current()
extern "C" KeyValuePair_2_t1_2015  InternalEnumerator_1_get_Current_m1_15905_gshared (InternalEnumerator_1_t1_2016 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1_15905(__this, method) (( KeyValuePair_2_t1_2015  (*) (InternalEnumerator_1_t1_2016 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1_15905_gshared)(__this, method)
