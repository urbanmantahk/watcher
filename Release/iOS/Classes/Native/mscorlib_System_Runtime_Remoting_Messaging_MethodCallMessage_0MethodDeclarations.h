﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Messaging.MethodCallMessageWrapper
struct MethodCallMessageWrapper_t1_946;
// System.Runtime.Remoting.Messaging.IMethodCallMessage
struct IMethodCallMessage_t1_949;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Runtime.Remoting.Messaging.LogicalCallContext
struct LogicalCallContext_t1_941;
// System.Reflection.MethodBase
struct MethodBase_t1_335;
// System.String
struct String_t;
// System.Object
struct Object_t;
// System.Collections.IDictionary
struct IDictionary_t1_35;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Messaging.MethodCallMessageWrapper::.ctor(System.Runtime.Remoting.Messaging.IMethodCallMessage)
extern "C" void MethodCallMessageWrapper__ctor_m1_8458 (MethodCallMessageWrapper_t1_946 * __this, Object_t * ___msg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.Remoting.Messaging.MethodCallMessageWrapper::get_ArgCount()
extern "C" int32_t MethodCallMessageWrapper_get_ArgCount_m1_8459 (MethodCallMessageWrapper_t1_946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Runtime.Remoting.Messaging.MethodCallMessageWrapper::get_Args()
extern "C" ObjectU5BU5D_t1_272* MethodCallMessageWrapper_get_Args_m1_8460 (MethodCallMessageWrapper_t1_946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Messaging.MethodCallMessageWrapper::set_Args(System.Object[])
extern "C" void MethodCallMessageWrapper_set_Args_m1_8461 (MethodCallMessageWrapper_t1_946 * __this, ObjectU5BU5D_t1_272* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Remoting.Messaging.MethodCallMessageWrapper::get_HasVarArgs()
extern "C" bool MethodCallMessageWrapper_get_HasVarArgs_m1_8462 (MethodCallMessageWrapper_t1_946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.Remoting.Messaging.MethodCallMessageWrapper::get_InArgCount()
extern "C" int32_t MethodCallMessageWrapper_get_InArgCount_m1_8463 (MethodCallMessageWrapper_t1_946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Runtime.Remoting.Messaging.MethodCallMessageWrapper::get_InArgs()
extern "C" ObjectU5BU5D_t1_272* MethodCallMessageWrapper_get_InArgs_m1_8464 (MethodCallMessageWrapper_t1_946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Messaging.LogicalCallContext System.Runtime.Remoting.Messaging.MethodCallMessageWrapper::get_LogicalCallContext()
extern "C" LogicalCallContext_t1_941 * MethodCallMessageWrapper_get_LogicalCallContext_m1_8465 (MethodCallMessageWrapper_t1_946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodBase System.Runtime.Remoting.Messaging.MethodCallMessageWrapper::get_MethodBase()
extern "C" MethodBase_t1_335 * MethodCallMessageWrapper_get_MethodBase_m1_8466 (MethodCallMessageWrapper_t1_946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Messaging.MethodCallMessageWrapper::get_MethodName()
extern "C" String_t* MethodCallMessageWrapper_get_MethodName_m1_8467 (MethodCallMessageWrapper_t1_946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Messaging.MethodCallMessageWrapper::get_MethodSignature()
extern "C" Object_t * MethodCallMessageWrapper_get_MethodSignature_m1_8468 (MethodCallMessageWrapper_t1_946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IDictionary System.Runtime.Remoting.Messaging.MethodCallMessageWrapper::get_Properties()
extern "C" Object_t * MethodCallMessageWrapper_get_Properties_m1_8469 (MethodCallMessageWrapper_t1_946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Messaging.MethodCallMessageWrapper::get_TypeName()
extern "C" String_t* MethodCallMessageWrapper_get_TypeName_m1_8470 (MethodCallMessageWrapper_t1_946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Messaging.MethodCallMessageWrapper::get_Uri()
extern "C" String_t* MethodCallMessageWrapper_get_Uri_m1_8471 (MethodCallMessageWrapper_t1_946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Messaging.MethodCallMessageWrapper::set_Uri(System.String)
extern "C" void MethodCallMessageWrapper_set_Uri_m1_8472 (MethodCallMessageWrapper_t1_946 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Messaging.MethodCallMessageWrapper::GetArg(System.Int32)
extern "C" Object_t * MethodCallMessageWrapper_GetArg_m1_8473 (MethodCallMessageWrapper_t1_946 * __this, int32_t ___argNum, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Messaging.MethodCallMessageWrapper::GetArgName(System.Int32)
extern "C" String_t* MethodCallMessageWrapper_GetArgName_m1_8474 (MethodCallMessageWrapper_t1_946 * __this, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Messaging.MethodCallMessageWrapper::GetInArg(System.Int32)
extern "C" Object_t * MethodCallMessageWrapper_GetInArg_m1_8475 (MethodCallMessageWrapper_t1_946 * __this, int32_t ___argNum, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Messaging.MethodCallMessageWrapper::GetInArgName(System.Int32)
extern "C" String_t* MethodCallMessageWrapper_GetInArgName_m1_8476 (MethodCallMessageWrapper_t1_946 * __this, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
