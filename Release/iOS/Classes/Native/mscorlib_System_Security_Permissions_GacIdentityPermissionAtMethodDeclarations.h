﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Permissions.GacIdentityPermissionAttribute
struct GacIdentityPermissionAttribute_t1_1278;
// System.Security.IPermission
struct IPermission_t1_1400;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Permissions_SecurityAction.h"

// System.Void System.Security.Permissions.GacIdentityPermissionAttribute::.ctor(System.Security.Permissions.SecurityAction)
extern "C" void GacIdentityPermissionAttribute__ctor_m1_10890 (GacIdentityPermissionAttribute_t1_1278 * __this, int32_t ___action, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.GacIdentityPermissionAttribute::CreatePermission()
extern "C" Object_t * GacIdentityPermissionAttribute_CreatePermission_m1_10891 (GacIdentityPermissionAttribute_t1_1278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
