﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Reflection.Assembly
struct Assembly_t1_467;

#include "mscorlib_System_EventArgs.h"

// System.AssemblyLoadEventArgs
struct  AssemblyLoadEventArgs_t1_1504  : public EventArgs_t1_158
{
	// System.Reflection.Assembly System.AssemblyLoadEventArgs::m_loadedAssembly
	Assembly_t1_467 * ___m_loadedAssembly_1;
};
