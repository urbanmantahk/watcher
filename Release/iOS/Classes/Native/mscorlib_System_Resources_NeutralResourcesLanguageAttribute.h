﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Attribute.h"
#include "mscorlib_System_Resources_UltimateResourceFallbackLocation.h"

// System.Resources.NeutralResourcesLanguageAttribute
struct  NeutralResourcesLanguageAttribute_t1_644  : public Attribute_t1_2
{
	// System.String System.Resources.NeutralResourcesLanguageAttribute::culture
	String_t* ___culture_0;
	// System.Resources.UltimateResourceFallbackLocation System.Resources.NeutralResourcesLanguageAttribute::loc
	int32_t ___loc_1;
};
