﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String[]
struct StringU5BU5D_t1_238;

#include "mscorlib_System_Object.h"
#include "mscorlib_System_DateTime.h"

// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapTime
struct  SoapTime_t1_993  : public Object_t
{
	// System.DateTime System.Runtime.Remoting.Metadata.W3cXsd2001.SoapTime::_value
	DateTime_t1_150  ____value_1;
};
struct SoapTime_t1_993_StaticFields{
	// System.String[] System.Runtime.Remoting.Metadata.W3cXsd2001.SoapTime::_datetimeFormats
	StringU5BU5D_t1_238* ____datetimeFormats_0;
};
