﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType.h"
#include "AssemblyU2DCSharp_ExifLibrary_MathEx_Fraction32.h"

// ExifLibrary.MathEx/Fraction32
struct  Fraction32_t8_127 
{
	// System.Boolean ExifLibrary.MathEx/Fraction32::mIsNegative
	bool ___mIsNegative_1;
	// System.Int32 ExifLibrary.MathEx/Fraction32::mNumerator
	int32_t ___mNumerator_2;
	// System.Int32 ExifLibrary.MathEx/Fraction32::mDenominator
	int32_t ___mDenominator_3;
};
struct Fraction32_t8_127_StaticFields{
	// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::NaN
	Fraction32_t8_127  ___NaN_4;
	// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::NegativeInfinity
	Fraction32_t8_127  ___NegativeInfinity_5;
	// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::PositiveInfinity
	Fraction32_t8_127  ___PositiveInfinity_6;
};
// Native definition for marshalling of: ExifLibrary.MathEx/Fraction32
struct Fraction32_t8_127_marshaled
{
	int32_t ___mIsNegative_1;
	int32_t ___mNumerator_2;
	int32_t ___mDenominator_3;
};
