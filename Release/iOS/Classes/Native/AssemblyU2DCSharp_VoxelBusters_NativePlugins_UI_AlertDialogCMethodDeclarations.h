﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.UI/AlertDialogCompletion
struct AlertDialogCompletion_t8_300;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_String.h"

// System.Void VoxelBusters.NativePlugins.UI/AlertDialogCompletion::.ctor(System.Object,System.IntPtr)
extern "C" void AlertDialogCompletion__ctor_m8_1748 (AlertDialogCompletion_t8_300 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.UI/AlertDialogCompletion::Invoke(System.String)
extern "C" void AlertDialogCompletion_Invoke_m8_1749 (AlertDialogCompletion_t8_300 * __this, String_t* ____buttonPressed, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_AlertDialogCompletion_t8_300(Il2CppObject* delegate, String_t* ____buttonPressed);
// System.IAsyncResult VoxelBusters.NativePlugins.UI/AlertDialogCompletion::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C" Object_t * AlertDialogCompletion_BeginInvoke_m8_1750 (AlertDialogCompletion_t8_300 * __this, String_t* ____buttonPressed, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.UI/AlertDialogCompletion::EndInvoke(System.IAsyncResult)
extern "C" void AlertDialogCompletion_EndInvoke_m8_1751 (AlertDialogCompletion_t8_300 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
