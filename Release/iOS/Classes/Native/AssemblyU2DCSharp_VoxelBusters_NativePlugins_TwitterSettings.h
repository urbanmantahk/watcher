﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object.h"

// VoxelBusters.NativePlugins.TwitterSettings
struct  TwitterSettings_t8_292  : public Object_t
{
	// System.String VoxelBusters.NativePlugins.TwitterSettings::m_consumerKey
	String_t* ___m_consumerKey_0;
	// System.String VoxelBusters.NativePlugins.TwitterSettings::m_consumerSecret
	String_t* ___m_consumerSecret_1;
};
