﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.IO.FileInfo
struct FileInfo_t1_422;
// System.String
struct String_t;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;
// System.IO.DirectoryInfo
struct DirectoryInfo_t1_399;
// System.IO.StreamReader
struct StreamReader_t1_447;
// System.IO.StreamWriter
struct StreamWriter_t1_448;
// System.IO.FileStream
struct FileStream_t1_146;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_IO_FileMode.h"
#include "mscorlib_System_IO_FileAccess.h"
#include "mscorlib_System_IO_FileShare.h"

// System.Void System.IO.FileInfo::.ctor(System.String)
extern "C" void FileInfo__ctor_m1_4897 (FileInfo_t1_422 * __this, String_t* ___fileName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.FileInfo::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void FileInfo__ctor_m1_4898 (FileInfo_t1_422 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.FileInfo::InternalRefresh()
extern "C" void FileInfo_InternalRefresh_m1_4899 (FileInfo_t1_422 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IO.FileInfo::get_Exists()
extern "C" bool FileInfo_get_Exists_m1_4900 (FileInfo_t1_422 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.IO.FileInfo::get_Name()
extern "C" String_t* FileInfo_get_Name_m1_4901 (FileInfo_t1_422 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IO.FileInfo::get_IsReadOnly()
extern "C" bool FileInfo_get_IsReadOnly_m1_4902 (FileInfo_t1_422 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.FileInfo::set_IsReadOnly(System.Boolean)
extern "C" void FileInfo_set_IsReadOnly_m1_4903 (FileInfo_t1_422 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.FileInfo::Encrypt()
extern "C" void FileInfo_Encrypt_m1_4904 (FileInfo_t1_422 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.FileInfo::Decrypt()
extern "C" void FileInfo_Decrypt_m1_4905 (FileInfo_t1_422 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.IO.FileInfo::get_Length()
extern "C" int64_t FileInfo_get_Length_m1_4906 (FileInfo_t1_422 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.IO.FileInfo::get_DirectoryName()
extern "C" String_t* FileInfo_get_DirectoryName_m1_4907 (FileInfo_t1_422 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.DirectoryInfo System.IO.FileInfo::get_Directory()
extern "C" DirectoryInfo_t1_399 * FileInfo_get_Directory_m1_4908 (FileInfo_t1_422 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.StreamReader System.IO.FileInfo::OpenText()
extern "C" StreamReader_t1_447 * FileInfo_OpenText_m1_4909 (FileInfo_t1_422 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.StreamWriter System.IO.FileInfo::CreateText()
extern "C" StreamWriter_t1_448 * FileInfo_CreateText_m1_4910 (FileInfo_t1_422 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.StreamWriter System.IO.FileInfo::AppendText()
extern "C" StreamWriter_t1_448 * FileInfo_AppendText_m1_4911 (FileInfo_t1_422 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.FileStream System.IO.FileInfo::Create()
extern "C" FileStream_t1_146 * FileInfo_Create_m1_4912 (FileInfo_t1_422 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.FileStream System.IO.FileInfo::OpenRead()
extern "C" FileStream_t1_146 * FileInfo_OpenRead_m1_4913 (FileInfo_t1_422 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.FileStream System.IO.FileInfo::OpenWrite()
extern "C" FileStream_t1_146 * FileInfo_OpenWrite_m1_4914 (FileInfo_t1_422 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.FileStream System.IO.FileInfo::Open(System.IO.FileMode)
extern "C" FileStream_t1_146 * FileInfo_Open_m1_4915 (FileInfo_t1_422 * __this, int32_t ___mode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.FileStream System.IO.FileInfo::Open(System.IO.FileMode,System.IO.FileAccess)
extern "C" FileStream_t1_146 * FileInfo_Open_m1_4916 (FileInfo_t1_422 * __this, int32_t ___mode, int32_t ___access, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.FileStream System.IO.FileInfo::Open(System.IO.FileMode,System.IO.FileAccess,System.IO.FileShare)
extern "C" FileStream_t1_146 * FileInfo_Open_m1_4917 (FileInfo_t1_422 * __this, int32_t ___mode, int32_t ___access, int32_t ___share, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.FileInfo::Delete()
extern "C" void FileInfo_Delete_m1_4918 (FileInfo_t1_422 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.FileInfo::MoveTo(System.String)
extern "C" void FileInfo_MoveTo_m1_4919 (FileInfo_t1_422 * __this, String_t* ___destFileName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.FileInfo System.IO.FileInfo::CopyTo(System.String)
extern "C" FileInfo_t1_422 * FileInfo_CopyTo_m1_4920 (FileInfo_t1_422 * __this, String_t* ___destFileName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.FileInfo System.IO.FileInfo::CopyTo(System.String,System.Boolean)
extern "C" FileInfo_t1_422 * FileInfo_CopyTo_m1_4921 (FileInfo_t1_422 * __this, String_t* ___destFileName, bool ___overwrite, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.IO.FileInfo::ToString()
extern "C" String_t* FileInfo_ToString_m1_4922 (FileInfo_t1_422 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.FileInfo System.IO.FileInfo::Replace(System.String,System.String)
extern "C" FileInfo_t1_422 * FileInfo_Replace_m1_4923 (FileInfo_t1_422 * __this, String_t* ___destinationFileName, String_t* ___destinationBackupFileName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.FileInfo System.IO.FileInfo::Replace(System.String,System.String,System.Boolean)
extern "C" FileInfo_t1_422 * FileInfo_Replace_m1_4924 (FileInfo_t1_422 * __this, String_t* ___destinationFileName, String_t* ___destinationBackupFileName, bool ___ignoreMetadataErrors, const MethodInfo* method) IL2CPP_METHOD_ATTR;
