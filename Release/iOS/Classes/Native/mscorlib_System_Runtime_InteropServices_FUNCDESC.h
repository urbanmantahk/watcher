﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_Runtime_InteropServices_FUNCKIND.h"
#include "mscorlib_System_Runtime_InteropServices_INVOKEKIND.h"
#include "mscorlib_System_Runtime_InteropServices_CALLCONV.h"
#include "mscorlib_System_Runtime_InteropServices_ELEMDESC.h"

// System.Runtime.InteropServices.FUNCDESC
struct  FUNCDESC_t1_792 
{
	// System.Int32 System.Runtime.InteropServices.FUNCDESC::memid
	int32_t ___memid_0;
	// System.IntPtr System.Runtime.InteropServices.FUNCDESC::lprgscode
	IntPtr_t ___lprgscode_1;
	// System.IntPtr System.Runtime.InteropServices.FUNCDESC::lprgelemdescParam
	IntPtr_t ___lprgelemdescParam_2;
	// System.Runtime.InteropServices.FUNCKIND System.Runtime.InteropServices.FUNCDESC::funckind
	int32_t ___funckind_3;
	// System.Runtime.InteropServices.INVOKEKIND System.Runtime.InteropServices.FUNCDESC::invkind
	int32_t ___invkind_4;
	// System.Runtime.InteropServices.CALLCONV System.Runtime.InteropServices.FUNCDESC::callconv
	int32_t ___callconv_5;
	// System.Int16 System.Runtime.InteropServices.FUNCDESC::cParams
	int16_t ___cParams_6;
	// System.Int16 System.Runtime.InteropServices.FUNCDESC::cParamsOpt
	int16_t ___cParamsOpt_7;
	// System.Int16 System.Runtime.InteropServices.FUNCDESC::oVft
	int16_t ___oVft_8;
	// System.Int16 System.Runtime.InteropServices.FUNCDESC::cScodes
	int16_t ___cScodes_9;
	// System.Runtime.InteropServices.ELEMDESC System.Runtime.InteropServices.FUNCDESC::elemdescFunc
	ELEMDESC_t1_785  ___elemdescFunc_10;
	// System.Int16 System.Runtime.InteropServices.FUNCDESC::wFuncFlags
	int16_t ___wFuncFlags_11;
};
// Native definition for marshalling of: System.Runtime.InteropServices.FUNCDESC
struct FUNCDESC_t1_792_marshaled
{
	int32_t ___memid_0;
	intptr_t ___lprgscode_1;
	intptr_t ___lprgelemdescParam_2;
	int32_t ___funckind_3;
	int32_t ___invkind_4;
	int32_t ___callconv_5;
	int16_t ___cParams_6;
	int16_t ___cParamsOpt_7;
	int16_t ___oVft_8;
	int16_t ___cScodes_9;
	ELEMDESC_t1_785_marshaled ___elemdescFunc_10;
	int16_t ___wFuncFlags_11;
};
