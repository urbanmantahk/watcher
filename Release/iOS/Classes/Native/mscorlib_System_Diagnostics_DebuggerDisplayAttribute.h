﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Type
struct Type_t;

#include "mscorlib_System_Attribute.h"

// System.Diagnostics.DebuggerDisplayAttribute
struct  DebuggerDisplayAttribute_t1_328  : public Attribute_t1_2
{
	// System.String System.Diagnostics.DebuggerDisplayAttribute::value
	String_t* ___value_0;
	// System.String System.Diagnostics.DebuggerDisplayAttribute::type
	String_t* ___type_1;
	// System.String System.Diagnostics.DebuggerDisplayAttribute::name
	String_t* ___name_2;
	// System.String System.Diagnostics.DebuggerDisplayAttribute::target_type_name
	String_t* ___target_type_name_3;
	// System.Type System.Diagnostics.DebuggerDisplayAttribute::target_type
	Type_t * ___target_type_4;
};
