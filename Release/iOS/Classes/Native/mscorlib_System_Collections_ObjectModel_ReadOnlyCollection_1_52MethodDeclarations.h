﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCollection_1_4MethodDeclarations.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderProperty>::.ctor(System.Collections.Generic.IList`1<T>)
#define ReadOnlyCollection_1__ctor_m1_26135(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t1_2645 *, Object_t*, const MethodInfo*))ReadOnlyCollection_1__ctor_m1_15265_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderProperty>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1_26136(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t1_2645 *, ShaderProperty_t8_27 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1_15266_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderProperty>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1_26137(__this, method) (( void (*) (ReadOnlyCollection_1_t1_2645 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1_15267_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderProperty>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1_26138(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t1_2645 *, int32_t, ShaderProperty_t8_27 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1_15268_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderProperty>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1_26139(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t1_2645 *, ShaderProperty_t8_27 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1_15269_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderProperty>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1_26140(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t1_2645 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1_15270_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderProperty>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1_26141(__this, ___index, method) (( ShaderProperty_t8_27 * (*) (ReadOnlyCollection_1_t1_2645 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1_15271_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderProperty>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1_26142(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t1_2645 *, int32_t, ShaderProperty_t8_27 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1_15272_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderProperty>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_26143(__this, method) (( bool (*) (ReadOnlyCollection_1_t1_2645 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_15273_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderProperty>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1_26144(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t1_2645 *, Array_t *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1_15274_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderProperty>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1_26145(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t1_2645 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1_15275_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderProperty>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m1_26146(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t1_2645 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m1_15276_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderProperty>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m1_26147(__this, method) (( void (*) (ReadOnlyCollection_1_t1_2645 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m1_15277_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderProperty>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m1_26148(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t1_2645 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m1_15278_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderProperty>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1_26149(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t1_2645 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1_15279_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderProperty>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m1_26150(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t1_2645 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m1_15280_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderProperty>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m1_26151(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t1_2645 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m1_15281_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderProperty>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1_26152(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t1_2645 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1_15282_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderProperty>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1_26153(__this, method) (( bool (*) (ReadOnlyCollection_1_t1_2645 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1_15283_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderProperty>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1_26154(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t1_2645 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1_15284_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderProperty>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1_26155(__this, method) (( bool (*) (ReadOnlyCollection_1_t1_2645 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1_15285_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderProperty>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1_26156(__this, method) (( bool (*) (ReadOnlyCollection_1_t1_2645 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1_15286_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderProperty>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m1_26157(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t1_2645 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m1_15287_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderProperty>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m1_26158(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t1_2645 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m1_15288_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderProperty>::Contains(T)
#define ReadOnlyCollection_1_Contains_m1_26159(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t1_2645 *, ShaderProperty_t8_27 *, const MethodInfo*))ReadOnlyCollection_1_Contains_m1_15289_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderProperty>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m1_26160(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t1_2645 *, ShaderPropertyU5BU5D_t8_377*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m1_15290_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderProperty>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m1_26161(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t1_2645 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m1_15291_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderProperty>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m1_26162(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t1_2645 *, ShaderProperty_t8_27 *, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m1_15292_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderProperty>::get_Count()
#define ReadOnlyCollection_1_get_Count_m1_26163(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t1_2645 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m1_15293_gshared)(__this, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderProperty>::get_Items()
#define ReadOnlyCollection_1_get_Items_m1_26164(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t1_2645 *, const MethodInfo*))ReadOnlyCollection_1_get_Items_m1_15294_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.Utility.ShaderUtility/ShaderProperty>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m1_26165(__this, ___index, method) (( ShaderProperty_t8_27 * (*) (ReadOnlyCollection_1_t1_2645 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m1_15295_gshared)(__this, ___index, method)
