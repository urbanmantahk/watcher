﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VoxelBusters.NativePlugins.TwitterSettings
struct TwitterSettings_t8_292;

#include "mscorlib_System_Object.h"

// VoxelBusters.NativePlugins.SocialNetworkSettings
struct  SocialNetworkSettings_t8_291  : public Object_t
{
	// VoxelBusters.NativePlugins.TwitterSettings VoxelBusters.NativePlugins.SocialNetworkSettings::m_twitterSettings
	TwitterSettings_t8_292 * ___m_twitterSettings_0;
};
