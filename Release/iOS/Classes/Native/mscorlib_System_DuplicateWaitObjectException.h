﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ArgumentException.h"

// System.DuplicateWaitObjectException
struct  DuplicateWaitObjectException_t1_1536  : public ArgumentException_t1_1425
{
};
