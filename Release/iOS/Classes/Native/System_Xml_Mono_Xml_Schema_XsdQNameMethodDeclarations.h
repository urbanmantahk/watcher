﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Xml.Schema.XsdQName
struct XsdQName_t4_39;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlTokenizedType.h"

// System.Void Mono.Xml.Schema.XsdQName::.ctor()
extern "C" void XsdQName__ctor_m4_56 (XsdQName_t4_39 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdQName::get_TokenizedType()
extern "C" int32_t XsdQName_get_TokenizedType_m4_57 (XsdQName_t4_39 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
