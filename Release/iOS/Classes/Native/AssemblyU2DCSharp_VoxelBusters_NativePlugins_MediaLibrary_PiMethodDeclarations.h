﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.MediaLibrary/PickImageCompletion
struct PickImageCompletion_t8_240;
// System.Object
struct Object_t;
// UnityEngine.Texture2D
struct Texture2D_t6_33;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_ePickImageFinis.h"

// System.Void VoxelBusters.NativePlugins.MediaLibrary/PickImageCompletion::.ctor(System.Object,System.IntPtr)
extern "C" void PickImageCompletion__ctor_m8_1347 (PickImageCompletion_t8_240 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.MediaLibrary/PickImageCompletion::Invoke(VoxelBusters.NativePlugins.ePickImageFinishReason,UnityEngine.Texture2D)
extern "C" void PickImageCompletion_Invoke_m8_1348 (PickImageCompletion_t8_240 * __this, int32_t ____reason, Texture2D_t6_33 * ____image, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_PickImageCompletion_t8_240(Il2CppObject* delegate, int32_t ____reason, Texture2D_t6_33 * ____image);
// System.IAsyncResult VoxelBusters.NativePlugins.MediaLibrary/PickImageCompletion::BeginInvoke(VoxelBusters.NativePlugins.ePickImageFinishReason,UnityEngine.Texture2D,System.AsyncCallback,System.Object)
extern "C" Object_t * PickImageCompletion_BeginInvoke_m8_1349 (PickImageCompletion_t8_240 * __this, int32_t ____reason, Texture2D_t6_33 * ____image, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.MediaLibrary/PickImageCompletion::EndInvoke(System.IAsyncResult)
extern "C" void PickImageCompletion_EndInvoke_m8_1350 (PickImageCompletion_t8_240 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
