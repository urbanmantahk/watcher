﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_ArgumentException.h"

// System.Text.EncoderFallbackException
struct  EncoderFallbackException_t1_1432  : public ArgumentException_t1_1425
{
	// System.Char System.Text.EncoderFallbackException::char_unknown
	uint16_t ___char_unknown_15;
	// System.Char System.Text.EncoderFallbackException::char_unknown_high
	uint16_t ___char_unknown_high_16;
	// System.Char System.Text.EncoderFallbackException::char_unknown_low
	uint16_t ___char_unknown_low_17;
	// System.Int32 System.Text.EncoderFallbackException::index
	int32_t ___index_18;
};
