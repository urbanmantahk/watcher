﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Hashtable
struct Hashtable_t1_100;

#include "mscorlib_System_Object.h"

// System.Collections.DictionaryBase
struct  DictionaryBase_t1_283  : public Object_t
{
	// System.Collections.Hashtable System.Collections.DictionaryBase::hashtable
	Hashtable_t1_100 * ___hashtable_0;
};
