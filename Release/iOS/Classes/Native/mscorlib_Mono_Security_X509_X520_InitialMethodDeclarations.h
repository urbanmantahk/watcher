﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.X509.X520/Initial
struct Initial_t1_217;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Security.X509.X520/Initial::.ctor()
extern "C" void Initial__ctor_m1_2417 (Initial_t1_217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
