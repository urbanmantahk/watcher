﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_ArgIterator.h"
#include "mscorlib_System_RuntimeArgumentHandle.h"
#include "mscorlib_System_Void.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_TypedReference.h"
#include "mscorlib_System_RuntimeTypeHandle.h"

// System.Void System.ArgIterator::.ctor(System.RuntimeArgumentHandle)
extern "C" void ArgIterator__ctor_m1_1357 (ArgIterator_t1_68 * __this, RuntimeArgumentHandle_t1_66  ___arglist, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgIterator::.ctor(System.RuntimeArgumentHandle,System.Void*)
extern "C" void ArgIterator__ctor_m1_1358 (ArgIterator_t1_68 * __this, RuntimeArgumentHandle_t1_66  ___arglist, void* ___ptr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgIterator::Setup(System.IntPtr,System.IntPtr)
extern "C" void ArgIterator_Setup_m1_1359 (ArgIterator_t1_68 * __this, IntPtr_t ___argsp, IntPtr_t ___start, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgIterator::End()
extern "C" void ArgIterator_End_m1_1360 (ArgIterator_t1_68 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ArgIterator::Equals(System.Object)
extern "C" bool ArgIterator_Equals_m1_1361 (ArgIterator_t1_68 * __this, Object_t * ___o, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.ArgIterator::GetHashCode()
extern "C" int32_t ArgIterator_GetHashCode_m1_1362 (ArgIterator_t1_68 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.TypedReference System.ArgIterator::GetNextArg()
extern "C" TypedReference_t1_67  ArgIterator_GetNextArg_m1_1363 (ArgIterator_t1_68 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.TypedReference System.ArgIterator::IntGetNextArg()
extern "C" TypedReference_t1_67  ArgIterator_IntGetNextArg_m1_1364 (ArgIterator_t1_68 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.TypedReference System.ArgIterator::GetNextArg(System.RuntimeTypeHandle)
extern "C" TypedReference_t1_67  ArgIterator_GetNextArg_m1_1365 (ArgIterator_t1_68 * __this, RuntimeTypeHandle_t1_30  ___rth, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.TypedReference System.ArgIterator::IntGetNextArg(System.IntPtr)
extern "C" TypedReference_t1_67  ArgIterator_IntGetNextArg_m1_1366 (ArgIterator_t1_68 * __this, IntPtr_t ___rth, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.RuntimeTypeHandle System.ArgIterator::GetNextArgType()
extern "C" RuntimeTypeHandle_t1_30  ArgIterator_GetNextArgType_m1_1367 (ArgIterator_t1_68 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.ArgIterator::IntGetNextArgType()
extern "C" IntPtr_t ArgIterator_IntGetNextArgType_m1_1368 (ArgIterator_t1_68 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.ArgIterator::GetRemainingCount()
extern "C" int32_t ArgIterator_GetRemainingCount_m1_1369 (ArgIterator_t1_68 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
