﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapYearMonth
struct SoapYearMonth_t1_996;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime.h"

// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapYearMonth::.ctor()
extern "C" void SoapYearMonth__ctor_m1_8923 (SoapYearMonth_t1_996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapYearMonth::.ctor(System.DateTime)
extern "C" void SoapYearMonth__ctor_m1_8924 (SoapYearMonth_t1_996 * __this, DateTime_t1_150  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapYearMonth::.ctor(System.DateTime,System.Int32)
extern "C" void SoapYearMonth__ctor_m1_8925 (SoapYearMonth_t1_996 * __this, DateTime_t1_150  ___value, int32_t ___sign, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapYearMonth::.cctor()
extern "C" void SoapYearMonth__cctor_m1_8926 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.Remoting.Metadata.W3cXsd2001.SoapYearMonth::get_Sign()
extern "C" int32_t SoapYearMonth_get_Sign_m1_8927 (SoapYearMonth_t1_996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapYearMonth::set_Sign(System.Int32)
extern "C" void SoapYearMonth_set_Sign_m1_8928 (SoapYearMonth_t1_996 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Runtime.Remoting.Metadata.W3cXsd2001.SoapYearMonth::get_Value()
extern "C" DateTime_t1_150  SoapYearMonth_get_Value_m1_8929 (SoapYearMonth_t1_996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapYearMonth::set_Value(System.DateTime)
extern "C" void SoapYearMonth_set_Value_m1_8930 (SoapYearMonth_t1_996 * __this, DateTime_t1_150  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapYearMonth::get_XsdType()
extern "C" String_t* SoapYearMonth_get_XsdType_m1_8931 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapYearMonth::GetXsdType()
extern "C" String_t* SoapYearMonth_GetXsdType_m1_8932 (SoapYearMonth_t1_996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapYearMonth System.Runtime.Remoting.Metadata.W3cXsd2001.SoapYearMonth::Parse(System.String)
extern "C" SoapYearMonth_t1_996 * SoapYearMonth_Parse_m1_8933 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapYearMonth::ToString()
extern "C" String_t* SoapYearMonth_ToString_m1_8934 (SoapYearMonth_t1_996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
