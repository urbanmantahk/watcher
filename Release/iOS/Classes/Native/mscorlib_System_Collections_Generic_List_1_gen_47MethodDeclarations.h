﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>
struct List_1_t1_1908;
// System.Collections.Generic.IEnumerable`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>
struct IEnumerable_1_t1_2894;
// VoxelBusters.DebugPRO.Internal.ConsoleLog[]
struct ConsoleLogU5BU5D_t8_382;
// System.Collections.Generic.IEnumerator`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>
struct IEnumerator_1_t1_2895;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>
struct ICollection_1_t1_2896;
// System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>
struct ReadOnlyCollection_1_t1_2722;
// System.Collections.Generic.IComparer`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>
struct IComparer_1_t1_2897;
// System.Predicate`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>
struct Predicate_1_t1_2729;
// System.Action`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>
struct Action_1_t1_2730;
// System.Comparison`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>
struct Comparison_1_t1_2731;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VoxelBusters_DebugPRO_Internal_ConsoleLog.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_51.h"

// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::.ctor()
extern "C" void List_1__ctor_m1_15060_gshared (List_1_t1_1908 * __this, const MethodInfo* method);
#define List_1__ctor_m1_15060(__this, method) (( void (*) (List_1_t1_1908 *, const MethodInfo*))List_1__ctor_m1_15060_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m1_27109_gshared (List_1_t1_1908 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m1_27109(__this, ___collection, method) (( void (*) (List_1_t1_1908 *, Object_t*, const MethodInfo*))List_1__ctor_m1_27109_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::.ctor(System.Int32)
extern "C" void List_1__ctor_m1_27110_gshared (List_1_t1_1908 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m1_27110(__this, ___capacity, method) (( void (*) (List_1_t1_1908 *, int32_t, const MethodInfo*))List_1__ctor_m1_27110_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::.ctor(T[],System.Int32)
extern "C" void List_1__ctor_m1_27111_gshared (List_1_t1_1908 * __this, ConsoleLogU5BU5D_t8_382* ___data, int32_t ___size, const MethodInfo* method);
#define List_1__ctor_m1_27111(__this, ___data, ___size, method) (( void (*) (List_1_t1_1908 *, ConsoleLogU5BU5D_t8_382*, int32_t, const MethodInfo*))List_1__ctor_m1_27111_gshared)(__this, ___data, ___size, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::.cctor()
extern "C" void List_1__cctor_m1_27112_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m1_27112(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m1_27112_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_27113_gshared (List_1_t1_1908 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_27113(__this, method) (( Object_t* (*) (List_1_t1_1908 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_27113_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m1_27114_gshared (List_1_t1_1908 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m1_27114(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_1908 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m1_27114_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m1_27115_gshared (List_1_t1_1908 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1_27115(__this, method) (( Object_t * (*) (List_1_t1_1908 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1_27115_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m1_27116_gshared (List_1_t1_1908 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m1_27116(__this, ___item, method) (( int32_t (*) (List_1_t1_1908 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m1_27116_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m1_27117_gshared (List_1_t1_1908 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m1_27117(__this, ___item, method) (( bool (*) (List_1_t1_1908 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1_27117_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m1_27118_gshared (List_1_t1_1908 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m1_27118(__this, ___item, method) (( int32_t (*) (List_1_t1_1908 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m1_27118_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m1_27119_gshared (List_1_t1_1908 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m1_27119(__this, ___index, ___item, method) (( void (*) (List_1_t1_1908 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m1_27119_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m1_27120_gshared (List_1_t1_1908 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m1_27120(__this, ___item, method) (( void (*) (List_1_t1_1908 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1_27120_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_27121_gshared (List_1_t1_1908 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_27121(__this, method) (( bool (*) (List_1_t1_1908 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_27121_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m1_27122_gshared (List_1_t1_1908 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m1_27122(__this, method) (( bool (*) (List_1_t1_1908 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m1_27122_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m1_27123_gshared (List_1_t1_1908 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m1_27123(__this, method) (( Object_t * (*) (List_1_t1_1908 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1_27123_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m1_27124_gshared (List_1_t1_1908 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m1_27124(__this, method) (( bool (*) (List_1_t1_1908 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m1_27124_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m1_27125_gshared (List_1_t1_1908 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m1_27125(__this, method) (( bool (*) (List_1_t1_1908 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m1_27125_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m1_27126_gshared (List_1_t1_1908 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m1_27126(__this, ___index, method) (( Object_t * (*) (List_1_t1_1908 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m1_27126_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m1_27127_gshared (List_1_t1_1908 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m1_27127(__this, ___index, ___value, method) (( void (*) (List_1_t1_1908 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m1_27127_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Add(T)
extern "C" void List_1_Add_m1_27128_gshared (List_1_t1_1908 * __this, ConsoleLog_t8_170  ___item, const MethodInfo* method);
#define List_1_Add_m1_27128(__this, ___item, method) (( void (*) (List_1_t1_1908 *, ConsoleLog_t8_170 , const MethodInfo*))List_1_Add_m1_27128_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m1_27129_gshared (List_1_t1_1908 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m1_27129(__this, ___newCount, method) (( void (*) (List_1_t1_1908 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m1_27129_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::CheckRange(System.Int32,System.Int32)
extern "C" void List_1_CheckRange_m1_27130_gshared (List_1_t1_1908 * __this, int32_t ___idx, int32_t ___count, const MethodInfo* method);
#define List_1_CheckRange_m1_27130(__this, ___idx, ___count, method) (( void (*) (List_1_t1_1908 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m1_27130_gshared)(__this, ___idx, ___count, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m1_27131_gshared (List_1_t1_1908 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m1_27131(__this, ___collection, method) (( void (*) (List_1_t1_1908 *, Object_t*, const MethodInfo*))List_1_AddCollection_m1_27131_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m1_27132_gshared (List_1_t1_1908 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m1_27132(__this, ___enumerable, method) (( void (*) (List_1_t1_1908 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m1_27132_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m1_27133_gshared (List_1_t1_1908 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m1_27133(__this, ___collection, method) (( void (*) (List_1_t1_1908 *, Object_t*, const MethodInfo*))List_1_AddRange_m1_27133_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t1_2722 * List_1_AsReadOnly_m1_27134_gshared (List_1_t1_1908 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m1_27134(__this, method) (( ReadOnlyCollection_1_t1_2722 * (*) (List_1_t1_1908 *, const MethodInfo*))List_1_AsReadOnly_m1_27134_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::BinarySearch(T)
extern "C" int32_t List_1_BinarySearch_m1_27135_gshared (List_1_t1_1908 * __this, ConsoleLog_t8_170  ___item, const MethodInfo* method);
#define List_1_BinarySearch_m1_27135(__this, ___item, method) (( int32_t (*) (List_1_t1_1908 *, ConsoleLog_t8_170 , const MethodInfo*))List_1_BinarySearch_m1_27135_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::BinarySearch(T,System.Collections.Generic.IComparer`1<T>)
extern "C" int32_t List_1_BinarySearch_m1_27136_gshared (List_1_t1_1908 * __this, ConsoleLog_t8_170  ___item, Object_t* ___comparer, const MethodInfo* method);
#define List_1_BinarySearch_m1_27136(__this, ___item, ___comparer, method) (( int32_t (*) (List_1_t1_1908 *, ConsoleLog_t8_170 , Object_t*, const MethodInfo*))List_1_BinarySearch_m1_27136_gshared)(__this, ___item, ___comparer, method)
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::BinarySearch(System.Int32,System.Int32,T,System.Collections.Generic.IComparer`1<T>)
extern "C" int32_t List_1_BinarySearch_m1_27137_gshared (List_1_t1_1908 * __this, int32_t ___index, int32_t ___count, ConsoleLog_t8_170  ___item, Object_t* ___comparer, const MethodInfo* method);
#define List_1_BinarySearch_m1_27137(__this, ___index, ___count, ___item, ___comparer, method) (( int32_t (*) (List_1_t1_1908 *, int32_t, int32_t, ConsoleLog_t8_170 , Object_t*, const MethodInfo*))List_1_BinarySearch_m1_27137_gshared)(__this, ___index, ___count, ___item, ___comparer, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Clear()
extern "C" void List_1_Clear_m1_27138_gshared (List_1_t1_1908 * __this, const MethodInfo* method);
#define List_1_Clear_m1_27138(__this, method) (( void (*) (List_1_t1_1908 *, const MethodInfo*))List_1_Clear_m1_27138_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Contains(T)
extern "C" bool List_1_Contains_m1_27139_gshared (List_1_t1_1908 * __this, ConsoleLog_t8_170  ___item, const MethodInfo* method);
#define List_1_Contains_m1_27139(__this, ___item, method) (( bool (*) (List_1_t1_1908 *, ConsoleLog_t8_170 , const MethodInfo*))List_1_Contains_m1_27139_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::CopyTo(T[])
extern "C" void List_1_CopyTo_m1_27140_gshared (List_1_t1_1908 * __this, ConsoleLogU5BU5D_t8_382* ___array, const MethodInfo* method);
#define List_1_CopyTo_m1_27140(__this, ___array, method) (( void (*) (List_1_t1_1908 *, ConsoleLogU5BU5D_t8_382*, const MethodInfo*))List_1_CopyTo_m1_27140_gshared)(__this, ___array, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m1_27141_gshared (List_1_t1_1908 * __this, ConsoleLogU5BU5D_t8_382* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m1_27141(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_1908 *, ConsoleLogU5BU5D_t8_382*, int32_t, const MethodInfo*))List_1_CopyTo_m1_27141_gshared)(__this, ___array, ___arrayIndex, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::CopyTo(System.Int32,T[],System.Int32,System.Int32)
extern "C" void List_1_CopyTo_m1_27142_gshared (List_1_t1_1908 * __this, int32_t ___index, ConsoleLogU5BU5D_t8_382* ___array, int32_t ___arrayIndex, int32_t ___count, const MethodInfo* method);
#define List_1_CopyTo_m1_27142(__this, ___index, ___array, ___arrayIndex, ___count, method) (( void (*) (List_1_t1_1908 *, int32_t, ConsoleLogU5BU5D_t8_382*, int32_t, int32_t, const MethodInfo*))List_1_CopyTo_m1_27142_gshared)(__this, ___index, ___array, ___arrayIndex, ___count, method)
// System.Boolean System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Exists(System.Predicate`1<T>)
extern "C" bool List_1_Exists_m1_27143_gshared (List_1_t1_1908 * __this, Predicate_1_t1_2729 * ___match, const MethodInfo* method);
#define List_1_Exists_m1_27143(__this, ___match, method) (( bool (*) (List_1_t1_1908 *, Predicate_1_t1_2729 *, const MethodInfo*))List_1_Exists_m1_27143_gshared)(__this, ___match, method)
// T System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Find(System.Predicate`1<T>)
extern "C" ConsoleLog_t8_170  List_1_Find_m1_27144_gshared (List_1_t1_1908 * __this, Predicate_1_t1_2729 * ___match, const MethodInfo* method);
#define List_1_Find_m1_27144(__this, ___match, method) (( ConsoleLog_t8_170  (*) (List_1_t1_1908 *, Predicate_1_t1_2729 *, const MethodInfo*))List_1_Find_m1_27144_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m1_27145_gshared (Object_t * __this /* static, unused */, Predicate_1_t1_2729 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m1_27145(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t1_2729 *, const MethodInfo*))List_1_CheckMatch_m1_27145_gshared)(__this /* static, unused */, ___match, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::FindAll(System.Predicate`1<T>)
extern "C" List_1_t1_1908 * List_1_FindAll_m1_27146_gshared (List_1_t1_1908 * __this, Predicate_1_t1_2729 * ___match, const MethodInfo* method);
#define List_1_FindAll_m1_27146(__this, ___match, method) (( List_1_t1_1908 * (*) (List_1_t1_1908 *, Predicate_1_t1_2729 *, const MethodInfo*))List_1_FindAll_m1_27146_gshared)(__this, ___match, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::FindAllStackBits(System.Predicate`1<T>)
extern "C" List_1_t1_1908 * List_1_FindAllStackBits_m1_27147_gshared (List_1_t1_1908 * __this, Predicate_1_t1_2729 * ___match, const MethodInfo* method);
#define List_1_FindAllStackBits_m1_27147(__this, ___match, method) (( List_1_t1_1908 * (*) (List_1_t1_1908 *, Predicate_1_t1_2729 *, const MethodInfo*))List_1_FindAllStackBits_m1_27147_gshared)(__this, ___match, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::FindAllList(System.Predicate`1<T>)
extern "C" List_1_t1_1908 * List_1_FindAllList_m1_27148_gshared (List_1_t1_1908 * __this, Predicate_1_t1_2729 * ___match, const MethodInfo* method);
#define List_1_FindAllList_m1_27148(__this, ___match, method) (( List_1_t1_1908 * (*) (List_1_t1_1908 *, Predicate_1_t1_2729 *, const MethodInfo*))List_1_FindAllList_m1_27148_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::FindIndex(System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m1_27149_gshared (List_1_t1_1908 * __this, Predicate_1_t1_2729 * ___match, const MethodInfo* method);
#define List_1_FindIndex_m1_27149(__this, ___match, method) (( int32_t (*) (List_1_t1_1908 *, Predicate_1_t1_2729 *, const MethodInfo*))List_1_FindIndex_m1_27149_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::FindIndex(System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m1_27150_gshared (List_1_t1_1908 * __this, int32_t ___startIndex, Predicate_1_t1_2729 * ___match, const MethodInfo* method);
#define List_1_FindIndex_m1_27150(__this, ___startIndex, ___match, method) (( int32_t (*) (List_1_t1_1908 *, int32_t, Predicate_1_t1_2729 *, const MethodInfo*))List_1_FindIndex_m1_27150_gshared)(__this, ___startIndex, ___match, method)
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::FindIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m1_27151_gshared (List_1_t1_1908 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_2729 * ___match, const MethodInfo* method);
#define List_1_FindIndex_m1_27151(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_1908 *, int32_t, int32_t, Predicate_1_t1_2729 *, const MethodInfo*))List_1_FindIndex_m1_27151_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m1_27152_gshared (List_1_t1_1908 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_2729 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m1_27152(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_1908 *, int32_t, int32_t, Predicate_1_t1_2729 *, const MethodInfo*))List_1_GetIndex_m1_27152_gshared)(__this, ___startIndex, ___count, ___match, method)
// T System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::FindLast(System.Predicate`1<T>)
extern "C" ConsoleLog_t8_170  List_1_FindLast_m1_27153_gshared (List_1_t1_1908 * __this, Predicate_1_t1_2729 * ___match, const MethodInfo* method);
#define List_1_FindLast_m1_27153(__this, ___match, method) (( ConsoleLog_t8_170  (*) (List_1_t1_1908 *, Predicate_1_t1_2729 *, const MethodInfo*))List_1_FindLast_m1_27153_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::FindLastIndex(System.Predicate`1<T>)
extern "C" int32_t List_1_FindLastIndex_m1_27154_gshared (List_1_t1_1908 * __this, Predicate_1_t1_2729 * ___match, const MethodInfo* method);
#define List_1_FindLastIndex_m1_27154(__this, ___match, method) (( int32_t (*) (List_1_t1_1908 *, Predicate_1_t1_2729 *, const MethodInfo*))List_1_FindLastIndex_m1_27154_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::FindLastIndex(System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindLastIndex_m1_27155_gshared (List_1_t1_1908 * __this, int32_t ___startIndex, Predicate_1_t1_2729 * ___match, const MethodInfo* method);
#define List_1_FindLastIndex_m1_27155(__this, ___startIndex, ___match, method) (( int32_t (*) (List_1_t1_1908 *, int32_t, Predicate_1_t1_2729 *, const MethodInfo*))List_1_FindLastIndex_m1_27155_gshared)(__this, ___startIndex, ___match, method)
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::FindLastIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindLastIndex_m1_27156_gshared (List_1_t1_1908 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_2729 * ___match, const MethodInfo* method);
#define List_1_FindLastIndex_m1_27156(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_1908 *, int32_t, int32_t, Predicate_1_t1_2729 *, const MethodInfo*))List_1_FindLastIndex_m1_27156_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::GetLastIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetLastIndex_m1_27157_gshared (List_1_t1_1908 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_2729 * ___match, const MethodInfo* method);
#define List_1_GetLastIndex_m1_27157(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_1908 *, int32_t, int32_t, Predicate_1_t1_2729 *, const MethodInfo*))List_1_GetLastIndex_m1_27157_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::ForEach(System.Action`1<T>)
extern "C" void List_1_ForEach_m1_27158_gshared (List_1_t1_1908 * __this, Action_1_t1_2730 * ___action, const MethodInfo* method);
#define List_1_ForEach_m1_27158(__this, ___action, method) (( void (*) (List_1_t1_1908 *, Action_1_t1_2730 *, const MethodInfo*))List_1_ForEach_m1_27158_gshared)(__this, ___action, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::GetEnumerator()
extern "C" Enumerator_t1_2721  List_1_GetEnumerator_m1_27159_gshared (List_1_t1_1908 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m1_27159(__this, method) (( Enumerator_t1_2721  (*) (List_1_t1_1908 *, const MethodInfo*))List_1_GetEnumerator_m1_27159_gshared)(__this, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::GetRange(System.Int32,System.Int32)
extern "C" List_1_t1_1908 * List_1_GetRange_m1_27160_gshared (List_1_t1_1908 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_GetRange_m1_27160(__this, ___index, ___count, method) (( List_1_t1_1908 * (*) (List_1_t1_1908 *, int32_t, int32_t, const MethodInfo*))List_1_GetRange_m1_27160_gshared)(__this, ___index, ___count, method)
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m1_27161_gshared (List_1_t1_1908 * __this, ConsoleLog_t8_170  ___item, const MethodInfo* method);
#define List_1_IndexOf_m1_27161(__this, ___item, method) (( int32_t (*) (List_1_t1_1908 *, ConsoleLog_t8_170 , const MethodInfo*))List_1_IndexOf_m1_27161_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::IndexOf(T,System.Int32)
extern "C" int32_t List_1_IndexOf_m1_27162_gshared (List_1_t1_1908 * __this, ConsoleLog_t8_170  ___item, int32_t ___index, const MethodInfo* method);
#define List_1_IndexOf_m1_27162(__this, ___item, ___index, method) (( int32_t (*) (List_1_t1_1908 *, ConsoleLog_t8_170 , int32_t, const MethodInfo*))List_1_IndexOf_m1_27162_gshared)(__this, ___item, ___index, method)
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::IndexOf(T,System.Int32,System.Int32)
extern "C" int32_t List_1_IndexOf_m1_27163_gshared (List_1_t1_1908 * __this, ConsoleLog_t8_170  ___item, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_IndexOf_m1_27163(__this, ___item, ___index, ___count, method) (( int32_t (*) (List_1_t1_1908 *, ConsoleLog_t8_170 , int32_t, int32_t, const MethodInfo*))List_1_IndexOf_m1_27163_gshared)(__this, ___item, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m1_27164_gshared (List_1_t1_1908 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m1_27164(__this, ___start, ___delta, method) (( void (*) (List_1_t1_1908 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m1_27164_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m1_27165_gshared (List_1_t1_1908 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m1_27165(__this, ___index, method) (( void (*) (List_1_t1_1908 *, int32_t, const MethodInfo*))List_1_CheckIndex_m1_27165_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m1_27166_gshared (List_1_t1_1908 * __this, int32_t ___index, ConsoleLog_t8_170  ___item, const MethodInfo* method);
#define List_1_Insert_m1_27166(__this, ___index, ___item, method) (( void (*) (List_1_t1_1908 *, int32_t, ConsoleLog_t8_170 , const MethodInfo*))List_1_Insert_m1_27166_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m1_27167_gshared (List_1_t1_1908 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m1_27167(__this, ___collection, method) (( void (*) (List_1_t1_1908 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m1_27167_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::InsertRange(System.Int32,System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_InsertRange_m1_27168_gshared (List_1_t1_1908 * __this, int32_t ___index, Object_t* ___collection, const MethodInfo* method);
#define List_1_InsertRange_m1_27168(__this, ___index, ___collection, method) (( void (*) (List_1_t1_1908 *, int32_t, Object_t*, const MethodInfo*))List_1_InsertRange_m1_27168_gshared)(__this, ___index, ___collection, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::InsertCollection(System.Int32,System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_InsertCollection_m1_27169_gshared (List_1_t1_1908 * __this, int32_t ___index, Object_t* ___collection, const MethodInfo* method);
#define List_1_InsertCollection_m1_27169(__this, ___index, ___collection, method) (( void (*) (List_1_t1_1908 *, int32_t, Object_t*, const MethodInfo*))List_1_InsertCollection_m1_27169_gshared)(__this, ___index, ___collection, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::InsertEnumeration(System.Int32,System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_InsertEnumeration_m1_27170_gshared (List_1_t1_1908 * __this, int32_t ___index, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_InsertEnumeration_m1_27170(__this, ___index, ___enumerable, method) (( void (*) (List_1_t1_1908 *, int32_t, Object_t*, const MethodInfo*))List_1_InsertEnumeration_m1_27170_gshared)(__this, ___index, ___enumerable, method)
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::LastIndexOf(T)
extern "C" int32_t List_1_LastIndexOf_m1_27171_gshared (List_1_t1_1908 * __this, ConsoleLog_t8_170  ___item, const MethodInfo* method);
#define List_1_LastIndexOf_m1_27171(__this, ___item, method) (( int32_t (*) (List_1_t1_1908 *, ConsoleLog_t8_170 , const MethodInfo*))List_1_LastIndexOf_m1_27171_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::LastIndexOf(T,System.Int32)
extern "C" int32_t List_1_LastIndexOf_m1_27172_gshared (List_1_t1_1908 * __this, ConsoleLog_t8_170  ___item, int32_t ___index, const MethodInfo* method);
#define List_1_LastIndexOf_m1_27172(__this, ___item, ___index, method) (( int32_t (*) (List_1_t1_1908 *, ConsoleLog_t8_170 , int32_t, const MethodInfo*))List_1_LastIndexOf_m1_27172_gshared)(__this, ___item, ___index, method)
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::LastIndexOf(T,System.Int32,System.Int32)
extern "C" int32_t List_1_LastIndexOf_m1_27173_gshared (List_1_t1_1908 * __this, ConsoleLog_t8_170  ___item, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_LastIndexOf_m1_27173(__this, ___item, ___index, ___count, method) (( int32_t (*) (List_1_t1_1908 *, ConsoleLog_t8_170 , int32_t, int32_t, const MethodInfo*))List_1_LastIndexOf_m1_27173_gshared)(__this, ___item, ___index, ___count, method)
// System.Boolean System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Remove(T)
extern "C" bool List_1_Remove_m1_27174_gshared (List_1_t1_1908 * __this, ConsoleLog_t8_170  ___item, const MethodInfo* method);
#define List_1_Remove_m1_27174(__this, ___item, method) (( bool (*) (List_1_t1_1908 *, ConsoleLog_t8_170 , const MethodInfo*))List_1_Remove_m1_27174_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m1_27175_gshared (List_1_t1_1908 * __this, Predicate_1_t1_2729 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m1_27175(__this, ___match, method) (( int32_t (*) (List_1_t1_1908 *, Predicate_1_t1_2729 *, const MethodInfo*))List_1_RemoveAll_m1_27175_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m1_27176_gshared (List_1_t1_1908 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m1_27176(__this, ___index, method) (( void (*) (List_1_t1_1908 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1_27176_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::RemoveRange(System.Int32,System.Int32)
extern "C" void List_1_RemoveRange_m1_27177_gshared (List_1_t1_1908 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_RemoveRange_m1_27177(__this, ___index, ___count, method) (( void (*) (List_1_t1_1908 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m1_27177_gshared)(__this, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Reverse()
extern "C" void List_1_Reverse_m1_27178_gshared (List_1_t1_1908 * __this, const MethodInfo* method);
#define List_1_Reverse_m1_27178(__this, method) (( void (*) (List_1_t1_1908 *, const MethodInfo*))List_1_Reverse_m1_27178_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Reverse(System.Int32,System.Int32)
extern "C" void List_1_Reverse_m1_27179_gshared (List_1_t1_1908 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_Reverse_m1_27179(__this, ___index, ___count, method) (( void (*) (List_1_t1_1908 *, int32_t, int32_t, const MethodInfo*))List_1_Reverse_m1_27179_gshared)(__this, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Sort()
extern "C" void List_1_Sort_m1_27180_gshared (List_1_t1_1908 * __this, const MethodInfo* method);
#define List_1_Sort_m1_27180(__this, method) (( void (*) (List_1_t1_1908 *, const MethodInfo*))List_1_Sort_m1_27180_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C" void List_1_Sort_m1_27181_gshared (List_1_t1_1908 * __this, Object_t* ___comparer, const MethodInfo* method);
#define List_1_Sort_m1_27181(__this, ___comparer, method) (( void (*) (List_1_t1_1908 *, Object_t*, const MethodInfo*))List_1_Sort_m1_27181_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m1_27182_gshared (List_1_t1_1908 * __this, Comparison_1_t1_2731 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m1_27182(__this, ___comparison, method) (( void (*) (List_1_t1_1908 *, Comparison_1_t1_2731 *, const MethodInfo*))List_1_Sort_m1_27182_gshared)(__this, ___comparison, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Sort(System.Int32,System.Int32,System.Collections.Generic.IComparer`1<T>)
extern "C" void List_1_Sort_m1_27183_gshared (List_1_t1_1908 * __this, int32_t ___index, int32_t ___count, Object_t* ___comparer, const MethodInfo* method);
#define List_1_Sort_m1_27183(__this, ___index, ___count, ___comparer, method) (( void (*) (List_1_t1_1908 *, int32_t, int32_t, Object_t*, const MethodInfo*))List_1_Sort_m1_27183_gshared)(__this, ___index, ___count, ___comparer, method)
// T[] System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::ToArray()
extern "C" ConsoleLogU5BU5D_t8_382* List_1_ToArray_m1_27184_gshared (List_1_t1_1908 * __this, const MethodInfo* method);
#define List_1_ToArray_m1_27184(__this, method) (( ConsoleLogU5BU5D_t8_382* (*) (List_1_t1_1908 *, const MethodInfo*))List_1_ToArray_m1_27184_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::TrimExcess()
extern "C" void List_1_TrimExcess_m1_27185_gshared (List_1_t1_1908 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m1_27185(__this, method) (( void (*) (List_1_t1_1908 *, const MethodInfo*))List_1_TrimExcess_m1_27185_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::TrueForAll(System.Predicate`1<T>)
extern "C" bool List_1_TrueForAll_m1_27186_gshared (List_1_t1_1908 * __this, Predicate_1_t1_2729 * ___match, const MethodInfo* method);
#define List_1_TrueForAll_m1_27186(__this, ___match, method) (( bool (*) (List_1_t1_1908 *, Predicate_1_t1_2729 *, const MethodInfo*))List_1_TrueForAll_m1_27186_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m1_27187_gshared (List_1_t1_1908 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m1_27187(__this, method) (( int32_t (*) (List_1_t1_1908 *, const MethodInfo*))List_1_get_Capacity_m1_27187_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m1_27188_gshared (List_1_t1_1908 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m1_27188(__this, ___value, method) (( void (*) (List_1_t1_1908 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1_27188_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::get_Count()
extern "C" int32_t List_1_get_Count_m1_27189_gshared (List_1_t1_1908 * __this, const MethodInfo* method);
#define List_1_get_Count_m1_27189(__this, method) (( int32_t (*) (List_1_t1_1908 *, const MethodInfo*))List_1_get_Count_m1_27189_gshared)(__this, method)
// T System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::get_Item(System.Int32)
extern "C" ConsoleLog_t8_170  List_1_get_Item_m1_27190_gshared (List_1_t1_1908 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m1_27190(__this, ___index, method) (( ConsoleLog_t8_170  (*) (List_1_t1_1908 *, int32_t, const MethodInfo*))List_1_get_Item_m1_27190_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m1_27191_gshared (List_1_t1_1908 * __this, int32_t ___index, ConsoleLog_t8_170  ___value, const MethodInfo* method);
#define List_1_set_Item_m1_27191(__this, ___index, ___value, method) (( void (*) (List_1_t1_1908 *, int32_t, ConsoleLog_t8_170 , const MethodInfo*))List_1_set_Item_m1_27191_gshared)(__this, ___index, ___value, method)
