﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "System_Xml_Mono_Xml_Schema_XsdWhitespaceFacet.h"

// Mono.Xml.Schema.XsdWhitespaceFacet
struct  XsdWhitespaceFacet_t4_2 
{
	// System.Int32 Mono.Xml.Schema.XsdWhitespaceFacet::value__
	int32_t ___value___1;
};
