﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>
struct ReadOnlyCollection_1_t1_2469;
// System.Collections.Generic.IList`1<UnityEngine.EventSystems.RaycastResult>
struct IList_1_t1_2470;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Object
struct Object_t;
// UnityEngine.EventSystems.RaycastResult[]
struct RaycastResultU5BU5D_t7_205;
// System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.RaycastResult>
struct IEnumerator_1_t1_2841;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C" void ReadOnlyCollection_1__ctor_m1_22361_gshared (ReadOnlyCollection_1_t1_2469 * __this, Object_t* ___list, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m1_22361(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t1_2469 *, Object_t*, const MethodInfo*))ReadOnlyCollection_1__ctor_m1_22361_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1_22362_gshared (ReadOnlyCollection_1_t1_2469 * __this, RaycastResult_t7_31  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1_22362(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t1_2469 *, RaycastResult_t7_31 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1_22362_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.Generic.ICollection<T>.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1_22363_gshared (ReadOnlyCollection_1_t1_2469 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1_22363(__this, method) (( void (*) (ReadOnlyCollection_1_t1_2469 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1_22363_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1_22364_gshared (ReadOnlyCollection_1_t1_2469 * __this, int32_t ___index, RaycastResult_t7_31  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1_22364(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t1_2469 *, int32_t, RaycastResult_t7_31 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1_22364_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1_22365_gshared (ReadOnlyCollection_1_t1_2469 * __this, RaycastResult_t7_31  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1_22365(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t1_2469 *, RaycastResult_t7_31 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1_22365_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1_22366_gshared (ReadOnlyCollection_1_t1_2469 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1_22366(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t1_2469 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1_22366_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C" RaycastResult_t7_31  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1_22367_gshared (ReadOnlyCollection_1_t1_2469 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1_22367(__this, ___index, method) (( RaycastResult_t7_31  (*) (ReadOnlyCollection_1_t1_2469 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1_22367_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1_22368_gshared (ReadOnlyCollection_1_t1_2469 * __this, int32_t ___index, RaycastResult_t7_31  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1_22368(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t1_2469 *, int32_t, RaycastResult_t7_31 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1_22368_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_22369_gshared (ReadOnlyCollection_1_t1_2469 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_22369(__this, method) (( bool (*) (ReadOnlyCollection_1_t1_2469 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_22369_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1_22370_gshared (ReadOnlyCollection_1_t1_2469 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1_22370(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t1_2469 *, Array_t *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1_22370_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1_22371_gshared (ReadOnlyCollection_1_t1_2469 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1_22371(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t1_2469 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1_22371_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Add(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m1_22372_gshared (ReadOnlyCollection_1_t1_2469 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m1_22372(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t1_2469 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m1_22372_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m1_22373_gshared (ReadOnlyCollection_1_t1_2469 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m1_22373(__this, method) (( void (*) (ReadOnlyCollection_1_t1_2469 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m1_22373_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Contains(System.Object)
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_Contains_m1_22374_gshared (ReadOnlyCollection_1_t1_2469 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m1_22374(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t1_2469 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m1_22374_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1_22375_gshared (ReadOnlyCollection_1_t1_2469 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1_22375(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t1_2469 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1_22375_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m1_22376_gshared (ReadOnlyCollection_1_t1_2469 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m1_22376(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t1_2469 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m1_22376_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Remove(System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m1_22377_gshared (ReadOnlyCollection_1_t1_2469 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m1_22377(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t1_2469 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m1_22377_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1_22378_gshared (ReadOnlyCollection_1_t1_2469 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1_22378(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t1_2469 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1_22378_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1_22379_gshared (ReadOnlyCollection_1_t1_2469 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1_22379(__this, method) (( bool (*) (ReadOnlyCollection_1_t1_2469 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1_22379_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1_22380_gshared (ReadOnlyCollection_1_t1_2469 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1_22380(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t1_2469 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1_22380_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.get_IsFixedSize()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1_22381_gshared (ReadOnlyCollection_1_t1_2469 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1_22381(__this, method) (( bool (*) (ReadOnlyCollection_1_t1_2469 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1_22381_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1_22382_gshared (ReadOnlyCollection_1_t1_2469 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1_22382(__this, method) (( bool (*) (ReadOnlyCollection_1_t1_2469 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1_22382_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IList_get_Item_m1_22383_gshared (ReadOnlyCollection_1_t1_2469 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m1_22383(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t1_2469 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m1_22383_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m1_22384_gshared (ReadOnlyCollection_1_t1_2469 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m1_22384(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t1_2469 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m1_22384_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::Contains(T)
extern "C" bool ReadOnlyCollection_1_Contains_m1_22385_gshared (ReadOnlyCollection_1_t1_2469 * __this, RaycastResult_t7_31  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m1_22385(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t1_2469 *, RaycastResult_t7_31 , const MethodInfo*))ReadOnlyCollection_1_Contains_m1_22385_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::CopyTo(T[],System.Int32)
extern "C" void ReadOnlyCollection_1_CopyTo_m1_22386_gshared (ReadOnlyCollection_1_t1_2469 * __this, RaycastResultU5BU5D_t7_205* ___array, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m1_22386(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t1_2469 *, RaycastResultU5BU5D_t7_205*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m1_22386_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::GetEnumerator()
extern "C" Object_t* ReadOnlyCollection_1_GetEnumerator_m1_22387_gshared (ReadOnlyCollection_1_t1_2469 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m1_22387(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t1_2469 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m1_22387_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::IndexOf(T)
extern "C" int32_t ReadOnlyCollection_1_IndexOf_m1_22388_gshared (ReadOnlyCollection_1_t1_2469 * __this, RaycastResult_t7_31  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m1_22388(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t1_2469 *, RaycastResult_t7_31 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m1_22388_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::get_Count()
extern "C" int32_t ReadOnlyCollection_1_get_Count_m1_22389_gshared (ReadOnlyCollection_1_t1_2469 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m1_22389(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t1_2469 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m1_22389_gshared)(__this, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::get_Items()
extern "C" Object_t* ReadOnlyCollection_1_get_Items_m1_22390_gshared (ReadOnlyCollection_1_t1_2469 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Items_m1_22390(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t1_2469 *, const MethodInfo*))ReadOnlyCollection_1_get_Items_m1_22390_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::get_Item(System.Int32)
extern "C" RaycastResult_t7_31  ReadOnlyCollection_1_get_Item_m1_22391_gshared (ReadOnlyCollection_1_t1_2469 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m1_22391(__this, ___index, method) (( RaycastResult_t7_31  (*) (ReadOnlyCollection_1_t1_2469 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m1_22391_gshared)(__this, ___index, method)
