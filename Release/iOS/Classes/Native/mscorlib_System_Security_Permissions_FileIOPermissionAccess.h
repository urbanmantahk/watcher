﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Security_Permissions_FileIOPermissionAccess.h"

// System.Security.Permissions.FileIOPermissionAccess
struct  FileIOPermissionAccess_t1_1275 
{
	// System.Int32 System.Security.Permissions.FileIOPermissionAccess::value__
	int32_t ___value___1;
};
