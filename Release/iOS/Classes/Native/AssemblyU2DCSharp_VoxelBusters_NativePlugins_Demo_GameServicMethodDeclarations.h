﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.Demo.GameServicesDemo
struct GameServicesDemo_t8_179;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.Demo.GameServicesDemo::.ctor()
extern "C" void GameServicesDemo__ctor_m8_1058 (GameServicesDemo_t8_179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
