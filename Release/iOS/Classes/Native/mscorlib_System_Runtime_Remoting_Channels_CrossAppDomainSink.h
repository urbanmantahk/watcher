﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Runtime.Remoting.Messaging.CADMethodReturnMessage
struct CADMethodReturnMessage_t1_881;

#include "mscorlib_System_ValueType.h"

// System.Runtime.Remoting.Channels.CrossAppDomainSink/ProcessMessageRes
struct  ProcessMessageRes_t1_880 
{
	// System.Byte[] System.Runtime.Remoting.Channels.CrossAppDomainSink/ProcessMessageRes::arrResponse
	ByteU5BU5D_t1_109* ___arrResponse_0;
	// System.Runtime.Remoting.Messaging.CADMethodReturnMessage System.Runtime.Remoting.Channels.CrossAppDomainSink/ProcessMessageRes::cadMrm
	CADMethodReturnMessage_t1_881 * ___cadMrm_1;
};
