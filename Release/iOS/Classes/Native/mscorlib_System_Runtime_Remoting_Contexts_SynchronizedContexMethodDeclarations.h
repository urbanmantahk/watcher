﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Contexts.SynchronizedContextReplySink
struct SynchronizedContextReplySink_t1_904;
// System.Runtime.Remoting.Messaging.IMessageSink
struct IMessageSink_t1_874;
// System.Runtime.Remoting.Contexts.SynchronizationAttribute
struct SynchronizationAttribute_t1_900;
// System.Runtime.Remoting.Messaging.IMessageCtrl
struct IMessageCtrl_t1_920;
// System.Runtime.Remoting.Messaging.IMessage
struct IMessage_t1_875;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Contexts.SynchronizedContextReplySink::.ctor(System.Runtime.Remoting.Messaging.IMessageSink,System.Runtime.Remoting.Contexts.SynchronizationAttribute,System.Boolean)
extern "C" void SynchronizedContextReplySink__ctor_m1_8212 (SynchronizedContextReplySink_t1_904 * __this, Object_t * ___next, SynchronizationAttribute_t1_900 * ___att, bool ___newLock, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Messaging.IMessageSink System.Runtime.Remoting.Contexts.SynchronizedContextReplySink::get_NextSink()
extern "C" Object_t * SynchronizedContextReplySink_get_NextSink_m1_8213 (SynchronizedContextReplySink_t1_904 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Messaging.IMessageCtrl System.Runtime.Remoting.Contexts.SynchronizedContextReplySink::AsyncProcessMessage(System.Runtime.Remoting.Messaging.IMessage,System.Runtime.Remoting.Messaging.IMessageSink)
extern "C" Object_t * SynchronizedContextReplySink_AsyncProcessMessage_m1_8214 (SynchronizedContextReplySink_t1_904 * __this, Object_t * ___msg, Object_t * ___replySink, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Messaging.IMessage System.Runtime.Remoting.Contexts.SynchronizedContextReplySink::SyncProcessMessage(System.Runtime.Remoting.Messaging.IMessage)
extern "C" Object_t * SynchronizedContextReplySink_SyncProcessMessage_m1_8215 (SynchronizedContextReplySink_t1_904 * __this, Object_t * ___msg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
