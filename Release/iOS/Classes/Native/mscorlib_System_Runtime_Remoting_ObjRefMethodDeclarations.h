﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.ObjRef
struct ObjRef_t1_923;
// System.String
struct String_t;
// System.Runtime.Remoting.IChannelInfo
struct IChannelInfo_t1_1016;
// System.MarshalByRefObject
struct MarshalByRefObject_t1_69;
// System.Type
struct Type_t;
// System.Object
struct Object_t;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;
// System.Runtime.Remoting.IEnvoyInfo
struct IEnvoyInfo_t1_1018;
// System.Runtime.Remoting.IRemotingTypeInfo
struct IRemotingTypeInfo_t1_1017;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.Runtime.Remoting.ObjRef::.ctor()
extern "C" void ObjRef__ctor_m1_9080 (ObjRef_t1_923 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.ObjRef::.ctor(System.String,System.String,System.Runtime.Remoting.IChannelInfo)
extern "C" void ObjRef__ctor_m1_9081 (ObjRef_t1_923 * __this, String_t* ___typeName, String_t* ___uri, Object_t * ___cinfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.ObjRef::.ctor(System.Runtime.Remoting.ObjRef,System.Boolean)
extern "C" void ObjRef__ctor_m1_9082 (ObjRef_t1_923 * __this, ObjRef_t1_923 * ___o, bool ___unmarshalAsProxy, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.ObjRef::.ctor(System.MarshalByRefObject,System.Type)
extern "C" void ObjRef__ctor_m1_9083 (ObjRef_t1_923 * __this, MarshalByRefObject_t1_69 * ___o, Type_t * ___requestedType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.ObjRef::.ctor(System.Type,System.String,System.Object)
extern "C" void ObjRef__ctor_m1_9084 (ObjRef_t1_923 * __this, Type_t * ___type, String_t* ___url, Object_t * ___remoteChannelData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.ObjRef::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void ObjRef__ctor_m1_9085 (ObjRef_t1_923 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.ObjRef::.cctor()
extern "C" void ObjRef__cctor_m1_9086 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Remoting.ObjRef::IsPossibleToCAD()
extern "C" bool ObjRef_IsPossibleToCAD_m1_9087 (ObjRef_t1_923 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Remoting.ObjRef::get_IsReferenceToWellKnow()
extern "C" bool ObjRef_get_IsReferenceToWellKnow_m1_9088 (ObjRef_t1_923 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.IChannelInfo System.Runtime.Remoting.ObjRef::get_ChannelInfo()
extern "C" Object_t * ObjRef_get_ChannelInfo_m1_9089 (ObjRef_t1_923 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.ObjRef::set_ChannelInfo(System.Runtime.Remoting.IChannelInfo)
extern "C" void ObjRef_set_ChannelInfo_m1_9090 (ObjRef_t1_923 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.IEnvoyInfo System.Runtime.Remoting.ObjRef::get_EnvoyInfo()
extern "C" Object_t * ObjRef_get_EnvoyInfo_m1_9091 (ObjRef_t1_923 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.ObjRef::set_EnvoyInfo(System.Runtime.Remoting.IEnvoyInfo)
extern "C" void ObjRef_set_EnvoyInfo_m1_9092 (ObjRef_t1_923 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.IRemotingTypeInfo System.Runtime.Remoting.ObjRef::get_TypeInfo()
extern "C" Object_t * ObjRef_get_TypeInfo_m1_9093 (ObjRef_t1_923 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.ObjRef::set_TypeInfo(System.Runtime.Remoting.IRemotingTypeInfo)
extern "C" void ObjRef_set_TypeInfo_m1_9094 (ObjRef_t1_923 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.ObjRef::get_URI()
extern "C" String_t* ObjRef_get_URI_m1_9095 (ObjRef_t1_923 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.ObjRef::set_URI(System.String)
extern "C" void ObjRef_set_URI_m1_9096 (ObjRef_t1_923 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.ObjRef::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void ObjRef_GetObjectData_m1_9097 (ObjRef_t1_923 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.ObjRef::GetRealObject(System.Runtime.Serialization.StreamingContext)
extern "C" Object_t * ObjRef_GetRealObject_m1_9098 (ObjRef_t1_923 * __this, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Remoting.ObjRef::IsFromThisAppDomain()
extern "C" bool ObjRef_IsFromThisAppDomain_m1_9099 (ObjRef_t1_923 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Remoting.ObjRef::IsFromThisProcess()
extern "C" bool ObjRef_IsFromThisProcess_m1_9100 (ObjRef_t1_923 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.ObjRef::UpdateChannelInfo()
extern "C" void ObjRef_UpdateChannelInfo_m1_9101 (ObjRef_t1_923 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Runtime.Remoting.ObjRef::get_ServerType()
extern "C" Type_t * ObjRef_get_ServerType_m1_9102 (ObjRef_t1_923 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
