﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Channels.BaseChannelObjectWithProperties
struct BaseChannelObjectWithProperties_t1_864;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Object
struct Object_t;
// System.Collections.ICollection
struct ICollection_t1_280;
// System.Collections.IDictionary
struct IDictionary_t1_35;
// System.Array
struct Array_t;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1_863;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Channels.BaseChannelObjectWithProperties::.ctor()
extern "C" void BaseChannelObjectWithProperties__ctor_m1_8007 (BaseChannelObjectWithProperties_t1_864 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Runtime.Remoting.Channels.BaseChannelObjectWithProperties::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * BaseChannelObjectWithProperties_System_Collections_IEnumerable_GetEnumerator_m1_8008 (BaseChannelObjectWithProperties_t1_864 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.Remoting.Channels.BaseChannelObjectWithProperties::get_Count()
extern "C" int32_t BaseChannelObjectWithProperties_get_Count_m1_8009 (BaseChannelObjectWithProperties_t1_864 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Remoting.Channels.BaseChannelObjectWithProperties::get_IsFixedSize()
extern "C" bool BaseChannelObjectWithProperties_get_IsFixedSize_m1_8010 (BaseChannelObjectWithProperties_t1_864 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Remoting.Channels.BaseChannelObjectWithProperties::get_IsReadOnly()
extern "C" bool BaseChannelObjectWithProperties_get_IsReadOnly_m1_8011 (BaseChannelObjectWithProperties_t1_864 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Remoting.Channels.BaseChannelObjectWithProperties::get_IsSynchronized()
extern "C" bool BaseChannelObjectWithProperties_get_IsSynchronized_m1_8012 (BaseChannelObjectWithProperties_t1_864 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Channels.BaseChannelObjectWithProperties::get_Item(System.Object)
extern "C" Object_t * BaseChannelObjectWithProperties_get_Item_m1_8013 (BaseChannelObjectWithProperties_t1_864 * __this, Object_t * ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Channels.BaseChannelObjectWithProperties::set_Item(System.Object,System.Object)
extern "C" void BaseChannelObjectWithProperties_set_Item_m1_8014 (BaseChannelObjectWithProperties_t1_864 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ICollection System.Runtime.Remoting.Channels.BaseChannelObjectWithProperties::get_Keys()
extern "C" Object_t * BaseChannelObjectWithProperties_get_Keys_m1_8015 (BaseChannelObjectWithProperties_t1_864 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IDictionary System.Runtime.Remoting.Channels.BaseChannelObjectWithProperties::get_Properties()
extern "C" Object_t * BaseChannelObjectWithProperties_get_Properties_m1_8016 (BaseChannelObjectWithProperties_t1_864 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Channels.BaseChannelObjectWithProperties::get_SyncRoot()
extern "C" Object_t * BaseChannelObjectWithProperties_get_SyncRoot_m1_8017 (BaseChannelObjectWithProperties_t1_864 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ICollection System.Runtime.Remoting.Channels.BaseChannelObjectWithProperties::get_Values()
extern "C" Object_t * BaseChannelObjectWithProperties_get_Values_m1_8018 (BaseChannelObjectWithProperties_t1_864 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Channels.BaseChannelObjectWithProperties::Add(System.Object,System.Object)
extern "C" void BaseChannelObjectWithProperties_Add_m1_8019 (BaseChannelObjectWithProperties_t1_864 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Channels.BaseChannelObjectWithProperties::Clear()
extern "C" void BaseChannelObjectWithProperties_Clear_m1_8020 (BaseChannelObjectWithProperties_t1_864 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Remoting.Channels.BaseChannelObjectWithProperties::Contains(System.Object)
extern "C" bool BaseChannelObjectWithProperties_Contains_m1_8021 (BaseChannelObjectWithProperties_t1_864 * __this, Object_t * ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Channels.BaseChannelObjectWithProperties::CopyTo(System.Array,System.Int32)
extern "C" void BaseChannelObjectWithProperties_CopyTo_m1_8022 (BaseChannelObjectWithProperties_t1_864 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IDictionaryEnumerator System.Runtime.Remoting.Channels.BaseChannelObjectWithProperties::GetEnumerator()
extern "C" Object_t * BaseChannelObjectWithProperties_GetEnumerator_m1_8023 (BaseChannelObjectWithProperties_t1_864 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Channels.BaseChannelObjectWithProperties::Remove(System.Object)
extern "C" void BaseChannelObjectWithProperties_Remove_m1_8024 (BaseChannelObjectWithProperties_t1_864 * __this, Object_t * ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
