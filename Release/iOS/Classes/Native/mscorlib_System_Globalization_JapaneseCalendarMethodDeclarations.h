﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Globalization.JapaneseCalendar
struct JapaneseCalendar_t1_378;
// System.Int32[]
struct Int32U5BU5D_t1_275;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime.h"
#include "mscorlib_System_DayOfWeek.h"
#include "mscorlib_System_Globalization_CalendarWeekRule.h"

// System.Void System.Globalization.JapaneseCalendar::.ctor()
extern "C" void JapaneseCalendar__ctor_m1_4214 (JapaneseCalendar_t1_378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.JapaneseCalendar::.cctor()
extern "C" void JapaneseCalendar__cctor_m1_4215 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] System.Globalization.JapaneseCalendar::get_Eras()
extern "C" Int32U5BU5D_t1_275* JapaneseCalendar_get_Eras_m1_4216 (JapaneseCalendar_t1_378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.JapaneseCalendar::get_TwoDigitYearMax()
extern "C" int32_t JapaneseCalendar_get_TwoDigitYearMax_m1_4217 (JapaneseCalendar_t1_378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.JapaneseCalendar::set_TwoDigitYearMax(System.Int32)
extern "C" void JapaneseCalendar_set_TwoDigitYearMax_m1_4218 (JapaneseCalendar_t1_378 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.JapaneseCalendar::M_CheckDateTime(System.DateTime)
extern "C" void JapaneseCalendar_M_CheckDateTime_m1_4219 (JapaneseCalendar_t1_378 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.JapaneseCalendar::M_CheckEra(System.Int32&)
extern "C" void JapaneseCalendar_M_CheckEra_m1_4220 (JapaneseCalendar_t1_378 * __this, int32_t* ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.JapaneseCalendar::M_CheckYEG(System.Int32,System.Int32&)
extern "C" int32_t JapaneseCalendar_M_CheckYEG_m1_4221 (JapaneseCalendar_t1_378 * __this, int32_t ___year, int32_t* ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.JapaneseCalendar::M_CheckYE(System.Int32,System.Int32&)
extern "C" void JapaneseCalendar_M_CheckYE_m1_4222 (JapaneseCalendar_t1_378 * __this, int32_t ___year, int32_t* ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.JapaneseCalendar::M_CheckYMEG(System.Int32,System.Int32,System.Int32&)
extern "C" int32_t JapaneseCalendar_M_CheckYMEG_m1_4223 (JapaneseCalendar_t1_378 * __this, int32_t ___year, int32_t ___month, int32_t* ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.JapaneseCalendar::M_CheckYMDEG(System.Int32,System.Int32,System.Int32,System.Int32&)
extern "C" int32_t JapaneseCalendar_M_CheckYMDEG_m1_4224 (JapaneseCalendar_t1_378 * __this, int32_t ___year, int32_t ___month, int32_t ___day, int32_t* ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.JapaneseCalendar::AddMonths(System.DateTime,System.Int32)
extern "C" DateTime_t1_150  JapaneseCalendar_AddMonths_m1_4225 (JapaneseCalendar_t1_378 * __this, DateTime_t1_150  ___time, int32_t ___months, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.JapaneseCalendar::AddYears(System.DateTime,System.Int32)
extern "C" DateTime_t1_150  JapaneseCalendar_AddYears_m1_4226 (JapaneseCalendar_t1_378 * __this, DateTime_t1_150  ___time, int32_t ___years, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.JapaneseCalendar::GetDayOfMonth(System.DateTime)
extern "C" int32_t JapaneseCalendar_GetDayOfMonth_m1_4227 (JapaneseCalendar_t1_378 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DayOfWeek System.Globalization.JapaneseCalendar::GetDayOfWeek(System.DateTime)
extern "C" int32_t JapaneseCalendar_GetDayOfWeek_m1_4228 (JapaneseCalendar_t1_378 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.JapaneseCalendar::GetDayOfYear(System.DateTime)
extern "C" int32_t JapaneseCalendar_GetDayOfYear_m1_4229 (JapaneseCalendar_t1_378 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.JapaneseCalendar::GetDaysInMonth(System.Int32,System.Int32,System.Int32)
extern "C" int32_t JapaneseCalendar_GetDaysInMonth_m1_4230 (JapaneseCalendar_t1_378 * __this, int32_t ___year, int32_t ___month, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.JapaneseCalendar::GetDaysInYear(System.Int32,System.Int32)
extern "C" int32_t JapaneseCalendar_GetDaysInYear_m1_4231 (JapaneseCalendar_t1_378 * __this, int32_t ___year, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.JapaneseCalendar::GetEra(System.DateTime)
extern "C" int32_t JapaneseCalendar_GetEra_m1_4232 (JapaneseCalendar_t1_378 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.JapaneseCalendar::GetLeapMonth(System.Int32,System.Int32)
extern "C" int32_t JapaneseCalendar_GetLeapMonth_m1_4233 (JapaneseCalendar_t1_378 * __this, int32_t ___year, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.JapaneseCalendar::GetMonth(System.DateTime)
extern "C" int32_t JapaneseCalendar_GetMonth_m1_4234 (JapaneseCalendar_t1_378 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.JapaneseCalendar::GetMonthsInYear(System.Int32,System.Int32)
extern "C" int32_t JapaneseCalendar_GetMonthsInYear_m1_4235 (JapaneseCalendar_t1_378 * __this, int32_t ___year, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.JapaneseCalendar::GetWeekOfYear(System.DateTime,System.Globalization.CalendarWeekRule,System.DayOfWeek)
extern "C" int32_t JapaneseCalendar_GetWeekOfYear_m1_4236 (JapaneseCalendar_t1_378 * __this, DateTime_t1_150  ___time, int32_t ___rule, int32_t ___firstDayOfWeek, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.JapaneseCalendar::GetYear(System.DateTime)
extern "C" int32_t JapaneseCalendar_GetYear_m1_4237 (JapaneseCalendar_t1_378 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.JapaneseCalendar::IsLeapDay(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" bool JapaneseCalendar_IsLeapDay_m1_4238 (JapaneseCalendar_t1_378 * __this, int32_t ___year, int32_t ___month, int32_t ___day, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.JapaneseCalendar::IsLeapMonth(System.Int32,System.Int32,System.Int32)
extern "C" bool JapaneseCalendar_IsLeapMonth_m1_4239 (JapaneseCalendar_t1_378 * __this, int32_t ___year, int32_t ___month, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.JapaneseCalendar::IsLeapYear(System.Int32,System.Int32)
extern "C" bool JapaneseCalendar_IsLeapYear_m1_4240 (JapaneseCalendar_t1_378 * __this, int32_t ___year, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.JapaneseCalendar::ToDateTime(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" DateTime_t1_150  JapaneseCalendar_ToDateTime_m1_4241 (JapaneseCalendar_t1_378 * __this, int32_t ___year, int32_t ___month, int32_t ___day, int32_t ___hour, int32_t ___minute, int32_t ___second, int32_t ___millisecond, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.JapaneseCalendar::ToFourDigitYear(System.Int32)
extern "C" int32_t JapaneseCalendar_ToFourDigitYear_m1_4242 (JapaneseCalendar_t1_378 * __this, int32_t ___year, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.JapaneseCalendar::get_MinSupportedDateTime()
extern "C" DateTime_t1_150  JapaneseCalendar_get_MinSupportedDateTime_m1_4243 (JapaneseCalendar_t1_378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.JapaneseCalendar::get_MaxSupportedDateTime()
extern "C" DateTime_t1_150  JapaneseCalendar_get_MaxSupportedDateTime_m1_4244 (JapaneseCalendar_t1_378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
