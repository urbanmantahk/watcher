﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.IO.IsolatedStorage.IsolatedStorageFileStream
struct IsolatedStorageFileStream_t1_402;
// System.String
struct String_t;
// System.IO.IsolatedStorage.IsolatedStorageFile
struct IsolatedStorageFile_t1_397;
// Microsoft.Win32.SafeHandles.SafeFileHandle
struct SafeFileHandle_t1_85;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.AsyncCallback
struct AsyncCallback_t1_28;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_FileMode.h"
#include "mscorlib_System_IO_FileAccess.h"
#include "mscorlib_System_IO_FileShare.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_IO_SeekOrigin.h"

// System.Void System.IO.IsolatedStorage.IsolatedStorageFileStream::.ctor(System.String,System.IO.FileMode)
extern "C" void IsolatedStorageFileStream__ctor_m1_4648 (IsolatedStorageFileStream_t1_402 * __this, String_t* ___path, int32_t ___mode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.IsolatedStorage.IsolatedStorageFileStream::.ctor(System.String,System.IO.FileMode,System.IO.FileAccess)
extern "C" void IsolatedStorageFileStream__ctor_m1_4649 (IsolatedStorageFileStream_t1_402 * __this, String_t* ___path, int32_t ___mode, int32_t ___access, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.IsolatedStorage.IsolatedStorageFileStream::.ctor(System.String,System.IO.FileMode,System.IO.FileAccess,System.IO.FileShare)
extern "C" void IsolatedStorageFileStream__ctor_m1_4650 (IsolatedStorageFileStream_t1_402 * __this, String_t* ___path, int32_t ___mode, int32_t ___access, int32_t ___share, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.IsolatedStorage.IsolatedStorageFileStream::.ctor(System.String,System.IO.FileMode,System.IO.FileAccess,System.IO.FileShare,System.Int32)
extern "C" void IsolatedStorageFileStream__ctor_m1_4651 (IsolatedStorageFileStream_t1_402 * __this, String_t* ___path, int32_t ___mode, int32_t ___access, int32_t ___share, int32_t ___bufferSize, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.IsolatedStorage.IsolatedStorageFileStream::.ctor(System.String,System.IO.FileMode,System.IO.FileAccess,System.IO.FileShare,System.Int32,System.IO.IsolatedStorage.IsolatedStorageFile)
extern "C" void IsolatedStorageFileStream__ctor_m1_4652 (IsolatedStorageFileStream_t1_402 * __this, String_t* ___path, int32_t ___mode, int32_t ___access, int32_t ___share, int32_t ___bufferSize, IsolatedStorageFile_t1_397 * ___isf, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.IsolatedStorage.IsolatedStorageFileStream::.ctor(System.String,System.IO.FileMode,System.IO.FileAccess,System.IO.FileShare,System.IO.IsolatedStorage.IsolatedStorageFile)
extern "C" void IsolatedStorageFileStream__ctor_m1_4653 (IsolatedStorageFileStream_t1_402 * __this, String_t* ___path, int32_t ___mode, int32_t ___access, int32_t ___share, IsolatedStorageFile_t1_397 * ___isf, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.IsolatedStorage.IsolatedStorageFileStream::.ctor(System.String,System.IO.FileMode,System.IO.FileAccess,System.IO.IsolatedStorage.IsolatedStorageFile)
extern "C" void IsolatedStorageFileStream__ctor_m1_4654 (IsolatedStorageFileStream_t1_402 * __this, String_t* ___path, int32_t ___mode, int32_t ___access, IsolatedStorageFile_t1_397 * ___isf, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.IsolatedStorage.IsolatedStorageFileStream::.ctor(System.String,System.IO.FileMode,System.IO.IsolatedStorage.IsolatedStorageFile)
extern "C" void IsolatedStorageFileStream__ctor_m1_4655 (IsolatedStorageFileStream_t1_402 * __this, String_t* ___path, int32_t ___mode, IsolatedStorageFile_t1_397 * ___isf, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.IO.IsolatedStorage.IsolatedStorageFileStream::CreateIsolatedPath(System.IO.IsolatedStorage.IsolatedStorageFile,System.String,System.IO.FileMode)
extern "C" String_t* IsolatedStorageFileStream_CreateIsolatedPath_m1_4656 (Object_t * __this /* static, unused */, IsolatedStorageFile_t1_397 * ___isf, String_t* ___path, int32_t ___mode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IO.IsolatedStorage.IsolatedStorageFileStream::get_CanRead()
extern "C" bool IsolatedStorageFileStream_get_CanRead_m1_4657 (IsolatedStorageFileStream_t1_402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IO.IsolatedStorage.IsolatedStorageFileStream::get_CanSeek()
extern "C" bool IsolatedStorageFileStream_get_CanSeek_m1_4658 (IsolatedStorageFileStream_t1_402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IO.IsolatedStorage.IsolatedStorageFileStream::get_CanWrite()
extern "C" bool IsolatedStorageFileStream_get_CanWrite_m1_4659 (IsolatedStorageFileStream_t1_402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Microsoft.Win32.SafeHandles.SafeFileHandle System.IO.IsolatedStorage.IsolatedStorageFileStream::get_SafeFileHandle()
extern "C" SafeFileHandle_t1_85 * IsolatedStorageFileStream_get_SafeFileHandle_m1_4660 (IsolatedStorageFileStream_t1_402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.IO.IsolatedStorage.IsolatedStorageFileStream::get_Handle()
extern "C" IntPtr_t IsolatedStorageFileStream_get_Handle_m1_4661 (IsolatedStorageFileStream_t1_402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IO.IsolatedStorage.IsolatedStorageFileStream::get_IsAsync()
extern "C" bool IsolatedStorageFileStream_get_IsAsync_m1_4662 (IsolatedStorageFileStream_t1_402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.IO.IsolatedStorage.IsolatedStorageFileStream::get_Length()
extern "C" int64_t IsolatedStorageFileStream_get_Length_m1_4663 (IsolatedStorageFileStream_t1_402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.IO.IsolatedStorage.IsolatedStorageFileStream::get_Position()
extern "C" int64_t IsolatedStorageFileStream_get_Position_m1_4664 (IsolatedStorageFileStream_t1_402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.IsolatedStorage.IsolatedStorageFileStream::set_Position(System.Int64)
extern "C" void IsolatedStorageFileStream_set_Position_m1_4665 (IsolatedStorageFileStream_t1_402 * __this, int64_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.IO.IsolatedStorage.IsolatedStorageFileStream::BeginRead(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern "C" Object_t * IsolatedStorageFileStream_BeginRead_m1_4666 (IsolatedStorageFileStream_t1_402 * __this, ByteU5BU5D_t1_109* ___buffer, int32_t ___offset, int32_t ___numBytes, AsyncCallback_t1_28 * ___userCallback, Object_t * ___stateObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.IO.IsolatedStorage.IsolatedStorageFileStream::BeginWrite(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern "C" Object_t * IsolatedStorageFileStream_BeginWrite_m1_4667 (IsolatedStorageFileStream_t1_402 * __this, ByteU5BU5D_t1_109* ___buffer, int32_t ___offset, int32_t ___numBytes, AsyncCallback_t1_28 * ___userCallback, Object_t * ___stateObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.IO.IsolatedStorage.IsolatedStorageFileStream::EndRead(System.IAsyncResult)
extern "C" int32_t IsolatedStorageFileStream_EndRead_m1_4668 (IsolatedStorageFileStream_t1_402 * __this, Object_t * ___asyncResult, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.IsolatedStorage.IsolatedStorageFileStream::EndWrite(System.IAsyncResult)
extern "C" void IsolatedStorageFileStream_EndWrite_m1_4669 (IsolatedStorageFileStream_t1_402 * __this, Object_t * ___asyncResult, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.IsolatedStorage.IsolatedStorageFileStream::Flush()
extern "C" void IsolatedStorageFileStream_Flush_m1_4670 (IsolatedStorageFileStream_t1_402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.IO.IsolatedStorage.IsolatedStorageFileStream::Read(System.Byte[],System.Int32,System.Int32)
extern "C" int32_t IsolatedStorageFileStream_Read_m1_4671 (IsolatedStorageFileStream_t1_402 * __this, ByteU5BU5D_t1_109* ___buffer, int32_t ___offset, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.IO.IsolatedStorage.IsolatedStorageFileStream::ReadByte()
extern "C" int32_t IsolatedStorageFileStream_ReadByte_m1_4672 (IsolatedStorageFileStream_t1_402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.IO.IsolatedStorage.IsolatedStorageFileStream::Seek(System.Int64,System.IO.SeekOrigin)
extern "C" int64_t IsolatedStorageFileStream_Seek_m1_4673 (IsolatedStorageFileStream_t1_402 * __this, int64_t ___offset, int32_t ___origin, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.IsolatedStorage.IsolatedStorageFileStream::SetLength(System.Int64)
extern "C" void IsolatedStorageFileStream_SetLength_m1_4674 (IsolatedStorageFileStream_t1_402 * __this, int64_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.IsolatedStorage.IsolatedStorageFileStream::Write(System.Byte[],System.Int32,System.Int32)
extern "C" void IsolatedStorageFileStream_Write_m1_4675 (IsolatedStorageFileStream_t1_402 * __this, ByteU5BU5D_t1_109* ___buffer, int32_t ___offset, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.IsolatedStorage.IsolatedStorageFileStream::WriteByte(System.Byte)
extern "C" void IsolatedStorageFileStream_WriteByte_m1_4676 (IsolatedStorageFileStream_t1_402 * __this, uint8_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.IsolatedStorage.IsolatedStorageFileStream::Dispose(System.Boolean)
extern "C" void IsolatedStorageFileStream_Dispose_m1_4677 (IsolatedStorageFileStream_t1_402 * __this, bool ___disposing, const MethodInfo* method) IL2CPP_METHOD_ATTR;
