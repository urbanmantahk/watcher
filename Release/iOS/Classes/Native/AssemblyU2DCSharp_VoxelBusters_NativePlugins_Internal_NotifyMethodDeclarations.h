﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.Internal.NotifyNPSettingsOnValueChangeAttribute
struct NotifyNPSettingsOnValueChangeAttribute_t8_321;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.Internal.NotifyNPSettingsOnValueChangeAttribute::.ctor()
extern "C" void NotifyNPSettingsOnValueChangeAttribute__ctor_m8_1888 (NotifyNPSettingsOnValueChangeAttribute_t8_321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
