﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "AssemblyU2DCSharp_ExifLibrary_ExifProperty.h"

// ExifLibrary.ExifUndefined
struct  ExifUndefined_t8_123  : public ExifProperty_t8_99
{
	// System.Byte[] ExifLibrary.ExifUndefined::mValue
	ByteU5BU5D_t1_109* ___mValue_3;
};
