﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Resources.MissingSatelliteAssemblyException
struct MissingSatelliteAssemblyException_t1_643;
// System.String
struct String_t;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;
// System.Exception
struct Exception_t1_33;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.Resources.MissingSatelliteAssemblyException::.ctor()
extern "C" void MissingSatelliteAssemblyException__ctor_m1_7350 (MissingSatelliteAssemblyException_t1_643 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.MissingSatelliteAssemblyException::.ctor(System.String)
extern "C" void MissingSatelliteAssemblyException__ctor_m1_7351 (MissingSatelliteAssemblyException_t1_643 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.MissingSatelliteAssemblyException::.ctor(System.String,System.String)
extern "C" void MissingSatelliteAssemblyException__ctor_m1_7352 (MissingSatelliteAssemblyException_t1_643 * __this, String_t* ___message, String_t* ___cultureName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.MissingSatelliteAssemblyException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void MissingSatelliteAssemblyException__ctor_m1_7353 (MissingSatelliteAssemblyException_t1_643 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.MissingSatelliteAssemblyException::.ctor(System.String,System.Exception)
extern "C" void MissingSatelliteAssemblyException__ctor_m1_7354 (MissingSatelliteAssemblyException_t1_643 * __this, String_t* ___message, Exception_t1_33 * ___inner, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Resources.MissingSatelliteAssemblyException::get_CultureName()
extern "C" String_t* MissingSatelliteAssemblyException_get_CultureName_m1_7355 (MissingSatelliteAssemblyException_t1_643 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
