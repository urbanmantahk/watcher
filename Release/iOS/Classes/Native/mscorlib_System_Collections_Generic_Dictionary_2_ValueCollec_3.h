﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__1.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>
struct  Enumerator_t1_1926 
{
	// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator::host_enumerator
	Enumerator_t1_1925  ___host_enumerator_0;
};
