﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.AssetStoreProductUtility.Demo.DemoMainMenu
struct DemoMainMenu_t8_16;
// VoxelBusters.AssetStoreProductUtility.Demo.DemoSubMenu
struct DemoSubMenu_t8_18;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.AssetStoreProductUtility.Demo.DemoMainMenu::.ctor()
extern "C" void DemoMainMenu__ctor_m8_124 (DemoMainMenu_t8_16 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.AssetStoreProductUtility.Demo.DemoMainMenu::Start()
extern "C" void DemoMainMenu_Start_m8_125 (DemoMainMenu_t8_16 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.AssetStoreProductUtility.Demo.DemoMainMenu::Update()
extern "C" void DemoMainMenu_Update_m8_126 (DemoMainMenu_t8_16 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.AssetStoreProductUtility.Demo.DemoMainMenu::DisableAllSubMenus()
extern "C" void DemoMainMenu_DisableAllSubMenus_m8_127 (DemoMainMenu_t8_16 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.AssetStoreProductUtility.Demo.DemoMainMenu::EnableSubMenu(VoxelBusters.AssetStoreProductUtility.Demo.DemoSubMenu)
extern "C" void DemoMainMenu_EnableSubMenu_m8_128 (DemoMainMenu_t8_16 * __this, DemoSubMenu_t8_18 * ____enabledSubMenu, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.AssetStoreProductUtility.Demo.DemoMainMenu::OnGUIWindow()
extern "C" void DemoMainMenu_OnGUIWindow_m8_129 (DemoMainMenu_t8_16 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
