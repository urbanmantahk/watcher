﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType.h"
#include "mscorlib_System_IntPtr.h"

// System.Runtime.InteropServices.ComTypes.BINDPTR
#pragma pack(push, tp, 1)
struct  BINDPTR_t1_720 
{
	union
	{
		// System.IntPtr System.Runtime.InteropServices.ComTypes.BINDPTR::lpfuncdesc
		struct
		{
			IntPtr_t ___lpfuncdesc_0;
		};
		// System.IntPtr System.Runtime.InteropServices.ComTypes.BINDPTR::lptcomp
		struct
		{
			IntPtr_t ___lptcomp_1;
		};
		// System.IntPtr System.Runtime.InteropServices.ComTypes.BINDPTR::lpvardesc
		struct
		{
			IntPtr_t ___lpvardesc_2;
		};
	};
};
#pragma pack(pop, tp)
