﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Xml.DTDAttListDeclarationCollection
struct DTDAttListDeclarationCollection_t4_84;
// Mono.Xml.DTDObjectModel
struct DTDObjectModel_t4_82;
// Mono.Xml.DTDAttListDeclaration
struct DTDAttListDeclaration_t4_96;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Xml.DTDAttListDeclarationCollection::.ctor(Mono.Xml.DTDObjectModel)
extern "C" void DTDAttListDeclarationCollection__ctor_m4_167 (DTDAttListDeclarationCollection_t4_84 * __this, DTDObjectModel_t4_82 * ___root, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Xml.DTDAttListDeclaration Mono.Xml.DTDAttListDeclarationCollection::get_Item(System.String)
extern "C" DTDAttListDeclaration_t4_96 * DTDAttListDeclarationCollection_get_Item_m4_168 (DTDAttListDeclarationCollection_t4_84 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Xml.DTDAttListDeclarationCollection::Add(System.String,Mono.Xml.DTDAttListDeclaration)
extern "C" void DTDAttListDeclarationCollection_Add_m4_169 (DTDAttListDeclarationCollection_t4_84 * __this, String_t* ___name, DTDAttListDeclaration_t4_96 * ___decl, const MethodInfo* method) IL2CPP_METHOD_ATTR;
