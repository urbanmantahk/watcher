﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Permissions.UIPermission
struct UIPermission_t1_1317;
// System.Security.IPermission
struct IPermission_t1_1400;
// System.Security.SecurityElement
struct SecurityElement_t1_242;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Permissions_PermissionState.h"
#include "mscorlib_System_Security_Permissions_UIPermissionClipboard.h"
#include "mscorlib_System_Security_Permissions_UIPermissionWindow.h"

// System.Void System.Security.Permissions.UIPermission::.ctor(System.Security.Permissions.PermissionState)
extern "C" void UIPermission__ctor_m1_11256 (UIPermission_t1_1317 * __this, int32_t ___state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.UIPermission::.ctor(System.Security.Permissions.UIPermissionClipboard)
extern "C" void UIPermission__ctor_m1_11257 (UIPermission_t1_1317 * __this, int32_t ___clipboardFlag, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.UIPermission::.ctor(System.Security.Permissions.UIPermissionWindow)
extern "C" void UIPermission__ctor_m1_11258 (UIPermission_t1_1317 * __this, int32_t ___windowFlag, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.UIPermission::.ctor(System.Security.Permissions.UIPermissionWindow,System.Security.Permissions.UIPermissionClipboard)
extern "C" void UIPermission__ctor_m1_11259 (UIPermission_t1_1317 * __this, int32_t ___windowFlag, int32_t ___clipboardFlag, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Permissions.UIPermission::System.Security.Permissions.IBuiltInPermission.GetTokenIndex()
extern "C" int32_t UIPermission_System_Security_Permissions_IBuiltInPermission_GetTokenIndex_m1_11260 (UIPermission_t1_1317 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Permissions.UIPermissionClipboard System.Security.Permissions.UIPermission::get_Clipboard()
extern "C" int32_t UIPermission_get_Clipboard_m1_11261 (UIPermission_t1_1317 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.UIPermission::set_Clipboard(System.Security.Permissions.UIPermissionClipboard)
extern "C" void UIPermission_set_Clipboard_m1_11262 (UIPermission_t1_1317 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Permissions.UIPermissionWindow System.Security.Permissions.UIPermission::get_Window()
extern "C" int32_t UIPermission_get_Window_m1_11263 (UIPermission_t1_1317 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.UIPermission::set_Window(System.Security.Permissions.UIPermissionWindow)
extern "C" void UIPermission_set_Window_m1_11264 (UIPermission_t1_1317 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.UIPermission::Copy()
extern "C" Object_t * UIPermission_Copy_m1_11265 (UIPermission_t1_1317 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.UIPermission::FromXml(System.Security.SecurityElement)
extern "C" void UIPermission_FromXml_m1_11266 (UIPermission_t1_1317 * __this, SecurityElement_t1_242 * ___esd, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.UIPermission::Intersect(System.Security.IPermission)
extern "C" Object_t * UIPermission_Intersect_m1_11267 (UIPermission_t1_1317 * __this, Object_t * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.UIPermission::IsSubsetOf(System.Security.IPermission)
extern "C" bool UIPermission_IsSubsetOf_m1_11268 (UIPermission_t1_1317 * __this, Object_t * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.UIPermission::IsUnrestricted()
extern "C" bool UIPermission_IsUnrestricted_m1_11269 (UIPermission_t1_1317 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityElement System.Security.Permissions.UIPermission::ToXml()
extern "C" SecurityElement_t1_242 * UIPermission_ToXml_m1_11270 (UIPermission_t1_1317 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.UIPermission::Union(System.Security.IPermission)
extern "C" Object_t * UIPermission_Union_m1_11271 (UIPermission_t1_1317 * __this, Object_t * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.UIPermission::IsEmpty(System.Security.Permissions.UIPermissionWindow,System.Security.Permissions.UIPermissionClipboard)
extern "C" bool UIPermission_IsEmpty_m1_11272 (UIPermission_t1_1317 * __this, int32_t ___w, int32_t ___c, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Permissions.UIPermission System.Security.Permissions.UIPermission::Cast(System.Security.IPermission)
extern "C" UIPermission_t1_1317 * UIPermission_Cast_m1_11273 (UIPermission_t1_1317 * __this, Object_t * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
