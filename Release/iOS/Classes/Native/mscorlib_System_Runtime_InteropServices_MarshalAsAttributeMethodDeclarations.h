﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.InteropServices.MarshalAsAttribute
struct MarshalAsAttribute_t1_42;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_InteropServices_UnmanagedType.h"

// System.Void System.Runtime.InteropServices.MarshalAsAttribute::.ctor(System.Int16)
extern "C" void MarshalAsAttribute__ctor_m1_1295 (MarshalAsAttribute_t1_42 * __this, int16_t ___unmanagedType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.MarshalAsAttribute::.ctor(System.Runtime.InteropServices.UnmanagedType)
extern "C" void MarshalAsAttribute__ctor_m1_1296 (MarshalAsAttribute_t1_42 * __this, int32_t ___unmanagedType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.InteropServices.UnmanagedType System.Runtime.InteropServices.MarshalAsAttribute::get_Value()
extern "C" int32_t MarshalAsAttribute_get_Value_m1_1297 (MarshalAsAttribute_t1_42 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
