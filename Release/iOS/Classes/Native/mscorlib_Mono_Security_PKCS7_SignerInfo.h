﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Mono.Security.X509.X509Certificate
struct X509Certificate_t1_151;
// System.String
struct String_t;
// System.Security.Cryptography.AsymmetricAlgorithm
struct AsymmetricAlgorithm_t1_228;
// System.Collections.ArrayList
struct ArrayList_t1_170;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "mscorlib_System_Object.h"

// Mono.Security.PKCS7/SignerInfo
struct  SignerInfo_t1_227  : public Object_t
{
	// System.Byte Mono.Security.PKCS7/SignerInfo::version
	uint8_t ___version_0;
	// Mono.Security.X509.X509Certificate Mono.Security.PKCS7/SignerInfo::x509
	X509Certificate_t1_151 * ___x509_1;
	// System.String Mono.Security.PKCS7/SignerInfo::hashAlgorithm
	String_t* ___hashAlgorithm_2;
	// System.Security.Cryptography.AsymmetricAlgorithm Mono.Security.PKCS7/SignerInfo::key
	AsymmetricAlgorithm_t1_228 * ___key_3;
	// System.Collections.ArrayList Mono.Security.PKCS7/SignerInfo::authenticatedAttributes
	ArrayList_t1_170 * ___authenticatedAttributes_4;
	// System.Collections.ArrayList Mono.Security.PKCS7/SignerInfo::unauthenticatedAttributes
	ArrayList_t1_170 * ___unauthenticatedAttributes_5;
	// System.Byte[] Mono.Security.PKCS7/SignerInfo::signature
	ByteU5BU5D_t1_109* ___signature_6;
	// System.String Mono.Security.PKCS7/SignerInfo::issuer
	String_t* ___issuer_7;
	// System.Byte[] Mono.Security.PKCS7/SignerInfo::serial
	ByteU5BU5D_t1_109* ___serial_8;
	// System.Byte[] Mono.Security.PKCS7/SignerInfo::ski
	ByteU5BU5D_t1_109* ___ski_9;
};
