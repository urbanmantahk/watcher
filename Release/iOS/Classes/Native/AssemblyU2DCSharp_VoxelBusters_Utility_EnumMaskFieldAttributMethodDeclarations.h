﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.Utility.EnumMaskFieldAttribute
struct EnumMaskFieldAttribute_t8_145;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.Utility.EnumMaskFieldAttribute::.ctor()
extern "C" void EnumMaskFieldAttribute__ctor_m8_852 (EnumMaskFieldAttribute_t8_145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.EnumMaskFieldAttribute::.ctor(System.Type)
extern "C" void EnumMaskFieldAttribute__ctor_m8_853 (EnumMaskFieldAttribute_t8_145 * __this, Type_t * ____targetType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type VoxelBusters.Utility.EnumMaskFieldAttribute::get_TargetType()
extern "C" Type_t * EnumMaskFieldAttribute_get_TargetType_m8_854 (EnumMaskFieldAttribute_t8_145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.EnumMaskFieldAttribute::set_TargetType(System.Type)
extern "C" void EnumMaskFieldAttribute_set_TargetType_m8_855 (EnumMaskFieldAttribute_t8_145 * __this, Type_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.Utility.EnumMaskFieldAttribute::IsEnum()
extern "C" bool EnumMaskFieldAttribute_IsEnum_m8_856 (EnumMaskFieldAttribute_t8_145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
