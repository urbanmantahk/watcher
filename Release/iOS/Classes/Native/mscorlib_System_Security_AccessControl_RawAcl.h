﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.Security.AccessControl.GenericAce>
struct List_1_t1_1125;

#include "mscorlib_System_Security_AccessControl_GenericAcl.h"

// System.Security.AccessControl.RawAcl
struct  RawAcl_t1_1170  : public GenericAcl_t1_1114
{
	// System.Byte System.Security.AccessControl.RawAcl::revision
	uint8_t ___revision_3;
	// System.Collections.Generic.List`1<System.Security.AccessControl.GenericAce> System.Security.AccessControl.RawAcl::list
	List_1_t1_1125 * ___list_4;
};
