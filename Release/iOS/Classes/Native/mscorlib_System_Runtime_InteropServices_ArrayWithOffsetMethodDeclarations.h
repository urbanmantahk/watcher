﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_InteropServices_ArrayWithOffset.h"

// System.Void System.Runtime.InteropServices.ArrayWithOffset::.ctor(System.Object,System.Int32)
extern "C" void ArrayWithOffset__ctor_m1_7576 (ArrayWithOffset_t1_752 * __this, Object_t * ___array, int32_t ___offset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.InteropServices.ArrayWithOffset::Equals(System.Object)
extern "C" bool ArrayWithOffset_Equals_m1_7577 (ArrayWithOffset_t1_752 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.InteropServices.ArrayWithOffset::Equals(System.Runtime.InteropServices.ArrayWithOffset)
extern "C" bool ArrayWithOffset_Equals_m1_7578 (ArrayWithOffset_t1_752 * __this, ArrayWithOffset_t1_752  ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.InteropServices.ArrayWithOffset::GetHashCode()
extern "C" int32_t ArrayWithOffset_GetHashCode_m1_7579 (ArrayWithOffset_t1_752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.InteropServices.ArrayWithOffset::GetArray()
extern "C" Object_t * ArrayWithOffset_GetArray_m1_7580 (ArrayWithOffset_t1_752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.InteropServices.ArrayWithOffset::GetOffset()
extern "C" int32_t ArrayWithOffset_GetOffset_m1_7581 (ArrayWithOffset_t1_752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.InteropServices.ArrayWithOffset::op_Equality(System.Runtime.InteropServices.ArrayWithOffset,System.Runtime.InteropServices.ArrayWithOffset)
extern "C" bool ArrayWithOffset_op_Equality_m1_7582 (Object_t * __this /* static, unused */, ArrayWithOffset_t1_752  ___a, ArrayWithOffset_t1_752  ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.InteropServices.ArrayWithOffset::op_Inequality(System.Runtime.InteropServices.ArrayWithOffset,System.Runtime.InteropServices.ArrayWithOffset)
extern "C" bool ArrayWithOffset_op_Inequality_m1_7583 (Object_t * __this /* static, unused */, ArrayWithOffset_t1_752  ___a, ArrayWithOffset_t1_752  ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
