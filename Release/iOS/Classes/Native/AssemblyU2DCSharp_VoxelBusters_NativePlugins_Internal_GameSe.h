﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VoxelBusters.NativePlugins.IDContainer[]
struct IDContainerU5BU5D_t8_239;

#include "mscorlib_System_Object.h"

// VoxelBusters.NativePlugins.Internal.GameServicesIDHandler
struct  GameServicesIDHandler_t8_322  : public Object_t
{
};
struct GameServicesIDHandler_t8_322_StaticFields{
	// VoxelBusters.NativePlugins.IDContainer[] VoxelBusters.NativePlugins.Internal.GameServicesIDHandler::leaderboardIDCollection
	IDContainerU5BU5D_t8_239* ___leaderboardIDCollection_0;
	// VoxelBusters.NativePlugins.IDContainer[] VoxelBusters.NativePlugins.Internal.GameServicesIDHandler::achievementIDCollection
	IDContainerU5BU5D_t8_239* ___achievementIDCollection_1;
};
