﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.Utility.ShaderUtility/ShaderInfo
struct ShaderInfo_t8_26;
// System.String
struct String_t;
// System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderProperty>
struct List_1_t1_1900;
// UnityEngine.Shader
struct Shader_t6_71;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.Utility.ShaderUtility/ShaderInfo::.ctor()
extern "C" void ShaderInfo__ctor_m8_165 (ShaderInfo_t8_26 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.Utility.ShaderUtility/ShaderInfo::get_Name()
extern "C" String_t* ShaderInfo_get_Name_m8_166 (ShaderInfo_t8_26 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.ShaderUtility/ShaderInfo::set_Name(System.String)
extern "C" void ShaderInfo_set_Name_m8_167 (ShaderInfo_t8_26 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderProperty> VoxelBusters.Utility.ShaderUtility/ShaderInfo::get_PropertyList()
extern "C" List_1_t1_1900 * ShaderInfo_get_PropertyList_m8_168 (ShaderInfo_t8_26 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.ShaderUtility/ShaderInfo::set_PropertyList(System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderProperty>)
extern "C" void ShaderInfo_set_PropertyList_m8_169 (ShaderInfo_t8_26 * __this, List_1_t1_1900 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Shader VoxelBusters.Utility.ShaderUtility/ShaderInfo::GetShader()
extern "C" Shader_t6_71 * ShaderInfo_GetShader_m8_170 (ShaderInfo_t8_26 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
