﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.Utility.UnityGUI.MENU.GUIMainMenu
struct GUIMainMenu_t8_140;
// VoxelBusters.Utility.UnityGUI.MENU.GUISubMenu[]
struct GUISubMenuU5BU5D_t8_142;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.Utility.UnityGUI.MENU.GUIMainMenu::.ctor()
extern "C" void GUIMainMenu__ctor_m8_821 (GUIMainMenu_t8_140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.UnityGUI.MENU.GUIMainMenu::Start()
extern "C" void GUIMainMenu_Start_m8_822 (GUIMainMenu_t8_140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.UnityGUI.MENU.GUIMainMenu::Update()
extern "C" void GUIMainMenu_Update_m8_823 (GUIMainMenu_t8_140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.UnityGUI.MENU.GUIMainMenu::OnGUI()
extern "C" void GUIMainMenu_OnGUI_m8_824 (GUIMainMenu_t8_140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.UnityGUI.MENU.GUIMainMenu::DrawMainMenu()
extern "C" void GUIMainMenu_DrawMainMenu_m8_825 (GUIMainMenu_t8_140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.UnityGUI.MENU.GUIMainMenu::DrawSubMenuColumn(VoxelBusters.Utility.UnityGUI.MENU.GUISubMenu[],System.Int32,System.Int32)
extern "C" void GUIMainMenu_DrawSubMenuColumn_m8_826 (GUIMainMenu_t8_140 * __this, GUISubMenuU5BU5D_t8_142* ____subMenuList, int32_t ____startIndex, int32_t ____endIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
