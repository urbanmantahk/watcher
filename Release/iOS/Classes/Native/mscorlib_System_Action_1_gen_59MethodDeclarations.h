﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Action_1_gen_7MethodDeclarations.h"

// System.Void System.Action`1<VoxelBusters.DebugPRO.Internal.ConsoleTag>::.ctor(System.Object,System.IntPtr)
#define Action_1__ctor_m1_27416(__this, ___object, ___method, method) (( void (*) (Action_1_t1_2734 *, Object_t *, IntPtr_t, const MethodInfo*))Action_1__ctor_m1_15358_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`1<VoxelBusters.DebugPRO.Internal.ConsoleTag>::Invoke(T)
#define Action_1_Invoke_m1_27417(__this, ___obj, method) (( void (*) (Action_1_t1_2734 *, ConsoleTag_t8_171 *, const MethodInfo*))Action_1_Invoke_m1_15359_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Action`1<VoxelBusters.DebugPRO.Internal.ConsoleTag>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Action_1_BeginInvoke_m1_27418(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Action_1_t1_2734 *, ConsoleTag_t8_171 *, AsyncCallback_t1_28 *, Object_t *, const MethodInfo*))Action_1_BeginInvoke_m1_15360_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Void System.Action`1<VoxelBusters.DebugPRO.Internal.ConsoleTag>::EndInvoke(System.IAsyncResult)
#define Action_1_EndInvoke_m1_27419(__this, ___result, method) (( void (*) (Action_1_t1_2734 *, Object_t *, const MethodInfo*))Action_1_EndInvoke_m1_15361_gshared)(__this, ___result, method)
