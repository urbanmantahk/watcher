﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.X509.X520/UserId
struct UserId_t1_210;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Security.X509.X520/UserId::.ctor()
extern "C" void UserId__ctor_m1_2410 (UserId_t1_210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
