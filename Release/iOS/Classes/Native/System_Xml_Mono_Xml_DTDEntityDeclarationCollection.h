﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "System_Xml_Mono_Xml_DTDCollectionBase.h"

// Mono.Xml.DTDEntityDeclarationCollection
struct  DTDEntityDeclarationCollection_t4_86  : public DTDCollectionBase_t4_91
{
};
