﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExifLibrary.ExifByteArray
struct ExifByteArray_t8_115;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Object
struct Object_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTag.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifInterOperability.h"

// System.Void ExifLibrary.ExifByteArray::.ctor(ExifLibrary.ExifTag,System.Byte[])
extern "C" void ExifByteArray__ctor_m8_517 (ExifByteArray_t8_115 * __this, int32_t ___tag, ByteU5BU5D_t1_109* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ExifLibrary.ExifByteArray::get__Value()
extern "C" Object_t * ExifByteArray_get__Value_m8_518 (ExifByteArray_t8_115 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.ExifByteArray::set__Value(System.Object)
extern "C" void ExifByteArray_set__Value_m8_519 (ExifByteArray_t8_115 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ExifLibrary.ExifByteArray::get_Value()
extern "C" ByteU5BU5D_t1_109* ExifByteArray_get_Value_m8_520 (ExifByteArray_t8_115 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.ExifByteArray::set_Value(System.Byte[])
extern "C" void ExifByteArray_set_Value_m8_521 (ExifByteArray_t8_115 * __this, ByteU5BU5D_t1_109* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExifLibrary.ExifByteArray::ToString()
extern "C" String_t* ExifByteArray_ToString_m8_522 (ExifByteArray_t8_115 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.ExifInterOperability ExifLibrary.ExifByteArray::get_Interoperability()
extern "C" ExifInterOperability_t8_113  ExifByteArray_get_Interoperability_m8_523 (ExifByteArray_t8_115 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ExifLibrary.ExifByteArray::op_Implicit(ExifLibrary.ExifByteArray)
extern "C" ByteU5BU5D_t1_109* ExifByteArray_op_Implicit_m8_524 (Object_t * __this /* static, unused */, ExifByteArray_t8_115 * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
