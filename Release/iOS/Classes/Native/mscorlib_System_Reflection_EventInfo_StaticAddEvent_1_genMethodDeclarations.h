﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.EventInfo/StaticAddEvent`1<System.Object>
struct StaticAddEvent_1_t1_2095;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"

// System.Void System.Reflection.EventInfo/StaticAddEvent`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C" void StaticAddEvent_1__ctor_m1_16730_gshared (StaticAddEvent_1_t1_2095 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define StaticAddEvent_1__ctor_m1_16730(__this, ___object, ___method, method) (( void (*) (StaticAddEvent_1_t1_2095 *, Object_t *, IntPtr_t, const MethodInfo*))StaticAddEvent_1__ctor_m1_16730_gshared)(__this, ___object, ___method, method)
// System.Void System.Reflection.EventInfo/StaticAddEvent`1<System.Object>::Invoke(D)
extern "C" void StaticAddEvent_1_Invoke_m1_16731_gshared (StaticAddEvent_1_t1_2095 * __this, Object_t * ___dele, const MethodInfo* method);
#define StaticAddEvent_1_Invoke_m1_16731(__this, ___dele, method) (( void (*) (StaticAddEvent_1_t1_2095 *, Object_t *, const MethodInfo*))StaticAddEvent_1_Invoke_m1_16731_gshared)(__this, ___dele, method)
// System.IAsyncResult System.Reflection.EventInfo/StaticAddEvent`1<System.Object>::BeginInvoke(D,System.AsyncCallback,System.Object)
extern "C" Object_t * StaticAddEvent_1_BeginInvoke_m1_16732_gshared (StaticAddEvent_1_t1_2095 * __this, Object_t * ___dele, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method);
#define StaticAddEvent_1_BeginInvoke_m1_16732(__this, ___dele, ___callback, ___object, method) (( Object_t * (*) (StaticAddEvent_1_t1_2095 *, Object_t *, AsyncCallback_t1_28 *, Object_t *, const MethodInfo*))StaticAddEvent_1_BeginInvoke_m1_16732_gshared)(__this, ___dele, ___callback, ___object, method)
// System.Void System.Reflection.EventInfo/StaticAddEvent`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C" void StaticAddEvent_1_EndInvoke_m1_16733_gshared (StaticAddEvent_1_t1_2095 * __this, Object_t * ___result, const MethodInfo* method);
#define StaticAddEvent_1_EndInvoke_m1_16733(__this, ___result, method) (( void (*) (StaticAddEvent_1_t1_2095 *, Object_t *, const MethodInfo*))StaticAddEvent_1_EndInvoke_m1_16733_gshared)(__this, ___result, method)
