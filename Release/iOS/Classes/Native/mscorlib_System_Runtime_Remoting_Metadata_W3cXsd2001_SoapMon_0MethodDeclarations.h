﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapMonthDay
struct SoapMonthDay_t1_981;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime.h"

// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapMonthDay::.ctor()
extern "C" void SoapMonthDay__ctor_m1_8791 (SoapMonthDay_t1_981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapMonthDay::.ctor(System.DateTime)
extern "C" void SoapMonthDay__ctor_m1_8792 (SoapMonthDay_t1_981 * __this, DateTime_t1_150  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapMonthDay::.cctor()
extern "C" void SoapMonthDay__cctor_m1_8793 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Runtime.Remoting.Metadata.W3cXsd2001.SoapMonthDay::get_Value()
extern "C" DateTime_t1_150  SoapMonthDay_get_Value_m1_8794 (SoapMonthDay_t1_981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapMonthDay::set_Value(System.DateTime)
extern "C" void SoapMonthDay_set_Value_m1_8795 (SoapMonthDay_t1_981 * __this, DateTime_t1_150  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapMonthDay::get_XsdType()
extern "C" String_t* SoapMonthDay_get_XsdType_m1_8796 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapMonthDay::GetXsdType()
extern "C" String_t* SoapMonthDay_GetXsdType_m1_8797 (SoapMonthDay_t1_981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapMonthDay System.Runtime.Remoting.Metadata.W3cXsd2001.SoapMonthDay::Parse(System.String)
extern "C" SoapMonthDay_t1_981 * SoapMonthDay_Parse_m1_8798 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapMonthDay::ToString()
extern "C" String_t* SoapMonthDay_ToString_m1_8799 (SoapMonthDay_t1_981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
