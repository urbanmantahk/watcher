﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.Internal.iOSBillingProduct
struct iOSBillingProduct_t8_209;
// System.Collections.IDictionary
struct IDictionary_t1_35;
// VoxelBusters.NativePlugins.BillingProduct
struct BillingProduct_t8_207;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.Internal.iOSBillingProduct::.ctor(System.Collections.IDictionary)
extern "C" void iOSBillingProduct__ctor_m8_1191 (iOSBillingProduct_t8_209 * __this, Object_t * ____productJsonDict, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IDictionary VoxelBusters.NativePlugins.Internal.iOSBillingProduct::CreateJSONObject(VoxelBusters.NativePlugins.BillingProduct)
extern "C" Object_t * iOSBillingProduct_CreateJSONObject_m8_1192 (Object_t * __this /* static, unused */, BillingProduct_t8_207 * ____product, const MethodInfo* method) IL2CPP_METHOD_ATTR;
