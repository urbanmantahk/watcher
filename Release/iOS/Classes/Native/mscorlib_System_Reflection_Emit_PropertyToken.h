﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType.h"
#include "mscorlib_System_Reflection_Emit_PropertyToken.h"

// System.Reflection.Emit.PropertyToken
struct  PropertyToken_t1_549 
{
	// System.Int32 System.Reflection.Emit.PropertyToken::tokValue
	int32_t ___tokValue_0;
};
struct PropertyToken_t1_549_StaticFields{
	// System.Reflection.Emit.PropertyToken System.Reflection.Emit.PropertyToken::Empty
	PropertyToken_t1_549  ___Empty_1;
};
