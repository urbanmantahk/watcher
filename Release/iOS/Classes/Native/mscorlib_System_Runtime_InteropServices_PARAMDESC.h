﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_Runtime_InteropServices_PARAMFLAG.h"

// System.Runtime.InteropServices.PARAMDESC
struct  PARAMDESC_t1_784 
{
	// System.IntPtr System.Runtime.InteropServices.PARAMDESC::lpVarValue
	IntPtr_t ___lpVarValue_0;
	// System.Runtime.InteropServices.PARAMFLAG System.Runtime.InteropServices.PARAMDESC::wParamFlags
	int32_t ___wParamFlags_1;
};
// Native definition for marshalling of: System.Runtime.InteropServices.PARAMDESC
struct PARAMDESC_t1_784_marshaled
{
	intptr_t ___lpVarValue_0;
	int32_t ___wParamFlags_1;
};
