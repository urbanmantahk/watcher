﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.AccessControl.GenericAce
struct GenericAce_t1_1145;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_AccessControl_InheritanceFlags.h"
#include "mscorlib_System_Security_AccessControl_PropagationFlags.h"
#include "mscorlib_System_Security_AccessControl_AceType.h"
#include "mscorlib_System_Security_AccessControl_AceFlags.h"
#include "mscorlib_System_Security_AccessControl_AuditFlags.h"

// System.Void System.Security.AccessControl.GenericAce::.ctor(System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags)
extern "C" void GenericAce__ctor_m1_9898 (GenericAce_t1_1145 * __this, int32_t ___inheritanceFlags, int32_t ___propagationFlags, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.GenericAce::.ctor(System.Security.AccessControl.AceType)
extern "C" void GenericAce__ctor_m1_9899 (GenericAce_t1_1145 * __this, int32_t ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.AccessControl.AceFlags System.Security.AccessControl.GenericAce::get_AceFlags()
extern "C" uint8_t GenericAce_get_AceFlags_m1_9900 (GenericAce_t1_1145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.GenericAce::set_AceFlags(System.Security.AccessControl.AceFlags)
extern "C" void GenericAce_set_AceFlags_m1_9901 (GenericAce_t1_1145 * __this, uint8_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.AccessControl.AceType System.Security.AccessControl.GenericAce::get_AceType()
extern "C" int32_t GenericAce_get_AceType_m1_9902 (GenericAce_t1_1145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.AccessControl.AuditFlags System.Security.AccessControl.GenericAce::get_AuditFlags()
extern "C" int32_t GenericAce_get_AuditFlags_m1_9903 (GenericAce_t1_1145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.AccessControl.InheritanceFlags System.Security.AccessControl.GenericAce::get_InheritanceFlags()
extern "C" int32_t GenericAce_get_InheritanceFlags_m1_9904 (GenericAce_t1_1145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.AccessControl.GenericAce::get_IsInherited()
extern "C" bool GenericAce_get_IsInherited_m1_9905 (GenericAce_t1_1145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.AccessControl.PropagationFlags System.Security.AccessControl.GenericAce::get_PropagationFlags()
extern "C" int32_t GenericAce_get_PropagationFlags_m1_9906 (GenericAce_t1_1145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.AccessControl.GenericAce System.Security.AccessControl.GenericAce::Copy()
extern "C" GenericAce_t1_1145 * GenericAce_Copy_m1_9907 (GenericAce_t1_1145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.AccessControl.GenericAce System.Security.AccessControl.GenericAce::CreateFromBinaryForm(System.Byte[],System.Int32)
extern "C" GenericAce_t1_1145 * GenericAce_CreateFromBinaryForm_m1_9908 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___binaryForm, int32_t ___offset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.AccessControl.GenericAce::Equals(System.Object)
extern "C" bool GenericAce_Equals_m1_9909 (GenericAce_t1_1145 * __this, Object_t * ___o, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.AccessControl.GenericAce::GetHashCode()
extern "C" int32_t GenericAce_GetHashCode_m1_9910 (GenericAce_t1_1145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.AccessControl.GenericAce::op_Equality(System.Security.AccessControl.GenericAce,System.Security.AccessControl.GenericAce)
extern "C" bool GenericAce_op_Equality_m1_9911 (Object_t * __this /* static, unused */, GenericAce_t1_1145 * ___left, GenericAce_t1_1145 * ___right, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.AccessControl.GenericAce::op_Inequality(System.Security.AccessControl.GenericAce,System.Security.AccessControl.GenericAce)
extern "C" bool GenericAce_op_Inequality_m1_9912 (Object_t * __this /* static, unused */, GenericAce_t1_1145 * ___left, GenericAce_t1_1145 * ___right, const MethodInfo* method) IL2CPP_METHOD_ATTR;
