﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType.h"

// System.Threading.LockCookie
struct  LockCookie_t1_1466 
{
	// System.Int32 System.Threading.LockCookie::ThreadId
	int32_t ___ThreadId_0;
	// System.Int32 System.Threading.LockCookie::ReaderLocks
	int32_t ___ReaderLocks_1;
	// System.Int32 System.Threading.LockCookie::WriterLocks
	int32_t ___WriterLocks_2;
};
