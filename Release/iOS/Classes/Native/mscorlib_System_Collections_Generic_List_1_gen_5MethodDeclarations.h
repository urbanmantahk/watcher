﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_45MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<System.Net.Cookie>::.ctor()
#define List_1__ctor_m1_14928(__this, method) (( void (*) (List_1_t1_1811 *, const MethodInfo*))List_1__ctor_m1_15034_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Net.Cookie>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m1_18102(__this, ___collection, method) (( void (*) (List_1_t1_1811 *, Object_t*, const MethodInfo*))List_1__ctor_m1_15095_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Net.Cookie>::.ctor(System.Int32)
#define List_1__ctor_m1_18103(__this, ___capacity, method) (( void (*) (List_1_t1_1811 *, int32_t, const MethodInfo*))List_1__ctor_m1_15097_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<System.Net.Cookie>::.ctor(T[],System.Int32)
#define List_1__ctor_m1_18104(__this, ___data, ___size, method) (( void (*) (List_1_t1_1811 *, CookieU5BU5D_t3_266*, int32_t, const MethodInfo*))List_1__ctor_m1_15099_gshared)(__this, ___data, ___size, method)
// System.Void System.Collections.Generic.List`1<System.Net.Cookie>::.cctor()
#define List_1__cctor_m1_18105(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m1_15101_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Net.Cookie>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_18106(__this, method) (( Object_t* (*) (List_1_t1_1811 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_15103_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Net.Cookie>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m1_18107(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_1811 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m1_15105_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Net.Cookie>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1_18108(__this, method) (( Object_t * (*) (List_1_t1_1811 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1_15107_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Net.Cookie>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m1_18109(__this, ___item, method) (( int32_t (*) (List_1_t1_1811 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m1_15109_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Net.Cookie>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m1_18110(__this, ___item, method) (( bool (*) (List_1_t1_1811 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1_15111_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Net.Cookie>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m1_18111(__this, ___item, method) (( int32_t (*) (List_1_t1_1811 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m1_15113_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Net.Cookie>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m1_18112(__this, ___index, ___item, method) (( void (*) (List_1_t1_1811 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m1_15115_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Net.Cookie>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m1_18113(__this, ___item, method) (( void (*) (List_1_t1_1811 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1_15117_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Net.Cookie>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_18114(__this, method) (( bool (*) (List_1_t1_1811 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_15119_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Net.Cookie>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m1_18115(__this, method) (( bool (*) (List_1_t1_1811 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m1_15121_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Net.Cookie>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m1_18116(__this, method) (( Object_t * (*) (List_1_t1_1811 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1_15123_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Net.Cookie>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m1_18117(__this, method) (( bool (*) (List_1_t1_1811 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m1_15125_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Net.Cookie>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m1_18118(__this, method) (( bool (*) (List_1_t1_1811 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m1_15127_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Net.Cookie>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m1_18119(__this, ___index, method) (( Object_t * (*) (List_1_t1_1811 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m1_15129_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Net.Cookie>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m1_18120(__this, ___index, ___value, method) (( void (*) (List_1_t1_1811 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m1_15131_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<System.Net.Cookie>::Add(T)
#define List_1_Add_m1_18121(__this, ___item, method) (( void (*) (List_1_t1_1811 *, Cookie_t3_81 *, const MethodInfo*))List_1_Add_m1_15133_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Net.Cookie>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m1_18122(__this, ___newCount, method) (( void (*) (List_1_t1_1811 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m1_15135_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<System.Net.Cookie>::CheckRange(System.Int32,System.Int32)
#define List_1_CheckRange_m1_18123(__this, ___idx, ___count, method) (( void (*) (List_1_t1_1811 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m1_15137_gshared)(__this, ___idx, ___count, method)
// System.Void System.Collections.Generic.List`1<System.Net.Cookie>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m1_18124(__this, ___collection, method) (( void (*) (List_1_t1_1811 *, Object_t*, const MethodInfo*))List_1_AddCollection_m1_15139_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Net.Cookie>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m1_18125(__this, ___enumerable, method) (( void (*) (List_1_t1_1811 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m1_15141_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<System.Net.Cookie>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m1_18126(__this, ___collection, method) (( void (*) (List_1_t1_1811 *, Object_t*, const MethodInfo*))List_1_AddRange_m1_15143_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Net.Cookie>::AsReadOnly()
#define List_1_AsReadOnly_m1_18127(__this, method) (( ReadOnlyCollection_1_t1_2216 * (*) (List_1_t1_1811 *, const MethodInfo*))List_1_AsReadOnly_m1_15145_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Net.Cookie>::BinarySearch(T)
#define List_1_BinarySearch_m1_18128(__this, ___item, method) (( int32_t (*) (List_1_t1_1811 *, Cookie_t3_81 *, const MethodInfo*))List_1_BinarySearch_m1_15147_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Net.Cookie>::BinarySearch(T,System.Collections.Generic.IComparer`1<T>)
#define List_1_BinarySearch_m1_18129(__this, ___item, ___comparer, method) (( int32_t (*) (List_1_t1_1811 *, Cookie_t3_81 *, Object_t*, const MethodInfo*))List_1_BinarySearch_m1_15149_gshared)(__this, ___item, ___comparer, method)
// System.Int32 System.Collections.Generic.List`1<System.Net.Cookie>::BinarySearch(System.Int32,System.Int32,T,System.Collections.Generic.IComparer`1<T>)
#define List_1_BinarySearch_m1_18130(__this, ___index, ___count, ___item, ___comparer, method) (( int32_t (*) (List_1_t1_1811 *, int32_t, int32_t, Cookie_t3_81 *, Object_t*, const MethodInfo*))List_1_BinarySearch_m1_15151_gshared)(__this, ___index, ___count, ___item, ___comparer, method)
// System.Void System.Collections.Generic.List`1<System.Net.Cookie>::Clear()
#define List_1_Clear_m1_18131(__this, method) (( void (*) (List_1_t1_1811 *, const MethodInfo*))List_1_Clear_m1_15153_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Net.Cookie>::Contains(T)
#define List_1_Contains_m1_18132(__this, ___item, method) (( bool (*) (List_1_t1_1811 *, Cookie_t3_81 *, const MethodInfo*))List_1_Contains_m1_15155_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Net.Cookie>::CopyTo(T[])
#define List_1_CopyTo_m1_18133(__this, ___array, method) (( void (*) (List_1_t1_1811 *, CookieU5BU5D_t3_266*, const MethodInfo*))List_1_CopyTo_m1_15157_gshared)(__this, ___array, method)
// System.Void System.Collections.Generic.List`1<System.Net.Cookie>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m1_18134(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_1811 *, CookieU5BU5D_t3_266*, int32_t, const MethodInfo*))List_1_CopyTo_m1_15159_gshared)(__this, ___array, ___arrayIndex, method)
// System.Void System.Collections.Generic.List`1<System.Net.Cookie>::CopyTo(System.Int32,T[],System.Int32,System.Int32)
#define List_1_CopyTo_m1_18135(__this, ___index, ___array, ___arrayIndex, ___count, method) (( void (*) (List_1_t1_1811 *, int32_t, CookieU5BU5D_t3_266*, int32_t, int32_t, const MethodInfo*))List_1_CopyTo_m1_15161_gshared)(__this, ___index, ___array, ___arrayIndex, ___count, method)
// System.Boolean System.Collections.Generic.List`1<System.Net.Cookie>::Exists(System.Predicate`1<T>)
#define List_1_Exists_m1_18136(__this, ___match, method) (( bool (*) (List_1_t1_1811 *, Predicate_1_t1_2217 *, const MethodInfo*))List_1_Exists_m1_15163_gshared)(__this, ___match, method)
// T System.Collections.Generic.List`1<System.Net.Cookie>::Find(System.Predicate`1<T>)
#define List_1_Find_m1_18137(__this, ___match, method) (( Cookie_t3_81 * (*) (List_1_t1_1811 *, Predicate_1_t1_2217 *, const MethodInfo*))List_1_Find_m1_15165_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Net.Cookie>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m1_18138(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t1_2217 *, const MethodInfo*))List_1_CheckMatch_m1_15167_gshared)(__this /* static, unused */, ___match, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<System.Net.Cookie>::FindAll(System.Predicate`1<T>)
#define List_1_FindAll_m1_18139(__this, ___match, method) (( List_1_t1_1811 * (*) (List_1_t1_1811 *, Predicate_1_t1_2217 *, const MethodInfo*))List_1_FindAll_m1_15169_gshared)(__this, ___match, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<System.Net.Cookie>::FindAllStackBits(System.Predicate`1<T>)
#define List_1_FindAllStackBits_m1_18140(__this, ___match, method) (( List_1_t1_1811 * (*) (List_1_t1_1811 *, Predicate_1_t1_2217 *, const MethodInfo*))List_1_FindAllStackBits_m1_15171_gshared)(__this, ___match, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<System.Net.Cookie>::FindAllList(System.Predicate`1<T>)
#define List_1_FindAllList_m1_18141(__this, ___match, method) (( List_1_t1_1811 * (*) (List_1_t1_1811 *, Predicate_1_t1_2217 *, const MethodInfo*))List_1_FindAllList_m1_15173_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Net.Cookie>::FindIndex(System.Predicate`1<T>)
#define List_1_FindIndex_m1_18142(__this, ___match, method) (( int32_t (*) (List_1_t1_1811 *, Predicate_1_t1_2217 *, const MethodInfo*))List_1_FindIndex_m1_15175_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Net.Cookie>::FindIndex(System.Int32,System.Predicate`1<T>)
#define List_1_FindIndex_m1_18143(__this, ___startIndex, ___match, method) (( int32_t (*) (List_1_t1_1811 *, int32_t, Predicate_1_t1_2217 *, const MethodInfo*))List_1_FindIndex_m1_15177_gshared)(__this, ___startIndex, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Net.Cookie>::FindIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_FindIndex_m1_18144(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_1811 *, int32_t, int32_t, Predicate_1_t1_2217 *, const MethodInfo*))List_1_FindIndex_m1_15179_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Net.Cookie>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m1_18145(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_1811 *, int32_t, int32_t, Predicate_1_t1_2217 *, const MethodInfo*))List_1_GetIndex_m1_15181_gshared)(__this, ___startIndex, ___count, ___match, method)
// T System.Collections.Generic.List`1<System.Net.Cookie>::FindLast(System.Predicate`1<T>)
#define List_1_FindLast_m1_18146(__this, ___match, method) (( Cookie_t3_81 * (*) (List_1_t1_1811 *, Predicate_1_t1_2217 *, const MethodInfo*))List_1_FindLast_m1_15183_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Net.Cookie>::FindLastIndex(System.Predicate`1<T>)
#define List_1_FindLastIndex_m1_18147(__this, ___match, method) (( int32_t (*) (List_1_t1_1811 *, Predicate_1_t1_2217 *, const MethodInfo*))List_1_FindLastIndex_m1_15185_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Net.Cookie>::FindLastIndex(System.Int32,System.Predicate`1<T>)
#define List_1_FindLastIndex_m1_18148(__this, ___startIndex, ___match, method) (( int32_t (*) (List_1_t1_1811 *, int32_t, Predicate_1_t1_2217 *, const MethodInfo*))List_1_FindLastIndex_m1_15187_gshared)(__this, ___startIndex, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Net.Cookie>::FindLastIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_FindLastIndex_m1_18149(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_1811 *, int32_t, int32_t, Predicate_1_t1_2217 *, const MethodInfo*))List_1_FindLastIndex_m1_15189_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Net.Cookie>::GetLastIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetLastIndex_m1_18150(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_1811 *, int32_t, int32_t, Predicate_1_t1_2217 *, const MethodInfo*))List_1_GetLastIndex_m1_15191_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Net.Cookie>::ForEach(System.Action`1<T>)
#define List_1_ForEach_m1_18151(__this, ___action, method) (( void (*) (List_1_t1_1811 *, Action_1_t1_2218 *, const MethodInfo*))List_1_ForEach_m1_15193_gshared)(__this, ___action, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Net.Cookie>::GetEnumerator()
#define List_1_GetEnumerator_m1_14929(__this, method) (( Enumerator_t1_1814  (*) (List_1_t1_1811 *, const MethodInfo*))List_1_GetEnumerator_m1_15195_gshared)(__this, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<System.Net.Cookie>::GetRange(System.Int32,System.Int32)
#define List_1_GetRange_m1_18152(__this, ___index, ___count, method) (( List_1_t1_1811 * (*) (List_1_t1_1811 *, int32_t, int32_t, const MethodInfo*))List_1_GetRange_m1_15197_gshared)(__this, ___index, ___count, method)
// System.Int32 System.Collections.Generic.List`1<System.Net.Cookie>::IndexOf(T)
#define List_1_IndexOf_m1_18153(__this, ___item, method) (( int32_t (*) (List_1_t1_1811 *, Cookie_t3_81 *, const MethodInfo*))List_1_IndexOf_m1_15199_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Net.Cookie>::IndexOf(T,System.Int32)
#define List_1_IndexOf_m1_18154(__this, ___item, ___index, method) (( int32_t (*) (List_1_t1_1811 *, Cookie_t3_81 *, int32_t, const MethodInfo*))List_1_IndexOf_m1_15201_gshared)(__this, ___item, ___index, method)
// System.Int32 System.Collections.Generic.List`1<System.Net.Cookie>::IndexOf(T,System.Int32,System.Int32)
#define List_1_IndexOf_m1_18155(__this, ___item, ___index, ___count, method) (( int32_t (*) (List_1_t1_1811 *, Cookie_t3_81 *, int32_t, int32_t, const MethodInfo*))List_1_IndexOf_m1_15203_gshared)(__this, ___item, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<System.Net.Cookie>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m1_18156(__this, ___start, ___delta, method) (( void (*) (List_1_t1_1811 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m1_15205_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<System.Net.Cookie>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m1_18157(__this, ___index, method) (( void (*) (List_1_t1_1811 *, int32_t, const MethodInfo*))List_1_CheckIndex_m1_15207_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Net.Cookie>::Insert(System.Int32,T)
#define List_1_Insert_m1_18158(__this, ___index, ___item, method) (( void (*) (List_1_t1_1811 *, int32_t, Cookie_t3_81 *, const MethodInfo*))List_1_Insert_m1_15209_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Net.Cookie>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m1_18159(__this, ___collection, method) (( void (*) (List_1_t1_1811 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m1_15211_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Net.Cookie>::InsertRange(System.Int32,System.Collections.Generic.IEnumerable`1<T>)
#define List_1_InsertRange_m1_18160(__this, ___index, ___collection, method) (( void (*) (List_1_t1_1811 *, int32_t, Object_t*, const MethodInfo*))List_1_InsertRange_m1_15213_gshared)(__this, ___index, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Net.Cookie>::InsertCollection(System.Int32,System.Collections.Generic.ICollection`1<T>)
#define List_1_InsertCollection_m1_18161(__this, ___index, ___collection, method) (( void (*) (List_1_t1_1811 *, int32_t, Object_t*, const MethodInfo*))List_1_InsertCollection_m1_15215_gshared)(__this, ___index, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Net.Cookie>::InsertEnumeration(System.Int32,System.Collections.Generic.IEnumerable`1<T>)
#define List_1_InsertEnumeration_m1_18162(__this, ___index, ___enumerable, method) (( void (*) (List_1_t1_1811 *, int32_t, Object_t*, const MethodInfo*))List_1_InsertEnumeration_m1_15217_gshared)(__this, ___index, ___enumerable, method)
// System.Int32 System.Collections.Generic.List`1<System.Net.Cookie>::LastIndexOf(T)
#define List_1_LastIndexOf_m1_18163(__this, ___item, method) (( int32_t (*) (List_1_t1_1811 *, Cookie_t3_81 *, const MethodInfo*))List_1_LastIndexOf_m1_15219_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Net.Cookie>::LastIndexOf(T,System.Int32)
#define List_1_LastIndexOf_m1_18164(__this, ___item, ___index, method) (( int32_t (*) (List_1_t1_1811 *, Cookie_t3_81 *, int32_t, const MethodInfo*))List_1_LastIndexOf_m1_15221_gshared)(__this, ___item, ___index, method)
// System.Int32 System.Collections.Generic.List`1<System.Net.Cookie>::LastIndexOf(T,System.Int32,System.Int32)
#define List_1_LastIndexOf_m1_18165(__this, ___item, ___index, ___count, method) (( int32_t (*) (List_1_t1_1811 *, Cookie_t3_81 *, int32_t, int32_t, const MethodInfo*))List_1_LastIndexOf_m1_15223_gshared)(__this, ___item, ___index, ___count, method)
// System.Boolean System.Collections.Generic.List`1<System.Net.Cookie>::Remove(T)
#define List_1_Remove_m1_18166(__this, ___item, method) (( bool (*) (List_1_t1_1811 *, Cookie_t3_81 *, const MethodInfo*))List_1_Remove_m1_15225_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Net.Cookie>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m1_18167(__this, ___match, method) (( int32_t (*) (List_1_t1_1811 *, Predicate_1_t1_2217 *, const MethodInfo*))List_1_RemoveAll_m1_15227_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Net.Cookie>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m1_18168(__this, ___index, method) (( void (*) (List_1_t1_1811 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1_15229_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Net.Cookie>::RemoveRange(System.Int32,System.Int32)
#define List_1_RemoveRange_m1_18169(__this, ___index, ___count, method) (( void (*) (List_1_t1_1811 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m1_15231_gshared)(__this, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<System.Net.Cookie>::Reverse()
#define List_1_Reverse_m1_18170(__this, method) (( void (*) (List_1_t1_1811 *, const MethodInfo*))List_1_Reverse_m1_15233_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Net.Cookie>::Reverse(System.Int32,System.Int32)
#define List_1_Reverse_m1_18171(__this, ___index, ___count, method) (( void (*) (List_1_t1_1811 *, int32_t, int32_t, const MethodInfo*))List_1_Reverse_m1_15235_gshared)(__this, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<System.Net.Cookie>::Sort()
#define List_1_Sort_m1_18172(__this, method) (( void (*) (List_1_t1_1811 *, const MethodInfo*))List_1_Sort_m1_15237_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Net.Cookie>::Sort(System.Collections.Generic.IComparer`1<T>)
#define List_1_Sort_m1_14930(__this, ___comparer, method) (( void (*) (List_1_t1_1811 *, Object_t*, const MethodInfo*))List_1_Sort_m1_15239_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.List`1<System.Net.Cookie>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m1_18173(__this, ___comparison, method) (( void (*) (List_1_t1_1811 *, Comparison_1_t1_2219 *, const MethodInfo*))List_1_Sort_m1_15241_gshared)(__this, ___comparison, method)
// System.Void System.Collections.Generic.List`1<System.Net.Cookie>::Sort(System.Int32,System.Int32,System.Collections.Generic.IComparer`1<T>)
#define List_1_Sort_m1_18174(__this, ___index, ___count, ___comparer, method) (( void (*) (List_1_t1_1811 *, int32_t, int32_t, Object_t*, const MethodInfo*))List_1_Sort_m1_15243_gshared)(__this, ___index, ___count, ___comparer, method)
// T[] System.Collections.Generic.List`1<System.Net.Cookie>::ToArray()
#define List_1_ToArray_m1_18175(__this, method) (( CookieU5BU5D_t3_266* (*) (List_1_t1_1811 *, const MethodInfo*))List_1_ToArray_m1_15035_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Net.Cookie>::TrimExcess()
#define List_1_TrimExcess_m1_18176(__this, method) (( void (*) (List_1_t1_1811 *, const MethodInfo*))List_1_TrimExcess_m1_15245_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Net.Cookie>::TrueForAll(System.Predicate`1<T>)
#define List_1_TrueForAll_m1_18177(__this, ___match, method) (( bool (*) (List_1_t1_1811 *, Predicate_1_t1_2217 *, const MethodInfo*))List_1_TrueForAll_m1_15247_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Net.Cookie>::get_Capacity()
#define List_1_get_Capacity_m1_18178(__this, method) (( int32_t (*) (List_1_t1_1811 *, const MethodInfo*))List_1_get_Capacity_m1_15249_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Net.Cookie>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m1_18179(__this, ___value, method) (( void (*) (List_1_t1_1811 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1_15251_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<System.Net.Cookie>::get_Count()
#define List_1_get_Count_m1_18180(__this, method) (( int32_t (*) (List_1_t1_1811 *, const MethodInfo*))List_1_get_Count_m1_15253_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Net.Cookie>::get_Item(System.Int32)
#define List_1_get_Item_m1_18181(__this, ___index, method) (( Cookie_t3_81 * (*) (List_1_t1_1811 *, int32_t, const MethodInfo*))List_1_get_Item_m1_15255_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Net.Cookie>::set_Item(System.Int32,T)
#define List_1_set_Item_m1_18182(__this, ___index, ___value, method) (( void (*) (List_1_t1_1811 *, int32_t, Cookie_t3_81 *, const MethodInfo*))List_1_set_Item_m1_15257_gshared)(__this, ___index, ___value, method)
