﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "System_Xml_Mono_Xml_Schema_XsdAnySimpleType.h"

// Mono.Xml.Schema.XsdDate
struct  XsdDate_t4_46  : public XsdAnySimpleType_t4_3
{
};
