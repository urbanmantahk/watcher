﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VoxelBusters.Utility.UnityGUI.MENU.GUISubMenu[]
struct GUISubMenuU5BU5D_t8_142;
// VoxelBusters.Utility.UnityGUI.MENU.GUISubMenu
struct GUISubMenu_t8_143;

#include "AssemblyU2DCSharp_VoxelBusters_Utility_UnityGUI_MENU_GUIMenu.h"

// VoxelBusters.Utility.UnityGUI.MENU.GUIMainMenu
struct  GUIMainMenu_t8_140  : public GUIMenuBase_t8_141
{
	// VoxelBusters.Utility.UnityGUI.MENU.GUISubMenu[] VoxelBusters.Utility.UnityGUI.MENU.GUIMainMenu::m_subMenuList
	GUISubMenuU5BU5D_t8_142* ___m_subMenuList_10;
	// VoxelBusters.Utility.UnityGUI.MENU.GUISubMenu VoxelBusters.Utility.UnityGUI.MENU.GUIMainMenu::m_activeSubMenu
	GUISubMenu_t8_143 * ___m_activeSubMenu_11;
	// System.Boolean VoxelBusters.Utility.UnityGUI.MENU.GUIMainMenu::m_showingMainMenu
	bool ___m_showingMainMenu_12;
};
