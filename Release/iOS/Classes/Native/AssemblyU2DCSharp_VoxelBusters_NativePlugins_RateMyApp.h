﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1_238;
// VoxelBusters.NativePlugins.RateMyApp/Settings
struct Settings_t8_310;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// VoxelBusters.NativePlugins.RateMyApp
struct  RateMyApp_t8_307  : public MonoBehaviour_t6_91
{
	// System.String[] VoxelBusters.NativePlugins.RateMyApp::m_buttonList
	StringU5BU5D_t1_238* ___m_buttonList_7;
	// VoxelBusters.NativePlugins.RateMyApp/Settings VoxelBusters.NativePlugins.RateMyApp::m_rateMyAppSettings
	Settings_t8_310 * ___m_rateMyAppSettings_8;
};
