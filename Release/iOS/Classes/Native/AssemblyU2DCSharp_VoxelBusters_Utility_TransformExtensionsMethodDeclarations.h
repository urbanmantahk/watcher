﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_t6_65;

#include "codegen/il2cpp-codegen.h"

// System.String VoxelBusters.Utility.TransformExtensions::GetPath(UnityEngine.Transform)
extern "C" String_t* TransformExtensions_GetPath_m8_244 (Object_t * __this /* static, unused */, Transform_t6_65 * ____transform, const MethodInfo* method) IL2CPP_METHOD_ATTR;
