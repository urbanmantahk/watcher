﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapDuration
struct SoapDuration_t1_970;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_TimeSpan.h"

// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapDuration::.ctor()
extern "C" void SoapDuration__ctor_m1_8708 (SoapDuration_t1_970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapDuration::get_XsdType()
extern "C" String_t* SoapDuration_get_XsdType_m1_8709 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.TimeSpan System.Runtime.Remoting.Metadata.W3cXsd2001.SoapDuration::Parse(System.String)
extern "C" TimeSpan_t1_368  SoapDuration_Parse_m1_8710 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapDuration::ToString(System.TimeSpan)
extern "C" String_t* SoapDuration_ToString_m1_8711 (Object_t * __this /* static, unused */, TimeSpan_t1_368  ___timeSpan, const MethodInfo* method) IL2CPP_METHOD_ATTR;
