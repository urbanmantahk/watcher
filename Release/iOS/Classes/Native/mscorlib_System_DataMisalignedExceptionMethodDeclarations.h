﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.DataMisalignedException
struct DataMisalignedException_t1_1524;
// System.String
struct String_t;
// System.Exception
struct Exception_t1_33;

#include "codegen/il2cpp-codegen.h"

// System.Void System.DataMisalignedException::.ctor()
extern "C" void DataMisalignedException__ctor_m1_13781 (DataMisalignedException_t1_1524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.DataMisalignedException::.ctor(System.String)
extern "C" void DataMisalignedException__ctor_m1_13782 (DataMisalignedException_t1_1524 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.DataMisalignedException::.ctor(System.String,System.Exception)
extern "C" void DataMisalignedException__ctor_m1_13783 (DataMisalignedException_t1_1524 * __this, String_t* ___message, Exception_t1_33 * ___innerException, const MethodInfo* method) IL2CPP_METHOD_ATTR;
