﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_179.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTag.h"

// System.Void System.Array/InternalEnumerator`1<ExifLibrary.ExifTag>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_26438_gshared (InternalEnumerator_1_t1_2667 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1_26438(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1_2667 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m1_26438_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<ExifLibrary.ExifTag>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_26439_gshared (InternalEnumerator_1_t1_2667 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_26439(__this, method) (( void (*) (InternalEnumerator_1_t1_2667 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_26439_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<ExifLibrary.ExifTag>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_26440_gshared (InternalEnumerator_1_t1_2667 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_26440(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1_2667 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_26440_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<ExifLibrary.ExifTag>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_26441_gshared (InternalEnumerator_1_t1_2667 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1_26441(__this, method) (( void (*) (InternalEnumerator_1_t1_2667 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1_26441_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<ExifLibrary.ExifTag>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_26442_gshared (InternalEnumerator_1_t1_2667 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1_26442(__this, method) (( bool (*) (InternalEnumerator_1_t1_2667 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1_26442_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<ExifLibrary.ExifTag>::get_Current()
extern "C" int32_t InternalEnumerator_1_get_Current_m1_26443_gshared (InternalEnumerator_1_t1_2667 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1_26443(__this, method) (( int32_t (*) (InternalEnumerator_1_t1_2667 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1_26443_gshared)(__this, method)
