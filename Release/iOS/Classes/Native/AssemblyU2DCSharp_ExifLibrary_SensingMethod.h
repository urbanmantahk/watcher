﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "AssemblyU2DCSharp_ExifLibrary_SensingMethod.h"

// ExifLibrary.SensingMethod
struct  SensingMethod_t8_74 
{
	// System.UInt16 ExifLibrary.SensingMethod::value__
	uint16_t ___value___1;
};
