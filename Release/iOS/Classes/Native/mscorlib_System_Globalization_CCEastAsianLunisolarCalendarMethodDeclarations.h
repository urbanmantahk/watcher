﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Globalization.CCEastAsianLunisolarCalendar
struct CCEastAsianLunisolarCalendar_t1_351;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime.h"

// System.Void System.Globalization.CCEastAsianLunisolarCalendar::.ctor()
extern "C" void CCEastAsianLunisolarCalendar__ctor_m1_3779 (CCEastAsianLunisolarCalendar_t1_351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCEastAsianLunisolarCalendar::fixed_from_dmy(System.Int32,System.Int32,System.Int32)
extern "C" int32_t CCEastAsianLunisolarCalendar_fixed_from_dmy_m1_3780 (Object_t * __this /* static, unused */, int32_t ___day, int32_t ___month, int32_t ___year, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCEastAsianLunisolarCalendar::year_from_fixed(System.Int32)
extern "C" int32_t CCEastAsianLunisolarCalendar_year_from_fixed_m1_3781 (Object_t * __this /* static, unused */, int32_t ___date, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.CCEastAsianLunisolarCalendar::my_from_fixed(System.Int32&,System.Int32&,System.Int32)
extern "C" void CCEastAsianLunisolarCalendar_my_from_fixed_m1_3782 (Object_t * __this /* static, unused */, int32_t* ___month, int32_t* ___year, int32_t ___date, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.CCEastAsianLunisolarCalendar::dmy_from_fixed(System.Int32&,System.Int32&,System.Int32&,System.Int32)
extern "C" void CCEastAsianLunisolarCalendar_dmy_from_fixed_m1_3783 (Object_t * __this /* static, unused */, int32_t* ___day, int32_t* ___month, int32_t* ___year, int32_t ___date, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.CCEastAsianLunisolarCalendar::AddMonths(System.DateTime,System.Int32)
extern "C" DateTime_t1_150  CCEastAsianLunisolarCalendar_AddMonths_m1_3784 (Object_t * __this /* static, unused */, DateTime_t1_150  ___date, int32_t ___months, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.CCEastAsianLunisolarCalendar::AddYears(System.DateTime,System.Int32)
extern "C" DateTime_t1_150  CCEastAsianLunisolarCalendar_AddYears_m1_3785 (Object_t * __this /* static, unused */, DateTime_t1_150  ___date, int32_t ___years, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCEastAsianLunisolarCalendar::GetDayOfMonth(System.DateTime)
extern "C" int32_t CCEastAsianLunisolarCalendar_GetDayOfMonth_m1_3786 (Object_t * __this /* static, unused */, DateTime_t1_150  ___date, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCEastAsianLunisolarCalendar::GetDayOfYear(System.DateTime)
extern "C" int32_t CCEastAsianLunisolarCalendar_GetDayOfYear_m1_3787 (Object_t * __this /* static, unused */, DateTime_t1_150  ___date, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCEastAsianLunisolarCalendar::GetDaysInMonth(System.Int32,System.Int32)
extern "C" int32_t CCEastAsianLunisolarCalendar_GetDaysInMonth_m1_3788 (Object_t * __this /* static, unused */, int32_t ___gyear, int32_t ___month, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCEastAsianLunisolarCalendar::GetDaysInYear(System.Int32)
extern "C" int32_t CCEastAsianLunisolarCalendar_GetDaysInYear_m1_3789 (Object_t * __this /* static, unused */, int32_t ___year, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCEastAsianLunisolarCalendar::GetMonth(System.DateTime)
extern "C" int32_t CCEastAsianLunisolarCalendar_GetMonth_m1_3790 (Object_t * __this /* static, unused */, DateTime_t1_150  ___date, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.CCEastAsianLunisolarCalendar::IsLeapMonth(System.Int32,System.Int32)
extern "C" bool CCEastAsianLunisolarCalendar_IsLeapMonth_m1_3791 (Object_t * __this /* static, unused */, int32_t ___gyear, int32_t ___month, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.CCEastAsianLunisolarCalendar::IsLeapYear(System.Int32)
extern "C" bool CCEastAsianLunisolarCalendar_IsLeapYear_m1_3792 (Object_t * __this /* static, unused */, int32_t ___gyear, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.CCEastAsianLunisolarCalendar::ToDateTime(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" DateTime_t1_150  CCEastAsianLunisolarCalendar_ToDateTime_m1_3793 (Object_t * __this /* static, unused */, int32_t ___year, int32_t ___month, int32_t ___day, int32_t ___hour, int32_t ___minute, int32_t ___second, int32_t ___millisecond, const MethodInfo* method) IL2CPP_METHOD_ATTR;
