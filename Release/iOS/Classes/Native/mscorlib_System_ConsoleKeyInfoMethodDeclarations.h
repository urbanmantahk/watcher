﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_ConsoleKeyInfo.h"
#include "mscorlib_System_ConsoleKey.h"
#include "mscorlib_System_ConsoleModifiers.h"

// System.Void System.ConsoleKeyInfo::.ctor(System.Char,System.ConsoleKey,System.Boolean,System.Boolean,System.Boolean)
extern "C" void ConsoleKeyInfo__ctor_m1_13416 (ConsoleKeyInfo_t1_1516 * __this, uint16_t ___keyChar, int32_t ___key, bool ___shift, bool ___alt, bool ___control, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ConsoleKeyInfo::.ctor(System.ConsoleKeyInfo)
extern "C" void ConsoleKeyInfo__ctor_m1_13417 (ConsoleKeyInfo_t1_1516 * __this, ConsoleKeyInfo_t1_1516  ___other, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ConsoleKeyInfo::.cctor()
extern "C" void ConsoleKeyInfo__cctor_m1_13418 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ConsoleKeyInfo::SetKey(System.ConsoleKey)
extern "C" void ConsoleKeyInfo_SetKey_m1_13419 (ConsoleKeyInfo_t1_1516 * __this, int32_t ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ConsoleKeyInfo::SetKeyChar(System.Char)
extern "C" void ConsoleKeyInfo_SetKeyChar_m1_13420 (ConsoleKeyInfo_t1_1516 * __this, uint16_t ___keyChar, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ConsoleKeyInfo::SetModifiers(System.Boolean,System.Boolean,System.Boolean)
extern "C" void ConsoleKeyInfo_SetModifiers_m1_13421 (ConsoleKeyInfo_t1_1516 * __this, bool ___shift, bool ___alt, bool ___control, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ConsoleKey System.ConsoleKeyInfo::get_Key()
extern "C" int32_t ConsoleKeyInfo_get_Key_m1_13422 (ConsoleKeyInfo_t1_1516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char System.ConsoleKeyInfo::get_KeyChar()
extern "C" uint16_t ConsoleKeyInfo_get_KeyChar_m1_13423 (ConsoleKeyInfo_t1_1516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ConsoleModifiers System.ConsoleKeyInfo::get_Modifiers()
extern "C" int32_t ConsoleKeyInfo_get_Modifiers_m1_13424 (ConsoleKeyInfo_t1_1516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ConsoleKeyInfo::Equals(System.Object)
extern "C" bool ConsoleKeyInfo_Equals_m1_13425 (ConsoleKeyInfo_t1_1516 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ConsoleKeyInfo::Equals(System.ConsoleKeyInfo)
extern "C" bool ConsoleKeyInfo_Equals_m1_13426 (ConsoleKeyInfo_t1_1516 * __this, ConsoleKeyInfo_t1_1516  ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.ConsoleKeyInfo::GetHashCode()
extern "C" int32_t ConsoleKeyInfo_GetHashCode_m1_13427 (ConsoleKeyInfo_t1_1516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ConsoleKeyInfo::op_Equality(System.ConsoleKeyInfo,System.ConsoleKeyInfo)
extern "C" bool ConsoleKeyInfo_op_Equality_m1_13428 (Object_t * __this /* static, unused */, ConsoleKeyInfo_t1_1516  ___a, ConsoleKeyInfo_t1_1516  ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ConsoleKeyInfo::op_Inequality(System.ConsoleKeyInfo,System.ConsoleKeyInfo)
extern "C" bool ConsoleKeyInfo_op_Inequality_m1_13429 (Object_t * __this /* static, unused */, ConsoleKeyInfo_t1_1516  ___a, ConsoleKeyInfo_t1_1516  ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void ConsoleKeyInfo_t1_1516_marshal(const ConsoleKeyInfo_t1_1516& unmarshaled, ConsoleKeyInfo_t1_1516_marshaled& marshaled);
extern "C" void ConsoleKeyInfo_t1_1516_marshal_back(const ConsoleKeyInfo_t1_1516_marshaled& marshaled, ConsoleKeyInfo_t1_1516& unmarshaled);
extern "C" void ConsoleKeyInfo_t1_1516_marshal_cleanup(ConsoleKeyInfo_t1_1516_marshaled& marshaled);
