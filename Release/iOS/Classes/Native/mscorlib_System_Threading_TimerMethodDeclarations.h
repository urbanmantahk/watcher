﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Threading.Timer
struct Timer_t1_910;
// System.Threading.TimerCallback
struct TimerCallback_t1_1488;
// System.Object
struct Object_t;
// System.Threading.WaitHandle
struct WaitHandle_t1_917;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_TimeSpan.h"

// System.Void System.Threading.Timer::.ctor(System.Threading.TimerCallback,System.Object,System.Int32,System.Int32)
extern "C" void Timer__ctor_m1_12950 (Timer_t1_910 * __this, TimerCallback_t1_1488 * ___callback, Object_t * ___state, int32_t ___dueTime, int32_t ___period, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Timer::.ctor(System.Threading.TimerCallback,System.Object,System.Int64,System.Int64)
extern "C" void Timer__ctor_m1_12951 (Timer_t1_910 * __this, TimerCallback_t1_1488 * ___callback, Object_t * ___state, int64_t ___dueTime, int64_t ___period, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Timer::.ctor(System.Threading.TimerCallback,System.Object,System.TimeSpan,System.TimeSpan)
extern "C" void Timer__ctor_m1_12952 (Timer_t1_910 * __this, TimerCallback_t1_1488 * ___callback, Object_t * ___state, TimeSpan_t1_368  ___dueTime, TimeSpan_t1_368  ___period, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Timer::.ctor(System.Threading.TimerCallback,System.Object,System.UInt32,System.UInt32)
extern "C" void Timer__ctor_m1_12953 (Timer_t1_910 * __this, TimerCallback_t1_1488 * ___callback, Object_t * ___state, uint32_t ___dueTime, uint32_t ___period, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Timer::.ctor(System.Threading.TimerCallback)
extern "C" void Timer__ctor_m1_12954 (Timer_t1_910 * __this, TimerCallback_t1_1488 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Timer::.cctor()
extern "C" void Timer__cctor_m1_12955 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Timer::Init(System.Threading.TimerCallback,System.Object,System.Int64,System.Int64)
extern "C" void Timer_Init_m1_12956 (Timer_t1_910 * __this, TimerCallback_t1_1488 * ___callback, Object_t * ___state, int64_t ___dueTime, int64_t ___period, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.Timer::Change(System.Int32,System.Int32)
extern "C" bool Timer_Change_m1_12957 (Timer_t1_910 * __this, int32_t ___dueTime, int32_t ___period, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.Timer::Change(System.TimeSpan,System.TimeSpan)
extern "C" bool Timer_Change_m1_12958 (Timer_t1_910 * __this, TimeSpan_t1_368  ___dueTime, TimeSpan_t1_368  ___period, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.Timer::Change(System.UInt32,System.UInt32)
extern "C" bool Timer_Change_m1_12959 (Timer_t1_910 * __this, uint32_t ___dueTime, uint32_t ___period, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Timer::Dispose()
extern "C" void Timer_Dispose_m1_12960 (Timer_t1_910 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.Timer::Change(System.Int64,System.Int64)
extern "C" bool Timer_Change_m1_12961 (Timer_t1_910 * __this, int64_t ___dueTime, int64_t ___period, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.Timer::Change(System.Int64,System.Int64,System.Boolean)
extern "C" bool Timer_Change_m1_12962 (Timer_t1_910 * __this, int64_t ___dueTime, int64_t ___period, bool ___first, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.Timer::Dispose(System.Threading.WaitHandle)
extern "C" bool Timer_Dispose_m1_12963 (Timer_t1_910 * __this, WaitHandle_t1_917 * ___notifyObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
