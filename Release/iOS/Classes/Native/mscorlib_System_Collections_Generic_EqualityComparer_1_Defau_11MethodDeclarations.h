﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Vector2>
struct DefaultComparer_t1_2297;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector2.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Vector2>::.ctor()
extern "C" void DefaultComparer__ctor_m1_19304_gshared (DefaultComparer_t1_2297 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1_19304(__this, method) (( void (*) (DefaultComparer_t1_2297 *, const MethodInfo*))DefaultComparer__ctor_m1_19304_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Vector2>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m1_19305_gshared (DefaultComparer_t1_2297 * __this, Vector2_t6_47  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m1_19305(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t1_2297 *, Vector2_t6_47 , const MethodInfo*))DefaultComparer_GetHashCode_m1_19305_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Vector2>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m1_19306_gshared (DefaultComparer_t1_2297 * __this, Vector2_t6_47  ___x, Vector2_t6_47  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m1_19306(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t1_2297 *, Vector2_t6_47 , Vector2_t6_47 , const MethodInfo*))DefaultComparer_Equals_m1_19306_gshared)(__this, ___x, ___y, method)
