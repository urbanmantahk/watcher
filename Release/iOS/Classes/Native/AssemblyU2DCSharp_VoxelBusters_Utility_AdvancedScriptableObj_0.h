﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// VoxelBusters.NativePlugins.NPSettings
struct NPSettings_t8_335;

#include "UnityEngine_UnityEngine_ScriptableObject.h"

// VoxelBusters.Utility.AdvancedScriptableObject`1<VoxelBusters.NativePlugins.NPSettings>
struct  AdvancedScriptableObject_1_t8_336  : public ScriptableObject_t6_15
{
};
struct AdvancedScriptableObject_1_t8_336_StaticFields{
	// T VoxelBusters.Utility.AdvancedScriptableObject`1::instance
	NPSettings_t8_335 * ___instance_6;
	// System.String VoxelBusters.Utility.AdvancedScriptableObject`1::assetGroupFolderName
	String_t* ___assetGroupFolderName_7;
};
