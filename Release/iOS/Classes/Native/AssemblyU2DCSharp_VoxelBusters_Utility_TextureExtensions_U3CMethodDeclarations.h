﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.Utility.TextureExtensions/<TakeScreenshot>c__Iterator3
struct U3CTakeScreenshotU3Ec__Iterator3_t8_43;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.Utility.TextureExtensions/<TakeScreenshot>c__Iterator3::.ctor()
extern "C" void U3CTakeScreenshotU3Ec__Iterator3__ctor_m8_229 (U3CTakeScreenshotU3Ec__Iterator3_t8_43 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VoxelBusters.Utility.TextureExtensions/<TakeScreenshot>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CTakeScreenshotU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m8_230 (U3CTakeScreenshotU3Ec__Iterator3_t8_43 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VoxelBusters.Utility.TextureExtensions/<TakeScreenshot>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CTakeScreenshotU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m8_231 (U3CTakeScreenshotU3Ec__Iterator3_t8_43 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.Utility.TextureExtensions/<TakeScreenshot>c__Iterator3::MoveNext()
extern "C" bool U3CTakeScreenshotU3Ec__Iterator3_MoveNext_m8_232 (U3CTakeScreenshotU3Ec__Iterator3_t8_43 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.TextureExtensions/<TakeScreenshot>c__Iterator3::Dispose()
extern "C" void U3CTakeScreenshotU3Ec__Iterator3_Dispose_m8_233 (U3CTakeScreenshotU3Ec__Iterator3_t8_43 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.TextureExtensions/<TakeScreenshot>c__Iterator3::Reset()
extern "C" void U3CTakeScreenshotU3Ec__Iterator3_Reset_m8_234 (U3CTakeScreenshotU3Ec__Iterator3_t8_43 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
