﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Exception[]
struct ExceptionU5BU5D_t1_634;
// System.Type[]
struct TypeU5BU5D_t1_31;

#include "mscorlib_System_SystemException.h"

// System.Reflection.ReflectionTypeLoadException
struct  ReflectionTypeLoadException_t1_633  : public SystemException_t1_250
{
	// System.Exception[] System.Reflection.ReflectionTypeLoadException::loaderExceptions
	ExceptionU5BU5D_t1_634* ___loaderExceptions_12;
	// System.Type[] System.Reflection.ReflectionTypeLoadException::types
	TypeU5BU5D_t1_31* ___types_13;
};
