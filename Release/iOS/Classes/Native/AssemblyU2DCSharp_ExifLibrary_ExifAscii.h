﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_ExifLibrary_ExifProperty.h"

// ExifLibrary.ExifAscii
struct  ExifAscii_t8_116  : public ExifProperty_t8_99
{
	// System.String ExifLibrary.ExifAscii::mValue
	String_t* ___mValue_3;
};
