﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Serialization.FormatterConverter
struct FormatterConverter_t1_1075;
// System.Object
struct Object_t;
// System.Type
struct Type_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_TypeCode.h"
#include "mscorlib_System_DateTime.h"
#include "mscorlib_System_Decimal.h"

// System.Void System.Runtime.Serialization.FormatterConverter::.ctor()
extern "C" void FormatterConverter__ctor_m1_9506 (FormatterConverter_t1_1075 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Serialization.FormatterConverter::Convert(System.Object,System.Type)
extern "C" Object_t * FormatterConverter_Convert_m1_9507 (FormatterConverter_t1_1075 * __this, Object_t * ___value, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Serialization.FormatterConverter::Convert(System.Object,System.TypeCode)
extern "C" Object_t * FormatterConverter_Convert_m1_9508 (FormatterConverter_t1_1075 * __this, Object_t * ___value, int32_t ___typeCode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Serialization.FormatterConverter::ToBoolean(System.Object)
extern "C" bool FormatterConverter_ToBoolean_m1_9509 (FormatterConverter_t1_1075 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte System.Runtime.Serialization.FormatterConverter::ToByte(System.Object)
extern "C" uint8_t FormatterConverter_ToByte_m1_9510 (FormatterConverter_t1_1075 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char System.Runtime.Serialization.FormatterConverter::ToChar(System.Object)
extern "C" uint16_t FormatterConverter_ToChar_m1_9511 (FormatterConverter_t1_1075 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Runtime.Serialization.FormatterConverter::ToDateTime(System.Object)
extern "C" DateTime_t1_150  FormatterConverter_ToDateTime_m1_9512 (FormatterConverter_t1_1075 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Runtime.Serialization.FormatterConverter::ToDecimal(System.Object)
extern "C" Decimal_t1_19  FormatterConverter_ToDecimal_m1_9513 (FormatterConverter_t1_1075 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Runtime.Serialization.FormatterConverter::ToDouble(System.Object)
extern "C" double FormatterConverter_ToDouble_m1_9514 (FormatterConverter_t1_1075 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 System.Runtime.Serialization.FormatterConverter::ToInt16(System.Object)
extern "C" int16_t FormatterConverter_ToInt16_m1_9515 (FormatterConverter_t1_1075 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.Serialization.FormatterConverter::ToInt32(System.Object)
extern "C" int32_t FormatterConverter_ToInt32_m1_9516 (FormatterConverter_t1_1075 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Runtime.Serialization.FormatterConverter::ToInt64(System.Object)
extern "C" int64_t FormatterConverter_ToInt64_m1_9517 (FormatterConverter_t1_1075 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single System.Runtime.Serialization.FormatterConverter::ToSingle(System.Object)
extern "C" float FormatterConverter_ToSingle_m1_9518 (FormatterConverter_t1_1075 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Serialization.FormatterConverter::ToString(System.Object)
extern "C" String_t* FormatterConverter_ToString_m1_9519 (FormatterConverter_t1_1075 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.SByte System.Runtime.Serialization.FormatterConverter::ToSByte(System.Object)
extern "C" int8_t FormatterConverter_ToSByte_m1_9520 (FormatterConverter_t1_1075 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 System.Runtime.Serialization.FormatterConverter::ToUInt16(System.Object)
extern "C" uint16_t FormatterConverter_ToUInt16_m1_9521 (FormatterConverter_t1_1075 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 System.Runtime.Serialization.FormatterConverter::ToUInt32(System.Object)
extern "C" uint32_t FormatterConverter_ToUInt32_m1_9522 (FormatterConverter_t1_1075 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 System.Runtime.Serialization.FormatterConverter::ToUInt64(System.Object)
extern "C" uint64_t FormatterConverter_ToUInt64_m1_9523 (FormatterConverter_t1_1075 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
