﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.AddressBookContact
struct AddressBookContact_t8_198;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1_238;
// UnityEngine.Texture2D
struct Texture2D_t6_33;
// VoxelBusters.Utility.DownloadTexture/Completion
struct Completion_t8_161;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.AddressBookContact::.ctor()
extern "C" void AddressBookContact__ctor_m8_1140 (AddressBookContact_t8_198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.AddressBookContact::.ctor(VoxelBusters.NativePlugins.AddressBookContact)
extern "C" void AddressBookContact__ctor_m8_1141 (AddressBookContact_t8_198 * __this, AddressBookContact_t8_198 * ____source, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.AddressBookContact::get_FirstName()
extern "C" String_t* AddressBookContact_get_FirstName_m8_1142 (AddressBookContact_t8_198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.AddressBookContact::set_FirstName(System.String)
extern "C" void AddressBookContact_set_FirstName_m8_1143 (AddressBookContact_t8_198 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.AddressBookContact::get_LastName()
extern "C" String_t* AddressBookContact_get_LastName_m8_1144 (AddressBookContact_t8_198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.AddressBookContact::set_LastName(System.String)
extern "C" void AddressBookContact_set_LastName_m8_1145 (AddressBookContact_t8_198 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.AddressBookContact::get_ImagePath()
extern "C" String_t* AddressBookContact_get_ImagePath_m8_1146 (AddressBookContact_t8_198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.AddressBookContact::set_ImagePath(System.String)
extern "C" void AddressBookContact_set_ImagePath_m8_1147 (AddressBookContact_t8_198 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] VoxelBusters.NativePlugins.AddressBookContact::get_PhoneNumberList()
extern "C" StringU5BU5D_t1_238* AddressBookContact_get_PhoneNumberList_m8_1148 (AddressBookContact_t8_198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.AddressBookContact::set_PhoneNumberList(System.String[])
extern "C" void AddressBookContact_set_PhoneNumberList_m8_1149 (AddressBookContact_t8_198 * __this, StringU5BU5D_t1_238* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] VoxelBusters.NativePlugins.AddressBookContact::get_EmailIDList()
extern "C" StringU5BU5D_t1_238* AddressBookContact_get_EmailIDList_m8_1150 (AddressBookContact_t8_198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.AddressBookContact::set_EmailIDList(System.String[])
extern "C" void AddressBookContact_set_EmailIDList_m8_1151 (AddressBookContact_t8_198 * __this, StringU5BU5D_t1_238* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D VoxelBusters.NativePlugins.AddressBookContact::GetDefaultImage()
extern "C" Texture2D_t6_33 * AddressBookContact_GetDefaultImage_m8_1152 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.AddressBookContact::GetImageAsync(VoxelBusters.Utility.DownloadTexture/Completion)
extern "C" void AddressBookContact_GetImageAsync_m8_1153 (AddressBookContact_t8_198 * __this, Completion_t8_161 * ____onCompletion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.AddressBookContact::ToString()
extern "C" String_t* AddressBookContact_ToString_m8_1154 (AddressBookContact_t8_198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
