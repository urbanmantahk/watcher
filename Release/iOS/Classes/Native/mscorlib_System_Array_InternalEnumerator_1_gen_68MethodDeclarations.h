﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_68.h"
#include "mscorlib_System_Reflection_Emit_SequencePoint.h"

// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.SequencePoint>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_16251_gshared (InternalEnumerator_1_t1_2060 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1_16251(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1_2060 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m1_16251_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.SequencePoint>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_16252_gshared (InternalEnumerator_1_t1_2060 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_16252(__this, method) (( void (*) (InternalEnumerator_1_t1_2060 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_16252_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.SequencePoint>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_16253_gshared (InternalEnumerator_1_t1_2060 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_16253(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1_2060 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_16253_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.SequencePoint>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_16254_gshared (InternalEnumerator_1_t1_2060 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1_16254(__this, method) (( void (*) (InternalEnumerator_1_t1_2060 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1_16254_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.SequencePoint>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_16255_gshared (InternalEnumerator_1_t1_2060 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1_16255(__this, method) (( bool (*) (InternalEnumerator_1_t1_2060 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1_16255_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.SequencePoint>::get_Current()
extern "C" SequencePoint_t1_526  InternalEnumerator_1_get_Current_m1_16256_gshared (InternalEnumerator_1_t1_2060 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1_16256(__this, method) (( SequencePoint_t1_526  (*) (InternalEnumerator_1_t1_2060 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1_16256_gshared)(__this, method)
