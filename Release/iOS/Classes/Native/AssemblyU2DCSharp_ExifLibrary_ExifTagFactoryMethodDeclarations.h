﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTag.h"
#include "AssemblyU2DCSharp_ExifLibrary_IFD.h"

// ExifLibrary.ExifTag ExifLibrary.ExifTagFactory::GetExifTag(ExifLibrary.IFD,System.UInt16)
extern "C" int32_t ExifTagFactory_GetExifTag_m8_629 (Object_t * __this /* static, unused */, int32_t ___ifd, uint16_t ___tagid, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 ExifLibrary.ExifTagFactory::GetTagID(ExifLibrary.ExifTag)
extern "C" uint16_t ExifTagFactory_GetTagID_m8_630 (Object_t * __this /* static, unused */, int32_t ___exiftag, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.IFD ExifLibrary.ExifTagFactory::GetTagIFD(ExifLibrary.ExifTag)
extern "C" int32_t ExifTagFactory_GetTagIFD_m8_631 (Object_t * __this /* static, unused */, int32_t ___tag, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExifLibrary.ExifTagFactory::GetTagName(ExifLibrary.ExifTag)
extern "C" String_t* ExifTagFactory_GetTagName_m8_632 (Object_t * __this /* static, unused */, int32_t ___tag, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExifLibrary.ExifTagFactory::GetTagName(ExifLibrary.IFD,System.UInt16)
extern "C" String_t* ExifTagFactory_GetTagName_m8_633 (Object_t * __this /* static, unused */, int32_t ___ifd, uint16_t ___tagid, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExifLibrary.ExifTagFactory::GetTagLongName(ExifLibrary.ExifTag)
extern "C" String_t* ExifTagFactory_GetTagLongName_m8_634 (Object_t * __this /* static, unused */, int32_t ___tag, const MethodInfo* method) IL2CPP_METHOD_ATTR;
