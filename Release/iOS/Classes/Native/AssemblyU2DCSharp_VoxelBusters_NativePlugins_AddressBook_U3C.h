﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VoxelBusters.NativePlugins.AddressBook/ReadContactsCompletion
struct ReadContactsCompletion_t8_194;
// VoxelBusters.NativePlugins.AddressBook
struct AddressBook_t8_196;

#include "mscorlib_System_Object.h"

// VoxelBusters.NativePlugins.AddressBook/<ReadContacts>c__AnonStorey9
struct  U3CReadContactsU3Ec__AnonStorey9_t8_195  : public Object_t
{
	// VoxelBusters.NativePlugins.AddressBook/ReadContactsCompletion VoxelBusters.NativePlugins.AddressBook/<ReadContacts>c__AnonStorey9::_onCompletion
	ReadContactsCompletion_t8_194 * ____onCompletion_0;
	// VoxelBusters.NativePlugins.AddressBook VoxelBusters.NativePlugins.AddressBook/<ReadContacts>c__AnonStorey9::<>f__this
	AddressBook_t8_196 * ___U3CU3Ef__this_1;
};
