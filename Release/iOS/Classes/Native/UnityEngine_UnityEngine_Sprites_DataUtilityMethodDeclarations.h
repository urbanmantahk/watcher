﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Sprite
struct Sprite_t6_75;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector4.h"
#include "UnityEngine_UnityEngine_Vector2.h"

// UnityEngine.Vector4 UnityEngine.Sprites.DataUtility::GetInnerUV(UnityEngine.Sprite)
extern "C" Vector4_t6_54  DataUtility_GetInnerUV_m6_536 (Object_t * __this /* static, unused */, Sprite_t6_75 * ___sprite, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Sprites.DataUtility::GetOuterUV(UnityEngine.Sprite)
extern "C" Vector4_t6_54  DataUtility_GetOuterUV_m6_537 (Object_t * __this /* static, unused */, Sprite_t6_75 * ___sprite, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Sprites.DataUtility::GetPadding(UnityEngine.Sprite)
extern "C" Vector4_t6_54  DataUtility_GetPadding_m6_538 (Object_t * __this /* static, unused */, Sprite_t6_75 * ___sprite, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Sprites.DataUtility::GetMinSize(UnityEngine.Sprite)
extern "C" Vector2_t6_47  DataUtility_GetMinSize_m6_539 (Object_t * __this /* static, unused */, Sprite_t6_75 * ___sprite, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Sprites.DataUtility::Internal_GetMinSize(UnityEngine.Sprite,UnityEngine.Vector2&)
extern "C" void DataUtility_Internal_GetMinSize_m6_540 (Object_t * __this /* static, unused */, Sprite_t6_75 * ___sprite, Vector2_t6_47 * ___output, const MethodInfo* method) IL2CPP_METHOD_ATTR;
