﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.AccessControl.MutexAuditRule
struct MutexAuditRule_t1_1159;
// System.Security.Principal.IdentityReference
struct IdentityReference_t1_1120;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_AccessControl_MutexRights.h"
#include "mscorlib_System_Security_AccessControl_AuditFlags.h"

// System.Void System.Security.AccessControl.MutexAuditRule::.ctor(System.Security.Principal.IdentityReference,System.Security.AccessControl.MutexRights,System.Security.AccessControl.AuditFlags)
extern "C" void MutexAuditRule__ctor_m1_9935 (MutexAuditRule_t1_1159 * __this, IdentityReference_t1_1120 * ___identity, int32_t ___eventRights, int32_t ___flags, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.AccessControl.MutexRights System.Security.AccessControl.MutexAuditRule::get_MutexRights()
extern "C" int32_t MutexAuditRule_get_MutexRights_m1_9936 (MutexAuditRule_t1_1159 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
