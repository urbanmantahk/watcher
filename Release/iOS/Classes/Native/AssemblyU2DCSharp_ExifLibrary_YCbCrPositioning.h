﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "AssemblyU2DCSharp_ExifLibrary_YCbCrPositioning.h"

// ExifLibrary.YCbCrPositioning
struct  YCbCrPositioning_t8_67 
{
	// System.UInt16 ExifLibrary.YCbCrPositioning::value__
	uint16_t ___value___1;
};
