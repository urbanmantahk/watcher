﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<VoxelBusters.NativePlugins.BillingProduct>
struct List_1_t1_1910;
// VoxelBusters.NativePlugins.BillingSettings/iOSSettings
struct iOSSettings_t8_216;
// VoxelBusters.NativePlugins.BillingSettings/AndroidSettings
struct AndroidSettings_t8_215;

#include "mscorlib_System_Object.h"

// VoxelBusters.NativePlugins.BillingSettings
struct  BillingSettings_t8_217  : public Object_t
{
	// System.Collections.Generic.List`1<VoxelBusters.NativePlugins.BillingProduct> VoxelBusters.NativePlugins.BillingSettings::m_products
	List_1_t1_1910 * ___m_products_0;
	// VoxelBusters.NativePlugins.BillingSettings/iOSSettings VoxelBusters.NativePlugins.BillingSettings::m_iOS
	iOSSettings_t8_216 * ___m_iOS_1;
	// VoxelBusters.NativePlugins.BillingSettings/AndroidSettings VoxelBusters.NativePlugins.BillingSettings::m_android
	AndroidSettings_t8_215 * ___m_android_2;
};
