﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Collection_1_t1_2190;
// System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IList_1_t1_2189;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Object
struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t1_2186;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t1_2777;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6.h"

// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor()
extern "C" void Collection_1__ctor_m1_17917_gshared (Collection_1_t1_2190 * __this, const MethodInfo* method);
#define Collection_1__ctor_m1_17917(__this, method) (( void (*) (Collection_1_t1_2190 *, const MethodInfo*))Collection_1__ctor_m1_17917_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1__ctor_m1_17918_gshared (Collection_1_t1_2190 * __this, Object_t* ___list, const MethodInfo* method);
#define Collection_1__ctor_m1_17918(__this, ___list, method) (( void (*) (Collection_1_t1_2190 *, Object_t*, const MethodInfo*))Collection_1__ctor_m1_17918_gshared)(__this, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_17919_gshared (Collection_1_t1_2190 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_17919(__this, method) (( bool (*) (Collection_1_t1_2190 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_17919_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m1_17920_gshared (Collection_1_t1_2190 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m1_17920(__this, ___array, ___index, method) (( void (*) (Collection_1_t1_2190 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m1_17920_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m1_17921_gshared (Collection_1_t1_2190 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m1_17921(__this, method) (( Object_t * (*) (Collection_1_t1_2190 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m1_17921_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m1_17922_gshared (Collection_1_t1_2190 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m1_17922(__this, ___value, method) (( int32_t (*) (Collection_1_t1_2190 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m1_17922_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m1_17923_gshared (Collection_1_t1_2190 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m1_17923(__this, ___value, method) (( bool (*) (Collection_1_t1_2190 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m1_17923_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m1_17924_gshared (Collection_1_t1_2190 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m1_17924(__this, ___value, method) (( int32_t (*) (Collection_1_t1_2190 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m1_17924_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m1_17925_gshared (Collection_1_t1_2190 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m1_17925(__this, ___index, ___value, method) (( void (*) (Collection_1_t1_2190 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m1_17925_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m1_17926_gshared (Collection_1_t1_2190 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m1_17926(__this, ___value, method) (( void (*) (Collection_1_t1_2190 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m1_17926_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m1_17927_gshared (Collection_1_t1_2190 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m1_17927(__this, method) (( bool (*) (Collection_1_t1_2190 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m1_17927_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m1_17928_gshared (Collection_1_t1_2190 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m1_17928(__this, method) (( Object_t * (*) (Collection_1_t1_2190 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m1_17928_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m1_17929_gshared (Collection_1_t1_2190 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m1_17929(__this, method) (( bool (*) (Collection_1_t1_2190 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m1_17929_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m1_17930_gshared (Collection_1_t1_2190 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m1_17930(__this, method) (( bool (*) (Collection_1_t1_2190 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m1_17930_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m1_17931_gshared (Collection_1_t1_2190 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m1_17931(__this, ___index, method) (( Object_t * (*) (Collection_1_t1_2190 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m1_17931_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m1_17932_gshared (Collection_1_t1_2190 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m1_17932(__this, ___index, ___value, method) (( void (*) (Collection_1_t1_2190 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m1_17932_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Add(T)
extern "C" void Collection_1_Add_m1_17933_gshared (Collection_1_t1_2190 * __this, KeyValuePair_2_t1_2015  ___item, const MethodInfo* method);
#define Collection_1_Add_m1_17933(__this, ___item, method) (( void (*) (Collection_1_t1_2190 *, KeyValuePair_2_t1_2015 , const MethodInfo*))Collection_1_Add_m1_17933_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Clear()
extern "C" void Collection_1_Clear_m1_17934_gshared (Collection_1_t1_2190 * __this, const MethodInfo* method);
#define Collection_1_Clear_m1_17934(__this, method) (( void (*) (Collection_1_t1_2190 *, const MethodInfo*))Collection_1_Clear_m1_17934_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ClearItems()
extern "C" void Collection_1_ClearItems_m1_17935_gshared (Collection_1_t1_2190 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m1_17935(__this, method) (( void (*) (Collection_1_t1_2190 *, const MethodInfo*))Collection_1_ClearItems_m1_17935_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Contains(T)
extern "C" bool Collection_1_Contains_m1_17936_gshared (Collection_1_t1_2190 * __this, KeyValuePair_2_t1_2015  ___item, const MethodInfo* method);
#define Collection_1_Contains_m1_17936(__this, ___item, method) (( bool (*) (Collection_1_t1_2190 *, KeyValuePair_2_t1_2015 , const MethodInfo*))Collection_1_Contains_m1_17936_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m1_17937_gshared (Collection_1_t1_2190 * __this, KeyValuePair_2U5BU5D_t1_2186* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m1_17937(__this, ___array, ___index, method) (( void (*) (Collection_1_t1_2190 *, KeyValuePair_2U5BU5D_t1_2186*, int32_t, const MethodInfo*))Collection_1_CopyTo_m1_17937_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m1_17938_gshared (Collection_1_t1_2190 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m1_17938(__this, method) (( Object_t* (*) (Collection_1_t1_2190 *, const MethodInfo*))Collection_1_GetEnumerator_m1_17938_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m1_17939_gshared (Collection_1_t1_2190 * __this, KeyValuePair_2_t1_2015  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m1_17939(__this, ___item, method) (( int32_t (*) (Collection_1_t1_2190 *, KeyValuePair_2_t1_2015 , const MethodInfo*))Collection_1_IndexOf_m1_17939_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m1_17940_gshared (Collection_1_t1_2190 * __this, int32_t ___index, KeyValuePair_2_t1_2015  ___item, const MethodInfo* method);
#define Collection_1_Insert_m1_17940(__this, ___index, ___item, method) (( void (*) (Collection_1_t1_2190 *, int32_t, KeyValuePair_2_t1_2015 , const MethodInfo*))Collection_1_Insert_m1_17940_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m1_17941_gshared (Collection_1_t1_2190 * __this, int32_t ___index, KeyValuePair_2_t1_2015  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m1_17941(__this, ___index, ___item, method) (( void (*) (Collection_1_t1_2190 *, int32_t, KeyValuePair_2_t1_2015 , const MethodInfo*))Collection_1_InsertItem_m1_17941_gshared)(__this, ___index, ___item, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Items()
extern "C" Object_t* Collection_1_get_Items_m1_17942_gshared (Collection_1_t1_2190 * __this, const MethodInfo* method);
#define Collection_1_get_Items_m1_17942(__this, method) (( Object_t* (*) (Collection_1_t1_2190 *, const MethodInfo*))Collection_1_get_Items_m1_17942_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Remove(T)
extern "C" bool Collection_1_Remove_m1_17943_gshared (Collection_1_t1_2190 * __this, KeyValuePair_2_t1_2015  ___item, const MethodInfo* method);
#define Collection_1_Remove_m1_17943(__this, ___item, method) (( bool (*) (Collection_1_t1_2190 *, KeyValuePair_2_t1_2015 , const MethodInfo*))Collection_1_Remove_m1_17943_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m1_17944_gshared (Collection_1_t1_2190 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m1_17944(__this, ___index, method) (( void (*) (Collection_1_t1_2190 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m1_17944_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m1_17945_gshared (Collection_1_t1_2190 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m1_17945(__this, ___index, method) (( void (*) (Collection_1_t1_2190 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m1_17945_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Count()
extern "C" int32_t Collection_1_get_Count_m1_17946_gshared (Collection_1_t1_2190 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m1_17946(__this, method) (( int32_t (*) (Collection_1_t1_2190 *, const MethodInfo*))Collection_1_get_Count_m1_17946_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Item(System.Int32)
extern "C" KeyValuePair_2_t1_2015  Collection_1_get_Item_m1_17947_gshared (Collection_1_t1_2190 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m1_17947(__this, ___index, method) (( KeyValuePair_2_t1_2015  (*) (Collection_1_t1_2190 *, int32_t, const MethodInfo*))Collection_1_get_Item_m1_17947_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m1_17948_gshared (Collection_1_t1_2190 * __this, int32_t ___index, KeyValuePair_2_t1_2015  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m1_17948(__this, ___index, ___value, method) (( void (*) (Collection_1_t1_2190 *, int32_t, KeyValuePair_2_t1_2015 , const MethodInfo*))Collection_1_set_Item_m1_17948_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m1_17949_gshared (Collection_1_t1_2190 * __this, int32_t ___index, KeyValuePair_2_t1_2015  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m1_17949(__this, ___index, ___item, method) (( void (*) (Collection_1_t1_2190 *, int32_t, KeyValuePair_2_t1_2015 , const MethodInfo*))Collection_1_SetItem_m1_17949_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m1_17950_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m1_17950(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m1_17950_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ConvertItem(System.Object)
extern "C" KeyValuePair_2_t1_2015  Collection_1_ConvertItem_m1_17951_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m1_17951(__this /* static, unused */, ___item, method) (( KeyValuePair_2_t1_2015  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m1_17951_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m1_17952_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m1_17952(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m1_17952_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m1_17953_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m1_17953(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m1_17953_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m1_17954_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m1_17954(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m1_17954_gshared)(__this /* static, unused */, ___list, method)
