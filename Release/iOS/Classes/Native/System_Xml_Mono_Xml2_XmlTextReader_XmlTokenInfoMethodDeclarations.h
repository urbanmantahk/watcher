﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Xml2.XmlTextReader/XmlTokenInfo
struct XmlTokenInfo_t4_168;
// Mono.Xml2.XmlTextReader
struct XmlTextReader_t4_169;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Xml2.XmlTextReader/XmlTokenInfo::.ctor(Mono.Xml2.XmlTextReader)
extern "C" void XmlTokenInfo__ctor_m4_780 (XmlTokenInfo_t4_168 * __this, XmlTextReader_t4_169 * ___xtr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Xml2.XmlTextReader/XmlTokenInfo::get_Value()
extern "C" String_t* XmlTokenInfo_get_Value_m4_781 (XmlTokenInfo_t4_168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Xml2.XmlTextReader/XmlTokenInfo::set_Value(System.String)
extern "C" void XmlTokenInfo_set_Value_m4_782 (XmlTokenInfo_t4_168 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Xml2.XmlTextReader/XmlTokenInfo::Clear()
extern "C" void XmlTokenInfo_Clear_m4_783 (XmlTokenInfo_t4_168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
