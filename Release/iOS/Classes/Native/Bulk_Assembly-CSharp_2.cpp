﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// VoxelBusters.NativePlugins.Internal.iOSBillingProduct
struct iOSBillingProduct_t8_209;
// System.Collections.IDictionary
struct IDictionary_t1_35;
// System.String
struct String_t;
// System.Object
struct Object_t;
// VoxelBusters.NativePlugins.BillingProduct
struct BillingProduct_t8_207;
// VoxelBusters.NativePlugins.Internal.AndroidBillingTransaction
struct AndroidBillingTransaction_t8_210;
// VoxelBusters.NativePlugins.BillingTransaction
struct BillingTransaction_t8_211;
// VoxelBusters.NativePlugins.Internal.EditorBillingTransaction
struct EditorBillingTransaction_t8_212;
// VoxelBusters.NativePlugins.Internal.iOSBillingTransaction
struct iOSBillingTransaction_t8_214;
// VoxelBusters.NativePlugins.BillingSettings/AndroidSettings
struct AndroidSettings_t8_215;
// VoxelBusters.NativePlugins.BillingSettings/iOSSettings
struct iOSSettings_t8_216;
// VoxelBusters.NativePlugins.BillingSettings
struct BillingSettings_t8_217;
// VoxelBusters.NativePlugins.BillingProduct[]
struct BillingProductU5BU5D_t8_337;
// VoxelBusters.NativePlugins.Achievement/LoadAchievementsCompletion
struct LoadAchievementsCompletion_t8_218;
// VoxelBusters.NativePlugins.Achievement[]
struct AchievementU5BU5D_t8_219;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;
// VoxelBusters.NativePlugins.Achievement/ReportProgressCompletion
struct ReportProgressCompletion_t8_220;
// VoxelBusters.NativePlugins.Achievement
struct Achievement_t8_221;
// VoxelBusters.NativePlugins.AchievementDescription
struct AchievementDescription_t8_225;
// VoxelBusters.NativePlugins.AchievementDescription/LoadAchievementDescriptionsCompletion
struct LoadAchievementDescriptionsCompletion_t8_223;
// VoxelBusters.NativePlugins.AchievementDescription[]
struct AchievementDescriptionU5BU5D_t8_224;
// VoxelBusters.Utility.DownloadTexture/Completion
struct Completion_t8_161;
// UnityEngine.Texture2D
struct Texture2D_t6_33;
// VoxelBusters.NativePlugins.Leaderboard/LoadScoreCompletion
struct LoadScoreCompletion_t8_229;
// VoxelBusters.NativePlugins.Score[]
struct ScoreU5BU5D_t8_230;
// VoxelBusters.NativePlugins.Score
struct Score_t8_231;
// VoxelBusters.NativePlugins.Leaderboard
struct Leaderboard_t8_180;
// VoxelBusters.NativePlugins.Score/ReportScoreCompletion
struct ReportScoreCompletion_t8_232;
// VoxelBusters.NativePlugins.User
struct User_t8_235;
// VoxelBusters.NativePlugins.User/LoadUsersCompletion
struct LoadUsersCompletion_t8_233;
// VoxelBusters.NativePlugins.User[]
struct UserU5BU5D_t8_234;
// VoxelBusters.NativePlugins.GameServicesSettings/AndroidSettings
struct AndroidSettings_t8_236;
// System.String[]
struct StringU5BU5D_t1_238;
// VoxelBusters.NativePlugins.GameServicesSettings/iOSSettings
struct iOSSettings_t8_237;
// VoxelBusters.NativePlugins.GameServicesSettings
struct GameServicesSettings_t8_238;
// VoxelBusters.NativePlugins.IDContainer[]
struct IDContainerU5BU5D_t8_239;
// VoxelBusters.NativePlugins.MediaLibrary/PickImageCompletion
struct PickImageCompletion_t8_240;
// VoxelBusters.NativePlugins.MediaLibrary/SaveImageToGalleryCompletion
struct SaveImageToGalleryCompletion_t8_241;
// VoxelBusters.NativePlugins.MediaLibrary/PickVideoCompletion
struct PickVideoCompletion_t8_242;
// VoxelBusters.NativePlugins.MediaLibrary/PlayVideoCompletion
struct PlayVideoCompletion_t8_243;
// VoxelBusters.NativePlugins.MediaLibrary/<PickImageFinished>c__AnonStoreyB
struct U3CPickImageFinishedU3Ec__AnonStoreyB_t8_244;
// VoxelBusters.NativePlugins.MediaLibrary/<SaveScreenshotToGallery>c__AnonStoreyC
struct U3CSaveScreenshotToGalleryU3Ec__AnonStoreyC_t8_246;
// VoxelBusters.NativePlugins.MediaLibrary/<SaveImageToGallery>c__AnonStoreyD
struct U3CSaveImageToGalleryU3Ec__AnonStoreyD_t8_247;
// VoxelBusters.NativePlugins.MediaLibrary
struct MediaLibrary_t8_245;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// VoxelBusters.NativePlugins.MediaLibrarySettings/AndroidSettings
struct AndroidSettings_t8_252;
// VoxelBusters.NativePlugins.MediaLibrarySettings
struct MediaLibrarySettings_t8_253;
// VoxelBusters.NativePlugins.NetworkConnectivitySettings/EditorSettings
struct EditorSettings_t8_254;
// VoxelBusters.NativePlugins.NetworkConnectivitySettings/AndroidSettings
struct AndroidSettings_t8_255;
// VoxelBusters.NativePlugins.NetworkConnectivitySettings
struct NetworkConnectivitySettings_t8_256;
// VoxelBusters.NativePlugins.NotificationService/RegisterForRemoteNotificationCompletion
struct RegisterForRemoteNotificationCompletion_t8_257;
// VoxelBusters.NativePlugins.NotificationService/ReceivedNotificationResponse
struct ReceivedNotificationResponse_t8_258;
// VoxelBusters.NativePlugins.CrossPlatformNotification
struct CrossPlatformNotification_t8_259;
// VoxelBusters.NativePlugins.NotificationService/<ProcessAppLaunchInfo>c__Iterator5
struct U3CProcessAppLaunchInfoU3Ec__Iterator5_t8_260;
// VoxelBusters.NativePlugins.NotificationService
struct NotificationService_t8_261;
// VoxelBusters.NativePlugins.NotificationServiceSettings
struct NotificationServiceSettings_t8_270;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// VoxelBusters.NativePlugins.NotificationServiceIOS
struct NotificationServiceIOS_t8_262;
// VoxelBusters.NativePlugins.CrossPlatformNotification/AndroidSpecificProperties
struct AndroidSpecificProperties_t8_265;
// VoxelBusters.NativePlugins.CrossPlatformNotification/iOSSpecificProperties
struct iOSSpecificProperties_t8_266;
// VoxelBusters.NativePlugins.Internal.iOSNotificationPayload
struct iOSNotificationPayload_t8_267;
// VoxelBusters.NativePlugins.NotificationServiceSettings/iOSSettings
struct iOSSettings_t8_268;
// VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings
struct AndroidSettings_t8_269;
// VoxelBusters.NativePlugins.Sharing/SharingCompletion
struct SharingCompletion_t8_271;
// VoxelBusters.NativePlugins.Sharing/<ShowViewCoroutine>c__Iterator6
struct U3CShowViewCoroutineU3Ec__Iterator6_t8_272;
// VoxelBusters.NativePlugins.Sharing/<SendMailWithScreenshot>c__AnonStoreyE
struct U3CSendMailWithScreenshotU3Ec__AnonStoreyE_t8_275;
// VoxelBusters.NativePlugins.Sharing/<ShareScreenshotOnWhatsApp>c__AnonStoreyF
struct U3CShareScreenshotOnWhatsAppU3Ec__AnonStoreyF_t8_276;
// VoxelBusters.NativePlugins.Sharing/<ShareScreenShot>c__AnonStorey10
struct U3CShareScreenShotU3Ec__AnonStorey10_t8_277;
// VoxelBusters.NativePlugins.Sharing/<ShareImageAtPath>c__AnonStorey11
struct U3CShareImageAtPathU3Ec__AnonStorey11_t8_278;
// VoxelBusters.NativePlugins.Sharing
struct Sharing_t8_273;
// VoxelBusters.NativePlugins.MailShareComposer
struct MailShareComposer_t8_279;
// VoxelBusters.NativePlugins.MessageShareComposer
struct MessageShareComposer_t8_281;
// VoxelBusters.NativePlugins.FBShareComposer
struct FBShareComposer_t8_282;
// VoxelBusters.NativePlugins.TwitterShareComposer
struct TwitterShareComposer_t8_284;
// VoxelBusters.NativePlugins.WhatsAppShareComposer
struct WhatsAppShareComposer_t8_285;
// VoxelBusters.NativePlugins.IShareView
struct IShareView_t8_274;
// VoxelBusters.NativePlugins.ShareSheet
struct ShareSheet_t8_289;
// VoxelBusters.NativePlugins.eShareOptions[]
struct eShareOptionsU5BU5D_t8_186;
// VoxelBusters.NativePlugins.SocialShareComposerBase
struct SocialShareComposerBase_t8_283;
// VoxelBusters.NativePlugins.SocialShareSheet
struct SocialShareSheet_t8_290;
// VoxelBusters.NativePlugins.Internal.ShareImageUtility
struct ShareImageUtility_t8_280;
// VoxelBusters.NativePlugins.SocialNetworkSettings
struct SocialNetworkSettings_t8_291;
// VoxelBusters.NativePlugins.TwitterSettings
struct TwitterSettings_t8_292;
// VoxelBusters.NativePlugins.Internal.AndroidTwitterSession
struct AndroidTwitterSession_t8_294;
// VoxelBusters.NativePlugins.TwitterSession
struct TwitterSession_t8_295;
// VoxelBusters.NativePlugins.Internal.iOSTwitterSession
struct iOSTwitterSession_t8_296;
// VoxelBusters.NativePlugins.Internal.AndroidTwitterUser
struct AndroidTwitterUser_t8_297;
// VoxelBusters.NativePlugins.TwitterUser
struct TwitterUser_t8_298;
// VoxelBusters.NativePlugins.Internal.iOSTwitterUser
struct iOSTwitterUser_t8_299;
// VoxelBusters.NativePlugins.UI/AlertDialogCompletion
struct AlertDialogCompletion_t8_300;
// VoxelBusters.NativePlugins.UI/SingleFieldPromptCompletion
struct SingleFieldPromptCompletion_t8_301;
// VoxelBusters.NativePlugins.UI/LoginPromptCompletion
struct LoginPromptCompletion_t8_302;
// VoxelBusters.NativePlugins.UI
struct UI_t8_303;
// VoxelBusters.NativePlugins.UIIOS
struct UIIOS_t8_304;
// VoxelBusters.NativePlugins.Utility
struct Utility_t8_306;
// VoxelBusters.NativePlugins.RateMyApp
struct RateMyApp_t8_307;
// VoxelBusters.NativePlugins.UtilityIOS
struct UtilityIOS_t8_308;
// VoxelBusters.NativePlugins.UtilitySettings
struct UtilitySettings_t8_309;
// VoxelBusters.NativePlugins.RateMyApp/Settings
struct Settings_t8_310;
// VoxelBusters.NativePlugins.Internal.WebViewNative
struct WebViewNative_t8_311;
// VoxelBusters.NativePlugins.WebView
struct WebView_t8_191;
// VoxelBusters.NativePlugins.WebViewMessage
struct WebViewMessage_t8_314;
// VoxelBusters.NativePlugins.Internal.AndroidWebViewMessage
struct AndroidWebViewMessage_t8_313;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t1_1839;
// VoxelBusters.NativePlugins.Internal.iOSWebViewMessage
struct iOSWebViewMessage_t8_315;
// VoxelBusters.NativePlugins.PlatformBindingHelper
struct PlatformBindingHelper_t8_316;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_iOSBil.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_iOSBilMethodDeclarations.h"
#include "mscorlib_System_Void.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_MutablMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_IDictionaryExtensionsMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_BillingProductMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_Mutabl.h"
#include "mscorlib_System_String.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_IDictionaryExtensions.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_BillingProduct.h"
#include "mscorlib_System_Single.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_1MethodDeclarations.h"
#include "mscorlib_System_SingleMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_1.h"
#include "mscorlib_System_Object.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_Androi_1.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_Androi_1MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_BillingTransactMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_DateTimeExtensionsMethodDeclarations.h"
#include "mscorlib_System_DateTimeMethodDeclarations.h"
#include "mscorlib_System_Int64.h"
#include "mscorlib_System_DateTime.h"
#include "mscorlib_System_Int32.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_BillingTransact.h"
#include "mscorlib_System_Boolean.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eBillingTransac.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eBillingTransac_0.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_12MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_12.h"
#include "mscorlib_System_StringMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_DebugPRO_ConsoleMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object.h"
#include "mscorlib_System_ObjectMethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_Editor_1.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_Editor_1MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_JSONParserExtensionsMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_iOSBil_1MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_iOSBil_0.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_iOSBil_0MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_iOSBil_1.h"
#include "mscorlib_System_Int32MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_BillingSettings.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_BillingSettingsMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_BillingSettings_0.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_BillingSettings_0MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_BillingSettings_1.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_BillingSettings_1MethodDeclarations.h"
#include "Assembly-CSharp_ArrayTypes.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_49MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_49.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Achievement_Loa.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Achievement_LoaMethodDeclarations.h"
#include "mscorlib_System_IntPtr.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Achievement.h"
#include "mscorlib_System_AsyncCallback.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Achievement_Rep.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Achievement_RepMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_AchievementMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_NPObjeMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_NPObje.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_NPObje_0.h"
#include "mscorlib_System_DelegateMethodDeclarations.h"
#include "mscorlib_System_Delegate.h"
#include "mscorlib_System_Double.h"
#include "mscorlib_System_MathMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_AchievementDesc_0.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_AchievementDesc_0MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_AchievementHandMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_AchievementDesc.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_AchievementDescMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_DownloadTexture_Compl.h"
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_DownloadTexture_ComplMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2D.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_URL.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_DownloadTextureMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_RequestMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_DownloadTexture.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_Request.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eLeaderboardUse.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eLeaderboardUseMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eLeaderboardTim.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eLeaderboardTimMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eLeaderboardPag.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eLeaderboardPagMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Leaderboard_Loa.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Leaderboard_LoaMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Score.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Leaderboard.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_LeaderboardMethodDeclarations.h"
#include "UnityEngine_UnityEngine_MathfMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Score_ReportSco.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Score_ReportScoMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_ScoreMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_User.h"
#include "mscorlib_System_Globalization_CultureInfoMethodDeclarations.h"
#include "mscorlib_System_Int64MethodDeclarations.h"
#include "mscorlib_System_Globalization_CultureInfo.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_UserMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_User_LoadUsersC.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_User_LoadUsersCMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_GameServicesSet.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_GameServicesSetMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_GameServicesSet_0.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_GameServicesSet_0MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_GameServicesSet_1.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_GameServicesSet_1MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_IDContainer.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_MediaLibrary_Pi.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_MediaLibrary_PiMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_ePickImageFinis.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_MediaLibrary_Sa.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_MediaLibrary_SaMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_MediaLibrary_Pi_0.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_MediaLibrary_Pi_0MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_ePickVideoFinis.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_MediaLibrary_Pl.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_MediaLibrary_PlMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_ePlayVideoFinis.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_MediaLibrary_U3.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_MediaLibrary_U3MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_URLMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_MediaLibrary.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_MediaLibrary_U3_0.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_MediaLibrary_U3_0MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_MediaLibraryMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_MediaLibrary_U3_1.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_MediaLibrary_U3_1MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_JSONUtilityMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_MonoBehaviourExtensioMethodDeclarations.h"
#include "mscorlib_System_BooleanMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eImageSource.h"
#include "mscorlib_System_Action_1_gen_5MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_TextureExtensionsMethodDeclarations.h"
#include "mscorlib_System_Action_1_gen_5.h"
#include "UnityEngine_UnityEngine_Coroutine.h"
#include "UnityEngine_UnityEngine_Texture2DMethodDeclarations.h"
#include "mscorlib_System_Byte.h"
#include "System_System_Text_RegularExpressions_RegexMethodDeclarations.h"
#include "System_System_Text_RegularExpressions_GroupMethodDeclarations.h"
#include "System_System_Text_RegularExpressions_CaptureMethodDeclarations.h"
#include "System_System_Text_RegularExpressions_GroupCollectionMethodDeclarations.h"
#include "System_System_Text_RegularExpressions_Match.h"
#include "System_System_Text_RegularExpressions_Group.h"
#include "System_System_Text_RegularExpressions_RegexOptions.h"
#include "System_System_Text_RegularExpressions_MatchMethodDeclarations.h"
#include "System_System_Text_RegularExpressions_GroupCollection.h"
#include "System_System_Text_RegularExpressions_Capture.h"
#include "mscorlib_System_TypeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ResourcesMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextAssetMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextAsset.h"
#include "mscorlib_System_Type.h"
#include "mscorlib_System_RuntimeTypeHandle.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eImageSourceMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_ePickImageFinisMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_ePickVideoFinisMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_ePlayVideoFinisMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_MediaLibrarySet.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_MediaLibrarySetMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_MediaLibrarySet_0.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_MediaLibrarySet_0MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_NetworkConnecti.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_NetworkConnectiMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_NetworkConnecti_0.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_NetworkConnecti_0MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_NetworkConnecti_1.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_NetworkConnecti_1MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_NotificationSer.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_NotificationSerMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_NotificationSer_0.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_NotificationSer_0MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_CrossPlatformNo_1.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_NotificationSer_1.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_NotificationSer_1MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForEndOfFrameMethodDeclarations.h"
#include "mscorlib_System_UInt32.h"
#include "UnityEngine_UnityEngine_WaitForEndOfFrame.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_NotificationSer_2.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_NotificationSer_2MethodDeclarations.h"
#include "mscorlib_System_NotSupportedExceptionMethodDeclarations.h"
#include "mscorlib_System_NotSupportedException.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_CrossPlatformNo_1MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_NPSettingsMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_ApplicationSett_2MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_ApplicationSett_0MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_ApplicationSett_2.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_ApplicationSett_0.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_NotificationSer_6.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_NotificationTyp.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_NotificationSer_3.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_NotificationSer_3MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_iOSNotMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_iOSNot.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_NotificationSer_6MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_NotificationSer_4MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_NotificationSer_4.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_NotificationTypMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eNotificationRe.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eNotificationReMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_CrossPlatformNo.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_CrossPlatformNoMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_CrossPlatformNo_0.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_CrossPlatformNo_0MethodDeclarations.h"
#include "AssemblyU2DCSharp_NPBindingMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Utility.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_UtilityMethodDeclarations.h"
#include "mscorlib_System_ConvertMethodDeclarations.h"
#include "UnityEngine_UnityEngine_iOS_CalendarUnit.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_NotificationSer_5.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_NotificationSer_5MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Sharing_Sharing.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Sharing_SharingMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eShareResult.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Sharing_U3CShow.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Sharing_U3CShowMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Sharing.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_MailShareCompos.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_SharingMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_MessageShareCom.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_WhatsAppShareCo.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_FBShareComposer.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_TwitterShareCom.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_ShareSheet.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Sharing_U3CSend.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Sharing_U3CSendMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Sharing_U3CShar.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Sharing_U3CSharMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Sharing_U3CShar_0.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Sharing_U3CShar_0MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eShareOptions.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Sharing_U3CShar_1.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Sharing_U3CShar_1MethodDeclarations.h"
#include "mscorlib_System_IO_FileMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_FileOperationsMethodDeclarations.h"
#include "mscorlib_System_IO_PathMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_MailShareComposMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_ShareIMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_ShareI.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_MessageShareComMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_FBShareComposerMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_SocialShareCompMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_SocialShareComp.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_eSocia.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_TwitterShareComMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_WhatsAppShareCoMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eShareResultMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eShareOptionsMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_eSociaMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_ShareSheetMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_SocialShareShee.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_SocialShareSheeMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_DesignPatterns_SingletonPatteMethodDeclarations.h"
#include "AssemblyU2DCSharp_NPBinding.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_SocialNetworkSe.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_SocialNetworkSeMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_TwitterSettingsMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_TwitterSettings.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eTwitterCompose.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eTwitterComposeMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_Androi_2.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_Androi_2MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_TwitterSessionMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_TwitterSession.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_iOSTwi.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_iOSTwiMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_Androi_3.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_Androi_3MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_TwitterUserMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_TwitterUser.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_iOSTwi_0.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_iOSTwi_0MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_UI_AlertDialogC.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_UI_AlertDialogCMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_UI_SingleFieldP.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_UI_SingleFieldPMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_UI_LoginPromptC.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_UI_LoginPromptCMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_UI.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_UIMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_17MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_17.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eToastMessageLe.h"
#include "UnityEngine_UnityEngine_Vector2.h"
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"
#include "UnityEngine_UnityEngine_InputMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TouchMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ScreenMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Touch.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_UIIOS.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_UIIOSMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eToastMessageLeMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_RateMyApp.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_UtilitySettingsMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_RateMyApp_SettiMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObjectMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_UtilitySettings.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_RateMyApp_Setti.h"
#include "UnityEngine_UnityEngine_Component.h"
#include "UnityEngine_UnityEngine_GameObject.h"
#include "mscorlib_System_GuidMethodDeclarations.h"
#include "mscorlib_System_Guid.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_PlayerSettingsMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_UtilityIOS.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_UtilityIOSMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SystemInfoMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ApplicationMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_WebVie.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_WebVieMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_18MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_18.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_WebView.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_ReflectionExtensionsMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_WebViewMessage.h"
#include "UnityEngine_UnityEngine_Rect.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_WebViewMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eWebviewControl.h"
#include "UnityEngine_UnityEngine_Color.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eWebviewControlMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_Androi_4.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_Androi_4MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_WebViewMessageMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_iOSWeb.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_iOSWebMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_PlatformBinding.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_PlatformBindingMethodDeclarations.h"

// !!0 VoxelBusters.Utility.IDictionaryExtensions::GetIfAvailable<System.Object>(System.Collections.IDictionary,System.String)
extern "C" Object_t * IDictionaryExtensions_GetIfAvailable_TisObject_t_m8_1973_gshared (Object_t * __this /* static, unused */, Object_t * p0, String_t* p1, const MethodInfo* method);
#define IDictionaryExtensions_GetIfAvailable_TisObject_t_m8_1973(__this /* static, unused */, p0, p1, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, String_t*, const MethodInfo*))IDictionaryExtensions_GetIfAvailable_TisObject_t_m8_1973_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 VoxelBusters.Utility.IDictionaryExtensions::GetIfAvailable<System.String>(System.Collections.IDictionary,System.String)
#define IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(__this /* static, unused */, p0, p1, method) (( String_t* (*) (Object_t * /* static, unused */, Object_t *, String_t*, const MethodInfo*))IDictionaryExtensions_GetIfAvailable_TisObject_t_m8_1973_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 VoxelBusters.Utility.IDictionaryExtensions::GetIfAvailable<System.Single>(System.Collections.IDictionary,System.String)
extern "C" float IDictionaryExtensions_GetIfAvailable_TisSingle_t1_17_m8_2011_gshared (Object_t * __this /* static, unused */, Object_t * p0, String_t* p1, const MethodInfo* method);
#define IDictionaryExtensions_GetIfAvailable_TisSingle_t1_17_m8_2011(__this /* static, unused */, p0, p1, method) (( float (*) (Object_t * /* static, unused */, Object_t *, String_t*, const MethodInfo*))IDictionaryExtensions_GetIfAvailable_TisSingle_t1_17_m8_2011_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 VoxelBusters.Utility.IDictionaryExtensions::GetIfAvailable<System.Int64>(System.Collections.IDictionary,System.String)
extern "C" int64_t IDictionaryExtensions_GetIfAvailable_TisInt64_t1_7_m8_2010_gshared (Object_t * __this /* static, unused */, Object_t * p0, String_t* p1, const MethodInfo* method);
#define IDictionaryExtensions_GetIfAvailable_TisInt64_t1_7_m8_2010(__this /* static, unused */, p0, p1, method) (( int64_t (*) (Object_t * /* static, unused */, Object_t *, String_t*, const MethodInfo*))IDictionaryExtensions_GetIfAvailable_TisInt64_t1_7_m8_2010_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 VoxelBusters.Utility.IDictionaryExtensions::GetIfAvailable<System.Int32>(System.Collections.IDictionary,System.String)
extern "C" int32_t IDictionaryExtensions_GetIfAvailable_TisInt32_t1_3_m8_2007_gshared (Object_t * __this /* static, unused */, Object_t * p0, String_t* p1, const MethodInfo* method);
#define IDictionaryExtensions_GetIfAvailable_TisInt32_t1_3_m8_2007(__this /* static, unused */, p0, p1, method) (( int32_t (*) (Object_t * /* static, unused */, Object_t *, String_t*, const MethodInfo*))IDictionaryExtensions_GetIfAvailable_TisInt32_t1_3_m8_2007_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 VoxelBusters.Utility.IDictionaryExtensions::GetIfAvailable<System.Boolean>(System.Collections.IDictionary,System.String)
extern "C" bool IDictionaryExtensions_GetIfAvailable_TisBoolean_t1_20_m8_2012_gshared (Object_t * __this /* static, unused */, Object_t * p0, String_t* p1, const MethodInfo* method);
#define IDictionaryExtensions_GetIfAvailable_TisBoolean_t1_20_m8_2012(__this /* static, unused */, p0, p1, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, String_t*, const MethodInfo*))IDictionaryExtensions_GetIfAvailable_TisBoolean_t1_20_m8_2012_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 VoxelBusters.Utility.IDictionaryExtensions::GetIfAvailable<VoxelBusters.NativePlugins.eNotificationRepeatInterval>(System.Collections.IDictionary,System.String)
extern "C" int32_t IDictionaryExtensions_GetIfAvailable_TiseNotificationRepeatInterval_t8_264_m8_2013_gshared (Object_t * __this /* static, unused */, Object_t * p0, String_t* p1, const MethodInfo* method);
#define IDictionaryExtensions_GetIfAvailable_TiseNotificationRepeatInterval_t8_264_m8_2013(__this /* static, unused */, p0, p1, method) (( int32_t (*) (Object_t * /* static, unused */, Object_t *, String_t*, const MethodInfo*))IDictionaryExtensions_GetIfAvailable_TiseNotificationRepeatInterval_t8_264_m8_2013_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 VoxelBusters.Utility.IDictionaryExtensions::GetIfAvailable<System.Collections.IDictionary>(System.Collections.IDictionary,System.String)
#define IDictionaryExtensions_GetIfAvailable_TisIDictionary_t1_35_m8_2014(__this /* static, unused */, p0, p1, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, String_t*, const MethodInfo*))IDictionaryExtensions_GetIfAvailable_TisObject_t_m8_1973_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 VoxelBusters.Utility.IDictionaryExtensions::GetIfAvailable<UnityEngine.iOS.CalendarUnit>(System.Collections.IDictionary,System.String)
extern "C" int32_t IDictionaryExtensions_GetIfAvailable_TisCalendarUnit_t6_103_m8_2015_gshared (Object_t * __this /* static, unused */, Object_t * p0, String_t* p1, const MethodInfo* method);
#define IDictionaryExtensions_GetIfAvailable_TisCalendarUnit_t6_103_m8_2015(__this /* static, unused */, p0, p1, method) (( int32_t (*) (Object_t * /* static, unused */, Object_t *, String_t*, const MethodInfo*))IDictionaryExtensions_GetIfAvailable_TisCalendarUnit_t6_103_m8_2015_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C" Object_t * GameObject_AddComponent_TisObject_t_m6_1906_gshared (GameObject_t6_97 * __this, const MethodInfo* method);
#define GameObject_AddComponent_TisObject_t_m6_1906(__this, method) (( Object_t * (*) (GameObject_t6_97 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m6_1906_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<VoxelBusters.NativePlugins.RateMyApp>()
#define GameObject_AddComponent_TisRateMyApp_t8_307_m6_1927(__this, method) (( RateMyApp_t8_307 * (*) (GameObject_t6_97 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m6_1906_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void VoxelBusters.NativePlugins.Internal.iOSBillingProduct::.ctor(System.Collections.IDictionary)
extern const MethodInfo* IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var;
extern const MethodInfo* IDictionaryExtensions_GetIfAvailable_TisSingle_t1_17_m8_2011_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral5388;
extern Il2CppCodeGenString* _stringLiteral5389;
extern Il2CppCodeGenString* _stringLiteral5390;
extern Il2CppCodeGenString* _stringLiteral5384;
extern Il2CppCodeGenString* _stringLiteral5391;
extern Il2CppCodeGenString* _stringLiteral5392;
extern Il2CppCodeGenString* _stringLiteral5393;
extern "C" void iOSBillingProduct__ctor_m8_1191 (iOSBillingProduct_t8_209 * __this, Object_t * ____productJsonDict, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484117);
		IDictionaryExtensions_GetIfAvailable_TisSingle_t1_17_m8_2011_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484225);
		_stringLiteral5388 = il2cpp_codegen_string_literal_from_index(5388);
		_stringLiteral5389 = il2cpp_codegen_string_literal_from_index(5389);
		_stringLiteral5390 = il2cpp_codegen_string_literal_from_index(5390);
		_stringLiteral5384 = il2cpp_codegen_string_literal_from_index(5384);
		_stringLiteral5391 = il2cpp_codegen_string_literal_from_index(5391);
		_stringLiteral5392 = il2cpp_codegen_string_literal_from_index(5392);
		_stringLiteral5393 = il2cpp_codegen_string_literal_from_index(5393);
		s_Il2CppMethodIntialized = true;
	}
	{
		MutableBillingProduct__ctor_m8_1185(__this, /*hidden argument*/NULL);
		Object_t * L_0 = ____productJsonDict;
		String_t* L_1 = IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(NULL /*static, unused*/, L_0, _stringLiteral5388, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var);
		BillingProduct_set_Name_m8_1163(__this, L_1, /*hidden argument*/NULL);
		Object_t * L_2 = ____productJsonDict;
		String_t* L_3 = IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(NULL /*static, unused*/, L_2, _stringLiteral5389, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var);
		BillingProduct_set_IOSProductID_m8_1177(__this, L_3, /*hidden argument*/NULL);
		Object_t * L_4 = ____productJsonDict;
		String_t* L_5 = IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(NULL /*static, unused*/, L_4, _stringLiteral5390, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var);
		BillingProduct_set_Description_m8_1165(__this, L_5, /*hidden argument*/NULL);
		Object_t * L_6 = ____productJsonDict;
		float L_7 = IDictionaryExtensions_GetIfAvailable_TisSingle_t1_17_m8_2011(NULL /*static, unused*/, L_6, _stringLiteral5384, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisSingle_t1_17_m8_2011_MethodInfo_var);
		BillingProduct_set_Price_m8_1169(__this, L_7, /*hidden argument*/NULL);
		Object_t * L_8 = ____productJsonDict;
		String_t* L_9 = IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(NULL /*static, unused*/, L_8, _stringLiteral5391, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var);
		BillingProduct_set_LocalizedPrice_m8_1171(__this, L_9, /*hidden argument*/NULL);
		Object_t * L_10 = ____productJsonDict;
		String_t* L_11 = IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(NULL /*static, unused*/, L_10, _stringLiteral5392, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var);
		BillingProduct_set_CurrencyCode_m8_1173(__this, L_11, /*hidden argument*/NULL);
		Object_t * L_12 = ____productJsonDict;
		String_t* L_13 = IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(NULL /*static, unused*/, L_12, _stringLiteral5393, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var);
		BillingProduct_set_CurrencySymbol_m8_1175(__this, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IDictionary VoxelBusters.NativePlugins.Internal.iOSBillingProduct::CreateJSONObject(VoxelBusters.NativePlugins.BillingProduct)
extern TypeInfo* Dictionary_2_t1_1839_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionary_t1_35_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_14949_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral5388;
extern Il2CppCodeGenString* _stringLiteral5389;
extern Il2CppCodeGenString* _stringLiteral5390;
extern Il2CppCodeGenString* _stringLiteral5384;
extern Il2CppCodeGenString* _stringLiteral5391;
extern Il2CppCodeGenString* _stringLiteral5392;
extern Il2CppCodeGenString* _stringLiteral5393;
extern "C" Object_t * iOSBillingProduct_CreateJSONObject_m8_1192 (Object_t * __this /* static, unused */, BillingProduct_t8_207 * ____product, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t1_1839_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1603);
		IDictionary_t1_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		Dictionary_2__ctor_m1_14949_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483789);
		_stringLiteral5388 = il2cpp_codegen_string_literal_from_index(5388);
		_stringLiteral5389 = il2cpp_codegen_string_literal_from_index(5389);
		_stringLiteral5390 = il2cpp_codegen_string_literal_from_index(5390);
		_stringLiteral5384 = il2cpp_codegen_string_literal_from_index(5384);
		_stringLiteral5391 = il2cpp_codegen_string_literal_from_index(5391);
		_stringLiteral5392 = il2cpp_codegen_string_literal_from_index(5392);
		_stringLiteral5393 = il2cpp_codegen_string_literal_from_index(5393);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	float V_1 = 0.0f;
	{
		Dictionary_2_t1_1839 * L_0 = (Dictionary_2_t1_1839 *)il2cpp_codegen_object_new (Dictionary_2_t1_1839_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_14949(L_0, /*hidden argument*/Dictionary_2__ctor_m1_14949_MethodInfo_var);
		V_0 = L_0;
		Object_t * L_1 = V_0;
		BillingProduct_t8_207 * L_2 = ____product;
		NullCheck(L_2);
		String_t* L_3 = BillingProduct_get_Name_m8_1162(L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		InterfaceActionInvoker2< Object_t *, Object_t * >::Invoke(3 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_1, _stringLiteral5388, L_3);
		Object_t * L_4 = V_0;
		BillingProduct_t8_207 * L_5 = ____product;
		NullCheck(L_5);
		String_t* L_6 = BillingProduct_get_ProductIdentifier_m8_1180(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		InterfaceActionInvoker2< Object_t *, Object_t * >::Invoke(3 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_4, _stringLiteral5389, L_6);
		Object_t * L_7 = V_0;
		BillingProduct_t8_207 * L_8 = ____product;
		NullCheck(L_8);
		String_t* L_9 = BillingProduct_get_Description_m8_1164(L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		InterfaceActionInvoker2< Object_t *, Object_t * >::Invoke(3 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_7, _stringLiteral5390, L_9);
		Object_t * L_10 = V_0;
		BillingProduct_t8_207 * L_11 = ____product;
		NullCheck(L_11);
		float L_12 = BillingProduct_get_Price_m8_1168(L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		String_t* L_13 = Single_ToString_m1_633((&V_1), /*hidden argument*/NULL);
		NullCheck(L_10);
		InterfaceActionInvoker2< Object_t *, Object_t * >::Invoke(3 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_10, _stringLiteral5384, L_13);
		Object_t * L_14 = V_0;
		BillingProduct_t8_207 * L_15 = ____product;
		NullCheck(L_15);
		String_t* L_16 = BillingProduct_get_LocalizedPrice_m8_1170(L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		InterfaceActionInvoker2< Object_t *, Object_t * >::Invoke(3 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_14, _stringLiteral5391, L_16);
		Object_t * L_17 = V_0;
		BillingProduct_t8_207 * L_18 = ____product;
		NullCheck(L_18);
		String_t* L_19 = BillingProduct_get_CurrencyCode_m8_1172(L_18, /*hidden argument*/NULL);
		NullCheck(L_17);
		InterfaceActionInvoker2< Object_t *, Object_t * >::Invoke(3 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_17, _stringLiteral5392, L_19);
		Object_t * L_20 = V_0;
		BillingProduct_t8_207 * L_21 = ____product;
		NullCheck(L_21);
		String_t* L_22 = BillingProduct_get_CurrencySymbol_m8_1174(L_21, /*hidden argument*/NULL);
		NullCheck(L_20);
		InterfaceActionInvoker2< Object_t *, Object_t * >::Invoke(3 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_20, _stringLiteral5393, L_22);
		Object_t * L_23 = V_0;
		return L_23;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.AndroidBillingTransaction::.ctor(System.Collections.IDictionary)
extern TypeInfo* IDictionary_t1_35_il2cpp_TypeInfo_var;
extern const MethodInfo* IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var;
extern const MethodInfo* IDictionaryExtensions_GetIfAvailable_TisInt64_t1_7_m8_2010_MethodInfo_var;
extern const MethodInfo* IDictionaryExtensions_GetIfAvailable_TisInt32_t1_3_m8_2007_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral5394;
extern Il2CppCodeGenString* _stringLiteral5395;
extern Il2CppCodeGenString* _stringLiteral5381;
extern Il2CppCodeGenString* _stringLiteral5396;
extern Il2CppCodeGenString* _stringLiteral5397;
extern Il2CppCodeGenString* _stringLiteral1989;
extern Il2CppCodeGenString* _stringLiteral5398;
extern Il2CppCodeGenString* _stringLiteral5241;
extern "C" void AndroidBillingTransaction__ctor_m8_1193 (AndroidBillingTransaction_t8_210 * __this, Object_t * ____transactionInfo, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionary_t1_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484117);
		IDictionaryExtensions_GetIfAvailable_TisInt64_t1_7_m8_2010_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484224);
		IDictionaryExtensions_GetIfAvailable_TisInt32_t1_3_m8_2007_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484196);
		_stringLiteral5394 = il2cpp_codegen_string_literal_from_index(5394);
		_stringLiteral5395 = il2cpp_codegen_string_literal_from_index(5395);
		_stringLiteral5381 = il2cpp_codegen_string_literal_from_index(5381);
		_stringLiteral5396 = il2cpp_codegen_string_literal_from_index(5396);
		_stringLiteral5397 = il2cpp_codegen_string_literal_from_index(5397);
		_stringLiteral1989 = il2cpp_codegen_string_literal_from_index(1989);
		_stringLiteral5398 = il2cpp_codegen_string_literal_from_index(5398);
		_stringLiteral5241 = il2cpp_codegen_string_literal_from_index(5241);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	int64_t V_1 = 0;
	DateTime_t1_150  V_2 = {0};
	int32_t V_3 = 0;
	String_t* V_4 = {0};
	{
		BillingTransaction__ctor_m8_1200(__this, /*hidden argument*/NULL);
		Object_t * L_0 = ____transactionInfo;
		NullCheck(L_0);
		Object_t * L_1 = (Object_t *)InterfaceFuncInvoker1< Object_t *, Object_t * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_0, _stringLiteral5394);
		V_0 = ((Object_t *)IsInst(L_1, IDictionary_t1_35_il2cpp_TypeInfo_var));
		Object_t * L_2 = ____transactionInfo;
		String_t* L_3 = IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(NULL /*static, unused*/, L_2, _stringLiteral5395, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var);
		BillingTransaction_set_RawPurchaseData_m8_1218(__this, L_3, /*hidden argument*/NULL);
		Object_t * L_4 = V_0;
		String_t* L_5 = IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(NULL /*static, unused*/, L_4, _stringLiteral5381, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var);
		BillingTransaction_set_ProductIdentifier_m8_1202(__this, L_5, /*hidden argument*/NULL);
		Object_t * L_6 = V_0;
		int64_t L_7 = IDictionaryExtensions_GetIfAvailable_TisInt64_t1_7_m8_2010(NULL /*static, unused*/, L_6, _stringLiteral5396, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisInt64_t1_7_m8_2010_MethodInfo_var);
		V_1 = L_7;
		int64_t L_8 = V_1;
		DateTime_t1_150  L_9 = DateTimeExtensions_ToDateTimeFromJavaTime_m8_188(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		DateTime_t1_150  L_10 = DateTime_ToUniversalTime_m1_13901((&V_2), /*hidden argument*/NULL);
		BillingTransaction_set_TransactionDateUTC_m8_1204(__this, L_10, /*hidden argument*/NULL);
		DateTime_t1_150  L_11 = DateTime_ToLocalTime_m1_13900((&V_2), /*hidden argument*/NULL);
		BillingTransaction_set_TransactionDateLocal_m8_1206(__this, L_11, /*hidden argument*/NULL);
		Object_t * L_12 = V_0;
		String_t* L_13 = AndroidBillingTransaction_GetPurchaseIdentifier_m8_1195(__this, L_12, /*hidden argument*/NULL);
		BillingTransaction_set_TransactionIdentifier_m8_1208(__this, L_13, /*hidden argument*/NULL);
		Object_t * L_14 = ____transactionInfo;
		NullCheck(L_14);
		bool L_15 = (bool)InterfaceFuncInvoker1< bool, Object_t * >::Invoke(8 /* System.Boolean System.Collections.IDictionary::Contains(System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_14, _stringLiteral5397);
		if (!L_15)
		{
			goto IL_0094;
		}
	}
	{
		Object_t * L_16 = ____transactionInfo;
		int32_t L_17 = IDictionaryExtensions_GetIfAvailable_TisInt32_t1_3_m8_2007(NULL /*static, unused*/, L_16, _stringLiteral5397, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisInt32_t1_3_m8_2007_MethodInfo_var);
		V_3 = L_17;
		goto IL_00a0;
	}

IL_0094:
	{
		Object_t * L_18 = V_0;
		int32_t L_19 = IDictionaryExtensions_GetIfAvailable_TisInt32_t1_3_m8_2007(NULL /*static, unused*/, L_18, _stringLiteral5397, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisInt32_t1_3_m8_2007_MethodInfo_var);
		V_3 = L_19;
	}

IL_00a0:
	{
		int32_t L_20 = V_3;
		int32_t L_21 = AndroidBillingTransaction_GetConvertedPurchaseState_m8_1198(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		BillingTransaction_set_TransactionState_m8_1212(__this, L_21, /*hidden argument*/NULL);
		Object_t * L_22 = ____transactionInfo;
		String_t* L_23 = IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(NULL /*static, unused*/, L_22, _stringLiteral1989, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var);
		BillingTransaction_set_TransactionReceipt_m8_1210(__this, L_23, /*hidden argument*/NULL);
		Object_t * L_24 = ____transactionInfo;
		String_t* L_25 = IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(NULL /*static, unused*/, L_24, _stringLiteral5398, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var);
		V_4 = L_25;
		String_t* L_26 = V_4;
		int32_t L_27 = AndroidBillingTransaction_GetValidationState_m8_1196(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		BillingTransaction_set_VerificationState_m8_1214(__this, L_27, /*hidden argument*/NULL);
		Object_t * L_28 = ____transactionInfo;
		String_t* L_29 = IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(NULL /*static, unused*/, L_28, _stringLiteral5241, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var);
		BillingTransaction_set_Error_m8_1216(__this, L_29, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IDictionary VoxelBusters.NativePlugins.Internal.AndroidBillingTransaction::CreateJSONObject(VoxelBusters.NativePlugins.BillingTransaction)
extern TypeInfo* Dictionary_2_t1_1903_il2cpp_TypeInfo_var;
extern TypeInfo* Int64_t1_7_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionary_t1_35_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_15037_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral5396;
extern Il2CppCodeGenString* _stringLiteral5399;
extern Il2CppCodeGenString* _stringLiteral5397;
extern Il2CppCodeGenString* _stringLiteral5381;
extern Il2CppCodeGenString* _stringLiteral5394;
extern Il2CppCodeGenString* _stringLiteral5395;
extern Il2CppCodeGenString* _stringLiteral5398;
extern Il2CppCodeGenString* _stringLiteral1989;
extern Il2CppCodeGenString* _stringLiteral5241;
extern "C" Object_t * AndroidBillingTransaction_CreateJSONObject_m8_1194 (Object_t * __this /* static, unused */, BillingTransaction_t8_211 * ____transaction, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t1_1903_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1960);
		Int64_t1_7_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(21);
		IDictionary_t1_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		Dictionary_2__ctor_m1_15037_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484124);
		_stringLiteral5396 = il2cpp_codegen_string_literal_from_index(5396);
		_stringLiteral5399 = il2cpp_codegen_string_literal_from_index(5399);
		_stringLiteral5397 = il2cpp_codegen_string_literal_from_index(5397);
		_stringLiteral5381 = il2cpp_codegen_string_literal_from_index(5381);
		_stringLiteral5394 = il2cpp_codegen_string_literal_from_index(5394);
		_stringLiteral5395 = il2cpp_codegen_string_literal_from_index(5395);
		_stringLiteral5398 = il2cpp_codegen_string_literal_from_index(5398);
		_stringLiteral1989 = il2cpp_codegen_string_literal_from_index(1989);
		_stringLiteral5241 = il2cpp_codegen_string_literal_from_index(5241);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	Object_t * V_1 = {0};
	{
		Dictionary_2_t1_1903 * L_0 = (Dictionary_2_t1_1903 *)il2cpp_codegen_object_new (Dictionary_2_t1_1903_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_15037(L_0, /*hidden argument*/Dictionary_2__ctor_m1_15037_MethodInfo_var);
		V_0 = L_0;
		Dictionary_2_t1_1903 * L_1 = (Dictionary_2_t1_1903 *)il2cpp_codegen_object_new (Dictionary_2_t1_1903_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_15037(L_1, /*hidden argument*/Dictionary_2__ctor_m1_15037_MethodInfo_var);
		V_1 = L_1;
		Object_t * L_2 = V_1;
		BillingTransaction_t8_211 * L_3 = ____transaction;
		NullCheck(L_3);
		DateTime_t1_150  L_4 = BillingTransaction_get_TransactionDateUTC_m8_1203(L_3, /*hidden argument*/NULL);
		int64_t L_5 = DateTimeExtensions_ToJavaTimeFromDateTime_m8_189(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		int64_t L_6 = L_5;
		Object_t * L_7 = Box(Int64_t1_7_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_2);
		InterfaceActionInvoker2< Object_t *, Object_t * >::Invoke(3 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_2, _stringLiteral5396, L_7);
		Object_t * L_8 = V_1;
		BillingTransaction_t8_211 * L_9 = ____transaction;
		NullCheck(L_9);
		String_t* L_10 = BillingTransaction_get_TransactionIdentifier_m8_1207(L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		InterfaceActionInvoker2< Object_t *, Object_t * >::Invoke(3 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_8, _stringLiteral5399, L_10);
		Object_t * L_11 = V_1;
		BillingTransaction_t8_211 * L_12 = ____transaction;
		NullCheck(L_12);
		int32_t L_13 = BillingTransaction_get_TransactionState_m8_1211(L_12, /*hidden argument*/NULL);
		int32_t L_14 = AndroidBillingTransaction_GetConvertedPurchaseState_m8_1199(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		int32_t L_15 = L_14;
		Object_t * L_16 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_11);
		InterfaceActionInvoker2< Object_t *, Object_t * >::Invoke(3 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_11, _stringLiteral5397, L_16);
		Object_t * L_17 = V_1;
		BillingTransaction_t8_211 * L_18 = ____transaction;
		NullCheck(L_18);
		String_t* L_19 = BillingTransaction_get_ProductIdentifier_m8_1201(L_18, /*hidden argument*/NULL);
		NullCheck(L_17);
		InterfaceActionInvoker2< Object_t *, Object_t * >::Invoke(3 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_17, _stringLiteral5381, L_19);
		Object_t * L_20 = V_0;
		Object_t * L_21 = V_1;
		NullCheck(L_20);
		InterfaceActionInvoker2< Object_t *, Object_t * >::Invoke(3 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_20, _stringLiteral5394, L_21);
		Object_t * L_22 = V_0;
		BillingTransaction_t8_211 * L_23 = ____transaction;
		NullCheck(L_23);
		String_t* L_24 = BillingTransaction_get_RawPurchaseData_m8_1217(L_23, /*hidden argument*/NULL);
		NullCheck(L_22);
		InterfaceActionInvoker2< Object_t *, Object_t * >::Invoke(3 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_22, _stringLiteral5395, L_24);
		Object_t * L_25 = V_0;
		BillingTransaction_t8_211 * L_26 = ____transaction;
		NullCheck(L_26);
		int32_t L_27 = BillingTransaction_get_VerificationState_m8_1213(L_26, /*hidden argument*/NULL);
		String_t* L_28 = AndroidBillingTransaction_GetValidationState_m8_1197(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
		NullCheck(L_25);
		InterfaceActionInvoker2< Object_t *, Object_t * >::Invoke(3 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_25, _stringLiteral5398, L_28);
		Object_t * L_29 = V_0;
		BillingTransaction_t8_211 * L_30 = ____transaction;
		NullCheck(L_30);
		String_t* L_31 = BillingTransaction_get_TransactionReceipt_m8_1209(L_30, /*hidden argument*/NULL);
		NullCheck(L_29);
		InterfaceActionInvoker2< Object_t *, Object_t * >::Invoke(3 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_29, _stringLiteral1989, L_31);
		Object_t * L_32 = V_0;
		BillingTransaction_t8_211 * L_33 = ____transaction;
		NullCheck(L_33);
		String_t* L_34 = BillingTransaction_get_Error_m8_1215(L_33, /*hidden argument*/NULL);
		NullCheck(L_32);
		InterfaceActionInvoker2< Object_t *, Object_t * >::Invoke(3 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_32, _stringLiteral5241, L_34);
		Object_t * L_35 = V_0;
		return L_35;
	}
}
// System.String VoxelBusters.NativePlugins.Internal.AndroidBillingTransaction::GetPurchaseIdentifier(System.Collections.IDictionary)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral5399;
extern Il2CppCodeGenString* _stringLiteral5400;
extern "C" String_t* AndroidBillingTransaction_GetPurchaseIdentifier_m8_1195 (AndroidBillingTransaction_t8_210 * __this, Object_t * ____transactionJsonDict, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484117);
		_stringLiteral5399 = il2cpp_codegen_string_literal_from_index(5399);
		_stringLiteral5400 = il2cpp_codegen_string_literal_from_index(5400);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	{
		Object_t * L_0 = ____transactionJsonDict;
		String_t* L_1 = IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(NULL /*static, unused*/, L_0, _stringLiteral5399, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var);
		V_0 = L_1;
		String_t* L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m1_521(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0023;
		}
	}
	{
		Object_t * L_4 = ____transactionJsonDict;
		String_t* L_5 = IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(NULL /*static, unused*/, L_4, _stringLiteral5400, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var);
		V_0 = L_5;
	}

IL_0023:
	{
		String_t* L_6 = V_0;
		return L_6;
	}
}
// VoxelBusters.NativePlugins.eBillingTransactionVerificationState VoxelBusters.NativePlugins.Internal.AndroidBillingTransaction::GetValidationState(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5401;
extern Il2CppCodeGenString* _stringLiteral5402;
extern Il2CppCodeGenString* _stringLiteral5403;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5404;
extern "C" int32_t AndroidBillingTransaction_GetValidationState_m8_1196 (Object_t * __this /* static, unused */, String_t* ____validationState, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		_stringLiteral5401 = il2cpp_codegen_string_literal_from_index(5401);
		_stringLiteral5402 = il2cpp_codegen_string_literal_from_index(5402);
		_stringLiteral5403 = il2cpp_codegen_string_literal_from_index(5403);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5404 = il2cpp_codegen_string_literal_from_index(5404);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	{
		String_t* L_0 = ____validationState;
		NullCheck(L_0);
		bool L_1 = String_Equals_m1_441(L_0, _stringLiteral5401, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		V_0 = 2;
		goto IL_005d;
	}

IL_0017:
	{
		String_t* L_2 = ____validationState;
		NullCheck(L_2);
		bool L_3 = String_Equals_m1_441(L_2, _stringLiteral5402, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		V_0 = 1;
		goto IL_005d;
	}

IL_002e:
	{
		String_t* L_4 = ____validationState;
		NullCheck(L_4);
		bool L_5 = String_Equals_m1_441(L_4, _stringLiteral5403, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0045;
		}
	}
	{
		V_0 = 0;
		goto IL_005d;
	}

IL_0045:
	{
		String_t* L_6 = ____validationState;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m1_559(NULL /*static, unused*/, _stringLiteral5404, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_LogError_m8_1009(NULL /*static, unused*/, _stringLiteral5368, L_7, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		V_0 = 2;
	}

IL_005d:
	{
		int32_t L_8 = V_0;
		return L_8;
	}
}
// System.String VoxelBusters.NativePlugins.Internal.AndroidBillingTransaction::GetValidationState(VoxelBusters.NativePlugins.eBillingTransactionVerificationState)
extern TypeInfo* eBillingTransactionVerificationState_t8_204_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5401;
extern Il2CppCodeGenString* _stringLiteral5402;
extern Il2CppCodeGenString* _stringLiteral5403;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5404;
extern "C" String_t* AndroidBillingTransaction_GetValidationState_m8_1197 (Object_t * __this /* static, unused */, int32_t ____state, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		eBillingTransactionVerificationState_t8_204_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2118);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		_stringLiteral5401 = il2cpp_codegen_string_literal_from_index(5401);
		_stringLiteral5402 = il2cpp_codegen_string_literal_from_index(5402);
		_stringLiteral5403 = il2cpp_codegen_string_literal_from_index(5403);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5404 = il2cpp_codegen_string_literal_from_index(5404);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	{
		int32_t L_0 = ____state;
		if ((!(((uint32_t)L_0) == ((uint32_t)2))))
		{
			goto IL_0012;
		}
	}
	{
		V_0 = _stringLiteral5401;
		goto IL_0056;
	}

IL_0012:
	{
		int32_t L_1 = ____state;
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0024;
		}
	}
	{
		V_0 = _stringLiteral5402;
		goto IL_0056;
	}

IL_0024:
	{
		int32_t L_2 = ____state;
		if (L_2)
		{
			goto IL_0035;
		}
	}
	{
		V_0 = _stringLiteral5403;
		goto IL_0056;
	}

IL_0035:
	{
		int32_t L_3 = ____state;
		int32_t L_4 = L_3;
		Object_t * L_5 = Box(eBillingTransactionVerificationState_t8_204_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m1_556(NULL /*static, unused*/, _stringLiteral5404, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_LogError_m8_1009(NULL /*static, unused*/, _stringLiteral5368, L_6, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		V_0 = _stringLiteral5401;
	}

IL_0056:
	{
		String_t* L_7 = V_0;
		return L_7;
	}
}
// VoxelBusters.NativePlugins.eBillingTransactionState VoxelBusters.NativePlugins.Internal.AndroidBillingTransaction::GetConvertedPurchaseState(System.Int32)
extern "C" int32_t AndroidBillingTransaction_GetConvertedPurchaseState_m8_1198 (Object_t * __this /* static, unused */, int32_t ____stateInt, const MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		V_0 = 1;
		int32_t L_0 = ____stateInt;
		if ((((int32_t)L_0) == ((int32_t)(-1))))
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_1 = ____stateInt;
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0017;
		}
	}

IL_0010:
	{
		V_0 = 1;
		goto IL_003b;
	}

IL_0017:
	{
		int32_t L_2 = ____stateInt;
		if (L_2)
		{
			goto IL_0024;
		}
	}
	{
		V_0 = 0;
		goto IL_003b;
	}

IL_0024:
	{
		int32_t L_3 = ____stateInt;
		if ((!(((uint32_t)L_3) == ((uint32_t)2))))
		{
			goto IL_0032;
		}
	}
	{
		V_0 = 3;
		goto IL_003b;
	}

IL_0032:
	{
		int32_t L_4 = ____stateInt;
		if ((!(((uint32_t)L_4) == ((uint32_t)3))))
		{
			goto IL_003b;
		}
	}
	{
		V_0 = 2;
	}

IL_003b:
	{
		int32_t L_5 = V_0;
		return L_5;
	}
}
// System.Int32 VoxelBusters.NativePlugins.Internal.AndroidBillingTransaction::GetConvertedPurchaseState(VoxelBusters.NativePlugins.eBillingTransactionState)
extern "C" int32_t AndroidBillingTransaction_GetConvertedPurchaseState_m8_1199 (Object_t * __this /* static, unused */, int32_t ____state, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (-1);
		int32_t L_0 = ____state;
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0010;
		}
	}
	{
		V_0 = 1;
		goto IL_0034;
	}

IL_0010:
	{
		int32_t L_1 = ____state;
		if (L_1)
		{
			goto IL_001d;
		}
	}
	{
		V_0 = 0;
		goto IL_0034;
	}

IL_001d:
	{
		int32_t L_2 = ____state;
		if ((!(((uint32_t)L_2) == ((uint32_t)3))))
		{
			goto IL_002b;
		}
	}
	{
		V_0 = 2;
		goto IL_0034;
	}

IL_002b:
	{
		int32_t L_3 = ____state;
		if ((!(((uint32_t)L_3) == ((uint32_t)2))))
		{
			goto IL_0034;
		}
	}
	{
		V_0 = 3;
	}

IL_0034:
	{
		int32_t L_4 = V_0;
		return L_4;
	}
}
// System.Void VoxelBusters.NativePlugins.BillingTransaction::.ctor()
extern "C" void BillingTransaction__ctor_m8_1200 (BillingTransaction_t8_211 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String VoxelBusters.NativePlugins.BillingTransaction::get_ProductIdentifier()
extern "C" String_t* BillingTransaction_get_ProductIdentifier_m8_1201 (BillingTransaction_t8_211 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CProductIdentifierU3Ek__BackingField_0);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.BillingTransaction::set_ProductIdentifier(System.String)
extern "C" void BillingTransaction_set_ProductIdentifier_m8_1202 (BillingTransaction_t8_211 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CProductIdentifierU3Ek__BackingField_0 = L_0;
		return;
	}
}
// System.DateTime VoxelBusters.NativePlugins.BillingTransaction::get_TransactionDateUTC()
extern "C" DateTime_t1_150  BillingTransaction_get_TransactionDateUTC_m8_1203 (BillingTransaction_t8_211 * __this, const MethodInfo* method)
{
	{
		DateTime_t1_150  L_0 = (__this->___U3CTransactionDateUTCU3Ek__BackingField_1);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.BillingTransaction::set_TransactionDateUTC(System.DateTime)
extern "C" void BillingTransaction_set_TransactionDateUTC_m8_1204 (BillingTransaction_t8_211 * __this, DateTime_t1_150  ___value, const MethodInfo* method)
{
	{
		DateTime_t1_150  L_0 = ___value;
		__this->___U3CTransactionDateUTCU3Ek__BackingField_1 = L_0;
		return;
	}
}
// System.DateTime VoxelBusters.NativePlugins.BillingTransaction::get_TransactionDateLocal()
extern "C" DateTime_t1_150  BillingTransaction_get_TransactionDateLocal_m8_1205 (BillingTransaction_t8_211 * __this, const MethodInfo* method)
{
	{
		DateTime_t1_150  L_0 = (__this->___U3CTransactionDateLocalU3Ek__BackingField_2);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.BillingTransaction::set_TransactionDateLocal(System.DateTime)
extern "C" void BillingTransaction_set_TransactionDateLocal_m8_1206 (BillingTransaction_t8_211 * __this, DateTime_t1_150  ___value, const MethodInfo* method)
{
	{
		DateTime_t1_150  L_0 = ___value;
		__this->___U3CTransactionDateLocalU3Ek__BackingField_2 = L_0;
		return;
	}
}
// System.String VoxelBusters.NativePlugins.BillingTransaction::get_TransactionIdentifier()
extern "C" String_t* BillingTransaction_get_TransactionIdentifier_m8_1207 (BillingTransaction_t8_211 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CTransactionIdentifierU3Ek__BackingField_3);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.BillingTransaction::set_TransactionIdentifier(System.String)
extern "C" void BillingTransaction_set_TransactionIdentifier_m8_1208 (BillingTransaction_t8_211 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CTransactionIdentifierU3Ek__BackingField_3 = L_0;
		return;
	}
}
// System.String VoxelBusters.NativePlugins.BillingTransaction::get_TransactionReceipt()
extern "C" String_t* BillingTransaction_get_TransactionReceipt_m8_1209 (BillingTransaction_t8_211 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CTransactionReceiptU3Ek__BackingField_4);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.BillingTransaction::set_TransactionReceipt(System.String)
extern "C" void BillingTransaction_set_TransactionReceipt_m8_1210 (BillingTransaction_t8_211 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CTransactionReceiptU3Ek__BackingField_4 = L_0;
		return;
	}
}
// VoxelBusters.NativePlugins.eBillingTransactionState VoxelBusters.NativePlugins.BillingTransaction::get_TransactionState()
extern "C" int32_t BillingTransaction_get_TransactionState_m8_1211 (BillingTransaction_t8_211 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___U3CTransactionStateU3Ek__BackingField_5);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.BillingTransaction::set_TransactionState(VoxelBusters.NativePlugins.eBillingTransactionState)
extern "C" void BillingTransaction_set_TransactionState_m8_1212 (BillingTransaction_t8_211 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___U3CTransactionStateU3Ek__BackingField_5 = L_0;
		return;
	}
}
// VoxelBusters.NativePlugins.eBillingTransactionVerificationState VoxelBusters.NativePlugins.BillingTransaction::get_VerificationState()
extern "C" int32_t BillingTransaction_get_VerificationState_m8_1213 (BillingTransaction_t8_211 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___U3CVerificationStateU3Ek__BackingField_6);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.BillingTransaction::set_VerificationState(VoxelBusters.NativePlugins.eBillingTransactionVerificationState)
extern "C" void BillingTransaction_set_VerificationState_m8_1214 (BillingTransaction_t8_211 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___U3CVerificationStateU3Ek__BackingField_6 = L_0;
		return;
	}
}
// System.String VoxelBusters.NativePlugins.BillingTransaction::get_Error()
extern "C" String_t* BillingTransaction_get_Error_m8_1215 (BillingTransaction_t8_211 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CErrorU3Ek__BackingField_7);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.BillingTransaction::set_Error(System.String)
extern "C" void BillingTransaction_set_Error_m8_1216 (BillingTransaction_t8_211 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CErrorU3Ek__BackingField_7 = L_0;
		return;
	}
}
// System.String VoxelBusters.NativePlugins.BillingTransaction::get_RawPurchaseData()
extern "C" String_t* BillingTransaction_get_RawPurchaseData_m8_1217 (BillingTransaction_t8_211 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CRawPurchaseDataU3Ek__BackingField_8);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.BillingTransaction::set_RawPurchaseData(System.String)
extern "C" void BillingTransaction_set_RawPurchaseData_m8_1218 (BillingTransaction_t8_211 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CRawPurchaseDataU3Ek__BackingField_8 = L_0;
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.BillingTransaction::UpdateVerificationState(VoxelBusters.NativePlugins.eBillingTransactionVerificationState)
extern "C" void BillingTransaction_UpdateVerificationState_m8_1219 (BillingTransaction_t8_211 * __this, int32_t ____newState, const MethodInfo* method)
{
	{
		int32_t L_0 = ____newState;
		BillingTransaction_set_VerificationState_m8_1214(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String VoxelBusters.NativePlugins.BillingTransaction::ToString()
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* DateTime_t1_150_il2cpp_TypeInfo_var;
extern TypeInfo* eBillingTransactionState_t8_203_il2cpp_TypeInfo_var;
extern TypeInfo* eBillingTransactionVerificationState_t8_204_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5405;
extern "C" String_t* BillingTransaction_ToString_m8_1220 (BillingTransaction_t8_211 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		DateTime_t1_150_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		eBillingTransactionState_t8_203_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2119);
		eBillingTransactionVerificationState_t8_204_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2118);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5405 = il2cpp_codegen_string_literal_from_index(5405);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_272* L_0 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 6));
		String_t* L_1 = BillingTransaction_get_ProductIdentifier_m8_1201(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_1;
		ObjectU5BU5D_t1_272* L_2 = L_0;
		DateTime_t1_150  L_3 = BillingTransaction_get_TransactionDateUTC_m8_1203(__this, /*hidden argument*/NULL);
		DateTime_t1_150  L_4 = L_3;
		Object_t * L_5 = Box(DateTime_t1_150_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_5);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1, sizeof(Object_t *))) = (Object_t *)L_5;
		ObjectU5BU5D_t1_272* L_6 = L_2;
		String_t* L_7 = BillingTransaction_get_TransactionIdentifier_m8_1207(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 2, sizeof(Object_t *))) = (Object_t *)L_7;
		ObjectU5BU5D_t1_272* L_8 = L_6;
		int32_t L_9 = BillingTransaction_get_TransactionState_m8_1211(__this, /*hidden argument*/NULL);
		int32_t L_10 = L_9;
		Object_t * L_11 = Box(eBillingTransactionState_t8_203_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 3);
		ArrayElementTypeCheck (L_8, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 3, sizeof(Object_t *))) = (Object_t *)L_11;
		ObjectU5BU5D_t1_272* L_12 = L_8;
		int32_t L_13 = BillingTransaction_get_VerificationState_m8_1213(__this, /*hidden argument*/NULL);
		int32_t L_14 = L_13;
		Object_t * L_15 = Box(eBillingTransactionVerificationState_t8_204_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 4, sizeof(Object_t *))) = (Object_t *)L_15;
		ObjectU5BU5D_t1_272* L_16 = L_12;
		String_t* L_17 = BillingTransaction_get_Error_m8_1215(__this, /*hidden argument*/NULL);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 5);
		ArrayElementTypeCheck (L_16, L_17);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_16, 5, sizeof(Object_t *))) = (Object_t *)L_17;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Format_m1_551(NULL /*static, unused*/, _stringLiteral5405, L_16, /*hidden argument*/NULL);
		return L_18;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.EditorBillingTransaction::.ctor(System.String,System.DateTime,System.String,System.String,VoxelBusters.NativePlugins.eBillingTransactionState,VoxelBusters.NativePlugins.eBillingTransactionVerificationState,System.String)
extern "C" void EditorBillingTransaction__ctor_m8_1221 (EditorBillingTransaction_t8_212 * __this, String_t* ____productID, DateTime_t1_150  ____timeUTC, String_t* ____transactionID, String_t* ____receipt, int32_t ____transactionState, int32_t ____verificationState, String_t* ____error, const MethodInfo* method)
{
	DateTime_t1_150  V_0 = {0};
	{
		BillingTransaction__ctor_m8_1200(__this, /*hidden argument*/NULL);
		String_t* L_0 = ____productID;
		BillingTransaction_set_ProductIdentifier_m8_1202(__this, L_0, /*hidden argument*/NULL);
		DateTime_t1_150  L_1 = ____timeUTC;
		BillingTransaction_set_TransactionDateUTC_m8_1204(__this, L_1, /*hidden argument*/NULL);
		DateTime_t1_150  L_2 = BillingTransaction_get_TransactionDateUTC_m8_1203(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		DateTime_t1_150  L_3 = DateTime_ToLocalTime_m1_13900((&V_0), /*hidden argument*/NULL);
		BillingTransaction_set_TransactionDateLocal_m8_1206(__this, L_3, /*hidden argument*/NULL);
		String_t* L_4 = ____transactionID;
		BillingTransaction_set_TransactionIdentifier_m8_1208(__this, L_4, /*hidden argument*/NULL);
		String_t* L_5 = ____receipt;
		BillingTransaction_set_TransactionReceipt_m8_1210(__this, L_5, /*hidden argument*/NULL);
		int32_t L_6 = ____transactionState;
		BillingTransaction_set_TransactionState_m8_1212(__this, L_6, /*hidden argument*/NULL);
		int32_t L_7 = ____verificationState;
		BillingTransaction_set_VerificationState_m8_1214(__this, L_7, /*hidden argument*/NULL);
		String_t* L_8 = ____error;
		BillingTransaction_set_Error_m8_1216(__this, L_8, /*hidden argument*/NULL);
		Object_t * L_9 = EditorBillingTransaction_ToJSONObject_m8_1222(__this, /*hidden argument*/NULL);
		String_t* L_10 = JSONParserExtensions_ToJSON_m8_269(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		BillingTransaction_set_RawPurchaseData_m8_1218(__this, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IDictionary VoxelBusters.NativePlugins.Internal.EditorBillingTransaction::ToJSONObject()
extern "C" Object_t * EditorBillingTransaction_ToJSONObject_m8_1222 (EditorBillingTransaction_t8_212 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = iOSBillingTransaction_CreateJSONObject_m8_1224(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.iOSBillingTransaction::.ctor(System.Collections.IDictionary)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionary_t1_35_il2cpp_TypeInfo_var;
extern const MethodInfo* IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral5389;
extern Il2CppCodeGenString* _stringLiteral5406;
extern Il2CppCodeGenString* _stringLiteral5407;
extern Il2CppCodeGenString* _stringLiteral5408;
extern Il2CppCodeGenString* _stringLiteral5409;
extern Il2CppCodeGenString* _stringLiteral5410;
extern Il2CppCodeGenString* _stringLiteral5241;
extern "C" void iOSBillingTransaction__ctor_m8_1223 (iOSBillingTransaction_t8_214 * __this, Object_t * ____transactionJsonDict, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		IDictionary_t1_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484117);
		_stringLiteral5389 = il2cpp_codegen_string_literal_from_index(5389);
		_stringLiteral5406 = il2cpp_codegen_string_literal_from_index(5406);
		_stringLiteral5407 = il2cpp_codegen_string_literal_from_index(5407);
		_stringLiteral5408 = il2cpp_codegen_string_literal_from_index(5408);
		_stringLiteral5409 = il2cpp_codegen_string_literal_from_index(5409);
		_stringLiteral5410 = il2cpp_codegen_string_literal_from_index(5410);
		_stringLiteral5241 = il2cpp_codegen_string_literal_from_index(5241);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	int32_t V_1 = {0};
	DateTime_t1_150  V_2 = {0};
	{
		BillingTransaction__ctor_m8_1200(__this, /*hidden argument*/NULL);
		Object_t * L_0 = ____transactionJsonDict;
		String_t* L_1 = IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(NULL /*static, unused*/, L_0, _stringLiteral5389, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var);
		BillingTransaction_set_ProductIdentifier_m8_1202(__this, L_1, /*hidden argument*/NULL);
		Object_t * L_2 = ____transactionJsonDict;
		String_t* L_3 = IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(NULL /*static, unused*/, L_2, _stringLiteral5406, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var);
		V_0 = L_3;
		String_t* L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_IsNullOrEmpty_m1_521(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_004e;
		}
	}
	{
		String_t* L_6 = V_0;
		DateTime_t1_150  L_7 = DateTimeExtensions_ToZuluFormatDateTimeUTC_m8_185(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		BillingTransaction_set_TransactionDateUTC_m8_1204(__this, L_7, /*hidden argument*/NULL);
		DateTime_t1_150  L_8 = BillingTransaction_get_TransactionDateUTC_m8_1203(__this, /*hidden argument*/NULL);
		V_2 = L_8;
		DateTime_t1_150  L_9 = DateTime_ToLocalTime_m1_13900((&V_2), /*hidden argument*/NULL);
		BillingTransaction_set_TransactionDateLocal_m8_1206(__this, L_9, /*hidden argument*/NULL);
	}

IL_004e:
	{
		Object_t * L_10 = ____transactionJsonDict;
		String_t* L_11 = IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(NULL /*static, unused*/, L_10, _stringLiteral5407, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var);
		BillingTransaction_set_TransactionIdentifier_m8_1208(__this, L_11, /*hidden argument*/NULL);
		Object_t * L_12 = ____transactionJsonDict;
		String_t* L_13 = IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(NULL /*static, unused*/, L_12, _stringLiteral5408, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var);
		BillingTransaction_set_TransactionReceipt_m8_1210(__this, L_13, /*hidden argument*/NULL);
		Object_t * L_14 = ____transactionJsonDict;
		NullCheck(L_14);
		Object_t * L_15 = (Object_t *)InterfaceFuncInvoker1< Object_t *, Object_t * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_14, _stringLiteral5409);
		NullCheck(L_15);
		String_t* L_16 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_15);
		int32_t L_17 = Int32_Parse_m1_97(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		int32_t L_18 = V_1;
		int32_t L_19 = iOSBillingTransaction_ConvertToBillingTransactionState_m8_1225(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		BillingTransaction_set_TransactionState_m8_1212(__this, L_19, /*hidden argument*/NULL);
		Object_t * L_20 = ____transactionJsonDict;
		NullCheck(L_20);
		Object_t * L_21 = (Object_t *)InterfaceFuncInvoker1< Object_t *, Object_t * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_20, _stringLiteral5410);
		NullCheck(L_21);
		String_t* L_22 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_21);
		int32_t L_23 = Int32_Parse_m1_97(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		BillingTransaction_set_VerificationState_m8_1214(__this, L_23, /*hidden argument*/NULL);
		Object_t * L_24 = ____transactionJsonDict;
		String_t* L_25 = IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(NULL /*static, unused*/, L_24, _stringLiteral5241, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var);
		BillingTransaction_set_Error_m8_1216(__this, L_25, /*hidden argument*/NULL);
		Object_t * L_26 = ____transactionJsonDict;
		String_t* L_27 = JSONParserExtensions_ToJSON_m8_269(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		BillingTransaction_set_RawPurchaseData_m8_1218(__this, L_27, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IDictionary VoxelBusters.NativePlugins.Internal.iOSBillingTransaction::CreateJSONObject(VoxelBusters.NativePlugins.BillingTransaction)
extern TypeInfo* Dictionary_2_t1_1903_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionary_t1_35_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_15037_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral5406;
extern Il2CppCodeGenString* _stringLiteral5410;
extern Il2CppCodeGenString* _stringLiteral5407;
extern Il2CppCodeGenString* _stringLiteral5408;
extern Il2CppCodeGenString* _stringLiteral5409;
extern Il2CppCodeGenString* _stringLiteral5389;
extern Il2CppCodeGenString* _stringLiteral5241;
extern "C" Object_t * iOSBillingTransaction_CreateJSONObject_m8_1224 (Object_t * __this /* static, unused */, BillingTransaction_t8_211 * ____transaction, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t1_1903_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1960);
		IDictionary_t1_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		Dictionary_2__ctor_m1_15037_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484124);
		_stringLiteral5406 = il2cpp_codegen_string_literal_from_index(5406);
		_stringLiteral5410 = il2cpp_codegen_string_literal_from_index(5410);
		_stringLiteral5407 = il2cpp_codegen_string_literal_from_index(5407);
		_stringLiteral5408 = il2cpp_codegen_string_literal_from_index(5408);
		_stringLiteral5409 = il2cpp_codegen_string_literal_from_index(5409);
		_stringLiteral5389 = il2cpp_codegen_string_literal_from_index(5389);
		_stringLiteral5241 = il2cpp_codegen_string_literal_from_index(5241);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	Object_t * V_1 = {0};
	{
		BillingTransaction_t8_211 * L_0 = ____transaction;
		NullCheck(L_0);
		int32_t L_1 = BillingTransaction_get_TransactionState_m8_1211(L_0, /*hidden argument*/NULL);
		int32_t L_2 = iOSBillingTransaction_ConvertToSKTransactionState_m8_1226(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Dictionary_2_t1_1903 * L_3 = (Dictionary_2_t1_1903 *)il2cpp_codegen_object_new (Dictionary_2_t1_1903_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_15037(L_3, /*hidden argument*/Dictionary_2__ctor_m1_15037_MethodInfo_var);
		V_1 = L_3;
		Object_t * L_4 = V_1;
		BillingTransaction_t8_211 * L_5 = ____transaction;
		NullCheck(L_5);
		DateTime_t1_150  L_6 = BillingTransaction_get_TransactionDateUTC_m8_1203(L_5, /*hidden argument*/NULL);
		String_t* L_7 = DateTimeExtensions_ToStringUsingZuluFormat_m8_187(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		NullCheck(L_4);
		InterfaceActionInvoker2< Object_t *, Object_t * >::Invoke(3 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_4, _stringLiteral5406, L_7);
		Object_t * L_8 = V_1;
		BillingTransaction_t8_211 * L_9 = ____transaction;
		NullCheck(L_9);
		int32_t L_10 = BillingTransaction_get_VerificationState_m8_1213(L_9, /*hidden argument*/NULL);
		int32_t L_11 = L_10;
		Object_t * L_12 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_8);
		InterfaceActionInvoker2< Object_t *, Object_t * >::Invoke(3 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_8, _stringLiteral5410, L_12);
		Object_t * L_13 = V_1;
		BillingTransaction_t8_211 * L_14 = ____transaction;
		NullCheck(L_14);
		String_t* L_15 = BillingTransaction_get_TransactionIdentifier_m8_1207(L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		InterfaceActionInvoker2< Object_t *, Object_t * >::Invoke(3 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_13, _stringLiteral5407, L_15);
		Object_t * L_16 = V_1;
		BillingTransaction_t8_211 * L_17 = ____transaction;
		NullCheck(L_17);
		String_t* L_18 = BillingTransaction_get_TransactionReceipt_m8_1209(L_17, /*hidden argument*/NULL);
		NullCheck(L_16);
		InterfaceActionInvoker2< Object_t *, Object_t * >::Invoke(3 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_16, _stringLiteral5408, L_18);
		Object_t * L_19 = V_1;
		int32_t L_20 = V_0;
		int32_t L_21 = L_20;
		Object_t * L_22 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_21);
		NullCheck(L_19);
		InterfaceActionInvoker2< Object_t *, Object_t * >::Invoke(3 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_19, _stringLiteral5409, L_22);
		Object_t * L_23 = V_1;
		BillingTransaction_t8_211 * L_24 = ____transaction;
		NullCheck(L_24);
		String_t* L_25 = BillingTransaction_get_ProductIdentifier_m8_1201(L_24, /*hidden argument*/NULL);
		NullCheck(L_23);
		InterfaceActionInvoker2< Object_t *, Object_t * >::Invoke(3 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_23, _stringLiteral5389, L_25);
		Object_t * L_26 = V_1;
		BillingTransaction_t8_211 * L_27 = ____transaction;
		NullCheck(L_27);
		String_t* L_28 = BillingTransaction_get_Error_m8_1215(L_27, /*hidden argument*/NULL);
		NullCheck(L_26);
		InterfaceActionInvoker2< Object_t *, Object_t * >::Invoke(3 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_26, _stringLiteral5241, L_28);
		Object_t * L_29 = V_1;
		return L_29;
	}
}
// VoxelBusters.NativePlugins.eBillingTransactionState VoxelBusters.NativePlugins.Internal.iOSBillingTransaction::ConvertToBillingTransactionState(VoxelBusters.NativePlugins.Internal.iOSBillingTransaction/SKPaymentTransactionState)
extern "C" int32_t iOSBillingTransaction_ConvertToBillingTransactionState_m8_1225 (Object_t * __this /* static, unused */, int32_t ____skTransactionState, const MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		int32_t L_0 = ____skTransactionState;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 0)
		{
			goto IL_001b;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 1)
		{
			goto IL_001d;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 2)
		{
			goto IL_001f;
		}
	}
	{
		goto IL_0021;
	}

IL_001b:
	{
		return (int32_t)(0);
	}

IL_001d:
	{
		return (int32_t)(1);
	}

IL_001f:
	{
		return (int32_t)(2);
	}

IL_0021:
	{
		return (int32_t)(1);
	}
}
// VoxelBusters.NativePlugins.Internal.iOSBillingTransaction/SKPaymentTransactionState VoxelBusters.NativePlugins.Internal.iOSBillingTransaction::ConvertToSKTransactionState(VoxelBusters.NativePlugins.eBillingTransactionState)
extern "C" int32_t iOSBillingTransaction_ConvertToSKTransactionState_m8_1226 (Object_t * __this /* static, unused */, int32_t ____billingTransactionState, const MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		int32_t L_0 = ____billingTransactionState;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0019;
		}
		if (L_1 == 1)
		{
			goto IL_001b;
		}
		if (L_1 == 2)
		{
			goto IL_001d;
		}
	}
	{
		goto IL_001f;
	}

IL_0019:
	{
		return (int32_t)(1);
	}

IL_001b:
	{
		return (int32_t)(2);
	}

IL_001d:
	{
		return (int32_t)(3);
	}

IL_001f:
	{
		return (int32_t)(2);
	}
}
// System.Void VoxelBusters.NativePlugins.BillingSettings/AndroidSettings::.ctor()
extern "C" void AndroidSettings__ctor_m8_1227 (AndroidSettings_t8_215 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String VoxelBusters.NativePlugins.BillingSettings/AndroidSettings::get_PublicKey()
extern "C" String_t* AndroidSettings_get_PublicKey_m8_1228 (AndroidSettings_t8_215 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_publicKey_0);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.BillingSettings/AndroidSettings::set_PublicKey(System.String)
extern "C" void AndroidSettings_set_PublicKey_m8_1229 (AndroidSettings_t8_215 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_publicKey_0 = L_0;
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.BillingSettings/iOSSettings::.ctor()
extern "C" void iOSSettings__ctor_m8_1230 (iOSSettings_t8_216 * __this, const MethodInfo* method)
{
	{
		__this->___m_supportsReceiptValidation_0 = 1;
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean VoxelBusters.NativePlugins.BillingSettings/iOSSettings::get_SupportsReceiptValidation()
extern "C" bool iOSSettings_get_SupportsReceiptValidation_m8_1231 (iOSSettings_t8_216 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_supportsReceiptValidation_0);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.BillingSettings/iOSSettings::set_SupportsReceiptValidation(System.Boolean)
extern "C" void iOSSettings_set_SupportsReceiptValidation_m8_1232 (iOSSettings_t8_216 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___m_supportsReceiptValidation_0 = L_0;
		return;
	}
}
// System.String VoxelBusters.NativePlugins.BillingSettings/iOSSettings::get_ValidateUsingServerURL()
extern "C" String_t* iOSSettings_get_ValidateUsingServerURL_m8_1233 (iOSSettings_t8_216 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_validateUsingServerURL_1);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.BillingSettings/iOSSettings::set_ValidateUsingServerURL(System.String)
extern "C" void iOSSettings_set_ValidateUsingServerURL_m8_1234 (iOSSettings_t8_216 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_validateUsingServerURL_1 = L_0;
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.BillingSettings::.ctor()
extern TypeInfo* BillingProductU5BU5D_t8_337_il2cpp_TypeInfo_var;
extern TypeInfo* iOSSettings_t8_216_il2cpp_TypeInfo_var;
extern TypeInfo* AndroidSettings_t8_215_il2cpp_TypeInfo_var;
extern "C" void BillingSettings__ctor_m8_1235 (BillingSettings_t8_217 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		BillingProductU5BU5D_t8_337_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2120);
		iOSSettings_t8_216_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2121);
		AndroidSettings_t8_215_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2122);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		BillingSettings_set_Products_m8_1237(__this, ((BillingProductU5BU5D_t8_337*)SZArrayNew(BillingProductU5BU5D_t8_337_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		iOSSettings_t8_216 * L_0 = (iOSSettings_t8_216 *)il2cpp_codegen_object_new (iOSSettings_t8_216_il2cpp_TypeInfo_var);
		iOSSettings__ctor_m8_1230(L_0, /*hidden argument*/NULL);
		BillingSettings_set_iOS_m8_1239(__this, L_0, /*hidden argument*/NULL);
		AndroidSettings_t8_215 * L_1 = (AndroidSettings_t8_215 *)il2cpp_codegen_object_new (AndroidSettings_t8_215_il2cpp_TypeInfo_var);
		AndroidSettings__ctor_m8_1227(L_1, /*hidden argument*/NULL);
		BillingSettings_set_Android_m8_1241(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// VoxelBusters.NativePlugins.BillingProduct[] VoxelBusters.NativePlugins.BillingSettings::get_Products()
extern const MethodInfo* List_1_ToArray_m1_15064_MethodInfo_var;
extern "C" BillingProductU5BU5D_t8_337* BillingSettings_get_Products_m8_1236 (BillingSettings_t8_217 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_ToArray_m1_15064_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484226);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1_1910 * L_0 = (__this->___m_products_0);
		NullCheck(L_0);
		BillingProductU5BU5D_t8_337* L_1 = List_1_ToArray_m1_15064(L_0, /*hidden argument*/List_1_ToArray_m1_15064_MethodInfo_var);
		return L_1;
	}
}
// System.Void VoxelBusters.NativePlugins.BillingSettings::set_Products(VoxelBusters.NativePlugins.BillingProduct[])
extern TypeInfo* List_1_t1_1910_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1_15065_MethodInfo_var;
extern "C" void BillingSettings_set_Products_m8_1237 (BillingSettings_t8_217 * __this, BillingProductU5BU5D_t8_337* ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t1_1910_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2123);
		List_1__ctor_m1_15065_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484227);
		s_Il2CppMethodIntialized = true;
	}
	{
		BillingProductU5BU5D_t8_337* L_0 = ___value;
		List_1_t1_1910 * L_1 = (List_1_t1_1910 *)il2cpp_codegen_object_new (List_1_t1_1910_il2cpp_TypeInfo_var);
		List_1__ctor_m1_15065(L_1, (Object_t*)(Object_t*)L_0, /*hidden argument*/List_1__ctor_m1_15065_MethodInfo_var);
		__this->___m_products_0 = L_1;
		return;
	}
}
// VoxelBusters.NativePlugins.BillingSettings/iOSSettings VoxelBusters.NativePlugins.BillingSettings::get_iOS()
extern "C" iOSSettings_t8_216 * BillingSettings_get_iOS_m8_1238 (BillingSettings_t8_217 * __this, const MethodInfo* method)
{
	{
		iOSSettings_t8_216 * L_0 = (__this->___m_iOS_1);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.BillingSettings::set_iOS(VoxelBusters.NativePlugins.BillingSettings/iOSSettings)
extern "C" void BillingSettings_set_iOS_m8_1239 (BillingSettings_t8_217 * __this, iOSSettings_t8_216 * ___value, const MethodInfo* method)
{
	{
		iOSSettings_t8_216 * L_0 = ___value;
		__this->___m_iOS_1 = L_0;
		return;
	}
}
// VoxelBusters.NativePlugins.BillingSettings/AndroidSettings VoxelBusters.NativePlugins.BillingSettings::get_Android()
extern "C" AndroidSettings_t8_215 * BillingSettings_get_Android_m8_1240 (BillingSettings_t8_217 * __this, const MethodInfo* method)
{
	{
		AndroidSettings_t8_215 * L_0 = (__this->___m_android_2);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.BillingSettings::set_Android(VoxelBusters.NativePlugins.BillingSettings/AndroidSettings)
extern "C" void BillingSettings_set_Android_m8_1241 (BillingSettings_t8_217 * __this, AndroidSettings_t8_215 * ___value, const MethodInfo* method)
{
	{
		AndroidSettings_t8_215 * L_0 = ___value;
		__this->___m_android_2 = L_0;
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Achievement/LoadAchievementsCompletion::.ctor(System.Object,System.IntPtr)
extern "C" void LoadAchievementsCompletion__ctor_m8_1242 (LoadAchievementsCompletion_t8_218 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void VoxelBusters.NativePlugins.Achievement/LoadAchievementsCompletion::Invoke(VoxelBusters.NativePlugins.Achievement[],System.String)
extern "C" void LoadAchievementsCompletion_Invoke_m8_1243 (LoadAchievementsCompletion_t8_218 * __this, AchievementU5BU5D_t8_219* ____achievements, String_t* ____error, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		LoadAchievementsCompletion_Invoke_m8_1243((LoadAchievementsCompletion_t8_218 *)__this->___prev_9,____achievements, ____error, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, AchievementU5BU5D_t8_219* ____achievements, String_t* ____error, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,____achievements, ____error,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, AchievementU5BU5D_t8_219* ____achievements, String_t* ____error, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,____achievements, ____error,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, String_t* ____error, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(____achievements, ____error,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_LoadAchievementsCompletion_t8_218(Il2CppObject* delegate, AchievementU5BU5D_t8_219* ____achievements, String_t* ____error)
{
	// Marshaling of parameter '____achievements' to native representation
	AchievementU5BU5D_t8_219* _____achievements_marshaled = { 0 };
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'VoxelBusters.NativePlugins.Achievement[]'."));
}
// System.IAsyncResult VoxelBusters.NativePlugins.Achievement/LoadAchievementsCompletion::BeginInvoke(VoxelBusters.NativePlugins.Achievement[],System.String,System.AsyncCallback,System.Object)
extern "C" Object_t * LoadAchievementsCompletion_BeginInvoke_m8_1244 (LoadAchievementsCompletion_t8_218 * __this, AchievementU5BU5D_t8_219* ____achievements, String_t* ____error, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ____achievements;
	__d_args[1] = ____error;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void VoxelBusters.NativePlugins.Achievement/LoadAchievementsCompletion::EndInvoke(System.IAsyncResult)
extern "C" void LoadAchievementsCompletion_EndInvoke_m8_1245 (LoadAchievementsCompletion_t8_218 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void VoxelBusters.NativePlugins.Achievement/ReportProgressCompletion::.ctor(System.Object,System.IntPtr)
extern "C" void ReportProgressCompletion__ctor_m8_1246 (ReportProgressCompletion_t8_220 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void VoxelBusters.NativePlugins.Achievement/ReportProgressCompletion::Invoke(System.Boolean,System.String)
extern "C" void ReportProgressCompletion_Invoke_m8_1247 (ReportProgressCompletion_t8_220 * __this, bool ____success, String_t* ____error, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		ReportProgressCompletion_Invoke_m8_1247((ReportProgressCompletion_t8_220 *)__this->___prev_9,____success, ____error, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, bool ____success, String_t* ____error, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,____success, ____error,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, bool ____success, String_t* ____error, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,____success, ____error,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_ReportProgressCompletion_t8_220(Il2CppObject* delegate, bool ____success, String_t* ____error)
{
	typedef void (STDCALL *native_function_ptr_type)(int32_t, char*);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '____success' to native representation

	// Marshaling of parameter '____error' to native representation
	char* _____error_marshaled = { 0 };
	_____error_marshaled = il2cpp_codegen_marshal_string(____error);

	// Native function invocation
	_il2cpp_pinvoke_func(____success, _____error_marshaled);

	// Marshaling cleanup of parameter '____success' native representation

	// Marshaling cleanup of parameter '____error' native representation
	il2cpp_codegen_marshal_free(_____error_marshaled);
	_____error_marshaled = NULL;

}
// System.IAsyncResult VoxelBusters.NativePlugins.Achievement/ReportProgressCompletion::BeginInvoke(System.Boolean,System.String,System.AsyncCallback,System.Object)
extern TypeInfo* Boolean_t1_20_il2cpp_TypeInfo_var;
extern "C" Object_t * ReportProgressCompletion_BeginInvoke_m8_1248 (ReportProgressCompletion_t8_220 * __this, bool ____success, String_t* ____error, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Boolean_t1_20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(54);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Boolean_t1_20_il2cpp_TypeInfo_var, &____success);
	__d_args[1] = ____error;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void VoxelBusters.NativePlugins.Achievement/ReportProgressCompletion::EndInvoke(System.IAsyncResult)
extern "C" void ReportProgressCompletion_EndInvoke_m8_1249 (ReportProgressCompletion_t8_220 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void VoxelBusters.NativePlugins.Achievement::.ctor()
extern "C" void Achievement__ctor_m8_1250 (Achievement_t8_221 * __this, const MethodInfo* method)
{
	{
		NPObject__ctor_m8_1903(__this, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Achievement::.ctor(System.String,System.String,System.Int32,System.Boolean,System.DateTime)
extern "C" void Achievement__ctor_m8_1251 (Achievement_t8_221 * __this, String_t* ____globalIdentifier, String_t* ____identifier, int32_t ____pointsScored, bool ____completed, DateTime_t1_150  ____reportedDate, const MethodInfo* method)
{
	{
		NPObject__ctor_m8_1903(__this, 0, /*hidden argument*/NULL);
		String_t* L_0 = ____globalIdentifier;
		Achievement_set_GlobalIdentifier_m8_1256(__this, L_0, /*hidden argument*/NULL);
		String_t* L_1 = ____identifier;
		VirtActionInvoker1< String_t* >::Invoke(5 /* System.Void VoxelBusters.NativePlugins.Achievement::set_Identifier(System.String) */, __this, L_1);
		int32_t L_2 = ____pointsScored;
		VirtActionInvoker1< int32_t >::Invoke(7 /* System.Void VoxelBusters.NativePlugins.Achievement::set_PointsScored(System.Int32) */, __this, L_2);
		bool L_3 = ____completed;
		VirtActionInvoker1< bool >::Invoke(9 /* System.Void VoxelBusters.NativePlugins.Achievement::set_Completed(System.Boolean) */, __this, L_3);
		DateTime_t1_150  L_4 = ____reportedDate;
		VirtActionInvoker1< DateTime_t1_150  >::Invoke(11 /* System.Void VoxelBusters.NativePlugins.Achievement::set_LastReportedDate(System.DateTime) */, __this, L_4);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Achievement::.ctor(System.String,System.String,System.Int32)
extern TypeInfo* DateTime_t1_150_il2cpp_TypeInfo_var;
extern "C" void Achievement__ctor_m8_1252 (Achievement_t8_221 * __this, String_t* ____globalIdentifier, String_t* ____identifier, int32_t ____pointsScored, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DateTime_t1_150_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ____globalIdentifier;
		String_t* L_1 = ____identifier;
		int32_t L_2 = ____pointsScored;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		DateTime_t1_150  L_3 = DateTime_get_Now_m1_13826(NULL /*static, unused*/, /*hidden argument*/NULL);
		Achievement__ctor_m8_1251(__this, L_0, L_1, L_2, 0, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Achievement::add_ReportProgressFinishedEvent(VoxelBusters.NativePlugins.Achievement/ReportProgressCompletion)
extern TypeInfo* ReportProgressCompletion_t8_220_il2cpp_TypeInfo_var;
extern "C" void Achievement_add_ReportProgressFinishedEvent_m8_1253 (Achievement_t8_221 * __this, ReportProgressCompletion_t8_220 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReportProgressCompletion_t8_220_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2124);
		s_Il2CppMethodIntialized = true;
	}
	{
		ReportProgressCompletion_t8_220 * L_0 = (__this->___ReportProgressFinishedEvent_1);
		ReportProgressCompletion_t8_220 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Combine_m1_908(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->___ReportProgressFinishedEvent_1 = ((ReportProgressCompletion_t8_220 *)CastclassSealed(L_2, ReportProgressCompletion_t8_220_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Achievement::remove_ReportProgressFinishedEvent(VoxelBusters.NativePlugins.Achievement/ReportProgressCompletion)
extern TypeInfo* ReportProgressCompletion_t8_220_il2cpp_TypeInfo_var;
extern "C" void Achievement_remove_ReportProgressFinishedEvent_m8_1254 (Achievement_t8_221 * __this, ReportProgressCompletion_t8_220 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReportProgressCompletion_t8_220_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2124);
		s_Il2CppMethodIntialized = true;
	}
	{
		ReportProgressCompletion_t8_220 * L_0 = (__this->___ReportProgressFinishedEvent_1);
		ReportProgressCompletion_t8_220 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Remove_m1_911(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->___ReportProgressFinishedEvent_1 = ((ReportProgressCompletion_t8_220 *)CastclassSealed(L_2, ReportProgressCompletion_t8_220_il2cpp_TypeInfo_var));
		return;
	}
}
// System.String VoxelBusters.NativePlugins.Achievement::get_GlobalIdentifier()
extern "C" String_t* Achievement_get_GlobalIdentifier_m8_1255 (Achievement_t8_221 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CGlobalIdentifierU3Ek__BackingField_2);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.Achievement::set_GlobalIdentifier(System.String)
extern "C" void Achievement_set_GlobalIdentifier_m8_1256 (Achievement_t8_221 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CGlobalIdentifierU3Ek__BackingField_2 = L_0;
		return;
	}
}
// System.Double VoxelBusters.NativePlugins.Achievement::get_PercentageCompleted()
extern "C" double Achievement_get_PercentageCompleted_m8_1257 (Achievement_t8_221 * __this, const MethodInfo* method)
{
	double V_0 = 0.0;
	{
		AchievementDescription_t8_225 * L_0 = Achievement_get_Description_m8_1258(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		return (0.0);
	}

IL_0015:
	{
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 VoxelBusters.NativePlugins.Achievement::get_PointsScored() */, __this);
		AchievementDescription_t8_225 * L_2 = Achievement_get_Description_m8_1258(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(12 /* System.Int32 VoxelBusters.NativePlugins.AchievementDescription::get_MaximumPoints() */, L_2);
		V_0 = ((double)((double)((double)((double)(((double)((double)L_1)))*(double)(100.0)))/(double)(((double)((double)L_3)))));
		double L_4 = V_0;
		double L_5 = Math_Min_m1_14216(NULL /*static, unused*/, (100.0), L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// VoxelBusters.NativePlugins.AchievementDescription VoxelBusters.NativePlugins.Achievement::get_Description()
extern TypeInfo* AchievementHandler_t8_327_il2cpp_TypeInfo_var;
extern "C" AchievementDescription_t8_225 * Achievement_get_Description_m8_1258 (Achievement_t8_221 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AchievementHandler_t8_327_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2125);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String VoxelBusters.NativePlugins.Achievement::get_Identifier() */, __this);
		IL2CPP_RUNTIME_CLASS_INIT(AchievementHandler_t8_327_il2cpp_TypeInfo_var);
		AchievementDescription_t8_225 * L_1 = AchievementHandler_GetAchievementDescription_m8_1911(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void VoxelBusters.NativePlugins.Achievement::ReportProgress(VoxelBusters.NativePlugins.Achievement/ReportProgressCompletion)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5411;
extern Il2CppCodeGenString* _stringLiteral5412;
extern "C" void Achievement_ReportProgress_m8_1259 (Achievement_t8_221 * __this, ReportProgressCompletion_t8_220 * ____onCompletion, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5411 = il2cpp_codegen_string_literal_from_index(5411);
		_stringLiteral5412 = il2cpp_codegen_string_literal_from_index(5412);
		s_Il2CppMethodIntialized = true;
	}
	{
		ReportProgressCompletion_t8_220 * L_0 = ____onCompletion;
		__this->___ReportProgressFinishedEvent_1 = L_0;
		AchievementDescription_t8_225 * L_1 = Achievement_get_Description_m8_1258(__this, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_003a;
		}
	}
	{
		String_t* L_2 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String VoxelBusters.NativePlugins.Achievement::get_Identifier() */, __this);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Format_m1_548(NULL /*static, unused*/, _stringLiteral5411, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_LogError_m8_1009(NULL /*static, unused*/, _stringLiteral5368, L_3, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		Achievement_ReportProgressFinished_m8_1262(__this, 0, _stringLiteral5412, /*hidden argument*/NULL);
		return;
	}

IL_003a:
	{
		return;
	}
}
// System.String VoxelBusters.NativePlugins.Achievement::ToString()
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t1_20_il2cpp_TypeInfo_var;
extern TypeInfo* DateTime_t1_150_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5413;
extern "C" String_t* Achievement_ToString_m8_1260 (Achievement_t8_221 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		Boolean_t1_20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(54);
		DateTime_t1_150_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5413 = il2cpp_codegen_string_literal_from_index(5413);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_272* L_0 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 4));
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String VoxelBusters.NativePlugins.Achievement::get_Identifier() */, __this);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_1;
		ObjectU5BU5D_t1_272* L_2 = L_0;
		int32_t L_3 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 VoxelBusters.NativePlugins.Achievement::get_PointsScored() */, __this);
		int32_t L_4 = L_3;
		Object_t * L_5 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_5);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1, sizeof(Object_t *))) = (Object_t *)L_5;
		ObjectU5BU5D_t1_272* L_6 = L_2;
		bool L_7 = (bool)VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean VoxelBusters.NativePlugins.Achievement::get_Completed() */, __this);
		bool L_8 = L_7;
		Object_t * L_9 = Box(Boolean_t1_20_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, L_9);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 2, sizeof(Object_t *))) = (Object_t *)L_9;
		ObjectU5BU5D_t1_272* L_10 = L_6;
		DateTime_t1_150  L_11 = (DateTime_t1_150 )VirtFuncInvoker0< DateTime_t1_150  >::Invoke(10 /* System.DateTime VoxelBusters.NativePlugins.Achievement::get_LastReportedDate() */, __this);
		DateTime_t1_150  L_12 = L_11;
		Object_t * L_13 = Box(DateTime_t1_150_il2cpp_TypeInfo_var, &L_12);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 3);
		ArrayElementTypeCheck (L_10, L_13);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_10, 3, sizeof(Object_t *))) = (Object_t *)L_13;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Format_m1_551(NULL /*static, unused*/, _stringLiteral5413, L_10, /*hidden argument*/NULL);
		return L_14;
	}
}
// System.Void VoxelBusters.NativePlugins.Achievement::ReportProgressFinished(System.Collections.IDictionary)
extern "C" void Achievement_ReportProgressFinished_m8_1261 (Achievement_t8_221 * __this, Object_t * ____dataDict, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Achievement::ReportProgressFinished(System.Boolean,System.String)
extern "C" void Achievement_ReportProgressFinished_m8_1262 (Achievement_t8_221 * __this, bool ____success, String_t* ____error, const MethodInfo* method)
{
	{
		ReportProgressCompletion_t8_220 * L_0 = (__this->___ReportProgressFinishedEvent_1);
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		ReportProgressCompletion_t8_220 * L_1 = (__this->___ReportProgressFinishedEvent_1);
		bool L_2 = ____success;
		String_t* L_3 = ____error;
		NullCheck(L_1);
		ReportProgressCompletion_Invoke_m8_1247(L_1, L_2, L_3, /*hidden argument*/NULL);
	}

IL_0018:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.AchievementDescription/LoadAchievementDescriptionsCompletion::.ctor(System.Object,System.IntPtr)
extern "C" void LoadAchievementDescriptionsCompletion__ctor_m8_1263 (LoadAchievementDescriptionsCompletion_t8_223 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void VoxelBusters.NativePlugins.AchievementDescription/LoadAchievementDescriptionsCompletion::Invoke(VoxelBusters.NativePlugins.AchievementDescription[],System.String)
extern "C" void LoadAchievementDescriptionsCompletion_Invoke_m8_1264 (LoadAchievementDescriptionsCompletion_t8_223 * __this, AchievementDescriptionU5BU5D_t8_224* ____descriptions, String_t* ____error, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		LoadAchievementDescriptionsCompletion_Invoke_m8_1264((LoadAchievementDescriptionsCompletion_t8_223 *)__this->___prev_9,____descriptions, ____error, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, AchievementDescriptionU5BU5D_t8_224* ____descriptions, String_t* ____error, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,____descriptions, ____error,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, AchievementDescriptionU5BU5D_t8_224* ____descriptions, String_t* ____error, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,____descriptions, ____error,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, String_t* ____error, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(____descriptions, ____error,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_LoadAchievementDescriptionsCompletion_t8_223(Il2CppObject* delegate, AchievementDescriptionU5BU5D_t8_224* ____descriptions, String_t* ____error)
{
	// Marshaling of parameter '____descriptions' to native representation
	AchievementDescriptionU5BU5D_t8_224* _____descriptions_marshaled = { 0 };
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'VoxelBusters.NativePlugins.AchievementDescription[]'."));
}
// System.IAsyncResult VoxelBusters.NativePlugins.AchievementDescription/LoadAchievementDescriptionsCompletion::BeginInvoke(VoxelBusters.NativePlugins.AchievementDescription[],System.String,System.AsyncCallback,System.Object)
extern "C" Object_t * LoadAchievementDescriptionsCompletion_BeginInvoke_m8_1265 (LoadAchievementDescriptionsCompletion_t8_223 * __this, AchievementDescriptionU5BU5D_t8_224* ____descriptions, String_t* ____error, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ____descriptions;
	__d_args[1] = ____error;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void VoxelBusters.NativePlugins.AchievementDescription/LoadAchievementDescriptionsCompletion::EndInvoke(System.IAsyncResult)
extern "C" void LoadAchievementDescriptionsCompletion_EndInvoke_m8_1266 (LoadAchievementDescriptionsCompletion_t8_223 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void VoxelBusters.NativePlugins.AchievementDescription::.ctor()
extern "C" void AchievementDescription__ctor_m8_1267 (AchievementDescription_t8_225 * __this, const MethodInfo* method)
{
	{
		NPObject__ctor_m8_1903(__this, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.AchievementDescription::add_DownloadImageFinishedEvent(VoxelBusters.Utility.DownloadTexture/Completion)
extern TypeInfo* Completion_t8_161_il2cpp_TypeInfo_var;
extern "C" void AchievementDescription_add_DownloadImageFinishedEvent_m8_1268 (AchievementDescription_t8_225 * __this, Completion_t8_161 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Completion_t8_161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2088);
		s_Il2CppMethodIntialized = true;
	}
	{
		Completion_t8_161 * L_0 = (__this->___DownloadImageFinishedEvent_2);
		Completion_t8_161 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Combine_m1_908(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->___DownloadImageFinishedEvent_2 = ((Completion_t8_161 *)CastclassSealed(L_2, Completion_t8_161_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.AchievementDescription::remove_DownloadImageFinishedEvent(VoxelBusters.Utility.DownloadTexture/Completion)
extern TypeInfo* Completion_t8_161_il2cpp_TypeInfo_var;
extern "C" void AchievementDescription_remove_DownloadImageFinishedEvent_m8_1269 (AchievementDescription_t8_225 * __this, Completion_t8_161 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Completion_t8_161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2088);
		s_Il2CppMethodIntialized = true;
	}
	{
		Completion_t8_161 * L_0 = (__this->___DownloadImageFinishedEvent_2);
		Completion_t8_161 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Remove_m1_911(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->___DownloadImageFinishedEvent_2 = ((Completion_t8_161 *)CastclassSealed(L_2, Completion_t8_161_il2cpp_TypeInfo_var));
		return;
	}
}
// System.String VoxelBusters.NativePlugins.AchievementDescription::get_GlobalIdentifier()
extern "C" String_t* AchievementDescription_get_GlobalIdentifier_m8_1270 (AchievementDescription_t8_225 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CGlobalIdentifierU3Ek__BackingField_3);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.AchievementDescription::set_GlobalIdentifier(System.String)
extern "C" void AchievementDescription_set_GlobalIdentifier_m8_1271 (AchievementDescription_t8_225 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CGlobalIdentifierU3Ek__BackingField_3 = L_0;
		return;
	}
}
// System.String VoxelBusters.NativePlugins.AchievementDescription::get_InstanceID()
extern "C" String_t* AchievementDescription_get_InstanceID_m8_1272 (AchievementDescription_t8_225 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CInstanceIDU3Ek__BackingField_4);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.AchievementDescription::set_InstanceID(System.String)
extern "C" void AchievementDescription_set_InstanceID_m8_1273 (AchievementDescription_t8_225 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CInstanceIDU3Ek__BackingField_4 = L_0;
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.AchievementDescription::GetImageAsync(VoxelBusters.Utility.DownloadTexture/Completion)
extern "C" void AchievementDescription_GetImageAsync_m8_1274 (AchievementDescription_t8_225 * __this, Completion_t8_161 * ____onCompletion, const MethodInfo* method)
{
	{
		Completion_t8_161 * L_0 = ____onCompletion;
		__this->___DownloadImageFinishedEvent_2 = L_0;
		Texture2D_t6_33 * L_1 = (__this->___m_image_1);
		bool L_2 = Object_op_Inequality_m6_722(NULL /*static, unused*/, L_1, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002b;
		}
	}
	{
		Completion_t8_161 * L_3 = (__this->___DownloadImageFinishedEvent_2);
		Texture2D_t6_33 * L_4 = (__this->___m_image_1);
		NullCheck(L_3);
		Completion_Invoke_m8_926(L_3, L_4, (String_t*)NULL, /*hidden argument*/NULL);
		return;
	}

IL_002b:
	{
		VirtActionInvoker0::Invoke(16 /* System.Void VoxelBusters.NativePlugins.AchievementDescription::RequestForImage() */, __this);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.AchievementDescription::RequestForImage()
extern "C" void AchievementDescription_RequestForImage_m8_1275 (AchievementDescription_t8_225 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.String VoxelBusters.NativePlugins.AchievementDescription::ToString()
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t1_20_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5414;
extern "C" String_t* AchievementDescription_ToString_m8_1276 (AchievementDescription_t8_225 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		Boolean_t1_20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(54);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5414 = il2cpp_codegen_string_literal_from_index(5414);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_272* L_0 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 4));
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String VoxelBusters.NativePlugins.AchievementDescription::get_Identifier() */, __this);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_1;
		ObjectU5BU5D_t1_272* L_2 = L_0;
		String_t* L_3 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String VoxelBusters.NativePlugins.AchievementDescription::get_Title() */, __this);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1, sizeof(Object_t *))) = (Object_t *)L_3;
		ObjectU5BU5D_t1_272* L_4 = L_2;
		int32_t L_5 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(12 /* System.Int32 VoxelBusters.NativePlugins.AchievementDescription::get_MaximumPoints() */, __this);
		int32_t L_6 = L_5;
		Object_t * L_7 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 2, sizeof(Object_t *))) = (Object_t *)L_7;
		ObjectU5BU5D_t1_272* L_8 = L_4;
		bool L_9 = (bool)VirtFuncInvoker0< bool >::Invoke(14 /* System.Boolean VoxelBusters.NativePlugins.AchievementDescription::get_IsHidden() */, __this);
		bool L_10 = L_9;
		Object_t * L_11 = Box(Boolean_t1_20_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 3);
		ArrayElementTypeCheck (L_8, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 3, sizeof(Object_t *))) = (Object_t *)L_11;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Format_m1_551(NULL /*static, unused*/, _stringLiteral5414, L_8, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Void VoxelBusters.NativePlugins.AchievementDescription::RequestForImageFinished(System.Collections.IDictionary)
extern "C" void AchievementDescription_RequestForImageFinished_m8_1277 (AchievementDescription_t8_225 * __this, Object_t * ____dataDict, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.AchievementDescription::RequestForImageFinished(VoxelBusters.Utility.URL,System.String)
extern TypeInfo* DownloadTexture_t8_162_il2cpp_TypeInfo_var;
extern TypeInfo* Completion_t8_161_il2cpp_TypeInfo_var;
extern const MethodInfo* AchievementDescription_U3CRequestForImageFinishedU3Em__A_m8_1280_MethodInfo_var;
extern "C" void AchievementDescription_RequestForImageFinished_m8_1278 (AchievementDescription_t8_225 * __this, URL_t8_156  ____imagePathURL, String_t* ____error, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DownloadTexture_t8_162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2087);
		Completion_t8_161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2088);
		AchievementDescription_U3CRequestForImageFinishedU3Em__A_m8_1280_MethodInfo_var = il2cpp_codegen_method_info_from_index(580);
		s_Il2CppMethodIntialized = true;
	}
	DownloadTexture_t8_162 * V_0 = {0};
	{
		String_t* L_0 = ____error;
		if (!L_0)
		{
			goto IL_000f;
		}
	}
	{
		String_t* L_1 = ____error;
		AchievementDescription_DownloadImageFinished_m8_1279(__this, (Texture2D_t6_33 *)NULL, L_1, /*hidden argument*/NULL);
		return;
	}

IL_000f:
	{
		URL_t8_156  L_2 = ____imagePathURL;
		DownloadTexture_t8_162 * L_3 = (DownloadTexture_t8_162 *)il2cpp_codegen_object_new (DownloadTexture_t8_162_il2cpp_TypeInfo_var);
		DownloadTexture__ctor_m8_929(L_3, L_2, 1, 1, /*hidden argument*/NULL);
		V_0 = L_3;
		DownloadTexture_t8_162 * L_4 = V_0;
		IntPtr_t L_5 = { (void*)AchievementDescription_U3CRequestForImageFinishedU3Em__A_m8_1280_MethodInfo_var };
		Completion_t8_161 * L_6 = (Completion_t8_161 *)il2cpp_codegen_object_new (Completion_t8_161_il2cpp_TypeInfo_var);
		Completion__ctor_m8_925(L_6, __this, L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		DownloadTexture_set_OnCompletion_m8_935(L_4, L_6, /*hidden argument*/NULL);
		DownloadTexture_t8_162 * L_7 = V_0;
		NullCheck(L_7);
		Request_StartRequest_m8_900(L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.AchievementDescription::DownloadImageFinished(UnityEngine.Texture2D,System.String)
extern "C" void AchievementDescription_DownloadImageFinished_m8_1279 (AchievementDescription_t8_225 * __this, Texture2D_t6_33 * ____image, String_t* ____error, const MethodInfo* method)
{
	{
		Texture2D_t6_33 * L_0 = ____image;
		__this->___m_image_1 = L_0;
		Completion_t8_161 * L_1 = (__this->___DownloadImageFinishedEvent_2);
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		Completion_t8_161 * L_2 = (__this->___DownloadImageFinishedEvent_2);
		Texture2D_t6_33 * L_3 = ____image;
		String_t* L_4 = ____error;
		NullCheck(L_2);
		Completion_Invoke_m8_926(L_2, L_3, L_4, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.AchievementDescription::<RequestForImageFinished>m__A(UnityEngine.Texture2D,System.String)
extern "C" void AchievementDescription_U3CRequestForImageFinishedU3Em__A_m8_1280 (AchievementDescription_t8_225 * __this, Texture2D_t6_33 * ____image, String_t* ____downloadError, const MethodInfo* method)
{
	{
		Texture2D_t6_33 * L_0 = ____image;
		String_t* L_1 = ____downloadError;
		AchievementDescription_DownloadImageFinished_m8_1279(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Leaderboard/LoadScoreCompletion::.ctor(System.Object,System.IntPtr)
extern "C" void LoadScoreCompletion__ctor_m8_1281 (LoadScoreCompletion_t8_229 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void VoxelBusters.NativePlugins.Leaderboard/LoadScoreCompletion::Invoke(VoxelBusters.NativePlugins.Score[],VoxelBusters.NativePlugins.Score,System.String)
extern "C" void LoadScoreCompletion_Invoke_m8_1282 (LoadScoreCompletion_t8_229 * __this, ScoreU5BU5D_t8_230* ____scores, Score_t8_231 * ____localUserScore, String_t* ____error, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		LoadScoreCompletion_Invoke_m8_1282((LoadScoreCompletion_t8_229 *)__this->___prev_9,____scores, ____localUserScore, ____error, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, ScoreU5BU5D_t8_230* ____scores, Score_t8_231 * ____localUserScore, String_t* ____error, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,____scores, ____localUserScore, ____error,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, ScoreU5BU5D_t8_230* ____scores, Score_t8_231 * ____localUserScore, String_t* ____error, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,____scores, ____localUserScore, ____error,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, Score_t8_231 * ____localUserScore, String_t* ____error, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(____scores, ____localUserScore, ____error,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_LoadScoreCompletion_t8_229(Il2CppObject* delegate, ScoreU5BU5D_t8_230* ____scores, Score_t8_231 * ____localUserScore, String_t* ____error)
{
	// Marshaling of parameter '____scores' to native representation
	ScoreU5BU5D_t8_230* _____scores_marshaled = { 0 };
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'VoxelBusters.NativePlugins.Score[]'."));
}
// System.IAsyncResult VoxelBusters.NativePlugins.Leaderboard/LoadScoreCompletion::BeginInvoke(VoxelBusters.NativePlugins.Score[],VoxelBusters.NativePlugins.Score,System.String,System.AsyncCallback,System.Object)
extern "C" Object_t * LoadScoreCompletion_BeginInvoke_m8_1283 (LoadScoreCompletion_t8_229 * __this, ScoreU5BU5D_t8_230* ____scores, Score_t8_231 * ____localUserScore, String_t* ____error, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[4] = {0};
	__d_args[0] = ____scores;
	__d_args[1] = ____localUserScore;
	__d_args[2] = ____error;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void VoxelBusters.NativePlugins.Leaderboard/LoadScoreCompletion::EndInvoke(System.IAsyncResult)
extern "C" void LoadScoreCompletion_EndInvoke_m8_1284 (LoadScoreCompletion_t8_229 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void VoxelBusters.NativePlugins.Leaderboard::.ctor()
extern "C" void Leaderboard__ctor_m8_1285 (Leaderboard_t8_180 * __this, const MethodInfo* method)
{
	{
		NPObject__ctor_m8_1903(__this, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Leaderboard::.ctor(System.String,System.String,System.String,VoxelBusters.NativePlugins.eLeaderboardUserScope,VoxelBusters.NativePlugins.eLeaderboardTimeScope,System.Int32,VoxelBusters.NativePlugins.Score[],VoxelBusters.NativePlugins.Score)
extern "C" void Leaderboard__ctor_m8_1286 (Leaderboard_t8_180 * __this, String_t* ____globalIdentifer, String_t* ____identifier, String_t* ____title, int32_t ____userScope, int32_t ____timeScope, int32_t ____maxResults, ScoreU5BU5D_t8_230* ____scores, Score_t8_231 * ____localUserScore, const MethodInfo* method)
{
	{
		NPObject__ctor_m8_1903(__this, 0, /*hidden argument*/NULL);
		String_t* L_0 = ____globalIdentifer;
		Leaderboard_set_GlobalIdentifier_m8_1290(__this, L_0, /*hidden argument*/NULL);
		String_t* L_1 = ____identifier;
		VirtActionInvoker1< String_t* >::Invoke(5 /* System.Void VoxelBusters.NativePlugins.Leaderboard::set_Identifier(System.String) */, __this, L_1);
		String_t* L_2 = ____title;
		VirtActionInvoker1< String_t* >::Invoke(7 /* System.Void VoxelBusters.NativePlugins.Leaderboard::set_Title(System.String) */, __this, L_2);
		int32_t L_3 = ____userScope;
		VirtActionInvoker1< int32_t >::Invoke(9 /* System.Void VoxelBusters.NativePlugins.Leaderboard::set_UserScope(VoxelBusters.NativePlugins.eLeaderboardUserScope) */, __this, L_3);
		int32_t L_4 = ____timeScope;
		VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void VoxelBusters.NativePlugins.Leaderboard::set_TimeScope(VoxelBusters.NativePlugins.eLeaderboardTimeScope) */, __this, L_4);
		int32_t L_5 = ____maxResults;
		Leaderboard_set_MaxResults_m8_1292(__this, L_5, /*hidden argument*/NULL);
		ScoreU5BU5D_t8_230* L_6 = ____scores;
		VirtActionInvoker1< ScoreU5BU5D_t8_230* >::Invoke(13 /* System.Void VoxelBusters.NativePlugins.Leaderboard::set_Scores(VoxelBusters.NativePlugins.Score[]) */, __this, L_6);
		Score_t8_231 * L_7 = ____localUserScore;
		VirtActionInvoker1< Score_t8_231 * >::Invoke(15 /* System.Void VoxelBusters.NativePlugins.Leaderboard::set_LocalUserScore(VoxelBusters.NativePlugins.Score) */, __this, L_7);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Leaderboard::add_LoadScoreFinishedEvent(VoxelBusters.NativePlugins.Leaderboard/LoadScoreCompletion)
extern TypeInfo* LoadScoreCompletion_t8_229_il2cpp_TypeInfo_var;
extern "C" void Leaderboard_add_LoadScoreFinishedEvent_m8_1287 (Leaderboard_t8_180 * __this, LoadScoreCompletion_t8_229 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LoadScoreCompletion_t8_229_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2126);
		s_Il2CppMethodIntialized = true;
	}
	{
		LoadScoreCompletion_t8_229 * L_0 = (__this->___LoadScoreFinishedEvent_4);
		LoadScoreCompletion_t8_229 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Combine_m1_908(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->___LoadScoreFinishedEvent_4 = ((LoadScoreCompletion_t8_229 *)CastclassSealed(L_2, LoadScoreCompletion_t8_229_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Leaderboard::remove_LoadScoreFinishedEvent(VoxelBusters.NativePlugins.Leaderboard/LoadScoreCompletion)
extern TypeInfo* LoadScoreCompletion_t8_229_il2cpp_TypeInfo_var;
extern "C" void Leaderboard_remove_LoadScoreFinishedEvent_m8_1288 (Leaderboard_t8_180 * __this, LoadScoreCompletion_t8_229 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LoadScoreCompletion_t8_229_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2126);
		s_Il2CppMethodIntialized = true;
	}
	{
		LoadScoreCompletion_t8_229 * L_0 = (__this->___LoadScoreFinishedEvent_4);
		LoadScoreCompletion_t8_229 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Remove_m1_911(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->___LoadScoreFinishedEvent_4 = ((LoadScoreCompletion_t8_229 *)CastclassSealed(L_2, LoadScoreCompletion_t8_229_il2cpp_TypeInfo_var));
		return;
	}
}
// System.String VoxelBusters.NativePlugins.Leaderboard::get_GlobalIdentifier()
extern "C" String_t* Leaderboard_get_GlobalIdentifier_m8_1289 (Leaderboard_t8_180 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CGlobalIdentifierU3Ek__BackingField_5);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.Leaderboard::set_GlobalIdentifier(System.String)
extern "C" void Leaderboard_set_GlobalIdentifier_m8_1290 (Leaderboard_t8_180 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CGlobalIdentifierU3Ek__BackingField_5 = L_0;
		return;
	}
}
// System.Int32 VoxelBusters.NativePlugins.Leaderboard::get_MaxResults()
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" int32_t Leaderboard_get_MaxResults_m8_1291 (Leaderboard_t8_180 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1589);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (__this->___m_maxResults_3);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		int32_t L_1 = Mathf_Clamp_m6_440(NULL /*static, unused*/, L_0, 1, ((int32_t)100), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void VoxelBusters.NativePlugins.Leaderboard::set_MaxResults(System.Int32)
extern "C" void Leaderboard_set_MaxResults_m8_1292 (Leaderboard_t8_180 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_maxResults_3 = L_0;
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Leaderboard::LoadTopScores(VoxelBusters.NativePlugins.Leaderboard/LoadScoreCompletion)
extern "C" void Leaderboard_LoadTopScores_m8_1293 (Leaderboard_t8_180 * __this, LoadScoreCompletion_t8_229 * ____onCompletion, const MethodInfo* method)
{
	{
		LoadScoreCompletion_t8_229 * L_0 = ____onCompletion;
		__this->___LoadScoreFinishedEvent_4 = L_0;
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Leaderboard::LoadPlayerCenteredScores(VoxelBusters.NativePlugins.Leaderboard/LoadScoreCompletion)
extern "C" void Leaderboard_LoadPlayerCenteredScores_m8_1294 (Leaderboard_t8_180 * __this, LoadScoreCompletion_t8_229 * ____onCompletion, const MethodInfo* method)
{
	{
		LoadScoreCompletion_t8_229 * L_0 = ____onCompletion;
		__this->___LoadScoreFinishedEvent_4 = L_0;
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Leaderboard::LoadMoreScores(VoxelBusters.NativePlugins.eLeaderboardPageDirection,VoxelBusters.NativePlugins.Leaderboard/LoadScoreCompletion)
extern "C" void Leaderboard_LoadMoreScores_m8_1295 (Leaderboard_t8_180 * __this, int32_t ____pageDirection, LoadScoreCompletion_t8_229 * ____onCompletion, const MethodInfo* method)
{
	{
		LoadScoreCompletion_t8_229 * L_0 = ____onCompletion;
		__this->___LoadScoreFinishedEvent_4 = L_0;
		return;
	}
}
// System.String VoxelBusters.NativePlugins.Leaderboard::ToString()
extern TypeInfo* eLeaderboardUserScope_t8_226_il2cpp_TypeInfo_var;
extern TypeInfo* eLeaderboardTimeScope_t8_227_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5415;
extern "C" String_t* Leaderboard_ToString_m8_1296 (Leaderboard_t8_180 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		eLeaderboardUserScope_t8_226_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2127);
		eLeaderboardTimeScope_t8_227_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2128);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5415 = il2cpp_codegen_string_literal_from_index(5415);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String VoxelBusters.NativePlugins.Leaderboard::get_Identifier() */, __this);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(8 /* VoxelBusters.NativePlugins.eLeaderboardUserScope VoxelBusters.NativePlugins.Leaderboard::get_UserScope() */, __this);
		int32_t L_2 = L_1;
		Object_t * L_3 = Box(eLeaderboardUserScope_t8_226_il2cpp_TypeInfo_var, &L_2);
		int32_t L_4 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(10 /* VoxelBusters.NativePlugins.eLeaderboardTimeScope VoxelBusters.NativePlugins.Leaderboard::get_TimeScope() */, __this);
		int32_t L_5 = L_4;
		Object_t * L_6 = Box(eLeaderboardTimeScope_t8_227_il2cpp_TypeInfo_var, &L_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Format_m1_550(NULL /*static, unused*/, _stringLiteral5415, L_0, L_3, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void VoxelBusters.NativePlugins.Leaderboard::SetLoadScoreFinishedEvent(VoxelBusters.NativePlugins.Leaderboard/LoadScoreCompletion)
extern "C" void Leaderboard_SetLoadScoreFinishedEvent_m8_1297 (Leaderboard_t8_180 * __this, LoadScoreCompletion_t8_229 * ____onCompletion, const MethodInfo* method)
{
	{
		LoadScoreCompletion_t8_229 * L_0 = ____onCompletion;
		__this->___LoadScoreFinishedEvent_4 = L_0;
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Leaderboard::LoadScoresFinished(System.Collections.IDictionary)
extern "C" void Leaderboard_LoadScoresFinished_m8_1298 (Leaderboard_t8_180 * __this, Object_t * ____dataDict, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Leaderboard::LoadScoresFinished(VoxelBusters.NativePlugins.Score[],VoxelBusters.NativePlugins.Score,System.String)
extern "C" void Leaderboard_LoadScoresFinished_m8_1299 (Leaderboard_t8_180 * __this, ScoreU5BU5D_t8_230* ____scores, Score_t8_231 * ____localUserScore, String_t* ____error, const MethodInfo* method)
{
	{
		ScoreU5BU5D_t8_230* L_0 = ____scores;
		VirtActionInvoker1< ScoreU5BU5D_t8_230* >::Invoke(13 /* System.Void VoxelBusters.NativePlugins.Leaderboard::set_Scores(VoxelBusters.NativePlugins.Score[]) */, __this, L_0);
		Score_t8_231 * L_1 = ____localUserScore;
		VirtActionInvoker1< Score_t8_231 * >::Invoke(15 /* System.Void VoxelBusters.NativePlugins.Leaderboard::set_LocalUserScore(VoxelBusters.NativePlugins.Score) */, __this, L_1);
		LoadScoreCompletion_t8_229 * L_2 = (__this->___LoadScoreFinishedEvent_4);
		if (!L_2)
		{
			goto IL_0027;
		}
	}
	{
		LoadScoreCompletion_t8_229 * L_3 = (__this->___LoadScoreFinishedEvent_4);
		ScoreU5BU5D_t8_230* L_4 = ____scores;
		Score_t8_231 * L_5 = ____localUserScore;
		String_t* L_6 = ____error;
		NullCheck(L_3);
		LoadScoreCompletion_Invoke_m8_1282(L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
	}

IL_0027:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Score/ReportScoreCompletion::.ctor(System.Object,System.IntPtr)
extern "C" void ReportScoreCompletion__ctor_m8_1300 (ReportScoreCompletion_t8_232 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void VoxelBusters.NativePlugins.Score/ReportScoreCompletion::Invoke(System.Boolean,System.String)
extern "C" void ReportScoreCompletion_Invoke_m8_1301 (ReportScoreCompletion_t8_232 * __this, bool ____success, String_t* ____error, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		ReportScoreCompletion_Invoke_m8_1301((ReportScoreCompletion_t8_232 *)__this->___prev_9,____success, ____error, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, bool ____success, String_t* ____error, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,____success, ____error,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, bool ____success, String_t* ____error, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,____success, ____error,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_ReportScoreCompletion_t8_232(Il2CppObject* delegate, bool ____success, String_t* ____error)
{
	typedef void (STDCALL *native_function_ptr_type)(int32_t, char*);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '____success' to native representation

	// Marshaling of parameter '____error' to native representation
	char* _____error_marshaled = { 0 };
	_____error_marshaled = il2cpp_codegen_marshal_string(____error);

	// Native function invocation
	_il2cpp_pinvoke_func(____success, _____error_marshaled);

	// Marshaling cleanup of parameter '____success' native representation

	// Marshaling cleanup of parameter '____error' native representation
	il2cpp_codegen_marshal_free(_____error_marshaled);
	_____error_marshaled = NULL;

}
// System.IAsyncResult VoxelBusters.NativePlugins.Score/ReportScoreCompletion::BeginInvoke(System.Boolean,System.String,System.AsyncCallback,System.Object)
extern TypeInfo* Boolean_t1_20_il2cpp_TypeInfo_var;
extern "C" Object_t * ReportScoreCompletion_BeginInvoke_m8_1302 (ReportScoreCompletion_t8_232 * __this, bool ____success, String_t* ____error, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Boolean_t1_20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(54);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Boolean_t1_20_il2cpp_TypeInfo_var, &____success);
	__d_args[1] = ____error;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void VoxelBusters.NativePlugins.Score/ReportScoreCompletion::EndInvoke(System.IAsyncResult)
extern "C" void ReportScoreCompletion_EndInvoke_m8_1303 (ReportScoreCompletion_t8_232 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void VoxelBusters.NativePlugins.Score::.ctor()
extern "C" void Score__ctor_m8_1304 (Score_t8_231 * __this, const MethodInfo* method)
{
	{
		NPObject__ctor_m8_1903(__this, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Score::.ctor(System.String,System.String,VoxelBusters.NativePlugins.User,System.Int64)
extern TypeInfo* DateTime_t1_150_il2cpp_TypeInfo_var;
extern "C" void Score__ctor_m8_1305 (Score_t8_231 * __this, String_t* ____leaderboardGlobalID, String_t* ____leaderboardID, User_t8_235 * ____user, int64_t ____scoreValue, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DateTime_t1_150_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	{
		NPObject__ctor_m8_1903(__this, 0, /*hidden argument*/NULL);
		String_t* L_0 = ____leaderboardGlobalID;
		Score_set_LeaderboardGlobalID_m8_1309(__this, L_0, /*hidden argument*/NULL);
		String_t* L_1 = ____leaderboardID;
		VirtActionInvoker1< String_t* >::Invoke(5 /* System.Void VoxelBusters.NativePlugins.Score::set_LeaderboardID(System.String) */, __this, L_1);
		User_t8_235 * L_2 = ____user;
		VirtActionInvoker1< User_t8_235 * >::Invoke(7 /* System.Void VoxelBusters.NativePlugins.Score::set_User(VoxelBusters.NativePlugins.User) */, __this, L_2);
		int64_t L_3 = ____scoreValue;
		VirtActionInvoker1< int64_t >::Invoke(9 /* System.Void VoxelBusters.NativePlugins.Score::set_Value(System.Int64) */, __this, L_3);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		DateTime_t1_150  L_4 = DateTime_get_Now_m1_13826(NULL /*static, unused*/, /*hidden argument*/NULL);
		VirtActionInvoker1< DateTime_t1_150  >::Invoke(11 /* System.Void VoxelBusters.NativePlugins.Score::set_Date(System.DateTime) */, __this, L_4);
		VirtActionInvoker1< int32_t >::Invoke(14 /* System.Void VoxelBusters.NativePlugins.Score::set_Rank(System.Int32) */, __this, 0);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Score::add_ReportScoreFinishedEvent(VoxelBusters.NativePlugins.Score/ReportScoreCompletion)
extern TypeInfo* ReportScoreCompletion_t8_232_il2cpp_TypeInfo_var;
extern "C" void Score_add_ReportScoreFinishedEvent_m8_1306 (Score_t8_231 * __this, ReportScoreCompletion_t8_232 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReportScoreCompletion_t8_232_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2129);
		s_Il2CppMethodIntialized = true;
	}
	{
		ReportScoreCompletion_t8_232 * L_0 = (__this->___ReportScoreFinishedEvent_1);
		ReportScoreCompletion_t8_232 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Combine_m1_908(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->___ReportScoreFinishedEvent_1 = ((ReportScoreCompletion_t8_232 *)CastclassSealed(L_2, ReportScoreCompletion_t8_232_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Score::remove_ReportScoreFinishedEvent(VoxelBusters.NativePlugins.Score/ReportScoreCompletion)
extern TypeInfo* ReportScoreCompletion_t8_232_il2cpp_TypeInfo_var;
extern "C" void Score_remove_ReportScoreFinishedEvent_m8_1307 (Score_t8_231 * __this, ReportScoreCompletion_t8_232 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReportScoreCompletion_t8_232_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2129);
		s_Il2CppMethodIntialized = true;
	}
	{
		ReportScoreCompletion_t8_232 * L_0 = (__this->___ReportScoreFinishedEvent_1);
		ReportScoreCompletion_t8_232 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Remove_m1_911(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->___ReportScoreFinishedEvent_1 = ((ReportScoreCompletion_t8_232 *)CastclassSealed(L_2, ReportScoreCompletion_t8_232_il2cpp_TypeInfo_var));
		return;
	}
}
// System.String VoxelBusters.NativePlugins.Score::get_LeaderboardGlobalID()
extern "C" String_t* Score_get_LeaderboardGlobalID_m8_1308 (Score_t8_231 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CLeaderboardGlobalIDU3Ek__BackingField_2);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.Score::set_LeaderboardGlobalID(System.String)
extern "C" void Score_set_LeaderboardGlobalID_m8_1309 (Score_t8_231 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CLeaderboardGlobalIDU3Ek__BackingField_2 = L_0;
		return;
	}
}
// System.String VoxelBusters.NativePlugins.Score::get_FormattedValue()
extern TypeInfo* CultureInfo_t1_277_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5416;
extern "C" String_t* Score_get_FormattedValue_m8_1310 (Score_t8_231 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CultureInfo_t1_277_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(31);
		_stringLiteral5416 = il2cpp_codegen_string_literal_from_index(5416);
		s_Il2CppMethodIntialized = true;
	}
	int64_t V_0 = 0;
	{
		int64_t L_0 = (int64_t)VirtFuncInvoker0< int64_t >::Invoke(8 /* System.Int64 VoxelBusters.NativePlugins.Score::get_Value() */, __this);
		V_0 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t1_277_il2cpp_TypeInfo_var);
		CultureInfo_t1_277 * L_1 = CultureInfo_get_InvariantCulture_m1_3911(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_2 = Int64_ToString_m1_146((&V_0), _stringLiteral5416, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void VoxelBusters.NativePlugins.Score::ReportScore(VoxelBusters.NativePlugins.Score/ReportScoreCompletion)
extern "C" void Score_ReportScore_m8_1311 (Score_t8_231 * __this, ReportScoreCompletion_t8_232 * ____onCompletion, const MethodInfo* method)
{
	{
		ReportScoreCompletion_t8_232 * L_0 = ____onCompletion;
		__this->___ReportScoreFinishedEvent_1 = L_0;
		return;
	}
}
// System.String VoxelBusters.NativePlugins.Score::ToString()
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* Int64_t1_7_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5417;
extern "C" String_t* Score_ToString_m8_1312 (Score_t8_231 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		Int64_t1_7_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(21);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5417 = il2cpp_codegen_string_literal_from_index(5417);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(13 /* System.Int32 VoxelBusters.NativePlugins.Score::get_Rank() */, __this);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_1);
		User_t8_235 * L_3 = (User_t8_235 *)VirtFuncInvoker0< User_t8_235 * >::Invoke(6 /* VoxelBusters.NativePlugins.User VoxelBusters.NativePlugins.Score::get_User() */, __this);
		NullCheck(L_3);
		String_t* L_4 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String VoxelBusters.NativePlugins.User::get_Name() */, L_3);
		int64_t L_5 = (int64_t)VirtFuncInvoker0< int64_t >::Invoke(8 /* System.Int64 VoxelBusters.NativePlugins.Score::get_Value() */, __this);
		int64_t L_6 = L_5;
		Object_t * L_7 = Box(Int64_t1_7_il2cpp_TypeInfo_var, &L_6);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Format_m1_550(NULL /*static, unused*/, _stringLiteral5417, L_2, L_4, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Void VoxelBusters.NativePlugins.Score::ReportScoreFinished(System.Collections.IDictionary)
extern "C" void Score_ReportScoreFinished_m8_1313 (Score_t8_231 * __this, Object_t * ____dataDict, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Score::ReportScoreFinished(System.Boolean,System.String)
extern "C" void Score_ReportScoreFinished_m8_1314 (Score_t8_231 * __this, bool ____success, String_t* ____error, const MethodInfo* method)
{
	{
		ReportScoreCompletion_t8_232 * L_0 = (__this->___ReportScoreFinishedEvent_1);
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		ReportScoreCompletion_t8_232 * L_1 = (__this->___ReportScoreFinishedEvent_1);
		bool L_2 = ____success;
		String_t* L_3 = ____error;
		NullCheck(L_1);
		ReportScoreCompletion_Invoke_m8_1301(L_1, L_2, L_3, /*hidden argument*/NULL);
	}

IL_0018:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.User/LoadUsersCompletion::.ctor(System.Object,System.IntPtr)
extern "C" void LoadUsersCompletion__ctor_m8_1315 (LoadUsersCompletion_t8_233 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void VoxelBusters.NativePlugins.User/LoadUsersCompletion::Invoke(VoxelBusters.NativePlugins.User[],System.String)
extern "C" void LoadUsersCompletion_Invoke_m8_1316 (LoadUsersCompletion_t8_233 * __this, UserU5BU5D_t8_234* ____users, String_t* ____error, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		LoadUsersCompletion_Invoke_m8_1316((LoadUsersCompletion_t8_233 *)__this->___prev_9,____users, ____error, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, UserU5BU5D_t8_234* ____users, String_t* ____error, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,____users, ____error,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, UserU5BU5D_t8_234* ____users, String_t* ____error, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,____users, ____error,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, String_t* ____error, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(____users, ____error,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_LoadUsersCompletion_t8_233(Il2CppObject* delegate, UserU5BU5D_t8_234* ____users, String_t* ____error)
{
	// Marshaling of parameter '____users' to native representation
	UserU5BU5D_t8_234* _____users_marshaled = { 0 };
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'VoxelBusters.NativePlugins.User[]'."));
}
// System.IAsyncResult VoxelBusters.NativePlugins.User/LoadUsersCompletion::BeginInvoke(VoxelBusters.NativePlugins.User[],System.String,System.AsyncCallback,System.Object)
extern "C" Object_t * LoadUsersCompletion_BeginInvoke_m8_1317 (LoadUsersCompletion_t8_233 * __this, UserU5BU5D_t8_234* ____users, String_t* ____error, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ____users;
	__d_args[1] = ____error;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void VoxelBusters.NativePlugins.User/LoadUsersCompletion::EndInvoke(System.IAsyncResult)
extern "C" void LoadUsersCompletion_EndInvoke_m8_1318 (LoadUsersCompletion_t8_233 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void VoxelBusters.NativePlugins.User::.ctor()
extern "C" void User__ctor_m8_1319 (User_t8_235 * __this, const MethodInfo* method)
{
	{
		NPObject__ctor_m8_1903(__this, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.User::.ctor(System.String,System.String)
extern "C" void User__ctor_m8_1320 (User_t8_235 * __this, String_t* ____id, String_t* ____name, const MethodInfo* method)
{
	{
		NPObject__ctor_m8_1903(__this, 0, /*hidden argument*/NULL);
		String_t* L_0 = ____id;
		VirtActionInvoker1< String_t* >::Invoke(5 /* System.Void VoxelBusters.NativePlugins.User::set_Identifier(System.String) */, __this, L_0);
		String_t* L_1 = ____name;
		VirtActionInvoker1< String_t* >::Invoke(7 /* System.Void VoxelBusters.NativePlugins.User::set_Name(System.String) */, __this, L_1);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.User::add_DownloadImageFinishedEvent(VoxelBusters.Utility.DownloadTexture/Completion)
extern TypeInfo* Completion_t8_161_il2cpp_TypeInfo_var;
extern "C" void User_add_DownloadImageFinishedEvent_m8_1321 (User_t8_235 * __this, Completion_t8_161 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Completion_t8_161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2088);
		s_Il2CppMethodIntialized = true;
	}
	{
		Completion_t8_161 * L_0 = (__this->___DownloadImageFinishedEvent_2);
		Completion_t8_161 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Combine_m1_908(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->___DownloadImageFinishedEvent_2 = ((Completion_t8_161 *)CastclassSealed(L_2, Completion_t8_161_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.User::remove_DownloadImageFinishedEvent(VoxelBusters.Utility.DownloadTexture/Completion)
extern TypeInfo* Completion_t8_161_il2cpp_TypeInfo_var;
extern "C" void User_remove_DownloadImageFinishedEvent_m8_1322 (User_t8_235 * __this, Completion_t8_161 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Completion_t8_161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2088);
		s_Il2CppMethodIntialized = true;
	}
	{
		Completion_t8_161 * L_0 = (__this->___DownloadImageFinishedEvent_2);
		Completion_t8_161 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Remove_m1_911(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->___DownloadImageFinishedEvent_2 = ((Completion_t8_161 *)CastclassSealed(L_2, Completion_t8_161_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.User::GetImageAsync(VoxelBusters.Utility.DownloadTexture/Completion)
extern "C" void User_GetImageAsync_m8_1323 (User_t8_235 * __this, Completion_t8_161 * ____onCompletion, const MethodInfo* method)
{
	{
		Completion_t8_161 * L_0 = ____onCompletion;
		__this->___DownloadImageFinishedEvent_2 = L_0;
		Texture2D_t6_33 * L_1 = (__this->___m_image_1);
		bool L_2 = Object_op_Inequality_m6_722(NULL /*static, unused*/, L_1, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002b;
		}
	}
	{
		Completion_t8_161 * L_3 = (__this->___DownloadImageFinishedEvent_2);
		Texture2D_t6_33 * L_4 = (__this->___m_image_1);
		NullCheck(L_3);
		Completion_Invoke_m8_926(L_3, L_4, (String_t*)NULL, /*hidden argument*/NULL);
		return;
	}

IL_002b:
	{
		VirtActionInvoker0::Invoke(9 /* System.Void VoxelBusters.NativePlugins.User::RequestForImage() */, __this);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.User::RequestForImage()
extern "C" void User_RequestForImage_m8_1324 (User_t8_235 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.String VoxelBusters.NativePlugins.User::ToString()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5418;
extern "C" String_t* User_ToString_m8_1325 (User_t8_235 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5418 = il2cpp_codegen_string_literal_from_index(5418);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String VoxelBusters.NativePlugins.User::get_Name() */, __this);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Format_m1_548(NULL /*static, unused*/, _stringLiteral5418, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void VoxelBusters.NativePlugins.User::RequestForImageFinished(System.Collections.IDictionary)
extern "C" void User_RequestForImageFinished_m8_1326 (User_t8_235 * __this, Object_t * ____dataDict, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.User::RequestForImageFinished(VoxelBusters.Utility.URL,System.String)
extern TypeInfo* DownloadTexture_t8_162_il2cpp_TypeInfo_var;
extern TypeInfo* Completion_t8_161_il2cpp_TypeInfo_var;
extern const MethodInfo* User_U3CRequestForImageFinishedU3Em__B_m8_1329_MethodInfo_var;
extern "C" void User_RequestForImageFinished_m8_1327 (User_t8_235 * __this, URL_t8_156  ____imagePathURL, String_t* ____error, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DownloadTexture_t8_162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2087);
		Completion_t8_161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2088);
		User_U3CRequestForImageFinishedU3Em__B_m8_1329_MethodInfo_var = il2cpp_codegen_method_info_from_index(581);
		s_Il2CppMethodIntialized = true;
	}
	DownloadTexture_t8_162 * V_0 = {0};
	{
		String_t* L_0 = ____error;
		if (!L_0)
		{
			goto IL_000f;
		}
	}
	{
		String_t* L_1 = ____error;
		User_DownloadImageFinished_m8_1328(__this, (Texture2D_t6_33 *)NULL, L_1, /*hidden argument*/NULL);
		return;
	}

IL_000f:
	{
		URL_t8_156  L_2 = ____imagePathURL;
		DownloadTexture_t8_162 * L_3 = (DownloadTexture_t8_162 *)il2cpp_codegen_object_new (DownloadTexture_t8_162_il2cpp_TypeInfo_var);
		DownloadTexture__ctor_m8_929(L_3, L_2, 1, 1, /*hidden argument*/NULL);
		V_0 = L_3;
		DownloadTexture_t8_162 * L_4 = V_0;
		IntPtr_t L_5 = { (void*)User_U3CRequestForImageFinishedU3Em__B_m8_1329_MethodInfo_var };
		Completion_t8_161 * L_6 = (Completion_t8_161 *)il2cpp_codegen_object_new (Completion_t8_161_il2cpp_TypeInfo_var);
		Completion__ctor_m8_925(L_6, __this, L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		DownloadTexture_set_OnCompletion_m8_935(L_4, L_6, /*hidden argument*/NULL);
		DownloadTexture_t8_162 * L_7 = V_0;
		NullCheck(L_7);
		Request_StartRequest_m8_900(L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.User::DownloadImageFinished(UnityEngine.Texture2D,System.String)
extern "C" void User_DownloadImageFinished_m8_1328 (User_t8_235 * __this, Texture2D_t6_33 * ____image, String_t* ____error, const MethodInfo* method)
{
	{
		Texture2D_t6_33 * L_0 = ____image;
		__this->___m_image_1 = L_0;
		Completion_t8_161 * L_1 = (__this->___DownloadImageFinishedEvent_2);
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		Completion_t8_161 * L_2 = (__this->___DownloadImageFinishedEvent_2);
		Texture2D_t6_33 * L_3 = ____image;
		String_t* L_4 = ____error;
		NullCheck(L_2);
		Completion_Invoke_m8_926(L_2, L_3, L_4, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.User::<RequestForImageFinished>m__B(UnityEngine.Texture2D,System.String)
extern "C" void User_U3CRequestForImageFinishedU3Em__B_m8_1329 (User_t8_235 * __this, Texture2D_t6_33 * ____image, String_t* ____downloadError, const MethodInfo* method)
{
	{
		Texture2D_t6_33 * L_0 = ____image;
		String_t* L_1 = ____downloadError;
		User_DownloadImageFinished_m8_1328(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.GameServicesSettings/AndroidSettings::.ctor()
extern TypeInfo* StringU5BU5D_t1_238_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5419;
extern "C" void AndroidSettings__ctor_m8_1330 (AndroidSettings_t8_236 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringU5BU5D_t1_238_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(35);
		_stringLiteral5419 = il2cpp_codegen_string_literal_from_index(5419);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringU5BU5D_t1_238* L_0 = ((StringU5BU5D_t1_238*)SZArrayNew(StringU5BU5D_t1_238_il2cpp_TypeInfo_var, 1));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral5419);
		*((String_t**)(String_t**)SZArrayLdElema(L_0, 0, sizeof(String_t*))) = (String_t*)_stringLiteral5419;
		__this->___m_achievedDescriptionFormats_1 = L_0;
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String VoxelBusters.NativePlugins.GameServicesSettings/AndroidSettings::get_PlayServicesApplicationID()
extern "C" String_t* AndroidSettings_get_PlayServicesApplicationID_m8_1331 (AndroidSettings_t8_236 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_playServicesApplicationID_0);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.GameServicesSettings/AndroidSettings::set_PlayServicesApplicationID(System.String)
extern "C" void AndroidSettings_set_PlayServicesApplicationID_m8_1332 (AndroidSettings_t8_236 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_playServicesApplicationID_0 = L_0;
		return;
	}
}
// System.String[] VoxelBusters.NativePlugins.GameServicesSettings/AndroidSettings::get_AchievedDescriptionFormats()
extern "C" StringU5BU5D_t1_238* AndroidSettings_get_AchievedDescriptionFormats_m8_1333 (AndroidSettings_t8_236 * __this, const MethodInfo* method)
{
	{
		StringU5BU5D_t1_238* L_0 = (__this->___m_achievedDescriptionFormats_1);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.GameServicesSettings/AndroidSettings::set_AchievedDescriptionFormats(System.String[])
extern "C" void AndroidSettings_set_AchievedDescriptionFormats_m8_1334 (AndroidSettings_t8_236 * __this, StringU5BU5D_t1_238* ___value, const MethodInfo* method)
{
	{
		StringU5BU5D_t1_238* L_0 = ___value;
		__this->___m_achievedDescriptionFormats_1 = L_0;
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.GameServicesSettings/iOSSettings::.ctor()
extern "C" void iOSSettings__ctor_m8_1335 (iOSSettings_t8_237 * __this, const MethodInfo* method)
{
	{
		__this->___m_showDefaultAchievementCompletionBanner_0 = 1;
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean VoxelBusters.NativePlugins.GameServicesSettings/iOSSettings::get_ShowDefaultAchievementCompletionBanner()
extern "C" bool iOSSettings_get_ShowDefaultAchievementCompletionBanner_m8_1336 (iOSSettings_t8_237 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_showDefaultAchievementCompletionBanner_0);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.GameServicesSettings/iOSSettings::set_ShowDefaultAchievementCompletionBanner(System.Boolean)
extern "C" void iOSSettings_set_ShowDefaultAchievementCompletionBanner_m8_1337 (iOSSettings_t8_237 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___m_showDefaultAchievementCompletionBanner_0 = L_0;
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.GameServicesSettings::.ctor()
extern TypeInfo* IDContainerU5BU5D_t8_239_il2cpp_TypeInfo_var;
extern TypeInfo* AndroidSettings_t8_236_il2cpp_TypeInfo_var;
extern "C" void GameServicesSettings__ctor_m8_1338 (GameServicesSettings_t8_238 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDContainerU5BU5D_t8_239_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2130);
		AndroidSettings_t8_236_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2132);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___m_achievementIDCollection_2 = ((IDContainerU5BU5D_t8_239*)SZArrayNew(IDContainerU5BU5D_t8_239_il2cpp_TypeInfo_var, 0));
		__this->___m_leaderboardIDCollection_3 = ((IDContainerU5BU5D_t8_239*)SZArrayNew(IDContainerU5BU5D_t8_239_il2cpp_TypeInfo_var, 0));
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		AndroidSettings_t8_236 * L_0 = (AndroidSettings_t8_236 *)il2cpp_codegen_object_new (AndroidSettings_t8_236_il2cpp_TypeInfo_var);
		AndroidSettings__ctor_m8_1330(L_0, /*hidden argument*/NULL);
		GameServicesSettings_set_Android_m8_1340(__this, L_0, /*hidden argument*/NULL);
		GameServicesSettings_set_AchievementIDCollection_m8_1344(__this, ((IDContainerU5BU5D_t8_239*)SZArrayNew(IDContainerU5BU5D_t8_239_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		GameServicesSettings_set_LeaderboardIDCollection_m8_1346(__this, ((IDContainerU5BU5D_t8_239*)SZArrayNew(IDContainerU5BU5D_t8_239_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		return;
	}
}
// VoxelBusters.NativePlugins.GameServicesSettings/AndroidSettings VoxelBusters.NativePlugins.GameServicesSettings::get_Android()
extern "C" AndroidSettings_t8_236 * GameServicesSettings_get_Android_m8_1339 (GameServicesSettings_t8_238 * __this, const MethodInfo* method)
{
	{
		AndroidSettings_t8_236 * L_0 = (__this->___m_android_0);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.GameServicesSettings::set_Android(VoxelBusters.NativePlugins.GameServicesSettings/AndroidSettings)
extern "C" void GameServicesSettings_set_Android_m8_1340 (GameServicesSettings_t8_238 * __this, AndroidSettings_t8_236 * ___value, const MethodInfo* method)
{
	{
		AndroidSettings_t8_236 * L_0 = ___value;
		__this->___m_android_0 = L_0;
		return;
	}
}
// VoxelBusters.NativePlugins.GameServicesSettings/iOSSettings VoxelBusters.NativePlugins.GameServicesSettings::get_IOS()
extern "C" iOSSettings_t8_237 * GameServicesSettings_get_IOS_m8_1341 (GameServicesSettings_t8_238 * __this, const MethodInfo* method)
{
	{
		iOSSettings_t8_237 * L_0 = (__this->___m_iOS_1);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.GameServicesSettings::set_IOS(VoxelBusters.NativePlugins.GameServicesSettings/iOSSettings)
extern "C" void GameServicesSettings_set_IOS_m8_1342 (GameServicesSettings_t8_238 * __this, iOSSettings_t8_237 * ___value, const MethodInfo* method)
{
	{
		iOSSettings_t8_237 * L_0 = ___value;
		__this->___m_iOS_1 = L_0;
		return;
	}
}
// VoxelBusters.NativePlugins.IDContainer[] VoxelBusters.NativePlugins.GameServicesSettings::get_AchievementIDCollection()
extern "C" IDContainerU5BU5D_t8_239* GameServicesSettings_get_AchievementIDCollection_m8_1343 (GameServicesSettings_t8_238 * __this, const MethodInfo* method)
{
	{
		IDContainerU5BU5D_t8_239* L_0 = (__this->___m_achievementIDCollection_2);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.GameServicesSettings::set_AchievementIDCollection(VoxelBusters.NativePlugins.IDContainer[])
extern "C" void GameServicesSettings_set_AchievementIDCollection_m8_1344 (GameServicesSettings_t8_238 * __this, IDContainerU5BU5D_t8_239* ___value, const MethodInfo* method)
{
	{
		IDContainerU5BU5D_t8_239* L_0 = ___value;
		__this->___m_achievementIDCollection_2 = L_0;
		return;
	}
}
// VoxelBusters.NativePlugins.IDContainer[] VoxelBusters.NativePlugins.GameServicesSettings::get_LeaderboardIDCollection()
extern "C" IDContainerU5BU5D_t8_239* GameServicesSettings_get_LeaderboardIDCollection_m8_1345 (GameServicesSettings_t8_238 * __this, const MethodInfo* method)
{
	{
		IDContainerU5BU5D_t8_239* L_0 = (__this->___m_leaderboardIDCollection_3);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.GameServicesSettings::set_LeaderboardIDCollection(VoxelBusters.NativePlugins.IDContainer[])
extern "C" void GameServicesSettings_set_LeaderboardIDCollection_m8_1346 (GameServicesSettings_t8_238 * __this, IDContainerU5BU5D_t8_239* ___value, const MethodInfo* method)
{
	{
		IDContainerU5BU5D_t8_239* L_0 = ___value;
		__this->___m_leaderboardIDCollection_3 = L_0;
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.MediaLibrary/PickImageCompletion::.ctor(System.Object,System.IntPtr)
extern "C" void PickImageCompletion__ctor_m8_1347 (PickImageCompletion_t8_240 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void VoxelBusters.NativePlugins.MediaLibrary/PickImageCompletion::Invoke(VoxelBusters.NativePlugins.ePickImageFinishReason,UnityEngine.Texture2D)
extern "C" void PickImageCompletion_Invoke_m8_1348 (PickImageCompletion_t8_240 * __this, int32_t ____reason, Texture2D_t6_33 * ____image, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		PickImageCompletion_Invoke_m8_1348((PickImageCompletion_t8_240 *)__this->___prev_9,____reason, ____image, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, int32_t ____reason, Texture2D_t6_33 * ____image, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,____reason, ____image,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, int32_t ____reason, Texture2D_t6_33 * ____image, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,____reason, ____image,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_PickImageCompletion_t8_240(Il2CppObject* delegate, int32_t ____reason, Texture2D_t6_33 * ____image)
{
	// Marshaling of parameter '____image' to native representation
	Texture2D_t6_33 * _____image_marshaled = { 0 };
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'UnityEngine.Texture2D'."));
}
// System.IAsyncResult VoxelBusters.NativePlugins.MediaLibrary/PickImageCompletion::BeginInvoke(VoxelBusters.NativePlugins.ePickImageFinishReason,UnityEngine.Texture2D,System.AsyncCallback,System.Object)
extern TypeInfo* ePickImageFinishReason_t8_249_il2cpp_TypeInfo_var;
extern "C" Object_t * PickImageCompletion_BeginInvoke_m8_1349 (PickImageCompletion_t8_240 * __this, int32_t ____reason, Texture2D_t6_33 * ____image, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ePickImageFinishReason_t8_249_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2133);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(ePickImageFinishReason_t8_249_il2cpp_TypeInfo_var, &____reason);
	__d_args[1] = ____image;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void VoxelBusters.NativePlugins.MediaLibrary/PickImageCompletion::EndInvoke(System.IAsyncResult)
extern "C" void PickImageCompletion_EndInvoke_m8_1350 (PickImageCompletion_t8_240 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void VoxelBusters.NativePlugins.MediaLibrary/SaveImageToGalleryCompletion::.ctor(System.Object,System.IntPtr)
extern "C" void SaveImageToGalleryCompletion__ctor_m8_1351 (SaveImageToGalleryCompletion_t8_241 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void VoxelBusters.NativePlugins.MediaLibrary/SaveImageToGalleryCompletion::Invoke(System.Boolean)
extern "C" void SaveImageToGalleryCompletion_Invoke_m8_1352 (SaveImageToGalleryCompletion_t8_241 * __this, bool ____savedSuccessfully, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		SaveImageToGalleryCompletion_Invoke_m8_1352((SaveImageToGalleryCompletion_t8_241 *)__this->___prev_9,____savedSuccessfully, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, bool ____savedSuccessfully, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,____savedSuccessfully,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, bool ____savedSuccessfully, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,____savedSuccessfully,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_SaveImageToGalleryCompletion_t8_241(Il2CppObject* delegate, bool ____savedSuccessfully)
{
	typedef void (STDCALL *native_function_ptr_type)(int32_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '____savedSuccessfully' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____savedSuccessfully);

	// Marshaling cleanup of parameter '____savedSuccessfully' native representation

}
// System.IAsyncResult VoxelBusters.NativePlugins.MediaLibrary/SaveImageToGalleryCompletion::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern TypeInfo* Boolean_t1_20_il2cpp_TypeInfo_var;
extern "C" Object_t * SaveImageToGalleryCompletion_BeginInvoke_m8_1353 (SaveImageToGalleryCompletion_t8_241 * __this, bool ____savedSuccessfully, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Boolean_t1_20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(54);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Boolean_t1_20_il2cpp_TypeInfo_var, &____savedSuccessfully);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void VoxelBusters.NativePlugins.MediaLibrary/SaveImageToGalleryCompletion::EndInvoke(System.IAsyncResult)
extern "C" void SaveImageToGalleryCompletion_EndInvoke_m8_1354 (SaveImageToGalleryCompletion_t8_241 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void VoxelBusters.NativePlugins.MediaLibrary/PickVideoCompletion::.ctor(System.Object,System.IntPtr)
extern "C" void PickVideoCompletion__ctor_m8_1355 (PickVideoCompletion_t8_242 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void VoxelBusters.NativePlugins.MediaLibrary/PickVideoCompletion::Invoke(VoxelBusters.NativePlugins.ePickVideoFinishReason)
extern "C" void PickVideoCompletion_Invoke_m8_1356 (PickVideoCompletion_t8_242 * __this, int32_t ____reason, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		PickVideoCompletion_Invoke_m8_1356((PickVideoCompletion_t8_242 *)__this->___prev_9,____reason, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, int32_t ____reason, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,____reason,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, int32_t ____reason, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,____reason,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_PickVideoCompletion_t8_242(Il2CppObject* delegate, int32_t ____reason)
{
	typedef void (STDCALL *native_function_ptr_type)(int32_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '____reason' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____reason);

	// Marshaling cleanup of parameter '____reason' native representation

}
// System.IAsyncResult VoxelBusters.NativePlugins.MediaLibrary/PickVideoCompletion::BeginInvoke(VoxelBusters.NativePlugins.ePickVideoFinishReason,System.AsyncCallback,System.Object)
extern TypeInfo* ePickVideoFinishReason_t8_250_il2cpp_TypeInfo_var;
extern "C" Object_t * PickVideoCompletion_BeginInvoke_m8_1357 (PickVideoCompletion_t8_242 * __this, int32_t ____reason, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ePickVideoFinishReason_t8_250_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2134);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(ePickVideoFinishReason_t8_250_il2cpp_TypeInfo_var, &____reason);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void VoxelBusters.NativePlugins.MediaLibrary/PickVideoCompletion::EndInvoke(System.IAsyncResult)
extern "C" void PickVideoCompletion_EndInvoke_m8_1358 (PickVideoCompletion_t8_242 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void VoxelBusters.NativePlugins.MediaLibrary/PlayVideoCompletion::.ctor(System.Object,System.IntPtr)
extern "C" void PlayVideoCompletion__ctor_m8_1359 (PlayVideoCompletion_t8_243 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void VoxelBusters.NativePlugins.MediaLibrary/PlayVideoCompletion::Invoke(VoxelBusters.NativePlugins.ePlayVideoFinishReason)
extern "C" void PlayVideoCompletion_Invoke_m8_1360 (PlayVideoCompletion_t8_243 * __this, int32_t ____reason, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		PlayVideoCompletion_Invoke_m8_1360((PlayVideoCompletion_t8_243 *)__this->___prev_9,____reason, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, int32_t ____reason, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,____reason,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, int32_t ____reason, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,____reason,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_PlayVideoCompletion_t8_243(Il2CppObject* delegate, int32_t ____reason)
{
	typedef void (STDCALL *native_function_ptr_type)(int32_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '____reason' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____reason);

	// Marshaling cleanup of parameter '____reason' native representation

}
// System.IAsyncResult VoxelBusters.NativePlugins.MediaLibrary/PlayVideoCompletion::BeginInvoke(VoxelBusters.NativePlugins.ePlayVideoFinishReason,System.AsyncCallback,System.Object)
extern TypeInfo* ePlayVideoFinishReason_t8_251_il2cpp_TypeInfo_var;
extern "C" Object_t * PlayVideoCompletion_BeginInvoke_m8_1361 (PlayVideoCompletion_t8_243 * __this, int32_t ____reason, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ePlayVideoFinishReason_t8_251_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2135);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(ePlayVideoFinishReason_t8_251_il2cpp_TypeInfo_var, &____reason);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void VoxelBusters.NativePlugins.MediaLibrary/PlayVideoCompletion::EndInvoke(System.IAsyncResult)
extern "C" void PlayVideoCompletion_EndInvoke_m8_1362 (PlayVideoCompletion_t8_243 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void VoxelBusters.NativePlugins.MediaLibrary/<PickImageFinished>c__AnonStoreyB::.ctor()
extern "C" void U3CPickImageFinishedU3Ec__AnonStoreyB__ctor_m8_1363 (U3CPickImageFinishedU3Ec__AnonStoreyB_t8_244 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.MediaLibrary/<PickImageFinished>c__AnonStoreyB::<>m__C(UnityEngine.Texture2D,System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5434;
extern "C" void U3CPickImageFinishedU3Ec__AnonStoreyB_U3CU3Em__C_m8_1364 (U3CPickImageFinishedU3Ec__AnonStoreyB_t8_244 * __this, Texture2D_t6_33 * ____texture, String_t* ____error, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5434 = il2cpp_codegen_string_literal_from_index(5434);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ____error;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1_521(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		MediaLibrary_t8_245 * L_2 = (__this->___U3CU3Ef__this_1);
		NullCheck(L_2);
		PickImageCompletion_t8_240 * L_3 = (L_2->___OnPickImageFinished_2);
		Texture2D_t6_33 * L_4 = ____texture;
		NullCheck(L_3);
		PickImageCompletion_Invoke_m8_1348(L_3, 0, L_4, /*hidden argument*/NULL);
		goto IL_0054;
	}

IL_0022:
	{
		URL_t8_156 * L_5 = &(__this->____imagePathURL_0);
		String_t* L_6 = URL_get_URLString_m8_958(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m1_559(NULL /*static, unused*/, _stringLiteral5434, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_LogError_m8_1009(NULL /*static, unused*/, _stringLiteral5368, L_7, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		MediaLibrary_t8_245 * L_8 = (__this->___U3CU3Ef__this_1);
		NullCheck(L_8);
		PickImageCompletion_t8_240 * L_9 = (L_8->___OnPickImageFinished_2);
		NullCheck(L_9);
		PickImageCompletion_Invoke_m8_1348(L_9, 2, (Texture2D_t6_33 *)NULL, /*hidden argument*/NULL);
	}

IL_0054:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.MediaLibrary/<SaveScreenshotToGallery>c__AnonStoreyC::.ctor()
extern "C" void U3CSaveScreenshotToGalleryU3Ec__AnonStoreyC__ctor_m8_1365 (U3CSaveScreenshotToGalleryU3Ec__AnonStoreyC_t8_246 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.MediaLibrary/<SaveScreenshotToGallery>c__AnonStoreyC::<>m__D(UnityEngine.Texture2D)
extern "C" void U3CSaveScreenshotToGalleryU3Ec__AnonStoreyC_U3CU3Em__D_m8_1366 (U3CSaveScreenshotToGalleryU3Ec__AnonStoreyC_t8_246 * __this, Texture2D_t6_33 * ____texture, const MethodInfo* method)
{
	{
		MediaLibrary_t8_245 * L_0 = (__this->___U3CU3Ef__this_1);
		Texture2D_t6_33 * L_1 = ____texture;
		SaveImageToGalleryCompletion_t8_241 * L_2 = (__this->____onCompletion_0);
		NullCheck(L_0);
		MediaLibrary_SaveImageToGallery_m8_1386(L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.MediaLibrary/<SaveImageToGallery>c__AnonStoreyD::.ctor()
extern "C" void U3CSaveImageToGalleryU3Ec__AnonStoreyD__ctor_m8_1367 (U3CSaveImageToGalleryU3Ec__AnonStoreyD_t8_247 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.MediaLibrary/<SaveImageToGallery>c__AnonStoreyD::<>m__E(UnityEngine.Texture2D,System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5434;
extern "C" void U3CSaveImageToGalleryU3Ec__AnonStoreyD_U3CU3Em__E_m8_1368 (U3CSaveImageToGalleryU3Ec__AnonStoreyD_t8_247 * __this, Texture2D_t6_33 * ____texture, String_t* ____error, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5434 = il2cpp_codegen_string_literal_from_index(5434);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ____error;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1_521(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_002b;
		}
	}
	{
		URL_t8_156 * L_2 = &(__this->____URL_0);
		String_t* L_3 = URL_get_URLString_m8_958(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m1_559(NULL /*static, unused*/, _stringLiteral5434, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_LogError_m8_1009(NULL /*static, unused*/, _stringLiteral5368, L_4, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
	}

IL_002b:
	{
		MediaLibrary_t8_245 * L_5 = (__this->___U3CU3Ef__this_2);
		Texture2D_t6_33 * L_6 = ____texture;
		SaveImageToGalleryCompletion_t8_241 * L_7 = (__this->____onCompletion_1);
		NullCheck(L_5);
		MediaLibrary_SaveImageToGallery_m8_1386(L_5, L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.MediaLibrary::.ctor()
extern "C" void MediaLibrary__ctor_m8_1369 (MediaLibrary_t8_245 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m6_671(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.MediaLibrary::PickImageFinished(System.String)
extern TypeInfo* IDictionary_t1_35_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void MediaLibrary_PickImageFinished_m8_1370 (MediaLibrary_t8_245 * __this, String_t* ____responseJson, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionary_t1_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	String_t* V_1 = {0};
	int32_t V_2 = {0};
	{
		String_t* L_0 = ____responseJson;
		Object_t * L_1 = JSONUtility_FromJSON_m8_298(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = ((Object_t *)IsInst(L_1, IDictionary_t1_35_il2cpp_TypeInfo_var));
		Object_t * L_2 = V_0;
		VirtActionInvoker3< Object_t *, String_t**, int32_t* >::Invoke(4 /* System.Void VoxelBusters.NativePlugins.MediaLibrary::ParsePickImageFinishedData(System.Collections.IDictionary,System.String&,VoxelBusters.NativePlugins.ePickImageFinishReason&) */, __this, L_2, (&V_1), (&V_2));
		String_t* L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_IsNullOrEmpty_m1_521(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_002d;
		}
	}
	{
		int32_t L_5 = V_2;
		if ((((int32_t)L_5) == ((int32_t)1)))
		{
			goto IL_002d;
		}
	}
	{
		V_2 = 2;
		V_1 = (String_t*)NULL;
	}

IL_002d:
	{
		String_t* L_6 = V_1;
		int32_t L_7 = V_2;
		MediaLibrary_PickImageFinished_m8_1371(__this, L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.MediaLibrary::PickImageFinished(System.String,VoxelBusters.NativePlugins.ePickImageFinishReason)
extern TypeInfo* MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* ePickImageFinishReason_t8_249_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern TypeInfo* U3CPickImageFinishedU3Ec__AnonStoreyB_t8_244_il2cpp_TypeInfo_var;
extern TypeInfo* DownloadTexture_t8_162_il2cpp_TypeInfo_var;
extern TypeInfo* Completion_t8_161_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CPickImageFinishedU3Ec__AnonStoreyB_U3CU3Em__C_m8_1364_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5420;
extern Il2CppCodeGenString* _stringLiteral5421;
extern "C" void MediaLibrary_PickImageFinished_m8_1371 (MediaLibrary_t8_245 * __this, String_t* ____imagePath, int32_t ____finishReason, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1959);
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		ePickImageFinishReason_t8_249_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2133);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		U3CPickImageFinishedU3Ec__AnonStoreyB_t8_244_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2136);
		DownloadTexture_t8_162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2087);
		Completion_t8_161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2088);
		U3CPickImageFinishedU3Ec__AnonStoreyB_U3CU3Em__C_m8_1364_MethodInfo_var = il2cpp_codegen_method_info_from_index(582);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5420 = il2cpp_codegen_string_literal_from_index(5420);
		_stringLiteral5421 = il2cpp_codegen_string_literal_from_index(5421);
		s_Il2CppMethodIntialized = true;
	}
	DownloadTexture_t8_162 * V_0 = {0};
	U3CPickImageFinishedU3Ec__AnonStoreyB_t8_244 * V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var);
		MonoBehaviourExtensions_ResumeUnity_m8_211(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		ObjectU5BU5D_t1_272* L_0 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 4));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral5420);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral5420;
		ObjectU5BU5D_t1_272* L_1 = L_0;
		String_t* L_2 = ____imagePath;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_2);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_1, 1, sizeof(Object_t *))) = (Object_t *)L_2;
		ObjectU5BU5D_t1_272* L_3 = L_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 2);
		ArrayElementTypeCheck (L_3, _stringLiteral5421);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_3, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral5421;
		ObjectU5BU5D_t1_272* L_4 = L_3;
		int32_t L_5 = ____finishReason;
		int32_t L_6 = L_5;
		Object_t * L_7 = Box(ePickImageFinishReason_t8_249_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 3);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 3, sizeof(Object_t *))) = (Object_t *)L_7;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m1_562(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_Log_m8_1007(NULL /*static, unused*/, _stringLiteral5368, L_8, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		PickImageCompletion_t8_240 * L_9 = (__this->___OnPickImageFinished_2);
		if (!L_9)
		{
			goto IL_00a3;
		}
	}
	{
		U3CPickImageFinishedU3Ec__AnonStoreyB_t8_244 * L_10 = (U3CPickImageFinishedU3Ec__AnonStoreyB_t8_244 *)il2cpp_codegen_object_new (U3CPickImageFinishedU3Ec__AnonStoreyB_t8_244_il2cpp_TypeInfo_var);
		U3CPickImageFinishedU3Ec__AnonStoreyB__ctor_m8_1363(L_10, /*hidden argument*/NULL);
		V_1 = L_10;
		U3CPickImageFinishedU3Ec__AnonStoreyB_t8_244 * L_11 = V_1;
		NullCheck(L_11);
		L_11->___U3CU3Ef__this_1 = __this;
		int32_t L_12 = ____finishReason;
		if (!L_12)
		{
			goto IL_0065;
		}
	}
	{
		PickImageCompletion_t8_240 * L_13 = (__this->___OnPickImageFinished_2);
		int32_t L_14 = ____finishReason;
		NullCheck(L_13);
		PickImageCompletion_Invoke_m8_1348(L_13, L_14, (Texture2D_t6_33 *)NULL, /*hidden argument*/NULL);
		return;
	}

IL_0065:
	{
		U3CPickImageFinishedU3Ec__AnonStoreyB_t8_244 * L_15 = V_1;
		String_t* L_16 = ____imagePath;
		URL_t8_156  L_17 = URL_FileURLWithPath_m8_961(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		NullCheck(L_15);
		L_15->____imagePathURL_0 = L_17;
		U3CPickImageFinishedU3Ec__AnonStoreyB_t8_244 * L_18 = V_1;
		NullCheck(L_18);
		URL_t8_156  L_19 = (L_18->____imagePathURL_0);
		DownloadTexture_t8_162 * L_20 = (DownloadTexture_t8_162 *)il2cpp_codegen_object_new (DownloadTexture_t8_162_il2cpp_TypeInfo_var);
		DownloadTexture__ctor_m8_929(L_20, L_19, 1, 1, /*hidden argument*/NULL);
		V_0 = L_20;
		DownloadTexture_t8_162 * L_21 = V_0;
		float L_22 = (__this->___m_scaleFactor_6);
		NullCheck(L_21);
		DownloadTexture_set_ScaleFactor_m8_933(L_21, L_22, /*hidden argument*/NULL);
		DownloadTexture_t8_162 * L_23 = V_0;
		U3CPickImageFinishedU3Ec__AnonStoreyB_t8_244 * L_24 = V_1;
		IntPtr_t L_25 = { (void*)U3CPickImageFinishedU3Ec__AnonStoreyB_U3CU3Em__C_m8_1364_MethodInfo_var };
		Completion_t8_161 * L_26 = (Completion_t8_161 *)il2cpp_codegen_object_new (Completion_t8_161_il2cpp_TypeInfo_var);
		Completion__ctor_m8_925(L_26, L_24, L_25, /*hidden argument*/NULL);
		NullCheck(L_23);
		DownloadTexture_set_OnCompletion_m8_935(L_23, L_26, /*hidden argument*/NULL);
		DownloadTexture_t8_162 * L_27 = V_0;
		NullCheck(L_27);
		Request_StartRequest_m8_900(L_27, /*hidden argument*/NULL);
	}

IL_00a3:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.MediaLibrary::SaveImageToGalleryFinished(System.String)
extern TypeInfo* Boolean_t1_20_il2cpp_TypeInfo_var;
extern "C" void MediaLibrary_SaveImageToGalleryFinished_m8_1372 (MediaLibrary_t8_245 * __this, String_t* ____savedStatus, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Boolean_t1_20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(54);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		String_t* L_0 = ____savedStatus;
		IL2CPP_RUNTIME_CLASS_INIT(Boolean_t1_20_il2cpp_TypeInfo_var);
		bool L_1 = Boolean_Parse_m1_822(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = V_0;
		MediaLibrary_SaveImageToGalleryFinished_m8_1373(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.MediaLibrary::SaveImageToGalleryFinished(System.Boolean)
extern TypeInfo* Boolean_t1_20_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5422;
extern "C" void MediaLibrary_SaveImageToGalleryFinished_m8_1373 (MediaLibrary_t8_245 * __this, bool ____savedSuccessfully, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Boolean_t1_20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(54);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5422 = il2cpp_codegen_string_literal_from_index(5422);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ____savedSuccessfully;
		bool L_1 = L_0;
		Object_t * L_2 = Box(Boolean_t1_20_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m1_556(NULL /*static, unused*/, _stringLiteral5422, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_Log_m8_1007(NULL /*static, unused*/, _stringLiteral5368, L_3, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		SaveImageToGalleryCompletion_t8_241 * L_4 = (__this->___OnSaveImageToGalleryFinished_3);
		if (!L_4)
		{
			goto IL_0032;
		}
	}
	{
		SaveImageToGalleryCompletion_t8_241 * L_5 = (__this->___OnSaveImageToGalleryFinished_3);
		bool L_6 = ____savedSuccessfully;
		NullCheck(L_5);
		SaveImageToGalleryCompletion_Invoke_m8_1352(L_5, L_6, /*hidden argument*/NULL);
	}

IL_0032:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.MediaLibrary::PickVideoFinished(System.String)
extern "C" void MediaLibrary_PickVideoFinished_m8_1374 (MediaLibrary_t8_245 * __this, String_t* ____reasonStr, const MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		String_t* L_0 = ____reasonStr;
		VirtActionInvoker2< String_t*, int32_t* >::Invoke(5 /* System.Void VoxelBusters.NativePlugins.MediaLibrary::ParsePickVideoFinishedData(System.String,VoxelBusters.NativePlugins.ePickVideoFinishReason&) */, __this, L_0, (&V_0));
		int32_t L_1 = V_0;
		MediaLibrary_PickVideoFinished_m8_1375(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.MediaLibrary::PickVideoFinished(VoxelBusters.NativePlugins.ePickVideoFinishReason)
extern TypeInfo* ePickVideoFinishReason_t8_250_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern TypeInfo* MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5423;
extern "C" void MediaLibrary_PickVideoFinished_m8_1375 (MediaLibrary_t8_245 * __this, int32_t ____finishReason, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ePickVideoFinishReason_t8_250_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2134);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1959);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5423 = il2cpp_codegen_string_literal_from_index(5423);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ____finishReason;
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(ePickVideoFinishReason_t8_250_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m1_556(NULL /*static, unused*/, _stringLiteral5423, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_Log_m8_1007(NULL /*static, unused*/, _stringLiteral5368, L_3, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		int32_t L_4 = ____finishReason;
		if (!L_4)
		{
			goto IL_0027;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var);
		MonoBehaviourExtensions_ResumeUnity_m8_211(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
	}

IL_0027:
	{
		PickVideoCompletion_t8_242 * L_5 = (__this->___OnPickVideoFinished_4);
		if (!L_5)
		{
			goto IL_003e;
		}
	}
	{
		PickVideoCompletion_t8_242 * L_6 = (__this->___OnPickVideoFinished_4);
		int32_t L_7 = ____finishReason;
		NullCheck(L_6);
		PickVideoCompletion_Invoke_m8_1356(L_6, L_7, /*hidden argument*/NULL);
	}

IL_003e:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.MediaLibrary::PlayVideoFinished(System.String)
extern "C" void MediaLibrary_PlayVideoFinished_m8_1376 (MediaLibrary_t8_245 * __this, String_t* ____reasonStr, const MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		String_t* L_0 = ____reasonStr;
		VirtActionInvoker2< String_t*, int32_t* >::Invoke(6 /* System.Void VoxelBusters.NativePlugins.MediaLibrary::ParsePlayVideoFinishedData(System.String,VoxelBusters.NativePlugins.ePlayVideoFinishReason&) */, __this, L_0, (&V_0));
		int32_t L_1 = V_0;
		MediaLibrary_PlayVideoFinished_m8_1377(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.MediaLibrary::PlayVideoFinished(VoxelBusters.NativePlugins.ePlayVideoFinishReason)
extern TypeInfo* MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var;
extern TypeInfo* ePlayVideoFinishReason_t8_251_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5424;
extern "C" void MediaLibrary_PlayVideoFinished_m8_1377 (MediaLibrary_t8_245 * __this, int32_t ____finishReason, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1959);
		ePlayVideoFinishReason_t8_251_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2135);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5424 = il2cpp_codegen_string_literal_from_index(5424);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var);
		MonoBehaviourExtensions_ResumeUnity_m8_211(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		int32_t L_0 = ____finishReason;
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(ePlayVideoFinishReason_t8_251_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m1_556(NULL /*static, unused*/, _stringLiteral5424, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_Log_m8_1007(NULL /*static, unused*/, _stringLiteral5368, L_3, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		PlayVideoCompletion_t8_243 * L_4 = (__this->___OnPlayVideoFinished_5);
		if (!L_4)
		{
			goto IL_0038;
		}
	}
	{
		PlayVideoCompletion_t8_243 * L_5 = (__this->___OnPlayVideoFinished_5);
		int32_t L_6 = ____finishReason;
		NullCheck(L_5);
		PlayVideoCompletion_Invoke_m8_1360(L_5, L_6, /*hidden argument*/NULL);
	}

IL_0038:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.MediaLibrary::ParsePickImageFinishedData(System.Collections.IDictionary,System.String&,VoxelBusters.NativePlugins.ePickImageFinishReason&)
extern "C" void MediaLibrary_ParsePickImageFinishedData_m8_1378 (MediaLibrary_t8_245 * __this, Object_t * ____infoDict, String_t** ____selectedImagePath, int32_t* ____finishReason, const MethodInfo* method)
{
	{
		String_t** L_0 = ____selectedImagePath;
		*((Object_t **)(L_0)) = (Object_t *)NULL;
		int32_t* L_1 = ____finishReason;
		*((int32_t*)(L_1)) = (int32_t)2;
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.MediaLibrary::ParsePickVideoFinishedData(System.String,VoxelBusters.NativePlugins.ePickVideoFinishReason&)
extern "C" void MediaLibrary_ParsePickVideoFinishedData_m8_1379 (MediaLibrary_t8_245 * __this, String_t* ____reasonString, int32_t* ____finishReason, const MethodInfo* method)
{
	{
		int32_t* L_0 = ____finishReason;
		*((int32_t*)(L_0)) = (int32_t)2;
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.MediaLibrary::ParsePlayVideoFinishedData(System.String,VoxelBusters.NativePlugins.ePlayVideoFinishReason&)
extern "C" void MediaLibrary_ParsePlayVideoFinishedData_m8_1380 (MediaLibrary_t8_245 * __this, String_t* ____reasonString, int32_t* ____finishReason, const MethodInfo* method)
{
	{
		int32_t* L_0 = ____finishReason;
		*((int32_t*)(L_0)) = (int32_t)1;
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.MediaLibrary::Awake()
extern "C" void MediaLibrary_Awake_m8_1381 (MediaLibrary_t8_245 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Boolean VoxelBusters.NativePlugins.MediaLibrary::IsCameraSupported()
extern TypeInfo* Boolean_t1_20_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5425;
extern "C" bool MediaLibrary_IsCameraSupported_m8_1382 (MediaLibrary_t8_245 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Boolean_t1_20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(54);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5425 = il2cpp_codegen_string_literal_from_index(5425);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		V_0 = 0;
		bool L_0 = V_0;
		bool L_1 = L_0;
		Object_t * L_2 = Box(Boolean_t1_20_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m1_556(NULL /*static, unused*/, _stringLiteral5425, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_Log_m8_1007(NULL /*static, unused*/, _stringLiteral5368, L_3, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		bool L_4 = V_0;
		return L_4;
	}
}
// System.Void VoxelBusters.NativePlugins.MediaLibrary::PickImage(VoxelBusters.NativePlugins.eImageSource,System.Single,VoxelBusters.NativePlugins.MediaLibrary/PickImageCompletion)
extern TypeInfo* MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var;
extern "C" void MediaLibrary_PickImage_m8_1383 (MediaLibrary_t8_245 * __this, int32_t ____source, float ____scaleFactor, PickImageCompletion_t8_240 * ____onCompletion, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1959);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var);
		MonoBehaviourExtensions_PauseUnity_m8_210(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		float L_0 = ____scaleFactor;
		__this->___m_scaleFactor_6 = L_0;
		PickImageCompletion_t8_240 * L_1 = ____onCompletion;
		__this->___OnPickImageFinished_2 = L_1;
		float L_2 = ____scaleFactor;
		if ((!(((float)L_2) <= ((float)(0.0f)))))
		{
			goto IL_0028;
		}
	}
	{
		MediaLibrary_PickImageFinished_m8_1371(__this, (String_t*)NULL, 2, /*hidden argument*/NULL);
		return;
	}

IL_0028:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.MediaLibrary::SaveScreenshotToGallery(VoxelBusters.NativePlugins.MediaLibrary/SaveImageToGalleryCompletion)
extern TypeInfo* U3CSaveScreenshotToGalleryU3Ec__AnonStoreyC_t8_246_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t1_1902_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CSaveScreenshotToGalleryU3Ec__AnonStoreyC_U3CU3Em__D_m8_1366_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m1_15066_MethodInfo_var;
extern "C" void MediaLibrary_SaveScreenshotToGallery_m8_1384 (MediaLibrary_t8_245 * __this, SaveImageToGalleryCompletion_t8_241 * ____onCompletion, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CSaveScreenshotToGalleryU3Ec__AnonStoreyC_t8_246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2137);
		Action_1_t1_1902_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2138);
		U3CSaveScreenshotToGalleryU3Ec__AnonStoreyC_U3CU3Em__D_m8_1366_MethodInfo_var = il2cpp_codegen_method_info_from_index(583);
		Action_1__ctor_m1_15066_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484232);
		s_Il2CppMethodIntialized = true;
	}
	U3CSaveScreenshotToGalleryU3Ec__AnonStoreyC_t8_246 * V_0 = {0};
	{
		U3CSaveScreenshotToGalleryU3Ec__AnonStoreyC_t8_246 * L_0 = (U3CSaveScreenshotToGalleryU3Ec__AnonStoreyC_t8_246 *)il2cpp_codegen_object_new (U3CSaveScreenshotToGalleryU3Ec__AnonStoreyC_t8_246_il2cpp_TypeInfo_var);
		U3CSaveScreenshotToGalleryU3Ec__AnonStoreyC__ctor_m8_1365(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CSaveScreenshotToGalleryU3Ec__AnonStoreyC_t8_246 * L_1 = V_0;
		SaveImageToGalleryCompletion_t8_241 * L_2 = ____onCompletion;
		NullCheck(L_1);
		L_1->____onCompletion_0 = L_2;
		U3CSaveScreenshotToGalleryU3Ec__AnonStoreyC_t8_246 * L_3 = V_0;
		NullCheck(L_3);
		L_3->___U3CU3Ef__this_1 = __this;
		U3CSaveScreenshotToGalleryU3Ec__AnonStoreyC_t8_246 * L_4 = V_0;
		IntPtr_t L_5 = { (void*)U3CSaveScreenshotToGalleryU3Ec__AnonStoreyC_U3CU3Em__D_m8_1366_MethodInfo_var };
		Action_1_t1_1902 * L_6 = (Action_1_t1_1902 *)il2cpp_codegen_object_new (Action_1_t1_1902_il2cpp_TypeInfo_var);
		Action_1__ctor_m1_15066(L_6, L_4, L_5, /*hidden argument*/Action_1__ctor_m1_15066_MethodInfo_var);
		Object_t * L_7 = TextureExtensions_TakeScreenshot_m8_239(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m6_672(__this, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.MediaLibrary::SaveImageToGallery(VoxelBusters.Utility.URL,VoxelBusters.NativePlugins.MediaLibrary/SaveImageToGalleryCompletion)
extern TypeInfo* U3CSaveImageToGalleryU3Ec__AnonStoreyD_t8_247_il2cpp_TypeInfo_var;
extern TypeInfo* DownloadTexture_t8_162_il2cpp_TypeInfo_var;
extern TypeInfo* Completion_t8_161_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CSaveImageToGalleryU3Ec__AnonStoreyD_U3CU3Em__E_m8_1368_MethodInfo_var;
extern "C" void MediaLibrary_SaveImageToGallery_m8_1385 (MediaLibrary_t8_245 * __this, URL_t8_156  ____URL, SaveImageToGalleryCompletion_t8_241 * ____onCompletion, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CSaveImageToGalleryU3Ec__AnonStoreyD_t8_247_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2139);
		DownloadTexture_t8_162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2087);
		Completion_t8_161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2088);
		U3CSaveImageToGalleryU3Ec__AnonStoreyD_U3CU3Em__E_m8_1368_MethodInfo_var = il2cpp_codegen_method_info_from_index(585);
		s_Il2CppMethodIntialized = true;
	}
	DownloadTexture_t8_162 * V_0 = {0};
	U3CSaveImageToGalleryU3Ec__AnonStoreyD_t8_247 * V_1 = {0};
	{
		U3CSaveImageToGalleryU3Ec__AnonStoreyD_t8_247 * L_0 = (U3CSaveImageToGalleryU3Ec__AnonStoreyD_t8_247 *)il2cpp_codegen_object_new (U3CSaveImageToGalleryU3Ec__AnonStoreyD_t8_247_il2cpp_TypeInfo_var);
		U3CSaveImageToGalleryU3Ec__AnonStoreyD__ctor_m8_1367(L_0, /*hidden argument*/NULL);
		V_1 = L_0;
		U3CSaveImageToGalleryU3Ec__AnonStoreyD_t8_247 * L_1 = V_1;
		URL_t8_156  L_2 = ____URL;
		NullCheck(L_1);
		L_1->____URL_0 = L_2;
		U3CSaveImageToGalleryU3Ec__AnonStoreyD_t8_247 * L_3 = V_1;
		SaveImageToGalleryCompletion_t8_241 * L_4 = ____onCompletion;
		NullCheck(L_3);
		L_3->____onCompletion_1 = L_4;
		U3CSaveImageToGalleryU3Ec__AnonStoreyD_t8_247 * L_5 = V_1;
		NullCheck(L_5);
		L_5->___U3CU3Ef__this_2 = __this;
		U3CSaveImageToGalleryU3Ec__AnonStoreyD_t8_247 * L_6 = V_1;
		NullCheck(L_6);
		URL_t8_156  L_7 = (L_6->____URL_0);
		DownloadTexture_t8_162 * L_8 = (DownloadTexture_t8_162 *)il2cpp_codegen_object_new (DownloadTexture_t8_162_il2cpp_TypeInfo_var);
		DownloadTexture__ctor_m8_929(L_8, L_7, 1, 0, /*hidden argument*/NULL);
		V_0 = L_8;
		DownloadTexture_t8_162 * L_9 = V_0;
		U3CSaveImageToGalleryU3Ec__AnonStoreyD_t8_247 * L_10 = V_1;
		IntPtr_t L_11 = { (void*)U3CSaveImageToGalleryU3Ec__AnonStoreyD_U3CU3Em__E_m8_1368_MethodInfo_var };
		Completion_t8_161 * L_12 = (Completion_t8_161 *)il2cpp_codegen_object_new (Completion_t8_161_il2cpp_TypeInfo_var);
		Completion__ctor_m8_925(L_12, L_10, L_11, /*hidden argument*/NULL);
		NullCheck(L_9);
		DownloadTexture_set_OnCompletion_m8_935(L_9, L_12, /*hidden argument*/NULL);
		DownloadTexture_t8_162 * L_13 = V_0;
		NullCheck(L_13);
		Request_StartRequest_m8_900(L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.MediaLibrary::SaveImageToGallery(UnityEngine.Texture2D,VoxelBusters.NativePlugins.MediaLibrary/SaveImageToGalleryCompletion)
extern "C" void MediaLibrary_SaveImageToGallery_m8_1386 (MediaLibrary_t8_245 * __this, Texture2D_t6_33 * ____texture, SaveImageToGalleryCompletion_t8_241 * ____onCompletion, const MethodInfo* method)
{
	ByteU5BU5D_t1_109* V_0 = {0};
	{
		V_0 = (ByteU5BU5D_t1_109*)NULL;
		Texture2D_t6_33 * L_0 = ____texture;
		bool L_1 = Object_op_Inequality_m6_722(NULL /*static, unused*/, L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0015;
		}
	}
	{
		Texture2D_t6_33 * L_2 = ____texture;
		NullCheck(L_2);
		ByteU5BU5D_t1_109* L_3 = Texture2D_EncodeToPNG_m6_172(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
	}

IL_0015:
	{
		ByteU5BU5D_t1_109* L_4 = V_0;
		SaveImageToGalleryCompletion_t8_241 * L_5 = ____onCompletion;
		VirtActionInvoker2< ByteU5BU5D_t1_109*, SaveImageToGalleryCompletion_t8_241 * >::Invoke(10 /* System.Void VoxelBusters.NativePlugins.MediaLibrary::SaveImageToGallery(System.Byte[],VoxelBusters.NativePlugins.MediaLibrary/SaveImageToGalleryCompletion) */, __this, L_4, L_5);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.MediaLibrary::SaveImageToGallery(System.Byte[],VoxelBusters.NativePlugins.MediaLibrary/SaveImageToGalleryCompletion)
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5426;
extern "C" void MediaLibrary_SaveImageToGallery_m8_1387 (MediaLibrary_t8_245 * __this, ByteU5BU5D_t1_109* ____imageByteArray, SaveImageToGalleryCompletion_t8_241 * ____onCompletion, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5426 = il2cpp_codegen_string_literal_from_index(5426);
		s_Il2CppMethodIntialized = true;
	}
	{
		SaveImageToGalleryCompletion_t8_241 * L_0 = ____onCompletion;
		__this->___OnSaveImageToGalleryFinished_3 = L_0;
		ByteU5BU5D_t1_109* L_1 = ____imageByteArray;
		if (L_1)
		{
			goto IL_0025;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_LogError_m8_1009(NULL /*static, unused*/, _stringLiteral5368, _stringLiteral5426, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		MediaLibrary_SaveImageToGalleryFinished_m8_1373(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.MediaLibrary::PlayYoutubeVideo(System.String,VoxelBusters.NativePlugins.MediaLibrary/PlayVideoCompletion)
extern TypeInfo* MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5427;
extern "C" void MediaLibrary_PlayYoutubeVideo_m8_1388 (MediaLibrary_t8_245 * __this, String_t* ____videoID, PlayVideoCompletion_t8_243 * ____onCompletion, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1959);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5427 = il2cpp_codegen_string_literal_from_index(5427);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var);
		MonoBehaviourExtensions_PauseUnity_m8_210(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		PlayVideoCompletion_t8_243 * L_0 = ____onCompletion;
		__this->___OnPlayVideoFinished_5 = L_0;
		String_t* L_1 = ____videoID;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_IsNullOrEmpty_m1_521(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0030;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_LogError_m8_1009(NULL /*static, unused*/, _stringLiteral5368, _stringLiteral5427, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		MediaLibrary_PlayVideoFinished_m8_1377(__this, 1, /*hidden argument*/NULL);
		return;
	}

IL_0030:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.MediaLibrary::PlayEmbeddedVideo(System.String,VoxelBusters.NativePlugins.MediaLibrary/PlayVideoCompletion)
extern TypeInfo* MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5428;
extern "C" void MediaLibrary_PlayEmbeddedVideo_m8_1389 (MediaLibrary_t8_245 * __this, String_t* ____embedHTMLString, PlayVideoCompletion_t8_243 * ____onCompletion, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1959);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5428 = il2cpp_codegen_string_literal_from_index(5428);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var);
		MonoBehaviourExtensions_PauseUnity_m8_210(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		PlayVideoCompletion_t8_243 * L_0 = ____onCompletion;
		__this->___OnPlayVideoFinished_5 = L_0;
		String_t* L_1 = ____embedHTMLString;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_IsNullOrEmpty_m1_521(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0030;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_LogError_m8_1009(NULL /*static, unused*/, _stringLiteral5368, _stringLiteral5428, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		MediaLibrary_PlayVideoFinished_m8_1377(__this, 1, /*hidden argument*/NULL);
		return;
	}

IL_0030:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.MediaLibrary::PlayVideoFromURL(VoxelBusters.Utility.URL,VoxelBusters.NativePlugins.MediaLibrary/PlayVideoCompletion)
extern TypeInfo* MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5429;
extern "C" void MediaLibrary_PlayVideoFromURL_m8_1390 (MediaLibrary_t8_245 * __this, URL_t8_156  ____URL, PlayVideoCompletion_t8_243 * ____onCompletion, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1959);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5429 = il2cpp_codegen_string_literal_from_index(5429);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var);
		MonoBehaviourExtensions_PauseUnity_m8_210(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		PlayVideoCompletion_t8_243 * L_0 = ____onCompletion;
		__this->___OnPlayVideoFinished_5 = L_0;
		String_t* L_1 = URL_get_URLString_m8_958((&____URL), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_IsNullOrEmpty_m1_521(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0036;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_LogError_m8_1009(NULL /*static, unused*/, _stringLiteral5368, _stringLiteral5429, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		MediaLibrary_PlayVideoFinished_m8_1377(__this, 1, /*hidden argument*/NULL);
		return;
	}

IL_0036:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.MediaLibrary::PlayVideoFromGallery(VoxelBusters.NativePlugins.MediaLibrary/PickVideoCompletion,VoxelBusters.NativePlugins.MediaLibrary/PlayVideoCompletion)
extern TypeInfo* MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var;
extern "C" void MediaLibrary_PlayVideoFromGallery_m8_1391 (MediaLibrary_t8_245 * __this, PickVideoCompletion_t8_242 * ____onPickVideoCompletion, PlayVideoCompletion_t8_243 * ____onPlayVideoCompletion, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1959);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var);
		MonoBehaviourExtensions_PauseUnity_m8_210(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		PickVideoCompletion_t8_242 * L_0 = ____onPickVideoCompletion;
		__this->___OnPickVideoFinished_4 = L_0;
		PlayVideoCompletion_t8_243 * L_1 = ____onPlayVideoCompletion;
		__this->___OnPlayVideoFinished_5 = L_1;
		return;
	}
}
// System.String VoxelBusters.NativePlugins.MediaLibrary::ExtractYoutubeVideoID(System.String)
extern TypeInfo* Regex_t3_11_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t1_193_il2cpp_TypeInfo_var;
extern TypeInfo* Group_t3_15_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5430;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5431;
extern "C" String_t* MediaLibrary_ExtractYoutubeVideoID_m8_1392 (MediaLibrary_t8_245 * __this, String_t* ____url, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Regex_t3_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1211);
		IEnumerator_t1_193_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(144);
		Group_t3_15_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1504);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		_stringLiteral5430 = il2cpp_codegen_string_literal_from_index(5430);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5431 = il2cpp_codegen_string_literal_from_index(5431);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	Match_t3_13 * V_1 = {0};
	Group_t3_15 * V_2 = {0};
	Object_t * V_3 = {0};
	Object_t * V_4 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (String_t*)NULL;
		String_t* L_0 = ____url;
		IL2CPP_RUNTIME_CLASS_INIT(Regex_t3_11_il2cpp_TypeInfo_var);
		Match_t3_13 * L_1 = Regex_Match_m3_1388(NULL /*static, unused*/, L_0, _stringLiteral5430, 1, /*hidden argument*/NULL);
		V_1 = L_1;
		Match_t3_13 * L_2 = V_1;
		NullCheck(L_2);
		bool L_3 = Group_get_Success_m3_19(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_009a;
		}
	}
	{
		Match_t3_13 * L_4 = V_1;
		NullCheck(L_4);
		GroupCollection_t3_14 * L_5 = (GroupCollection_t3_14 *)VirtFuncInvoker0< GroupCollection_t3_14 * >::Invoke(4 /* System.Text.RegularExpressions.GroupCollection System.Text.RegularExpressions.Match::get_Groups() */, L_4);
		NullCheck(L_5);
		Object_t * L_6 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(8 /* System.Collections.IEnumerator System.Text.RegularExpressions.GroupCollection::GetEnumerator() */, L_5);
		V_3 = L_6;
	}

IL_0026:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0052;
		}

IL_002b:
		{
			Object_t * L_7 = V_3;
			NullCheck(L_7);
			Object_t * L_8 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_7);
			V_2 = ((Group_t3_15 *)CastclassClass(L_8, Group_t3_15_il2cpp_TypeInfo_var));
			Group_t3_15 * L_9 = V_2;
			NullCheck(L_9);
			String_t* L_10 = Capture_get_Value_m3_21(L_9, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_11 = String_Concat_m1_559(NULL /*static, unused*/, _stringLiteral5431, L_10, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
			Console_Log_m8_1007(NULL /*static, unused*/, _stringLiteral5368, L_11, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		}

IL_0052:
		{
			Object_t * L_12 = V_3;
			NullCheck(L_12);
			bool L_13 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_12);
			if (L_13)
			{
				goto IL_002b;
			}
		}

IL_005d:
		{
			IL2CPP_LEAVE(0x77, FINALLY_0062);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0062;
	}

FINALLY_0062:
	{ // begin finally (depth: 1)
		{
			Object_t * L_14 = V_3;
			V_4 = ((Object_t *)IsInst(L_14, IDisposable_t1_1035_il2cpp_TypeInfo_var));
			Object_t * L_15 = V_4;
			if (L_15)
			{
				goto IL_006f;
			}
		}

IL_006e:
		{
			IL2CPP_END_FINALLY(98)
		}

IL_006f:
		{
			Object_t * L_16 = V_4;
			NullCheck(L_16);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_16);
			IL2CPP_END_FINALLY(98)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(98)
	{
		IL2CPP_JUMP_TBL(0x77, IL_0077)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0077:
	{
		Match_t3_13 * L_17 = V_1;
		NullCheck(L_17);
		GroupCollection_t3_14 * L_18 = (GroupCollection_t3_14 *)VirtFuncInvoker0< GroupCollection_t3_14 * >::Invoke(4 /* System.Text.RegularExpressions.GroupCollection System.Text.RegularExpressions.Match::get_Groups() */, L_17);
		NullCheck(L_18);
		int32_t L_19 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 System.Text.RegularExpressions.GroupCollection::get_Count() */, L_18);
		if ((((int32_t)L_19) <= ((int32_t)1)))
		{
			goto IL_009a;
		}
	}
	{
		Match_t3_13 * L_20 = V_1;
		NullCheck(L_20);
		GroupCollection_t3_14 * L_21 = (GroupCollection_t3_14 *)VirtFuncInvoker0< GroupCollection_t3_14 * >::Invoke(4 /* System.Text.RegularExpressions.GroupCollection System.Text.RegularExpressions.Match::get_Groups() */, L_20);
		NullCheck(L_21);
		Group_t3_15 * L_22 = GroupCollection_get_Item_m3_20(L_21, 1, /*hidden argument*/NULL);
		NullCheck(L_22);
		String_t* L_23 = Capture_get_Value_m3_21(L_22, /*hidden argument*/NULL);
		V_0 = L_23;
	}

IL_009a:
	{
		String_t* L_24 = V_0;
		return L_24;
	}
}
// System.String VoxelBusters.NativePlugins.MediaLibrary::GetYoutubeEmbedHTMLString(System.String)
extern const Il2CppType* TextAsset_t6_68_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* TextAsset_t6_68_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5432;
extern Il2CppCodeGenString* _stringLiteral5433;
extern "C" String_t* MediaLibrary_GetYoutubeEmbedHTMLString_m8_1393 (MediaLibrary_t8_245 * __this, String_t* ____videoID, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TextAsset_t6_68_0_0_0_var = il2cpp_codegen_type_from_index(2140);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		TextAsset_t6_68_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2140);
		_stringLiteral5432 = il2cpp_codegen_string_literal_from_index(5432);
		_stringLiteral5433 = il2cpp_codegen_string_literal_from_index(5433);
		s_Il2CppMethodIntialized = true;
	}
	TextAsset_t6_68 * V_0 = {0};
	String_t* V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(TextAsset_t6_68_0_0_0_var), /*hidden argument*/NULL);
		Object_t6_5 * L_1 = Resources_Load_m6_488(NULL /*static, unused*/, _stringLiteral5432, L_0, /*hidden argument*/NULL);
		V_0 = ((TextAsset_t6_68 *)IsInstClass(L_1, TextAsset_t6_68_il2cpp_TypeInfo_var));
		V_1 = (String_t*)NULL;
		TextAsset_t6_68 * L_2 = V_0;
		bool L_3 = Object_op_Inequality_m6_722(NULL /*static, unused*/, L_2, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003a;
		}
	}
	{
		TextAsset_t6_68 * L_4 = V_0;
		NullCheck(L_4);
		String_t* L_5 = TextAsset_get_text_m6_489(L_4, /*hidden argument*/NULL);
		String_t* L_6 = ____videoID;
		NullCheck(L_5);
		String_t* L_7 = String_Replace_m1_536(L_5, _stringLiteral5433, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
	}

IL_003a:
	{
		String_t* L_8 = V_1;
		return L_8;
	}
}
// System.Void VoxelBusters.NativePlugins.MediaLibrarySettings/AndroidSettings::.ctor()
extern "C" void AndroidSettings__ctor_m8_1394 (AndroidSettings_t8_252 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String VoxelBusters.NativePlugins.MediaLibrarySettings/AndroidSettings::get_YoutubeAPIKey()
extern "C" String_t* AndroidSettings_get_YoutubeAPIKey_m8_1395 (AndroidSettings_t8_252 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_youtubeAPIKey_0);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.MediaLibrarySettings/AndroidSettings::set_YoutubeAPIKey(System.String)
extern "C" void AndroidSettings_set_YoutubeAPIKey_m8_1396 (AndroidSettings_t8_252 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_youtubeAPIKey_0 = L_0;
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.MediaLibrarySettings::.ctor()
extern TypeInfo* AndroidSettings_t8_252_il2cpp_TypeInfo_var;
extern "C" void MediaLibrarySettings__ctor_m8_1397 (MediaLibrarySettings_t8_253 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AndroidSettings_t8_252_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2141);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		AndroidSettings_t8_252 * L_0 = (AndroidSettings_t8_252 *)il2cpp_codegen_object_new (AndroidSettings_t8_252_il2cpp_TypeInfo_var);
		AndroidSettings__ctor_m8_1394(L_0, /*hidden argument*/NULL);
		MediaLibrarySettings_set_Android_m8_1399(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// VoxelBusters.NativePlugins.MediaLibrarySettings/AndroidSettings VoxelBusters.NativePlugins.MediaLibrarySettings::get_Android()
extern "C" AndroidSettings_t8_252 * MediaLibrarySettings_get_Android_m8_1398 (MediaLibrarySettings_t8_253 * __this, const MethodInfo* method)
{
	{
		AndroidSettings_t8_252 * L_0 = (__this->___m_android_0);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.MediaLibrarySettings::set_Android(VoxelBusters.NativePlugins.MediaLibrarySettings/AndroidSettings)
extern "C" void MediaLibrarySettings_set_Android_m8_1399 (MediaLibrarySettings_t8_253 * __this, AndroidSettings_t8_252 * ___value, const MethodInfo* method)
{
	{
		AndroidSettings_t8_252 * L_0 = ___value;
		__this->___m_android_0 = L_0;
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.NetworkConnectivitySettings/EditorSettings::.ctor()
extern "C" void EditorSettings__ctor_m8_1400 (EditorSettings_t8_254 * __this, const MethodInfo* method)
{
	{
		__this->___m_timeOutPeriod_0 = ((int32_t)60);
		__this->___m_maxRetryCount_1 = 2;
		__this->___m_timeGapBetweenPolling_2 = (2.0f);
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 VoxelBusters.NativePlugins.NetworkConnectivitySettings/EditorSettings::get_TimeOutPeriod()
extern "C" int32_t EditorSettings_get_TimeOutPeriod_m8_1401 (EditorSettings_t8_254 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_timeOutPeriod_0);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.NetworkConnectivitySettings/EditorSettings::set_TimeOutPeriod(System.Int32)
extern "C" void EditorSettings_set_TimeOutPeriod_m8_1402 (EditorSettings_t8_254 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_timeOutPeriod_0 = L_0;
		return;
	}
}
// System.Int32 VoxelBusters.NativePlugins.NetworkConnectivitySettings/EditorSettings::get_MaxRetryCount()
extern "C" int32_t EditorSettings_get_MaxRetryCount_m8_1403 (EditorSettings_t8_254 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_maxRetryCount_1);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.NetworkConnectivitySettings/EditorSettings::set_MaxRetryCount(System.Int32)
extern "C" void EditorSettings_set_MaxRetryCount_m8_1404 (EditorSettings_t8_254 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_maxRetryCount_1 = L_0;
		return;
	}
}
// System.Single VoxelBusters.NativePlugins.NetworkConnectivitySettings/EditorSettings::get_TimeGapBetweenPolling()
extern "C" float EditorSettings_get_TimeGapBetweenPolling_m8_1405 (EditorSettings_t8_254 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_timeGapBetweenPolling_2);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.NetworkConnectivitySettings/EditorSettings::set_TimeGapBetweenPolling(System.Single)
extern "C" void EditorSettings_set_TimeGapBetweenPolling_m8_1406 (EditorSettings_t8_254 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_timeGapBetweenPolling_2 = L_0;
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.NetworkConnectivitySettings/AndroidSettings::.ctor()
extern "C" void AndroidSettings__ctor_m8_1407 (AndroidSettings_t8_255 * __this, const MethodInfo* method)
{
	{
		__this->___m_port_0 = ((int32_t)53);
		__this->___m_timeOutPeriod_1 = ((int32_t)60);
		__this->___m_maxRetryCount_2 = 2;
		__this->___m_timeGapBetweenPolling_3 = (2.0f);
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 VoxelBusters.NativePlugins.NetworkConnectivitySettings/AndroidSettings::get_Port()
extern "C" int32_t AndroidSettings_get_Port_m8_1408 (AndroidSettings_t8_255 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_port_0);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.NetworkConnectivitySettings/AndroidSettings::set_Port(System.Int32)
extern "C" void AndroidSettings_set_Port_m8_1409 (AndroidSettings_t8_255 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_port_0 = L_0;
		return;
	}
}
// System.Int32 VoxelBusters.NativePlugins.NetworkConnectivitySettings/AndroidSettings::get_TimeOutPeriod()
extern "C" int32_t AndroidSettings_get_TimeOutPeriod_m8_1410 (AndroidSettings_t8_255 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_timeOutPeriod_1);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.NetworkConnectivitySettings/AndroidSettings::set_TimeOutPeriod(System.Int32)
extern "C" void AndroidSettings_set_TimeOutPeriod_m8_1411 (AndroidSettings_t8_255 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_timeOutPeriod_1 = L_0;
		return;
	}
}
// System.Int32 VoxelBusters.NativePlugins.NetworkConnectivitySettings/AndroidSettings::get_MaxRetryCount()
extern "C" int32_t AndroidSettings_get_MaxRetryCount_m8_1412 (AndroidSettings_t8_255 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_maxRetryCount_2);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.NetworkConnectivitySettings/AndroidSettings::set_MaxRetryCount(System.Int32)
extern "C" void AndroidSettings_set_MaxRetryCount_m8_1413 (AndroidSettings_t8_255 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_maxRetryCount_2 = L_0;
		return;
	}
}
// System.Single VoxelBusters.NativePlugins.NetworkConnectivitySettings/AndroidSettings::get_TimeGapBetweenPolling()
extern "C" float AndroidSettings_get_TimeGapBetweenPolling_m8_1414 (AndroidSettings_t8_255 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_timeGapBetweenPolling_3);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.NetworkConnectivitySettings/AndroidSettings::set_TimeGapBetweenPolling(System.Single)
extern "C" void AndroidSettings_set_TimeGapBetweenPolling_m8_1415 (AndroidSettings_t8_255 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_timeGapBetweenPolling_3 = L_0;
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.NetworkConnectivitySettings::.ctor()
extern TypeInfo* AndroidSettings_t8_255_il2cpp_TypeInfo_var;
extern TypeInfo* EditorSettings_t8_254_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5435;
extern "C" void NetworkConnectivitySettings__ctor_m8_1416 (NetworkConnectivitySettings_t8_256 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AndroidSettings_t8_255_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2142);
		EditorSettings_t8_254_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2143);
		_stringLiteral5435 = il2cpp_codegen_string_literal_from_index(5435);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___m_ipAddress_0 = _stringLiteral5435;
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		AndroidSettings_t8_255 * L_0 = (AndroidSettings_t8_255 *)il2cpp_codegen_object_new (AndroidSettings_t8_255_il2cpp_TypeInfo_var);
		AndroidSettings__ctor_m8_1407(L_0, /*hidden argument*/NULL);
		NetworkConnectivitySettings_set_Android_m8_1422(__this, L_0, /*hidden argument*/NULL);
		EditorSettings_t8_254 * L_1 = (EditorSettings_t8_254 *)il2cpp_codegen_object_new (EditorSettings_t8_254_il2cpp_TypeInfo_var);
		EditorSettings__ctor_m8_1400(L_1, /*hidden argument*/NULL);
		NetworkConnectivitySettings_set_Editor_m8_1420(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String VoxelBusters.NativePlugins.NetworkConnectivitySettings::get_IPAddress()
extern "C" String_t* NetworkConnectivitySettings_get_IPAddress_m8_1417 (NetworkConnectivitySettings_t8_256 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_ipAddress_0);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.NetworkConnectivitySettings::set_IPAddress(System.String)
extern "C" void NetworkConnectivitySettings_set_IPAddress_m8_1418 (NetworkConnectivitySettings_t8_256 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_ipAddress_0 = L_0;
		return;
	}
}
// VoxelBusters.NativePlugins.NetworkConnectivitySettings/EditorSettings VoxelBusters.NativePlugins.NetworkConnectivitySettings::get_Editor()
extern "C" EditorSettings_t8_254 * NetworkConnectivitySettings_get_Editor_m8_1419 (NetworkConnectivitySettings_t8_256 * __this, const MethodInfo* method)
{
	{
		EditorSettings_t8_254 * L_0 = (__this->___m_editor_1);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.NetworkConnectivitySettings::set_Editor(VoxelBusters.NativePlugins.NetworkConnectivitySettings/EditorSettings)
extern "C" void NetworkConnectivitySettings_set_Editor_m8_1420 (NetworkConnectivitySettings_t8_256 * __this, EditorSettings_t8_254 * ___value, const MethodInfo* method)
{
	{
		EditorSettings_t8_254 * L_0 = ___value;
		__this->___m_editor_1 = L_0;
		return;
	}
}
// VoxelBusters.NativePlugins.NetworkConnectivitySettings/AndroidSettings VoxelBusters.NativePlugins.NetworkConnectivitySettings::get_Android()
extern "C" AndroidSettings_t8_255 * NetworkConnectivitySettings_get_Android_m8_1421 (NetworkConnectivitySettings_t8_256 * __this, const MethodInfo* method)
{
	{
		AndroidSettings_t8_255 * L_0 = (__this->___m_android_2);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.NetworkConnectivitySettings::set_Android(VoxelBusters.NativePlugins.NetworkConnectivitySettings/AndroidSettings)
extern "C" void NetworkConnectivitySettings_set_Android_m8_1422 (NetworkConnectivitySettings_t8_256 * __this, AndroidSettings_t8_255 * ___value, const MethodInfo* method)
{
	{
		AndroidSettings_t8_255 * L_0 = ___value;
		__this->___m_android_2 = L_0;
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.NotificationService/RegisterForRemoteNotificationCompletion::.ctor(System.Object,System.IntPtr)
extern "C" void RegisterForRemoteNotificationCompletion__ctor_m8_1423 (RegisterForRemoteNotificationCompletion_t8_257 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void VoxelBusters.NativePlugins.NotificationService/RegisterForRemoteNotificationCompletion::Invoke(System.String,System.String)
extern "C" void RegisterForRemoteNotificationCompletion_Invoke_m8_1424 (RegisterForRemoteNotificationCompletion_t8_257 * __this, String_t* ____deviceToken, String_t* ____error, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		RegisterForRemoteNotificationCompletion_Invoke_m8_1424((RegisterForRemoteNotificationCompletion_t8_257 *)__this->___prev_9,____deviceToken, ____error, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, String_t* ____deviceToken, String_t* ____error, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,____deviceToken, ____error,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, String_t* ____deviceToken, String_t* ____error, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,____deviceToken, ____error,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, String_t* ____error, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(____deviceToken, ____error,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_RegisterForRemoteNotificationCompletion_t8_257(Il2CppObject* delegate, String_t* ____deviceToken, String_t* ____error)
{
	typedef void (STDCALL *native_function_ptr_type)(char*, char*);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '____deviceToken' to native representation
	char* _____deviceToken_marshaled = { 0 };
	_____deviceToken_marshaled = il2cpp_codegen_marshal_string(____deviceToken);

	// Marshaling of parameter '____error' to native representation
	char* _____error_marshaled = { 0 };
	_____error_marshaled = il2cpp_codegen_marshal_string(____error);

	// Native function invocation
	_il2cpp_pinvoke_func(_____deviceToken_marshaled, _____error_marshaled);

	// Marshaling cleanup of parameter '____deviceToken' native representation
	il2cpp_codegen_marshal_free(_____deviceToken_marshaled);
	_____deviceToken_marshaled = NULL;

	// Marshaling cleanup of parameter '____error' native representation
	il2cpp_codegen_marshal_free(_____error_marshaled);
	_____error_marshaled = NULL;

}
// System.IAsyncResult VoxelBusters.NativePlugins.NotificationService/RegisterForRemoteNotificationCompletion::BeginInvoke(System.String,System.String,System.AsyncCallback,System.Object)
extern "C" Object_t * RegisterForRemoteNotificationCompletion_BeginInvoke_m8_1425 (RegisterForRemoteNotificationCompletion_t8_257 * __this, String_t* ____deviceToken, String_t* ____error, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ____deviceToken;
	__d_args[1] = ____error;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void VoxelBusters.NativePlugins.NotificationService/RegisterForRemoteNotificationCompletion::EndInvoke(System.IAsyncResult)
extern "C" void RegisterForRemoteNotificationCompletion_EndInvoke_m8_1426 (RegisterForRemoteNotificationCompletion_t8_257 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void VoxelBusters.NativePlugins.NotificationService/ReceivedNotificationResponse::.ctor(System.Object,System.IntPtr)
extern "C" void ReceivedNotificationResponse__ctor_m8_1427 (ReceivedNotificationResponse_t8_258 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void VoxelBusters.NativePlugins.NotificationService/ReceivedNotificationResponse::Invoke(VoxelBusters.NativePlugins.CrossPlatformNotification)
extern "C" void ReceivedNotificationResponse_Invoke_m8_1428 (ReceivedNotificationResponse_t8_258 * __this, CrossPlatformNotification_t8_259 * ____notification, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		ReceivedNotificationResponse_Invoke_m8_1428((ReceivedNotificationResponse_t8_258 *)__this->___prev_9,____notification, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, CrossPlatformNotification_t8_259 * ____notification, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,____notification,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, CrossPlatformNotification_t8_259 * ____notification, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,____notification,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(____notification,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_ReceivedNotificationResponse_t8_258(Il2CppObject* delegate, CrossPlatformNotification_t8_259 * ____notification)
{
	// Marshaling of parameter '____notification' to native representation
	CrossPlatformNotification_t8_259 * _____notification_marshaled = { 0 };
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'VoxelBusters.NativePlugins.CrossPlatformNotification'."));
}
// System.IAsyncResult VoxelBusters.NativePlugins.NotificationService/ReceivedNotificationResponse::BeginInvoke(VoxelBusters.NativePlugins.CrossPlatformNotification,System.AsyncCallback,System.Object)
extern "C" Object_t * ReceivedNotificationResponse_BeginInvoke_m8_1429 (ReceivedNotificationResponse_t8_258 * __this, CrossPlatformNotification_t8_259 * ____notification, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ____notification;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void VoxelBusters.NativePlugins.NotificationService/ReceivedNotificationResponse::EndInvoke(System.IAsyncResult)
extern "C" void ReceivedNotificationResponse_EndInvoke_m8_1430 (ReceivedNotificationResponse_t8_258 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void VoxelBusters.NativePlugins.NotificationService/<ProcessAppLaunchInfo>c__Iterator5::.ctor()
extern "C" void U3CProcessAppLaunchInfoU3Ec__Iterator5__ctor_m8_1431 (U3CProcessAppLaunchInfoU3Ec__Iterator5_t8_260 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object VoxelBusters.NativePlugins.NotificationService/<ProcessAppLaunchInfo>c__Iterator5::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CProcessAppLaunchInfoU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m8_1432 (U3CProcessAppLaunchInfoU3Ec__Iterator5_t8_260 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_1);
		return L_0;
	}
}
// System.Object VoxelBusters.NativePlugins.NotificationService/<ProcessAppLaunchInfo>c__Iterator5::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CProcessAppLaunchInfoU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m8_1433 (U3CProcessAppLaunchInfoU3Ec__Iterator5_t8_260 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_1);
		return L_0;
	}
}
// System.Boolean VoxelBusters.NativePlugins.NotificationService/<ProcessAppLaunchInfo>c__Iterator5::MoveNext()
extern TypeInfo* WaitForEndOfFrame_t6_13_il2cpp_TypeInfo_var;
extern TypeInfo* NotificationService_t8_261_il2cpp_TypeInfo_var;
extern "C" bool U3CProcessAppLaunchInfoU3Ec__Iterator5_MoveNext_m8_1434 (U3CProcessAppLaunchInfoU3Ec__Iterator5_t8_260 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WaitForEndOfFrame_t6_13_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1876);
		NotificationService_t8_261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2144);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_0);
		V_0 = L_0;
		__this->___U24PC_0 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0025;
		}
		if (L_1 == 1)
		{
			goto IL_003c;
		}
		if (L_1 == 2)
		{
			goto IL_0054;
		}
	}
	{
		goto IL_00c9;
	}

IL_0025:
	{
		WaitForEndOfFrame_t6_13 * L_2 = (WaitForEndOfFrame_t6_13 *)il2cpp_codegen_object_new (WaitForEndOfFrame_t6_13_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_m6_12(L_2, /*hidden argument*/NULL);
		__this->___U24current_1 = L_2;
		__this->___U24PC_0 = 1;
		goto IL_00cb;
	}

IL_003c:
	{
		goto IL_0054;
	}

IL_0041:
	{
		__this->___U24current_1 = NULL;
		__this->___U24PC_0 = 2;
		goto IL_00cb;
	}

IL_0054:
	{
		NotificationService_t8_261 * L_3 = (__this->___U3CU3Ef__this_2);
		NullCheck(L_3);
		bool L_4 = (L_3->___m_receivedAppLaunchInfo_5);
		if (!L_4)
		{
			goto IL_0041;
		}
	}
	{
		NotificationService_t8_261 * L_5 = (__this->___U3CU3Ef__this_2);
		NullCheck(L_5);
		CrossPlatformNotification_t8_259 * L_6 = (L_5->___m_launchLocalNotification_3);
		if (!L_6)
		{
			goto IL_0093;
		}
	}
	{
		ReceivedNotificationResponse_t8_258 * L_7 = ((NotificationService_t8_261_StaticFields*)NotificationService_t8_261_il2cpp_TypeInfo_var->static_fields)->___DidLaunchWithLocalNotificationEvent_7;
		if (!L_7)
		{
			goto IL_0093;
		}
	}
	{
		ReceivedNotificationResponse_t8_258 * L_8 = ((NotificationService_t8_261_StaticFields*)NotificationService_t8_261_il2cpp_TypeInfo_var->static_fields)->___DidLaunchWithLocalNotificationEvent_7;
		NotificationService_t8_261 * L_9 = (__this->___U3CU3Ef__this_2);
		NullCheck(L_9);
		CrossPlatformNotification_t8_259 * L_10 = (L_9->___m_launchLocalNotification_3);
		NullCheck(L_8);
		ReceivedNotificationResponse_Invoke_m8_1428(L_8, L_10, /*hidden argument*/NULL);
	}

IL_0093:
	{
		NotificationService_t8_261 * L_11 = (__this->___U3CU3Ef__this_2);
		NullCheck(L_11);
		CrossPlatformNotification_t8_259 * L_12 = (L_11->___m_launchRemoteNotification_4);
		if (!L_12)
		{
			goto IL_00c2;
		}
	}
	{
		ReceivedNotificationResponse_t8_258 * L_13 = ((NotificationService_t8_261_StaticFields*)NotificationService_t8_261_il2cpp_TypeInfo_var->static_fields)->___DidLaunchWithRemoteNotificationEvent_8;
		if (!L_13)
		{
			goto IL_00c2;
		}
	}
	{
		ReceivedNotificationResponse_t8_258 * L_14 = ((NotificationService_t8_261_StaticFields*)NotificationService_t8_261_il2cpp_TypeInfo_var->static_fields)->___DidLaunchWithRemoteNotificationEvent_8;
		NotificationService_t8_261 * L_15 = (__this->___U3CU3Ef__this_2);
		NullCheck(L_15);
		CrossPlatformNotification_t8_259 * L_16 = (L_15->___m_launchRemoteNotification_4);
		NullCheck(L_14);
		ReceivedNotificationResponse_Invoke_m8_1428(L_14, L_16, /*hidden argument*/NULL);
	}

IL_00c2:
	{
		__this->___U24PC_0 = (-1);
	}

IL_00c9:
	{
		return 0;
	}

IL_00cb:
	{
		return 1;
	}
	// Dead block : IL_00cd: ldloc.1
}
// System.Void VoxelBusters.NativePlugins.NotificationService/<ProcessAppLaunchInfo>c__Iterator5::Dispose()
extern "C" void U3CProcessAppLaunchInfoU3Ec__Iterator5_Dispose_m8_1435 (U3CProcessAppLaunchInfoU3Ec__Iterator5_t8_260 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_0 = (-1);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.NotificationService/<ProcessAppLaunchInfo>c__Iterator5::Reset()
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern "C" void U3CProcessAppLaunchInfoU3Ec__Iterator5_Reset_m8_1436 (U3CProcessAppLaunchInfoU3Ec__Iterator5_t8_260 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14416(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void VoxelBusters.NativePlugins.NotificationService::.ctor()
extern Il2CppCodeGenString* _stringLiteral5436;
extern "C" void NotificationService__ctor_m8_1437 (NotificationService_t8_261 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral5436 = il2cpp_codegen_string_literal_from_index(5436);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___notificationDefaultSoundName_2 = _stringLiteral5436;
		MonoBehaviour__ctor_m6_671(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.NotificationService::add_DidFinishRegisterForRemoteNotificationEvent(VoxelBusters.NativePlugins.NotificationService/RegisterForRemoteNotificationCompletion)
extern TypeInfo* NotificationService_t8_261_il2cpp_TypeInfo_var;
extern TypeInfo* RegisterForRemoteNotificationCompletion_t8_257_il2cpp_TypeInfo_var;
extern "C" void NotificationService_add_DidFinishRegisterForRemoteNotificationEvent_m8_1438 (Object_t * __this /* static, unused */, RegisterForRemoteNotificationCompletion_t8_257 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotificationService_t8_261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2144);
		RegisterForRemoteNotificationCompletion_t8_257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2105);
		s_Il2CppMethodIntialized = true;
	}
	{
		RegisterForRemoteNotificationCompletion_t8_257 * L_0 = ((NotificationService_t8_261_StaticFields*)NotificationService_t8_261_il2cpp_TypeInfo_var->static_fields)->___DidFinishRegisterForRemoteNotificationEvent_6;
		RegisterForRemoteNotificationCompletion_t8_257 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Combine_m1_908(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((NotificationService_t8_261_StaticFields*)NotificationService_t8_261_il2cpp_TypeInfo_var->static_fields)->___DidFinishRegisterForRemoteNotificationEvent_6 = ((RegisterForRemoteNotificationCompletion_t8_257 *)CastclassSealed(L_2, RegisterForRemoteNotificationCompletion_t8_257_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.NotificationService::remove_DidFinishRegisterForRemoteNotificationEvent(VoxelBusters.NativePlugins.NotificationService/RegisterForRemoteNotificationCompletion)
extern TypeInfo* NotificationService_t8_261_il2cpp_TypeInfo_var;
extern TypeInfo* RegisterForRemoteNotificationCompletion_t8_257_il2cpp_TypeInfo_var;
extern "C" void NotificationService_remove_DidFinishRegisterForRemoteNotificationEvent_m8_1439 (Object_t * __this /* static, unused */, RegisterForRemoteNotificationCompletion_t8_257 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotificationService_t8_261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2144);
		RegisterForRemoteNotificationCompletion_t8_257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2105);
		s_Il2CppMethodIntialized = true;
	}
	{
		RegisterForRemoteNotificationCompletion_t8_257 * L_0 = ((NotificationService_t8_261_StaticFields*)NotificationService_t8_261_il2cpp_TypeInfo_var->static_fields)->___DidFinishRegisterForRemoteNotificationEvent_6;
		RegisterForRemoteNotificationCompletion_t8_257 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Remove_m1_911(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((NotificationService_t8_261_StaticFields*)NotificationService_t8_261_il2cpp_TypeInfo_var->static_fields)->___DidFinishRegisterForRemoteNotificationEvent_6 = ((RegisterForRemoteNotificationCompletion_t8_257 *)CastclassSealed(L_2, RegisterForRemoteNotificationCompletion_t8_257_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.NotificationService::add_DidLaunchWithLocalNotificationEvent(VoxelBusters.NativePlugins.NotificationService/ReceivedNotificationResponse)
extern TypeInfo* NotificationService_t8_261_il2cpp_TypeInfo_var;
extern TypeInfo* ReceivedNotificationResponse_t8_258_il2cpp_TypeInfo_var;
extern "C" void NotificationService_add_DidLaunchWithLocalNotificationEvent_m8_1440 (Object_t * __this /* static, unused */, ReceivedNotificationResponse_t8_258 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotificationService_t8_261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2144);
		ReceivedNotificationResponse_t8_258_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2106);
		s_Il2CppMethodIntialized = true;
	}
	{
		ReceivedNotificationResponse_t8_258 * L_0 = ((NotificationService_t8_261_StaticFields*)NotificationService_t8_261_il2cpp_TypeInfo_var->static_fields)->___DidLaunchWithLocalNotificationEvent_7;
		ReceivedNotificationResponse_t8_258 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Combine_m1_908(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((NotificationService_t8_261_StaticFields*)NotificationService_t8_261_il2cpp_TypeInfo_var->static_fields)->___DidLaunchWithLocalNotificationEvent_7 = ((ReceivedNotificationResponse_t8_258 *)CastclassSealed(L_2, ReceivedNotificationResponse_t8_258_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.NotificationService::remove_DidLaunchWithLocalNotificationEvent(VoxelBusters.NativePlugins.NotificationService/ReceivedNotificationResponse)
extern TypeInfo* NotificationService_t8_261_il2cpp_TypeInfo_var;
extern TypeInfo* ReceivedNotificationResponse_t8_258_il2cpp_TypeInfo_var;
extern "C" void NotificationService_remove_DidLaunchWithLocalNotificationEvent_m8_1441 (Object_t * __this /* static, unused */, ReceivedNotificationResponse_t8_258 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotificationService_t8_261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2144);
		ReceivedNotificationResponse_t8_258_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2106);
		s_Il2CppMethodIntialized = true;
	}
	{
		ReceivedNotificationResponse_t8_258 * L_0 = ((NotificationService_t8_261_StaticFields*)NotificationService_t8_261_il2cpp_TypeInfo_var->static_fields)->___DidLaunchWithLocalNotificationEvent_7;
		ReceivedNotificationResponse_t8_258 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Remove_m1_911(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((NotificationService_t8_261_StaticFields*)NotificationService_t8_261_il2cpp_TypeInfo_var->static_fields)->___DidLaunchWithLocalNotificationEvent_7 = ((ReceivedNotificationResponse_t8_258 *)CastclassSealed(L_2, ReceivedNotificationResponse_t8_258_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.NotificationService::add_DidLaunchWithRemoteNotificationEvent(VoxelBusters.NativePlugins.NotificationService/ReceivedNotificationResponse)
extern TypeInfo* NotificationService_t8_261_il2cpp_TypeInfo_var;
extern TypeInfo* ReceivedNotificationResponse_t8_258_il2cpp_TypeInfo_var;
extern "C" void NotificationService_add_DidLaunchWithRemoteNotificationEvent_m8_1442 (Object_t * __this /* static, unused */, ReceivedNotificationResponse_t8_258 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotificationService_t8_261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2144);
		ReceivedNotificationResponse_t8_258_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2106);
		s_Il2CppMethodIntialized = true;
	}
	{
		ReceivedNotificationResponse_t8_258 * L_0 = ((NotificationService_t8_261_StaticFields*)NotificationService_t8_261_il2cpp_TypeInfo_var->static_fields)->___DidLaunchWithRemoteNotificationEvent_8;
		ReceivedNotificationResponse_t8_258 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Combine_m1_908(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((NotificationService_t8_261_StaticFields*)NotificationService_t8_261_il2cpp_TypeInfo_var->static_fields)->___DidLaunchWithRemoteNotificationEvent_8 = ((ReceivedNotificationResponse_t8_258 *)CastclassSealed(L_2, ReceivedNotificationResponse_t8_258_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.NotificationService::remove_DidLaunchWithRemoteNotificationEvent(VoxelBusters.NativePlugins.NotificationService/ReceivedNotificationResponse)
extern TypeInfo* NotificationService_t8_261_il2cpp_TypeInfo_var;
extern TypeInfo* ReceivedNotificationResponse_t8_258_il2cpp_TypeInfo_var;
extern "C" void NotificationService_remove_DidLaunchWithRemoteNotificationEvent_m8_1443 (Object_t * __this /* static, unused */, ReceivedNotificationResponse_t8_258 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotificationService_t8_261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2144);
		ReceivedNotificationResponse_t8_258_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2106);
		s_Il2CppMethodIntialized = true;
	}
	{
		ReceivedNotificationResponse_t8_258 * L_0 = ((NotificationService_t8_261_StaticFields*)NotificationService_t8_261_il2cpp_TypeInfo_var->static_fields)->___DidLaunchWithRemoteNotificationEvent_8;
		ReceivedNotificationResponse_t8_258 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Remove_m1_911(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((NotificationService_t8_261_StaticFields*)NotificationService_t8_261_il2cpp_TypeInfo_var->static_fields)->___DidLaunchWithRemoteNotificationEvent_8 = ((ReceivedNotificationResponse_t8_258 *)CastclassSealed(L_2, ReceivedNotificationResponse_t8_258_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.NotificationService::add_DidReceiveLocalNotificationEvent(VoxelBusters.NativePlugins.NotificationService/ReceivedNotificationResponse)
extern TypeInfo* NotificationService_t8_261_il2cpp_TypeInfo_var;
extern TypeInfo* ReceivedNotificationResponse_t8_258_il2cpp_TypeInfo_var;
extern "C" void NotificationService_add_DidReceiveLocalNotificationEvent_m8_1444 (Object_t * __this /* static, unused */, ReceivedNotificationResponse_t8_258 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotificationService_t8_261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2144);
		ReceivedNotificationResponse_t8_258_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2106);
		s_Il2CppMethodIntialized = true;
	}
	{
		ReceivedNotificationResponse_t8_258 * L_0 = ((NotificationService_t8_261_StaticFields*)NotificationService_t8_261_il2cpp_TypeInfo_var->static_fields)->___DidReceiveLocalNotificationEvent_9;
		ReceivedNotificationResponse_t8_258 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Combine_m1_908(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((NotificationService_t8_261_StaticFields*)NotificationService_t8_261_il2cpp_TypeInfo_var->static_fields)->___DidReceiveLocalNotificationEvent_9 = ((ReceivedNotificationResponse_t8_258 *)CastclassSealed(L_2, ReceivedNotificationResponse_t8_258_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.NotificationService::remove_DidReceiveLocalNotificationEvent(VoxelBusters.NativePlugins.NotificationService/ReceivedNotificationResponse)
extern TypeInfo* NotificationService_t8_261_il2cpp_TypeInfo_var;
extern TypeInfo* ReceivedNotificationResponse_t8_258_il2cpp_TypeInfo_var;
extern "C" void NotificationService_remove_DidReceiveLocalNotificationEvent_m8_1445 (Object_t * __this /* static, unused */, ReceivedNotificationResponse_t8_258 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotificationService_t8_261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2144);
		ReceivedNotificationResponse_t8_258_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2106);
		s_Il2CppMethodIntialized = true;
	}
	{
		ReceivedNotificationResponse_t8_258 * L_0 = ((NotificationService_t8_261_StaticFields*)NotificationService_t8_261_il2cpp_TypeInfo_var->static_fields)->___DidReceiveLocalNotificationEvent_9;
		ReceivedNotificationResponse_t8_258 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Remove_m1_911(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((NotificationService_t8_261_StaticFields*)NotificationService_t8_261_il2cpp_TypeInfo_var->static_fields)->___DidReceiveLocalNotificationEvent_9 = ((ReceivedNotificationResponse_t8_258 *)CastclassSealed(L_2, ReceivedNotificationResponse_t8_258_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.NotificationService::add_DidReceiveRemoteNotificationEvent(VoxelBusters.NativePlugins.NotificationService/ReceivedNotificationResponse)
extern TypeInfo* NotificationService_t8_261_il2cpp_TypeInfo_var;
extern TypeInfo* ReceivedNotificationResponse_t8_258_il2cpp_TypeInfo_var;
extern "C" void NotificationService_add_DidReceiveRemoteNotificationEvent_m8_1446 (Object_t * __this /* static, unused */, ReceivedNotificationResponse_t8_258 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotificationService_t8_261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2144);
		ReceivedNotificationResponse_t8_258_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2106);
		s_Il2CppMethodIntialized = true;
	}
	{
		ReceivedNotificationResponse_t8_258 * L_0 = ((NotificationService_t8_261_StaticFields*)NotificationService_t8_261_il2cpp_TypeInfo_var->static_fields)->___DidReceiveRemoteNotificationEvent_10;
		ReceivedNotificationResponse_t8_258 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Combine_m1_908(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((NotificationService_t8_261_StaticFields*)NotificationService_t8_261_il2cpp_TypeInfo_var->static_fields)->___DidReceiveRemoteNotificationEvent_10 = ((ReceivedNotificationResponse_t8_258 *)CastclassSealed(L_2, ReceivedNotificationResponse_t8_258_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.NotificationService::remove_DidReceiveRemoteNotificationEvent(VoxelBusters.NativePlugins.NotificationService/ReceivedNotificationResponse)
extern TypeInfo* NotificationService_t8_261_il2cpp_TypeInfo_var;
extern TypeInfo* ReceivedNotificationResponse_t8_258_il2cpp_TypeInfo_var;
extern "C" void NotificationService_remove_DidReceiveRemoteNotificationEvent_m8_1447 (Object_t * __this /* static, unused */, ReceivedNotificationResponse_t8_258 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotificationService_t8_261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2144);
		ReceivedNotificationResponse_t8_258_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2106);
		s_Il2CppMethodIntialized = true;
	}
	{
		ReceivedNotificationResponse_t8_258 * L_0 = ((NotificationService_t8_261_StaticFields*)NotificationService_t8_261_il2cpp_TypeInfo_var->static_fields)->___DidReceiveRemoteNotificationEvent_10;
		ReceivedNotificationResponse_t8_258 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Remove_m1_911(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((NotificationService_t8_261_StaticFields*)NotificationService_t8_261_il2cpp_TypeInfo_var->static_fields)->___DidReceiveRemoteNotificationEvent_10 = ((ReceivedNotificationResponse_t8_258 *)CastclassSealed(L_2, ReceivedNotificationResponse_t8_258_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.NotificationService::DidReceiveAppLaunchInfo(System.String)
extern "C" void NotificationService_DidReceiveAppLaunchInfo_m8_1448 (NotificationService_t8_261 * __this, String_t* ____launchData, const MethodInfo* method)
{
	CrossPlatformNotification_t8_259 * V_0 = {0};
	CrossPlatformNotification_t8_259 * V_1 = {0};
	{
		String_t* L_0 = ____launchData;
		VirtActionInvoker3< String_t*, CrossPlatformNotification_t8_259 **, CrossPlatformNotification_t8_259 ** >::Invoke(4 /* System.Void VoxelBusters.NativePlugins.NotificationService::ParseAppLaunchInfo(System.String,VoxelBusters.NativePlugins.CrossPlatformNotification&,VoxelBusters.NativePlugins.CrossPlatformNotification&) */, __this, L_0, (&V_0), (&V_1));
		CrossPlatformNotification_t8_259 * L_1 = V_0;
		CrossPlatformNotification_t8_259 * L_2 = V_1;
		NotificationService_DidReceiveAppLaunchInfo_m8_1449(__this, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.NotificationService::DidReceiveAppLaunchInfo(VoxelBusters.NativePlugins.CrossPlatformNotification,VoxelBusters.NativePlugins.CrossPlatformNotification)
extern "C" void NotificationService_DidReceiveAppLaunchInfo_m8_1449 (NotificationService_t8_261 * __this, CrossPlatformNotification_t8_259 * ____launchLocalNotification, CrossPlatformNotification_t8_259 * ____launchRemoteNotification, const MethodInfo* method)
{
	{
		__this->___m_receivedAppLaunchInfo_5 = 1;
		CrossPlatformNotification_t8_259 * L_0 = ____launchLocalNotification;
		__this->___m_launchLocalNotification_3 = L_0;
		CrossPlatformNotification_t8_259 * L_1 = ____launchRemoteNotification;
		__this->___m_launchRemoteNotification_4 = L_1;
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.NotificationService::DidReceiveLocalNotification(System.String)
extern "C" void NotificationService_DidReceiveLocalNotification_m8_1450 (NotificationService_t8_261 * __this, String_t* ____notificationPayload, const MethodInfo* method)
{
	CrossPlatformNotification_t8_259 * V_0 = {0};
	{
		String_t* L_0 = ____notificationPayload;
		VirtActionInvoker2< String_t*, CrossPlatformNotification_t8_259 ** >::Invoke(5 /* System.Void VoxelBusters.NativePlugins.NotificationService::ParseNotificationPayloadData(System.String,VoxelBusters.NativePlugins.CrossPlatformNotification&) */, __this, L_0, (&V_0));
		CrossPlatformNotification_t8_259 * L_1 = V_0;
		NotificationService_DidReceiveLocalNotification_m8_1451(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.NotificationService::DidReceiveLocalNotification(VoxelBusters.NativePlugins.CrossPlatformNotification)
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern TypeInfo* NotificationService_t8_261_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5437;
extern "C" void NotificationService_DidReceiveLocalNotification_m8_1451 (NotificationService_t8_261 * __this, CrossPlatformNotification_t8_259 * ____notification, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		NotificationService_t8_261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2144);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5437 = il2cpp_codegen_string_literal_from_index(5437);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_Log_m8_1007(NULL /*static, unused*/, _stringLiteral5368, _stringLiteral5437, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		ReceivedNotificationResponse_t8_258 * L_0 = ((NotificationService_t8_261_StaticFields*)NotificationService_t8_261_il2cpp_TypeInfo_var->static_fields)->___DidReceiveLocalNotificationEvent_9;
		if (!L_0)
		{
			goto IL_0025;
		}
	}
	{
		ReceivedNotificationResponse_t8_258 * L_1 = ((NotificationService_t8_261_StaticFields*)NotificationService_t8_261_il2cpp_TypeInfo_var->static_fields)->___DidReceiveLocalNotificationEvent_9;
		CrossPlatformNotification_t8_259 * L_2 = ____notification;
		NullCheck(L_1);
		ReceivedNotificationResponse_Invoke_m8_1428(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0025:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.NotificationService::DidRegisterRemoteNotification(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern TypeInfo* NotificationService_t8_261_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5438;
extern "C" void NotificationService_DidRegisterRemoteNotification_m8_1452 (NotificationService_t8_261 * __this, String_t* ____deviceToken, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		NotificationService_t8_261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2144);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5438 = il2cpp_codegen_string_literal_from_index(5438);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ____deviceToken;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m1_559(NULL /*static, unused*/, _stringLiteral5438, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_Log_m8_1007(NULL /*static, unused*/, _stringLiteral5368, L_1, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		RegisterForRemoteNotificationCompletion_t8_257 * L_2 = ((NotificationService_t8_261_StaticFields*)NotificationService_t8_261_il2cpp_TypeInfo_var->static_fields)->___DidFinishRegisterForRemoteNotificationEvent_6;
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		RegisterForRemoteNotificationCompletion_t8_257 * L_3 = ((NotificationService_t8_261_StaticFields*)NotificationService_t8_261_il2cpp_TypeInfo_var->static_fields)->___DidFinishRegisterForRemoteNotificationEvent_6;
		String_t* L_4 = ____deviceToken;
		NullCheck(L_3);
		RegisterForRemoteNotificationCompletion_Invoke_m8_1424(L_3, L_4, (String_t*)NULL, /*hidden argument*/NULL);
	}

IL_002c:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.NotificationService::DidFailToRegisterRemoteNotifications(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern TypeInfo* NotificationService_t8_261_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5439;
extern "C" void NotificationService_DidFailToRegisterRemoteNotifications_m8_1453 (NotificationService_t8_261 * __this, String_t* ____errorDescription, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		NotificationService_t8_261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2144);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5439 = il2cpp_codegen_string_literal_from_index(5439);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ____errorDescription;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m1_559(NULL /*static, unused*/, _stringLiteral5439, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_Log_m8_1007(NULL /*static, unused*/, _stringLiteral5368, L_1, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		RegisterForRemoteNotificationCompletion_t8_257 * L_2 = ((NotificationService_t8_261_StaticFields*)NotificationService_t8_261_il2cpp_TypeInfo_var->static_fields)->___DidFinishRegisterForRemoteNotificationEvent_6;
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		RegisterForRemoteNotificationCompletion_t8_257 * L_3 = ((NotificationService_t8_261_StaticFields*)NotificationService_t8_261_il2cpp_TypeInfo_var->static_fields)->___DidFinishRegisterForRemoteNotificationEvent_6;
		String_t* L_4 = ____errorDescription;
		NullCheck(L_3);
		RegisterForRemoteNotificationCompletion_Invoke_m8_1424(L_3, (String_t*)NULL, L_4, /*hidden argument*/NULL);
	}

IL_002c:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.NotificationService::DidReceiveRemoteNotification(System.String)
extern "C" void NotificationService_DidReceiveRemoteNotification_m8_1454 (NotificationService_t8_261 * __this, String_t* ____notificationPayload, const MethodInfo* method)
{
	CrossPlatformNotification_t8_259 * V_0 = {0};
	{
		String_t* L_0 = ____notificationPayload;
		VirtActionInvoker2< String_t*, CrossPlatformNotification_t8_259 ** >::Invoke(5 /* System.Void VoxelBusters.NativePlugins.NotificationService::ParseNotificationPayloadData(System.String,VoxelBusters.NativePlugins.CrossPlatformNotification&) */, __this, L_0, (&V_0));
		CrossPlatformNotification_t8_259 * L_1 = V_0;
		NotificationService_DidReceiveRemoteNotification_m8_1455(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.NotificationService::DidReceiveRemoteNotification(VoxelBusters.NativePlugins.CrossPlatformNotification)
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern TypeInfo* NotificationService_t8_261_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5440;
extern "C" void NotificationService_DidReceiveRemoteNotification_m8_1455 (NotificationService_t8_261 * __this, CrossPlatformNotification_t8_259 * ____notification, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		NotificationService_t8_261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2144);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5440 = il2cpp_codegen_string_literal_from_index(5440);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_Log_m8_1007(NULL /*static, unused*/, _stringLiteral5368, _stringLiteral5440, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		ReceivedNotificationResponse_t8_258 * L_0 = ((NotificationService_t8_261_StaticFields*)NotificationService_t8_261_il2cpp_TypeInfo_var->static_fields)->___DidReceiveRemoteNotificationEvent_10;
		if (!L_0)
		{
			goto IL_0025;
		}
	}
	{
		ReceivedNotificationResponse_t8_258 * L_1 = ((NotificationService_t8_261_StaticFields*)NotificationService_t8_261_il2cpp_TypeInfo_var->static_fields)->___DidReceiveRemoteNotificationEvent_10;
		CrossPlatformNotification_t8_259 * L_2 = ____notification;
		NullCheck(L_1);
		ReceivedNotificationResponse_Invoke_m8_1428(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0025:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.NotificationService::ParseAppLaunchInfo(System.String,VoxelBusters.NativePlugins.CrossPlatformNotification&,VoxelBusters.NativePlugins.CrossPlatformNotification&)
extern "C" void NotificationService_ParseAppLaunchInfo_m8_1456 (NotificationService_t8_261 * __this, String_t* ____launchData, CrossPlatformNotification_t8_259 ** ____launchLocalNotification, CrossPlatformNotification_t8_259 ** ____launchRemoteNotification, const MethodInfo* method)
{
	{
		CrossPlatformNotification_t8_259 ** L_0 = ____launchLocalNotification;
		*((Object_t **)(L_0)) = (Object_t *)NULL;
		CrossPlatformNotification_t8_259 ** L_1 = ____launchRemoteNotification;
		*((Object_t **)(L_1)) = (Object_t *)NULL;
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.NotificationService::ParseNotificationPayloadData(System.String,VoxelBusters.NativePlugins.CrossPlatformNotification&)
extern "C" void NotificationService_ParseNotificationPayloadData_m8_1457 (NotificationService_t8_261 * __this, String_t* ____payload, CrossPlatformNotification_t8_259 ** ____notification, const MethodInfo* method)
{
	{
		CrossPlatformNotification_t8_259 ** L_0 = ____notification;
		String_t* L_1 = ____payload;
		CrossPlatformNotification_t8_259 * L_2 = CrossPlatformNotification_CreateNotificationFromPayload_m8_1533(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		*((Object_t **)(L_0)) = (Object_t *)L_2;
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.NotificationService::Awake()
extern "C" void NotificationService_Awake_m8_1458 (NotificationService_t8_261 * __this, const MethodInfo* method)
{
	{
		ApplicationSettings_t8_320 * L_0 = NPSettings_get_Application_m8_1934(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Features_t8_318 * L_1 = ApplicationSettings_get_SupportedFeatures_m8_1885(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_2 = Features_get_UsesNotificationService_m8_1872(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001f;
		}
	}
	{
		NotificationServiceSettings_t8_270 * L_3 = NPSettings_get_Notification_m8_1936(NULL /*static, unused*/, /*hidden argument*/NULL);
		VirtActionInvoker1< NotificationServiceSettings_t8_270 * >::Invoke(6 /* System.Void VoxelBusters.NativePlugins.NotificationService::Initialise(VoxelBusters.NativePlugins.NotificationServiceSettings) */, __this, L_3);
	}

IL_001f:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.NotificationService::Start()
extern "C" void NotificationService_Start_m8_1459 (NotificationService_t8_261 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = NotificationService_ProcessAppLaunchInfo_m8_1461(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m6_672(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.NotificationService::Initialise(VoxelBusters.NativePlugins.NotificationServiceSettings)
extern "C" void NotificationService_Initialise_m8_1460 (NotificationService_t8_261 * __this, NotificationServiceSettings_t8_270 * ____settings, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Collections.IEnumerator VoxelBusters.NativePlugins.NotificationService::ProcessAppLaunchInfo()
extern TypeInfo* U3CProcessAppLaunchInfoU3Ec__Iterator5_t8_260_il2cpp_TypeInfo_var;
extern "C" Object_t * NotificationService_ProcessAppLaunchInfo_m8_1461 (NotificationService_t8_261 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CProcessAppLaunchInfoU3Ec__Iterator5_t8_260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2145);
		s_Il2CppMethodIntialized = true;
	}
	U3CProcessAppLaunchInfoU3Ec__Iterator5_t8_260 * V_0 = {0};
	{
		U3CProcessAppLaunchInfoU3Ec__Iterator5_t8_260 * L_0 = (U3CProcessAppLaunchInfoU3Ec__Iterator5_t8_260 *)il2cpp_codegen_object_new (U3CProcessAppLaunchInfoU3Ec__Iterator5_t8_260_il2cpp_TypeInfo_var);
		U3CProcessAppLaunchInfoU3Ec__Iterator5__ctor_m8_1431(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CProcessAppLaunchInfoU3Ec__Iterator5_t8_260 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3Ef__this_2 = __this;
		U3CProcessAppLaunchInfoU3Ec__Iterator5_t8_260 * L_2 = V_0;
		return L_2;
	}
}
// System.Void VoxelBusters.NativePlugins.NotificationService::RegisterNotificationTypes(VoxelBusters.NativePlugins.NotificationType)
extern "C" void NotificationService_RegisterNotificationTypes_m8_1462 (NotificationService_t8_261 * __this, int32_t ____notificationTypes, const MethodInfo* method)
{
	{
		return;
	}
}
// System.String VoxelBusters.NativePlugins.NotificationService::ScheduleLocalNotification(VoxelBusters.NativePlugins.CrossPlatformNotification)
extern "C" String_t* NotificationService_ScheduleLocalNotification_m8_1463 (NotificationService_t8_261 * __this, CrossPlatformNotification_t8_259 * ____notification, const MethodInfo* method)
{
	{
		return (String_t*)NULL;
	}
}
// System.Void VoxelBusters.NativePlugins.NotificationService::CancelLocalNotification(System.String)
extern "C" void NotificationService_CancelLocalNotification_m8_1464 (NotificationService_t8_261 * __this, String_t* ____notificationID, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.NotificationService::CancelAllLocalNotification()
extern "C" void NotificationService_CancelAllLocalNotification_m8_1465 (NotificationService_t8_261 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.NotificationService::ClearNotifications()
extern "C" void NotificationService_ClearNotifications_m8_1466 (NotificationService_t8_261 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.NotificationService::RegisterForRemoteNotifications()
extern "C" void NotificationService_RegisterForRemoteNotifications_m8_1467 (NotificationService_t8_261 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.NotificationService::UnregisterForRemoteNotifications()
extern "C" void NotificationService_UnregisterForRemoteNotifications_m8_1468 (NotificationService_t8_261 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.NotificationServiceIOS::.ctor()
extern "C" void NotificationServiceIOS__ctor_m8_1469 (NotificationServiceIOS_t8_262 * __this, const MethodInfo* method)
{
	{
		NotificationService__ctor_m8_1437(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.NotificationServiceIOS::ParseAppLaunchInfo(System.String,VoxelBusters.NativePlugins.CrossPlatformNotification&,VoxelBusters.NativePlugins.CrossPlatformNotification&)
extern TypeInfo* Dictionary_2_t1_1903_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionary_t1_35_il2cpp_TypeInfo_var;
extern TypeInfo* iOSNotificationPayload_t8_267_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5441;
extern Il2CppCodeGenString* _stringLiteral5442;
extern "C" void NotificationServiceIOS_ParseAppLaunchInfo_m8_1470 (NotificationServiceIOS_t8_262 * __this, String_t* ____launchData, CrossPlatformNotification_t8_259 ** ____launchLocalNotification, CrossPlatformNotification_t8_259 ** ____launchRemoteNotification, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t1_1903_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1960);
		IDictionary_t1_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		iOSNotificationPayload_t8_267_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2146);
		_stringLiteral5441 = il2cpp_codegen_string_literal_from_index(5441);
		_stringLiteral5442 = il2cpp_codegen_string_literal_from_index(5442);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t1_1903 * V_0 = {0};
	Object_t * V_1 = {0};
	{
		String_t* L_0 = ____launchData;
		Object_t * L_1 = JSONUtility_FromJSON_m8_298(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = ((Dictionary_2_t1_1903 *)IsInstClass(L_1, Dictionary_2_t1_1903_il2cpp_TypeInfo_var));
		V_1 = NULL;
		Dictionary_2_t1_1903 * L_2 = V_0;
		NullCheck(L_2);
		bool L_3 = (bool)VirtFuncInvoker2< bool, String_t*, Object_t ** >::Invoke(21 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Object>::TryGetValue(!0,!1&) */, L_2, _stringLiteral5441, (&V_1));
		if (!L_3)
		{
			goto IL_0035;
		}
	}
	{
		CrossPlatformNotification_t8_259 ** L_4 = ____launchLocalNotification;
		Object_t * L_5 = V_1;
		iOSNotificationPayload_t8_267 * L_6 = (iOSNotificationPayload_t8_267 *)il2cpp_codegen_object_new (iOSNotificationPayload_t8_267_il2cpp_TypeInfo_var);
		iOSNotificationPayload__ctor_m8_1535(L_6, ((Object_t *)Castclass(L_5, IDictionary_t1_35_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		*((Object_t **)(L_4)) = (Object_t *)L_6;
		CrossPlatformNotification_t8_259 ** L_7 = ____launchRemoteNotification;
		*((Object_t **)(L_7)) = (Object_t *)NULL;
		goto IL_0062;
	}

IL_0035:
	{
		Dictionary_2_t1_1903 * L_8 = V_0;
		NullCheck(L_8);
		bool L_9 = (bool)VirtFuncInvoker2< bool, String_t*, Object_t ** >::Invoke(21 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Object>::TryGetValue(!0,!1&) */, L_8, _stringLiteral5442, (&V_1));
		if (!L_9)
		{
			goto IL_005c;
		}
	}
	{
		CrossPlatformNotification_t8_259 ** L_10 = ____launchLocalNotification;
		*((Object_t **)(L_10)) = (Object_t *)NULL;
		CrossPlatformNotification_t8_259 ** L_11 = ____launchRemoteNotification;
		Object_t * L_12 = V_1;
		iOSNotificationPayload_t8_267 * L_13 = (iOSNotificationPayload_t8_267 *)il2cpp_codegen_object_new (iOSNotificationPayload_t8_267_il2cpp_TypeInfo_var);
		iOSNotificationPayload__ctor_m8_1535(L_13, ((Object_t *)Castclass(L_12, IDictionary_t1_35_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		*((Object_t **)(L_11)) = (Object_t *)L_13;
		goto IL_0062;
	}

IL_005c:
	{
		CrossPlatformNotification_t8_259 ** L_14 = ____launchLocalNotification;
		*((Object_t **)(L_14)) = (Object_t *)NULL;
		CrossPlatformNotification_t8_259 ** L_15 = ____launchRemoteNotification;
		*((Object_t **)(L_15)) = (Object_t *)NULL;
	}

IL_0062:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.NotificationServiceIOS::initNotificationService(System.String)
extern "C" {void DEFAULT_CALL initNotificationService(char*);}
extern "C" void NotificationServiceIOS_initNotificationService_m8_1471 (Object_t * __this /* static, unused */, String_t* ____keyForUserInfo, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)initNotificationService;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'initNotificationService'"));
		}
	}

	// Marshaling of parameter '____keyForUserInfo' to native representation
	char* _____keyForUserInfo_marshaled = { 0 };
	_____keyForUserInfo_marshaled = il2cpp_codegen_marshal_string(____keyForUserInfo);

	// Native function invocation
	_il2cpp_pinvoke_func(_____keyForUserInfo_marshaled);

	// Marshaling cleanup of parameter '____keyForUserInfo' native representation
	il2cpp_codegen_marshal_free(_____keyForUserInfo_marshaled);
	_____keyForUserInfo_marshaled = NULL;

}
// System.Void VoxelBusters.NativePlugins.NotificationServiceIOS::registerNotificationTypes(System.Int32)
extern "C" {void DEFAULT_CALL registerNotificationTypes(int32_t);}
extern "C" void NotificationServiceIOS_registerNotificationTypes_m8_1472 (Object_t * __this /* static, unused */, int32_t ___notificationTypes, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)registerNotificationTypes;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'registerNotificationTypes'"));
		}
	}

	// Marshaling of parameter '___notificationTypes' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___notificationTypes);

	// Marshaling cleanup of parameter '___notificationTypes' native representation

}
// System.Int32 VoxelBusters.NativePlugins.NotificationServiceIOS::enabledNotificationTypes()
extern "C" {int32_t DEFAULT_CALL enabledNotificationTypes();}
extern "C" int32_t NotificationServiceIOS_enabledNotificationTypes_m8_1473 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)enabledNotificationTypes;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'enabledNotificationTypes'"));
		}
	}

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func();

	return _return_value;
}
// System.Void VoxelBusters.NativePlugins.NotificationServiceIOS::scheduleLocalNotification(System.String)
extern "C" {void DEFAULT_CALL scheduleLocalNotification(char*);}
extern "C" void NotificationServiceIOS_scheduleLocalNotification_m8_1474 (Object_t * __this /* static, unused */, String_t* ____payload, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)scheduleLocalNotification;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'scheduleLocalNotification'"));
		}
	}

	// Marshaling of parameter '____payload' to native representation
	char* _____payload_marshaled = { 0 };
	_____payload_marshaled = il2cpp_codegen_marshal_string(____payload);

	// Native function invocation
	_il2cpp_pinvoke_func(_____payload_marshaled);

	// Marshaling cleanup of parameter '____payload' native representation
	il2cpp_codegen_marshal_free(_____payload_marshaled);
	_____payload_marshaled = NULL;

}
// System.Void VoxelBusters.NativePlugins.NotificationServiceIOS::cancelLocalNotification(System.String)
extern "C" {void DEFAULT_CALL cancelLocalNotification(char*);}
extern "C" void NotificationServiceIOS_cancelLocalNotification_m8_1475 (Object_t * __this /* static, unused */, String_t* ____notificationID, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)cancelLocalNotification;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'cancelLocalNotification'"));
		}
	}

	// Marshaling of parameter '____notificationID' to native representation
	char* _____notificationID_marshaled = { 0 };
	_____notificationID_marshaled = il2cpp_codegen_marshal_string(____notificationID);

	// Native function invocation
	_il2cpp_pinvoke_func(_____notificationID_marshaled);

	// Marshaling cleanup of parameter '____notificationID' native representation
	il2cpp_codegen_marshal_free(_____notificationID_marshaled);
	_____notificationID_marshaled = NULL;

}
// System.Void VoxelBusters.NativePlugins.NotificationServiceIOS::cancelAllLocalNotifications()
extern "C" {void DEFAULT_CALL cancelAllLocalNotifications();}
extern "C" void NotificationServiceIOS_cancelAllLocalNotifications_m8_1476 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)cancelAllLocalNotifications;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'cancelAllLocalNotifications'"));
		}
	}

	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.Void VoxelBusters.NativePlugins.NotificationServiceIOS::clearNotifications()
extern "C" {void DEFAULT_CALL clearNotifications();}
extern "C" void NotificationServiceIOS_clearNotifications_m8_1477 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)clearNotifications;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'clearNotifications'"));
		}
	}

	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.Void VoxelBusters.NativePlugins.NotificationServiceIOS::registerForRemoteNotifications()
extern "C" {void DEFAULT_CALL registerForRemoteNotifications();}
extern "C" void NotificationServiceIOS_registerForRemoteNotifications_m8_1478 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)registerForRemoteNotifications;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'registerForRemoteNotifications'"));
		}
	}

	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.Void VoxelBusters.NativePlugins.NotificationServiceIOS::unregisterForRemoteNotifications()
extern "C" {void DEFAULT_CALL unregisterForRemoteNotifications();}
extern "C" void NotificationServiceIOS_unregisterForRemoteNotifications_m8_1479 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)unregisterForRemoteNotifications;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'unregisterForRemoteNotifications'"));
		}
	}

	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.Void VoxelBusters.NativePlugins.NotificationServiceIOS::Initialise(VoxelBusters.NativePlugins.NotificationServiceSettings)
extern "C" void NotificationServiceIOS_Initialise_m8_1480 (NotificationServiceIOS_t8_262 * __this, NotificationServiceSettings_t8_270 * ____settings, const MethodInfo* method)
{
	String_t* V_0 = {0};
	{
		NotificationServiceSettings_t8_270 * L_0 = ____settings;
		NullCheck(L_0);
		iOSSettings_t8_268 * L_1 = NotificationServiceSettings_get_iOS_m8_1564(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = iOSSettings_get_UserInfoKey_m8_1540(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		String_t* L_3 = V_0;
		NotificationServiceIOS_initNotificationService_m8_1471(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.NotificationServiceIOS::RegisterNotificationTypes(VoxelBusters.NativePlugins.NotificationType)
extern "C" void NotificationServiceIOS_RegisterNotificationTypes_m8_1481 (NotificationServiceIOS_t8_262 * __this, int32_t ____notificationTypes, const MethodInfo* method)
{
	{
		int32_t L_0 = ____notificationTypes;
		NotificationService_RegisterNotificationTypes_m8_1462(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = ____notificationTypes;
		NotificationServiceIOS_registerNotificationTypes_m8_1472(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String VoxelBusters.NativePlugins.NotificationServiceIOS::ScheduleLocalNotification(VoxelBusters.NativePlugins.CrossPlatformNotification)
extern "C" String_t* NotificationServiceIOS_ScheduleLocalNotification_m8_1482 (NotificationServiceIOS_t8_262 * __this, CrossPlatformNotification_t8_259 * ____notification, const MethodInfo* method)
{
	String_t* V_0 = {0};
	String_t* V_1 = {0};
	{
		CrossPlatformNotification_t8_259 * L_0 = ____notification;
		NullCheck(L_0);
		String_t* L_1 = CrossPlatformNotification_GenerateNotificationID_m8_1531(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		CrossPlatformNotification_t8_259 * L_2 = ____notification;
		Object_t * L_3 = iOSNotificationPayload_CreateNotificationPayload_m8_1536(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		String_t* L_4 = JSONParserExtensions_ToJSON_m8_269(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		String_t* L_5 = V_1;
		NotificationServiceIOS_scheduleLocalNotification_m8_1474(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		String_t* L_6 = V_0;
		return L_6;
	}
}
// System.Void VoxelBusters.NativePlugins.NotificationServiceIOS::CancelLocalNotification(System.String)
extern "C" void NotificationServiceIOS_CancelLocalNotification_m8_1483 (NotificationServiceIOS_t8_262 * __this, String_t* ____notificationID, const MethodInfo* method)
{
	{
		String_t* L_0 = ____notificationID;
		NotificationServiceIOS_cancelLocalNotification_m8_1475(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.NotificationServiceIOS::CancelAllLocalNotification()
extern "C" void NotificationServiceIOS_CancelAllLocalNotification_m8_1484 (NotificationServiceIOS_t8_262 * __this, const MethodInfo* method)
{
	{
		NotificationServiceIOS_cancelAllLocalNotifications_m8_1476(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.NotificationServiceIOS::ClearNotifications()
extern "C" void NotificationServiceIOS_ClearNotifications_m8_1485 (NotificationServiceIOS_t8_262 * __this, const MethodInfo* method)
{
	{
		NotificationServiceIOS_clearNotifications_m8_1477(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.NotificationServiceIOS::RegisterForRemoteNotifications()
extern "C" void NotificationServiceIOS_RegisterForRemoteNotifications_m8_1486 (NotificationServiceIOS_t8_262 * __this, const MethodInfo* method)
{
	{
		NotificationServiceIOS_registerForRemoteNotifications_m8_1478(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.NotificationServiceIOS::UnregisterForRemoteNotifications()
extern "C" void NotificationServiceIOS_UnregisterForRemoteNotifications_m8_1487 (NotificationServiceIOS_t8_262 * __this, const MethodInfo* method)
{
	{
		NotificationServiceIOS_unregisterForRemoteNotifications_m8_1479(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.CrossPlatformNotification/AndroidSpecificProperties::.ctor()
extern "C" void AndroidSpecificProperties__ctor_m8_1488 (AndroidSpecificProperties_t8_265 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		AndroidSpecificProperties_set_ContentTitle_m8_1491(__this, (String_t*)NULL, /*hidden argument*/NULL);
		AndroidSpecificProperties_set_TickerText_m8_1493(__this, (String_t*)NULL, /*hidden argument*/NULL);
		AndroidSpecificProperties_set_Tag_m8_1495(__this, (String_t*)NULL, /*hidden argument*/NULL);
		AndroidSpecificProperties_set_LargeIcon_m8_1499(__this, (String_t*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.CrossPlatformNotification/AndroidSpecificProperties::.ctor(System.Collections.IDictionary)
extern const MethodInfo* IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral5453;
extern Il2CppCodeGenString* _stringLiteral5454;
extern Il2CppCodeGenString* _stringLiteral3096;
extern Il2CppCodeGenString* _stringLiteral5455;
extern "C" void AndroidSpecificProperties__ctor_m8_1489 (AndroidSpecificProperties_t8_265 * __this, Object_t * ____jsonDict, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484117);
		_stringLiteral5453 = il2cpp_codegen_string_literal_from_index(5453);
		_stringLiteral5454 = il2cpp_codegen_string_literal_from_index(5454);
		_stringLiteral3096 = il2cpp_codegen_string_literal_from_index(3096);
		_stringLiteral5455 = il2cpp_codegen_string_literal_from_index(5455);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		Object_t * L_0 = ____jsonDict;
		String_t* L_1 = IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(NULL /*static, unused*/, L_0, _stringLiteral5453, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var);
		AndroidSpecificProperties_set_ContentTitle_m8_1491(__this, L_1, /*hidden argument*/NULL);
		Object_t * L_2 = ____jsonDict;
		String_t* L_3 = IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(NULL /*static, unused*/, L_2, _stringLiteral5454, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var);
		AndroidSpecificProperties_set_TickerText_m8_1493(__this, L_3, /*hidden argument*/NULL);
		Object_t * L_4 = ____jsonDict;
		String_t* L_5 = IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(NULL /*static, unused*/, L_4, _stringLiteral3096, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var);
		AndroidSpecificProperties_set_Tag_m8_1495(__this, L_5, /*hidden argument*/NULL);
		Object_t * L_6 = ____jsonDict;
		String_t* L_7 = IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(NULL /*static, unused*/, L_6, _stringLiteral5455, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var);
		AndroidSpecificProperties_set_LargeIcon_m8_1499(__this, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.String VoxelBusters.NativePlugins.CrossPlatformNotification/AndroidSpecificProperties::get_ContentTitle()
extern "C" String_t* AndroidSpecificProperties_get_ContentTitle_m8_1490 (AndroidSpecificProperties_t8_265 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CContentTitleU3Ek__BackingField_4);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.CrossPlatformNotification/AndroidSpecificProperties::set_ContentTitle(System.String)
extern "C" void AndroidSpecificProperties_set_ContentTitle_m8_1491 (AndroidSpecificProperties_t8_265 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CContentTitleU3Ek__BackingField_4 = L_0;
		return;
	}
}
// System.String VoxelBusters.NativePlugins.CrossPlatformNotification/AndroidSpecificProperties::get_TickerText()
extern "C" String_t* AndroidSpecificProperties_get_TickerText_m8_1492 (AndroidSpecificProperties_t8_265 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CTickerTextU3Ek__BackingField_5);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.CrossPlatformNotification/AndroidSpecificProperties::set_TickerText(System.String)
extern "C" void AndroidSpecificProperties_set_TickerText_m8_1493 (AndroidSpecificProperties_t8_265 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CTickerTextU3Ek__BackingField_5 = L_0;
		return;
	}
}
// System.String VoxelBusters.NativePlugins.CrossPlatformNotification/AndroidSpecificProperties::get_Tag()
extern "C" String_t* AndroidSpecificProperties_get_Tag_m8_1494 (AndroidSpecificProperties_t8_265 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CTagU3Ek__BackingField_6);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.CrossPlatformNotification/AndroidSpecificProperties::set_Tag(System.String)
extern "C" void AndroidSpecificProperties_set_Tag_m8_1495 (AndroidSpecificProperties_t8_265 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CTagU3Ek__BackingField_6 = L_0;
		return;
	}
}
// System.String VoxelBusters.NativePlugins.CrossPlatformNotification/AndroidSpecificProperties::get_CustomSound()
extern "C" String_t* AndroidSpecificProperties_get_CustomSound_m8_1496 (AndroidSpecificProperties_t8_265 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CCustomSoundU3Ek__BackingField_7);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.CrossPlatformNotification/AndroidSpecificProperties::set_CustomSound(System.String)
extern "C" void AndroidSpecificProperties_set_CustomSound_m8_1497 (AndroidSpecificProperties_t8_265 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CCustomSoundU3Ek__BackingField_7 = L_0;
		return;
	}
}
// System.String VoxelBusters.NativePlugins.CrossPlatformNotification/AndroidSpecificProperties::get_LargeIcon()
extern "C" String_t* AndroidSpecificProperties_get_LargeIcon_m8_1498 (AndroidSpecificProperties_t8_265 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CLargeIconU3Ek__BackingField_8);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.CrossPlatformNotification/AndroidSpecificProperties::set_LargeIcon(System.String)
extern "C" void AndroidSpecificProperties_set_LargeIcon_m8_1499 (AndroidSpecificProperties_t8_265 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CLargeIconU3Ek__BackingField_8 = L_0;
		return;
	}
}
// System.Collections.IDictionary VoxelBusters.NativePlugins.CrossPlatformNotification/AndroidSpecificProperties::JSONObject()
extern TypeInfo* Dictionary_2_t1_1839_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_14949_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral5453;
extern Il2CppCodeGenString* _stringLiteral5454;
extern Il2CppCodeGenString* _stringLiteral3096;
extern Il2CppCodeGenString* _stringLiteral5455;
extern "C" Object_t * AndroidSpecificProperties_JSONObject_m8_1500 (AndroidSpecificProperties_t8_265 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t1_1839_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1603);
		Dictionary_2__ctor_m1_14949_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483789);
		_stringLiteral5453 = il2cpp_codegen_string_literal_from_index(5453);
		_stringLiteral5454 = il2cpp_codegen_string_literal_from_index(5454);
		_stringLiteral3096 = il2cpp_codegen_string_literal_from_index(3096);
		_stringLiteral5455 = il2cpp_codegen_string_literal_from_index(5455);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t1_1839 * V_0 = {0};
	{
		Dictionary_2_t1_1839 * L_0 = (Dictionary_2_t1_1839 *)il2cpp_codegen_object_new (Dictionary_2_t1_1839_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_14949(L_0, /*hidden argument*/Dictionary_2__ctor_m1_14949_MethodInfo_var);
		V_0 = L_0;
		Dictionary_2_t1_1839 * L_1 = V_0;
		String_t* L_2 = AndroidSpecificProperties_get_ContentTitle_m8_1490(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		VirtActionInvoker2< String_t*, String_t* >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::set_Item(!0,!1) */, L_1, _stringLiteral5453, L_2);
		Dictionary_2_t1_1839 * L_3 = V_0;
		String_t* L_4 = AndroidSpecificProperties_get_TickerText_m8_1492(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		VirtActionInvoker2< String_t*, String_t* >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::set_Item(!0,!1) */, L_3, _stringLiteral5454, L_4);
		Dictionary_2_t1_1839 * L_5 = V_0;
		String_t* L_6 = AndroidSpecificProperties_get_Tag_m8_1494(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		VirtActionInvoker2< String_t*, String_t* >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::set_Item(!0,!1) */, L_5, _stringLiteral3096, L_6);
		Dictionary_2_t1_1839 * L_7 = V_0;
		String_t* L_8 = AndroidSpecificProperties_get_LargeIcon_m8_1498(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		VirtActionInvoker2< String_t*, String_t* >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::set_Item(!0,!1) */, L_7, _stringLiteral5455, L_8);
		Dictionary_2_t1_1839 * L_9 = V_0;
		return L_9;
	}
}
// System.String VoxelBusters.NativePlugins.CrossPlatformNotification/AndroidSpecificProperties::ToString()
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5456;
extern "C" String_t* AndroidSpecificProperties_ToString_m8_1501 (AndroidSpecificProperties_t8_265 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5456 = il2cpp_codegen_string_literal_from_index(5456);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_272* L_0 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 4));
		String_t* L_1 = AndroidSpecificProperties_get_ContentTitle_m8_1490(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_1;
		ObjectU5BU5D_t1_272* L_2 = L_0;
		String_t* L_3 = AndroidSpecificProperties_get_TickerText_m8_1492(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1, sizeof(Object_t *))) = (Object_t *)L_3;
		ObjectU5BU5D_t1_272* L_4 = L_2;
		String_t* L_5 = AndroidSpecificProperties_get_Tag_m8_1494(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, L_5);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 2, sizeof(Object_t *))) = (Object_t *)L_5;
		ObjectU5BU5D_t1_272* L_6 = L_4;
		String_t* L_7 = AndroidSpecificProperties_get_LargeIcon_m8_1498(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 3);
		ArrayElementTypeCheck (L_6, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 3, sizeof(Object_t *))) = (Object_t *)L_7;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Format_m1_551(NULL /*static, unused*/, _stringLiteral5456, L_6, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Void VoxelBusters.NativePlugins.CrossPlatformNotification/iOSSpecificProperties::.ctor()
extern "C" void iOSSpecificProperties__ctor_m8_1502 (iOSSpecificProperties_t8_266 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		iOSSpecificProperties_set_AlertAction_m8_1505(__this, (String_t*)NULL, /*hidden argument*/NULL);
		iOSSpecificProperties_set_HasAction_m8_1507(__this, 1, /*hidden argument*/NULL);
		iOSSpecificProperties_set_BadgeCount_m8_1509(__this, 0, /*hidden argument*/NULL);
		iOSSpecificProperties_set_LaunchImage_m8_1511(__this, (String_t*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.CrossPlatformNotification/iOSSpecificProperties::.ctor(System.Collections.IDictionary)
extern const MethodInfo* IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var;
extern const MethodInfo* IDictionaryExtensions_GetIfAvailable_TisBoolean_t1_20_m8_2012_MethodInfo_var;
extern const MethodInfo* IDictionaryExtensions_GetIfAvailable_TisInt32_t1_3_m8_2007_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral5457;
extern Il2CppCodeGenString* _stringLiteral5458;
extern Il2CppCodeGenString* _stringLiteral5459;
extern Il2CppCodeGenString* _stringLiteral5460;
extern "C" void iOSSpecificProperties__ctor_m8_1503 (iOSSpecificProperties_t8_266 * __this, Object_t * ____jsonDict, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484117);
		IDictionaryExtensions_GetIfAvailable_TisBoolean_t1_20_m8_2012_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484234);
		IDictionaryExtensions_GetIfAvailable_TisInt32_t1_3_m8_2007_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484196);
		_stringLiteral5457 = il2cpp_codegen_string_literal_from_index(5457);
		_stringLiteral5458 = il2cpp_codegen_string_literal_from_index(5458);
		_stringLiteral5459 = il2cpp_codegen_string_literal_from_index(5459);
		_stringLiteral5460 = il2cpp_codegen_string_literal_from_index(5460);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		Object_t * L_0 = ____jsonDict;
		String_t* L_1 = IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(NULL /*static, unused*/, L_0, _stringLiteral5457, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var);
		iOSSpecificProperties_set_AlertAction_m8_1505(__this, L_1, /*hidden argument*/NULL);
		Object_t * L_2 = ____jsonDict;
		bool L_3 = IDictionaryExtensions_GetIfAvailable_TisBoolean_t1_20_m8_2012(NULL /*static, unused*/, L_2, _stringLiteral5458, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisBoolean_t1_20_m8_2012_MethodInfo_var);
		iOSSpecificProperties_set_HasAction_m8_1507(__this, L_3, /*hidden argument*/NULL);
		Object_t * L_4 = ____jsonDict;
		int32_t L_5 = IDictionaryExtensions_GetIfAvailable_TisInt32_t1_3_m8_2007(NULL /*static, unused*/, L_4, _stringLiteral5459, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisInt32_t1_3_m8_2007_MethodInfo_var);
		iOSSpecificProperties_set_BadgeCount_m8_1509(__this, L_5, /*hidden argument*/NULL);
		Object_t * L_6 = ____jsonDict;
		String_t* L_7 = IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(NULL /*static, unused*/, L_6, _stringLiteral5460, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var);
		iOSSpecificProperties_set_LaunchImage_m8_1511(__this, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.String VoxelBusters.NativePlugins.CrossPlatformNotification/iOSSpecificProperties::get_AlertAction()
extern "C" String_t* iOSSpecificProperties_get_AlertAction_m8_1504 (iOSSpecificProperties_t8_266 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CAlertActionU3Ek__BackingField_4);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.CrossPlatformNotification/iOSSpecificProperties::set_AlertAction(System.String)
extern "C" void iOSSpecificProperties_set_AlertAction_m8_1505 (iOSSpecificProperties_t8_266 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CAlertActionU3Ek__BackingField_4 = L_0;
		return;
	}
}
// System.Boolean VoxelBusters.NativePlugins.CrossPlatformNotification/iOSSpecificProperties::get_HasAction()
extern "C" bool iOSSpecificProperties_get_HasAction_m8_1506 (iOSSpecificProperties_t8_266 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___U3CHasActionU3Ek__BackingField_5);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.CrossPlatformNotification/iOSSpecificProperties::set_HasAction(System.Boolean)
extern "C" void iOSSpecificProperties_set_HasAction_m8_1507 (iOSSpecificProperties_t8_266 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___U3CHasActionU3Ek__BackingField_5 = L_0;
		return;
	}
}
// System.Int32 VoxelBusters.NativePlugins.CrossPlatformNotification/iOSSpecificProperties::get_BadgeCount()
extern "C" int32_t iOSSpecificProperties_get_BadgeCount_m8_1508 (iOSSpecificProperties_t8_266 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___U3CBadgeCountU3Ek__BackingField_6);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.CrossPlatformNotification/iOSSpecificProperties::set_BadgeCount(System.Int32)
extern "C" void iOSSpecificProperties_set_BadgeCount_m8_1509 (iOSSpecificProperties_t8_266 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___U3CBadgeCountU3Ek__BackingField_6 = L_0;
		return;
	}
}
// System.String VoxelBusters.NativePlugins.CrossPlatformNotification/iOSSpecificProperties::get_LaunchImage()
extern "C" String_t* iOSSpecificProperties_get_LaunchImage_m8_1510 (iOSSpecificProperties_t8_266 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CLaunchImageU3Ek__BackingField_7);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.CrossPlatformNotification/iOSSpecificProperties::set_LaunchImage(System.String)
extern "C" void iOSSpecificProperties_set_LaunchImage_m8_1511 (iOSSpecificProperties_t8_266 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CLaunchImageU3Ek__BackingField_7 = L_0;
		return;
	}
}
// System.Collections.IDictionary VoxelBusters.NativePlugins.CrossPlatformNotification/iOSSpecificProperties::JSONObject()
extern TypeInfo* Dictionary_2_t1_1903_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t1_20_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_15037_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral5457;
extern Il2CppCodeGenString* _stringLiteral5458;
extern Il2CppCodeGenString* _stringLiteral5459;
extern Il2CppCodeGenString* _stringLiteral5460;
extern "C" Object_t * iOSSpecificProperties_JSONObject_m8_1512 (iOSSpecificProperties_t8_266 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t1_1903_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1960);
		Boolean_t1_20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(54);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		Dictionary_2__ctor_m1_15037_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484124);
		_stringLiteral5457 = il2cpp_codegen_string_literal_from_index(5457);
		_stringLiteral5458 = il2cpp_codegen_string_literal_from_index(5458);
		_stringLiteral5459 = il2cpp_codegen_string_literal_from_index(5459);
		_stringLiteral5460 = il2cpp_codegen_string_literal_from_index(5460);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t1_1903 * V_0 = {0};
	{
		Dictionary_2_t1_1903 * L_0 = (Dictionary_2_t1_1903 *)il2cpp_codegen_object_new (Dictionary_2_t1_1903_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_15037(L_0, /*hidden argument*/Dictionary_2__ctor_m1_15037_MethodInfo_var);
		V_0 = L_0;
		Dictionary_2_t1_1903 * L_1 = V_0;
		String_t* L_2 = iOSSpecificProperties_get_AlertAction_m8_1504(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		VirtActionInvoker2< String_t*, Object_t * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Object>::set_Item(!0,!1) */, L_1, _stringLiteral5457, L_2);
		Dictionary_2_t1_1903 * L_3 = V_0;
		bool L_4 = iOSSpecificProperties_get_HasAction_m8_1506(__this, /*hidden argument*/NULL);
		bool L_5 = L_4;
		Object_t * L_6 = Box(Boolean_t1_20_il2cpp_TypeInfo_var, &L_5);
		NullCheck(L_3);
		VirtActionInvoker2< String_t*, Object_t * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Object>::set_Item(!0,!1) */, L_3, _stringLiteral5458, L_6);
		Dictionary_2_t1_1903 * L_7 = V_0;
		int32_t L_8 = iOSSpecificProperties_get_BadgeCount_m8_1508(__this, /*hidden argument*/NULL);
		int32_t L_9 = L_8;
		Object_t * L_10 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_7);
		VirtActionInvoker2< String_t*, Object_t * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Object>::set_Item(!0,!1) */, L_7, _stringLiteral5459, L_10);
		Dictionary_2_t1_1903 * L_11 = V_0;
		String_t* L_12 = iOSSpecificProperties_get_LaunchImage_m8_1510(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		VirtActionInvoker2< String_t*, Object_t * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Object>::set_Item(!0,!1) */, L_11, _stringLiteral5460, L_12);
		Dictionary_2_t1_1903 * L_13 = V_0;
		return L_13;
	}
}
// System.String VoxelBusters.NativePlugins.CrossPlatformNotification/iOSSpecificProperties::ToString()
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t1_20_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5461;
extern "C" String_t* iOSSpecificProperties_ToString_m8_1513 (iOSSpecificProperties_t8_266 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Boolean_t1_20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(54);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5461 = il2cpp_codegen_string_literal_from_index(5461);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_272* L_0 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 4));
		String_t* L_1 = iOSSpecificProperties_get_AlertAction_m8_1504(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_1;
		ObjectU5BU5D_t1_272* L_2 = L_0;
		bool L_3 = iOSSpecificProperties_get_HasAction_m8_1506(__this, /*hidden argument*/NULL);
		bool L_4 = L_3;
		Object_t * L_5 = Box(Boolean_t1_20_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_5);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1, sizeof(Object_t *))) = (Object_t *)L_5;
		ObjectU5BU5D_t1_272* L_6 = L_2;
		int32_t L_7 = iOSSpecificProperties_get_BadgeCount_m8_1508(__this, /*hidden argument*/NULL);
		int32_t L_8 = L_7;
		Object_t * L_9 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, L_9);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 2, sizeof(Object_t *))) = (Object_t *)L_9;
		ObjectU5BU5D_t1_272* L_10 = L_6;
		String_t* L_11 = iOSSpecificProperties_get_LaunchImage_m8_1510(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 3);
		ArrayElementTypeCheck (L_10, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_10, 3, sizeof(Object_t *))) = (Object_t *)L_11;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Format_m1_551(NULL /*static, unused*/, _stringLiteral5461, L_10, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Void VoxelBusters.NativePlugins.CrossPlatformNotification::.ctor()
extern "C" void CrossPlatformNotification__ctor_m8_1514 (CrossPlatformNotification_t8_259 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		CrossPlatformNotification_set_AlertBody_m8_1517(__this, (String_t*)NULL, /*hidden argument*/NULL);
		CrossPlatformNotification_set_UserInfo_m8_1523(__this, (Object_t *)NULL, /*hidden argument*/NULL);
		CrossPlatformNotification_set_RepeatInterval_m8_1521(__this, 0, /*hidden argument*/NULL);
		CrossPlatformNotification_set_SoundName_m8_1525(__this, (String_t*)NULL, /*hidden argument*/NULL);
		CrossPlatformNotification_set_iOSProperties_m8_1527(__this, (iOSSpecificProperties_t8_266 *)NULL, /*hidden argument*/NULL);
		CrossPlatformNotification_set_AndroidProperties_m8_1529(__this, (AndroidSpecificProperties_t8_265 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.CrossPlatformNotification::.ctor(System.Collections.IDictionary)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionary_t1_35_il2cpp_TypeInfo_var;
extern TypeInfo* iOSSpecificProperties_t8_266_il2cpp_TypeInfo_var;
extern TypeInfo* AndroidSpecificProperties_t8_265_il2cpp_TypeInfo_var;
extern const MethodInfo* IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var;
extern const MethodInfo* IDictionaryExtensions_GetIfAvailable_TiseNotificationRepeatInterval_t8_264_m8_2013_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral5443;
extern Il2CppCodeGenString* _stringLiteral5444;
extern Il2CppCodeGenString* _stringLiteral5445;
extern Il2CppCodeGenString* _stringLiteral5446;
extern Il2CppCodeGenString* _stringLiteral5447;
extern Il2CppCodeGenString* _stringLiteral5448;
extern Il2CppCodeGenString* _stringLiteral5449;
extern "C" void CrossPlatformNotification__ctor_m8_1515 (CrossPlatformNotification_t8_259 * __this, Object_t * ____jsonDict, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		IDictionary_t1_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		iOSSpecificProperties_t8_266_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1946);
		AndroidSpecificProperties_t8_265_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1947);
		IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484117);
		IDictionaryExtensions_GetIfAvailable_TiseNotificationRepeatInterval_t8_264_m8_2013_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484235);
		_stringLiteral5443 = il2cpp_codegen_string_literal_from_index(5443);
		_stringLiteral5444 = il2cpp_codegen_string_literal_from_index(5444);
		_stringLiteral5445 = il2cpp_codegen_string_literal_from_index(5445);
		_stringLiteral5446 = il2cpp_codegen_string_literal_from_index(5446);
		_stringLiteral5447 = il2cpp_codegen_string_literal_from_index(5447);
		_stringLiteral5448 = il2cpp_codegen_string_literal_from_index(5448);
		_stringLiteral5449 = il2cpp_codegen_string_literal_from_index(5449);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		Object_t * L_0 = ____jsonDict;
		String_t* L_1 = IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(NULL /*static, unused*/, L_0, _stringLiteral5443, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var);
		CrossPlatformNotification_set_AlertBody_m8_1517(__this, L_1, /*hidden argument*/NULL);
		Object_t * L_2 = ____jsonDict;
		String_t* L_3 = IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(NULL /*static, unused*/, L_2, _stringLiteral5444, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var);
		V_0 = L_3;
		String_t* L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_IsNullOrEmpty_m1_521(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_003a;
		}
	}
	{
		String_t* L_6 = V_0;
		DateTime_t1_150  L_7 = DateTimeExtensions_ToZuluFormatDateTimeLocal_m8_186(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		CrossPlatformNotification_set_FireDate_m8_1519(__this, L_7, /*hidden argument*/NULL);
	}

IL_003a:
	{
		Object_t * L_8 = ____jsonDict;
		int32_t L_9 = IDictionaryExtensions_GetIfAvailable_TiseNotificationRepeatInterval_t8_264_m8_2013(NULL /*static, unused*/, L_8, _stringLiteral5445, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TiseNotificationRepeatInterval_t8_264_m8_2013_MethodInfo_var);
		CrossPlatformNotification_set_RepeatInterval_m8_1521(__this, L_9, /*hidden argument*/NULL);
		Object_t * L_10 = ____jsonDict;
		NullCheck(L_10);
		Object_t * L_11 = (Object_t *)InterfaceFuncInvoker1< Object_t *, Object_t * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_10, _stringLiteral5446);
		CrossPlatformNotification_set_UserInfo_m8_1523(__this, ((Object_t *)IsInst(L_11, IDictionary_t1_35_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		Object_t * L_12 = ____jsonDict;
		String_t* L_13 = IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(NULL /*static, unused*/, L_12, _stringLiteral5447, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var);
		CrossPlatformNotification_set_SoundName_m8_1525(__this, L_13, /*hidden argument*/NULL);
		Object_t * L_14 = ____jsonDict;
		NullCheck(L_14);
		bool L_15 = (bool)InterfaceFuncInvoker1< bool, Object_t * >::Invoke(8 /* System.Boolean System.Collections.IDictionary::Contains(System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_14, _stringLiteral5448);
		if (!L_15)
		{
			goto IL_009d;
		}
	}
	{
		Object_t * L_16 = ____jsonDict;
		NullCheck(L_16);
		Object_t * L_17 = (Object_t *)InterfaceFuncInvoker1< Object_t *, Object_t * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_16, _stringLiteral5448);
		iOSSpecificProperties_t8_266 * L_18 = (iOSSpecificProperties_t8_266 *)il2cpp_codegen_object_new (iOSSpecificProperties_t8_266_il2cpp_TypeInfo_var);
		iOSSpecificProperties__ctor_m8_1503(L_18, ((Object_t *)IsInst(L_17, IDictionary_t1_35_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		CrossPlatformNotification_set_iOSProperties_m8_1527(__this, L_18, /*hidden argument*/NULL);
	}

IL_009d:
	{
		Object_t * L_19 = ____jsonDict;
		NullCheck(L_19);
		bool L_20 = (bool)InterfaceFuncInvoker1< bool, Object_t * >::Invoke(8 /* System.Boolean System.Collections.IDictionary::Contains(System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_19, _stringLiteral5449);
		if (!L_20)
		{
			goto IL_00c8;
		}
	}
	{
		Object_t * L_21 = ____jsonDict;
		NullCheck(L_21);
		Object_t * L_22 = (Object_t *)InterfaceFuncInvoker1< Object_t *, Object_t * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_21, _stringLiteral5449);
		AndroidSpecificProperties_t8_265 * L_23 = (AndroidSpecificProperties_t8_265 *)il2cpp_codegen_object_new (AndroidSpecificProperties_t8_265_il2cpp_TypeInfo_var);
		AndroidSpecificProperties__ctor_m8_1489(L_23, ((Object_t *)IsInst(L_22, IDictionary_t1_35_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		CrossPlatformNotification_set_AndroidProperties_m8_1529(__this, L_23, /*hidden argument*/NULL);
	}

IL_00c8:
	{
		return;
	}
}
// System.String VoxelBusters.NativePlugins.CrossPlatformNotification::get_AlertBody()
extern "C" String_t* CrossPlatformNotification_get_AlertBody_m8_1516 (CrossPlatformNotification_t8_259 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CAlertBodyU3Ek__BackingField_8);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.CrossPlatformNotification::set_AlertBody(System.String)
extern "C" void CrossPlatformNotification_set_AlertBody_m8_1517 (CrossPlatformNotification_t8_259 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CAlertBodyU3Ek__BackingField_8 = L_0;
		return;
	}
}
// System.DateTime VoxelBusters.NativePlugins.CrossPlatformNotification::get_FireDate()
extern "C" DateTime_t1_150  CrossPlatformNotification_get_FireDate_m8_1518 (CrossPlatformNotification_t8_259 * __this, const MethodInfo* method)
{
	{
		DateTime_t1_150  L_0 = (__this->___U3CFireDateU3Ek__BackingField_9);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.CrossPlatformNotification::set_FireDate(System.DateTime)
extern "C" void CrossPlatformNotification_set_FireDate_m8_1519 (CrossPlatformNotification_t8_259 * __this, DateTime_t1_150  ___value, const MethodInfo* method)
{
	{
		DateTime_t1_150  L_0 = ___value;
		__this->___U3CFireDateU3Ek__BackingField_9 = L_0;
		return;
	}
}
// VoxelBusters.NativePlugins.eNotificationRepeatInterval VoxelBusters.NativePlugins.CrossPlatformNotification::get_RepeatInterval()
extern "C" int32_t CrossPlatformNotification_get_RepeatInterval_m8_1520 (CrossPlatformNotification_t8_259 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___U3CRepeatIntervalU3Ek__BackingField_10);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.CrossPlatformNotification::set_RepeatInterval(VoxelBusters.NativePlugins.eNotificationRepeatInterval)
extern "C" void CrossPlatformNotification_set_RepeatInterval_m8_1521 (CrossPlatformNotification_t8_259 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___U3CRepeatIntervalU3Ek__BackingField_10 = L_0;
		return;
	}
}
// System.Collections.IDictionary VoxelBusters.NativePlugins.CrossPlatformNotification::get_UserInfo()
extern "C" Object_t * CrossPlatformNotification_get_UserInfo_m8_1522 (CrossPlatformNotification_t8_259 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U3CUserInfoU3Ek__BackingField_11);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.CrossPlatformNotification::set_UserInfo(System.Collections.IDictionary)
extern "C" void CrossPlatformNotification_set_UserInfo_m8_1523 (CrossPlatformNotification_t8_259 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		__this->___U3CUserInfoU3Ek__BackingField_11 = L_0;
		return;
	}
}
// System.String VoxelBusters.NativePlugins.CrossPlatformNotification::get_SoundName()
extern "C" String_t* CrossPlatformNotification_get_SoundName_m8_1524 (CrossPlatformNotification_t8_259 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CSoundNameU3Ek__BackingField_12);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.CrossPlatformNotification::set_SoundName(System.String)
extern "C" void CrossPlatformNotification_set_SoundName_m8_1525 (CrossPlatformNotification_t8_259 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CSoundNameU3Ek__BackingField_12 = L_0;
		return;
	}
}
// VoxelBusters.NativePlugins.CrossPlatformNotification/iOSSpecificProperties VoxelBusters.NativePlugins.CrossPlatformNotification::get_iOSProperties()
extern "C" iOSSpecificProperties_t8_266 * CrossPlatformNotification_get_iOSProperties_m8_1526 (CrossPlatformNotification_t8_259 * __this, const MethodInfo* method)
{
	{
		iOSSpecificProperties_t8_266 * L_0 = (__this->___U3CiOSPropertiesU3Ek__BackingField_13);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.CrossPlatformNotification::set_iOSProperties(VoxelBusters.NativePlugins.CrossPlatformNotification/iOSSpecificProperties)
extern "C" void CrossPlatformNotification_set_iOSProperties_m8_1527 (CrossPlatformNotification_t8_259 * __this, iOSSpecificProperties_t8_266 * ___value, const MethodInfo* method)
{
	{
		iOSSpecificProperties_t8_266 * L_0 = ___value;
		__this->___U3CiOSPropertiesU3Ek__BackingField_13 = L_0;
		return;
	}
}
// VoxelBusters.NativePlugins.CrossPlatformNotification/AndroidSpecificProperties VoxelBusters.NativePlugins.CrossPlatformNotification::get_AndroidProperties()
extern "C" AndroidSpecificProperties_t8_265 * CrossPlatformNotification_get_AndroidProperties_m8_1528 (CrossPlatformNotification_t8_259 * __this, const MethodInfo* method)
{
	{
		AndroidSpecificProperties_t8_265 * L_0 = (__this->___U3CAndroidPropertiesU3Ek__BackingField_14);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.CrossPlatformNotification::set_AndroidProperties(VoxelBusters.NativePlugins.CrossPlatformNotification/AndroidSpecificProperties)
extern "C" void CrossPlatformNotification_set_AndroidProperties_m8_1529 (CrossPlatformNotification_t8_259 * __this, AndroidSpecificProperties_t8_265 * ___value, const MethodInfo* method)
{
	{
		AndroidSpecificProperties_t8_265 * L_0 = ___value;
		__this->___U3CAndroidPropertiesU3Ek__BackingField_14 = L_0;
		return;
	}
}
// System.String VoxelBusters.NativePlugins.CrossPlatformNotification::GetNotificationID()
extern TypeInfo* IDictionary_t1_35_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5450;
extern "C" String_t* CrossPlatformNotification_GetNotificationID_m8_1530 (CrossPlatformNotification_t8_259 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionary_t1_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5450 = il2cpp_codegen_string_literal_from_index(5450);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = CrossPlatformNotification_get_UserInfo_m8_1522(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0036;
		}
	}
	{
		Object_t * L_1 = CrossPlatformNotification_get_UserInfo_m8_1522(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_2 = (bool)InterfaceFuncInvoker1< bool, Object_t * >::Invoke(8 /* System.Boolean System.Collections.IDictionary::Contains(System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_1, _stringLiteral5450);
		if (!L_2)
		{
			goto IL_0036;
		}
	}
	{
		Object_t * L_3 = CrossPlatformNotification_get_UserInfo_m8_1522(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Object_t * L_4 = (Object_t *)InterfaceFuncInvoker1< Object_t *, Object_t * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_3, _stringLiteral5450);
		return ((String_t*)IsInstSealed(L_4, String_t_il2cpp_TypeInfo_var));
	}

IL_0036:
	{
		return (String_t*)NULL;
	}
}
// System.String VoxelBusters.NativePlugins.CrossPlatformNotification::GenerateNotificationID()
extern TypeInfo* Dictionary_2_t1_1839_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionary_t1_35_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_14949_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral5450;
extern "C" String_t* CrossPlatformNotification_GenerateNotificationID_m8_1531 (CrossPlatformNotification_t8_259 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t1_1839_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1603);
		IDictionary_t1_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		Dictionary_2__ctor_m1_14949_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483789);
		_stringLiteral5450 = il2cpp_codegen_string_literal_from_index(5450);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	{
		Utility_t8_306 * L_0 = NPBinding_get_Utility_m8_1931(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(5 /* System.String VoxelBusters.NativePlugins.Utility::GetUUID() */, L_0);
		V_0 = L_1;
		Object_t * L_2 = CrossPlatformNotification_get_UserInfo_m8_1522(__this, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		Dictionary_2_t1_1839 * L_3 = (Dictionary_2_t1_1839 *)il2cpp_codegen_object_new (Dictionary_2_t1_1839_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_14949(L_3, /*hidden argument*/Dictionary_2__ctor_m1_14949_MethodInfo_var);
		CrossPlatformNotification_set_UserInfo_m8_1523(__this, L_3, /*hidden argument*/NULL);
	}

IL_0021:
	{
		Object_t * L_4 = CrossPlatformNotification_get_UserInfo_m8_1522(__this, /*hidden argument*/NULL);
		String_t* L_5 = V_0;
		NullCheck(L_4);
		InterfaceActionInvoker2< Object_t *, Object_t * >::Invoke(3 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_4, _stringLiteral5450, L_5);
		String_t* L_6 = V_0;
		return L_6;
	}
}
// System.Collections.IDictionary VoxelBusters.NativePlugins.CrossPlatformNotification::JSONObject()
extern TypeInfo* Dictionary_2_t1_1903_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_15037_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral5443;
extern Il2CppCodeGenString* _stringLiteral5444;
extern Il2CppCodeGenString* _stringLiteral5445;
extern Il2CppCodeGenString* _stringLiteral5446;
extern Il2CppCodeGenString* _stringLiteral5447;
extern Il2CppCodeGenString* _stringLiteral5448;
extern Il2CppCodeGenString* _stringLiteral5449;
extern "C" Object_t * CrossPlatformNotification_JSONObject_m8_1532 (CrossPlatformNotification_t8_259 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t1_1903_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1960);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		Dictionary_2__ctor_m1_15037_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484124);
		_stringLiteral5443 = il2cpp_codegen_string_literal_from_index(5443);
		_stringLiteral5444 = il2cpp_codegen_string_literal_from_index(5444);
		_stringLiteral5445 = il2cpp_codegen_string_literal_from_index(5445);
		_stringLiteral5446 = il2cpp_codegen_string_literal_from_index(5446);
		_stringLiteral5447 = il2cpp_codegen_string_literal_from_index(5447);
		_stringLiteral5448 = il2cpp_codegen_string_literal_from_index(5448);
		_stringLiteral5449 = il2cpp_codegen_string_literal_from_index(5449);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t1_1903 * V_0 = {0};
	{
		Dictionary_2_t1_1903 * L_0 = (Dictionary_2_t1_1903 *)il2cpp_codegen_object_new (Dictionary_2_t1_1903_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_15037(L_0, /*hidden argument*/Dictionary_2__ctor_m1_15037_MethodInfo_var);
		V_0 = L_0;
		Dictionary_2_t1_1903 * L_1 = V_0;
		String_t* L_2 = CrossPlatformNotification_get_AlertBody_m8_1516(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		VirtActionInvoker2< String_t*, Object_t * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Object>::set_Item(!0,!1) */, L_1, _stringLiteral5443, L_2);
		Dictionary_2_t1_1903 * L_3 = V_0;
		DateTime_t1_150  L_4 = CrossPlatformNotification_get_FireDate_m8_1518(__this, /*hidden argument*/NULL);
		String_t* L_5 = DateTimeExtensions_ToStringUsingZuluFormat_m8_187(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		VirtActionInvoker2< String_t*, Object_t * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Object>::set_Item(!0,!1) */, L_3, _stringLiteral5444, L_5);
		Dictionary_2_t1_1903 * L_6 = V_0;
		int32_t L_7 = CrossPlatformNotification_get_RepeatInterval_m8_1520(__this, /*hidden argument*/NULL);
		int32_t L_8 = L_7;
		Object_t * L_9 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		VirtActionInvoker2< String_t*, Object_t * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Object>::set_Item(!0,!1) */, L_6, _stringLiteral5445, L_9);
		Dictionary_2_t1_1903 * L_10 = V_0;
		Object_t * L_11 = CrossPlatformNotification_get_UserInfo_m8_1522(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		VirtActionInvoker2< String_t*, Object_t * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Object>::set_Item(!0,!1) */, L_10, _stringLiteral5446, L_11);
		Dictionary_2_t1_1903 * L_12 = V_0;
		String_t* L_13 = CrossPlatformNotification_get_SoundName_m8_1524(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		VirtActionInvoker2< String_t*, Object_t * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Object>::set_Item(!0,!1) */, L_12, _stringLiteral5447, L_13);
		iOSSpecificProperties_t8_266 * L_14 = CrossPlatformNotification_get_iOSProperties_m8_1526(__this, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0086;
		}
	}
	{
		Dictionary_2_t1_1903 * L_15 = V_0;
		iOSSpecificProperties_t8_266 * L_16 = CrossPlatformNotification_get_iOSProperties_m8_1526(__this, /*hidden argument*/NULL);
		NullCheck(L_16);
		Object_t * L_17 = iOSSpecificProperties_JSONObject_m8_1512(L_16, /*hidden argument*/NULL);
		NullCheck(L_15);
		VirtActionInvoker2< String_t*, Object_t * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Object>::set_Item(!0,!1) */, L_15, _stringLiteral5448, L_17);
	}

IL_0086:
	{
		AndroidSpecificProperties_t8_265 * L_18 = CrossPlatformNotification_get_AndroidProperties_m8_1528(__this, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00a7;
		}
	}
	{
		Dictionary_2_t1_1903 * L_19 = V_0;
		AndroidSpecificProperties_t8_265 * L_20 = CrossPlatformNotification_get_AndroidProperties_m8_1528(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		Object_t * L_21 = AndroidSpecificProperties_JSONObject_m8_1500(L_20, /*hidden argument*/NULL);
		NullCheck(L_19);
		VirtActionInvoker2< String_t*, Object_t * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Object>::set_Item(!0,!1) */, L_19, _stringLiteral5449, L_21);
	}

IL_00a7:
	{
		Dictionary_2_t1_1903 * L_22 = V_0;
		return L_22;
	}
}
// VoxelBusters.NativePlugins.CrossPlatformNotification VoxelBusters.NativePlugins.CrossPlatformNotification::CreateNotificationFromPayload(System.String)
extern TypeInfo* IDictionary_t1_35_il2cpp_TypeInfo_var;
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern TypeInfo* iOSNotificationPayload_t8_267_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5451;
extern "C" CrossPlatformNotification_t8_259 * CrossPlatformNotification_CreateNotificationFromPayload_m8_1533 (Object_t * __this /* static, unused */, String_t* ____notificationPayload, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionary_t1_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		iOSNotificationPayload_t8_267_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2146);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5451 = il2cpp_codegen_string_literal_from_index(5451);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	{
		String_t* L_0 = ____notificationPayload;
		Object_t * L_1 = JSONUtility_FromJSON_m8_298(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = ((Object_t *)IsInst(L_1, IDictionary_t1_35_il2cpp_TypeInfo_var));
		Object_t * L_2 = V_0;
		if (L_2)
		{
			goto IL_0024;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_LogError_m8_1009(NULL /*static, unused*/, _stringLiteral5368, _stringLiteral5451, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		return (CrossPlatformNotification_t8_259 *)NULL;
	}

IL_0024:
	{
		Object_t * L_3 = V_0;
		iOSNotificationPayload_t8_267 * L_4 = (iOSNotificationPayload_t8_267 *)il2cpp_codegen_object_new (iOSNotificationPayload_t8_267_il2cpp_TypeInfo_var);
		iOSNotificationPayload__ctor_m8_1535(L_4, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.String VoxelBusters.NativePlugins.CrossPlatformNotification::ToString()
extern TypeInfo* DateTime_t1_150_il2cpp_TypeInfo_var;
extern TypeInfo* eNotificationRepeatInterval_t8_264_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5452;
extern "C" String_t* CrossPlatformNotification_ToString_m8_1534 (CrossPlatformNotification_t8_259 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DateTime_t1_150_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		eNotificationRepeatInterval_t8_264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2147);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5452 = il2cpp_codegen_string_literal_from_index(5452);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = CrossPlatformNotification_get_AlertBody_m8_1516(__this, /*hidden argument*/NULL);
		DateTime_t1_150  L_1 = CrossPlatformNotification_get_FireDate_m8_1518(__this, /*hidden argument*/NULL);
		DateTime_t1_150  L_2 = L_1;
		Object_t * L_3 = Box(DateTime_t1_150_il2cpp_TypeInfo_var, &L_2);
		int32_t L_4 = CrossPlatformNotification_get_RepeatInterval_m8_1520(__this, /*hidden argument*/NULL);
		int32_t L_5 = L_4;
		Object_t * L_6 = Box(eNotificationRepeatInterval_t8_264_il2cpp_TypeInfo_var, &L_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Format_m1_550(NULL /*static, unused*/, _stringLiteral5452, L_0, L_3, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.iOSNotificationPayload::.ctor(System.Collections.IDictionary)
extern TypeInfo* iOSSpecificProperties_t8_266_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionary_t1_35_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Convert_t1_1522_il2cpp_TypeInfo_var;
extern const MethodInfo* IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var;
extern const MethodInfo* IDictionaryExtensions_GetIfAvailable_TisInt32_t1_3_m8_2007_MethodInfo_var;
extern const MethodInfo* IDictionaryExtensions_GetIfAvailable_TisIDictionary_t1_35_m8_2014_MethodInfo_var;
extern const MethodInfo* IDictionaryExtensions_GetIfAvailable_TisCalendarUnit_t6_103_m8_2015_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral5462;
extern Il2CppCodeGenString* _stringLiteral5463;
extern Il2CppCodeGenString* _stringLiteral5464;
extern Il2CppCodeGenString* _stringLiteral5458;
extern Il2CppCodeGenString* _stringLiteral5465;
extern Il2CppCodeGenString* _stringLiteral5460;
extern Il2CppCodeGenString* _stringLiteral5466;
extern Il2CppCodeGenString* _stringLiteral5467;
extern Il2CppCodeGenString* _stringLiteral5444;
extern Il2CppCodeGenString* _stringLiteral5445;
extern "C" void iOSNotificationPayload__ctor_m8_1535 (iOSNotificationPayload_t8_267 * __this, Object_t * ____payloadDict, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		iOSSpecificProperties_t8_266_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1946);
		IDictionary_t1_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Convert_t1_1522_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484117);
		IDictionaryExtensions_GetIfAvailable_TisInt32_t1_3_m8_2007_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484196);
		IDictionaryExtensions_GetIfAvailable_TisIDictionary_t1_35_m8_2014_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484236);
		IDictionaryExtensions_GetIfAvailable_TisCalendarUnit_t6_103_m8_2015_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484237);
		_stringLiteral5462 = il2cpp_codegen_string_literal_from_index(5462);
		_stringLiteral5463 = il2cpp_codegen_string_literal_from_index(5463);
		_stringLiteral5464 = il2cpp_codegen_string_literal_from_index(5464);
		_stringLiteral5458 = il2cpp_codegen_string_literal_from_index(5458);
		_stringLiteral5465 = il2cpp_codegen_string_literal_from_index(5465);
		_stringLiteral5460 = il2cpp_codegen_string_literal_from_index(5460);
		_stringLiteral5466 = il2cpp_codegen_string_literal_from_index(5466);
		_stringLiteral5467 = il2cpp_codegen_string_literal_from_index(5467);
		_stringLiteral5444 = il2cpp_codegen_string_literal_from_index(5444);
		_stringLiteral5445 = il2cpp_codegen_string_literal_from_index(5445);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	Object_t * V_1 = {0};
	Object_t * V_2 = {0};
	Object_t * V_3 = {0};
	String_t* V_4 = {0};
	bool V_5 = false;
	String_t* V_6 = {0};
	{
		CrossPlatformNotification__ctor_m8_1514(__this, /*hidden argument*/NULL);
		iOSSpecificProperties_t8_266 * L_0 = (iOSSpecificProperties_t8_266 *)il2cpp_codegen_object_new (iOSSpecificProperties_t8_266_il2cpp_TypeInfo_var);
		iOSSpecificProperties__ctor_m8_1502(L_0, /*hidden argument*/NULL);
		CrossPlatformNotification_set_iOSProperties_m8_1527(__this, L_0, /*hidden argument*/NULL);
		NotificationServiceSettings_t8_270 * L_1 = NPSettings_get_Notification_m8_1936(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		iOSSettings_t8_268 * L_2 = NotificationServiceSettings_get_iOS_m8_1564(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3 = iOSSettings_get_UserInfoKey_m8_1540(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Object_t * L_4 = ____payloadDict;
		NullCheck(L_4);
		Object_t * L_5 = (Object_t *)InterfaceFuncInvoker1< Object_t *, Object_t * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_4, _stringLiteral5462);
		V_1 = ((Object_t *)IsInst(L_5, IDictionary_t1_35_il2cpp_TypeInfo_var));
		Object_t * L_6 = V_1;
		NullCheck(L_6);
		bool L_7 = (bool)InterfaceFuncInvoker1< bool, Object_t * >::Invoke(8 /* System.Boolean System.Collections.IDictionary::Contains(System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_6, _stringLiteral5463);
		if (!L_7)
		{
			goto IL_00f9;
		}
	}
	{
		Object_t * L_8 = V_1;
		NullCheck(L_8);
		Object_t * L_9 = (Object_t *)InterfaceFuncInvoker1< Object_t *, Object_t * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_8, _stringLiteral5463);
		V_2 = L_9;
		Object_t * L_10 = V_2;
		if (!L_10)
		{
			goto IL_00f9;
		}
	}
	{
		Object_t * L_11 = V_2;
		if (!((String_t*)IsInstSealed(L_11, String_t_il2cpp_TypeInfo_var)))
		{
			goto IL_0070;
		}
	}
	{
		Object_t * L_12 = V_2;
		CrossPlatformNotification_set_AlertBody_m8_1517(__this, ((String_t*)IsInstSealed(L_12, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		goto IL_00f9;
	}

IL_0070:
	{
		Object_t * L_13 = V_2;
		V_3 = ((Object_t *)IsInst(L_13, IDictionary_t1_35_il2cpp_TypeInfo_var));
		Object_t * L_14 = V_3;
		String_t* L_15 = IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(NULL /*static, unused*/, L_14, _stringLiteral5464, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var);
		V_4 = L_15;
		V_5 = 0;
		Object_t * L_16 = V_3;
		NullCheck(L_16);
		bool L_17 = (bool)InterfaceFuncInvoker1< bool, Object_t * >::Invoke(8 /* System.Boolean System.Collections.IDictionary::Contains(System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_16, _stringLiteral5458);
		if (!L_17)
		{
			goto IL_00ae;
		}
	}
	{
		Object_t * L_18 = V_3;
		NullCheck(L_18);
		Object_t * L_19 = (Object_t *)InterfaceFuncInvoker1< Object_t *, Object_t * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_18, _stringLiteral5458);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1_1522_il2cpp_TypeInfo_var);
		bool L_20 = Convert_ToBoolean_m1_13467(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		V_5 = L_20;
		goto IL_00b8;
	}

IL_00ae:
	{
		String_t* L_21 = V_4;
		V_5 = ((((int32_t)((((Object_t*)(String_t*)L_21) == ((Object_t*)(Object_t *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}

IL_00b8:
	{
		Object_t * L_22 = V_3;
		String_t* L_23 = IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(NULL /*static, unused*/, L_22, _stringLiteral5465, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var);
		CrossPlatformNotification_set_AlertBody_m8_1517(__this, L_23, /*hidden argument*/NULL);
		iOSSpecificProperties_t8_266 * L_24 = CrossPlatformNotification_get_iOSProperties_m8_1526(__this, /*hidden argument*/NULL);
		String_t* L_25 = V_4;
		NullCheck(L_24);
		iOSSpecificProperties_set_AlertAction_m8_1505(L_24, L_25, /*hidden argument*/NULL);
		iOSSpecificProperties_t8_266 * L_26 = CrossPlatformNotification_get_iOSProperties_m8_1526(__this, /*hidden argument*/NULL);
		bool L_27 = V_5;
		NullCheck(L_26);
		iOSSpecificProperties_set_HasAction_m8_1507(L_26, L_27, /*hidden argument*/NULL);
		iOSSpecificProperties_t8_266 * L_28 = CrossPlatformNotification_get_iOSProperties_m8_1526(__this, /*hidden argument*/NULL);
		Object_t * L_29 = V_3;
		String_t* L_30 = IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(NULL /*static, unused*/, L_29, _stringLiteral5460, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var);
		NullCheck(L_28);
		iOSSpecificProperties_set_LaunchImage_m8_1511(L_28, L_30, /*hidden argument*/NULL);
	}

IL_00f9:
	{
		Object_t * L_31 = V_1;
		String_t* L_32 = IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(NULL /*static, unused*/, L_31, _stringLiteral5466, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var);
		CrossPlatformNotification_set_SoundName_m8_1525(__this, L_32, /*hidden argument*/NULL);
		iOSSpecificProperties_t8_266 * L_33 = CrossPlatformNotification_get_iOSProperties_m8_1526(__this, /*hidden argument*/NULL);
		Object_t * L_34 = V_1;
		int32_t L_35 = IDictionaryExtensions_GetIfAvailable_TisInt32_t1_3_m8_2007(NULL /*static, unused*/, L_34, _stringLiteral5467, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisInt32_t1_3_m8_2007_MethodInfo_var);
		NullCheck(L_33);
		iOSSpecificProperties_set_BadgeCount_m8_1509(L_33, L_35, /*hidden argument*/NULL);
		Object_t * L_36 = ____payloadDict;
		String_t* L_37 = V_0;
		Object_t * L_38 = IDictionaryExtensions_GetIfAvailable_TisIDictionary_t1_35_m8_2014(NULL /*static, unused*/, L_36, L_37, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisIDictionary_t1_35_m8_2014_MethodInfo_var);
		CrossPlatformNotification_set_UserInfo_m8_1523(__this, L_38, /*hidden argument*/NULL);
		Object_t * L_39 = ____payloadDict;
		String_t* L_40 = IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(NULL /*static, unused*/, L_39, _stringLiteral5444, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var);
		V_6 = L_40;
		String_t* L_41 = V_6;
		if (!L_41)
		{
			goto IL_014e;
		}
	}
	{
		String_t* L_42 = V_6;
		DateTime_t1_150  L_43 = DateTimeExtensions_ToZuluFormatDateTimeLocal_m8_186(NULL /*static, unused*/, L_42, /*hidden argument*/NULL);
		CrossPlatformNotification_set_FireDate_m8_1519(__this, L_43, /*hidden argument*/NULL);
	}

IL_014e:
	{
		Object_t * L_44 = ____payloadDict;
		int32_t L_45 = IDictionaryExtensions_GetIfAvailable_TisCalendarUnit_t6_103_m8_2015(NULL /*static, unused*/, L_44, _stringLiteral5445, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisCalendarUnit_t6_103_m8_2015_MethodInfo_var);
		int32_t L_46 = iOSNotificationPayload_ConvertToRepeatInterval_m8_1537(NULL /*static, unused*/, L_45, /*hidden argument*/NULL);
		CrossPlatformNotification_set_RepeatInterval_m8_1521(__this, L_46, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IDictionary VoxelBusters.NativePlugins.Internal.iOSNotificationPayload::CreateNotificationPayload(VoxelBusters.NativePlugins.CrossPlatformNotification)
extern TypeInfo* Dictionary_2_t1_1903_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionary_t1_35_il2cpp_TypeInfo_var;
extern TypeInfo* Convert_t1_1522_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_15037_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral5465;
extern Il2CppCodeGenString* _stringLiteral5464;
extern Il2CppCodeGenString* _stringLiteral5460;
extern Il2CppCodeGenString* _stringLiteral5458;
extern Il2CppCodeGenString* _stringLiteral5463;
extern Il2CppCodeGenString* _stringLiteral5467;
extern Il2CppCodeGenString* _stringLiteral5466;
extern Il2CppCodeGenString* _stringLiteral5462;
extern Il2CppCodeGenString* _stringLiteral5444;
extern Il2CppCodeGenString* _stringLiteral5445;
extern "C" Object_t * iOSNotificationPayload_CreateNotificationPayload_m8_1536 (Object_t * __this /* static, unused */, CrossPlatformNotification_t8_259 * ____notification, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t1_1903_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1960);
		IDictionary_t1_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		Convert_t1_1522_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		Dictionary_2__ctor_m1_15037_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484124);
		_stringLiteral5465 = il2cpp_codegen_string_literal_from_index(5465);
		_stringLiteral5464 = il2cpp_codegen_string_literal_from_index(5464);
		_stringLiteral5460 = il2cpp_codegen_string_literal_from_index(5460);
		_stringLiteral5458 = il2cpp_codegen_string_literal_from_index(5458);
		_stringLiteral5463 = il2cpp_codegen_string_literal_from_index(5463);
		_stringLiteral5467 = il2cpp_codegen_string_literal_from_index(5467);
		_stringLiteral5466 = il2cpp_codegen_string_literal_from_index(5466);
		_stringLiteral5462 = il2cpp_codegen_string_literal_from_index(5462);
		_stringLiteral5444 = il2cpp_codegen_string_literal_from_index(5444);
		_stringLiteral5445 = il2cpp_codegen_string_literal_from_index(5445);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	Object_t * V_1 = {0};
	iOSSpecificProperties_t8_266 * V_2 = {0};
	String_t* V_3 = {0};
	Object_t * V_4 = {0};
	{
		Dictionary_2_t1_1903 * L_0 = (Dictionary_2_t1_1903 *)il2cpp_codegen_object_new (Dictionary_2_t1_1903_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_15037(L_0, /*hidden argument*/Dictionary_2__ctor_m1_15037_MethodInfo_var);
		V_0 = L_0;
		Dictionary_2_t1_1903 * L_1 = (Dictionary_2_t1_1903 *)il2cpp_codegen_object_new (Dictionary_2_t1_1903_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_15037(L_1, /*hidden argument*/Dictionary_2__ctor_m1_15037_MethodInfo_var);
		V_1 = L_1;
		CrossPlatformNotification_t8_259 * L_2 = ____notification;
		NullCheck(L_2);
		iOSSpecificProperties_t8_266 * L_3 = CrossPlatformNotification_get_iOSProperties_m8_1526(L_2, /*hidden argument*/NULL);
		V_2 = L_3;
		NotificationServiceSettings_t8_270 * L_4 = NPSettings_get_Notification_m8_1936(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		iOSSettings_t8_268 * L_5 = NotificationServiceSettings_get_iOS_m8_1564(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		String_t* L_6 = iOSSettings_get_UserInfoKey_m8_1540(L_5, /*hidden argument*/NULL);
		V_3 = L_6;
		Dictionary_2_t1_1903 * L_7 = (Dictionary_2_t1_1903 *)il2cpp_codegen_object_new (Dictionary_2_t1_1903_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_15037(L_7, /*hidden argument*/Dictionary_2__ctor_m1_15037_MethodInfo_var);
		V_4 = L_7;
		CrossPlatformNotification_t8_259 * L_8 = ____notification;
		NullCheck(L_8);
		String_t* L_9 = CrossPlatformNotification_get_AlertBody_m8_1516(L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0047;
		}
	}
	{
		Object_t * L_10 = V_4;
		CrossPlatformNotification_t8_259 * L_11 = ____notification;
		NullCheck(L_11);
		String_t* L_12 = CrossPlatformNotification_get_AlertBody_m8_1516(L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		InterfaceActionInvoker2< Object_t *, Object_t * >::Invoke(3 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_10, _stringLiteral5465, L_12);
	}

IL_0047:
	{
		iOSSpecificProperties_t8_266 * L_13 = V_2;
		NullCheck(L_13);
		String_t* L_14 = iOSSpecificProperties_get_AlertAction_m8_1504(L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0064;
		}
	}
	{
		Object_t * L_15 = V_4;
		iOSSpecificProperties_t8_266 * L_16 = V_2;
		NullCheck(L_16);
		String_t* L_17 = iOSSpecificProperties_get_AlertAction_m8_1504(L_16, /*hidden argument*/NULL);
		NullCheck(L_15);
		InterfaceActionInvoker2< Object_t *, Object_t * >::Invoke(3 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_15, _stringLiteral5464, L_17);
	}

IL_0064:
	{
		iOSSpecificProperties_t8_266 * L_18 = V_2;
		NullCheck(L_18);
		String_t* L_19 = iOSSpecificProperties_get_LaunchImage_m8_1510(L_18, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_0081;
		}
	}
	{
		Object_t * L_20 = V_4;
		iOSSpecificProperties_t8_266 * L_21 = V_2;
		NullCheck(L_21);
		String_t* L_22 = iOSSpecificProperties_get_LaunchImage_m8_1510(L_21, /*hidden argument*/NULL);
		NullCheck(L_20);
		InterfaceActionInvoker2< Object_t *, Object_t * >::Invoke(3 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_20, _stringLiteral5460, L_22);
	}

IL_0081:
	{
		Object_t * L_23 = V_4;
		iOSSpecificProperties_t8_266 * L_24 = V_2;
		NullCheck(L_24);
		bool L_25 = iOSSpecificProperties_get_HasAction_m8_1506(L_24, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1_1522_il2cpp_TypeInfo_var);
		int32_t L_26 = Convert_ToInt32_m1_13579(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		int32_t L_27 = L_26;
		Object_t * L_28 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_27);
		NullCheck(L_23);
		InterfaceActionInvoker2< Object_t *, Object_t * >::Invoke(3 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_23, _stringLiteral5458, L_28);
		Object_t * L_29 = V_1;
		Object_t * L_30 = V_4;
		NullCheck(L_29);
		InterfaceActionInvoker2< Object_t *, Object_t * >::Invoke(3 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_29, _stringLiteral5463, L_30);
		Object_t * L_31 = V_1;
		iOSSpecificProperties_t8_266 * L_32 = V_2;
		NullCheck(L_32);
		int32_t L_33 = iOSSpecificProperties_get_BadgeCount_m8_1508(L_32, /*hidden argument*/NULL);
		int32_t L_34 = L_33;
		Object_t * L_35 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_34);
		NullCheck(L_31);
		InterfaceActionInvoker2< Object_t *, Object_t * >::Invoke(3 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_31, _stringLiteral5467, L_35);
		CrossPlatformNotification_t8_259 * L_36 = ____notification;
		NullCheck(L_36);
		String_t* L_37 = CrossPlatformNotification_get_SoundName_m8_1524(L_36, /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_00dc;
		}
	}
	{
		Object_t * L_38 = V_1;
		CrossPlatformNotification_t8_259 * L_39 = ____notification;
		NullCheck(L_39);
		String_t* L_40 = CrossPlatformNotification_get_SoundName_m8_1524(L_39, /*hidden argument*/NULL);
		NullCheck(L_38);
		InterfaceActionInvoker2< Object_t *, Object_t * >::Invoke(3 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_38, _stringLiteral5466, L_40);
	}

IL_00dc:
	{
		Object_t * L_41 = V_0;
		Object_t * L_42 = V_1;
		NullCheck(L_41);
		InterfaceActionInvoker2< Object_t *, Object_t * >::Invoke(3 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_41, _stringLiteral5462, L_42);
		Object_t * L_43 = V_0;
		String_t* L_44 = V_3;
		CrossPlatformNotification_t8_259 * L_45 = ____notification;
		NullCheck(L_45);
		Object_t * L_46 = CrossPlatformNotification_get_UserInfo_m8_1522(L_45, /*hidden argument*/NULL);
		NullCheck(L_43);
		InterfaceActionInvoker2< Object_t *, Object_t * >::Invoke(3 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_43, L_44, L_46);
		Object_t * L_47 = V_0;
		CrossPlatformNotification_t8_259 * L_48 = ____notification;
		NullCheck(L_48);
		DateTime_t1_150  L_49 = CrossPlatformNotification_get_FireDate_m8_1518(L_48, /*hidden argument*/NULL);
		String_t* L_50 = DateTimeExtensions_ToStringUsingZuluFormat_m8_187(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		NullCheck(L_47);
		InterfaceActionInvoker2< Object_t *, Object_t * >::Invoke(3 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_47, _stringLiteral5444, L_50);
		Object_t * L_51 = V_0;
		CrossPlatformNotification_t8_259 * L_52 = ____notification;
		NullCheck(L_52);
		int32_t L_53 = CrossPlatformNotification_get_RepeatInterval_m8_1520(L_52, /*hidden argument*/NULL);
		int32_t L_54 = iOSNotificationPayload_ConvertToCalendarUnit_m8_1538(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		int32_t L_55 = L_54;
		Object_t * L_56 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_55);
		NullCheck(L_51);
		InterfaceActionInvoker2< Object_t *, Object_t * >::Invoke(3 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_51, _stringLiteral5445, L_56);
		Object_t * L_57 = V_0;
		return L_57;
	}
}
// VoxelBusters.NativePlugins.eNotificationRepeatInterval VoxelBusters.NativePlugins.Internal.iOSNotificationPayload::ConvertToRepeatInterval(UnityEngine.iOS.CalendarUnit)
extern "C" int32_t iOSNotificationPayload_ConvertToRepeatInterval_m8_1537 (Object_t * __this /* static, unused */, int32_t ____unit, const MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		int32_t L_0 = ____unit;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)4)))
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)8)))
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)((int32_t)16))))
		{
			goto IL_0032;
		}
	}
	{
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) == ((int32_t)((int32_t)64))))
		{
			goto IL_0030;
		}
	}
	{
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) == ((int32_t)((int32_t)256))))
		{
			goto IL_0034;
		}
	}
	{
		goto IL_003a;
	}

IL_0030:
	{
		return (int32_t)(1);
	}

IL_0032:
	{
		return (int32_t)(3);
	}

IL_0034:
	{
		return (int32_t)(4);
	}

IL_0036:
	{
		return (int32_t)(5);
	}

IL_0038:
	{
		return (int32_t)(6);
	}

IL_003a:
	{
		return (int32_t)(0);
	}
}
// UnityEngine.iOS.CalendarUnit VoxelBusters.NativePlugins.Internal.iOSNotificationPayload::ConvertToCalendarUnit(VoxelBusters.NativePlugins.eNotificationRepeatInterval)
extern "C" int32_t iOSNotificationPayload_ConvertToCalendarUnit_m8_1538 (Object_t * __this /* static, unused */, int32_t ____repeatInterval, const MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		int32_t L_0 = ____repeatInterval;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 0)
		{
			goto IL_0027;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 1)
		{
			goto IL_0037;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 2)
		{
			goto IL_002a;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 3)
		{
			goto IL_002d;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 4)
		{
			goto IL_0033;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 5)
		{
			goto IL_0035;
		}
	}
	{
		goto IL_0037;
	}

IL_0027:
	{
		return (int32_t)(((int32_t)64));
	}

IL_002a:
	{
		return (int32_t)(((int32_t)16));
	}

IL_002d:
	{
		return (int32_t)(((int32_t)256));
	}

IL_0033:
	{
		return (int32_t)(8);
	}

IL_0035:
	{
		return (int32_t)(4);
	}

IL_0037:
	{
		return (int32_t)(0);
	}
}
// System.Void VoxelBusters.NativePlugins.NotificationServiceSettings/iOSSettings::.ctor()
extern Il2CppCodeGenString* _stringLiteral5468;
extern "C" void iOSSettings__ctor_m8_1539 (iOSSettings_t8_268 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral5468 = il2cpp_codegen_string_literal_from_index(5468);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		iOSSettings_set_UserInfoKey_m8_1541(__this, _stringLiteral5468, /*hidden argument*/NULL);
		return;
	}
}
// System.String VoxelBusters.NativePlugins.NotificationServiceSettings/iOSSettings::get_UserInfoKey()
extern "C" String_t* iOSSettings_get_UserInfoKey_m8_1540 (iOSSettings_t8_268 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_userInfoKey_0);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.NotificationServiceSettings/iOSSettings::set_UserInfoKey(System.String)
extern "C" void iOSSettings_set_UserInfoKey_m8_1541 (iOSSettings_t8_268 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_userInfoKey_0 = L_0;
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings::.ctor()
extern Il2CppCodeGenString* _stringLiteral5469;
extern Il2CppCodeGenString* _stringLiteral5470;
extern Il2CppCodeGenString* _stringLiteral5471;
extern Il2CppCodeGenString* _stringLiteral5468;
extern Il2CppCodeGenString* _stringLiteral3096;
extern "C" void AndroidSettings__ctor_m8_1542 (AndroidSettings_t8_269 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral5469 = il2cpp_codegen_string_literal_from_index(5469);
		_stringLiteral5470 = il2cpp_codegen_string_literal_from_index(5470);
		_stringLiteral5471 = il2cpp_codegen_string_literal_from_index(5471);
		_stringLiteral5468 = il2cpp_codegen_string_literal_from_index(5468);
		_stringLiteral3096 = il2cpp_codegen_string_literal_from_index(3096);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___m_allowVibration_2 = 1;
		__this->___m_tickerTextKey_5 = _stringLiteral5469;
		__this->___m_contentTextKey_6 = _stringLiteral5470;
		__this->___m_contentTitleKey_7 = _stringLiteral5471;
		__this->___m_userInfoKey_8 = _stringLiteral5468;
		__this->___m_tagKey_9 = _stringLiteral3096;
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String[] VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings::get_SenderIDList()
extern "C" StringU5BU5D_t1_238* AndroidSettings_get_SenderIDList_m8_1543 (AndroidSettings_t8_269 * __this, const MethodInfo* method)
{
	{
		StringU5BU5D_t1_238* L_0 = (__this->___m_senderIDs_0);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings::set_SenderIDList(System.String[])
extern "C" void AndroidSettings_set_SenderIDList_m8_1544 (AndroidSettings_t8_269 * __this, StringU5BU5D_t1_238* ___value, const MethodInfo* method)
{
	{
		StringU5BU5D_t1_238* L_0 = ___value;
		__this->___m_senderIDs_0 = L_0;
		return;
	}
}
// System.Boolean VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings::get_NeedsBigStyle()
extern "C" bool AndroidSettings_get_NeedsBigStyle_m8_1545 (AndroidSettings_t8_269 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_needsBigStyle_1);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings::set_NeedsBigStyle(System.Boolean)
extern "C" void AndroidSettings_set_NeedsBigStyle_m8_1546 (AndroidSettings_t8_269 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___m_needsBigStyle_1 = L_0;
		return;
	}
}
// System.Boolean VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings::get_AllowVibration()
extern "C" bool AndroidSettings_get_AllowVibration_m8_1547 (AndroidSettings_t8_269 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_allowVibration_2);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings::set_AllowVibration(System.Boolean)
extern "C" void AndroidSettings_set_AllowVibration_m8_1548 (AndroidSettings_t8_269 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___m_allowVibration_2 = L_0;
		return;
	}
}
// UnityEngine.Texture2D VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings::get_WhiteSmallIcon()
extern "C" Texture2D_t6_33 * AndroidSettings_get_WhiteSmallIcon_m8_1549 (AndroidSettings_t8_269 * __this, const MethodInfo* method)
{
	{
		Texture2D_t6_33 * L_0 = (__this->___m_whiteSmallIcon_3);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings::set_WhiteSmallIcon(UnityEngine.Texture2D)
extern "C" void AndroidSettings_set_WhiteSmallIcon_m8_1550 (AndroidSettings_t8_269 * __this, Texture2D_t6_33 * ___value, const MethodInfo* method)
{
	{
		Texture2D_t6_33 * L_0 = ___value;
		__this->___m_whiteSmallIcon_3 = L_0;
		return;
	}
}
// UnityEngine.Texture2D VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings::get_ColouredSmallIcon()
extern "C" Texture2D_t6_33 * AndroidSettings_get_ColouredSmallIcon_m8_1551 (AndroidSettings_t8_269 * __this, const MethodInfo* method)
{
	{
		Texture2D_t6_33 * L_0 = (__this->___m_colouredSmallIcon_4);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings::set_ColouredSmallIcon(UnityEngine.Texture2D)
extern "C" void AndroidSettings_set_ColouredSmallIcon_m8_1552 (AndroidSettings_t8_269 * __this, Texture2D_t6_33 * ___value, const MethodInfo* method)
{
	{
		Texture2D_t6_33 * L_0 = ___value;
		__this->___m_colouredSmallIcon_4 = L_0;
		return;
	}
}
// System.String VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings::get_TickerTextKey()
extern "C" String_t* AndroidSettings_get_TickerTextKey_m8_1553 (AndroidSettings_t8_269 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_tickerTextKey_5);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings::set_TickerTextKey(System.String)
extern "C" void AndroidSettings_set_TickerTextKey_m8_1554 (AndroidSettings_t8_269 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_tickerTextKey_5 = L_0;
		return;
	}
}
// System.String VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings::get_ContentTitleKey()
extern "C" String_t* AndroidSettings_get_ContentTitleKey_m8_1555 (AndroidSettings_t8_269 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_contentTitleKey_7);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings::set_ContentTitleKey(System.String)
extern "C" void AndroidSettings_set_ContentTitleKey_m8_1556 (AndroidSettings_t8_269 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_contentTitleKey_7 = L_0;
		return;
	}
}
// System.String VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings::get_ContentTextKey()
extern "C" String_t* AndroidSettings_get_ContentTextKey_m8_1557 (AndroidSettings_t8_269 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_contentTextKey_6);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings::set_ContentTextKey(System.String)
extern "C" void AndroidSettings_set_ContentTextKey_m8_1558 (AndroidSettings_t8_269 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_contentTextKey_6 = L_0;
		return;
	}
}
// System.String VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings::get_UserInfoKey()
extern "C" String_t* AndroidSettings_get_UserInfoKey_m8_1559 (AndroidSettings_t8_269 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_userInfoKey_8);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings::set_UserInfoKey(System.String)
extern "C" void AndroidSettings_set_UserInfoKey_m8_1560 (AndroidSettings_t8_269 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_userInfoKey_8 = L_0;
		return;
	}
}
// System.String VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings::get_TagKey()
extern "C" String_t* AndroidSettings_get_TagKey_m8_1561 (AndroidSettings_t8_269 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_tagKey_9);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings::set_TagKey(System.String)
extern "C" void AndroidSettings_set_TagKey_m8_1562 (AndroidSettings_t8_269 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_tagKey_9 = L_0;
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.NotificationServiceSettings::.ctor()
extern TypeInfo* iOSSettings_t8_268_il2cpp_TypeInfo_var;
extern TypeInfo* AndroidSettings_t8_269_il2cpp_TypeInfo_var;
extern "C" void NotificationServiceSettings__ctor_m8_1563 (NotificationServiceSettings_t8_270 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		iOSSettings_t8_268_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2149);
		AndroidSettings_t8_269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2150);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		iOSSettings_t8_268 * L_0 = (iOSSettings_t8_268 *)il2cpp_codegen_object_new (iOSSettings_t8_268_il2cpp_TypeInfo_var);
		iOSSettings__ctor_m8_1539(L_0, /*hidden argument*/NULL);
		NotificationServiceSettings_set_iOS_m8_1565(__this, L_0, /*hidden argument*/NULL);
		AndroidSettings_t8_269 * L_1 = (AndroidSettings_t8_269 *)il2cpp_codegen_object_new (AndroidSettings_t8_269_il2cpp_TypeInfo_var);
		AndroidSettings__ctor_m8_1542(L_1, /*hidden argument*/NULL);
		NotificationServiceSettings_set_Android_m8_1567(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// VoxelBusters.NativePlugins.NotificationServiceSettings/iOSSettings VoxelBusters.NativePlugins.NotificationServiceSettings::get_iOS()
extern "C" iOSSettings_t8_268 * NotificationServiceSettings_get_iOS_m8_1564 (NotificationServiceSettings_t8_270 * __this, const MethodInfo* method)
{
	{
		iOSSettings_t8_268 * L_0 = (__this->___m_iOS_0);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.NotificationServiceSettings::set_iOS(VoxelBusters.NativePlugins.NotificationServiceSettings/iOSSettings)
extern "C" void NotificationServiceSettings_set_iOS_m8_1565 (NotificationServiceSettings_t8_270 * __this, iOSSettings_t8_268 * ___value, const MethodInfo* method)
{
	{
		iOSSettings_t8_268 * L_0 = ___value;
		__this->___m_iOS_0 = L_0;
		return;
	}
}
// VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings VoxelBusters.NativePlugins.NotificationServiceSettings::get_Android()
extern "C" AndroidSettings_t8_269 * NotificationServiceSettings_get_Android_m8_1566 (NotificationServiceSettings_t8_270 * __this, const MethodInfo* method)
{
	{
		AndroidSettings_t8_269 * L_0 = (__this->___m_android_1);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.NotificationServiceSettings::set_Android(VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings)
extern "C" void NotificationServiceSettings_set_Android_m8_1567 (NotificationServiceSettings_t8_270 * __this, AndroidSettings_t8_269 * ___value, const MethodInfo* method)
{
	{
		AndroidSettings_t8_269 * L_0 = ___value;
		__this->___m_android_1 = L_0;
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Sharing/SharingCompletion::.ctor(System.Object,System.IntPtr)
extern "C" void SharingCompletion__ctor_m8_1568 (SharingCompletion_t8_271 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void VoxelBusters.NativePlugins.Sharing/SharingCompletion::Invoke(VoxelBusters.NativePlugins.eShareResult)
extern "C" void SharingCompletion_Invoke_m8_1569 (SharingCompletion_t8_271 * __this, int32_t ____result, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		SharingCompletion_Invoke_m8_1569((SharingCompletion_t8_271 *)__this->___prev_9,____result, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, int32_t ____result, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,____result,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, int32_t ____result, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,____result,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_SharingCompletion_t8_271(Il2CppObject* delegate, int32_t ____result)
{
	typedef void (STDCALL *native_function_ptr_type)(int32_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '____result' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____result);

	// Marshaling cleanup of parameter '____result' native representation

}
// System.IAsyncResult VoxelBusters.NativePlugins.Sharing/SharingCompletion::BeginInvoke(VoxelBusters.NativePlugins.eShareResult,System.AsyncCallback,System.Object)
extern TypeInfo* eShareResult_t8_286_il2cpp_TypeInfo_var;
extern "C" Object_t * SharingCompletion_BeginInvoke_m8_1570 (SharingCompletion_t8_271 * __this, int32_t ____result, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		eShareResult_t8_286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2151);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(eShareResult_t8_286_il2cpp_TypeInfo_var, &____result);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void VoxelBusters.NativePlugins.Sharing/SharingCompletion::EndInvoke(System.IAsyncResult)
extern "C" void SharingCompletion_EndInvoke_m8_1571 (SharingCompletion_t8_271 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void VoxelBusters.NativePlugins.Sharing/<ShowViewCoroutine>c__Iterator6::.ctor()
extern "C" void U3CShowViewCoroutineU3Ec__Iterator6__ctor_m8_1572 (U3CShowViewCoroutineU3Ec__Iterator6_t8_272 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object VoxelBusters.NativePlugins.Sharing/<ShowViewCoroutine>c__Iterator6::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CShowViewCoroutineU3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m8_1573 (U3CShowViewCoroutineU3Ec__Iterator6_t8_272 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_3);
		return L_0;
	}
}
// System.Object VoxelBusters.NativePlugins.Sharing/<ShowViewCoroutine>c__Iterator6::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CShowViewCoroutineU3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m8_1574 (U3CShowViewCoroutineU3Ec__Iterator6_t8_272 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_3);
		return L_0;
	}
}
// System.Boolean VoxelBusters.NativePlugins.Sharing/<ShowViewCoroutine>c__Iterator6::MoveNext()
extern TypeInfo* IShareView_t8_274_il2cpp_TypeInfo_var;
extern TypeInfo* MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var;
extern TypeInfo* MailShareComposer_t8_279_il2cpp_TypeInfo_var;
extern TypeInfo* MessageShareComposer_t8_281_il2cpp_TypeInfo_var;
extern TypeInfo* WhatsAppShareComposer_t8_285_il2cpp_TypeInfo_var;
extern TypeInfo* FBShareComposer_t8_282_il2cpp_TypeInfo_var;
extern TypeInfo* TwitterShareComposer_t8_284_il2cpp_TypeInfo_var;
extern TypeInfo* ShareSheet_t8_289_il2cpp_TypeInfo_var;
extern "C" bool U3CShowViewCoroutineU3Ec__Iterator6_MoveNext_m8_1575 (U3CShowViewCoroutineU3Ec__Iterator6_t8_272 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IShareView_t8_274_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2152);
		MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1959);
		MailShareComposer_t8_279_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2153);
		MessageShareComposer_t8_281_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2154);
		WhatsAppShareComposer_t8_285_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2155);
		FBShareComposer_t8_282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2156);
		TwitterShareComposer_t8_284_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2157);
		ShareSheet_t8_289_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2158);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_2);
		V_0 = L_0;
		__this->___U24PC_2 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0039;
		}
	}
	{
		goto IL_0159;
	}

IL_0021:
	{
		goto IL_0039;
	}

IL_0026:
	{
		__this->___U24current_3 = NULL;
		__this->___U24PC_2 = 1;
		goto IL_015b;
	}

IL_0039:
	{
		Object_t * L_2 = (__this->____shareView_0);
		NullCheck(L_2);
		bool L_3 = (bool)InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean VoxelBusters.NativePlugins.IShareView::get_IsReadyToShowView() */, IShareView_t8_274_il2cpp_TypeInfo_var, L_2);
		if (!L_3)
		{
			goto IL_0026;
		}
	}
	{
		Sharing_t8_273 * L_4 = (__this->___U3CU3Ef__this_6);
		IL2CPP_RUNTIME_CLASS_INIT(MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var);
		MonoBehaviourExtensions_PauseUnity_m8_210(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		Sharing_t8_273 * L_5 = (__this->___U3CU3Ef__this_6);
		SharingCompletion_t8_271 * L_6 = (__this->____onCompletion_1);
		NullCheck(L_5);
		L_5->___OnSharingFinished_3 = L_6;
		Object_t * L_7 = (__this->____shareView_0);
		if (!((MailShareComposer_t8_279 *)IsInstClass(L_7, MailShareComposer_t8_279_il2cpp_TypeInfo_var)))
		{
			goto IL_0090;
		}
	}
	{
		Sharing_t8_273 * L_8 = (__this->___U3CU3Ef__this_6);
		Object_t * L_9 = (__this->____shareView_0);
		NullCheck(L_8);
		VirtActionInvoker1< MailShareComposer_t8_279 * >::Invoke(9 /* System.Void VoxelBusters.NativePlugins.Sharing::ShowMailShareComposer(VoxelBusters.NativePlugins.MailShareComposer) */, L_8, ((MailShareComposer_t8_279 *)CastclassClass(L_9, MailShareComposer_t8_279_il2cpp_TypeInfo_var)));
		goto IL_0152;
	}

IL_0090:
	{
		Object_t * L_10 = (__this->____shareView_0);
		if (!((MessageShareComposer_t8_281 *)IsInstClass(L_10, MessageShareComposer_t8_281_il2cpp_TypeInfo_var)))
		{
			goto IL_00bb;
		}
	}
	{
		Sharing_t8_273 * L_11 = (__this->___U3CU3Ef__this_6);
		Object_t * L_12 = (__this->____shareView_0);
		NullCheck(L_11);
		VirtActionInvoker1< MessageShareComposer_t8_281 * >::Invoke(14 /* System.Void VoxelBusters.NativePlugins.Sharing::ShowMessageShareComposer(VoxelBusters.NativePlugins.MessageShareComposer) */, L_11, ((MessageShareComposer_t8_281 *)CastclassClass(L_12, MessageShareComposer_t8_281_il2cpp_TypeInfo_var)));
		goto IL_0152;
	}

IL_00bb:
	{
		Object_t * L_13 = (__this->____shareView_0);
		if (!((WhatsAppShareComposer_t8_285 *)IsInstClass(L_13, WhatsAppShareComposer_t8_285_il2cpp_TypeInfo_var)))
		{
			goto IL_00e6;
		}
	}
	{
		Sharing_t8_273 * L_14 = (__this->___U3CU3Ef__this_6);
		Object_t * L_15 = (__this->____shareView_0);
		NullCheck(L_14);
		VirtActionInvoker1< WhatsAppShareComposer_t8_285 * >::Invoke(27 /* System.Void VoxelBusters.NativePlugins.Sharing::ShowWhatsAppShareComposer(VoxelBusters.NativePlugins.WhatsAppShareComposer) */, L_14, ((WhatsAppShareComposer_t8_285 *)CastclassClass(L_15, WhatsAppShareComposer_t8_285_il2cpp_TypeInfo_var)));
		goto IL_0152;
	}

IL_00e6:
	{
		Object_t * L_16 = (__this->____shareView_0);
		if (!((FBShareComposer_t8_282 *)IsInstClass(L_16, FBShareComposer_t8_282_il2cpp_TypeInfo_var)))
		{
			goto IL_0111;
		}
	}
	{
		Sharing_t8_273 * L_17 = (__this->___U3CU3Ef__this_6);
		Object_t * L_18 = (__this->____shareView_0);
		NullCheck(L_17);
		VirtActionInvoker1< FBShareComposer_t8_282 * >::Invoke(22 /* System.Void VoxelBusters.NativePlugins.Sharing::ShowFBShareComposer(VoxelBusters.NativePlugins.FBShareComposer) */, L_17, ((FBShareComposer_t8_282 *)CastclassClass(L_18, FBShareComposer_t8_282_il2cpp_TypeInfo_var)));
		goto IL_0152;
	}

IL_0111:
	{
		Object_t * L_19 = (__this->____shareView_0);
		if (!((TwitterShareComposer_t8_284 *)IsInstClass(L_19, TwitterShareComposer_t8_284_il2cpp_TypeInfo_var)))
		{
			goto IL_013c;
		}
	}
	{
		Sharing_t8_273 * L_20 = (__this->___U3CU3Ef__this_6);
		Object_t * L_21 = (__this->____shareView_0);
		NullCheck(L_20);
		VirtActionInvoker1< TwitterShareComposer_t8_284 * >::Invoke(23 /* System.Void VoxelBusters.NativePlugins.Sharing::ShowTwitterShareComposer(VoxelBusters.NativePlugins.TwitterShareComposer) */, L_20, ((TwitterShareComposer_t8_284 *)CastclassClass(L_21, TwitterShareComposer_t8_284_il2cpp_TypeInfo_var)));
		goto IL_0152;
	}

IL_013c:
	{
		Sharing_t8_273 * L_22 = (__this->___U3CU3Ef__this_6);
		Object_t * L_23 = (__this->____shareView_0);
		NullCheck(L_22);
		VirtActionInvoker1< ShareSheet_t8_289 * >::Invoke(30 /* System.Void VoxelBusters.NativePlugins.Sharing::ShowShareSheet(VoxelBusters.NativePlugins.ShareSheet) */, L_22, ((ShareSheet_t8_289 *)CastclassClass(L_23, ShareSheet_t8_289_il2cpp_TypeInfo_var)));
	}

IL_0152:
	{
		__this->___U24PC_2 = (-1);
	}

IL_0159:
	{
		return 0;
	}

IL_015b:
	{
		return 1;
	}
	// Dead block : IL_015d: ldloc.1
}
// System.Void VoxelBusters.NativePlugins.Sharing/<ShowViewCoroutine>c__Iterator6::Dispose()
extern "C" void U3CShowViewCoroutineU3Ec__Iterator6_Dispose_m8_1576 (U3CShowViewCoroutineU3Ec__Iterator6_t8_272 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_2 = (-1);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Sharing/<ShowViewCoroutine>c__Iterator6::Reset()
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern "C" void U3CShowViewCoroutineU3Ec__Iterator6_Reset_m8_1577 (U3CShowViewCoroutineU3Ec__Iterator6_t8_272 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14416(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void VoxelBusters.NativePlugins.Sharing/<SendMailWithScreenshot>c__AnonStoreyE::.ctor()
extern "C" void U3CSendMailWithScreenshotU3Ec__AnonStoreyE__ctor_m8_1578 (U3CSendMailWithScreenshotU3Ec__AnonStoreyE_t8_275 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Sharing/<SendMailWithScreenshot>c__AnonStoreyE::<>m__F(UnityEngine.Texture2D)
extern Il2CppCodeGenString* _stringLiteral5476;
extern Il2CppCodeGenString* _stringLiteral5367;
extern "C" void U3CSendMailWithScreenshotU3Ec__AnonStoreyE_U3CU3Em__F_m8_1579 (U3CSendMailWithScreenshotU3Ec__AnonStoreyE_t8_275 * __this, Texture2D_t6_33 * ____texture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral5476 = il2cpp_codegen_string_literal_from_index(5476);
		_stringLiteral5367 = il2cpp_codegen_string_literal_from_index(5367);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	{
		Texture2D_t6_33 * L_0 = ____texture;
		NullCheck(L_0);
		ByteU5BU5D_t1_109* L_1 = Texture2D_EncodeToPNG_m6_172(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Sharing_t8_273 * L_2 = (__this->___U3CU3Ef__this_5);
		String_t* L_3 = (__this->____subject_0);
		String_t* L_4 = (__this->____body_1);
		bool L_5 = (__this->____isHTMLBody_2);
		ByteU5BU5D_t1_109* L_6 = V_0;
		StringU5BU5D_t1_238* L_7 = (__this->____recipients_3);
		SharingCompletion_t8_271 * L_8 = (__this->____onCompletion_4);
		NullCheck(L_2);
		VirtActionInvoker8< String_t*, String_t*, bool, ByteU5BU5D_t1_109*, String_t*, String_t*, StringU5BU5D_t1_238*, SharingCompletion_t8_271 * >::Invoke(10 /* System.Void VoxelBusters.NativePlugins.Sharing::SendMail(System.String,System.String,System.Boolean,System.Byte[],System.String,System.String,System.String[],VoxelBusters.NativePlugins.Sharing/SharingCompletion) */, L_2, L_3, L_4, L_5, L_6, _stringLiteral5476, _stringLiteral5367, L_7, L_8);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Sharing/<ShareScreenshotOnWhatsApp>c__AnonStoreyF::.ctor()
extern "C" void U3CShareScreenshotOnWhatsAppU3Ec__AnonStoreyF__ctor_m8_1580 (U3CShareScreenshotOnWhatsAppU3Ec__AnonStoreyF_t8_276 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Sharing/<ShareScreenshotOnWhatsApp>c__AnonStoreyF::<>m__10(UnityEngine.Texture2D)
extern "C" void U3CShareScreenshotOnWhatsAppU3Ec__AnonStoreyF_U3CU3Em__10_m8_1581 (U3CShareScreenshotOnWhatsAppU3Ec__AnonStoreyF_t8_276 * __this, Texture2D_t6_33 * ____texture, const MethodInfo* method)
{
	ByteU5BU5D_t1_109* V_0 = {0};
	{
		Texture2D_t6_33 * L_0 = ____texture;
		NullCheck(L_0);
		ByteU5BU5D_t1_109* L_1 = Texture2D_EncodeToPNG_m6_172(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Sharing_t8_273 * L_2 = (__this->___U3CU3Ef__this_1);
		ByteU5BU5D_t1_109* L_3 = V_0;
		SharingCompletion_t8_271 * L_4 = (__this->____onCompletion_0);
		NullCheck(L_2);
		VirtActionInvoker2< ByteU5BU5D_t1_109*, SharingCompletion_t8_271 * >::Invoke(29 /* System.Void VoxelBusters.NativePlugins.Sharing::ShareImageOnWhatsApp(System.Byte[],VoxelBusters.NativePlugins.Sharing/SharingCompletion) */, L_2, L_3, L_4);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Sharing/<ShareScreenShot>c__AnonStorey10::.ctor()
extern "C" void U3CShareScreenShotU3Ec__AnonStorey10__ctor_m8_1582 (U3CShareScreenShotU3Ec__AnonStorey10_t8_277 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Sharing/<ShareScreenShot>c__AnonStorey10::<>m__11(UnityEngine.Texture2D)
extern "C" void U3CShareScreenShotU3Ec__AnonStorey10_U3CU3Em__11_m8_1583 (U3CShareScreenShotU3Ec__AnonStorey10_t8_277 * __this, Texture2D_t6_33 * ____texture, const MethodInfo* method)
{
	{
		Sharing_t8_273 * L_0 = (__this->___U3CU3Ef__this_3);
		String_t* L_1 = (__this->____message_0);
		Texture2D_t6_33 * L_2 = ____texture;
		eShareOptionsU5BU5D_t8_186* L_3 = (__this->____excludedOptions_1);
		SharingCompletion_t8_271 * L_4 = (__this->____onCompletion_2);
		NullCheck(L_0);
		Sharing_ShareImage_m8_1639(L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Sharing/<ShareImageAtPath>c__AnonStorey11::.ctor()
extern "C" void U3CShareImageAtPathU3Ec__AnonStorey11__ctor_m8_1584 (U3CShareImageAtPathU3Ec__AnonStorey11_t8_278 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Sharing/<ShareImageAtPath>c__AnonStorey11::<>m__12(UnityEngine.Texture2D,System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5494;
extern "C" void U3CShareImageAtPathU3Ec__AnonStorey11_U3CU3Em__12_m8_1585 (U3CShareImageAtPathU3Ec__AnonStorey11_t8_278 * __this, Texture2D_t6_33 * ____texture, String_t* ____error, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5494 = il2cpp_codegen_string_literal_from_index(5494);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ____error;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1_521(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0021;
		}
	}
	{
		String_t* L_2 = ____error;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m1_559(NULL /*static, unused*/, _stringLiteral5494, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_LogWarning_m8_1008(NULL /*static, unused*/, _stringLiteral5368, L_3, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
	}

IL_0021:
	{
		Sharing_t8_273 * L_4 = (__this->___U3CU3Ef__this_3);
		String_t* L_5 = (__this->____message_0);
		Texture2D_t6_33 * L_6 = ____texture;
		eShareOptionsU5BU5D_t8_186* L_7 = (__this->____excludedOptions_1);
		SharingCompletion_t8_271 * L_8 = (__this->____onCompletion_2);
		NullCheck(L_4);
		Sharing_ShareImage_m8_1639(L_4, L_5, L_6, L_7, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Sharing::.ctor()
extern TypeInfo* eShareOptionsU5BU5D_t8_186_il2cpp_TypeInfo_var;
extern "C" void Sharing__ctor_m8_1586 (Sharing_t8_273 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		eShareOptionsU5BU5D_t8_186_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2107);
		s_Il2CppMethodIntialized = true;
	}
	{
		eShareOptionsU5BU5D_t8_186* L_0 = ((eShareOptionsU5BU5D_t8_186*)SZArrayNew(eShareOptionsU5BU5D_t8_186_il2cpp_TypeInfo_var, 3));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_0, 0, sizeof(int32_t))) = (int32_t)1;
		eShareOptionsU5BU5D_t8_186* L_1 = L_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_1, 1, sizeof(int32_t))) = (int32_t)2;
		eShareOptionsU5BU5D_t8_186* L_2 = L_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 2);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_2, 2, sizeof(int32_t))) = (int32_t)5;
		__this->___m_socialNetworkExcludedList_4 = L_2;
		MonoBehaviour__ctor_m6_671(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Sharing::SharingFinished(System.String)
extern TypeInfo* MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var;
extern TypeInfo* eShareResult_t8_286_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5472;
extern "C" void Sharing_SharingFinished_m8_1587 (Sharing_t8_273 * __this, String_t* ____reasonString, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1959);
		eShareResult_t8_286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2151);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5472 = il2cpp_codegen_string_literal_from_index(5472);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var);
		MonoBehaviourExtensions_ResumeUnity_m8_211(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		String_t* L_0 = ____reasonString;
		VirtActionInvoker2< String_t*, int32_t* >::Invoke(4 /* System.Void VoxelBusters.NativePlugins.Sharing::ParseSharingFinishedData(System.String,VoxelBusters.NativePlugins.eShareResult&) */, __this, L_0, (&V_0));
		int32_t L_1 = V_0;
		int32_t L_2 = L_1;
		Object_t * L_3 = Box(eShareResult_t8_286_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m1_556(NULL /*static, unused*/, _stringLiteral5472, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_Log_m8_1007(NULL /*static, unused*/, _stringLiteral5368, L_4, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		SharingCompletion_t8_271 * L_5 = (__this->___OnSharingFinished_3);
		if (!L_5)
		{
			goto IL_0041;
		}
	}
	{
		SharingCompletion_t8_271 * L_6 = (__this->___OnSharingFinished_3);
		int32_t L_7 = V_0;
		NullCheck(L_6);
		SharingCompletion_Invoke_m8_1569(L_6, L_7, /*hidden argument*/NULL);
	}

IL_0041:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Sharing::ParseSharingFinishedData(System.String,VoxelBusters.NativePlugins.eShareResult&)
extern "C" void Sharing_ParseSharingFinishedData_m8_1588 (Sharing_t8_273 * __this, String_t* ____resultString, int32_t* ____shareResult, const MethodInfo* method)
{
	{
		int32_t* L_0 = ____shareResult;
		*((int32_t*)(L_0)) = (int32_t)0;
		return;
	}
}
// System.String VoxelBusters.NativePlugins.Sharing::SharingFailedResponse()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" String_t* Sharing_SharingFailedResponse_m8_1589 (Sharing_t8_273 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.Sharing::MailShareFinished(System.String)
extern TypeInfo* MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var;
extern TypeInfo* eShareResult_t8_286_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5473;
extern "C" void Sharing_MailShareFinished_m8_1590 (Sharing_t8_273 * __this, String_t* ____reasonString, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1959);
		eShareResult_t8_286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2151);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5473 = il2cpp_codegen_string_literal_from_index(5473);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var);
		MonoBehaviourExtensions_ResumeUnity_m8_211(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		String_t* L_0 = ____reasonString;
		VirtActionInvoker2< String_t*, int32_t* >::Invoke(6 /* System.Void VoxelBusters.NativePlugins.Sharing::ParseMailShareFinishedData(System.String,VoxelBusters.NativePlugins.eShareResult&) */, __this, L_0, (&V_0));
		int32_t L_1 = V_0;
		int32_t L_2 = L_1;
		Object_t * L_3 = Box(eShareResult_t8_286_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m1_556(NULL /*static, unused*/, _stringLiteral5473, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_Log_m8_1007(NULL /*static, unused*/, _stringLiteral5368, L_4, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		SharingCompletion_t8_271 * L_5 = (__this->___OnSharingFinished_3);
		if (!L_5)
		{
			goto IL_0041;
		}
	}
	{
		SharingCompletion_t8_271 * L_6 = (__this->___OnSharingFinished_3);
		int32_t L_7 = V_0;
		NullCheck(L_6);
		SharingCompletion_Invoke_m8_1569(L_6, L_7, /*hidden argument*/NULL);
	}

IL_0041:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Sharing::ParseMailShareFinishedData(System.String,VoxelBusters.NativePlugins.eShareResult&)
extern "C" void Sharing_ParseMailShareFinishedData_m8_1591 (Sharing_t8_273 * __this, String_t* ____resultString, int32_t* ____shareResult, const MethodInfo* method)
{
	{
		int32_t* L_0 = ____shareResult;
		*((int32_t*)(L_0)) = (int32_t)0;
		return;
	}
}
// System.String VoxelBusters.NativePlugins.Sharing::MailShareFailedResponse()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" String_t* Sharing_MailShareFailedResponse_m8_1592 (Sharing_t8_273 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		return L_0;
	}
}
// System.Boolean VoxelBusters.NativePlugins.Sharing::IsMailServiceAvailable()
extern TypeInfo* Boolean_t1_20_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5474;
extern "C" bool Sharing_IsMailServiceAvailable_m8_1593 (Sharing_t8_273 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Boolean_t1_20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(54);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5474 = il2cpp_codegen_string_literal_from_index(5474);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		V_0 = 0;
		bool L_0 = V_0;
		bool L_1 = L_0;
		Object_t * L_2 = Box(Boolean_t1_20_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m1_556(NULL /*static, unused*/, _stringLiteral5474, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_Log_m8_1007(NULL /*static, unused*/, _stringLiteral5368, L_3, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		bool L_4 = V_0;
		return L_4;
	}
}
// System.Void VoxelBusters.NativePlugins.Sharing::ShowMailShareComposer(VoxelBusters.NativePlugins.MailShareComposer)
extern "C" void Sharing_ShowMailShareComposer_m8_1594 (Sharing_t8_273 * __this, MailShareComposer_t8_279 * ____composer, const MethodInfo* method)
{
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean VoxelBusters.NativePlugins.Sharing::IsMailServiceAvailable() */, __this);
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String VoxelBusters.NativePlugins.Sharing::MailShareFailedResponse() */, __this);
		Sharing_MailShareFinished_m8_1590(__this, L_1, /*hidden argument*/NULL);
		return;
	}

IL_0018:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Sharing::SendPlainTextMail(System.String,System.String,System.String[],VoxelBusters.NativePlugins.Sharing/SharingCompletion)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void Sharing_SendPlainTextMail_m8_1595 (Sharing_t8_273 * __this, String_t* ____subject, String_t* ____body, StringU5BU5D_t1_238* ____recipients, SharingCompletion_t8_271 * ____onCompletion, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ____subject;
		String_t* L_1 = ____body;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		StringU5BU5D_t1_238* L_4 = ____recipients;
		SharingCompletion_t8_271 * L_5 = ____onCompletion;
		VirtActionInvoker8< String_t*, String_t*, bool, ByteU5BU5D_t1_109*, String_t*, String_t*, StringU5BU5D_t1_238*, SharingCompletion_t8_271 * >::Invoke(10 /* System.Void VoxelBusters.NativePlugins.Sharing::SendMail(System.String,System.String,System.Boolean,System.Byte[],System.String,System.String,System.String[],VoxelBusters.NativePlugins.Sharing/SharingCompletion) */, __this, L_0, L_1, 0, (ByteU5BU5D_t1_109*)(ByteU5BU5D_t1_109*)NULL, L_2, L_3, L_4, L_5);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Sharing::SendHTMLTextMail(System.String,System.String,System.String[],VoxelBusters.NativePlugins.Sharing/SharingCompletion)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void Sharing_SendHTMLTextMail_m8_1596 (Sharing_t8_273 * __this, String_t* ____subject, String_t* ____htmlBody, StringU5BU5D_t1_238* ____recipients, SharingCompletion_t8_271 * ____onCompletion, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ____subject;
		String_t* L_1 = ____htmlBody;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		StringU5BU5D_t1_238* L_4 = ____recipients;
		SharingCompletion_t8_271 * L_5 = ____onCompletion;
		VirtActionInvoker8< String_t*, String_t*, bool, ByteU5BU5D_t1_109*, String_t*, String_t*, StringU5BU5D_t1_238*, SharingCompletion_t8_271 * >::Invoke(10 /* System.Void VoxelBusters.NativePlugins.Sharing::SendMail(System.String,System.String,System.Boolean,System.Byte[],System.String,System.String,System.String[],VoxelBusters.NativePlugins.Sharing/SharingCompletion) */, __this, L_0, L_1, 1, (ByteU5BU5D_t1_109*)(ByteU5BU5D_t1_109*)NULL, L_2, L_3, L_4, L_5);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Sharing::SendMailWithScreenshot(System.String,System.String,System.Boolean,System.String[],VoxelBusters.NativePlugins.Sharing/SharingCompletion)
extern TypeInfo* U3CSendMailWithScreenshotU3Ec__AnonStoreyE_t8_275_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t1_1902_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CSendMailWithScreenshotU3Ec__AnonStoreyE_U3CU3Em__F_m8_1579_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m1_15066_MethodInfo_var;
extern "C" void Sharing_SendMailWithScreenshot_m8_1597 (Sharing_t8_273 * __this, String_t* ____subject, String_t* ____body, bool ____isHTMLBody, StringU5BU5D_t1_238* ____recipients, SharingCompletion_t8_271 * ____onCompletion, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CSendMailWithScreenshotU3Ec__AnonStoreyE_t8_275_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2159);
		Action_1_t1_1902_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2138);
		U3CSendMailWithScreenshotU3Ec__AnonStoreyE_U3CU3Em__F_m8_1579_MethodInfo_var = il2cpp_codegen_method_info_from_index(590);
		Action_1__ctor_m1_15066_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484232);
		s_Il2CppMethodIntialized = true;
	}
	U3CSendMailWithScreenshotU3Ec__AnonStoreyE_t8_275 * V_0 = {0};
	{
		U3CSendMailWithScreenshotU3Ec__AnonStoreyE_t8_275 * L_0 = (U3CSendMailWithScreenshotU3Ec__AnonStoreyE_t8_275 *)il2cpp_codegen_object_new (U3CSendMailWithScreenshotU3Ec__AnonStoreyE_t8_275_il2cpp_TypeInfo_var);
		U3CSendMailWithScreenshotU3Ec__AnonStoreyE__ctor_m8_1578(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CSendMailWithScreenshotU3Ec__AnonStoreyE_t8_275 * L_1 = V_0;
		String_t* L_2 = ____subject;
		NullCheck(L_1);
		L_1->____subject_0 = L_2;
		U3CSendMailWithScreenshotU3Ec__AnonStoreyE_t8_275 * L_3 = V_0;
		String_t* L_4 = ____body;
		NullCheck(L_3);
		L_3->____body_1 = L_4;
		U3CSendMailWithScreenshotU3Ec__AnonStoreyE_t8_275 * L_5 = V_0;
		bool L_6 = ____isHTMLBody;
		NullCheck(L_5);
		L_5->____isHTMLBody_2 = L_6;
		U3CSendMailWithScreenshotU3Ec__AnonStoreyE_t8_275 * L_7 = V_0;
		StringU5BU5D_t1_238* L_8 = ____recipients;
		NullCheck(L_7);
		L_7->____recipients_3 = L_8;
		U3CSendMailWithScreenshotU3Ec__AnonStoreyE_t8_275 * L_9 = V_0;
		SharingCompletion_t8_271 * L_10 = ____onCompletion;
		NullCheck(L_9);
		L_9->____onCompletion_4 = L_10;
		U3CSendMailWithScreenshotU3Ec__AnonStoreyE_t8_275 * L_11 = V_0;
		NullCheck(L_11);
		L_11->___U3CU3Ef__this_5 = __this;
		U3CSendMailWithScreenshotU3Ec__AnonStoreyE_t8_275 * L_12 = V_0;
		IntPtr_t L_13 = { (void*)U3CSendMailWithScreenshotU3Ec__AnonStoreyE_U3CU3Em__F_m8_1579_MethodInfo_var };
		Action_1_t1_1902 * L_14 = (Action_1_t1_1902 *)il2cpp_codegen_object_new (Action_1_t1_1902_il2cpp_TypeInfo_var);
		Action_1__ctor_m1_15066(L_14, L_12, L_13, /*hidden argument*/Action_1__ctor_m1_15066_MethodInfo_var);
		Object_t * L_15 = TextureExtensions_TakeScreenshot_m8_239(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m6_672(__this, L_15, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Sharing::SendMailWithTexture(System.String,System.String,System.Boolean,UnityEngine.Texture2D,System.String[],VoxelBusters.NativePlugins.Sharing/SharingCompletion)
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5475;
extern Il2CppCodeGenString* _stringLiteral5476;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5477;
extern "C" void Sharing_SendMailWithTexture_m8_1598 (Sharing_t8_273 * __this, String_t* ____subject, String_t* ____body, bool ____isHTMLBody, Texture2D_t6_33 * ____texture, StringU5BU5D_t1_238* ____recipients, SharingCompletion_t8_271 * ____onCompletion, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		_stringLiteral5475 = il2cpp_codegen_string_literal_from_index(5475);
		_stringLiteral5476 = il2cpp_codegen_string_literal_from_index(5476);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5477 = il2cpp_codegen_string_literal_from_index(5477);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	String_t* V_1 = {0};
	String_t* V_2 = {0};
	{
		V_0 = (ByteU5BU5D_t1_109*)NULL;
		V_1 = (String_t*)NULL;
		V_2 = (String_t*)NULL;
		Texture2D_t6_33 * L_0 = ____texture;
		bool L_1 = Object_op_Inequality_m6_722(NULL /*static, unused*/, L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002c;
		}
	}
	{
		Texture2D_t6_33 * L_2 = ____texture;
		NullCheck(L_2);
		ByteU5BU5D_t1_109* L_3 = Texture2D_EncodeToPNG_m6_172(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		V_2 = _stringLiteral5475;
		V_1 = _stringLiteral5476;
		goto IL_003c;
	}

IL_002c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_LogWarning_m8_1008(NULL /*static, unused*/, _stringLiteral5368, _stringLiteral5477, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
	}

IL_003c:
	{
		String_t* L_4 = ____subject;
		String_t* L_5 = ____body;
		bool L_6 = ____isHTMLBody;
		ByteU5BU5D_t1_109* L_7 = V_0;
		String_t* L_8 = V_1;
		String_t* L_9 = V_2;
		StringU5BU5D_t1_238* L_10 = ____recipients;
		SharingCompletion_t8_271 * L_11 = ____onCompletion;
		VirtActionInvoker8< String_t*, String_t*, bool, ByteU5BU5D_t1_109*, String_t*, String_t*, StringU5BU5D_t1_238*, SharingCompletion_t8_271 * >::Invoke(10 /* System.Void VoxelBusters.NativePlugins.Sharing::SendMail(System.String,System.String,System.Boolean,System.Byte[],System.String,System.String,System.String[],VoxelBusters.NativePlugins.Sharing/SharingCompletion) */, __this, L_4, L_5, L_6, L_7, L_8, L_9, L_10, L_11);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Sharing::SendMailWithAttachment(System.String,System.String,System.Boolean,System.String,System.String,System.String[],VoxelBusters.NativePlugins.Sharing/SharingCompletion)
extern TypeInfo* Path_t1_437_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5478;
extern "C" void Sharing_SendMailWithAttachment_m8_1599 (Sharing_t8_273 * __this, String_t* ____subject, String_t* ____body, bool ____isHTMLBody, String_t* ____attachmentPath, String_t* ____mimeType, StringU5BU5D_t1_238* ____recipients, SharingCompletion_t8_271 * ____onCompletion, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Path_t1_437_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(131);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5478 = il2cpp_codegen_string_literal_from_index(5478);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	String_t* V_1 = {0};
	{
		V_0 = (ByteU5BU5D_t1_109*)NULL;
		V_1 = (String_t*)NULL;
		String_t* L_0 = ____attachmentPath;
		bool L_1 = File_Exists_m1_4857(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0025;
		}
	}
	{
		String_t* L_2 = ____attachmentPath;
		ByteU5BU5D_t1_109* L_3 = FileOperations_ReadAllBytes_m8_253(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		String_t* L_4 = ____attachmentPath;
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1_437_il2cpp_TypeInfo_var);
		String_t* L_5 = Path_GetFileName_m1_5127(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		goto IL_003c;
	}

IL_0025:
	{
		String_t* L_6 = ____attachmentPath;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m1_559(NULL /*static, unused*/, _stringLiteral5478, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_LogWarning_m8_1008(NULL /*static, unused*/, _stringLiteral5368, L_7, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
	}

IL_003c:
	{
		String_t* L_8 = ____subject;
		String_t* L_9 = ____body;
		bool L_10 = ____isHTMLBody;
		ByteU5BU5D_t1_109* L_11 = V_0;
		String_t* L_12 = ____mimeType;
		String_t* L_13 = V_1;
		StringU5BU5D_t1_238* L_14 = ____recipients;
		SharingCompletion_t8_271 * L_15 = ____onCompletion;
		VirtActionInvoker8< String_t*, String_t*, bool, ByteU5BU5D_t1_109*, String_t*, String_t*, StringU5BU5D_t1_238*, SharingCompletion_t8_271 * >::Invoke(10 /* System.Void VoxelBusters.NativePlugins.Sharing::SendMail(System.String,System.String,System.Boolean,System.Byte[],System.String,System.String,System.String[],VoxelBusters.NativePlugins.Sharing/SharingCompletion) */, __this, L_8, L_9, L_10, L_11, L_12, L_13, L_14, L_15);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Sharing::SendMail(System.String,System.String,System.Boolean,System.Byte[],System.String,System.String,System.String[],VoxelBusters.NativePlugins.Sharing/SharingCompletion)
extern TypeInfo* MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var;
extern "C" void Sharing_SendMail_m8_1600 (Sharing_t8_273 * __this, String_t* ____subject, String_t* ____body, bool ____isHTMLBody, ByteU5BU5D_t1_109* ____attachmentByteArray, String_t* ____mimeType, String_t* ____attachmentFileNameWithExtn, StringU5BU5D_t1_238* ____recipients, SharingCompletion_t8_271 * ____onCompletion, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1959);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var);
		MonoBehaviourExtensions_PauseUnity_m8_210(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		SharingCompletion_t8_271 * L_0 = ____onCompletion;
		__this->___OnSharingFinished_3 = L_0;
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean VoxelBusters.NativePlugins.Sharing::IsMailServiceAvailable() */, __this);
		if (L_1)
		{
			goto IL_0026;
		}
	}
	{
		String_t* L_2 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String VoxelBusters.NativePlugins.Sharing::MailShareFailedResponse() */, __this);
		Sharing_MailShareFinished_m8_1590(__this, L_2, /*hidden argument*/NULL);
		return;
	}

IL_0026:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Sharing::MessagingShareFinished(System.String)
extern TypeInfo* MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var;
extern TypeInfo* eShareResult_t8_286_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5479;
extern "C" void Sharing_MessagingShareFinished_m8_1601 (Sharing_t8_273 * __this, String_t* ____reasonString, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1959);
		eShareResult_t8_286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2151);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5479 = il2cpp_codegen_string_literal_from_index(5479);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var);
		MonoBehaviourExtensions_ResumeUnity_m8_211(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		String_t* L_0 = ____reasonString;
		VirtActionInvoker2< String_t*, int32_t* >::Invoke(11 /* System.Void VoxelBusters.NativePlugins.Sharing::ParseMessagingShareFinishedData(System.String,VoxelBusters.NativePlugins.eShareResult&) */, __this, L_0, (&V_0));
		int32_t L_1 = V_0;
		int32_t L_2 = L_1;
		Object_t * L_3 = Box(eShareResult_t8_286_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m1_556(NULL /*static, unused*/, _stringLiteral5479, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_Log_m8_1007(NULL /*static, unused*/, _stringLiteral5368, L_4, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		SharingCompletion_t8_271 * L_5 = (__this->___OnSharingFinished_3);
		if (!L_5)
		{
			goto IL_0041;
		}
	}
	{
		SharingCompletion_t8_271 * L_6 = (__this->___OnSharingFinished_3);
		int32_t L_7 = V_0;
		NullCheck(L_6);
		SharingCompletion_Invoke_m8_1569(L_6, L_7, /*hidden argument*/NULL);
	}

IL_0041:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Sharing::ParseMessagingShareFinishedData(System.String,VoxelBusters.NativePlugins.eShareResult&)
extern "C" void Sharing_ParseMessagingShareFinishedData_m8_1602 (Sharing_t8_273 * __this, String_t* ____resultString, int32_t* ____shareResult, const MethodInfo* method)
{
	{
		int32_t* L_0 = ____shareResult;
		*((int32_t*)(L_0)) = (int32_t)0;
		return;
	}
}
// System.String VoxelBusters.NativePlugins.Sharing::MessagingShareFailedResponse()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" String_t* Sharing_MessagingShareFailedResponse_m8_1603 (Sharing_t8_273 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		return L_0;
	}
}
// System.Boolean VoxelBusters.NativePlugins.Sharing::IsMessagingServiceAvailable()
extern TypeInfo* Boolean_t1_20_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5480;
extern "C" bool Sharing_IsMessagingServiceAvailable_m8_1604 (Sharing_t8_273 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Boolean_t1_20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(54);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5480 = il2cpp_codegen_string_literal_from_index(5480);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		V_0 = 0;
		bool L_0 = V_0;
		bool L_1 = L_0;
		Object_t * L_2 = Box(Boolean_t1_20_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m1_556(NULL /*static, unused*/, _stringLiteral5480, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_Log_m8_1007(NULL /*static, unused*/, _stringLiteral5368, L_3, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		bool L_4 = V_0;
		return L_4;
	}
}
// System.Void VoxelBusters.NativePlugins.Sharing::ShowMessageShareComposer(VoxelBusters.NativePlugins.MessageShareComposer)
extern "C" void Sharing_ShowMessageShareComposer_m8_1605 (Sharing_t8_273 * __this, MessageShareComposer_t8_281 * ____composer, const MethodInfo* method)
{
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(13 /* System.Boolean VoxelBusters.NativePlugins.Sharing::IsMessagingServiceAvailable() */, __this);
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(12 /* System.String VoxelBusters.NativePlugins.Sharing::MessagingShareFailedResponse() */, __this);
		Sharing_MessagingShareFinished_m8_1601(__this, L_1, /*hidden argument*/NULL);
		return;
	}

IL_0018:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Sharing::SendTextMessage(System.String,System.String[],VoxelBusters.NativePlugins.Sharing/SharingCompletion)
extern TypeInfo* MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var;
extern "C" void Sharing_SendTextMessage_m8_1606 (Sharing_t8_273 * __this, String_t* ____body, StringU5BU5D_t1_238* ____recipients, SharingCompletion_t8_271 * ____onCompletion, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1959);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var);
		MonoBehaviourExtensions_PauseUnity_m8_210(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		SharingCompletion_t8_271 * L_0 = ____onCompletion;
		__this->___OnSharingFinished_3 = L_0;
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(13 /* System.Boolean VoxelBusters.NativePlugins.Sharing::IsMessagingServiceAvailable() */, __this);
		if (L_1)
		{
			goto IL_0025;
		}
	}
	{
		String_t* L_2 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(12 /* System.String VoxelBusters.NativePlugins.Sharing::MessagingShareFailedResponse() */, __this);
		Sharing_MessagingShareFinished_m8_1601(__this, L_2, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Sharing::FBShareFinished(System.String)
extern TypeInfo* MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var;
extern TypeInfo* eShareResult_t8_286_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5481;
extern "C" void Sharing_FBShareFinished_m8_1607 (Sharing_t8_273 * __this, String_t* ____reasonString, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1959);
		eShareResult_t8_286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2151);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5481 = il2cpp_codegen_string_literal_from_index(5481);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var);
		MonoBehaviourExtensions_ResumeUnity_m8_211(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		String_t* L_0 = ____reasonString;
		VirtActionInvoker2< String_t*, int32_t* >::Invoke(16 /* System.Void VoxelBusters.NativePlugins.Sharing::ParseFBShareFinishedResponse(System.String,VoxelBusters.NativePlugins.eShareResult&) */, __this, L_0, (&V_0));
		int32_t L_1 = V_0;
		int32_t L_2 = L_1;
		Object_t * L_3 = Box(eShareResult_t8_286_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m1_556(NULL /*static, unused*/, _stringLiteral5481, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_Log_m8_1007(NULL /*static, unused*/, _stringLiteral5368, L_4, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		SharingCompletion_t8_271 * L_5 = (__this->___OnSharingFinished_3);
		if (!L_5)
		{
			goto IL_0041;
		}
	}
	{
		SharingCompletion_t8_271 * L_6 = (__this->___OnSharingFinished_3);
		int32_t L_7 = V_0;
		NullCheck(L_6);
		SharingCompletion_Invoke_m8_1569(L_6, L_7, /*hidden argument*/NULL);
	}

IL_0041:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Sharing::TwitterShareFinished(System.String)
extern TypeInfo* MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var;
extern TypeInfo* eShareResult_t8_286_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5482;
extern "C" void Sharing_TwitterShareFinished_m8_1608 (Sharing_t8_273 * __this, String_t* ____reasonString, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1959);
		eShareResult_t8_286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2151);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5482 = il2cpp_codegen_string_literal_from_index(5482);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var);
		MonoBehaviourExtensions_ResumeUnity_m8_211(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		String_t* L_0 = ____reasonString;
		VirtActionInvoker2< String_t*, int32_t* >::Invoke(17 /* System.Void VoxelBusters.NativePlugins.Sharing::ParseTwitterShareFinishedResponse(System.String,VoxelBusters.NativePlugins.eShareResult&) */, __this, L_0, (&V_0));
		int32_t L_1 = V_0;
		int32_t L_2 = L_1;
		Object_t * L_3 = Box(eShareResult_t8_286_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m1_556(NULL /*static, unused*/, _stringLiteral5482, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_Log_m8_1007(NULL /*static, unused*/, _stringLiteral5368, L_4, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		SharingCompletion_t8_271 * L_5 = (__this->___OnSharingFinished_3);
		if (!L_5)
		{
			goto IL_0041;
		}
	}
	{
		SharingCompletion_t8_271 * L_6 = (__this->___OnSharingFinished_3);
		int32_t L_7 = V_0;
		NullCheck(L_6);
		SharingCompletion_Invoke_m8_1569(L_6, L_7, /*hidden argument*/NULL);
	}

IL_0041:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Sharing::ParseFBShareFinishedResponse(System.String,VoxelBusters.NativePlugins.eShareResult&)
extern "C" void Sharing_ParseFBShareFinishedResponse_m8_1609 (Sharing_t8_273 * __this, String_t* ____resultString, int32_t* ____shareResult, const MethodInfo* method)
{
	{
		int32_t* L_0 = ____shareResult;
		*((int32_t*)(L_0)) = (int32_t)0;
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Sharing::ParseTwitterShareFinishedResponse(System.String,VoxelBusters.NativePlugins.eShareResult&)
extern "C" void Sharing_ParseTwitterShareFinishedResponse_m8_1610 (Sharing_t8_273 * __this, String_t* ____resultString, int32_t* ____shareResult, const MethodInfo* method)
{
	{
		int32_t* L_0 = ____shareResult;
		*((int32_t*)(L_0)) = (int32_t)0;
		return;
	}
}
// System.String VoxelBusters.NativePlugins.Sharing::FBShareFailedResponse()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" String_t* Sharing_FBShareFailedResponse_m8_1611 (Sharing_t8_273 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		return L_0;
	}
}
// System.String VoxelBusters.NativePlugins.Sharing::TwitterShareFailedResponse()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" String_t* Sharing_TwitterShareFailedResponse_m8_1612 (Sharing_t8_273 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		return L_0;
	}
}
// System.Boolean VoxelBusters.NativePlugins.Sharing::IsFBShareServiceAvailable()
extern TypeInfo* Boolean_t1_20_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5483;
extern "C" bool Sharing_IsFBShareServiceAvailable_m8_1613 (Sharing_t8_273 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Boolean_t1_20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(54);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5483 = il2cpp_codegen_string_literal_from_index(5483);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		V_0 = 0;
		bool L_0 = V_0;
		bool L_1 = L_0;
		Object_t * L_2 = Box(Boolean_t1_20_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m1_556(NULL /*static, unused*/, _stringLiteral5483, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_Log_m8_1007(NULL /*static, unused*/, _stringLiteral5368, L_3, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		bool L_4 = V_0;
		return L_4;
	}
}
// System.Boolean VoxelBusters.NativePlugins.Sharing::IsTwitterShareServiceAvailable()
extern TypeInfo* Boolean_t1_20_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5484;
extern "C" bool Sharing_IsTwitterShareServiceAvailable_m8_1614 (Sharing_t8_273 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Boolean_t1_20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(54);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5484 = il2cpp_codegen_string_literal_from_index(5484);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		V_0 = 0;
		bool L_0 = V_0;
		bool L_1 = L_0;
		Object_t * L_2 = Box(Boolean_t1_20_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m1_556(NULL /*static, unused*/, _stringLiteral5484, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_Log_m8_1007(NULL /*static, unused*/, _stringLiteral5368, L_3, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		bool L_4 = V_0;
		return L_4;
	}
}
// System.Void VoxelBusters.NativePlugins.Sharing::ShowFBShareComposer(VoxelBusters.NativePlugins.FBShareComposer)
extern "C" void Sharing_ShowFBShareComposer_m8_1615 (Sharing_t8_273 * __this, FBShareComposer_t8_282 * ____composer, const MethodInfo* method)
{
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(20 /* System.Boolean VoxelBusters.NativePlugins.Sharing::IsFBShareServiceAvailable() */, __this);
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String VoxelBusters.NativePlugins.Sharing::FBShareFailedResponse() */, __this);
		Sharing_FBShareFinished_m8_1607(__this, L_1, /*hidden argument*/NULL);
		return;
	}

IL_0018:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Sharing::ShowTwitterShareComposer(VoxelBusters.NativePlugins.TwitterShareComposer)
extern "C" void Sharing_ShowTwitterShareComposer_m8_1616 (Sharing_t8_273 * __this, TwitterShareComposer_t8_284 * ____composer, const MethodInfo* method)
{
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(21 /* System.Boolean VoxelBusters.NativePlugins.Sharing::IsTwitterShareServiceAvailable() */, __this);
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String VoxelBusters.NativePlugins.Sharing::TwitterShareFailedResponse() */, __this);
		Sharing_TwitterShareFinished_m8_1608(__this, L_1, /*hidden argument*/NULL);
		return;
	}

IL_0018:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Sharing::WhatsAppShareFinished(System.String)
extern TypeInfo* MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var;
extern TypeInfo* eShareResult_t8_286_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5485;
extern "C" void Sharing_WhatsAppShareFinished_m8_1617 (Sharing_t8_273 * __this, String_t* ____reasonString, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1959);
		eShareResult_t8_286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2151);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5485 = il2cpp_codegen_string_literal_from_index(5485);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var);
		MonoBehaviourExtensions_ResumeUnity_m8_211(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		String_t* L_0 = ____reasonString;
		VirtActionInvoker2< String_t*, int32_t* >::Invoke(24 /* System.Void VoxelBusters.NativePlugins.Sharing::ParseWhatsAppShareFinishedData(System.String,VoxelBusters.NativePlugins.eShareResult&) */, __this, L_0, (&V_0));
		int32_t L_1 = V_0;
		int32_t L_2 = L_1;
		Object_t * L_3 = Box(eShareResult_t8_286_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m1_556(NULL /*static, unused*/, _stringLiteral5485, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_Log_m8_1007(NULL /*static, unused*/, _stringLiteral5368, L_4, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		SharingCompletion_t8_271 * L_5 = (__this->___OnSharingFinished_3);
		if (!L_5)
		{
			goto IL_0041;
		}
	}
	{
		SharingCompletion_t8_271 * L_6 = (__this->___OnSharingFinished_3);
		int32_t L_7 = V_0;
		NullCheck(L_6);
		SharingCompletion_Invoke_m8_1569(L_6, L_7, /*hidden argument*/NULL);
	}

IL_0041:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Sharing::ParseWhatsAppShareFinishedData(System.String,VoxelBusters.NativePlugins.eShareResult&)
extern "C" void Sharing_ParseWhatsAppShareFinishedData_m8_1618 (Sharing_t8_273 * __this, String_t* ____resultString, int32_t* ____shareResult, const MethodInfo* method)
{
	{
		int32_t* L_0 = ____shareResult;
		*((int32_t*)(L_0)) = (int32_t)0;
		return;
	}
}
// System.String VoxelBusters.NativePlugins.Sharing::WhatsAppShareFailedResponse()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" String_t* Sharing_WhatsAppShareFailedResponse_m8_1619 (Sharing_t8_273 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		return L_0;
	}
}
// System.Boolean VoxelBusters.NativePlugins.Sharing::IsWhatsAppServiceAvailable()
extern TypeInfo* Boolean_t1_20_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5486;
extern "C" bool Sharing_IsWhatsAppServiceAvailable_m8_1620 (Sharing_t8_273 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Boolean_t1_20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(54);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5486 = il2cpp_codegen_string_literal_from_index(5486);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		V_0 = 0;
		bool L_0 = V_0;
		bool L_1 = L_0;
		Object_t * L_2 = Box(Boolean_t1_20_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m1_556(NULL /*static, unused*/, _stringLiteral5486, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_Log_m8_1007(NULL /*static, unused*/, _stringLiteral5368, L_3, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		bool L_4 = V_0;
		return L_4;
	}
}
// System.Void VoxelBusters.NativePlugins.Sharing::ShowWhatsAppShareComposer(VoxelBusters.NativePlugins.WhatsAppShareComposer)
extern "C" void Sharing_ShowWhatsAppShareComposer_m8_1621 (Sharing_t8_273 * __this, WhatsAppShareComposer_t8_285 * ____composer, const MethodInfo* method)
{
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(26 /* System.Boolean VoxelBusters.NativePlugins.Sharing::IsWhatsAppServiceAvailable() */, __this);
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(25 /* System.String VoxelBusters.NativePlugins.Sharing::WhatsAppShareFailedResponse() */, __this);
		Sharing_WhatsAppShareFinished_m8_1617(__this, L_1, /*hidden argument*/NULL);
		return;
	}

IL_0018:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Sharing::ShareTextMessageOnWhatsApp(System.String,VoxelBusters.NativePlugins.Sharing/SharingCompletion)
extern TypeInfo* MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5487;
extern "C" void Sharing_ShareTextMessageOnWhatsApp_m8_1622 (Sharing_t8_273 * __this, String_t* ____message, SharingCompletion_t8_271 * ____onCompletion, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1959);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5487 = il2cpp_codegen_string_literal_from_index(5487);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var);
		MonoBehaviourExtensions_PauseUnity_m8_210(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		SharingCompletion_t8_271 * L_0 = ____onCompletion;
		__this->___OnSharingFinished_3 = L_0;
		String_t* L_1 = ____message;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_IsNullOrEmpty_m1_521(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0023;
		}
	}
	{
		bool L_3 = (bool)VirtFuncInvoker0< bool >::Invoke(26 /* System.Boolean VoxelBusters.NativePlugins.Sharing::IsWhatsAppServiceAvailable() */, __this);
		if (L_3)
		{
			goto IL_0040;
		}
	}

IL_0023:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_Log_m8_1007(NULL /*static, unused*/, _stringLiteral5368, _stringLiteral5487, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		String_t* L_4 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(25 /* System.String VoxelBusters.NativePlugins.Sharing::WhatsAppShareFailedResponse() */, __this);
		Sharing_WhatsAppShareFinished_m8_1617(__this, L_4, /*hidden argument*/NULL);
		return;
	}

IL_0040:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Sharing::ShareScreenshotOnWhatsApp(VoxelBusters.NativePlugins.Sharing/SharingCompletion)
extern TypeInfo* U3CShareScreenshotOnWhatsAppU3Ec__AnonStoreyF_t8_276_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t1_1902_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CShareScreenshotOnWhatsAppU3Ec__AnonStoreyF_U3CU3Em__10_m8_1581_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m1_15066_MethodInfo_var;
extern "C" void Sharing_ShareScreenshotOnWhatsApp_m8_1623 (Sharing_t8_273 * __this, SharingCompletion_t8_271 * ____onCompletion, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CShareScreenshotOnWhatsAppU3Ec__AnonStoreyF_t8_276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2160);
		Action_1_t1_1902_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2138);
		U3CShareScreenshotOnWhatsAppU3Ec__AnonStoreyF_U3CU3Em__10_m8_1581_MethodInfo_var = il2cpp_codegen_method_info_from_index(591);
		Action_1__ctor_m1_15066_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484232);
		s_Il2CppMethodIntialized = true;
	}
	U3CShareScreenshotOnWhatsAppU3Ec__AnonStoreyF_t8_276 * V_0 = {0};
	{
		U3CShareScreenshotOnWhatsAppU3Ec__AnonStoreyF_t8_276 * L_0 = (U3CShareScreenshotOnWhatsAppU3Ec__AnonStoreyF_t8_276 *)il2cpp_codegen_object_new (U3CShareScreenshotOnWhatsAppU3Ec__AnonStoreyF_t8_276_il2cpp_TypeInfo_var);
		U3CShareScreenshotOnWhatsAppU3Ec__AnonStoreyF__ctor_m8_1580(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CShareScreenshotOnWhatsAppU3Ec__AnonStoreyF_t8_276 * L_1 = V_0;
		SharingCompletion_t8_271 * L_2 = ____onCompletion;
		NullCheck(L_1);
		L_1->____onCompletion_0 = L_2;
		U3CShareScreenshotOnWhatsAppU3Ec__AnonStoreyF_t8_276 * L_3 = V_0;
		NullCheck(L_3);
		L_3->___U3CU3Ef__this_1 = __this;
		U3CShareScreenshotOnWhatsAppU3Ec__AnonStoreyF_t8_276 * L_4 = V_0;
		IntPtr_t L_5 = { (void*)U3CShareScreenshotOnWhatsAppU3Ec__AnonStoreyF_U3CU3Em__10_m8_1581_MethodInfo_var };
		Action_1_t1_1902 * L_6 = (Action_1_t1_1902 *)il2cpp_codegen_object_new (Action_1_t1_1902_il2cpp_TypeInfo_var);
		Action_1__ctor_m1_15066(L_6, L_4, L_5, /*hidden argument*/Action_1__ctor_m1_15066_MethodInfo_var);
		Object_t * L_7 = TextureExtensions_TakeScreenshot_m8_239(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m6_672(__this, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Sharing::ShareImageOnWhatsApp(System.String,VoxelBusters.NativePlugins.Sharing/SharingCompletion)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5488;
extern "C" void Sharing_ShareImageOnWhatsApp_m8_1624 (Sharing_t8_273 * __this, String_t* ____imagePath, SharingCompletion_t8_271 * ____onCompletion, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5488 = il2cpp_codegen_string_literal_from_index(5488);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	{
		String_t* L_0 = ____imagePath;
		bool L_1 = File_Exists_m1_4857(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_002a;
		}
	}
	{
		String_t* L_2 = ____imagePath;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m1_559(NULL /*static, unused*/, _stringLiteral5488, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_LogError_m8_1009(NULL /*static, unused*/, _stringLiteral5368, L_3, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		SharingCompletion_t8_271 * L_4 = ____onCompletion;
		VirtActionInvoker2< ByteU5BU5D_t1_109*, SharingCompletion_t8_271 * >::Invoke(29 /* System.Void VoxelBusters.NativePlugins.Sharing::ShareImageOnWhatsApp(System.Byte[],VoxelBusters.NativePlugins.Sharing/SharingCompletion) */, __this, (ByteU5BU5D_t1_109*)(ByteU5BU5D_t1_109*)NULL, L_4);
		return;
	}

IL_002a:
	{
		String_t* L_5 = ____imagePath;
		ByteU5BU5D_t1_109* L_6 = FileOperations_ReadAllBytes_m8_253(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		ByteU5BU5D_t1_109* L_7 = V_0;
		SharingCompletion_t8_271 * L_8 = ____onCompletion;
		VirtActionInvoker2< ByteU5BU5D_t1_109*, SharingCompletion_t8_271 * >::Invoke(29 /* System.Void VoxelBusters.NativePlugins.Sharing::ShareImageOnWhatsApp(System.Byte[],VoxelBusters.NativePlugins.Sharing/SharingCompletion) */, __this, L_7, L_8);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Sharing::ShareImageOnWhatsApp(UnityEngine.Texture2D,VoxelBusters.NativePlugins.Sharing/SharingCompletion)
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5489;
extern "C" void Sharing_ShareImageOnWhatsApp_m8_1625 (Sharing_t8_273 * __this, Texture2D_t6_33 * ____texture, SharingCompletion_t8_271 * ____onCompletion, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5489 = il2cpp_codegen_string_literal_from_index(5489);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	{
		Texture2D_t6_33 * L_0 = ____texture;
		bool L_1 = Object_op_Equality_m6_721(NULL /*static, unused*/, L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0025;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_LogError_m8_1009(NULL /*static, unused*/, _stringLiteral5368, _stringLiteral5489, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		SharingCompletion_t8_271 * L_2 = ____onCompletion;
		VirtActionInvoker2< ByteU5BU5D_t1_109*, SharingCompletion_t8_271 * >::Invoke(29 /* System.Void VoxelBusters.NativePlugins.Sharing::ShareImageOnWhatsApp(System.Byte[],VoxelBusters.NativePlugins.Sharing/SharingCompletion) */, __this, (ByteU5BU5D_t1_109*)(ByteU5BU5D_t1_109*)NULL, L_2);
		return;
	}

IL_0025:
	{
		Texture2D_t6_33 * L_3 = ____texture;
		NullCheck(L_3);
		ByteU5BU5D_t1_109* L_4 = Texture2D_EncodeToPNG_m6_172(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		ByteU5BU5D_t1_109* L_5 = V_0;
		SharingCompletion_t8_271 * L_6 = ____onCompletion;
		VirtActionInvoker2< ByteU5BU5D_t1_109*, SharingCompletion_t8_271 * >::Invoke(29 /* System.Void VoxelBusters.NativePlugins.Sharing::ShareImageOnWhatsApp(System.Byte[],VoxelBusters.NativePlugins.Sharing/SharingCompletion) */, __this, L_5, L_6);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Sharing::ShareImageOnWhatsApp(System.Byte[],VoxelBusters.NativePlugins.Sharing/SharingCompletion)
extern TypeInfo* MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var;
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5490;
extern "C" void Sharing_ShareImageOnWhatsApp_m8_1626 (Sharing_t8_273 * __this, ByteU5BU5D_t1_109* ____imageByteArray, SharingCompletion_t8_271 * ____onCompletion, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1959);
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5490 = il2cpp_codegen_string_literal_from_index(5490);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var);
		MonoBehaviourExtensions_PauseUnity_m8_210(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		SharingCompletion_t8_271 * L_0 = ____onCompletion;
		__this->___OnSharingFinished_3 = L_0;
		ByteU5BU5D_t1_109* L_1 = ____imageByteArray;
		if (!L_1)
		{
			goto IL_001e;
		}
	}
	{
		bool L_2 = (bool)VirtFuncInvoker0< bool >::Invoke(26 /* System.Boolean VoxelBusters.NativePlugins.Sharing::IsWhatsAppServiceAvailable() */, __this);
		if (L_2)
		{
			goto IL_003b;
		}
	}

IL_001e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_LogError_m8_1009(NULL /*static, unused*/, _stringLiteral5368, _stringLiteral5490, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		String_t* L_3 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(25 /* System.String VoxelBusters.NativePlugins.Sharing::WhatsAppShareFailedResponse() */, __this);
		Sharing_WhatsAppShareFinished_m8_1617(__this, L_3, /*hidden argument*/NULL);
		return;
	}

IL_003b:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Sharing::ShowView(VoxelBusters.NativePlugins.IShareView,VoxelBusters.NativePlugins.Sharing/SharingCompletion)
extern "C" void Sharing_ShowView_m8_1627 (Sharing_t8_273 * __this, Object_t * ____shareView, SharingCompletion_t8_271 * ____onCompletion, const MethodInfo* method)
{
	{
		Object_t * L_0 = ____shareView;
		SharingCompletion_t8_271 * L_1 = ____onCompletion;
		Object_t * L_2 = Sharing_ShowViewCoroutine_m8_1628(__this, L_0, L_1, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m6_672(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator VoxelBusters.NativePlugins.Sharing::ShowViewCoroutine(VoxelBusters.NativePlugins.IShareView,VoxelBusters.NativePlugins.Sharing/SharingCompletion)
extern TypeInfo* U3CShowViewCoroutineU3Ec__Iterator6_t8_272_il2cpp_TypeInfo_var;
extern "C" Object_t * Sharing_ShowViewCoroutine_m8_1628 (Sharing_t8_273 * __this, Object_t * ____shareView, SharingCompletion_t8_271 * ____onCompletion, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CShowViewCoroutineU3Ec__Iterator6_t8_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2161);
		s_Il2CppMethodIntialized = true;
	}
	U3CShowViewCoroutineU3Ec__Iterator6_t8_272 * V_0 = {0};
	{
		U3CShowViewCoroutineU3Ec__Iterator6_t8_272 * L_0 = (U3CShowViewCoroutineU3Ec__Iterator6_t8_272 *)il2cpp_codegen_object_new (U3CShowViewCoroutineU3Ec__Iterator6_t8_272_il2cpp_TypeInfo_var);
		U3CShowViewCoroutineU3Ec__Iterator6__ctor_m8_1572(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CShowViewCoroutineU3Ec__Iterator6_t8_272 * L_1 = V_0;
		Object_t * L_2 = ____shareView;
		NullCheck(L_1);
		L_1->____shareView_0 = L_2;
		U3CShowViewCoroutineU3Ec__Iterator6_t8_272 * L_3 = V_0;
		SharingCompletion_t8_271 * L_4 = ____onCompletion;
		NullCheck(L_3);
		L_3->____onCompletion_1 = L_4;
		U3CShowViewCoroutineU3Ec__Iterator6_t8_272 * L_5 = V_0;
		Object_t * L_6 = ____shareView;
		NullCheck(L_5);
		L_5->___U3CU24U3E_shareView_4 = L_6;
		U3CShowViewCoroutineU3Ec__Iterator6_t8_272 * L_7 = V_0;
		SharingCompletion_t8_271 * L_8 = ____onCompletion;
		NullCheck(L_7);
		L_7->___U3CU24U3E_onCompletion_5 = L_8;
		U3CShowViewCoroutineU3Ec__Iterator6_t8_272 * L_9 = V_0;
		NullCheck(L_9);
		L_9->___U3CU3Ef__this_6 = __this;
		U3CShowViewCoroutineU3Ec__Iterator6_t8_272 * L_10 = V_0;
		return L_10;
	}
}
// System.Void VoxelBusters.NativePlugins.Sharing::ShowShareSheet(VoxelBusters.NativePlugins.ShareSheet)
extern "C" void Sharing_ShowShareSheet_m8_1629 (Sharing_t8_273 * __this, ShareSheet_t8_289 * ____shareSheet, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Sharing::ShareTextMessageOnSocialNetwork(System.String,VoxelBusters.NativePlugins.Sharing/SharingCompletion)
extern "C" void Sharing_ShareTextMessageOnSocialNetwork_m8_1630 (Sharing_t8_273 * __this, String_t* ____message, SharingCompletion_t8_271 * ____onCompletion, const MethodInfo* method)
{
	{
		String_t* L_0 = ____message;
		eShareOptionsU5BU5D_t8_186* L_1 = (__this->___m_socialNetworkExcludedList_4);
		SharingCompletion_t8_271 * L_2 = ____onCompletion;
		Sharing_ShareMessage_m8_1636(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Sharing::ShareURLOnSocialNetwork(System.String,System.String,VoxelBusters.NativePlugins.Sharing/SharingCompletion)
extern "C" void Sharing_ShareURLOnSocialNetwork_m8_1631 (Sharing_t8_273 * __this, String_t* ____message, String_t* ____URLString, SharingCompletion_t8_271 * ____onCompletion, const MethodInfo* method)
{
	{
		String_t* L_0 = ____message;
		String_t* L_1 = ____URLString;
		eShareOptionsU5BU5D_t8_186* L_2 = (__this->___m_socialNetworkExcludedList_4);
		SharingCompletion_t8_271 * L_3 = ____onCompletion;
		Sharing_ShareURL_m8_1637(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Sharing::ShareScreenShotOnSocialNetwork(System.String,VoxelBusters.NativePlugins.Sharing/SharingCompletion)
extern "C" void Sharing_ShareScreenShotOnSocialNetwork_m8_1632 (Sharing_t8_273 * __this, String_t* ____message, SharingCompletion_t8_271 * ____onCompletion, const MethodInfo* method)
{
	{
		String_t* L_0 = ____message;
		eShareOptionsU5BU5D_t8_186* L_1 = (__this->___m_socialNetworkExcludedList_4);
		SharingCompletion_t8_271 * L_2 = ____onCompletion;
		Sharing_ShareScreenShot_m8_1638(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Sharing::ShareImageOnSocialNetwork(System.String,UnityEngine.Texture2D,VoxelBusters.NativePlugins.Sharing/SharingCompletion)
extern "C" void Sharing_ShareImageOnSocialNetwork_m8_1633 (Sharing_t8_273 * __this, String_t* ____message, Texture2D_t6_33 * ____texture, SharingCompletion_t8_271 * ____onCompletion, const MethodInfo* method)
{
	{
		String_t* L_0 = ____message;
		Texture2D_t6_33 * L_1 = ____texture;
		eShareOptionsU5BU5D_t8_186* L_2 = (__this->___m_socialNetworkExcludedList_4);
		SharingCompletion_t8_271 * L_3 = ____onCompletion;
		Sharing_ShareImage_m8_1639(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Sharing::ShareImageOnSocialNetwork(System.String,System.String,VoxelBusters.NativePlugins.Sharing/SharingCompletion)
extern "C" void Sharing_ShareImageOnSocialNetwork_m8_1634 (Sharing_t8_273 * __this, String_t* ____message, String_t* ____imagePath, SharingCompletion_t8_271 * ____onCompletion, const MethodInfo* method)
{
	{
		String_t* L_0 = ____message;
		String_t* L_1 = ____imagePath;
		eShareOptionsU5BU5D_t8_186* L_2 = (__this->___m_socialNetworkExcludedList_4);
		SharingCompletion_t8_271 * L_3 = ____onCompletion;
		Sharing_ShareImageAtPath_m8_1640(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Sharing::ShareImageOnSocialNetwork(System.String,System.Byte[],VoxelBusters.NativePlugins.Sharing/SharingCompletion)
extern "C" void Sharing_ShareImageOnSocialNetwork_m8_1635 (Sharing_t8_273 * __this, String_t* ____message, ByteU5BU5D_t1_109* ____imageByteArray, SharingCompletion_t8_271 * ____onCompletion, const MethodInfo* method)
{
	{
		String_t* L_0 = ____message;
		ByteU5BU5D_t1_109* L_1 = ____imageByteArray;
		eShareOptionsU5BU5D_t8_186* L_2 = (__this->___m_socialNetworkExcludedList_4);
		SharingCompletion_t8_271 * L_3 = ____onCompletion;
		Sharing_Share_m8_1641(__this, L_0, (String_t*)NULL, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Sharing::ShareMessage(System.String,VoxelBusters.NativePlugins.eShareOptions[],VoxelBusters.NativePlugins.Sharing/SharingCompletion)
extern "C" void Sharing_ShareMessage_m8_1636 (Sharing_t8_273 * __this, String_t* ____message, eShareOptionsU5BU5D_t8_186* ____excludedOptions, SharingCompletion_t8_271 * ____onCompletion, const MethodInfo* method)
{
	{
		String_t* L_0 = ____message;
		eShareOptionsU5BU5D_t8_186* L_1 = ____excludedOptions;
		SharingCompletion_t8_271 * L_2 = ____onCompletion;
		Sharing_Share_m8_1641(__this, L_0, (String_t*)NULL, (ByteU5BU5D_t1_109*)(ByteU5BU5D_t1_109*)NULL, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Sharing::ShareURL(System.String,System.String,VoxelBusters.NativePlugins.eShareOptions[],VoxelBusters.NativePlugins.Sharing/SharingCompletion)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5491;
extern "C" void Sharing_ShareURL_m8_1637 (Sharing_t8_273 * __this, String_t* ____message, String_t* ____URLString, eShareOptionsU5BU5D_t8_186* ____excludedOptions, SharingCompletion_t8_271 * ____onCompletion, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5491 = il2cpp_codegen_string_literal_from_index(5491);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ____URLString;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1_521(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_LogWarning_m8_1008(NULL /*static, unused*/, _stringLiteral5368, _stringLiteral5491, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
	}

IL_001b:
	{
		String_t* L_2 = ____message;
		String_t* L_3 = ____URLString;
		eShareOptionsU5BU5D_t8_186* L_4 = ____excludedOptions;
		SharingCompletion_t8_271 * L_5 = ____onCompletion;
		Sharing_Share_m8_1641(__this, L_2, L_3, (ByteU5BU5D_t1_109*)(ByteU5BU5D_t1_109*)NULL, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Sharing::ShareScreenShot(System.String,VoxelBusters.NativePlugins.eShareOptions[],VoxelBusters.NativePlugins.Sharing/SharingCompletion)
extern TypeInfo* U3CShareScreenShotU3Ec__AnonStorey10_t8_277_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t1_1902_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CShareScreenShotU3Ec__AnonStorey10_U3CU3Em__11_m8_1583_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m1_15066_MethodInfo_var;
extern "C" void Sharing_ShareScreenShot_m8_1638 (Sharing_t8_273 * __this, String_t* ____message, eShareOptionsU5BU5D_t8_186* ____excludedOptions, SharingCompletion_t8_271 * ____onCompletion, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CShareScreenShotU3Ec__AnonStorey10_t8_277_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2162);
		Action_1_t1_1902_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2138);
		U3CShareScreenShotU3Ec__AnonStorey10_U3CU3Em__11_m8_1583_MethodInfo_var = il2cpp_codegen_method_info_from_index(592);
		Action_1__ctor_m1_15066_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484232);
		s_Il2CppMethodIntialized = true;
	}
	U3CShareScreenShotU3Ec__AnonStorey10_t8_277 * V_0 = {0};
	{
		U3CShareScreenShotU3Ec__AnonStorey10_t8_277 * L_0 = (U3CShareScreenShotU3Ec__AnonStorey10_t8_277 *)il2cpp_codegen_object_new (U3CShareScreenShotU3Ec__AnonStorey10_t8_277_il2cpp_TypeInfo_var);
		U3CShareScreenShotU3Ec__AnonStorey10__ctor_m8_1582(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CShareScreenShotU3Ec__AnonStorey10_t8_277 * L_1 = V_0;
		String_t* L_2 = ____message;
		NullCheck(L_1);
		L_1->____message_0 = L_2;
		U3CShareScreenShotU3Ec__AnonStorey10_t8_277 * L_3 = V_0;
		eShareOptionsU5BU5D_t8_186* L_4 = ____excludedOptions;
		NullCheck(L_3);
		L_3->____excludedOptions_1 = L_4;
		U3CShareScreenShotU3Ec__AnonStorey10_t8_277 * L_5 = V_0;
		SharingCompletion_t8_271 * L_6 = ____onCompletion;
		NullCheck(L_5);
		L_5->____onCompletion_2 = L_6;
		U3CShareScreenShotU3Ec__AnonStorey10_t8_277 * L_7 = V_0;
		NullCheck(L_7);
		L_7->___U3CU3Ef__this_3 = __this;
		U3CShareScreenShotU3Ec__AnonStorey10_t8_277 * L_8 = V_0;
		IntPtr_t L_9 = { (void*)U3CShareScreenShotU3Ec__AnonStorey10_U3CU3Em__11_m8_1583_MethodInfo_var };
		Action_1_t1_1902 * L_10 = (Action_1_t1_1902 *)il2cpp_codegen_object_new (Action_1_t1_1902_il2cpp_TypeInfo_var);
		Action_1__ctor_m1_15066(L_10, L_8, L_9, /*hidden argument*/Action_1__ctor_m1_15066_MethodInfo_var);
		Object_t * L_11 = TextureExtensions_TakeScreenshot_m8_239(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m6_672(__this, L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Sharing::ShareImage(System.String,UnityEngine.Texture2D,VoxelBusters.NativePlugins.eShareOptions[],VoxelBusters.NativePlugins.Sharing/SharingCompletion)
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5492;
extern "C" void Sharing_ShareImage_m8_1639 (Sharing_t8_273 * __this, String_t* ____message, Texture2D_t6_33 * ____texture, eShareOptionsU5BU5D_t8_186* ____excludedOptions, SharingCompletion_t8_271 * ____onCompletion, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5492 = il2cpp_codegen_string_literal_from_index(5492);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	{
		V_0 = (ByteU5BU5D_t1_109*)NULL;
		Texture2D_t6_33 * L_0 = ____texture;
		bool L_1 = Object_op_Inequality_m6_722(NULL /*static, unused*/, L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		Texture2D_t6_33 * L_2 = ____texture;
		NullCheck(L_2);
		ByteU5BU5D_t1_109* L_3 = Texture2D_EncodeToPNG_m6_172(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_002a;
	}

IL_001a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_LogWarning_m8_1008(NULL /*static, unused*/, _stringLiteral5368, _stringLiteral5492, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
	}

IL_002a:
	{
		String_t* L_4 = ____message;
		ByteU5BU5D_t1_109* L_5 = V_0;
		eShareOptionsU5BU5D_t8_186* L_6 = ____excludedOptions;
		SharingCompletion_t8_271 * L_7 = ____onCompletion;
		Sharing_Share_m8_1641(__this, L_4, (String_t*)NULL, L_5, L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Sharing::ShareImageAtPath(System.String,System.String,VoxelBusters.NativePlugins.eShareOptions[],VoxelBusters.NativePlugins.Sharing/SharingCompletion)
extern TypeInfo* U3CShareImageAtPathU3Ec__AnonStorey11_t8_278_il2cpp_TypeInfo_var;
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern TypeInfo* DownloadTexture_t8_162_il2cpp_TypeInfo_var;
extern TypeInfo* Completion_t8_161_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CShareImageAtPathU3Ec__AnonStorey11_U3CU3Em__12_m8_1585_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5493;
extern "C" void Sharing_ShareImageAtPath_m8_1640 (Sharing_t8_273 * __this, String_t* ____message, String_t* ____imagePath, eShareOptionsU5BU5D_t8_186* ____excludedOptions, SharingCompletion_t8_271 * ____onCompletion, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CShareImageAtPathU3Ec__AnonStorey11_t8_278_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2163);
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		DownloadTexture_t8_162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2087);
		Completion_t8_161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2088);
		U3CShareImageAtPathU3Ec__AnonStorey11_U3CU3Em__12_m8_1585_MethodInfo_var = il2cpp_codegen_method_info_from_index(593);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5493 = il2cpp_codegen_string_literal_from_index(5493);
		s_Il2CppMethodIntialized = true;
	}
	URL_t8_156  V_0 = {0};
	DownloadTexture_t8_162 * V_1 = {0};
	U3CShareImageAtPathU3Ec__AnonStorey11_t8_278 * V_2 = {0};
	{
		U3CShareImageAtPathU3Ec__AnonStorey11_t8_278 * L_0 = (U3CShareImageAtPathU3Ec__AnonStorey11_t8_278 *)il2cpp_codegen_object_new (U3CShareImageAtPathU3Ec__AnonStorey11_t8_278_il2cpp_TypeInfo_var);
		U3CShareImageAtPathU3Ec__AnonStorey11__ctor_m8_1584(L_0, /*hidden argument*/NULL);
		V_2 = L_0;
		U3CShareImageAtPathU3Ec__AnonStorey11_t8_278 * L_1 = V_2;
		String_t* L_2 = ____message;
		NullCheck(L_1);
		L_1->____message_0 = L_2;
		U3CShareImageAtPathU3Ec__AnonStorey11_t8_278 * L_3 = V_2;
		eShareOptionsU5BU5D_t8_186* L_4 = ____excludedOptions;
		NullCheck(L_3);
		L_3->____excludedOptions_1 = L_4;
		U3CShareImageAtPathU3Ec__AnonStorey11_t8_278 * L_5 = V_2;
		SharingCompletion_t8_271 * L_6 = ____onCompletion;
		NullCheck(L_5);
		L_5->____onCompletion_2 = L_6;
		U3CShareImageAtPathU3Ec__AnonStorey11_t8_278 * L_7 = V_2;
		NullCheck(L_7);
		L_7->___U3CU3Ef__this_3 = __this;
		String_t* L_8 = ____imagePath;
		bool L_9 = File_Exists_m1_4857(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_0058;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_LogWarning_m8_1008(NULL /*static, unused*/, _stringLiteral5368, _stringLiteral5493, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		U3CShareImageAtPathU3Ec__AnonStorey11_t8_278 * L_10 = V_2;
		NullCheck(L_10);
		String_t* L_11 = (L_10->____message_0);
		U3CShareImageAtPathU3Ec__AnonStorey11_t8_278 * L_12 = V_2;
		NullCheck(L_12);
		eShareOptionsU5BU5D_t8_186* L_13 = (L_12->____excludedOptions_1);
		U3CShareImageAtPathU3Ec__AnonStorey11_t8_278 * L_14 = V_2;
		NullCheck(L_14);
		SharingCompletion_t8_271 * L_15 = (L_14->____onCompletion_2);
		Sharing_ShareImage_m8_1639(__this, L_11, (Texture2D_t6_33 *)NULL, L_13, L_15, /*hidden argument*/NULL);
		return;
	}

IL_0058:
	{
		String_t* L_16 = ____imagePath;
		URL_t8_156  L_17 = URL_FileURLWithPath_m8_961(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		V_0 = L_17;
		URL_t8_156  L_18 = V_0;
		DownloadTexture_t8_162 * L_19 = (DownloadTexture_t8_162 *)il2cpp_codegen_object_new (DownloadTexture_t8_162_il2cpp_TypeInfo_var);
		DownloadTexture__ctor_m8_929(L_19, L_18, 1, 0, /*hidden argument*/NULL);
		V_1 = L_19;
		DownloadTexture_t8_162 * L_20 = V_1;
		U3CShareImageAtPathU3Ec__AnonStorey11_t8_278 * L_21 = V_2;
		IntPtr_t L_22 = { (void*)U3CShareImageAtPathU3Ec__AnonStorey11_U3CU3Em__12_m8_1585_MethodInfo_var };
		Completion_t8_161 * L_23 = (Completion_t8_161 *)il2cpp_codegen_object_new (Completion_t8_161_il2cpp_TypeInfo_var);
		Completion__ctor_m8_925(L_23, L_21, L_22, /*hidden argument*/NULL);
		NullCheck(L_20);
		DownloadTexture_set_OnCompletion_m8_935(L_20, L_23, /*hidden argument*/NULL);
		DownloadTexture_t8_162 * L_24 = V_1;
		NullCheck(L_24);
		Request_StartRequest_m8_900(L_24, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Sharing::Share(System.String,System.String,System.Byte[],VoxelBusters.NativePlugins.eShareOptions[],VoxelBusters.NativePlugins.Sharing/SharingCompletion)
extern "C" void Sharing_Share_m8_1641 (Sharing_t8_273 * __this, String_t* ____message, String_t* ____URLString, ByteU5BU5D_t1_109* ____imageByteArray, eShareOptionsU5BU5D_t8_186* ____excludedOptions, SharingCompletion_t8_271 * ____onCompletion, const MethodInfo* method)
{
	String_t* V_0 = {0};
	{
		V_0 = (String_t*)NULL;
		eShareOptionsU5BU5D_t8_186* L_0 = ____excludedOptions;
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		eShareOptionsU5BU5D_t8_186* L_1 = ____excludedOptions;
		String_t* L_2 = JSONParserExtensions_ToJSON_m8_270(NULL /*static, unused*/, (Object_t *)(Object_t *)L_1, /*hidden argument*/NULL);
		V_0 = L_2;
	}

IL_0011:
	{
		String_t* L_3 = ____message;
		String_t* L_4 = ____URLString;
		ByteU5BU5D_t1_109* L_5 = ____imageByteArray;
		String_t* L_6 = V_0;
		SharingCompletion_t8_271 * L_7 = ____onCompletion;
		VirtActionInvoker5< String_t*, String_t*, ByteU5BU5D_t1_109*, String_t*, SharingCompletion_t8_271 * >::Invoke(31 /* System.Void VoxelBusters.NativePlugins.Sharing::Share(System.String,System.String,System.Byte[],System.String,VoxelBusters.NativePlugins.Sharing/SharingCompletion) */, __this, L_3, L_4, L_5, L_6, L_7);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Sharing::Share(System.String,System.String,System.Byte[],System.String,VoxelBusters.NativePlugins.Sharing/SharingCompletion)
extern TypeInfo* MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var;
extern "C" void Sharing_Share_m8_1642 (Sharing_t8_273 * __this, String_t* ____message, String_t* ____URLString, ByteU5BU5D_t1_109* ____imageByteArray, String_t* ____excludedOptionsJsonString, SharingCompletion_t8_271 * ____onCompletion, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1959);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var);
		MonoBehaviourExtensions_PauseUnity_m8_210(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		SharingCompletion_t8_271 * L_0 = ____onCompletion;
		__this->___OnSharingFinished_3 = L_0;
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.MailShareComposer::.ctor()
extern "C" void MailShareComposer__ctor_m8_1643 (MailShareComposer_t8_279 * __this, const MethodInfo* method)
{
	{
		ShareImageUtility__ctor_m8_1707(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String VoxelBusters.NativePlugins.MailShareComposer::get_Subject()
extern "C" String_t* MailShareComposer_get_Subject_m8_1644 (MailShareComposer_t8_279 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CSubjectU3Ek__BackingField_3);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.MailShareComposer::set_Subject(System.String)
extern "C" void MailShareComposer_set_Subject_m8_1645 (MailShareComposer_t8_279 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CSubjectU3Ek__BackingField_3 = L_0;
		return;
	}
}
// System.String VoxelBusters.NativePlugins.MailShareComposer::get_Body()
extern "C" String_t* MailShareComposer_get_Body_m8_1646 (MailShareComposer_t8_279 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CBodyU3Ek__BackingField_4);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.MailShareComposer::set_Body(System.String)
extern "C" void MailShareComposer_set_Body_m8_1647 (MailShareComposer_t8_279 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CBodyU3Ek__BackingField_4 = L_0;
		return;
	}
}
// System.Boolean VoxelBusters.NativePlugins.MailShareComposer::get_IsHTMLBody()
extern "C" bool MailShareComposer_get_IsHTMLBody_m8_1648 (MailShareComposer_t8_279 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___U3CIsHTMLBodyU3Ek__BackingField_5);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.MailShareComposer::set_IsHTMLBody(System.Boolean)
extern "C" void MailShareComposer_set_IsHTMLBody_m8_1649 (MailShareComposer_t8_279 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___U3CIsHTMLBodyU3Ek__BackingField_5 = L_0;
		return;
	}
}
// System.String[] VoxelBusters.NativePlugins.MailShareComposer::get_ToRecipients()
extern "C" StringU5BU5D_t1_238* MailShareComposer_get_ToRecipients_m8_1650 (MailShareComposer_t8_279 * __this, const MethodInfo* method)
{
	{
		StringU5BU5D_t1_238* L_0 = (__this->___U3CToRecipientsU3Ek__BackingField_6);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.MailShareComposer::set_ToRecipients(System.String[])
extern "C" void MailShareComposer_set_ToRecipients_m8_1651 (MailShareComposer_t8_279 * __this, StringU5BU5D_t1_238* ___value, const MethodInfo* method)
{
	{
		StringU5BU5D_t1_238* L_0 = ___value;
		__this->___U3CToRecipientsU3Ek__BackingField_6 = L_0;
		return;
	}
}
// System.String[] VoxelBusters.NativePlugins.MailShareComposer::get_CCRecipients()
extern "C" StringU5BU5D_t1_238* MailShareComposer_get_CCRecipients_m8_1652 (MailShareComposer_t8_279 * __this, const MethodInfo* method)
{
	{
		StringU5BU5D_t1_238* L_0 = (__this->___U3CCCRecipientsU3Ek__BackingField_7);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.MailShareComposer::set_CCRecipients(System.String[])
extern "C" void MailShareComposer_set_CCRecipients_m8_1653 (MailShareComposer_t8_279 * __this, StringU5BU5D_t1_238* ___value, const MethodInfo* method)
{
	{
		StringU5BU5D_t1_238* L_0 = ___value;
		__this->___U3CCCRecipientsU3Ek__BackingField_7 = L_0;
		return;
	}
}
// System.String[] VoxelBusters.NativePlugins.MailShareComposer::get_BCCRecipients()
extern "C" StringU5BU5D_t1_238* MailShareComposer_get_BCCRecipients_m8_1654 (MailShareComposer_t8_279 * __this, const MethodInfo* method)
{
	{
		StringU5BU5D_t1_238* L_0 = (__this->___U3CBCCRecipientsU3Ek__BackingField_8);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.MailShareComposer::set_BCCRecipients(System.String[])
extern "C" void MailShareComposer_set_BCCRecipients_m8_1655 (MailShareComposer_t8_279 * __this, StringU5BU5D_t1_238* ___value, const MethodInfo* method)
{
	{
		StringU5BU5D_t1_238* L_0 = ___value;
		__this->___U3CBCCRecipientsU3Ek__BackingField_8 = L_0;
		return;
	}
}
// System.Byte[] VoxelBusters.NativePlugins.MailShareComposer::get_AttachmentData()
extern "C" ByteU5BU5D_t1_109* MailShareComposer_get_AttachmentData_m8_1656 (MailShareComposer_t8_279 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t1_109* L_0 = (__this->___U3CAttachmentDataU3Ek__BackingField_9);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.MailShareComposer::set_AttachmentData(System.Byte[])
extern "C" void MailShareComposer_set_AttachmentData_m8_1657 (MailShareComposer_t8_279 * __this, ByteU5BU5D_t1_109* ___value, const MethodInfo* method)
{
	{
		ByteU5BU5D_t1_109* L_0 = ___value;
		__this->___U3CAttachmentDataU3Ek__BackingField_9 = L_0;
		return;
	}
}
// System.String VoxelBusters.NativePlugins.MailShareComposer::get_AttachmentFileName()
extern "C" String_t* MailShareComposer_get_AttachmentFileName_m8_1658 (MailShareComposer_t8_279 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CAttachmentFileNameU3Ek__BackingField_10);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.MailShareComposer::set_AttachmentFileName(System.String)
extern "C" void MailShareComposer_set_AttachmentFileName_m8_1659 (MailShareComposer_t8_279 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CAttachmentFileNameU3Ek__BackingField_10 = L_0;
		return;
	}
}
// System.String VoxelBusters.NativePlugins.MailShareComposer::get_MimeType()
extern "C" String_t* MailShareComposer_get_MimeType_m8_1660 (MailShareComposer_t8_279 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CMimeTypeU3Ek__BackingField_11);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.MailShareComposer::set_MimeType(System.String)
extern "C" void MailShareComposer_set_MimeType_m8_1661 (MailShareComposer_t8_279 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CMimeTypeU3Ek__BackingField_11 = L_0;
		return;
	}
}
// System.Boolean VoxelBusters.NativePlugins.MailShareComposer::get_IsReadyToShowView()
extern "C" bool MailShareComposer_get_IsReadyToShowView_m8_1662 (MailShareComposer_t8_279 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ShareImageUtility_get_ImageAsyncDownloadInProgress_m8_1708(__this, /*hidden argument*/NULL);
		return ((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void VoxelBusters.NativePlugins.MailShareComposer::AttachImage(UnityEngine.Texture2D)
extern Il2CppCodeGenString* _stringLiteral5495;
extern Il2CppCodeGenString* _stringLiteral5476;
extern "C" void MailShareComposer_AttachImage_m8_1663 (MailShareComposer_t8_279 * __this, Texture2D_t6_33 * ____texture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral5495 = il2cpp_codegen_string_literal_from_index(5495);
		_stringLiteral5476 = il2cpp_codegen_string_literal_from_index(5476);
		s_Il2CppMethodIntialized = true;
	}
	{
		Texture2D_t6_33 * L_0 = ____texture;
		bool L_1 = Object_op_Inequality_m6_722(NULL /*static, unused*/, L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0033;
		}
	}
	{
		Texture2D_t6_33 * L_2 = ____texture;
		NullCheck(L_2);
		ByteU5BU5D_t1_109* L_3 = Texture2D_EncodeToPNG_m6_172(L_2, /*hidden argument*/NULL);
		MailShareComposer_set_AttachmentData_m8_1657(__this, L_3, /*hidden argument*/NULL);
		MailShareComposer_set_AttachmentFileName_m8_1659(__this, _stringLiteral5495, /*hidden argument*/NULL);
		MailShareComposer_set_MimeType_m8_1661(__this, _stringLiteral5476, /*hidden argument*/NULL);
		goto IL_0048;
	}

IL_0033:
	{
		MailShareComposer_set_AttachmentData_m8_1657(__this, (ByteU5BU5D_t1_109*)(ByteU5BU5D_t1_109*)NULL, /*hidden argument*/NULL);
		MailShareComposer_set_AttachmentFileName_m8_1659(__this, (String_t*)NULL, /*hidden argument*/NULL);
		MailShareComposer_set_MimeType_m8_1661(__this, (String_t*)NULL, /*hidden argument*/NULL);
	}

IL_0048:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.MailShareComposer::AddAttachmentAtPath(System.String,System.String)
extern TypeInfo* Path_t1_437_il2cpp_TypeInfo_var;
extern "C" void MailShareComposer_AddAttachmentAtPath_m8_1664 (MailShareComposer_t8_279 * __this, String_t* ____attachmentPath, String_t* ____MIMEType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Path_t1_437_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(131);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	String_t* V_1 = {0};
	{
		V_0 = (ByteU5BU5D_t1_109*)NULL;
		V_1 = (String_t*)NULL;
		String_t* L_0 = ____attachmentPath;
		bool L_1 = FileOperations_Exists_m8_252(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		String_t* L_2 = ____attachmentPath;
		ByteU5BU5D_t1_109* L_3 = FileOperations_ReadAllBytes_m8_253(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		String_t* L_4 = ____attachmentPath;
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1_437_il2cpp_TypeInfo_var);
		String_t* L_5 = Path_GetFileName_m1_5127(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
	}

IL_001d:
	{
		ByteU5BU5D_t1_109* L_6 = V_0;
		String_t* L_7 = V_1;
		String_t* L_8 = ____MIMEType;
		MailShareComposer_AddAttachment_m8_1665(__this, L_6, L_7, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.MailShareComposer::AddAttachment(System.Byte[],System.String,System.String)
extern "C" void MailShareComposer_AddAttachment_m8_1665 (MailShareComposer_t8_279 * __this, ByteU5BU5D_t1_109* ____attachmentData, String_t* ____attachmentFileName, String_t* ____MIMEType, const MethodInfo* method)
{
	{
		ByteU5BU5D_t1_109* L_0 = ____attachmentData;
		MailShareComposer_set_AttachmentData_m8_1657(__this, L_0, /*hidden argument*/NULL);
		String_t* L_1 = ____attachmentFileName;
		MailShareComposer_set_AttachmentFileName_m8_1659(__this, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ____MIMEType;
		MailShareComposer_set_MimeType_m8_1661(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.MessageShareComposer::.ctor()
extern "C" void MessageShareComposer__ctor_m8_1666 (MessageShareComposer_t8_281 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String VoxelBusters.NativePlugins.MessageShareComposer::get_Body()
extern "C" String_t* MessageShareComposer_get_Body_m8_1667 (MessageShareComposer_t8_281 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CBodyU3Ek__BackingField_0);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.MessageShareComposer::set_Body(System.String)
extern "C" void MessageShareComposer_set_Body_m8_1668 (MessageShareComposer_t8_281 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CBodyU3Ek__BackingField_0 = L_0;
		return;
	}
}
// System.String[] VoxelBusters.NativePlugins.MessageShareComposer::get_ToRecipients()
extern "C" StringU5BU5D_t1_238* MessageShareComposer_get_ToRecipients_m8_1669 (MessageShareComposer_t8_281 * __this, const MethodInfo* method)
{
	{
		StringU5BU5D_t1_238* L_0 = (__this->___U3CToRecipientsU3Ek__BackingField_1);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.MessageShareComposer::set_ToRecipients(System.String[])
extern "C" void MessageShareComposer_set_ToRecipients_m8_1670 (MessageShareComposer_t8_281 * __this, StringU5BU5D_t1_238* ___value, const MethodInfo* method)
{
	{
		StringU5BU5D_t1_238* L_0 = ___value;
		__this->___U3CToRecipientsU3Ek__BackingField_1 = L_0;
		return;
	}
}
// System.Boolean VoxelBusters.NativePlugins.MessageShareComposer::get_IsReadyToShowView()
extern "C" bool MessageShareComposer_get_IsReadyToShowView_m8_1671 (MessageShareComposer_t8_281 * __this, const MethodInfo* method)
{
	{
		return 1;
	}
}
// System.Void VoxelBusters.NativePlugins.FBShareComposer::.ctor()
extern "C" void FBShareComposer__ctor_m8_1672 (FBShareComposer_t8_282 * __this, const MethodInfo* method)
{
	{
		SocialShareComposerBase__ctor_m8_1674(__this, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.SocialShareComposerBase::.ctor()
extern "C" void SocialShareComposerBase__ctor_m8_1673 (SocialShareComposerBase_t8_283 * __this, const MethodInfo* method)
{
	{
		ShareImageUtility__ctor_m8_1707(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.SocialShareComposerBase::.ctor(VoxelBusters.NativePlugins.Internal.eSocialServiceType)
extern "C" void SocialShareComposerBase__ctor_m8_1674 (SocialShareComposerBase_t8_283 * __this, int32_t ____serviceType, const MethodInfo* method)
{
	{
		ShareImageUtility__ctor_m8_1707(__this, /*hidden argument*/NULL);
		int32_t L_0 = ____serviceType;
		SocialShareComposerBase_set_ServiceType_m8_1676(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// VoxelBusters.NativePlugins.Internal.eSocialServiceType VoxelBusters.NativePlugins.SocialShareComposerBase::get_ServiceType()
extern "C" int32_t SocialShareComposerBase_get_ServiceType_m8_1675 (SocialShareComposerBase_t8_283 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___U3CServiceTypeU3Ek__BackingField_3);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.SocialShareComposerBase::set_ServiceType(VoxelBusters.NativePlugins.Internal.eSocialServiceType)
extern "C" void SocialShareComposerBase_set_ServiceType_m8_1676 (SocialShareComposerBase_t8_283 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___U3CServiceTypeU3Ek__BackingField_3 = L_0;
		return;
	}
}
// System.String VoxelBusters.NativePlugins.SocialShareComposerBase::get_Text()
extern "C" String_t* SocialShareComposerBase_get_Text_m8_1677 (SocialShareComposerBase_t8_283 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CTextU3Ek__BackingField_4);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.SocialShareComposerBase::set_Text(System.String)
extern "C" void SocialShareComposerBase_set_Text_m8_1678 (SocialShareComposerBase_t8_283 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CTextU3Ek__BackingField_4 = L_0;
		return;
	}
}
// System.String VoxelBusters.NativePlugins.SocialShareComposerBase::get_URL()
extern "C" String_t* SocialShareComposerBase_get_URL_m8_1679 (SocialShareComposerBase_t8_283 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CURLU3Ek__BackingField_5);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.SocialShareComposerBase::set_URL(System.String)
extern "C" void SocialShareComposerBase_set_URL_m8_1680 (SocialShareComposerBase_t8_283 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CURLU3Ek__BackingField_5 = L_0;
		return;
	}
}
// System.Byte[] VoxelBusters.NativePlugins.SocialShareComposerBase::get_ImageData()
extern "C" ByteU5BU5D_t1_109* SocialShareComposerBase_get_ImageData_m8_1681 (SocialShareComposerBase_t8_283 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t1_109* L_0 = (__this->___U3CImageDataU3Ek__BackingField_6);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.SocialShareComposerBase::set_ImageData(System.Byte[])
extern "C" void SocialShareComposerBase_set_ImageData_m8_1682 (SocialShareComposerBase_t8_283 * __this, ByteU5BU5D_t1_109* ___value, const MethodInfo* method)
{
	{
		ByteU5BU5D_t1_109* L_0 = ___value;
		__this->___U3CImageDataU3Ek__BackingField_6 = L_0;
		return;
	}
}
// System.Boolean VoxelBusters.NativePlugins.SocialShareComposerBase::get_IsReadyToShowView()
extern "C" bool SocialShareComposerBase_get_IsReadyToShowView_m8_1683 (SocialShareComposerBase_t8_283 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ShareImageUtility_get_ImageAsyncDownloadInProgress_m8_1708(__this, /*hidden argument*/NULL);
		return ((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void VoxelBusters.NativePlugins.SocialShareComposerBase::AttachImage(UnityEngine.Texture2D)
extern "C" void SocialShareComposerBase_AttachImage_m8_1684 (SocialShareComposerBase_t8_283 * __this, Texture2D_t6_33 * ____texture, const MethodInfo* method)
{
	{
		Texture2D_t6_33 * L_0 = ____texture;
		bool L_1 = Object_op_Inequality_m6_722(NULL /*static, unused*/, L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Texture2D_t6_33 * L_2 = ____texture;
		NullCheck(L_2);
		ByteU5BU5D_t1_109* L_3 = Texture2D_EncodeToPNG_m6_172(L_2, /*hidden argument*/NULL);
		SocialShareComposerBase_set_ImageData_m8_1682(__this, L_3, /*hidden argument*/NULL);
		goto IL_0024;
	}

IL_001d:
	{
		SocialShareComposerBase_set_ImageData_m8_1682(__this, (ByteU5BU5D_t1_109*)(ByteU5BU5D_t1_109*)NULL, /*hidden argument*/NULL);
	}

IL_0024:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.TwitterShareComposer::.ctor()
extern "C" void TwitterShareComposer__ctor_m8_1685 (TwitterShareComposer_t8_284 * __this, const MethodInfo* method)
{
	{
		SocialShareComposerBase__ctor_m8_1674(__this, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.WhatsAppShareComposer::.ctor()
extern "C" void WhatsAppShareComposer__ctor_m8_1686 (WhatsAppShareComposer_t8_285 * __this, const MethodInfo* method)
{
	{
		ShareImageUtility__ctor_m8_1707(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String VoxelBusters.NativePlugins.WhatsAppShareComposer::get_Text()
extern "C" String_t* WhatsAppShareComposer_get_Text_m8_1687 (WhatsAppShareComposer_t8_285 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CTextU3Ek__BackingField_3);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.WhatsAppShareComposer::set_Text(System.String)
extern "C" void WhatsAppShareComposer_set_Text_m8_1688 (WhatsAppShareComposer_t8_285 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CTextU3Ek__BackingField_3 = L_0;
		return;
	}
}
// System.Byte[] VoxelBusters.NativePlugins.WhatsAppShareComposer::get_ImageData()
extern "C" ByteU5BU5D_t1_109* WhatsAppShareComposer_get_ImageData_m8_1689 (WhatsAppShareComposer_t8_285 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t1_109* L_0 = (__this->___U3CImageDataU3Ek__BackingField_4);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.WhatsAppShareComposer::set_ImageData(System.Byte[])
extern "C" void WhatsAppShareComposer_set_ImageData_m8_1690 (WhatsAppShareComposer_t8_285 * __this, ByteU5BU5D_t1_109* ___value, const MethodInfo* method)
{
	{
		ByteU5BU5D_t1_109* L_0 = ___value;
		__this->___U3CImageDataU3Ek__BackingField_4 = L_0;
		return;
	}
}
// System.Boolean VoxelBusters.NativePlugins.WhatsAppShareComposer::get_IsReadyToShowView()
extern "C" bool WhatsAppShareComposer_get_IsReadyToShowView_m8_1691 (WhatsAppShareComposer_t8_285 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ShareImageUtility_get_ImageAsyncDownloadInProgress_m8_1708(__this, /*hidden argument*/NULL);
		return ((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void VoxelBusters.NativePlugins.WhatsAppShareComposer::AttachImage(UnityEngine.Texture2D)
extern "C" void WhatsAppShareComposer_AttachImage_m8_1692 (WhatsAppShareComposer_t8_285 * __this, Texture2D_t6_33 * ____texture, const MethodInfo* method)
{
	{
		Texture2D_t6_33 * L_0 = ____texture;
		bool L_1 = Object_op_Inequality_m6_722(NULL /*static, unused*/, L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Texture2D_t6_33 * L_2 = ____texture;
		NullCheck(L_2);
		ByteU5BU5D_t1_109* L_3 = Texture2D_EncodeToPNG_m6_172(L_2, /*hidden argument*/NULL);
		WhatsAppShareComposer_set_ImageData_m8_1690(__this, L_3, /*hidden argument*/NULL);
		goto IL_0024;
	}

IL_001d:
	{
		WhatsAppShareComposer_set_ImageData_m8_1690(__this, (ByteU5BU5D_t1_109*)(ByteU5BU5D_t1_109*)NULL, /*hidden argument*/NULL);
	}

IL_0024:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.ShareSheet::.ctor()
extern "C" void ShareSheet__ctor_m8_1693 (ShareSheet_t8_289 * __this, const MethodInfo* method)
{
	{
		ShareImageUtility__ctor_m8_1707(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String VoxelBusters.NativePlugins.ShareSheet::get_Text()
extern "C" String_t* ShareSheet_get_Text_m8_1694 (ShareSheet_t8_289 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CTextU3Ek__BackingField_4);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.ShareSheet::set_Text(System.String)
extern "C" void ShareSheet_set_Text_m8_1695 (ShareSheet_t8_289 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CTextU3Ek__BackingField_4 = L_0;
		return;
	}
}
// System.String VoxelBusters.NativePlugins.ShareSheet::get_URL()
extern "C" String_t* ShareSheet_get_URL_m8_1696 (ShareSheet_t8_289 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CURLU3Ek__BackingField_5);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.ShareSheet::set_URL(System.String)
extern "C" void ShareSheet_set_URL_m8_1697 (ShareSheet_t8_289 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CURLU3Ek__BackingField_5 = L_0;
		return;
	}
}
// System.Byte[] VoxelBusters.NativePlugins.ShareSheet::get_ImageData()
extern "C" ByteU5BU5D_t1_109* ShareSheet_get_ImageData_m8_1698 (ShareSheet_t8_289 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t1_109* L_0 = (__this->___U3CImageDataU3Ek__BackingField_6);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.ShareSheet::set_ImageData(System.Byte[])
extern "C" void ShareSheet_set_ImageData_m8_1699 (ShareSheet_t8_289 * __this, ByteU5BU5D_t1_109* ___value, const MethodInfo* method)
{
	{
		ByteU5BU5D_t1_109* L_0 = ___value;
		__this->___U3CImageDataU3Ek__BackingField_6 = L_0;
		return;
	}
}
// VoxelBusters.NativePlugins.eShareOptions[] VoxelBusters.NativePlugins.ShareSheet::get_ExcludedShareOptions()
extern "C" eShareOptionsU5BU5D_t8_186* ShareSheet_get_ExcludedShareOptions_m8_1700 (ShareSheet_t8_289 * __this, const MethodInfo* method)
{
	{
		eShareOptionsU5BU5D_t8_186* L_0 = (__this->___m_excludedShareOptions_3);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.ShareSheet::set_ExcludedShareOptions(VoxelBusters.NativePlugins.eShareOptions[])
extern "C" void ShareSheet_set_ExcludedShareOptions_m8_1701 (ShareSheet_t8_289 * __this, eShareOptionsU5BU5D_t8_186* ___value, const MethodInfo* method)
{
	{
		eShareOptionsU5BU5D_t8_186* L_0 = ___value;
		__this->___m_excludedShareOptions_3 = L_0;
		return;
	}
}
// System.Boolean VoxelBusters.NativePlugins.ShareSheet::get_IsReadyToShowView()
extern "C" bool ShareSheet_get_IsReadyToShowView_m8_1702 (ShareSheet_t8_289 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ShareImageUtility_get_ImageAsyncDownloadInProgress_m8_1708(__this, /*hidden argument*/NULL);
		return ((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void VoxelBusters.NativePlugins.ShareSheet::AttachImage(UnityEngine.Texture2D)
extern "C" void ShareSheet_AttachImage_m8_1703 (ShareSheet_t8_289 * __this, Texture2D_t6_33 * ____texture, const MethodInfo* method)
{
	{
		Texture2D_t6_33 * L_0 = ____texture;
		bool L_1 = Object_op_Inequality_m6_722(NULL /*static, unused*/, L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Texture2D_t6_33 * L_2 = ____texture;
		NullCheck(L_2);
		ByteU5BU5D_t1_109* L_3 = Texture2D_EncodeToPNG_m6_172(L_2, /*hidden argument*/NULL);
		ShareSheet_set_ImageData_m8_1699(__this, L_3, /*hidden argument*/NULL);
		goto IL_0024;
	}

IL_001d:
	{
		ShareSheet_set_ImageData_m8_1699(__this, (ByteU5BU5D_t1_109*)(ByteU5BU5D_t1_109*)NULL, /*hidden argument*/NULL);
	}

IL_0024:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.SocialShareSheet::.ctor()
extern TypeInfo* eShareOptionsU5BU5D_t8_186_il2cpp_TypeInfo_var;
extern "C" void SocialShareSheet__ctor_m8_1704 (SocialShareSheet_t8_290 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		eShareOptionsU5BU5D_t8_186_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2107);
		s_Il2CppMethodIntialized = true;
	}
	{
		ShareSheet__ctor_m8_1693(__this, /*hidden argument*/NULL);
		eShareOptionsU5BU5D_t8_186* L_0 = ((eShareOptionsU5BU5D_t8_186*)SZArrayNew(eShareOptionsU5BU5D_t8_186_il2cpp_TypeInfo_var, 3));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_0, 0, sizeof(int32_t))) = (int32_t)5;
		eShareOptionsU5BU5D_t8_186* L_1 = L_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_1, 1, sizeof(int32_t))) = (int32_t)2;
		eShareOptionsU5BU5D_t8_186* L_2 = L_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 2);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_2, 2, sizeof(int32_t))) = (int32_t)1;
		ShareSheet_set_ExcludedShareOptions_m8_1701(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// VoxelBusters.NativePlugins.eShareOptions[] VoxelBusters.NativePlugins.SocialShareSheet::get_ExcludedShareOptions()
extern "C" eShareOptionsU5BU5D_t8_186* SocialShareSheet_get_ExcludedShareOptions_m8_1705 (SocialShareSheet_t8_290 * __this, const MethodInfo* method)
{
	{
		eShareOptionsU5BU5D_t8_186* L_0 = ShareSheet_get_ExcludedShareOptions_m8_1700(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.SocialShareSheet::set_ExcludedShareOptions(VoxelBusters.NativePlugins.eShareOptions[])
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5496;
extern "C" void SocialShareSheet_set_ExcludedShareOptions_m8_1706 (SocialShareSheet_t8_290 * __this, eShareOptionsU5BU5D_t8_186* ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5496 = il2cpp_codegen_string_literal_from_index(5496);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_LogWarning_m8_1008(NULL /*static, unused*/, _stringLiteral5368, _stringLiteral5496, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.ShareImageUtility::.ctor()
extern "C" void ShareImageUtility__ctor_m8_1707 (ShareImageUtility_t8_280 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean VoxelBusters.NativePlugins.Internal.ShareImageUtility::get_ImageAsyncDownloadInProgress()
extern "C" bool ShareImageUtility_get_ImageAsyncDownloadInProgress_m8_1708 (ShareImageUtility_t8_280 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___U3CImageAsyncDownloadInProgressU3Ek__BackingField_2);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.ShareImageUtility::set_ImageAsyncDownloadInProgress(System.Boolean)
extern "C" void ShareImageUtility_set_ImageAsyncDownloadInProgress_m8_1709 (ShareImageUtility_t8_280 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___U3CImageAsyncDownloadInProgressU3Ek__BackingField_2 = L_0;
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.ShareImageUtility::AttachScreenShot()
extern TypeInfo* Action_1_t1_1902_il2cpp_TypeInfo_var;
extern TypeInfo* SingletonPattern_1_t8_334_il2cpp_TypeInfo_var;
extern const MethodInfo* ShareImageUtility_U3CAttachScreenShotU3Em__13_m8_1713_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m1_15066_MethodInfo_var;
extern const MethodInfo* SingletonPattern_1_get_Instance_m8_2016_MethodInfo_var;
extern "C" void ShareImageUtility_AttachScreenShot_m8_1710 (ShareImageUtility_t8_280 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_1_t1_1902_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2138);
		SingletonPattern_1_t8_334_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2165);
		ShareImageUtility_U3CAttachScreenShotU3Em__13_m8_1713_MethodInfo_var = il2cpp_codegen_method_info_from_index(594);
		Action_1__ctor_m1_15066_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484232);
		SingletonPattern_1_get_Instance_m8_2016_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484243);
		s_Il2CppMethodIntialized = true;
	}
	{
		ShareImageUtility_StopAsyncRequests_m8_1712(__this, /*hidden argument*/NULL);
		ShareImageUtility_set_ImageAsyncDownloadInProgress_m8_1709(__this, 1, /*hidden argument*/NULL);
		IntPtr_t L_0 = { (void*)ShareImageUtility_U3CAttachScreenShotU3Em__13_m8_1713_MethodInfo_var };
		Action_1_t1_1902 * L_1 = (Action_1_t1_1902 *)il2cpp_codegen_object_new (Action_1_t1_1902_il2cpp_TypeInfo_var);
		Action_1__ctor_m1_15066(L_1, __this, L_0, /*hidden argument*/Action_1__ctor_m1_15066_MethodInfo_var);
		Object_t * L_2 = TextureExtensions_TakeScreenshot_m8_239(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		__this->___m_takeScreenShotCoroutine_1 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(SingletonPattern_1_t8_334_il2cpp_TypeInfo_var);
		NPBinding_t8_333 * L_3 = SingletonPattern_1_get_Instance_m8_2016(NULL /*static, unused*/, /*hidden argument*/SingletonPattern_1_get_Instance_m8_2016_MethodInfo_var);
		Object_t * L_4 = (__this->___m_takeScreenShotCoroutine_1);
		NullCheck(L_3);
		MonoBehaviour_StartCoroutine_m6_672(L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.ShareImageUtility::AttachImageAtPath(System.String)
extern TypeInfo* DownloadTexture_t8_162_il2cpp_TypeInfo_var;
extern TypeInfo* Completion_t8_161_il2cpp_TypeInfo_var;
extern const MethodInfo* ShareImageUtility_U3CAttachImageAtPathU3Em__14_m8_1714_MethodInfo_var;
extern "C" void ShareImageUtility_AttachImageAtPath_m8_1711 (ShareImageUtility_t8_280 * __this, String_t* ____imagePath, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DownloadTexture_t8_162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2087);
		Completion_t8_161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2088);
		ShareImageUtility_U3CAttachImageAtPathU3Em__14_m8_1714_MethodInfo_var = il2cpp_codegen_method_info_from_index(596);
		s_Il2CppMethodIntialized = true;
	}
	URL_t8_156  V_0 = {0};
	{
		ShareImageUtility_StopAsyncRequests_m8_1712(__this, /*hidden argument*/NULL);
		ShareImageUtility_set_ImageAsyncDownloadInProgress_m8_1709(__this, 1, /*hidden argument*/NULL);
		String_t* L_0 = ____imagePath;
		URL_t8_156  L_1 = URL_FileURLWithPath_m8_961(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		URL_t8_156  L_2 = V_0;
		DownloadTexture_t8_162 * L_3 = (DownloadTexture_t8_162 *)il2cpp_codegen_object_new (DownloadTexture_t8_162_il2cpp_TypeInfo_var);
		DownloadTexture__ctor_m8_929(L_3, L_2, 1, 0, /*hidden argument*/NULL);
		__this->___m_downloadTexture_0 = L_3;
		DownloadTexture_t8_162 * L_4 = (__this->___m_downloadTexture_0);
		IntPtr_t L_5 = { (void*)ShareImageUtility_U3CAttachImageAtPathU3Em__14_m8_1714_MethodInfo_var };
		Completion_t8_161 * L_6 = (Completion_t8_161 *)il2cpp_codegen_object_new (Completion_t8_161_il2cpp_TypeInfo_var);
		Completion__ctor_m8_925(L_6, __this, L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		DownloadTexture_set_OnCompletion_m8_935(L_4, L_6, /*hidden argument*/NULL);
		DownloadTexture_t8_162 * L_7 = (__this->___m_downloadTexture_0);
		NullCheck(L_7);
		Request_StartRequest_m8_900(L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.ShareImageUtility::StopAsyncRequests()
extern TypeInfo* SingletonPattern_1_t8_334_il2cpp_TypeInfo_var;
extern const MethodInfo* SingletonPattern_1_get_Instance_m8_2016_MethodInfo_var;
extern "C" void ShareImageUtility_StopAsyncRequests_m8_1712 (ShareImageUtility_t8_280 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SingletonPattern_1_t8_334_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2165);
		SingletonPattern_1_get_Instance_m8_2016_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484243);
		s_Il2CppMethodIntialized = true;
	}
	{
		DownloadTexture_t8_162 * L_0 = (__this->___m_downloadTexture_0);
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		DownloadTexture_t8_162 * L_1 = (__this->___m_downloadTexture_0);
		NullCheck(L_1);
		Request_Abort_m8_902(L_1, /*hidden argument*/NULL);
		__this->___m_downloadTexture_0 = (DownloadTexture_t8_162 *)NULL;
	}

IL_001d:
	{
		Object_t * L_2 = (__this->___m_takeScreenShotCoroutine_1);
		if (!L_2)
		{
			goto IL_003f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SingletonPattern_1_t8_334_il2cpp_TypeInfo_var);
		NPBinding_t8_333 * L_3 = SingletonPattern_1_get_Instance_m8_2016(NULL /*static, unused*/, /*hidden argument*/SingletonPattern_1_get_Instance_m8_2016_MethodInfo_var);
		Object_t * L_4 = (__this->___m_takeScreenShotCoroutine_1);
		NullCheck(L_3);
		MonoBehaviour_StopCoroutine_m6_674(L_3, L_4, /*hidden argument*/NULL);
		__this->___m_takeScreenShotCoroutine_1 = (Object_t *)NULL;
	}

IL_003f:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.ShareImageUtility::<AttachScreenShot>m__13(UnityEngine.Texture2D)
extern "C" void ShareImageUtility_U3CAttachScreenShotU3Em__13_m8_1713 (ShareImageUtility_t8_280 * __this, Texture2D_t6_33 * ____texture, const MethodInfo* method)
{
	{
		Texture2D_t6_33 * L_0 = ____texture;
		VirtActionInvoker1< Texture2D_t6_33 * >::Invoke(4 /* System.Void VoxelBusters.NativePlugins.Internal.ShareImageUtility::AttachImage(UnityEngine.Texture2D) */, __this, L_0);
		ShareImageUtility_set_ImageAsyncDownloadInProgress_m8_1709(__this, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.ShareImageUtility::<AttachImageAtPath>m__14(UnityEngine.Texture2D,System.String)
extern "C" void ShareImageUtility_U3CAttachImageAtPathU3Em__14_m8_1714 (ShareImageUtility_t8_280 * __this, Texture2D_t6_33 * ____texture, String_t* ____error, const MethodInfo* method)
{
	{
		Texture2D_t6_33 * L_0 = ____texture;
		VirtActionInvoker1< Texture2D_t6_33 * >::Invoke(4 /* System.Void VoxelBusters.NativePlugins.Internal.ShareImageUtility::AttachImage(UnityEngine.Texture2D) */, __this, L_0);
		ShareImageUtility_set_ImageAsyncDownloadInProgress_m8_1709(__this, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.SocialNetworkSettings::.ctor()
extern TypeInfo* TwitterSettings_t8_292_il2cpp_TypeInfo_var;
extern "C" void SocialNetworkSettings__ctor_m8_1715 (SocialNetworkSettings_t8_291 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TwitterSettings_t8_292_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2166);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		TwitterSettings_t8_292 * L_0 = (TwitterSettings_t8_292 *)il2cpp_codegen_object_new (TwitterSettings_t8_292_il2cpp_TypeInfo_var);
		TwitterSettings__ctor_m8_1743(L_0, /*hidden argument*/NULL);
		__this->___m_twitterSettings_0 = L_0;
		return;
	}
}
// VoxelBusters.NativePlugins.TwitterSettings VoxelBusters.NativePlugins.SocialNetworkSettings::get_TwitterSettings()
extern "C" TwitterSettings_t8_292 * SocialNetworkSettings_get_TwitterSettings_m8_1716 (SocialNetworkSettings_t8_291 * __this, const MethodInfo* method)
{
	{
		TwitterSettings_t8_292 * L_0 = (__this->___m_twitterSettings_0);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.AndroidTwitterSession::.ctor(System.Collections.IDictionary)
extern TypeInfo* IDictionary_t1_35_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5497;
extern Il2CppCodeGenString* _stringLiteral5498;
extern Il2CppCodeGenString* _stringLiteral5499;
extern Il2CppCodeGenString* _stringLiteral5500;
extern "C" void AndroidTwitterSession__ctor_m8_1717 (AndroidTwitterSession_t8_294 * __this, Object_t * ____sessionJsonDict, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionary_t1_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5497 = il2cpp_codegen_string_literal_from_index(5497);
		_stringLiteral5498 = il2cpp_codegen_string_literal_from_index(5498);
		_stringLiteral5499 = il2cpp_codegen_string_literal_from_index(5499);
		_stringLiteral5500 = il2cpp_codegen_string_literal_from_index(5500);
		s_Il2CppMethodIntialized = true;
	}
	{
		TwitterSession__ctor_m8_1718(__this, /*hidden argument*/NULL);
		Object_t * L_0 = ____sessionJsonDict;
		NullCheck(L_0);
		Object_t * L_1 = (Object_t *)InterfaceFuncInvoker1< Object_t *, Object_t * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_0, _stringLiteral5497);
		TwitterSession_set_AuthToken_m8_1720(__this, ((String_t*)IsInstSealed(L_1, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		Object_t * L_2 = ____sessionJsonDict;
		NullCheck(L_2);
		Object_t * L_3 = (Object_t *)InterfaceFuncInvoker1< Object_t *, Object_t * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_2, _stringLiteral5498);
		TwitterSession_set_AuthTokenSecret_m8_1722(__this, ((String_t*)IsInstSealed(L_3, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		Object_t * L_4 = ____sessionJsonDict;
		NullCheck(L_4);
		Object_t * L_5 = (Object_t *)InterfaceFuncInvoker1< Object_t *, Object_t * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_4, _stringLiteral5499);
		TwitterSession_set_UserName_m8_1724(__this, ((String_t*)IsInstSealed(L_5, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		Object_t * L_6 = ____sessionJsonDict;
		NullCheck(L_6);
		Object_t * L_7 = (Object_t *)InterfaceFuncInvoker1< Object_t *, Object_t * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_6, _stringLiteral5500);
		TwitterSession_set_UserID_m8_1726(__this, ((String_t*)IsInstSealed(L_7, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.TwitterSession::.ctor()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void TwitterSession__ctor_m8_1718 (TwitterSession_t8_295 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		TwitterSession_set_AuthToken_m8_1720(__this, L_0, /*hidden argument*/NULL);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		TwitterSession_set_AuthTokenSecret_m8_1722(__this, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		TwitterSession_set_UserName_m8_1724(__this, L_2, /*hidden argument*/NULL);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		TwitterSession_set_UserID_m8_1726(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.String VoxelBusters.NativePlugins.TwitterSession::get_AuthToken()
extern "C" String_t* TwitterSession_get_AuthToken_m8_1719 (TwitterSession_t8_295 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CAuthTokenU3Ek__BackingField_0);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.TwitterSession::set_AuthToken(System.String)
extern "C" void TwitterSession_set_AuthToken_m8_1720 (TwitterSession_t8_295 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CAuthTokenU3Ek__BackingField_0 = L_0;
		return;
	}
}
// System.String VoxelBusters.NativePlugins.TwitterSession::get_AuthTokenSecret()
extern "C" String_t* TwitterSession_get_AuthTokenSecret_m8_1721 (TwitterSession_t8_295 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CAuthTokenSecretU3Ek__BackingField_1);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.TwitterSession::set_AuthTokenSecret(System.String)
extern "C" void TwitterSession_set_AuthTokenSecret_m8_1722 (TwitterSession_t8_295 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CAuthTokenSecretU3Ek__BackingField_1 = L_0;
		return;
	}
}
// System.String VoxelBusters.NativePlugins.TwitterSession::get_UserName()
extern "C" String_t* TwitterSession_get_UserName_m8_1723 (TwitterSession_t8_295 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CUserNameU3Ek__BackingField_2);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.TwitterSession::set_UserName(System.String)
extern "C" void TwitterSession_set_UserName_m8_1724 (TwitterSession_t8_295 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CUserNameU3Ek__BackingField_2 = L_0;
		return;
	}
}
// System.String VoxelBusters.NativePlugins.TwitterSession::get_UserID()
extern "C" String_t* TwitterSession_get_UserID_m8_1725 (TwitterSession_t8_295 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CUserIDU3Ek__BackingField_3);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.TwitterSession::set_UserID(System.String)
extern "C" void TwitterSession_set_UserID_m8_1726 (TwitterSession_t8_295 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CUserIDU3Ek__BackingField_3 = L_0;
		return;
	}
}
// System.String VoxelBusters.NativePlugins.TwitterSession::ToString()
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5501;
extern "C" String_t* TwitterSession_ToString_m8_1727 (TwitterSession_t8_295 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5501 = il2cpp_codegen_string_literal_from_index(5501);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_272* L_0 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 4));
		String_t* L_1 = TwitterSession_get_AuthToken_m8_1719(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_1;
		ObjectU5BU5D_t1_272* L_2 = L_0;
		String_t* L_3 = TwitterSession_get_AuthTokenSecret_m8_1721(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1, sizeof(Object_t *))) = (Object_t *)L_3;
		ObjectU5BU5D_t1_272* L_4 = L_2;
		String_t* L_5 = TwitterSession_get_UserName_m8_1723(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, L_5);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 2, sizeof(Object_t *))) = (Object_t *)L_5;
		ObjectU5BU5D_t1_272* L_6 = L_4;
		String_t* L_7 = TwitterSession_get_UserID_m8_1725(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 3);
		ArrayElementTypeCheck (L_6, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 3, sizeof(Object_t *))) = (Object_t *)L_7;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Format_m1_551(NULL /*static, unused*/, _stringLiteral5501, L_6, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.iOSTwitterSession::.ctor(System.Collections.IDictionary)
extern const MethodInfo* IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral5497;
extern Il2CppCodeGenString* _stringLiteral5498;
extern Il2CppCodeGenString* _stringLiteral5499;
extern Il2CppCodeGenString* _stringLiteral5502;
extern "C" void iOSTwitterSession__ctor_m8_1728 (iOSTwitterSession_t8_296 * __this, Object_t * ____sessionJsonDict, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484117);
		_stringLiteral5497 = il2cpp_codegen_string_literal_from_index(5497);
		_stringLiteral5498 = il2cpp_codegen_string_literal_from_index(5498);
		_stringLiteral5499 = il2cpp_codegen_string_literal_from_index(5499);
		_stringLiteral5502 = il2cpp_codegen_string_literal_from_index(5502);
		s_Il2CppMethodIntialized = true;
	}
	{
		TwitterSession__ctor_m8_1718(__this, /*hidden argument*/NULL);
		Object_t * L_0 = ____sessionJsonDict;
		String_t* L_1 = IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(NULL /*static, unused*/, L_0, _stringLiteral5497, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var);
		TwitterSession_set_AuthToken_m8_1720(__this, L_1, /*hidden argument*/NULL);
		Object_t * L_2 = ____sessionJsonDict;
		String_t* L_3 = IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(NULL /*static, unused*/, L_2, _stringLiteral5498, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var);
		TwitterSession_set_AuthTokenSecret_m8_1722(__this, L_3, /*hidden argument*/NULL);
		Object_t * L_4 = ____sessionJsonDict;
		String_t* L_5 = IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(NULL /*static, unused*/, L_4, _stringLiteral5499, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var);
		TwitterSession_set_UserName_m8_1724(__this, L_5, /*hidden argument*/NULL);
		Object_t * L_6 = ____sessionJsonDict;
		String_t* L_7 = IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(NULL /*static, unused*/, L_6, _stringLiteral5502, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var);
		TwitterSession_set_UserID_m8_1726(__this, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.AndroidTwitterUser::.ctor(System.Collections.IDictionary)
extern TypeInfo* IDictionary_t1_35_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* IDictionaryExtensions_GetIfAvailable_TisBoolean_t1_20_m8_2012_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral5500;
extern Il2CppCodeGenString* _stringLiteral230;
extern Il2CppCodeGenString* _stringLiteral5503;
extern Il2CppCodeGenString* _stringLiteral5504;
extern Il2CppCodeGenString* _stringLiteral5505;
extern "C" void AndroidTwitterUser__ctor_m8_1729 (AndroidTwitterUser_t8_297 * __this, Object_t * ____userJsonDict, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionary_t1_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		IDictionaryExtensions_GetIfAvailable_TisBoolean_t1_20_m8_2012_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484234);
		_stringLiteral5500 = il2cpp_codegen_string_literal_from_index(5500);
		_stringLiteral230 = il2cpp_codegen_string_literal_from_index(230);
		_stringLiteral5503 = il2cpp_codegen_string_literal_from_index(5503);
		_stringLiteral5504 = il2cpp_codegen_string_literal_from_index(5504);
		_stringLiteral5505 = il2cpp_codegen_string_literal_from_index(5505);
		s_Il2CppMethodIntialized = true;
	}
	{
		TwitterUser__ctor_m8_1730(__this, /*hidden argument*/NULL);
		Object_t * L_0 = ____userJsonDict;
		NullCheck(L_0);
		Object_t * L_1 = (Object_t *)InterfaceFuncInvoker1< Object_t *, Object_t * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_0, _stringLiteral5500);
		TwitterUser_set_UserID_m8_1732(__this, ((String_t*)IsInstSealed(L_1, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		Object_t * L_2 = ____userJsonDict;
		NullCheck(L_2);
		Object_t * L_3 = (Object_t *)InterfaceFuncInvoker1< Object_t *, Object_t * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_2, _stringLiteral230);
		TwitterUser_set_Name_m8_1734(__this, ((String_t*)IsInstSealed(L_3, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		Object_t * L_4 = ____userJsonDict;
		bool L_5 = IDictionaryExtensions_GetIfAvailable_TisBoolean_t1_20_m8_2012(NULL /*static, unused*/, L_4, _stringLiteral5503, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisBoolean_t1_20_m8_2012_MethodInfo_var);
		TwitterUser_set_IsVerified_m8_1736(__this, L_5, /*hidden argument*/NULL);
		Object_t * L_6 = ____userJsonDict;
		bool L_7 = IDictionaryExtensions_GetIfAvailable_TisBoolean_t1_20_m8_2012(NULL /*static, unused*/, L_6, _stringLiteral5504, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisBoolean_t1_20_m8_2012_MethodInfo_var);
		TwitterUser_set_IsProtected_m8_1738(__this, L_7, /*hidden argument*/NULL);
		Object_t * L_8 = ____userJsonDict;
		NullCheck(L_8);
		Object_t * L_9 = (Object_t *)InterfaceFuncInvoker1< Object_t *, Object_t * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_8, _stringLiteral5505);
		TwitterUser_set_ProfileImageURL_m8_1740(__this, ((String_t*)IsInstSealed(L_9, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.TwitterUser::.ctor()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void TwitterUser__ctor_m8_1730 (TwitterUser_t8_298 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		TwitterUser_set_UserID_m8_1732(__this, L_0, /*hidden argument*/NULL);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		TwitterUser_set_Name_m8_1734(__this, L_1, /*hidden argument*/NULL);
		TwitterUser_set_IsVerified_m8_1736(__this, 0, /*hidden argument*/NULL);
		TwitterUser_set_IsProtected_m8_1738(__this, 0, /*hidden argument*/NULL);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		TwitterUser_set_ProfileImageURL_m8_1740(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.String VoxelBusters.NativePlugins.TwitterUser::get_UserID()
extern "C" String_t* TwitterUser_get_UserID_m8_1731 (TwitterUser_t8_298 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CUserIDU3Ek__BackingField_0);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.TwitterUser::set_UserID(System.String)
extern "C" void TwitterUser_set_UserID_m8_1732 (TwitterUser_t8_298 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CUserIDU3Ek__BackingField_0 = L_0;
		return;
	}
}
// System.String VoxelBusters.NativePlugins.TwitterUser::get_Name()
extern "C" String_t* TwitterUser_get_Name_m8_1733 (TwitterUser_t8_298 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CNameU3Ek__BackingField_1);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.TwitterUser::set_Name(System.String)
extern "C" void TwitterUser_set_Name_m8_1734 (TwitterUser_t8_298 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CNameU3Ek__BackingField_1 = L_0;
		return;
	}
}
// System.Boolean VoxelBusters.NativePlugins.TwitterUser::get_IsVerified()
extern "C" bool TwitterUser_get_IsVerified_m8_1735 (TwitterUser_t8_298 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___U3CIsVerifiedU3Ek__BackingField_2);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.TwitterUser::set_IsVerified(System.Boolean)
extern "C" void TwitterUser_set_IsVerified_m8_1736 (TwitterUser_t8_298 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___U3CIsVerifiedU3Ek__BackingField_2 = L_0;
		return;
	}
}
// System.Boolean VoxelBusters.NativePlugins.TwitterUser::get_IsProtected()
extern "C" bool TwitterUser_get_IsProtected_m8_1737 (TwitterUser_t8_298 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___U3CIsProtectedU3Ek__BackingField_3);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.TwitterUser::set_IsProtected(System.Boolean)
extern "C" void TwitterUser_set_IsProtected_m8_1738 (TwitterUser_t8_298 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___U3CIsProtectedU3Ek__BackingField_3 = L_0;
		return;
	}
}
// System.String VoxelBusters.NativePlugins.TwitterUser::get_ProfileImageURL()
extern "C" String_t* TwitterUser_get_ProfileImageURL_m8_1739 (TwitterUser_t8_298 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CProfileImageURLU3Ek__BackingField_4);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.TwitterUser::set_ProfileImageURL(System.String)
extern "C" void TwitterUser_set_ProfileImageURL_m8_1740 (TwitterUser_t8_298 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CProfileImageURLU3Ek__BackingField_4 = L_0;
		return;
	}
}
// System.String VoxelBusters.NativePlugins.TwitterUser::ToString()
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t1_20_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5506;
extern "C" String_t* TwitterUser_ToString_m8_1741 (TwitterUser_t8_298 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Boolean_t1_20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(54);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5506 = il2cpp_codegen_string_literal_from_index(5506);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_272* L_0 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 5));
		String_t* L_1 = TwitterUser_get_UserID_m8_1731(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_1;
		ObjectU5BU5D_t1_272* L_2 = L_0;
		String_t* L_3 = TwitterUser_get_Name_m8_1733(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1, sizeof(Object_t *))) = (Object_t *)L_3;
		ObjectU5BU5D_t1_272* L_4 = L_2;
		bool L_5 = TwitterUser_get_IsVerified_m8_1735(__this, /*hidden argument*/NULL);
		bool L_6 = L_5;
		Object_t * L_7 = Box(Boolean_t1_20_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 2, sizeof(Object_t *))) = (Object_t *)L_7;
		ObjectU5BU5D_t1_272* L_8 = L_4;
		bool L_9 = TwitterUser_get_IsProtected_m8_1737(__this, /*hidden argument*/NULL);
		bool L_10 = L_9;
		Object_t * L_11 = Box(Boolean_t1_20_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 3);
		ArrayElementTypeCheck (L_8, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 3, sizeof(Object_t *))) = (Object_t *)L_11;
		ObjectU5BU5D_t1_272* L_12 = L_8;
		String_t* L_13 = TwitterUser_get_ProfileImageURL_m8_1739(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, L_13);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 4, sizeof(Object_t *))) = (Object_t *)L_13;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Format_m1_551(NULL /*static, unused*/, _stringLiteral5506, L_12, /*hidden argument*/NULL);
		return L_14;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.iOSTwitterUser::.ctor(System.Collections.IDictionary)
extern const MethodInfo* IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var;
extern const MethodInfo* IDictionaryExtensions_GetIfAvailable_TisBoolean_t1_20_m8_2012_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral5502;
extern Il2CppCodeGenString* _stringLiteral230;
extern Il2CppCodeGenString* _stringLiteral5503;
extern Il2CppCodeGenString* _stringLiteral5504;
extern Il2CppCodeGenString* _stringLiteral5507;
extern "C" void iOSTwitterUser__ctor_m8_1742 (iOSTwitterUser_t8_299 * __this, Object_t * ____userJsonDict, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484117);
		IDictionaryExtensions_GetIfAvailable_TisBoolean_t1_20_m8_2012_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484234);
		_stringLiteral5502 = il2cpp_codegen_string_literal_from_index(5502);
		_stringLiteral230 = il2cpp_codegen_string_literal_from_index(230);
		_stringLiteral5503 = il2cpp_codegen_string_literal_from_index(5503);
		_stringLiteral5504 = il2cpp_codegen_string_literal_from_index(5504);
		_stringLiteral5507 = il2cpp_codegen_string_literal_from_index(5507);
		s_Il2CppMethodIntialized = true;
	}
	{
		TwitterUser__ctor_m8_1730(__this, /*hidden argument*/NULL);
		Object_t * L_0 = ____userJsonDict;
		String_t* L_1 = IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(NULL /*static, unused*/, L_0, _stringLiteral5502, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var);
		TwitterUser_set_UserID_m8_1732(__this, L_1, /*hidden argument*/NULL);
		Object_t * L_2 = ____userJsonDict;
		String_t* L_3 = IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(NULL /*static, unused*/, L_2, _stringLiteral230, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var);
		TwitterUser_set_Name_m8_1734(__this, L_3, /*hidden argument*/NULL);
		Object_t * L_4 = ____userJsonDict;
		bool L_5 = IDictionaryExtensions_GetIfAvailable_TisBoolean_t1_20_m8_2012(NULL /*static, unused*/, L_4, _stringLiteral5503, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisBoolean_t1_20_m8_2012_MethodInfo_var);
		TwitterUser_set_IsVerified_m8_1736(__this, L_5, /*hidden argument*/NULL);
		Object_t * L_6 = ____userJsonDict;
		bool L_7 = IDictionaryExtensions_GetIfAvailable_TisBoolean_t1_20_m8_2012(NULL /*static, unused*/, L_6, _stringLiteral5504, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisBoolean_t1_20_m8_2012_MethodInfo_var);
		TwitterUser_set_IsProtected_m8_1738(__this, L_7, /*hidden argument*/NULL);
		Object_t * L_8 = ____userJsonDict;
		String_t* L_9 = IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(NULL /*static, unused*/, L_8, _stringLiteral5507, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var);
		TwitterUser_set_ProfileImageURL_m8_1740(__this, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.TwitterSettings::.ctor()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void TwitterSettings__ctor_m8_1743 (TwitterSettings_t8_292 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		TwitterSettings_set_ConsumerKey_m8_1745(__this, L_0, /*hidden argument*/NULL);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		TwitterSettings_set_ConsumerSecret_m8_1747(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String VoxelBusters.NativePlugins.TwitterSettings::get_ConsumerKey()
extern "C" String_t* TwitterSettings_get_ConsumerKey_m8_1744 (TwitterSettings_t8_292 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_consumerKey_0);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.TwitterSettings::set_ConsumerKey(System.String)
extern "C" void TwitterSettings_set_ConsumerKey_m8_1745 (TwitterSettings_t8_292 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_consumerKey_0 = L_0;
		return;
	}
}
// System.String VoxelBusters.NativePlugins.TwitterSettings::get_ConsumerSecret()
extern "C" String_t* TwitterSettings_get_ConsumerSecret_m8_1746 (TwitterSettings_t8_292 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_consumerSecret_1);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.TwitterSettings::set_ConsumerSecret(System.String)
extern "C" void TwitterSettings_set_ConsumerSecret_m8_1747 (TwitterSettings_t8_292 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_consumerSecret_1 = L_0;
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.UI/AlertDialogCompletion::.ctor(System.Object,System.IntPtr)
extern "C" void AlertDialogCompletion__ctor_m8_1748 (AlertDialogCompletion_t8_300 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void VoxelBusters.NativePlugins.UI/AlertDialogCompletion::Invoke(System.String)
extern "C" void AlertDialogCompletion_Invoke_m8_1749 (AlertDialogCompletion_t8_300 * __this, String_t* ____buttonPressed, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		AlertDialogCompletion_Invoke_m8_1749((AlertDialogCompletion_t8_300 *)__this->___prev_9,____buttonPressed, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, String_t* ____buttonPressed, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,____buttonPressed,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, String_t* ____buttonPressed, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,____buttonPressed,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(____buttonPressed,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_AlertDialogCompletion_t8_300(Il2CppObject* delegate, String_t* ____buttonPressed)
{
	typedef void (STDCALL *native_function_ptr_type)(char*);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '____buttonPressed' to native representation
	char* _____buttonPressed_marshaled = { 0 };
	_____buttonPressed_marshaled = il2cpp_codegen_marshal_string(____buttonPressed);

	// Native function invocation
	_il2cpp_pinvoke_func(_____buttonPressed_marshaled);

	// Marshaling cleanup of parameter '____buttonPressed' native representation
	il2cpp_codegen_marshal_free(_____buttonPressed_marshaled);
	_____buttonPressed_marshaled = NULL;

}
// System.IAsyncResult VoxelBusters.NativePlugins.UI/AlertDialogCompletion::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C" Object_t * AlertDialogCompletion_BeginInvoke_m8_1750 (AlertDialogCompletion_t8_300 * __this, String_t* ____buttonPressed, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ____buttonPressed;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void VoxelBusters.NativePlugins.UI/AlertDialogCompletion::EndInvoke(System.IAsyncResult)
extern "C" void AlertDialogCompletion_EndInvoke_m8_1751 (AlertDialogCompletion_t8_300 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void VoxelBusters.NativePlugins.UI/SingleFieldPromptCompletion::.ctor(System.Object,System.IntPtr)
extern "C" void SingleFieldPromptCompletion__ctor_m8_1752 (SingleFieldPromptCompletion_t8_301 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void VoxelBusters.NativePlugins.UI/SingleFieldPromptCompletion::Invoke(System.String,System.String)
extern "C" void SingleFieldPromptCompletion_Invoke_m8_1753 (SingleFieldPromptCompletion_t8_301 * __this, String_t* ____buttonPressed, String_t* ____inputText, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		SingleFieldPromptCompletion_Invoke_m8_1753((SingleFieldPromptCompletion_t8_301 *)__this->___prev_9,____buttonPressed, ____inputText, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, String_t* ____buttonPressed, String_t* ____inputText, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,____buttonPressed, ____inputText,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, String_t* ____buttonPressed, String_t* ____inputText, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,____buttonPressed, ____inputText,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, String_t* ____inputText, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(____buttonPressed, ____inputText,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_SingleFieldPromptCompletion_t8_301(Il2CppObject* delegate, String_t* ____buttonPressed, String_t* ____inputText)
{
	typedef void (STDCALL *native_function_ptr_type)(char*, char*);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '____buttonPressed' to native representation
	char* _____buttonPressed_marshaled = { 0 };
	_____buttonPressed_marshaled = il2cpp_codegen_marshal_string(____buttonPressed);

	// Marshaling of parameter '____inputText' to native representation
	char* _____inputText_marshaled = { 0 };
	_____inputText_marshaled = il2cpp_codegen_marshal_string(____inputText);

	// Native function invocation
	_il2cpp_pinvoke_func(_____buttonPressed_marshaled, _____inputText_marshaled);

	// Marshaling cleanup of parameter '____buttonPressed' native representation
	il2cpp_codegen_marshal_free(_____buttonPressed_marshaled);
	_____buttonPressed_marshaled = NULL;

	// Marshaling cleanup of parameter '____inputText' native representation
	il2cpp_codegen_marshal_free(_____inputText_marshaled);
	_____inputText_marshaled = NULL;

}
// System.IAsyncResult VoxelBusters.NativePlugins.UI/SingleFieldPromptCompletion::BeginInvoke(System.String,System.String,System.AsyncCallback,System.Object)
extern "C" Object_t * SingleFieldPromptCompletion_BeginInvoke_m8_1754 (SingleFieldPromptCompletion_t8_301 * __this, String_t* ____buttonPressed, String_t* ____inputText, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ____buttonPressed;
	__d_args[1] = ____inputText;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void VoxelBusters.NativePlugins.UI/SingleFieldPromptCompletion::EndInvoke(System.IAsyncResult)
extern "C" void SingleFieldPromptCompletion_EndInvoke_m8_1755 (SingleFieldPromptCompletion_t8_301 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void VoxelBusters.NativePlugins.UI/LoginPromptCompletion::.ctor(System.Object,System.IntPtr)
extern "C" void LoginPromptCompletion__ctor_m8_1756 (LoginPromptCompletion_t8_302 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void VoxelBusters.NativePlugins.UI/LoginPromptCompletion::Invoke(System.String,System.String,System.String)
extern "C" void LoginPromptCompletion_Invoke_m8_1757 (LoginPromptCompletion_t8_302 * __this, String_t* ____buttonPressed, String_t* ____usernameText, String_t* ____passwordText, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		LoginPromptCompletion_Invoke_m8_1757((LoginPromptCompletion_t8_302 *)__this->___prev_9,____buttonPressed, ____usernameText, ____passwordText, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, String_t* ____buttonPressed, String_t* ____usernameText, String_t* ____passwordText, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,____buttonPressed, ____usernameText, ____passwordText,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, String_t* ____buttonPressed, String_t* ____usernameText, String_t* ____passwordText, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,____buttonPressed, ____usernameText, ____passwordText,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, String_t* ____usernameText, String_t* ____passwordText, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(____buttonPressed, ____usernameText, ____passwordText,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_LoginPromptCompletion_t8_302(Il2CppObject* delegate, String_t* ____buttonPressed, String_t* ____usernameText, String_t* ____passwordText)
{
	typedef void (STDCALL *native_function_ptr_type)(char*, char*, char*);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '____buttonPressed' to native representation
	char* _____buttonPressed_marshaled = { 0 };
	_____buttonPressed_marshaled = il2cpp_codegen_marshal_string(____buttonPressed);

	// Marshaling of parameter '____usernameText' to native representation
	char* _____usernameText_marshaled = { 0 };
	_____usernameText_marshaled = il2cpp_codegen_marshal_string(____usernameText);

	// Marshaling of parameter '____passwordText' to native representation
	char* _____passwordText_marshaled = { 0 };
	_____passwordText_marshaled = il2cpp_codegen_marshal_string(____passwordText);

	// Native function invocation
	_il2cpp_pinvoke_func(_____buttonPressed_marshaled, _____usernameText_marshaled, _____passwordText_marshaled);

	// Marshaling cleanup of parameter '____buttonPressed' native representation
	il2cpp_codegen_marshal_free(_____buttonPressed_marshaled);
	_____buttonPressed_marshaled = NULL;

	// Marshaling cleanup of parameter '____usernameText' native representation
	il2cpp_codegen_marshal_free(_____usernameText_marshaled);
	_____usernameText_marshaled = NULL;

	// Marshaling cleanup of parameter '____passwordText' native representation
	il2cpp_codegen_marshal_free(_____passwordText_marshaled);
	_____passwordText_marshaled = NULL;

}
// System.IAsyncResult VoxelBusters.NativePlugins.UI/LoginPromptCompletion::BeginInvoke(System.String,System.String,System.String,System.AsyncCallback,System.Object)
extern "C" Object_t * LoginPromptCompletion_BeginInvoke_m8_1758 (LoginPromptCompletion_t8_302 * __this, String_t* ____buttonPressed, String_t* ____usernameText, String_t* ____passwordText, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[4] = {0};
	__d_args[0] = ____buttonPressed;
	__d_args[1] = ____usernameText;
	__d_args[2] = ____passwordText;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void VoxelBusters.NativePlugins.UI/LoginPromptCompletion::EndInvoke(System.IAsyncResult)
extern "C" void LoginPromptCompletion_EndInvoke_m8_1759 (LoginPromptCompletion_t8_302 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void VoxelBusters.NativePlugins.UI::.ctor()
extern TypeInfo* Dictionary_2_t1_1911_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_15067_MethodInfo_var;
extern "C" void UI__ctor_m8_1760 (UI_t8_303 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t1_1911_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2167);
		Dictionary_2__ctor_m1_15067_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484245);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t1_1911 * L_0 = (Dictionary_2_t1_1911 *)il2cpp_codegen_object_new (Dictionary_2_t1_1911_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_15067(L_0, /*hidden argument*/Dictionary_2__ctor_m1_15067_MethodInfo_var);
		__this->___m_alertDialogCallbackCollection_3 = L_0;
		MonoBehaviour__ctor_m6_671(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.UI::AlertDialogClosed(System.String)
extern TypeInfo* IDictionary_t1_35_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5508;
extern "C" void UI_AlertDialogClosed_m8_1761 (UI_t8_303 * __this, String_t* ____jsonStr, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionary_t1_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5508 = il2cpp_codegen_string_literal_from_index(5508);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	String_t* V_1 = {0};
	String_t* V_2 = {0};
	AlertDialogCompletion_t8_300 * V_3 = {0};
	{
		String_t* L_0 = ____jsonStr;
		Object_t * L_1 = JSONUtility_FromJSON_m8_298(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = ((Object_t *)IsInst(L_1, IDictionary_t1_35_il2cpp_TypeInfo_var));
		Object_t * L_2 = V_0;
		VirtActionInvoker3< Object_t *, String_t**, String_t** >::Invoke(4 /* System.Void VoxelBusters.NativePlugins.UI::ParseAlertDialogDismissedData(System.Collections.IDictionary,System.String&,System.String&) */, __this, L_2, (&V_1), (&V_2));
		String_t* L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m1_559(NULL /*static, unused*/, _stringLiteral5508, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_Log_m8_1007(NULL /*static, unused*/, _stringLiteral5368, L_4, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		String_t* L_5 = V_2;
		AlertDialogCompletion_t8_300 * L_6 = UI_GetAlertDialogCallback_m8_1764(__this, L_5, /*hidden argument*/NULL);
		V_3 = L_6;
		AlertDialogCompletion_t8_300 * L_7 = V_3;
		if (!L_7)
		{
			goto IL_0042;
		}
	}
	{
		AlertDialogCompletion_t8_300 * L_8 = V_3;
		String_t* L_9 = V_1;
		NullCheck(L_8);
		AlertDialogCompletion_Invoke_m8_1749(L_8, L_9, /*hidden argument*/NULL);
	}

IL_0042:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.UI::ParseAlertDialogDismissedData(System.Collections.IDictionary,System.String&,System.String&)
extern "C" void UI_ParseAlertDialogDismissedData_m8_1762 (UI_t8_303 * __this, Object_t * ____dataDict, String_t** ____buttonPressed, String_t** ____callerTag, const MethodInfo* method)
{
	{
		String_t** L_0 = ____buttonPressed;
		*((Object_t **)(L_0)) = (Object_t *)NULL;
		String_t** L_1 = ____callerTag;
		*((Object_t **)(L_1)) = (Object_t *)NULL;
		return;
	}
}
// System.String VoxelBusters.NativePlugins.UI::CacheAlertDialogCallback(VoxelBusters.NativePlugins.UI/AlertDialogCompletion)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" String_t* UI_CacheAlertDialogCallback_m8_1763 (UI_t8_303 * __this, AlertDialogCompletion_t8_300 * ____newCallback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	{
		AlertDialogCompletion_t8_300 * L_0 = ____newCallback;
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		Utility_t8_306 * L_1 = NPBinding_get_Utility_m8_1931(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(5 /* System.String VoxelBusters.NativePlugins.Utility::GetUUID() */, L_1);
		V_0 = L_2;
		Dictionary_2_t1_1911 * L_3 = (__this->___m_alertDialogCallbackCollection_3);
		String_t* L_4 = V_0;
		AlertDialogCompletion_t8_300 * L_5 = ____newCallback;
		NullCheck(L_3);
		VirtActionInvoker2< String_t*, AlertDialogCompletion_t8_300 * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,VoxelBusters.NativePlugins.UI/AlertDialogCompletion>::set_Item(!0,!1) */, L_3, L_4, L_5);
		String_t* L_6 = V_0;
		return L_6;
	}

IL_0020:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		return L_7;
	}
}
// VoxelBusters.NativePlugins.UI/AlertDialogCompletion VoxelBusters.NativePlugins.UI::GetAlertDialogCallback(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" AlertDialogCompletion_t8_300 * UI_GetAlertDialogCallback_m8_1764 (UI_t8_303 * __this, String_t* ____tag, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ____tag;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1_521(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0029;
		}
	}
	{
		Dictionary_2_t1_1911 * L_2 = (__this->___m_alertDialogCallbackCollection_3);
		String_t* L_3 = ____tag;
		NullCheck(L_2);
		bool L_4 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,VoxelBusters.NativePlugins.UI/AlertDialogCompletion>::ContainsKey(!0) */, L_2, L_3);
		if (!L_4)
		{
			goto IL_0029;
		}
	}
	{
		Dictionary_2_t1_1911 * L_5 = (__this->___m_alertDialogCallbackCollection_3);
		String_t* L_6 = ____tag;
		NullCheck(L_5);
		AlertDialogCompletion_t8_300 * L_7 = (AlertDialogCompletion_t8_300 *)VirtFuncInvoker1< AlertDialogCompletion_t8_300 *, String_t* >::Invoke(22 /* !1 System.Collections.Generic.Dictionary`2<System.String,VoxelBusters.NativePlugins.UI/AlertDialogCompletion>::get_Item(!0) */, L_5, L_6);
		return L_7;
	}

IL_0029:
	{
		return (AlertDialogCompletion_t8_300 *)NULL;
	}
}
// System.Void VoxelBusters.NativePlugins.UI::ShowAlertDialogWithSingleButton(System.String,System.String,System.String,VoxelBusters.NativePlugins.UI/AlertDialogCompletion)
extern TypeInfo* StringU5BU5D_t1_238_il2cpp_TypeInfo_var;
extern "C" void UI_ShowAlertDialogWithSingleButton_m8_1765 (UI_t8_303 * __this, String_t* ____title, String_t* ____message, String_t* ____button, AlertDialogCompletion_t8_300 * ____onCompletion, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringU5BU5D_t1_238_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(35);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ____title;
		String_t* L_1 = ____message;
		StringU5BU5D_t1_238* L_2 = ((StringU5BU5D_t1_238*)SZArrayNew(StringU5BU5D_t1_238_il2cpp_TypeInfo_var, 1));
		String_t* L_3 = ____button;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_3);
		*((String_t**)(String_t**)SZArrayLdElema(L_2, 0, sizeof(String_t*))) = (String_t*)L_3;
		AlertDialogCompletion_t8_300 * L_4 = ____onCompletion;
		UI_ShowAlertDialogWithMultipleButtons_m8_1766(__this, L_0, L_1, L_2, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.UI::ShowAlertDialogWithMultipleButtons(System.String,System.String,System.String[],VoxelBusters.NativePlugins.UI/AlertDialogCompletion)
extern "C" void UI_ShowAlertDialogWithMultipleButtons_m8_1766 (UI_t8_303 * __this, String_t* ____title, String_t* ____message, StringU5BU5D_t1_238* ____buttonsList, AlertDialogCompletion_t8_300 * ____onCompletion, const MethodInfo* method)
{
	String_t* V_0 = {0};
	{
		AlertDialogCompletion_t8_300 * L_0 = ____onCompletion;
		String_t* L_1 = UI_CacheAlertDialogCallback_m8_1763(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = ____title;
		String_t* L_3 = ____message;
		StringU5BU5D_t1_238* L_4 = ____buttonsList;
		String_t* L_5 = V_0;
		VirtActionInvoker4< String_t*, String_t*, StringU5BU5D_t1_238*, String_t* >::Invoke(5 /* System.Void VoxelBusters.NativePlugins.UI::ShowAlertDialogWithMultipleButtons(System.String,System.String,System.String[],System.String) */, __this, L_2, L_3, L_4, L_5);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.UI::ShowAlertDialogWithMultipleButtons(System.String,System.String,System.String[],System.String)
extern TypeInfo* StringU5BU5D_t1_238_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5334;
extern "C" void UI_ShowAlertDialogWithMultipleButtons_m8_1767 (UI_t8_303 * __this, String_t* ____title, String_t* ____message, StringU5BU5D_t1_238* ____buttonsList, String_t* ____callbackTag, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringU5BU5D_t1_238_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(35);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5334 = il2cpp_codegen_string_literal_from_index(5334);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringU5BU5D_t1_238* L_0 = ____buttonsList;
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		StringU5BU5D_t1_238* L_1 = ____buttonsList;
		NullCheck(L_1);
		if ((((int32_t)((int32_t)(((Array_t *)L_1)->max_length)))))
		{
			goto IL_0023;
		}
	}

IL_000e:
	{
		StringU5BU5D_t1_238* L_2 = ((StringU5BU5D_t1_238*)SZArrayNew(StringU5BU5D_t1_238_il2cpp_TypeInfo_var, 1));
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, _stringLiteral5334);
		*((String_t**)(String_t**)SZArrayLdElema(L_2, 0, sizeof(String_t*))) = (String_t*)_stringLiteral5334;
		____buttonsList = L_2;
		goto IL_0038;
	}

IL_0023:
	{
		StringU5BU5D_t1_238* L_3 = ____buttonsList;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		int32_t L_4 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_IsNullOrEmpty_m1_521(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_3, L_4, sizeof(String_t*))), /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0038;
		}
	}
	{
		StringU5BU5D_t1_238* L_6 = ____buttonsList;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 0);
		ArrayElementTypeCheck (L_6, _stringLiteral5334);
		*((String_t**)(String_t**)SZArrayLdElema(L_6, 0, sizeof(String_t*))) = (String_t*)_stringLiteral5334;
	}

IL_0038:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.UI::SingleFieldPromptDialogClosed(System.String)
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionary_t1_35_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5509;
extern "C" void UI_SingleFieldPromptDialogClosed_m8_1768 (UI_t8_303 * __this, String_t* ____jsonStr, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		IDictionary_t1_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5509 = il2cpp_codegen_string_literal_from_index(5509);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	String_t* V_1 = {0};
	String_t* V_2 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_Log_m8_1007(NULL /*static, unused*/, _stringLiteral5368, _stringLiteral5509, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		SingleFieldPromptCompletion_t8_301 * L_0 = (__this->___OnSingleFieldPromptClosed_4);
		if (!L_0)
		{
			goto IL_003f;
		}
	}
	{
		String_t* L_1 = ____jsonStr;
		Object_t * L_2 = JSONUtility_FromJSON_m8_298(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = ((Object_t *)IsInst(L_2, IDictionary_t1_35_il2cpp_TypeInfo_var));
		Object_t * L_3 = V_0;
		VirtActionInvoker3< Object_t *, String_t**, String_t** >::Invoke(6 /* System.Void VoxelBusters.NativePlugins.UI::ParseSingleFieldPromptClosedData(System.Collections.IDictionary,System.String&,System.String&) */, __this, L_3, (&V_1), (&V_2));
		SingleFieldPromptCompletion_t8_301 * L_4 = (__this->___OnSingleFieldPromptClosed_4);
		String_t* L_5 = V_1;
		String_t* L_6 = V_2;
		NullCheck(L_4);
		SingleFieldPromptCompletion_Invoke_m8_1753(L_4, L_5, L_6, /*hidden argument*/NULL);
	}

IL_003f:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.UI::LoginPromptDialogClosed(System.String)
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionary_t1_35_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5510;
extern "C" void UI_LoginPromptDialogClosed_m8_1769 (UI_t8_303 * __this, String_t* ____jsonStr, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		IDictionary_t1_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5510 = il2cpp_codegen_string_literal_from_index(5510);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	String_t* V_1 = {0};
	String_t* V_2 = {0};
	String_t* V_3 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_Log_m8_1007(NULL /*static, unused*/, _stringLiteral5368, _stringLiteral5510, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		LoginPromptCompletion_t8_302 * L_0 = (__this->___OnLoginPromptClosed_5);
		if (!L_0)
		{
			goto IL_0042;
		}
	}
	{
		String_t* L_1 = ____jsonStr;
		Object_t * L_2 = JSONUtility_FromJSON_m8_298(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = ((Object_t *)IsInst(L_2, IDictionary_t1_35_il2cpp_TypeInfo_var));
		Object_t * L_3 = V_0;
		VirtActionInvoker4< Object_t *, String_t**, String_t**, String_t** >::Invoke(7 /* System.Void VoxelBusters.NativePlugins.UI::ParseLoginPromptClosedData(System.Collections.IDictionary,System.String&,System.String&,System.String&) */, __this, L_3, (&V_1), (&V_2), (&V_3));
		LoginPromptCompletion_t8_302 * L_4 = (__this->___OnLoginPromptClosed_5);
		String_t* L_5 = V_1;
		String_t* L_6 = V_2;
		String_t* L_7 = V_3;
		NullCheck(L_4);
		LoginPromptCompletion_Invoke_m8_1757(L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
	}

IL_0042:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.UI::ParseSingleFieldPromptClosedData(System.Collections.IDictionary,System.String&,System.String&)
extern "C" void UI_ParseSingleFieldPromptClosedData_m8_1770 (UI_t8_303 * __this, Object_t * ____dataDict, String_t** ____buttonPressed, String_t** ____inputText, const MethodInfo* method)
{
	{
		String_t** L_0 = ____buttonPressed;
		*((Object_t **)(L_0)) = (Object_t *)NULL;
		String_t** L_1 = ____inputText;
		*((Object_t **)(L_1)) = (Object_t *)NULL;
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.UI::ParseLoginPromptClosedData(System.Collections.IDictionary,System.String&,System.String&,System.String&)
extern "C" void UI_ParseLoginPromptClosedData_m8_1771 (UI_t8_303 * __this, Object_t * ____dataDict, String_t** ____buttonPressed, String_t** ____usernameText, String_t** ____passwordText, const MethodInfo* method)
{
	{
		String_t** L_0 = ____buttonPressed;
		*((Object_t **)(L_0)) = (Object_t *)NULL;
		String_t** L_1 = ____usernameText;
		*((Object_t **)(L_1)) = (Object_t *)NULL;
		String_t** L_2 = ____passwordText;
		*((Object_t **)(L_2)) = (Object_t *)NULL;
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.UI::ShowSingleFieldPromptDialogWithPlainText(System.String,System.String,System.String,System.String[],VoxelBusters.NativePlugins.UI/SingleFieldPromptCompletion)
extern "C" void UI_ShowSingleFieldPromptDialogWithPlainText_m8_1772 (UI_t8_303 * __this, String_t* ____title, String_t* ____message, String_t* ____placeholder, StringU5BU5D_t1_238* ____buttonsList, SingleFieldPromptCompletion_t8_301 * ____onCompletion, const MethodInfo* method)
{
	{
		String_t* L_0 = ____title;
		String_t* L_1 = ____message;
		String_t* L_2 = ____placeholder;
		StringU5BU5D_t1_238* L_3 = ____buttonsList;
		SingleFieldPromptCompletion_t8_301 * L_4 = ____onCompletion;
		VirtActionInvoker6< String_t*, String_t*, String_t*, bool, StringU5BU5D_t1_238*, SingleFieldPromptCompletion_t8_301 * >::Invoke(8 /* System.Void VoxelBusters.NativePlugins.UI::ShowSingleFieldPromptDialog(System.String,System.String,System.String,System.Boolean,System.String[],VoxelBusters.NativePlugins.UI/SingleFieldPromptCompletion) */, __this, L_0, L_1, L_2, 0, L_3, L_4);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.UI::ShowSingleFieldPromptDialogWithSecuredText(System.String,System.String,System.String,System.String[],VoxelBusters.NativePlugins.UI/SingleFieldPromptCompletion)
extern "C" void UI_ShowSingleFieldPromptDialogWithSecuredText_m8_1773 (UI_t8_303 * __this, String_t* ____title, String_t* ____message, String_t* ____placeholder, StringU5BU5D_t1_238* ____buttonsList, SingleFieldPromptCompletion_t8_301 * ____onCompletion, const MethodInfo* method)
{
	{
		String_t* L_0 = ____title;
		String_t* L_1 = ____message;
		String_t* L_2 = ____placeholder;
		StringU5BU5D_t1_238* L_3 = ____buttonsList;
		SingleFieldPromptCompletion_t8_301 * L_4 = ____onCompletion;
		VirtActionInvoker6< String_t*, String_t*, String_t*, bool, StringU5BU5D_t1_238*, SingleFieldPromptCompletion_t8_301 * >::Invoke(8 /* System.Void VoxelBusters.NativePlugins.UI::ShowSingleFieldPromptDialog(System.String,System.String,System.String,System.Boolean,System.String[],VoxelBusters.NativePlugins.UI/SingleFieldPromptCompletion) */, __this, L_0, L_1, L_2, 1, L_3, L_4);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.UI::ShowSingleFieldPromptDialog(System.String,System.String,System.String,System.Boolean,System.String[],VoxelBusters.NativePlugins.UI/SingleFieldPromptCompletion)
extern "C" void UI_ShowSingleFieldPromptDialog_m8_1774 (UI_t8_303 * __this, String_t* ____title, String_t* ____message, String_t* ____placeholder, bool ____useSecureText, StringU5BU5D_t1_238* ____buttonsList, SingleFieldPromptCompletion_t8_301 * ____onCompletion, const MethodInfo* method)
{
	{
		SingleFieldPromptCompletion_t8_301 * L_0 = ____onCompletion;
		__this->___OnSingleFieldPromptClosed_4 = L_0;
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.UI::ShowLoginPromptDialog(System.String,System.String,System.String,System.String,System.String[],VoxelBusters.NativePlugins.UI/LoginPromptCompletion)
extern "C" void UI_ShowLoginPromptDialog_m8_1775 (UI_t8_303 * __this, String_t* ____title, String_t* ____message, String_t* ____usernamePlaceHolder, String_t* ____passwordPlaceHolder, StringU5BU5D_t1_238* ____buttonsList, LoginPromptCompletion_t8_302 * ____onCompletion, const MethodInfo* method)
{
	{
		LoginPromptCompletion_t8_302 * L_0 = ____onCompletion;
		__this->___OnLoginPromptClosed_5 = L_0;
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.UI::ShowToast(System.String,VoxelBusters.NativePlugins.eToastMessageLength)
extern "C" void UI_ShowToast_m8_1776 (UI_t8_303 * __this, String_t* ____message, int32_t ____length, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.UI::SetPopoverPoint(UnityEngine.Vector2)
extern "C" void UI_SetPopoverPoint_m8_1777 (UI_t8_303 * __this, Vector2_t6_47  ____position, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.UI::SetPopoverPointAtLastTouchPosition()
extern TypeInfo* Input_t6_95_il2cpp_TypeInfo_var;
extern "C" void UI_SetPopoverPointAtLastTouchPosition_m8_1778 (UI_t8_303 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t6_95_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1613);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t6_47  V_0 = {0};
	Touch_t6_94  V_1 = {0};
	{
		Vector2_t6_47  L_0 = Vector2_get_zero_m6_225(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t6_95_il2cpp_TypeInfo_var);
		int32_t L_1 = Input_get_touchCount_m6_695(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_0035;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t6_95_il2cpp_TypeInfo_var);
		Touch_t6_94  L_2 = Input_GetTouch_m6_694(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_1 = L_2;
		Vector2_t6_47  L_3 = Touch_get_position_m6_679((&V_1), /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = Screen_get_height_m6_146(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_5 = ((&V_0)->___y_2);
		(&V_0)->___y_2 = ((float)((float)(((float)((float)L_4)))-(float)L_5));
	}

IL_0035:
	{
		Vector2_t6_47  L_6 = V_0;
		VirtActionInvoker1< Vector2_t6_47  >::Invoke(11 /* System.Void VoxelBusters.NativePlugins.UI::SetPopoverPoint(UnityEngine.Vector2) */, __this, L_6);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.UIIOS::.ctor()
extern "C" void UIIOS__ctor_m8_1779 (UIIOS_t8_304 * __this, const MethodInfo* method)
{
	{
		UI__ctor_m8_1760(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.UIIOS::ParseAlertDialogDismissedData(System.Collections.IDictionary,System.String&,System.String&)
extern TypeInfo* IDictionary_t1_35_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5511;
extern Il2CppCodeGenString* _stringLiteral5512;
extern "C" void UIIOS_ParseAlertDialogDismissedData_m8_1780 (UIIOS_t8_304 * __this, Object_t * ____dataDict, String_t** ____buttonPressed, String_t** ____callerTag, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionary_t1_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5511 = il2cpp_codegen_string_literal_from_index(5511);
		_stringLiteral5512 = il2cpp_codegen_string_literal_from_index(5512);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t** L_0 = ____buttonPressed;
		Object_t * L_1 = ____dataDict;
		NullCheck(L_1);
		Object_t * L_2 = (Object_t *)InterfaceFuncInvoker1< Object_t *, Object_t * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_1, _stringLiteral5511);
		*((Object_t **)(L_0)) = (Object_t *)((String_t*)IsInstSealed(L_2, String_t_il2cpp_TypeInfo_var));
		String_t** L_3 = ____callerTag;
		Object_t * L_4 = ____dataDict;
		NullCheck(L_4);
		Object_t * L_5 = (Object_t *)InterfaceFuncInvoker1< Object_t *, Object_t * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_4, _stringLiteral5512);
		*((Object_t **)(L_3)) = (Object_t *)((String_t*)IsInstSealed(L_5, String_t_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.UIIOS::showAlertDialog(System.String,System.String,System.String,System.String)
extern "C" {void DEFAULT_CALL showAlertDialog(char*, char*, char*, char*);}
extern "C" void UIIOS_showAlertDialog_m8_1781 (Object_t * __this /* static, unused */, String_t* ____title, String_t* ____message, String_t* ____buttonsList, String_t* ____callerTag, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*, char*, char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)showAlertDialog;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'showAlertDialog'"));
		}
	}

	// Marshaling of parameter '____title' to native representation
	char* _____title_marshaled = { 0 };
	_____title_marshaled = il2cpp_codegen_marshal_string(____title);

	// Marshaling of parameter '____message' to native representation
	char* _____message_marshaled = { 0 };
	_____message_marshaled = il2cpp_codegen_marshal_string(____message);

	// Marshaling of parameter '____buttonsList' to native representation
	char* _____buttonsList_marshaled = { 0 };
	_____buttonsList_marshaled = il2cpp_codegen_marshal_string(____buttonsList);

	// Marshaling of parameter '____callerTag' to native representation
	char* _____callerTag_marshaled = { 0 };
	_____callerTag_marshaled = il2cpp_codegen_marshal_string(____callerTag);

	// Native function invocation
	_il2cpp_pinvoke_func(_____title_marshaled, _____message_marshaled, _____buttonsList_marshaled, _____callerTag_marshaled);

	// Marshaling cleanup of parameter '____title' native representation
	il2cpp_codegen_marshal_free(_____title_marshaled);
	_____title_marshaled = NULL;

	// Marshaling cleanup of parameter '____message' native representation
	il2cpp_codegen_marshal_free(_____message_marshaled);
	_____message_marshaled = NULL;

	// Marshaling cleanup of parameter '____buttonsList' native representation
	il2cpp_codegen_marshal_free(_____buttonsList_marshaled);
	_____buttonsList_marshaled = NULL;

	// Marshaling cleanup of parameter '____callerTag' native representation
	il2cpp_codegen_marshal_free(_____callerTag_marshaled);
	_____callerTag_marshaled = NULL;

}
// System.Void VoxelBusters.NativePlugins.UIIOS::ShowAlertDialogWithMultipleButtons(System.String,System.String,System.String[],System.String)
extern "C" void UIIOS_ShowAlertDialogWithMultipleButtons_m8_1782 (UIIOS_t8_304 * __this, String_t* ____title, String_t* ____message, StringU5BU5D_t1_238* ____buttonsList, String_t* ____callbackTag, const MethodInfo* method)
{
	{
		String_t* L_0 = ____title;
		String_t* L_1 = ____message;
		StringU5BU5D_t1_238* L_2 = ____buttonsList;
		String_t* L_3 = ____callbackTag;
		UI_ShowAlertDialogWithMultipleButtons_m8_1767(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		String_t* L_4 = ____title;
		String_t* L_5 = ____message;
		StringU5BU5D_t1_238* L_6 = ____buttonsList;
		String_t* L_7 = JSONParserExtensions_ToJSON_m8_270(NULL /*static, unused*/, (Object_t *)(Object_t *)L_6, /*hidden argument*/NULL);
		String_t* L_8 = ____callbackTag;
		UIIOS_showAlertDialog_m8_1781(NULL /*static, unused*/, L_4, L_5, L_7, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.UIIOS::ParseSingleFieldPromptClosedData(System.Collections.IDictionary,System.String&,System.String&)
extern TypeInfo* IDictionary_t1_35_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5511;
extern Il2CppCodeGenString* _stringLiteral3487;
extern "C" void UIIOS_ParseSingleFieldPromptClosedData_m8_1783 (UIIOS_t8_304 * __this, Object_t * ____dataDict, String_t** ____buttonPressed, String_t** ____inputText, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionary_t1_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5511 = il2cpp_codegen_string_literal_from_index(5511);
		_stringLiteral3487 = il2cpp_codegen_string_literal_from_index(3487);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t** L_0 = ____buttonPressed;
		Object_t * L_1 = ____dataDict;
		NullCheck(L_1);
		Object_t * L_2 = (Object_t *)InterfaceFuncInvoker1< Object_t *, Object_t * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_1, _stringLiteral5511);
		*((Object_t **)(L_0)) = (Object_t *)((String_t*)IsInstSealed(L_2, String_t_il2cpp_TypeInfo_var));
		String_t** L_3 = ____inputText;
		Object_t * L_4 = ____dataDict;
		NullCheck(L_4);
		Object_t * L_5 = (Object_t *)InterfaceFuncInvoker1< Object_t *, Object_t * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_4, _stringLiteral3487);
		*((Object_t **)(L_3)) = (Object_t *)((String_t*)IsInstSealed(L_5, String_t_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.UIIOS::ParseLoginPromptClosedData(System.Collections.IDictionary,System.String&,System.String&,System.String&)
extern TypeInfo* IDictionary_t1_35_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5511;
extern Il2CppCodeGenString* _stringLiteral5333;
extern Il2CppCodeGenString* _stringLiteral2745;
extern "C" void UIIOS_ParseLoginPromptClosedData_m8_1784 (UIIOS_t8_304 * __this, Object_t * ____dataDict, String_t** ____buttonPressed, String_t** ____usernameText, String_t** ____passwordText, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionary_t1_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5511 = il2cpp_codegen_string_literal_from_index(5511);
		_stringLiteral5333 = il2cpp_codegen_string_literal_from_index(5333);
		_stringLiteral2745 = il2cpp_codegen_string_literal_from_index(2745);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t** L_0 = ____buttonPressed;
		Object_t * L_1 = ____dataDict;
		NullCheck(L_1);
		Object_t * L_2 = (Object_t *)InterfaceFuncInvoker1< Object_t *, Object_t * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_1, _stringLiteral5511);
		*((Object_t **)(L_0)) = (Object_t *)((String_t*)IsInstSealed(L_2, String_t_il2cpp_TypeInfo_var));
		String_t** L_3 = ____usernameText;
		Object_t * L_4 = ____dataDict;
		NullCheck(L_4);
		Object_t * L_5 = (Object_t *)InterfaceFuncInvoker1< Object_t *, Object_t * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_4, _stringLiteral5333);
		*((Object_t **)(L_3)) = (Object_t *)((String_t*)IsInstSealed(L_5, String_t_il2cpp_TypeInfo_var));
		String_t** L_6 = ____passwordText;
		Object_t * L_7 = ____dataDict;
		NullCheck(L_7);
		Object_t * L_8 = (Object_t *)InterfaceFuncInvoker1< Object_t *, Object_t * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_7, _stringLiteral2745);
		*((Object_t **)(L_6)) = (Object_t *)((String_t*)IsInstSealed(L_8, String_t_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.UIIOS::showSingleFieldPromptDialog(System.String,System.String,System.String,System.Boolean,System.String)
extern "C" {void DEFAULT_CALL showSingleFieldPromptDialog(char*, char*, char*, int32_t, char*);}
extern "C" void UIIOS_showSingleFieldPromptDialog_m8_1785 (Object_t * __this /* static, unused */, String_t* ____title, String_t* ____message, String_t* ____placeholder, bool ____useSecureText, String_t* ____buttonsList, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*, char*, int32_t, char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)showSingleFieldPromptDialog;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'showSingleFieldPromptDialog'"));
		}
	}

	// Marshaling of parameter '____title' to native representation
	char* _____title_marshaled = { 0 };
	_____title_marshaled = il2cpp_codegen_marshal_string(____title);

	// Marshaling of parameter '____message' to native representation
	char* _____message_marshaled = { 0 };
	_____message_marshaled = il2cpp_codegen_marshal_string(____message);

	// Marshaling of parameter '____placeholder' to native representation
	char* _____placeholder_marshaled = { 0 };
	_____placeholder_marshaled = il2cpp_codegen_marshal_string(____placeholder);

	// Marshaling of parameter '____useSecureText' to native representation

	// Marshaling of parameter '____buttonsList' to native representation
	char* _____buttonsList_marshaled = { 0 };
	_____buttonsList_marshaled = il2cpp_codegen_marshal_string(____buttonsList);

	// Native function invocation
	_il2cpp_pinvoke_func(_____title_marshaled, _____message_marshaled, _____placeholder_marshaled, ____useSecureText, _____buttonsList_marshaled);

	// Marshaling cleanup of parameter '____title' native representation
	il2cpp_codegen_marshal_free(_____title_marshaled);
	_____title_marshaled = NULL;

	// Marshaling cleanup of parameter '____message' native representation
	il2cpp_codegen_marshal_free(_____message_marshaled);
	_____message_marshaled = NULL;

	// Marshaling cleanup of parameter '____placeholder' native representation
	il2cpp_codegen_marshal_free(_____placeholder_marshaled);
	_____placeholder_marshaled = NULL;

	// Marshaling cleanup of parameter '____useSecureText' native representation

	// Marshaling cleanup of parameter '____buttonsList' native representation
	il2cpp_codegen_marshal_free(_____buttonsList_marshaled);
	_____buttonsList_marshaled = NULL;

}
// System.Void VoxelBusters.NativePlugins.UIIOS::showLoginPromptDialog(System.String,System.String,System.String,System.String,System.String)
extern "C" {void DEFAULT_CALL showLoginPromptDialog(char*, char*, char*, char*, char*);}
extern "C" void UIIOS_showLoginPromptDialog_m8_1786 (Object_t * __this /* static, unused */, String_t* ____title, String_t* ____message, String_t* ____placeholder1, String_t* ____placeholder2, String_t* ____buttonsList, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*, char*, char*, char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)showLoginPromptDialog;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'showLoginPromptDialog'"));
		}
	}

	// Marshaling of parameter '____title' to native representation
	char* _____title_marshaled = { 0 };
	_____title_marshaled = il2cpp_codegen_marshal_string(____title);

	// Marshaling of parameter '____message' to native representation
	char* _____message_marshaled = { 0 };
	_____message_marshaled = il2cpp_codegen_marshal_string(____message);

	// Marshaling of parameter '____placeholder1' to native representation
	char* _____placeholder1_marshaled = { 0 };
	_____placeholder1_marshaled = il2cpp_codegen_marshal_string(____placeholder1);

	// Marshaling of parameter '____placeholder2' to native representation
	char* _____placeholder2_marshaled = { 0 };
	_____placeholder2_marshaled = il2cpp_codegen_marshal_string(____placeholder2);

	// Marshaling of parameter '____buttonsList' to native representation
	char* _____buttonsList_marshaled = { 0 };
	_____buttonsList_marshaled = il2cpp_codegen_marshal_string(____buttonsList);

	// Native function invocation
	_il2cpp_pinvoke_func(_____title_marshaled, _____message_marshaled, _____placeholder1_marshaled, _____placeholder2_marshaled, _____buttonsList_marshaled);

	// Marshaling cleanup of parameter '____title' native representation
	il2cpp_codegen_marshal_free(_____title_marshaled);
	_____title_marshaled = NULL;

	// Marshaling cleanup of parameter '____message' native representation
	il2cpp_codegen_marshal_free(_____message_marshaled);
	_____message_marshaled = NULL;

	// Marshaling cleanup of parameter '____placeholder1' native representation
	il2cpp_codegen_marshal_free(_____placeholder1_marshaled);
	_____placeholder1_marshaled = NULL;

	// Marshaling cleanup of parameter '____placeholder2' native representation
	il2cpp_codegen_marshal_free(_____placeholder2_marshaled);
	_____placeholder2_marshaled = NULL;

	// Marshaling cleanup of parameter '____buttonsList' native representation
	il2cpp_codegen_marshal_free(_____buttonsList_marshaled);
	_____buttonsList_marshaled = NULL;

}
// System.Void VoxelBusters.NativePlugins.UIIOS::ShowSingleFieldPromptDialog(System.String,System.String,System.String,System.Boolean,System.String[],VoxelBusters.NativePlugins.UI/SingleFieldPromptCompletion)
extern "C" void UIIOS_ShowSingleFieldPromptDialog_m8_1787 (UIIOS_t8_304 * __this, String_t* ____title, String_t* ____message, String_t* ____placeholder, bool ____useSecureText, StringU5BU5D_t1_238* ____buttonsList, SingleFieldPromptCompletion_t8_301 * ____onCompletion, const MethodInfo* method)
{
	{
		String_t* L_0 = ____title;
		String_t* L_1 = ____message;
		String_t* L_2 = ____placeholder;
		bool L_3 = ____useSecureText;
		StringU5BU5D_t1_238* L_4 = ____buttonsList;
		SingleFieldPromptCompletion_t8_301 * L_5 = ____onCompletion;
		UI_ShowSingleFieldPromptDialog_m8_1774(__this, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		String_t* L_6 = ____title;
		String_t* L_7 = ____message;
		String_t* L_8 = ____placeholder;
		bool L_9 = ____useSecureText;
		StringU5BU5D_t1_238* L_10 = ____buttonsList;
		String_t* L_11 = JSONParserExtensions_ToJSON_m8_270(NULL /*static, unused*/, (Object_t *)(Object_t *)L_10, /*hidden argument*/NULL);
		UIIOS_showSingleFieldPromptDialog_m8_1785(NULL /*static, unused*/, L_6, L_7, L_8, L_9, L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.UIIOS::ShowLoginPromptDialog(System.String,System.String,System.String,System.String,System.String[],VoxelBusters.NativePlugins.UI/LoginPromptCompletion)
extern "C" void UIIOS_ShowLoginPromptDialog_m8_1788 (UIIOS_t8_304 * __this, String_t* ____title, String_t* ____message, String_t* ____placeholder1, String_t* ____placeholder2, StringU5BU5D_t1_238* ____buttonsList, LoginPromptCompletion_t8_302 * ____onCompletion, const MethodInfo* method)
{
	{
		String_t* L_0 = ____title;
		String_t* L_1 = ____message;
		String_t* L_2 = ____placeholder1;
		String_t* L_3 = ____placeholder2;
		StringU5BU5D_t1_238* L_4 = ____buttonsList;
		LoginPromptCompletion_t8_302 * L_5 = ____onCompletion;
		UI_ShowLoginPromptDialog_m8_1775(__this, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		String_t* L_6 = ____title;
		String_t* L_7 = ____message;
		String_t* L_8 = ____placeholder1;
		String_t* L_9 = ____placeholder2;
		StringU5BU5D_t1_238* L_10 = ____buttonsList;
		String_t* L_11 = JSONParserExtensions_ToJSON_m8_270(NULL /*static, unused*/, (Object_t *)(Object_t *)L_10, /*hidden argument*/NULL);
		UIIOS_showLoginPromptDialog_m8_1786(NULL /*static, unused*/, L_6, L_7, L_8, L_9, L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.UIIOS::setPopoverPoint(System.Single,System.Single)
extern "C" {void DEFAULT_CALL setPopoverPoint(float, float);}
extern "C" void UIIOS_setPopoverPoint_m8_1789 (Object_t * __this /* static, unused */, float ___x, float ___y, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (float, float);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)setPopoverPoint;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'setPopoverPoint'"));
		}
	}

	// Marshaling of parameter '___x' to native representation

	// Marshaling of parameter '___y' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___x, ___y);

	// Marshaling cleanup of parameter '___x' native representation

	// Marshaling cleanup of parameter '___y' native representation

}
// System.Void VoxelBusters.NativePlugins.UIIOS::ShowToast(System.String,VoxelBusters.NativePlugins.eToastMessageLength)
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5513;
extern "C" void UIIOS_ShowToast_m8_1790 (UIIOS_t8_304 * __this, String_t* ____message, int32_t ____length, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5513 = il2cpp_codegen_string_literal_from_index(5513);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_LogWarning_m8_1008(NULL /*static, unused*/, _stringLiteral5368, _stringLiteral5513, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.UIIOS::SetPopoverPoint(UnityEngine.Vector2)
extern "C" void UIIOS_SetPopoverPoint_m8_1791 (UIIOS_t8_304 * __this, Vector2_t6_47  ____position, const MethodInfo* method)
{
	{
		float L_0 = ((&____position)->___x_1);
		float L_1 = ((&____position)->___y_2);
		UIIOS_setPopoverPoint_m8_1789(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Utility::.ctor()
extern "C" void Utility__ctor_m8_1792 (Utility_t8_306 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m6_671(__this, /*hidden argument*/NULL);
		return;
	}
}
// VoxelBusters.NativePlugins.RateMyApp VoxelBusters.NativePlugins.Utility::get_RateMyApp()
extern "C" RateMyApp_t8_307 * Utility_get_RateMyApp_m8_1793 (Utility_t8_306 * __this, const MethodInfo* method)
{
	{
		RateMyApp_t8_307 * L_0 = (__this->___m_rateMyApp_2);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.Utility::set_RateMyApp(VoxelBusters.NativePlugins.RateMyApp)
extern "C" void Utility_set_RateMyApp_m8_1794 (Utility_t8_306 * __this, RateMyApp_t8_307 * ___value, const MethodInfo* method)
{
	{
		RateMyApp_t8_307 * L_0 = ___value;
		__this->___m_rateMyApp_2 = L_0;
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Utility::Awake()
extern const MethodInfo* GameObject_AddComponent_TisRateMyApp_t8_307_m6_1927_MethodInfo_var;
extern "C" void Utility_Awake_m8_1795 (Utility_t8_306 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_AddComponent_TisRateMyApp_t8_307_m6_1927_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484246);
		s_Il2CppMethodIntialized = true;
	}
	{
		UtilitySettings_t8_309 * L_0 = NPSettings_get_Utility_m8_1935(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Settings_t8_310 * L_1 = UtilitySettings_get_RateMyApp_m8_1806(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_2 = Settings_get_IsEnabled_m8_1941(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		GameObject_t6_97 * L_3 = Component_get_gameObject_m6_725(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		RateMyApp_t8_307 * L_4 = GameObject_AddComponent_TisRateMyApp_t8_307_m6_1927(L_3, /*hidden argument*/GameObject_AddComponent_TisRateMyApp_t8_307_m6_1927_MethodInfo_var);
		__this->___m_rateMyApp_2 = L_4;
	}

IL_0025:
	{
		return;
	}
}
// System.String VoxelBusters.NativePlugins.Utility::GetUUID()
extern TypeInfo* Guid_t1_319_il2cpp_TypeInfo_var;
extern "C" String_t* Utility_GetUUID_m8_1796 (Utility_t8_306 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Guid_t1_319_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(208);
		s_Il2CppMethodIntialized = true;
	}
	Guid_t1_319  V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Guid_t1_319_il2cpp_TypeInfo_var);
		Guid_t1_319  L_0 = Guid_NewGuid_m1_14146(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = Guid_ToString_m1_14153((&V_0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void VoxelBusters.NativePlugins.Utility::OpenStoreLink(System.String)
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5514;
extern "C" void Utility_OpenStoreLink_m8_1797 (Utility_t8_306 * __this, String_t* ____applicationID, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5514 = il2cpp_codegen_string_literal_from_index(5514);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_LogError_m8_1009(NULL /*static, unused*/, _stringLiteral5368, _stringLiteral5514, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Utility::SetApplicationIconBadgeNumber(System.Int32)
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5514;
extern "C" void Utility_SetApplicationIconBadgeNumber_m8_1798 (Utility_t8_306 * __this, int32_t ____badgeNumber, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5514 = il2cpp_codegen_string_literal_from_index(5514);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_LogError_m8_1009(NULL /*static, unused*/, _stringLiteral5368, _stringLiteral5514, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.String VoxelBusters.NativePlugins.Utility::GetBundleVersion()
extern "C" String_t* Utility_GetBundleVersion_m8_1799 (Utility_t8_306 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = PlayerSettings_GetBundleVersion_m8_213(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String VoxelBusters.NativePlugins.Utility::GetBundleIdentifier()
extern "C" String_t* Utility_GetBundleIdentifier_m8_1800 (Utility_t8_306 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = PlayerSettings_GetBundleIdentifier_m8_214(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.UtilityIOS::.ctor()
extern "C" void UtilityIOS__ctor_m8_1801 (UtilityIOS_t8_308 * __this, const MethodInfo* method)
{
	{
		Utility__ctor_m8_1792(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.UtilityIOS::setApplicationIconBadgeNumber(System.Int32)
extern "C" {void DEFAULT_CALL setApplicationIconBadgeNumber(int32_t);}
extern "C" void UtilityIOS_setApplicationIconBadgeNumber_m8_1802 (Object_t * __this /* static, unused */, int32_t ____badgeNumber, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)setApplicationIconBadgeNumber;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'setApplicationIconBadgeNumber'"));
		}
	}

	// Marshaling of parameter '____badgeNumber' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____badgeNumber);

	// Marshaling cleanup of parameter '____badgeNumber' native representation

}
// System.Void VoxelBusters.NativePlugins.UtilityIOS::OpenStoreLink(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5515;
extern Il2CppCodeGenString* _stringLiteral5516;
extern Il2CppCodeGenString* _stringLiteral5517;
extern Il2CppCodeGenString* _stringLiteral5518;
extern "C" void UtilityIOS_OpenStoreLink_m8_1803 (UtilityIOS_t8_308 * __this, String_t* ____applicationID, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5515 = il2cpp_codegen_string_literal_from_index(5515);
		_stringLiteral5516 = il2cpp_codegen_string_literal_from_index(5516);
		_stringLiteral5517 = il2cpp_codegen_string_literal_from_index(5517);
		_stringLiteral5518 = il2cpp_codegen_string_literal_from_index(5518);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	String_t* V_1 = {0};
	{
		String_t* L_0 = ____applicationID;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m1_559(NULL /*static, unused*/, _stringLiteral5515, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_Log_m8_1007(NULL /*static, unused*/, _stringLiteral5368, L_1, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		String_t* L_2 = SystemInfo_get_operatingSystem_m6_9(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_2;
		V_1 = (String_t*)NULL;
		String_t* L_3 = V_0;
		NullCheck(L_3);
		int32_t L_4 = String_CompareTo_m1_476(L_3, _stringLiteral5516, /*hidden argument*/NULL);
		if ((((int32_t)L_4) < ((int32_t)0)))
		{
			goto IL_0040;
		}
	}
	{
		String_t* L_5 = ____applicationID;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Format_m1_548(NULL /*static, unused*/, _stringLiteral5517, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		goto IL_004c;
	}

IL_0040:
	{
		String_t* L_7 = ____applicationID;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Format_m1_548(NULL /*static, unused*/, _stringLiteral5518, L_7, /*hidden argument*/NULL);
		V_1 = L_8;
	}

IL_004c:
	{
		String_t* L_9 = V_1;
		Application_OpenURL_m6_592(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.UtilityIOS::SetApplicationIconBadgeNumber(System.Int32)
extern "C" void UtilityIOS_SetApplicationIconBadgeNumber_m8_1804 (UtilityIOS_t8_308 * __this, int32_t ____badgeNumber, const MethodInfo* method)
{
	{
		int32_t L_0 = ____badgeNumber;
		UtilityIOS_setApplicationIconBadgeNumber_m8_1802(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.UtilitySettings::.ctor()
extern "C" void UtilitySettings__ctor_m8_1805 (UtilitySettings_t8_309 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// VoxelBusters.NativePlugins.RateMyApp/Settings VoxelBusters.NativePlugins.UtilitySettings::get_RateMyApp()
extern "C" Settings_t8_310 * UtilitySettings_get_RateMyApp_m8_1806 (UtilitySettings_t8_309 * __this, const MethodInfo* method)
{
	{
		Settings_t8_310 * L_0 = (__this->___m_rateMyApp_0);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.UtilitySettings::set_RateMyApp(VoxelBusters.NativePlugins.RateMyApp/Settings)
extern "C" void UtilitySettings_set_RateMyApp_m8_1807 (UtilitySettings_t8_309 * __this, Settings_t8_310 * ___value, const MethodInfo* method)
{
	{
		Settings_t8_310 * L_0 = ___value;
		__this->___m_rateMyApp_0 = L_0;
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::.ctor()
extern TypeInfo* Dictionary_2_t1_1912_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_15068_MethodInfo_var;
extern "C" void WebViewNative__ctor_m8_1808 (WebViewNative_t8_311 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t1_1912_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2170);
		Dictionary_2__ctor_m1_15068_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484247);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t1_1912 * L_0 = (Dictionary_2_t1_1912 *)il2cpp_codegen_object_new (Dictionary_2_t1_1912_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_15068(L_0, /*hidden argument*/Dictionary_2__ctor_m1_15068_MethodInfo_var);
		__this->___m_webviewCollection_10 = L_0;
		MonoBehaviour__ctor_m6_671(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::WebViewDidShow(System.String)
extern "C" void WebViewNative_WebViewDidShow_m8_1809 (WebViewNative_t8_311 * __this, String_t* ____tag, const MethodInfo* method)
{
	{
		String_t* L_0 = ____tag;
		WebView_t8_191 * L_1 = WebViewNative_GetWebViewWithTag_m8_1848(__this, L_0, /*hidden argument*/NULL);
		WebViewNative_WebViewDidShow_m8_1810(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::WebViewDidShow(VoxelBusters.NativePlugins.WebView)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5519;
extern Il2CppCodeGenString* _stringLiteral5520;
extern "C" void WebViewNative_WebViewDidShow_m8_1810 (WebViewNative_t8_311 * __this, WebView_t8_191 * ____webview, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5519 = il2cpp_codegen_string_literal_from_index(5519);
		_stringLiteral5520 = il2cpp_codegen_string_literal_from_index(5520);
		s_Il2CppMethodIntialized = true;
	}
	{
		WebView_t8_191 * L_0 = ____webview;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m1_556(NULL /*static, unused*/, _stringLiteral5519, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_Log_m8_1007(NULL /*static, unused*/, _stringLiteral5368, L_1, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		WebView_t8_191 * L_2 = ____webview;
		bool L_3 = Object_op_Inequality_m6_722(NULL /*static, unused*/, L_2, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		WebView_t8_191 * L_4 = ____webview;
		ReflectionExtensions_InvokeMethod_m8_219(NULL /*static, unused*/, L_4, _stringLiteral5520, NULL, /*hidden argument*/NULL);
	}

IL_002e:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::WebViewDidHide(System.String)
extern "C" void WebViewNative_WebViewDidHide_m8_1811 (WebViewNative_t8_311 * __this, String_t* ____tag, const MethodInfo* method)
{
	{
		String_t* L_0 = ____tag;
		WebView_t8_191 * L_1 = WebViewNative_GetWebViewWithTag_m8_1848(__this, L_0, /*hidden argument*/NULL);
		WebViewNative_WebViewDidHide_m8_1812(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::WebViewDidHide(VoxelBusters.NativePlugins.WebView)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5521;
extern Il2CppCodeGenString* _stringLiteral5522;
extern "C" void WebViewNative_WebViewDidHide_m8_1812 (WebViewNative_t8_311 * __this, WebView_t8_191 * ____webview, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5521 = il2cpp_codegen_string_literal_from_index(5521);
		_stringLiteral5522 = il2cpp_codegen_string_literal_from_index(5522);
		s_Il2CppMethodIntialized = true;
	}
	{
		WebView_t8_191 * L_0 = ____webview;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m1_556(NULL /*static, unused*/, _stringLiteral5521, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_Log_m8_1007(NULL /*static, unused*/, _stringLiteral5368, L_1, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		WebView_t8_191 * L_2 = ____webview;
		bool L_3 = Object_op_Inequality_m6_722(NULL /*static, unused*/, L_2, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		WebView_t8_191 * L_4 = ____webview;
		ReflectionExtensions_InvokeMethod_m8_219(NULL /*static, unused*/, L_4, _stringLiteral5522, NULL, /*hidden argument*/NULL);
	}

IL_002e:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::WebViewDidDestroy(System.String)
extern "C" void WebViewNative_WebViewDidDestroy_m8_1813 (WebViewNative_t8_311 * __this, String_t* ____tag, const MethodInfo* method)
{
	{
		String_t* L_0 = ____tag;
		WebView_t8_191 * L_1 = WebViewNative_GetWebViewWithTag_m8_1848(__this, L_0, /*hidden argument*/NULL);
		WebViewNative_WebViewDidDestroy_m8_1814(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::WebViewDidDestroy(VoxelBusters.NativePlugins.WebView)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5523;
extern Il2CppCodeGenString* _stringLiteral5524;
extern "C" void WebViewNative_WebViewDidDestroy_m8_1814 (WebViewNative_t8_311 * __this, WebView_t8_191 * ____webview, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5523 = il2cpp_codegen_string_literal_from_index(5523);
		_stringLiteral5524 = il2cpp_codegen_string_literal_from_index(5524);
		s_Il2CppMethodIntialized = true;
	}
	{
		WebView_t8_191 * L_0 = ____webview;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m1_556(NULL /*static, unused*/, _stringLiteral5523, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_Log_m8_1007(NULL /*static, unused*/, _stringLiteral5368, L_1, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		WebView_t8_191 * L_2 = ____webview;
		bool L_3 = Object_op_Inequality_m6_722(NULL /*static, unused*/, L_2, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		WebView_t8_191 * L_4 = ____webview;
		ReflectionExtensions_InvokeMethod_m8_219(NULL /*static, unused*/, L_4, _stringLiteral5524, NULL, /*hidden argument*/NULL);
	}

IL_002e:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::WebViewDidStartLoad(System.String)
extern "C" void WebViewNative_WebViewDidStartLoad_m8_1815 (WebViewNative_t8_311 * __this, String_t* ____tag, const MethodInfo* method)
{
	{
		String_t* L_0 = ____tag;
		WebView_t8_191 * L_1 = WebViewNative_GetWebViewWithTag_m8_1848(__this, L_0, /*hidden argument*/NULL);
		WebViewNative_WebViewDidStartLoad_m8_1816(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::WebViewDidStartLoad(VoxelBusters.NativePlugins.WebView)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5525;
extern Il2CppCodeGenString* _stringLiteral5526;
extern "C" void WebViewNative_WebViewDidStartLoad_m8_1816 (WebViewNative_t8_311 * __this, WebView_t8_191 * ____webview, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5525 = il2cpp_codegen_string_literal_from_index(5525);
		_stringLiteral5526 = il2cpp_codegen_string_literal_from_index(5526);
		s_Il2CppMethodIntialized = true;
	}
	{
		WebView_t8_191 * L_0 = ____webview;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m1_556(NULL /*static, unused*/, _stringLiteral5525, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_Log_m8_1007(NULL /*static, unused*/, _stringLiteral5368, L_1, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		WebView_t8_191 * L_2 = ____webview;
		bool L_3 = Object_op_Inequality_m6_722(NULL /*static, unused*/, L_2, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		WebView_t8_191 * L_4 = ____webview;
		ReflectionExtensions_InvokeMethod_m8_219(NULL /*static, unused*/, L_4, _stringLiteral5526, NULL, /*hidden argument*/NULL);
	}

IL_002e:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::WebViewDidFinishLoad(System.String)
extern "C" void WebViewNative_WebViewDidFinishLoad_m8_1817 (WebViewNative_t8_311 * __this, String_t* ____tag, const MethodInfo* method)
{
	{
		String_t* L_0 = ____tag;
		WebView_t8_191 * L_1 = WebViewNative_GetWebViewWithTag_m8_1848(__this, L_0, /*hidden argument*/NULL);
		WebViewNative_WebViewDidFinishLoad_m8_1818(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::WebViewDidFinishLoad(VoxelBusters.NativePlugins.WebView)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5527;
extern Il2CppCodeGenString* _stringLiteral5528;
extern "C" void WebViewNative_WebViewDidFinishLoad_m8_1818 (WebViewNative_t8_311 * __this, WebView_t8_191 * ____webview, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5527 = il2cpp_codegen_string_literal_from_index(5527);
		_stringLiteral5528 = il2cpp_codegen_string_literal_from_index(5528);
		s_Il2CppMethodIntialized = true;
	}
	{
		WebView_t8_191 * L_0 = ____webview;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m1_556(NULL /*static, unused*/, _stringLiteral5527, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_Log_m8_1007(NULL /*static, unused*/, _stringLiteral5368, L_1, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		WebView_t8_191 * L_2 = ____webview;
		bool L_3 = Object_op_Inequality_m6_722(NULL /*static, unused*/, L_2, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		WebView_t8_191 * L_4 = ____webview;
		ReflectionExtensions_InvokeMethod_m8_219(NULL /*static, unused*/, L_4, _stringLiteral5528, NULL, /*hidden argument*/NULL);
	}

IL_002e:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::WebViewDidFailLoadWithError(System.String)
extern TypeInfo* IDictionary_t1_35_il2cpp_TypeInfo_var;
extern "C" void WebViewNative_WebViewDidFailLoadWithError_m8_1819 (WebViewNative_t8_311 * __this, String_t* ____errorJsonStr, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionary_t1_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	String_t* V_1 = {0};
	String_t* V_2 = {0};
	{
		String_t* L_0 = ____errorJsonStr;
		Object_t * L_1 = JSONUtility_FromJSON_m8_298(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = ((Object_t *)IsInst(L_1, IDictionary_t1_35_il2cpp_TypeInfo_var));
		Object_t * L_2 = V_0;
		VirtActionInvoker3< Object_t *, String_t**, String_t** >::Invoke(4 /* System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::ParseLoadErrorData(System.Collections.IDictionary,System.String&,System.String&) */, __this, L_2, (&V_1), (&V_2));
		String_t* L_3 = V_1;
		WebView_t8_191 * L_4 = WebViewNative_GetWebViewWithTag_m8_1848(__this, L_3, /*hidden argument*/NULL);
		String_t* L_5 = V_2;
		WebViewNative_WebViewDidFailLoadWithError_m8_1820(__this, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::WebViewDidFailLoadWithError(VoxelBusters.NativePlugins.WebView,System.String)
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5529;
extern Il2CppCodeGenString* _stringLiteral5530;
extern Il2CppCodeGenString* _stringLiteral5531;
extern "C" void WebViewNative_WebViewDidFailLoadWithError_m8_1820 (WebViewNative_t8_311 * __this, WebView_t8_191 * ____webview, String_t* ____error, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5529 = il2cpp_codegen_string_literal_from_index(5529);
		_stringLiteral5530 = il2cpp_codegen_string_literal_from_index(5530);
		_stringLiteral5531 = il2cpp_codegen_string_literal_from_index(5531);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_272* L_0 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 4));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral5529);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral5529;
		ObjectU5BU5D_t1_272* L_1 = L_0;
		WebView_t8_191 * L_2 = ____webview;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_2);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_1, 1, sizeof(Object_t *))) = (Object_t *)L_2;
		ObjectU5BU5D_t1_272* L_3 = L_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 2);
		ArrayElementTypeCheck (L_3, _stringLiteral5530);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_3, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral5530;
		ObjectU5BU5D_t1_272* L_4 = L_3;
		String_t* L_5 = ____error;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 3);
		ArrayElementTypeCheck (L_4, L_5);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 3, sizeof(Object_t *))) = (Object_t *)L_5;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m1_562(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_Log_m8_1007(NULL /*static, unused*/, _stringLiteral5368, L_6, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		WebView_t8_191 * L_7 = ____webview;
		bool L_8 = Object_op_Inequality_m6_722(NULL /*static, unused*/, L_7, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0046;
		}
	}
	{
		WebView_t8_191 * L_9 = ____webview;
		String_t* L_10 = ____error;
		ReflectionExtensions_InvokeMethod_m8_219(NULL /*static, unused*/, L_9, _stringLiteral5531, L_10, /*hidden argument*/NULL);
	}

IL_0046:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::WebViewDidFinishEvaluatingJS(System.String)
extern TypeInfo* IDictionary_t1_35_il2cpp_TypeInfo_var;
extern "C" void WebViewNative_WebViewDidFinishEvaluatingJS_m8_1821 (WebViewNative_t8_311 * __this, String_t* ____resultJsonStr, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionary_t1_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	String_t* V_1 = {0};
	String_t* V_2 = {0};
	{
		String_t* L_0 = ____resultJsonStr;
		Object_t * L_1 = JSONUtility_FromJSON_m8_298(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = ((Object_t *)IsInst(L_1, IDictionary_t1_35_il2cpp_TypeInfo_var));
		Object_t * L_2 = V_0;
		VirtActionInvoker3< Object_t *, String_t**, String_t** >::Invoke(5 /* System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::ParseEvalJSData(System.Collections.IDictionary,System.String&,System.String&) */, __this, L_2, (&V_1), (&V_2));
		String_t* L_3 = V_1;
		WebView_t8_191 * L_4 = WebViewNative_GetWebViewWithTag_m8_1848(__this, L_3, /*hidden argument*/NULL);
		String_t* L_5 = V_2;
		WebViewNative_WebViewDidFinishEvaluatingJS_m8_1822(__this, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::WebViewDidFinishEvaluatingJS(VoxelBusters.NativePlugins.WebView,System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5532;
extern Il2CppCodeGenString* _stringLiteral5533;
extern "C" void WebViewNative_WebViewDidFinishEvaluatingJS_m8_1822 (WebViewNative_t8_311 * __this, WebView_t8_191 * ____webview, String_t* ____result, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5532 = il2cpp_codegen_string_literal_from_index(5532);
		_stringLiteral5533 = il2cpp_codegen_string_literal_from_index(5533);
		s_Il2CppMethodIntialized = true;
	}
	{
		WebView_t8_191 * L_0 = ____webview;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m1_556(NULL /*static, unused*/, _stringLiteral5532, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_Log_m8_1007(NULL /*static, unused*/, _stringLiteral5368, L_1, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		WebView_t8_191 * L_2 = ____webview;
		bool L_3 = Object_op_Inequality_m6_722(NULL /*static, unused*/, L_2, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		WebView_t8_191 * L_4 = ____webview;
		String_t* L_5 = ____result;
		ReflectionExtensions_InvokeMethod_m8_219(NULL /*static, unused*/, L_4, _stringLiteral5533, L_5, /*hidden argument*/NULL);
	}

IL_002e:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::WebViewDidReceiveMessage(System.String)
extern TypeInfo* IDictionary_t1_35_il2cpp_TypeInfo_var;
extern "C" void WebViewNative_WebViewDidReceiveMessage_m8_1823 (WebViewNative_t8_311 * __this, String_t* ____dataJsonStr, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionary_t1_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	String_t* V_1 = {0};
	WebViewMessage_t8_314 * V_2 = {0};
	{
		String_t* L_0 = ____dataJsonStr;
		Object_t * L_1 = JSONUtility_FromJSON_m8_298(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = ((Object_t *)IsInst(L_1, IDictionary_t1_35_il2cpp_TypeInfo_var));
		Object_t * L_2 = V_0;
		VirtActionInvoker3< Object_t *, String_t**, WebViewMessage_t8_314 ** >::Invoke(6 /* System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::ParseMessageData(System.Collections.IDictionary,System.String&,VoxelBusters.NativePlugins.WebViewMessage&) */, __this, L_2, (&V_1), (&V_2));
		String_t* L_3 = V_1;
		WebView_t8_191 * L_4 = WebViewNative_GetWebViewWithTag_m8_1848(__this, L_3, /*hidden argument*/NULL);
		WebViewMessage_t8_314 * L_5 = V_2;
		WebViewNative_WebViewDidReceiveMessage_m8_1824(__this, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::WebViewDidReceiveMessage(VoxelBusters.NativePlugins.WebView,VoxelBusters.NativePlugins.WebViewMessage)
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5534;
extern Il2CppCodeGenString* _stringLiteral5535;
extern Il2CppCodeGenString* _stringLiteral5536;
extern "C" void WebViewNative_WebViewDidReceiveMessage_m8_1824 (WebViewNative_t8_311 * __this, WebView_t8_191 * ____webview, WebViewMessage_t8_314 * ____message, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5534 = il2cpp_codegen_string_literal_from_index(5534);
		_stringLiteral5535 = il2cpp_codegen_string_literal_from_index(5535);
		_stringLiteral5536 = il2cpp_codegen_string_literal_from_index(5536);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_272* L_0 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 4));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral5534);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral5534;
		ObjectU5BU5D_t1_272* L_1 = L_0;
		WebView_t8_191 * L_2 = ____webview;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_2);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_1, 1, sizeof(Object_t *))) = (Object_t *)L_2;
		ObjectU5BU5D_t1_272* L_3 = L_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 2);
		ArrayElementTypeCheck (L_3, _stringLiteral5535);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_3, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral5535;
		ObjectU5BU5D_t1_272* L_4 = L_3;
		WebViewMessage_t8_314 * L_5 = ____message;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 3);
		ArrayElementTypeCheck (L_4, L_5);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 3, sizeof(Object_t *))) = (Object_t *)L_5;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m1_562(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_Log_m8_1007(NULL /*static, unused*/, _stringLiteral5368, L_6, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		WebView_t8_191 * L_7 = ____webview;
		bool L_8 = Object_op_Inequality_m6_722(NULL /*static, unused*/, L_7, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0046;
		}
	}
	{
		WebView_t8_191 * L_9 = ____webview;
		WebViewMessage_t8_314 * L_10 = ____message;
		ReflectionExtensions_InvokeMethod_m8_219(NULL /*static, unused*/, L_9, _stringLiteral5536, L_10, /*hidden argument*/NULL);
	}

IL_0046:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::ParseLoadErrorData(System.Collections.IDictionary,System.String&,System.String&)
extern "C" void WebViewNative_ParseLoadErrorData_m8_1825 (WebViewNative_t8_311 * __this, Object_t * ____dataDict, String_t** ____tag, String_t** ____error, const MethodInfo* method)
{
	{
		String_t** L_0 = ____tag;
		*((Object_t **)(L_0)) = (Object_t *)NULL;
		String_t** L_1 = ____error;
		*((Object_t **)(L_1)) = (Object_t *)NULL;
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::ParseEvalJSData(System.Collections.IDictionary,System.String&,System.String&)
extern "C" void WebViewNative_ParseEvalJSData_m8_1826 (WebViewNative_t8_311 * __this, Object_t * ____resultData, String_t** ____tag, String_t** ____result, const MethodInfo* method)
{
	{
		String_t** L_0 = ____tag;
		*((Object_t **)(L_0)) = (Object_t *)NULL;
		String_t** L_1 = ____result;
		*((Object_t **)(L_1)) = (Object_t *)NULL;
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::ParseMessageData(System.Collections.IDictionary,System.String&,VoxelBusters.NativePlugins.WebViewMessage&)
extern "C" void WebViewNative_ParseMessageData_m8_1827 (WebViewNative_t8_311 * __this, Object_t * ____dataDict, String_t** ____tag, WebViewMessage_t8_314 ** ____message, const MethodInfo* method)
{
	{
		String_t** L_0 = ____tag;
		*((Object_t **)(L_0)) = (Object_t *)NULL;
		WebViewMessage_t8_314 ** L_1 = ____message;
		*((Object_t **)(L_1)) = (Object_t *)NULL;
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::Create(VoxelBusters.NativePlugins.WebView,UnityEngine.Rect)
extern "C" void WebViewNative_Create_m8_1828 (WebViewNative_t8_311 * __this, WebView_t8_191 * ____webview, Rect_t6_51  ____frame, const MethodInfo* method)
{
	{
		WebView_t8_191 * L_0 = ____webview;
		NullCheck(L_0);
		String_t* L_1 = WebView_get_UniqueID_m8_1966(L_0, /*hidden argument*/NULL);
		WebView_t8_191 * L_2 = ____webview;
		WebViewNative_AddWebViewToCollection_m8_1849(__this, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::Destroy(VoxelBusters.NativePlugins.WebView)
extern "C" void WebViewNative_Destroy_m8_1829 (WebViewNative_t8_311 * __this, WebView_t8_191 * ____webview, const MethodInfo* method)
{
	{
		WebView_t8_191 * L_0 = ____webview;
		NullCheck(L_0);
		String_t* L_1 = WebView_get_UniqueID_m8_1966(L_0, /*hidden argument*/NULL);
		WebViewNative_RemoveWebViewFromCollection_m8_1850(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::Show(VoxelBusters.NativePlugins.WebView)
extern "C" void WebViewNative_Show_m8_1830 (WebViewNative_t8_311 * __this, WebView_t8_191 * ____webview, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::Hide(VoxelBusters.NativePlugins.WebView)
extern "C" void WebViewNative_Hide_m8_1831 (WebViewNative_t8_311 * __this, WebView_t8_191 * ____webview, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::LoadRequest(System.String,VoxelBusters.NativePlugins.WebView)
extern "C" void WebViewNative_LoadRequest_m8_1832 (WebViewNative_t8_311 * __this, String_t* ____URL, WebView_t8_191 * ____webview, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::LoadHTMLString(System.String,System.String,VoxelBusters.NativePlugins.WebView)
extern "C" void WebViewNative_LoadHTMLString_m8_1833 (WebViewNative_t8_311 * __this, String_t* ____HTMLString, String_t* ____baseURL, WebView_t8_191 * ____webview, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::LoadData(System.Byte[],System.String,System.String,System.String,VoxelBusters.NativePlugins.WebView)
extern "C" void WebViewNative_LoadData_m8_1834 (WebViewNative_t8_311 * __this, ByteU5BU5D_t1_109* ____byteArray, String_t* ____MIMEType, String_t* ____textEncodingName, String_t* ____baseURL, WebView_t8_191 * ____webview, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::EvaluateJavaScriptFromString(System.String,VoxelBusters.NativePlugins.WebView)
extern "C" void WebViewNative_EvaluateJavaScriptFromString_m8_1835 (WebViewNative_t8_311 * __this, String_t* ____javaScript, WebView_t8_191 * ____webview, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::Reload(VoxelBusters.NativePlugins.WebView)
extern "C" void WebViewNative_Reload_m8_1836 (WebViewNative_t8_311 * __this, WebView_t8_191 * ____webview, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::StopLoading(VoxelBusters.NativePlugins.WebView)
extern "C" void WebViewNative_StopLoading_m8_1837 (WebViewNative_t8_311 * __this, WebView_t8_191 * ____webview, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::SetCanHide(System.Boolean,VoxelBusters.NativePlugins.WebView)
extern "C" void WebViewNative_SetCanHide_m8_1838 (WebViewNative_t8_311 * __this, bool ____canHide, WebView_t8_191 * ____webview, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::SetCanBounce(System.Boolean,VoxelBusters.NativePlugins.WebView)
extern "C" void WebViewNative_SetCanBounce_m8_1839 (WebViewNative_t8_311 * __this, bool ____canBounce, WebView_t8_191 * ____webview, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::SetControlType(VoxelBusters.NativePlugins.eWebviewControlType,VoxelBusters.NativePlugins.WebView)
extern "C" void WebViewNative_SetControlType_m8_1840 (WebViewNative_t8_311 * __this, int32_t ____type, WebView_t8_191 * ____webview, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::SetShowSpinnerOnLoad(System.Boolean,VoxelBusters.NativePlugins.WebView)
extern "C" void WebViewNative_SetShowSpinnerOnLoad_m8_1841 (WebViewNative_t8_311 * __this, bool ____showSpinner, WebView_t8_191 * ____webview, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::SetAutoShowOnLoadFinish(System.Boolean,VoxelBusters.NativePlugins.WebView)
extern "C" void WebViewNative_SetAutoShowOnLoadFinish_m8_1842 (WebViewNative_t8_311 * __this, bool ____autoShow, WebView_t8_191 * ____webview, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::SetScalesPageToFit(System.Boolean,VoxelBusters.NativePlugins.WebView)
extern "C" void WebViewNative_SetScalesPageToFit_m8_1843 (WebViewNative_t8_311 * __this, bool ____scaleToFit, WebView_t8_191 * ____webview, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::SetFrame(UnityEngine.Rect,VoxelBusters.NativePlugins.WebView)
extern "C" void WebViewNative_SetFrame_m8_1844 (WebViewNative_t8_311 * __this, Rect_t6_51  ____frame, WebView_t8_191 * ____webview, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::SetBackgroundColor(UnityEngine.Color,VoxelBusters.NativePlugins.WebView)
extern "C" void WebViewNative_SetBackgroundColor_m8_1845 (WebViewNative_t8_311 * __this, Color_t6_40  ____color, WebView_t8_191 * ____webview, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::AddNewURLSchemeName(System.String,VoxelBusters.NativePlugins.WebView)
extern "C" void WebViewNative_AddNewURLSchemeName_m8_1846 (WebViewNative_t8_311 * __this, String_t* ____newURLScheme, WebView_t8_191 * ____webview, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::ClearCache()
extern "C" void WebViewNative_ClearCache_m8_1847 (WebViewNative_t8_311 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// VoxelBusters.NativePlugins.WebView VoxelBusters.NativePlugins.Internal.WebViewNative::GetWebViewWithTag(System.String)
extern "C" WebView_t8_191 * WebViewNative_GetWebViewWithTag_m8_1848 (WebViewNative_t8_311 * __this, String_t* ____tag, const MethodInfo* method)
{
	{
		Dictionary_2_t1_1912 * L_0 = (__this->___m_webviewCollection_10);
		String_t* L_1 = ____tag;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,VoxelBusters.NativePlugins.WebView>::ContainsKey(!0) */, L_0, L_1);
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		Dictionary_2_t1_1912 * L_3 = (__this->___m_webviewCollection_10);
		String_t* L_4 = ____tag;
		NullCheck(L_3);
		WebView_t8_191 * L_5 = (WebView_t8_191 *)VirtFuncInvoker1< WebView_t8_191 *, String_t* >::Invoke(22 /* !1 System.Collections.Generic.Dictionary`2<System.String,VoxelBusters.NativePlugins.WebView>::get_Item(!0) */, L_3, L_4);
		return L_5;
	}

IL_001e:
	{
		return (WebView_t8_191 *)NULL;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::AddWebViewToCollection(System.String,VoxelBusters.NativePlugins.WebView)
extern "C" void WebViewNative_AddWebViewToCollection_m8_1849 (WebViewNative_t8_311 * __this, String_t* ____tag, WebView_t8_191 * ____webview, const MethodInfo* method)
{
	{
		Dictionary_2_t1_1912 * L_0 = (__this->___m_webviewCollection_10);
		String_t* L_1 = ____tag;
		WebView_t8_191 * L_2 = ____webview;
		NullCheck(L_0);
		VirtActionInvoker2< String_t*, WebView_t8_191 * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,VoxelBusters.NativePlugins.WebView>::set_Item(!0,!1) */, L_0, L_1, L_2);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::RemoveWebViewFromCollection(System.String)
extern "C" void WebViewNative_RemoveWebViewFromCollection_m8_1850 (WebViewNative_t8_311 * __this, String_t* ____tag, const MethodInfo* method)
{
	{
		Dictionary_2_t1_1912 * L_0 = (__this->___m_webviewCollection_10);
		String_t* L_1 = ____tag;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,VoxelBusters.NativePlugins.WebView>::ContainsKey(!0) */, L_0, L_1);
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		Dictionary_2_t1_1912 * L_3 = (__this->___m_webviewCollection_10);
		String_t* L_4 = ____tag;
		NullCheck(L_3);
		VirtFuncInvoker1< bool, String_t* >::Invoke(20 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,VoxelBusters.NativePlugins.WebView>::Remove(!0) */, L_3, L_4);
	}

IL_001e:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.AndroidWebViewMessage::.ctor(System.Collections.IDictionary)
extern TypeInfo* IDictionary_t1_35_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t1_1839_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerable_t1_1677_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t1_193_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern const MethodInfo* IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_14949_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral5537;
extern Il2CppCodeGenString* _stringLiteral4504;
extern Il2CppCodeGenString* _stringLiteral1863;
extern "C" void AndroidWebViewMessage__ctor_m8_1851 (AndroidWebViewMessage_t8_313 * __this, Object_t * ____schemeDataJsonDict, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionary_t1_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		Dictionary_2_t1_1839_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1603);
		IEnumerable_t1_1677_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(253);
		IEnumerator_t1_193_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(144);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484117);
		Dictionary_2__ctor_m1_14949_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483789);
		_stringLiteral5537 = il2cpp_codegen_string_literal_from_index(5537);
		_stringLiteral4504 = il2cpp_codegen_string_literal_from_index(4504);
		_stringLiteral1863 = il2cpp_codegen_string_literal_from_index(1863);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	String_t* V_1 = {0};
	Object_t * V_2 = {0};
	Object_t * V_3 = {0};
	Object_t * V_4 = {0};
	String_t* V_5 = {0};
	String_t* V_6 = {0};
	Object_t * V_7 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		WebViewMessage__ctor_m8_1852(__this, /*hidden argument*/NULL);
		Object_t * L_0 = ____schemeDataJsonDict;
		String_t* L_1 = IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(NULL /*static, unused*/, L_0, _stringLiteral5537, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var);
		V_0 = L_1;
		Object_t * L_2 = ____schemeDataJsonDict;
		String_t* L_3 = IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(NULL /*static, unused*/, L_2, _stringLiteral4504, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var);
		V_1 = L_3;
		Object_t * L_4 = ____schemeDataJsonDict;
		NullCheck(L_4);
		Object_t * L_5 = (Object_t *)InterfaceFuncInvoker1< Object_t *, Object_t * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_4, _stringLiteral1863);
		V_2 = ((Object_t *)IsInst(L_5, IDictionary_t1_35_il2cpp_TypeInfo_var));
		String_t* L_6 = V_0;
		WebViewMessage_set_SchemeName_m8_1854(__this, L_6, /*hidden argument*/NULL);
		String_t* L_7 = V_1;
		WebViewMessage_set_Host_m8_1856(__this, L_7, /*hidden argument*/NULL);
		Dictionary_2_t1_1839 * L_8 = (Dictionary_2_t1_1839 *)il2cpp_codegen_object_new (Dictionary_2_t1_1839_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_14949(L_8, /*hidden argument*/Dictionary_2__ctor_m1_14949_MethodInfo_var);
		WebViewMessage_set_Arguments_m8_1858(__this, L_8, /*hidden argument*/NULL);
		Object_t * L_9 = V_2;
		NullCheck(L_9);
		Object_t * L_10 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(4 /* System.Collections.ICollection System.Collections.IDictionary::get_Keys() */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_9);
		NullCheck(L_10);
		Object_t * L_11 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t1_1677_il2cpp_TypeInfo_var, L_10);
		V_4 = L_11;
	}

IL_0055:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0087;
		}

IL_005a:
		{
			Object_t * L_12 = V_4;
			NullCheck(L_12);
			Object_t * L_13 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_12);
			V_3 = L_13;
			Object_t * L_14 = V_3;
			NullCheck(L_14);
			String_t* L_15 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_14);
			V_5 = L_15;
			Object_t * L_16 = V_2;
			Object_t * L_17 = V_3;
			NullCheck(L_16);
			Object_t * L_18 = (Object_t *)InterfaceFuncInvoker1< Object_t *, Object_t * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_16, L_17);
			NullCheck(L_18);
			String_t* L_19 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_18);
			V_6 = L_19;
			Dictionary_2_t1_1839 * L_20 = WebViewMessage_get_Arguments_m8_1857(__this, /*hidden argument*/NULL);
			String_t* L_21 = V_5;
			String_t* L_22 = V_6;
			NullCheck(L_20);
			VirtActionInvoker2< String_t*, String_t* >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::set_Item(!0,!1) */, L_20, L_21, L_22);
		}

IL_0087:
		{
			Object_t * L_23 = V_4;
			NullCheck(L_23);
			bool L_24 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_23);
			if (L_24)
			{
				goto IL_005a;
			}
		}

IL_0093:
		{
			IL2CPP_LEAVE(0xAE, FINALLY_0098);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0098;
	}

FINALLY_0098:
	{ // begin finally (depth: 1)
		{
			Object_t * L_25 = V_4;
			V_7 = ((Object_t *)IsInst(L_25, IDisposable_t1_1035_il2cpp_TypeInfo_var));
			Object_t * L_26 = V_7;
			if (L_26)
			{
				goto IL_00a6;
			}
		}

IL_00a5:
		{
			IL2CPP_END_FINALLY(152)
		}

IL_00a6:
		{
			Object_t * L_27 = V_7;
			NullCheck(L_27);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_27);
			IL2CPP_END_FINALLY(152)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(152)
	{
		IL2CPP_JUMP_TBL(0xAE, IL_00ae)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_00ae:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.WebViewMessage::.ctor()
extern "C" void WebViewMessage__ctor_m8_1852 (WebViewMessage_t8_314 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		WebViewMessage_set_SchemeName_m8_1854(__this, (String_t*)NULL, /*hidden argument*/NULL);
		WebViewMessage_set_Host_m8_1856(__this, (String_t*)NULL, /*hidden argument*/NULL);
		WebViewMessage_set_Arguments_m8_1858(__this, (Dictionary_2_t1_1839 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.String VoxelBusters.NativePlugins.WebViewMessage::get_SchemeName()
extern "C" String_t* WebViewMessage_get_SchemeName_m8_1853 (WebViewMessage_t8_314 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CSchemeNameU3Ek__BackingField_0);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.WebViewMessage::set_SchemeName(System.String)
extern "C" void WebViewMessage_set_SchemeName_m8_1854 (WebViewMessage_t8_314 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CSchemeNameU3Ek__BackingField_0 = L_0;
		return;
	}
}
// System.String VoxelBusters.NativePlugins.WebViewMessage::get_Host()
extern "C" String_t* WebViewMessage_get_Host_m8_1855 (WebViewMessage_t8_314 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CHostU3Ek__BackingField_1);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.WebViewMessage::set_Host(System.String)
extern "C" void WebViewMessage_set_Host_m8_1856 (WebViewMessage_t8_314 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CHostU3Ek__BackingField_1 = L_0;
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.String> VoxelBusters.NativePlugins.WebViewMessage::get_Arguments()
extern "C" Dictionary_2_t1_1839 * WebViewMessage_get_Arguments_m8_1857 (WebViewMessage_t8_314 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1_1839 * L_0 = (__this->___U3CArgumentsU3Ek__BackingField_2);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.WebViewMessage::set_Arguments(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C" void WebViewMessage_set_Arguments_m8_1858 (WebViewMessage_t8_314 * __this, Dictionary_2_t1_1839 * ___value, const MethodInfo* method)
{
	{
		Dictionary_2_t1_1839 * L_0 = ___value;
		__this->___U3CArgumentsU3Ek__BackingField_2 = L_0;
		return;
	}
}
// System.String VoxelBusters.NativePlugins.WebViewMessage::ToString()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5538;
extern "C" String_t* WebViewMessage_ToString_m8_1859 (WebViewMessage_t8_314 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5538 = il2cpp_codegen_string_literal_from_index(5538);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = WebViewMessage_get_SchemeName_m8_1853(__this, /*hidden argument*/NULL);
		String_t* L_1 = WebViewMessage_get_Host_m8_1855(__this, /*hidden argument*/NULL);
		Dictionary_2_t1_1839 * L_2 = WebViewMessage_get_Arguments_m8_1857(__this, /*hidden argument*/NULL);
		String_t* L_3 = JSONParserExtensions_ToJSON_m8_269(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Format_m1_550(NULL /*static, unused*/, _stringLiteral5538, L_0, L_1, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.iOSWebViewMessage::.ctor(System.Collections.IDictionary)
extern TypeInfo* IDictionary_t1_35_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t1_1839_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerable_t1_1677_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t1_193_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern const MethodInfo* IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_14949_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral5537;
extern Il2CppCodeGenString* _stringLiteral4504;
extern Il2CppCodeGenString* _stringLiteral1863;
extern "C" void iOSWebViewMessage__ctor_m8_1860 (iOSWebViewMessage_t8_315 * __this, Object_t * ____schemeDataJsonDict, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionary_t1_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		Dictionary_2_t1_1839_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1603);
		IEnumerable_t1_1677_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(253);
		IEnumerator_t1_193_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(144);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484117);
		Dictionary_2__ctor_m1_14949_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483789);
		_stringLiteral5537 = il2cpp_codegen_string_literal_from_index(5537);
		_stringLiteral4504 = il2cpp_codegen_string_literal_from_index(4504);
		_stringLiteral1863 = il2cpp_codegen_string_literal_from_index(1863);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	String_t* V_1 = {0};
	Object_t * V_2 = {0};
	Object_t * V_3 = {0};
	Object_t * V_4 = {0};
	String_t* V_5 = {0};
	String_t* V_6 = {0};
	Object_t * V_7 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		WebViewMessage__ctor_m8_1852(__this, /*hidden argument*/NULL);
		Object_t * L_0 = ____schemeDataJsonDict;
		String_t* L_1 = IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(NULL /*static, unused*/, L_0, _stringLiteral5537, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var);
		V_0 = L_1;
		Object_t * L_2 = ____schemeDataJsonDict;
		String_t* L_3 = IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(NULL /*static, unused*/, L_2, _stringLiteral4504, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var);
		V_1 = L_3;
		Object_t * L_4 = ____schemeDataJsonDict;
		NullCheck(L_4);
		Object_t * L_5 = (Object_t *)InterfaceFuncInvoker1< Object_t *, Object_t * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_4, _stringLiteral1863);
		V_2 = ((Object_t *)IsInst(L_5, IDictionary_t1_35_il2cpp_TypeInfo_var));
		String_t* L_6 = V_0;
		WebViewMessage_set_SchemeName_m8_1854(__this, L_6, /*hidden argument*/NULL);
		String_t* L_7 = V_1;
		WebViewMessage_set_Host_m8_1856(__this, L_7, /*hidden argument*/NULL);
		Dictionary_2_t1_1839 * L_8 = (Dictionary_2_t1_1839 *)il2cpp_codegen_object_new (Dictionary_2_t1_1839_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_14949(L_8, /*hidden argument*/Dictionary_2__ctor_m1_14949_MethodInfo_var);
		WebViewMessage_set_Arguments_m8_1858(__this, L_8, /*hidden argument*/NULL);
		Object_t * L_9 = V_2;
		NullCheck(L_9);
		Object_t * L_10 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(4 /* System.Collections.ICollection System.Collections.IDictionary::get_Keys() */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_9);
		NullCheck(L_10);
		Object_t * L_11 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t1_1677_il2cpp_TypeInfo_var, L_10);
		V_4 = L_11;
	}

IL_0055:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0087;
		}

IL_005a:
		{
			Object_t * L_12 = V_4;
			NullCheck(L_12);
			Object_t * L_13 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_12);
			V_3 = L_13;
			Object_t * L_14 = V_3;
			NullCheck(L_14);
			String_t* L_15 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_14);
			V_5 = L_15;
			Object_t * L_16 = V_2;
			Object_t * L_17 = V_3;
			NullCheck(L_16);
			Object_t * L_18 = (Object_t *)InterfaceFuncInvoker1< Object_t *, Object_t * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_16, L_17);
			NullCheck(L_18);
			String_t* L_19 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_18);
			V_6 = L_19;
			Dictionary_2_t1_1839 * L_20 = WebViewMessage_get_Arguments_m8_1857(__this, /*hidden argument*/NULL);
			String_t* L_21 = V_5;
			String_t* L_22 = V_6;
			NullCheck(L_20);
			VirtActionInvoker2< String_t*, String_t* >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::set_Item(!0,!1) */, L_20, L_21, L_22);
		}

IL_0087:
		{
			Object_t * L_23 = V_4;
			NullCheck(L_23);
			bool L_24 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_23);
			if (L_24)
			{
				goto IL_005a;
			}
		}

IL_0093:
		{
			IL2CPP_LEAVE(0xAE, FINALLY_0098);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0098;
	}

FINALLY_0098:
	{ // begin finally (depth: 1)
		{
			Object_t * L_25 = V_4;
			V_7 = ((Object_t *)IsInst(L_25, IDisposable_t1_1035_il2cpp_TypeInfo_var));
			Object_t * L_26 = V_7;
			if (L_26)
			{
				goto IL_00a6;
			}
		}

IL_00a5:
		{
			IL2CPP_END_FINALLY(152)
		}

IL_00a6:
		{
			Object_t * L_27 = V_7;
			NullCheck(L_27);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_27);
			IL2CPP_END_FINALLY(152)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(152)
	{
		IL2CPP_JUMP_TBL(0xAE, IL_00ae)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_00ae:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.PlatformBindingHelper::.ctor()
extern "C" void PlatformBindingHelper__ctor_m8_1861 (PlatformBindingHelper_t8_316 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m6_671(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.PlatformBindingHelper::Start()
extern "C" void PlatformBindingHelper_Start_m8_1862 (PlatformBindingHelper_t8_316 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
