﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t1_1978;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_0.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m1_15703_gshared (Enumerator_t1_1985 * __this, Dictionary_2_t1_1978 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m1_15703(__this, ___host, method) (( void (*) (Enumerator_t1_1985 *, Dictionary_2_t1_1978 *, const MethodInfo*))Enumerator__ctor_m1_15703_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m1_15704_gshared (Enumerator_t1_1985 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_15704(__this, method) (( Object_t * (*) (Enumerator_t1_1985 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_15704_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m1_15705_gshared (Enumerator_t1_1985 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1_15705(__this, method) (( void (*) (Enumerator_t1_1985 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_15705_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>::Dispose()
extern "C" void Enumerator_Dispose_m1_15706_gshared (Enumerator_t1_1985 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1_15706(__this, method) (( void (*) (Enumerator_t1_1985 *, const MethodInfo*))Enumerator_Dispose_m1_15706_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>::MoveNext()
extern "C" bool Enumerator_MoveNext_m1_15707_gshared (Enumerator_t1_1985 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1_15707(__this, method) (( bool (*) (Enumerator_t1_1985 *, const MethodInfo*))Enumerator_MoveNext_m1_15707_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m1_15708_gshared (Enumerator_t1_1985 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1_15708(__this, method) (( Object_t * (*) (Enumerator_t1_1985 *, const MethodInfo*))Enumerator_get_Current_m1_15708_gshared)(__this, method)
