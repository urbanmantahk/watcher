﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>
struct Collection_1_t1_2082;
// System.Collections.Generic.IList`1<System.Reflection.CustomAttributeNamedArgument>
struct IList_1_t1_590;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Object
struct Object_t;
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t1_1749;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeNamedArgument>
struct IEnumerator_1_t1_2781;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgument.h"

// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::.ctor()
extern "C" void Collection_1__ctor_m1_16549_gshared (Collection_1_t1_2082 * __this, const MethodInfo* method);
#define Collection_1__ctor_m1_16549(__this, method) (( void (*) (Collection_1_t1_2082 *, const MethodInfo*))Collection_1__ctor_m1_16549_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1__ctor_m1_16550_gshared (Collection_1_t1_2082 * __this, Object_t* ___list, const MethodInfo* method);
#define Collection_1__ctor_m1_16550(__this, ___list, method) (( void (*) (Collection_1_t1_2082 *, Object_t*, const MethodInfo*))Collection_1__ctor_m1_16550_gshared)(__this, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_16551_gshared (Collection_1_t1_2082 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_16551(__this, method) (( bool (*) (Collection_1_t1_2082 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_16551_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m1_16552_gshared (Collection_1_t1_2082 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m1_16552(__this, ___array, ___index, method) (( void (*) (Collection_1_t1_2082 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m1_16552_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m1_16553_gshared (Collection_1_t1_2082 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m1_16553(__this, method) (( Object_t * (*) (Collection_1_t1_2082 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m1_16553_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m1_16554_gshared (Collection_1_t1_2082 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m1_16554(__this, ___value, method) (( int32_t (*) (Collection_1_t1_2082 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m1_16554_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m1_16555_gshared (Collection_1_t1_2082 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m1_16555(__this, ___value, method) (( bool (*) (Collection_1_t1_2082 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m1_16555_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m1_16556_gshared (Collection_1_t1_2082 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m1_16556(__this, ___value, method) (( int32_t (*) (Collection_1_t1_2082 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m1_16556_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m1_16557_gshared (Collection_1_t1_2082 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m1_16557(__this, ___index, ___value, method) (( void (*) (Collection_1_t1_2082 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m1_16557_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m1_16558_gshared (Collection_1_t1_2082 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m1_16558(__this, ___value, method) (( void (*) (Collection_1_t1_2082 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m1_16558_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m1_16559_gshared (Collection_1_t1_2082 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m1_16559(__this, method) (( bool (*) (Collection_1_t1_2082 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m1_16559_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m1_16560_gshared (Collection_1_t1_2082 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m1_16560(__this, method) (( Object_t * (*) (Collection_1_t1_2082 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m1_16560_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m1_16561_gshared (Collection_1_t1_2082 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m1_16561(__this, method) (( bool (*) (Collection_1_t1_2082 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m1_16561_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m1_16562_gshared (Collection_1_t1_2082 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m1_16562(__this, method) (( bool (*) (Collection_1_t1_2082 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m1_16562_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m1_16563_gshared (Collection_1_t1_2082 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m1_16563(__this, ___index, method) (( Object_t * (*) (Collection_1_t1_2082 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m1_16563_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m1_16564_gshared (Collection_1_t1_2082 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m1_16564(__this, ___index, ___value, method) (( void (*) (Collection_1_t1_2082 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m1_16564_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::Add(T)
extern "C" void Collection_1_Add_m1_16565_gshared (Collection_1_t1_2082 * __this, CustomAttributeNamedArgument_t1_593  ___item, const MethodInfo* method);
#define Collection_1_Add_m1_16565(__this, ___item, method) (( void (*) (Collection_1_t1_2082 *, CustomAttributeNamedArgument_t1_593 , const MethodInfo*))Collection_1_Add_m1_16565_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::Clear()
extern "C" void Collection_1_Clear_m1_16566_gshared (Collection_1_t1_2082 * __this, const MethodInfo* method);
#define Collection_1_Clear_m1_16566(__this, method) (( void (*) (Collection_1_t1_2082 *, const MethodInfo*))Collection_1_Clear_m1_16566_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::ClearItems()
extern "C" void Collection_1_ClearItems_m1_16567_gshared (Collection_1_t1_2082 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m1_16567(__this, method) (( void (*) (Collection_1_t1_2082 *, const MethodInfo*))Collection_1_ClearItems_m1_16567_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::Contains(T)
extern "C" bool Collection_1_Contains_m1_16568_gshared (Collection_1_t1_2082 * __this, CustomAttributeNamedArgument_t1_593  ___item, const MethodInfo* method);
#define Collection_1_Contains_m1_16568(__this, ___item, method) (( bool (*) (Collection_1_t1_2082 *, CustomAttributeNamedArgument_t1_593 , const MethodInfo*))Collection_1_Contains_m1_16568_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m1_16569_gshared (Collection_1_t1_2082 * __this, CustomAttributeNamedArgumentU5BU5D_t1_1749* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m1_16569(__this, ___array, ___index, method) (( void (*) (Collection_1_t1_2082 *, CustomAttributeNamedArgumentU5BU5D_t1_1749*, int32_t, const MethodInfo*))Collection_1_CopyTo_m1_16569_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m1_16570_gshared (Collection_1_t1_2082 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m1_16570(__this, method) (( Object_t* (*) (Collection_1_t1_2082 *, const MethodInfo*))Collection_1_GetEnumerator_m1_16570_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m1_16571_gshared (Collection_1_t1_2082 * __this, CustomAttributeNamedArgument_t1_593  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m1_16571(__this, ___item, method) (( int32_t (*) (Collection_1_t1_2082 *, CustomAttributeNamedArgument_t1_593 , const MethodInfo*))Collection_1_IndexOf_m1_16571_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m1_16572_gshared (Collection_1_t1_2082 * __this, int32_t ___index, CustomAttributeNamedArgument_t1_593  ___item, const MethodInfo* method);
#define Collection_1_Insert_m1_16572(__this, ___index, ___item, method) (( void (*) (Collection_1_t1_2082 *, int32_t, CustomAttributeNamedArgument_t1_593 , const MethodInfo*))Collection_1_Insert_m1_16572_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m1_16573_gshared (Collection_1_t1_2082 * __this, int32_t ___index, CustomAttributeNamedArgument_t1_593  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m1_16573(__this, ___index, ___item, method) (( void (*) (Collection_1_t1_2082 *, int32_t, CustomAttributeNamedArgument_t1_593 , const MethodInfo*))Collection_1_InsertItem_m1_16573_gshared)(__this, ___index, ___item, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::get_Items()
extern "C" Object_t* Collection_1_get_Items_m1_16574_gshared (Collection_1_t1_2082 * __this, const MethodInfo* method);
#define Collection_1_get_Items_m1_16574(__this, method) (( Object_t* (*) (Collection_1_t1_2082 *, const MethodInfo*))Collection_1_get_Items_m1_16574_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::Remove(T)
extern "C" bool Collection_1_Remove_m1_16575_gshared (Collection_1_t1_2082 * __this, CustomAttributeNamedArgument_t1_593  ___item, const MethodInfo* method);
#define Collection_1_Remove_m1_16575(__this, ___item, method) (( bool (*) (Collection_1_t1_2082 *, CustomAttributeNamedArgument_t1_593 , const MethodInfo*))Collection_1_Remove_m1_16575_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m1_16576_gshared (Collection_1_t1_2082 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m1_16576(__this, ___index, method) (( void (*) (Collection_1_t1_2082 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m1_16576_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m1_16577_gshared (Collection_1_t1_2082 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m1_16577(__this, ___index, method) (( void (*) (Collection_1_t1_2082 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m1_16577_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::get_Count()
extern "C" int32_t Collection_1_get_Count_m1_16578_gshared (Collection_1_t1_2082 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m1_16578(__this, method) (( int32_t (*) (Collection_1_t1_2082 *, const MethodInfo*))Collection_1_get_Count_m1_16578_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::get_Item(System.Int32)
extern "C" CustomAttributeNamedArgument_t1_593  Collection_1_get_Item_m1_16579_gshared (Collection_1_t1_2082 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m1_16579(__this, ___index, method) (( CustomAttributeNamedArgument_t1_593  (*) (Collection_1_t1_2082 *, int32_t, const MethodInfo*))Collection_1_get_Item_m1_16579_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m1_16580_gshared (Collection_1_t1_2082 * __this, int32_t ___index, CustomAttributeNamedArgument_t1_593  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m1_16580(__this, ___index, ___value, method) (( void (*) (Collection_1_t1_2082 *, int32_t, CustomAttributeNamedArgument_t1_593 , const MethodInfo*))Collection_1_set_Item_m1_16580_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m1_16581_gshared (Collection_1_t1_2082 * __this, int32_t ___index, CustomAttributeNamedArgument_t1_593  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m1_16581(__this, ___index, ___item, method) (( void (*) (Collection_1_t1_2082 *, int32_t, CustomAttributeNamedArgument_t1_593 , const MethodInfo*))Collection_1_SetItem_m1_16581_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m1_16582_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m1_16582(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m1_16582_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::ConvertItem(System.Object)
extern "C" CustomAttributeNamedArgument_t1_593  Collection_1_ConvertItem_m1_16583_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m1_16583(__this /* static, unused */, ___item, method) (( CustomAttributeNamedArgument_t1_593  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m1_16583_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m1_16584_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m1_16584(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m1_16584_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m1_16585_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m1_16585(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m1_16585_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m1_16586_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m1_16586(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m1_16586_gshared)(__this /* static, unused */, ___list, method)
