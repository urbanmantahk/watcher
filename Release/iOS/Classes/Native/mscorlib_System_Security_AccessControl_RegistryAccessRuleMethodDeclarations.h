﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.AccessControl.RegistryAccessRule
struct RegistryAccessRule_t1_1172;
// System.Security.Principal.IdentityReference
struct IdentityReference_t1_1120;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_AccessControl_RegistryRights.h"
#include "mscorlib_System_Security_AccessControl_AccessControlType.h"
#include "mscorlib_System_Security_AccessControl_InheritanceFlags.h"
#include "mscorlib_System_Security_AccessControl_PropagationFlags.h"

// System.Void System.Security.AccessControl.RegistryAccessRule::.ctor(System.Security.Principal.IdentityReference,System.Security.AccessControl.RegistryRights,System.Security.AccessControl.AccessControlType)
extern "C" void RegistryAccessRule__ctor_m1_10064 (RegistryAccessRule_t1_1172 * __this, IdentityReference_t1_1120 * ___identity, int32_t ___registryRights, int32_t ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.RegistryAccessRule::.ctor(System.String,System.Security.AccessControl.RegistryRights,System.Security.AccessControl.AccessControlType)
extern "C" void RegistryAccessRule__ctor_m1_10065 (RegistryAccessRule_t1_1172 * __this, String_t* ___identity, int32_t ___registryRights, int32_t ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.RegistryAccessRule::.ctor(System.Security.Principal.IdentityReference,System.Security.AccessControl.RegistryRights,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.AccessControlType)
extern "C" void RegistryAccessRule__ctor_m1_10066 (RegistryAccessRule_t1_1172 * __this, IdentityReference_t1_1120 * ___identity, int32_t ___registryRights, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.RegistryAccessRule::.ctor(System.String,System.Security.AccessControl.RegistryRights,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.AccessControlType)
extern "C" void RegistryAccessRule__ctor_m1_10067 (RegistryAccessRule_t1_1172 * __this, String_t* ___identity, int32_t ___registryRights, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.AccessControl.RegistryRights System.Security.AccessControl.RegistryAccessRule::get_RegistryRights()
extern "C" int32_t RegistryAccessRule_get_RegistryRights_m1_10068 (RegistryAccessRule_t1_1172 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
