﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.AddressBook/RequestAccessCompletion
struct RequestAccessCompletion_t8_193;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eABAuthorizatio.h"
#include "mscorlib_System_String.h"

// System.Void VoxelBusters.NativePlugins.AddressBook/RequestAccessCompletion::.ctor(System.Object,System.IntPtr)
extern "C" void RequestAccessCompletion__ctor_m8_1118 (RequestAccessCompletion_t8_193 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.AddressBook/RequestAccessCompletion::Invoke(VoxelBusters.NativePlugins.eABAuthorizationStatus,System.String)
extern "C" void RequestAccessCompletion_Invoke_m8_1119 (RequestAccessCompletion_t8_193 * __this, int32_t ____authorizationStatus, String_t* ____error, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_RequestAccessCompletion_t8_193(Il2CppObject* delegate, int32_t ____authorizationStatus, String_t* ____error);
// System.IAsyncResult VoxelBusters.NativePlugins.AddressBook/RequestAccessCompletion::BeginInvoke(VoxelBusters.NativePlugins.eABAuthorizationStatus,System.String,System.AsyncCallback,System.Object)
extern "C" Object_t * RequestAccessCompletion_BeginInvoke_m8_1120 (RequestAccessCompletion_t8_193 * __this, int32_t ____authorizationStatus, String_t* ____error, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.AddressBook/RequestAccessCompletion::EndInvoke(System.IAsyncResult)
extern "C" void RequestAccessCompletion_EndInvoke_m8_1121 (RequestAccessCompletion_t8_193 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
