﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.InteropServices.TypeLibFuncAttribute
struct TypeLibFuncAttribute_t1_833;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_InteropServices_TypeLibFuncFlags.h"

// System.Void System.Runtime.InteropServices.TypeLibFuncAttribute::.ctor(System.Int16)
extern "C" void TypeLibFuncAttribute__ctor_m1_7922 (TypeLibFuncAttribute_t1_833 * __this, int16_t ___flags, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.TypeLibFuncAttribute::.ctor(System.Runtime.InteropServices.TypeLibFuncFlags)
extern "C" void TypeLibFuncAttribute__ctor_m1_7923 (TypeLibFuncAttribute_t1_833 * __this, int32_t ___flags, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.InteropServices.TypeLibFuncFlags System.Runtime.InteropServices.TypeLibFuncAttribute::get_Value()
extern "C" int32_t TypeLibFuncAttribute_get_Value_m1_7924 (TypeLibFuncAttribute_t1_833 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
