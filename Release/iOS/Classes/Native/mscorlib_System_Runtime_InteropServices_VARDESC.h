﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_ValueType.h"
#include "mscorlib_System_Runtime_InteropServices_ELEMDESC.h"
#include "mscorlib_System_Runtime_InteropServices_VarEnum.h"

// System.Runtime.InteropServices.VARDESC
struct  VARDESC_t1_846 
{
	// System.Int32 System.Runtime.InteropServices.VARDESC::memid
	int32_t ___memid_0;
	// System.String System.Runtime.InteropServices.VARDESC::lpstrSchema
	String_t* ___lpstrSchema_1;
	// System.Runtime.InteropServices.ELEMDESC System.Runtime.InteropServices.VARDESC::elemdescVar
	ELEMDESC_t1_785  ___elemdescVar_2;
	// System.Int16 System.Runtime.InteropServices.VARDESC::wVarFlags
	int16_t ___wVarFlags_3;
	// System.Runtime.InteropServices.VarEnum System.Runtime.InteropServices.VARDESC::varkind
	int32_t ___varkind_4;
};
// Native definition for marshalling of: System.Runtime.InteropServices.VARDESC
struct VARDESC_t1_846_marshaled
{
	int32_t ___memid_0;
	char* ___lpstrSchema_1;
	ELEMDESC_t1_785_marshaled ___elemdescVar_2;
	int16_t ___wVarFlags_3;
	int32_t ___varkind_4;
};
