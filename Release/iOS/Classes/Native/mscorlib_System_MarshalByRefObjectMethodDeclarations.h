﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.MarshalByRefObject
struct MarshalByRefObject_t1_69;
// System.Runtime.Remoting.Identity
struct Identity_t1_943;
// System.Runtime.Remoting.ServerIdentity
struct ServerIdentity_t1_70;
// System.Runtime.Remoting.ObjRef
struct ObjRef_t1_923;
// System.Type
struct Type_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.MarshalByRefObject::.ctor()
extern "C" void MarshalByRefObject__ctor_m1_1370 (MarshalByRefObject_t1_69 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Identity System.MarshalByRefObject::GetObjectIdentity(System.MarshalByRefObject,System.Boolean&)
extern "C" Identity_t1_943 * MarshalByRefObject_GetObjectIdentity_m1_1371 (MarshalByRefObject_t1_69 * __this, MarshalByRefObject_t1_69 * ___obj, bool* ___IsClient, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.ServerIdentity System.MarshalByRefObject::get_ObjectIdentity()
extern "C" ServerIdentity_t1_70 * MarshalByRefObject_get_ObjectIdentity_m1_1372 (MarshalByRefObject_t1_69 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.MarshalByRefObject::set_ObjectIdentity(System.Runtime.Remoting.ServerIdentity)
extern "C" void MarshalByRefObject_set_ObjectIdentity_m1_1373 (MarshalByRefObject_t1_69 * __this, ServerIdentity_t1_70 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.ObjRef System.MarshalByRefObject::CreateObjRef(System.Type)
extern "C" ObjRef_t1_923 * MarshalByRefObject_CreateObjRef_m1_1374 (MarshalByRefObject_t1_69 * __this, Type_t * ___requestedType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.MarshalByRefObject::GetLifetimeService()
extern "C" Object_t * MarshalByRefObject_GetLifetimeService_m1_1375 (MarshalByRefObject_t1_69 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.MarshalByRefObject::InitializeLifetimeService()
extern "C" Object_t * MarshalByRefObject_InitializeLifetimeService_m1_1376 (MarshalByRefObject_t1_69 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.MarshalByRefObject System.MarshalByRefObject::MemberwiseClone(System.Boolean)
extern "C" MarshalByRefObject_t1_69 * MarshalByRefObject_MemberwiseClone_m1_1377 (MarshalByRefObject_t1_69 * __this, bool ___cloneIdentity, const MethodInfo* method) IL2CPP_METHOD_ATTR;
