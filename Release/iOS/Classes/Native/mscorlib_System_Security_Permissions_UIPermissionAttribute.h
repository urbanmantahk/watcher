﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Security_Permissions_CodeAccessSecurityAttri.h"
#include "mscorlib_System_Security_Permissions_UIPermissionClipboard.h"
#include "mscorlib_System_Security_Permissions_UIPermissionWindow.h"

// System.Security.Permissions.UIPermissionAttribute
struct  UIPermissionAttribute_t1_1318  : public CodeAccessSecurityAttribute_t1_1266
{
	// System.Security.Permissions.UIPermissionClipboard System.Security.Permissions.UIPermissionAttribute::clipboard
	int32_t ___clipboard_2;
	// System.Security.Permissions.UIPermissionWindow System.Security.Permissions.UIPermissionAttribute::window
	int32_t ___window_3;
};
