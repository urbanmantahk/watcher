﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Collections_ReadOnlyCollectionBase.h"

// System.Security.AccessControl.AuthorizationRuleCollection
struct  AuthorizationRuleCollection_t1_1121  : public ReadOnlyCollectionBase_t1_300
{
};
