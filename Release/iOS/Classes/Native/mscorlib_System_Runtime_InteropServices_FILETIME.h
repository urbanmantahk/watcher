﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType.h"

// System.Runtime.InteropServices.FILETIME
struct  FILETIME_t1_791 
{
	// System.Int32 System.Runtime.InteropServices.FILETIME::dwLowDateTime
	int32_t ___dwLowDateTime_0;
	// System.Int32 System.Runtime.InteropServices.FILETIME::dwHighDateTime
	int32_t ___dwHighDateTime_1;
};
