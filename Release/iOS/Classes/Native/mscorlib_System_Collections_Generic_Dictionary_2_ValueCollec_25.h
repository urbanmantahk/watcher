﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,Boomlagoon.JSON.JSONValue>
struct Dictionary_2_t1_1919;

#include "mscorlib_System_Object.h"

// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Boomlagoon.JSON.JSONValue>
struct  ValueCollection_t1_2629  : public Object_t
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection::dictionary
	Dictionary_2_t1_1919 * ___dictionary_0;
};
