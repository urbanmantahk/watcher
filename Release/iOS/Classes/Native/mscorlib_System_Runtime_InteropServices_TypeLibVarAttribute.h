﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Attribute.h"
#include "mscorlib_System_Runtime_InteropServices_TypeLibVarFlags.h"

// System.Runtime.InteropServices.TypeLibVarAttribute
struct  TypeLibVarAttribute_t1_839  : public Attribute_t1_2
{
	// System.Runtime.InteropServices.TypeLibVarFlags System.Runtime.InteropServices.TypeLibVarAttribute::flags
	int32_t ___flags_0;
};
