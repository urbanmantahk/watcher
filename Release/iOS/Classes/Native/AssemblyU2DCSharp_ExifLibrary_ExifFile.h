﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ExifLibrary.JPEGFile
struct JPEGFile_t8_111;
// ExifLibrary.JPEGSection
struct JPEGSection_t8_112;
// System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>
struct Dictionary_2_t1_1904;
// System.Predicate`1<ExifLibrary.JPEGSection>
struct Predicate_1_t1_1905;

#include "mscorlib_System_Object.h"
#include "AssemblyU2DCSharp_ExifLibrary_BitConverterEx_ByteOrder.h"

// ExifLibrary.ExifFile
struct  ExifFile_t8_110  : public Object_t
{
	// ExifLibrary.JPEGFile ExifLibrary.ExifFile::file
	JPEGFile_t8_111 * ___file_0;
	// ExifLibrary.JPEGSection ExifLibrary.ExifFile::app1
	JPEGSection_t8_112 * ___app1_1;
	// System.UInt32 ExifLibrary.ExifFile::makerNoteOffset
	uint32_t ___makerNoteOffset_2;
	// System.Int64 ExifLibrary.ExifFile::exifIFDFieldOffset
	int64_t ___exifIFDFieldOffset_3;
	// System.Int64 ExifLibrary.ExifFile::gpsIFDFieldOffset
	int64_t ___gpsIFDFieldOffset_4;
	// System.Int64 ExifLibrary.ExifFile::interopIFDFieldOffset
	int64_t ___interopIFDFieldOffset_5;
	// System.Int64 ExifLibrary.ExifFile::firstIFDFieldOffset
	int64_t ___firstIFDFieldOffset_6;
	// System.Int64 ExifLibrary.ExifFile::thumbOffsetLocation
	int64_t ___thumbOffsetLocation_7;
	// System.Int64 ExifLibrary.ExifFile::thumbSizeLocation
	int64_t ___thumbSizeLocation_8;
	// System.UInt32 ExifLibrary.ExifFile::thumbOffsetValue
	uint32_t ___thumbOffsetValue_9;
	// System.UInt32 ExifLibrary.ExifFile::thumbSizeValue
	uint32_t ___thumbSizeValue_10;
	// System.Boolean ExifLibrary.ExifFile::makerNoteProcessed
	bool ___makerNoteProcessed_11;
	// System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,ExifLibrary.ExifProperty> ExifLibrary.ExifFile::<Properties>k__BackingField
	Dictionary_2_t1_1904 * ___U3CPropertiesU3Ek__BackingField_12;
	// ExifLibrary.BitConverterEx/ByteOrder ExifLibrary.ExifFile::<ByteOrder>k__BackingField
	int32_t ___U3CByteOrderU3Ek__BackingField_13;
	// ExifLibrary.JPEGFile ExifLibrary.ExifFile::<Thumbnail>k__BackingField
	JPEGFile_t8_111 * ___U3CThumbnailU3Ek__BackingField_14;
};
struct ExifFile_t8_110_StaticFields{
	// System.Predicate`1<ExifLibrary.JPEGSection> ExifLibrary.ExifFile::<>f__am$cacheF
	Predicate_1_t1_1905 * ___U3CU3Ef__amU24cacheF_15;
	// System.Predicate`1<ExifLibrary.JPEGSection> ExifLibrary.ExifFile::<>f__am$cache10
	Predicate_1_t1_1905 * ___U3CU3Ef__amU24cache10_16;
};
