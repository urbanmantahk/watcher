﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaValidity.h"

// System.Xml.Schema.XmlSchemaValidity
struct  XmlSchemaValidity_t4_75 
{
	// System.Int32 System.Xml.Schema.XmlSchemaValidity::value__
	int32_t ___value___1;
};
