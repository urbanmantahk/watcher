﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Runtime_Versioning_ResourceScope.h"

// System.Runtime.Versioning.ResourceScope
struct  ResourceScope_t1_1102 
{
	// System.Int32 System.Runtime.Versioning.ResourceScope::value__
	int32_t ___value___1;
};
