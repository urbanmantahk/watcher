﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VoxelBusters.Utility.DownloadTexture
struct DownloadTexture_t8_162;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;

#include "mscorlib_System_Object.h"

// VoxelBusters.NativePlugins.Internal.ShareImageUtility
struct  ShareImageUtility_t8_280  : public Object_t
{
	// VoxelBusters.Utility.DownloadTexture VoxelBusters.NativePlugins.Internal.ShareImageUtility::m_downloadTexture
	DownloadTexture_t8_162 * ___m_downloadTexture_0;
	// System.Collections.IEnumerator VoxelBusters.NativePlugins.Internal.ShareImageUtility::m_takeScreenShotCoroutine
	Object_t * ___m_takeScreenShotCoroutine_1;
	// System.Boolean VoxelBusters.NativePlugins.Internal.ShareImageUtility::<ImageAsyncDownloadInProgress>k__BackingField
	bool ___U3CImageAsyncDownloadInProgressU3Ek__BackingField_2;
};
