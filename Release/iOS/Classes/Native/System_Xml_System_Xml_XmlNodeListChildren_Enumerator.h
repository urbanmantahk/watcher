﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Xml.IHasXmlChildNode
struct IHasXmlChildNode_t4_152;
// System.Xml.XmlLinkedNode
struct XmlLinkedNode_t4_118;

#include "mscorlib_System_Object.h"

// System.Xml.XmlNodeListChildren/Enumerator
struct  Enumerator_t4_151  : public Object_t
{
	// System.Xml.IHasXmlChildNode System.Xml.XmlNodeListChildren/Enumerator::parent
	Object_t * ___parent_0;
	// System.Xml.XmlLinkedNode System.Xml.XmlNodeListChildren/Enumerator::currentChild
	XmlLinkedNode_t4_118 * ___currentChild_1;
	// System.Boolean System.Xml.XmlNodeListChildren/Enumerator::passedLastNode
	bool ___passedLastNode_2;
};
