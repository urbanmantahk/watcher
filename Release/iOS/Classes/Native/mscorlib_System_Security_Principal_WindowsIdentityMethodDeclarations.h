﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Principal.WindowsIdentity
struct WindowsIdentity_t1_1384;
// System.String
struct String_t;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;
// System.Object
struct Object_t;
// System.Security.Principal.WindowsImpersonationContext
struct WindowsImpersonationContext_t1_1385;
// System.Security.Principal.IdentityReferenceCollection
struct IdentityReferenceCollection_t1_1376;
// System.Security.Principal.SecurityIdentifier
struct SecurityIdentifier_t1_1132;
// System.String[]
struct StringU5BU5D_t1_238;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_Security_Principal_WindowsAccountType.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Security_Principal_TokenAccessLevels.h"
#include "mscorlib_System_Security_Principal_TokenImpersonationLevel.h"

// System.Void System.Security.Principal.WindowsIdentity::.ctor(System.IntPtr)
extern "C" void WindowsIdentity__ctor_m1_11846 (WindowsIdentity_t1_1384 * __this, IntPtr_t ___userToken, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Principal.WindowsIdentity::.ctor(System.IntPtr,System.String)
extern "C" void WindowsIdentity__ctor_m1_11847 (WindowsIdentity_t1_1384 * __this, IntPtr_t ___userToken, String_t* ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Principal.WindowsIdentity::.ctor(System.IntPtr,System.String,System.Security.Principal.WindowsAccountType)
extern "C" void WindowsIdentity__ctor_m1_11848 (WindowsIdentity_t1_1384 * __this, IntPtr_t ___userToken, String_t* ___type, int32_t ___acctType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Principal.WindowsIdentity::.ctor(System.IntPtr,System.String,System.Security.Principal.WindowsAccountType,System.Boolean)
extern "C" void WindowsIdentity__ctor_m1_11849 (WindowsIdentity_t1_1384 * __this, IntPtr_t ___userToken, String_t* ___type, int32_t ___acctType, bool ___isAuthenticated, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Principal.WindowsIdentity::.ctor(System.String)
extern "C" void WindowsIdentity__ctor_m1_11850 (WindowsIdentity_t1_1384 * __this, String_t* ___sUserPrincipalName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Principal.WindowsIdentity::.ctor(System.String,System.String)
extern "C" void WindowsIdentity__ctor_m1_11851 (WindowsIdentity_t1_1384 * __this, String_t* ___sUserPrincipalName, String_t* ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Principal.WindowsIdentity::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void WindowsIdentity__ctor_m1_11852 (WindowsIdentity_t1_1384 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Principal.WindowsIdentity::.cctor()
extern "C" void WindowsIdentity__cctor_m1_11853 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Principal.WindowsIdentity::System.Runtime.Serialization.IDeserializationCallback.OnDeserialization(System.Object)
extern "C" void WindowsIdentity_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m1_11854 (WindowsIdentity_t1_1384 * __this, Object_t * ___sender, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Principal.WindowsIdentity::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void WindowsIdentity_System_Runtime_Serialization_ISerializable_GetObjectData_m1_11855 (WindowsIdentity_t1_1384 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Principal.WindowsIdentity::Dispose()
extern "C" void WindowsIdentity_Dispose_m1_11856 (WindowsIdentity_t1_1384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Principal.WindowsIdentity::Dispose(System.Boolean)
extern "C" void WindowsIdentity_Dispose_m1_11857 (WindowsIdentity_t1_1384 * __this, bool ___disposing, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Principal.WindowsIdentity System.Security.Principal.WindowsIdentity::GetAnonymous()
extern "C" WindowsIdentity_t1_1384 * WindowsIdentity_GetAnonymous_m1_11858 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Principal.WindowsIdentity System.Security.Principal.WindowsIdentity::GetCurrent()
extern "C" WindowsIdentity_t1_1384 * WindowsIdentity_GetCurrent_m1_11859 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Principal.WindowsIdentity System.Security.Principal.WindowsIdentity::GetCurrent(System.Boolean)
extern "C" WindowsIdentity_t1_1384 * WindowsIdentity_GetCurrent_m1_11860 (Object_t * __this /* static, unused */, bool ___ifImpersonating, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Principal.WindowsIdentity System.Security.Principal.WindowsIdentity::GetCurrent(System.Security.Principal.TokenAccessLevels)
extern "C" WindowsIdentity_t1_1384 * WindowsIdentity_GetCurrent_m1_11861 (Object_t * __this /* static, unused */, int32_t ___desiredAccess, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Principal.WindowsImpersonationContext System.Security.Principal.WindowsIdentity::Impersonate()
extern "C" WindowsImpersonationContext_t1_1385 * WindowsIdentity_Impersonate_m1_11862 (WindowsIdentity_t1_1384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Principal.WindowsImpersonationContext System.Security.Principal.WindowsIdentity::Impersonate(System.IntPtr)
extern "C" WindowsImpersonationContext_t1_1385 * WindowsIdentity_Impersonate_m1_11863 (Object_t * __this /* static, unused */, IntPtr_t ___userToken, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Principal.WindowsIdentity::get_AuthenticationType()
extern "C" String_t* WindowsIdentity_get_AuthenticationType_m1_11864 (WindowsIdentity_t1_1384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Principal.WindowsIdentity::get_IsAnonymous()
extern "C" bool WindowsIdentity_get_IsAnonymous_m1_11865 (WindowsIdentity_t1_1384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Principal.WindowsIdentity::get_IsAuthenticated()
extern "C" bool WindowsIdentity_get_IsAuthenticated_m1_11866 (WindowsIdentity_t1_1384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Principal.WindowsIdentity::get_IsGuest()
extern "C" bool WindowsIdentity_get_IsGuest_m1_11867 (WindowsIdentity_t1_1384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Principal.WindowsIdentity::get_IsSystem()
extern "C" bool WindowsIdentity_get_IsSystem_m1_11868 (WindowsIdentity_t1_1384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Principal.WindowsIdentity::get_Name()
extern "C" String_t* WindowsIdentity_get_Name_m1_11869 (WindowsIdentity_t1_1384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Security.Principal.WindowsIdentity::get_Token()
extern "C" IntPtr_t WindowsIdentity_get_Token_m1_11870 (WindowsIdentity_t1_1384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Principal.IdentityReferenceCollection System.Security.Principal.WindowsIdentity::get_Groups()
extern "C" IdentityReferenceCollection_t1_1376 * WindowsIdentity_get_Groups_m1_11871 (WindowsIdentity_t1_1384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Principal.TokenImpersonationLevel System.Security.Principal.WindowsIdentity::get_ImpersonationLevel()
extern "C" int32_t WindowsIdentity_get_ImpersonationLevel_m1_11872 (WindowsIdentity_t1_1384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Principal.SecurityIdentifier System.Security.Principal.WindowsIdentity::get_Owner()
extern "C" SecurityIdentifier_t1_1132 * WindowsIdentity_get_Owner_m1_11873 (WindowsIdentity_t1_1384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Principal.SecurityIdentifier System.Security.Principal.WindowsIdentity::get_User()
extern "C" SecurityIdentifier_t1_1132 * WindowsIdentity_get_User_m1_11874 (WindowsIdentity_t1_1384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Principal.WindowsIdentity::get_IsPosix()
extern "C" bool WindowsIdentity_get_IsPosix_m1_11875 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Principal.WindowsIdentity::SetToken(System.IntPtr)
extern "C" void WindowsIdentity_SetToken_m1_11876 (WindowsIdentity_t1_1384 * __this, IntPtr_t ___token, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.Security.Principal.WindowsIdentity::_GetRoles(System.IntPtr)
extern "C" StringU5BU5D_t1_238* WindowsIdentity__GetRoles_m1_11877 (Object_t * __this /* static, unused */, IntPtr_t ___token, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Security.Principal.WindowsIdentity::GetCurrentToken()
extern "C" IntPtr_t WindowsIdentity_GetCurrentToken_m1_11878 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Principal.WindowsIdentity::GetTokenName(System.IntPtr)
extern "C" String_t* WindowsIdentity_GetTokenName_m1_11879 (Object_t * __this /* static, unused */, IntPtr_t ___token, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Security.Principal.WindowsIdentity::GetUserToken(System.String)
extern "C" IntPtr_t WindowsIdentity_GetUserToken_m1_11880 (Object_t * __this /* static, unused */, String_t* ___username, const MethodInfo* method) IL2CPP_METHOD_ATTR;
