﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExifLibrary.ExifAscii
struct ExifAscii_t8_116;
// System.String
struct String_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTag.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifInterOperability.h"

// System.Void ExifLibrary.ExifAscii::.ctor(ExifLibrary.ExifTag,System.String)
extern "C" void ExifAscii__ctor_m8_525 (ExifAscii_t8_116 * __this, int32_t ___tag, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ExifLibrary.ExifAscii::get__Value()
extern "C" Object_t * ExifAscii_get__Value_m8_526 (ExifAscii_t8_116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.ExifAscii::set__Value(System.Object)
extern "C" void ExifAscii_set__Value_m8_527 (ExifAscii_t8_116 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExifLibrary.ExifAscii::get_Value()
extern "C" String_t* ExifAscii_get_Value_m8_528 (ExifAscii_t8_116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.ExifAscii::set_Value(System.String)
extern "C" void ExifAscii_set_Value_m8_529 (ExifAscii_t8_116 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExifLibrary.ExifAscii::ToString()
extern "C" String_t* ExifAscii_ToString_m8_530 (ExifAscii_t8_116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.ExifInterOperability ExifLibrary.ExifAscii::get_Interoperability()
extern "C" ExifInterOperability_t8_113  ExifAscii_get_Interoperability_m8_531 (ExifAscii_t8_116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExifLibrary.ExifAscii::op_Implicit(ExifLibrary.ExifAscii)
extern "C" String_t* ExifAscii_op_Implicit_m8_532 (Object_t * __this /* static, unused */, ExifAscii_t8_116 * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
