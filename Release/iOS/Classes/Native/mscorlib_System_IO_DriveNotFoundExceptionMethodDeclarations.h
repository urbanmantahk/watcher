﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.IO.DriveNotFoundException
struct DriveNotFoundException_t1_416;
// System.String
struct String_t;
// System.Exception
struct Exception_t1_33;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.IO.DriveNotFoundException::.ctor()
extern "C" void DriveNotFoundException__ctor_m1_4840 (DriveNotFoundException_t1_416 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.DriveNotFoundException::.ctor(System.String)
extern "C" void DriveNotFoundException__ctor_m1_4841 (DriveNotFoundException_t1_416 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.DriveNotFoundException::.ctor(System.String,System.Exception)
extern "C" void DriveNotFoundException__ctor_m1_4842 (DriveNotFoundException_t1_416 * __this, String_t* ___message, Exception_t1_33 * ___innerException, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.DriveNotFoundException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void DriveNotFoundException__ctor_m1_4843 (DriveNotFoundException_t1_416 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
