﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.FtpWebResponse
struct FtpWebResponse_t3_99;
// System.Net.FtpWebRequest
struct FtpWebRequest_t3_103;
// System.Uri
struct Uri_t3_3;
// System.String
struct String_t;
// System.Net.FtpStatus
struct FtpStatus_t3_105;
// System.Net.WebHeaderCollection
struct WebHeaderCollection_t3_80;
// System.IO.Stream
struct Stream_t1_405;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_FtpStatusCode.h"
#include "mscorlib_System_DateTime.h"

// System.Void System.Net.FtpWebResponse::.ctor(System.Net.FtpWebRequest,System.Uri,System.String,System.Boolean)
extern "C" void FtpWebResponse__ctor_m3_680 (FtpWebResponse_t3_99 * __this, FtpWebRequest_t3_103 * ___request, Uri_t3_3 * ___uri, String_t* ___method, bool ___keepAlive, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.FtpWebResponse::.ctor(System.Net.FtpWebRequest,System.Uri,System.String,System.Net.FtpStatusCode,System.String)
extern "C" void FtpWebResponse__ctor_m3_681 (FtpWebResponse_t3_99 * __this, FtpWebRequest_t3_103 * ___request, Uri_t3_3 * ___uri, String_t* ___method, int32_t ___statusCode, String_t* ___statusDescription, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.FtpWebResponse::.ctor(System.Net.FtpWebRequest,System.Uri,System.String,System.Net.FtpStatus)
extern "C" void FtpWebResponse__ctor_m3_682 (FtpWebResponse_t3_99 * __this, FtpWebRequest_t3_103 * ___request, Uri_t3_3 * ___uri, String_t* ___method, FtpStatus_t3_105 * ___status, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebHeaderCollection System.Net.FtpWebResponse::get_Headers()
extern "C" WebHeaderCollection_t3_80 * FtpWebResponse_get_Headers_m3_683 (FtpWebResponse_t3_99 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.FtpWebResponse::set_LastModified(System.DateTime)
extern "C" void FtpWebResponse_set_LastModified_m3_684 (FtpWebResponse_t3_99 * __this, DateTime_t1_150  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.FtpWebResponse::set_BannerMessage(System.String)
extern "C" void FtpWebResponse_set_BannerMessage_m3_685 (FtpWebResponse_t3_99 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.FtpWebResponse::set_WelcomeMessage(System.String)
extern "C" void FtpWebResponse_set_WelcomeMessage_m3_686 (FtpWebResponse_t3_99 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.FtpWebResponse::Close()
extern "C" void FtpWebResponse_Close_m3_687 (FtpWebResponse_t3_99 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream System.Net.FtpWebResponse::GetResponseStream()
extern "C" Stream_t1_405 * FtpWebResponse_GetResponseStream_m3_688 (FtpWebResponse_t3_99 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.FtpWebResponse::set_Stream(System.IO.Stream)
extern "C" void FtpWebResponse_set_Stream_m3_689 (FtpWebResponse_t3_99 * __this, Stream_t1_405 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.FtpWebResponse::UpdateStatus(System.Net.FtpStatus)
extern "C" void FtpWebResponse_UpdateStatus_m3_690 (FtpWebResponse_t3_99 * __this, FtpStatus_t3_105 * ___status, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.FtpWebResponse::CheckDisposed()
extern "C" void FtpWebResponse_CheckDisposed_m3_691 (FtpWebResponse_t3_99 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.FtpWebResponse::IsFinal()
extern "C" bool FtpWebResponse_IsFinal_m3_692 (FtpWebResponse_t3_99 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
