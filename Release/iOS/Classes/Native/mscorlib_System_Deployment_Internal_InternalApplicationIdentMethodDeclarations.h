﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Object_t;
// System.ActivationContext
struct ActivationContext_t1_717;
// System.ApplicationIdentity
struct ApplicationIdentity_t1_718;

#include "codegen/il2cpp-codegen.h"

// System.Object System.Deployment.Internal.InternalApplicationIdentityHelper::GetActivationContextData(System.ActivationContext)
extern "C" Object_t * InternalApplicationIdentityHelper_GetActivationContextData_m1_3550 (Object_t * __this /* static, unused */, ActivationContext_t1_717 * ___appInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Deployment.Internal.InternalApplicationIdentityHelper::GetInternalAppId(System.ApplicationIdentity)
extern "C" Object_t * InternalApplicationIdentityHelper_GetInternalAppId_m1_3551 (Object_t * __this /* static, unused */, ApplicationIdentity_t1_718 * ___id, const MethodInfo* method) IL2CPP_METHOD_ATTR;
