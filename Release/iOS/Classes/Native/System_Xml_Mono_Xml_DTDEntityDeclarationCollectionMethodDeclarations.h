﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Xml.DTDEntityDeclarationCollection
struct DTDEntityDeclarationCollection_t4_86;
// Mono.Xml.DTDObjectModel
struct DTDObjectModel_t4_82;
// Mono.Xml.DTDEntityDeclaration
struct DTDEntityDeclaration_t4_98;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Xml.DTDEntityDeclarationCollection::.ctor(Mono.Xml.DTDObjectModel)
extern "C" void DTDEntityDeclarationCollection__ctor_m4_170 (DTDEntityDeclarationCollection_t4_86 * __this, DTDObjectModel_t4_82 * ___root, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Xml.DTDEntityDeclaration Mono.Xml.DTDEntityDeclarationCollection::get_Item(System.String)
extern "C" DTDEntityDeclaration_t4_98 * DTDEntityDeclarationCollection_get_Item_m4_171 (DTDEntityDeclarationCollection_t4_86 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Xml.DTDEntityDeclarationCollection::Add(System.String,Mono.Xml.DTDEntityDeclaration)
extern "C" void DTDEntityDeclarationCollection_Add_m4_172 (DTDEntityDeclarationCollection_t4_86 * __this, String_t* ___name, DTDEntityDeclaration_t4_98 * ___decl, const MethodInfo* method) IL2CPP_METHOD_ATTR;
