﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Object
struct Object_t;

#include "mscorlib_System_Attribute.h"

// System.Runtime.Remoting.Metadata.SoapAttribute
struct  SoapAttribute_t1_997  : public Attribute_t1_2
{
	// System.Boolean System.Runtime.Remoting.Metadata.SoapAttribute::_nested
	bool ____nested_0;
	// System.Boolean System.Runtime.Remoting.Metadata.SoapAttribute::_useAttribute
	bool ____useAttribute_1;
	// System.String System.Runtime.Remoting.Metadata.SoapAttribute::ProtXmlNamespace
	String_t* ___ProtXmlNamespace_2;
	// System.Object System.Runtime.Remoting.Metadata.SoapAttribute::ReflectInfo
	Object_t * ___ReflectInfo_3;
};
