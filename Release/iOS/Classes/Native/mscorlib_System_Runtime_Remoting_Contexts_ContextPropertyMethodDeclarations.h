﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Contexts.ContextProperty
struct ContextProperty_t1_898;
// System.String
struct String_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Contexts.ContextProperty::.ctor(System.String,System.Object)
extern "C" void ContextProperty__ctor_m1_8178 (ContextProperty_t1_898 * __this, String_t* ___name, Object_t * ___prop, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Contexts.ContextProperty::get_Name()
extern "C" String_t* ContextProperty_get_Name_m1_8179 (ContextProperty_t1_898 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Contexts.ContextProperty::get_Property()
extern "C" Object_t * ContextProperty_get_Property_m1_8180 (ContextProperty_t1_898 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
