﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Serialization.Formatters.InternalST
struct InternalST_t1_1069;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Reflection.Assembly
struct Assembly_t1_467;
// System.String
struct String_t;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Serialization.Formatters.InternalST::.ctor()
extern "C" void InternalST__ctor_m1_9463 (InternalST_t1_1069 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.Formatters.InternalST::InfoSoap(System.Object[])
extern "C" void InternalST_InfoSoap_m1_9464 (Object_t * __this /* static, unused */, ObjectU5BU5D_t1_272* ___messages, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.Runtime.Serialization.Formatters.InternalST::LoadAssemblyFromString(System.String)
extern "C" Assembly_t1_467 * InternalST_LoadAssemblyFromString_m1_9465 (Object_t * __this /* static, unused */, String_t* ___assemblyString, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.Formatters.InternalST::SerializationSetValue(System.Reflection.FieldInfo,System.Object,System.Object)
extern "C" void InternalST_SerializationSetValue_m1_9466 (Object_t * __this /* static, unused */, FieldInfo_t * ___fi, Object_t * ___target, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.Formatters.InternalST::Soap(System.Object[])
extern "C" void InternalST_Soap_m1_9467 (Object_t * __this /* static, unused */, ObjectU5BU5D_t1_272* ___messages, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.Formatters.InternalST::SoapAssert(System.Boolean,System.String)
extern "C" void InternalST_SoapAssert_m1_9468 (Object_t * __this /* static, unused */, bool ___condition, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Serialization.Formatters.InternalST::SoapCheckEnabled()
extern "C" bool InternalST_SoapCheckEnabled_m1_9469 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
