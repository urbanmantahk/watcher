﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Runtime.Remoting.Messaging.Header[]
struct HeaderU5BU5D_t1_927;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1_238;
// System.Type[]
struct TypeU5BU5D_t1_31;
// System.Object[]
struct ObjectU5BU5D_t1_272;

#include "mscorlib_System_Object.h"

// System.Runtime.Serialization.Formatters.SoapMessage
struct  SoapMessage_t1_1072  : public Object_t
{
	// System.Runtime.Remoting.Messaging.Header[] System.Runtime.Serialization.Formatters.SoapMessage::headers
	HeaderU5BU5D_t1_927* ___headers_0;
	// System.String System.Runtime.Serialization.Formatters.SoapMessage::methodName
	String_t* ___methodName_1;
	// System.String[] System.Runtime.Serialization.Formatters.SoapMessage::paramNames
	StringU5BU5D_t1_238* ___paramNames_2;
	// System.Type[] System.Runtime.Serialization.Formatters.SoapMessage::paramTypes
	TypeU5BU5D_t1_31* ___paramTypes_3;
	// System.Object[] System.Runtime.Serialization.Formatters.SoapMessage::paramValues
	ObjectU5BU5D_t1_272* ___paramValues_4;
	// System.String System.Runtime.Serialization.Formatters.SoapMessage::xmlNameSpace
	String_t* ___xmlNameSpace_5;
};
