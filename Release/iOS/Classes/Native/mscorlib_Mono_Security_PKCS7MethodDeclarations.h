﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.PKCS7
struct PKCS7_t1_229;
// Mono.Security.ASN1
struct ASN1_t1_149;
// System.String
struct String_t;
// Mono.Security.X509.X509Certificate
struct X509Certificate_t1_151;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Security.PKCS7::.ctor()
extern "C" void PKCS7__ctor_m1_2555 (PKCS7_t1_229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.ASN1 Mono.Security.PKCS7::Attribute(System.String,Mono.Security.ASN1)
extern "C" ASN1_t1_149 * PKCS7_Attribute_m1_2556 (Object_t * __this /* static, unused */, String_t* ___oid, ASN1_t1_149 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.ASN1 Mono.Security.PKCS7::AlgorithmIdentifier(System.String)
extern "C" ASN1_t1_149 * PKCS7_AlgorithmIdentifier_m1_2557 (Object_t * __this /* static, unused */, String_t* ___oid, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.ASN1 Mono.Security.PKCS7::AlgorithmIdentifier(System.String,Mono.Security.ASN1)
extern "C" ASN1_t1_149 * PKCS7_AlgorithmIdentifier_m1_2558 (Object_t * __this /* static, unused */, String_t* ___oid, ASN1_t1_149 * ___parameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.ASN1 Mono.Security.PKCS7::IssuerAndSerialNumber(Mono.Security.X509.X509Certificate)
extern "C" ASN1_t1_149 * PKCS7_IssuerAndSerialNumber_m1_2559 (Object_t * __this /* static, unused */, X509Certificate_t1_151 * ___x509, const MethodInfo* method) IL2CPP_METHOD_ATTR;
