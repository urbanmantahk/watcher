﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.EqualityComparer`1<System.Object>
struct EqualityComparer_1_t1_1942;

#include "mscorlib_System_Object.h"

// System.Collections.Generic.EqualityComparer`1<System.Object>
struct  EqualityComparer_1_t1_1942  : public Object_t
{
};
struct EqualityComparer_1_t1_1942_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t1_1942 * ____default_0;
};
