﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.TwitterSession
struct TwitterSession_t8_295;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.TwitterSession::.ctor()
extern "C" void TwitterSession__ctor_m8_1718 (TwitterSession_t8_295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.TwitterSession::get_AuthToken()
extern "C" String_t* TwitterSession_get_AuthToken_m8_1719 (TwitterSession_t8_295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.TwitterSession::set_AuthToken(System.String)
extern "C" void TwitterSession_set_AuthToken_m8_1720 (TwitterSession_t8_295 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.TwitterSession::get_AuthTokenSecret()
extern "C" String_t* TwitterSession_get_AuthTokenSecret_m8_1721 (TwitterSession_t8_295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.TwitterSession::set_AuthTokenSecret(System.String)
extern "C" void TwitterSession_set_AuthTokenSecret_m8_1722 (TwitterSession_t8_295 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.TwitterSession::get_UserName()
extern "C" String_t* TwitterSession_get_UserName_m8_1723 (TwitterSession_t8_295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.TwitterSession::set_UserName(System.String)
extern "C" void TwitterSession_set_UserName_m8_1724 (TwitterSession_t8_295 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.TwitterSession::get_UserID()
extern "C" String_t* TwitterSession_get_UserID_m8_1725 (TwitterSession_t8_295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.TwitterSession::set_UserID(System.String)
extern "C" void TwitterSession_set_UserID_m8_1726 (TwitterSession_t8_295 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.TwitterSession::ToString()
extern "C" String_t* TwitterSession_ToString_m8_1727 (TwitterSession_t8_295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
