﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Text.ASCIIEncoding
struct ASCIIEncoding_t1_1416;
// System.Char[]
struct CharU5BU5D_t1_16;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Text.EncoderFallbackBuffer
struct EncoderFallbackBuffer_t1_1429;
// System.Text.DecoderFallbackBuffer
struct DecoderFallbackBuffer_t1_1421;
// System.Text.Decoder
struct Decoder_t1_407;
// System.Text.Encoder
struct Encoder_t1_1428;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Text.ASCIIEncoding::.ctor()
extern "C" void ASCIIEncoding__ctor_m1_12174 (ASCIIEncoding_t1_1416 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Text.ASCIIEncoding::get_IsSingleByte()
extern "C" bool ASCIIEncoding_get_IsSingleByte_m1_12175 (ASCIIEncoding_t1_1416 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.ASCIIEncoding::GetByteCount(System.Char[],System.Int32,System.Int32)
extern "C" int32_t ASCIIEncoding_GetByteCount_m1_12176 (ASCIIEncoding_t1_1416 * __this, CharU5BU5D_t1_16* ___chars, int32_t ___index, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.ASCIIEncoding::GetByteCount(System.String)
extern "C" int32_t ASCIIEncoding_GetByteCount_m1_12177 (ASCIIEncoding_t1_1416 * __this, String_t* ___chars, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.ASCIIEncoding::GetBytes(System.Char[],System.Int32,System.Int32,System.Byte[],System.Int32)
extern "C" int32_t ASCIIEncoding_GetBytes_m1_12178 (ASCIIEncoding_t1_1416 * __this, CharU5BU5D_t1_16* ___chars, int32_t ___charIndex, int32_t ___charCount, ByteU5BU5D_t1_109* ___bytes, int32_t ___byteIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.ASCIIEncoding::GetBytes(System.Char[],System.Int32,System.Int32,System.Byte[],System.Int32,System.Text.EncoderFallbackBuffer&,System.Char[]&)
extern "C" int32_t ASCIIEncoding_GetBytes_m1_12179 (ASCIIEncoding_t1_1416 * __this, CharU5BU5D_t1_16* ___chars, int32_t ___charIndex, int32_t ___charCount, ByteU5BU5D_t1_109* ___bytes, int32_t ___byteIndex, EncoderFallbackBuffer_t1_1429 ** ___buffer, CharU5BU5D_t1_16** ___fallback_chars, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.ASCIIEncoding::GetBytes(System.String,System.Int32,System.Int32,System.Byte[],System.Int32)
extern "C" int32_t ASCIIEncoding_GetBytes_m1_12180 (ASCIIEncoding_t1_1416 * __this, String_t* ___chars, int32_t ___charIndex, int32_t ___charCount, ByteU5BU5D_t1_109* ___bytes, int32_t ___byteIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.ASCIIEncoding::GetBytes(System.String,System.Int32,System.Int32,System.Byte[],System.Int32,System.Text.EncoderFallbackBuffer&,System.Char[]&)
extern "C" int32_t ASCIIEncoding_GetBytes_m1_12181 (ASCIIEncoding_t1_1416 * __this, String_t* ___chars, int32_t ___charIndex, int32_t ___charCount, ByteU5BU5D_t1_109* ___bytes, int32_t ___byteIndex, EncoderFallbackBuffer_t1_1429 ** ___buffer, CharU5BU5D_t1_16** ___fallback_chars, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.ASCIIEncoding::GetCharCount(System.Byte[],System.Int32,System.Int32)
extern "C" int32_t ASCIIEncoding_GetCharCount_m1_12182 (ASCIIEncoding_t1_1416 * __this, ByteU5BU5D_t1_109* ___bytes, int32_t ___index, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.ASCIIEncoding::GetChars(System.Byte[],System.Int32,System.Int32,System.Char[],System.Int32)
extern "C" int32_t ASCIIEncoding_GetChars_m1_12183 (ASCIIEncoding_t1_1416 * __this, ByteU5BU5D_t1_109* ___bytes, int32_t ___byteIndex, int32_t ___byteCount, CharU5BU5D_t1_16* ___chars, int32_t ___charIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.ASCIIEncoding::GetChars(System.Byte[],System.Int32,System.Int32,System.Char[],System.Int32,System.Text.DecoderFallbackBuffer&)
extern "C" int32_t ASCIIEncoding_GetChars_m1_12184 (ASCIIEncoding_t1_1416 * __this, ByteU5BU5D_t1_109* ___bytes, int32_t ___byteIndex, int32_t ___byteCount, CharU5BU5D_t1_16* ___chars, int32_t ___charIndex, DecoderFallbackBuffer_t1_1421 ** ___buffer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.ASCIIEncoding::GetMaxByteCount(System.Int32)
extern "C" int32_t ASCIIEncoding_GetMaxByteCount_m1_12185 (ASCIIEncoding_t1_1416 * __this, int32_t ___charCount, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.ASCIIEncoding::GetMaxCharCount(System.Int32)
extern "C" int32_t ASCIIEncoding_GetMaxCharCount_m1_12186 (ASCIIEncoding_t1_1416 * __this, int32_t ___byteCount, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Text.ASCIIEncoding::GetString(System.Byte[],System.Int32,System.Int32)
extern "C" String_t* ASCIIEncoding_GetString_m1_12187 (ASCIIEncoding_t1_1416 * __this, ByteU5BU5D_t1_109* ___bytes, int32_t ___byteIndex, int32_t ___byteCount, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.ASCIIEncoding::GetBytes(System.Char*,System.Int32,System.Byte*,System.Int32)
extern "C" int32_t ASCIIEncoding_GetBytes_m1_12188 (ASCIIEncoding_t1_1416 * __this, uint16_t* ___chars, int32_t ___charCount, uint8_t* ___bytes, int32_t ___byteCount, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.ASCIIEncoding::GetChars(System.Byte*,System.Int32,System.Char*,System.Int32)
extern "C" int32_t ASCIIEncoding_GetChars_m1_12189 (ASCIIEncoding_t1_1416 * __this, uint8_t* ___bytes, int32_t ___byteCount, uint16_t* ___chars, int32_t ___charCount, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.ASCIIEncoding::GetCharCount(System.Byte*,System.Int32)
extern "C" int32_t ASCIIEncoding_GetCharCount_m1_12190 (ASCIIEncoding_t1_1416 * __this, uint8_t* ___bytes, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.ASCIIEncoding::GetByteCount(System.Char*,System.Int32)
extern "C" int32_t ASCIIEncoding_GetByteCount_m1_12191 (ASCIIEncoding_t1_1416 * __this, uint16_t* ___chars, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.Decoder System.Text.ASCIIEncoding::GetDecoder()
extern "C" Decoder_t1_407 * ASCIIEncoding_GetDecoder_m1_12192 (ASCIIEncoding_t1_1416 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.Encoder System.Text.ASCIIEncoding::GetEncoder()
extern "C" Encoder_t1_1428 * ASCIIEncoding_GetEncoder_m1_12193 (ASCIIEncoding_t1_1416 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
