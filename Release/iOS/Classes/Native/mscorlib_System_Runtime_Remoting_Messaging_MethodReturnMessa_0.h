﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Runtime.Remoting.Messaging.ArgInfo
struct ArgInfo_t1_915;
// System.Runtime.Remoting.Messaging.MethodReturnMessageWrapper/DictionaryWrapper
struct DictionaryWrapper_t1_951;
// System.Exception
struct Exception_t1_33;
// System.Object
struct Object_t;

#include "mscorlib_System_Runtime_Remoting_Messaging_InternalMessageWr.h"

// System.Runtime.Remoting.Messaging.MethodReturnMessageWrapper
struct  MethodReturnMessageWrapper_t1_952  : public InternalMessageWrapper_t1_940
{
	// System.Object[] System.Runtime.Remoting.Messaging.MethodReturnMessageWrapper::_args
	ObjectU5BU5D_t1_272* ____args_1;
	// System.Runtime.Remoting.Messaging.ArgInfo System.Runtime.Remoting.Messaging.MethodReturnMessageWrapper::_outArgInfo
	ArgInfo_t1_915 * ____outArgInfo_2;
	// System.Runtime.Remoting.Messaging.MethodReturnMessageWrapper/DictionaryWrapper System.Runtime.Remoting.Messaging.MethodReturnMessageWrapper::_properties
	DictionaryWrapper_t1_951 * ____properties_3;
	// System.Exception System.Runtime.Remoting.Messaging.MethodReturnMessageWrapper::_exception
	Exception_t1_33 * ____exception_4;
	// System.Object System.Runtime.Remoting.Messaging.MethodReturnMessageWrapper::_return
	Object_t * ____return_5;
};
