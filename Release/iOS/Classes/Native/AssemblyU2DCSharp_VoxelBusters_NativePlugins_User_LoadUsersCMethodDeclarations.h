﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.User/LoadUsersCompletion
struct LoadUsersCompletion_t8_233;
// System.Object
struct Object_t;
// VoxelBusters.NativePlugins.User[]
struct UserU5BU5D_t8_234;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_String.h"

// System.Void VoxelBusters.NativePlugins.User/LoadUsersCompletion::.ctor(System.Object,System.IntPtr)
extern "C" void LoadUsersCompletion__ctor_m8_1315 (LoadUsersCompletion_t8_233 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.User/LoadUsersCompletion::Invoke(VoxelBusters.NativePlugins.User[],System.String)
extern "C" void LoadUsersCompletion_Invoke_m8_1316 (LoadUsersCompletion_t8_233 * __this, UserU5BU5D_t8_234* ____users, String_t* ____error, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_LoadUsersCompletion_t8_233(Il2CppObject* delegate, UserU5BU5D_t8_234* ____users, String_t* ____error);
// System.IAsyncResult VoxelBusters.NativePlugins.User/LoadUsersCompletion::BeginInvoke(VoxelBusters.NativePlugins.User[],System.String,System.AsyncCallback,System.Object)
extern "C" Object_t * LoadUsersCompletion_BeginInvoke_m8_1317 (LoadUsersCompletion_t8_233 * __this, UserU5BU5D_t8_234* ____users, String_t* ____error, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.User/LoadUsersCompletion::EndInvoke(System.IAsyncResult)
extern "C" void LoadUsersCompletion_EndInvoke_m8_1318 (LoadUsersCompletion_t8_233 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
