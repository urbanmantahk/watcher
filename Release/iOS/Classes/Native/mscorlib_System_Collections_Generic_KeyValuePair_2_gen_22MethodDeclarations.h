﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_24MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Single>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1_26801(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t1_2696 *, String_t*, float, const MethodInfo*))KeyValuePair_2__ctor_m1_26802_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Single>::get_Key()
#define KeyValuePair_2_get_Key_m1_26803(__this, method) (( String_t* (*) (KeyValuePair_2_t1_2696 *, const MethodInfo*))KeyValuePair_2_get_Key_m1_26804_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Single>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1_26805(__this, ___value, method) (( void (*) (KeyValuePair_2_t1_2696 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m1_26806_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Single>::get_Value()
#define KeyValuePair_2_get_Value_m1_26807(__this, method) (( float (*) (KeyValuePair_2_t1_2696 *, const MethodInfo*))KeyValuePair_2_get_Value_m1_26808_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Single>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1_26809(__this, ___value, method) (( void (*) (KeyValuePair_2_t1_2696 *, float, const MethodInfo*))KeyValuePair_2_set_Value_m1_26810_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Single>::ToString()
#define KeyValuePair_2_ToString_m1_26811(__this, method) (( String_t* (*) (KeyValuePair_2_t1_2696 *, const MethodInfo*))KeyValuePair_2_ToString_m1_26812_gshared)(__this, method)
