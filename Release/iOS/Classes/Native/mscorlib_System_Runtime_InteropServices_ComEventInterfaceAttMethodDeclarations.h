﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.InteropServices.ComEventInterfaceAttribute
struct ComEventInterfaceAttribute_t1_771;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.InteropServices.ComEventInterfaceAttribute::.ctor(System.Type,System.Type)
extern "C" void ComEventInterfaceAttribute__ctor_m1_7609 (ComEventInterfaceAttribute_t1_771 * __this, Type_t * ___SourceInterface, Type_t * ___EventProvider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Runtime.InteropServices.ComEventInterfaceAttribute::get_EventProvider()
extern "C" Type_t * ComEventInterfaceAttribute_get_EventProvider_m1_7610 (ComEventInterfaceAttribute_t1_771 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Runtime.InteropServices.ComEventInterfaceAttribute::get_SourceInterface()
extern "C" Type_t * ComEventInterfaceAttribute_get_SourceInterface_m1_7611 (ComEventInterfaceAttribute_t1_771 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
