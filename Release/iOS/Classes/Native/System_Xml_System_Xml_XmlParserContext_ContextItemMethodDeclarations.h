﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XmlParserContext/ContextItem
struct ContextItem_t4_156;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Xml.XmlParserContext/ContextItem::.ctor()
extern "C" void ContextItem__ctor_m4_679 (ContextItem_t4_156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
