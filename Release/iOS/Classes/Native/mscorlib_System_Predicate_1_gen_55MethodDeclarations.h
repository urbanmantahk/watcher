﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Predicate_1_gen_4MethodDeclarations.h"

// System.Void System.Predicate`1<VoxelBusters.NativePlugins.BillingProduct>::.ctor(System.Object,System.IntPtr)
#define Predicate_1__ctor_m1_27574(__this, ___object, ___method, method) (( void (*) (Predicate_1_t1_2744 *, Object_t *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m1_15354_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<VoxelBusters.NativePlugins.BillingProduct>::Invoke(T)
#define Predicate_1_Invoke_m1_27575(__this, ___obj, method) (( bool (*) (Predicate_1_t1_2744 *, BillingProduct_t8_207 *, const MethodInfo*))Predicate_1_Invoke_m1_15355_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<VoxelBusters.NativePlugins.BillingProduct>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m1_27576(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t1_2744 *, BillingProduct_t8_207 *, AsyncCallback_t1_28 *, Object_t *, const MethodInfo*))Predicate_1_BeginInvoke_m1_15356_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<VoxelBusters.NativePlugins.BillingProduct>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m1_27577(__this, ___result, method) (( bool (*) (Predicate_1_t1_2744 *, Object_t *, const MethodInfo*))Predicate_1_EndInvoke_m1_15357_gshared)(__this, ___result, method)
