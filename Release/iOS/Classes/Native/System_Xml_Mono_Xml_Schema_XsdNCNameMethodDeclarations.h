﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Xml.Schema.XsdNCName
struct XsdNCName_t4_14;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlTokenizedType.h"

// System.Void Mono.Xml.Schema.XsdNCName::.ctor()
extern "C" void XsdNCName__ctor_m4_21 (XsdNCName_t4_14 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdNCName::get_TokenizedType()
extern "C" int32_t XsdNCName_get_TokenizedType_m4_22 (XsdNCName_t4_14 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
