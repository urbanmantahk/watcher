﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.Utility.DownloadTextAsset/Completion
struct Completion_t8_159;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_String.h"

// System.Void VoxelBusters.Utility.DownloadTextAsset/Completion::.ctor(System.Object,System.IntPtr)
extern "C" void Completion__ctor_m8_916 (Completion_t8_159 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.DownloadTextAsset/Completion::Invoke(System.String,System.String)
extern "C" void Completion_Invoke_m8_917 (Completion_t8_159 * __this, String_t* ____text, String_t* ____error, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_Completion_t8_159(Il2CppObject* delegate, String_t* ____text, String_t* ____error);
// System.IAsyncResult VoxelBusters.Utility.DownloadTextAsset/Completion::BeginInvoke(System.String,System.String,System.AsyncCallback,System.Object)
extern "C" Object_t * Completion_BeginInvoke_m8_918 (Completion_t8_159 * __this, String_t* ____text, String_t* ____error, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.DownloadTextAsset/Completion::EndInvoke(System.IAsyncResult)
extern "C" void Completion_EndInvoke_m8_919 (Completion_t8_159 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
