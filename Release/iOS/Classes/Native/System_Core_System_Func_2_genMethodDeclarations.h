﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen_2MethodDeclarations.h"

// System.Void System.Func`2<UnityEngine.UI.Toggle,System.Boolean>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m5_54(__this, ___object, ___method, method) (( void (*) (Func_2_t5_16 *, Object_t *, IntPtr_t, const MethodInfo*))Func_2__ctor_m5_62_gshared)(__this, ___object, ___method, method)
// TResult System.Func`2<UnityEngine.UI.Toggle,System.Boolean>::Invoke(T)
#define Func_2_Invoke_m5_78(__this, ___arg1, method) (( bool (*) (Func_2_t5_16 *, Toggle_t7_65 *, const MethodInfo*))Func_2_Invoke_m5_63_gshared)(__this, ___arg1, method)
// System.IAsyncResult System.Func`2<UnityEngine.UI.Toggle,System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m5_79(__this, ___arg1, ___callback, ___object, method) (( Object_t * (*) (Func_2_t5_16 *, Toggle_t7_65 *, AsyncCallback_t1_28 *, Object_t *, const MethodInfo*))Func_2_BeginInvoke_m5_64_gshared)(__this, ___arg1, ___callback, ___object, method)
// TResult System.Func`2<UnityEngine.UI.Toggle,System.Boolean>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m5_80(__this, ___result, method) (( bool (*) (Func_2_t5_16 *, Object_t *, const MethodInfo*))Func_2_EndInvoke_m5_65_gshared)(__this, ___result, method)
