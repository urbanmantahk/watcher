﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Globalization.TaiwanCalendar
struct TaiwanCalendar_t1_387;
// System.Int32[]
struct Int32U5BU5D_t1_275;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime.h"
#include "mscorlib_System_DayOfWeek.h"
#include "mscorlib_System_Globalization_CalendarWeekRule.h"

// System.Void System.Globalization.TaiwanCalendar::.ctor()
extern "C" void TaiwanCalendar__ctor_m1_4456 (TaiwanCalendar_t1_387 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.TaiwanCalendar::.cctor()
extern "C" void TaiwanCalendar__cctor_m1_4457 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] System.Globalization.TaiwanCalendar::get_Eras()
extern "C" Int32U5BU5D_t1_275* TaiwanCalendar_get_Eras_m1_4458 (TaiwanCalendar_t1_387 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.TaiwanCalendar::get_TwoDigitYearMax()
extern "C" int32_t TaiwanCalendar_get_TwoDigitYearMax_m1_4459 (TaiwanCalendar_t1_387 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.TaiwanCalendar::set_TwoDigitYearMax(System.Int32)
extern "C" void TaiwanCalendar_set_TwoDigitYearMax_m1_4460 (TaiwanCalendar_t1_387 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.TaiwanCalendar::M_CheckDateTime(System.DateTime)
extern "C" void TaiwanCalendar_M_CheckDateTime_m1_4461 (TaiwanCalendar_t1_387 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.TaiwanCalendar::M_CheckEra(System.Int32&)
extern "C" void TaiwanCalendar_M_CheckEra_m1_4462 (TaiwanCalendar_t1_387 * __this, int32_t* ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.TaiwanCalendar::M_CheckYEG(System.Int32,System.Int32&)
extern "C" int32_t TaiwanCalendar_M_CheckYEG_m1_4463 (TaiwanCalendar_t1_387 * __this, int32_t ___year, int32_t* ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.TaiwanCalendar::M_CheckYE(System.Int32,System.Int32&)
extern "C" void TaiwanCalendar_M_CheckYE_m1_4464 (TaiwanCalendar_t1_387 * __this, int32_t ___year, int32_t* ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.TaiwanCalendar::M_CheckYMEG(System.Int32,System.Int32,System.Int32&)
extern "C" int32_t TaiwanCalendar_M_CheckYMEG_m1_4465 (TaiwanCalendar_t1_387 * __this, int32_t ___year, int32_t ___month, int32_t* ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.TaiwanCalendar::M_CheckYMDEG(System.Int32,System.Int32,System.Int32,System.Int32&)
extern "C" int32_t TaiwanCalendar_M_CheckYMDEG_m1_4466 (TaiwanCalendar_t1_387 * __this, int32_t ___year, int32_t ___month, int32_t ___day, int32_t* ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.TaiwanCalendar::AddMonths(System.DateTime,System.Int32)
extern "C" DateTime_t1_150  TaiwanCalendar_AddMonths_m1_4467 (TaiwanCalendar_t1_387 * __this, DateTime_t1_150  ___time, int32_t ___months, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.TaiwanCalendar::AddYears(System.DateTime,System.Int32)
extern "C" DateTime_t1_150  TaiwanCalendar_AddYears_m1_4468 (TaiwanCalendar_t1_387 * __this, DateTime_t1_150  ___time, int32_t ___years, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.TaiwanCalendar::GetDayOfMonth(System.DateTime)
extern "C" int32_t TaiwanCalendar_GetDayOfMonth_m1_4469 (TaiwanCalendar_t1_387 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DayOfWeek System.Globalization.TaiwanCalendar::GetDayOfWeek(System.DateTime)
extern "C" int32_t TaiwanCalendar_GetDayOfWeek_m1_4470 (TaiwanCalendar_t1_387 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.TaiwanCalendar::GetDayOfYear(System.DateTime)
extern "C" int32_t TaiwanCalendar_GetDayOfYear_m1_4471 (TaiwanCalendar_t1_387 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.TaiwanCalendar::GetDaysInMonth(System.Int32,System.Int32,System.Int32)
extern "C" int32_t TaiwanCalendar_GetDaysInMonth_m1_4472 (TaiwanCalendar_t1_387 * __this, int32_t ___year, int32_t ___month, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.TaiwanCalendar::GetDaysInYear(System.Int32,System.Int32)
extern "C" int32_t TaiwanCalendar_GetDaysInYear_m1_4473 (TaiwanCalendar_t1_387 * __this, int32_t ___year, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.TaiwanCalendar::GetEra(System.DateTime)
extern "C" int32_t TaiwanCalendar_GetEra_m1_4474 (TaiwanCalendar_t1_387 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.TaiwanCalendar::GetLeapMonth(System.Int32,System.Int32)
extern "C" int32_t TaiwanCalendar_GetLeapMonth_m1_4475 (TaiwanCalendar_t1_387 * __this, int32_t ___year, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.TaiwanCalendar::GetMonth(System.DateTime)
extern "C" int32_t TaiwanCalendar_GetMonth_m1_4476 (TaiwanCalendar_t1_387 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.TaiwanCalendar::GetMonthsInYear(System.Int32,System.Int32)
extern "C" int32_t TaiwanCalendar_GetMonthsInYear_m1_4477 (TaiwanCalendar_t1_387 * __this, int32_t ___year, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.TaiwanCalendar::GetWeekOfYear(System.DateTime,System.Globalization.CalendarWeekRule,System.DayOfWeek)
extern "C" int32_t TaiwanCalendar_GetWeekOfYear_m1_4478 (TaiwanCalendar_t1_387 * __this, DateTime_t1_150  ___time, int32_t ___rule, int32_t ___firstDayOfWeek, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.TaiwanCalendar::GetYear(System.DateTime)
extern "C" int32_t TaiwanCalendar_GetYear_m1_4479 (TaiwanCalendar_t1_387 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.TaiwanCalendar::IsLeapDay(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" bool TaiwanCalendar_IsLeapDay_m1_4480 (TaiwanCalendar_t1_387 * __this, int32_t ___year, int32_t ___month, int32_t ___day, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.TaiwanCalendar::IsLeapMonth(System.Int32,System.Int32,System.Int32)
extern "C" bool TaiwanCalendar_IsLeapMonth_m1_4481 (TaiwanCalendar_t1_387 * __this, int32_t ___year, int32_t ___month, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.TaiwanCalendar::IsLeapYear(System.Int32,System.Int32)
extern "C" bool TaiwanCalendar_IsLeapYear_m1_4482 (TaiwanCalendar_t1_387 * __this, int32_t ___year, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.TaiwanCalendar::ToDateTime(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" DateTime_t1_150  TaiwanCalendar_ToDateTime_m1_4483 (TaiwanCalendar_t1_387 * __this, int32_t ___year, int32_t ___month, int32_t ___day, int32_t ___hour, int32_t ___minute, int32_t ___second, int32_t ___millisecond, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.TaiwanCalendar::ToFourDigitYear(System.Int32)
extern "C" int32_t TaiwanCalendar_ToFourDigitYear_m1_4484 (TaiwanCalendar_t1_387 * __this, int32_t ___year, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.TaiwanCalendar::get_MinSupportedDateTime()
extern "C" DateTime_t1_150  TaiwanCalendar_get_MinSupportedDateTime_m1_4485 (TaiwanCalendar_t1_387 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.TaiwanCalendar::get_MaxSupportedDateTime()
extern "C" DateTime_t1_150  TaiwanCalendar_get_MaxSupportedDateTime_m1_4486 (TaiwanCalendar_t1_387 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
