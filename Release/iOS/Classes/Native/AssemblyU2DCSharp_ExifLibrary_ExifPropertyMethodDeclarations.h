﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExifLibrary.ExifProperty
struct ExifProperty_t8_99;
// System.String
struct String_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTag.h"
#include "AssemblyU2DCSharp_ExifLibrary_IFD.h"

// System.Void ExifLibrary.ExifProperty::.ctor(ExifLibrary.ExifTag)
extern "C" void ExifProperty__ctor_m8_502 (ExifProperty_t8_99 * __this, int32_t ___tag, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.ExifTag ExifLibrary.ExifProperty::get_Tag()
extern "C" int32_t ExifProperty_get_Tag_m8_503 (ExifProperty_t8_99 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.IFD ExifLibrary.ExifProperty::get_IFD()
extern "C" int32_t ExifProperty_get_IFD_m8_504 (ExifProperty_t8_99 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExifLibrary.ExifProperty::get_Name()
extern "C" String_t* ExifProperty_get_Name_m8_505 (ExifProperty_t8_99 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.ExifProperty::set_Name(System.String)
extern "C" void ExifProperty_set_Name_m8_506 (ExifProperty_t8_99 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ExifLibrary.ExifProperty::get_Value()
extern "C" Object_t * ExifProperty_get_Value_m8_507 (ExifProperty_t8_99 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.ExifProperty::set_Value(System.Object)
extern "C" void ExifProperty_set_Value_m8_508 (ExifProperty_t8_99 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
