﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "System_Xml_Mono_Xml_Schema_XsdUnsignedLong.h"

// Mono.Xml.Schema.XsdUnsignedInt
struct  XsdUnsignedInt_t4_29  : public XsdUnsignedLong_t4_28
{
};
