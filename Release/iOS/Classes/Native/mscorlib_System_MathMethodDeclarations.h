﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Decimal.h"
#include "mscorlib_System_MidpointRounding.h"

// System.Decimal System.Math::Abs(System.Decimal)
extern "C" Decimal_t1_19  Math_Abs_m1_14188 (Object_t * __this /* static, unused */, Decimal_t1_19  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Abs(System.Double)
extern "C" double Math_Abs_m1_14189 (Object_t * __this /* static, unused */, double ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single System.Math::Abs(System.Single)
extern "C" float Math_Abs_m1_14190 (Object_t * __this /* static, unused */, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Math::Abs(System.Int32)
extern "C" int32_t Math_Abs_m1_14191 (Object_t * __this /* static, unused */, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Math::Abs(System.Int64)
extern "C" int64_t Math_Abs_m1_14192 (Object_t * __this /* static, unused */, int64_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.SByte System.Math::Abs(System.SByte)
extern "C" int8_t Math_Abs_m1_14193 (Object_t * __this /* static, unused */, int8_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 System.Math::Abs(System.Int16)
extern "C" int16_t Math_Abs_m1_14194 (Object_t * __this /* static, unused */, int16_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Math::Ceiling(System.Decimal)
extern "C" Decimal_t1_19  Math_Ceiling_m1_14195 (Object_t * __this /* static, unused */, Decimal_t1_19  ___d, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Ceiling(System.Double)
extern "C" double Math_Ceiling_m1_14196 (Object_t * __this /* static, unused */, double ___a, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Math::BigMul(System.Int32,System.Int32)
extern "C" int64_t Math_BigMul_m1_14197 (Object_t * __this /* static, unused */, int32_t ___a, int32_t ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Math::DivRem(System.Int32,System.Int32,System.Int32&)
extern "C" int32_t Math_DivRem_m1_14198 (Object_t * __this /* static, unused */, int32_t ___a, int32_t ___b, int32_t* ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Math::DivRem(System.Int64,System.Int64,System.Int64&)
extern "C" int64_t Math_DivRem_m1_14199 (Object_t * __this /* static, unused */, int64_t ___a, int64_t ___b, int64_t* ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Floor(System.Double)
extern "C" double Math_Floor_m1_14200 (Object_t * __this /* static, unused */, double ___d, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::IEEERemainder(System.Double,System.Double)
extern "C" double Math_IEEERemainder_m1_14201 (Object_t * __this /* static, unused */, double ___x, double ___y, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Log(System.Double,System.Double)
extern "C" double Math_Log_m1_14202 (Object_t * __this /* static, unused */, double ___a, double ___newBase, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte System.Math::Max(System.Byte,System.Byte)
extern "C" uint8_t Math_Max_m1_14203 (Object_t * __this /* static, unused */, uint8_t ___val1, uint8_t ___val2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Math::Max(System.Decimal,System.Decimal)
extern "C" Decimal_t1_19  Math_Max_m1_14204 (Object_t * __this /* static, unused */, Decimal_t1_19  ___val1, Decimal_t1_19  ___val2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Max(System.Double,System.Double)
extern "C" double Math_Max_m1_14205 (Object_t * __this /* static, unused */, double ___val1, double ___val2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single System.Math::Max(System.Single,System.Single)
extern "C" float Math_Max_m1_14206 (Object_t * __this /* static, unused */, float ___val1, float ___val2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Math::Max(System.Int32,System.Int32)
extern "C" int32_t Math_Max_m1_14207 (Object_t * __this /* static, unused */, int32_t ___val1, int32_t ___val2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Math::Max(System.Int64,System.Int64)
extern "C" int64_t Math_Max_m1_14208 (Object_t * __this /* static, unused */, int64_t ___val1, int64_t ___val2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.SByte System.Math::Max(System.SByte,System.SByte)
extern "C" int8_t Math_Max_m1_14209 (Object_t * __this /* static, unused */, int8_t ___val1, int8_t ___val2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 System.Math::Max(System.Int16,System.Int16)
extern "C" int16_t Math_Max_m1_14210 (Object_t * __this /* static, unused */, int16_t ___val1, int16_t ___val2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 System.Math::Max(System.UInt32,System.UInt32)
extern "C" uint32_t Math_Max_m1_14211 (Object_t * __this /* static, unused */, uint32_t ___val1, uint32_t ___val2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 System.Math::Max(System.UInt64,System.UInt64)
extern "C" uint64_t Math_Max_m1_14212 (Object_t * __this /* static, unused */, uint64_t ___val1, uint64_t ___val2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 System.Math::Max(System.UInt16,System.UInt16)
extern "C" uint16_t Math_Max_m1_14213 (Object_t * __this /* static, unused */, uint16_t ___val1, uint16_t ___val2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte System.Math::Min(System.Byte,System.Byte)
extern "C" uint8_t Math_Min_m1_14214 (Object_t * __this /* static, unused */, uint8_t ___val1, uint8_t ___val2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Math::Min(System.Decimal,System.Decimal)
extern "C" Decimal_t1_19  Math_Min_m1_14215 (Object_t * __this /* static, unused */, Decimal_t1_19  ___val1, Decimal_t1_19  ___val2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Min(System.Double,System.Double)
extern "C" double Math_Min_m1_14216 (Object_t * __this /* static, unused */, double ___val1, double ___val2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single System.Math::Min(System.Single,System.Single)
extern "C" float Math_Min_m1_14217 (Object_t * __this /* static, unused */, float ___val1, float ___val2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Math::Min(System.Int32,System.Int32)
extern "C" int32_t Math_Min_m1_14218 (Object_t * __this /* static, unused */, int32_t ___val1, int32_t ___val2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Math::Min(System.Int64,System.Int64)
extern "C" int64_t Math_Min_m1_14219 (Object_t * __this /* static, unused */, int64_t ___val1, int64_t ___val2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.SByte System.Math::Min(System.SByte,System.SByte)
extern "C" int8_t Math_Min_m1_14220 (Object_t * __this /* static, unused */, int8_t ___val1, int8_t ___val2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 System.Math::Min(System.Int16,System.Int16)
extern "C" int16_t Math_Min_m1_14221 (Object_t * __this /* static, unused */, int16_t ___val1, int16_t ___val2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 System.Math::Min(System.UInt32,System.UInt32)
extern "C" uint32_t Math_Min_m1_14222 (Object_t * __this /* static, unused */, uint32_t ___val1, uint32_t ___val2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 System.Math::Min(System.UInt64,System.UInt64)
extern "C" uint64_t Math_Min_m1_14223 (Object_t * __this /* static, unused */, uint64_t ___val1, uint64_t ___val2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 System.Math::Min(System.UInt16,System.UInt16)
extern "C" uint16_t Math_Min_m1_14224 (Object_t * __this /* static, unused */, uint16_t ___val1, uint16_t ___val2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Math::Round(System.Decimal)
extern "C" Decimal_t1_19  Math_Round_m1_14225 (Object_t * __this /* static, unused */, Decimal_t1_19  ___d, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Math::Round(System.Decimal,System.Int32)
extern "C" Decimal_t1_19  Math_Round_m1_14226 (Object_t * __this /* static, unused */, Decimal_t1_19  ___d, int32_t ___decimals, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Math::Round(System.Decimal,System.MidpointRounding)
extern "C" Decimal_t1_19  Math_Round_m1_14227 (Object_t * __this /* static, unused */, Decimal_t1_19  ___d, int32_t ___mode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Math::RoundAwayFromZero(System.Decimal)
extern "C" Decimal_t1_19  Math_RoundAwayFromZero_m1_14228 (Object_t * __this /* static, unused */, Decimal_t1_19  ___d, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Math::Round(System.Decimal,System.Int32,System.MidpointRounding)
extern "C" Decimal_t1_19  Math_Round_m1_14229 (Object_t * __this /* static, unused */, Decimal_t1_19  ___d, int32_t ___decimals, int32_t ___mode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Round(System.Double)
extern "C" double Math_Round_m1_14230 (Object_t * __this /* static, unused */, double ___a, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Round(System.Double,System.Int32)
extern "C" double Math_Round_m1_14231 (Object_t * __this /* static, unused */, double ___value, int32_t ___digits, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Round2(System.Double,System.Int32,System.Boolean)
extern "C" double Math_Round2_m1_14232 (Object_t * __this /* static, unused */, double ___value, int32_t ___digits, bool ___away_from_zero, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Round(System.Double,System.MidpointRounding)
extern "C" double Math_Round_m1_14233 (Object_t * __this /* static, unused */, double ___value, int32_t ___mode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Round(System.Double,System.Int32,System.MidpointRounding)
extern "C" double Math_Round_m1_14234 (Object_t * __this /* static, unused */, double ___value, int32_t ___digits, int32_t ___mode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Truncate(System.Double)
extern "C" double Math_Truncate_m1_14235 (Object_t * __this /* static, unused */, double ___d, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Math::Truncate(System.Decimal)
extern "C" Decimal_t1_19  Math_Truncate_m1_14236 (Object_t * __this /* static, unused */, Decimal_t1_19  ___d, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Math::Floor(System.Decimal)
extern "C" Decimal_t1_19  Math_Floor_m1_14237 (Object_t * __this /* static, unused */, Decimal_t1_19  ___d, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Math::Sign(System.Decimal)
extern "C" int32_t Math_Sign_m1_14238 (Object_t * __this /* static, unused */, Decimal_t1_19  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Math::Sign(System.Double)
extern "C" int32_t Math_Sign_m1_14239 (Object_t * __this /* static, unused */, double ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Math::Sign(System.Single)
extern "C" int32_t Math_Sign_m1_14240 (Object_t * __this /* static, unused */, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Math::Sign(System.Int32)
extern "C" int32_t Math_Sign_m1_14241 (Object_t * __this /* static, unused */, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Math::Sign(System.Int64)
extern "C" int32_t Math_Sign_m1_14242 (Object_t * __this /* static, unused */, int64_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Math::Sign(System.SByte)
extern "C" int32_t Math_Sign_m1_14243 (Object_t * __this /* static, unused */, int8_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Math::Sign(System.Int16)
extern "C" int32_t Math_Sign_m1_14244 (Object_t * __this /* static, unused */, int16_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Sin(System.Double)
extern "C" double Math_Sin_m1_14245 (Object_t * __this /* static, unused */, double ___a, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Cos(System.Double)
extern "C" double Math_Cos_m1_14246 (Object_t * __this /* static, unused */, double ___d, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Tan(System.Double)
extern "C" double Math_Tan_m1_14247 (Object_t * __this /* static, unused */, double ___a, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Sinh(System.Double)
extern "C" double Math_Sinh_m1_14248 (Object_t * __this /* static, unused */, double ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Cosh(System.Double)
extern "C" double Math_Cosh_m1_14249 (Object_t * __this /* static, unused */, double ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Tanh(System.Double)
extern "C" double Math_Tanh_m1_14250 (Object_t * __this /* static, unused */, double ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Acos(System.Double)
extern "C" double Math_Acos_m1_14251 (Object_t * __this /* static, unused */, double ___d, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Asin(System.Double)
extern "C" double Math_Asin_m1_14252 (Object_t * __this /* static, unused */, double ___d, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Atan(System.Double)
extern "C" double Math_Atan_m1_14253 (Object_t * __this /* static, unused */, double ___d, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Atan2(System.Double,System.Double)
extern "C" double Math_Atan2_m1_14254 (Object_t * __this /* static, unused */, double ___y, double ___x, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Exp(System.Double)
extern "C" double Math_Exp_m1_14255 (Object_t * __this /* static, unused */, double ___d, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Log(System.Double)
extern "C" double Math_Log_m1_14256 (Object_t * __this /* static, unused */, double ___d, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Log10(System.Double)
extern "C" double Math_Log10_m1_14257 (Object_t * __this /* static, unused */, double ___d, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Pow(System.Double,System.Double)
extern "C" double Math_Pow_m1_14258 (Object_t * __this /* static, unused */, double ___x, double ___y, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Sqrt(System.Double)
extern "C" double Math_Sqrt_m1_14259 (Object_t * __this /* static, unused */, double ___d, const MethodInfo* method) IL2CPP_METHOD_ATTR;
