﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Security_AccessControl_AuditRule.h"
#include "mscorlib_System_Security_AccessControl_RegistryRights.h"

// System.Security.AccessControl.RegistryAuditRule
struct  RegistryAuditRule_t1_1173  : public AuditRule_t1_1119
{
	// System.Security.AccessControl.RegistryRights System.Security.AccessControl.RegistryAuditRule::rights
	int32_t ___rights_6;
};
