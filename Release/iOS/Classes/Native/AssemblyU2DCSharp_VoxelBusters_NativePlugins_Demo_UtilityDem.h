﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Demo_NPDemoBase.h"

// VoxelBusters.NativePlugins.Demo.UtilityDemo
struct  UtilityDemo_t8_189  : public NPDemoBase_t8_174
{
	// System.Int32 VoxelBusters.NativePlugins.Demo.UtilityDemo::m_applicationBadgeNumber
	int32_t ___m_applicationBadgeNumber_13;
};
