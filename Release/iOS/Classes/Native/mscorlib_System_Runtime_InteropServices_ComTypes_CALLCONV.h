﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_CALLCONV.h"

// System.Runtime.InteropServices.ComTypes.CALLCONV
struct  CALLCONV_t1_722 
{
	// System.Int32 System.Runtime.InteropServices.ComTypes.CALLCONV::value__
	int32_t ___value___1;
};
