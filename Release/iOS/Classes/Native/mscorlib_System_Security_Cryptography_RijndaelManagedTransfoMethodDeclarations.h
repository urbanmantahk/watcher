﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.RijndaelManagedTransform
struct RijndaelManagedTransform_t1_1247;
// System.Security.Cryptography.Rijndael
struct Rijndael_t1_1244;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Cryptography.RijndaelManagedTransform::.ctor(System.Security.Cryptography.Rijndael,System.Boolean,System.Byte[],System.Byte[])
extern "C" void RijndaelManagedTransform__ctor_m1_10629 (RijndaelManagedTransform_t1_1247 * __this, Rijndael_t1_1244 * ___algo, bool ___encryption, ByteU5BU5D_t1_109* ___key, ByteU5BU5D_t1_109* ___iv, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RijndaelManagedTransform::System.IDisposable.Dispose()
extern "C" void RijndaelManagedTransform_System_IDisposable_Dispose_m1_10630 (RijndaelManagedTransform_t1_1247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Cryptography.RijndaelManagedTransform::get_BlockSizeValue()
extern "C" int32_t RijndaelManagedTransform_get_BlockSizeValue_m1_10631 (RijndaelManagedTransform_t1_1247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.RijndaelManagedTransform::get_CanTransformMultipleBlocks()
extern "C" bool RijndaelManagedTransform_get_CanTransformMultipleBlocks_m1_10632 (RijndaelManagedTransform_t1_1247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.RijndaelManagedTransform::get_CanReuseTransform()
extern "C" bool RijndaelManagedTransform_get_CanReuseTransform_m1_10633 (RijndaelManagedTransform_t1_1247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Cryptography.RijndaelManagedTransform::get_InputBlockSize()
extern "C" int32_t RijndaelManagedTransform_get_InputBlockSize_m1_10634 (RijndaelManagedTransform_t1_1247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Cryptography.RijndaelManagedTransform::get_OutputBlockSize()
extern "C" int32_t RijndaelManagedTransform_get_OutputBlockSize_m1_10635 (RijndaelManagedTransform_t1_1247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RijndaelManagedTransform::Clear()
extern "C" void RijndaelManagedTransform_Clear_m1_10636 (RijndaelManagedTransform_t1_1247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RijndaelManagedTransform::Reset()
extern "C" void RijndaelManagedTransform_Reset_m1_10637 (RijndaelManagedTransform_t1_1247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Cryptography.RijndaelManagedTransform::TransformBlock(System.Byte[],System.Int32,System.Int32,System.Byte[],System.Int32)
extern "C" int32_t RijndaelManagedTransform_TransformBlock_m1_10638 (RijndaelManagedTransform_t1_1247 * __this, ByteU5BU5D_t1_109* ___inputBuffer, int32_t ___inputOffset, int32_t ___inputCount, ByteU5BU5D_t1_109* ___outputBuffer, int32_t ___outputOffset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.RijndaelManagedTransform::TransformFinalBlock(System.Byte[],System.Int32,System.Int32)
extern "C" ByteU5BU5D_t1_109* RijndaelManagedTransform_TransformFinalBlock_m1_10639 (RijndaelManagedTransform_t1_1247 * __this, ByteU5BU5D_t1_109* ___inputBuffer, int32_t ___inputOffset, int32_t ___inputCount, const MethodInfo* method) IL2CPP_METHOD_ATTR;
