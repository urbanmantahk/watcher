﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.AccessControl.NativeObjectSecurity
struct NativeObjectSecurity_t1_1143;
// System.Security.AccessControl.NativeObjectSecurity/ExceptionFromErrorCode
struct ExceptionFromErrorCode_t1_1162;
// System.Object
struct Object_t;
// System.Runtime.InteropServices.SafeHandle
struct SafeHandle_t1_88;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_AccessControl_ResourceType.h"
#include "mscorlib_System_Security_AccessControl_AccessControlSections.h"

// System.Void System.Security.AccessControl.NativeObjectSecurity::.ctor()
extern "C" void NativeObjectSecurity__ctor_m1_9959 (NativeObjectSecurity_t1_1143 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.NativeObjectSecurity::.ctor(System.Boolean,System.Security.AccessControl.ResourceType)
extern "C" void NativeObjectSecurity__ctor_m1_9960 (NativeObjectSecurity_t1_1143 * __this, bool ___isContainer, int32_t ___resourceType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.NativeObjectSecurity::.ctor(System.Boolean,System.Security.AccessControl.ResourceType,System.Security.AccessControl.NativeObjectSecurity/ExceptionFromErrorCode,System.Object)
extern "C" void NativeObjectSecurity__ctor_m1_9961 (NativeObjectSecurity_t1_1143 * __this, bool ___isContainer, int32_t ___resourceType, ExceptionFromErrorCode_t1_1162 * ___exceptionFromErrorCode, Object_t * ___exceptionContext, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.NativeObjectSecurity::.ctor(System.Boolean,System.Security.AccessControl.ResourceType,System.Runtime.InteropServices.SafeHandle,System.Security.AccessControl.AccessControlSections)
extern "C" void NativeObjectSecurity__ctor_m1_9962 (NativeObjectSecurity_t1_1143 * __this, bool ___isContainer, int32_t ___resourceType, SafeHandle_t1_88 * ___handle, int32_t ___includeSections, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.NativeObjectSecurity::.ctor(System.Boolean,System.Security.AccessControl.ResourceType,System.String,System.Security.AccessControl.AccessControlSections)
extern "C" void NativeObjectSecurity__ctor_m1_9963 (NativeObjectSecurity_t1_1143 * __this, bool ___isContainer, int32_t ___resourceType, String_t* ___name, int32_t ___includeSections, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.NativeObjectSecurity::.ctor(System.Boolean,System.Security.AccessControl.ResourceType,System.Runtime.InteropServices.SafeHandle,System.Security.AccessControl.AccessControlSections,System.Security.AccessControl.NativeObjectSecurity/ExceptionFromErrorCode,System.Object)
extern "C" void NativeObjectSecurity__ctor_m1_9964 (NativeObjectSecurity_t1_1143 * __this, bool ___isContainer, int32_t ___resourceType, SafeHandle_t1_88 * ___handle, int32_t ___includeSections, ExceptionFromErrorCode_t1_1162 * ___exceptionFromErrorCode, Object_t * ___exceptionContext, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.NativeObjectSecurity::.ctor(System.Boolean,System.Security.AccessControl.ResourceType,System.String,System.Security.AccessControl.AccessControlSections,System.Security.AccessControl.NativeObjectSecurity/ExceptionFromErrorCode,System.Object)
extern "C" void NativeObjectSecurity__ctor_m1_9965 (NativeObjectSecurity_t1_1143 * __this, bool ___isContainer, int32_t ___resourceType, String_t* ___name, int32_t ___includeSections, ExceptionFromErrorCode_t1_1162 * ___exceptionFromErrorCode, Object_t * ___exceptionContext, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.NativeObjectSecurity::Persist(System.Runtime.InteropServices.SafeHandle,System.Security.AccessControl.AccessControlSections)
extern "C" void NativeObjectSecurity_Persist_m1_9966 (NativeObjectSecurity_t1_1143 * __this, SafeHandle_t1_88 * ___handle, int32_t ___includeSections, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.NativeObjectSecurity::Persist(System.String,System.Security.AccessControl.AccessControlSections)
extern "C" void NativeObjectSecurity_Persist_m1_9967 (NativeObjectSecurity_t1_1143 * __this, String_t* ___name, int32_t ___includeSections, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.NativeObjectSecurity::Persist(System.Runtime.InteropServices.SafeHandle,System.Security.AccessControl.AccessControlSections,System.Object)
extern "C" void NativeObjectSecurity_Persist_m1_9968 (NativeObjectSecurity_t1_1143 * __this, SafeHandle_t1_88 * ___handle, int32_t ___includeSections, Object_t * ___exceptionContext, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.NativeObjectSecurity::Persist(System.String,System.Security.AccessControl.AccessControlSections,System.Object)
extern "C" void NativeObjectSecurity_Persist_m1_9969 (NativeObjectSecurity_t1_1143 * __this, String_t* ___name, int32_t ___includeSections, Object_t * ___exceptionContext, const MethodInfo* method) IL2CPP_METHOD_ATTR;
