﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.ActivatedClientTypeEntry
struct ActivatedClientTypeEntry_t1_1008;
// System.Type
struct Type_t;
// System.String
struct String_t;
// System.Runtime.Remoting.Contexts.IContextAttribute[]
struct IContextAttributeU5BU5D_t1_1722;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.ActivatedClientTypeEntry::.ctor(System.Type,System.String)
extern "C" void ActivatedClientTypeEntry__ctor_m1_9036 (ActivatedClientTypeEntry_t1_1008 * __this, Type_t * ___type, String_t* ___appUrl, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.ActivatedClientTypeEntry::.ctor(System.String,System.String,System.String)
extern "C" void ActivatedClientTypeEntry__ctor_m1_9037 (ActivatedClientTypeEntry_t1_1008 * __this, String_t* ___typeName, String_t* ___assemblyName, String_t* ___appUrl, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.ActivatedClientTypeEntry::get_ApplicationUrl()
extern "C" String_t* ActivatedClientTypeEntry_get_ApplicationUrl_m1_9038 (ActivatedClientTypeEntry_t1_1008 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Contexts.IContextAttribute[] System.Runtime.Remoting.ActivatedClientTypeEntry::get_ContextAttributes()
extern "C" IContextAttributeU5BU5D_t1_1722* ActivatedClientTypeEntry_get_ContextAttributes_m1_9039 (ActivatedClientTypeEntry_t1_1008 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.ActivatedClientTypeEntry::set_ContextAttributes(System.Runtime.Remoting.Contexts.IContextAttribute[])
extern "C" void ActivatedClientTypeEntry_set_ContextAttributes_m1_9040 (ActivatedClientTypeEntry_t1_1008 * __this, IContextAttributeU5BU5D_t1_1722* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Runtime.Remoting.ActivatedClientTypeEntry::get_ObjectType()
extern "C" Type_t * ActivatedClientTypeEntry_get_ObjectType_m1_9041 (ActivatedClientTypeEntry_t1_1008 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.ActivatedClientTypeEntry::ToString()
extern "C" String_t* ActivatedClientTypeEntry_ToString_m1_9042 (ActivatedClientTypeEntry_t1_1008 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
