﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.MediaLibrarySettings/AndroidSettings
struct AndroidSettings_t8_252;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.MediaLibrarySettings/AndroidSettings::.ctor()
extern "C" void AndroidSettings__ctor_m8_1394 (AndroidSettings_t8_252 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.MediaLibrarySettings/AndroidSettings::get_YoutubeAPIKey()
extern "C" String_t* AndroidSettings_get_YoutubeAPIKey_m8_1395 (AndroidSettings_t8_252 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.MediaLibrarySettings/AndroidSettings::set_YoutubeAPIKey(System.String)
extern "C" void AndroidSettings_set_YoutubeAPIKey_m8_1396 (AndroidSettings_t8_252 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
