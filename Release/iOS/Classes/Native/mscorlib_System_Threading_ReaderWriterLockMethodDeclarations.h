﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Threading.ReaderWriterLock
struct ReaderWriterLock_t1_1468;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_TimeSpan.h"
#include "mscorlib_System_Threading_LockCookie.h"

// System.Void System.Threading.ReaderWriterLock::.ctor()
extern "C" void ReaderWriterLock__ctor_m1_12744 (ReaderWriterLock_t1_1468 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.ReaderWriterLock::Finalize()
extern "C" void ReaderWriterLock_Finalize_m1_12745 (ReaderWriterLock_t1_1468 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.ReaderWriterLock::get_IsReaderLockHeld()
extern "C" bool ReaderWriterLock_get_IsReaderLockHeld_m1_12746 (ReaderWriterLock_t1_1468 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.ReaderWriterLock::get_IsWriterLockHeld()
extern "C" bool ReaderWriterLock_get_IsWriterLockHeld_m1_12747 (ReaderWriterLock_t1_1468 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Threading.ReaderWriterLock::get_WriterSeqNum()
extern "C" int32_t ReaderWriterLock_get_WriterSeqNum_m1_12748 (ReaderWriterLock_t1_1468 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.ReaderWriterLock::AcquireReaderLock(System.Int32)
extern "C" void ReaderWriterLock_AcquireReaderLock_m1_12749 (ReaderWriterLock_t1_1468 * __this, int32_t ___millisecondsTimeout, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.ReaderWriterLock::AcquireReaderLock(System.Int32,System.Int32)
extern "C" void ReaderWriterLock_AcquireReaderLock_m1_12750 (ReaderWriterLock_t1_1468 * __this, int32_t ___millisecondsTimeout, int32_t ___initialLockCount, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.ReaderWriterLock::AcquireReaderLock(System.TimeSpan)
extern "C" void ReaderWriterLock_AcquireReaderLock_m1_12751 (ReaderWriterLock_t1_1468 * __this, TimeSpan_t1_368  ___timeout, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.ReaderWriterLock::AcquireWriterLock(System.Int32)
extern "C" void ReaderWriterLock_AcquireWriterLock_m1_12752 (ReaderWriterLock_t1_1468 * __this, int32_t ___millisecondsTimeout, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.ReaderWriterLock::AcquireWriterLock(System.Int32,System.Int32)
extern "C" void ReaderWriterLock_AcquireWriterLock_m1_12753 (ReaderWriterLock_t1_1468 * __this, int32_t ___millisecondsTimeout, int32_t ___initialLockCount, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.ReaderWriterLock::AcquireWriterLock(System.TimeSpan)
extern "C" void ReaderWriterLock_AcquireWriterLock_m1_12754 (ReaderWriterLock_t1_1468 * __this, TimeSpan_t1_368  ___timeout, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.ReaderWriterLock::AnyWritersSince(System.Int32)
extern "C" bool ReaderWriterLock_AnyWritersSince_m1_12755 (ReaderWriterLock_t1_1468 * __this, int32_t ___seqNum, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.ReaderWriterLock::DowngradeFromWriterLock(System.Threading.LockCookie&)
extern "C" void ReaderWriterLock_DowngradeFromWriterLock_m1_12756 (ReaderWriterLock_t1_1468 * __this, LockCookie_t1_1466 * ___lockCookie, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.LockCookie System.Threading.ReaderWriterLock::ReleaseLock()
extern "C" LockCookie_t1_1466  ReaderWriterLock_ReleaseLock_m1_12757 (ReaderWriterLock_t1_1468 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.ReaderWriterLock::ReleaseReaderLock()
extern "C" void ReaderWriterLock_ReleaseReaderLock_m1_12758 (ReaderWriterLock_t1_1468 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.ReaderWriterLock::ReleaseReaderLock(System.Int32,System.Int32)
extern "C" void ReaderWriterLock_ReleaseReaderLock_m1_12759 (ReaderWriterLock_t1_1468 * __this, int32_t ___currentCount, int32_t ___releaseCount, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.ReaderWriterLock::ReleaseWriterLock()
extern "C" void ReaderWriterLock_ReleaseWriterLock_m1_12760 (ReaderWriterLock_t1_1468 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.ReaderWriterLock::ReleaseWriterLock(System.Int32)
extern "C" void ReaderWriterLock_ReleaseWriterLock_m1_12761 (ReaderWriterLock_t1_1468 * __this, int32_t ___releaseCount, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.ReaderWriterLock::RestoreLock(System.Threading.LockCookie&)
extern "C" void ReaderWriterLock_RestoreLock_m1_12762 (ReaderWriterLock_t1_1468 * __this, LockCookie_t1_1466 * ___lockCookie, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.LockCookie System.Threading.ReaderWriterLock::UpgradeToWriterLock(System.Int32)
extern "C" LockCookie_t1_1466  ReaderWriterLock_UpgradeToWriterLock_m1_12763 (ReaderWriterLock_t1_1468 * __this, int32_t ___millisecondsTimeout, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.LockCookie System.Threading.ReaderWriterLock::UpgradeToWriterLock(System.TimeSpan)
extern "C" LockCookie_t1_1466  ReaderWriterLock_UpgradeToWriterLock_m1_12764 (ReaderWriterLock_t1_1468 * __this, TimeSpan_t1_368  ___timeout, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.LockCookie System.Threading.ReaderWriterLock::GetLockCookie()
extern "C" LockCookie_t1_1466  ReaderWriterLock_GetLockCookie_m1_12765 (ReaderWriterLock_t1_1468 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.ReaderWriterLock::HasWriterLock()
extern "C" bool ReaderWriterLock_HasWriterLock_m1_12766 (ReaderWriterLock_t1_1468 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Threading.ReaderWriterLock::CheckTimeout(System.TimeSpan)
extern "C" int32_t ReaderWriterLock_CheckTimeout_m1_12767 (ReaderWriterLock_t1_1468 * __this, TimeSpan_t1_368  ___timeout, const MethodInfo* method) IL2CPP_METHOD_ATTR;
