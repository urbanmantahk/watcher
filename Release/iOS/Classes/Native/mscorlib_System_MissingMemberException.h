﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "mscorlib_System_MemberAccessException.h"

// System.MissingMemberException
struct  MissingMemberException_t1_1570  : public MemberAccessException_t1_1549
{
	// System.String System.MissingMemberException::ClassName
	String_t* ___ClassName_14;
	// System.String System.MissingMemberException::MemberName
	String_t* ___MemberName_15;
	// System.Byte[] System.MissingMemberException::Signature
	ByteU5BU5D_t1_109* ___Signature_16;
};
