﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture2D
struct Texture2D_t6_33;
// System.String
struct String_t;

#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Demo_NPDisabled.h"

// VoxelBusters.NativePlugins.Demo.MediaLibraryDemo
struct  MediaLibraryDemo_t8_181  : public NPDisabledFeatureDemo_t8_175
{
	// UnityEngine.Texture2D VoxelBusters.NativePlugins.Demo.MediaLibraryDemo::m_texture
	Texture2D_t6_33 * ___m_texture_10;
	// System.String VoxelBusters.NativePlugins.Demo.MediaLibraryDemo::m_videoURL
	String_t* ___m_videoURL_11;
	// System.String VoxelBusters.NativePlugins.Demo.MediaLibraryDemo::m_youtubeVideoID
	String_t* ___m_youtubeVideoID_12;
	// System.String VoxelBusters.NativePlugins.Demo.MediaLibraryDemo::m_embedHTMLString
	String_t* ___m_embedHTMLString_13;
};
