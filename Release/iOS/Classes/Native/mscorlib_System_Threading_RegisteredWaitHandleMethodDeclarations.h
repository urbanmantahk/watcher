﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Threading.RegisteredWaitHandle
struct RegisteredWaitHandle_t1_1473;
// System.Threading.WaitHandle
struct WaitHandle_t1_917;
// System.Threading.WaitOrTimerCallback
struct WaitOrTimerCallback_t1_1474;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_TimeSpan.h"

// System.Void System.Threading.RegisteredWaitHandle::.ctor(System.Threading.WaitHandle,System.Threading.WaitOrTimerCallback,System.Object,System.TimeSpan,System.Boolean)
extern "C" void RegisteredWaitHandle__ctor_m1_12768 (RegisteredWaitHandle_t1_1473 * __this, WaitHandle_t1_917 * ___waitObject, WaitOrTimerCallback_t1_1474 * ___callback, Object_t * ___state, TimeSpan_t1_368  ___timeout, bool ___executeOnlyOnce, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.RegisteredWaitHandle::Wait(System.Object)
extern "C" void RegisteredWaitHandle_Wait_m1_12769 (RegisteredWaitHandle_t1_1473 * __this, Object_t * ___state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.RegisteredWaitHandle::DoCallBack(System.Object)
extern "C" void RegisteredWaitHandle_DoCallBack_m1_12770 (RegisteredWaitHandle_t1_1473 * __this, Object_t * ___timedOut, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.RegisteredWaitHandle::Unregister(System.Threading.WaitHandle)
extern "C" bool RegisteredWaitHandle_Unregister_m1_12771 (RegisteredWaitHandle_t1_1473 * __this, WaitHandle_t1_917 * ___waitObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
