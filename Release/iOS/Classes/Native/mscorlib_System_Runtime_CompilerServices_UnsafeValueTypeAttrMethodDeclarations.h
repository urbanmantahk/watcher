﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.CompilerServices.UnsafeValueTypeAttribute
struct UnsafeValueTypeAttribute_t1_61;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.CompilerServices.UnsafeValueTypeAttribute::.ctor()
extern "C" void UnsafeValueTypeAttribute__ctor_m1_1335 (UnsafeValueTypeAttribute_t1_61 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
