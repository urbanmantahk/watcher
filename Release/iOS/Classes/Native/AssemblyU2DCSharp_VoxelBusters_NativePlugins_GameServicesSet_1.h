﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VoxelBusters.NativePlugins.GameServicesSettings/AndroidSettings
struct AndroidSettings_t8_236;
// VoxelBusters.NativePlugins.GameServicesSettings/iOSSettings
struct iOSSettings_t8_237;
// VoxelBusters.NativePlugins.IDContainer[]
struct IDContainerU5BU5D_t8_239;

#include "mscorlib_System_Object.h"

// VoxelBusters.NativePlugins.GameServicesSettings
struct  GameServicesSettings_t8_238  : public Object_t
{
	// VoxelBusters.NativePlugins.GameServicesSettings/AndroidSettings VoxelBusters.NativePlugins.GameServicesSettings::m_android
	AndroidSettings_t8_236 * ___m_android_0;
	// VoxelBusters.NativePlugins.GameServicesSettings/iOSSettings VoxelBusters.NativePlugins.GameServicesSettings::m_iOS
	iOSSettings_t8_237 * ___m_iOS_1;
	// VoxelBusters.NativePlugins.IDContainer[] VoxelBusters.NativePlugins.GameServicesSettings::m_achievementIDCollection
	IDContainerU5BU5D_t8_239* ___m_achievementIDCollection_2;
	// VoxelBusters.NativePlugins.IDContainer[] VoxelBusters.NativePlugins.GameServicesSettings::m_leaderboardIDCollection
	IDContainerU5BU5D_t8_239* ___m_leaderboardIDCollection_3;
};
