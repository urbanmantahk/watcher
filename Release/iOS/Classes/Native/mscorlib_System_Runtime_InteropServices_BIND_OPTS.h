﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType.h"

// System.Runtime.InteropServices.BIND_OPTS
struct  BIND_OPTS_t1_756 
{
	// System.Int32 System.Runtime.InteropServices.BIND_OPTS::cbStruct
	int32_t ___cbStruct_0;
	// System.Int32 System.Runtime.InteropServices.BIND_OPTS::grfFlags
	int32_t ___grfFlags_1;
	// System.Int32 System.Runtime.InteropServices.BIND_OPTS::grfMode
	int32_t ___grfMode_2;
	// System.Int32 System.Runtime.InteropServices.BIND_OPTS::dwTickCountDeadline
	int32_t ___dwTickCountDeadline_3;
};
