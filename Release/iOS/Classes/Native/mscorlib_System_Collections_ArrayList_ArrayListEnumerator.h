﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Object_t;
// System.Collections.ArrayList
struct ArrayList_t1_170;

#include "mscorlib_System_Object.h"

// System.Collections.ArrayList/ArrayListEnumerator
struct  ArrayListEnumerator_t1_258  : public Object_t
{
	// System.Int32 System.Collections.ArrayList/ArrayListEnumerator::m_Pos
	int32_t ___m_Pos_0;
	// System.Int32 System.Collections.ArrayList/ArrayListEnumerator::m_Index
	int32_t ___m_Index_1;
	// System.Int32 System.Collections.ArrayList/ArrayListEnumerator::m_Count
	int32_t ___m_Count_2;
	// System.Object System.Collections.ArrayList/ArrayListEnumerator::m_Current
	Object_t * ___m_Current_3;
	// System.Collections.ArrayList System.Collections.ArrayList/ArrayListEnumerator::m_List
	ArrayList_t1_170 * ___m_List_4;
	// System.Int32 System.Collections.ArrayList/ArrayListEnumerator::m_ExpectedStateChanges
	int32_t ___m_ExpectedStateChanges_5;
};
