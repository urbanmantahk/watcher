﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Runtime_InteropServices_ExternalException.h"

// System.Runtime.InteropServices.COMException
struct  COMException_t1_760  : public ExternalException_t1_761
{
};
