﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExifLibrary.ExifRectangularSubjectArea
struct ExifRectangularSubjectArea_t8_105;
// System.UInt16[]
struct UInt16U5BU5D_t1_1231;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTag.h"

// System.Void ExifLibrary.ExifRectangularSubjectArea::.ctor(ExifLibrary.ExifTag,System.UInt16[])
extern "C" void ExifRectangularSubjectArea__ctor_m8_448 (ExifRectangularSubjectArea_t8_105 * __this, int32_t ___tag, UInt16U5BU5D_t1_1231* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 ExifLibrary.ExifRectangularSubjectArea::get_Width()
extern "C" uint16_t ExifRectangularSubjectArea_get_Width_m8_449 (ExifRectangularSubjectArea_t8_105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.ExifRectangularSubjectArea::set_Width(System.UInt16)
extern "C" void ExifRectangularSubjectArea_set_Width_m8_450 (ExifRectangularSubjectArea_t8_105 * __this, uint16_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 ExifLibrary.ExifRectangularSubjectArea::get_Height()
extern "C" uint16_t ExifRectangularSubjectArea_get_Height_m8_451 (ExifRectangularSubjectArea_t8_105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.ExifRectangularSubjectArea::set_Height(System.UInt16)
extern "C" void ExifRectangularSubjectArea_set_Height_m8_452 (ExifRectangularSubjectArea_t8_105 * __this, uint16_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExifLibrary.ExifRectangularSubjectArea::ToString()
extern "C" String_t* ExifRectangularSubjectArea_ToString_m8_453 (ExifRectangularSubjectArea_t8_105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
