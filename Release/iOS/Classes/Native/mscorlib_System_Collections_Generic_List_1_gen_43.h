﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VoxelBusters.Utility.ShaderUtility/ShaderProperty[]
struct ShaderPropertyU5BU5D_t8_377;

#include "mscorlib_System_Object.h"

// System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderProperty>
struct  List_1_t1_1900  : public Object_t
{
	// T[] System.Collections.Generic.List`1::_items
	ShaderPropertyU5BU5D_t8_377* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
};
struct List_1_t1_1900_StaticFields{
	// T[] System.Collections.Generic.List`1::EmptyArray
	ShaderPropertyU5BU5D_t8_377* ___EmptyArray_4;
};
