﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Messaging.RemotingSurrogateSelector
struct RemotingSurrogateSelector_t1_957;
// System.Runtime.Remoting.Messaging.MessageSurrogateFilter
struct MessageSurrogateFilter_t1_958;
// System.Runtime.Serialization.ISurrogateSelector
struct ISurrogateSelector_t1_959;
// System.Object
struct Object_t;
// System.Runtime.Serialization.ISerializationSurrogate
struct ISerializationSurrogate_t1_1085;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.Runtime.Remoting.Messaging.RemotingSurrogateSelector::.ctor()
extern "C" void RemotingSurrogateSelector__ctor_m1_8613 (RemotingSurrogateSelector_t1_957 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Messaging.RemotingSurrogateSelector::.cctor()
extern "C" void RemotingSurrogateSelector__cctor_m1_8614 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Messaging.MessageSurrogateFilter System.Runtime.Remoting.Messaging.RemotingSurrogateSelector::get_Filter()
extern "C" MessageSurrogateFilter_t1_958 * RemotingSurrogateSelector_get_Filter_m1_8615 (RemotingSurrogateSelector_t1_957 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Messaging.RemotingSurrogateSelector::set_Filter(System.Runtime.Remoting.Messaging.MessageSurrogateFilter)
extern "C" void RemotingSurrogateSelector_set_Filter_m1_8616 (RemotingSurrogateSelector_t1_957 * __this, MessageSurrogateFilter_t1_958 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Messaging.RemotingSurrogateSelector::ChainSelector(System.Runtime.Serialization.ISurrogateSelector)
extern "C" void RemotingSurrogateSelector_ChainSelector_m1_8617 (RemotingSurrogateSelector_t1_957 * __this, Object_t * ___selector, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Serialization.ISurrogateSelector System.Runtime.Remoting.Messaging.RemotingSurrogateSelector::GetNextSelector()
extern "C" Object_t * RemotingSurrogateSelector_GetNextSelector_m1_8618 (RemotingSurrogateSelector_t1_957 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Messaging.RemotingSurrogateSelector::GetRootObject()
extern "C" Object_t * RemotingSurrogateSelector_GetRootObject_m1_8619 (RemotingSurrogateSelector_t1_957 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Serialization.ISerializationSurrogate System.Runtime.Remoting.Messaging.RemotingSurrogateSelector::GetSurrogate(System.Type,System.Runtime.Serialization.StreamingContext,System.Runtime.Serialization.ISurrogateSelector&)
extern "C" Object_t * RemotingSurrogateSelector_GetSurrogate_m1_8620 (RemotingSurrogateSelector_t1_957 * __this, Type_t * ___type, StreamingContext_t1_1050  ___context, Object_t ** ___ssout, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Messaging.RemotingSurrogateSelector::SetRootObject(System.Object)
extern "C" void RemotingSurrogateSelector_SetRootObject_m1_8621 (RemotingSurrogateSelector_t1_957 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Messaging.RemotingSurrogateSelector::UseSoapFormat()
extern "C" void RemotingSurrogateSelector_UseSoapFormat_m1_8622 (RemotingSurrogateSelector_t1_957 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
