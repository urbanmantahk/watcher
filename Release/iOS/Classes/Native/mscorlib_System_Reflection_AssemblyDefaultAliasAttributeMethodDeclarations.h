﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.AssemblyDefaultAliasAttribute
struct AssemblyDefaultAliasAttribute_t1_568;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Reflection.AssemblyDefaultAliasAttribute::.ctor(System.String)
extern "C" void AssemblyDefaultAliasAttribute__ctor_m1_6676 (AssemblyDefaultAliasAttribute_t1_568 * __this, String_t* ___defaultAlias, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.AssemblyDefaultAliasAttribute::get_DefaultAlias()
extern "C" String_t* AssemblyDefaultAliasAttribute_get_DefaultAlias_m1_6677 (AssemblyDefaultAliasAttribute_t1_568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
