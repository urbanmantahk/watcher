﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapIdref
struct SoapIdref_t1_976;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapIdref::.ctor()
extern "C" void SoapIdref__ctor_m1_8750 (SoapIdref_t1_976 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapIdref::.ctor(System.String)
extern "C" void SoapIdref__ctor_m1_8751 (SoapIdref_t1_976 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapIdref::get_Value()
extern "C" String_t* SoapIdref_get_Value_m1_8752 (SoapIdref_t1_976 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapIdref::set_Value(System.String)
extern "C" void SoapIdref_set_Value_m1_8753 (SoapIdref_t1_976 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapIdref::get_XsdType()
extern "C" String_t* SoapIdref_get_XsdType_m1_8754 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapIdref::GetXsdType()
extern "C" String_t* SoapIdref_GetXsdType_m1_8755 (SoapIdref_t1_976 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapIdref System.Runtime.Remoting.Metadata.W3cXsd2001.SoapIdref::Parse(System.String)
extern "C" SoapIdref_t1_976 * SoapIdref_Parse_m1_8756 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapIdref::ToString()
extern "C" String_t* SoapIdref_ToString_m1_8757 (SoapIdref_t1_976 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
