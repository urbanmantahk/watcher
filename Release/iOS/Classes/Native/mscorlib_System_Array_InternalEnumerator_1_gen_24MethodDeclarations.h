﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_24.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_4.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_15671_gshared (InternalEnumerator_1_t1_1982 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1_15671(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1_1982 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m1_15671_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_15672_gshared (InternalEnumerator_1_t1_1982 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_15672(__this, method) (( void (*) (InternalEnumerator_1_t1_1982 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_15672_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_15673_gshared (InternalEnumerator_1_t1_1982 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_15673(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1_1982 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_15673_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_15674_gshared (InternalEnumerator_1_t1_1982 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1_15674(__this, method) (( void (*) (InternalEnumerator_1_t1_1982 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1_15674_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_15675_gshared (InternalEnumerator_1_t1_1982 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1_15675(__this, method) (( bool (*) (InternalEnumerator_1_t1_1982 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1_15675_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::get_Current()
extern "C" KeyValuePair_2_t1_1981  InternalEnumerator_1_get_Current_m1_15676_gshared (InternalEnumerator_1_t1_1982 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1_15676(__this, method) (( KeyValuePair_2_t1_1981  (*) (InternalEnumerator_1_t1_1982 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1_15676_gshared)(__this, method)
