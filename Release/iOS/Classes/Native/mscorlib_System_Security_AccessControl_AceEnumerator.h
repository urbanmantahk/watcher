﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Security.AccessControl.GenericAcl
struct GenericAcl_t1_1114;

#include "mscorlib_System_Object.h"

// System.Security.AccessControl.AceEnumerator
struct  AceEnumerator_t1_1113  : public Object_t
{
	// System.Security.AccessControl.GenericAcl System.Security.AccessControl.AceEnumerator::owner
	GenericAcl_t1_1114 * ___owner_0;
	// System.Int32 System.Security.AccessControl.AceEnumerator::current
	int32_t ___current_1;
};
