﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.RSACryptoServiceProvider
struct RSACryptoServiceProvider_t1_1235;
// System.Security.Cryptography.CspParameters
struct CspParameters_t1_164;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Security.Cryptography.HashAlgorithm
struct HashAlgorithm_t1_162;
// System.Object
struct Object_t;
// System.IO.Stream
struct Stream_t1_405;
// System.EventArgs
struct EventArgs_t1_158;
// System.Security.Cryptography.CspKeyContainerInfo
struct CspKeyContainerInfo_t1_1196;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Cryptography_RSAParameters.h"

// System.Void System.Security.Cryptography.RSACryptoServiceProvider::.ctor()
extern "C" void RSACryptoServiceProvider__ctor_m1_10518 (RSACryptoServiceProvider_t1_1235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RSACryptoServiceProvider::.ctor(System.Security.Cryptography.CspParameters)
extern "C" void RSACryptoServiceProvider__ctor_m1_10519 (RSACryptoServiceProvider_t1_1235 * __this, CspParameters_t1_164 * ___parameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RSACryptoServiceProvider::.ctor(System.Int32)
extern "C" void RSACryptoServiceProvider__ctor_m1_10520 (RSACryptoServiceProvider_t1_1235 * __this, int32_t ___dwKeySize, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RSACryptoServiceProvider::.ctor(System.Int32,System.Security.Cryptography.CspParameters)
extern "C" void RSACryptoServiceProvider__ctor_m1_10521 (RSACryptoServiceProvider_t1_1235 * __this, int32_t ___dwKeySize, CspParameters_t1_164 * ___parameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RSACryptoServiceProvider::.cctor()
extern "C" void RSACryptoServiceProvider__cctor_m1_10522 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RSACryptoServiceProvider::Common(System.Int32,System.Security.Cryptography.CspParameters)
extern "C" void RSACryptoServiceProvider_Common_m1_10523 (RSACryptoServiceProvider_t1_1235 * __this, int32_t ___dwKeySize, CspParameters_t1_164 * ___p, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.RSACryptoServiceProvider::get_UseMachineKeyStore()
extern "C" bool RSACryptoServiceProvider_get_UseMachineKeyStore_m1_10524 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RSACryptoServiceProvider::set_UseMachineKeyStore(System.Boolean)
extern "C" void RSACryptoServiceProvider_set_UseMachineKeyStore_m1_10525 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RSACryptoServiceProvider::Finalize()
extern "C" void RSACryptoServiceProvider_Finalize_m1_10526 (RSACryptoServiceProvider_t1_1235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Cryptography.RSACryptoServiceProvider::get_KeyExchangeAlgorithm()
extern "C" String_t* RSACryptoServiceProvider_get_KeyExchangeAlgorithm_m1_10527 (RSACryptoServiceProvider_t1_1235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Cryptography.RSACryptoServiceProvider::get_KeySize()
extern "C" int32_t RSACryptoServiceProvider_get_KeySize_m1_10528 (RSACryptoServiceProvider_t1_1235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.RSACryptoServiceProvider::get_PersistKeyInCsp()
extern "C" bool RSACryptoServiceProvider_get_PersistKeyInCsp_m1_10529 (RSACryptoServiceProvider_t1_1235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RSACryptoServiceProvider::set_PersistKeyInCsp(System.Boolean)
extern "C" void RSACryptoServiceProvider_set_PersistKeyInCsp_m1_10530 (RSACryptoServiceProvider_t1_1235 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.RSACryptoServiceProvider::get_PublicOnly()
extern "C" bool RSACryptoServiceProvider_get_PublicOnly_m1_10531 (RSACryptoServiceProvider_t1_1235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Cryptography.RSACryptoServiceProvider::get_SignatureAlgorithm()
extern "C" String_t* RSACryptoServiceProvider_get_SignatureAlgorithm_m1_10532 (RSACryptoServiceProvider_t1_1235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.RSACryptoServiceProvider::Decrypt(System.Byte[],System.Boolean)
extern "C" ByteU5BU5D_t1_109* RSACryptoServiceProvider_Decrypt_m1_10533 (RSACryptoServiceProvider_t1_1235 * __this, ByteU5BU5D_t1_109* ___rgb, bool ___fOAEP, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.RSACryptoServiceProvider::DecryptValue(System.Byte[])
extern "C" ByteU5BU5D_t1_109* RSACryptoServiceProvider_DecryptValue_m1_10534 (RSACryptoServiceProvider_t1_1235 * __this, ByteU5BU5D_t1_109* ___rgb, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.RSACryptoServiceProvider::Encrypt(System.Byte[],System.Boolean)
extern "C" ByteU5BU5D_t1_109* RSACryptoServiceProvider_Encrypt_m1_10535 (RSACryptoServiceProvider_t1_1235 * __this, ByteU5BU5D_t1_109* ___rgb, bool ___fOAEP, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.RSACryptoServiceProvider::EncryptValue(System.Byte[])
extern "C" ByteU5BU5D_t1_109* RSACryptoServiceProvider_EncryptValue_m1_10536 (RSACryptoServiceProvider_t1_1235 * __this, ByteU5BU5D_t1_109* ___rgb, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.RSAParameters System.Security.Cryptography.RSACryptoServiceProvider::ExportParameters(System.Boolean)
extern "C" RSAParameters_t1_1242  RSACryptoServiceProvider_ExportParameters_m1_10537 (RSACryptoServiceProvider_t1_1235 * __this, bool ___includePrivateParameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RSACryptoServiceProvider::ImportParameters(System.Security.Cryptography.RSAParameters)
extern "C" void RSACryptoServiceProvider_ImportParameters_m1_10538 (RSACryptoServiceProvider_t1_1235 * __this, RSAParameters_t1_1242  ___parameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.HashAlgorithm System.Security.Cryptography.RSACryptoServiceProvider::GetHash(System.Object)
extern "C" HashAlgorithm_t1_162 * RSACryptoServiceProvider_GetHash_m1_10539 (RSACryptoServiceProvider_t1_1235 * __this, Object_t * ___halg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.RSACryptoServiceProvider::SignData(System.Byte[],System.Object)
extern "C" ByteU5BU5D_t1_109* RSACryptoServiceProvider_SignData_m1_10540 (RSACryptoServiceProvider_t1_1235 * __this, ByteU5BU5D_t1_109* ___buffer, Object_t * ___halg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.RSACryptoServiceProvider::SignData(System.IO.Stream,System.Object)
extern "C" ByteU5BU5D_t1_109* RSACryptoServiceProvider_SignData_m1_10541 (RSACryptoServiceProvider_t1_1235 * __this, Stream_t1_405 * ___inputStream, Object_t * ___halg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.RSACryptoServiceProvider::SignData(System.Byte[],System.Int32,System.Int32,System.Object)
extern "C" ByteU5BU5D_t1_109* RSACryptoServiceProvider_SignData_m1_10542 (RSACryptoServiceProvider_t1_1235 * __this, ByteU5BU5D_t1_109* ___buffer, int32_t ___offset, int32_t ___count, Object_t * ___halg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Cryptography.RSACryptoServiceProvider::GetHashNameFromOID(System.String)
extern "C" String_t* RSACryptoServiceProvider_GetHashNameFromOID_m1_10543 (RSACryptoServiceProvider_t1_1235 * __this, String_t* ___oid, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.RSACryptoServiceProvider::SignHash(System.Byte[],System.String)
extern "C" ByteU5BU5D_t1_109* RSACryptoServiceProvider_SignHash_m1_10544 (RSACryptoServiceProvider_t1_1235 * __this, ByteU5BU5D_t1_109* ___rgbHash, String_t* ___str, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.RSACryptoServiceProvider::VerifyData(System.Byte[],System.Object,System.Byte[])
extern "C" bool RSACryptoServiceProvider_VerifyData_m1_10545 (RSACryptoServiceProvider_t1_1235 * __this, ByteU5BU5D_t1_109* ___buffer, Object_t * ___halg, ByteU5BU5D_t1_109* ___signature, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.RSACryptoServiceProvider::VerifyHash(System.Byte[],System.String,System.Byte[])
extern "C" bool RSACryptoServiceProvider_VerifyHash_m1_10546 (RSACryptoServiceProvider_t1_1235 * __this, ByteU5BU5D_t1_109* ___rgbHash, String_t* ___str, ByteU5BU5D_t1_109* ___rgbSignature, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RSACryptoServiceProvider::Dispose(System.Boolean)
extern "C" void RSACryptoServiceProvider_Dispose_m1_10547 (RSACryptoServiceProvider_t1_1235 * __this, bool ___disposing, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RSACryptoServiceProvider::OnKeyGenerated(System.Object,System.EventArgs)
extern "C" void RSACryptoServiceProvider_OnKeyGenerated_m1_10548 (RSACryptoServiceProvider_t1_1235 * __this, Object_t * ___sender, EventArgs_t1_158 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.CspKeyContainerInfo System.Security.Cryptography.RSACryptoServiceProvider::get_CspKeyContainerInfo()
extern "C" CspKeyContainerInfo_t1_1196 * RSACryptoServiceProvider_get_CspKeyContainerInfo_m1_10549 (RSACryptoServiceProvider_t1_1235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.RSACryptoServiceProvider::ExportCspBlob(System.Boolean)
extern "C" ByteU5BU5D_t1_109* RSACryptoServiceProvider_ExportCspBlob_m1_10550 (RSACryptoServiceProvider_t1_1235 * __this, bool ___includePrivateParameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RSACryptoServiceProvider::ImportCspBlob(System.Byte[])
extern "C" void RSACryptoServiceProvider_ImportCspBlob_m1_10551 (RSACryptoServiceProvider_t1_1235 * __this, ByteU5BU5D_t1_109* ___keyBlob, const MethodInfo* method) IL2CPP_METHOD_ATTR;
