﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.StrongNameManager/Element
struct Element_t1_233;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Security.StrongNameManager/Element::.ctor()
extern "C" void Element__ctor_m1_2600 (Element_t1_233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.StrongNameManager/Element::.ctor(System.String,System.String)
extern "C" void Element__ctor_m1_2601 (Element_t1_233 * __this, String_t* ___assembly, String_t* ___users, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.StrongNameManager/Element::GetUsers(System.String)
extern "C" String_t* Element_GetUsers_m1_2602 (Element_t1_233 * __this, String_t* ___assembly, const MethodInfo* method) IL2CPP_METHOD_ATTR;
