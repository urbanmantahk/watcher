﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_VARKIND.h"

// System.Runtime.InteropServices.ComTypes.VARKIND
struct  VARKIND_t1_750 
{
	// System.Int32 System.Runtime.InteropServices.ComTypes.VARKIND::value__
	int32_t ___value___1;
};
