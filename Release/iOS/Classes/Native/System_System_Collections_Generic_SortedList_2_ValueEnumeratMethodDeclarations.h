﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedList`2<System.Object,System.Object>
struct SortedList_2_t3_255;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_SortedList_2_ValueEnumerat.h"

// System.Void System.Collections.Generic.SortedList`2/ValueEnumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.SortedList`2<TKey,TValue>)
extern "C" void ValueEnumerator__ctor_m3_1971_gshared (ValueEnumerator_t3_261 * __this, SortedList_2_t3_255 * ___l, const MethodInfo* method);
#define ValueEnumerator__ctor_m3_1971(__this, ___l, method) (( void (*) (ValueEnumerator_t3_261 *, SortedList_2_t3_255 *, const MethodInfo*))ValueEnumerator__ctor_m3_1971_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.SortedList`2/ValueEnumerator<System.Object,System.Object>::System.Collections.IEnumerator.Reset()
extern "C" void ValueEnumerator_System_Collections_IEnumerator_Reset_m3_1972_gshared (ValueEnumerator_t3_261 * __this, const MethodInfo* method);
#define ValueEnumerator_System_Collections_IEnumerator_Reset_m3_1972(__this, method) (( void (*) (ValueEnumerator_t3_261 *, const MethodInfo*))ValueEnumerator_System_Collections_IEnumerator_Reset_m3_1972_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2/ValueEnumerator<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * ValueEnumerator_System_Collections_IEnumerator_get_Current_m3_1973_gshared (ValueEnumerator_t3_261 * __this, const MethodInfo* method);
#define ValueEnumerator_System_Collections_IEnumerator_get_Current_m3_1973(__this, method) (( Object_t * (*) (ValueEnumerator_t3_261 *, const MethodInfo*))ValueEnumerator_System_Collections_IEnumerator_get_Current_m3_1973_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/ValueEnumerator<System.Object,System.Object>::Dispose()
extern "C" void ValueEnumerator_Dispose_m3_1974_gshared (ValueEnumerator_t3_261 * __this, const MethodInfo* method);
#define ValueEnumerator_Dispose_m3_1974(__this, method) (( void (*) (ValueEnumerator_t3_261 *, const MethodInfo*))ValueEnumerator_Dispose_m3_1974_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedList`2/ValueEnumerator<System.Object,System.Object>::MoveNext()
extern "C" bool ValueEnumerator_MoveNext_m3_1975_gshared (ValueEnumerator_t3_261 * __this, const MethodInfo* method);
#define ValueEnumerator_MoveNext_m3_1975(__this, method) (( bool (*) (ValueEnumerator_t3_261 *, const MethodInfo*))ValueEnumerator_MoveNext_m3_1975_gshared)(__this, method)
// TValue System.Collections.Generic.SortedList`2/ValueEnumerator<System.Object,System.Object>::get_Current()
extern "C" Object_t * ValueEnumerator_get_Current_m3_1976_gshared (ValueEnumerator_t3_261 * __this, const MethodInfo* method);
#define ValueEnumerator_get_Current_m3_1976(__this, method) (( Object_t * (*) (ValueEnumerator_t3_261 *, const MethodInfo*))ValueEnumerator_get_Current_m3_1976_gshared)(__this, method)
