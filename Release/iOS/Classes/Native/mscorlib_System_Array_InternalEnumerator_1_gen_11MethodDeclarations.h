﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_11.h"

// System.Void System.Array/InternalEnumerator`1<System.UInt16>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_15456_gshared (InternalEnumerator_1_t1_1959 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1_15456(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1_1959 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m1_15456_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.UInt16>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_15457_gshared (InternalEnumerator_1_t1_1959 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_15457(__this, method) (( void (*) (InternalEnumerator_1_t1_1959 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_15457_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.UInt16>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_15458_gshared (InternalEnumerator_1_t1_1959 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_15458(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1_1959 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_15458_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.UInt16>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_15459_gshared (InternalEnumerator_1_t1_1959 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1_15459(__this, method) (( void (*) (InternalEnumerator_1_t1_1959 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1_15459_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.UInt16>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_15460_gshared (InternalEnumerator_1_t1_1959 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1_15460(__this, method) (( bool (*) (InternalEnumerator_1_t1_1959 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1_15460_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.UInt16>::get_Current()
extern "C" uint16_t InternalEnumerator_1_get_Current_m1_15461_gshared (InternalEnumerator_1_t1_1959 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1_15461(__this, method) (( uint16_t (*) (InternalEnumerator_1_t1_1959 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1_15461_gshared)(__this, method)
