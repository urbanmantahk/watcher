﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.DSACryptoServiceProvider
struct DSACryptoServiceProvider_t1_1203;
// System.Security.Cryptography.CspParameters
struct CspParameters_t1_164;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.IO.Stream
struct Stream_t1_405;
// System.Object
struct Object_t;
// System.EventArgs
struct EventArgs_t1_158;
// System.Security.Cryptography.CspKeyContainerInfo
struct CspKeyContainerInfo_t1_1196;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Cryptography_DSAParameters.h"

// System.Void System.Security.Cryptography.DSACryptoServiceProvider::.ctor()
extern "C" void DSACryptoServiceProvider__ctor_m1_10287 (DSACryptoServiceProvider_t1_1203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.DSACryptoServiceProvider::.ctor(System.Security.Cryptography.CspParameters)
extern "C" void DSACryptoServiceProvider__ctor_m1_10288 (DSACryptoServiceProvider_t1_1203 * __this, CspParameters_t1_164 * ___parameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.DSACryptoServiceProvider::.ctor(System.Int32)
extern "C" void DSACryptoServiceProvider__ctor_m1_10289 (DSACryptoServiceProvider_t1_1203 * __this, int32_t ___dwKeySize, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.DSACryptoServiceProvider::.ctor(System.Int32,System.Security.Cryptography.CspParameters)
extern "C" void DSACryptoServiceProvider__ctor_m1_10290 (DSACryptoServiceProvider_t1_1203 * __this, int32_t ___dwKeySize, CspParameters_t1_164 * ___parameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.DSACryptoServiceProvider::.cctor()
extern "C" void DSACryptoServiceProvider__cctor_m1_10291 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.DSACryptoServiceProvider::Finalize()
extern "C" void DSACryptoServiceProvider_Finalize_m1_10292 (DSACryptoServiceProvider_t1_1203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Cryptography.DSACryptoServiceProvider::get_KeyExchangeAlgorithm()
extern "C" String_t* DSACryptoServiceProvider_get_KeyExchangeAlgorithm_m1_10293 (DSACryptoServiceProvider_t1_1203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Cryptography.DSACryptoServiceProvider::get_KeySize()
extern "C" int32_t DSACryptoServiceProvider_get_KeySize_m1_10294 (DSACryptoServiceProvider_t1_1203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.DSACryptoServiceProvider::get_PersistKeyInCsp()
extern "C" bool DSACryptoServiceProvider_get_PersistKeyInCsp_m1_10295 (DSACryptoServiceProvider_t1_1203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.DSACryptoServiceProvider::set_PersistKeyInCsp(System.Boolean)
extern "C" void DSACryptoServiceProvider_set_PersistKeyInCsp_m1_10296 (DSACryptoServiceProvider_t1_1203 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.DSACryptoServiceProvider::get_PublicOnly()
extern "C" bool DSACryptoServiceProvider_get_PublicOnly_m1_10297 (DSACryptoServiceProvider_t1_1203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Cryptography.DSACryptoServiceProvider::get_SignatureAlgorithm()
extern "C" String_t* DSACryptoServiceProvider_get_SignatureAlgorithm_m1_10298 (DSACryptoServiceProvider_t1_1203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.DSACryptoServiceProvider::get_UseMachineKeyStore()
extern "C" bool DSACryptoServiceProvider_get_UseMachineKeyStore_m1_10299 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.DSACryptoServiceProvider::set_UseMachineKeyStore(System.Boolean)
extern "C" void DSACryptoServiceProvider_set_UseMachineKeyStore_m1_10300 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.DSAParameters System.Security.Cryptography.DSACryptoServiceProvider::ExportParameters(System.Boolean)
extern "C" DSAParameters_t1_1204  DSACryptoServiceProvider_ExportParameters_m1_10301 (DSACryptoServiceProvider_t1_1203 * __this, bool ___includePrivateParameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.DSACryptoServiceProvider::ImportParameters(System.Security.Cryptography.DSAParameters)
extern "C" void DSACryptoServiceProvider_ImportParameters_m1_10302 (DSACryptoServiceProvider_t1_1203 * __this, DSAParameters_t1_1204  ___parameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.DSACryptoServiceProvider::CreateSignature(System.Byte[])
extern "C" ByteU5BU5D_t1_109* DSACryptoServiceProvider_CreateSignature_m1_10303 (DSACryptoServiceProvider_t1_1203 * __this, ByteU5BU5D_t1_109* ___rgbHash, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.DSACryptoServiceProvider::SignData(System.Byte[])
extern "C" ByteU5BU5D_t1_109* DSACryptoServiceProvider_SignData_m1_10304 (DSACryptoServiceProvider_t1_1203 * __this, ByteU5BU5D_t1_109* ___buffer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.DSACryptoServiceProvider::SignData(System.Byte[],System.Int32,System.Int32)
extern "C" ByteU5BU5D_t1_109* DSACryptoServiceProvider_SignData_m1_10305 (DSACryptoServiceProvider_t1_1203 * __this, ByteU5BU5D_t1_109* ___buffer, int32_t ___offset, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.DSACryptoServiceProvider::SignData(System.IO.Stream)
extern "C" ByteU5BU5D_t1_109* DSACryptoServiceProvider_SignData_m1_10306 (DSACryptoServiceProvider_t1_1203 * __this, Stream_t1_405 * ___inputStream, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.DSACryptoServiceProvider::SignHash(System.Byte[],System.String)
extern "C" ByteU5BU5D_t1_109* DSACryptoServiceProvider_SignHash_m1_10307 (DSACryptoServiceProvider_t1_1203 * __this, ByteU5BU5D_t1_109* ___rgbHash, String_t* ___str, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.DSACryptoServiceProvider::VerifyData(System.Byte[],System.Byte[])
extern "C" bool DSACryptoServiceProvider_VerifyData_m1_10308 (DSACryptoServiceProvider_t1_1203 * __this, ByteU5BU5D_t1_109* ___rgbData, ByteU5BU5D_t1_109* ___rgbSignature, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.DSACryptoServiceProvider::VerifyHash(System.Byte[],System.String,System.Byte[])
extern "C" bool DSACryptoServiceProvider_VerifyHash_m1_10309 (DSACryptoServiceProvider_t1_1203 * __this, ByteU5BU5D_t1_109* ___rgbHash, String_t* ___str, ByteU5BU5D_t1_109* ___rgbSignature, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.DSACryptoServiceProvider::VerifySignature(System.Byte[],System.Byte[])
extern "C" bool DSACryptoServiceProvider_VerifySignature_m1_10310 (DSACryptoServiceProvider_t1_1203 * __this, ByteU5BU5D_t1_109* ___rgbHash, ByteU5BU5D_t1_109* ___rgbSignature, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.DSACryptoServiceProvider::Dispose(System.Boolean)
extern "C" void DSACryptoServiceProvider_Dispose_m1_10311 (DSACryptoServiceProvider_t1_1203 * __this, bool ___disposing, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.DSACryptoServiceProvider::OnKeyGenerated(System.Object,System.EventArgs)
extern "C" void DSACryptoServiceProvider_OnKeyGenerated_m1_10312 (DSACryptoServiceProvider_t1_1203 * __this, Object_t * ___sender, EventArgs_t1_158 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.CspKeyContainerInfo System.Security.Cryptography.DSACryptoServiceProvider::get_CspKeyContainerInfo()
extern "C" CspKeyContainerInfo_t1_1196 * DSACryptoServiceProvider_get_CspKeyContainerInfo_m1_10313 (DSACryptoServiceProvider_t1_1203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.DSACryptoServiceProvider::ExportCspBlob(System.Boolean)
extern "C" ByteU5BU5D_t1_109* DSACryptoServiceProvider_ExportCspBlob_m1_10314 (DSACryptoServiceProvider_t1_1203 * __this, bool ___includePrivateParameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.DSACryptoServiceProvider::ImportCspBlob(System.Byte[])
extern "C" void DSACryptoServiceProvider_ImportCspBlob_m1_10315 (DSACryptoServiceProvider_t1_1203 * __this, ByteU5BU5D_t1_109* ___keyBlob, const MethodInfo* method) IL2CPP_METHOD_ATTR;
