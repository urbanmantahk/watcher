﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Reflection.Emit.ILGenerator
struct ILGenerator_t1_480;
// System.Type[]
struct TypeU5BU5D_t1_31;
// System.Reflection.Emit.TypeBuilder
struct TypeBuilder_t1_481;
// System.Reflection.Emit.ParameterBuilder[]
struct ParameterBuilderU5BU5D_t1_482;
// System.Reflection.Emit.CustomAttributeBuilder[]
struct CustomAttributeBuilderU5BU5D_t1_469;
// System.Type[][]
struct TypeU5BU5DU5BU5D_t1_483;
// System.Reflection.Emit.RefEmitPermissionSet[]
struct RefEmitPermissionSetU5BU5D_t1_473;

#include "mscorlib_System_Reflection_ConstructorInfo.h"
#include "mscorlib_System_RuntimeMethodHandle.h"
#include "mscorlib_System_Reflection_MethodAttributes.h"
#include "mscorlib_System_Reflection_MethodImplAttributes.h"
#include "mscorlib_System_Reflection_CallingConventions.h"

// System.Reflection.Emit.ConstructorBuilder
struct  ConstructorBuilder_t1_477  : public ConstructorInfo_t1_478
{
	// System.RuntimeMethodHandle System.Reflection.Emit.ConstructorBuilder::mhandle
	RuntimeMethodHandle_t1_479  ___mhandle_2;
	// System.Reflection.Emit.ILGenerator System.Reflection.Emit.ConstructorBuilder::ilgen
	ILGenerator_t1_480 * ___ilgen_3;
	// System.Type[] System.Reflection.Emit.ConstructorBuilder::parameters
	TypeU5BU5D_t1_31* ___parameters_4;
	// System.Reflection.MethodAttributes System.Reflection.Emit.ConstructorBuilder::attrs
	int32_t ___attrs_5;
	// System.Reflection.MethodImplAttributes System.Reflection.Emit.ConstructorBuilder::iattrs
	int32_t ___iattrs_6;
	// System.Int32 System.Reflection.Emit.ConstructorBuilder::table_idx
	int32_t ___table_idx_7;
	// System.Reflection.CallingConventions System.Reflection.Emit.ConstructorBuilder::call_conv
	int32_t ___call_conv_8;
	// System.Reflection.Emit.TypeBuilder System.Reflection.Emit.ConstructorBuilder::type
	TypeBuilder_t1_481 * ___type_9;
	// System.Reflection.Emit.ParameterBuilder[] System.Reflection.Emit.ConstructorBuilder::pinfo
	ParameterBuilderU5BU5D_t1_482* ___pinfo_10;
	// System.Reflection.Emit.CustomAttributeBuilder[] System.Reflection.Emit.ConstructorBuilder::cattrs
	CustomAttributeBuilderU5BU5D_t1_469* ___cattrs_11;
	// System.Boolean System.Reflection.Emit.ConstructorBuilder::init_locals
	bool ___init_locals_12;
	// System.Type[][] System.Reflection.Emit.ConstructorBuilder::paramModReq
	TypeU5BU5DU5BU5D_t1_483* ___paramModReq_13;
	// System.Type[][] System.Reflection.Emit.ConstructorBuilder::paramModOpt
	TypeU5BU5DU5BU5D_t1_483* ___paramModOpt_14;
	// System.Reflection.Emit.RefEmitPermissionSet[] System.Reflection.Emit.ConstructorBuilder::permissions
	RefEmitPermissionSetU5BU5D_t1_473* ___permissions_15;
};
