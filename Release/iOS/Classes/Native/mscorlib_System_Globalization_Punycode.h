﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Globalization_Bootstring.h"

// System.Globalization.Punycode
struct  Punycode_t1_376  : public Bootstring_t1_377
{
};
