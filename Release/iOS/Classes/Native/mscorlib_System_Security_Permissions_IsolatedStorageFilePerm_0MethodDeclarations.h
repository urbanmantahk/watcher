﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Permissions.IsolatedStorageFilePermissionAttribute
struct IsolatedStorageFilePermissionAttribute_t1_1286;
// System.Security.IPermission
struct IPermission_t1_1400;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Permissions_SecurityAction.h"

// System.Void System.Security.Permissions.IsolatedStorageFilePermissionAttribute::.ctor(System.Security.Permissions.SecurityAction)
extern "C" void IsolatedStorageFilePermissionAttribute__ctor_m1_10936 (IsolatedStorageFilePermissionAttribute_t1_1286 * __this, int32_t ___action, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.IsolatedStorageFilePermissionAttribute::CreatePermission()
extern "C" Object_t * IsolatedStorageFilePermissionAttribute_CreatePermission_m1_10937 (IsolatedStorageFilePermissionAttribute_t1_1286 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
