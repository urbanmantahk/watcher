﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Permissions.UrlIdentityPermissionAttribute
struct UrlIdentityPermissionAttribute_t1_1322;
// System.String
struct String_t;
// System.Security.IPermission
struct IPermission_t1_1400;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Permissions_SecurityAction.h"

// System.Void System.Security.Permissions.UrlIdentityPermissionAttribute::.ctor(System.Security.Permissions.SecurityAction)
extern "C" void UrlIdentityPermissionAttribute__ctor_m1_11294 (UrlIdentityPermissionAttribute_t1_1322 * __this, int32_t ___action, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Permissions.UrlIdentityPermissionAttribute::get_Url()
extern "C" String_t* UrlIdentityPermissionAttribute_get_Url_m1_11295 (UrlIdentityPermissionAttribute_t1_1322 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.UrlIdentityPermissionAttribute::set_Url(System.String)
extern "C" void UrlIdentityPermissionAttribute_set_Url_m1_11296 (UrlIdentityPermissionAttribute_t1_1322 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.UrlIdentityPermissionAttribute::CreatePermission()
extern "C" Object_t * UrlIdentityPermissionAttribute_CreatePermission_m1_11297 (UrlIdentityPermissionAttribute_t1_1322 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
