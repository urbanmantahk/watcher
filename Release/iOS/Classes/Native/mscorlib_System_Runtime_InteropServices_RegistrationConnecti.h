﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Runtime_InteropServices_RegistrationConnecti.h"

// System.Runtime.InteropServices.RegistrationConnectionType
struct  RegistrationConnectionType_t1_818 
{
	// System.Int32 System.Runtime.InteropServices.RegistrationConnectionType::value__
	int32_t ___value___1;
};
