﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Attribute.h"

// System.Xml.Serialization.XmlEnumAttribute
struct  XmlEnumAttribute_t4_78  : public Attribute_t1_2
{
	// System.String System.Xml.Serialization.XmlEnumAttribute::name
	String_t* ___name_0;
};
