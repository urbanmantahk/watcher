﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Serialization.SerializationObjectManager/<RegisterObject>c__AnonStorey3
struct U3CRegisterObjectU3Ec__AnonStorey3_t1_1097;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.Runtime.Serialization.SerializationObjectManager/<RegisterObject>c__AnonStorey3::.ctor()
extern "C" void U3CRegisterObjectU3Ec__AnonStorey3__ctor_m1_9667 (U3CRegisterObjectU3Ec__AnonStorey3_t1_1097 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.SerializationObjectManager/<RegisterObject>c__AnonStorey3::<>m__2(System.Runtime.Serialization.StreamingContext)
extern "C" void U3CRegisterObjectU3Ec__AnonStorey3_U3CU3Em__2_m1_9668 (U3CRegisterObjectU3Ec__AnonStorey3_t1_1097 * __this, StreamingContext_t1_1050  ___ctx, const MethodInfo* method) IL2CPP_METHOD_ATTR;
