﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.MonoBehaviour
struct MonoBehaviour_t6_91;
// UnityEngine.WWW
struct WWW_t6_78;

#include "mscorlib_System_Object.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_URL.h"

// VoxelBusters.Utility.Request
struct  Request_t8_155  : public Object_t
{
	// System.Boolean VoxelBusters.Utility.Request::<IsAsynchronous>k__BackingField
	bool ___U3CIsAsynchronousU3Ek__BackingField_1;
	// VoxelBusters.Utility.URL VoxelBusters.Utility.Request::<URL>k__BackingField
	URL_t8_156  ___U3CURLU3Ek__BackingField_2;
	// UnityEngine.WWW VoxelBusters.Utility.Request::<WWWObject>k__BackingField
	WWW_t6_78 * ___U3CWWWObjectU3Ek__BackingField_3;
};
struct Request_t8_155_StaticFields{
	// UnityEngine.MonoBehaviour VoxelBusters.Utility.Request::surrogateMonobehaviour
	MonoBehaviour_t6_91 * ___surrogateMonobehaviour_0;
};
