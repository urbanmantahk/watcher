﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "AssemblyU2DCSharp_ExifLibrary_SceneType.h"

// ExifLibrary.SceneType
struct  SceneType_t8_76 
{
	// System.Byte ExifLibrary.SceneType::value__
	uint8_t ___value___1;
};
