﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Text_Encoder.h"

// System.Text.UTF8Encoding/UTF8Encoder
struct  UTF8Encoder_t1_1452  : public Encoder_t1_1428
{
	// System.Char System.Text.UTF8Encoding/UTF8Encoder::leftOverForCount
	uint16_t ___leftOverForCount_2;
	// System.Char System.Text.UTF8Encoding/UTF8Encoder::leftOverForConv
	uint16_t ___leftOverForConv_3;
};
