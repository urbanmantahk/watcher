﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.NameTable/Entry
struct Entry_t4_107;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Xml.NameTable/Entry::.ctor(System.String,System.Int32,System.Xml.NameTable/Entry)
extern "C" void Entry__ctor_m4_307 (Entry_t4_107 * __this, String_t* ___str, int32_t ___hash, Entry_t4_107 * ___next, const MethodInfo* method) IL2CPP_METHOD_ATTR;
