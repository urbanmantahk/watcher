﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.Schema.XmlSchemaSimpleTypeContent
struct XmlSchemaSimpleTypeContent_t4_69;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Xml.Schema.XmlSchemaSimpleTypeContent::.ctor()
extern "C" void XmlSchemaSimpleTypeContent__ctor_m4_101 (XmlSchemaSimpleTypeContent_t4_69 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
