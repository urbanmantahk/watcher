﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.Cryptography.PKCS8
struct PKCS8_t1_172;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_Mono_Security_Cryptography_PKCS8_KeyInfo.h"

// System.Void Mono.Security.Cryptography.PKCS8::.ctor()
extern "C" void PKCS8__ctor_m1_2045 (PKCS8_t1_172 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.Cryptography.PKCS8/KeyInfo Mono.Security.Cryptography.PKCS8::GetType(System.Byte[])
extern "C" int32_t PKCS8_GetType_m1_2046 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
