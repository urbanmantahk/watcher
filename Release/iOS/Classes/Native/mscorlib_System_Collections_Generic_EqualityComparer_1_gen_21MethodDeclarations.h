﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>
struct EqualityComparer_1_t1_2727;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Generic.EqualityComparer`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::.ctor()
extern "C" void EqualityComparer_1__ctor_m1_27280_gshared (EqualityComparer_1_t1_2727 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m1_27280(__this, method) (( void (*) (EqualityComparer_1_t1_2727 *, const MethodInfo*))EqualityComparer_1__ctor_m1_27280_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::.cctor()
extern "C" void EqualityComparer_1__cctor_m1_27281_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m1_27281(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m1_27281_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C" int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1_27282_gshared (EqualityComparer_1_t1_2727 * __this, Object_t * ___obj, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1_27282(__this, ___obj, method) (( int32_t (*) (EqualityComparer_1_t1_2727 *, Object_t *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1_27282_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C" bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1_27283_gshared (EqualityComparer_1_t1_2727 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1_27283(__this, ___x, ___y, method) (( bool (*) (EqualityComparer_1_t1_2727 *, Object_t *, Object_t *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1_27283_gshared)(__this, ___x, ___y, method)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::get_Default()
extern "C" EqualityComparer_1_t1_2727 * EqualityComparer_1_get_Default_m1_27284_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m1_27284(__this /* static, unused */, method) (( EqualityComparer_1_t1_2727 * (*) (Object_t * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m1_27284_gshared)(__this /* static, unused */, method)
