﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_Emit_ParameterToken.h"

// System.Void System.Reflection.Emit.ParameterToken::.ctor(System.Int32)
extern "C" void ParameterToken__ctor_m1_6290 (ParameterToken_t1_546 * __this, int32_t ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ParameterToken::.cctor()
extern "C" void ParameterToken__cctor_m1_6291 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.ParameterToken::Equals(System.Object)
extern "C" bool ParameterToken_Equals_m1_6292 (ParameterToken_t1_546 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.ParameterToken::Equals(System.Reflection.Emit.ParameterToken)
extern "C" bool ParameterToken_Equals_m1_6293 (ParameterToken_t1_546 * __this, ParameterToken_t1_546  ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.ParameterToken::GetHashCode()
extern "C" int32_t ParameterToken_GetHashCode_m1_6294 (ParameterToken_t1_546 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.ParameterToken::get_Token()
extern "C" int32_t ParameterToken_get_Token_m1_6295 (ParameterToken_t1_546 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.ParameterToken::op_Equality(System.Reflection.Emit.ParameterToken,System.Reflection.Emit.ParameterToken)
extern "C" bool ParameterToken_op_Equality_m1_6296 (Object_t * __this /* static, unused */, ParameterToken_t1_546  ___a, ParameterToken_t1_546  ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.ParameterToken::op_Inequality(System.Reflection.Emit.ParameterToken,System.Reflection.Emit.ParameterToken)
extern "C" bool ParameterToken_op_Inequality_m1_6297 (Object_t * __this /* static, unused */, ParameterToken_t1_546  ___a, ParameterToken_t1_546  ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
