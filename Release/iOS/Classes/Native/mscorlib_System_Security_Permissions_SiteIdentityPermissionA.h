﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Security_Permissions_CodeAccessSecurityAttri.h"

// System.Security.Permissions.SiteIdentityPermissionAttribute
struct  SiteIdentityPermissionAttribute_t1_1312  : public CodeAccessSecurityAttribute_t1_1266
{
	// System.String System.Security.Permissions.SiteIdentityPermissionAttribute::site
	String_t* ___site_2;
};
