﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExifLibrary.ExifEnumProperty`1<ExifLibrary.Sharpness>
struct ExifEnumProperty_1_t8_359;
// System.Object
struct Object_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTag.h"
#include "AssemblyU2DCSharp_ExifLibrary_Sharpness.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifInterOperability.h"

// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.Sharpness>::.ctor(ExifLibrary.ExifTag,T,System.Boolean)
extern "C" void ExifEnumProperty_1__ctor_m8_1996_gshared (ExifEnumProperty_1_t8_359 * __this, int32_t ___tag, uint16_t ___value, bool ___isbitfield, const MethodInfo* method);
#define ExifEnumProperty_1__ctor_m8_1996(__this, ___tag, ___value, ___isbitfield, method) (( void (*) (ExifEnumProperty_1_t8_359 *, int32_t, uint16_t, bool, const MethodInfo*))ExifEnumProperty_1__ctor_m8_1996_gshared)(__this, ___tag, ___value, ___isbitfield, method)
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.Sharpness>::.ctor(ExifLibrary.ExifTag,T)
extern "C" void ExifEnumProperty_1__ctor_m8_2261_gshared (ExifEnumProperty_1_t8_359 * __this, int32_t ___tag, uint16_t ___value, const MethodInfo* method);
#define ExifEnumProperty_1__ctor_m8_2261(__this, ___tag, ___value, method) (( void (*) (ExifEnumProperty_1_t8_359 *, int32_t, uint16_t, const MethodInfo*))ExifEnumProperty_1__ctor_m8_2261_gshared)(__this, ___tag, ___value, method)
// System.Object ExifLibrary.ExifEnumProperty`1<ExifLibrary.Sharpness>::get__Value()
extern "C" Object_t * ExifEnumProperty_1_get__Value_m8_2262_gshared (ExifEnumProperty_1_t8_359 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get__Value_m8_2262(__this, method) (( Object_t * (*) (ExifEnumProperty_1_t8_359 *, const MethodInfo*))ExifEnumProperty_1_get__Value_m8_2262_gshared)(__this, method)
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.Sharpness>::set__Value(System.Object)
extern "C" void ExifEnumProperty_1_set__Value_m8_2263_gshared (ExifEnumProperty_1_t8_359 * __this, Object_t * ___value, const MethodInfo* method);
#define ExifEnumProperty_1_set__Value_m8_2263(__this, ___value, method) (( void (*) (ExifEnumProperty_1_t8_359 *, Object_t *, const MethodInfo*))ExifEnumProperty_1_set__Value_m8_2263_gshared)(__this, ___value, method)
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.Sharpness>::get_Value()
extern "C" uint16_t ExifEnumProperty_1_get_Value_m8_2264_gshared (ExifEnumProperty_1_t8_359 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get_Value_m8_2264(__this, method) (( uint16_t (*) (ExifEnumProperty_1_t8_359 *, const MethodInfo*))ExifEnumProperty_1_get_Value_m8_2264_gshared)(__this, method)
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.Sharpness>::set_Value(T)
extern "C" void ExifEnumProperty_1_set_Value_m8_2265_gshared (ExifEnumProperty_1_t8_359 * __this, uint16_t ___value, const MethodInfo* method);
#define ExifEnumProperty_1_set_Value_m8_2265(__this, ___value, method) (( void (*) (ExifEnumProperty_1_t8_359 *, uint16_t, const MethodInfo*))ExifEnumProperty_1_set_Value_m8_2265_gshared)(__this, ___value, method)
// System.Boolean ExifLibrary.ExifEnumProperty`1<ExifLibrary.Sharpness>::get_IsBitField()
extern "C" bool ExifEnumProperty_1_get_IsBitField_m8_2266_gshared (ExifEnumProperty_1_t8_359 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get_IsBitField_m8_2266(__this, method) (( bool (*) (ExifEnumProperty_1_t8_359 *, const MethodInfo*))ExifEnumProperty_1_get_IsBitField_m8_2266_gshared)(__this, method)
// System.String ExifLibrary.ExifEnumProperty`1<ExifLibrary.Sharpness>::ToString()
extern "C" String_t* ExifEnumProperty_1_ToString_m8_2267_gshared (ExifEnumProperty_1_t8_359 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_ToString_m8_2267(__this, method) (( String_t* (*) (ExifEnumProperty_1_t8_359 *, const MethodInfo*))ExifEnumProperty_1_ToString_m8_2267_gshared)(__this, method)
// ExifLibrary.ExifInterOperability ExifLibrary.ExifEnumProperty`1<ExifLibrary.Sharpness>::get_Interoperability()
extern "C" ExifInterOperability_t8_113  ExifEnumProperty_1_get_Interoperability_m8_2268_gshared (ExifEnumProperty_1_t8_359 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get_Interoperability_m8_2268(__this, method) (( ExifInterOperability_t8_113  (*) (ExifEnumProperty_1_t8_359 *, const MethodInfo*))ExifEnumProperty_1_get_Interoperability_m8_2268_gshared)(__this, method)
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.Sharpness>::op_Implicit(ExifLibrary.ExifEnumProperty`1<T>)
extern "C" uint16_t ExifEnumProperty_1_op_Implicit_m8_2269_gshared (Object_t * __this /* static, unused */, ExifEnumProperty_1_t8_359 * ___obj, const MethodInfo* method);
#define ExifEnumProperty_1_op_Implicit_m8_2269(__this /* static, unused */, ___obj, method) (( uint16_t (*) (Object_t * /* static, unused */, ExifEnumProperty_1_t8_359 *, const MethodInfo*))ExifEnumProperty_1_op_Implicit_m8_2269_gshared)(__this /* static, unused */, ___obj, method)
