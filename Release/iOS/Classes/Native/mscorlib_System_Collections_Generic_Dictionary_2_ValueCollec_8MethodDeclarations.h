﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>
struct ValueCollection_t1_2024;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t1_2018;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1_2210;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Object[]
struct ObjectU5BU5D_t1_272;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_9.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ValueCollection__ctor_m1_16008_gshared (ValueCollection_t1_2024 * __this, Dictionary_2_t1_2018 * ___dictionary, const MethodInfo* method);
#define ValueCollection__ctor_m1_16008(__this, ___dictionary, method) (( void (*) (ValueCollection_t1_2024 *, Dictionary_2_t1_2018 *, const MethodInfo*))ValueCollection__ctor_m1_16008_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1_16009_gshared (ValueCollection_t1_2024 * __this, Object_t * ___item, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1_16009(__this, ___item, method) (( void (*) (ValueCollection_t1_2024 *, Object_t *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1_16009_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1_16010_gshared (ValueCollection_t1_2024 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1_16010(__this, method) (( void (*) (ValueCollection_t1_2024 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1_16010_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1_16011_gshared (ValueCollection_t1_2024 * __this, Object_t * ___item, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1_16011(__this, ___item, method) (( bool (*) (ValueCollection_t1_2024 *, Object_t *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1_16011_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1_16012_gshared (ValueCollection_t1_2024 * __this, Object_t * ___item, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1_16012(__this, ___item, method) (( bool (*) (ValueCollection_t1_2024 *, Object_t *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1_16012_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C" Object_t* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1_16013_gshared (ValueCollection_t1_2024 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1_16013(__this, method) (( Object_t* (*) (ValueCollection_t1_2024 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1_16013_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m1_16014_gshared (ValueCollection_t1_2024 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m1_16014(__this, ___array, ___index, method) (( void (*) (ValueCollection_t1_2024 *, Array_t *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m1_16014_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1_16015_gshared (ValueCollection_t1_2024 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1_16015(__this, method) (( Object_t * (*) (ValueCollection_t1_2024 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1_16015_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1_16016_gshared (ValueCollection_t1_2024 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1_16016(__this, method) (( bool (*) (ValueCollection_t1_2024 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1_16016_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1_16017_gshared (ValueCollection_t1_2024 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1_16017(__this, method) (( bool (*) (ValueCollection_t1_2024 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1_16017_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * ValueCollection_System_Collections_ICollection_get_SyncRoot_m1_16018_gshared (ValueCollection_t1_2024 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m1_16018(__this, method) (( Object_t * (*) (ValueCollection_t1_2024 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1_16018_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>::CopyTo(TValue[],System.Int32)
extern "C" void ValueCollection_CopyTo_m1_16019_gshared (ValueCollection_t1_2024 * __this, ObjectU5BU5D_t1_272* ___array, int32_t ___index, const MethodInfo* method);
#define ValueCollection_CopyTo_m1_16019(__this, ___array, ___index, method) (( void (*) (ValueCollection_t1_2024 *, ObjectU5BU5D_t1_272*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1_16019_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>::GetEnumerator()
extern "C" Enumerator_t1_2025  ValueCollection_GetEnumerator_m1_16020_gshared (ValueCollection_t1_2024 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m1_16020(__this, method) (( Enumerator_t1_2025  (*) (ValueCollection_t1_2024 *, const MethodInfo*))ValueCollection_GetEnumerator_m1_16020_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>::get_Count()
extern "C" int32_t ValueCollection_get_Count_m1_16021_gshared (ValueCollection_t1_2024 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m1_16021(__this, method) (( int32_t (*) (ValueCollection_t1_2024 *, const MethodInfo*))ValueCollection_get_Count_m1_16021_gshared)(__this, method)
