﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapMonth
struct SoapMonth_t1_980;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime.h"

// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapMonth::.ctor()
extern "C" void SoapMonth__ctor_m1_8782 (SoapMonth_t1_980 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapMonth::.ctor(System.DateTime)
extern "C" void SoapMonth__ctor_m1_8783 (SoapMonth_t1_980 * __this, DateTime_t1_150  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapMonth::.cctor()
extern "C" void SoapMonth__cctor_m1_8784 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Runtime.Remoting.Metadata.W3cXsd2001.SoapMonth::get_Value()
extern "C" DateTime_t1_150  SoapMonth_get_Value_m1_8785 (SoapMonth_t1_980 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapMonth::set_Value(System.DateTime)
extern "C" void SoapMonth_set_Value_m1_8786 (SoapMonth_t1_980 * __this, DateTime_t1_150  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapMonth::get_XsdType()
extern "C" String_t* SoapMonth_get_XsdType_m1_8787 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapMonth::GetXsdType()
extern "C" String_t* SoapMonth_GetXsdType_m1_8788 (SoapMonth_t1_980 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapMonth System.Runtime.Remoting.Metadata.W3cXsd2001.SoapMonth::Parse(System.String)
extern "C" SoapMonth_t1_980 * SoapMonth_Parse_m1_8789 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapMonth::ToString()
extern "C" String_t* SoapMonth_ToString_m1_8790 (SoapMonth_t1_980 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
