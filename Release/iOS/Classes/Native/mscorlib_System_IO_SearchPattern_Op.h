﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.IO.SearchPattern/Op
struct Op_t1_440;

#include "mscorlib_System_Object.h"
#include "mscorlib_System_IO_SearchPattern_OpCode.h"

// System.IO.SearchPattern/Op
struct  Op_t1_440  : public Object_t
{
	// System.IO.SearchPattern/OpCode System.IO.SearchPattern/Op::Code
	int32_t ___Code_0;
	// System.String System.IO.SearchPattern/Op::Argument
	String_t* ___Argument_1;
	// System.IO.SearchPattern/Op System.IO.SearchPattern/Op::Next
	Op_t1_440 * ___Next_2;
};
