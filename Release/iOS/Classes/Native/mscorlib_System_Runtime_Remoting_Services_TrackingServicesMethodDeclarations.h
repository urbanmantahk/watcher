﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Services.TrackingServices
struct TrackingServices_t1_1007;
// System.Runtime.Remoting.Services.ITrackingHandler
struct ITrackingHandler_t1_1721;
// System.Runtime.Remoting.Services.ITrackingHandler[]
struct ITrackingHandlerU5BU5D_t1_1720;
// System.Object
struct Object_t;
// System.Runtime.Remoting.ObjRef
struct ObjRef_t1_923;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Services.TrackingServices::.ctor()
extern "C" void TrackingServices__ctor_m1_9028 (TrackingServices_t1_1007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Services.TrackingServices::.cctor()
extern "C" void TrackingServices__cctor_m1_9029 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Services.TrackingServices::RegisterTrackingHandler(System.Runtime.Remoting.Services.ITrackingHandler)
extern "C" void TrackingServices_RegisterTrackingHandler_m1_9030 (Object_t * __this /* static, unused */, Object_t * ___handler, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Services.TrackingServices::UnregisterTrackingHandler(System.Runtime.Remoting.Services.ITrackingHandler)
extern "C" void TrackingServices_UnregisterTrackingHandler_m1_9031 (Object_t * __this /* static, unused */, Object_t * ___handler, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Services.ITrackingHandler[] System.Runtime.Remoting.Services.TrackingServices::get_RegisteredHandlers()
extern "C" ITrackingHandlerU5BU5D_t1_1720* TrackingServices_get_RegisteredHandlers_m1_9032 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Services.TrackingServices::NotifyMarshaledObject(System.Object,System.Runtime.Remoting.ObjRef)
extern "C" void TrackingServices_NotifyMarshaledObject_m1_9033 (Object_t * __this /* static, unused */, Object_t * ___obj, ObjRef_t1_923 * ___or, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Services.TrackingServices::NotifyUnmarshaledObject(System.Object,System.Runtime.Remoting.ObjRef)
extern "C" void TrackingServices_NotifyUnmarshaledObject_m1_9034 (Object_t * __this /* static, unused */, Object_t * ___obj, ObjRef_t1_923 * ___or, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Services.TrackingServices::NotifyDisconnectedObject(System.Object)
extern "C" void TrackingServices_NotifyDisconnectedObject_m1_9035 (Object_t * __this /* static, unused */, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
