﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExifLibrary.ExifBitConverter
struct ExifBitConverter_t8_62;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.UInt16[]
struct UInt16U5BU5D_t1_1231;
// System.UInt32[]
struct UInt32U5BU5D_t1_142;
// System.Int32[]
struct Int32U5BU5D_t1_275;
// ExifLibrary.MathEx/UFraction32[]
struct UFraction32U5BU5D_t8_122;
// ExifLibrary.MathEx/Fraction32[]
struct Fraction32U5BU5D_t8_129;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ExifLibrary_BitConverterEx_ByteOrder.h"
#include "mscorlib_System_DateTime.h"
#include "AssemblyU2DCSharp_ExifLibrary_MathEx_UFraction32.h"
#include "AssemblyU2DCSharp_ExifLibrary_MathEx_Fraction32.h"

// System.Void ExifLibrary.ExifBitConverter::.ctor(ExifLibrary.BitConverterEx/ByteOrder,ExifLibrary.BitConverterEx/ByteOrder)
extern "C" void ExifBitConverter__ctor_m8_382 (ExifBitConverter_t8_62 * __this, int32_t ___from, int32_t ___to, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExifLibrary.ExifBitConverter::ToAscii(System.Byte[],System.Boolean)
extern "C" String_t* ExifBitConverter_ToAscii_m8_383 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___data, bool ___endatfirstnull, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExifLibrary.ExifBitConverter::ToAscii(System.Byte[])
extern "C" String_t* ExifBitConverter_ToAscii_m8_384 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExifLibrary.ExifBitConverter::ToString(System.Byte[])
extern "C" String_t* ExifBitConverter_ToString_m8_385 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime ExifLibrary.ExifBitConverter::ToDateTime(System.Byte[],System.Boolean)
extern "C" DateTime_t1_150  ExifBitConverter_ToDateTime_m8_386 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___data, bool ___hastime, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime ExifLibrary.ExifBitConverter::ToDateTime(System.Byte[])
extern "C" DateTime_t1_150  ExifBitConverter_ToDateTime_m8_387 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/UFraction32 ExifLibrary.ExifBitConverter::ToURational(System.Byte[],ExifLibrary.BitConverterEx/ByteOrder)
extern "C" UFraction32_t8_121  ExifBitConverter_ToURational_m8_388 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___data, int32_t ___frombyteorder, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/Fraction32 ExifLibrary.ExifBitConverter::ToSRational(System.Byte[],ExifLibrary.BitConverterEx/ByteOrder)
extern "C" Fraction32_t8_127  ExifBitConverter_ToSRational_m8_389 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___data, int32_t ___frombyteorder, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16[] ExifLibrary.ExifBitConverter::ToUShortArray(System.Byte[],System.Int32,ExifLibrary.BitConverterEx/ByteOrder)
extern "C" UInt16U5BU5D_t1_1231* ExifBitConverter_ToUShortArray_m8_390 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___data, int32_t ___count, int32_t ___frombyteorder, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32[] ExifLibrary.ExifBitConverter::ToUIntArray(System.Byte[],System.Int32,ExifLibrary.BitConverterEx/ByteOrder)
extern "C" UInt32U5BU5D_t1_142* ExifBitConverter_ToUIntArray_m8_391 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___data, int32_t ___count, int32_t ___frombyteorder, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] ExifLibrary.ExifBitConverter::ToSIntArray(System.Byte[],System.Int32,ExifLibrary.BitConverterEx/ByteOrder)
extern "C" Int32U5BU5D_t1_275* ExifBitConverter_ToSIntArray_m8_392 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___data, int32_t ___count, int32_t ___byteorder, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/UFraction32[] ExifLibrary.ExifBitConverter::ToURationalArray(System.Byte[],System.Int32,ExifLibrary.BitConverterEx/ByteOrder)
extern "C" UFraction32U5BU5D_t8_122* ExifBitConverter_ToURationalArray_m8_393 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___data, int32_t ___count, int32_t ___frombyteorder, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/Fraction32[] ExifLibrary.ExifBitConverter::ToSRationalArray(System.Byte[],System.Int32,ExifLibrary.BitConverterEx/ByteOrder)
extern "C" Fraction32U5BU5D_t8_129* ExifBitConverter_ToSRationalArray_m8_394 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___data, int32_t ___count, int32_t ___frombyteorder, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ExifLibrary.ExifBitConverter::GetBytes(System.String,System.Boolean)
extern "C" ByteU5BU5D_t1_109* ExifBitConverter_GetBytes_m8_395 (Object_t * __this /* static, unused */, String_t* ___value, bool ___addnull, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ExifLibrary.ExifBitConverter::GetBytes(System.String)
extern "C" ByteU5BU5D_t1_109* ExifBitConverter_GetBytes_m8_396 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ExifLibrary.ExifBitConverter::GetBytes(System.DateTime,System.Boolean)
extern "C" ByteU5BU5D_t1_109* ExifBitConverter_GetBytes_m8_397 (Object_t * __this /* static, unused */, DateTime_t1_150  ___value, bool ___hastime, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ExifLibrary.ExifBitConverter::GetBytes(ExifLibrary.MathEx/UFraction32,ExifLibrary.BitConverterEx/ByteOrder)
extern "C" ByteU5BU5D_t1_109* ExifBitConverter_GetBytes_m8_398 (Object_t * __this /* static, unused */, UFraction32_t8_121  ___value, int32_t ___tobyteorder, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ExifLibrary.ExifBitConverter::GetBytes(ExifLibrary.MathEx/Fraction32,ExifLibrary.BitConverterEx/ByteOrder)
extern "C" ByteU5BU5D_t1_109* ExifBitConverter_GetBytes_m8_399 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___value, int32_t ___tobyteorder, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ExifLibrary.ExifBitConverter::GetBytes(System.UInt16[],ExifLibrary.BitConverterEx/ByteOrder)
extern "C" ByteU5BU5D_t1_109* ExifBitConverter_GetBytes_m8_400 (Object_t * __this /* static, unused */, UInt16U5BU5D_t1_1231* ___value, int32_t ___tobyteorder, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ExifLibrary.ExifBitConverter::GetBytes(System.UInt32[],ExifLibrary.BitConverterEx/ByteOrder)
extern "C" ByteU5BU5D_t1_109* ExifBitConverter_GetBytes_m8_401 (Object_t * __this /* static, unused */, UInt32U5BU5D_t1_142* ___value, int32_t ___tobyteorder, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ExifLibrary.ExifBitConverter::GetBytes(System.Int32[],ExifLibrary.BitConverterEx/ByteOrder)
extern "C" ByteU5BU5D_t1_109* ExifBitConverter_GetBytes_m8_402 (Object_t * __this /* static, unused */, Int32U5BU5D_t1_275* ___value, int32_t ___tobyteorder, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ExifLibrary.ExifBitConverter::GetBytes(ExifLibrary.MathEx/UFraction32[],ExifLibrary.BitConverterEx/ByteOrder)
extern "C" ByteU5BU5D_t1_109* ExifBitConverter_GetBytes_m8_403 (Object_t * __this /* static, unused */, UFraction32U5BU5D_t8_122* ___value, int32_t ___tobyteorder, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ExifLibrary.ExifBitConverter::GetBytes(ExifLibrary.MathEx/Fraction32[],ExifLibrary.BitConverterEx/ByteOrder)
extern "C" ByteU5BU5D_t1_109* ExifBitConverter_GetBytes_m8_404 (Object_t * __this /* static, unused */, Fraction32U5BU5D_t8_129* ___value, int32_t ___tobyteorder, const MethodInfo* method) IL2CPP_METHOD_ATTR;
