﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object.h"

// VoxelBusters.NativePlugins.BillingSettings/iOSSettings
struct  iOSSettings_t8_216  : public Object_t
{
	// System.Boolean VoxelBusters.NativePlugins.BillingSettings/iOSSettings::m_supportsReceiptValidation
	bool ___m_supportsReceiptValidation_0;
	// System.String VoxelBusters.NativePlugins.BillingSettings/iOSSettings::m_validateUsingServerURL
	String_t* ___m_validateUsingServerURL_1;
};
