﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Serialization.Formatters.Binary.MessageFormatter
struct MessageFormatter_t1_1052;
// System.IO.BinaryWriter
struct BinaryWriter_t1_408;
// System.Object
struct Object_t;
// System.Runtime.Remoting.Messaging.Header[]
struct HeaderU5BU5D_t1_927;
// System.Runtime.Serialization.ISurrogateSelector
struct ISurrogateSelector_t1_959;
// System.IO.BinaryReader
struct BinaryReader_t1_404;
// System.Runtime.Remoting.Messaging.HeaderHandler
struct HeaderHandler_t1_1623;
// System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
struct BinaryFormatter_t1_1028;
// System.Runtime.Remoting.Messaging.IMethodCallMessage
struct IMethodCallMessage_t1_949;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Type
struct Type_t;
// System.Collections.IDictionary
struct IDictionary_t1_35;
// System.String[]
struct StringU5BU5D_t1_238;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_FormatterAs.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_FormatterTy.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Bina_0.h"

// System.Void System.Runtime.Serialization.Formatters.Binary.MessageFormatter::.ctor()
extern "C" void MessageFormatter__ctor_m1_9375 (MessageFormatter_t1_1052 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.Formatters.Binary.MessageFormatter::WriteMethodCall(System.IO.BinaryWriter,System.Object,System.Runtime.Remoting.Messaging.Header[],System.Runtime.Serialization.ISurrogateSelector,System.Runtime.Serialization.StreamingContext,System.Runtime.Serialization.Formatters.FormatterAssemblyStyle,System.Runtime.Serialization.Formatters.FormatterTypeStyle)
extern "C" void MessageFormatter_WriteMethodCall_m1_9376 (Object_t * __this /* static, unused */, BinaryWriter_t1_408 * ___writer, Object_t * ___obj, HeaderU5BU5D_t1_927* ___headers, Object_t * ___surrogateSelector, StreamingContext_t1_1050  ___context, int32_t ___assemblyFormat, int32_t ___typeFormat, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.Formatters.Binary.MessageFormatter::WriteMethodResponse(System.IO.BinaryWriter,System.Object,System.Runtime.Remoting.Messaging.Header[],System.Runtime.Serialization.ISurrogateSelector,System.Runtime.Serialization.StreamingContext,System.Runtime.Serialization.Formatters.FormatterAssemblyStyle,System.Runtime.Serialization.Formatters.FormatterTypeStyle)
extern "C" void MessageFormatter_WriteMethodResponse_m1_9377 (Object_t * __this /* static, unused */, BinaryWriter_t1_408 * ___writer, Object_t * ___obj, HeaderU5BU5D_t1_927* ___headers, Object_t * ___surrogateSelector, StreamingContext_t1_1050  ___context, int32_t ___assemblyFormat, int32_t ___typeFormat, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Serialization.Formatters.Binary.MessageFormatter::ReadMethodCall(System.IO.BinaryReader,System.Boolean,System.Runtime.Remoting.Messaging.HeaderHandler,System.Runtime.Serialization.Formatters.Binary.BinaryFormatter)
extern "C" Object_t * MessageFormatter_ReadMethodCall_m1_9378 (Object_t * __this /* static, unused */, BinaryReader_t1_404 * ___reader, bool ___hasHeaders, HeaderHandler_t1_1623 * ___headerHandler, BinaryFormatter_t1_1028 * ___formatter, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Serialization.Formatters.Binary.MessageFormatter::ReadMethodCall(System.Runtime.Serialization.Formatters.Binary.BinaryElement,System.IO.BinaryReader,System.Boolean,System.Runtime.Remoting.Messaging.HeaderHandler,System.Runtime.Serialization.Formatters.Binary.BinaryFormatter)
extern "C" Object_t * MessageFormatter_ReadMethodCall_m1_9379 (Object_t * __this /* static, unused */, uint8_t ___elem, BinaryReader_t1_404 * ___reader, bool ___hasHeaders, HeaderHandler_t1_1623 * ___headerHandler, BinaryFormatter_t1_1028 * ___formatter, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Serialization.Formatters.Binary.MessageFormatter::ReadMethodResponse(System.IO.BinaryReader,System.Boolean,System.Runtime.Remoting.Messaging.HeaderHandler,System.Runtime.Remoting.Messaging.IMethodCallMessage,System.Runtime.Serialization.Formatters.Binary.BinaryFormatter)
extern "C" Object_t * MessageFormatter_ReadMethodResponse_m1_9380 (Object_t * __this /* static, unused */, BinaryReader_t1_404 * ___reader, bool ___hasHeaders, HeaderHandler_t1_1623 * ___headerHandler, Object_t * ___methodCallMessage, BinaryFormatter_t1_1028 * ___formatter, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Serialization.Formatters.Binary.MessageFormatter::ReadMethodResponse(System.Runtime.Serialization.Formatters.Binary.BinaryElement,System.IO.BinaryReader,System.Boolean,System.Runtime.Remoting.Messaging.HeaderHandler,System.Runtime.Remoting.Messaging.IMethodCallMessage,System.Runtime.Serialization.Formatters.Binary.BinaryFormatter)
extern "C" Object_t * MessageFormatter_ReadMethodResponse_m1_9381 (Object_t * __this /* static, unused */, uint8_t ___elem, BinaryReader_t1_404 * ___reader, bool ___hasHeaders, HeaderHandler_t1_1623 * ___headerHandler, Object_t * ___methodCallMessage, BinaryFormatter_t1_1028 * ___formatter, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Serialization.Formatters.Binary.MessageFormatter::AllTypesArePrimitive(System.Object[])
extern "C" bool MessageFormatter_AllTypesArePrimitive_m1_9382 (Object_t * __this /* static, unused */, ObjectU5BU5D_t1_272* ___objects, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Serialization.Formatters.Binary.MessageFormatter::IsMethodPrimitive(System.Type)
extern "C" bool MessageFormatter_IsMethodPrimitive_m1_9383 (Object_t * __this /* static, unused */, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Runtime.Serialization.Formatters.Binary.MessageFormatter::GetExtraProperties(System.Collections.IDictionary,System.String[])
extern "C" ObjectU5BU5D_t1_272* MessageFormatter_GetExtraProperties_m1_9384 (Object_t * __this /* static, unused */, Object_t * ___properties, StringU5BU5D_t1_238* ___internalKeys, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Serialization.Formatters.Binary.MessageFormatter::IsInternalKey(System.String,System.String[])
extern "C" bool MessageFormatter_IsInternalKey_m1_9385 (Object_t * __this /* static, unused */, String_t* ___key, StringU5BU5D_t1_238* ___internalKeys, const MethodInfo* method) IL2CPP_METHOD_ATTR;
