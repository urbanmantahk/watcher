﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.KeyedCollection`2<System.Object,System.Object>
struct KeyedCollection_2_t1_2029;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t1_1980;
// System.Object
struct Object_t;
// System.Collections.Generic.IDictionary`2<System.Object,System.Object>
struct IDictionary_2_t1_2776;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.ObjectModel.KeyedCollection`2<System.Object,System.Object>::.ctor()
extern "C" void KeyedCollection_2__ctor_m1_16047_gshared (KeyedCollection_2_t1_2029 * __this, const MethodInfo* method);
#define KeyedCollection_2__ctor_m1_16047(__this, method) (( void (*) (KeyedCollection_2_t1_2029 *, const MethodInfo*))KeyedCollection_2__ctor_m1_16047_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.KeyedCollection`2<System.Object,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void KeyedCollection_2__ctor_m1_16048_gshared (KeyedCollection_2_t1_2029 * __this, Object_t* ___comparer, const MethodInfo* method);
#define KeyedCollection_2__ctor_m1_16048(__this, ___comparer, method) (( void (*) (KeyedCollection_2_t1_2029 *, Object_t*, const MethodInfo*))KeyedCollection_2__ctor_m1_16048_gshared)(__this, ___comparer, method)
// System.Void System.Collections.ObjectModel.KeyedCollection`2<System.Object,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>,System.Int32)
extern "C" void KeyedCollection_2__ctor_m1_16049_gshared (KeyedCollection_2_t1_2029 * __this, Object_t* ___comparer, int32_t ___dictionaryCreationThreshold, const MethodInfo* method);
#define KeyedCollection_2__ctor_m1_16049(__this, ___comparer, ___dictionaryCreationThreshold, method) (( void (*) (KeyedCollection_2_t1_2029 *, Object_t*, int32_t, const MethodInfo*))KeyedCollection_2__ctor_m1_16049_gshared)(__this, ___comparer, ___dictionaryCreationThreshold, method)
// System.Boolean System.Collections.ObjectModel.KeyedCollection`2<System.Object,System.Object>::Contains(TKey)
extern "C" bool KeyedCollection_2_Contains_m1_16050_gshared (KeyedCollection_2_t1_2029 * __this, Object_t * ___key, const MethodInfo* method);
#define KeyedCollection_2_Contains_m1_16050(__this, ___key, method) (( bool (*) (KeyedCollection_2_t1_2029 *, Object_t *, const MethodInfo*))KeyedCollection_2_Contains_m1_16050_gshared)(__this, ___key, method)
// System.Int32 System.Collections.ObjectModel.KeyedCollection`2<System.Object,System.Object>::IndexOfKey(TKey)
extern "C" int32_t KeyedCollection_2_IndexOfKey_m1_16051_gshared (KeyedCollection_2_t1_2029 * __this, Object_t * ___key, const MethodInfo* method);
#define KeyedCollection_2_IndexOfKey_m1_16051(__this, ___key, method) (( int32_t (*) (KeyedCollection_2_t1_2029 *, Object_t *, const MethodInfo*))KeyedCollection_2_IndexOfKey_m1_16051_gshared)(__this, ___key, method)
// System.Boolean System.Collections.ObjectModel.KeyedCollection`2<System.Object,System.Object>::Remove(TKey)
extern "C" bool KeyedCollection_2_Remove_m1_16052_gshared (KeyedCollection_2_t1_2029 * __this, Object_t * ___key, const MethodInfo* method);
#define KeyedCollection_2_Remove_m1_16052(__this, ___key, method) (( bool (*) (KeyedCollection_2_t1_2029 *, Object_t *, const MethodInfo*))KeyedCollection_2_Remove_m1_16052_gshared)(__this, ___key, method)
// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.ObjectModel.KeyedCollection`2<System.Object,System.Object>::get_Comparer()
extern "C" Object_t* KeyedCollection_2_get_Comparer_m1_16053_gshared (KeyedCollection_2_t1_2029 * __this, const MethodInfo* method);
#define KeyedCollection_2_get_Comparer_m1_16053(__this, method) (( Object_t* (*) (KeyedCollection_2_t1_2029 *, const MethodInfo*))KeyedCollection_2_get_Comparer_m1_16053_gshared)(__this, method)
// TItem System.Collections.ObjectModel.KeyedCollection`2<System.Object,System.Object>::get_Item(TKey)
extern "C" Object_t * KeyedCollection_2_get_Item_m1_16054_gshared (KeyedCollection_2_t1_2029 * __this, Object_t * ___key, const MethodInfo* method);
#define KeyedCollection_2_get_Item_m1_16054(__this, ___key, method) (( Object_t * (*) (KeyedCollection_2_t1_2029 *, Object_t *, const MethodInfo*))KeyedCollection_2_get_Item_m1_16054_gshared)(__this, ___key, method)
// System.Void System.Collections.ObjectModel.KeyedCollection`2<System.Object,System.Object>::ChangeItemKey(TItem,TKey)
extern "C" void KeyedCollection_2_ChangeItemKey_m1_16055_gshared (KeyedCollection_2_t1_2029 * __this, Object_t * ___item, Object_t * ___newKey, const MethodInfo* method);
#define KeyedCollection_2_ChangeItemKey_m1_16055(__this, ___item, ___newKey, method) (( void (*) (KeyedCollection_2_t1_2029 *, Object_t *, Object_t *, const MethodInfo*))KeyedCollection_2_ChangeItemKey_m1_16055_gshared)(__this, ___item, ___newKey, method)
// System.Void System.Collections.ObjectModel.KeyedCollection`2<System.Object,System.Object>::ClearItems()
extern "C" void KeyedCollection_2_ClearItems_m1_16056_gshared (KeyedCollection_2_t1_2029 * __this, const MethodInfo* method);
#define KeyedCollection_2_ClearItems_m1_16056(__this, method) (( void (*) (KeyedCollection_2_t1_2029 *, const MethodInfo*))KeyedCollection_2_ClearItems_m1_16056_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.KeyedCollection`2<System.Object,System.Object>::InsertItem(System.Int32,TItem)
extern "C" void KeyedCollection_2_InsertItem_m1_16057_gshared (KeyedCollection_2_t1_2029 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define KeyedCollection_2_InsertItem_m1_16057(__this, ___index, ___item, method) (( void (*) (KeyedCollection_2_t1_2029 *, int32_t, Object_t *, const MethodInfo*))KeyedCollection_2_InsertItem_m1_16057_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.KeyedCollection`2<System.Object,System.Object>::RemoveItem(System.Int32)
extern "C" void KeyedCollection_2_RemoveItem_m1_16058_gshared (KeyedCollection_2_t1_2029 * __this, int32_t ___index, const MethodInfo* method);
#define KeyedCollection_2_RemoveItem_m1_16058(__this, ___index, method) (( void (*) (KeyedCollection_2_t1_2029 *, int32_t, const MethodInfo*))KeyedCollection_2_RemoveItem_m1_16058_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.KeyedCollection`2<System.Object,System.Object>::SetItem(System.Int32,TItem)
extern "C" void KeyedCollection_2_SetItem_m1_16059_gshared (KeyedCollection_2_t1_2029 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define KeyedCollection_2_SetItem_m1_16059(__this, ___index, ___item, method) (( void (*) (KeyedCollection_2_t1_2029 *, int32_t, Object_t *, const MethodInfo*))KeyedCollection_2_SetItem_m1_16059_gshared)(__this, ___index, ___item, method)
// System.Collections.Generic.IDictionary`2<TKey,TItem> System.Collections.ObjectModel.KeyedCollection`2<System.Object,System.Object>::get_Dictionary()
extern "C" Object_t* KeyedCollection_2_get_Dictionary_m1_16060_gshared (KeyedCollection_2_t1_2029 * __this, const MethodInfo* method);
#define KeyedCollection_2_get_Dictionary_m1_16060(__this, method) (( Object_t* (*) (KeyedCollection_2_t1_2029 *, const MethodInfo*))KeyedCollection_2_get_Dictionary_m1_16060_gshared)(__this, method)
