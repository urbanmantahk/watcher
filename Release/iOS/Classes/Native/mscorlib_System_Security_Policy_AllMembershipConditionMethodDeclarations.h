﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Policy.AllMembershipCondition
struct AllMembershipCondition_t1_1325;
// System.Security.Policy.Evidence
struct Evidence_t1_398;
// System.Security.Policy.IMembershipCondition
struct IMembershipCondition_t1_1340;
// System.Object
struct Object_t;
// System.Security.SecurityElement
struct SecurityElement_t1_242;
// System.Security.Policy.PolicyLevel
struct PolicyLevel_t1_1357;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Policy.AllMembershipCondition::.ctor()
extern "C" void AllMembershipCondition__ctor_m1_11314 (AllMembershipCondition_t1_1325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Policy.AllMembershipCondition::Check(System.Security.Policy.Evidence)
extern "C" bool AllMembershipCondition_Check_m1_11315 (AllMembershipCondition_t1_1325 * __this, Evidence_t1_398 * ___evidence, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.IMembershipCondition System.Security.Policy.AllMembershipCondition::Copy()
extern "C" Object_t * AllMembershipCondition_Copy_m1_11316 (AllMembershipCondition_t1_1325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Policy.AllMembershipCondition::Equals(System.Object)
extern "C" bool AllMembershipCondition_Equals_m1_11317 (AllMembershipCondition_t1_1325 * __this, Object_t * ___o, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.AllMembershipCondition::FromXml(System.Security.SecurityElement)
extern "C" void AllMembershipCondition_FromXml_m1_11318 (AllMembershipCondition_t1_1325 * __this, SecurityElement_t1_242 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.AllMembershipCondition::FromXml(System.Security.SecurityElement,System.Security.Policy.PolicyLevel)
extern "C" void AllMembershipCondition_FromXml_m1_11319 (AllMembershipCondition_t1_1325 * __this, SecurityElement_t1_242 * ___e, PolicyLevel_t1_1357 * ___level, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Policy.AllMembershipCondition::GetHashCode()
extern "C" int32_t AllMembershipCondition_GetHashCode_m1_11320 (AllMembershipCondition_t1_1325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Policy.AllMembershipCondition::ToString()
extern "C" String_t* AllMembershipCondition_ToString_m1_11321 (AllMembershipCondition_t1_1325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityElement System.Security.Policy.AllMembershipCondition::ToXml()
extern "C" SecurityElement_t1_242 * AllMembershipCondition_ToXml_m1_11322 (AllMembershipCondition_t1_1325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityElement System.Security.Policy.AllMembershipCondition::ToXml(System.Security.Policy.PolicyLevel)
extern "C" SecurityElement_t1_242 * AllMembershipCondition_ToXml_m1_11323 (AllMembershipCondition_t1_1325 * __this, PolicyLevel_t1_1357 * ___level, const MethodInfo* method) IL2CPP_METHOD_ATTR;
