﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.CompilerServices.IndexerNameAttribute
struct IndexerNameAttribute_t1_44;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.CompilerServices.IndexerNameAttribute::.ctor(System.String)
extern "C" void IndexerNameAttribute__ctor_m1_1299 (IndexerNameAttribute_t1_44 * __this, String_t* ___indexerName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
