﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Serialization.OnSerializingAttribute
struct OnSerializingAttribute_t1_1089;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Serialization.OnSerializingAttribute::.ctor()
extern "C" void OnSerializingAttribute__ctor_m1_9586 (OnSerializingAttribute_t1_1089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
