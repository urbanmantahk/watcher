﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.AccessControl.RawSecurityDescriptor
struct RawSecurityDescriptor_t1_1171;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Security.Principal.SecurityIdentifier
struct SecurityIdentifier_t1_1132;
// System.Security.AccessControl.RawAcl
struct RawAcl_t1_1170;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_AccessControl_ControlFlags.h"

// System.Void System.Security.AccessControl.RawSecurityDescriptor::.ctor(System.String)
extern "C" void RawSecurityDescriptor__ctor_m1_10049 (RawSecurityDescriptor_t1_1171 * __this, String_t* ___sddlForm, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.RawSecurityDescriptor::.ctor(System.Byte[],System.Int32)
extern "C" void RawSecurityDescriptor__ctor_m1_10050 (RawSecurityDescriptor_t1_1171 * __this, ByteU5BU5D_t1_109* ___binaryForm, int32_t ___offset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.RawSecurityDescriptor::.ctor(System.Security.AccessControl.ControlFlags,System.Security.Principal.SecurityIdentifier,System.Security.Principal.SecurityIdentifier,System.Security.AccessControl.RawAcl,System.Security.AccessControl.RawAcl)
extern "C" void RawSecurityDescriptor__ctor_m1_10051 (RawSecurityDescriptor_t1_1171 * __this, int32_t ___flags, SecurityIdentifier_t1_1132 * ___owner, SecurityIdentifier_t1_1132 * ___group, RawAcl_t1_1170 * ___systemAcl, RawAcl_t1_1170 * ___discretionaryAcl, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.AccessControl.ControlFlags System.Security.AccessControl.RawSecurityDescriptor::get_ControlFlags()
extern "C" int32_t RawSecurityDescriptor_get_ControlFlags_m1_10052 (RawSecurityDescriptor_t1_1171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.AccessControl.RawAcl System.Security.AccessControl.RawSecurityDescriptor::get_DiscretionaryAcl()
extern "C" RawAcl_t1_1170 * RawSecurityDescriptor_get_DiscretionaryAcl_m1_10053 (RawSecurityDescriptor_t1_1171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.RawSecurityDescriptor::set_DiscretionaryAcl(System.Security.AccessControl.RawAcl)
extern "C" void RawSecurityDescriptor_set_DiscretionaryAcl_m1_10054 (RawSecurityDescriptor_t1_1171 * __this, RawAcl_t1_1170 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Principal.SecurityIdentifier System.Security.AccessControl.RawSecurityDescriptor::get_Group()
extern "C" SecurityIdentifier_t1_1132 * RawSecurityDescriptor_get_Group_m1_10055 (RawSecurityDescriptor_t1_1171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.RawSecurityDescriptor::set_Group(System.Security.Principal.SecurityIdentifier)
extern "C" void RawSecurityDescriptor_set_Group_m1_10056 (RawSecurityDescriptor_t1_1171 * __this, SecurityIdentifier_t1_1132 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Principal.SecurityIdentifier System.Security.AccessControl.RawSecurityDescriptor::get_Owner()
extern "C" SecurityIdentifier_t1_1132 * RawSecurityDescriptor_get_Owner_m1_10057 (RawSecurityDescriptor_t1_1171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.RawSecurityDescriptor::set_Owner(System.Security.Principal.SecurityIdentifier)
extern "C" void RawSecurityDescriptor_set_Owner_m1_10058 (RawSecurityDescriptor_t1_1171 * __this, SecurityIdentifier_t1_1132 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte System.Security.AccessControl.RawSecurityDescriptor::get_ResourceManagerControl()
extern "C" uint8_t RawSecurityDescriptor_get_ResourceManagerControl_m1_10059 (RawSecurityDescriptor_t1_1171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.RawSecurityDescriptor::set_ResourceManagerControl(System.Byte)
extern "C" void RawSecurityDescriptor_set_ResourceManagerControl_m1_10060 (RawSecurityDescriptor_t1_1171 * __this, uint8_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.AccessControl.RawAcl System.Security.AccessControl.RawSecurityDescriptor::get_SystemAcl()
extern "C" RawAcl_t1_1170 * RawSecurityDescriptor_get_SystemAcl_m1_10061 (RawSecurityDescriptor_t1_1171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.RawSecurityDescriptor::set_SystemAcl(System.Security.AccessControl.RawAcl)
extern "C" void RawSecurityDescriptor_set_SystemAcl_m1_10062 (RawSecurityDescriptor_t1_1171 * __this, RawAcl_t1_1170 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.RawSecurityDescriptor::SetFlags(System.Security.AccessControl.ControlFlags)
extern "C" void RawSecurityDescriptor_SetFlags_m1_10063 (RawSecurityDescriptor_t1_1171 * __this, int32_t ___flags, const MethodInfo* method) IL2CPP_METHOD_ATTR;
