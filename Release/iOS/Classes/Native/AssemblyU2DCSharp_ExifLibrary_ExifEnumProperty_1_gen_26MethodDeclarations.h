﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSMeasureMode>
struct ExifEnumProperty_1_t8_365;
// System.Object
struct Object_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTag.h"
#include "AssemblyU2DCSharp_ExifLibrary_GPSMeasureMode.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifInterOperability.h"

// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSMeasureMode>::.ctor(ExifLibrary.ExifTag,T,System.Boolean)
extern "C" void ExifEnumProperty_1__ctor_m8_2315_gshared (ExifEnumProperty_1_t8_365 * __this, int32_t ___tag, uint8_t ___value, bool ___isbitfield, const MethodInfo* method);
#define ExifEnumProperty_1__ctor_m8_2315(__this, ___tag, ___value, ___isbitfield, method) (( void (*) (ExifEnumProperty_1_t8_365 *, int32_t, uint8_t, bool, const MethodInfo*))ExifEnumProperty_1__ctor_m8_2315_gshared)(__this, ___tag, ___value, ___isbitfield, method)
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSMeasureMode>::.ctor(ExifLibrary.ExifTag,T)
extern "C" void ExifEnumProperty_1__ctor_m8_2002_gshared (ExifEnumProperty_1_t8_365 * __this, int32_t ___tag, uint8_t ___value, const MethodInfo* method);
#define ExifEnumProperty_1__ctor_m8_2002(__this, ___tag, ___value, method) (( void (*) (ExifEnumProperty_1_t8_365 *, int32_t, uint8_t, const MethodInfo*))ExifEnumProperty_1__ctor_m8_2002_gshared)(__this, ___tag, ___value, method)
// System.Object ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSMeasureMode>::get__Value()
extern "C" Object_t * ExifEnumProperty_1_get__Value_m8_2316_gshared (ExifEnumProperty_1_t8_365 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get__Value_m8_2316(__this, method) (( Object_t * (*) (ExifEnumProperty_1_t8_365 *, const MethodInfo*))ExifEnumProperty_1_get__Value_m8_2316_gshared)(__this, method)
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSMeasureMode>::set__Value(System.Object)
extern "C" void ExifEnumProperty_1_set__Value_m8_2317_gshared (ExifEnumProperty_1_t8_365 * __this, Object_t * ___value, const MethodInfo* method);
#define ExifEnumProperty_1_set__Value_m8_2317(__this, ___value, method) (( void (*) (ExifEnumProperty_1_t8_365 *, Object_t *, const MethodInfo*))ExifEnumProperty_1_set__Value_m8_2317_gshared)(__this, ___value, method)
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSMeasureMode>::get_Value()
extern "C" uint8_t ExifEnumProperty_1_get_Value_m8_2318_gshared (ExifEnumProperty_1_t8_365 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get_Value_m8_2318(__this, method) (( uint8_t (*) (ExifEnumProperty_1_t8_365 *, const MethodInfo*))ExifEnumProperty_1_get_Value_m8_2318_gshared)(__this, method)
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSMeasureMode>::set_Value(T)
extern "C" void ExifEnumProperty_1_set_Value_m8_2319_gshared (ExifEnumProperty_1_t8_365 * __this, uint8_t ___value, const MethodInfo* method);
#define ExifEnumProperty_1_set_Value_m8_2319(__this, ___value, method) (( void (*) (ExifEnumProperty_1_t8_365 *, uint8_t, const MethodInfo*))ExifEnumProperty_1_set_Value_m8_2319_gshared)(__this, ___value, method)
// System.Boolean ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSMeasureMode>::get_IsBitField()
extern "C" bool ExifEnumProperty_1_get_IsBitField_m8_2320_gshared (ExifEnumProperty_1_t8_365 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get_IsBitField_m8_2320(__this, method) (( bool (*) (ExifEnumProperty_1_t8_365 *, const MethodInfo*))ExifEnumProperty_1_get_IsBitField_m8_2320_gshared)(__this, method)
// System.String ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSMeasureMode>::ToString()
extern "C" String_t* ExifEnumProperty_1_ToString_m8_2321_gshared (ExifEnumProperty_1_t8_365 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_ToString_m8_2321(__this, method) (( String_t* (*) (ExifEnumProperty_1_t8_365 *, const MethodInfo*))ExifEnumProperty_1_ToString_m8_2321_gshared)(__this, method)
// ExifLibrary.ExifInterOperability ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSMeasureMode>::get_Interoperability()
extern "C" ExifInterOperability_t8_113  ExifEnumProperty_1_get_Interoperability_m8_2322_gshared (ExifEnumProperty_1_t8_365 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get_Interoperability_m8_2322(__this, method) (( ExifInterOperability_t8_113  (*) (ExifEnumProperty_1_t8_365 *, const MethodInfo*))ExifEnumProperty_1_get_Interoperability_m8_2322_gshared)(__this, method)
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSMeasureMode>::op_Implicit(ExifLibrary.ExifEnumProperty`1<T>)
extern "C" uint8_t ExifEnumProperty_1_op_Implicit_m8_2323_gshared (Object_t * __this /* static, unused */, ExifEnumProperty_1_t8_365 * ___obj, const MethodInfo* method);
#define ExifEnumProperty_1_op_Implicit_m8_2323(__this /* static, unused */, ___obj, method) (( uint8_t (*) (Object_t * /* static, unused */, ExifEnumProperty_1_t8_365 *, const MethodInfo*))ExifEnumProperty_1_op_Implicit_m8_2323_gshared)(__this /* static, unused */, ___obj, method)
