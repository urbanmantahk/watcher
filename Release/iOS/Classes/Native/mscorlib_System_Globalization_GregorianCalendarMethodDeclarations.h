﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Globalization.GregorianCalendar
struct GregorianCalendar_t1_370;
// System.Int32[]
struct Int32U5BU5D_t1_275;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Globalization_GregorianCalendarTypes.h"
#include "mscorlib_System_DateTime.h"
#include "mscorlib_System_DayOfWeek.h"
#include "mscorlib_System_Globalization_CalendarWeekRule.h"

// System.Void System.Globalization.GregorianCalendar::.ctor(System.Globalization.GregorianCalendarTypes)
extern "C" void GregorianCalendar__ctor_m1_4087 (GregorianCalendar_t1_370 * __this, int32_t ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.GregorianCalendar::.ctor()
extern "C" void GregorianCalendar__ctor_m1_4088 (GregorianCalendar_t1_370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] System.Globalization.GregorianCalendar::get_Eras()
extern "C" Int32U5BU5D_t1_275* GregorianCalendar_get_Eras_m1_4089 (GregorianCalendar_t1_370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.GregorianCalendar::get_TwoDigitYearMax()
extern "C" int32_t GregorianCalendar_get_TwoDigitYearMax_m1_4090 (GregorianCalendar_t1_370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.GregorianCalendar::set_TwoDigitYearMax(System.Int32)
extern "C" void GregorianCalendar_set_TwoDigitYearMax_m1_4091 (GregorianCalendar_t1_370 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.GregorianCalendarTypes System.Globalization.GregorianCalendar::get_CalendarType()
extern "C" int32_t GregorianCalendar_get_CalendarType_m1_4092 (GregorianCalendar_t1_370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.GregorianCalendar::set_CalendarType(System.Globalization.GregorianCalendarTypes)
extern "C" void GregorianCalendar_set_CalendarType_m1_4093 (GregorianCalendar_t1_370 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.GregorianCalendar::M_CheckEra(System.Int32&)
extern "C" void GregorianCalendar_M_CheckEra_m1_4094 (GregorianCalendar_t1_370 * __this, int32_t* ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.GregorianCalendar::M_CheckYE(System.Int32,System.Int32&)
extern "C" void GregorianCalendar_M_CheckYE_m1_4095 (GregorianCalendar_t1_370 * __this, int32_t ___year, int32_t* ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.GregorianCalendar::M_CheckYME(System.Int32,System.Int32,System.Int32&)
extern "C" void GregorianCalendar_M_CheckYME_m1_4096 (GregorianCalendar_t1_370 * __this, int32_t ___year, int32_t ___month, int32_t* ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.GregorianCalendar::M_CheckYMDE(System.Int32,System.Int32,System.Int32,System.Int32&)
extern "C" void GregorianCalendar_M_CheckYMDE_m1_4097 (GregorianCalendar_t1_370 * __this, int32_t ___year, int32_t ___month, int32_t ___day, int32_t* ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.GregorianCalendar::AddMonths(System.DateTime,System.Int32)
extern "C" DateTime_t1_150  GregorianCalendar_AddMonths_m1_4098 (GregorianCalendar_t1_370 * __this, DateTime_t1_150  ___time, int32_t ___months, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.GregorianCalendar::AddYears(System.DateTime,System.Int32)
extern "C" DateTime_t1_150  GregorianCalendar_AddYears_m1_4099 (GregorianCalendar_t1_370 * __this, DateTime_t1_150  ___time, int32_t ___years, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.GregorianCalendar::GetDayOfMonth(System.DateTime)
extern "C" int32_t GregorianCalendar_GetDayOfMonth_m1_4100 (GregorianCalendar_t1_370 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DayOfWeek System.Globalization.GregorianCalendar::GetDayOfWeek(System.DateTime)
extern "C" int32_t GregorianCalendar_GetDayOfWeek_m1_4101 (GregorianCalendar_t1_370 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.GregorianCalendar::GetDayOfYear(System.DateTime)
extern "C" int32_t GregorianCalendar_GetDayOfYear_m1_4102 (GregorianCalendar_t1_370 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.GregorianCalendar::GetDaysInMonth(System.Int32,System.Int32,System.Int32)
extern "C" int32_t GregorianCalendar_GetDaysInMonth_m1_4103 (GregorianCalendar_t1_370 * __this, int32_t ___year, int32_t ___month, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.GregorianCalendar::GetDaysInYear(System.Int32,System.Int32)
extern "C" int32_t GregorianCalendar_GetDaysInYear_m1_4104 (GregorianCalendar_t1_370 * __this, int32_t ___year, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.GregorianCalendar::GetEra(System.DateTime)
extern "C" int32_t GregorianCalendar_GetEra_m1_4105 (GregorianCalendar_t1_370 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.GregorianCalendar::GetLeapMonth(System.Int32,System.Int32)
extern "C" int32_t GregorianCalendar_GetLeapMonth_m1_4106 (GregorianCalendar_t1_370 * __this, int32_t ___year, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.GregorianCalendar::GetMonth(System.DateTime)
extern "C" int32_t GregorianCalendar_GetMonth_m1_4107 (GregorianCalendar_t1_370 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.GregorianCalendar::GetMonthsInYear(System.Int32,System.Int32)
extern "C" int32_t GregorianCalendar_GetMonthsInYear_m1_4108 (GregorianCalendar_t1_370 * __this, int32_t ___year, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.GregorianCalendar::GetWeekOfYear(System.DateTime,System.Globalization.CalendarWeekRule,System.DayOfWeek)
extern "C" int32_t GregorianCalendar_GetWeekOfYear_m1_4109 (GregorianCalendar_t1_370 * __this, DateTime_t1_150  ___time, int32_t ___rule, int32_t ___firstDayOfWeek, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.GregorianCalendar::GetYear(System.DateTime)
extern "C" int32_t GregorianCalendar_GetYear_m1_4110 (GregorianCalendar_t1_370 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.GregorianCalendar::IsLeapDay(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" bool GregorianCalendar_IsLeapDay_m1_4111 (GregorianCalendar_t1_370 * __this, int32_t ___year, int32_t ___month, int32_t ___day, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.GregorianCalendar::IsLeapMonth(System.Int32,System.Int32,System.Int32)
extern "C" bool GregorianCalendar_IsLeapMonth_m1_4112 (GregorianCalendar_t1_370 * __this, int32_t ___year, int32_t ___month, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.GregorianCalendar::IsLeapYear(System.Int32,System.Int32)
extern "C" bool GregorianCalendar_IsLeapYear_m1_4113 (GregorianCalendar_t1_370 * __this, int32_t ___year, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.GregorianCalendar::ToDateTime(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" DateTime_t1_150  GregorianCalendar_ToDateTime_m1_4114 (GregorianCalendar_t1_370 * __this, int32_t ___year, int32_t ___month, int32_t ___day, int32_t ___hour, int32_t ___minute, int32_t ___second, int32_t ___millisecond, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.GregorianCalendar::ToFourDigitYear(System.Int32)
extern "C" int32_t GregorianCalendar_ToFourDigitYear_m1_4115 (GregorianCalendar_t1_370 * __this, int32_t ___year, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.GregorianCalendar::get_MinSupportedDateTime()
extern "C" DateTime_t1_150  GregorianCalendar_get_MinSupportedDateTime_m1_4116 (GregorianCalendar_t1_370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.GregorianCalendar::get_MaxSupportedDateTime()
extern "C" DateTime_t1_150  GregorianCalendar_get_MaxSupportedDateTime_m1_4117 (GregorianCalendar_t1_370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
