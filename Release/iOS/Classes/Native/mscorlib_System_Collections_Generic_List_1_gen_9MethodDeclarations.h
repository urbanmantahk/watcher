﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t1_1836;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Vector2>
struct IEnumerable_1_t1_2802;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t6_279;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector2>
struct IEnumerator_1_t1_2803;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<UnityEngine.Vector2>
struct ICollection_1_t1_2804;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector2>
struct ReadOnlyCollection_1_t1_2291;
// System.Collections.Generic.IComparer`1<UnityEngine.Vector2>
struct IComparer_1_t1_2805;
// System.Predicate`1<UnityEngine.Vector2>
struct Predicate_1_t1_2298;
// System.Action`1<UnityEngine.Vector2>
struct Action_1_t1_2299;
// System.Comparison`1<UnityEngine.Vector2>
struct Comparison_1_t1_2300;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector2.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_17.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::.ctor()
extern "C" void List_1__ctor_m1_19134_gshared (List_1_t1_1836 * __this, const MethodInfo* method);
#define List_1__ctor_m1_19134(__this, method) (( void (*) (List_1_t1_1836 *, const MethodInfo*))List_1__ctor_m1_19134_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m1_19135_gshared (List_1_t1_1836 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m1_19135(__this, ___collection, method) (( void (*) (List_1_t1_1836 *, Object_t*, const MethodInfo*))List_1__ctor_m1_19135_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::.ctor(System.Int32)
extern "C" void List_1__ctor_m1_19136_gshared (List_1_t1_1836 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m1_19136(__this, ___capacity, method) (( void (*) (List_1_t1_1836 *, int32_t, const MethodInfo*))List_1__ctor_m1_19136_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::.ctor(T[],System.Int32)
extern "C" void List_1__ctor_m1_19137_gshared (List_1_t1_1836 * __this, Vector2U5BU5D_t6_279* ___data, int32_t ___size, const MethodInfo* method);
#define List_1__ctor_m1_19137(__this, ___data, ___size, method) (( void (*) (List_1_t1_1836 *, Vector2U5BU5D_t6_279*, int32_t, const MethodInfo*))List_1__ctor_m1_19137_gshared)(__this, ___data, ___size, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::.cctor()
extern "C" void List_1__cctor_m1_19138_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m1_19138(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m1_19138_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_19139_gshared (List_1_t1_1836 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_19139(__this, method) (( Object_t* (*) (List_1_t1_1836 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_19139_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m1_19140_gshared (List_1_t1_1836 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m1_19140(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_1836 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m1_19140_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m1_19141_gshared (List_1_t1_1836 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1_19141(__this, method) (( Object_t * (*) (List_1_t1_1836 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1_19141_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m1_19142_gshared (List_1_t1_1836 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m1_19142(__this, ___item, method) (( int32_t (*) (List_1_t1_1836 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m1_19142_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m1_19143_gshared (List_1_t1_1836 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m1_19143(__this, ___item, method) (( bool (*) (List_1_t1_1836 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1_19143_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m1_19144_gshared (List_1_t1_1836 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m1_19144(__this, ___item, method) (( int32_t (*) (List_1_t1_1836 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m1_19144_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m1_19145_gshared (List_1_t1_1836 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m1_19145(__this, ___index, ___item, method) (( void (*) (List_1_t1_1836 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m1_19145_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m1_19146_gshared (List_1_t1_1836 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m1_19146(__this, ___item, method) (( void (*) (List_1_t1_1836 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1_19146_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_19147_gshared (List_1_t1_1836 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_19147(__this, method) (( bool (*) (List_1_t1_1836 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_19147_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m1_19148_gshared (List_1_t1_1836 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m1_19148(__this, method) (( bool (*) (List_1_t1_1836 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m1_19148_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m1_19149_gshared (List_1_t1_1836 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m1_19149(__this, method) (( Object_t * (*) (List_1_t1_1836 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1_19149_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m1_19150_gshared (List_1_t1_1836 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m1_19150(__this, method) (( bool (*) (List_1_t1_1836 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m1_19150_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m1_19151_gshared (List_1_t1_1836 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m1_19151(__this, method) (( bool (*) (List_1_t1_1836 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m1_19151_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m1_19152_gshared (List_1_t1_1836 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m1_19152(__this, ___index, method) (( Object_t * (*) (List_1_t1_1836 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m1_19152_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m1_19153_gshared (List_1_t1_1836 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m1_19153(__this, ___index, ___value, method) (( void (*) (List_1_t1_1836 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m1_19153_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Add(T)
extern "C" void List_1_Add_m1_19154_gshared (List_1_t1_1836 * __this, Vector2_t6_47  ___item, const MethodInfo* method);
#define List_1_Add_m1_19154(__this, ___item, method) (( void (*) (List_1_t1_1836 *, Vector2_t6_47 , const MethodInfo*))List_1_Add_m1_19154_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m1_19155_gshared (List_1_t1_1836 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m1_19155(__this, ___newCount, method) (( void (*) (List_1_t1_1836 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m1_19155_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::CheckRange(System.Int32,System.Int32)
extern "C" void List_1_CheckRange_m1_19156_gshared (List_1_t1_1836 * __this, int32_t ___idx, int32_t ___count, const MethodInfo* method);
#define List_1_CheckRange_m1_19156(__this, ___idx, ___count, method) (( void (*) (List_1_t1_1836 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m1_19156_gshared)(__this, ___idx, ___count, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m1_19157_gshared (List_1_t1_1836 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m1_19157(__this, ___collection, method) (( void (*) (List_1_t1_1836 *, Object_t*, const MethodInfo*))List_1_AddCollection_m1_19157_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m1_19158_gshared (List_1_t1_1836 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m1_19158(__this, ___enumerable, method) (( void (*) (List_1_t1_1836 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m1_19158_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m1_15021_gshared (List_1_t1_1836 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m1_15021(__this, ___collection, method) (( void (*) (List_1_t1_1836 *, Object_t*, const MethodInfo*))List_1_AddRange_m1_15021_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.Vector2>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t1_2291 * List_1_AsReadOnly_m1_19159_gshared (List_1_t1_1836 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m1_19159(__this, method) (( ReadOnlyCollection_1_t1_2291 * (*) (List_1_t1_1836 *, const MethodInfo*))List_1_AsReadOnly_m1_19159_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::BinarySearch(T)
extern "C" int32_t List_1_BinarySearch_m1_19160_gshared (List_1_t1_1836 * __this, Vector2_t6_47  ___item, const MethodInfo* method);
#define List_1_BinarySearch_m1_19160(__this, ___item, method) (( int32_t (*) (List_1_t1_1836 *, Vector2_t6_47 , const MethodInfo*))List_1_BinarySearch_m1_19160_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::BinarySearch(T,System.Collections.Generic.IComparer`1<T>)
extern "C" int32_t List_1_BinarySearch_m1_19161_gshared (List_1_t1_1836 * __this, Vector2_t6_47  ___item, Object_t* ___comparer, const MethodInfo* method);
#define List_1_BinarySearch_m1_19161(__this, ___item, ___comparer, method) (( int32_t (*) (List_1_t1_1836 *, Vector2_t6_47 , Object_t*, const MethodInfo*))List_1_BinarySearch_m1_19161_gshared)(__this, ___item, ___comparer, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::BinarySearch(System.Int32,System.Int32,T,System.Collections.Generic.IComparer`1<T>)
extern "C" int32_t List_1_BinarySearch_m1_19162_gshared (List_1_t1_1836 * __this, int32_t ___index, int32_t ___count, Vector2_t6_47  ___item, Object_t* ___comparer, const MethodInfo* method);
#define List_1_BinarySearch_m1_19162(__this, ___index, ___count, ___item, ___comparer, method) (( int32_t (*) (List_1_t1_1836 *, int32_t, int32_t, Vector2_t6_47 , Object_t*, const MethodInfo*))List_1_BinarySearch_m1_19162_gshared)(__this, ___index, ___count, ___item, ___comparer, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Clear()
extern "C" void List_1_Clear_m1_19163_gshared (List_1_t1_1836 * __this, const MethodInfo* method);
#define List_1_Clear_m1_19163(__this, method) (( void (*) (List_1_t1_1836 *, const MethodInfo*))List_1_Clear_m1_19163_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector2>::Contains(T)
extern "C" bool List_1_Contains_m1_19164_gshared (List_1_t1_1836 * __this, Vector2_t6_47  ___item, const MethodInfo* method);
#define List_1_Contains_m1_19164(__this, ___item, method) (( bool (*) (List_1_t1_1836 *, Vector2_t6_47 , const MethodInfo*))List_1_Contains_m1_19164_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::CopyTo(T[])
extern "C" void List_1_CopyTo_m1_19165_gshared (List_1_t1_1836 * __this, Vector2U5BU5D_t6_279* ___array, const MethodInfo* method);
#define List_1_CopyTo_m1_19165(__this, ___array, method) (( void (*) (List_1_t1_1836 *, Vector2U5BU5D_t6_279*, const MethodInfo*))List_1_CopyTo_m1_19165_gshared)(__this, ___array, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m1_19166_gshared (List_1_t1_1836 * __this, Vector2U5BU5D_t6_279* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m1_19166(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_1836 *, Vector2U5BU5D_t6_279*, int32_t, const MethodInfo*))List_1_CopyTo_m1_19166_gshared)(__this, ___array, ___arrayIndex, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::CopyTo(System.Int32,T[],System.Int32,System.Int32)
extern "C" void List_1_CopyTo_m1_19167_gshared (List_1_t1_1836 * __this, int32_t ___index, Vector2U5BU5D_t6_279* ___array, int32_t ___arrayIndex, int32_t ___count, const MethodInfo* method);
#define List_1_CopyTo_m1_19167(__this, ___index, ___array, ___arrayIndex, ___count, method) (( void (*) (List_1_t1_1836 *, int32_t, Vector2U5BU5D_t6_279*, int32_t, int32_t, const MethodInfo*))List_1_CopyTo_m1_19167_gshared)(__this, ___index, ___array, ___arrayIndex, ___count, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector2>::Exists(System.Predicate`1<T>)
extern "C" bool List_1_Exists_m1_19168_gshared (List_1_t1_1836 * __this, Predicate_1_t1_2298 * ___match, const MethodInfo* method);
#define List_1_Exists_m1_19168(__this, ___match, method) (( bool (*) (List_1_t1_1836 *, Predicate_1_t1_2298 *, const MethodInfo*))List_1_Exists_m1_19168_gshared)(__this, ___match, method)
// T System.Collections.Generic.List`1<UnityEngine.Vector2>::Find(System.Predicate`1<T>)
extern "C" Vector2_t6_47  List_1_Find_m1_19169_gshared (List_1_t1_1836 * __this, Predicate_1_t1_2298 * ___match, const MethodInfo* method);
#define List_1_Find_m1_19169(__this, ___match, method) (( Vector2_t6_47  (*) (List_1_t1_1836 *, Predicate_1_t1_2298 *, const MethodInfo*))List_1_Find_m1_19169_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m1_19170_gshared (Object_t * __this /* static, unused */, Predicate_1_t1_2298 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m1_19170(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t1_2298 *, const MethodInfo*))List_1_CheckMatch_m1_19170_gshared)(__this /* static, unused */, ___match, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<UnityEngine.Vector2>::FindAll(System.Predicate`1<T>)
extern "C" List_1_t1_1836 * List_1_FindAll_m1_19171_gshared (List_1_t1_1836 * __this, Predicate_1_t1_2298 * ___match, const MethodInfo* method);
#define List_1_FindAll_m1_19171(__this, ___match, method) (( List_1_t1_1836 * (*) (List_1_t1_1836 *, Predicate_1_t1_2298 *, const MethodInfo*))List_1_FindAll_m1_19171_gshared)(__this, ___match, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<UnityEngine.Vector2>::FindAllStackBits(System.Predicate`1<T>)
extern "C" List_1_t1_1836 * List_1_FindAllStackBits_m1_19172_gshared (List_1_t1_1836 * __this, Predicate_1_t1_2298 * ___match, const MethodInfo* method);
#define List_1_FindAllStackBits_m1_19172(__this, ___match, method) (( List_1_t1_1836 * (*) (List_1_t1_1836 *, Predicate_1_t1_2298 *, const MethodInfo*))List_1_FindAllStackBits_m1_19172_gshared)(__this, ___match, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<UnityEngine.Vector2>::FindAllList(System.Predicate`1<T>)
extern "C" List_1_t1_1836 * List_1_FindAllList_m1_19173_gshared (List_1_t1_1836 * __this, Predicate_1_t1_2298 * ___match, const MethodInfo* method);
#define List_1_FindAllList_m1_19173(__this, ___match, method) (( List_1_t1_1836 * (*) (List_1_t1_1836 *, Predicate_1_t1_2298 *, const MethodInfo*))List_1_FindAllList_m1_19173_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::FindIndex(System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m1_19174_gshared (List_1_t1_1836 * __this, Predicate_1_t1_2298 * ___match, const MethodInfo* method);
#define List_1_FindIndex_m1_19174(__this, ___match, method) (( int32_t (*) (List_1_t1_1836 *, Predicate_1_t1_2298 *, const MethodInfo*))List_1_FindIndex_m1_19174_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::FindIndex(System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m1_19175_gshared (List_1_t1_1836 * __this, int32_t ___startIndex, Predicate_1_t1_2298 * ___match, const MethodInfo* method);
#define List_1_FindIndex_m1_19175(__this, ___startIndex, ___match, method) (( int32_t (*) (List_1_t1_1836 *, int32_t, Predicate_1_t1_2298 *, const MethodInfo*))List_1_FindIndex_m1_19175_gshared)(__this, ___startIndex, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::FindIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m1_19176_gshared (List_1_t1_1836 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_2298 * ___match, const MethodInfo* method);
#define List_1_FindIndex_m1_19176(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_1836 *, int32_t, int32_t, Predicate_1_t1_2298 *, const MethodInfo*))List_1_FindIndex_m1_19176_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m1_19177_gshared (List_1_t1_1836 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_2298 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m1_19177(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_1836 *, int32_t, int32_t, Predicate_1_t1_2298 *, const MethodInfo*))List_1_GetIndex_m1_19177_gshared)(__this, ___startIndex, ___count, ___match, method)
// T System.Collections.Generic.List`1<UnityEngine.Vector2>::FindLast(System.Predicate`1<T>)
extern "C" Vector2_t6_47  List_1_FindLast_m1_19178_gshared (List_1_t1_1836 * __this, Predicate_1_t1_2298 * ___match, const MethodInfo* method);
#define List_1_FindLast_m1_19178(__this, ___match, method) (( Vector2_t6_47  (*) (List_1_t1_1836 *, Predicate_1_t1_2298 *, const MethodInfo*))List_1_FindLast_m1_19178_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::FindLastIndex(System.Predicate`1<T>)
extern "C" int32_t List_1_FindLastIndex_m1_19179_gshared (List_1_t1_1836 * __this, Predicate_1_t1_2298 * ___match, const MethodInfo* method);
#define List_1_FindLastIndex_m1_19179(__this, ___match, method) (( int32_t (*) (List_1_t1_1836 *, Predicate_1_t1_2298 *, const MethodInfo*))List_1_FindLastIndex_m1_19179_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::FindLastIndex(System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindLastIndex_m1_19180_gshared (List_1_t1_1836 * __this, int32_t ___startIndex, Predicate_1_t1_2298 * ___match, const MethodInfo* method);
#define List_1_FindLastIndex_m1_19180(__this, ___startIndex, ___match, method) (( int32_t (*) (List_1_t1_1836 *, int32_t, Predicate_1_t1_2298 *, const MethodInfo*))List_1_FindLastIndex_m1_19180_gshared)(__this, ___startIndex, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::FindLastIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindLastIndex_m1_19181_gshared (List_1_t1_1836 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_2298 * ___match, const MethodInfo* method);
#define List_1_FindLastIndex_m1_19181(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_1836 *, int32_t, int32_t, Predicate_1_t1_2298 *, const MethodInfo*))List_1_FindLastIndex_m1_19181_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::GetLastIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetLastIndex_m1_19182_gshared (List_1_t1_1836 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_2298 * ___match, const MethodInfo* method);
#define List_1_GetLastIndex_m1_19182(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_1836 *, int32_t, int32_t, Predicate_1_t1_2298 *, const MethodInfo*))List_1_GetLastIndex_m1_19182_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::ForEach(System.Action`1<T>)
extern "C" void List_1_ForEach_m1_19183_gshared (List_1_t1_1836 * __this, Action_1_t1_2299 * ___action, const MethodInfo* method);
#define List_1_ForEach_m1_19183(__this, ___action, method) (( void (*) (List_1_t1_1836 *, Action_1_t1_2299 *, const MethodInfo*))List_1_ForEach_m1_19183_gshared)(__this, ___action, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Vector2>::GetEnumerator()
extern "C" Enumerator_t1_2290  List_1_GetEnumerator_m1_19184_gshared (List_1_t1_1836 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m1_19184(__this, method) (( Enumerator_t1_2290  (*) (List_1_t1_1836 *, const MethodInfo*))List_1_GetEnumerator_m1_19184_gshared)(__this, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<UnityEngine.Vector2>::GetRange(System.Int32,System.Int32)
extern "C" List_1_t1_1836 * List_1_GetRange_m1_19185_gshared (List_1_t1_1836 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_GetRange_m1_19185(__this, ___index, ___count, method) (( List_1_t1_1836 * (*) (List_1_t1_1836 *, int32_t, int32_t, const MethodInfo*))List_1_GetRange_m1_19185_gshared)(__this, ___index, ___count, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m1_19186_gshared (List_1_t1_1836 * __this, Vector2_t6_47  ___item, const MethodInfo* method);
#define List_1_IndexOf_m1_19186(__this, ___item, method) (( int32_t (*) (List_1_t1_1836 *, Vector2_t6_47 , const MethodInfo*))List_1_IndexOf_m1_19186_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::IndexOf(T,System.Int32)
extern "C" int32_t List_1_IndexOf_m1_19187_gshared (List_1_t1_1836 * __this, Vector2_t6_47  ___item, int32_t ___index, const MethodInfo* method);
#define List_1_IndexOf_m1_19187(__this, ___item, ___index, method) (( int32_t (*) (List_1_t1_1836 *, Vector2_t6_47 , int32_t, const MethodInfo*))List_1_IndexOf_m1_19187_gshared)(__this, ___item, ___index, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::IndexOf(T,System.Int32,System.Int32)
extern "C" int32_t List_1_IndexOf_m1_19188_gshared (List_1_t1_1836 * __this, Vector2_t6_47  ___item, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_IndexOf_m1_19188(__this, ___item, ___index, ___count, method) (( int32_t (*) (List_1_t1_1836 *, Vector2_t6_47 , int32_t, int32_t, const MethodInfo*))List_1_IndexOf_m1_19188_gshared)(__this, ___item, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m1_19189_gshared (List_1_t1_1836 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m1_19189(__this, ___start, ___delta, method) (( void (*) (List_1_t1_1836 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m1_19189_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m1_19190_gshared (List_1_t1_1836 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m1_19190(__this, ___index, method) (( void (*) (List_1_t1_1836 *, int32_t, const MethodInfo*))List_1_CheckIndex_m1_19190_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m1_19191_gshared (List_1_t1_1836 * __this, int32_t ___index, Vector2_t6_47  ___item, const MethodInfo* method);
#define List_1_Insert_m1_19191(__this, ___index, ___item, method) (( void (*) (List_1_t1_1836 *, int32_t, Vector2_t6_47 , const MethodInfo*))List_1_Insert_m1_19191_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m1_19192_gshared (List_1_t1_1836 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m1_19192(__this, ___collection, method) (( void (*) (List_1_t1_1836 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m1_19192_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::InsertRange(System.Int32,System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_InsertRange_m1_19193_gshared (List_1_t1_1836 * __this, int32_t ___index, Object_t* ___collection, const MethodInfo* method);
#define List_1_InsertRange_m1_19193(__this, ___index, ___collection, method) (( void (*) (List_1_t1_1836 *, int32_t, Object_t*, const MethodInfo*))List_1_InsertRange_m1_19193_gshared)(__this, ___index, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::InsertCollection(System.Int32,System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_InsertCollection_m1_19194_gshared (List_1_t1_1836 * __this, int32_t ___index, Object_t* ___collection, const MethodInfo* method);
#define List_1_InsertCollection_m1_19194(__this, ___index, ___collection, method) (( void (*) (List_1_t1_1836 *, int32_t, Object_t*, const MethodInfo*))List_1_InsertCollection_m1_19194_gshared)(__this, ___index, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::InsertEnumeration(System.Int32,System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_InsertEnumeration_m1_19195_gshared (List_1_t1_1836 * __this, int32_t ___index, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_InsertEnumeration_m1_19195(__this, ___index, ___enumerable, method) (( void (*) (List_1_t1_1836 *, int32_t, Object_t*, const MethodInfo*))List_1_InsertEnumeration_m1_19195_gshared)(__this, ___index, ___enumerable, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::LastIndexOf(T)
extern "C" int32_t List_1_LastIndexOf_m1_19196_gshared (List_1_t1_1836 * __this, Vector2_t6_47  ___item, const MethodInfo* method);
#define List_1_LastIndexOf_m1_19196(__this, ___item, method) (( int32_t (*) (List_1_t1_1836 *, Vector2_t6_47 , const MethodInfo*))List_1_LastIndexOf_m1_19196_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::LastIndexOf(T,System.Int32)
extern "C" int32_t List_1_LastIndexOf_m1_19197_gshared (List_1_t1_1836 * __this, Vector2_t6_47  ___item, int32_t ___index, const MethodInfo* method);
#define List_1_LastIndexOf_m1_19197(__this, ___item, ___index, method) (( int32_t (*) (List_1_t1_1836 *, Vector2_t6_47 , int32_t, const MethodInfo*))List_1_LastIndexOf_m1_19197_gshared)(__this, ___item, ___index, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::LastIndexOf(T,System.Int32,System.Int32)
extern "C" int32_t List_1_LastIndexOf_m1_19198_gshared (List_1_t1_1836 * __this, Vector2_t6_47  ___item, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_LastIndexOf_m1_19198(__this, ___item, ___index, ___count, method) (( int32_t (*) (List_1_t1_1836 *, Vector2_t6_47 , int32_t, int32_t, const MethodInfo*))List_1_LastIndexOf_m1_19198_gshared)(__this, ___item, ___index, ___count, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector2>::Remove(T)
extern "C" bool List_1_Remove_m1_19199_gshared (List_1_t1_1836 * __this, Vector2_t6_47  ___item, const MethodInfo* method);
#define List_1_Remove_m1_19199(__this, ___item, method) (( bool (*) (List_1_t1_1836 *, Vector2_t6_47 , const MethodInfo*))List_1_Remove_m1_19199_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m1_19200_gshared (List_1_t1_1836 * __this, Predicate_1_t1_2298 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m1_19200(__this, ___match, method) (( int32_t (*) (List_1_t1_1836 *, Predicate_1_t1_2298 *, const MethodInfo*))List_1_RemoveAll_m1_19200_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m1_19201_gshared (List_1_t1_1836 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m1_19201(__this, ___index, method) (( void (*) (List_1_t1_1836 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1_19201_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::RemoveRange(System.Int32,System.Int32)
extern "C" void List_1_RemoveRange_m1_19202_gshared (List_1_t1_1836 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_RemoveRange_m1_19202(__this, ___index, ___count, method) (( void (*) (List_1_t1_1836 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m1_19202_gshared)(__this, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Reverse()
extern "C" void List_1_Reverse_m1_19203_gshared (List_1_t1_1836 * __this, const MethodInfo* method);
#define List_1_Reverse_m1_19203(__this, method) (( void (*) (List_1_t1_1836 *, const MethodInfo*))List_1_Reverse_m1_19203_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Reverse(System.Int32,System.Int32)
extern "C" void List_1_Reverse_m1_19204_gshared (List_1_t1_1836 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_Reverse_m1_19204(__this, ___index, ___count, method) (( void (*) (List_1_t1_1836 *, int32_t, int32_t, const MethodInfo*))List_1_Reverse_m1_19204_gshared)(__this, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Sort()
extern "C" void List_1_Sort_m1_19205_gshared (List_1_t1_1836 * __this, const MethodInfo* method);
#define List_1_Sort_m1_19205(__this, method) (( void (*) (List_1_t1_1836 *, const MethodInfo*))List_1_Sort_m1_19205_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C" void List_1_Sort_m1_19206_gshared (List_1_t1_1836 * __this, Object_t* ___comparer, const MethodInfo* method);
#define List_1_Sort_m1_19206(__this, ___comparer, method) (( void (*) (List_1_t1_1836 *, Object_t*, const MethodInfo*))List_1_Sort_m1_19206_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m1_19207_gshared (List_1_t1_1836 * __this, Comparison_1_t1_2300 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m1_19207(__this, ___comparison, method) (( void (*) (List_1_t1_1836 *, Comparison_1_t1_2300 *, const MethodInfo*))List_1_Sort_m1_19207_gshared)(__this, ___comparison, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Sort(System.Int32,System.Int32,System.Collections.Generic.IComparer`1<T>)
extern "C" void List_1_Sort_m1_19208_gshared (List_1_t1_1836 * __this, int32_t ___index, int32_t ___count, Object_t* ___comparer, const MethodInfo* method);
#define List_1_Sort_m1_19208(__this, ___index, ___count, ___comparer, method) (( void (*) (List_1_t1_1836 *, int32_t, int32_t, Object_t*, const MethodInfo*))List_1_Sort_m1_19208_gshared)(__this, ___index, ___count, ___comparer, method)
// T[] System.Collections.Generic.List`1<UnityEngine.Vector2>::ToArray()
extern "C" Vector2U5BU5D_t6_279* List_1_ToArray_m1_19209_gshared (List_1_t1_1836 * __this, const MethodInfo* method);
#define List_1_ToArray_m1_19209(__this, method) (( Vector2U5BU5D_t6_279* (*) (List_1_t1_1836 *, const MethodInfo*))List_1_ToArray_m1_19209_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::TrimExcess()
extern "C" void List_1_TrimExcess_m1_19210_gshared (List_1_t1_1836 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m1_19210(__this, method) (( void (*) (List_1_t1_1836 *, const MethodInfo*))List_1_TrimExcess_m1_19210_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector2>::TrueForAll(System.Predicate`1<T>)
extern "C" bool List_1_TrueForAll_m1_19211_gshared (List_1_t1_1836 * __this, Predicate_1_t1_2298 * ___match, const MethodInfo* method);
#define List_1_TrueForAll_m1_19211(__this, ___match, method) (( bool (*) (List_1_t1_1836 *, Predicate_1_t1_2298 *, const MethodInfo*))List_1_TrueForAll_m1_19211_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m1_19212_gshared (List_1_t1_1836 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m1_19212(__this, method) (( int32_t (*) (List_1_t1_1836 *, const MethodInfo*))List_1_get_Capacity_m1_19212_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m1_19213_gshared (List_1_t1_1836 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m1_19213(__this, ___value, method) (( void (*) (List_1_t1_1836 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1_19213_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::get_Count()
extern "C" int32_t List_1_get_Count_m1_19214_gshared (List_1_t1_1836 * __this, const MethodInfo* method);
#define List_1_get_Count_m1_19214(__this, method) (( int32_t (*) (List_1_t1_1836 *, const MethodInfo*))List_1_get_Count_m1_19214_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.Vector2>::get_Item(System.Int32)
extern "C" Vector2_t6_47  List_1_get_Item_m1_19215_gshared (List_1_t1_1836 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m1_19215(__this, ___index, method) (( Vector2_t6_47  (*) (List_1_t1_1836 *, int32_t, const MethodInfo*))List_1_get_Item_m1_19215_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m1_19216_gshared (List_1_t1_1836 * __this, int32_t ___index, Vector2_t6_47  ___value, const MethodInfo* method);
#define List_1_set_Item_m1_19216(__this, ___index, ___value, method) (( void (*) (List_1_t1_1836 *, int32_t, Vector2_t6_47 , const MethodInfo*))List_1_set_Item_m1_19216_gshared)(__this, ___index, ___value, method)
