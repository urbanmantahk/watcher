﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Threading.ReaderWriterLock
struct ReaderWriterLock_t1_1468;

#include "mscorlib_System_Object.h"

// System.Threading.LockQueue
struct  LockQueue_t1_1467  : public Object_t
{
	// System.Threading.ReaderWriterLock System.Threading.LockQueue::rwlock
	ReaderWriterLock_t1_1468 * ___rwlock_0;
	// System.Int32 System.Threading.LockQueue::lockCount
	int32_t ___lockCount_1;
};
