﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.DataConverter/SwapConverter
struct SwapConverter_t1_254;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.DataConverter/SwapConverter::.ctor()
extern "C" void SwapConverter__ctor_m1_2740 (SwapConverter_t1_254 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double Mono.DataConverter/SwapConverter::GetDouble(System.Byte[],System.Int32)
extern "C" double SwapConverter_GetDouble_m1_2741 (SwapConverter_t1_254 * __this, ByteU5BU5D_t1_109* ___data, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 Mono.DataConverter/SwapConverter::GetUInt64(System.Byte[],System.Int32)
extern "C" uint64_t SwapConverter_GetUInt64_m1_2742 (SwapConverter_t1_254 * __this, ByteU5BU5D_t1_109* ___data, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Mono.DataConverter/SwapConverter::GetInt64(System.Byte[],System.Int32)
extern "C" int64_t SwapConverter_GetInt64_m1_2743 (SwapConverter_t1_254 * __this, ByteU5BU5D_t1_109* ___data, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Mono.DataConverter/SwapConverter::GetFloat(System.Byte[],System.Int32)
extern "C" float SwapConverter_GetFloat_m1_2744 (SwapConverter_t1_254 * __this, ByteU5BU5D_t1_109* ___data, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.DataConverter/SwapConverter::GetInt32(System.Byte[],System.Int32)
extern "C" int32_t SwapConverter_GetInt32_m1_2745 (SwapConverter_t1_254 * __this, ByteU5BU5D_t1_109* ___data, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Mono.DataConverter/SwapConverter::GetUInt32(System.Byte[],System.Int32)
extern "C" uint32_t SwapConverter_GetUInt32_m1_2746 (SwapConverter_t1_254 * __this, ByteU5BU5D_t1_109* ___data, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 Mono.DataConverter/SwapConverter::GetInt16(System.Byte[],System.Int32)
extern "C" int16_t SwapConverter_GetInt16_m1_2747 (SwapConverter_t1_254 * __this, ByteU5BU5D_t1_109* ___data, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 Mono.DataConverter/SwapConverter::GetUInt16(System.Byte[],System.Int32)
extern "C" uint16_t SwapConverter_GetUInt16_m1_2748 (SwapConverter_t1_254 * __this, ByteU5BU5D_t1_109* ___data, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.DataConverter/SwapConverter::PutBytes(System.Byte[],System.Int32,System.Double)
extern "C" void SwapConverter_PutBytes_m1_2749 (SwapConverter_t1_254 * __this, ByteU5BU5D_t1_109* ___dest, int32_t ___destIdx, double ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.DataConverter/SwapConverter::PutBytes(System.Byte[],System.Int32,System.Single)
extern "C" void SwapConverter_PutBytes_m1_2750 (SwapConverter_t1_254 * __this, ByteU5BU5D_t1_109* ___dest, int32_t ___destIdx, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.DataConverter/SwapConverter::PutBytes(System.Byte[],System.Int32,System.Int32)
extern "C" void SwapConverter_PutBytes_m1_2751 (SwapConverter_t1_254 * __this, ByteU5BU5D_t1_109* ___dest, int32_t ___destIdx, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.DataConverter/SwapConverter::PutBytes(System.Byte[],System.Int32,System.UInt32)
extern "C" void SwapConverter_PutBytes_m1_2752 (SwapConverter_t1_254 * __this, ByteU5BU5D_t1_109* ___dest, int32_t ___destIdx, uint32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.DataConverter/SwapConverter::PutBytes(System.Byte[],System.Int32,System.Int64)
extern "C" void SwapConverter_PutBytes_m1_2753 (SwapConverter_t1_254 * __this, ByteU5BU5D_t1_109* ___dest, int32_t ___destIdx, int64_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.DataConverter/SwapConverter::PutBytes(System.Byte[],System.Int32,System.UInt64)
extern "C" void SwapConverter_PutBytes_m1_2754 (SwapConverter_t1_254 * __this, ByteU5BU5D_t1_109* ___dest, int32_t ___destIdx, uint64_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.DataConverter/SwapConverter::PutBytes(System.Byte[],System.Int32,System.Int16)
extern "C" void SwapConverter_PutBytes_m1_2755 (SwapConverter_t1_254 * __this, ByteU5BU5D_t1_109* ___dest, int32_t ___destIdx, int16_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.DataConverter/SwapConverter::PutBytes(System.Byte[],System.Int32,System.UInt16)
extern "C" void SwapConverter_PutBytes_m1_2756 (SwapConverter_t1_254 * __this, ByteU5BU5D_t1_109* ___dest, int32_t ___destIdx, uint16_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
