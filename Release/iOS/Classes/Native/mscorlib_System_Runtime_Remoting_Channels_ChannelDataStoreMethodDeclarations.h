﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Channels.ChannelDataStore
struct ChannelDataStore_t1_868;
// System.String[]
struct StringU5BU5D_t1_238;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Channels.ChannelDataStore::.ctor(System.String[])
extern "C" void ChannelDataStore__ctor_m1_8028 (ChannelDataStore_t1_868 * __this, StringU5BU5D_t1_238* ___channelURIs, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.Runtime.Remoting.Channels.ChannelDataStore::get_ChannelUris()
extern "C" StringU5BU5D_t1_238* ChannelDataStore_get_ChannelUris_m1_8029 (ChannelDataStore_t1_868 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Channels.ChannelDataStore::set_ChannelUris(System.String[])
extern "C" void ChannelDataStore_set_ChannelUris_m1_8030 (ChannelDataStore_t1_868 * __this, StringU5BU5D_t1_238* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Channels.ChannelDataStore::get_Item(System.Object)
extern "C" Object_t * ChannelDataStore_get_Item_m1_8031 (ChannelDataStore_t1_868 * __this, Object_t * ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Channels.ChannelDataStore::set_Item(System.Object,System.Object)
extern "C" void ChannelDataStore_set_Item_m1_8032 (ChannelDataStore_t1_868 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
