﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Channels.TransportHeaders
struct TransportHeaders_t1_890;
// System.Object
struct Object_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Channels.TransportHeaders::.ctor()
extern "C" void TransportHeaders__ctor_m1_8124 (TransportHeaders_t1_890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Channels.TransportHeaders::get_Item(System.Object)
extern "C" Object_t * TransportHeaders_get_Item_m1_8125 (TransportHeaders_t1_890 * __this, Object_t * ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Channels.TransportHeaders::set_Item(System.Object,System.Object)
extern "C" void TransportHeaders_set_Item_m1_8126 (TransportHeaders_t1_890 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Runtime.Remoting.Channels.TransportHeaders::GetEnumerator()
extern "C" Object_t * TransportHeaders_GetEnumerator_m1_8127 (TransportHeaders_t1_890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
