﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.ObfuscationAttribute
struct ObfuscationAttribute_t1_625;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Reflection.ObfuscationAttribute::.ctor()
extern "C" void ObfuscationAttribute__ctor_m1_7212 (ObfuscationAttribute_t1_625 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.ObfuscationAttribute::get_Exclude()
extern "C" bool ObfuscationAttribute_get_Exclude_m1_7213 (ObfuscationAttribute_t1_625 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.ObfuscationAttribute::set_Exclude(System.Boolean)
extern "C" void ObfuscationAttribute_set_Exclude_m1_7214 (ObfuscationAttribute_t1_625 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.ObfuscationAttribute::get_StripAfterObfuscation()
extern "C" bool ObfuscationAttribute_get_StripAfterObfuscation_m1_7215 (ObfuscationAttribute_t1_625 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.ObfuscationAttribute::set_StripAfterObfuscation(System.Boolean)
extern "C" void ObfuscationAttribute_set_StripAfterObfuscation_m1_7216 (ObfuscationAttribute_t1_625 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.ObfuscationAttribute::get_ApplyToMembers()
extern "C" bool ObfuscationAttribute_get_ApplyToMembers_m1_7217 (ObfuscationAttribute_t1_625 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.ObfuscationAttribute::set_ApplyToMembers(System.Boolean)
extern "C" void ObfuscationAttribute_set_ApplyToMembers_m1_7218 (ObfuscationAttribute_t1_625 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.ObfuscationAttribute::get_Feature()
extern "C" String_t* ObfuscationAttribute_get_Feature_m1_7219 (ObfuscationAttribute_t1_625 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.ObfuscationAttribute::set_Feature(System.String)
extern "C" void ObfuscationAttribute_set_Feature_m1_7220 (ObfuscationAttribute_t1_625 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
