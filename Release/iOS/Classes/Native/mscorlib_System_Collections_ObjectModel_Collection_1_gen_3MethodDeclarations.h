﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>
struct Collection_1_t1_2271;
// System.Collections.Generic.IList`1<UnityEngine.Vector3>
struct IList_1_t1_2270;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Object
struct Object_t;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t6_161;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector3>
struct IEnumerator_1_t1_2795;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::.ctor()
extern "C" void Collection_1__ctor_m1_18885_gshared (Collection_1_t1_2271 * __this, const MethodInfo* method);
#define Collection_1__ctor_m1_18885(__this, method) (( void (*) (Collection_1_t1_2271 *, const MethodInfo*))Collection_1__ctor_m1_18885_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1__ctor_m1_18886_gshared (Collection_1_t1_2271 * __this, Object_t* ___list, const MethodInfo* method);
#define Collection_1__ctor_m1_18886(__this, ___list, method) (( void (*) (Collection_1_t1_2271 *, Object_t*, const MethodInfo*))Collection_1__ctor_m1_18886_gshared)(__this, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_18887_gshared (Collection_1_t1_2271 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_18887(__this, method) (( bool (*) (Collection_1_t1_2271 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_18887_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m1_18888_gshared (Collection_1_t1_2271 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m1_18888(__this, ___array, ___index, method) (( void (*) (Collection_1_t1_2271 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m1_18888_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m1_18889_gshared (Collection_1_t1_2271 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m1_18889(__this, method) (( Object_t * (*) (Collection_1_t1_2271 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m1_18889_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m1_18890_gshared (Collection_1_t1_2271 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m1_18890(__this, ___value, method) (( int32_t (*) (Collection_1_t1_2271 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m1_18890_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m1_18891_gshared (Collection_1_t1_2271 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m1_18891(__this, ___value, method) (( bool (*) (Collection_1_t1_2271 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m1_18891_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m1_18892_gshared (Collection_1_t1_2271 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m1_18892(__this, ___value, method) (( int32_t (*) (Collection_1_t1_2271 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m1_18892_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m1_18893_gshared (Collection_1_t1_2271 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m1_18893(__this, ___index, ___value, method) (( void (*) (Collection_1_t1_2271 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m1_18893_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m1_18894_gshared (Collection_1_t1_2271 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m1_18894(__this, ___value, method) (( void (*) (Collection_1_t1_2271 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m1_18894_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m1_18895_gshared (Collection_1_t1_2271 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m1_18895(__this, method) (( bool (*) (Collection_1_t1_2271 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m1_18895_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m1_18896_gshared (Collection_1_t1_2271 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m1_18896(__this, method) (( Object_t * (*) (Collection_1_t1_2271 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m1_18896_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m1_18897_gshared (Collection_1_t1_2271 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m1_18897(__this, method) (( bool (*) (Collection_1_t1_2271 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m1_18897_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m1_18898_gshared (Collection_1_t1_2271 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m1_18898(__this, method) (( bool (*) (Collection_1_t1_2271 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m1_18898_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m1_18899_gshared (Collection_1_t1_2271 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m1_18899(__this, ___index, method) (( Object_t * (*) (Collection_1_t1_2271 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m1_18899_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m1_18900_gshared (Collection_1_t1_2271 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m1_18900(__this, ___index, ___value, method) (( void (*) (Collection_1_t1_2271 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m1_18900_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::Add(T)
extern "C" void Collection_1_Add_m1_18901_gshared (Collection_1_t1_2271 * __this, Vector3_t6_48  ___item, const MethodInfo* method);
#define Collection_1_Add_m1_18901(__this, ___item, method) (( void (*) (Collection_1_t1_2271 *, Vector3_t6_48 , const MethodInfo*))Collection_1_Add_m1_18901_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::Clear()
extern "C" void Collection_1_Clear_m1_18902_gshared (Collection_1_t1_2271 * __this, const MethodInfo* method);
#define Collection_1_Clear_m1_18902(__this, method) (( void (*) (Collection_1_t1_2271 *, const MethodInfo*))Collection_1_Clear_m1_18902_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::ClearItems()
extern "C" void Collection_1_ClearItems_m1_18903_gshared (Collection_1_t1_2271 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m1_18903(__this, method) (( void (*) (Collection_1_t1_2271 *, const MethodInfo*))Collection_1_ClearItems_m1_18903_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::Contains(T)
extern "C" bool Collection_1_Contains_m1_18904_gshared (Collection_1_t1_2271 * __this, Vector3_t6_48  ___item, const MethodInfo* method);
#define Collection_1_Contains_m1_18904(__this, ___item, method) (( bool (*) (Collection_1_t1_2271 *, Vector3_t6_48 , const MethodInfo*))Collection_1_Contains_m1_18904_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m1_18905_gshared (Collection_1_t1_2271 * __this, Vector3U5BU5D_t6_161* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m1_18905(__this, ___array, ___index, method) (( void (*) (Collection_1_t1_2271 *, Vector3U5BU5D_t6_161*, int32_t, const MethodInfo*))Collection_1_CopyTo_m1_18905_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m1_18906_gshared (Collection_1_t1_2271 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m1_18906(__this, method) (( Object_t* (*) (Collection_1_t1_2271 *, const MethodInfo*))Collection_1_GetEnumerator_m1_18906_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m1_18907_gshared (Collection_1_t1_2271 * __this, Vector3_t6_48  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m1_18907(__this, ___item, method) (( int32_t (*) (Collection_1_t1_2271 *, Vector3_t6_48 , const MethodInfo*))Collection_1_IndexOf_m1_18907_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m1_18908_gshared (Collection_1_t1_2271 * __this, int32_t ___index, Vector3_t6_48  ___item, const MethodInfo* method);
#define Collection_1_Insert_m1_18908(__this, ___index, ___item, method) (( void (*) (Collection_1_t1_2271 *, int32_t, Vector3_t6_48 , const MethodInfo*))Collection_1_Insert_m1_18908_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m1_18909_gshared (Collection_1_t1_2271 * __this, int32_t ___index, Vector3_t6_48  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m1_18909(__this, ___index, ___item, method) (( void (*) (Collection_1_t1_2271 *, int32_t, Vector3_t6_48 , const MethodInfo*))Collection_1_InsertItem_m1_18909_gshared)(__this, ___index, ___item, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::get_Items()
extern "C" Object_t* Collection_1_get_Items_m1_18910_gshared (Collection_1_t1_2271 * __this, const MethodInfo* method);
#define Collection_1_get_Items_m1_18910(__this, method) (( Object_t* (*) (Collection_1_t1_2271 *, const MethodInfo*))Collection_1_get_Items_m1_18910_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::Remove(T)
extern "C" bool Collection_1_Remove_m1_18911_gshared (Collection_1_t1_2271 * __this, Vector3_t6_48  ___item, const MethodInfo* method);
#define Collection_1_Remove_m1_18911(__this, ___item, method) (( bool (*) (Collection_1_t1_2271 *, Vector3_t6_48 , const MethodInfo*))Collection_1_Remove_m1_18911_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m1_18912_gshared (Collection_1_t1_2271 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m1_18912(__this, ___index, method) (( void (*) (Collection_1_t1_2271 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m1_18912_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m1_18913_gshared (Collection_1_t1_2271 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m1_18913(__this, ___index, method) (( void (*) (Collection_1_t1_2271 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m1_18913_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::get_Count()
extern "C" int32_t Collection_1_get_Count_m1_18914_gshared (Collection_1_t1_2271 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m1_18914(__this, method) (( int32_t (*) (Collection_1_t1_2271 *, const MethodInfo*))Collection_1_get_Count_m1_18914_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::get_Item(System.Int32)
extern "C" Vector3_t6_48  Collection_1_get_Item_m1_18915_gshared (Collection_1_t1_2271 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m1_18915(__this, ___index, method) (( Vector3_t6_48  (*) (Collection_1_t1_2271 *, int32_t, const MethodInfo*))Collection_1_get_Item_m1_18915_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m1_18916_gshared (Collection_1_t1_2271 * __this, int32_t ___index, Vector3_t6_48  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m1_18916(__this, ___index, ___value, method) (( void (*) (Collection_1_t1_2271 *, int32_t, Vector3_t6_48 , const MethodInfo*))Collection_1_set_Item_m1_18916_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m1_18917_gshared (Collection_1_t1_2271 * __this, int32_t ___index, Vector3_t6_48  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m1_18917(__this, ___index, ___item, method) (( void (*) (Collection_1_t1_2271 *, int32_t, Vector3_t6_48 , const MethodInfo*))Collection_1_SetItem_m1_18917_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m1_18918_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m1_18918(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m1_18918_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::ConvertItem(System.Object)
extern "C" Vector3_t6_48  Collection_1_ConvertItem_m1_18919_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m1_18919(__this /* static, unused */, ___item, method) (( Vector3_t6_48  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m1_18919_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m1_18920_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m1_18920(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m1_18920_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m1_18921_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m1_18921(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m1_18921_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m1_18922_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m1_18922(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m1_18922_gshared)(__this /* static, unused */, ___list, method)
