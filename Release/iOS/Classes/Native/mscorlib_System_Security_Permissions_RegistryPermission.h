﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.ArrayList
struct ArrayList_t1_170;

#include "mscorlib_System_Security_CodeAccessPermission.h"
#include "mscorlib_System_Security_Permissions_PermissionState.h"

// System.Security.Permissions.RegistryPermission
struct  RegistryPermission_t1_1304  : public CodeAccessPermission_t1_1268
{
	// System.Security.Permissions.PermissionState System.Security.Permissions.RegistryPermission::_state
	int32_t ____state_1;
	// System.Collections.ArrayList System.Security.Permissions.RegistryPermission::createList
	ArrayList_t1_170 * ___createList_2;
	// System.Collections.ArrayList System.Security.Permissions.RegistryPermission::readList
	ArrayList_t1_170 * ___readList_3;
	// System.Collections.ArrayList System.Security.Permissions.RegistryPermission::writeList
	ArrayList_t1_170 * ___writeList_4;
};
