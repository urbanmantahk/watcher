﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapInteger
struct SoapInteger_t1_978;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Decimal.h"

// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapInteger::.ctor()
extern "C" void SoapInteger__ctor_m1_8766 (SoapInteger_t1_978 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapInteger::.ctor(System.Decimal)
extern "C" void SoapInteger__ctor_m1_8767 (SoapInteger_t1_978 * __this, Decimal_t1_19  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Runtime.Remoting.Metadata.W3cXsd2001.SoapInteger::get_Value()
extern "C" Decimal_t1_19  SoapInteger_get_Value_m1_8768 (SoapInteger_t1_978 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapInteger::set_Value(System.Decimal)
extern "C" void SoapInteger_set_Value_m1_8769 (SoapInteger_t1_978 * __this, Decimal_t1_19  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapInteger::get_XsdType()
extern "C" String_t* SoapInteger_get_XsdType_m1_8770 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapInteger::GetXsdType()
extern "C" String_t* SoapInteger_GetXsdType_m1_8771 (SoapInteger_t1_978 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapInteger System.Runtime.Remoting.Metadata.W3cXsd2001.SoapInteger::Parse(System.String)
extern "C" SoapInteger_t1_978 * SoapInteger_Parse_m1_8772 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapInteger::ToString()
extern "C" String_t* SoapInteger_ToString_m1_8773 (SoapInteger_t1_978 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
