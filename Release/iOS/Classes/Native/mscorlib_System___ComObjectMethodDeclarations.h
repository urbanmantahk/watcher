﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.__ComObject
struct __ComObject_t1_131;
// System.Type
struct Type_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_Guid.h"

// System.Void System.__ComObject::.ctor()
extern "C" void __ComObject__ctor_m1_14760 (__ComObject_t1_131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.__ComObject::.ctor(System.Type)
extern "C" void __ComObject__ctor_m1_14761 (__ComObject_t1_131 * __this, Type_t * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.__ComObject::.ctor(System.IntPtr)
extern "C" void __ComObject__ctor_m1_14762 (__ComObject_t1_131 * __this, IntPtr_t ___pItf, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.__ComObject System.__ComObject::CreateRCW(System.Type)
extern "C" __ComObject_t1_131 * __ComObject_CreateRCW_m1_14763 (Object_t * __this /* static, unused */, Type_t * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.__ComObject::ReleaseInterfaces()
extern "C" void __ComObject_ReleaseInterfaces_m1_14764 (__ComObject_t1_131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.__ComObject::Finalize()
extern "C" void __ComObject_Finalize_m1_14765 (__ComObject_t1_131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.__ComObject::Initialize(System.Type)
extern "C" void __ComObject_Initialize_m1_14766 (__ComObject_t1_131 * __this, Type_t * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Guid System.__ComObject::GetCLSID(System.Type)
extern "C" Guid_t1_319  __ComObject_GetCLSID_m1_14767 (Object_t * __this /* static, unused */, Type_t * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.__ComObject::GetInterfaceInternal(System.Type,System.Boolean)
extern "C" IntPtr_t __ComObject_GetInterfaceInternal_m1_14768 (__ComObject_t1_131 * __this, Type_t * ___t, bool ___throwException, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.__ComObject::GetInterface(System.Type,System.Boolean)
extern "C" IntPtr_t __ComObject_GetInterface_m1_14769 (__ComObject_t1_131 * __this, Type_t * ___t, bool ___throwException, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.__ComObject::GetInterface(System.Type)
extern "C" IntPtr_t __ComObject_GetInterface_m1_14770 (__ComObject_t1_131 * __this, Type_t * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.__ComObject::CheckIUnknown()
extern "C" void __ComObject_CheckIUnknown_m1_14771 (__ComObject_t1_131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.__ComObject::get_IUnknown()
extern "C" IntPtr_t __ComObject_get_IUnknown_m1_14772 (__ComObject_t1_131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.__ComObject::get_IDispatch()
extern "C" IntPtr_t __ComObject_get_IDispatch_m1_14773 (__ComObject_t1_131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Guid System.__ComObject::get_IID_IUnknown()
extern "C" Guid_t1_319  __ComObject_get_IID_IUnknown_m1_14774 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Guid System.__ComObject::get_IID_IDispatch()
extern "C" Guid_t1_319  __ComObject_get_IID_IDispatch_m1_14775 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.__ComObject::Equals(System.Object)
extern "C" bool __ComObject_Equals_m1_14776 (__ComObject_t1_131 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.__ComObject::GetHashCode()
extern "C" int32_t __ComObject_GetHashCode_m1_14777 (__ComObject_t1_131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.__ComObject::CoCreateInstance(System.Guid,System.IntPtr,System.UInt32,System.Guid,System.IntPtr&)
extern "C" int32_t __ComObject_CoCreateInstance_m1_14778 (Object_t * __this /* static, unused */, Guid_t1_319  ___rclsid, IntPtr_t ___pUnkOuter, uint32_t ___dwClsContext, Guid_t1_319  ___riid, IntPtr_t* ___pUnk, const MethodInfo* method) IL2CPP_METHOD_ATTR;
