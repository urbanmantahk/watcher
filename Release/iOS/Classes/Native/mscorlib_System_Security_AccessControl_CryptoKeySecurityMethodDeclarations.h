﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.AccessControl.CryptoKeySecurity
struct CryptoKeySecurity_t1_1142;
// System.Security.AccessControl.CommonSecurityDescriptor
struct CommonSecurityDescriptor_t1_1130;
// System.Type
struct Type_t;
// System.Security.AccessControl.AccessRule
struct AccessRule_t1_1111;
// System.Security.Principal.IdentityReference
struct IdentityReference_t1_1120;
// System.Security.AccessControl.CryptoKeyAccessRule
struct CryptoKeyAccessRule_t1_1139;
// System.Security.AccessControl.AuditRule
struct AuditRule_t1_1119;
// System.Security.AccessControl.CryptoKeyAuditRule
struct CryptoKeyAuditRule_t1_1140;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_AccessControl_InheritanceFlags.h"
#include "mscorlib_System_Security_AccessControl_PropagationFlags.h"
#include "mscorlib_System_Security_AccessControl_AccessControlType.h"
#include "mscorlib_System_Security_AccessControl_AuditFlags.h"

// System.Void System.Security.AccessControl.CryptoKeySecurity::.ctor()
extern "C" void CryptoKeySecurity__ctor_m1_9790 (CryptoKeySecurity_t1_1142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.CryptoKeySecurity::.ctor(System.Security.AccessControl.CommonSecurityDescriptor)
extern "C" void CryptoKeySecurity__ctor_m1_9791 (CryptoKeySecurity_t1_1142 * __this, CommonSecurityDescriptor_t1_1130 * ___securityDescriptor, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Security.AccessControl.CryptoKeySecurity::get_AccessRightType()
extern "C" Type_t * CryptoKeySecurity_get_AccessRightType_m1_9792 (CryptoKeySecurity_t1_1142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Security.AccessControl.CryptoKeySecurity::get_AccessRuleType()
extern "C" Type_t * CryptoKeySecurity_get_AccessRuleType_m1_9793 (CryptoKeySecurity_t1_1142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Security.AccessControl.CryptoKeySecurity::get_AuditRuleType()
extern "C" Type_t * CryptoKeySecurity_get_AuditRuleType_m1_9794 (CryptoKeySecurity_t1_1142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.AccessControl.AccessRule System.Security.AccessControl.CryptoKeySecurity::AccessRuleFactory(System.Security.Principal.IdentityReference,System.Int32,System.Boolean,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.AccessControlType)
extern "C" AccessRule_t1_1111 * CryptoKeySecurity_AccessRuleFactory_m1_9795 (CryptoKeySecurity_t1_1142 * __this, IdentityReference_t1_1120 * ___identityReference, int32_t ___accessMask, bool ___isInherited, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.CryptoKeySecurity::AddAccessRule(System.Security.AccessControl.CryptoKeyAccessRule)
extern "C" void CryptoKeySecurity_AddAccessRule_m1_9796 (CryptoKeySecurity_t1_1142 * __this, CryptoKeyAccessRule_t1_1139 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.AccessControl.CryptoKeySecurity::RemoveAccessRule(System.Security.AccessControl.CryptoKeyAccessRule)
extern "C" bool CryptoKeySecurity_RemoveAccessRule_m1_9797 (CryptoKeySecurity_t1_1142 * __this, CryptoKeyAccessRule_t1_1139 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.CryptoKeySecurity::RemoveAccessRuleAll(System.Security.AccessControl.CryptoKeyAccessRule)
extern "C" void CryptoKeySecurity_RemoveAccessRuleAll_m1_9798 (CryptoKeySecurity_t1_1142 * __this, CryptoKeyAccessRule_t1_1139 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.CryptoKeySecurity::RemoveAccessRuleSpecific(System.Security.AccessControl.CryptoKeyAccessRule)
extern "C" void CryptoKeySecurity_RemoveAccessRuleSpecific_m1_9799 (CryptoKeySecurity_t1_1142 * __this, CryptoKeyAccessRule_t1_1139 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.CryptoKeySecurity::ResetAccessRule(System.Security.AccessControl.CryptoKeyAccessRule)
extern "C" void CryptoKeySecurity_ResetAccessRule_m1_9800 (CryptoKeySecurity_t1_1142 * __this, CryptoKeyAccessRule_t1_1139 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.CryptoKeySecurity::SetAccessRule(System.Security.AccessControl.CryptoKeyAccessRule)
extern "C" void CryptoKeySecurity_SetAccessRule_m1_9801 (CryptoKeySecurity_t1_1142 * __this, CryptoKeyAccessRule_t1_1139 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.AccessControl.AuditRule System.Security.AccessControl.CryptoKeySecurity::AuditRuleFactory(System.Security.Principal.IdentityReference,System.Int32,System.Boolean,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.AuditFlags)
extern "C" AuditRule_t1_1119 * CryptoKeySecurity_AuditRuleFactory_m1_9802 (CryptoKeySecurity_t1_1142 * __this, IdentityReference_t1_1120 * ___identityReference, int32_t ___accessMask, bool ___isInherited, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___flags, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.CryptoKeySecurity::AddAuditRule(System.Security.AccessControl.CryptoKeyAuditRule)
extern "C" void CryptoKeySecurity_AddAuditRule_m1_9803 (CryptoKeySecurity_t1_1142 * __this, CryptoKeyAuditRule_t1_1140 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.AccessControl.CryptoKeySecurity::RemoveAuditRule(System.Security.AccessControl.CryptoKeyAuditRule)
extern "C" bool CryptoKeySecurity_RemoveAuditRule_m1_9804 (CryptoKeySecurity_t1_1142 * __this, CryptoKeyAuditRule_t1_1140 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.CryptoKeySecurity::RemoveAuditRuleAll(System.Security.AccessControl.CryptoKeyAuditRule)
extern "C" void CryptoKeySecurity_RemoveAuditRuleAll_m1_9805 (CryptoKeySecurity_t1_1142 * __this, CryptoKeyAuditRule_t1_1140 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.CryptoKeySecurity::RemoveAuditRuleSpecific(System.Security.AccessControl.CryptoKeyAuditRule)
extern "C" void CryptoKeySecurity_RemoveAuditRuleSpecific_m1_9806 (CryptoKeySecurity_t1_1142 * __this, CryptoKeyAuditRule_t1_1140 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.CryptoKeySecurity::SetAuditRule(System.Security.AccessControl.CryptoKeyAuditRule)
extern "C" void CryptoKeySecurity_SetAuditRule_m1_9807 (CryptoKeySecurity_t1_1142 * __this, CryptoKeyAuditRule_t1_1140 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
