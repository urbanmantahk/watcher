﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_ConsoleKey.h"

// System.ConsoleKey
struct  ConsoleKey_t1_1515 
{
	// System.Int32 System.ConsoleKey::value__
	int32_t ___value___1;
};
