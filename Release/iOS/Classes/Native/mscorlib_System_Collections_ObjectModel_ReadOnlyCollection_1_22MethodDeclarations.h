﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>
struct ReadOnlyCollection_1_t1_2350;
// System.Collections.Generic.IList`1<UnityEngine.UIVertex>
struct IList_1_t1_1841;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Object
struct Object_t;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t6_287;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UIVertex>
struct IEnumerator_1_t1_2815;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_UIVertex.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C" void ReadOnlyCollection_1__ctor_m1_20331_gshared (ReadOnlyCollection_1_t1_2350 * __this, Object_t* ___list, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m1_20331(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t1_2350 *, Object_t*, const MethodInfo*))ReadOnlyCollection_1__ctor_m1_20331_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1_20332_gshared (ReadOnlyCollection_1_t1_2350 * __this, UIVertex_t6_158  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1_20332(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t1_2350 *, UIVertex_t6_158 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1_20332_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::System.Collections.Generic.ICollection<T>.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1_20333_gshared (ReadOnlyCollection_1_t1_2350 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1_20333(__this, method) (( void (*) (ReadOnlyCollection_1_t1_2350 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1_20333_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1_20334_gshared (ReadOnlyCollection_1_t1_2350 * __this, int32_t ___index, UIVertex_t6_158  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1_20334(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t1_2350 *, int32_t, UIVertex_t6_158 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1_20334_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1_20335_gshared (ReadOnlyCollection_1_t1_2350 * __this, UIVertex_t6_158  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1_20335(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t1_2350 *, UIVertex_t6_158 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1_20335_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1_20336_gshared (ReadOnlyCollection_1_t1_2350 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1_20336(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t1_2350 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1_20336_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C" UIVertex_t6_158  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1_20337_gshared (ReadOnlyCollection_1_t1_2350 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1_20337(__this, ___index, method) (( UIVertex_t6_158  (*) (ReadOnlyCollection_1_t1_2350 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1_20337_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1_20338_gshared (ReadOnlyCollection_1_t1_2350 * __this, int32_t ___index, UIVertex_t6_158  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1_20338(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t1_2350 *, int32_t, UIVertex_t6_158 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1_20338_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_20339_gshared (ReadOnlyCollection_1_t1_2350 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_20339(__this, method) (( bool (*) (ReadOnlyCollection_1_t1_2350 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_20339_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1_20340_gshared (ReadOnlyCollection_1_t1_2350 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1_20340(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t1_2350 *, Array_t *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1_20340_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1_20341_gshared (ReadOnlyCollection_1_t1_2350 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1_20341(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t1_2350 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1_20341_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::System.Collections.IList.Add(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m1_20342_gshared (ReadOnlyCollection_1_t1_2350 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m1_20342(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t1_2350 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m1_20342_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::System.Collections.IList.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m1_20343_gshared (ReadOnlyCollection_1_t1_2350 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m1_20343(__this, method) (( void (*) (ReadOnlyCollection_1_t1_2350 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m1_20343_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::System.Collections.IList.Contains(System.Object)
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_Contains_m1_20344_gshared (ReadOnlyCollection_1_t1_2350 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m1_20344(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t1_2350 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m1_20344_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1_20345_gshared (ReadOnlyCollection_1_t1_2350 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1_20345(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t1_2350 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1_20345_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m1_20346_gshared (ReadOnlyCollection_1_t1_2350 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m1_20346(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t1_2350 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m1_20346_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::System.Collections.IList.Remove(System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m1_20347_gshared (ReadOnlyCollection_1_t1_2350 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m1_20347(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t1_2350 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m1_20347_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::System.Collections.IList.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1_20348_gshared (ReadOnlyCollection_1_t1_2350 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1_20348(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t1_2350 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1_20348_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1_20349_gshared (ReadOnlyCollection_1_t1_2350 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1_20349(__this, method) (( bool (*) (ReadOnlyCollection_1_t1_2350 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1_20349_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1_20350_gshared (ReadOnlyCollection_1_t1_2350 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1_20350(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t1_2350 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1_20350_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::System.Collections.IList.get_IsFixedSize()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1_20351_gshared (ReadOnlyCollection_1_t1_2350 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1_20351(__this, method) (( bool (*) (ReadOnlyCollection_1_t1_2350 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1_20351_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::System.Collections.IList.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1_20352_gshared (ReadOnlyCollection_1_t1_2350 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1_20352(__this, method) (( bool (*) (ReadOnlyCollection_1_t1_2350 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1_20352_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IList_get_Item_m1_20353_gshared (ReadOnlyCollection_1_t1_2350 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m1_20353(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t1_2350 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m1_20353_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m1_20354_gshared (ReadOnlyCollection_1_t1_2350 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m1_20354(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t1_2350 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m1_20354_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::Contains(T)
extern "C" bool ReadOnlyCollection_1_Contains_m1_20355_gshared (ReadOnlyCollection_1_t1_2350 * __this, UIVertex_t6_158  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m1_20355(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t1_2350 *, UIVertex_t6_158 , const MethodInfo*))ReadOnlyCollection_1_Contains_m1_20355_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::CopyTo(T[],System.Int32)
extern "C" void ReadOnlyCollection_1_CopyTo_m1_20356_gshared (ReadOnlyCollection_1_t1_2350 * __this, UIVertexU5BU5D_t6_287* ___array, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m1_20356(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t1_2350 *, UIVertexU5BU5D_t6_287*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m1_20356_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::GetEnumerator()
extern "C" Object_t* ReadOnlyCollection_1_GetEnumerator_m1_20357_gshared (ReadOnlyCollection_1_t1_2350 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m1_20357(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t1_2350 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m1_20357_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::IndexOf(T)
extern "C" int32_t ReadOnlyCollection_1_IndexOf_m1_20358_gshared (ReadOnlyCollection_1_t1_2350 * __this, UIVertex_t6_158  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m1_20358(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t1_2350 *, UIVertex_t6_158 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m1_20358_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::get_Count()
extern "C" int32_t ReadOnlyCollection_1_get_Count_m1_20359_gshared (ReadOnlyCollection_1_t1_2350 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m1_20359(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t1_2350 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m1_20359_gshared)(__this, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::get_Items()
extern "C" Object_t* ReadOnlyCollection_1_get_Items_m1_20360_gshared (ReadOnlyCollection_1_t1_2350 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Items_m1_20360(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t1_2350 *, const MethodInfo*))ReadOnlyCollection_1_get_Items_m1_20360_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>::get_Item(System.Int32)
extern "C" UIVertex_t6_158  ReadOnlyCollection_1_get_Item_m1_20361_gshared (ReadOnlyCollection_1_t1_2350 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m1_20361(__this, ___index, method) (( UIVertex_t6_158  (*) (ReadOnlyCollection_1_t1_2350 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m1_20361_gshared)(__this, ___index, method)
