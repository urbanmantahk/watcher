﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExifLibrary.ExifEnumProperty`1<ExifLibrary.FileSource>
struct ExifEnumProperty_1_t8_350;
// System.Object
struct Object_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTag.h"
#include "AssemblyU2DCSharp_ExifLibrary_FileSource.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifInterOperability.h"

// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.FileSource>::.ctor(ExifLibrary.ExifTag,T,System.Boolean)
extern "C" void ExifEnumProperty_1__ctor_m8_1987_gshared (ExifEnumProperty_1_t8_350 * __this, int32_t ___tag, uint8_t ___value, bool ___isbitfield, const MethodInfo* method);
#define ExifEnumProperty_1__ctor_m8_1987(__this, ___tag, ___value, ___isbitfield, method) (( void (*) (ExifEnumProperty_1_t8_350 *, int32_t, uint8_t, bool, const MethodInfo*))ExifEnumProperty_1__ctor_m8_1987_gshared)(__this, ___tag, ___value, ___isbitfield, method)
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.FileSource>::.ctor(ExifLibrary.ExifTag,T)
extern "C" void ExifEnumProperty_1__ctor_m8_2180_gshared (ExifEnumProperty_1_t8_350 * __this, int32_t ___tag, uint8_t ___value, const MethodInfo* method);
#define ExifEnumProperty_1__ctor_m8_2180(__this, ___tag, ___value, method) (( void (*) (ExifEnumProperty_1_t8_350 *, int32_t, uint8_t, const MethodInfo*))ExifEnumProperty_1__ctor_m8_2180_gshared)(__this, ___tag, ___value, method)
// System.Object ExifLibrary.ExifEnumProperty`1<ExifLibrary.FileSource>::get__Value()
extern "C" Object_t * ExifEnumProperty_1_get__Value_m8_2181_gshared (ExifEnumProperty_1_t8_350 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get__Value_m8_2181(__this, method) (( Object_t * (*) (ExifEnumProperty_1_t8_350 *, const MethodInfo*))ExifEnumProperty_1_get__Value_m8_2181_gshared)(__this, method)
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.FileSource>::set__Value(System.Object)
extern "C" void ExifEnumProperty_1_set__Value_m8_2182_gshared (ExifEnumProperty_1_t8_350 * __this, Object_t * ___value, const MethodInfo* method);
#define ExifEnumProperty_1_set__Value_m8_2182(__this, ___value, method) (( void (*) (ExifEnumProperty_1_t8_350 *, Object_t *, const MethodInfo*))ExifEnumProperty_1_set__Value_m8_2182_gshared)(__this, ___value, method)
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.FileSource>::get_Value()
extern "C" uint8_t ExifEnumProperty_1_get_Value_m8_2183_gshared (ExifEnumProperty_1_t8_350 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get_Value_m8_2183(__this, method) (( uint8_t (*) (ExifEnumProperty_1_t8_350 *, const MethodInfo*))ExifEnumProperty_1_get_Value_m8_2183_gshared)(__this, method)
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.FileSource>::set_Value(T)
extern "C" void ExifEnumProperty_1_set_Value_m8_2184_gshared (ExifEnumProperty_1_t8_350 * __this, uint8_t ___value, const MethodInfo* method);
#define ExifEnumProperty_1_set_Value_m8_2184(__this, ___value, method) (( void (*) (ExifEnumProperty_1_t8_350 *, uint8_t, const MethodInfo*))ExifEnumProperty_1_set_Value_m8_2184_gshared)(__this, ___value, method)
// System.Boolean ExifLibrary.ExifEnumProperty`1<ExifLibrary.FileSource>::get_IsBitField()
extern "C" bool ExifEnumProperty_1_get_IsBitField_m8_2185_gshared (ExifEnumProperty_1_t8_350 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get_IsBitField_m8_2185(__this, method) (( bool (*) (ExifEnumProperty_1_t8_350 *, const MethodInfo*))ExifEnumProperty_1_get_IsBitField_m8_2185_gshared)(__this, method)
// System.String ExifLibrary.ExifEnumProperty`1<ExifLibrary.FileSource>::ToString()
extern "C" String_t* ExifEnumProperty_1_ToString_m8_2186_gshared (ExifEnumProperty_1_t8_350 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_ToString_m8_2186(__this, method) (( String_t* (*) (ExifEnumProperty_1_t8_350 *, const MethodInfo*))ExifEnumProperty_1_ToString_m8_2186_gshared)(__this, method)
// ExifLibrary.ExifInterOperability ExifLibrary.ExifEnumProperty`1<ExifLibrary.FileSource>::get_Interoperability()
extern "C" ExifInterOperability_t8_113  ExifEnumProperty_1_get_Interoperability_m8_2187_gshared (ExifEnumProperty_1_t8_350 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get_Interoperability_m8_2187(__this, method) (( ExifInterOperability_t8_113  (*) (ExifEnumProperty_1_t8_350 *, const MethodInfo*))ExifEnumProperty_1_get_Interoperability_m8_2187_gshared)(__this, method)
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.FileSource>::op_Implicit(ExifLibrary.ExifEnumProperty`1<T>)
extern "C" uint8_t ExifEnumProperty_1_op_Implicit_m8_2188_gshared (Object_t * __this /* static, unused */, ExifEnumProperty_1_t8_350 * ___obj, const MethodInfo* method);
#define ExifEnumProperty_1_op_Implicit_m8_2188(__this /* static, unused */, ___obj, method) (( uint8_t (*) (Object_t * /* static, unused */, ExifEnumProperty_1_t8_350 *, const MethodInfo*))ExifEnumProperty_1_op_Implicit_m8_2188_gshared)(__this /* static, unused */, ___obj, method)
