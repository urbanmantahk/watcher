﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Xml.Schema.XdtUntypedAtomic
struct XdtUntypedAtomic_t4_6;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Xml.Schema.XdtUntypedAtomic::.ctor()
extern "C" void XdtUntypedAtomic__ctor_m4_6 (XdtUntypedAtomic_t4_6 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
