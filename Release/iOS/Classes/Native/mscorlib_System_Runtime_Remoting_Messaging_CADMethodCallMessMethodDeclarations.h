﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Messaging.CADMethodCallMessage
struct CADMethodCallMessage_t1_925;
// System.Runtime.Remoting.Messaging.IMethodCallMessage
struct IMethodCallMessage_t1_949;
// System.String
struct String_t;
// System.Runtime.Remoting.Messaging.IMessage
struct IMessage_t1_875;
// System.Collections.ArrayList
struct ArrayList_t1_170;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Type[]
struct TypeU5BU5D_t1_31;
// System.Reflection.MethodBase
struct MethodBase_t1_335;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Messaging.CADMethodCallMessage::.ctor(System.Runtime.Remoting.Messaging.IMethodCallMessage)
extern "C" void CADMethodCallMessage__ctor_m1_8307 (CADMethodCallMessage_t1_925 * __this, Object_t * ___callMsg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Messaging.CADMethodCallMessage::get_Uri()
extern "C" String_t* CADMethodCallMessage_get_Uri_m1_8308 (CADMethodCallMessage_t1_925 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Messaging.CADMethodCallMessage System.Runtime.Remoting.Messaging.CADMethodCallMessage::Create(System.Runtime.Remoting.Messaging.IMessage)
extern "C" CADMethodCallMessage_t1_925 * CADMethodCallMessage_Create_m1_8309 (Object_t * __this /* static, unused */, Object_t * ___callMsg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList System.Runtime.Remoting.Messaging.CADMethodCallMessage::GetArguments()
extern "C" ArrayList_t1_170 * CADMethodCallMessage_GetArguments_m1_8310 (CADMethodCallMessage_t1_925 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Runtime.Remoting.Messaging.CADMethodCallMessage::GetArgs(System.Collections.ArrayList)
extern "C" ObjectU5BU5D_t1_272* CADMethodCallMessage_GetArgs_m1_8311 (CADMethodCallMessage_t1_925 * __this, ArrayList_t1_170 * ___args, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.Remoting.Messaging.CADMethodCallMessage::get_PropertiesCount()
extern "C" int32_t CADMethodCallMessage_get_PropertiesCount_m1_8312 (CADMethodCallMessage_t1_925 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type[] System.Runtime.Remoting.Messaging.CADMethodCallMessage::GetSignature(System.Reflection.MethodBase,System.Boolean)
extern "C" TypeU5BU5D_t1_31* CADMethodCallMessage_GetSignature_m1_8313 (Object_t * __this /* static, unused */, MethodBase_t1_335 * ___methodBase, bool ___load, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodBase System.Runtime.Remoting.Messaging.CADMethodCallMessage::GetMethod()
extern "C" MethodBase_t1_335 * CADMethodCallMessage_GetMethod_m1_8314 (CADMethodCallMessage_t1_925 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
