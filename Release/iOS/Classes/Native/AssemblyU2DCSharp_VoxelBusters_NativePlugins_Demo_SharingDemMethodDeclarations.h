﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.Demo.SharingDemo
struct SharingDemo_t8_185;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.Demo.SharingDemo::.ctor()
extern "C" void SharingDemo__ctor_m8_1092 (SharingDemo_t8_185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
