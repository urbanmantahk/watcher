﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.IO.Stream
struct Stream_t1_405;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "mscorlib_System_IO_Stream.h"

// System.IO.BufferedStream
struct  BufferedStream_t1_409  : public Stream_t1_405
{
	// System.IO.Stream System.IO.BufferedStream::m_stream
	Stream_t1_405 * ___m_stream_1;
	// System.Byte[] System.IO.BufferedStream::m_buffer
	ByteU5BU5D_t1_109* ___m_buffer_2;
	// System.Int32 System.IO.BufferedStream::m_buffer_pos
	int32_t ___m_buffer_pos_3;
	// System.Int32 System.IO.BufferedStream::m_buffer_read_ahead
	int32_t ___m_buffer_read_ahead_4;
	// System.Boolean System.IO.BufferedStream::m_buffer_reading
	bool ___m_buffer_reading_5;
	// System.Boolean System.IO.BufferedStream::disposed
	bool ___disposed_6;
};
