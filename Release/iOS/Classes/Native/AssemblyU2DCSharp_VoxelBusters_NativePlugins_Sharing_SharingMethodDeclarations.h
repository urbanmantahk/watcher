﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.Sharing/SharingCompletion
struct SharingCompletion_t8_271;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eShareResult.h"

// System.Void VoxelBusters.NativePlugins.Sharing/SharingCompletion::.ctor(System.Object,System.IntPtr)
extern "C" void SharingCompletion__ctor_m8_1568 (SharingCompletion_t8_271 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Sharing/SharingCompletion::Invoke(VoxelBusters.NativePlugins.eShareResult)
extern "C" void SharingCompletion_Invoke_m8_1569 (SharingCompletion_t8_271 * __this, int32_t ____result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_SharingCompletion_t8_271(Il2CppObject* delegate, int32_t ____result);
// System.IAsyncResult VoxelBusters.NativePlugins.Sharing/SharingCompletion::BeginInvoke(VoxelBusters.NativePlugins.eShareResult,System.AsyncCallback,System.Object)
extern "C" Object_t * SharingCompletion_BeginInvoke_m8_1570 (SharingCompletion_t8_271 * __this, int32_t ____result, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Sharing/SharingCompletion::EndInvoke(System.IAsyncResult)
extern "C" void SharingCompletion_EndInvoke_m8_1571 (SharingCompletion_t8_271 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
