﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExifLibrary.ExifEnumProperty`1<ExifLibrary.MeteringMode>
struct ExifEnumProperty_1_t8_346;
// System.Object
struct Object_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTag.h"
#include "AssemblyU2DCSharp_ExifLibrary_MeteringMode.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifInterOperability.h"

// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.MeteringMode>::.ctor(ExifLibrary.ExifTag,T,System.Boolean)
extern "C" void ExifEnumProperty_1__ctor_m8_2144_gshared (ExifEnumProperty_1_t8_346 * __this, int32_t ___tag, uint16_t ___value, bool ___isbitfield, const MethodInfo* method);
#define ExifEnumProperty_1__ctor_m8_2144(__this, ___tag, ___value, ___isbitfield, method) (( void (*) (ExifEnumProperty_1_t8_346 *, int32_t, uint16_t, bool, const MethodInfo*))ExifEnumProperty_1__ctor_m8_2144_gshared)(__this, ___tag, ___value, ___isbitfield, method)
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.MeteringMode>::.ctor(ExifLibrary.ExifTag,T)
extern "C" void ExifEnumProperty_1__ctor_m8_1982_gshared (ExifEnumProperty_1_t8_346 * __this, int32_t ___tag, uint16_t ___value, const MethodInfo* method);
#define ExifEnumProperty_1__ctor_m8_1982(__this, ___tag, ___value, method) (( void (*) (ExifEnumProperty_1_t8_346 *, int32_t, uint16_t, const MethodInfo*))ExifEnumProperty_1__ctor_m8_1982_gshared)(__this, ___tag, ___value, method)
// System.Object ExifLibrary.ExifEnumProperty`1<ExifLibrary.MeteringMode>::get__Value()
extern "C" Object_t * ExifEnumProperty_1_get__Value_m8_2145_gshared (ExifEnumProperty_1_t8_346 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get__Value_m8_2145(__this, method) (( Object_t * (*) (ExifEnumProperty_1_t8_346 *, const MethodInfo*))ExifEnumProperty_1_get__Value_m8_2145_gshared)(__this, method)
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.MeteringMode>::set__Value(System.Object)
extern "C" void ExifEnumProperty_1_set__Value_m8_2146_gshared (ExifEnumProperty_1_t8_346 * __this, Object_t * ___value, const MethodInfo* method);
#define ExifEnumProperty_1_set__Value_m8_2146(__this, ___value, method) (( void (*) (ExifEnumProperty_1_t8_346 *, Object_t *, const MethodInfo*))ExifEnumProperty_1_set__Value_m8_2146_gshared)(__this, ___value, method)
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.MeteringMode>::get_Value()
extern "C" uint16_t ExifEnumProperty_1_get_Value_m8_2147_gshared (ExifEnumProperty_1_t8_346 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get_Value_m8_2147(__this, method) (( uint16_t (*) (ExifEnumProperty_1_t8_346 *, const MethodInfo*))ExifEnumProperty_1_get_Value_m8_2147_gshared)(__this, method)
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.MeteringMode>::set_Value(T)
extern "C" void ExifEnumProperty_1_set_Value_m8_2148_gshared (ExifEnumProperty_1_t8_346 * __this, uint16_t ___value, const MethodInfo* method);
#define ExifEnumProperty_1_set_Value_m8_2148(__this, ___value, method) (( void (*) (ExifEnumProperty_1_t8_346 *, uint16_t, const MethodInfo*))ExifEnumProperty_1_set_Value_m8_2148_gshared)(__this, ___value, method)
// System.Boolean ExifLibrary.ExifEnumProperty`1<ExifLibrary.MeteringMode>::get_IsBitField()
extern "C" bool ExifEnumProperty_1_get_IsBitField_m8_2149_gshared (ExifEnumProperty_1_t8_346 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get_IsBitField_m8_2149(__this, method) (( bool (*) (ExifEnumProperty_1_t8_346 *, const MethodInfo*))ExifEnumProperty_1_get_IsBitField_m8_2149_gshared)(__this, method)
// System.String ExifLibrary.ExifEnumProperty`1<ExifLibrary.MeteringMode>::ToString()
extern "C" String_t* ExifEnumProperty_1_ToString_m8_2150_gshared (ExifEnumProperty_1_t8_346 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_ToString_m8_2150(__this, method) (( String_t* (*) (ExifEnumProperty_1_t8_346 *, const MethodInfo*))ExifEnumProperty_1_ToString_m8_2150_gshared)(__this, method)
// ExifLibrary.ExifInterOperability ExifLibrary.ExifEnumProperty`1<ExifLibrary.MeteringMode>::get_Interoperability()
extern "C" ExifInterOperability_t8_113  ExifEnumProperty_1_get_Interoperability_m8_2151_gshared (ExifEnumProperty_1_t8_346 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get_Interoperability_m8_2151(__this, method) (( ExifInterOperability_t8_113  (*) (ExifEnumProperty_1_t8_346 *, const MethodInfo*))ExifEnumProperty_1_get_Interoperability_m8_2151_gshared)(__this, method)
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.MeteringMode>::op_Implicit(ExifLibrary.ExifEnumProperty`1<T>)
extern "C" uint16_t ExifEnumProperty_1_op_Implicit_m8_2152_gshared (Object_t * __this /* static, unused */, ExifEnumProperty_1_t8_346 * ___obj, const MethodInfo* method);
#define ExifEnumProperty_1_op_Implicit_m8_2152(__this /* static, unused */, ___obj, method) (( uint16_t (*) (Object_t * /* static, unused */, ExifEnumProperty_1_t8_346 *, const MethodInfo*))ExifEnumProperty_1_op_Implicit_m8_2152_gshared)(__this /* static, unused */, ___obj, method)
