﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.CompilerServices.IDispatchConstantAttribute
struct IDispatchConstantAttribute_t1_688;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.CompilerServices.IDispatchConstantAttribute::.ctor()
extern "C" void IDispatchConstantAttribute__ctor_m1_7546 (IDispatchConstantAttribute_t1_688 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.CompilerServices.IDispatchConstantAttribute::get_Value()
extern "C" Object_t * IDispatchConstantAttribute_get_Value_m1_7547 (IDispatchConstantAttribute_t1_688 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
