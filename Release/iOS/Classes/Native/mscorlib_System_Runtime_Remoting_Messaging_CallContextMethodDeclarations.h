﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Messaging.CallContext
struct CallContext_t1_926;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Runtime.Remoting.Messaging.Header[]
struct HeaderU5BU5D_t1_927;
// System.Runtime.Remoting.Messaging.LogicalCallContext
struct LogicalCallContext_t1_941;
// System.Collections.Hashtable
struct Hashtable_t1_100;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Messaging.CallContext::.ctor()
extern "C" void CallContext__ctor_m1_8322 (CallContext_t1_926 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Messaging.CallContext::get_HostContext()
extern "C" Object_t * CallContext_get_HostContext_m1_8323 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Messaging.CallContext::set_HostContext(System.Object)
extern "C" void CallContext_set_HostContext_m1_8324 (Object_t * __this /* static, unused */, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Messaging.CallContext::FreeNamedDataSlot(System.String)
extern "C" void CallContext_FreeNamedDataSlot_m1_8325 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Messaging.CallContext::GetData(System.String)
extern "C" Object_t * CallContext_GetData_m1_8326 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Messaging.CallContext::SetData(System.String,System.Object)
extern "C" void CallContext_SetData_m1_8327 (Object_t * __this /* static, unused */, String_t* ___name, Object_t * ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Messaging.CallContext::LogicalGetData(System.String)
extern "C" Object_t * CallContext_LogicalGetData_m1_8328 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Messaging.CallContext::LogicalSetData(System.String,System.Object)
extern "C" void CallContext_LogicalSetData_m1_8329 (Object_t * __this /* static, unused */, String_t* ___name, Object_t * ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Messaging.Header[] System.Runtime.Remoting.Messaging.CallContext::GetHeaders()
extern "C" HeaderU5BU5D_t1_927* CallContext_GetHeaders_m1_8330 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Messaging.CallContext::SetHeaders(System.Runtime.Remoting.Messaging.Header[])
extern "C" void CallContext_SetHeaders_m1_8331 (Object_t * __this /* static, unused */, HeaderU5BU5D_t1_927* ___headers, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Messaging.LogicalCallContext System.Runtime.Remoting.Messaging.CallContext::CreateLogicalCallContext(System.Boolean)
extern "C" LogicalCallContext_t1_941 * CallContext_CreateLogicalCallContext_m1_8332 (Object_t * __this /* static, unused */, bool ___createEmpty, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Messaging.CallContext::SetCurrentCallContext(System.Runtime.Remoting.Messaging.LogicalCallContext)
extern "C" Object_t * CallContext_SetCurrentCallContext_m1_8333 (Object_t * __this /* static, unused */, LogicalCallContext_t1_941 * ___ctx, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Messaging.CallContext::UpdateCurrentCallContext(System.Runtime.Remoting.Messaging.LogicalCallContext)
extern "C" void CallContext_UpdateCurrentCallContext_m1_8334 (Object_t * __this /* static, unused */, LogicalCallContext_t1_941 * ___ctx, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Messaging.CallContext::RestoreCallContext(System.Object)
extern "C" void CallContext_RestoreCallContext_m1_8335 (Object_t * __this /* static, unused */, Object_t * ___oldContext, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Hashtable System.Runtime.Remoting.Messaging.CallContext::get_Datastore()
extern "C" Hashtable_t1_100 * CallContext_get_Datastore_m1_8336 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
