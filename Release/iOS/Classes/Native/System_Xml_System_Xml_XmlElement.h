﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Xml.XmlAttributeCollection
struct XmlAttributeCollection_t4_120;
// System.Xml.XmlNameEntry
struct XmlNameEntry_t4_117;
// System.Xml.XmlLinkedNode
struct XmlLinkedNode_t4_118;
// System.Xml.Schema.IXmlSchemaInfo
struct IXmlSchemaInfo_t4_119;

#include "System_Xml_System_Xml_XmlLinkedNode.h"

// System.Xml.XmlElement
struct  XmlElement_t4_122  : public XmlLinkedNode_t4_118
{
	// System.Xml.XmlAttributeCollection System.Xml.XmlElement::attributes
	XmlAttributeCollection_t4_120 * ___attributes_6;
	// System.Xml.XmlNameEntry System.Xml.XmlElement::name
	XmlNameEntry_t4_117 * ___name_7;
	// System.Xml.XmlLinkedNode System.Xml.XmlElement::lastLinkedChild
	XmlLinkedNode_t4_118 * ___lastLinkedChild_8;
	// System.Boolean System.Xml.XmlElement::isNotEmpty
	bool ___isNotEmpty_9;
	// System.Xml.Schema.IXmlSchemaInfo System.Xml.XmlElement::schemaInfo
	Object_t * ___schemaInfo_10;
};
