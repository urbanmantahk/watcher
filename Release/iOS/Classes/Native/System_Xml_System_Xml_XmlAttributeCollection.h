﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Xml.XmlElement
struct XmlElement_t4_122;
// System.Xml.XmlDocument
struct XmlDocument_t4_123;

#include "System_Xml_System_Xml_XmlNamedNodeMap.h"

// System.Xml.XmlAttributeCollection
struct  XmlAttributeCollection_t4_120  : public XmlNamedNodeMap_t4_121
{
	// System.Xml.XmlElement System.Xml.XmlAttributeCollection::ownerElement
	XmlElement_t4_122 * ___ownerElement_4;
	// System.Xml.XmlDocument System.Xml.XmlAttributeCollection::ownerDocument
	XmlDocument_t4_123 * ___ownerDocument_5;
};
