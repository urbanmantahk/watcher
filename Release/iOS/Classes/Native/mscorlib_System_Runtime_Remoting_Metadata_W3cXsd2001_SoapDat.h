﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String[]
struct StringU5BU5D_t1_238;

#include "mscorlib_System_Object.h"
#include "mscorlib_System_DateTime.h"

// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapDate
struct  SoapDate_t1_967  : public Object_t
{
	// System.Int32 System.Runtime.Remoting.Metadata.W3cXsd2001.SoapDate::_sign
	int32_t ____sign_1;
	// System.DateTime System.Runtime.Remoting.Metadata.W3cXsd2001.SoapDate::_value
	DateTime_t1_150  ____value_2;
};
struct SoapDate_t1_967_StaticFields{
	// System.String[] System.Runtime.Remoting.Metadata.W3cXsd2001.SoapDate::_datetimeFormats
	StringU5BU5D_t1_238* ____datetimeFormats_0;
};
