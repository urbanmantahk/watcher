﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Security_AccessControl_AuditRule.h"
#include "mscorlib_System_Security_AccessControl_CryptoKeyRights.h"

// System.Security.AccessControl.CryptoKeyAuditRule
struct  CryptoKeyAuditRule_t1_1140  : public AuditRule_t1_1119
{
	// System.Security.AccessControl.CryptoKeyRights System.Security.AccessControl.CryptoKeyAuditRule::rights
	int32_t ___rights_6;
};
