﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Globalization.HijriCalendar
struct HijriCalendar_t1_374;
// System.Int32[]
struct Int32U5BU5D_t1_275;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime.h"
#include "mscorlib_System_DayOfWeek.h"

// System.Void System.Globalization.HijriCalendar::.ctor()
extern "C" void HijriCalendar__ctor_m1_4150 (HijriCalendar_t1_374 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.HijriCalendar::.cctor()
extern "C" void HijriCalendar__cctor_m1_4151 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] System.Globalization.HijriCalendar::get_Eras()
extern "C" Int32U5BU5D_t1_275* HijriCalendar_get_Eras_m1_4152 (HijriCalendar_t1_374 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.HijriCalendar::get_HijriAdjustment()
extern "C" int32_t HijriCalendar_get_HijriAdjustment_m1_4153 (HijriCalendar_t1_374 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.HijriCalendar::set_HijriAdjustment(System.Int32)
extern "C" void HijriCalendar_set_HijriAdjustment_m1_4154 (HijriCalendar_t1_374 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.HijriCalendar::get_TwoDigitYearMax()
extern "C" int32_t HijriCalendar_get_TwoDigitYearMax_m1_4155 (HijriCalendar_t1_374 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.HijriCalendar::set_TwoDigitYearMax(System.Int32)
extern "C" void HijriCalendar_set_TwoDigitYearMax_m1_4156 (HijriCalendar_t1_374 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.HijriCalendar::get_AddHijriDate()
extern "C" int32_t HijriCalendar_get_AddHijriDate_m1_4157 (HijriCalendar_t1_374 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.HijriCalendar::set_AddHijriDate(System.Int32)
extern "C" void HijriCalendar_set_AddHijriDate_m1_4158 (HijriCalendar_t1_374 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.HijriCalendar::M_CheckFixedHijri(System.String,System.Int32)
extern "C" void HijriCalendar_M_CheckFixedHijri_m1_4159 (HijriCalendar_t1_374 * __this, String_t* ___param, int32_t ___rdHijri, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.HijriCalendar::M_CheckDateTime(System.DateTime)
extern "C" void HijriCalendar_M_CheckDateTime_m1_4160 (HijriCalendar_t1_374 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.HijriCalendar::M_FromDateTime(System.DateTime)
extern "C" int32_t HijriCalendar_M_FromDateTime_m1_4161 (HijriCalendar_t1_374 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.HijriCalendar::M_ToDateTime(System.Int32)
extern "C" DateTime_t1_150  HijriCalendar_M_ToDateTime_m1_4162 (HijriCalendar_t1_374 * __this, int32_t ___rd, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.HijriCalendar::M_ToDateTime(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" DateTime_t1_150  HijriCalendar_M_ToDateTime_m1_4163 (HijriCalendar_t1_374 * __this, int32_t ___date, int32_t ___hour, int32_t ___minute, int32_t ___second, int32_t ___milliseconds, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.HijriCalendar::M_CheckEra(System.Int32&)
extern "C" void HijriCalendar_M_CheckEra_m1_4164 (HijriCalendar_t1_374 * __this, int32_t* ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.HijriCalendar::M_CheckYE(System.Int32,System.Int32&)
extern "C" void HijriCalendar_M_CheckYE_m1_4165 (HijriCalendar_t1_374 * __this, int32_t ___year, int32_t* ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.HijriCalendar::M_CheckYME(System.Int32,System.Int32,System.Int32&)
extern "C" void HijriCalendar_M_CheckYME_m1_4166 (HijriCalendar_t1_374 * __this, int32_t ___year, int32_t ___month, int32_t* ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.HijriCalendar::M_CheckYMDE(System.Int32,System.Int32,System.Int32,System.Int32&)
extern "C" void HijriCalendar_M_CheckYMDE_m1_4167 (HijriCalendar_t1_374 * __this, int32_t ___year, int32_t ___month, int32_t ___day, int32_t* ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.HijriCalendar::AddMonths(System.DateTime,System.Int32)
extern "C" DateTime_t1_150  HijriCalendar_AddMonths_m1_4168 (HijriCalendar_t1_374 * __this, DateTime_t1_150  ___time, int32_t ___months, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.HijriCalendar::AddYears(System.DateTime,System.Int32)
extern "C" DateTime_t1_150  HijriCalendar_AddYears_m1_4169 (HijriCalendar_t1_374 * __this, DateTime_t1_150  ___time, int32_t ___years, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.HijriCalendar::GetDayOfMonth(System.DateTime)
extern "C" int32_t HijriCalendar_GetDayOfMonth_m1_4170 (HijriCalendar_t1_374 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DayOfWeek System.Globalization.HijriCalendar::GetDayOfWeek(System.DateTime)
extern "C" int32_t HijriCalendar_GetDayOfWeek_m1_4171 (HijriCalendar_t1_374 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.HijriCalendar::GetDayOfYear(System.DateTime)
extern "C" int32_t HijriCalendar_GetDayOfYear_m1_4172 (HijriCalendar_t1_374 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.HijriCalendar::GetDaysInMonth(System.Int32,System.Int32,System.Int32)
extern "C" int32_t HijriCalendar_GetDaysInMonth_m1_4173 (HijriCalendar_t1_374 * __this, int32_t ___year, int32_t ___month, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.HijriCalendar::GetDaysInYear(System.Int32,System.Int32)
extern "C" int32_t HijriCalendar_GetDaysInYear_m1_4174 (HijriCalendar_t1_374 * __this, int32_t ___year, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.HijriCalendar::GetEra(System.DateTime)
extern "C" int32_t HijriCalendar_GetEra_m1_4175 (HijriCalendar_t1_374 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.HijriCalendar::GetLeapMonth(System.Int32,System.Int32)
extern "C" int32_t HijriCalendar_GetLeapMonth_m1_4176 (HijriCalendar_t1_374 * __this, int32_t ___year, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.HijriCalendar::GetMonth(System.DateTime)
extern "C" int32_t HijriCalendar_GetMonth_m1_4177 (HijriCalendar_t1_374 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.HijriCalendar::GetMonthsInYear(System.Int32,System.Int32)
extern "C" int32_t HijriCalendar_GetMonthsInYear_m1_4178 (HijriCalendar_t1_374 * __this, int32_t ___year, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.HijriCalendar::GetYear(System.DateTime)
extern "C" int32_t HijriCalendar_GetYear_m1_4179 (HijriCalendar_t1_374 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.HijriCalendar::IsLeapDay(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" bool HijriCalendar_IsLeapDay_m1_4180 (HijriCalendar_t1_374 * __this, int32_t ___year, int32_t ___month, int32_t ___day, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.HijriCalendar::IsLeapMonth(System.Int32,System.Int32,System.Int32)
extern "C" bool HijriCalendar_IsLeapMonth_m1_4181 (HijriCalendar_t1_374 * __this, int32_t ___year, int32_t ___month, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.HijriCalendar::IsLeapYear(System.Int32,System.Int32)
extern "C" bool HijriCalendar_IsLeapYear_m1_4182 (HijriCalendar_t1_374 * __this, int32_t ___year, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.HijriCalendar::ToDateTime(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" DateTime_t1_150  HijriCalendar_ToDateTime_m1_4183 (HijriCalendar_t1_374 * __this, int32_t ___year, int32_t ___month, int32_t ___day, int32_t ___hour, int32_t ___minute, int32_t ___second, int32_t ___millisecond, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.HijriCalendar::ToFourDigitYear(System.Int32)
extern "C" int32_t HijriCalendar_ToFourDigitYear_m1_4184 (HijriCalendar_t1_374 * __this, int32_t ___year, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.HijriCalendar::get_MinSupportedDateTime()
extern "C" DateTime_t1_150  HijriCalendar_get_MinSupportedDateTime_m1_4185 (HijriCalendar_t1_374 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.HijriCalendar::get_MaxSupportedDateTime()
extern "C" DateTime_t1_150  HijriCalendar_get_MaxSupportedDateTime_m1_4186 (HijriCalendar_t1_374 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
