﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.X509.Extensions.BasicConstraintsExtension
struct BasicConstraintsExtension_t1_177;
// Mono.Security.ASN1
struct ASN1_t1_149;
// Mono.Security.X509.X509Extension
struct X509Extension_t1_178;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Security.X509.Extensions.BasicConstraintsExtension::.ctor()
extern "C" void BasicConstraintsExtension__ctor_m1_2093 (BasicConstraintsExtension_t1_177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.Extensions.BasicConstraintsExtension::.ctor(Mono.Security.ASN1)
extern "C" void BasicConstraintsExtension__ctor_m1_2094 (BasicConstraintsExtension_t1_177 * __this, ASN1_t1_149 * ___asn1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.Extensions.BasicConstraintsExtension::.ctor(Mono.Security.X509.X509Extension)
extern "C" void BasicConstraintsExtension__ctor_m1_2095 (BasicConstraintsExtension_t1_177 * __this, X509Extension_t1_178 * ___extension, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.Extensions.BasicConstraintsExtension::Decode()
extern "C" void BasicConstraintsExtension_Decode_m1_2096 (BasicConstraintsExtension_t1_177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.Extensions.BasicConstraintsExtension::Encode()
extern "C" void BasicConstraintsExtension_Encode_m1_2097 (BasicConstraintsExtension_t1_177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.Extensions.BasicConstraintsExtension::get_CertificateAuthority()
extern "C" bool BasicConstraintsExtension_get_CertificateAuthority_m1_2098 (BasicConstraintsExtension_t1_177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.Extensions.BasicConstraintsExtension::set_CertificateAuthority(System.Boolean)
extern "C" void BasicConstraintsExtension_set_CertificateAuthority_m1_2099 (BasicConstraintsExtension_t1_177 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.X509.Extensions.BasicConstraintsExtension::get_Name()
extern "C" String_t* BasicConstraintsExtension_get_Name_m1_2100 (BasicConstraintsExtension_t1_177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Security.X509.Extensions.BasicConstraintsExtension::get_PathLenConstraint()
extern "C" int32_t BasicConstraintsExtension_get_PathLenConstraint_m1_2101 (BasicConstraintsExtension_t1_177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.Extensions.BasicConstraintsExtension::set_PathLenConstraint(System.Int32)
extern "C" void BasicConstraintsExtension_set_PathLenConstraint_m1_2102 (BasicConstraintsExtension_t1_177 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.X509.Extensions.BasicConstraintsExtension::ToString()
extern "C" String_t* BasicConstraintsExtension_ToString_m1_2103 (BasicConstraintsExtension_t1_177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
