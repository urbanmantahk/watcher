﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.IO.Stream
struct Stream_t1_405;

#include "mscorlib_System_Object.h"

// System.Resources.Win32ResFileReader
struct  Win32ResFileReader_t1_670  : public Object_t
{
	// System.IO.Stream System.Resources.Win32ResFileReader::res_file
	Stream_t1_405 * ___res_file_0;
};
