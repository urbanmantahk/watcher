﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Object
struct Object_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgument.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArgument.h"

// System.Void System.Reflection.CustomAttributeNamedArgument::.ctor(System.Reflection.MemberInfo,System.Object)
extern "C" void CustomAttributeNamedArgument__ctor_m1_6794 (CustomAttributeNamedArgument_t1_593 * __this, MemberInfo_t * ___memberInfo, Object_t * ___typedArgument, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MemberInfo System.Reflection.CustomAttributeNamedArgument::get_MemberInfo()
extern "C" MemberInfo_t * CustomAttributeNamedArgument_get_MemberInfo_m1_6795 (CustomAttributeNamedArgument_t1_593 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.CustomAttributeTypedArgument System.Reflection.CustomAttributeNamedArgument::get_TypedValue()
extern "C" CustomAttributeTypedArgument_t1_594  CustomAttributeNamedArgument_get_TypedValue_m1_6796 (CustomAttributeNamedArgument_t1_593 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.CustomAttributeNamedArgument::ToString()
extern "C" String_t* CustomAttributeNamedArgument_ToString_m1_6797 (CustomAttributeNamedArgument_t1_593 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.CustomAttributeNamedArgument::Equals(System.Object)
extern "C" bool CustomAttributeNamedArgument_Equals_m1_6798 (CustomAttributeNamedArgument_t1_593 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.CustomAttributeNamedArgument::GetHashCode()
extern "C" int32_t CustomAttributeNamedArgument_GetHashCode_m1_6799 (CustomAttributeNamedArgument_t1_593 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.CustomAttributeNamedArgument::op_Equality(System.Reflection.CustomAttributeNamedArgument,System.Reflection.CustomAttributeNamedArgument)
extern "C" bool CustomAttributeNamedArgument_op_Equality_m1_6800 (Object_t * __this /* static, unused */, CustomAttributeNamedArgument_t1_593  ___left, CustomAttributeNamedArgument_t1_593  ___right, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.CustomAttributeNamedArgument::op_Inequality(System.Reflection.CustomAttributeNamedArgument,System.Reflection.CustomAttributeNamedArgument)
extern "C" bool CustomAttributeNamedArgument_op_Inequality_m1_6801 (Object_t * __this /* static, unused */, CustomAttributeNamedArgument_t1_593  ___left, CustomAttributeNamedArgument_t1_593  ___right, const MethodInfo* method) IL2CPP_METHOD_ATTR;
