﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Text.Encoding
struct Encoding_t1_406;

#include "AssemblyU2DCSharp_ExifLibrary_ExifProperty.h"

// ExifLibrary.ExifEncodedString
struct  ExifEncodedString_t8_98  : public ExifProperty_t8_99
{
	// System.String ExifLibrary.ExifEncodedString::mValue
	String_t* ___mValue_3;
	// System.Text.Encoding ExifLibrary.ExifEncodedString::mEncoding
	Encoding_t1_406 * ___mEncoding_4;
};
