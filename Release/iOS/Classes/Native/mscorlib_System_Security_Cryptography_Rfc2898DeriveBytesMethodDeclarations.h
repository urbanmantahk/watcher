﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.Rfc2898DeriveBytes
struct Rfc2898DeriveBytes_t1_1243;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Cryptography.Rfc2898DeriveBytes::.ctor(System.String,System.Byte[])
extern "C" void Rfc2898DeriveBytes__ctor_m1_10598 (Rfc2898DeriveBytes_t1_1243 * __this, String_t* ___password, ByteU5BU5D_t1_109* ___salt, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Rfc2898DeriveBytes::.ctor(System.String,System.Byte[],System.Int32)
extern "C" void Rfc2898DeriveBytes__ctor_m1_10599 (Rfc2898DeriveBytes_t1_1243 * __this, String_t* ___password, ByteU5BU5D_t1_109* ___salt, int32_t ___iterations, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Rfc2898DeriveBytes::.ctor(System.Byte[],System.Byte[],System.Int32)
extern "C" void Rfc2898DeriveBytes__ctor_m1_10600 (Rfc2898DeriveBytes_t1_1243 * __this, ByteU5BU5D_t1_109* ___password, ByteU5BU5D_t1_109* ___salt, int32_t ___iterations, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Rfc2898DeriveBytes::.ctor(System.String,System.Int32)
extern "C" void Rfc2898DeriveBytes__ctor_m1_10601 (Rfc2898DeriveBytes_t1_1243 * __this, String_t* ___password, int32_t ___saltSize, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Rfc2898DeriveBytes::.ctor(System.String,System.Int32,System.Int32)
extern "C" void Rfc2898DeriveBytes__ctor_m1_10602 (Rfc2898DeriveBytes_t1_1243 * __this, String_t* ___password, int32_t ___saltSize, int32_t ___iterations, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Cryptography.Rfc2898DeriveBytes::get_IterationCount()
extern "C" int32_t Rfc2898DeriveBytes_get_IterationCount_m1_10603 (Rfc2898DeriveBytes_t1_1243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Rfc2898DeriveBytes::set_IterationCount(System.Int32)
extern "C" void Rfc2898DeriveBytes_set_IterationCount_m1_10604 (Rfc2898DeriveBytes_t1_1243 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.Rfc2898DeriveBytes::get_Salt()
extern "C" ByteU5BU5D_t1_109* Rfc2898DeriveBytes_get_Salt_m1_10605 (Rfc2898DeriveBytes_t1_1243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Rfc2898DeriveBytes::set_Salt(System.Byte[])
extern "C" void Rfc2898DeriveBytes_set_Salt_m1_10606 (Rfc2898DeriveBytes_t1_1243 * __this, ByteU5BU5D_t1_109* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.Rfc2898DeriveBytes::F(System.Byte[],System.Int32,System.Int32)
extern "C" ByteU5BU5D_t1_109* Rfc2898DeriveBytes_F_m1_10607 (Rfc2898DeriveBytes_t1_1243 * __this, ByteU5BU5D_t1_109* ___s, int32_t ___c, int32_t ___i, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.Rfc2898DeriveBytes::GetBytes(System.Int32)
extern "C" ByteU5BU5D_t1_109* Rfc2898DeriveBytes_GetBytes_m1_10608 (Rfc2898DeriveBytes_t1_1243 * __this, int32_t ___cb, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Rfc2898DeriveBytes::Reset()
extern "C" void Rfc2898DeriveBytes_Reset_m1_10609 (Rfc2898DeriveBytes_t1_1243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
