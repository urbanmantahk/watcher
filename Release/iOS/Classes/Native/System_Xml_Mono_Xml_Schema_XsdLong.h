﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "System_Xml_Mono_Xml_Schema_XsdInteger.h"

// Mono.Xml.Schema.XsdLong
struct  XsdLong_t4_23  : public XsdInteger_t4_22
{
};
