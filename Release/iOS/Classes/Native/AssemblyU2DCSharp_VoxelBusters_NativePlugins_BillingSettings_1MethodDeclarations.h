﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.BillingSettings
struct BillingSettings_t8_217;
// VoxelBusters.NativePlugins.BillingProduct[]
struct BillingProductU5BU5D_t8_337;
// VoxelBusters.NativePlugins.BillingSettings/iOSSettings
struct iOSSettings_t8_216;
// VoxelBusters.NativePlugins.BillingSettings/AndroidSettings
struct AndroidSettings_t8_215;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.BillingSettings::.ctor()
extern "C" void BillingSettings__ctor_m8_1235 (BillingSettings_t8_217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.NativePlugins.BillingProduct[] VoxelBusters.NativePlugins.BillingSettings::get_Products()
extern "C" BillingProductU5BU5D_t8_337* BillingSettings_get_Products_m8_1236 (BillingSettings_t8_217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.BillingSettings::set_Products(VoxelBusters.NativePlugins.BillingProduct[])
extern "C" void BillingSettings_set_Products_m8_1237 (BillingSettings_t8_217 * __this, BillingProductU5BU5D_t8_337* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.NativePlugins.BillingSettings/iOSSettings VoxelBusters.NativePlugins.BillingSettings::get_iOS()
extern "C" iOSSettings_t8_216 * BillingSettings_get_iOS_m8_1238 (BillingSettings_t8_217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.BillingSettings::set_iOS(VoxelBusters.NativePlugins.BillingSettings/iOSSettings)
extern "C" void BillingSettings_set_iOS_m8_1239 (BillingSettings_t8_217 * __this, iOSSettings_t8_216 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.NativePlugins.BillingSettings/AndroidSettings VoxelBusters.NativePlugins.BillingSettings::get_Android()
extern "C" AndroidSettings_t8_215 * BillingSettings_get_Android_m8_1240 (BillingSettings_t8_217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.BillingSettings::set_Android(VoxelBusters.NativePlugins.BillingSettings/AndroidSettings)
extern "C" void BillingSettings_set_Android_m8_1241 (BillingSettings_t8_217 * __this, AndroidSettings_t8_215 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
