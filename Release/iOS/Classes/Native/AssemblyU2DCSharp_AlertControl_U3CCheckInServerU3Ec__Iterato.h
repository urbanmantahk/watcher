﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WWWForm
struct WWWForm_t6_79;
// UnityEngine.WWW
struct WWW_t6_78;
// Boomlagoon.JSON.JSONObject
struct JSONObject_t8_5;
// System.Action
struct Action_t5_11;
// System.Object
struct Object_t;
// AlertControl
struct AlertControl_t8_9;

#include "mscorlib_System_Object.h"

// AlertControl/<CheckInServer>c__Iterator0
struct  U3CCheckInServerU3Ec__Iterator0_t8_8  : public Object_t
{
	// UnityEngine.WWWForm AlertControl/<CheckInServer>c__Iterator0::<_f>__0
	WWWForm_t6_79 * ___U3C_fU3E__0_0;
	// UnityEngine.WWW AlertControl/<CheckInServer>c__Iterator0::<_w>__1
	WWW_t6_78 * ___U3C_wU3E__1_1;
	// System.Single AlertControl/<CheckInServer>c__Iterator0::<_elapse>__2
	float ___U3C_elapseU3E__2_2;
	// System.Single AlertControl/<CheckInServer>c__Iterator0::<_time>__3
	float ___U3C_timeU3E__3_3;
	// System.Single AlertControl/<CheckInServer>c__Iterator0::<_timeout>__4
	float ___U3C_timeoutU3E__4_4;
	// Boomlagoon.JSON.JSONObject AlertControl/<CheckInServer>c__Iterator0::<_json>__5
	JSONObject_t8_5 * ___U3C_jsonU3E__5_5;
	// System.Action AlertControl/<CheckInServer>c__Iterator0::_action
	Action_t5_11 * ____action_6;
	// System.Int32 AlertControl/<CheckInServer>c__Iterator0::$PC
	int32_t ___U24PC_7;
	// System.Object AlertControl/<CheckInServer>c__Iterator0::$current
	Object_t * ___U24current_8;
	// System.Action AlertControl/<CheckInServer>c__Iterator0::<$>_action
	Action_t5_11 * ___U3CU24U3E_action_9;
	// AlertControl AlertControl/<CheckInServer>c__Iterator0::<>f__this
	AlertControl_t8_9 * ___U3CU3Ef__this_10;
};
