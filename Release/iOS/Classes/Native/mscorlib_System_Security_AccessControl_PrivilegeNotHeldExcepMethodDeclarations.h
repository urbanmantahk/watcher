﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.AccessControl.PrivilegeNotHeldException
struct PrivilegeNotHeldException_t1_1167;
// System.String
struct String_t;
// System.Exception
struct Exception_t1_33;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.Security.AccessControl.PrivilegeNotHeldException::.ctor()
extern "C" void PrivilegeNotHeldException__ctor_m1_10028 (PrivilegeNotHeldException_t1_1167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.PrivilegeNotHeldException::.ctor(System.String)
extern "C" void PrivilegeNotHeldException__ctor_m1_10029 (PrivilegeNotHeldException_t1_1167 * __this, String_t* ___privilege, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.PrivilegeNotHeldException::.ctor(System.String,System.Exception)
extern "C" void PrivilegeNotHeldException__ctor_m1_10030 (PrivilegeNotHeldException_t1_1167 * __this, String_t* ___privilege, Exception_t1_33 * ___inner, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.AccessControl.PrivilegeNotHeldException::get_PrivilegeName()
extern "C" String_t* PrivilegeNotHeldException_get_PrivilegeName_m1_10031 (PrivilegeNotHeldException_t1_1167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.PrivilegeNotHeldException::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void PrivilegeNotHeldException_GetObjectData_m1_10032 (PrivilegeNotHeldException_t1_1167 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
