﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_63.h"
#include "mscorlib_System_Reflection_Emit_ILExceptionInfo.h"

// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILExceptionInfo>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_16221_gshared (InternalEnumerator_1_t1_2055 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1_16221(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1_2055 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m1_16221_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILExceptionInfo>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_16222_gshared (InternalEnumerator_1_t1_2055 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_16222(__this, method) (( void (*) (InternalEnumerator_1_t1_2055 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_16222_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ILExceptionInfo>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_16223_gshared (InternalEnumerator_1_t1_2055 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_16223(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1_2055 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_16223_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILExceptionInfo>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_16224_gshared (InternalEnumerator_1_t1_2055 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1_16224(__this, method) (( void (*) (InternalEnumerator_1_t1_2055 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1_16224_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ILExceptionInfo>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_16225_gshared (InternalEnumerator_1_t1_2055 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1_16225(__this, method) (( bool (*) (InternalEnumerator_1_t1_2055 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1_16225_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ILExceptionInfo>::get_Current()
extern "C" ILExceptionInfo_t1_511  InternalEnumerator_1_get_Current_m1_16226_gshared (InternalEnumerator_1_t1_2055 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1_16226(__this, method) (( ILExceptionInfo_t1_511  (*) (InternalEnumerator_1_t1_2055 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1_16226_gshared)(__this, method)
