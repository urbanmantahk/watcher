﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.IDictionary
struct IDictionary_t1_35;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1_1742;

#include "mscorlib_System_ValueType.h"

// VoxelBusters.Utility.WebResponse
struct  WebResponse_t8_164 
{
	// System.Int32 VoxelBusters.Utility.WebResponse::<Status>k__BackingField
	int32_t ___U3CStatusU3Ek__BackingField_0;
	// System.String VoxelBusters.Utility.WebResponse::<Message>k__BackingField
	String_t* ___U3CMessageU3Ek__BackingField_1;
	// System.Collections.IDictionary VoxelBusters.Utility.WebResponse::<Data>k__BackingField
	Object_t * ___U3CDataU3Ek__BackingField_2;
	// System.Collections.Generic.List`1<System.String> VoxelBusters.Utility.WebResponse::<Errors>k__BackingField
	List_1_t1_1742 * ___U3CErrorsU3Ek__BackingField_3;
};
