﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Permissions.IsolatedStoragePermissionAttribute
struct IsolatedStoragePermissionAttribute_t1_1287;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Permissions_SecurityAction.h"
#include "mscorlib_System_Security_Permissions_IsolatedStorageContainm.h"

// System.Void System.Security.Permissions.IsolatedStoragePermissionAttribute::.ctor(System.Security.Permissions.SecurityAction)
extern "C" void IsolatedStoragePermissionAttribute__ctor_m1_10947 (IsolatedStoragePermissionAttribute_t1_1287 * __this, int32_t ___action, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Permissions.IsolatedStorageContainment System.Security.Permissions.IsolatedStoragePermissionAttribute::get_UsageAllowed()
extern "C" int32_t IsolatedStoragePermissionAttribute_get_UsageAllowed_m1_10948 (IsolatedStoragePermissionAttribute_t1_1287 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.IsolatedStoragePermissionAttribute::set_UsageAllowed(System.Security.Permissions.IsolatedStorageContainment)
extern "C" void IsolatedStoragePermissionAttribute_set_UsageAllowed_m1_10949 (IsolatedStoragePermissionAttribute_t1_1287 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Security.Permissions.IsolatedStoragePermissionAttribute::get_UserQuota()
extern "C" int64_t IsolatedStoragePermissionAttribute_get_UserQuota_m1_10950 (IsolatedStoragePermissionAttribute_t1_1287 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.IsolatedStoragePermissionAttribute::set_UserQuota(System.Int64)
extern "C" void IsolatedStoragePermissionAttribute_set_UserQuota_m1_10951 (IsolatedStoragePermissionAttribute_t1_1287 * __this, int64_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
