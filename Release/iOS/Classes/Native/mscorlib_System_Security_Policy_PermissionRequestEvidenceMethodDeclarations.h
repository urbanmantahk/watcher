﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Policy.PermissionRequestEvidence
struct PermissionRequestEvidence_t1_1355;
// System.Security.PermissionSet
struct PermissionSet_t1_563;
// System.Char[]
struct CharU5BU5D_t1_16;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Policy.PermissionRequestEvidence::.ctor(System.Security.PermissionSet,System.Security.PermissionSet,System.Security.PermissionSet)
extern "C" void PermissionRequestEvidence__ctor_m1_11573 (PermissionRequestEvidence_t1_1355 * __this, PermissionSet_t1_563 * ___request, PermissionSet_t1_563 * ___optional, PermissionSet_t1_563 * ___denied, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Policy.PermissionRequestEvidence::System.Security.Policy.IBuiltInEvidence.GetRequiredSize(System.Boolean)
extern "C" int32_t PermissionRequestEvidence_System_Security_Policy_IBuiltInEvidence_GetRequiredSize_m1_11574 (PermissionRequestEvidence_t1_1355 * __this, bool ___verbose, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Policy.PermissionRequestEvidence::System.Security.Policy.IBuiltInEvidence.InitFromBuffer(System.Char[],System.Int32)
extern "C" int32_t PermissionRequestEvidence_System_Security_Policy_IBuiltInEvidence_InitFromBuffer_m1_11575 (PermissionRequestEvidence_t1_1355 * __this, CharU5BU5D_t1_16* ___buffer, int32_t ___position, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Policy.PermissionRequestEvidence::System.Security.Policy.IBuiltInEvidence.OutputToBuffer(System.Char[],System.Int32,System.Boolean)
extern "C" int32_t PermissionRequestEvidence_System_Security_Policy_IBuiltInEvidence_OutputToBuffer_m1_11576 (PermissionRequestEvidence_t1_1355 * __this, CharU5BU5D_t1_16* ___buffer, int32_t ___position, bool ___verbose, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.PermissionSet System.Security.Policy.PermissionRequestEvidence::get_DeniedPermissions()
extern "C" PermissionSet_t1_563 * PermissionRequestEvidence_get_DeniedPermissions_m1_11577 (PermissionRequestEvidence_t1_1355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.PermissionSet System.Security.Policy.PermissionRequestEvidence::get_OptionalPermissions()
extern "C" PermissionSet_t1_563 * PermissionRequestEvidence_get_OptionalPermissions_m1_11578 (PermissionRequestEvidence_t1_1355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.PermissionSet System.Security.Policy.PermissionRequestEvidence::get_RequestedPermissions()
extern "C" PermissionSet_t1_563 * PermissionRequestEvidence_get_RequestedPermissions_m1_11579 (PermissionRequestEvidence_t1_1355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.PermissionRequestEvidence System.Security.Policy.PermissionRequestEvidence::Copy()
extern "C" PermissionRequestEvidence_t1_1355 * PermissionRequestEvidence_Copy_m1_11580 (PermissionRequestEvidence_t1_1355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Policy.PermissionRequestEvidence::ToString()
extern "C" String_t* PermissionRequestEvidence_ToString_m1_11581 (PermissionRequestEvidence_t1_1355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
