﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.Internal.EditorAddressBookContact
struct EditorAddressBookContact_t8_200;
// VoxelBusters.NativePlugins.AddressBookContact
struct AddressBookContact_t8_198;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.Internal.EditorAddressBookContact::.ctor(VoxelBusters.NativePlugins.AddressBookContact)
extern "C" void EditorAddressBookContact__ctor_m8_1156 (EditorAddressBookContact_t8_200 * __this, AddressBookContact_t8_198 * ____source, const MethodInfo* method) IL2CPP_METHOD_ATTR;
