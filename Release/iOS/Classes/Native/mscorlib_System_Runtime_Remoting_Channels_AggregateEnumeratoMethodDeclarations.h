﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Channels.AggregateEnumerator
struct AggregateEnumerator_t1_862;
// System.Collections.IDictionary[]
struct IDictionaryU5BU5D_t1_861;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Runtime.Remoting.Channels.AggregateEnumerator::.ctor(System.Collections.IDictionary[])
extern "C" void AggregateEnumerator__ctor_m1_8000 (AggregateEnumerator_t1_862 * __this, IDictionaryU5BU5D_t1_861* ___dics, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.DictionaryEntry System.Runtime.Remoting.Channels.AggregateEnumerator::get_Entry()
extern "C" DictionaryEntry_t1_284  AggregateEnumerator_get_Entry_m1_8001 (AggregateEnumerator_t1_862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Channels.AggregateEnumerator::get_Key()
extern "C" Object_t * AggregateEnumerator_get_Key_m1_8002 (AggregateEnumerator_t1_862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Channels.AggregateEnumerator::get_Value()
extern "C" Object_t * AggregateEnumerator_get_Value_m1_8003 (AggregateEnumerator_t1_862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Channels.AggregateEnumerator::get_Current()
extern "C" Object_t * AggregateEnumerator_get_Current_m1_8004 (AggregateEnumerator_t1_862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Remoting.Channels.AggregateEnumerator::MoveNext()
extern "C" bool AggregateEnumerator_MoveNext_m1_8005 (AggregateEnumerator_t1_862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Channels.AggregateEnumerator::Reset()
extern "C" void AggregateEnumerator_Reset_m1_8006 (AggregateEnumerator_t1_862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
