﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// VoxelBusters.NativePlugins.eShareOptions[]
struct eShareOptionsU5BU5D_t8_186;
// VoxelBusters.NativePlugins.Sharing/SharingCompletion
struct SharingCompletion_t8_271;
// VoxelBusters.NativePlugins.Sharing
struct Sharing_t8_273;

#include "mscorlib_System_Object.h"

// VoxelBusters.NativePlugins.Sharing/<ShareScreenShot>c__AnonStorey10
struct  U3CShareScreenShotU3Ec__AnonStorey10_t8_277  : public Object_t
{
	// System.String VoxelBusters.NativePlugins.Sharing/<ShareScreenShot>c__AnonStorey10::_message
	String_t* ____message_0;
	// VoxelBusters.NativePlugins.eShareOptions[] VoxelBusters.NativePlugins.Sharing/<ShareScreenShot>c__AnonStorey10::_excludedOptions
	eShareOptionsU5BU5D_t8_186* ____excludedOptions_1;
	// VoxelBusters.NativePlugins.Sharing/SharingCompletion VoxelBusters.NativePlugins.Sharing/<ShareScreenShot>c__AnonStorey10::_onCompletion
	SharingCompletion_t8_271 * ____onCompletion_2;
	// VoxelBusters.NativePlugins.Sharing VoxelBusters.NativePlugins.Sharing/<ShareScreenShot>c__AnonStorey10::<>f__this
	Sharing_t8_273 * ___U3CU3Ef__this_3;
};
