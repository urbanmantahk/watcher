﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.DelegateData
struct DelegateData_t1_23;

#include "codegen/il2cpp-codegen.h"

// System.Void System.DelegateData::.ctor()
extern "C" void DelegateData__ctor_m1_14001 (DelegateData_t1_23 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
