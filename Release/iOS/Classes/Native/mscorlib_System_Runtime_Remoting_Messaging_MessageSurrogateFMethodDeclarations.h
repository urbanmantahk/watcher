﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Messaging.MessageSurrogateFilter
struct MessageSurrogateFilter_t1_958;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_String.h"

// System.Void System.Runtime.Remoting.Messaging.MessageSurrogateFilter::.ctor(System.Object,System.IntPtr)
extern "C" void MessageSurrogateFilter__ctor_m1_14811 (MessageSurrogateFilter_t1_958 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Remoting.Messaging.MessageSurrogateFilter::Invoke(System.String,System.Object)
extern "C" bool MessageSurrogateFilter_Invoke_m1_14812 (MessageSurrogateFilter_t1_958 * __this, String_t* ___key, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" bool pinvoke_delegate_wrapper_MessageSurrogateFilter_t1_958(Il2CppObject* delegate, String_t* ___key, Object_t * ___value);
// System.IAsyncResult System.Runtime.Remoting.Messaging.MessageSurrogateFilter::BeginInvoke(System.String,System.Object,System.AsyncCallback,System.Object)
extern "C" Object_t * MessageSurrogateFilter_BeginInvoke_m1_14813 (MessageSurrogateFilter_t1_958 * __this, String_t* ___key, Object_t * ___value, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Remoting.Messaging.MessageSurrogateFilter::EndInvoke(System.IAsyncResult)
extern "C" bool MessageSurrogateFilter_EndInvoke_m1_14814 (MessageSurrogateFilter_t1_958 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
