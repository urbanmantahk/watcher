﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Security_AccessControl_ControlFlags.h"

// System.Security.AccessControl.ControlFlags
struct  ControlFlags_t1_1138 
{
	// System.Int32 System.Security.AccessControl.ControlFlags::value__
	int32_t ___value___1;
};
