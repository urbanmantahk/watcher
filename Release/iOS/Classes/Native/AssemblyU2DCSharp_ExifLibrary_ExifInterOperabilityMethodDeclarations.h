﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifInterOperability.h"

// System.Void ExifLibrary.ExifInterOperability::.ctor(System.UInt16,System.UInt16,System.UInt32,System.Byte[])
extern "C" void ExifInterOperability__ctor_m8_496 (ExifInterOperability_t8_113 * __this, uint16_t ___tagid, uint16_t ___typeid, uint32_t ___count, ByteU5BU5D_t1_109* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 ExifLibrary.ExifInterOperability::get_TagID()
extern "C" uint16_t ExifInterOperability_get_TagID_m8_497 (ExifInterOperability_t8_113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 ExifLibrary.ExifInterOperability::get_TypeID()
extern "C" uint16_t ExifInterOperability_get_TypeID_m8_498 (ExifInterOperability_t8_113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 ExifLibrary.ExifInterOperability::get_Count()
extern "C" uint32_t ExifInterOperability_get_Count_m8_499 (ExifInterOperability_t8_113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ExifLibrary.ExifInterOperability::get_Data()
extern "C" ByteU5BU5D_t1_109* ExifInterOperability_get_Data_m8_500 (ExifInterOperability_t8_113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExifLibrary.ExifInterOperability::ToString()
extern "C" String_t* ExifInterOperability_ToString_m8_501 (ExifInterOperability_t8_113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void ExifInterOperability_t8_113_marshal(const ExifInterOperability_t8_113& unmarshaled, ExifInterOperability_t8_113_marshaled& marshaled);
extern "C" void ExifInterOperability_t8_113_marshal_back(const ExifInterOperability_t8_113_marshaled& marshaled, ExifInterOperability_t8_113& unmarshaled);
extern "C" void ExifInterOperability_t8_113_marshal_cleanup(ExifInterOperability_t8_113_marshaled& marshaled);
