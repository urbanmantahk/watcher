﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object.h"

// VoxelBusters.NativePlugins.CrossPlatformNotification/iOSSpecificProperties
struct  iOSSpecificProperties_t8_266  : public Object_t
{
	// System.String VoxelBusters.NativePlugins.CrossPlatformNotification/iOSSpecificProperties::<AlertAction>k__BackingField
	String_t* ___U3CAlertActionU3Ek__BackingField_4;
	// System.Boolean VoxelBusters.NativePlugins.CrossPlatformNotification/iOSSpecificProperties::<HasAction>k__BackingField
	bool ___U3CHasActionU3Ek__BackingField_5;
	// System.Int32 VoxelBusters.NativePlugins.CrossPlatformNotification/iOSSpecificProperties::<BadgeCount>k__BackingField
	int32_t ___U3CBadgeCountU3Ek__BackingField_6;
	// System.String VoxelBusters.NativePlugins.CrossPlatformNotification/iOSSpecificProperties::<LaunchImage>k__BackingField
	String_t* ___U3CLaunchImageU3Ek__BackingField_7;
};
