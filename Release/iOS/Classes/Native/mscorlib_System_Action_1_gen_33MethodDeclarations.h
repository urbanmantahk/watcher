﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Action_1_gen_7MethodDeclarations.h"

// System.Void System.Action`1<UnityEngine.EventSystems.BaseInputModule>::.ctor(System.Object,System.IntPtr)
#define Action_1__ctor_m1_22114(__this, ___object, ___method, method) (( void (*) (Action_1_t1_2455 *, Object_t *, IntPtr_t, const MethodInfo*))Action_1__ctor_m1_15358_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`1<UnityEngine.EventSystems.BaseInputModule>::Invoke(T)
#define Action_1_Invoke_m1_22115(__this, ___obj, method) (( void (*) (Action_1_t1_2455 *, BaseInputModule_t7_4 *, const MethodInfo*))Action_1_Invoke_m1_15359_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Action`1<UnityEngine.EventSystems.BaseInputModule>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Action_1_BeginInvoke_m1_22116(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Action_1_t1_2455 *, BaseInputModule_t7_4 *, AsyncCallback_t1_28 *, Object_t *, const MethodInfo*))Action_1_BeginInvoke_m1_15360_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Void System.Action`1<UnityEngine.EventSystems.BaseInputModule>::EndInvoke(System.IAsyncResult)
#define Action_1_EndInvoke_m1_22117(__this, ___result, method) (( void (*) (Action_1_t1_2455 *, Object_t *, const MethodInfo*))Action_1_EndInvoke_m1_15361_gshared)(__this, ___result, method)
