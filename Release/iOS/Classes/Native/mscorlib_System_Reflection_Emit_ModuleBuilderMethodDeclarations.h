﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.Emit.ModuleBuilder
struct ModuleBuilder_t1_475;
// System.Reflection.Emit.AssemblyBuilder
struct AssemblyBuilder_t1_466;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Reflection.Emit.FieldBuilder
struct FieldBuilder_t1_499;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Reflection.Emit.MethodBuilder
struct MethodBuilder_t1_501;
// System.Type[]
struct TypeU5BU5D_t1_31;
// System.Type[][]
struct TypeU5BU5DU5BU5D_t1_483;
// System.Reflection.Emit.TypeBuilder
struct TypeBuilder_t1_481;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Reflection.Emit.EnumBuilder
struct EnumBuilder_t1_498;
// System.Reflection.Emit.TypeBuilder[]
struct TypeBuilderU5BU5D_t1_533;
// System.Object
struct Object_t;
// System.Reflection.Emit.CustomAttributeBuilder
struct CustomAttributeBuilder_t1_487;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t1_478;
// System.Diagnostics.SymbolStore.ISymbolWriter
struct ISymbolWriter_t1_536;
// System.Diagnostics.SymbolStore.ISymbolDocumentWriter
struct ISymbolDocumentWriter_t1_525;
// System.Resources.IResourceWriter
struct IResourceWriter_t1_1684;
// System.IO.Stream
struct Stream_t1_405;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// System.Reflection.Emit.SignatureHelper
struct SignatureHelper_t1_551;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Reflection.Emit.TokenGenerator
struct TokenGenerator_t1_523;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Guid.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_Reflection_FieldAttributes.h"
#include "mscorlib_System_Reflection_MethodAttributes.h"
#include "mscorlib_System_Reflection_CallingConventions.h"
#include "mscorlib_System_Runtime_InteropServices_CallingConvention.h"
#include "mscorlib_System_Runtime_InteropServices_CharSet.h"
#include "mscorlib_System_Reflection_TypeAttributes.h"
#include "mscorlib_System_Reflection_Emit_PackingSize.h"
#include "mscorlib_System_Reflection_ResourceAttributes.h"
#include "mscorlib_System_Reflection_Emit_MethodToken.h"
#include "mscorlib_System_Reflection_Emit_FieldToken.h"
#include "mscorlib_System_Reflection_Emit_SignatureToken.h"
#include "mscorlib_System_Reflection_Emit_StringToken.h"
#include "mscorlib_System_Reflection_Emit_TypeToken.h"

// System.Void System.Reflection.Emit.ModuleBuilder::.ctor(System.Reflection.Emit.AssemblyBuilder,System.String,System.String,System.Boolean,System.Boolean)
extern "C" void ModuleBuilder__ctor_m1_6155 (ModuleBuilder_t1_475 * __this, AssemblyBuilder_t1_466 * ___assb, String_t* ___name, String_t* ___fullyqname, bool ___emitSymbolInfo, bool ___transient, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ModuleBuilder::.cctor()
extern "C" void ModuleBuilder__cctor_m1_6156 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ModuleBuilder::System.Runtime.InteropServices._ModuleBuilder.GetIDsOfNames(System.Guid&,System.IntPtr,System.UInt32,System.UInt32,System.IntPtr)
extern "C" void ModuleBuilder_System_Runtime_InteropServices__ModuleBuilder_GetIDsOfNames_m1_6157 (ModuleBuilder_t1_475 * __this, Guid_t1_319 * ___riid, IntPtr_t ___rgszNames, uint32_t ___cNames, uint32_t ___lcid, IntPtr_t ___rgDispId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ModuleBuilder::System.Runtime.InteropServices._ModuleBuilder.GetTypeInfo(System.UInt32,System.UInt32,System.IntPtr)
extern "C" void ModuleBuilder_System_Runtime_InteropServices__ModuleBuilder_GetTypeInfo_m1_6158 (ModuleBuilder_t1_475 * __this, uint32_t ___iTInfo, uint32_t ___lcid, IntPtr_t ___ppTInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ModuleBuilder::System.Runtime.InteropServices._ModuleBuilder.GetTypeInfoCount(System.UInt32&)
extern "C" void ModuleBuilder_System_Runtime_InteropServices__ModuleBuilder_GetTypeInfoCount_m1_6159 (ModuleBuilder_t1_475 * __this, uint32_t* ___pcTInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ModuleBuilder::System.Runtime.InteropServices._ModuleBuilder.Invoke(System.UInt32,System.Guid&,System.UInt32,System.Int16,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr)
extern "C" void ModuleBuilder_System_Runtime_InteropServices__ModuleBuilder_Invoke_m1_6160 (ModuleBuilder_t1_475 * __this, uint32_t ___dispIdMember, Guid_t1_319 * ___riid, uint32_t ___lcid, int16_t ___wFlags, IntPtr_t ___pDispParams, IntPtr_t ___pVarResult, IntPtr_t ___pExcepInfo, IntPtr_t ___puArgErr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ModuleBuilder::basic_init(System.Reflection.Emit.ModuleBuilder)
extern "C" void ModuleBuilder_basic_init_m1_6161 (Object_t * __this /* static, unused */, ModuleBuilder_t1_475 * ___ab, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ModuleBuilder::set_wrappers_type(System.Reflection.Emit.ModuleBuilder,System.Type)
extern "C" void ModuleBuilder_set_wrappers_type_m1_6162 (Object_t * __this /* static, unused */, ModuleBuilder_t1_475 * ___mb, Type_t * ___ab, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Emit.ModuleBuilder::get_FullyQualifiedName()
extern "C" String_t* ModuleBuilder_get_FullyQualifiedName_m1_6163 (ModuleBuilder_t1_475 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.ModuleBuilder::IsTransient()
extern "C" bool ModuleBuilder_IsTransient_m1_6164 (ModuleBuilder_t1_475 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ModuleBuilder::CreateGlobalFunctions()
extern "C" void ModuleBuilder_CreateGlobalFunctions_m1_6165 (ModuleBuilder_t1_475 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.FieldBuilder System.Reflection.Emit.ModuleBuilder::DefineInitializedData(System.String,System.Byte[],System.Reflection.FieldAttributes)
extern "C" FieldBuilder_t1_499 * ModuleBuilder_DefineInitializedData_m1_6166 (ModuleBuilder_t1_475 * __this, String_t* ___name, ByteU5BU5D_t1_109* ___data, int32_t ___attributes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.FieldBuilder System.Reflection.Emit.ModuleBuilder::DefineUninitializedData(System.String,System.Int32,System.Reflection.FieldAttributes)
extern "C" FieldBuilder_t1_499 * ModuleBuilder_DefineUninitializedData_m1_6167 (ModuleBuilder_t1_475 * __this, String_t* ___name, int32_t ___size, int32_t ___attributes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ModuleBuilder::addGlobalMethod(System.Reflection.Emit.MethodBuilder)
extern "C" void ModuleBuilder_addGlobalMethod_m1_6168 (ModuleBuilder_t1_475 * __this, MethodBuilder_t1_501 * ___mb, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.MethodBuilder System.Reflection.Emit.ModuleBuilder::DefineGlobalMethod(System.String,System.Reflection.MethodAttributes,System.Type,System.Type[])
extern "C" MethodBuilder_t1_501 * ModuleBuilder_DefineGlobalMethod_m1_6169 (ModuleBuilder_t1_475 * __this, String_t* ___name, int32_t ___attributes, Type_t * ___returnType, TypeU5BU5D_t1_31* ___parameterTypes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.MethodBuilder System.Reflection.Emit.ModuleBuilder::DefineGlobalMethod(System.String,System.Reflection.MethodAttributes,System.Reflection.CallingConventions,System.Type,System.Type[])
extern "C" MethodBuilder_t1_501 * ModuleBuilder_DefineGlobalMethod_m1_6170 (ModuleBuilder_t1_475 * __this, String_t* ___name, int32_t ___attributes, int32_t ___callingConvention, Type_t * ___returnType, TypeU5BU5D_t1_31* ___parameterTypes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.MethodBuilder System.Reflection.Emit.ModuleBuilder::DefineGlobalMethod(System.String,System.Reflection.MethodAttributes,System.Reflection.CallingConventions,System.Type,System.Type[],System.Type[],System.Type[],System.Type[][],System.Type[][])
extern "C" MethodBuilder_t1_501 * ModuleBuilder_DefineGlobalMethod_m1_6171 (ModuleBuilder_t1_475 * __this, String_t* ___name, int32_t ___attributes, int32_t ___callingConvention, Type_t * ___returnType, TypeU5BU5D_t1_31* ___requiredReturnTypeCustomModifiers, TypeU5BU5D_t1_31* ___optionalReturnTypeCustomModifiers, TypeU5BU5D_t1_31* ___parameterTypes, TypeU5BU5DU5BU5D_t1_483* ___requiredParameterTypeCustomModifiers, TypeU5BU5DU5BU5D_t1_483* ___optionalParameterTypeCustomModifiers, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.MethodBuilder System.Reflection.Emit.ModuleBuilder::DefinePInvokeMethod(System.String,System.String,System.Reflection.MethodAttributes,System.Reflection.CallingConventions,System.Type,System.Type[],System.Runtime.InteropServices.CallingConvention,System.Runtime.InteropServices.CharSet)
extern "C" MethodBuilder_t1_501 * ModuleBuilder_DefinePInvokeMethod_m1_6172 (ModuleBuilder_t1_475 * __this, String_t* ___name, String_t* ___dllName, int32_t ___attributes, int32_t ___callingConvention, Type_t * ___returnType, TypeU5BU5D_t1_31* ___parameterTypes, int32_t ___nativeCallConv, int32_t ___nativeCharSet, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.MethodBuilder System.Reflection.Emit.ModuleBuilder::DefinePInvokeMethod(System.String,System.String,System.String,System.Reflection.MethodAttributes,System.Reflection.CallingConventions,System.Type,System.Type[],System.Runtime.InteropServices.CallingConvention,System.Runtime.InteropServices.CharSet)
extern "C" MethodBuilder_t1_501 * ModuleBuilder_DefinePInvokeMethod_m1_6173 (ModuleBuilder_t1_475 * __this, String_t* ___name, String_t* ___dllName, String_t* ___entryName, int32_t ___attributes, int32_t ___callingConvention, Type_t * ___returnType, TypeU5BU5D_t1_31* ___parameterTypes, int32_t ___nativeCallConv, int32_t ___nativeCharSet, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.TypeBuilder System.Reflection.Emit.ModuleBuilder::DefineType(System.String)
extern "C" TypeBuilder_t1_481 * ModuleBuilder_DefineType_m1_6174 (ModuleBuilder_t1_475 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.TypeBuilder System.Reflection.Emit.ModuleBuilder::DefineType(System.String,System.Reflection.TypeAttributes)
extern "C" TypeBuilder_t1_481 * ModuleBuilder_DefineType_m1_6175 (ModuleBuilder_t1_475 * __this, String_t* ___name, int32_t ___attr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.TypeBuilder System.Reflection.Emit.ModuleBuilder::DefineType(System.String,System.Reflection.TypeAttributes,System.Type)
extern "C" TypeBuilder_t1_481 * ModuleBuilder_DefineType_m1_6176 (ModuleBuilder_t1_475 * __this, String_t* ___name, int32_t ___attr, Type_t * ___parent, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ModuleBuilder::AddType(System.Reflection.Emit.TypeBuilder)
extern "C" void ModuleBuilder_AddType_m1_6177 (ModuleBuilder_t1_475 * __this, TypeBuilder_t1_481 * ___tb, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.TypeBuilder System.Reflection.Emit.ModuleBuilder::DefineType(System.String,System.Reflection.TypeAttributes,System.Type,System.Type[],System.Reflection.Emit.PackingSize,System.Int32)
extern "C" TypeBuilder_t1_481 * ModuleBuilder_DefineType_m1_6178 (ModuleBuilder_t1_475 * __this, String_t* ___name, int32_t ___attr, Type_t * ___parent, TypeU5BU5D_t1_31* ___interfaces, int32_t ___packingSize, int32_t ___typesize, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ModuleBuilder::RegisterTypeName(System.Reflection.Emit.TypeBuilder,System.String)
extern "C" void ModuleBuilder_RegisterTypeName_m1_6179 (ModuleBuilder_t1_475 * __this, TypeBuilder_t1_481 * ___tb, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.TypeBuilder System.Reflection.Emit.ModuleBuilder::GetRegisteredType(System.String)
extern "C" TypeBuilder_t1_481 * ModuleBuilder_GetRegisteredType_m1_6180 (ModuleBuilder_t1_475 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.TypeBuilder System.Reflection.Emit.ModuleBuilder::DefineType(System.String,System.Reflection.TypeAttributes,System.Type,System.Type[])
extern "C" TypeBuilder_t1_481 * ModuleBuilder_DefineType_m1_6181 (ModuleBuilder_t1_475 * __this, String_t* ___name, int32_t ___attr, Type_t * ___parent, TypeU5BU5D_t1_31* ___interfaces, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.TypeBuilder System.Reflection.Emit.ModuleBuilder::DefineType(System.String,System.Reflection.TypeAttributes,System.Type,System.Int32)
extern "C" TypeBuilder_t1_481 * ModuleBuilder_DefineType_m1_6182 (ModuleBuilder_t1_475 * __this, String_t* ___name, int32_t ___attr, Type_t * ___parent, int32_t ___typesize, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.TypeBuilder System.Reflection.Emit.ModuleBuilder::DefineType(System.String,System.Reflection.TypeAttributes,System.Type,System.Reflection.Emit.PackingSize)
extern "C" TypeBuilder_t1_481 * ModuleBuilder_DefineType_m1_6183 (ModuleBuilder_t1_475 * __this, String_t* ___name, int32_t ___attr, Type_t * ___parent, int32_t ___packsize, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.TypeBuilder System.Reflection.Emit.ModuleBuilder::DefineType(System.String,System.Reflection.TypeAttributes,System.Type,System.Reflection.Emit.PackingSize,System.Int32)
extern "C" TypeBuilder_t1_481 * ModuleBuilder_DefineType_m1_6184 (ModuleBuilder_t1_475 * __this, String_t* ___name, int32_t ___attr, Type_t * ___parent, int32_t ___packingSize, int32_t ___typesize, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Reflection.Emit.ModuleBuilder::GetArrayMethod(System.Type,System.String,System.Reflection.CallingConventions,System.Type,System.Type[])
extern "C" MethodInfo_t * ModuleBuilder_GetArrayMethod_m1_6185 (ModuleBuilder_t1_475 * __this, Type_t * ___arrayClass, String_t* ___methodName, int32_t ___callingConvention, Type_t * ___returnType, TypeU5BU5D_t1_31* ___parameterTypes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.EnumBuilder System.Reflection.Emit.ModuleBuilder::DefineEnum(System.String,System.Reflection.TypeAttributes,System.Type)
extern "C" EnumBuilder_t1_498 * ModuleBuilder_DefineEnum_m1_6186 (ModuleBuilder_t1_475 * __this, String_t* ___name, int32_t ___visibility, Type_t * ___underlyingType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.ModuleBuilder::GetType(System.String)
extern "C" Type_t * ModuleBuilder_GetType_m1_6187 (ModuleBuilder_t1_475 * __this, String_t* ___className, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.ModuleBuilder::GetType(System.String,System.Boolean)
extern "C" Type_t * ModuleBuilder_GetType_m1_6188 (ModuleBuilder_t1_475 * __this, String_t* ___className, bool ___ignoreCase, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.TypeBuilder System.Reflection.Emit.ModuleBuilder::search_in_array(System.Reflection.Emit.TypeBuilder[],System.Int32,System.String)
extern "C" TypeBuilder_t1_481 * ModuleBuilder_search_in_array_m1_6189 (ModuleBuilder_t1_475 * __this, TypeBuilderU5BU5D_t1_533* ___arr, int32_t ___validElementsInArray, String_t* ___className, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.TypeBuilder System.Reflection.Emit.ModuleBuilder::search_nested_in_array(System.Reflection.Emit.TypeBuilder[],System.Int32,System.String)
extern "C" TypeBuilder_t1_481 * ModuleBuilder_search_nested_in_array_m1_6190 (ModuleBuilder_t1_475 * __this, TypeBuilderU5BU5D_t1_533* ___arr, int32_t ___validElementsInArray, String_t* ___className, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.ModuleBuilder::create_modified_type(System.Reflection.Emit.TypeBuilder,System.String)
extern "C" Type_t * ModuleBuilder_create_modified_type_m1_6191 (Object_t * __this /* static, unused */, TypeBuilder_t1_481 * ___tb, String_t* ___modifiers, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.TypeBuilder System.Reflection.Emit.ModuleBuilder::GetMaybeNested(System.Reflection.Emit.TypeBuilder,System.String)
extern "C" TypeBuilder_t1_481 * ModuleBuilder_GetMaybeNested_m1_6192 (ModuleBuilder_t1_475 * __this, TypeBuilder_t1_481 * ___t, String_t* ___className, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.ModuleBuilder::GetType(System.String,System.Boolean,System.Boolean)
extern "C" Type_t * ModuleBuilder_GetType_m1_6193 (ModuleBuilder_t1_475 * __this, String_t* ___className, bool ___throwOnError, bool ___ignoreCase, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.ModuleBuilder::get_next_table_index(System.Object,System.Int32,System.Boolean)
extern "C" int32_t ModuleBuilder_get_next_table_index_m1_6194 (ModuleBuilder_t1_475 * __this, Object_t * ___obj, int32_t ___table, bool ___inc, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ModuleBuilder::SetCustomAttribute(System.Reflection.Emit.CustomAttributeBuilder)
extern "C" void ModuleBuilder_SetCustomAttribute_m1_6195 (ModuleBuilder_t1_475 * __this, CustomAttributeBuilder_t1_487 * ___customBuilder, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ModuleBuilder::SetCustomAttribute(System.Reflection.ConstructorInfo,System.Byte[])
extern "C" void ModuleBuilder_SetCustomAttribute_m1_6196 (ModuleBuilder_t1_475 * __this, ConstructorInfo_t1_478 * ___con, ByteU5BU5D_t1_109* ___binaryAttribute, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Diagnostics.SymbolStore.ISymbolWriter System.Reflection.Emit.ModuleBuilder::GetSymWriter()
extern "C" Object_t * ModuleBuilder_GetSymWriter_m1_6197 (ModuleBuilder_t1_475 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Diagnostics.SymbolStore.ISymbolDocumentWriter System.Reflection.Emit.ModuleBuilder::DefineDocument(System.String,System.Guid,System.Guid,System.Guid)
extern "C" Object_t * ModuleBuilder_DefineDocument_m1_6198 (ModuleBuilder_t1_475 * __this, String_t* ___url, Guid_t1_319  ___language, Guid_t1_319  ___languageVendor, Guid_t1_319  ___documentType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type[] System.Reflection.Emit.ModuleBuilder::GetTypes()
extern "C" TypeU5BU5D_t1_31* ModuleBuilder_GetTypes_m1_6199 (ModuleBuilder_t1_475 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Resources.IResourceWriter System.Reflection.Emit.ModuleBuilder::DefineResource(System.String,System.String,System.Reflection.ResourceAttributes)
extern "C" Object_t * ModuleBuilder_DefineResource_m1_6200 (ModuleBuilder_t1_475 * __this, String_t* ___name, String_t* ___description, int32_t ___attribute, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Resources.IResourceWriter System.Reflection.Emit.ModuleBuilder::DefineResource(System.String,System.String)
extern "C" Object_t * ModuleBuilder_DefineResource_m1_6201 (ModuleBuilder_t1_475 * __this, String_t* ___name, String_t* ___description, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ModuleBuilder::DefineUnmanagedResource(System.Byte[])
extern "C" void ModuleBuilder_DefineUnmanagedResource_m1_6202 (ModuleBuilder_t1_475 * __this, ByteU5BU5D_t1_109* ___resource, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ModuleBuilder::DefineUnmanagedResource(System.String)
extern "C" void ModuleBuilder_DefineUnmanagedResource_m1_6203 (ModuleBuilder_t1_475 * __this, String_t* ___resourceFileName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ModuleBuilder::DefineManifestResource(System.String,System.IO.Stream,System.Reflection.ResourceAttributes)
extern "C" void ModuleBuilder_DefineManifestResource_m1_6204 (ModuleBuilder_t1_475 * __this, String_t* ___name, Stream_t1_405 * ___stream, int32_t ___attribute, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ModuleBuilder::SetSymCustomAttribute(System.String,System.Byte[])
extern "C" void ModuleBuilder_SetSymCustomAttribute_m1_6205 (ModuleBuilder_t1_475 * __this, String_t* ___name, ByteU5BU5D_t1_109* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ModuleBuilder::SetUserEntryPoint(System.Reflection.MethodInfo)
extern "C" void ModuleBuilder_SetUserEntryPoint_m1_6206 (ModuleBuilder_t1_475 * __this, MethodInfo_t * ___entryPoint, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.MethodToken System.Reflection.Emit.ModuleBuilder::GetMethodToken(System.Reflection.MethodInfo)
extern "C" MethodToken_t1_532  ModuleBuilder_GetMethodToken_m1_6207 (ModuleBuilder_t1_475 * __this, MethodInfo_t * ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.MethodToken System.Reflection.Emit.ModuleBuilder::GetArrayMethodToken(System.Type,System.String,System.Reflection.CallingConventions,System.Type,System.Type[])
extern "C" MethodToken_t1_532  ModuleBuilder_GetArrayMethodToken_m1_6208 (ModuleBuilder_t1_475 * __this, Type_t * ___arrayClass, String_t* ___methodName, int32_t ___callingConvention, Type_t * ___returnType, TypeU5BU5D_t1_31* ___parameterTypes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.MethodToken System.Reflection.Emit.ModuleBuilder::GetConstructorToken(System.Reflection.ConstructorInfo)
extern "C" MethodToken_t1_532  ModuleBuilder_GetConstructorToken_m1_6209 (ModuleBuilder_t1_475 * __this, ConstructorInfo_t1_478 * ___con, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.FieldToken System.Reflection.Emit.ModuleBuilder::GetFieldToken(System.Reflection.FieldInfo)
extern "C" FieldToken_t1_507  ModuleBuilder_GetFieldToken_m1_6210 (ModuleBuilder_t1_475 * __this, FieldInfo_t * ___field, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.SignatureToken System.Reflection.Emit.ModuleBuilder::GetSignatureToken(System.Byte[],System.Int32)
extern "C" SignatureToken_t1_552  ModuleBuilder_GetSignatureToken_m1_6211 (ModuleBuilder_t1_475 * __this, ByteU5BU5D_t1_109* ___sigBytes, int32_t ___sigLength, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.SignatureToken System.Reflection.Emit.ModuleBuilder::GetSignatureToken(System.Reflection.Emit.SignatureHelper)
extern "C" SignatureToken_t1_552  ModuleBuilder_GetSignatureToken_m1_6212 (ModuleBuilder_t1_475 * __this, SignatureHelper_t1_551 * ___sigHelper, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.StringToken System.Reflection.Emit.ModuleBuilder::GetStringConstant(System.String)
extern "C" StringToken_t1_554  ModuleBuilder_GetStringConstant_m1_6213 (ModuleBuilder_t1_475 * __this, String_t* ___str, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.TypeToken System.Reflection.Emit.ModuleBuilder::GetTypeToken(System.Type)
extern "C" TypeToken_t1_558  ModuleBuilder_GetTypeToken_m1_6214 (ModuleBuilder_t1_475 * __this, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.TypeToken System.Reflection.Emit.ModuleBuilder::GetTypeToken(System.String)
extern "C" TypeToken_t1_558  ModuleBuilder_GetTypeToken_m1_6215 (ModuleBuilder_t1_475 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.ModuleBuilder::getUSIndex(System.Reflection.Emit.ModuleBuilder,System.String)
extern "C" int32_t ModuleBuilder_getUSIndex_m1_6216 (Object_t * __this /* static, unused */, ModuleBuilder_t1_475 * ___mb, String_t* ___str, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.ModuleBuilder::getToken(System.Reflection.Emit.ModuleBuilder,System.Object)
extern "C" int32_t ModuleBuilder_getToken_m1_6217 (Object_t * __this /* static, unused */, ModuleBuilder_t1_475 * ___mb, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.ModuleBuilder::getMethodToken(System.Reflection.Emit.ModuleBuilder,System.Reflection.MethodInfo,System.Type[])
extern "C" int32_t ModuleBuilder_getMethodToken_m1_6218 (Object_t * __this /* static, unused */, ModuleBuilder_t1_475 * ___mb, MethodInfo_t * ___method, TypeU5BU5D_t1_31* ___opt_param_types, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.ModuleBuilder::GetToken(System.String)
extern "C" int32_t ModuleBuilder_GetToken_m1_6219 (ModuleBuilder_t1_475 * __this, String_t* ___str, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.ModuleBuilder::GetToken(System.Reflection.MemberInfo)
extern "C" int32_t ModuleBuilder_GetToken_m1_6220 (ModuleBuilder_t1_475 * __this, MemberInfo_t * ___member, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.ModuleBuilder::GetToken(System.Reflection.MethodInfo,System.Type[])
extern "C" int32_t ModuleBuilder_GetToken_m1_6221 (ModuleBuilder_t1_475 * __this, MethodInfo_t * ___method, TypeU5BU5D_t1_31* ___opt_param_types, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.ModuleBuilder::GetToken(System.Reflection.Emit.SignatureHelper)
extern "C" int32_t ModuleBuilder_GetToken_m1_6222 (ModuleBuilder_t1_475 * __this, SignatureHelper_t1_551 * ___helper, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ModuleBuilder::RegisterToken(System.Object,System.Int32)
extern "C" void ModuleBuilder_RegisterToken_m1_6223 (ModuleBuilder_t1_475 * __this, Object_t * ___obj, int32_t ___token, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.TokenGenerator System.Reflection.Emit.ModuleBuilder::GetTokenGenerator()
extern "C" Object_t * ModuleBuilder_GetTokenGenerator_m1_6224 (ModuleBuilder_t1_475 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ModuleBuilder::build_metadata(System.Reflection.Emit.ModuleBuilder)
extern "C" void ModuleBuilder_build_metadata_m1_6225 (Object_t * __this /* static, unused */, ModuleBuilder_t1_475 * ___mb, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ModuleBuilder::WriteToFile(System.IntPtr)
extern "C" void ModuleBuilder_WriteToFile_m1_6226 (ModuleBuilder_t1_475 * __this, IntPtr_t ___handle, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ModuleBuilder::Save()
extern "C" void ModuleBuilder_Save_m1_6227 (ModuleBuilder_t1_475 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Emit.ModuleBuilder::get_FileName()
extern "C" String_t* ModuleBuilder_get_FileName_m1_6228 (ModuleBuilder_t1_475 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ModuleBuilder::set_IsMain(System.Boolean)
extern "C" void ModuleBuilder_set_IsMain_m1_6229 (ModuleBuilder_t1_475 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ModuleBuilder::CreateGlobalType()
extern "C" void ModuleBuilder_CreateGlobalType_m1_6230 (ModuleBuilder_t1_475 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Guid System.Reflection.Emit.ModuleBuilder::GetModuleVersionId()
extern "C" Guid_t1_319  ModuleBuilder_GetModuleVersionId_m1_6231 (ModuleBuilder_t1_475 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Guid System.Reflection.Emit.ModuleBuilder::Mono_GetGuid(System.Reflection.Emit.ModuleBuilder)
extern "C" Guid_t1_319  ModuleBuilder_Mono_GetGuid_m1_6232 (Object_t * __this /* static, unused */, ModuleBuilder_t1_475 * ___mb, const MethodInfo* method) IL2CPP_METHOD_ATTR;
