﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<VoxelBusters.DebugPRO.Internal.ConsoleLog>
struct DefaultComparer_t1_2728;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VoxelBusters_DebugPRO_Internal_ConsoleLog.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<VoxelBusters.DebugPRO.Internal.ConsoleLog>::.ctor()
extern "C" void DefaultComparer__ctor_m1_27285_gshared (DefaultComparer_t1_2728 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1_27285(__this, method) (( void (*) (DefaultComparer_t1_2728 *, const MethodInfo*))DefaultComparer__ctor_m1_27285_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<VoxelBusters.DebugPRO.Internal.ConsoleLog>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m1_27286_gshared (DefaultComparer_t1_2728 * __this, ConsoleLog_t8_170  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m1_27286(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t1_2728 *, ConsoleLog_t8_170 , const MethodInfo*))DefaultComparer_GetHashCode_m1_27286_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m1_27287_gshared (DefaultComparer_t1_2728 * __this, ConsoleLog_t8_170  ___x, ConsoleLog_t8_170  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m1_27287(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t1_2728 *, ConsoleLog_t8_170 , ConsoleLog_t8_170 , const MethodInfo*))DefaultComparer_Equals_m1_27287_gshared)(__this, ___x, ___y, method)
