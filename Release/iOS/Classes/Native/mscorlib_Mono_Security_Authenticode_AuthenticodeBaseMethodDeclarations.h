﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.Authenticode.AuthenticodeBase
struct AuthenticodeBase_t1_145;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Security.Cryptography.HashAlgorithm
struct HashAlgorithm_t1_162;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Security.Authenticode.AuthenticodeBase::.ctor()
extern "C" void AuthenticodeBase__ctor_m1_1860 (AuthenticodeBase_t1_145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Security.Authenticode.AuthenticodeBase::get_PEOffset()
extern "C" int32_t AuthenticodeBase_get_PEOffset_m1_1861 (AuthenticodeBase_t1_145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Security.Authenticode.AuthenticodeBase::get_CoffSymbolTableOffset()
extern "C" int32_t AuthenticodeBase_get_CoffSymbolTableOffset_m1_1862 (AuthenticodeBase_t1_145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Security.Authenticode.AuthenticodeBase::get_SecurityOffset()
extern "C" int32_t AuthenticodeBase_get_SecurityOffset_m1_1863 (AuthenticodeBase_t1_145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Authenticode.AuthenticodeBase::Open(System.String)
extern "C" void AuthenticodeBase_Open_m1_1864 (AuthenticodeBase_t1_145 * __this, String_t* ___filename, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Authenticode.AuthenticodeBase::Close()
extern "C" void AuthenticodeBase_Close_m1_1865 (AuthenticodeBase_t1_145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.Authenticode.AuthenticodeBase::ReadFirstBlock()
extern "C" bool AuthenticodeBase_ReadFirstBlock_m1_1866 (AuthenticodeBase_t1_145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Authenticode.AuthenticodeBase::GetSecurityEntry()
extern "C" ByteU5BU5D_t1_109* AuthenticodeBase_GetSecurityEntry_m1_1867 (AuthenticodeBase_t1_145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Authenticode.AuthenticodeBase::GetHash(System.Security.Cryptography.HashAlgorithm)
extern "C" ByteU5BU5D_t1_109* AuthenticodeBase_GetHash_m1_1868 (AuthenticodeBase_t1_145 * __this, HashAlgorithm_t1_162 * ___hash, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Authenticode.AuthenticodeBase::HashFile(System.String,System.String)
extern "C" ByteU5BU5D_t1_109* AuthenticodeBase_HashFile_m1_1869 (AuthenticodeBase_t1_145 * __this, String_t* ___fileName, String_t* ___hashName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
