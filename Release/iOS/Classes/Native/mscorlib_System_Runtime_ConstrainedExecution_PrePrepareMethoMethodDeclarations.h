﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.ConstrainedExecution.PrePrepareMethodAttribute
struct PrePrepareMethodAttribute_t1_714;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.ConstrainedExecution.PrePrepareMethodAttribute::.ctor()
extern "C" void PrePrepareMethodAttribute__ctor_m1_7560 (PrePrepareMethodAttribute_t1_714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
