﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Boomlagoon.JSON.JSONObject
struct JSONObject_t8_5;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.String
struct String_t;
// Boomlagoon.JSON.JSONValue
struct JSONValue_t8_4;
// Boomlagoon.JSON.JSONArray
struct JSONArray_t8_6;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,Boomlagoon.JSON.JSONValue>>
struct IEnumerator_1_t1_1916;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_2.h"

// System.Void Boomlagoon.JSON.JSONObject::.ctor()
extern "C" void JSONObject__ctor_m8_42 (JSONObject_t8_5 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Boomlagoon.JSON.JSONObject::.ctor(Boomlagoon.JSON.JSONObject)
extern "C" void JSONObject__ctor_m8_43 (JSONObject_t8_5 * __this, JSONObject_t8_5 * ___other, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Boomlagoon.JSON.JSONObject::.cctor()
extern "C" void JSONObject__cctor_m8_44 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Boomlagoon.JSON.JSONObject::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * JSONObject_System_Collections_IEnumerable_GetEnumerator_m8_45 (JSONObject_t8_5 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Boomlagoon.JSON.JSONObject::ContainsKey(System.String)
extern "C" bool JSONObject_ContainsKey_m8_46 (JSONObject_t8_5 * __this, String_t* ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Boomlagoon.JSON.JSONValue Boomlagoon.JSON.JSONObject::GetValue(System.String)
extern "C" JSONValue_t8_4 * JSONObject_GetValue_m8_47 (JSONObject_t8_5 * __this, String_t* ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Boomlagoon.JSON.JSONObject::GetString(System.String)
extern "C" String_t* JSONObject_GetString_m8_48 (JSONObject_t8_5 * __this, String_t* ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double Boomlagoon.JSON.JSONObject::GetNumber(System.String)
extern "C" double JSONObject_GetNumber_m8_49 (JSONObject_t8_5 * __this, String_t* ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Boomlagoon.JSON.JSONObject Boomlagoon.JSON.JSONObject::GetObject(System.String)
extern "C" JSONObject_t8_5 * JSONObject_GetObject_m8_50 (JSONObject_t8_5 * __this, String_t* ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Boomlagoon.JSON.JSONObject::GetBoolean(System.String)
extern "C" bool JSONObject_GetBoolean_m8_51 (JSONObject_t8_5 * __this, String_t* ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Boomlagoon.JSON.JSONArray Boomlagoon.JSON.JSONObject::GetArray(System.String)
extern "C" JSONArray_t8_6 * JSONObject_GetArray_m8_52 (JSONObject_t8_5 * __this, String_t* ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Boomlagoon.JSON.JSONValue Boomlagoon.JSON.JSONObject::get_Item(System.String)
extern "C" JSONValue_t8_4 * JSONObject_get_Item_m8_53 (JSONObject_t8_5 * __this, String_t* ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Boomlagoon.JSON.JSONObject::set_Item(System.String,Boomlagoon.JSON.JSONValue)
extern "C" void JSONObject_set_Item_m8_54 (JSONObject_t8_5 * __this, String_t* ___key, JSONValue_t8_4 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Boomlagoon.JSON.JSONObject::Add(System.String,Boomlagoon.JSON.JSONValue)
extern "C" void JSONObject_Add_m8_55 (JSONObject_t8_5 * __this, String_t* ___key, JSONValue_t8_4 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Boomlagoon.JSON.JSONObject::Add(System.Collections.Generic.KeyValuePair`2<System.String,Boomlagoon.JSON.JSONValue>)
extern "C" void JSONObject_Add_m8_56 (JSONObject_t8_5 * __this, KeyValuePair_2_t1_1915  ___pair, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Boomlagoon.JSON.JSONObject Boomlagoon.JSON.JSONObject::Parse(System.String)
extern "C" JSONObject_t8_5 * JSONObject_Parse_m8_57 (Object_t * __this /* static, unused */, String_t* ___jsonString, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Boomlagoon.JSON.JSONObject::SkipWhitespace(System.String,System.Int32)
extern "C" int32_t JSONObject_SkipWhitespace_m8_58 (Object_t * __this /* static, unused */, String_t* ___str, int32_t ___pos, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Boomlagoon.JSON.JSONObject::ParseString(System.String,System.Int32&)
extern "C" String_t* JSONObject_ParseString_m8_59 (Object_t * __this /* static, unused */, String_t* ___str, int32_t* ___startPosition, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double Boomlagoon.JSON.JSONObject::ParseNumber(System.String,System.Int32&)
extern "C" double JSONObject_ParseNumber_m8_60 (Object_t * __this /* static, unused */, String_t* ___str, int32_t* ___startPosition, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Boomlagoon.JSON.JSONObject Boomlagoon.JSON.JSONObject::Fail(System.Char,System.Int32)
extern "C" JSONObject_t8_5 * JSONObject_Fail_m8_61 (Object_t * __this /* static, unused */, uint16_t ___expected, int32_t ___position, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Boomlagoon.JSON.JSONObject Boomlagoon.JSON.JSONObject::Fail(System.String,System.Int32)
extern "C" JSONObject_t8_5 * JSONObject_Fail_m8_62 (Object_t * __this /* static, unused */, String_t* ___expected, int32_t ___position, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Boomlagoon.JSON.JSONObject::ToString()
extern "C" String_t* JSONObject_ToString_m8_63 (JSONObject_t8_5 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,Boomlagoon.JSON.JSONValue>> Boomlagoon.JSON.JSONObject::GetEnumerator()
extern "C" Object_t* JSONObject_GetEnumerator_m8_64 (JSONObject_t8_5 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Boomlagoon.JSON.JSONObject::Clear()
extern "C" void JSONObject_Clear_m8_65 (JSONObject_t8_5 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Boomlagoon.JSON.JSONObject::Remove(System.String)
extern "C" void JSONObject_Remove_m8_66 (JSONObject_t8_5 * __this, String_t* ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
