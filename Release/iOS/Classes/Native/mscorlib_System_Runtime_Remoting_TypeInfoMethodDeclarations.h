﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.TypeInfo
struct TypeInfo_t1_1038;
// System.Type
struct Type_t;
// System.String
struct String_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.TypeInfo::.ctor(System.Type)
extern "C" void TypeInfo__ctor_m1_9312 (TypeInfo_t1_1038 * __this, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.TypeInfo::get_TypeName()
extern "C" String_t* TypeInfo_get_TypeName_m1_9313 (TypeInfo_t1_1038 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.TypeInfo::set_TypeName(System.String)
extern "C" void TypeInfo_set_TypeName_m1_9314 (TypeInfo_t1_1038 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Remoting.TypeInfo::CanCastTo(System.Type,System.Object)
extern "C" bool TypeInfo_CanCastTo_m1_9315 (TypeInfo_t1_1038 * __this, Type_t * ___fromType, Object_t * ___o, const MethodInfo* method) IL2CPP_METHOD_ATTR;
