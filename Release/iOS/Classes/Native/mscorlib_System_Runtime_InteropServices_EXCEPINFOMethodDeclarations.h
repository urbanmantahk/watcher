﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

extern "C" void EXCEPINFO_t1_787_marshal(const EXCEPINFO_t1_787& unmarshaled, EXCEPINFO_t1_787_marshaled& marshaled);
extern "C" void EXCEPINFO_t1_787_marshal_back(const EXCEPINFO_t1_787_marshaled& marshaled, EXCEPINFO_t1_787& unmarshaled);
extern "C" void EXCEPINFO_t1_787_marshal_cleanup(EXCEPINFO_t1_787_marshaled& marshaled);
