﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapHexBinary
struct SoapHexBinary_t1_974;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.String
struct String_t;
// System.Exception
struct Exception_t1_33;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapHexBinary::.ctor()
extern "C" void SoapHexBinary__ctor_m1_8731 (SoapHexBinary_t1_974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapHexBinary::.ctor(System.Byte[])
extern "C" void SoapHexBinary__ctor_m1_8732 (SoapHexBinary_t1_974 * __this, ByteU5BU5D_t1_109* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Runtime.Remoting.Metadata.W3cXsd2001.SoapHexBinary::get_Value()
extern "C" ByteU5BU5D_t1_109* SoapHexBinary_get_Value_m1_8733 (SoapHexBinary_t1_974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapHexBinary::set_Value(System.Byte[])
extern "C" void SoapHexBinary_set_Value_m1_8734 (SoapHexBinary_t1_974 * __this, ByteU5BU5D_t1_109* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapHexBinary::get_XsdType()
extern "C" String_t* SoapHexBinary_get_XsdType_m1_8735 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapHexBinary::GetXsdType()
extern "C" String_t* SoapHexBinary_GetXsdType_m1_8736 (SoapHexBinary_t1_974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapHexBinary System.Runtime.Remoting.Metadata.W3cXsd2001.SoapHexBinary::Parse(System.String)
extern "C" SoapHexBinary_t1_974 * SoapHexBinary_Parse_m1_8737 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Runtime.Remoting.Metadata.W3cXsd2001.SoapHexBinary::FromBinHexString(System.String)
extern "C" ByteU5BU5D_t1_109* SoapHexBinary_FromBinHexString_m1_8738 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte System.Runtime.Remoting.Metadata.W3cXsd2001.SoapHexBinary::FromHex(System.Char,System.String)
extern "C" uint8_t SoapHexBinary_FromHex_m1_8739 (Object_t * __this /* static, unused */, uint16_t ___hexDigit, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception System.Runtime.Remoting.Metadata.W3cXsd2001.SoapHexBinary::CreateInvalidValueException(System.String)
extern "C" Exception_t1_33 * SoapHexBinary_CreateInvalidValueException_m1_8740 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapHexBinary::ToString()
extern "C" String_t* SoapHexBinary_ToString_m1_8741 (SoapHexBinary_t1_974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
