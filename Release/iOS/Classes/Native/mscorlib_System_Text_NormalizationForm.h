﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Text_NormalizationForm.h"

// System.Text.NormalizationForm
struct  NormalizationForm_t1_1443 
{
	// System.Int32 System.Text.NormalizationForm::value__
	int32_t ___value___1;
};
