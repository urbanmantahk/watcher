﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

extern "C" void EXCEPINFO_t1_731_marshal(const EXCEPINFO_t1_731& unmarshaled, EXCEPINFO_t1_731_marshaled& marshaled);
extern "C" void EXCEPINFO_t1_731_marshal_back(const EXCEPINFO_t1_731_marshaled& marshaled, EXCEPINFO_t1_731& unmarshaled);
extern "C" void EXCEPINFO_t1_731_marshal_cleanup(EXCEPINFO_t1_731_marshaled& marshaled);
