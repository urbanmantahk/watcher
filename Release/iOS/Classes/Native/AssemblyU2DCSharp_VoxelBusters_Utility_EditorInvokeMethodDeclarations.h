﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.Utility.EditorInvoke
struct EditorInvoke_t8_150;
// System.Action
struct Action_t5_11;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.Utility.EditorInvoke::.ctor()
extern "C" void EditorInvoke__ctor_m8_870 (EditorInvoke_t8_150 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.EditorInvoke::.cctor()
extern "C" void EditorInvoke__cctor_m8_871 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.EditorInvoke::Invoke(System.Action,System.Single)
extern "C" void EditorInvoke_Invoke_m8_872 (Object_t * __this /* static, unused */, Action_t5_11 * ____method, float ____time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.EditorInvoke::InvokeRepeating(System.Action,System.Single,System.Single)
extern "C" void EditorInvoke_InvokeRepeating_m8_873 (Object_t * __this /* static, unused */, Action_t5_11 * ____method, float ____time, float ____repeatRate, const MethodInfo* method) IL2CPP_METHOD_ATTR;
