﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.IO.FileStream
struct FileStream_t1_146;

#include "codegen/il2cpp-codegen.h"

// System.Byte[] ExifLibrary.ExifExtensionMethods::ReadBytes(System.IO.FileStream,System.Int32)
extern "C" ByteU5BU5D_t1_109* ExifExtensionMethods_ReadBytes_m8_476 (Object_t * __this /* static, unused */, FileStream_t1_146 * ___stream, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.ExifExtensionMethods::WriteBytes(System.IO.FileStream,System.Byte[])
extern "C" void ExifExtensionMethods_WriteBytes_m8_477 (Object_t * __this /* static, unused */, FileStream_t1_146 * ___stream, ByteU5BU5D_t1_109* ___buffer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
