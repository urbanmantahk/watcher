﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.DisposerReplySink
struct DisposerReplySink_t1_1034;
// System.Runtime.Remoting.Messaging.IMessageSink
struct IMessageSink_t1_874;
// System.IDisposable
struct IDisposable_t1_1035;
// System.Runtime.Remoting.Messaging.IMessage
struct IMessage_t1_875;
// System.Runtime.Remoting.Messaging.IMessageCtrl
struct IMessageCtrl_t1_920;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.DisposerReplySink::.ctor(System.Runtime.Remoting.Messaging.IMessageSink,System.IDisposable)
extern "C" void DisposerReplySink__ctor_m1_9270 (DisposerReplySink_t1_1034 * __this, Object_t * ___next, Object_t * ___disposable, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Messaging.IMessage System.Runtime.Remoting.DisposerReplySink::SyncProcessMessage(System.Runtime.Remoting.Messaging.IMessage)
extern "C" Object_t * DisposerReplySink_SyncProcessMessage_m1_9271 (DisposerReplySink_t1_1034 * __this, Object_t * ___msg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Messaging.IMessageCtrl System.Runtime.Remoting.DisposerReplySink::AsyncProcessMessage(System.Runtime.Remoting.Messaging.IMessage,System.Runtime.Remoting.Messaging.IMessageSink)
extern "C" Object_t * DisposerReplySink_AsyncProcessMessage_m1_9272 (DisposerReplySink_t1_1034 * __this, Object_t * ___msg, Object_t * ___replySink, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Messaging.IMessageSink System.Runtime.Remoting.DisposerReplySink::get_NextSink()
extern "C" Object_t * DisposerReplySink_get_NextSink_m1_9273 (DisposerReplySink_t1_1034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
