﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_TYPEKIND.h"

// System.Runtime.InteropServices.ComTypes.TYPEKIND
struct  TYPEKIND_t1_745 
{
	// System.Int32 System.Runtime.InteropServices.ComTypes.TYPEKIND::value__
	int32_t ___value___1;
};
