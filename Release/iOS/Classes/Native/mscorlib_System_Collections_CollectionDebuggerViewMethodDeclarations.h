﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.CollectionDebuggerView
struct CollectionDebuggerView_t1_279;
// System.Collections.ICollection
struct ICollection_t1_280;
// System.Object[]
struct ObjectU5BU5D_t1_272;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.CollectionDebuggerView::.ctor(System.Collections.ICollection)
extern "C" void CollectionDebuggerView__ctor_m1_3191 (CollectionDebuggerView_t1_279 * __this, Object_t * ___col, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Collections.CollectionDebuggerView::get_Items()
extern "C" ObjectU5BU5D_t1_272* CollectionDebuggerView_get_Items_m1_3192 (CollectionDebuggerView_t1_279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
