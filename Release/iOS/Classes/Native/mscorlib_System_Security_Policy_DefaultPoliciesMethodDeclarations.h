﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.PermissionSet
struct PermissionSet_t1_563;
// System.String
struct String_t;
// System.Security.Policy.StrongNameMembershipCondition
struct StrongNameMembershipCondition_t1_1364;
// System.Security.NamedPermissionSet
struct NamedPermissionSet_t1_1344;
// System.Security.SecurityElement
struct SecurityElement_t1_242;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Policy_DefaultPolicies_Key.h"

// System.Void System.Security.Policy.DefaultPolicies::.cctor()
extern "C" void DefaultPolicies__cctor_m1_11432 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.PermissionSet System.Security.Policy.DefaultPolicies::GetSpecialPermissionSet(System.String)
extern "C" PermissionSet_t1_563 * DefaultPolicies_GetSpecialPermissionSet_m1_11433 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.PermissionSet System.Security.Policy.DefaultPolicies::get_FullTrust()
extern "C" PermissionSet_t1_563 * DefaultPolicies_get_FullTrust_m1_11434 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.PermissionSet System.Security.Policy.DefaultPolicies::get_LocalIntranet()
extern "C" PermissionSet_t1_563 * DefaultPolicies_get_LocalIntranet_m1_11435 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.PermissionSet System.Security.Policy.DefaultPolicies::get_Internet()
extern "C" PermissionSet_t1_563 * DefaultPolicies_get_Internet_m1_11436 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.PermissionSet System.Security.Policy.DefaultPolicies::get_SkipVerification()
extern "C" PermissionSet_t1_563 * DefaultPolicies_get_SkipVerification_m1_11437 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.PermissionSet System.Security.Policy.DefaultPolicies::get_Execution()
extern "C" PermissionSet_t1_563 * DefaultPolicies_get_Execution_m1_11438 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.PermissionSet System.Security.Policy.DefaultPolicies::get_Nothing()
extern "C" PermissionSet_t1_563 * DefaultPolicies_get_Nothing_m1_11439 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.PermissionSet System.Security.Policy.DefaultPolicies::get_Everything()
extern "C" PermissionSet_t1_563 * DefaultPolicies_get_Everything_m1_11440 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.StrongNameMembershipCondition System.Security.Policy.DefaultPolicies::FullTrustMembership(System.String,System.Security.Policy.DefaultPolicies/Key)
extern "C" StrongNameMembershipCondition_t1_1364 * DefaultPolicies_FullTrustMembership_m1_11441 (Object_t * __this /* static, unused */, String_t* ___name, int32_t ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.NamedPermissionSet System.Security.Policy.DefaultPolicies::BuildFullTrust()
extern "C" NamedPermissionSet_t1_1344 * DefaultPolicies_BuildFullTrust_m1_11442 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.NamedPermissionSet System.Security.Policy.DefaultPolicies::BuildLocalIntranet()
extern "C" NamedPermissionSet_t1_1344 * DefaultPolicies_BuildLocalIntranet_m1_11443 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.NamedPermissionSet System.Security.Policy.DefaultPolicies::BuildInternet()
extern "C" NamedPermissionSet_t1_1344 * DefaultPolicies_BuildInternet_m1_11444 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.NamedPermissionSet System.Security.Policy.DefaultPolicies::BuildSkipVerification()
extern "C" NamedPermissionSet_t1_1344 * DefaultPolicies_BuildSkipVerification_m1_11445 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.NamedPermissionSet System.Security.Policy.DefaultPolicies::BuildExecution()
extern "C" NamedPermissionSet_t1_1344 * DefaultPolicies_BuildExecution_m1_11446 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.NamedPermissionSet System.Security.Policy.DefaultPolicies::BuildNothing()
extern "C" NamedPermissionSet_t1_1344 * DefaultPolicies_BuildNothing_m1_11447 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.NamedPermissionSet System.Security.Policy.DefaultPolicies::BuildEverything()
extern "C" NamedPermissionSet_t1_1344 * DefaultPolicies_BuildEverything_m1_11448 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityElement System.Security.Policy.DefaultPolicies::PrintingPermission(System.String)
extern "C" SecurityElement_t1_242 * DefaultPolicies_PrintingPermission_m1_11449 (Object_t * __this /* static, unused */, String_t* ___level, const MethodInfo* method) IL2CPP_METHOD_ATTR;
