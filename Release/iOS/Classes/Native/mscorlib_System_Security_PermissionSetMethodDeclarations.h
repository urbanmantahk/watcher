﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.PermissionSet
struct PermissionSet_t1_563;
// System.String
struct String_t;
// System.Security.IPermission
struct IPermission_t1_1400;
// System.Type
struct Type_t;
// System.Security.SecurityElement
struct SecurityElement_t1_242;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Security.Policy.PolicyLevel
struct PolicyLevel_t1_1357;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Permissions_PermissionState.h"

// System.Void System.Security.PermissionSet::.ctor()
extern "C" void PermissionSet__ctor_m1_11968 (PermissionSet_t1_563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.PermissionSet::.ctor(System.String)
extern "C" void PermissionSet__ctor_m1_11969 (PermissionSet_t1_563 * __this, String_t* ___xml, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.PermissionSet::.ctor(System.Security.Permissions.PermissionState)
extern "C" void PermissionSet__ctor_m1_11970 (PermissionSet_t1_563 * __this, int32_t ___state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.PermissionSet::.ctor(System.Security.PermissionSet)
extern "C" void PermissionSet__ctor_m1_11971 (PermissionSet_t1_563 * __this, PermissionSet_t1_563 * ___permSet, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.PermissionSet::AddPermission(System.Security.IPermission)
extern "C" Object_t * PermissionSet_AddPermission_m1_11972 (PermissionSet_t1_563 * __this, Object_t * ___perm, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.PermissionSet::Assert()
extern "C" void PermissionSet_Assert_m1_11973 (PermissionSet_t1_563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.PermissionSet System.Security.PermissionSet::Copy()
extern "C" PermissionSet_t1_563 * PermissionSet_Copy_m1_11974 (PermissionSet_t1_563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.PermissionSet::Demand()
extern "C" void PermissionSet_Demand_m1_11975 (PermissionSet_t1_563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.PermissionSet::PermitOnly()
extern "C" void PermissionSet_PermitOnly_m1_11976 (PermissionSet_t1_563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.PermissionSet::GetPermission(System.Type)
extern "C" Object_t * PermissionSet_GetPermission_m1_11977 (PermissionSet_t1_563 * __this, Type_t * ___permClass, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.PermissionSet System.Security.PermissionSet::Intersect(System.Security.PermissionSet)
extern "C" PermissionSet_t1_563 * PermissionSet_Intersect_m1_11978 (PermissionSet_t1_563 * __this, PermissionSet_t1_563 * ___other, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.PermissionSet::Deny()
extern "C" void PermissionSet_Deny_m1_11979 (PermissionSet_t1_563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.PermissionSet::FromXml(System.Security.SecurityElement)
extern "C" void PermissionSet_FromXml_m1_11980 (PermissionSet_t1_563 * __this, SecurityElement_t1_242 * ___et, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.PermissionSet::CopyTo(System.Array,System.Int32)
extern "C" void PermissionSet_CopyTo_m1_11981 (PermissionSet_t1_563 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityElement System.Security.PermissionSet::ToXml()
extern "C" SecurityElement_t1_242 * PermissionSet_ToXml_m1_11982 (PermissionSet_t1_563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.PermissionSet::IsSubsetOf(System.Security.PermissionSet)
extern "C" bool PermissionSet_IsSubsetOf_m1_11983 (PermissionSet_t1_563 * __this, PermissionSet_t1_563 * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.PermissionSet::SetReadOnly(System.Boolean)
extern "C" void PermissionSet_SetReadOnly_m1_11984 (PermissionSet_t1_563 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.PermissionSet::IsUnrestricted()
extern "C" bool PermissionSet_IsUnrestricted_m1_11985 (PermissionSet_t1_563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.PermissionSet System.Security.PermissionSet::Union(System.Security.PermissionSet)
extern "C" PermissionSet_t1_563 * PermissionSet_Union_m1_11986 (PermissionSet_t1_563 * __this, PermissionSet_t1_563 * ___other, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Security.PermissionSet::GetEnumerator()
extern "C" Object_t * PermissionSet_GetEnumerator_m1_11987 (PermissionSet_t1_563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.PolicyLevel System.Security.PermissionSet::get_Resolver()
extern "C" PolicyLevel_t1_1357 * PermissionSet_get_Resolver_m1_11988 (PermissionSet_t1_563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.PermissionSet::set_Resolver(System.Security.Policy.PolicyLevel)
extern "C" void PermissionSet_set_Resolver_m1_11989 (PermissionSet_t1_563 * __this, PolicyLevel_t1_1357 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.PermissionSet::get_DeclarativeSecurity()
extern "C" bool PermissionSet_get_DeclarativeSecurity_m1_11990 (PermissionSet_t1_563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.PermissionSet::set_DeclarativeSecurity(System.Boolean)
extern "C" void PermissionSet_set_DeclarativeSecurity_m1_11991 (PermissionSet_t1_563 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.PermissionSet::IsEmpty()
extern "C" bool PermissionSet_IsEmpty_m1_11992 (PermissionSet_t1_563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.PermissionSet System.Security.PermissionSet::CreateFromBinaryFormat(System.Byte[])
extern "C" PermissionSet_t1_563 * PermissionSet_CreateFromBinaryFormat_m1_11993 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
