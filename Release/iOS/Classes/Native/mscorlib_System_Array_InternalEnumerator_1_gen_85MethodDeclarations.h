﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_85.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_CONNECTDATA.h"

// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ComTypes.CONNECTDATA>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_16846_gshared (InternalEnumerator_1_t1_2105 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1_16846(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1_2105 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m1_16846_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ComTypes.CONNECTDATA>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_16847_gshared (InternalEnumerator_1_t1_2105 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_16847(__this, method) (( void (*) (InternalEnumerator_1_t1_2105 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_16847_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ComTypes.CONNECTDATA>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_16848_gshared (InternalEnumerator_1_t1_2105 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_16848(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1_2105 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_16848_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ComTypes.CONNECTDATA>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_16849_gshared (InternalEnumerator_1_t1_2105 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1_16849(__this, method) (( void (*) (InternalEnumerator_1_t1_2105 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1_16849_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ComTypes.CONNECTDATA>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_16850_gshared (InternalEnumerator_1_t1_2105 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1_16850(__this, method) (( bool (*) (InternalEnumerator_1_t1_2105 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1_16850_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ComTypes.CONNECTDATA>::get_Current()
extern "C" CONNECTDATA_t1_723  InternalEnumerator_1_get_Current_m1_16851_gshared (InternalEnumerator_1_t1_2105 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1_16851(__this, method) (( CONNECTDATA_t1_723  (*) (InternalEnumerator_1_t1_2105 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1_16851_gshared)(__this, method)
