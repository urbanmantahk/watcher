﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Text.Latin1Encoding
struct Latin1Encoding_t1_1439;
// System.Char[]
struct CharU5BU5D_t1_16;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Text.EncoderFallbackBuffer
struct EncoderFallbackBuffer_t1_1429;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Text_NormalizationForm.h"

// System.Void System.Text.Latin1Encoding::.ctor()
extern "C" void Latin1Encoding__ctor_m1_12380 (Latin1Encoding_t1_1439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Text.Latin1Encoding::get_IsSingleByte()
extern "C" bool Latin1Encoding_get_IsSingleByte_m1_12381 (Latin1Encoding_t1_1439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Text.Latin1Encoding::IsAlwaysNormalized(System.Text.NormalizationForm)
extern "C" bool Latin1Encoding_IsAlwaysNormalized_m1_12382 (Latin1Encoding_t1_1439 * __this, int32_t ___form, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.Latin1Encoding::GetByteCount(System.Char[],System.Int32,System.Int32)
extern "C" int32_t Latin1Encoding_GetByteCount_m1_12383 (Latin1Encoding_t1_1439 * __this, CharU5BU5D_t1_16* ___chars, int32_t ___index, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.Latin1Encoding::GetByteCount(System.String)
extern "C" int32_t Latin1Encoding_GetByteCount_m1_12384 (Latin1Encoding_t1_1439 * __this, String_t* ___s, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.Latin1Encoding::GetBytes(System.Char[],System.Int32,System.Int32,System.Byte[],System.Int32)
extern "C" int32_t Latin1Encoding_GetBytes_m1_12385 (Latin1Encoding_t1_1439 * __this, CharU5BU5D_t1_16* ___chars, int32_t ___charIndex, int32_t ___charCount, ByteU5BU5D_t1_109* ___bytes, int32_t ___byteIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.Latin1Encoding::GetBytes(System.Char[],System.Int32,System.Int32,System.Byte[],System.Int32,System.Text.EncoderFallbackBuffer&,System.Char[]&)
extern "C" int32_t Latin1Encoding_GetBytes_m1_12386 (Latin1Encoding_t1_1439 * __this, CharU5BU5D_t1_16* ___chars, int32_t ___charIndex, int32_t ___charCount, ByteU5BU5D_t1_109* ___bytes, int32_t ___byteIndex, EncoderFallbackBuffer_t1_1429 ** ___buffer, CharU5BU5D_t1_16** ___fallback_chars, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.Latin1Encoding::GetBytes(System.String,System.Int32,System.Int32,System.Byte[],System.Int32)
extern "C" int32_t Latin1Encoding_GetBytes_m1_12387 (Latin1Encoding_t1_1439 * __this, String_t* ___s, int32_t ___charIndex, int32_t ___charCount, ByteU5BU5D_t1_109* ___bytes, int32_t ___byteIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.Latin1Encoding::GetBytes(System.String,System.Int32,System.Int32,System.Byte[],System.Int32,System.Text.EncoderFallbackBuffer&,System.Char[]&)
extern "C" int32_t Latin1Encoding_GetBytes_m1_12388 (Latin1Encoding_t1_1439 * __this, String_t* ___s, int32_t ___charIndex, int32_t ___charCount, ByteU5BU5D_t1_109* ___bytes, int32_t ___byteIndex, EncoderFallbackBuffer_t1_1429 ** ___buffer, CharU5BU5D_t1_16** ___fallback_chars, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.Latin1Encoding::GetCharCount(System.Byte[],System.Int32,System.Int32)
extern "C" int32_t Latin1Encoding_GetCharCount_m1_12389 (Latin1Encoding_t1_1439 * __this, ByteU5BU5D_t1_109* ___bytes, int32_t ___index, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.Latin1Encoding::GetChars(System.Byte[],System.Int32,System.Int32,System.Char[],System.Int32)
extern "C" int32_t Latin1Encoding_GetChars_m1_12390 (Latin1Encoding_t1_1439 * __this, ByteU5BU5D_t1_109* ___bytes, int32_t ___byteIndex, int32_t ___byteCount, CharU5BU5D_t1_16* ___chars, int32_t ___charIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.Latin1Encoding::GetMaxByteCount(System.Int32)
extern "C" int32_t Latin1Encoding_GetMaxByteCount_m1_12391 (Latin1Encoding_t1_1439 * __this, int32_t ___charCount, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.Latin1Encoding::GetMaxCharCount(System.Int32)
extern "C" int32_t Latin1Encoding_GetMaxCharCount_m1_12392 (Latin1Encoding_t1_1439 * __this, int32_t ___byteCount, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Text.Latin1Encoding::GetString(System.Byte[],System.Int32,System.Int32)
extern "C" String_t* Latin1Encoding_GetString_m1_12393 (Latin1Encoding_t1_1439 * __this, ByteU5BU5D_t1_109* ___bytes, int32_t ___index, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Text.Latin1Encoding::GetString(System.Byte[])
extern "C" String_t* Latin1Encoding_GetString_m1_12394 (Latin1Encoding_t1_1439 * __this, ByteU5BU5D_t1_109* ___bytes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Text.Latin1Encoding::get_BodyName()
extern "C" String_t* Latin1Encoding_get_BodyName_m1_12395 (Latin1Encoding_t1_1439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Text.Latin1Encoding::get_EncodingName()
extern "C" String_t* Latin1Encoding_get_EncodingName_m1_12396 (Latin1Encoding_t1_1439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Text.Latin1Encoding::get_HeaderName()
extern "C" String_t* Latin1Encoding_get_HeaderName_m1_12397 (Latin1Encoding_t1_1439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Text.Latin1Encoding::get_IsBrowserDisplay()
extern "C" bool Latin1Encoding_get_IsBrowserDisplay_m1_12398 (Latin1Encoding_t1_1439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Text.Latin1Encoding::get_IsBrowserSave()
extern "C" bool Latin1Encoding_get_IsBrowserSave_m1_12399 (Latin1Encoding_t1_1439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Text.Latin1Encoding::get_IsMailNewsDisplay()
extern "C" bool Latin1Encoding_get_IsMailNewsDisplay_m1_12400 (Latin1Encoding_t1_1439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Text.Latin1Encoding::get_IsMailNewsSave()
extern "C" bool Latin1Encoding_get_IsMailNewsSave_m1_12401 (Latin1Encoding_t1_1439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Text.Latin1Encoding::get_WebName()
extern "C" String_t* Latin1Encoding_get_WebName_m1_12402 (Latin1Encoding_t1_1439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
