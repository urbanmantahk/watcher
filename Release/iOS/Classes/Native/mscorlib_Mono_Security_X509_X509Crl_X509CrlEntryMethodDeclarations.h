﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.X509.X509Crl/X509CrlEntry
struct X509CrlEntry_t1_189;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// Mono.Security.X509.X509ExtensionCollection
struct X509ExtensionCollection_t1_190;
// Mono.Security.ASN1
struct ASN1_t1_149;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime.h"

// System.Void Mono.Security.X509.X509Crl/X509CrlEntry::.ctor(System.Byte[],System.DateTime,Mono.Security.X509.X509ExtensionCollection)
extern "C" void X509CrlEntry__ctor_m1_2210 (X509CrlEntry_t1_189 * __this, ByteU5BU5D_t1_109* ___serialNumber, DateTime_t1_150  ___revocationDate, X509ExtensionCollection_t1_190 * ___extensions, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509Crl/X509CrlEntry::.ctor(Mono.Security.ASN1)
extern "C" void X509CrlEntry__ctor_m1_2211 (X509CrlEntry_t1_189 * __this, ASN1_t1_149 * ___entry, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.X509.X509Crl/X509CrlEntry::get_SerialNumber()
extern "C" ByteU5BU5D_t1_109* X509CrlEntry_get_SerialNumber_m1_2212 (X509CrlEntry_t1_189 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Mono.Security.X509.X509Crl/X509CrlEntry::get_RevocationDate()
extern "C" DateTime_t1_150  X509CrlEntry_get_RevocationDate_m1_2213 (X509CrlEntry_t1_189 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509ExtensionCollection Mono.Security.X509.X509Crl/X509CrlEntry::get_Extensions()
extern "C" X509ExtensionCollection_t1_190 * X509CrlEntry_get_Extensions_m1_2214 (X509CrlEntry_t1_189 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.X509.X509Crl/X509CrlEntry::GetBytes()
extern "C" ByteU5BU5D_t1_109* X509CrlEntry_GetBytes_m1_2215 (X509CrlEntry_t1_189 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
