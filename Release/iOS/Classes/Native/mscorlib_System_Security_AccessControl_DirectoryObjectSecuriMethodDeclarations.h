﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.AccessControl.DirectoryObjectSecurity
struct DirectoryObjectSecurity_t1_1146;
// System.Security.AccessControl.CommonSecurityDescriptor
struct CommonSecurityDescriptor_t1_1130;
// System.Security.AccessControl.AccessRule
struct AccessRule_t1_1111;
// System.Security.Principal.IdentityReference
struct IdentityReference_t1_1120;
// System.Security.AccessControl.AuditRule
struct AuditRule_t1_1119;
// System.Security.AccessControl.AuthorizationRuleCollection
struct AuthorizationRuleCollection_t1_1121;
// System.Type
struct Type_t;
// System.Security.AccessControl.ObjectAccessRule
struct ObjectAccessRule_t1_1163;
// System.Security.AccessControl.ObjectAuditRule
struct ObjectAuditRule_t1_1166;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_AccessControl_InheritanceFlags.h"
#include "mscorlib_System_Security_AccessControl_PropagationFlags.h"
#include "mscorlib_System_Security_AccessControl_AccessControlType.h"
#include "mscorlib_System_Guid.h"
#include "mscorlib_System_Security_AccessControl_AuditFlags.h"
#include "mscorlib_System_Security_AccessControl_AccessControlModifica.h"

// System.Void System.Security.AccessControl.DirectoryObjectSecurity::.ctor()
extern "C" void DirectoryObjectSecurity__ctor_m1_9814 (DirectoryObjectSecurity_t1_1146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.DirectoryObjectSecurity::.ctor(System.Security.AccessControl.CommonSecurityDescriptor)
extern "C" void DirectoryObjectSecurity__ctor_m1_9815 (DirectoryObjectSecurity_t1_1146 * __this, CommonSecurityDescriptor_t1_1130 * ___securityDescriptor, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.AccessControl.AccessRule System.Security.AccessControl.DirectoryObjectSecurity::AccessRuleFactory(System.Security.Principal.IdentityReference,System.Int32,System.Boolean,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.AccessControlType,System.Guid,System.Guid)
extern "C" AccessRule_t1_1111 * DirectoryObjectSecurity_AccessRuleFactory_m1_9816 (DirectoryObjectSecurity_t1_1146 * __this, IdentityReference_t1_1120 * ___identityReference, int32_t ___accessMask, bool ___isInherited, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___type, Guid_t1_319  ___objectType, Guid_t1_319  ___inheritedObjectType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.AccessControl.AuditRule System.Security.AccessControl.DirectoryObjectSecurity::AuditRuleFactory(System.Security.Principal.IdentityReference,System.Int32,System.Boolean,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.AuditFlags,System.Guid,System.Guid)
extern "C" AuditRule_t1_1119 * DirectoryObjectSecurity_AuditRuleFactory_m1_9817 (DirectoryObjectSecurity_t1_1146 * __this, IdentityReference_t1_1120 * ___identityReference, int32_t ___accessMask, bool ___isInherited, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___flags, Guid_t1_319  ___objectType, Guid_t1_319  ___inheritedObjectType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.AccessControl.AuthorizationRuleCollection System.Security.AccessControl.DirectoryObjectSecurity::GetAccessRules(System.Boolean,System.Boolean,System.Type)
extern "C" AuthorizationRuleCollection_t1_1121 * DirectoryObjectSecurity_GetAccessRules_m1_9818 (DirectoryObjectSecurity_t1_1146 * __this, bool ___includeExplicit, bool ___includeInherited, Type_t * ___targetType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.AccessControl.AuthorizationRuleCollection System.Security.AccessControl.DirectoryObjectSecurity::GetAuditRules(System.Boolean,System.Boolean,System.Type)
extern "C" AuthorizationRuleCollection_t1_1121 * DirectoryObjectSecurity_GetAuditRules_m1_9819 (DirectoryObjectSecurity_t1_1146 * __this, bool ___includeExplicit, bool ___includeInherited, Type_t * ___targetType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.DirectoryObjectSecurity::AddAccessRule(System.Security.AccessControl.ObjectAccessRule)
extern "C" void DirectoryObjectSecurity_AddAccessRule_m1_9820 (DirectoryObjectSecurity_t1_1146 * __this, ObjectAccessRule_t1_1163 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.DirectoryObjectSecurity::AddAuditRule(System.Security.AccessControl.ObjectAuditRule)
extern "C" void DirectoryObjectSecurity_AddAuditRule_m1_9821 (DirectoryObjectSecurity_t1_1146 * __this, ObjectAuditRule_t1_1166 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.AccessControl.DirectoryObjectSecurity::ModifyAccess(System.Security.AccessControl.AccessControlModification,System.Security.AccessControl.AccessRule,System.Boolean&)
extern "C" bool DirectoryObjectSecurity_ModifyAccess_m1_9822 (DirectoryObjectSecurity_t1_1146 * __this, int32_t ___modification, AccessRule_t1_1111 * ___rule, bool* ___modified, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.AccessControl.DirectoryObjectSecurity::ModifyAudit(System.Security.AccessControl.AccessControlModification,System.Security.AccessControl.AuditRule,System.Boolean&)
extern "C" bool DirectoryObjectSecurity_ModifyAudit_m1_9823 (DirectoryObjectSecurity_t1_1146 * __this, int32_t ___modification, AuditRule_t1_1119 * ___rule, bool* ___modified, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.AccessControl.DirectoryObjectSecurity::RemoveAccessRule(System.Security.AccessControl.ObjectAccessRule)
extern "C" bool DirectoryObjectSecurity_RemoveAccessRule_m1_9824 (DirectoryObjectSecurity_t1_1146 * __this, ObjectAccessRule_t1_1163 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.DirectoryObjectSecurity::RemoveAccessRuleAll(System.Security.AccessControl.ObjectAccessRule)
extern "C" void DirectoryObjectSecurity_RemoveAccessRuleAll_m1_9825 (DirectoryObjectSecurity_t1_1146 * __this, ObjectAccessRule_t1_1163 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.DirectoryObjectSecurity::RemoveAccessRuleSpecific(System.Security.AccessControl.ObjectAccessRule)
extern "C" void DirectoryObjectSecurity_RemoveAccessRuleSpecific_m1_9826 (DirectoryObjectSecurity_t1_1146 * __this, ObjectAccessRule_t1_1163 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.AccessControl.DirectoryObjectSecurity::RemoveAuditRule(System.Security.AccessControl.ObjectAuditRule)
extern "C" bool DirectoryObjectSecurity_RemoveAuditRule_m1_9827 (DirectoryObjectSecurity_t1_1146 * __this, ObjectAuditRule_t1_1166 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.DirectoryObjectSecurity::RemoveAuditRuleAll(System.Security.AccessControl.ObjectAuditRule)
extern "C" void DirectoryObjectSecurity_RemoveAuditRuleAll_m1_9828 (DirectoryObjectSecurity_t1_1146 * __this, ObjectAuditRule_t1_1166 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.DirectoryObjectSecurity::RemoveAuditRuleSpecific(System.Security.AccessControl.ObjectAuditRule)
extern "C" void DirectoryObjectSecurity_RemoveAuditRuleSpecific_m1_9829 (DirectoryObjectSecurity_t1_1146 * __this, ObjectAuditRule_t1_1166 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.DirectoryObjectSecurity::ResetAccessRule(System.Security.AccessControl.ObjectAccessRule)
extern "C" void DirectoryObjectSecurity_ResetAccessRule_m1_9830 (DirectoryObjectSecurity_t1_1146 * __this, ObjectAccessRule_t1_1163 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.DirectoryObjectSecurity::SetAccessRule(System.Security.AccessControl.ObjectAccessRule)
extern "C" void DirectoryObjectSecurity_SetAccessRule_m1_9831 (DirectoryObjectSecurity_t1_1146 * __this, ObjectAccessRule_t1_1163 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.DirectoryObjectSecurity::SetAuditRule(System.Security.AccessControl.ObjectAuditRule)
extern "C" void DirectoryObjectSecurity_SetAuditRule_m1_9832 (DirectoryObjectSecurity_t1_1146 * __this, ObjectAuditRule_t1_1166 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
