﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object.h"

// System.Security.Policy.Url
struct  Url_t1_1368  : public Object_t
{
	// System.String System.Security.Policy.Url::origin_url
	String_t* ___origin_url_0;
};
