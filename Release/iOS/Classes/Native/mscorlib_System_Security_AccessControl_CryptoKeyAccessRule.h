﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Security_AccessControl_AccessRule.h"
#include "mscorlib_System_Security_AccessControl_CryptoKeyRights.h"

// System.Security.AccessControl.CryptoKeyAccessRule
struct  CryptoKeyAccessRule_t1_1139  : public AccessRule_t1_1111
{
	// System.Security.AccessControl.CryptoKeyRights System.Security.AccessControl.CryptoKeyAccessRule::rights
	int32_t ___rights_6;
};
