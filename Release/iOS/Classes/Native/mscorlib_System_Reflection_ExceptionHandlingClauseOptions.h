﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Reflection_ExceptionHandlingClauseOptions.h"

// System.Reflection.ExceptionHandlingClauseOptions
struct  ExceptionHandlingClauseOptions_t1_598 
{
	// System.Int32 System.Reflection.ExceptionHandlingClauseOptions::value__
	int32_t ___value___1;
};
