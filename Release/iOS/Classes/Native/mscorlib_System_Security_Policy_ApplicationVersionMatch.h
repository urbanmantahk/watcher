﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Security_Policy_ApplicationVersionMatch.h"

// System.Security.Policy.ApplicationVersionMatch
struct  ApplicationVersionMatch_t1_1337 
{
	// System.Int32 System.Security.Policy.ApplicationVersionMatch::value__
	int32_t ___value___1;
};
