﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_162.h"
#include "UnityEngine_UnityEngine_UICharInfo.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_20426_gshared (InternalEnumerator_1_t1_2359 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1_20426(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1_2359 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m1_20426_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_20427_gshared (InternalEnumerator_1_t1_2359 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_20427(__this, method) (( void (*) (InternalEnumerator_1_t1_2359 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_20427_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_20428_gshared (InternalEnumerator_1_t1_2359 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_20428(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1_2359 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_20428_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_20429_gshared (InternalEnumerator_1_t1_2359 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1_20429(__this, method) (( void (*) (InternalEnumerator_1_t1_2359 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1_20429_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_20430_gshared (InternalEnumerator_1_t1_2359 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1_20430(__this, method) (( bool (*) (InternalEnumerator_1_t1_2359 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1_20430_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::get_Current()
extern "C" UICharInfo_t6_150  InternalEnumerator_1_get_Current_m1_20431_gshared (InternalEnumerator_1_t1_2359 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1_20431(__this, method) (( UICharInfo_t6_150  (*) (InternalEnumerator_1_t1_2359 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1_20431_gshared)(__this, method)
