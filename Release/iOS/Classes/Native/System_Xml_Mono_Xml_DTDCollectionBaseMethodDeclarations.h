﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Xml.DTDCollectionBase
struct DTDCollectionBase_t4_91;
// Mono.Xml.DTDObjectModel
struct DTDObjectModel_t4_82;
// System.String
struct String_t;
// Mono.Xml.DTDNode
struct DTDNode_t4_89;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Xml.DTDCollectionBase::.ctor(Mono.Xml.DTDObjectModel)
extern "C" void DTDCollectionBase__ctor_m4_160 (DTDCollectionBase_t4_91 * __this, DTDObjectModel_t4_82 * ___root, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Xml.DTDObjectModel Mono.Xml.DTDCollectionBase::get_Root()
extern "C" DTDObjectModel_t4_82 * DTDCollectionBase_get_Root_m4_161 (DTDCollectionBase_t4_91 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Xml.DTDCollectionBase::BaseAdd(System.String,Mono.Xml.DTDNode)
extern "C" void DTDCollectionBase_BaseAdd_m4_162 (DTDCollectionBase_t4_91 * __this, String_t* ___name, DTDNode_t4_89 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Xml.DTDCollectionBase::Contains(System.String)
extern "C" bool DTDCollectionBase_Contains_m4_163 (DTDCollectionBase_t4_91 * __this, String_t* ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Mono.Xml.DTDCollectionBase::BaseGet(System.String)
extern "C" Object_t * DTDCollectionBase_BaseGet_m4_164 (DTDCollectionBase_t4_91 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
