﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.Utility.EditorCoroutine
struct EditorCoroutine_t8_144;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.Utility.EditorCoroutine::.ctor()
extern "C" void EditorCoroutine__ctor_m8_845 (EditorCoroutine_t8_144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.EditorCoroutine::.ctor(System.Collections.IEnumerator)
extern "C" void EditorCoroutine__ctor_m8_846 (EditorCoroutine_t8_144 * __this, Object_t * ____enumerator, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator VoxelBusters.Utility.EditorCoroutine::get_CoroutineMethod()
extern "C" Object_t * EditorCoroutine_get_CoroutineMethod_m8_847 (EditorCoroutine_t8_144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.EditorCoroutine::set_CoroutineMethod(System.Collections.IEnumerator)
extern "C" void EditorCoroutine_set_CoroutineMethod_m8_848 (EditorCoroutine_t8_144 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.Utility.EditorCoroutine VoxelBusters.Utility.EditorCoroutine::StartCoroutine(System.Collections.IEnumerator)
extern "C" EditorCoroutine_t8_144 * EditorCoroutine_StartCoroutine_m8_849 (Object_t * __this /* static, unused */, Object_t * ____enumerator, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.EditorCoroutine::StopCoroutine()
extern "C" void EditorCoroutine_StopCoroutine_m8_850 (EditorCoroutine_t8_144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.EditorCoroutine::Update()
extern "C" void EditorCoroutine_Update_m8_851 (EditorCoroutine_t8_144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
