﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"

// System.Int32 System.Threading.Interlocked::CompareExchange(System.Int32&,System.Int32,System.Int32)
extern "C" int32_t Interlocked_CompareExchange_m1_12664 (Object_t * __this /* static, unused */, int32_t* ___location1, int32_t ___value, int32_t ___comparand, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Threading.Interlocked::CompareExchange(System.Object&,System.Object,System.Object)
extern "C" Object_t * Interlocked_CompareExchange_m1_12665 (Object_t * __this /* static, unused */, Object_t ** ___location1, Object_t * ___value, Object_t * ___comparand, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single System.Threading.Interlocked::CompareExchange(System.Single&,System.Single,System.Single)
extern "C" float Interlocked_CompareExchange_m1_12666 (Object_t * __this /* static, unused */, float* ___location1, float ___value, float ___comparand, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Threading.Interlocked::Decrement(System.Int32&)
extern "C" int32_t Interlocked_Decrement_m1_12667 (Object_t * __this /* static, unused */, int32_t* ___location, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Threading.Interlocked::Decrement(System.Int64&)
extern "C" int64_t Interlocked_Decrement_m1_12668 (Object_t * __this /* static, unused */, int64_t* ___location, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Threading.Interlocked::Increment(System.Int32&)
extern "C" int32_t Interlocked_Increment_m1_12669 (Object_t * __this /* static, unused */, int32_t* ___location, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Threading.Interlocked::Increment(System.Int64&)
extern "C" int64_t Interlocked_Increment_m1_12670 (Object_t * __this /* static, unused */, int64_t* ___location, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Threading.Interlocked::Exchange(System.Int32&,System.Int32)
extern "C" int32_t Interlocked_Exchange_m1_12671 (Object_t * __this /* static, unused */, int32_t* ___location1, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Threading.Interlocked::Exchange(System.Object&,System.Object)
extern "C" Object_t * Interlocked_Exchange_m1_12672 (Object_t * __this /* static, unused */, Object_t ** ___location1, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single System.Threading.Interlocked::Exchange(System.Single&,System.Single)
extern "C" float Interlocked_Exchange_m1_12673 (Object_t * __this /* static, unused */, float* ___location1, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Threading.Interlocked::CompareExchange(System.Int64&,System.Int64,System.Int64)
extern "C" int64_t Interlocked_CompareExchange_m1_12674 (Object_t * __this /* static, unused */, int64_t* ___location1, int64_t ___value, int64_t ___comparand, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Threading.Interlocked::CompareExchange(System.IntPtr&,System.IntPtr,System.IntPtr)
extern "C" IntPtr_t Interlocked_CompareExchange_m1_12675 (Object_t * __this /* static, unused */, IntPtr_t* ___location1, IntPtr_t ___value, IntPtr_t ___comparand, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Threading.Interlocked::CompareExchange(System.Double&,System.Double,System.Double)
extern "C" double Interlocked_CompareExchange_m1_12676 (Object_t * __this /* static, unused */, double* ___location1, double ___value, double ___comparand, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Threading.Interlocked::Exchange(System.Int64&,System.Int64)
extern "C" int64_t Interlocked_Exchange_m1_12677 (Object_t * __this /* static, unused */, int64_t* ___location1, int64_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Threading.Interlocked::Exchange(System.IntPtr&,System.IntPtr)
extern "C" IntPtr_t Interlocked_Exchange_m1_12678 (Object_t * __this /* static, unused */, IntPtr_t* ___location1, IntPtr_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Threading.Interlocked::Exchange(System.Double&,System.Double)
extern "C" double Interlocked_Exchange_m1_12679 (Object_t * __this /* static, unused */, double* ___location1, double ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Threading.Interlocked::Read(System.Int64&)
extern "C" int64_t Interlocked_Read_m1_12680 (Object_t * __this /* static, unused */, int64_t* ___location, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Threading.Interlocked::Add(System.Int32&,System.Int32)
extern "C" int32_t Interlocked_Add_m1_12681 (Object_t * __this /* static, unused */, int32_t* ___location1, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Threading.Interlocked::Add(System.Int64&,System.Int64)
extern "C" int64_t Interlocked_Add_m1_12682 (Object_t * __this /* static, unused */, int64_t* ___location1, int64_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
