﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Permissions.KeyContainerPermissionAttribute
struct KeyContainerPermissionAttribute_t1_1292;
// System.String
struct String_t;
// System.Security.IPermission
struct IPermission_t1_1400;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Permissions_SecurityAction.h"
#include "mscorlib_System_Security_Permissions_KeyContainerPermissionF.h"

// System.Void System.Security.Permissions.KeyContainerPermissionAttribute::.ctor(System.Security.Permissions.SecurityAction)
extern "C" void KeyContainerPermissionAttribute__ctor_m1_11003 (KeyContainerPermissionAttribute_t1_1292 * __this, int32_t ___action, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Permissions.KeyContainerPermissionFlags System.Security.Permissions.KeyContainerPermissionAttribute::get_Flags()
extern "C" int32_t KeyContainerPermissionAttribute_get_Flags_m1_11004 (KeyContainerPermissionAttribute_t1_1292 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.KeyContainerPermissionAttribute::set_Flags(System.Security.Permissions.KeyContainerPermissionFlags)
extern "C" void KeyContainerPermissionAttribute_set_Flags_m1_11005 (KeyContainerPermissionAttribute_t1_1292 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Permissions.KeyContainerPermissionAttribute::get_KeyContainerName()
extern "C" String_t* KeyContainerPermissionAttribute_get_KeyContainerName_m1_11006 (KeyContainerPermissionAttribute_t1_1292 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.KeyContainerPermissionAttribute::set_KeyContainerName(System.String)
extern "C" void KeyContainerPermissionAttribute_set_KeyContainerName_m1_11007 (KeyContainerPermissionAttribute_t1_1292 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Permissions.KeyContainerPermissionAttribute::get_KeySpec()
extern "C" int32_t KeyContainerPermissionAttribute_get_KeySpec_m1_11008 (KeyContainerPermissionAttribute_t1_1292 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.KeyContainerPermissionAttribute::set_KeySpec(System.Int32)
extern "C" void KeyContainerPermissionAttribute_set_KeySpec_m1_11009 (KeyContainerPermissionAttribute_t1_1292 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Permissions.KeyContainerPermissionAttribute::get_KeyStore()
extern "C" String_t* KeyContainerPermissionAttribute_get_KeyStore_m1_11010 (KeyContainerPermissionAttribute_t1_1292 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.KeyContainerPermissionAttribute::set_KeyStore(System.String)
extern "C" void KeyContainerPermissionAttribute_set_KeyStore_m1_11011 (KeyContainerPermissionAttribute_t1_1292 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Permissions.KeyContainerPermissionAttribute::get_ProviderName()
extern "C" String_t* KeyContainerPermissionAttribute_get_ProviderName_m1_11012 (KeyContainerPermissionAttribute_t1_1292 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.KeyContainerPermissionAttribute::set_ProviderName(System.String)
extern "C" void KeyContainerPermissionAttribute_set_ProviderName_m1_11013 (KeyContainerPermissionAttribute_t1_1292 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Permissions.KeyContainerPermissionAttribute::get_ProviderType()
extern "C" int32_t KeyContainerPermissionAttribute_get_ProviderType_m1_11014 (KeyContainerPermissionAttribute_t1_1292 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.KeyContainerPermissionAttribute::set_ProviderType(System.Int32)
extern "C" void KeyContainerPermissionAttribute_set_ProviderType_m1_11015 (KeyContainerPermissionAttribute_t1_1292 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.KeyContainerPermissionAttribute::CreatePermission()
extern "C" Object_t * KeyContainerPermissionAttribute_CreatePermission_m1_11016 (KeyContainerPermissionAttribute_t1_1292 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.KeyContainerPermissionAttribute::EmptyEntry()
extern "C" bool KeyContainerPermissionAttribute_EmptyEntry_m1_11017 (KeyContainerPermissionAttribute_t1_1292 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
