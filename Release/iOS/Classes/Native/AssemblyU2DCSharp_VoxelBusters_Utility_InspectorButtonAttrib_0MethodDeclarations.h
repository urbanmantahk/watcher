﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.Utility.InspectorButtonAttribute
struct InspectorButtonAttribute_t8_147;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_InspectorButtonAttrib.h"

// System.Void VoxelBusters.Utility.InspectorButtonAttribute::.ctor()
extern "C" void InspectorButtonAttribute__ctor_m8_857 (InspectorButtonAttribute_t8_147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.InspectorButtonAttribute::.ctor(System.String,System.String,VoxelBusters.Utility.InspectorButtonAttribute/ePosition)
extern "C" void InspectorButtonAttribute__ctor_m8_858 (InspectorButtonAttribute_t8_147 * __this, String_t* ____buttonName, String_t* ____invokeMethod, int32_t ____position, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.Utility.InspectorButtonAttribute::get_Name()
extern "C" String_t* InspectorButtonAttribute_get_Name_m8_859 (InspectorButtonAttribute_t8_147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.InspectorButtonAttribute::set_Name(System.String)
extern "C" void InspectorButtonAttribute_set_Name_m8_860 (InspectorButtonAttribute_t8_147 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.Utility.InspectorButtonAttribute::get_InvokeMethod()
extern "C" String_t* InspectorButtonAttribute_get_InvokeMethod_m8_861 (InspectorButtonAttribute_t8_147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.InspectorButtonAttribute::set_InvokeMethod(System.String)
extern "C" void InspectorButtonAttribute_set_InvokeMethod_m8_862 (InspectorButtonAttribute_t8_147 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.Utility.InspectorButtonAttribute/ePosition VoxelBusters.Utility.InspectorButtonAttribute::get_Position()
extern "C" int32_t InspectorButtonAttribute_get_Position_m8_863 (InspectorButtonAttribute_t8_147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.InspectorButtonAttribute::set_Position(VoxelBusters.Utility.InspectorButtonAttribute/ePosition)
extern "C" void InspectorButtonAttribute_set_Position_m8_864 (InspectorButtonAttribute_t8_147 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
