﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Globalization.CCEastAsianLunisolarEraHandler
struct CCEastAsianLunisolarEraHandler_t1_355;

#include "mscorlib_System_Globalization_EastAsianLunisolarCalendar.h"
#include "mscorlib_System_DateTime.h"

// System.Globalization.JapaneseLunisolarCalendar
struct  JapaneseLunisolarCalendar_t1_379  : public EastAsianLunisolarCalendar_t1_358
{
};
struct JapaneseLunisolarCalendar_t1_379_StaticFields{
	// System.Globalization.CCEastAsianLunisolarEraHandler System.Globalization.JapaneseLunisolarCalendar::era_handler
	CCEastAsianLunisolarEraHandler_t1_355 * ___era_handler_9;
	// System.DateTime System.Globalization.JapaneseLunisolarCalendar::JapanMin
	DateTime_t1_150  ___JapanMin_10;
	// System.DateTime System.Globalization.JapaneseLunisolarCalendar::JapanMax
	DateTime_t1_150  ___JapanMax_11;
};
