﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.ObsoleteAttribute
struct ObsoleteAttribute_t1_39;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.ObsoleteAttribute::.ctor()
extern "C" void ObsoleteAttribute__ctor_m1_1284 (ObsoleteAttribute_t1_39 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ObsoleteAttribute::.ctor(System.String)
extern "C" void ObsoleteAttribute__ctor_m1_1285 (ObsoleteAttribute_t1_39 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ObsoleteAttribute::.ctor(System.String,System.Boolean)
extern "C" void ObsoleteAttribute__ctor_m1_1286 (ObsoleteAttribute_t1_39 * __this, String_t* ___message, bool ___error, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.ObsoleteAttribute::get_Message()
extern "C" String_t* ObsoleteAttribute_get_Message_m1_1287 (ObsoleteAttribute_t1_39 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ObsoleteAttribute::get_IsError()
extern "C" bool ObsoleteAttribute_get_IsError_m1_1288 (ObsoleteAttribute_t1_39 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
