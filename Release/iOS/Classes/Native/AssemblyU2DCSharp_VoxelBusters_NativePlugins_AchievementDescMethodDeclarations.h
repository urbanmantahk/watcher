﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.AchievementDescription/LoadAchievementDescriptionsCompletion
struct LoadAchievementDescriptionsCompletion_t8_223;
// System.Object
struct Object_t;
// VoxelBusters.NativePlugins.AchievementDescription[]
struct AchievementDescriptionU5BU5D_t8_224;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_String.h"

// System.Void VoxelBusters.NativePlugins.AchievementDescription/LoadAchievementDescriptionsCompletion::.ctor(System.Object,System.IntPtr)
extern "C" void LoadAchievementDescriptionsCompletion__ctor_m8_1263 (LoadAchievementDescriptionsCompletion_t8_223 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.AchievementDescription/LoadAchievementDescriptionsCompletion::Invoke(VoxelBusters.NativePlugins.AchievementDescription[],System.String)
extern "C" void LoadAchievementDescriptionsCompletion_Invoke_m8_1264 (LoadAchievementDescriptionsCompletion_t8_223 * __this, AchievementDescriptionU5BU5D_t8_224* ____descriptions, String_t* ____error, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_LoadAchievementDescriptionsCompletion_t8_223(Il2CppObject* delegate, AchievementDescriptionU5BU5D_t8_224* ____descriptions, String_t* ____error);
// System.IAsyncResult VoxelBusters.NativePlugins.AchievementDescription/LoadAchievementDescriptionsCompletion::BeginInvoke(VoxelBusters.NativePlugins.AchievementDescription[],System.String,System.AsyncCallback,System.Object)
extern "C" Object_t * LoadAchievementDescriptionsCompletion_BeginInvoke_m8_1265 (LoadAchievementDescriptionsCompletion_t8_223 * __this, AchievementDescriptionU5BU5D_t8_224* ____descriptions, String_t* ____error, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.AchievementDescription/LoadAchievementDescriptionsCompletion::EndInvoke(System.IAsyncResult)
extern "C" void LoadAchievementDescriptionsCompletion_EndInvoke_m8_1266 (LoadAchievementDescriptionsCompletion_t8_223 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
