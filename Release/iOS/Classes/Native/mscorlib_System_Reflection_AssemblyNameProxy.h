﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_MarshalByRefObject.h"

// System.Reflection.AssemblyNameProxy
struct  AssemblyNameProxy_t1_580  : public MarshalByRefObject_t1_69
{
};
