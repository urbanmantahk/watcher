﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Globalization.CCGregorianEraHandler
struct CCGregorianEraHandler_t1_353;

#include "mscorlib_System_Globalization_Calendar.h"
#include "mscorlib_System_DateTime.h"

// System.Globalization.JapaneseCalendar
struct  JapaneseCalendar_t1_378  : public Calendar_t1_338
{
};
struct JapaneseCalendar_t1_378_StaticFields{
	// System.Globalization.CCGregorianEraHandler System.Globalization.JapaneseCalendar::M_EraHandler
	CCGregorianEraHandler_t1_353 * ___M_EraHandler_7;
	// System.DateTime System.Globalization.JapaneseCalendar::JapanMin
	DateTime_t1_150  ___JapanMin_8;
	// System.DateTime System.Globalization.JapaneseCalendar::JapanMax
	DateTime_t1_150  ___JapanMax_9;
};
