﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.DebugPRO.Internal.ConsoleTag
struct ConsoleTag_t8_171;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.DebugPRO.Internal.ConsoleTag::.ctor()
extern "C" void ConsoleTag__ctor_m8_1032 (ConsoleTag_t8_171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.DebugPRO.Internal.ConsoleTag::.ctor(System.String,System.Boolean,System.Boolean)
extern "C" void ConsoleTag__ctor_m8_1033 (ConsoleTag_t8_171 * __this, String_t* ____tagName, bool ____isActive, bool ____ignore, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.DebugPRO.Internal.ConsoleTag::get_Name()
extern "C" String_t* ConsoleTag_get_Name_m8_1034 (ConsoleTag_t8_171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.DebugPRO.Internal.ConsoleTag::set_Name(System.String)
extern "C" void ConsoleTag_set_Name_m8_1035 (ConsoleTag_t8_171 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.DebugPRO.Internal.ConsoleTag::get_IsActive()
extern "C" bool ConsoleTag_get_IsActive_m8_1036 (ConsoleTag_t8_171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.DebugPRO.Internal.ConsoleTag::set_IsActive(System.Boolean)
extern "C" void ConsoleTag_set_IsActive_m8_1037 (ConsoleTag_t8_171 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.DebugPRO.Internal.ConsoleTag::get_Ignore()
extern "C" bool ConsoleTag_get_Ignore_m8_1038 (ConsoleTag_t8_171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.DebugPRO.Internal.ConsoleTag::set_Ignore(System.Boolean)
extern "C" void ConsoleTag_set_Ignore_m8_1039 (ConsoleTag_t8_171 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
