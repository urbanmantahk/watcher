﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.X509.X520/StateOrProvinceName
struct StateOrProvinceName_t1_205;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Security.X509.X520/StateOrProvinceName::.ctor()
extern "C" void StateOrProvinceName__ctor_m1_2405 (StateOrProvinceName_t1_205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
