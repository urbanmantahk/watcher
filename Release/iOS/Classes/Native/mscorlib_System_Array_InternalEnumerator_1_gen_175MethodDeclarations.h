﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_genMethodDeclarations.h"

// System.Void System.Array/InternalEnumerator`1<VoxelBusters.AssetStoreProductUtility.Demo.DemoSubMenu>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m1_25778(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1_2631 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m1_15070_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<VoxelBusters.AssetStoreProductUtility.Demo.DemoSubMenu>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_25779(__this, method) (( void (*) (InternalEnumerator_1_t1_2631 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_15071_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<VoxelBusters.AssetStoreProductUtility.Demo.DemoSubMenu>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_25780(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1_2631 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_15072_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<VoxelBusters.AssetStoreProductUtility.Demo.DemoSubMenu>::Dispose()
#define InternalEnumerator_1_Dispose_m1_25781(__this, method) (( void (*) (InternalEnumerator_1_t1_2631 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1_15073_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<VoxelBusters.AssetStoreProductUtility.Demo.DemoSubMenu>::MoveNext()
#define InternalEnumerator_1_MoveNext_m1_25782(__this, method) (( bool (*) (InternalEnumerator_1_t1_2631 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1_15074_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<VoxelBusters.AssetStoreProductUtility.Demo.DemoSubMenu>::get_Current()
#define InternalEnumerator_1_get_Current_m1_25783(__this, method) (( DemoSubMenu_t8_18 * (*) (InternalEnumerator_1_t1_2631 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1_15075_gshared)(__this, method)
