﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// VoxelBusters.NativePlugins.UI/AlertDialogCompletion
struct AlertDialogCompletion_t8_300;

#include "mscorlib_System_ValueType.h"

// System.Collections.Generic.KeyValuePair`2<System.String,VoxelBusters.NativePlugins.UI/AlertDialogCompletion>
struct  KeyValuePair_2_t1_2754 
{
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	AlertDialogCompletion_t8_300 * ___value_1;
};
