﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.IO.UnexceptionalStreamWriter
struct UnexceptionalStreamWriter_t1_459;
// System.IO.Stream
struct Stream_t1_405;
// System.Text.Encoding
struct Encoding_t1_406;
// System.String
struct String_t;
// System.Char[]
struct CharU5BU5D_t1_16;

#include "codegen/il2cpp-codegen.h"

// System.Void System.IO.UnexceptionalStreamWriter::.ctor(System.IO.Stream)
extern "C" void UnexceptionalStreamWriter__ctor_m1_5416 (UnexceptionalStreamWriter_t1_459 * __this, Stream_t1_405 * ___stream, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.UnexceptionalStreamWriter::.ctor(System.IO.Stream,System.Text.Encoding)
extern "C" void UnexceptionalStreamWriter__ctor_m1_5417 (UnexceptionalStreamWriter_t1_459 * __this, Stream_t1_405 * ___stream, Encoding_t1_406 * ___encoding, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.UnexceptionalStreamWriter::.ctor(System.IO.Stream,System.Text.Encoding,System.Int32)
extern "C" void UnexceptionalStreamWriter__ctor_m1_5418 (UnexceptionalStreamWriter_t1_459 * __this, Stream_t1_405 * ___stream, Encoding_t1_406 * ___encoding, int32_t ___bufferSize, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.UnexceptionalStreamWriter::.ctor(System.String)
extern "C" void UnexceptionalStreamWriter__ctor_m1_5419 (UnexceptionalStreamWriter_t1_459 * __this, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.UnexceptionalStreamWriter::.ctor(System.String,System.Boolean)
extern "C" void UnexceptionalStreamWriter__ctor_m1_5420 (UnexceptionalStreamWriter_t1_459 * __this, String_t* ___path, bool ___append, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.UnexceptionalStreamWriter::.ctor(System.String,System.Boolean,System.Text.Encoding)
extern "C" void UnexceptionalStreamWriter__ctor_m1_5421 (UnexceptionalStreamWriter_t1_459 * __this, String_t* ___path, bool ___append, Encoding_t1_406 * ___encoding, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.UnexceptionalStreamWriter::.ctor(System.String,System.Boolean,System.Text.Encoding,System.Int32)
extern "C" void UnexceptionalStreamWriter__ctor_m1_5422 (UnexceptionalStreamWriter_t1_459 * __this, String_t* ___path, bool ___append, Encoding_t1_406 * ___encoding, int32_t ___bufferSize, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.UnexceptionalStreamWriter::Flush()
extern "C" void UnexceptionalStreamWriter_Flush_m1_5423 (UnexceptionalStreamWriter_t1_459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.UnexceptionalStreamWriter::Write(System.Char[],System.Int32,System.Int32)
extern "C" void UnexceptionalStreamWriter_Write_m1_5424 (UnexceptionalStreamWriter_t1_459 * __this, CharU5BU5D_t1_16* ___buffer, int32_t ___index, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.UnexceptionalStreamWriter::Write(System.Char)
extern "C" void UnexceptionalStreamWriter_Write_m1_5425 (UnexceptionalStreamWriter_t1_459 * __this, uint16_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.UnexceptionalStreamWriter::Write(System.Char[])
extern "C" void UnexceptionalStreamWriter_Write_m1_5426 (UnexceptionalStreamWriter_t1_459 * __this, CharU5BU5D_t1_16* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.UnexceptionalStreamWriter::Write(System.String)
extern "C" void UnexceptionalStreamWriter_Write_m1_5427 (UnexceptionalStreamWriter_t1_459 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
