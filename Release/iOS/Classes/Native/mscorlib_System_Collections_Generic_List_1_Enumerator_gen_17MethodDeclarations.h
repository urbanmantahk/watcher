﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t1_1836;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_17.h"
#include "UnityEngine_UnityEngine_Vector2.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m1_19217_gshared (Enumerator_t1_2290 * __this, List_1_t1_1836 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m1_19217(__this, ___l, method) (( void (*) (Enumerator_t1_2290 *, List_1_t1_1836 *, const MethodInfo*))Enumerator__ctor_m1_19217_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m1_19218_gshared (Enumerator_t1_2290 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1_19218(__this, method) (( void (*) (Enumerator_t1_2290 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_19218_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m1_19219_gshared (Enumerator_t1_2290 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_19219(__this, method) (( Object_t * (*) (Enumerator_t1_2290 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_19219_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::Dispose()
extern "C" void Enumerator_Dispose_m1_19220_gshared (Enumerator_t1_2290 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1_19220(__this, method) (( void (*) (Enumerator_t1_2290 *, const MethodInfo*))Enumerator_Dispose_m1_19220_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::VerifyState()
extern "C" void Enumerator_VerifyState_m1_19221_gshared (Enumerator_t1_2290 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m1_19221(__this, method) (( void (*) (Enumerator_t1_2290 *, const MethodInfo*))Enumerator_VerifyState_m1_19221_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::MoveNext()
extern "C" bool Enumerator_MoveNext_m1_19222_gshared (Enumerator_t1_2290 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1_19222(__this, method) (( bool (*) (Enumerator_t1_2290 *, const MethodInfo*))Enumerator_MoveNext_m1_19222_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::get_Current()
extern "C" Vector2_t6_47  Enumerator_get_Current_m1_19223_gshared (Enumerator_t1_2290 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1_19223(__this, method) (( Vector2_t6_47  (*) (Enumerator_t1_2290 *, const MethodInfo*))Enumerator_get_Current_m1_19223_gshared)(__this, method)
