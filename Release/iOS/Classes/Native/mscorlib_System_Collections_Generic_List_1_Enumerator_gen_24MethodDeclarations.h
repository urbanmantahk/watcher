﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.UICharInfo>
struct List_1_t1_1825;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_24.h"
#include "UnityEngine_UnityEngine_UICharInfo.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m1_20515_gshared (Enumerator_t1_2360 * __this, List_1_t1_1825 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m1_20515(__this, ___l, method) (( void (*) (Enumerator_t1_2360 *, List_1_t1_1825 *, const MethodInfo*))Enumerator__ctor_m1_20515_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m1_20516_gshared (Enumerator_t1_2360 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1_20516(__this, method) (( void (*) (Enumerator_t1_2360 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_20516_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m1_20517_gshared (Enumerator_t1_2360 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_20517(__this, method) (( Object_t * (*) (Enumerator_t1_2360 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_20517_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::Dispose()
extern "C" void Enumerator_Dispose_m1_20518_gshared (Enumerator_t1_2360 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1_20518(__this, method) (( void (*) (Enumerator_t1_2360 *, const MethodInfo*))Enumerator_Dispose_m1_20518_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::VerifyState()
extern "C" void Enumerator_VerifyState_m1_20519_gshared (Enumerator_t1_2360 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m1_20519(__this, method) (( void (*) (Enumerator_t1_2360 *, const MethodInfo*))Enumerator_VerifyState_m1_20519_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::MoveNext()
extern "C" bool Enumerator_MoveNext_m1_20520_gshared (Enumerator_t1_2360 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1_20520(__this, method) (( bool (*) (Enumerator_t1_2360 *, const MethodInfo*))Enumerator_MoveNext_m1_20520_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::get_Current()
extern "C" UICharInfo_t6_150  Enumerator_get_Current_m1_20521_gshared (Enumerator_t1_2360 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1_20521(__this, method) (( UICharInfo_t6_150  (*) (Enumerator_t1_2360 *, const MethodInfo*))Enumerator_get_Current_m1_20521_gshared)(__this, method)
