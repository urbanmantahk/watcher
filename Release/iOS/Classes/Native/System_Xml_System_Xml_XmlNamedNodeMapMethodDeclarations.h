﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XmlNamedNodeMap
struct XmlNamedNodeMap_t4_121;
// System.Xml.XmlNode
struct XmlNode_t4_116;
// System.Collections.ArrayList
struct ArrayList_t1_170;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Xml.XmlNamedNodeMap::.ctor(System.Xml.XmlNode)
extern "C" void XmlNamedNodeMap__ctor_m4_587 (XmlNamedNodeMap_t4_121 * __this, XmlNode_t4_116 * ___parent, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XmlNamedNodeMap::.cctor()
extern "C" void XmlNamedNodeMap__cctor_m4_588 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList System.Xml.XmlNamedNodeMap::get_NodeList()
extern "C" ArrayList_t1_170 * XmlNamedNodeMap_get_NodeList_m4_589 (XmlNamedNodeMap_t4_121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Xml.XmlNamedNodeMap::get_Count()
extern "C" int32_t XmlNamedNodeMap_get_Count_m4_590 (XmlNamedNodeMap_t4_121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Xml.XmlNamedNodeMap::GetEnumerator()
extern "C" Object_t * XmlNamedNodeMap_GetEnumerator_m4_591 (XmlNamedNodeMap_t4_121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNode System.Xml.XmlNamedNodeMap::GetNamedItem(System.String)
extern "C" XmlNode_t4_116 * XmlNamedNodeMap_GetNamedItem_m4_592 (XmlNamedNodeMap_t4_121 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNode System.Xml.XmlNamedNodeMap::RemoveNamedItem(System.String,System.String)
extern "C" XmlNode_t4_116 * XmlNamedNodeMap_RemoveNamedItem_m4_593 (XmlNamedNodeMap_t4_121 * __this, String_t* ___localName, String_t* ___namespaceURI, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNode System.Xml.XmlNamedNodeMap::SetNamedItem(System.Xml.XmlNode)
extern "C" XmlNode_t4_116 * XmlNamedNodeMap_SetNamedItem_m4_594 (XmlNamedNodeMap_t4_121 * __this, XmlNode_t4_116 * ___node, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNode System.Xml.XmlNamedNodeMap::SetNamedItem(System.Xml.XmlNode,System.Int32,System.Boolean)
extern "C" XmlNode_t4_116 * XmlNamedNodeMap_SetNamedItem_m4_595 (XmlNamedNodeMap_t4_121 * __this, XmlNode_t4_116 * ___node, int32_t ___pos, bool ___raiseEvent, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList System.Xml.XmlNamedNodeMap::get_Nodes()
extern "C" ArrayList_t1_170 * XmlNamedNodeMap_get_Nodes_m4_596 (XmlNamedNodeMap_t4_121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
