﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Channels.ClientChannelSinkStack
struct ClientChannelSinkStack_t1_877;
// System.Runtime.Remoting.Messaging.IMessageSink
struct IMessageSink_t1_874;
// System.Runtime.Remoting.Channels.ITransportHeaders
struct ITransportHeaders_t1_1711;
// System.IO.Stream
struct Stream_t1_405;
// System.Exception
struct Exception_t1_33;
// System.Runtime.Remoting.Messaging.IMessage
struct IMessage_t1_875;
// System.Object
struct Object_t;
// System.Runtime.Remoting.Channels.IClientChannelSink
struct IClientChannelSink_t1_1712;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Channels.ClientChannelSinkStack::.ctor()
extern "C" void ClientChannelSinkStack__ctor_m1_8064 (ClientChannelSinkStack_t1_877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Channels.ClientChannelSinkStack::.ctor(System.Runtime.Remoting.Messaging.IMessageSink)
extern "C" void ClientChannelSinkStack__ctor_m1_8065 (ClientChannelSinkStack_t1_877 * __this, Object_t * ___replySink, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Channels.ClientChannelSinkStack::AsyncProcessResponse(System.Runtime.Remoting.Channels.ITransportHeaders,System.IO.Stream)
extern "C" void ClientChannelSinkStack_AsyncProcessResponse_m1_8066 (ClientChannelSinkStack_t1_877 * __this, Object_t * ___headers, Stream_t1_405 * ___stream, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Channels.ClientChannelSinkStack::DispatchException(System.Exception)
extern "C" void ClientChannelSinkStack_DispatchException_m1_8067 (ClientChannelSinkStack_t1_877 * __this, Exception_t1_33 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Channels.ClientChannelSinkStack::DispatchReplyMessage(System.Runtime.Remoting.Messaging.IMessage)
extern "C" void ClientChannelSinkStack_DispatchReplyMessage_m1_8068 (ClientChannelSinkStack_t1_877 * __this, Object_t * ___msg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Channels.ClientChannelSinkStack::Pop(System.Runtime.Remoting.Channels.IClientChannelSink)
extern "C" Object_t * ClientChannelSinkStack_Pop_m1_8069 (ClientChannelSinkStack_t1_877 * __this, Object_t * ___sink, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Channels.ClientChannelSinkStack::Push(System.Runtime.Remoting.Channels.IClientChannelSink,System.Object)
extern "C" void ClientChannelSinkStack_Push_m1_8070 (ClientChannelSinkStack_t1_877 * __this, Object_t * ___sink, Object_t * ___state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
