﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Channels.ServerDispatchSink
struct ServerDispatchSink_t1_886;
// System.Runtime.Remoting.Channels.IServerChannelSink
struct IServerChannelSink_t1_1706;
// System.Collections.IDictionary
struct IDictionary_t1_35;
// System.Runtime.Remoting.Channels.IServerResponseChannelSinkStack
struct IServerResponseChannelSinkStack_t1_1713;
// System.Object
struct Object_t;
// System.Runtime.Remoting.Messaging.IMessage
struct IMessage_t1_875;
// System.Runtime.Remoting.Channels.ITransportHeaders
struct ITransportHeaders_t1_1711;
// System.IO.Stream
struct Stream_t1_405;
// System.Runtime.Remoting.Channels.IServerChannelSinkStack
struct IServerChannelSinkStack_t1_1709;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Remoting_Channels_ServerProcessing.h"

// System.Void System.Runtime.Remoting.Channels.ServerDispatchSink::.ctor()
extern "C" void ServerDispatchSink__ctor_m1_8108 (ServerDispatchSink_t1_886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Channels.IServerChannelSink System.Runtime.Remoting.Channels.ServerDispatchSink::get_NextChannelSink()
extern "C" Object_t * ServerDispatchSink_get_NextChannelSink_m1_8109 (ServerDispatchSink_t1_886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IDictionary System.Runtime.Remoting.Channels.ServerDispatchSink::get_Properties()
extern "C" Object_t * ServerDispatchSink_get_Properties_m1_8110 (ServerDispatchSink_t1_886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Channels.ServerDispatchSink::AsyncProcessResponse(System.Runtime.Remoting.Channels.IServerResponseChannelSinkStack,System.Object,System.Runtime.Remoting.Messaging.IMessage,System.Runtime.Remoting.Channels.ITransportHeaders,System.IO.Stream)
extern "C" void ServerDispatchSink_AsyncProcessResponse_m1_8111 (ServerDispatchSink_t1_886 * __this, Object_t * ___sinkStack, Object_t * ___state, Object_t * ___msg, Object_t * ___headers, Stream_t1_405 * ___stream, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream System.Runtime.Remoting.Channels.ServerDispatchSink::GetResponseStream(System.Runtime.Remoting.Channels.IServerResponseChannelSinkStack,System.Object,System.Runtime.Remoting.Messaging.IMessage,System.Runtime.Remoting.Channels.ITransportHeaders)
extern "C" Stream_t1_405 * ServerDispatchSink_GetResponseStream_m1_8112 (ServerDispatchSink_t1_886 * __this, Object_t * ___sinkStack, Object_t * ___state, Object_t * ___msg, Object_t * ___headers, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Channels.ServerProcessing System.Runtime.Remoting.Channels.ServerDispatchSink::ProcessMessage(System.Runtime.Remoting.Channels.IServerChannelSinkStack,System.Runtime.Remoting.Messaging.IMessage,System.Runtime.Remoting.Channels.ITransportHeaders,System.IO.Stream,System.Runtime.Remoting.Messaging.IMessage&,System.Runtime.Remoting.Channels.ITransportHeaders&,System.IO.Stream&)
extern "C" int32_t ServerDispatchSink_ProcessMessage_m1_8113 (ServerDispatchSink_t1_886 * __this, Object_t * ___sinkStack, Object_t * ___requestMsg, Object_t * ___requestHeaders, Stream_t1_405 * ___requestStream, Object_t ** ___responseMsg, Object_t ** ___responseHeaders, Stream_t1_405 ** ___responseStream, const MethodInfo* method) IL2CPP_METHOD_ATTR;
