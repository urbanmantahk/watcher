﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.IO.TextWriter
struct TextWriter_t1_449;
// System.IFormatProvider
struct IFormatProvider_t1_455;
// System.String
struct String_t;
// System.Char[]
struct CharU5BU5D_t1_16;
// System.Object
struct Object_t;
// System.Object[]
struct ObjectU5BU5D_t1_272;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Decimal.h"

// System.Void System.IO.TextWriter::.ctor()
extern "C" void TextWriter__ctor_m1_5310 (TextWriter_t1_449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.TextWriter::.ctor(System.IFormatProvider)
extern "C" void TextWriter__ctor_m1_5311 (TextWriter_t1_449 * __this, Object_t * ___formatProvider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.TextWriter::.cctor()
extern "C" void TextWriter__cctor_m1_5312 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IFormatProvider System.IO.TextWriter::get_FormatProvider()
extern "C" Object_t * TextWriter_get_FormatProvider_m1_5313 (TextWriter_t1_449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.IO.TextWriter::get_NewLine()
extern "C" String_t* TextWriter_get_NewLine_m1_5314 (TextWriter_t1_449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.TextWriter::set_NewLine(System.String)
extern "C" void TextWriter_set_NewLine_m1_5315 (TextWriter_t1_449 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.TextWriter::Close()
extern "C" void TextWriter_Close_m1_5316 (TextWriter_t1_449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.TextWriter::Dispose(System.Boolean)
extern "C" void TextWriter_Dispose_m1_5317 (TextWriter_t1_449 * __this, bool ___disposing, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.TextWriter::Dispose()
extern "C" void TextWriter_Dispose_m1_5318 (TextWriter_t1_449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.TextWriter::Flush()
extern "C" void TextWriter_Flush_m1_5319 (TextWriter_t1_449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.TextWriter System.IO.TextWriter::Synchronized(System.IO.TextWriter)
extern "C" TextWriter_t1_449 * TextWriter_Synchronized_m1_5320 (Object_t * __this /* static, unused */, TextWriter_t1_449 * ___writer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.TextWriter System.IO.TextWriter::Synchronized(System.IO.TextWriter,System.Boolean)
extern "C" TextWriter_t1_449 * TextWriter_Synchronized_m1_5321 (Object_t * __this /* static, unused */, TextWriter_t1_449 * ___writer, bool ___neverClose, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.TextWriter::Write(System.Boolean)
extern "C" void TextWriter_Write_m1_5322 (TextWriter_t1_449 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.TextWriter::Write(System.Char)
extern "C" void TextWriter_Write_m1_5323 (TextWriter_t1_449 * __this, uint16_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.TextWriter::Write(System.Char[])
extern "C" void TextWriter_Write_m1_5324 (TextWriter_t1_449 * __this, CharU5BU5D_t1_16* ___buffer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.TextWriter::Write(System.Decimal)
extern "C" void TextWriter_Write_m1_5325 (TextWriter_t1_449 * __this, Decimal_t1_19  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.TextWriter::Write(System.Double)
extern "C" void TextWriter_Write_m1_5326 (TextWriter_t1_449 * __this, double ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.TextWriter::Write(System.Int32)
extern "C" void TextWriter_Write_m1_5327 (TextWriter_t1_449 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.TextWriter::Write(System.Int64)
extern "C" void TextWriter_Write_m1_5328 (TextWriter_t1_449 * __this, int64_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.TextWriter::Write(System.Object)
extern "C" void TextWriter_Write_m1_5329 (TextWriter_t1_449 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.TextWriter::Write(System.Single)
extern "C" void TextWriter_Write_m1_5330 (TextWriter_t1_449 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.TextWriter::Write(System.String)
extern "C" void TextWriter_Write_m1_5331 (TextWriter_t1_449 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.TextWriter::Write(System.UInt32)
extern "C" void TextWriter_Write_m1_5332 (TextWriter_t1_449 * __this, uint32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.TextWriter::Write(System.UInt64)
extern "C" void TextWriter_Write_m1_5333 (TextWriter_t1_449 * __this, uint64_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.TextWriter::Write(System.String,System.Object)
extern "C" void TextWriter_Write_m1_5334 (TextWriter_t1_449 * __this, String_t* ___format, Object_t * ___arg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.TextWriter::Write(System.String,System.Object[])
extern "C" void TextWriter_Write_m1_5335 (TextWriter_t1_449 * __this, String_t* ___format, ObjectU5BU5D_t1_272* ___arg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.TextWriter::Write(System.Char[],System.Int32,System.Int32)
extern "C" void TextWriter_Write_m1_5336 (TextWriter_t1_449 * __this, CharU5BU5D_t1_16* ___buffer, int32_t ___index, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.TextWriter::Write(System.String,System.Object,System.Object)
extern "C" void TextWriter_Write_m1_5337 (TextWriter_t1_449 * __this, String_t* ___format, Object_t * ___arg0, Object_t * ___arg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.TextWriter::Write(System.String,System.Object,System.Object,System.Object)
extern "C" void TextWriter_Write_m1_5338 (TextWriter_t1_449 * __this, String_t* ___format, Object_t * ___arg0, Object_t * ___arg1, Object_t * ___arg2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.TextWriter::WriteLine()
extern "C" void TextWriter_WriteLine_m1_5339 (TextWriter_t1_449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.TextWriter::WriteLine(System.Boolean)
extern "C" void TextWriter_WriteLine_m1_5340 (TextWriter_t1_449 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.TextWriter::WriteLine(System.Char)
extern "C" void TextWriter_WriteLine_m1_5341 (TextWriter_t1_449 * __this, uint16_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.TextWriter::WriteLine(System.Char[])
extern "C" void TextWriter_WriteLine_m1_5342 (TextWriter_t1_449 * __this, CharU5BU5D_t1_16* ___buffer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.TextWriter::WriteLine(System.Decimal)
extern "C" void TextWriter_WriteLine_m1_5343 (TextWriter_t1_449 * __this, Decimal_t1_19  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.TextWriter::WriteLine(System.Double)
extern "C" void TextWriter_WriteLine_m1_5344 (TextWriter_t1_449 * __this, double ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.TextWriter::WriteLine(System.Int32)
extern "C" void TextWriter_WriteLine_m1_5345 (TextWriter_t1_449 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.TextWriter::WriteLine(System.Int64)
extern "C" void TextWriter_WriteLine_m1_5346 (TextWriter_t1_449 * __this, int64_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.TextWriter::WriteLine(System.Object)
extern "C" void TextWriter_WriteLine_m1_5347 (TextWriter_t1_449 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.TextWriter::WriteLine(System.Single)
extern "C" void TextWriter_WriteLine_m1_5348 (TextWriter_t1_449 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.TextWriter::WriteLine(System.String)
extern "C" void TextWriter_WriteLine_m1_5349 (TextWriter_t1_449 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.TextWriter::WriteLine(System.UInt32)
extern "C" void TextWriter_WriteLine_m1_5350 (TextWriter_t1_449 * __this, uint32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.TextWriter::WriteLine(System.UInt64)
extern "C" void TextWriter_WriteLine_m1_5351 (TextWriter_t1_449 * __this, uint64_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.TextWriter::WriteLine(System.String,System.Object)
extern "C" void TextWriter_WriteLine_m1_5352 (TextWriter_t1_449 * __this, String_t* ___format, Object_t * ___arg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.TextWriter::WriteLine(System.String,System.Object[])
extern "C" void TextWriter_WriteLine_m1_5353 (TextWriter_t1_449 * __this, String_t* ___format, ObjectU5BU5D_t1_272* ___arg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.TextWriter::WriteLine(System.Char[],System.Int32,System.Int32)
extern "C" void TextWriter_WriteLine_m1_5354 (TextWriter_t1_449 * __this, CharU5BU5D_t1_16* ___buffer, int32_t ___index, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.TextWriter::WriteLine(System.String,System.Object,System.Object)
extern "C" void TextWriter_WriteLine_m1_5355 (TextWriter_t1_449 * __this, String_t* ___format, Object_t * ___arg0, Object_t * ___arg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.TextWriter::WriteLine(System.String,System.Object,System.Object,System.Object)
extern "C" void TextWriter_WriteLine_m1_5356 (TextWriter_t1_449 * __this, String_t* ___format, Object_t * ___arg0, Object_t * ___arg1, Object_t * ___arg2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
