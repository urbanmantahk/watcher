﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.MessageShareComposer
struct MessageShareComposer_t8_281;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1_238;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.MessageShareComposer::.ctor()
extern "C" void MessageShareComposer__ctor_m8_1666 (MessageShareComposer_t8_281 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.MessageShareComposer::get_Body()
extern "C" String_t* MessageShareComposer_get_Body_m8_1667 (MessageShareComposer_t8_281 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.MessageShareComposer::set_Body(System.String)
extern "C" void MessageShareComposer_set_Body_m8_1668 (MessageShareComposer_t8_281 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] VoxelBusters.NativePlugins.MessageShareComposer::get_ToRecipients()
extern "C" StringU5BU5D_t1_238* MessageShareComposer_get_ToRecipients_m8_1669 (MessageShareComposer_t8_281 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.MessageShareComposer::set_ToRecipients(System.String[])
extern "C" void MessageShareComposer_set_ToRecipients_m8_1670 (MessageShareComposer_t8_281 * __this, StringU5BU5D_t1_238* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.NativePlugins.MessageShareComposer::get_IsReadyToShowView()
extern "C" bool MessageShareComposer_get_IsReadyToShowView_m8_1671 (MessageShareComposer_t8_281 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
