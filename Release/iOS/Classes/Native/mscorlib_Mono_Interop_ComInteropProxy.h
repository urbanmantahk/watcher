﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.__ComObject
struct __ComObject_t1_131;
// System.String
struct String_t;

#include "mscorlib_System_Runtime_Remoting_Proxies_RealProxy.h"

// Mono.Interop.ComInteropProxy
struct  ComInteropProxy_t1_129  : public RealProxy_t1_130
{
	// System.__ComObject Mono.Interop.ComInteropProxy::com_object
	__ComObject_t1_131 * ___com_object_8;
	// System.Int32 Mono.Interop.ComInteropProxy::ref_count
	int32_t ___ref_count_9;
	// System.String Mono.Interop.ComInteropProxy::type_name
	String_t* ___type_name_10;
};
