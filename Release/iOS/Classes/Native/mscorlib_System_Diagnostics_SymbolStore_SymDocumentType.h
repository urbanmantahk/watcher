﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object.h"
#include "mscorlib_System_Guid.h"

// System.Diagnostics.SymbolStore.SymDocumentType
struct  SymDocumentType_t1_318  : public Object_t
{
};
struct SymDocumentType_t1_318_StaticFields{
	// System.Guid System.Diagnostics.SymbolStore.SymDocumentType::Text
	Guid_t1_319  ___Text_0;
};
