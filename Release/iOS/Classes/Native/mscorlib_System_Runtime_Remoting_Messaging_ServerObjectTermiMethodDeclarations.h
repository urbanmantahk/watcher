﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Messaging.ServerObjectTerminatorSink
struct ServerObjectTerminatorSink_t1_962;
// System.Runtime.Remoting.Messaging.IMessageSink
struct IMessageSink_t1_874;
// System.Runtime.Remoting.Messaging.IMessage
struct IMessage_t1_875;
// System.Runtime.Remoting.Messaging.IMessageCtrl
struct IMessageCtrl_t1_920;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Messaging.ServerObjectTerminatorSink::.ctor(System.Runtime.Remoting.Messaging.IMessageSink)
extern "C" void ServerObjectTerminatorSink__ctor_m1_8652 (ServerObjectTerminatorSink_t1_962 * __this, Object_t * ___nextSink, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Messaging.IMessage System.Runtime.Remoting.Messaging.ServerObjectTerminatorSink::SyncProcessMessage(System.Runtime.Remoting.Messaging.IMessage)
extern "C" Object_t * ServerObjectTerminatorSink_SyncProcessMessage_m1_8653 (ServerObjectTerminatorSink_t1_962 * __this, Object_t * ___msg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Messaging.IMessageCtrl System.Runtime.Remoting.Messaging.ServerObjectTerminatorSink::AsyncProcessMessage(System.Runtime.Remoting.Messaging.IMessage,System.Runtime.Remoting.Messaging.IMessageSink)
extern "C" Object_t * ServerObjectTerminatorSink_AsyncProcessMessage_m1_8654 (ServerObjectTerminatorSink_t1_962 * __this, Object_t * ___msg, Object_t * ___replySink, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Messaging.IMessageSink System.Runtime.Remoting.Messaging.ServerObjectTerminatorSink::get_NextSink()
extern "C" Object_t * ServerObjectTerminatorSink_get_NextSink_m1_8655 (ServerObjectTerminatorSink_t1_962 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
