﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

extern "C" void TYPEATTR_t1_743_marshal(const TYPEATTR_t1_743& unmarshaled, TYPEATTR_t1_743_marshaled& marshaled);
extern "C" void TYPEATTR_t1_743_marshal_back(const TYPEATTR_t1_743_marshaled& marshaled, TYPEATTR_t1_743& unmarshaled);
extern "C" void TYPEATTR_t1_743_marshal_cleanup(TYPEATTR_t1_743_marshaled& marshaled);
