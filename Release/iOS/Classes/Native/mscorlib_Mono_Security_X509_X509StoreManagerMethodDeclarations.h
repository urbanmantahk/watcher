﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.X509.X509StoreManager
struct X509StoreManager_t1_197;
// Mono.Security.X509.X509Stores
struct X509Stores_t1_198;
// Mono.Security.X509.X509CertificateCollection
struct X509CertificateCollection_t1_148;
// System.Collections.ArrayList
struct ArrayList_t1_170;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Security.X509.X509StoreManager::.ctor()
extern "C" void X509StoreManager__ctor_m1_2374 (X509StoreManager_t1_197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Stores Mono.Security.X509.X509StoreManager::get_CurrentUser()
extern "C" X509Stores_t1_198 * X509StoreManager_get_CurrentUser_m1_2375 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Stores Mono.Security.X509.X509StoreManager::get_LocalMachine()
extern "C" X509Stores_t1_198 * X509StoreManager_get_LocalMachine_m1_2376 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509CertificateCollection Mono.Security.X509.X509StoreManager::get_IntermediateCACertificates()
extern "C" X509CertificateCollection_t1_148 * X509StoreManager_get_IntermediateCACertificates_m1_2377 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList Mono.Security.X509.X509StoreManager::get_IntermediateCACrls()
extern "C" ArrayList_t1_170 * X509StoreManager_get_IntermediateCACrls_m1_2378 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509CertificateCollection Mono.Security.X509.X509StoreManager::get_TrustedRootCertificates()
extern "C" X509CertificateCollection_t1_148 * X509StoreManager_get_TrustedRootCertificates_m1_2379 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList Mono.Security.X509.X509StoreManager::get_TrustedRootCACrls()
extern "C" ArrayList_t1_170 * X509StoreManager_get_TrustedRootCACrls_m1_2380 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509CertificateCollection Mono.Security.X509.X509StoreManager::get_UntrustedCertificates()
extern "C" X509CertificateCollection_t1_148 * X509StoreManager_get_UntrustedCertificates_m1_2381 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
