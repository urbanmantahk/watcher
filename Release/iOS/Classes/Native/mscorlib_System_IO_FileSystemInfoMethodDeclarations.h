﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.IO.FileSystemInfo
struct FileSystemInfo_t1_411;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_IO_FileAttributes.h"
#include "mscorlib_System_DateTime.h"

// System.Void System.IO.FileSystemInfo::.ctor()
extern "C" void FileSystemInfo__ctor_m1_5016 (FileSystemInfo_t1_411 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.FileSystemInfo::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void FileSystemInfo__ctor_m1_5017 (FileSystemInfo_t1_411 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.FileSystemInfo::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void FileSystemInfo_GetObjectData_m1_5018 (FileSystemInfo_t1_411 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.IO.FileSystemInfo::get_FullName()
extern "C" String_t* FileSystemInfo_get_FullName_m1_5019 (FileSystemInfo_t1_411 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.IO.FileSystemInfo::get_Extension()
extern "C" String_t* FileSystemInfo_get_Extension_m1_5020 (FileSystemInfo_t1_411 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.FileAttributes System.IO.FileSystemInfo::get_Attributes()
extern "C" int32_t FileSystemInfo_get_Attributes_m1_5021 (FileSystemInfo_t1_411 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.FileSystemInfo::set_Attributes(System.IO.FileAttributes)
extern "C" void FileSystemInfo_set_Attributes_m1_5022 (FileSystemInfo_t1_411 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.IO.FileSystemInfo::get_CreationTime()
extern "C" DateTime_t1_150  FileSystemInfo_get_CreationTime_m1_5023 (FileSystemInfo_t1_411 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.FileSystemInfo::set_CreationTime(System.DateTime)
extern "C" void FileSystemInfo_set_CreationTime_m1_5024 (FileSystemInfo_t1_411 * __this, DateTime_t1_150  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.IO.FileSystemInfo::get_CreationTimeUtc()
extern "C" DateTime_t1_150  FileSystemInfo_get_CreationTimeUtc_m1_5025 (FileSystemInfo_t1_411 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.FileSystemInfo::set_CreationTimeUtc(System.DateTime)
extern "C" void FileSystemInfo_set_CreationTimeUtc_m1_5026 (FileSystemInfo_t1_411 * __this, DateTime_t1_150  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.IO.FileSystemInfo::get_LastAccessTime()
extern "C" DateTime_t1_150  FileSystemInfo_get_LastAccessTime_m1_5027 (FileSystemInfo_t1_411 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.FileSystemInfo::set_LastAccessTime(System.DateTime)
extern "C" void FileSystemInfo_set_LastAccessTime_m1_5028 (FileSystemInfo_t1_411 * __this, DateTime_t1_150  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.IO.FileSystemInfo::get_LastAccessTimeUtc()
extern "C" DateTime_t1_150  FileSystemInfo_get_LastAccessTimeUtc_m1_5029 (FileSystemInfo_t1_411 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.FileSystemInfo::set_LastAccessTimeUtc(System.DateTime)
extern "C" void FileSystemInfo_set_LastAccessTimeUtc_m1_5030 (FileSystemInfo_t1_411 * __this, DateTime_t1_150  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.IO.FileSystemInfo::get_LastWriteTime()
extern "C" DateTime_t1_150  FileSystemInfo_get_LastWriteTime_m1_5031 (FileSystemInfo_t1_411 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.FileSystemInfo::set_LastWriteTime(System.DateTime)
extern "C" void FileSystemInfo_set_LastWriteTime_m1_5032 (FileSystemInfo_t1_411 * __this, DateTime_t1_150  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.IO.FileSystemInfo::get_LastWriteTimeUtc()
extern "C" DateTime_t1_150  FileSystemInfo_get_LastWriteTimeUtc_m1_5033 (FileSystemInfo_t1_411 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.FileSystemInfo::set_LastWriteTimeUtc(System.DateTime)
extern "C" void FileSystemInfo_set_LastWriteTimeUtc_m1_5034 (FileSystemInfo_t1_411 * __this, DateTime_t1_150  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.FileSystemInfo::Refresh()
extern "C" void FileSystemInfo_Refresh_m1_5035 (FileSystemInfo_t1_411 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.FileSystemInfo::Refresh(System.Boolean)
extern "C" void FileSystemInfo_Refresh_m1_5036 (FileSystemInfo_t1_411 * __this, bool ___force, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.FileSystemInfo::InternalRefresh()
extern "C" void FileSystemInfo_InternalRefresh_m1_5037 (FileSystemInfo_t1_411 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.FileSystemInfo::CheckPath(System.String)
extern "C" void FileSystemInfo_CheckPath_m1_5038 (FileSystemInfo_t1_411 * __this, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
