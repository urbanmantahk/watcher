﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Attribute.h"
#include "mscorlib_System_Runtime_CompilerServices_LoadHint.h"

// System.Runtime.CompilerServices.DependencyAttribute
struct  DependencyAttribute_t1_684  : public Attribute_t1_2
{
	// System.String System.Runtime.CompilerServices.DependencyAttribute::dependentAssembly
	String_t* ___dependentAssembly_0;
	// System.Runtime.CompilerServices.LoadHint System.Runtime.CompilerServices.DependencyAttribute::hint
	int32_t ___hint_1;
};
