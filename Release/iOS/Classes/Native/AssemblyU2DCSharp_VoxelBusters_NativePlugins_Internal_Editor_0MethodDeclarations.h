﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.Internal.EditorBillingProduct
struct EditorBillingProduct_t8_208;
// VoxelBusters.NativePlugins.BillingProduct
struct BillingProduct_t8_207;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.Internal.EditorBillingProduct::.ctor(VoxelBusters.NativePlugins.BillingProduct)
extern "C" void EditorBillingProduct__ctor_m8_1184 (EditorBillingProduct_t8_208 * __this, BillingProduct_t8_207 * ____product, const MethodInfo* method) IL2CPP_METHOD_ATTR;
