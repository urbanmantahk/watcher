﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.CompilerServices.CallConvCdecl
struct CallConvCdecl_t1_673;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.CompilerServices.CallConvCdecl::.ctor()
extern "C" void CallConvCdecl__ctor_m1_7526 (CallConvCdecl_t1_673 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
