﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_151.h"
#include "UnityEngine_UnityEngine_Color.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_19675_gshared (InternalEnumerator_1_t1_2318 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1_19675(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1_2318 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m1_19675_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_19676_gshared (InternalEnumerator_1_t1_2318 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_19676(__this, method) (( void (*) (InternalEnumerator_1_t1_2318 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_19676_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Color>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_19677_gshared (InternalEnumerator_1_t1_2318 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_19677(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1_2318 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_19677_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_19678_gshared (InternalEnumerator_1_t1_2318 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1_19678(__this, method) (( void (*) (InternalEnumerator_1_t1_2318 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1_19678_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Color>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_19679_gshared (InternalEnumerator_1_t1_2318 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1_19679(__this, method) (( bool (*) (InternalEnumerator_1_t1_2318 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1_19679_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Color>::get_Current()
extern "C" Color_t6_40  InternalEnumerator_1_get_Current_m1_19680_gshared (InternalEnumerator_1_t1_2318 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1_19680(__this, method) (( Color_t6_40  (*) (InternalEnumerator_1_t1_2318 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1_19680_gshared)(__this, method)
