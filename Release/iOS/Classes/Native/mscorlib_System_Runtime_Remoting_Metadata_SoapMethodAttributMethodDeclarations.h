﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Metadata.SoapMethodAttribute
struct SoapMethodAttribute_t1_999;
// System.String
struct String_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Metadata.SoapMethodAttribute::.ctor()
extern "C" void SoapMethodAttribute__ctor_m1_8950 (SoapMethodAttribute_t1_999 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.SoapMethodAttribute::get_ResponseXmlElementName()
extern "C" String_t* SoapMethodAttribute_get_ResponseXmlElementName_m1_8951 (SoapMethodAttribute_t1_999 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.SoapMethodAttribute::set_ResponseXmlElementName(System.String)
extern "C" void SoapMethodAttribute_set_ResponseXmlElementName_m1_8952 (SoapMethodAttribute_t1_999 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.SoapMethodAttribute::get_ResponseXmlNamespace()
extern "C" String_t* SoapMethodAttribute_get_ResponseXmlNamespace_m1_8953 (SoapMethodAttribute_t1_999 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.SoapMethodAttribute::set_ResponseXmlNamespace(System.String)
extern "C" void SoapMethodAttribute_set_ResponseXmlNamespace_m1_8954 (SoapMethodAttribute_t1_999 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.SoapMethodAttribute::get_ReturnXmlElementName()
extern "C" String_t* SoapMethodAttribute_get_ReturnXmlElementName_m1_8955 (SoapMethodAttribute_t1_999 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.SoapMethodAttribute::set_ReturnXmlElementName(System.String)
extern "C" void SoapMethodAttribute_set_ReturnXmlElementName_m1_8956 (SoapMethodAttribute_t1_999 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.SoapMethodAttribute::get_SoapAction()
extern "C" String_t* SoapMethodAttribute_get_SoapAction_m1_8957 (SoapMethodAttribute_t1_999 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.SoapMethodAttribute::set_SoapAction(System.String)
extern "C" void SoapMethodAttribute_set_SoapAction_m1_8958 (SoapMethodAttribute_t1_999 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Remoting.Metadata.SoapMethodAttribute::get_UseAttribute()
extern "C" bool SoapMethodAttribute_get_UseAttribute_m1_8959 (SoapMethodAttribute_t1_999 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.SoapMethodAttribute::set_UseAttribute(System.Boolean)
extern "C" void SoapMethodAttribute_set_UseAttribute_m1_8960 (SoapMethodAttribute_t1_999 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.SoapMethodAttribute::get_XmlNamespace()
extern "C" String_t* SoapMethodAttribute_get_XmlNamespace_m1_8961 (SoapMethodAttribute_t1_999 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.SoapMethodAttribute::set_XmlNamespace(System.String)
extern "C" void SoapMethodAttribute_set_XmlNamespace_m1_8962 (SoapMethodAttribute_t1_999 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.SoapMethodAttribute::SetReflectionObject(System.Object)
extern "C" void SoapMethodAttribute_SetReflectionObject_m1_8963 (SoapMethodAttribute_t1_999 * __this, Object_t * ___reflectionObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
