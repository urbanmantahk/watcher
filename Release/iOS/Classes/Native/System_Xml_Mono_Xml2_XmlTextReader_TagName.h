﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_ValueType.h"

// Mono.Xml2.XmlTextReader/TagName
struct  TagName_t4_171 
{
	// System.String Mono.Xml2.XmlTextReader/TagName::Name
	String_t* ___Name_0;
	// System.String Mono.Xml2.XmlTextReader/TagName::LocalName
	String_t* ___LocalName_1;
	// System.String Mono.Xml2.XmlTextReader/TagName::Prefix
	String_t* ___Prefix_2;
};
// Native definition for marshalling of: Mono.Xml2.XmlTextReader/TagName
struct TagName_t4_171_marshaled
{
	char* ___Name_0;
	char* ___LocalName_1;
	char* ___Prefix_2;
};
