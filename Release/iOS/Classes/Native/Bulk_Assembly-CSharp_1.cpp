﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// ExifLibrary.GPSTimeStamp
struct GPSTimeStamp_t8_108;
// ExifLibrary.MathEx/UFraction32[]
struct UFraction32U5BU5D_t8_122;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.IO.FileStream
struct FileStream_t1_146;
// ExifLibrary.ExifFile
struct ExifFile_t8_110;
// System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>
struct Dictionary_2_t1_1904;
// ExifLibrary.JPEGFile
struct JPEGFile_t8_111;
// ExifLibrary.ExifProperty
struct ExifProperty_t8_99;
// System.IO.MemoryStream
struct MemoryStream_t1_433;
// System.IO.Stream
struct Stream_t1_405;
// ExifLibrary.JPEGSection
struct JPEGSection_t8_112;
// System.Object
struct Object_t;
// ExifLibrary.ExifByte
struct ExifByte_t8_114;
// ExifLibrary.ExifByteArray
struct ExifByteArray_t8_115;
// ExifLibrary.ExifAscii
struct ExifAscii_t8_116;
// ExifLibrary.ExifUShort
struct ExifUShort_t8_117;
// ExifLibrary.ExifUShortArray
struct ExifUShortArray_t8_103;
// System.UInt16[]
struct UInt16U5BU5D_t1_1231;
// ExifLibrary.ExifUInt
struct ExifUInt_t8_118;
// ExifLibrary.ExifUIntArray
struct ExifUIntArray_t8_119;
// System.UInt32[]
struct UInt32U5BU5D_t1_142;
// ExifLibrary.ExifURational
struct ExifURational_t8_120;
// ExifLibrary.ExifURationalArray
struct ExifURationalArray_t8_107;
// System.Single[]
struct SingleU5BU5D_t1_1695;
// ExifLibrary.ExifUndefined
struct ExifUndefined_t8_123;
// ExifLibrary.ExifSInt
struct ExifSInt_t8_124;
// ExifLibrary.ExifSIntArray
struct ExifSIntArray_t8_125;
// System.Int32[]
struct Int32U5BU5D_t1_275;
// ExifLibrary.ExifSRational
struct ExifSRational_t8_126;
// ExifLibrary.ExifSRationalArray
struct ExifSRationalArray_t8_128;
// ExifLibrary.MathEx/Fraction32[]
struct Fraction32U5BU5D_t8_129;
// ExifLibrary.NotValidJPEGFileException
struct NotValidJPEGFileException_t8_134;
// ExifLibrary.SectionExceeds64KBException
struct SectionExceeds64KBException_t8_135;
// System.Collections.Generic.List`1<ExifLibrary.JPEGSection>
struct List_1_t1_1906;
// System.IFormatProvider
struct IFormatProvider_t1_455;
// VoxelBusters.Utility.GUIModalWindow
struct GUIModalWindow_t8_15;
// VoxelBusters.Utility.GUIScrollView
struct GUIScrollView_t8_19;
// UnityEngine.GUISkin
struct GUISkin_t6_169;
// UnityEngine.GUIStyle
struct GUIStyle_t6_176;
// UnityEngine.GUILayoutOption[]
struct GUILayoutOptionU5BU5D_t6_290;
// VoxelBusters.Utility.GUIExtensions
struct GUIExtensions_t8_138;
// System.Collections.ArrayList
struct ArrayList_t1_170;
// System.Action`1<System.String>
struct Action_1_t1_1917;
// VoxelBusters.Utility.UnityGUILayoutUtility
struct UnityGUILayoutUtility_t8_139;
// UnityEngine.GUIContent
struct GUIContent_t6_171;
// VoxelBusters.Utility.UnityGUI.MENU.GUIMainMenu
struct GUIMainMenu_t8_140;
// VoxelBusters.Utility.UnityGUI.MENU.GUISubMenu[]
struct GUISubMenuU5BU5D_t8_142;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// VoxelBusters.Utility.UnityGUI.MENU.GUIMenuBase
struct GUIMenuBase_t8_141;
// VoxelBusters.Utility.UnityGUI.MENU.GUISubMenu
struct GUISubMenu_t8_143;
// UnityEngine.GameObject
struct GameObject_t6_97;
// VoxelBusters.Utility.EditorCoroutine
struct EditorCoroutine_t8_144;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// VoxelBusters.Utility.EnumMaskFieldAttribute
struct EnumMaskFieldAttribute_t8_145;
// System.Type
struct Type_t;
// VoxelBusters.Utility.InspectorButtonAttribute
struct InspectorButtonAttribute_t8_147;
// VoxelBusters.Utility.ExecuteOnValueChangeAttribute
struct ExecuteOnValueChangeAttribute_t8_148;
// VoxelBusters.Utility.RegexAttribute
struct RegexAttribute_t8_149;
// VoxelBusters.Utility.EditorInvoke
struct EditorInvoke_t8_150;
// System.Action
struct Action_t5_11;
// VoxelBusters.Utility.EditorScheduler/CallbackFunction
struct CallbackFunction_t8_151;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;
// VoxelBusters.Utility.EditorScheduler
struct EditorScheduler_t8_152;
// VoxelBusters.Utility.DownloadTextureDemo
struct DownloadTextureDemo_t8_153;
// UnityEngine.Texture2D
struct Texture2D_t6_33;
// VoxelBusters.Utility.Request/<StartAsynchronousRequest>c__Iterator4
struct U3CStartAsynchronousRequestU3Ec__Iterator4_t8_154;
// VoxelBusters.Utility.Request
struct Request_t8_155;
// UnityEngine.WWW
struct WWW_t6_78;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t6_91;
// VoxelBusters.Utility.WebRequest/JSONResponse
struct JSONResponse_t8_157;
// System.Collections.IDictionary
struct IDictionary_t1_35;
// VoxelBusters.Utility.WebRequest
struct WebRequest_t8_158;
// VoxelBusters.Utility.DownloadTextAsset/Completion
struct Completion_t8_159;
// VoxelBusters.Utility.DownloadTextAsset
struct DownloadTextAsset_t8_160;
// VoxelBusters.Utility.DownloadTexture/Completion
struct Completion_t8_161;
// VoxelBusters.Utility.DownloadTexture
struct DownloadTexture_t8_162;
// VoxelBusters.Utility.GETRequest
struct GETRequest_t8_23;
// System.Text.StringBuilder
struct StringBuilder_t1_247;
// VoxelBusters.Utility.POSTRequest
struct POSTRequest_t8_163;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1_1742;
// Demo
struct Demo_t8_165;
// VoxelBusters.DebugPRO.Console
struct Console_t8_169;
// VoxelBusters.DebugPRO.Internal.NativeBinding
struct NativeBinding_t8_166;
// VoxelBusters.DebugPRO.Console/<GetConsoleTag>c__AnonStorey7
struct U3CGetConsoleTagU3Ec__AnonStorey7_t8_167;
// VoxelBusters.DebugPRO.Internal.ConsoleTag
struct ConsoleTag_t8_171;
// VoxelBusters.DebugPRO.Console/<GetIndexOfConsoleTag>c__AnonStorey8
struct U3CGetIndexOfConsoleTagU3Ec__AnonStorey8_t8_168;
// System.Collections.Generic.IEnumerable`1<VoxelBusters.DebugPRO.Internal.ConsoleTag>
struct IEnumerable_1_t1_1931;
// System.Func`2<VoxelBusters.DebugPRO.Internal.ConsoleTag,System.Boolean>
struct Func_2_t5_19;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t1_1897;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t5_18;
// UnityEngine.Object
struct Object_t6_5;
struct Object_t6_5_marshaled;
// System.Exception
struct Exception_t1_33;
// System.Diagnostics.StackTrace
struct StackTrace_t1_336;
// VoxelBusters.DebugPRO.UnityDebugUtility
struct UnityDebugUtility_t8_173;
// UnityEngine.Application/LogCallback
struct LogCallback_t6_83;
// VoxelBusters.NativePlugins.Demo.NPDemoBase
struct NPDemoBase_t8_174;
// System.String[]
struct StringU5BU5D_t1_238;
// VoxelBusters.NativePlugins.Demo.NPDisabledFeatureDemo
struct NPDisabledFeatureDemo_t8_175;
// VoxelBusters.NativePlugins.Demo.AddressBookDemo
struct AddressBookDemo_t8_176;
// VoxelBusters.NativePlugins.Demo.BillingDemo
struct BillingDemo_t8_178;
// VoxelBusters.NativePlugins.Demo.GameServicesDemo
struct GameServicesDemo_t8_179;
// VoxelBusters.NativePlugins.Demo.MediaLibraryDemo
struct MediaLibraryDemo_t8_181;
// VoxelBusters.NativePlugins.Demo.NetworkConnectivityDemo
struct NetworkConnectivityDemo_t8_182;
// VoxelBusters.NativePlugins.Demo.NotificationDemo
struct NotificationDemo_t8_183;
// VoxelBusters.NativePlugins.CrossPlatformNotification
struct CrossPlatformNotification_t8_259;
// VoxelBusters.NativePlugins.Demo.RemoteNotificationTest
struct RemoteNotificationTest_t8_184;
// VoxelBusters.NativePlugins.Demo.SharingDemo
struct SharingDemo_t8_185;
// VoxelBusters.NativePlugins.Demo.TwitterDemo
struct TwitterDemo_t8_187;
// VoxelBusters.NativePlugins.Demo.UIDemo
struct UIDemo_t8_188;
// VoxelBusters.NativePlugins.Demo.UtilityDemo
struct UtilityDemo_t8_189;
// VoxelBusters.NativePlugins.Demo.WebViewDemo
struct WebViewDemo_t8_190;
// VoxelBusters.NativePlugins.Demo.Utility
struct Utility_t8_192;
// VoxelBusters.NativePlugins.AddressBook/RequestAccessCompletion
struct RequestAccessCompletion_t8_193;
// VoxelBusters.NativePlugins.AddressBook/ReadContactsCompletion
struct ReadContactsCompletion_t8_194;
// VoxelBusters.NativePlugins.AddressBookContact[]
struct AddressBookContactU5BU5D_t8_177;
// VoxelBusters.NativePlugins.AddressBook/<ReadContacts>c__AnonStorey9
struct U3CReadContactsU3Ec__AnonStorey9_t8_195;
// VoxelBusters.NativePlugins.AddressBook
struct AddressBook_t8_196;
// VoxelBusters.NativePlugins.AddressBookContact/<GetImageAsync>c__AnonStoreyA
struct U3CGetImageAsyncU3Ec__AnonStoreyA_t8_197;
// VoxelBusters.NativePlugins.AddressBookContact
struct AddressBookContact_t8_198;
// VoxelBusters.NativePlugins.Internal.AndroidAddressBookContact
struct AndroidAddressBookContact_t8_199;
// System.Collections.IList
struct IList_t1_262;
// VoxelBusters.NativePlugins.Internal.EditorAddressBookContact
struct EditorAddressBookContact_t8_200;
// VoxelBusters.NativePlugins.Internal.iOSAddressBookContact
struct iOSAddressBookContact_t8_201;
// VoxelBusters.NativePlugins.Internal.AndroidBillingProduct
struct AndroidBillingProduct_t8_205;
// VoxelBusters.NativePlugins.BillingProduct
struct BillingProduct_t8_207;
// VoxelBusters.NativePlugins.PlatformID[]
struct PlatformIDU5BU5D_t8_329;
// VoxelBusters.NativePlugins.Internal.EditorBillingProduct
struct EditorBillingProduct_t8_208;
// VoxelBusters.NativePlugins.Internal.MutableBillingProduct
struct MutableBillingProduct_t8_206;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array.h"
#include "AssemblyU2DCSharp_ExifLibrary_GPSTimeStamp.h"
#include "AssemblyU2DCSharp_ExifLibrary_GPSTimeStampMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTag.h"
#include "mscorlib_System_Void.h"
#include "Assembly-CSharp_ArrayTypes.h"
#include "AssemblyU2DCSharp_ExifLibrary_MathEx_UFraction32.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifURationalArrayMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifURationalArray.h"
#include "mscorlib_System_String.h"
#include "AssemblyU2DCSharp_ExifLibrary_MathEx_UFraction32MethodDeclarations.h"
#include "mscorlib_System_StringMethodDeclarations.h"
#include "mscorlib_System_Single.h"
#include "mscorlib_System_Object.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifExtensionMethods.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifExtensionMethodsMethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Byte.h"
#include "mscorlib_System_IO_FileStream.h"
#include "mscorlib_System_Int32.h"
#include "mscorlib_System_IO_FileStreamMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifFile.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifFileMethodDeclarations.h"
#include "mscorlib_System_ObjectMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_14.h"
#include "AssemblyU2DCSharp_ExifLibrary_BitConverterEx_ByteOrder.h"
#include "AssemblyU2DCSharp_ExifLibrary_JPEGFile.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifProperty.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_14MethodDeclarations.h"
#include "mscorlib_System_Boolean.h"
#include "AssemblyU2DCSharp_ExifLibrary_JPEGFileMethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen_2MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_46MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_JPEGSectionMethodDeclarations.h"
#include "System_System_Collections_Generic_SortedList_2_genMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_BitConverterExMethodDeclarations.h"
#include "mscorlib_System_ArrayMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifPropertyFactoryMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifPropertyMethodDeclarations.h"
#include "mscorlib_System_IO_MemoryStreamMethodDeclarations.h"
#include "System_System_Collections_Generic_SortedList_2_gen.h"
#include "AssemblyU2DCSharp_ExifLibrary_BitConverterEx.h"
#include "AssemblyU2DCSharp_ExifLibrary_IFD.h"
#include "mscorlib_System_UInt16.h"
#include "mscorlib_System_Int16.h"
#include "mscorlib_System_UInt32.h"
#include "mscorlib_System_IO_MemoryStream.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_46.h"
#include "mscorlib_System_Predicate_1_gen_2.h"
#include "AssemblyU2DCSharp_ExifLibrary_JPEGSection.h"
#include "mscorlib_System_IntPtr.h"
#include "AssemblyU2DCSharp_ExifLibrary_JPEGMarker.h"
#include "mscorlib_System_Int64.h"
#include "mscorlib_System_IO_Stream.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__1MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_3MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifUIntMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_IFD0IsEmptyExceptionMethodDeclarations.h"
#include "mscorlib_System_Text_EncodingMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_3.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__1.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifUInt.h"
#include "AssemblyU2DCSharp_ExifLibrary_IFD0IsEmptyException.h"
#include "mscorlib_System_Text_Encoding.h"
#include "mscorlib_System_IO_SeekOrigin.h"
#include "mscorlib_System_IO_StreamMethodDeclarations.h"
#include "System_System_Collections_Generic_Queue_1_genMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_4MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_3MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifInterOperabilityMethodDeclarations.h"
#include "System_System_Collections_Generic_Queue_1_gen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_3.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifInterOperability.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_4.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTagFactoryMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifByte.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifByteMethodDeclarations.h"
#include "mscorlib_System_ByteMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifByteArray.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifByteArrayMethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilderMethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder.h"
#include "mscorlib_System_Char.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifAscii.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifAsciiMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifBitConverterMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifUShort.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifUShortMethodDeclarations.h"
#include "mscorlib_System_UInt16MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifUShortArray.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifUShortArrayMethodDeclarations.h"
#include "mscorlib_System_UInt32MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifUIntArray.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifUIntArrayMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifURational.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifURationalMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifUndefined.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifUndefinedMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifSInt.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifSIntMethodDeclarations.h"
#include "mscorlib_System_Int32MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifSIntArray.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifSIntArrayMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifSRational.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifSRationalMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_MathEx_Fraction32MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_MathEx_Fraction32.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifSRationalArray.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifSRationalArrayMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifPropertyFactory.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_genMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_0MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_1MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_2MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_3MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_4MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifDateTimeMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifVersionMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_5MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEncodedStringMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_6MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_7MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_8MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_9MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifCircularSubjectAreaMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifRectangularSubjectAreaMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifPointSubjectAreaMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_10MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_11MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_12MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_13MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_14MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_15MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_16MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_17MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_18MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_19MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_20MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_21MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_22MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_GPSLatitudeLongitudeMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_23MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_24MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_25MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_26MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_27MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_28MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_29MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_30MethodDeclarations.h"
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen.h"
#include "AssemblyU2DCSharp_ExifLibrary_Compression.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_0.h"
#include "AssemblyU2DCSharp_ExifLibrary_PhotometricInterpretation.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_1.h"
#include "AssemblyU2DCSharp_ExifLibrary_Orientation.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_2.h"
#include "AssemblyU2DCSharp_ExifLibrary_PlanarConfiguration.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_3.h"
#include "AssemblyU2DCSharp_ExifLibrary_YCbCrPositioning.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_4.h"
#include "AssemblyU2DCSharp_ExifLibrary_ResolutionUnit.h"
#include "mscorlib_System_DateTime.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifDateTime.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifVersion.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_5.h"
#include "AssemblyU2DCSharp_ExifLibrary_ColorSpace.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEncodedString.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_6.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExposureProgram.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_7.h"
#include "AssemblyU2DCSharp_ExifLibrary_MeteringMode.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_8.h"
#include "AssemblyU2DCSharp_ExifLibrary_LightSource.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_9.h"
#include "AssemblyU2DCSharp_ExifLibrary_Flash.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifCircularSubjectArea.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifRectangularSubjectArea.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifPointSubjectArea.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_10.h"
#include "AssemblyU2DCSharp_ExifLibrary_SensingMethod.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_11.h"
#include "AssemblyU2DCSharp_ExifLibrary_FileSource.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_12.h"
#include "AssemblyU2DCSharp_ExifLibrary_SceneType.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_13.h"
#include "AssemblyU2DCSharp_ExifLibrary_CustomRendered.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_14.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExposureMode.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_15.h"
#include "AssemblyU2DCSharp_ExifLibrary_WhiteBalance.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_16.h"
#include "AssemblyU2DCSharp_ExifLibrary_SceneCaptureType.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_17.h"
#include "AssemblyU2DCSharp_ExifLibrary_GainControl.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_18.h"
#include "AssemblyU2DCSharp_ExifLibrary_Contrast.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_19.h"
#include "AssemblyU2DCSharp_ExifLibrary_Saturation.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_20.h"
#include "AssemblyU2DCSharp_ExifLibrary_Sharpness.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_21.h"
#include "AssemblyU2DCSharp_ExifLibrary_SubjectDistanceRange.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_22.h"
#include "AssemblyU2DCSharp_ExifLibrary_GPSLatitudeRef.h"
#include "AssemblyU2DCSharp_ExifLibrary_GPSLatitudeLongitude.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_23.h"
#include "AssemblyU2DCSharp_ExifLibrary_GPSLongitudeRef.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_24.h"
#include "AssemblyU2DCSharp_ExifLibrary_GPSAltitudeRef.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_25.h"
#include "AssemblyU2DCSharp_ExifLibrary_GPSStatus.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_26.h"
#include "AssemblyU2DCSharp_ExifLibrary_GPSMeasureMode.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_27.h"
#include "AssemblyU2DCSharp_ExifLibrary_GPSSpeedRef.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_28.h"
#include "AssemblyU2DCSharp_ExifLibrary_GPSDirectionRef.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_29.h"
#include "AssemblyU2DCSharp_ExifLibrary_GPSDistanceRef.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_30.h"
#include "AssemblyU2DCSharp_ExifLibrary_GPSDifferential.h"
#include "mscorlib_System_ArgumentException.h"
#include "AssemblyU2DCSharp_ExifLibrary_IFDMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTagMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTagFactory.h"
#include "mscorlib_System_TypeMethodDeclarations.h"
#include "mscorlib_System_EnumMethodDeclarations.h"
#include "mscorlib_System_Type.h"
#include "mscorlib_System_RuntimeTypeHandle.h"
#include "AssemblyU2DCSharp_ExifLibrary_NotValidJPEGFileException.h"
#include "AssemblyU2DCSharp_ExifLibrary_NotValidJPEGFileExceptionMethodDeclarations.h"
#include "mscorlib_System_ExceptionMethodDeclarations.h"
#include "mscorlib_System_Exception.h"
#include "AssemblyU2DCSharp_ExifLibrary_SectionExceeds64KBException.h"
#include "AssemblyU2DCSharp_ExifLibrary_SectionExceeds64KBExceptionMethodDeclarations.h"
#include "mscorlib_System_IO_FileMode.h"
#include "mscorlib_System_IO_FileAccess.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_7MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_7.h"
#include "mscorlib_System_MathMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_JPEGMarkerMethodDeclarations.h"
#include "mscorlib_System_Double.h"
#include "mscorlib_System_DoubleMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_MathExMethodDeclarations.h"
#include "mscorlib_System_ArgumentNullExceptionMethodDeclarations.h"
#include "mscorlib_System_FormatExceptionMethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException.h"
#include "mscorlib_System_FormatException.h"
#include "AssemblyU2DCSharp_ExifLibrary_MathEx.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_GUIModalWindow.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_GUIModalWindowMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ScreenMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect.h"
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_GUIScrollView.h"
#include "UnityEngine_UnityEngine_GUISkin.h"
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object.h"
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObjectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component.h"
#include "UnityEngine_UnityEngine_GameObject.h"
#include "UnityEngine_UnityEngine_GUI_WindowFunctionMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUI_WindowFunction.h"
#include "UnityEngine_UnityEngine_GUISkinMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIStyleMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIStyle.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_GUIScrollViewMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector2.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_GUILayoutOption.h"
#include "UnityEngine_UnityEngine_GUILayoutMethodDeclarations.h"
#include "UnityEngine_UnityEngine_EventMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayoutUtilityMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Event.h"
#include "UnityEngine_UnityEngine_EventType.h"
#include "UnityEngine_UnityEngine_InputMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TouchMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Touch.h"
#include "UnityEngine_UnityEngine_TouchPhase.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_GUIExtensions.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_GUIExtensionsMethodDeclarations.h"
#include "mscorlib_System_Collections_ArrayList.h"
#include "mscorlib_System_Action_1_gen_6.h"
#include "mscorlib_System_Collections_ArrayListMethodDeclarations.h"
#include "mscorlib_System_Action_1_gen_6MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_UnityGUILayoutUtility.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_UnityGUILayoutUtilityMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIContent.h"
#include "UnityEngine_UnityEngine_GUIContentMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_UnityGUI_MENU_GUIMain.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_UnityGUI_MENU_GUIMainMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_UnityGUI_MENU_GUIMenuMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_UnityGUI_MENU_GUIMenu.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_UnityGUI_MENU_GUISubMMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_UnityGUI_MENU_GUISubM.h"
#include "UnityEngine_UnityEngine_TextAnchor.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_EditorCoroutine.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_EditorCoroutineMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_EnumMaskFieldAttribut.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_EnumMaskFieldAttributMethodDeclarations.h"
#include "UnityEngine_UnityEngine_PropertyAttributeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_PropertyAttribute.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_InspectorButtonAttrib.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_InspectorButtonAttribMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_InspectorButtonAttrib_0.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_InspectorButtonAttrib_0MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_ExecuteOnValueChangeA.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_ExecuteOnValueChangeAMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_RegexAttribute.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_RegexAttributeMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_EditorInvoke.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_EditorInvokeMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_15MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_15.h"
#include "System_Core_System_Action.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_16MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_16.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_EditorScheduler_Callb.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_EditorScheduler_CallbMethodDeclarations.h"
#include "mscorlib_System_AsyncCallback.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_EditorScheduler.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_EditorSchedulerMethodDeclarations.h"
#include "mscorlib_System_DelegateMethodDeclarations.h"
#include "mscorlib_System_Delegate.h"
#include "mscorlib_System_MulticastDelegate.h"
#include "mscorlib_System_MulticastDelegateMethodDeclarations.h"
#include "mscorlib_System_Reflection_MethodInfo.h"
#include "mscorlib_System_Reflection_MethodBase.h"
#include "mscorlib_System_Reflection_MethodBaseMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_DownloadTextureDemo.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_DownloadTextureDemoMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_URLMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_DownloadTextureMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_DownloadTexture_ComplMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_RequestMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_URL.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_DownloadTexture.h"
#include "UnityEngine_UnityEngine_Texture2D.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_DownloadTexture_Compl.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_Request.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_StringExtensionsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RendererMethodDeclarations.h"
#include "UnityEngine_UnityEngine_MaterialMethodDeclarations.h"
#include "UnityEngine_UnityEngine_MeshRenderer.h"
#include "UnityEngine_UnityEngine_Renderer.h"
#include "UnityEngine_UnityEngine_Material.h"
#include "UnityEngine_UnityEngine_Texture.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_Request_U3CStartAsync.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_Request_U3CStartAsyncMethodDeclarations.h"
#include "UnityEngine_UnityEngine_WWWMethodDeclarations.h"
#include "UnityEngine_UnityEngine_WWW.h"
#include "mscorlib_System_NotSupportedExceptionMethodDeclarations.h"
#include "mscorlib_System_NotSupportedException.h"
#include "UnityEngine_UnityEngine_HideFlags.h"
#include "UnityEngine_UnityEngine_Coroutine.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_WebRequest_JSONRespon.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_WebRequest_JSONResponMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_WebRequest.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_WebRequestMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_1MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_1.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_JSONUtilityMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_DownloadTextAsset_Com.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_DownloadTextAsset_ComMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_DownloadTextAsset.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_DownloadTextAssetMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_TextureExtensionsMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_GETRequest.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_GETRequestMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_POSTRequest.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_POSTRequestMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_JSONParserExtensionsMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_WebResponse.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_WebResponseMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_IDictionaryExtensionsMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_IDictionaryExtensions.h"
#include "mscorlib_System_Collections_Generic_List_1_genMethodDeclarations.h"
#include "AssemblyU2DCSharp_Demo.h"
#include "AssemblyU2DCSharp_DemoMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_DebugPRO_Console.h"
#include "AssemblyU2DCSharp_VoxelBusters_DebugPRO_ConsoleMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_DebugPRO_Internal_NativeBindi.h"
#include "AssemblyU2DCSharp_VoxelBusters_DebugPRO_Internal_NativeBindiMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_DebugPRO_Internal_eConsoleLog.h"
#include "AssemblyU2DCSharp_VoxelBusters_DebugPRO_Internal_ConsoleLog.h"
#include "AssemblyU2DCSharp_VoxelBusters_DebugPRO_Internal_ConsoleLogMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_DebugPRO_Console_U3CGetConsol.h"
#include "AssemblyU2DCSharp_VoxelBusters_DebugPRO_Console_U3CGetConsolMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_DebugPRO_Internal_ConsoleTag.h"
#include "AssemblyU2DCSharp_VoxelBusters_DebugPRO_Internal_ConsoleTagMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_DebugPRO_Console_U3CGetIndexO.h"
#include "AssemblyU2DCSharp_VoxelBusters_DebugPRO_Console_U3CGetIndexOMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_47MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_48MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ScriptableObjectMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_47.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_48.h"
#include "UnityEngine_UnityEngine_ScriptableObject.h"
#include "UnityEngine_UnityEngine_Application_LogCallbackMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_DebugPRO_UnityDebugUtilityMethodDeclarations.h"
#include "UnityEngine_UnityEngine_LogType.h"
#include "UnityEngine_UnityEngine_Application_LogCallback.h"
#include "System_Core_System_Func_2_gen_1MethodDeclarations.h"
#include "System_Core_System_Linq_EnumerableMethodDeclarations.h"
#include "System_Core_System_Func_2_gen_1.h"
#include "System_Core_System_Linq_Enumerable.h"
#include "mscorlib_System_Predicate_1_gen_3MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen_3.h"
#include "UnityEngine_UnityEngine_Vector3.h"
#include "UnityEngine_UnityEngine_Color.h"
#include "mscorlib_System_Diagnostics_StackTraceMethodDeclarations.h"
#include "mscorlib_System_Diagnostics_StackTrace.h"
#include "mscorlib_System_Diagnostics_StackFrame.h"
#include "mscorlib_System_Diagnostics_StackFrameMethodDeclarations.h"
#include "mscorlib_System_Reflection_MemberInfo.h"
#include "mscorlib_System_Reflection_MemberInfoMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ApplicationMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_DebugPRO_Internal_eConsoleLogMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_DebugPRO_UnityDebugUtility.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Demo_NPDemoBase.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Demo_NPDemoBaseMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_AssetStoreProductUtility_Demo_1MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_AssetStoreProductUtility_Demo_1.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Demo_NPDisabled.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Demo_NPDisabledMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Demo_AddressBoo.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Demo_AddressBooMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Demo_BillingDem.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Demo_BillingDemMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Demo_GameServic.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Demo_GameServicMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Demo_MediaLibra.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Demo_MediaLibraMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Demo_NetworkCon.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Demo_NetworkConMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Demo_Notificati.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Demo_NotificatiMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_NotificationSerMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_NotificationSer_2MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_NotificationSer_0MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_NotificationSer.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_CrossPlatformNo_1.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_NotificationSer_0.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_NotificationTyp.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eNotificationRe.h"
#include "AssemblyU2DCSharp_NPBindingMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_NotificationSer_2.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_CrossPlatformNo_1MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_CrossPlatformNo_0MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_CrossPlatformNoMethodDeclarations.h"
#include "mscorlib_System_DateTimeMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_CrossPlatformNo_0.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_CrossPlatformNo.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Demo_RemoteNoti.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Demo_RemoteNotiMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Demo_SharingDem.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Demo_SharingDemMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eShareOptions.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Demo_TwitterDem.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Demo_TwitterDemMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Demo_UIDemo.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Demo_UIDemoMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_UI_AlertDialogCMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_UIMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_UI.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_UI_AlertDialogC.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_UI_SingleFieldPMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_UI_SingleFieldP.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_UI_LoginPromptCMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_UI_LoginPromptC.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eToastMessageLe.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Demo_UtilityDem.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Demo_UtilityDemMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_NPSettingsMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_ApplicationSett_2MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_UtilityMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_ApplicationSett_2.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Utility.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_UtilitySettingsMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_RateMyApp_SettiMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_RateMyAppMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_UtilitySettings.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_RateMyApp_Setti.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_RateMyApp.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Demo_WebViewDem.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Demo_WebViewDemMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Demo_Utility.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Demo_UtilityMethodDeclarations.h"
#include "mscorlib_System_IO_PathMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_AddressBook_Req.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_AddressBook_ReqMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eABAuthorizatio.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_AddressBook_Rea.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_AddressBook_ReaMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_AddressBookCont_0.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_AddressBook_U3C.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_AddressBook_U3CMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_AddressBook.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_AddressBookMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_AddressBookCont.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_AddressBookContMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_AddressBookCont_0MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ResourcesMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_Androi.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_AndroiMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_Editor.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_EditorMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_iOSAdd.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_iOSAddMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eABAuthorizatioMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eBillingTransac.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eBillingTransacMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eBillingTransac_0.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eBillingTransac_0MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_Androi_0.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_Androi_0MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_MutablMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_BillingProductMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_Mutabl.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_BillingProduct.h"
#include "mscorlib_System_SingleMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_PlatformID.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_PlatformIDMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_PlatformID_ePla.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_Editor_0.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_Editor_0MethodDeclarations.h"

// System.Int32 System.Array::IndexOf<System.Byte>(!!0[],!!0)
extern "C" int32_t Array_IndexOf_TisByte_t1_11_m1_15039_gshared (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* p0, uint8_t p1, const MethodInfo* method);
#define Array_IndexOf_TisByte_t1_11_m1_15039(__this /* static, unused */, p0, p1, method) (( int32_t (*) (Object_t * /* static, unused */, ByteU5BU5D_t1_109*, uint8_t, const MethodInfo*))Array_IndexOf_TisByte_t1_11_m1_15039_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0)
extern "C" Object_t * Object_Instantiate_TisObject_t_m6_1911_gshared (Object_t * __this /* static, unused */, Object_t * p0, const MethodInfo* method);
#define Object_Instantiate_TisObject_t_m6_1911(__this /* static, unused */, p0, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Object_Instantiate_TisObject_t_m6_1911_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GUISkin>(!!0)
#define Object_Instantiate_TisGUISkin_t6_169_m6_1920(__this /* static, unused */, p0, method) (( GUISkin_t6_169 * (*) (Object_t * /* static, unused */, GUISkin_t6_169 *, const MethodInfo*))Object_Instantiate_TisObject_t_m6_1911_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C" Object_t * GameObject_AddComponent_TisObject_t_m6_1906_gshared (GameObject_t6_97 * __this, const MethodInfo* method);
#define GameObject_AddComponent_TisObject_t_m6_1906(__this, method) (( Object_t * (*) (GameObject_t6_97 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m6_1906_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<VoxelBusters.Utility.GUIScrollView>()
#define GameObject_AddComponent_TisGUIScrollView_t8_19_m6_1918(__this, method) (( GUIScrollView_t8_19 * (*) (GameObject_t6_97 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m6_1906_gshared)(__this, method)
// !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>()
extern "C" ObjectU5BU5D_t1_272* Component_GetComponentsInChildren_TisObject_t_m6_1925_gshared (Component_t6_26 * __this, const MethodInfo* method);
#define Component_GetComponentsInChildren_TisObject_t_m6_1925(__this, method) (( ObjectU5BU5D_t1_272* (*) (Component_t6_26 *, const MethodInfo*))Component_GetComponentsInChildren_TisObject_t_m6_1925_gshared)(__this, method)
// !!0[] UnityEngine.Component::GetComponentsInChildren<VoxelBusters.Utility.UnityGUI.MENU.GUISubMenu>()
#define Component_GetComponentsInChildren_TisGUISubMenu_t8_143_m6_1921(__this, method) (( GUISubMenuU5BU5D_t8_142* (*) (Component_t6_26 *, const MethodInfo*))Component_GetComponentsInChildren_TisObject_t_m6_1925_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.MonoBehaviour>()
#define GameObject_AddComponent_TisMonoBehaviour_t6_91_m6_1922(__this, method) (( MonoBehaviour_t6_91 * (*) (GameObject_t6_97 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m6_1906_gshared)(__this, method)
// !!0 VoxelBusters.Utility.IDictionaryExtensions::GetIfAvailable<System.Int32>(System.Collections.IDictionary,System.String)
extern "C" int32_t IDictionaryExtensions_GetIfAvailable_TisInt32_t1_3_m8_2007_gshared (Object_t * __this /* static, unused */, Object_t * p0, String_t* p1, const MethodInfo* method);
#define IDictionaryExtensions_GetIfAvailable_TisInt32_t1_3_m8_2007(__this /* static, unused */, p0, p1, method) (( int32_t (*) (Object_t * /* static, unused */, Object_t *, String_t*, const MethodInfo*))IDictionaryExtensions_GetIfAvailable_TisInt32_t1_3_m8_2007_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 VoxelBusters.Utility.IDictionaryExtensions::GetIfAvailable<System.Object>(System.Collections.IDictionary,System.String)
extern "C" Object_t * IDictionaryExtensions_GetIfAvailable_TisObject_t_m8_1973_gshared (Object_t * __this /* static, unused */, Object_t * p0, String_t* p1, const MethodInfo* method);
#define IDictionaryExtensions_GetIfAvailable_TisObject_t_m8_1973(__this /* static, unused */, p0, p1, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, String_t*, const MethodInfo*))IDictionaryExtensions_GetIfAvailable_TisObject_t_m8_1973_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 VoxelBusters.Utility.IDictionaryExtensions::GetIfAvailable<System.String>(System.Collections.IDictionary,System.String)
#define IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(__this /* static, unused */, p0, p1, method) (( String_t* (*) (Object_t * /* static, unused */, Object_t *, String_t*, const MethodInfo*))IDictionaryExtensions_GetIfAvailable_TisObject_t_m8_1973_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 VoxelBusters.Utility.IDictionaryExtensions::GetIfAvailable<System.Collections.Generic.List`1<System.String>>(System.Collections.IDictionary,System.String)
#define IDictionaryExtensions_GetIfAvailable_TisList_1_t1_1742_m8_2008(__this /* static, unused */, p0, p1, method) (( List_1_t1_1742 * (*) (Object_t * /* static, unused */, Object_t *, String_t*, const MethodInfo*))IDictionaryExtensions_GetIfAvailable_TisObject_t_m8_1973_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 UnityEngine.ScriptableObject::CreateInstance<System.Object>()
extern "C" Object_t * ScriptableObject_CreateInstance_TisObject_t_m6_1926_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define ScriptableObject_CreateInstance_TisObject_t_m6_1926(__this /* static, unused */, method) (( Object_t * (*) (Object_t * /* static, unused */, const MethodInfo*))ScriptableObject_CreateInstance_TisObject_t_m6_1926_gshared)(__this /* static, unused */, method)
// !!0 UnityEngine.ScriptableObject::CreateInstance<VoxelBusters.DebugPRO.Console>()
#define ScriptableObject_CreateInstance_TisConsole_t8_169_m6_1923(__this /* static, unused */, method) (( Console_t8_169 * (*) (Object_t * /* static, unused */, const MethodInfo*))ScriptableObject_CreateInstance_TisObject_t_m6_1926_gshared)(__this /* static, unused */, method)
// !!0 System.Linq.Enumerable::FirstOrDefault<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
extern "C" Object_t * Enumerable_FirstOrDefault_TisObject_t_m5_61_gshared (Object_t * __this /* static, unused */, Object_t* p0, Func_2_t5_18 * p1, const MethodInfo* method);
#define Enumerable_FirstOrDefault_TisObject_t_m5_61(__this /* static, unused */, p0, p1, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t*, Func_2_t5_18 *, const MethodInfo*))Enumerable_FirstOrDefault_TisObject_t_m5_61_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 System.Linq.Enumerable::FirstOrDefault<VoxelBusters.DebugPRO.Internal.ConsoleTag>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
#define Enumerable_FirstOrDefault_TisConsoleTag_t8_171_m5_60(__this /* static, unused */, p0, p1, method) (( ConsoleTag_t8_171 * (*) (Object_t * /* static, unused */, Object_t*, Func_2_t5_19 *, const MethodInfo*))Enumerable_FirstOrDefault_TisObject_t_m5_61_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 UnityEngine.ScriptableObject::CreateInstance<VoxelBusters.DebugPRO.UnityDebugUtility>()
#define ScriptableObject_CreateInstance_TisUnityDebugUtility_t8_173_m6_1924(__this /* static, unused */, method) (( UnityDebugUtility_t8_173 * (*) (Object_t * /* static, unused */, const MethodInfo*))ScriptableObject_CreateInstance_TisObject_t_m6_1926_gshared)(__this /* static, unused */, method)
// !!0 VoxelBusters.Utility.IDictionaryExtensions::GetIfAvailable<System.Collections.IList>(System.Collections.IDictionary,System.String)
#define IDictionaryExtensions_GetIfAvailable_TisIList_t1_262_m8_2009(__this /* static, unused */, p0, p1, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, String_t*, const MethodInfo*))IDictionaryExtensions_GetIfAvailable_TisObject_t_m8_1973_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 VoxelBusters.Utility.IDictionaryExtensions::GetIfAvailable<System.Int64>(System.Collections.IDictionary,System.String)
extern "C" int64_t IDictionaryExtensions_GetIfAvailable_TisInt64_t1_7_m8_2010_gshared (Object_t * __this /* static, unused */, Object_t * p0, String_t* p1, const MethodInfo* method);
#define IDictionaryExtensions_GetIfAvailable_TisInt64_t1_7_m8_2010(__this /* static, unused */, p0, p1, method) (( int64_t (*) (Object_t * /* static, unused */, Object_t *, String_t*, const MethodInfo*))IDictionaryExtensions_GetIfAvailable_TisInt64_t1_7_m8_2010_gshared)(__this /* static, unused */, p0, p1, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ExifLibrary.GPSTimeStamp::.ctor(ExifLibrary.ExifTag,ExifLibrary.MathEx/UFraction32[])
extern "C" void GPSTimeStamp__ctor_m8_466 (GPSTimeStamp_t8_108 * __this, int32_t ___tag, UFraction32U5BU5D_t8_122* ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		UFraction32U5BU5D_t8_122* L_1 = ___value;
		ExifURationalArray__ctor_m8_576(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// ExifLibrary.MathEx/UFraction32[] ExifLibrary.GPSTimeStamp::get_Value()
extern "C" UFraction32U5BU5D_t8_122* GPSTimeStamp_get_Value_m8_467 (GPSTimeStamp_t8_108 * __this, const MethodInfo* method)
{
	{
		UFraction32U5BU5D_t8_122* L_0 = (((ExifURationalArray_t8_107 *)__this)->___mValue_3);
		return L_0;
	}
}
// System.Void ExifLibrary.GPSTimeStamp::set_Value(ExifLibrary.MathEx/UFraction32[])
extern "C" void GPSTimeStamp_set_Value_m8_468 (GPSTimeStamp_t8_108 * __this, UFraction32U5BU5D_t8_122* ___value, const MethodInfo* method)
{
	{
		UFraction32U5BU5D_t8_122* L_0 = ___value;
		((ExifURationalArray_t8_107 *)__this)->___mValue_3 = L_0;
		return;
	}
}
// ExifLibrary.MathEx/UFraction32 ExifLibrary.GPSTimeStamp::get_Hour()
extern "C" UFraction32_t8_121  GPSTimeStamp_get_Hour_m8_469 (GPSTimeStamp_t8_108 * __this, const MethodInfo* method)
{
	{
		UFraction32U5BU5D_t8_122* L_0 = (((ExifURationalArray_t8_107 *)__this)->___mValue_3);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		return (*(UFraction32_t8_121 *)((UFraction32_t8_121 *)(UFraction32_t8_121 *)SZArrayLdElema(L_0, 0, sizeof(UFraction32_t8_121 ))));
	}
}
// System.Void ExifLibrary.GPSTimeStamp::set_Hour(ExifLibrary.MathEx/UFraction32)
extern "C" void GPSTimeStamp_set_Hour_m8_470 (GPSTimeStamp_t8_108 * __this, UFraction32_t8_121  ___value, const MethodInfo* method)
{
	{
		UFraction32U5BU5D_t8_122* L_0 = (((ExifURationalArray_t8_107 *)__this)->___mValue_3);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		UFraction32_t8_121  L_1 = ___value;
		(*(UFraction32_t8_121 *)((UFraction32_t8_121 *)(UFraction32_t8_121 *)SZArrayLdElema(L_0, 0, sizeof(UFraction32_t8_121 )))) = L_1;
		return;
	}
}
// ExifLibrary.MathEx/UFraction32 ExifLibrary.GPSTimeStamp::get_Minute()
extern "C" UFraction32_t8_121  GPSTimeStamp_get_Minute_m8_471 (GPSTimeStamp_t8_108 * __this, const MethodInfo* method)
{
	{
		UFraction32U5BU5D_t8_122* L_0 = (((ExifURationalArray_t8_107 *)__this)->___mValue_3);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 1);
		return (*(UFraction32_t8_121 *)((UFraction32_t8_121 *)(UFraction32_t8_121 *)SZArrayLdElema(L_0, 1, sizeof(UFraction32_t8_121 ))));
	}
}
// System.Void ExifLibrary.GPSTimeStamp::set_Minute(ExifLibrary.MathEx/UFraction32)
extern "C" void GPSTimeStamp_set_Minute_m8_472 (GPSTimeStamp_t8_108 * __this, UFraction32_t8_121  ___value, const MethodInfo* method)
{
	{
		UFraction32U5BU5D_t8_122* L_0 = (((ExifURationalArray_t8_107 *)__this)->___mValue_3);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 1);
		UFraction32_t8_121  L_1 = ___value;
		(*(UFraction32_t8_121 *)((UFraction32_t8_121 *)(UFraction32_t8_121 *)SZArrayLdElema(L_0, 1, sizeof(UFraction32_t8_121 )))) = L_1;
		return;
	}
}
// ExifLibrary.MathEx/UFraction32 ExifLibrary.GPSTimeStamp::get_Second()
extern "C" UFraction32_t8_121  GPSTimeStamp_get_Second_m8_473 (GPSTimeStamp_t8_108 * __this, const MethodInfo* method)
{
	{
		UFraction32U5BU5D_t8_122* L_0 = (((ExifURationalArray_t8_107 *)__this)->___mValue_3);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 2);
		return (*(UFraction32_t8_121 *)((UFraction32_t8_121 *)(UFraction32_t8_121 *)SZArrayLdElema(L_0, 2, sizeof(UFraction32_t8_121 ))));
	}
}
// System.Void ExifLibrary.GPSTimeStamp::set_Second(ExifLibrary.MathEx/UFraction32)
extern "C" void GPSTimeStamp_set_Second_m8_474 (GPSTimeStamp_t8_108 * __this, UFraction32_t8_121  ___value, const MethodInfo* method)
{
	{
		UFraction32U5BU5D_t8_122* L_0 = (((ExifURationalArray_t8_107 *)__this)->___mValue_3);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 2);
		UFraction32_t8_121  L_1 = ___value;
		(*(UFraction32_t8_121 *)((UFraction32_t8_121 *)(UFraction32_t8_121 *)SZArrayLdElema(L_0, 2, sizeof(UFraction32_t8_121 )))) = L_1;
		return;
	}
}
// System.String ExifLibrary.GPSTimeStamp::ToString()
extern TypeInfo* UFraction32_t8_121_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t1_17_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5221;
extern "C" String_t* GPSTimeStamp_ToString_m8_475 (GPSTimeStamp_t8_108 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UFraction32_t8_121_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1972);
		Single_t1_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5221 = il2cpp_codegen_string_literal_from_index(5221);
		s_Il2CppMethodIntialized = true;
	}
	{
		UFraction32_t8_121  L_0 = GPSTimeStamp_get_Hour_m8_469(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UFraction32_t8_121_il2cpp_TypeInfo_var);
		float L_1 = UFraction32_op_Explicit_m8_783(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		float L_2 = L_1;
		Object_t * L_3 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_2);
		UFraction32_t8_121  L_4 = GPSTimeStamp_get_Minute_m8_471(__this, /*hidden argument*/NULL);
		float L_5 = UFraction32_op_Explicit_m8_783(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		float L_6 = L_5;
		Object_t * L_7 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_6);
		UFraction32_t8_121  L_8 = GPSTimeStamp_get_Second_m8_473(__this, /*hidden argument*/NULL);
		float L_9 = UFraction32_op_Explicit_m8_783(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		float L_10 = L_9;
		Object_t * L_11 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_10);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Format_m1_550(NULL /*static, unused*/, _stringLiteral5221, L_3, L_7, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Byte[] ExifLibrary.ExifExtensionMethods::ReadBytes(System.IO.FileStream,System.Int32)
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern "C" ByteU5BU5D_t1_109* ExifExtensionMethods_ReadBytes_m8_476 (Object_t * __this /* static, unused */, FileStream_t1_146 * ___stream, int32_t ___count, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		int32_t L_0 = ___count;
		V_0 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, L_0));
		V_1 = 0;
		int32_t L_1 = ___count;
		V_2 = L_1;
		goto IL_002b;
	}

IL_0010:
	{
		FileStream_t1_146 * L_2 = ___stream;
		ByteU5BU5D_t1_109* L_3 = V_0;
		int32_t L_4 = V_1;
		int32_t L_5 = V_2;
		NullCheck(L_2);
		int32_t L_6 = (int32_t)VirtFuncInvoker3< int32_t, ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(20 /* System.Int32 System.IO.FileStream::Read(System.Byte[],System.Int32,System.Int32) */, L_2, L_3, L_4, L_5);
		V_3 = L_6;
		int32_t L_7 = V_3;
		if ((((int32_t)L_7) > ((int32_t)0)))
		{
			goto IL_0023;
		}
	}
	{
		return (ByteU5BU5D_t1_109*)NULL;
	}

IL_0023:
	{
		int32_t L_8 = V_2;
		int32_t L_9 = V_3;
		V_2 = ((int32_t)((int32_t)L_8-(int32_t)L_9));
		int32_t L_10 = V_1;
		int32_t L_11 = V_3;
		V_1 = ((int32_t)((int32_t)L_10+(int32_t)L_11));
	}

IL_002b:
	{
		int32_t L_12 = V_2;
		if ((((int32_t)L_12) > ((int32_t)0)))
		{
			goto IL_0010;
		}
	}
	{
		ByteU5BU5D_t1_109* L_13 = V_0;
		return L_13;
	}
}
// System.Void ExifLibrary.ExifExtensionMethods::WriteBytes(System.IO.FileStream,System.Byte[])
extern "C" void ExifExtensionMethods_WriteBytes_m8_477 (Object_t * __this /* static, unused */, FileStream_t1_146 * ___stream, ByteU5BU5D_t1_109* ___buffer, const MethodInfo* method)
{
	{
		FileStream_t1_146 * L_0 = ___stream;
		ByteU5BU5D_t1_109* L_1 = ___buffer;
		ByteU5BU5D_t1_109* L_2 = ___buffer;
		NullCheck(L_2);
		NullCheck(L_0);
		VirtActionInvoker3< ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(24 /* System.Void System.IO.FileStream::Write(System.Byte[],System.Int32,System.Int32) */, L_0, L_1, 0, (((int32_t)((int32_t)(((Array_t *)L_2)->max_length)))));
		return;
	}
}
// System.Void ExifLibrary.ExifFile::.ctor()
extern "C" void ExifFile__ctor_m8_478 (ExifFile_t8_110 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,ExifLibrary.ExifProperty> ExifLibrary.ExifFile::get_Properties()
extern "C" Dictionary_2_t1_1904 * ExifFile_get_Properties_m8_479 (ExifFile_t8_110 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1_1904 * L_0 = (__this->___U3CPropertiesU3Ek__BackingField_12);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifFile::set_Properties(System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>)
extern "C" void ExifFile_set_Properties_m8_480 (ExifFile_t8_110 * __this, Dictionary_2_t1_1904 * ___value, const MethodInfo* method)
{
	{
		Dictionary_2_t1_1904 * L_0 = ___value;
		__this->___U3CPropertiesU3Ek__BackingField_12 = L_0;
		return;
	}
}
// ExifLibrary.BitConverterEx/ByteOrder ExifLibrary.ExifFile::get_ByteOrder()
extern "C" int32_t ExifFile_get_ByteOrder_m8_481 (ExifFile_t8_110 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___U3CByteOrderU3Ek__BackingField_13);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifFile::set_ByteOrder(ExifLibrary.BitConverterEx/ByteOrder)
extern "C" void ExifFile_set_ByteOrder_m8_482 (ExifFile_t8_110 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___U3CByteOrderU3Ek__BackingField_13 = L_0;
		return;
	}
}
// ExifLibrary.JPEGFile ExifLibrary.ExifFile::get_Thumbnail()
extern "C" JPEGFile_t8_111 * ExifFile_get_Thumbnail_m8_483 (ExifFile_t8_110 * __this, const MethodInfo* method)
{
	{
		JPEGFile_t8_111 * L_0 = (__this->___U3CThumbnailU3Ek__BackingField_14);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifFile::set_Thumbnail(ExifLibrary.JPEGFile)
extern "C" void ExifFile_set_Thumbnail_m8_484 (ExifFile_t8_110 * __this, JPEGFile_t8_111 * ___value, const MethodInfo* method)
{
	{
		JPEGFile_t8_111 * L_0 = ___value;
		__this->___U3CThumbnailU3Ek__BackingField_14 = L_0;
		return;
	}
}
// ExifLibrary.ExifProperty ExifLibrary.ExifFile::get_Item(ExifLibrary.ExifTag)
extern "C" ExifProperty_t8_99 * ExifFile_get_Item_m8_485 (ExifFile_t8_110 * __this, int32_t ___key, const MethodInfo* method)
{
	{
		Dictionary_2_t1_1904 * L_0 = ExifFile_get_Properties_m8_479(__this, /*hidden argument*/NULL);
		int32_t L_1 = ___key;
		NullCheck(L_0);
		ExifProperty_t8_99 * L_2 = (ExifProperty_t8_99 *)VirtFuncInvoker1< ExifProperty_t8_99 *, int32_t >::Invoke(22 /* !1 System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::get_Item(!0) */, L_0, L_1);
		return L_2;
	}
}
// System.Void ExifLibrary.ExifFile::set_Item(ExifLibrary.ExifTag,ExifLibrary.ExifProperty)
extern "C" void ExifFile_set_Item_m8_486 (ExifFile_t8_110 * __this, int32_t ___key, ExifProperty_t8_99 * ___value, const MethodInfo* method)
{
	{
		Dictionary_2_t1_1904 * L_0 = ExifFile_get_Properties_m8_479(__this, /*hidden argument*/NULL);
		int32_t L_1 = ___key;
		ExifProperty_t8_99 * L_2 = ___value;
		NullCheck(L_0);
		VirtActionInvoker2< int32_t, ExifProperty_t8_99 * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::set_Item(!0,!1) */, L_0, L_1, L_2);
		return;
	}
}
// System.Void ExifLibrary.ExifFile::Save(System.String)
extern "C" void ExifFile_Save_m8_487 (ExifFile_t8_110 * __this, String_t* ___filename, const MethodInfo* method)
{
	{
		String_t* L_0 = ___filename;
		ExifFile_Save_m8_488(__this, L_0, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ExifLibrary.ExifFile::Save(System.String,System.Boolean)
extern "C" void ExifFile_Save_m8_488 (ExifFile_t8_110 * __this, String_t* ___filename, bool ___preserveMakerNote, const MethodInfo* method)
{
	{
		bool L_0 = ___preserveMakerNote;
		ExifFile_WriteApp1_m8_490(__this, L_0, /*hidden argument*/NULL);
		JPEGFile_t8_111 * L_1 = (__this->___file_0);
		String_t* L_2 = ___filename;
		NullCheck(L_1);
		JPEGFile_Save_m8_647(L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ExifLibrary.ExifFile::ReadAPP1()
extern TypeInfo* ExifFile_t8_110_il2cpp_TypeInfo_var;
extern TypeInfo* Predicate_1_t1_1905_il2cpp_TypeInfo_var;
extern TypeInfo* JPEGSection_t8_112_il2cpp_TypeInfo_var;
extern TypeInfo* SortedList_2_t3_248_il2cpp_TypeInfo_var;
extern TypeInfo* BitConverterEx_t8_61_il2cpp_TypeInfo_var;
extern TypeInfo* IList_1_t1_1922_il2cpp_TypeInfo_var;
extern TypeInfo* IList_1_t1_1923_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern TypeInfo* MemoryStream_t1_433_il2cpp_TypeInfo_var;
extern TypeInfo* JPEGFile_t8_111_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern const MethodInfo* ExifFile_U3CReadAPP1U3Em__2_m8_494_MethodInfo_var;
extern const MethodInfo* Predicate_1__ctor_m1_15040_MethodInfo_var;
extern const MethodInfo* List_1_Find_m1_15041_MethodInfo_var;
extern const MethodInfo* ExifFile_U3CReadAPP1U3Em__3_m8_495_MethodInfo_var;
extern const MethodInfo* List_1_FindLastIndex_m1_15042_MethodInfo_var;
extern const MethodInfo* SortedList_2__ctor_m3_1809_MethodInfo_var;
extern const MethodInfo* SortedList_2_get_Keys_m3_1810_MethodInfo_var;
extern const MethodInfo* SortedList_2_get_Values_m3_1811_MethodInfo_var;
extern const MethodInfo* SortedList_2_RemoveAt_m3_1812_MethodInfo_var;
extern "C" void ExifFile_ReadAPP1_m8_489 (ExifFile_t8_110 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExifFile_t8_110_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1977);
		Predicate_1_t1_1905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1978);
		JPEGSection_t8_112_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1975);
		SortedList_2_t3_248_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1979);
		BitConverterEx_t8_61_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1970);
		IList_1_t1_1922_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1980);
		IList_1_t1_1923_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1981);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		MemoryStream_t1_433_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(390);
		JPEGFile_t8_111_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1982);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		ExifFile_U3CReadAPP1U3Em__2_m8_494_MethodInfo_var = il2cpp_codegen_method_info_from_index(481);
		Predicate_1__ctor_m1_15040_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484130);
		List_1_Find_m1_15041_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484131);
		ExifFile_U3CReadAPP1U3Em__3_m8_495_MethodInfo_var = il2cpp_codegen_method_info_from_index(484);
		List_1_FindLastIndex_m1_15042_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484133);
		SortedList_2__ctor_m3_1809_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484134);
		SortedList_2_get_Keys_m3_1810_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484135);
		SortedList_2_get_Values_m3_1811_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484136);
		SortedList_2_RemoveAt_m3_1812_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484137);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	ByteU5BU5D_t1_109* V_1 = {0};
	SortedList_2_t3_248 * V_2 = {0};
	int32_t V_3 = 0;
	BitConverterEx_t8_61 * V_4 = {0};
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	int32_t V_10 = {0};
	uint16_t V_11 = 0;
	int16_t V_12 = 0;
	int32_t V_13 = 0;
	uint16_t V_14 = 0;
	uint16_t V_15 = 0;
	uint32_t V_16 = 0;
	ByteU5BU5D_t1_109* V_17 = {0};
	int32_t V_18 = 0;
	int32_t V_19 = 0;
	int32_t V_20 = 0;
	uint32_t V_21 = 0;
	uint32_t V_22 = 0;
	int32_t V_23 = 0;
	int32_t V_24 = 0;
	ExifProperty_t8_99 * V_25 = {0};
	int32_t V_26 = 0;
	MemoryStream_t1_433 * V_27 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	List_1_t1_1906 * G_B2_0 = {0};
	ExifFile_t8_110 * G_B2_1 = {0};
	List_1_t1_1906 * G_B1_0 = {0};
	ExifFile_t8_110 * G_B1_1 = {0};
	List_1_t1_1906 * G_B5_0 = {0};
	List_1_t1_1906 * G_B4_0 = {0};
	{
		JPEGFile_t8_111 * L_0 = (__this->___file_0);
		NullCheck(L_0);
		List_1_t1_1906 * L_1 = JPEGFile_get_Sections_m8_642(L_0, /*hidden argument*/NULL);
		Predicate_1_t1_1905 * L_2 = ((ExifFile_t8_110_StaticFields*)ExifFile_t8_110_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cacheF_15;
		G_B1_0 = L_1;
		G_B1_1 = __this;
		if (L_2)
		{
			G_B2_0 = L_1;
			G_B2_1 = __this;
			goto IL_0024;
		}
	}
	{
		IntPtr_t L_3 = { (void*)ExifFile_U3CReadAPP1U3Em__2_m8_494_MethodInfo_var };
		Predicate_1_t1_1905 * L_4 = (Predicate_1_t1_1905 *)il2cpp_codegen_object_new (Predicate_1_t1_1905_il2cpp_TypeInfo_var);
		Predicate_1__ctor_m1_15040(L_4, NULL, L_3, /*hidden argument*/Predicate_1__ctor_m1_15040_MethodInfo_var);
		((ExifFile_t8_110_StaticFields*)ExifFile_t8_110_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cacheF_15 = L_4;
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
	}

IL_0024:
	{
		Predicate_1_t1_1905 * L_5 = ((ExifFile_t8_110_StaticFields*)ExifFile_t8_110_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cacheF_15;
		NullCheck(G_B2_0);
		JPEGSection_t8_112 * L_6 = List_1_Find_m1_15041(G_B2_0, L_5, /*hidden argument*/List_1_Find_m1_15041_MethodInfo_var);
		NullCheck(G_B2_1);
		G_B2_1->___app1_1 = L_6;
		JPEGSection_t8_112 * L_7 = (__this->___app1_1);
		if (L_7)
		{
			goto IL_00a8;
		}
	}
	{
		JPEGFile_t8_111 * L_8 = (__this->___file_0);
		NullCheck(L_8);
		List_1_t1_1906 * L_9 = JPEGFile_get_Sections_m8_642(L_8, /*hidden argument*/NULL);
		Predicate_1_t1_1905 * L_10 = ((ExifFile_t8_110_StaticFields*)ExifFile_t8_110_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache10_16;
		G_B4_0 = L_9;
		if (L_10)
		{
			G_B5_0 = L_9;
			goto IL_0061;
		}
	}
	{
		IntPtr_t L_11 = { (void*)ExifFile_U3CReadAPP1U3Em__3_m8_495_MethodInfo_var };
		Predicate_1_t1_1905 * L_12 = (Predicate_1_t1_1905 *)il2cpp_codegen_object_new (Predicate_1_t1_1905_il2cpp_TypeInfo_var);
		Predicate_1__ctor_m1_15040(L_12, NULL, L_11, /*hidden argument*/Predicate_1__ctor_m1_15040_MethodInfo_var);
		((ExifFile_t8_110_StaticFields*)ExifFile_t8_110_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache10_16 = L_12;
		G_B5_0 = G_B4_0;
	}

IL_0061:
	{
		Predicate_1_t1_1905 * L_13 = ((ExifFile_t8_110_StaticFields*)ExifFile_t8_110_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache10_16;
		NullCheck(G_B5_0);
		int32_t L_14 = List_1_FindLastIndex_m1_15042(G_B5_0, L_13, /*hidden argument*/List_1_FindLastIndex_m1_15042_MethodInfo_var);
		V_0 = L_14;
		int32_t L_15 = V_0;
		if ((!(((uint32_t)L_15) == ((uint32_t)(-1)))))
		{
			goto IL_0075;
		}
	}
	{
		V_0 = 0;
	}

IL_0075:
	{
		int32_t L_16 = V_0;
		V_0 = ((int32_t)((int32_t)L_16+(int32_t)1));
		ExifFile_set_ByteOrder_m8_482(__this, 2, /*hidden argument*/NULL);
		JPEGSection_t8_112 * L_17 = (JPEGSection_t8_112 *)il2cpp_codegen_object_new (JPEGSection_t8_112_il2cpp_TypeInfo_var);
		JPEGSection__ctor_m8_651(L_17, ((int32_t)225), /*hidden argument*/NULL);
		__this->___app1_1 = L_17;
		JPEGFile_t8_111 * L_18 = (__this->___file_0);
		NullCheck(L_18);
		List_1_t1_1906 * L_19 = JPEGFile_get_Sections_m8_642(L_18, /*hidden argument*/NULL);
		int32_t L_20 = V_0;
		JPEGSection_t8_112 * L_21 = (__this->___app1_1);
		NullCheck(L_19);
		VirtActionInvoker2< int32_t, JPEGSection_t8_112 * >::Invoke(29 /* System.Void System.Collections.Generic.List`1<ExifLibrary.JPEGSection>::Insert(System.Int32,!0) */, L_19, L_20, L_21);
		return;
	}

IL_00a8:
	{
		JPEGSection_t8_112 * L_22 = (__this->___app1_1);
		NullCheck(L_22);
		ByteU5BU5D_t1_109* L_23 = JPEGSection_get_Header_m8_654(L_22, /*hidden argument*/NULL);
		V_1 = L_23;
		SortedList_2_t3_248 * L_24 = (SortedList_2_t3_248 *)il2cpp_codegen_object_new (SortedList_2_t3_248_il2cpp_TypeInfo_var);
		SortedList_2__ctor_m3_1809(L_24, /*hidden argument*/SortedList_2__ctor_m3_1809_MethodInfo_var);
		V_2 = L_24;
		__this->___makerNoteOffset_2 = 0;
		V_3 = 6;
		ByteU5BU5D_t1_109* L_25 = V_1;
		int32_t L_26 = V_3;
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, L_26);
		int32_t L_27 = L_26;
		if ((!(((uint32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_25, L_27, sizeof(uint8_t)))) == ((uint32_t)((int32_t)73)))))
		{
			goto IL_00d9;
		}
	}
	{
		ExifFile_set_ByteOrder_m8_482(__this, 1, /*hidden argument*/NULL);
		goto IL_00e0;
	}

IL_00d9:
	{
		ExifFile_set_ByteOrder_m8_482(__this, 2, /*hidden argument*/NULL);
	}

IL_00e0:
	{
		int32_t L_28 = ExifFile_get_ByteOrder_m8_481(__this, /*hidden argument*/NULL);
		BitConverterEx_t8_61 * L_29 = (BitConverterEx_t8_61 *)il2cpp_codegen_object_new (BitConverterEx_t8_61_il2cpp_TypeInfo_var);
		BitConverterEx__ctor_m8_340(L_29, L_28, 0, /*hidden argument*/NULL);
		V_4 = L_29;
		BitConverterEx_t8_61 * L_30 = V_4;
		ByteU5BU5D_t1_109* L_31 = V_1;
		int32_t L_32 = V_3;
		NullCheck(L_30);
		uint32_t L_33 = BitConverterEx_ToUInt32_m8_364(L_30, L_31, ((int32_t)((int32_t)L_32+(int32_t)4)), /*hidden argument*/NULL);
		V_5 = L_33;
		SortedList_2_t3_248 * L_34 = V_2;
		int32_t L_35 = V_5;
		NullCheck(L_34);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(28 /* System.Void System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::Add(!0,!1) */, L_34, L_35, ((int32_t)100000));
		V_6 = (-1);
		V_7 = 0;
		V_8 = (-1);
		goto IL_049f;
	}

IL_0116:
	{
		int32_t L_36 = V_3;
		SortedList_2_t3_248 * L_37 = V_2;
		NullCheck(L_37);
		Object_t* L_38 = SortedList_2_get_Keys_m3_1810(L_37, /*hidden argument*/SortedList_2_get_Keys_m3_1810_MethodInfo_var);
		NullCheck(L_38);
		int32_t L_39 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Int32>::get_Item(System.Int32) */, IList_1_t1_1922_il2cpp_TypeInfo_var, L_38, 0);
		V_9 = ((int32_t)((int32_t)L_36+(int32_t)L_39));
		SortedList_2_t3_248 * L_40 = V_2;
		NullCheck(L_40);
		Object_t* L_41 = SortedList_2_get_Values_m3_1811(L_40, /*hidden argument*/SortedList_2_get_Values_m3_1811_MethodInfo_var);
		NullCheck(L_41);
		int32_t L_42 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<ExifLibrary.IFD>::get_Item(System.Int32) */, IList_1_t1_1923_il2cpp_TypeInfo_var, L_41, 0);
		V_10 = L_42;
		SortedList_2_t3_248 * L_43 = V_2;
		NullCheck(L_43);
		SortedList_2_RemoveAt_m3_1812(L_43, 0, /*hidden argument*/SortedList_2_RemoveAt_m3_1812_MethodInfo_var);
		BitConverterEx_t8_61 * L_44 = V_4;
		ByteU5BU5D_t1_109* L_45 = V_1;
		int32_t L_46 = V_9;
		NullCheck(L_44);
		uint16_t L_47 = BitConverterEx_ToUInt16_m8_363(L_44, L_45, L_46, /*hidden argument*/NULL);
		V_11 = L_47;
		V_12 = 0;
		goto IL_041e;
	}

IL_014f:
	{
		int32_t L_48 = V_9;
		int16_t L_49 = V_12;
		V_13 = ((int32_t)((int32_t)((int32_t)((int32_t)L_48+(int32_t)2))+(int32_t)((int32_t)((int32_t)((int32_t)12)*(int32_t)L_49))));
		BitConverterEx_t8_61 * L_50 = V_4;
		ByteU5BU5D_t1_109* L_51 = V_1;
		int32_t L_52 = V_13;
		NullCheck(L_50);
		uint16_t L_53 = BitConverterEx_ToUInt16_m8_363(L_50, L_51, L_52, /*hidden argument*/NULL);
		V_14 = L_53;
		BitConverterEx_t8_61 * L_54 = V_4;
		ByteU5BU5D_t1_109* L_55 = V_1;
		int32_t L_56 = V_13;
		NullCheck(L_54);
		uint16_t L_57 = BitConverterEx_ToUInt16_m8_363(L_54, L_55, ((int32_t)((int32_t)L_56+(int32_t)2)), /*hidden argument*/NULL);
		V_15 = L_57;
		BitConverterEx_t8_61 * L_58 = V_4;
		ByteU5BU5D_t1_109* L_59 = V_1;
		int32_t L_60 = V_13;
		NullCheck(L_58);
		uint32_t L_61 = BitConverterEx_ToUInt32_m8_364(L_58, L_59, ((int32_t)((int32_t)L_60+(int32_t)4)), /*hidden argument*/NULL);
		V_16 = L_61;
		V_17 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 4));
		ByteU5BU5D_t1_109* L_62 = V_1;
		int32_t L_63 = V_13;
		ByteU5BU5D_t1_109* L_64 = V_17;
		Array_Copy_m1_1041(NULL /*static, unused*/, (Array_t *)(Array_t *)L_62, ((int32_t)((int32_t)L_63+(int32_t)8)), (Array_t *)(Array_t *)L_64, 0, 4, /*hidden argument*/NULL);
		int32_t L_65 = V_10;
		if ((!(((uint32_t)L_65) == ((uint32_t)((int32_t)100000)))))
		{
			goto IL_01cf;
		}
	}
	{
		uint16_t L_66 = V_14;
		if ((!(((uint32_t)L_66) == ((uint32_t)((int32_t)34665)))))
		{
			goto IL_01cf;
		}
	}
	{
		BitConverterEx_t8_61 * L_67 = V_4;
		ByteU5BU5D_t1_109* L_68 = V_17;
		NullCheck(L_67);
		uint32_t L_69 = BitConverterEx_ToUInt32_m8_364(L_67, L_68, 0, /*hidden argument*/NULL);
		V_18 = L_69;
		SortedList_2_t3_248 * L_70 = V_2;
		int32_t L_71 = V_18;
		NullCheck(L_70);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(28 /* System.Void System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::Add(!0,!1) */, L_70, L_71, ((int32_t)200000));
		goto IL_0236;
	}

IL_01cf:
	{
		int32_t L_72 = V_10;
		if ((!(((uint32_t)L_72) == ((uint32_t)((int32_t)100000)))))
		{
			goto IL_0205;
		}
	}
	{
		uint16_t L_73 = V_14;
		if ((!(((uint32_t)L_73) == ((uint32_t)((int32_t)34853)))))
		{
			goto IL_0205;
		}
	}
	{
		BitConverterEx_t8_61 * L_74 = V_4;
		ByteU5BU5D_t1_109* L_75 = V_17;
		NullCheck(L_74);
		uint32_t L_76 = BitConverterEx_ToUInt32_m8_364(L_74, L_75, 0, /*hidden argument*/NULL);
		V_19 = L_76;
		SortedList_2_t3_248 * L_77 = V_2;
		int32_t L_78 = V_19;
		NullCheck(L_77);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(28 /* System.Void System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::Add(!0,!1) */, L_77, L_78, ((int32_t)300000));
		goto IL_0236;
	}

IL_0205:
	{
		int32_t L_79 = V_10;
		if ((!(((uint32_t)L_79) == ((uint32_t)((int32_t)200000)))))
		{
			goto IL_0236;
		}
	}
	{
		uint16_t L_80 = V_14;
		if ((!(((uint32_t)L_80) == ((uint32_t)((int32_t)40965)))))
		{
			goto IL_0236;
		}
	}
	{
		BitConverterEx_t8_61 * L_81 = V_4;
		ByteU5BU5D_t1_109* L_82 = V_17;
		NullCheck(L_81);
		uint32_t L_83 = BitConverterEx_ToUInt32_m8_364(L_81, L_82, 0, /*hidden argument*/NULL);
		V_20 = L_83;
		SortedList_2_t3_248 * L_84 = V_2;
		int32_t L_85 = V_20;
		NullCheck(L_84);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(28 /* System.Void System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::Add(!0,!1) */, L_84, L_85, ((int32_t)400000));
	}

IL_0236:
	{
		int32_t L_86 = V_10;
		if ((!(((uint32_t)L_86) == ((uint32_t)((int32_t)200000)))))
		{
			goto IL_025e;
		}
	}
	{
		uint16_t L_87 = V_14;
		if ((!(((uint32_t)L_87) == ((uint32_t)((int32_t)37500)))))
		{
			goto IL_025e;
		}
	}
	{
		BitConverterEx_t8_61 * L_88 = V_4;
		ByteU5BU5D_t1_109* L_89 = V_17;
		NullCheck(L_88);
		uint32_t L_90 = BitConverterEx_ToUInt32_m8_364(L_88, L_89, 0, /*hidden argument*/NULL);
		__this->___makerNoteOffset_2 = L_90;
	}

IL_025e:
	{
		V_21 = 0;
		uint16_t L_91 = V_15;
		if ((((int32_t)L_91) == ((int32_t)1)))
		{
			goto IL_0279;
		}
	}
	{
		uint16_t L_92 = V_15;
		if ((((int32_t)L_92) == ((int32_t)2)))
		{
			goto IL_0279;
		}
	}
	{
		uint16_t L_93 = V_15;
		if ((!(((uint32_t)L_93) == ((uint32_t)7))))
		{
			goto IL_0281;
		}
	}

IL_0279:
	{
		V_21 = 1;
		goto IL_02be;
	}

IL_0281:
	{
		uint16_t L_94 = V_15;
		if ((!(((uint32_t)L_94) == ((uint32_t)3))))
		{
			goto IL_0291;
		}
	}
	{
		V_21 = 2;
		goto IL_02be;
	}

IL_0291:
	{
		uint16_t L_95 = V_15;
		if ((((int32_t)L_95) == ((int32_t)4)))
		{
			goto IL_02a2;
		}
	}
	{
		uint16_t L_96 = V_15;
		if ((!(((uint32_t)L_96) == ((uint32_t)((int32_t)9)))))
		{
			goto IL_02aa;
		}
	}

IL_02a2:
	{
		V_21 = 4;
		goto IL_02be;
	}

IL_02aa:
	{
		uint16_t L_97 = V_15;
		if ((((int32_t)L_97) == ((int32_t)5)))
		{
			goto IL_02bb;
		}
	}
	{
		uint16_t L_98 = V_15;
		if ((!(((uint32_t)L_98) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_02be;
		}
	}

IL_02bb:
	{
		V_21 = 8;
	}

IL_02be:
	{
		uint32_t L_99 = V_16;
		uint32_t L_100 = V_21;
		V_22 = ((int32_t)((int32_t)L_99*(int32_t)L_100));
		V_23 = 0;
		uint32_t L_101 = V_22;
		if ((!(((uint32_t)L_101) > ((uint32_t)4))))
		{
			goto IL_02f8;
		}
	}
	{
		int32_t L_102 = V_3;
		BitConverterEx_t8_61 * L_103 = V_4;
		ByteU5BU5D_t1_109* L_104 = V_17;
		NullCheck(L_103);
		uint32_t L_105 = BitConverterEx_ToUInt32_m8_364(L_103, L_104, 0, /*hidden argument*/NULL);
		V_23 = ((int32_t)((int32_t)L_102+(int32_t)L_105));
		uint32_t L_106 = V_22;
		V_17 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, (((uintptr_t)L_106))));
		ByteU5BU5D_t1_109* L_107 = V_1;
		int32_t L_108 = V_23;
		ByteU5BU5D_t1_109* L_109 = V_17;
		uint32_t L_110 = V_22;
		Array_Copy_m1_1042(NULL /*static, unused*/, (Array_t *)(Array_t *)L_107, (((int64_t)((int64_t)L_108))), (Array_t *)(Array_t *)L_109, (((int64_t)((int64_t)0))), (((int64_t)((uint64_t)L_110))), /*hidden argument*/NULL);
	}

IL_02f8:
	{
		int32_t L_111 = V_10;
		if ((!(((uint32_t)L_111) == ((uint32_t)((int32_t)500000)))))
		{
			goto IL_0324;
		}
	}
	{
		uint16_t L_112 = V_14;
		if ((!(((uint32_t)L_112) == ((uint32_t)((int32_t)513)))))
		{
			goto IL_0324;
		}
	}
	{
		V_8 = 0;
		BitConverterEx_t8_61 * L_113 = V_4;
		ByteU5BU5D_t1_109* L_114 = V_17;
		NullCheck(L_113);
		uint32_t L_115 = BitConverterEx_ToUInt32_m8_364(L_113, L_114, 0, /*hidden argument*/NULL);
		V_6 = L_115;
		goto IL_0348;
	}

IL_0324:
	{
		int32_t L_116 = V_10;
		if ((!(((uint32_t)L_116) == ((uint32_t)((int32_t)500000)))))
		{
			goto IL_0348;
		}
	}
	{
		uint16_t L_117 = V_14;
		if ((!(((uint32_t)L_117) == ((uint32_t)((int32_t)514)))))
		{
			goto IL_0348;
		}
	}
	{
		BitConverterEx_t8_61 * L_118 = V_4;
		ByteU5BU5D_t1_109* L_119 = V_17;
		NullCheck(L_118);
		uint32_t L_120 = BitConverterEx_ToUInt32_m8_364(L_118, L_119, 0, /*hidden argument*/NULL);
		V_7 = L_120;
	}

IL_0348:
	{
		int32_t L_121 = V_10;
		if ((!(((uint32_t)L_121) == ((uint32_t)((int32_t)500000)))))
		{
			goto IL_038d;
		}
	}
	{
		uint16_t L_122 = V_14;
		if ((!(((uint32_t)L_122) == ((uint32_t)((int32_t)273)))))
		{
			goto IL_038d;
		}
	}
	{
		V_8 = 1;
		uint16_t L_123 = V_15;
		if ((!(((uint32_t)L_123) == ((uint32_t)3))))
		{
			goto IL_037c;
		}
	}
	{
		BitConverterEx_t8_61 * L_124 = V_4;
		ByteU5BU5D_t1_109* L_125 = V_17;
		NullCheck(L_124);
		uint16_t L_126 = BitConverterEx_ToUInt16_m8_363(L_124, L_125, 0, /*hidden argument*/NULL);
		V_6 = L_126;
		goto IL_0388;
	}

IL_037c:
	{
		BitConverterEx_t8_61 * L_127 = V_4;
		ByteU5BU5D_t1_109* L_128 = V_17;
		NullCheck(L_127);
		uint32_t L_129 = BitConverterEx_ToUInt32_m8_364(L_127, L_128, 0, /*hidden argument*/NULL);
		V_6 = L_129;
	}

IL_0388:
	{
		goto IL_03ec;
	}

IL_038d:
	{
		int32_t L_130 = V_10;
		if ((!(((uint32_t)L_130) == ((uint32_t)((int32_t)500000)))))
		{
			goto IL_03ec;
		}
	}
	{
		uint16_t L_131 = V_14;
		if ((!(((uint32_t)L_131) == ((uint32_t)((int32_t)279)))))
		{
			goto IL_03ec;
		}
	}
	{
		V_7 = 0;
		V_24 = 0;
		goto IL_03e1;
	}

IL_03b0:
	{
		uint16_t L_132 = V_15;
		if ((!(((uint32_t)L_132) == ((uint32_t)3))))
		{
			goto IL_03cc;
		}
	}
	{
		int32_t L_133 = V_7;
		BitConverterEx_t8_61 * L_134 = V_4;
		ByteU5BU5D_t1_109* L_135 = V_17;
		NullCheck(L_134);
		uint16_t L_136 = BitConverterEx_ToUInt16_m8_363(L_134, L_135, 0, /*hidden argument*/NULL);
		V_7 = ((int32_t)((int32_t)L_133+(int32_t)L_136));
		goto IL_03db;
	}

IL_03cc:
	{
		int32_t L_137 = V_7;
		BitConverterEx_t8_61 * L_138 = V_4;
		ByteU5BU5D_t1_109* L_139 = V_17;
		NullCheck(L_138);
		uint32_t L_140 = BitConverterEx_ToUInt32_m8_364(L_138, L_139, 0, /*hidden argument*/NULL);
		V_7 = ((int32_t)((int32_t)L_137+(int32_t)L_140));
	}

IL_03db:
	{
		int32_t L_141 = V_24;
		V_24 = ((int32_t)((int32_t)L_141+(int32_t)1));
	}

IL_03e1:
	{
		int32_t L_142 = V_24;
		uint32_t L_143 = V_16;
		if ((((int64_t)(((int64_t)((int64_t)L_142)))) < ((int64_t)(((int64_t)((uint64_t)L_143))))))
		{
			goto IL_03b0;
		}
	}

IL_03ec:
	{
		uint16_t L_144 = V_14;
		uint16_t L_145 = V_15;
		uint32_t L_146 = V_16;
		ByteU5BU5D_t1_109* L_147 = V_17;
		int32_t L_148 = ExifFile_get_ByteOrder_m8_481(__this, /*hidden argument*/NULL);
		int32_t L_149 = V_10;
		ExifProperty_t8_99 * L_150 = ExifPropertyFactory_Get_m8_628(NULL /*static, unused*/, L_144, L_145, L_146, L_147, L_148, L_149, /*hidden argument*/NULL);
		V_25 = L_150;
		Dictionary_2_t1_1904 * L_151 = ExifFile_get_Properties_m8_479(__this, /*hidden argument*/NULL);
		ExifProperty_t8_99 * L_152 = V_25;
		NullCheck(L_152);
		int32_t L_153 = ExifProperty_get_Tag_m8_503(L_152, /*hidden argument*/NULL);
		ExifProperty_t8_99 * L_154 = V_25;
		NullCheck(L_151);
		VirtActionInvoker2< int32_t, ExifProperty_t8_99 * >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::Add(!0,!1) */, L_151, L_153, L_154);
		int16_t L_155 = V_12;
		V_12 = (((int16_t)((int16_t)((int32_t)((int32_t)L_155+(int32_t)1)))));
	}

IL_041e:
	{
		int16_t L_156 = V_12;
		uint16_t L_157 = V_11;
		if ((((int32_t)L_156) < ((int32_t)L_157)))
		{
			goto IL_014f;
		}
	}
	{
		BitConverterEx_t8_61 * L_158 = V_4;
		ByteU5BU5D_t1_109* L_159 = V_1;
		int32_t L_160 = V_9;
		uint16_t L_161 = V_11;
		NullCheck(L_158);
		uint32_t L_162 = BitConverterEx_ToUInt32_m8_364(L_158, L_159, ((int32_t)((int32_t)((int32_t)((int32_t)L_160+(int32_t)2))+(int32_t)((int32_t)((int32_t)((int32_t)12)*(int32_t)L_161)))), /*hidden argument*/NULL);
		V_26 = L_162;
		int32_t L_163 = V_26;
		if (!L_163)
		{
			goto IL_044f;
		}
	}
	{
		SortedList_2_t3_248 * L_164 = V_2;
		int32_t L_165 = V_26;
		NullCheck(L_164);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(28 /* System.Void System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::Add(!0,!1) */, L_164, L_165, ((int32_t)500000));
	}

IL_044f:
	{
		int32_t L_166 = V_6;
		if ((((int32_t)L_166) == ((int32_t)(-1))))
		{
			goto IL_049f;
		}
	}
	{
		int32_t L_167 = V_7;
		if (!L_167)
		{
			goto IL_049f;
		}
	}
	{
		JPEGFile_t8_111 * L_168 = ExifFile_get_Thumbnail_m8_483(__this, /*hidden argument*/NULL);
		if (L_168)
		{
			goto IL_049f;
		}
	}
	{
		int32_t L_169 = V_8;
		if (L_169)
		{
			goto IL_049f;
		}
	}
	{
		ByteU5BU5D_t1_109* L_170 = V_1;
		int32_t L_171 = V_3;
		int32_t L_172 = V_6;
		int32_t L_173 = V_7;
		MemoryStream_t1_433 * L_174 = (MemoryStream_t1_433 *)il2cpp_codegen_object_new (MemoryStream_t1_433_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m1_5048(L_174, L_170, ((int32_t)((int32_t)L_171+(int32_t)L_172)), L_173, /*hidden argument*/NULL);
		V_27 = L_174;
	}

IL_047e:
	try
	{ // begin try (depth: 1)
		MemoryStream_t1_433 * L_175 = V_27;
		JPEGFile_t8_111 * L_176 = (JPEGFile_t8_111 *)il2cpp_codegen_object_new (JPEGFile_t8_111_il2cpp_TypeInfo_var);
		JPEGFile__ctor_m8_640(L_176, L_175, /*hidden argument*/NULL);
		ExifFile_set_Thumbnail_m8_484(__this, L_176, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x49F, FINALLY_0490);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0490;
	}

FINALLY_0490:
	{ // begin finally (depth: 1)
		{
			MemoryStream_t1_433 * L_177 = V_27;
			if (!L_177)
			{
				goto IL_049e;
			}
		}

IL_0497:
		{
			MemoryStream_t1_433 * L_178 = V_27;
			NullCheck(L_178);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_178);
		}

IL_049e:
		{
			IL2CPP_END_FINALLY(1168)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(1168)
	{
		IL2CPP_JUMP_TBL(0x49F, IL_049f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_049f:
	{
		SortedList_2_t3_248 * L_179 = V_2;
		NullCheck(L_179);
		int32_t L_180 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::get_Count() */, L_179);
		if (L_180)
		{
			goto IL_0116;
		}
	}
	{
		return;
	}
}
// System.Void ExifLibrary.ExifFile::WriteApp1(System.Boolean)
extern TypeInfo* Dictionary_2_t1_1904_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t1_1925_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern TypeInfo* ExifUInt_t8_118_il2cpp_TypeInfo_var;
extern TypeInfo* IFD0IsEmptyException_t8_97_il2cpp_TypeInfo_var;
extern TypeInfo* BitConverterEx_t8_61_il2cpp_TypeInfo_var;
extern TypeInfo* MemoryStream_t1_433_il2cpp_TypeInfo_var;
extern TypeInfo* Encoding_t1_406_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_15043_MethodInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m1_15044_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1_15045_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m1_15046_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m1_15047_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1_15048_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral5222;
extern "C" void ExifFile_WriteApp1_m8_490 (ExifFile_t8_110 * __this, bool ___preserveMakerNote, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t1_1904_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1985);
		Enumerator_t1_1925_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1986);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		ExifUInt_t8_118_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1987);
		IFD0IsEmptyException_t8_97_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1988);
		BitConverterEx_t8_61_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1970);
		MemoryStream_t1_433_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(390);
		Encoding_t1_406_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		Dictionary_2__ctor_m1_15043_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484138);
		Dictionary_2_GetEnumerator_m1_15044_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484139);
		Enumerator_get_Current_m1_15045_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484140);
		KeyValuePair_2_get_Value_m1_15046_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484141);
		KeyValuePair_2_get_Key_m1_15047_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484142);
		Enumerator_MoveNext_m1_15048_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484143);
		_stringLiteral5222 = il2cpp_codegen_string_literal_from_index(5222);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t1_1904 * V_0 = {0};
	Dictionary_2_t1_1904 * V_1 = {0};
	Dictionary_2_t1_1904 * V_2 = {0};
	Dictionary_2_t1_1904 * V_3 = {0};
	Dictionary_2_t1_1904 * V_4 = {0};
	KeyValuePair_2_t1_1924  V_5 = {0};
	Enumerator_t1_1925  V_6 = {0};
	BitConverterEx_t8_61 * V_7 = {0};
	MemoryStream_t1_433 * V_8 = {0};
	int64_t V_9 = 0;
	uint32_t V_10 = 0;
	uint32_t V_11 = 0;
	uint32_t V_12 = 0;
	uint32_t V_13 = 0;
	int32_t V_14 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	MemoryStream_t1_433 * G_B38_0 = {0};
	MemoryStream_t1_433 * G_B37_0 = {0};
	ByteU5BU5D_t1_109* G_B39_0 = {0};
	MemoryStream_t1_433 * G_B39_1 = {0};
	{
		__this->___exifIFDFieldOffset_3 = (((int64_t)((int64_t)0)));
		__this->___gpsIFDFieldOffset_4 = (((int64_t)((int64_t)0)));
		__this->___interopIFDFieldOffset_5 = (((int64_t)((int64_t)0)));
		__this->___firstIFDFieldOffset_6 = (((int64_t)((int64_t)0)));
		__this->___thumbOffsetLocation_7 = (((int64_t)((int64_t)0)));
		__this->___thumbOffsetValue_9 = 0;
		__this->___thumbSizeLocation_8 = (((int64_t)((int64_t)0)));
		__this->___thumbSizeValue_10 = 0;
		Dictionary_2_t1_1904 * L_0 = (Dictionary_2_t1_1904 *)il2cpp_codegen_object_new (Dictionary_2_t1_1904_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_15043(L_0, /*hidden argument*/Dictionary_2__ctor_m1_15043_MethodInfo_var);
		V_0 = L_0;
		Dictionary_2_t1_1904 * L_1 = (Dictionary_2_t1_1904 *)il2cpp_codegen_object_new (Dictionary_2_t1_1904_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_15043(L_1, /*hidden argument*/Dictionary_2__ctor_m1_15043_MethodInfo_var);
		V_1 = L_1;
		Dictionary_2_t1_1904 * L_2 = (Dictionary_2_t1_1904 *)il2cpp_codegen_object_new (Dictionary_2_t1_1904_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_15043(L_2, /*hidden argument*/Dictionary_2__ctor_m1_15043_MethodInfo_var);
		V_2 = L_2;
		Dictionary_2_t1_1904 * L_3 = (Dictionary_2_t1_1904 *)il2cpp_codegen_object_new (Dictionary_2_t1_1904_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_15043(L_3, /*hidden argument*/Dictionary_2__ctor_m1_15043_MethodInfo_var);
		V_3 = L_3;
		Dictionary_2_t1_1904 * L_4 = (Dictionary_2_t1_1904 *)il2cpp_codegen_object_new (Dictionary_2_t1_1904_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_15043(L_4, /*hidden argument*/Dictionary_2__ctor_m1_15043_MethodInfo_var);
		V_4 = L_4;
		Dictionary_2_t1_1904 * L_5 = ExifFile_get_Properties_m8_479(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Enumerator_t1_1925  L_6 = Dictionary_2_GetEnumerator_m1_15044(L_5, /*hidden argument*/Dictionary_2_GetEnumerator_m1_15044_MethodInfo_var);
		V_6 = L_6;
	}

IL_006a:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0145;
		}

IL_006f:
		{
			KeyValuePair_2_t1_1924  L_7 = Enumerator_get_Current_m1_15045((&V_6), /*hidden argument*/Enumerator_get_Current_m1_15045_MethodInfo_var);
			V_5 = L_7;
			ExifProperty_t8_99 * L_8 = KeyValuePair_2_get_Value_m1_15046((&V_5), /*hidden argument*/KeyValuePair_2_get_Value_m1_15046_MethodInfo_var);
			NullCheck(L_8);
			int32_t L_9 = ExifProperty_get_IFD_m8_504(L_8, /*hidden argument*/NULL);
			V_14 = L_9;
			int32_t L_10 = V_14;
			if ((((int32_t)L_10) == ((int32_t)((int32_t)100000))))
			{
				goto IL_00c7;
			}
		}

IL_0092:
		{
			int32_t L_11 = V_14;
			if ((((int32_t)L_11) == ((int32_t)((int32_t)200000))))
			{
				goto IL_00e0;
			}
		}

IL_009e:
		{
			int32_t L_12 = V_14;
			if ((((int32_t)L_12) == ((int32_t)((int32_t)300000))))
			{
				goto IL_00f9;
			}
		}

IL_00aa:
		{
			int32_t L_13 = V_14;
			if ((((int32_t)L_13) == ((int32_t)((int32_t)400000))))
			{
				goto IL_0112;
			}
		}

IL_00b6:
		{
			int32_t L_14 = V_14;
			if ((((int32_t)L_14) == ((int32_t)((int32_t)500000))))
			{
				goto IL_012b;
			}
		}

IL_00c2:
		{
			goto IL_0145;
		}

IL_00c7:
		{
			Dictionary_2_t1_1904 * L_15 = V_0;
			int32_t L_16 = KeyValuePair_2_get_Key_m1_15047((&V_5), /*hidden argument*/KeyValuePair_2_get_Key_m1_15047_MethodInfo_var);
			ExifProperty_t8_99 * L_17 = KeyValuePair_2_get_Value_m1_15046((&V_5), /*hidden argument*/KeyValuePair_2_get_Value_m1_15046_MethodInfo_var);
			NullCheck(L_15);
			VirtActionInvoker2< int32_t, ExifProperty_t8_99 * >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::Add(!0,!1) */, L_15, L_16, L_17);
			goto IL_0145;
		}

IL_00e0:
		{
			Dictionary_2_t1_1904 * L_18 = V_1;
			int32_t L_19 = KeyValuePair_2_get_Key_m1_15047((&V_5), /*hidden argument*/KeyValuePair_2_get_Key_m1_15047_MethodInfo_var);
			ExifProperty_t8_99 * L_20 = KeyValuePair_2_get_Value_m1_15046((&V_5), /*hidden argument*/KeyValuePair_2_get_Value_m1_15046_MethodInfo_var);
			NullCheck(L_18);
			VirtActionInvoker2< int32_t, ExifProperty_t8_99 * >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::Add(!0,!1) */, L_18, L_19, L_20);
			goto IL_0145;
		}

IL_00f9:
		{
			Dictionary_2_t1_1904 * L_21 = V_2;
			int32_t L_22 = KeyValuePair_2_get_Key_m1_15047((&V_5), /*hidden argument*/KeyValuePair_2_get_Key_m1_15047_MethodInfo_var);
			ExifProperty_t8_99 * L_23 = KeyValuePair_2_get_Value_m1_15046((&V_5), /*hidden argument*/KeyValuePair_2_get_Value_m1_15046_MethodInfo_var);
			NullCheck(L_21);
			VirtActionInvoker2< int32_t, ExifProperty_t8_99 * >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::Add(!0,!1) */, L_21, L_22, L_23);
			goto IL_0145;
		}

IL_0112:
		{
			Dictionary_2_t1_1904 * L_24 = V_3;
			int32_t L_25 = KeyValuePair_2_get_Key_m1_15047((&V_5), /*hidden argument*/KeyValuePair_2_get_Key_m1_15047_MethodInfo_var);
			ExifProperty_t8_99 * L_26 = KeyValuePair_2_get_Value_m1_15046((&V_5), /*hidden argument*/KeyValuePair_2_get_Value_m1_15046_MethodInfo_var);
			NullCheck(L_24);
			VirtActionInvoker2< int32_t, ExifProperty_t8_99 * >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::Add(!0,!1) */, L_24, L_25, L_26);
			goto IL_0145;
		}

IL_012b:
		{
			Dictionary_2_t1_1904 * L_27 = V_4;
			int32_t L_28 = KeyValuePair_2_get_Key_m1_15047((&V_5), /*hidden argument*/KeyValuePair_2_get_Key_m1_15047_MethodInfo_var);
			ExifProperty_t8_99 * L_29 = KeyValuePair_2_get_Value_m1_15046((&V_5), /*hidden argument*/KeyValuePair_2_get_Value_m1_15046_MethodInfo_var);
			NullCheck(L_27);
			VirtActionInvoker2< int32_t, ExifProperty_t8_99 * >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::Add(!0,!1) */, L_27, L_28, L_29);
			goto IL_0145;
		}

IL_0145:
		{
			bool L_30 = Enumerator_MoveNext_m1_15048((&V_6), /*hidden argument*/Enumerator_MoveNext_m1_15048_MethodInfo_var);
			if (L_30)
			{
				goto IL_006f;
			}
		}

IL_0151:
		{
			IL2CPP_LEAVE(0x163, FINALLY_0156);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0156;
	}

FINALLY_0156:
	{ // begin finally (depth: 1)
		Enumerator_t1_1925  L_31 = V_6;
		Enumerator_t1_1925  L_32 = L_31;
		Object_t * L_33 = Box(Enumerator_t1_1925_il2cpp_TypeInfo_var, &L_32);
		NullCheck(L_33);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_33);
		IL2CPP_END_FINALLY(342)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(342)
	{
		IL2CPP_JUMP_TBL(0x163, IL_0163)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0163:
	{
		Dictionary_2_t1_1904 * L_34 = V_1;
		NullCheck(L_34);
		int32_t L_35 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::get_Count() */, L_34);
		if (!L_35)
		{
			goto IL_0194;
		}
	}
	{
		Dictionary_2_t1_1904 * L_36 = V_0;
		NullCheck(L_36);
		bool L_37 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::ContainsKey(!0) */, L_36, ((int32_t)134665));
		if (L_37)
		{
			goto IL_0194;
		}
	}
	{
		Dictionary_2_t1_1904 * L_38 = V_0;
		ExifUInt_t8_118 * L_39 = (ExifUInt_t8_118 *)il2cpp_codegen_object_new (ExifUInt_t8_118_il2cpp_TypeInfo_var);
		ExifUInt__ctor_m8_549(L_39, ((int32_t)134665), 0, /*hidden argument*/NULL);
		NullCheck(L_38);
		VirtActionInvoker2< int32_t, ExifProperty_t8_99 * >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::Add(!0,!1) */, L_38, ((int32_t)134665), L_39);
	}

IL_0194:
	{
		Dictionary_2_t1_1904 * L_40 = V_2;
		NullCheck(L_40);
		int32_t L_41 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::get_Count() */, L_40);
		if (!L_41)
		{
			goto IL_01c5;
		}
	}
	{
		Dictionary_2_t1_1904 * L_42 = V_0;
		NullCheck(L_42);
		bool L_43 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::ContainsKey(!0) */, L_42, ((int32_t)134853));
		if (L_43)
		{
			goto IL_01c5;
		}
	}
	{
		Dictionary_2_t1_1904 * L_44 = V_0;
		ExifUInt_t8_118 * L_45 = (ExifUInt_t8_118 *)il2cpp_codegen_object_new (ExifUInt_t8_118_il2cpp_TypeInfo_var);
		ExifUInt__ctor_m8_549(L_45, ((int32_t)134853), 0, /*hidden argument*/NULL);
		NullCheck(L_44);
		VirtActionInvoker2< int32_t, ExifProperty_t8_99 * >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::Add(!0,!1) */, L_44, ((int32_t)134853), L_45);
	}

IL_01c5:
	{
		Dictionary_2_t1_1904 * L_46 = V_3;
		NullCheck(L_46);
		int32_t L_47 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::get_Count() */, L_46);
		if (!L_47)
		{
			goto IL_01f6;
		}
	}
	{
		Dictionary_2_t1_1904 * L_48 = V_1;
		NullCheck(L_48);
		bool L_49 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::ContainsKey(!0) */, L_48, ((int32_t)240965));
		if (L_49)
		{
			goto IL_01f6;
		}
	}
	{
		Dictionary_2_t1_1904 * L_50 = V_1;
		ExifUInt_t8_118 * L_51 = (ExifUInt_t8_118 *)il2cpp_codegen_object_new (ExifUInt_t8_118_il2cpp_TypeInfo_var);
		ExifUInt__ctor_m8_549(L_51, ((int32_t)240965), 0, /*hidden argument*/NULL);
		NullCheck(L_50);
		VirtActionInvoker2< int32_t, ExifProperty_t8_99 * >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::Add(!0,!1) */, L_50, ((int32_t)240965), L_51);
	}

IL_01f6:
	{
		Dictionary_2_t1_1904 * L_52 = V_1;
		NullCheck(L_52);
		int32_t L_53 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::get_Count() */, L_52);
		if (L_53)
		{
			goto IL_021d;
		}
	}
	{
		Dictionary_2_t1_1904 * L_54 = V_0;
		NullCheck(L_54);
		bool L_55 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::ContainsKey(!0) */, L_54, ((int32_t)134665));
		if (!L_55)
		{
			goto IL_021d;
		}
	}
	{
		Dictionary_2_t1_1904 * L_56 = V_0;
		NullCheck(L_56);
		VirtFuncInvoker1< bool, int32_t >::Invoke(20 /* System.Boolean System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::Remove(!0) */, L_56, ((int32_t)134665));
	}

IL_021d:
	{
		Dictionary_2_t1_1904 * L_57 = V_2;
		NullCheck(L_57);
		int32_t L_58 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::get_Count() */, L_57);
		if (L_58)
		{
			goto IL_0244;
		}
	}
	{
		Dictionary_2_t1_1904 * L_59 = V_0;
		NullCheck(L_59);
		bool L_60 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::ContainsKey(!0) */, L_59, ((int32_t)134853));
		if (!L_60)
		{
			goto IL_0244;
		}
	}
	{
		Dictionary_2_t1_1904 * L_61 = V_0;
		NullCheck(L_61);
		VirtFuncInvoker1< bool, int32_t >::Invoke(20 /* System.Boolean System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::Remove(!0) */, L_61, ((int32_t)134853));
	}

IL_0244:
	{
		Dictionary_2_t1_1904 * L_62 = V_3;
		NullCheck(L_62);
		int32_t L_63 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::get_Count() */, L_62);
		if (L_63)
		{
			goto IL_026b;
		}
	}
	{
		Dictionary_2_t1_1904 * L_64 = V_1;
		NullCheck(L_64);
		bool L_65 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::ContainsKey(!0) */, L_64, ((int32_t)240965));
		if (!L_65)
		{
			goto IL_026b;
		}
	}
	{
		Dictionary_2_t1_1904 * L_66 = V_1;
		NullCheck(L_66);
		VirtFuncInvoker1< bool, int32_t >::Invoke(20 /* System.Boolean System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::Remove(!0) */, L_66, ((int32_t)240965));
	}

IL_026b:
	{
		Dictionary_2_t1_1904 * L_67 = V_0;
		NullCheck(L_67);
		int32_t L_68 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::get_Count() */, L_67);
		if (L_68)
		{
			goto IL_027c;
		}
	}
	{
		IFD0IsEmptyException_t8_97 * L_69 = (IFD0IsEmptyException_t8_97 *)il2cpp_codegen_object_new (IFD0IsEmptyException_t8_97_il2cpp_TypeInfo_var);
		IFD0IsEmptyException__ctor_m8_409(L_69, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_69);
	}

IL_027c:
	{
		int32_t L_70 = ExifFile_get_ByteOrder_m8_481(__this, /*hidden argument*/NULL);
		BitConverterEx_t8_61 * L_71 = (BitConverterEx_t8_61 *)il2cpp_codegen_object_new (BitConverterEx_t8_61_il2cpp_TypeInfo_var);
		BitConverterEx__ctor_m8_340(L_71, 0, L_70, /*hidden argument*/NULL);
		V_7 = L_71;
		MemoryStream_t1_433 * L_72 = (MemoryStream_t1_433 *)il2cpp_codegen_object_new (MemoryStream_t1_433_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m1_5044(L_72, /*hidden argument*/NULL);
		V_8 = L_72;
		MemoryStream_t1_433 * L_73 = V_8;
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t1_406_il2cpp_TypeInfo_var);
		Encoding_t1_406 * L_74 = Encoding_get_ASCII_m1_12357(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_74);
		ByteU5BU5D_t1_109* L_75 = (ByteU5BU5D_t1_109*)VirtFuncInvoker1< ByteU5BU5D_t1_109*, String_t* >::Invoke(11 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_74, _stringLiteral5222);
		NullCheck(L_73);
		VirtActionInvoker3< ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(24 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_73, L_75, 0, 6);
		MemoryStream_t1_433 * L_76 = V_8;
		NullCheck(L_76);
		int64_t L_77 = (int64_t)VirtFuncInvoker0< int64_t >::Invoke(10 /* System.Int64 System.IO.MemoryStream::get_Position() */, L_76);
		V_9 = L_77;
		MemoryStream_t1_433 * L_78 = V_8;
		int32_t L_79 = ExifFile_get_ByteOrder_m8_481(__this, /*hidden argument*/NULL);
		G_B37_0 = L_78;
		if ((!(((uint32_t)L_79) == ((uint32_t)1))))
		{
			G_B38_0 = L_78;
			goto IL_02d5;
		}
	}
	{
		ByteU5BU5D_t1_109* L_80 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 2));
		NullCheck(L_80);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_80, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_80, 0, sizeof(uint8_t))) = (uint8_t)((int32_t)73);
		ByteU5BU5D_t1_109* L_81 = L_80;
		NullCheck(L_81);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_81, 1);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_81, 1, sizeof(uint8_t))) = (uint8_t)((int32_t)73);
		G_B39_0 = L_81;
		G_B39_1 = G_B37_0;
		goto IL_02e5;
	}

IL_02d5:
	{
		ByteU5BU5D_t1_109* L_82 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 2));
		NullCheck(L_82);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_82, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_82, 0, sizeof(uint8_t))) = (uint8_t)((int32_t)77);
		ByteU5BU5D_t1_109* L_83 = L_82;
		NullCheck(L_83);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_83, 1);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_83, 1, sizeof(uint8_t))) = (uint8_t)((int32_t)77);
		G_B39_0 = L_83;
		G_B39_1 = G_B38_0;
	}

IL_02e5:
	{
		NullCheck(G_B39_1);
		VirtActionInvoker3< ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(24 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, G_B39_1, G_B39_0, 0, 2);
		MemoryStream_t1_433 * L_84 = V_8;
		BitConverterEx_t8_61 * L_85 = V_7;
		NullCheck(L_85);
		ByteU5BU5D_t1_109* L_86 = BitConverterEx_GetBytes_m8_371(L_85, ((int32_t)42), /*hidden argument*/NULL);
		NullCheck(L_84);
		VirtActionInvoker3< ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(24 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_84, L_86, 0, 2);
		MemoryStream_t1_433 * L_87 = V_8;
		BitConverterEx_t8_61 * L_88 = V_7;
		NullCheck(L_88);
		ByteU5BU5D_t1_109* L_89 = BitConverterEx_GetBytes_m8_372(L_88, 8, /*hidden argument*/NULL);
		NullCheck(L_87);
		VirtActionInvoker3< ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(24 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_87, L_89, 0, 4);
		MemoryStream_t1_433 * L_90 = V_8;
		Dictionary_2_t1_1904 * L_91 = V_0;
		int64_t L_92 = V_9;
		bool L_93 = ___preserveMakerNote;
		ExifFile_WriteIFD_m8_491(__this, L_90, L_91, ((int32_t)100000), L_92, L_93, /*hidden argument*/NULL);
		MemoryStream_t1_433 * L_94 = V_8;
		NullCheck(L_94);
		int64_t L_95 = (int64_t)VirtFuncInvoker0< int64_t >::Invoke(10 /* System.Int64 System.IO.MemoryStream::get_Position() */, L_94);
		int64_t L_96 = V_9;
		V_10 = (((int32_t)((uint32_t)((int64_t)((int64_t)L_95-(int64_t)L_96)))));
		MemoryStream_t1_433 * L_97 = V_8;
		Dictionary_2_t1_1904 * L_98 = V_1;
		int64_t L_99 = V_9;
		bool L_100 = ___preserveMakerNote;
		ExifFile_WriteIFD_m8_491(__this, L_97, L_98, ((int32_t)200000), L_99, L_100, /*hidden argument*/NULL);
		MemoryStream_t1_433 * L_101 = V_8;
		NullCheck(L_101);
		int64_t L_102 = (int64_t)VirtFuncInvoker0< int64_t >::Invoke(10 /* System.Int64 System.IO.MemoryStream::get_Position() */, L_101);
		int64_t L_103 = V_9;
		V_11 = (((int32_t)((uint32_t)((int64_t)((int64_t)L_102-(int64_t)L_103)))));
		MemoryStream_t1_433 * L_104 = V_8;
		Dictionary_2_t1_1904 * L_105 = V_2;
		int64_t L_106 = V_9;
		bool L_107 = ___preserveMakerNote;
		ExifFile_WriteIFD_m8_491(__this, L_104, L_105, ((int32_t)300000), L_106, L_107, /*hidden argument*/NULL);
		MemoryStream_t1_433 * L_108 = V_8;
		NullCheck(L_108);
		int64_t L_109 = (int64_t)VirtFuncInvoker0< int64_t >::Invoke(10 /* System.Int64 System.IO.MemoryStream::get_Position() */, L_108);
		int64_t L_110 = V_9;
		V_12 = (((int32_t)((uint32_t)((int64_t)((int64_t)L_109-(int64_t)L_110)))));
		MemoryStream_t1_433 * L_111 = V_8;
		Dictionary_2_t1_1904 * L_112 = V_3;
		int64_t L_113 = V_9;
		bool L_114 = ___preserveMakerNote;
		ExifFile_WriteIFD_m8_491(__this, L_111, L_112, ((int32_t)400000), L_113, L_114, /*hidden argument*/NULL);
		MemoryStream_t1_433 * L_115 = V_8;
		NullCheck(L_115);
		int64_t L_116 = (int64_t)VirtFuncInvoker0< int64_t >::Invoke(10 /* System.Int64 System.IO.MemoryStream::get_Position() */, L_115);
		int64_t L_117 = V_9;
		V_13 = (((int32_t)((uint32_t)((int64_t)((int64_t)L_116-(int64_t)L_117)))));
		MemoryStream_t1_433 * L_118 = V_8;
		Dictionary_2_t1_1904 * L_119 = V_4;
		int64_t L_120 = V_9;
		bool L_121 = ___preserveMakerNote;
		ExifFile_WriteIFD_m8_491(__this, L_118, L_119, ((int32_t)500000), L_120, L_121, /*hidden argument*/NULL);
		int64_t L_122 = (__this->___exifIFDFieldOffset_3);
		if (!L_122)
		{
			goto IL_03c5;
		}
	}
	{
		MemoryStream_t1_433 * L_123 = V_8;
		int64_t L_124 = (__this->___exifIFDFieldOffset_3);
		NullCheck(L_123);
		VirtFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(22 /* System.Int64 System.IO.MemoryStream::Seek(System.Int64,System.IO.SeekOrigin) */, L_123, L_124, 0);
		MemoryStream_t1_433 * L_125 = V_8;
		BitConverterEx_t8_61 * L_126 = V_7;
		uint32_t L_127 = V_10;
		NullCheck(L_126);
		ByteU5BU5D_t1_109* L_128 = BitConverterEx_GetBytes_m8_372(L_126, L_127, /*hidden argument*/NULL);
		NullCheck(L_125);
		VirtActionInvoker3< ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(24 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_125, L_128, 0, 4);
	}

IL_03c5:
	{
		int64_t L_129 = (__this->___gpsIFDFieldOffset_4);
		if (!L_129)
		{
			goto IL_03f1;
		}
	}
	{
		MemoryStream_t1_433 * L_130 = V_8;
		int64_t L_131 = (__this->___gpsIFDFieldOffset_4);
		NullCheck(L_130);
		VirtFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(22 /* System.Int64 System.IO.MemoryStream::Seek(System.Int64,System.IO.SeekOrigin) */, L_130, L_131, 0);
		MemoryStream_t1_433 * L_132 = V_8;
		BitConverterEx_t8_61 * L_133 = V_7;
		uint32_t L_134 = V_11;
		NullCheck(L_133);
		ByteU5BU5D_t1_109* L_135 = BitConverterEx_GetBytes_m8_372(L_133, L_134, /*hidden argument*/NULL);
		NullCheck(L_132);
		VirtActionInvoker3< ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(24 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_132, L_135, 0, 4);
	}

IL_03f1:
	{
		int64_t L_136 = (__this->___interopIFDFieldOffset_5);
		if (!L_136)
		{
			goto IL_041d;
		}
	}
	{
		MemoryStream_t1_433 * L_137 = V_8;
		int64_t L_138 = (__this->___interopIFDFieldOffset_5);
		NullCheck(L_137);
		VirtFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(22 /* System.Int64 System.IO.MemoryStream::Seek(System.Int64,System.IO.SeekOrigin) */, L_137, L_138, 0);
		MemoryStream_t1_433 * L_139 = V_8;
		BitConverterEx_t8_61 * L_140 = V_7;
		uint32_t L_141 = V_12;
		NullCheck(L_140);
		ByteU5BU5D_t1_109* L_142 = BitConverterEx_GetBytes_m8_372(L_140, L_141, /*hidden argument*/NULL);
		NullCheck(L_139);
		VirtActionInvoker3< ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(24 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_139, L_142, 0, 4);
	}

IL_041d:
	{
		int64_t L_143 = (__this->___firstIFDFieldOffset_6);
		if (!L_143)
		{
			goto IL_0449;
		}
	}
	{
		MemoryStream_t1_433 * L_144 = V_8;
		int64_t L_145 = (__this->___firstIFDFieldOffset_6);
		NullCheck(L_144);
		VirtFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(22 /* System.Int64 System.IO.MemoryStream::Seek(System.Int64,System.IO.SeekOrigin) */, L_144, L_145, 0);
		MemoryStream_t1_433 * L_146 = V_8;
		BitConverterEx_t8_61 * L_147 = V_7;
		uint32_t L_148 = V_13;
		NullCheck(L_147);
		ByteU5BU5D_t1_109* L_149 = BitConverterEx_GetBytes_m8_372(L_147, L_148, /*hidden argument*/NULL);
		NullCheck(L_146);
		VirtActionInvoker3< ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(24 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_146, L_149, 0, 4);
	}

IL_0449:
	{
		int64_t L_150 = (__this->___thumbOffsetLocation_7);
		if (!L_150)
		{
			goto IL_0479;
		}
	}
	{
		MemoryStream_t1_433 * L_151 = V_8;
		int64_t L_152 = (__this->___thumbOffsetLocation_7);
		NullCheck(L_151);
		VirtFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(22 /* System.Int64 System.IO.MemoryStream::Seek(System.Int64,System.IO.SeekOrigin) */, L_151, L_152, 0);
		MemoryStream_t1_433 * L_153 = V_8;
		BitConverterEx_t8_61 * L_154 = V_7;
		uint32_t L_155 = (__this->___thumbOffsetValue_9);
		NullCheck(L_154);
		ByteU5BU5D_t1_109* L_156 = BitConverterEx_GetBytes_m8_372(L_154, L_155, /*hidden argument*/NULL);
		NullCheck(L_153);
		VirtActionInvoker3< ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(24 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_153, L_156, 0, 4);
	}

IL_0479:
	{
		int64_t L_157 = (__this->___thumbSizeLocation_8);
		if (!L_157)
		{
			goto IL_04a9;
		}
	}
	{
		MemoryStream_t1_433 * L_158 = V_8;
		int64_t L_159 = (__this->___thumbSizeLocation_8);
		NullCheck(L_158);
		VirtFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(22 /* System.Int64 System.IO.MemoryStream::Seek(System.Int64,System.IO.SeekOrigin) */, L_158, L_159, 0);
		MemoryStream_t1_433 * L_160 = V_8;
		BitConverterEx_t8_61 * L_161 = V_7;
		uint32_t L_162 = (__this->___thumbSizeValue_10);
		NullCheck(L_161);
		ByteU5BU5D_t1_109* L_163 = BitConverterEx_GetBytes_m8_372(L_161, L_162, /*hidden argument*/NULL);
		NullCheck(L_160);
		VirtActionInvoker3< ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(24 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_160, L_163, 0, 4);
	}

IL_04a9:
	{
		MemoryStream_t1_433 * L_164 = V_8;
		NullCheck(L_164);
		VirtActionInvoker0::Invoke(13 /* System.Void System.IO.Stream::Close() */, L_164);
		JPEGSection_t8_112 * L_165 = (__this->___app1_1);
		MemoryStream_t1_433 * L_166 = V_8;
		NullCheck(L_166);
		ByteU5BU5D_t1_109* L_167 = (ByteU5BU5D_t1_109*)VirtFuncInvoker0< ByteU5BU5D_t1_109* >::Invoke(33 /* System.Byte[] System.IO.MemoryStream::ToArray() */, L_166);
		NullCheck(L_165);
		JPEGSection_set_Header_m8_655(L_165, L_167, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ExifLibrary.ExifFile::WriteIFD(System.IO.MemoryStream,System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>,ExifLibrary.IFD,System.Int64,System.Boolean)
extern TypeInfo* BitConverterEx_t8_61_il2cpp_TypeInfo_var;
extern TypeInfo* Queue_1_t3_249_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t1_1926_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern TypeInfo* MemoryStream_t1_433_il2cpp_TypeInfo_var;
extern const MethodInfo* Queue_1__ctor_m3_1813_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Values_m1_15049_MethodInfo_var;
extern const MethodInfo* ValueCollection_GetEnumerator_m1_15050_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1_15051_MethodInfo_var;
extern const MethodInfo* Queue_1_Enqueue_m3_1814_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1_15052_MethodInfo_var;
extern const MethodInfo* Queue_1_Dequeue_m3_1815_MethodInfo_var;
extern "C" void ExifFile_WriteIFD_m8_491 (ExifFile_t8_110 * __this, MemoryStream_t1_433 * ___stream, Dictionary_2_t1_1904 * ___ifd, int32_t ___ifdtype, int64_t ___tiffoffset, bool ___preserveMakerNote, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		BitConverterEx_t8_61_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1970);
		Queue_1_t3_249_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1989);
		Enumerator_t1_1926_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1990);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		MemoryStream_t1_433_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(390);
		Queue_1__ctor_m3_1813_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484144);
		Dictionary_2_get_Values_m1_15049_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484145);
		ValueCollection_GetEnumerator_m1_15050_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484146);
		Enumerator_get_Current_m1_15051_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484147);
		Queue_1_Enqueue_m3_1814_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484148);
		Enumerator_MoveNext_m1_15052_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484149);
		Queue_1_Dequeue_m3_1815_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484150);
		s_Il2CppMethodIntialized = true;
	}
	BitConverterEx_t8_61 * V_0 = {0};
	Queue_1_t3_249 * V_1 = {0};
	ExifProperty_t8_99 * V_2 = {0};
	Enumerator_t1_1926  V_3 = {0};
	uint32_t V_4 = 0;
	uint32_t V_5 = 0;
	int64_t V_6 = 0;
	bool V_7 = false;
	ExifProperty_t8_99 * V_8 = {0};
	ExifInterOperability_t8_113  V_9 = {0};
	uint32_t V_10 = 0;
	ByteU5BU5D_t1_109* V_11 = {0};
	int32_t V_12 = 0;
	int64_t V_13 = 0;
	int32_t V_14 = 0;
	MemoryStream_t1_433 * V_15 = {0};
	ByteU5BU5D_t1_109* V_16 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = ExifFile_get_ByteOrder_m8_481(__this, /*hidden argument*/NULL);
		BitConverterEx_t8_61 * L_1 = (BitConverterEx_t8_61 *)il2cpp_codegen_object_new (BitConverterEx_t8_61_il2cpp_TypeInfo_var);
		BitConverterEx__ctor_m8_340(L_1, 0, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Queue_1_t3_249 * L_2 = (Queue_1_t3_249 *)il2cpp_codegen_object_new (Queue_1_t3_249_il2cpp_TypeInfo_var);
		Queue_1__ctor_m3_1813(L_2, /*hidden argument*/Queue_1__ctor_m3_1813_MethodInfo_var);
		V_1 = L_2;
		Dictionary_2_t1_1904 * L_3 = ___ifd;
		NullCheck(L_3);
		ValueCollection_t1_1927 * L_4 = Dictionary_2_get_Values_m1_15049(L_3, /*hidden argument*/Dictionary_2_get_Values_m1_15049_MethodInfo_var);
		NullCheck(L_4);
		Enumerator_t1_1926  L_5 = ValueCollection_GetEnumerator_m1_15050(L_4, /*hidden argument*/ValueCollection_GetEnumerator_m1_15050_MethodInfo_var);
		V_3 = L_5;
	}

IL_001f:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0043;
		}

IL_0024:
		{
			ExifProperty_t8_99 * L_6 = Enumerator_get_Current_m1_15051((&V_3), /*hidden argument*/Enumerator_get_Current_m1_15051_MethodInfo_var);
			V_2 = L_6;
			ExifProperty_t8_99 * L_7 = V_2;
			NullCheck(L_7);
			int32_t L_8 = ExifProperty_get_Tag_m8_503(L_7, /*hidden argument*/NULL);
			if ((((int32_t)L_8) == ((int32_t)((int32_t)237500))))
			{
				goto IL_0043;
			}
		}

IL_003c:
		{
			Queue_1_t3_249 * L_9 = V_1;
			ExifProperty_t8_99 * L_10 = V_2;
			NullCheck(L_9);
			Queue_1_Enqueue_m3_1814(L_9, L_10, /*hidden argument*/Queue_1_Enqueue_m3_1814_MethodInfo_var);
		}

IL_0043:
		{
			bool L_11 = Enumerator_MoveNext_m1_15052((&V_3), /*hidden argument*/Enumerator_MoveNext_m1_15052_MethodInfo_var);
			if (L_11)
			{
				goto IL_0024;
			}
		}

IL_004f:
		{
			IL2CPP_LEAVE(0x60, FINALLY_0054);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0054;
	}

FINALLY_0054:
	{ // begin finally (depth: 1)
		Enumerator_t1_1926  L_12 = V_3;
		Enumerator_t1_1926  L_13 = L_12;
		Object_t * L_14 = Box(Enumerator_t1_1926_il2cpp_TypeInfo_var, &L_13);
		NullCheck(L_14);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_14);
		IL2CPP_END_FINALLY(84)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(84)
	{
		IL2CPP_JUMP_TBL(0x60, IL_0060)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0060:
	{
		Dictionary_2_t1_1904 * L_15 = ___ifd;
		NullCheck(L_15);
		bool L_16 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::ContainsKey(!0) */, L_15, ((int32_t)237500));
		if (!L_16)
		{
			goto IL_0081;
		}
	}
	{
		Queue_1_t3_249 * L_17 = V_1;
		Dictionary_2_t1_1904 * L_18 = ___ifd;
		NullCheck(L_18);
		ExifProperty_t8_99 * L_19 = (ExifProperty_t8_99 *)VirtFuncInvoker1< ExifProperty_t8_99 *, int32_t >::Invoke(22 /* !1 System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::get_Item(!0) */, L_18, ((int32_t)237500));
		NullCheck(L_17);
		Queue_1_Enqueue_m3_1814(L_17, L_19, /*hidden argument*/Queue_1_Enqueue_m3_1814_MethodInfo_var);
	}

IL_0081:
	{
		Dictionary_2_t1_1904 * L_20 = ___ifd;
		NullCheck(L_20);
		int32_t L_21 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::get_Count() */, L_20);
		MemoryStream_t1_433 * L_22 = ___stream;
		NullCheck(L_22);
		int64_t L_23 = (int64_t)VirtFuncInvoker0< int64_t >::Invoke(10 /* System.Int64 System.IO.MemoryStream::get_Position() */, L_22);
		int64_t L_24 = ___tiffoffset;
		V_4 = (((int32_t)((uint32_t)((int64_t)((int64_t)((int64_t)((int64_t)(((int64_t)((int64_t)((int32_t)((int32_t)((int32_t)((int32_t)2+(int32_t)((int32_t)((int32_t)L_21*(int32_t)((int32_t)12)))))+(int32_t)4)))))+(int64_t)L_23))-(int64_t)L_24)))));
		uint32_t L_25 = V_4;
		V_5 = L_25;
		MemoryStream_t1_433 * L_26 = ___stream;
		NullCheck(L_26);
		int64_t L_27 = (int64_t)VirtFuncInvoker0< int64_t >::Invoke(10 /* System.Int64 System.IO.MemoryStream::get_Position() */, L_26);
		Dictionary_2_t1_1904 * L_28 = ___ifd;
		NullCheck(L_28);
		int32_t L_29 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::get_Count() */, L_28);
		V_6 = ((int64_t)((int64_t)L_27+(int64_t)(((int64_t)((int64_t)((int32_t)((int32_t)((int32_t)((int32_t)2+(int32_t)((int32_t)((int32_t)L_29*(int32_t)((int32_t)12)))))+(int32_t)4)))))));
		V_7 = 0;
		MemoryStream_t1_433 * L_30 = ___stream;
		BitConverterEx_t8_61 * L_31 = V_0;
		Dictionary_2_t1_1904 * L_32 = ___ifd;
		NullCheck(L_32);
		int32_t L_33 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::get_Count() */, L_32);
		NullCheck(L_31);
		ByteU5BU5D_t1_109* L_34 = BitConverterEx_GetBytes_m8_371(L_31, (((int32_t)((uint16_t)L_33))), /*hidden argument*/NULL);
		NullCheck(L_30);
		VirtActionInvoker3< ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(24 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_30, L_34, 0, 2);
		goto IL_03f8;
	}

IL_00d4:
	{
		Queue_1_t3_249 * L_35 = V_1;
		NullCheck(L_35);
		ExifProperty_t8_99 * L_36 = Queue_1_Dequeue_m3_1815(L_35, /*hidden argument*/Queue_1_Dequeue_m3_1815_MethodInfo_var);
		V_8 = L_36;
		ExifProperty_t8_99 * L_37 = V_8;
		NullCheck(L_37);
		ExifInterOperability_t8_113  L_38 = (ExifInterOperability_t8_113 )VirtFuncInvoker0< ExifInterOperability_t8_113  >::Invoke(6 /* ExifLibrary.ExifInterOperability ExifLibrary.ExifProperty::get_Interoperability() */, L_37);
		V_9 = L_38;
		V_10 = 0;
		bool L_39 = V_7;
		if (L_39)
		{
			goto IL_0167;
		}
	}
	{
		bool L_40 = (__this->___makerNoteProcessed_11);
		if (L_40)
		{
			goto IL_0167;
		}
	}
	{
		uint32_t L_41 = (__this->___makerNoteOffset_2);
		if (!L_41)
		{
			goto IL_0167;
		}
	}
	{
		int32_t L_42 = ___ifdtype;
		if ((!(((uint32_t)L_42) == ((uint32_t)((int32_t)200000)))))
		{
			goto IL_0167;
		}
	}
	{
		ExifProperty_t8_99 * L_43 = V_8;
		NullCheck(L_43);
		int32_t L_44 = ExifProperty_get_Tag_m8_503(L_43, /*hidden argument*/NULL);
		if ((((int32_t)L_44) == ((int32_t)((int32_t)237500))))
		{
			goto IL_0167;
		}
	}
	{
		ByteU5BU5D_t1_109* L_45 = ExifInterOperability_get_Data_m8_500((&V_9), /*hidden argument*/NULL);
		NullCheck(L_45);
		if ((((int32_t)(((int32_t)((int32_t)(((Array_t *)L_45)->max_length))))) <= ((int32_t)4)))
		{
			goto IL_0167;
		}
	}
	{
		uint32_t L_46 = V_5;
		ByteU5BU5D_t1_109* L_47 = ExifInterOperability_get_Data_m8_500((&V_9), /*hidden argument*/NULL);
		NullCheck(L_47);
		uint32_t L_48 = (__this->___makerNoteOffset_2);
		if ((((int64_t)((int64_t)((int64_t)(((int64_t)((uint64_t)L_46)))+(int64_t)(((int64_t)((int64_t)(((int32_t)((int32_t)(((Array_t *)L_47)->max_length)))))))))) <= ((int64_t)(((int64_t)((uint64_t)L_48))))))
		{
			goto IL_0167;
		}
	}
	{
		Dictionary_2_t1_1904 * L_49 = ___ifd;
		NullCheck(L_49);
		bool L_50 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::ContainsKey(!0) */, L_49, ((int32_t)237500));
		if (!L_50)
		{
			goto IL_0167;
		}
	}
	{
		Queue_1_t3_249 * L_51 = V_1;
		ExifProperty_t8_99 * L_52 = V_8;
		NullCheck(L_51);
		Queue_1_Enqueue_m3_1814(L_51, L_52, /*hidden argument*/Queue_1_Enqueue_m3_1814_MethodInfo_var);
		goto IL_03f8;
	}

IL_0167:
	{
		ExifProperty_t8_99 * L_53 = V_8;
		NullCheck(L_53);
		int32_t L_54 = ExifProperty_get_Tag_m8_503(L_53, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_54) == ((uint32_t)((int32_t)237500)))))
		{
			goto IL_01a0;
		}
	}
	{
		V_7 = 1;
		bool L_55 = ___preserveMakerNote;
		if (!L_55)
		{
			goto IL_019d;
		}
	}
	{
		bool L_56 = (__this->___makerNoteProcessed_11);
		if (L_56)
		{
			goto IL_019d;
		}
	}
	{
		uint32_t L_57 = (__this->___makerNoteOffset_2);
		uint32_t L_58 = V_5;
		V_10 = ((int32_t)((int32_t)L_57-(int32_t)L_58));
		goto IL_01a0;
	}

IL_019d:
	{
		V_10 = 0;
	}

IL_01a0:
	{
		MemoryStream_t1_433 * L_59 = ___stream;
		BitConverterEx_t8_61 * L_60 = V_0;
		uint16_t L_61 = ExifInterOperability_get_TagID_m8_497((&V_9), /*hidden argument*/NULL);
		NullCheck(L_60);
		ByteU5BU5D_t1_109* L_62 = BitConverterEx_GetBytes_m8_371(L_60, L_61, /*hidden argument*/NULL);
		NullCheck(L_59);
		VirtActionInvoker3< ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(24 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_59, L_62, 0, 2);
		MemoryStream_t1_433 * L_63 = ___stream;
		BitConverterEx_t8_61 * L_64 = V_0;
		uint16_t L_65 = ExifInterOperability_get_TypeID_m8_498((&V_9), /*hidden argument*/NULL);
		NullCheck(L_64);
		ByteU5BU5D_t1_109* L_66 = BitConverterEx_GetBytes_m8_371(L_64, L_65, /*hidden argument*/NULL);
		NullCheck(L_63);
		VirtActionInvoker3< ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(24 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_63, L_66, 0, 2);
		MemoryStream_t1_433 * L_67 = ___stream;
		BitConverterEx_t8_61 * L_68 = V_0;
		uint32_t L_69 = ExifInterOperability_get_Count_m8_499((&V_9), /*hidden argument*/NULL);
		NullCheck(L_68);
		ByteU5BU5D_t1_109* L_70 = BitConverterEx_GetBytes_m8_372(L_68, L_69, /*hidden argument*/NULL);
		NullCheck(L_67);
		VirtActionInvoker3< ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(24 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_67, L_70, 0, 4);
		ByteU5BU5D_t1_109* L_71 = ExifInterOperability_get_Data_m8_500((&V_9), /*hidden argument*/NULL);
		V_11 = L_71;
		int32_t L_72 = ExifFile_get_ByteOrder_m8_481(__this, /*hidden argument*/NULL);
		int32_t L_73 = BitConverterEx_get_SystemByteOrder_m8_341(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_72) == ((int32_t)L_73)))
		{
			goto IL_0266;
		}
	}
	{
		uint16_t L_74 = ExifInterOperability_get_TypeID_m8_498((&V_9), /*hidden argument*/NULL);
		if ((((int32_t)L_74) == ((int32_t)1)))
		{
			goto IL_022d;
		}
	}
	{
		uint16_t L_75 = ExifInterOperability_get_TypeID_m8_498((&V_9), /*hidden argument*/NULL);
		if ((((int32_t)L_75) == ((int32_t)3)))
		{
			goto IL_022d;
		}
	}
	{
		uint16_t L_76 = ExifInterOperability_get_TypeID_m8_498((&V_9), /*hidden argument*/NULL);
		if ((((int32_t)L_76) == ((int32_t)4)))
		{
			goto IL_022d;
		}
	}
	{
		uint16_t L_77 = ExifInterOperability_get_TypeID_m8_498((&V_9), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_77) == ((uint32_t)((int32_t)9)))))
		{
			goto IL_0239;
		}
	}

IL_022d:
	{
		ByteU5BU5D_t1_109* L_78 = V_11;
		Array_Reverse_m1_1052(NULL /*static, unused*/, (Array_t *)(Array_t *)L_78, /*hidden argument*/NULL);
		goto IL_0266;
	}

IL_0239:
	{
		uint16_t L_79 = ExifInterOperability_get_TypeID_m8_498((&V_9), /*hidden argument*/NULL);
		if ((((int32_t)L_79) == ((int32_t)5)))
		{
			goto IL_0254;
		}
	}
	{
		uint16_t L_80 = ExifInterOperability_get_TypeID_m8_498((&V_9), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_80) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_0266;
		}
	}

IL_0254:
	{
		ByteU5BU5D_t1_109* L_81 = V_11;
		Array_Reverse_m1_1053(NULL /*static, unused*/, (Array_t *)(Array_t *)L_81, 0, 4, /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_82 = V_11;
		Array_Reverse_m1_1053(NULL /*static, unused*/, (Array_t *)(Array_t *)L_82, 4, 4, /*hidden argument*/NULL);
	}

IL_0266:
	{
		int32_t L_83 = ___ifdtype;
		if ((!(((uint32_t)L_83) == ((uint32_t)((int32_t)100000)))))
		{
			goto IL_0293;
		}
	}
	{
		uint16_t L_84 = ExifInterOperability_get_TagID_m8_497((&V_9), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_84) == ((uint32_t)((int32_t)34665)))))
		{
			goto IL_0293;
		}
	}
	{
		MemoryStream_t1_433 * L_85 = ___stream;
		NullCheck(L_85);
		int64_t L_86 = (int64_t)VirtFuncInvoker0< int64_t >::Invoke(10 /* System.Int64 System.IO.MemoryStream::get_Position() */, L_85);
		__this->___exifIFDFieldOffset_3 = L_86;
		goto IL_0342;
	}

IL_0293:
	{
		int32_t L_87 = ___ifdtype;
		if ((!(((uint32_t)L_87) == ((uint32_t)((int32_t)100000)))))
		{
			goto IL_02c0;
		}
	}
	{
		uint16_t L_88 = ExifInterOperability_get_TagID_m8_497((&V_9), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_88) == ((uint32_t)((int32_t)34853)))))
		{
			goto IL_02c0;
		}
	}
	{
		MemoryStream_t1_433 * L_89 = ___stream;
		NullCheck(L_89);
		int64_t L_90 = (int64_t)VirtFuncInvoker0< int64_t >::Invoke(10 /* System.Int64 System.IO.MemoryStream::get_Position() */, L_89);
		__this->___gpsIFDFieldOffset_4 = L_90;
		goto IL_0342;
	}

IL_02c0:
	{
		int32_t L_91 = ___ifdtype;
		if ((!(((uint32_t)L_91) == ((uint32_t)((int32_t)200000)))))
		{
			goto IL_02ed;
		}
	}
	{
		uint16_t L_92 = ExifInterOperability_get_TagID_m8_497((&V_9), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_92) == ((uint32_t)((int32_t)40965)))))
		{
			goto IL_02ed;
		}
	}
	{
		MemoryStream_t1_433 * L_93 = ___stream;
		NullCheck(L_93);
		int64_t L_94 = (int64_t)VirtFuncInvoker0< int64_t >::Invoke(10 /* System.Int64 System.IO.MemoryStream::get_Position() */, L_93);
		__this->___interopIFDFieldOffset_5 = L_94;
		goto IL_0342;
	}

IL_02ed:
	{
		int32_t L_95 = ___ifdtype;
		if ((!(((uint32_t)L_95) == ((uint32_t)((int32_t)500000)))))
		{
			goto IL_031a;
		}
	}
	{
		uint16_t L_96 = ExifInterOperability_get_TagID_m8_497((&V_9), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_96) == ((uint32_t)((int32_t)513)))))
		{
			goto IL_031a;
		}
	}
	{
		MemoryStream_t1_433 * L_97 = ___stream;
		NullCheck(L_97);
		int64_t L_98 = (int64_t)VirtFuncInvoker0< int64_t >::Invoke(10 /* System.Int64 System.IO.MemoryStream::get_Position() */, L_97);
		__this->___thumbOffsetLocation_7 = L_98;
		goto IL_0342;
	}

IL_031a:
	{
		int32_t L_99 = ___ifdtype;
		if ((!(((uint32_t)L_99) == ((uint32_t)((int32_t)500000)))))
		{
			goto IL_0342;
		}
	}
	{
		uint16_t L_100 = ExifInterOperability_get_TagID_m8_497((&V_9), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_100) == ((uint32_t)((int32_t)514)))))
		{
			goto IL_0342;
		}
	}
	{
		MemoryStream_t1_433 * L_101 = ___stream;
		NullCheck(L_101);
		int64_t L_102 = (int64_t)VirtFuncInvoker0< int64_t >::Invoke(10 /* System.Int64 System.IO.MemoryStream::get_Position() */, L_101);
		__this->___thumbSizeLocation_8 = L_102;
	}

IL_0342:
	{
		ByteU5BU5D_t1_109* L_103 = V_11;
		NullCheck(L_103);
		if ((((int32_t)(((int32_t)((int32_t)(((Array_t *)L_103)->max_length))))) > ((int32_t)4)))
		{
			goto IL_037e;
		}
	}
	{
		MemoryStream_t1_433 * L_104 = ___stream;
		ByteU5BU5D_t1_109* L_105 = V_11;
		ByteU5BU5D_t1_109* L_106 = V_11;
		NullCheck(L_106);
		NullCheck(L_104);
		VirtActionInvoker3< ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(24 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_104, L_105, 0, (((int32_t)((int32_t)(((Array_t *)L_106)->max_length)))));
		ByteU5BU5D_t1_109* L_107 = V_11;
		NullCheck(L_107);
		V_12 = (((int32_t)((int32_t)(((Array_t *)L_107)->max_length))));
		goto IL_0371;
	}

IL_0364:
	{
		MemoryStream_t1_433 * L_108 = ___stream;
		NullCheck(L_108);
		VirtActionInvoker1< uint8_t >::Invoke(25 /* System.Void System.IO.MemoryStream::WriteByte(System.Byte) */, L_108, 0);
		int32_t L_109 = V_12;
		V_12 = ((int32_t)((int32_t)L_109+(int32_t)1));
	}

IL_0371:
	{
		int32_t L_110 = V_12;
		if ((((int32_t)L_110) < ((int32_t)4)))
		{
			goto IL_0364;
		}
	}
	{
		goto IL_03f8;
	}

IL_037e:
	{
		MemoryStream_t1_433 * L_111 = ___stream;
		BitConverterEx_t8_61 * L_112 = V_0;
		uint32_t L_113 = V_5;
		uint32_t L_114 = V_10;
		NullCheck(L_112);
		ByteU5BU5D_t1_109* L_115 = BitConverterEx_GetBytes_m8_372(L_112, ((int32_t)((int32_t)L_113+(int32_t)L_114)), /*hidden argument*/NULL);
		NullCheck(L_111);
		VirtActionInvoker3< ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(24 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_111, L_115, 0, 4);
		MemoryStream_t1_433 * L_116 = ___stream;
		NullCheck(L_116);
		int64_t L_117 = (int64_t)VirtFuncInvoker0< int64_t >::Invoke(10 /* System.Int64 System.IO.MemoryStream::get_Position() */, L_116);
		V_13 = L_117;
		MemoryStream_t1_433 * L_118 = ___stream;
		int64_t L_119 = V_6;
		NullCheck(L_118);
		VirtFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(22 /* System.Int64 System.IO.MemoryStream::Seek(System.Int64,System.IO.SeekOrigin) */, L_118, L_119, 0);
		V_14 = 0;
		goto IL_03bc;
	}

IL_03ab:
	{
		MemoryStream_t1_433 * L_120 = ___stream;
		NullCheck(L_120);
		VirtActionInvoker1< uint8_t >::Invoke(25 /* System.Void System.IO.MemoryStream::WriteByte(System.Byte) */, L_120, ((int32_t)255));
		int32_t L_121 = V_14;
		V_14 = ((int32_t)((int32_t)L_121+(int32_t)1));
	}

IL_03bc:
	{
		int32_t L_122 = V_14;
		uint32_t L_123 = V_10;
		if ((((int64_t)(((int64_t)((int64_t)L_122)))) < ((int64_t)(((int64_t)((uint64_t)L_123))))))
		{
			goto IL_03ab;
		}
	}
	{
		MemoryStream_t1_433 * L_124 = ___stream;
		ByteU5BU5D_t1_109* L_125 = V_11;
		ByteU5BU5D_t1_109* L_126 = V_11;
		NullCheck(L_126);
		NullCheck(L_124);
		VirtActionInvoker3< ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(24 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_124, L_125, 0, (((int32_t)((int32_t)(((Array_t *)L_126)->max_length)))));
		MemoryStream_t1_433 * L_127 = ___stream;
		int64_t L_128 = V_13;
		NullCheck(L_127);
		VirtFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(22 /* System.Int64 System.IO.MemoryStream::Seek(System.Int64,System.IO.SeekOrigin) */, L_127, L_128, 0);
		uint32_t L_129 = V_5;
		uint32_t L_130 = V_10;
		ByteU5BU5D_t1_109* L_131 = V_11;
		NullCheck(L_131);
		V_5 = ((int32_t)((int32_t)L_129+(int32_t)((int32_t)((int32_t)L_130+(int32_t)(((int32_t)((int32_t)(((Array_t *)L_131)->max_length))))))));
		int64_t L_132 = V_6;
		uint32_t L_133 = V_10;
		ByteU5BU5D_t1_109* L_134 = V_11;
		NullCheck(L_134);
		V_6 = ((int64_t)((int64_t)L_132+(int64_t)((int64_t)((int64_t)(((int64_t)((uint64_t)L_133)))+(int64_t)(((int64_t)((int64_t)(((int32_t)((int32_t)(((Array_t *)L_134)->max_length)))))))))));
	}

IL_03f8:
	{
		Queue_1_t3_249 * L_135 = V_1;
		NullCheck(L_135);
		int32_t L_136 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 System.Collections.Generic.Queue`1<ExifLibrary.ExifProperty>::get_Count() */, L_135);
		if (L_136)
		{
			goto IL_00d4;
		}
	}
	{
		int32_t L_137 = ___ifdtype;
		if ((!(((uint32_t)L_137) == ((uint32_t)((int32_t)100000)))))
		{
			goto IL_041a;
		}
	}
	{
		MemoryStream_t1_433 * L_138 = ___stream;
		NullCheck(L_138);
		int64_t L_139 = (int64_t)VirtFuncInvoker0< int64_t >::Invoke(10 /* System.Int64 System.IO.MemoryStream::get_Position() */, L_138);
		__this->___firstIFDFieldOffset_6 = L_139;
	}

IL_041a:
	{
		MemoryStream_t1_433 * L_140 = ___stream;
		NullCheck(L_140);
		VirtActionInvoker3< ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(24 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_140, ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 4)), 0, 4);
		MemoryStream_t1_433 * L_141 = ___stream;
		int64_t L_142 = V_6;
		NullCheck(L_141);
		VirtFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(22 /* System.Int64 System.IO.MemoryStream::Seek(System.Int64,System.IO.SeekOrigin) */, L_141, L_142, 0);
		int32_t L_143 = ___ifdtype;
		if ((!(((uint32_t)L_143) == ((uint32_t)((int32_t)500000)))))
		{
			goto IL_04ad;
		}
	}
	{
		JPEGFile_t8_111 * L_144 = ExifFile_get_Thumbnail_m8_483(__this, /*hidden argument*/NULL);
		if (!L_144)
		{
			goto IL_049f;
		}
	}
	{
		MemoryStream_t1_433 * L_145 = (MemoryStream_t1_433 *)il2cpp_codegen_object_new (MemoryStream_t1_433_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m1_5044(L_145, /*hidden argument*/NULL);
		V_15 = L_145;
		JPEGFile_t8_111 * L_146 = ExifFile_get_Thumbnail_m8_483(__this, /*hidden argument*/NULL);
		MemoryStream_t1_433 * L_147 = V_15;
		NullCheck(L_146);
		JPEGFile_Save_m8_646(L_146, L_147, /*hidden argument*/NULL);
		MemoryStream_t1_433 * L_148 = V_15;
		NullCheck(L_148);
		VirtActionInvoker0::Invoke(13 /* System.Void System.IO.Stream::Close() */, L_148);
		MemoryStream_t1_433 * L_149 = V_15;
		NullCheck(L_149);
		ByteU5BU5D_t1_109* L_150 = (ByteU5BU5D_t1_109*)VirtFuncInvoker0< ByteU5BU5D_t1_109* >::Invoke(33 /* System.Byte[] System.IO.MemoryStream::ToArray() */, L_149);
		V_16 = L_150;
		MemoryStream_t1_433 * L_151 = ___stream;
		NullCheck(L_151);
		int64_t L_152 = (int64_t)VirtFuncInvoker0< int64_t >::Invoke(10 /* System.Int64 System.IO.MemoryStream::get_Position() */, L_151);
		int64_t L_153 = ___tiffoffset;
		__this->___thumbOffsetValue_9 = (((int32_t)((uint32_t)((int64_t)((int64_t)L_152-(int64_t)L_153)))));
		ByteU5BU5D_t1_109* L_154 = V_16;
		NullCheck(L_154);
		__this->___thumbSizeValue_10 = (((int32_t)((int32_t)(((Array_t *)L_154)->max_length))));
		MemoryStream_t1_433 * L_155 = ___stream;
		ByteU5BU5D_t1_109* L_156 = V_16;
		ByteU5BU5D_t1_109* L_157 = V_16;
		NullCheck(L_157);
		NullCheck(L_155);
		VirtActionInvoker3< ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(24 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_155, L_156, 0, (((int32_t)((int32_t)(((Array_t *)L_157)->max_length)))));
		MemoryStream_t1_433 * L_158 = V_15;
		NullCheck(L_158);
		VirtActionInvoker0::Invoke(4 /* System.Void System.IO.Stream::Dispose() */, L_158);
		goto IL_04ad;
	}

IL_049f:
	{
		__this->___thumbOffsetValue_9 = 0;
		__this->___thumbSizeValue_10 = 0;
	}

IL_04ad:
	{
		return;
	}
}
// ExifLibrary.ExifFile ExifLibrary.ExifFile::Read(System.String)
extern TypeInfo* ExifFile_t8_110_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t1_1904_il2cpp_TypeInfo_var;
extern TypeInfo* JPEGFile_t8_111_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_15043_MethodInfo_var;
extern "C" ExifFile_t8_110 * ExifFile_Read_m8_492 (Object_t * __this /* static, unused */, String_t* ___filename, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExifFile_t8_110_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1977);
		Dictionary_2_t1_1904_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1985);
		JPEGFile_t8_111_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1982);
		Dictionary_2__ctor_m1_15043_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484138);
		s_Il2CppMethodIntialized = true;
	}
	ExifFile_t8_110 * V_0 = {0};
	{
		ExifFile_t8_110 * L_0 = (ExifFile_t8_110 *)il2cpp_codegen_object_new (ExifFile_t8_110_il2cpp_TypeInfo_var);
		ExifFile__ctor_m8_478(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		ExifFile_t8_110 * L_1 = V_0;
		Dictionary_2_t1_1904 * L_2 = (Dictionary_2_t1_1904 *)il2cpp_codegen_object_new (Dictionary_2_t1_1904_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_15043(L_2, /*hidden argument*/Dictionary_2__ctor_m1_15043_MethodInfo_var);
		NullCheck(L_1);
		ExifFile_set_Properties_m8_480(L_1, L_2, /*hidden argument*/NULL);
		ExifFile_t8_110 * L_3 = V_0;
		String_t* L_4 = ___filename;
		JPEGFile_t8_111 * L_5 = (JPEGFile_t8_111 *)il2cpp_codegen_object_new (JPEGFile_t8_111_il2cpp_TypeInfo_var);
		JPEGFile__ctor_m8_641(L_5, L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		L_3->___file_0 = L_5;
		ExifFile_t8_110 * L_6 = V_0;
		NullCheck(L_6);
		ExifFile_ReadAPP1_m8_489(L_6, /*hidden argument*/NULL);
		ExifFile_t8_110 * L_7 = V_0;
		NullCheck(L_7);
		L_7->___makerNoteProcessed_11 = 0;
		ExifFile_t8_110 * L_8 = V_0;
		return L_8;
	}
}
// ExifLibrary.ExifFile ExifLibrary.ExifFile::Read(System.IO.Stream)
extern TypeInfo* ExifFile_t8_110_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t1_1904_il2cpp_TypeInfo_var;
extern TypeInfo* JPEGFile_t8_111_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_15043_MethodInfo_var;
extern "C" ExifFile_t8_110 * ExifFile_Read_m8_493 (Object_t * __this /* static, unused */, Stream_t1_405 * ___fileStream, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExifFile_t8_110_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1977);
		Dictionary_2_t1_1904_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1985);
		JPEGFile_t8_111_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1982);
		Dictionary_2__ctor_m1_15043_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484138);
		s_Il2CppMethodIntialized = true;
	}
	ExifFile_t8_110 * V_0 = {0};
	{
		ExifFile_t8_110 * L_0 = (ExifFile_t8_110 *)il2cpp_codegen_object_new (ExifFile_t8_110_il2cpp_TypeInfo_var);
		ExifFile__ctor_m8_478(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		ExifFile_t8_110 * L_1 = V_0;
		Dictionary_2_t1_1904 * L_2 = (Dictionary_2_t1_1904 *)il2cpp_codegen_object_new (Dictionary_2_t1_1904_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_15043(L_2, /*hidden argument*/Dictionary_2__ctor_m1_15043_MethodInfo_var);
		NullCheck(L_1);
		ExifFile_set_Properties_m8_480(L_1, L_2, /*hidden argument*/NULL);
		ExifFile_t8_110 * L_3 = V_0;
		JPEGFile_t8_111 * L_4 = (JPEGFile_t8_111 *)il2cpp_codegen_object_new (JPEGFile_t8_111_il2cpp_TypeInfo_var);
		JPEGFile__ctor_m8_639(L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		L_3->___file_0 = L_4;
		ExifFile_t8_110 * L_5 = V_0;
		NullCheck(L_5);
		JPEGFile_t8_111 * L_6 = (L_5->___file_0);
		Stream_t1_405 * L_7 = ___fileStream;
		NullCheck(L_6);
		bool L_8 = JPEGFile_Read_m8_648(L_6, L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0038;
		}
	}
	{
		ExifFile_t8_110 * L_9 = V_0;
		NullCheck(L_9);
		L_9->___file_0 = (JPEGFile_t8_111 *)NULL;
		V_0 = (ExifFile_t8_110 *)NULL;
		return (ExifFile_t8_110 *)NULL;
	}

IL_0038:
	{
		ExifFile_t8_110 * L_10 = V_0;
		NullCheck(L_10);
		ExifFile_ReadAPP1_m8_489(L_10, /*hidden argument*/NULL);
		ExifFile_t8_110 * L_11 = V_0;
		NullCheck(L_11);
		L_11->___makerNoteProcessed_11 = 0;
		ExifFile_t8_110 * L_12 = V_0;
		return L_12;
	}
}
// System.Boolean ExifLibrary.ExifFile::<ReadAPP1>m__2(ExifLibrary.JPEGSection)
extern TypeInfo* Encoding_t1_406_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5222;
extern "C" bool ExifFile_U3CReadAPP1U3Em__2_m8_494 (Object_t * __this /* static, unused */, JPEGSection_t8_112 * ___a, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Encoding_t1_406_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5222 = il2cpp_codegen_string_literal_from_index(5222);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		JPEGSection_t8_112 * L_0 = ___a;
		NullCheck(L_0);
		uint8_t L_1 = JPEGSection_get_Marker_m8_652(L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)225)))))
		{
			goto IL_002e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t1_406_il2cpp_TypeInfo_var);
		Encoding_t1_406 * L_2 = Encoding_get_ASCII_m1_12357(NULL /*static, unused*/, /*hidden argument*/NULL);
		JPEGSection_t8_112 * L_3 = ___a;
		NullCheck(L_3);
		ByteU5BU5D_t1_109* L_4 = JPEGSection_get_Header_m8_654(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_5 = (String_t*)VirtFuncInvoker3< String_t*, ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(26 /* System.String System.Text.Encoding::GetString(System.Byte[],System.Int32,System.Int32) */, L_2, L_4, 0, 6);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_6 = String_op_Equality_m1_601(NULL /*static, unused*/, L_5, _stringLiteral5222, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_002f;
	}

IL_002e:
	{
		G_B3_0 = 0;
	}

IL_002f:
	{
		return G_B3_0;
	}
}
// System.Boolean ExifLibrary.ExifFile::<ReadAPP1>m__3(ExifLibrary.JPEGSection)
extern "C" bool ExifFile_U3CReadAPP1U3Em__3_m8_495 (Object_t * __this /* static, unused */, JPEGSection_t8_112 * ___a, const MethodInfo* method)
{
	{
		JPEGSection_t8_112 * L_0 = ___a;
		NullCheck(L_0);
		uint8_t L_1 = JPEGSection_get_Marker_m8_652(L_0, /*hidden argument*/NULL);
		return ((((int32_t)L_1) == ((int32_t)((int32_t)224)))? 1 : 0);
	}
}
// System.Void ExifLibrary.ExifInterOperability::.ctor(System.UInt16,System.UInt16,System.UInt32,System.Byte[])
extern "C" void ExifInterOperability__ctor_m8_496 (ExifInterOperability_t8_113 * __this, uint16_t ___tagid, uint16_t ___typeid, uint32_t ___count, ByteU5BU5D_t1_109* ___data, const MethodInfo* method)
{
	{
		uint16_t L_0 = ___tagid;
		__this->___mTagID_0 = L_0;
		uint16_t L_1 = ___typeid;
		__this->___mTypeID_1 = L_1;
		uint32_t L_2 = ___count;
		__this->___mCount_2 = L_2;
		ByteU5BU5D_t1_109* L_3 = ___data;
		__this->___mData_3 = L_3;
		return;
	}
}
// System.UInt16 ExifLibrary.ExifInterOperability::get_TagID()
extern "C" uint16_t ExifInterOperability_get_TagID_m8_497 (ExifInterOperability_t8_113 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = (__this->___mTagID_0);
		return L_0;
	}
}
// System.UInt16 ExifLibrary.ExifInterOperability::get_TypeID()
extern "C" uint16_t ExifInterOperability_get_TypeID_m8_498 (ExifInterOperability_t8_113 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = (__this->___mTypeID_1);
		return L_0;
	}
}
// System.UInt32 ExifLibrary.ExifInterOperability::get_Count()
extern "C" uint32_t ExifInterOperability_get_Count_m8_499 (ExifInterOperability_t8_113 * __this, const MethodInfo* method)
{
	{
		uint32_t L_0 = (__this->___mCount_2);
		return L_0;
	}
}
// System.Byte[] ExifLibrary.ExifInterOperability::get_Data()
extern "C" ByteU5BU5D_t1_109* ExifInterOperability_get_Data_m8_500 (ExifInterOperability_t8_113 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t1_109* L_0 = (__this->___mData_3);
		return L_0;
	}
}
// System.String ExifLibrary.ExifInterOperability::ToString()
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* UInt16_t1_14_il2cpp_TypeInfo_var;
extern TypeInfo* UInt32_t1_8_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5223;
extern "C" String_t* ExifInterOperability_ToString_m8_501 (ExifInterOperability_t8_113 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		UInt16_t1_14_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		UInt32_t1_8_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5223 = il2cpp_codegen_string_literal_from_index(5223);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_272* L_0 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 4));
		uint16_t L_1 = (__this->___mTagID_0);
		uint16_t L_2 = L_1;
		Object_t * L_3 = Box(UInt16_t1_14_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_3;
		ObjectU5BU5D_t1_272* L_4 = L_0;
		uint16_t L_5 = (__this->___mTypeID_1);
		uint16_t L_6 = L_5;
		Object_t * L_7 = Box(UInt16_t1_14_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1, sizeof(Object_t *))) = (Object_t *)L_7;
		ObjectU5BU5D_t1_272* L_8 = L_4;
		uint32_t L_9 = (__this->___mCount_2);
		uint32_t L_10 = L_9;
		Object_t * L_11 = Box(UInt32_t1_8_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 2, sizeof(Object_t *))) = (Object_t *)L_11;
		ObjectU5BU5D_t1_272* L_12 = L_8;
		ByteU5BU5D_t1_109* L_13 = (__this->___mData_3);
		NullCheck(L_13);
		int32_t L_14 = (((int32_t)((int32_t)(((Array_t *)L_13)->max_length))));
		Object_t * L_15 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 3, sizeof(Object_t *))) = (Object_t *)L_15;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Format_m1_551(NULL /*static, unused*/, _stringLiteral5223, L_12, /*hidden argument*/NULL);
		return L_16;
	}
}
// Conversion methods for marshalling of: ExifLibrary.ExifInterOperability
extern "C" void ExifInterOperability_t8_113_marshal(const ExifInterOperability_t8_113& unmarshaled, ExifInterOperability_t8_113_marshaled& marshaled)
{
	marshaled.___mTagID_0 = unmarshaled.___mTagID_0;
	marshaled.___mTypeID_1 = unmarshaled.___mTypeID_1;
	marshaled.___mCount_2 = unmarshaled.___mCount_2;
	marshaled.___mData_3 = il2cpp_codegen_marshal_array<uint8_t>((Il2CppCodeGenArray*)unmarshaled.___mData_3);
}
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern "C" void ExifInterOperability_t8_113_marshal_back(const ExifInterOperability_t8_113_marshaled& marshaled, ExifInterOperability_t8_113& unmarshaled)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		s_Il2CppMethodIntialized = true;
	}
	unmarshaled.___mTagID_0 = marshaled.___mTagID_0;
	unmarshaled.___mTypeID_1 = marshaled.___mTypeID_1;
	unmarshaled.___mCount_2 = marshaled.___mCount_2;
	unmarshaled.___mData_3 = (ByteU5BU5D_t1_109*)il2cpp_codegen_marshal_array_result(Byte_t1_11_il2cpp_TypeInfo_var, marshaled.___mData_3, 1);
}
// Conversion method for clean up from marshalling of: ExifLibrary.ExifInterOperability
extern "C" void ExifInterOperability_t8_113_marshal_cleanup(ExifInterOperability_t8_113_marshaled& marshaled)
{
}
// System.Void ExifLibrary.ExifProperty::.ctor(ExifLibrary.ExifTag)
extern "C" void ExifProperty__ctor_m8_502 (ExifProperty_t8_99 * __this, int32_t ___tag, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___tag;
		__this->___mTag_0 = L_0;
		int32_t L_1 = ___tag;
		int32_t L_2 = ExifTagFactory_GetTagIFD_m8_631(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		__this->___mIFD_1 = L_2;
		return;
	}
}
// ExifLibrary.ExifTag ExifLibrary.ExifProperty::get_Tag()
extern "C" int32_t ExifProperty_get_Tag_m8_503 (ExifProperty_t8_99 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___mTag_0);
		return L_0;
	}
}
// ExifLibrary.IFD ExifLibrary.ExifProperty::get_IFD()
extern "C" int32_t ExifProperty_get_IFD_m8_504 (ExifProperty_t8_99 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___mIFD_1);
		return L_0;
	}
}
// System.String ExifLibrary.ExifProperty::get_Name()
extern "C" String_t* ExifProperty_get_Name_m8_505 (ExifProperty_t8_99 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___mName_2);
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		String_t* L_1 = (__this->___mName_2);
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m1_571(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0027;
		}
	}

IL_001b:
	{
		int32_t L_3 = (__this->___mTag_0);
		String_t* L_4 = ExifTagFactory_GetTagName_m8_632(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_0027:
	{
		String_t* L_5 = (__this->___mName_2);
		return L_5;
	}
}
// System.Void ExifLibrary.ExifProperty::set_Name(System.String)
extern "C" void ExifProperty_set_Name_m8_506 (ExifProperty_t8_99 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___mName_2 = L_0;
		return;
	}
}
// System.Object ExifLibrary.ExifProperty::get_Value()
extern "C" Object_t * ExifProperty_get_Value_m8_507 (ExifProperty_t8_99 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(4 /* System.Object ExifLibrary.ExifProperty::get__Value() */, __this);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifProperty::set_Value(System.Object)
extern "C" void ExifProperty_set_Value_m8_508 (ExifProperty_t8_99 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		VirtActionInvoker1< Object_t * >::Invoke(5 /* System.Void ExifLibrary.ExifProperty::set__Value(System.Object) */, __this, L_0);
		return;
	}
}
// System.Void ExifLibrary.ExifByte::.ctor(ExifLibrary.ExifTag,System.Byte)
extern "C" void ExifByte__ctor_m8_509 (ExifByte_t8_114 * __this, int32_t ___tag, uint8_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		ExifProperty__ctor_m8_502(__this, L_0, /*hidden argument*/NULL);
		uint8_t L_1 = ___value;
		__this->___mValue_3 = L_1;
		return;
	}
}
// System.Object ExifLibrary.ExifByte::get__Value()
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern "C" Object_t * ExifByte_get__Value_m8_510 (ExifByte_t8_114 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		s_Il2CppMethodIntialized = true;
	}
	{
		uint8_t L_0 = ExifByte_get_Value_m8_512(__this, /*hidden argument*/NULL);
		uint8_t L_1 = L_0;
		Object_t * L_2 = Box(Byte_t1_11_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void ExifLibrary.ExifByte::set__Value(System.Object)
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern "C" void ExifByte_set__Value_m8_511 (ExifByte_t8_114 * __this, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___value;
		ExifByte_set_Value_m8_513(__this, ((*(uint8_t*)((uint8_t*)UnBox (L_0, Byte_t1_11_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		return;
	}
}
// System.Byte ExifLibrary.ExifByte::get_Value()
extern "C" uint8_t ExifByte_get_Value_m8_512 (ExifByte_t8_114 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = (__this->___mValue_3);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifByte::set_Value(System.Byte)
extern "C" void ExifByte_set_Value_m8_513 (ExifByte_t8_114 * __this, uint8_t ___value, const MethodInfo* method)
{
	{
		uint8_t L_0 = ___value;
		__this->___mValue_3 = L_0;
		return;
	}
}
// System.String ExifLibrary.ExifByte::ToString()
extern "C" String_t* ExifByte_ToString_m8_514 (ExifByte_t8_114 * __this, const MethodInfo* method)
{
	{
		uint8_t* L_0 = &(__this->___mValue_3);
		String_t* L_1 = Byte_ToString_m1_241(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// ExifLibrary.ExifInterOperability ExifLibrary.ExifByte::get_Interoperability()
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern "C" ExifInterOperability_t8_113  ExifByte_get_Interoperability_m8_515 (ExifByte_t8_114 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (((ExifProperty_t8_99 *)__this)->___mTag_0);
		uint16_t L_1 = ExifTagFactory_GetTagID_m8_630(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_2 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint8_t L_3 = (__this->___mValue_3);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_2, 0, sizeof(uint8_t))) = (uint8_t)L_3;
		ExifInterOperability_t8_113  L_4 = {0};
		ExifInterOperability__ctor_m8_496(&L_4, L_1, 1, 1, L_2, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Byte ExifLibrary.ExifByte::op_Implicit(ExifLibrary.ExifByte)
extern "C" uint8_t ExifByte_op_Implicit_m8_516 (Object_t * __this /* static, unused */, ExifByte_t8_114 * ___obj, const MethodInfo* method)
{
	{
		ExifByte_t8_114 * L_0 = ___obj;
		NullCheck(L_0);
		uint8_t L_1 = (L_0->___mValue_3);
		return L_1;
	}
}
// System.Void ExifLibrary.ExifByteArray::.ctor(ExifLibrary.ExifTag,System.Byte[])
extern "C" void ExifByteArray__ctor_m8_517 (ExifByteArray_t8_115 * __this, int32_t ___tag, ByteU5BU5D_t1_109* ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		ExifProperty__ctor_m8_502(__this, L_0, /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_1 = ___value;
		__this->___mValue_3 = L_1;
		return;
	}
}
// System.Object ExifLibrary.ExifByteArray::get__Value()
extern "C" Object_t * ExifByteArray_get__Value_m8_518 (ExifByteArray_t8_115 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t1_109* L_0 = ExifByteArray_get_Value_m8_520(__this, /*hidden argument*/NULL);
		return (Object_t *)L_0;
	}
}
// System.Void ExifLibrary.ExifByteArray::set__Value(System.Object)
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern "C" void ExifByteArray_set__Value_m8_519 (ExifByteArray_t8_115 * __this, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___value;
		ExifByteArray_set_Value_m8_521(__this, ((ByteU5BU5D_t1_109*)Castclass(L_0, ByteU5BU5D_t1_109_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return;
	}
}
// System.Byte[] ExifLibrary.ExifByteArray::get_Value()
extern "C" ByteU5BU5D_t1_109* ExifByteArray_get_Value_m8_520 (ExifByteArray_t8_115 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t1_109* L_0 = (__this->___mValue_3);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifByteArray::set_Value(System.Byte[])
extern "C" void ExifByteArray_set_Value_m8_521 (ExifByteArray_t8_115 * __this, ByteU5BU5D_t1_109* ___value, const MethodInfo* method)
{
	{
		ByteU5BU5D_t1_109* L_0 = ___value;
		__this->___mValue_3 = L_0;
		return;
	}
}
// System.String ExifLibrary.ExifByteArray::ToString()
extern TypeInfo* StringBuilder_t1_247_il2cpp_TypeInfo_var;
extern "C" String_t* ExifByteArray_ToString_m8_522 (ExifByteArray_t8_115 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringBuilder_t1_247_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t1_247 * V_0 = {0};
	uint8_t V_1 = 0x0;
	ByteU5BU5D_t1_109* V_2 = {0};
	int32_t V_3 = 0;
	{
		StringBuilder_t1_247 * L_0 = (StringBuilder_t1_247 *)il2cpp_codegen_object_new (StringBuilder_t1_247_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1_12414(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		StringBuilder_t1_247 * L_1 = V_0;
		NullCheck(L_1);
		StringBuilder_Append_m1_12452(L_1, ((int32_t)91), /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_2 = (__this->___mValue_3);
		V_2 = L_2;
		V_3 = 0;
		goto IL_0036;
	}

IL_001d:
	{
		ByteU5BU5D_t1_109* L_3 = V_2;
		int32_t L_4 = V_3;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		V_1 = (*(uint8_t*)(uint8_t*)SZArrayLdElema(L_3, L_5, sizeof(uint8_t)));
		StringBuilder_t1_247 * L_6 = V_0;
		uint8_t L_7 = V_1;
		NullCheck(L_6);
		StringBuilder_Append_m1_12440(L_6, L_7, /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_8 = V_0;
		NullCheck(L_8);
		StringBuilder_Append_m1_12452(L_8, ((int32_t)32), /*hidden argument*/NULL);
		int32_t L_9 = V_3;
		V_3 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0036:
	{
		int32_t L_10 = V_3;
		ByteU5BU5D_t1_109* L_11 = V_2;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_11)->max_length)))))))
		{
			goto IL_001d;
		}
	}
	{
		StringBuilder_t1_247 * L_12 = V_0;
		StringBuilder_t1_247 * L_13 = V_0;
		NullCheck(L_13);
		int32_t L_14 = StringBuilder_get_Length_m1_12424(L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		StringBuilder_Remove_m1_12432(L_12, ((int32_t)((int32_t)L_14-(int32_t)1)), 1, /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_15 = V_0;
		NullCheck(L_15);
		StringBuilder_Append_m1_12452(L_15, ((int32_t)93), /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_16 = V_0;
		NullCheck(L_16);
		String_t* L_17 = StringBuilder_ToString_m1_12428(L_16, /*hidden argument*/NULL);
		return L_17;
	}
}
// ExifLibrary.ExifInterOperability ExifLibrary.ExifByteArray::get_Interoperability()
extern "C" ExifInterOperability_t8_113  ExifByteArray_get_Interoperability_m8_523 (ExifByteArray_t8_115 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (((ExifProperty_t8_99 *)__this)->___mTag_0);
		uint16_t L_1 = ExifTagFactory_GetTagID_m8_630(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_2 = (__this->___mValue_3);
		NullCheck(L_2);
		ByteU5BU5D_t1_109* L_3 = (__this->___mValue_3);
		ExifInterOperability_t8_113  L_4 = {0};
		ExifInterOperability__ctor_m8_496(&L_4, L_1, 1, (((int32_t)((int32_t)(((Array_t *)L_2)->max_length)))), L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Byte[] ExifLibrary.ExifByteArray::op_Implicit(ExifLibrary.ExifByteArray)
extern "C" ByteU5BU5D_t1_109* ExifByteArray_op_Implicit_m8_524 (Object_t * __this /* static, unused */, ExifByteArray_t8_115 * ___obj, const MethodInfo* method)
{
	{
		ExifByteArray_t8_115 * L_0 = ___obj;
		NullCheck(L_0);
		ByteU5BU5D_t1_109* L_1 = (L_0->___mValue_3);
		return L_1;
	}
}
// System.Void ExifLibrary.ExifAscii::.ctor(ExifLibrary.ExifTag,System.String)
extern "C" void ExifAscii__ctor_m8_525 (ExifAscii_t8_116 * __this, int32_t ___tag, String_t* ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		ExifProperty__ctor_m8_502(__this, L_0, /*hidden argument*/NULL);
		String_t* L_1 = ___value;
		__this->___mValue_3 = L_1;
		return;
	}
}
// System.Object ExifLibrary.ExifAscii::get__Value()
extern "C" Object_t * ExifAscii_get__Value_m8_526 (ExifAscii_t8_116 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = ExifAscii_get_Value_m8_528(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifAscii::set__Value(System.Object)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void ExifAscii_set__Value_m8_527 (ExifAscii_t8_116 * __this, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___value;
		ExifAscii_set_Value_m8_529(__this, ((String_t*)CastclassSealed(L_0, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return;
	}
}
// System.String ExifLibrary.ExifAscii::get_Value()
extern "C" String_t* ExifAscii_get_Value_m8_528 (ExifAscii_t8_116 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___mValue_3);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifAscii::set_Value(System.String)
extern "C" void ExifAscii_set_Value_m8_529 (ExifAscii_t8_116 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___mValue_3 = L_0;
		return;
	}
}
// System.String ExifLibrary.ExifAscii::ToString()
extern "C" String_t* ExifAscii_ToString_m8_530 (ExifAscii_t8_116 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___mValue_3);
		return L_0;
	}
}
// ExifLibrary.ExifInterOperability ExifLibrary.ExifAscii::get_Interoperability()
extern "C" ExifInterOperability_t8_113  ExifAscii_get_Interoperability_m8_531 (ExifAscii_t8_116 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (((ExifProperty_t8_99 *)__this)->___mTag_0);
		uint16_t L_1 = ExifTagFactory_GetTagID_m8_630(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		String_t* L_2 = (__this->___mValue_3);
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m1_571(L_2, /*hidden argument*/NULL);
		String_t* L_4 = (__this->___mValue_3);
		ByteU5BU5D_t1_109* L_5 = ExifBitConverter_GetBytes_m8_395(NULL /*static, unused*/, L_4, 1, /*hidden argument*/NULL);
		ExifInterOperability_t8_113  L_6 = {0};
		ExifInterOperability__ctor_m8_496(&L_6, L_1, 2, ((int32_t)((int32_t)L_3+(int32_t)1)), L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.String ExifLibrary.ExifAscii::op_Implicit(ExifLibrary.ExifAscii)
extern "C" String_t* ExifAscii_op_Implicit_m8_532 (Object_t * __this /* static, unused */, ExifAscii_t8_116 * ___obj, const MethodInfo* method)
{
	{
		ExifAscii_t8_116 * L_0 = ___obj;
		NullCheck(L_0);
		String_t* L_1 = (L_0->___mValue_3);
		return L_1;
	}
}
// System.Void ExifLibrary.ExifUShort::.ctor(ExifLibrary.ExifTag,System.UInt16)
extern "C" void ExifUShort__ctor_m8_533 (ExifUShort_t8_117 * __this, int32_t ___tag, uint16_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		ExifProperty__ctor_m8_502(__this, L_0, /*hidden argument*/NULL);
		uint16_t L_1 = ___value;
		__this->___mValue_3 = L_1;
		return;
	}
}
// System.Object ExifLibrary.ExifUShort::get__Value()
extern TypeInfo* UInt16_t1_14_il2cpp_TypeInfo_var;
extern "C" Object_t * ExifUShort_get__Value_m8_534 (ExifUShort_t8_117 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UInt16_t1_14_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		s_Il2CppMethodIntialized = true;
	}
	{
		uint16_t L_0 = ExifUShort_get_Value_m8_536(__this, /*hidden argument*/NULL);
		uint16_t L_1 = L_0;
		Object_t * L_2 = Box(UInt16_t1_14_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void ExifLibrary.ExifUShort::set__Value(System.Object)
extern TypeInfo* UInt16_t1_14_il2cpp_TypeInfo_var;
extern "C" void ExifUShort_set__Value_m8_535 (ExifUShort_t8_117 * __this, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UInt16_t1_14_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___value;
		ExifUShort_set_Value_m8_537(__this, ((*(uint16_t*)((uint16_t*)UnBox (L_0, UInt16_t1_14_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		return;
	}
}
// System.UInt16 ExifLibrary.ExifUShort::get_Value()
extern "C" uint16_t ExifUShort_get_Value_m8_536 (ExifUShort_t8_117 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = (__this->___mValue_3);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifUShort::set_Value(System.UInt16)
extern "C" void ExifUShort_set_Value_m8_537 (ExifUShort_t8_117 * __this, uint16_t ___value, const MethodInfo* method)
{
	{
		uint16_t L_0 = ___value;
		__this->___mValue_3 = L_0;
		return;
	}
}
// System.String ExifLibrary.ExifUShort::ToString()
extern "C" String_t* ExifUShort_ToString_m8_538 (ExifUShort_t8_117 * __this, const MethodInfo* method)
{
	{
		uint16_t* L_0 = &(__this->___mValue_3);
		String_t* L_1 = UInt16_ToString_m1_336(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// ExifLibrary.ExifInterOperability ExifLibrary.ExifUShort::get_Interoperability()
extern "C" ExifInterOperability_t8_113  ExifUShort_get_Interoperability_m8_539 (ExifUShort_t8_117 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (((ExifProperty_t8_99 *)__this)->___mTag_0);
		uint16_t L_1 = ExifTagFactory_GetTagID_m8_630(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		uint16_t L_2 = (__this->___mValue_3);
		ByteU5BU5D_t1_109* L_3 = BitConverterEx_GetBytes_m8_354(NULL /*static, unused*/, L_2, 0, 0, /*hidden argument*/NULL);
		ExifInterOperability_t8_113  L_4 = {0};
		ExifInterOperability__ctor_m8_496(&L_4, L_1, 3, 1, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.UInt16 ExifLibrary.ExifUShort::op_Implicit(ExifLibrary.ExifUShort)
extern "C" uint16_t ExifUShort_op_Implicit_m8_540 (Object_t * __this /* static, unused */, ExifUShort_t8_117 * ___obj, const MethodInfo* method)
{
	{
		ExifUShort_t8_117 * L_0 = ___obj;
		NullCheck(L_0);
		uint16_t L_1 = (L_0->___mValue_3);
		return L_1;
	}
}
// System.Void ExifLibrary.ExifUShortArray::.ctor(ExifLibrary.ExifTag,System.UInt16[])
extern "C" void ExifUShortArray__ctor_m8_541 (ExifUShortArray_t8_103 * __this, int32_t ___tag, UInt16U5BU5D_t1_1231* ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		ExifProperty__ctor_m8_502(__this, L_0, /*hidden argument*/NULL);
		UInt16U5BU5D_t1_1231* L_1 = ___value;
		__this->___mValue_3 = L_1;
		return;
	}
}
// System.Object ExifLibrary.ExifUShortArray::get__Value()
extern "C" Object_t * ExifUShortArray_get__Value_m8_542 (ExifUShortArray_t8_103 * __this, const MethodInfo* method)
{
	{
		UInt16U5BU5D_t1_1231* L_0 = ExifUShortArray_get_Value_m8_544(__this, /*hidden argument*/NULL);
		return (Object_t *)L_0;
	}
}
// System.Void ExifLibrary.ExifUShortArray::set__Value(System.Object)
extern TypeInfo* UInt16U5BU5D_t1_1231_il2cpp_TypeInfo_var;
extern "C" void ExifUShortArray_set__Value_m8_543 (ExifUShortArray_t8_103 * __this, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UInt16U5BU5D_t1_1231_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(70);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___value;
		ExifUShortArray_set_Value_m8_545(__this, ((UInt16U5BU5D_t1_1231*)Castclass(L_0, UInt16U5BU5D_t1_1231_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return;
	}
}
// System.UInt16[] ExifLibrary.ExifUShortArray::get_Value()
extern "C" UInt16U5BU5D_t1_1231* ExifUShortArray_get_Value_m8_544 (ExifUShortArray_t8_103 * __this, const MethodInfo* method)
{
	{
		UInt16U5BU5D_t1_1231* L_0 = (__this->___mValue_3);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifUShortArray::set_Value(System.UInt16[])
extern "C" void ExifUShortArray_set_Value_m8_545 (ExifUShortArray_t8_103 * __this, UInt16U5BU5D_t1_1231* ___value, const MethodInfo* method)
{
	{
		UInt16U5BU5D_t1_1231* L_0 = ___value;
		__this->___mValue_3 = L_0;
		return;
	}
}
// System.String ExifLibrary.ExifUShortArray::ToString()
extern TypeInfo* StringBuilder_t1_247_il2cpp_TypeInfo_var;
extern "C" String_t* ExifUShortArray_ToString_m8_546 (ExifUShortArray_t8_103 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringBuilder_t1_247_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t1_247 * V_0 = {0};
	uint16_t V_1 = 0;
	UInt16U5BU5D_t1_1231* V_2 = {0};
	int32_t V_3 = 0;
	{
		StringBuilder_t1_247 * L_0 = (StringBuilder_t1_247 *)il2cpp_codegen_object_new (StringBuilder_t1_247_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1_12414(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		StringBuilder_t1_247 * L_1 = V_0;
		NullCheck(L_1);
		StringBuilder_Append_m1_12452(L_1, ((int32_t)91), /*hidden argument*/NULL);
		UInt16U5BU5D_t1_1231* L_2 = (__this->___mValue_3);
		V_2 = L_2;
		V_3 = 0;
		goto IL_0036;
	}

IL_001d:
	{
		UInt16U5BU5D_t1_1231* L_3 = V_2;
		int32_t L_4 = V_3;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		V_1 = (*(uint16_t*)(uint16_t*)SZArrayLdElema(L_3, L_5, sizeof(uint16_t)));
		StringBuilder_t1_247 * L_6 = V_0;
		uint16_t L_7 = V_1;
		NullCheck(L_6);
		StringBuilder_Append_m1_12449(L_6, L_7, /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_8 = V_0;
		NullCheck(L_8);
		StringBuilder_Append_m1_12452(L_8, ((int32_t)32), /*hidden argument*/NULL);
		int32_t L_9 = V_3;
		V_3 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0036:
	{
		int32_t L_10 = V_3;
		UInt16U5BU5D_t1_1231* L_11 = V_2;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_11)->max_length)))))))
		{
			goto IL_001d;
		}
	}
	{
		StringBuilder_t1_247 * L_12 = V_0;
		StringBuilder_t1_247 * L_13 = V_0;
		NullCheck(L_13);
		int32_t L_14 = StringBuilder_get_Length_m1_12424(L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		StringBuilder_Remove_m1_12432(L_12, ((int32_t)((int32_t)L_14-(int32_t)1)), 1, /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_15 = V_0;
		NullCheck(L_15);
		StringBuilder_Append_m1_12452(L_15, ((int32_t)93), /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_16 = V_0;
		NullCheck(L_16);
		String_t* L_17 = StringBuilder_ToString_m1_12428(L_16, /*hidden argument*/NULL);
		return L_17;
	}
}
// ExifLibrary.ExifInterOperability ExifLibrary.ExifUShortArray::get_Interoperability()
extern "C" ExifInterOperability_t8_113  ExifUShortArray_get_Interoperability_m8_547 (ExifUShortArray_t8_103 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (((ExifProperty_t8_99 *)__this)->___mTag_0);
		uint16_t L_1 = ExifTagFactory_GetTagID_m8_630(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		UInt16U5BU5D_t1_1231* L_2 = (__this->___mValue_3);
		NullCheck(L_2);
		UInt16U5BU5D_t1_1231* L_3 = (__this->___mValue_3);
		ByteU5BU5D_t1_109* L_4 = ExifBitConverter_GetBytes_m8_400(NULL /*static, unused*/, L_3, 0, /*hidden argument*/NULL);
		ExifInterOperability_t8_113  L_5 = {0};
		ExifInterOperability__ctor_m8_496(&L_5, L_1, 3, (((int32_t)((int32_t)(((Array_t *)L_2)->max_length)))), L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.UInt16[] ExifLibrary.ExifUShortArray::op_Implicit(ExifLibrary.ExifUShortArray)
extern "C" UInt16U5BU5D_t1_1231* ExifUShortArray_op_Implicit_m8_548 (Object_t * __this /* static, unused */, ExifUShortArray_t8_103 * ___obj, const MethodInfo* method)
{
	{
		ExifUShortArray_t8_103 * L_0 = ___obj;
		NullCheck(L_0);
		UInt16U5BU5D_t1_1231* L_1 = (L_0->___mValue_3);
		return L_1;
	}
}
// System.Void ExifLibrary.ExifUInt::.ctor(ExifLibrary.ExifTag,System.UInt32)
extern "C" void ExifUInt__ctor_m8_549 (ExifUInt_t8_118 * __this, int32_t ___tag, uint32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		ExifProperty__ctor_m8_502(__this, L_0, /*hidden argument*/NULL);
		uint32_t L_1 = ___value;
		__this->___mValue_3 = L_1;
		return;
	}
}
// System.Object ExifLibrary.ExifUInt::get__Value()
extern TypeInfo* UInt32_t1_8_il2cpp_TypeInfo_var;
extern "C" Object_t * ExifUInt_get__Value_m8_550 (ExifUInt_t8_118 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UInt32_t1_8_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		s_Il2CppMethodIntialized = true;
	}
	{
		uint32_t L_0 = ExifUInt_get_Value_m8_552(__this, /*hidden argument*/NULL);
		uint32_t L_1 = L_0;
		Object_t * L_2 = Box(UInt32_t1_8_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void ExifLibrary.ExifUInt::set__Value(System.Object)
extern TypeInfo* UInt32_t1_8_il2cpp_TypeInfo_var;
extern "C" void ExifUInt_set__Value_m8_551 (ExifUInt_t8_118 * __this, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UInt32_t1_8_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___value;
		ExifUInt_set_Value_m8_553(__this, ((*(uint32_t*)((uint32_t*)UnBox (L_0, UInt32_t1_8_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		return;
	}
}
// System.UInt32 ExifLibrary.ExifUInt::get_Value()
extern "C" uint32_t ExifUInt_get_Value_m8_552 (ExifUInt_t8_118 * __this, const MethodInfo* method)
{
	{
		uint32_t L_0 = (__this->___mValue_3);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifUInt::set_Value(System.UInt32)
extern "C" void ExifUInt_set_Value_m8_553 (ExifUInt_t8_118 * __this, uint32_t ___value, const MethodInfo* method)
{
	{
		uint32_t L_0 = ___value;
		__this->___mValue_3 = L_0;
		return;
	}
}
// System.String ExifLibrary.ExifUInt::ToString()
extern "C" String_t* ExifUInt_ToString_m8_554 (ExifUInt_t8_118 * __this, const MethodInfo* method)
{
	{
		uint32_t* L_0 = &(__this->___mValue_3);
		String_t* L_1 = UInt32_ToString_m1_176(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// ExifLibrary.ExifInterOperability ExifLibrary.ExifUInt::get_Interoperability()
extern "C" ExifInterOperability_t8_113  ExifUInt_get_Interoperability_m8_555 (ExifUInt_t8_118 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (((ExifProperty_t8_99 *)__this)->___mTag_0);
		uint16_t L_1 = ExifTagFactory_GetTagID_m8_630(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		uint32_t L_2 = (__this->___mValue_3);
		ByteU5BU5D_t1_109* L_3 = BitConverterEx_GetBytes_m8_355(NULL /*static, unused*/, L_2, 0, 0, /*hidden argument*/NULL);
		ExifInterOperability_t8_113  L_4 = {0};
		ExifInterOperability__ctor_m8_496(&L_4, L_1, 4, 1, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.UInt32 ExifLibrary.ExifUInt::op_Implicit(ExifLibrary.ExifUInt)
extern "C" uint32_t ExifUInt_op_Implicit_m8_556 (Object_t * __this /* static, unused */, ExifUInt_t8_118 * ___obj, const MethodInfo* method)
{
	{
		ExifUInt_t8_118 * L_0 = ___obj;
		NullCheck(L_0);
		uint32_t L_1 = (L_0->___mValue_3);
		return L_1;
	}
}
// System.Void ExifLibrary.ExifUIntArray::.ctor(ExifLibrary.ExifTag,System.UInt32[])
extern "C" void ExifUIntArray__ctor_m8_557 (ExifUIntArray_t8_119 * __this, int32_t ___tag, UInt32U5BU5D_t1_142* ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		ExifProperty__ctor_m8_502(__this, L_0, /*hidden argument*/NULL);
		UInt32U5BU5D_t1_142* L_1 = ___value;
		__this->___mValue_3 = L_1;
		return;
	}
}
// System.Object ExifLibrary.ExifUIntArray::get__Value()
extern "C" Object_t * ExifUIntArray_get__Value_m8_558 (ExifUIntArray_t8_119 * __this, const MethodInfo* method)
{
	{
		UInt32U5BU5D_t1_142* L_0 = ExifUIntArray_get_Value_m8_560(__this, /*hidden argument*/NULL);
		return (Object_t *)L_0;
	}
}
// System.Void ExifLibrary.ExifUIntArray::set__Value(System.Object)
extern TypeInfo* UInt32U5BU5D_t1_142_il2cpp_TypeInfo_var;
extern "C" void ExifUIntArray_set__Value_m8_559 (ExifUIntArray_t8_119 * __this, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UInt32U5BU5D_t1_142_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(71);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___value;
		ExifUIntArray_set_Value_m8_561(__this, ((UInt32U5BU5D_t1_142*)Castclass(L_0, UInt32U5BU5D_t1_142_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return;
	}
}
// System.UInt32[] ExifLibrary.ExifUIntArray::get_Value()
extern "C" UInt32U5BU5D_t1_142* ExifUIntArray_get_Value_m8_560 (ExifUIntArray_t8_119 * __this, const MethodInfo* method)
{
	{
		UInt32U5BU5D_t1_142* L_0 = (__this->___mValue_3);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifUIntArray::set_Value(System.UInt32[])
extern "C" void ExifUIntArray_set_Value_m8_561 (ExifUIntArray_t8_119 * __this, UInt32U5BU5D_t1_142* ___value, const MethodInfo* method)
{
	{
		UInt32U5BU5D_t1_142* L_0 = ___value;
		__this->___mValue_3 = L_0;
		return;
	}
}
// System.String ExifLibrary.ExifUIntArray::ToString()
extern TypeInfo* StringBuilder_t1_247_il2cpp_TypeInfo_var;
extern "C" String_t* ExifUIntArray_ToString_m8_562 (ExifUIntArray_t8_119 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringBuilder_t1_247_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t1_247 * V_0 = {0};
	uint32_t V_1 = 0;
	UInt32U5BU5D_t1_142* V_2 = {0};
	int32_t V_3 = 0;
	{
		StringBuilder_t1_247 * L_0 = (StringBuilder_t1_247 *)il2cpp_codegen_object_new (StringBuilder_t1_247_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1_12414(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		StringBuilder_t1_247 * L_1 = V_0;
		NullCheck(L_1);
		StringBuilder_Append_m1_12452(L_1, ((int32_t)91), /*hidden argument*/NULL);
		UInt32U5BU5D_t1_142* L_2 = (__this->___mValue_3);
		V_2 = L_2;
		V_3 = 0;
		goto IL_0036;
	}

IL_001d:
	{
		UInt32U5BU5D_t1_142* L_3 = V_2;
		int32_t L_4 = V_3;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		V_1 = (*(uint32_t*)(uint32_t*)SZArrayLdElema(L_3, L_5, sizeof(uint32_t)));
		StringBuilder_t1_247 * L_6 = V_0;
		uint32_t L_7 = V_1;
		NullCheck(L_6);
		StringBuilder_Append_m1_12450(L_6, L_7, /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_8 = V_0;
		NullCheck(L_8);
		StringBuilder_Append_m1_12452(L_8, ((int32_t)32), /*hidden argument*/NULL);
		int32_t L_9 = V_3;
		V_3 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0036:
	{
		int32_t L_10 = V_3;
		UInt32U5BU5D_t1_142* L_11 = V_2;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_11)->max_length)))))))
		{
			goto IL_001d;
		}
	}
	{
		StringBuilder_t1_247 * L_12 = V_0;
		StringBuilder_t1_247 * L_13 = V_0;
		NullCheck(L_13);
		int32_t L_14 = StringBuilder_get_Length_m1_12424(L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		StringBuilder_Remove_m1_12432(L_12, ((int32_t)((int32_t)L_14-(int32_t)1)), 1, /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_15 = V_0;
		NullCheck(L_15);
		StringBuilder_Append_m1_12452(L_15, ((int32_t)93), /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_16 = V_0;
		NullCheck(L_16);
		String_t* L_17 = StringBuilder_ToString_m1_12428(L_16, /*hidden argument*/NULL);
		return L_17;
	}
}
// ExifLibrary.ExifInterOperability ExifLibrary.ExifUIntArray::get_Interoperability()
extern "C" ExifInterOperability_t8_113  ExifUIntArray_get_Interoperability_m8_563 (ExifUIntArray_t8_119 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (((ExifProperty_t8_99 *)__this)->___mTag_0);
		uint16_t L_1 = ExifTagFactory_GetTagID_m8_630(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		UInt32U5BU5D_t1_142* L_2 = (__this->___mValue_3);
		NullCheck(L_2);
		UInt32U5BU5D_t1_142* L_3 = (__this->___mValue_3);
		ByteU5BU5D_t1_109* L_4 = ExifBitConverter_GetBytes_m8_401(NULL /*static, unused*/, L_3, 0, /*hidden argument*/NULL);
		ExifInterOperability_t8_113  L_5 = {0};
		ExifInterOperability__ctor_m8_496(&L_5, L_1, 3, (((int32_t)((int32_t)(((Array_t *)L_2)->max_length)))), L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.UInt32[] ExifLibrary.ExifUIntArray::op_Implicit(ExifLibrary.ExifUIntArray)
extern "C" UInt32U5BU5D_t1_142* ExifUIntArray_op_Implicit_m8_564 (Object_t * __this /* static, unused */, ExifUIntArray_t8_119 * ___obj, const MethodInfo* method)
{
	{
		ExifUIntArray_t8_119 * L_0 = ___obj;
		NullCheck(L_0);
		UInt32U5BU5D_t1_142* L_1 = (L_0->___mValue_3);
		return L_1;
	}
}
// System.Void ExifLibrary.ExifURational::.ctor(ExifLibrary.ExifTag,System.UInt32,System.UInt32)
extern "C" void ExifURational__ctor_m8_565 (ExifURational_t8_120 * __this, int32_t ___tag, uint32_t ___numerator, uint32_t ___denominator, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		ExifProperty__ctor_m8_502(__this, L_0, /*hidden argument*/NULL);
		uint32_t L_1 = ___numerator;
		uint32_t L_2 = ___denominator;
		UFraction32_t8_121  L_3 = {0};
		UFraction32__ctor_m8_726(&L_3, L_1, L_2, /*hidden argument*/NULL);
		__this->___mValue_3 = L_3;
		return;
	}
}
// System.Void ExifLibrary.ExifURational::.ctor(ExifLibrary.ExifTag,ExifLibrary.MathEx/UFraction32)
extern "C" void ExifURational__ctor_m8_566 (ExifURational_t8_120 * __this, int32_t ___tag, UFraction32_t8_121  ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		ExifProperty__ctor_m8_502(__this, L_0, /*hidden argument*/NULL);
		UFraction32_t8_121  L_1 = ___value;
		__this->___mValue_3 = L_1;
		return;
	}
}
// System.Object ExifLibrary.ExifURational::get__Value()
extern TypeInfo* UFraction32_t8_121_il2cpp_TypeInfo_var;
extern "C" Object_t * ExifURational_get__Value_m8_567 (ExifURational_t8_120 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UFraction32_t8_121_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1972);
		s_Il2CppMethodIntialized = true;
	}
	{
		UFraction32_t8_121  L_0 = ExifURational_get_Value_m8_569(__this, /*hidden argument*/NULL);
		UFraction32_t8_121  L_1 = L_0;
		Object_t * L_2 = Box(UFraction32_t8_121_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void ExifLibrary.ExifURational::set__Value(System.Object)
extern TypeInfo* UFraction32_t8_121_il2cpp_TypeInfo_var;
extern "C" void ExifURational_set__Value_m8_568 (ExifURational_t8_120 * __this, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UFraction32_t8_121_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1972);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___value;
		ExifURational_set_Value_m8_570(__this, ((*(UFraction32_t8_121 *)((UFraction32_t8_121 *)UnBox (L_0, UFraction32_t8_121_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		return;
	}
}
// ExifLibrary.MathEx/UFraction32 ExifLibrary.ExifURational::get_Value()
extern "C" UFraction32_t8_121  ExifURational_get_Value_m8_569 (ExifURational_t8_120 * __this, const MethodInfo* method)
{
	{
		UFraction32_t8_121  L_0 = (__this->___mValue_3);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifURational::set_Value(ExifLibrary.MathEx/UFraction32)
extern "C" void ExifURational_set_Value_m8_570 (ExifURational_t8_120 * __this, UFraction32_t8_121  ___value, const MethodInfo* method)
{
	{
		UFraction32_t8_121  L_0 = ___value;
		__this->___mValue_3 = L_0;
		return;
	}
}
// System.String ExifLibrary.ExifURational::ToString()
extern "C" String_t* ExifURational_ToString_m8_571 (ExifURational_t8_120 * __this, const MethodInfo* method)
{
	{
		UFraction32_t8_121 * L_0 = &(__this->___mValue_3);
		String_t* L_1 = UFraction32_ToString_m8_749(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Single ExifLibrary.ExifURational::ToFloat()
extern TypeInfo* UFraction32_t8_121_il2cpp_TypeInfo_var;
extern "C" float ExifURational_ToFloat_m8_572 (ExifURational_t8_120 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UFraction32_t8_121_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1972);
		s_Il2CppMethodIntialized = true;
	}
	{
		UFraction32_t8_121  L_0 = (__this->___mValue_3);
		IL2CPP_RUNTIME_CLASS_INIT(UFraction32_t8_121_il2cpp_TypeInfo_var);
		float L_1 = UFraction32_op_Explicit_m8_783(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.UInt32[] ExifLibrary.ExifURational::ToArray()
extern TypeInfo* UInt32U5BU5D_t1_142_il2cpp_TypeInfo_var;
extern "C" UInt32U5BU5D_t1_142* ExifURational_ToArray_m8_573 (ExifURational_t8_120 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UInt32U5BU5D_t1_142_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(71);
		s_Il2CppMethodIntialized = true;
	}
	{
		UInt32U5BU5D_t1_142* L_0 = ((UInt32U5BU5D_t1_142*)SZArrayNew(UInt32U5BU5D_t1_142_il2cpp_TypeInfo_var, 2));
		UFraction32_t8_121 * L_1 = &(__this->___mValue_3);
		uint32_t L_2 = UFraction32_get_Numerator_m8_733(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		*((uint32_t*)(uint32_t*)SZArrayLdElema(L_0, 0, sizeof(uint32_t))) = (uint32_t)L_2;
		UInt32U5BU5D_t1_142* L_3 = L_0;
		UFraction32_t8_121 * L_4 = &(__this->___mValue_3);
		uint32_t L_5 = UFraction32_get_Denominator_m8_735(L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 1);
		*((uint32_t*)(uint32_t*)SZArrayLdElema(L_3, 1, sizeof(uint32_t))) = (uint32_t)L_5;
		return L_3;
	}
}
// ExifLibrary.ExifInterOperability ExifLibrary.ExifURational::get_Interoperability()
extern "C" ExifInterOperability_t8_113  ExifURational_get_Interoperability_m8_574 (ExifURational_t8_120 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (((ExifProperty_t8_99 *)__this)->___mTag_0);
		uint16_t L_1 = ExifTagFactory_GetTagID_m8_630(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		UFraction32_t8_121  L_2 = (__this->___mValue_3);
		ByteU5BU5D_t1_109* L_3 = ExifBitConverter_GetBytes_m8_398(NULL /*static, unused*/, L_2, 0, /*hidden argument*/NULL);
		ExifInterOperability_t8_113  L_4 = {0};
		ExifInterOperability__ctor_m8_496(&L_4, L_1, 5, 1, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Single ExifLibrary.ExifURational::op_Explicit(ExifLibrary.ExifURational)
extern TypeInfo* UFraction32_t8_121_il2cpp_TypeInfo_var;
extern "C" float ExifURational_op_Explicit_m8_575 (Object_t * __this /* static, unused */, ExifURational_t8_120 * ___obj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UFraction32_t8_121_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1972);
		s_Il2CppMethodIntialized = true;
	}
	{
		ExifURational_t8_120 * L_0 = ___obj;
		NullCheck(L_0);
		UFraction32_t8_121  L_1 = (L_0->___mValue_3);
		IL2CPP_RUNTIME_CLASS_INIT(UFraction32_t8_121_il2cpp_TypeInfo_var);
		float L_2 = UFraction32_op_Explicit_m8_783(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void ExifLibrary.ExifURationalArray::.ctor(ExifLibrary.ExifTag,ExifLibrary.MathEx/UFraction32[])
extern "C" void ExifURationalArray__ctor_m8_576 (ExifURationalArray_t8_107 * __this, int32_t ___tag, UFraction32U5BU5D_t8_122* ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		ExifProperty__ctor_m8_502(__this, L_0, /*hidden argument*/NULL);
		UFraction32U5BU5D_t8_122* L_1 = ___value;
		__this->___mValue_3 = L_1;
		return;
	}
}
// System.Object ExifLibrary.ExifURationalArray::get__Value()
extern "C" Object_t * ExifURationalArray_get__Value_m8_577 (ExifURationalArray_t8_107 * __this, const MethodInfo* method)
{
	{
		UFraction32U5BU5D_t8_122* L_0 = ExifURationalArray_get_Value_m8_579(__this, /*hidden argument*/NULL);
		return (Object_t *)L_0;
	}
}
// System.Void ExifLibrary.ExifURationalArray::set__Value(System.Object)
extern TypeInfo* UFraction32U5BU5D_t8_122_il2cpp_TypeInfo_var;
extern "C" void ExifURationalArray_set__Value_m8_578 (ExifURationalArray_t8_107 * __this, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UFraction32U5BU5D_t8_122_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1971);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___value;
		ExifURationalArray_set_Value_m8_580(__this, ((UFraction32U5BU5D_t8_122*)Castclass(L_0, UFraction32U5BU5D_t8_122_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return;
	}
}
// ExifLibrary.MathEx/UFraction32[] ExifLibrary.ExifURationalArray::get_Value()
extern "C" UFraction32U5BU5D_t8_122* ExifURationalArray_get_Value_m8_579 (ExifURationalArray_t8_107 * __this, const MethodInfo* method)
{
	{
		UFraction32U5BU5D_t8_122* L_0 = (__this->___mValue_3);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifURationalArray::set_Value(ExifLibrary.MathEx/UFraction32[])
extern "C" void ExifURationalArray_set_Value_m8_580 (ExifURationalArray_t8_107 * __this, UFraction32U5BU5D_t8_122* ___value, const MethodInfo* method)
{
	{
		UFraction32U5BU5D_t8_122* L_0 = ___value;
		__this->___mValue_3 = L_0;
		return;
	}
}
// System.String ExifLibrary.ExifURationalArray::ToString()
extern TypeInfo* StringBuilder_t1_247_il2cpp_TypeInfo_var;
extern "C" String_t* ExifURationalArray_ToString_m8_581 (ExifURationalArray_t8_107 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringBuilder_t1_247_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t1_247 * V_0 = {0};
	UFraction32_t8_121  V_1 = {0};
	UFraction32U5BU5D_t8_122* V_2 = {0};
	int32_t V_3 = 0;
	{
		StringBuilder_t1_247 * L_0 = (StringBuilder_t1_247 *)il2cpp_codegen_object_new (StringBuilder_t1_247_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1_12414(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		StringBuilder_t1_247 * L_1 = V_0;
		NullCheck(L_1);
		StringBuilder_Append_m1_12452(L_1, ((int32_t)91), /*hidden argument*/NULL);
		UFraction32U5BU5D_t8_122* L_2 = (__this->___mValue_3);
		V_2 = L_2;
		V_3 = 0;
		goto IL_0045;
	}

IL_001d:
	{
		UFraction32U5BU5D_t8_122* L_3 = V_2;
		int32_t L_4 = V_3;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		V_1 = (*(UFraction32_t8_121 *)((UFraction32_t8_121 *)(UFraction32_t8_121 *)SZArrayLdElema(L_3, L_4, sizeof(UFraction32_t8_121 ))));
		StringBuilder_t1_247 * L_5 = V_0;
		String_t* L_6 = UFraction32_ToString_m8_749((&V_1), /*hidden argument*/NULL);
		NullCheck(L_5);
		StringBuilder_Append_m1_12438(L_5, L_6, /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_7 = V_0;
		NullCheck(L_7);
		StringBuilder_Append_m1_12452(L_7, ((int32_t)32), /*hidden argument*/NULL);
		int32_t L_8 = V_3;
		V_3 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0045:
	{
		int32_t L_9 = V_3;
		UFraction32U5BU5D_t8_122* L_10 = V_2;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_10)->max_length)))))))
		{
			goto IL_001d;
		}
	}
	{
		StringBuilder_t1_247 * L_11 = V_0;
		StringBuilder_t1_247 * L_12 = V_0;
		NullCheck(L_12);
		int32_t L_13 = StringBuilder_get_Length_m1_12424(L_12, /*hidden argument*/NULL);
		NullCheck(L_11);
		StringBuilder_Remove_m1_12432(L_11, ((int32_t)((int32_t)L_13-(int32_t)1)), 1, /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_14 = V_0;
		NullCheck(L_14);
		StringBuilder_Append_m1_12452(L_14, ((int32_t)93), /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_15 = V_0;
		NullCheck(L_15);
		String_t* L_16 = StringBuilder_ToString_m1_12428(L_15, /*hidden argument*/NULL);
		return L_16;
	}
}
// ExifLibrary.ExifInterOperability ExifLibrary.ExifURationalArray::get_Interoperability()
extern "C" ExifInterOperability_t8_113  ExifURationalArray_get_Interoperability_m8_582 (ExifURationalArray_t8_107 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (((ExifProperty_t8_99 *)__this)->___mTag_0);
		uint16_t L_1 = ExifTagFactory_GetTagID_m8_630(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		UFraction32U5BU5D_t8_122* L_2 = (__this->___mValue_3);
		NullCheck(L_2);
		UFraction32U5BU5D_t8_122* L_3 = (__this->___mValue_3);
		ByteU5BU5D_t1_109* L_4 = ExifBitConverter_GetBytes_m8_403(NULL /*static, unused*/, L_3, 0, /*hidden argument*/NULL);
		ExifInterOperability_t8_113  L_5 = {0};
		ExifInterOperability__ctor_m8_496(&L_5, L_1, 5, (((int32_t)((int32_t)(((Array_t *)L_2)->max_length)))), L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Single[] ExifLibrary.ExifURationalArray::op_Explicit(ExifLibrary.ExifURationalArray)
extern TypeInfo* SingleU5BU5D_t1_1695_il2cpp_TypeInfo_var;
extern TypeInfo* UFraction32_t8_121_il2cpp_TypeInfo_var;
extern "C" SingleU5BU5D_t1_1695* ExifURationalArray_op_Explicit_m8_583 (Object_t * __this /* static, unused */, ExifURationalArray_t8_107 * ___obj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SingleU5BU5D_t1_1695_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(692);
		UFraction32_t8_121_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1972);
		s_Il2CppMethodIntialized = true;
	}
	SingleU5BU5D_t1_1695* V_0 = {0};
	int32_t V_1 = 0;
	{
		ExifURationalArray_t8_107 * L_0 = ___obj;
		NullCheck(L_0);
		UFraction32U5BU5D_t8_122* L_1 = (L_0->___mValue_3);
		NullCheck(L_1);
		V_0 = ((SingleU5BU5D_t1_1695*)SZArrayNew(SingleU5BU5D_t1_1695_il2cpp_TypeInfo_var, (((int32_t)((int32_t)(((Array_t *)L_1)->max_length))))));
		V_1 = 0;
		goto IL_0032;
	}

IL_0015:
	{
		SingleU5BU5D_t1_1695* L_2 = V_0;
		int32_t L_3 = V_1;
		ExifURationalArray_t8_107 * L_4 = ___obj;
		NullCheck(L_4);
		UFraction32U5BU5D_t8_122* L_5 = (L_4->___mValue_3);
		int32_t L_6 = V_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		IL2CPP_RUNTIME_CLASS_INIT(UFraction32_t8_121_il2cpp_TypeInfo_var);
		float L_7 = UFraction32_op_Explicit_m8_783(NULL /*static, unused*/, (*(UFraction32_t8_121 *)((UFraction32_t8_121 *)(UFraction32_t8_121 *)SZArrayLdElema(L_5, L_6, sizeof(UFraction32_t8_121 )))), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		*((float*)(float*)SZArrayLdElema(L_2, L_3, sizeof(float))) = (float)L_7;
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0032:
	{
		int32_t L_9 = V_1;
		ExifURationalArray_t8_107 * L_10 = ___obj;
		NullCheck(L_10);
		UFraction32U5BU5D_t8_122* L_11 = (L_10->___mValue_3);
		NullCheck(L_11);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_11)->max_length)))))))
		{
			goto IL_0015;
		}
	}
	{
		SingleU5BU5D_t1_1695* L_12 = V_0;
		return L_12;
	}
}
// System.Void ExifLibrary.ExifUndefined::.ctor(ExifLibrary.ExifTag,System.Byte[])
extern "C" void ExifUndefined__ctor_m8_584 (ExifUndefined_t8_123 * __this, int32_t ___tag, ByteU5BU5D_t1_109* ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		ExifProperty__ctor_m8_502(__this, L_0, /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_1 = ___value;
		__this->___mValue_3 = L_1;
		return;
	}
}
// System.Object ExifLibrary.ExifUndefined::get__Value()
extern "C" Object_t * ExifUndefined_get__Value_m8_585 (ExifUndefined_t8_123 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t1_109* L_0 = ExifUndefined_get_Value_m8_587(__this, /*hidden argument*/NULL);
		return (Object_t *)L_0;
	}
}
// System.Void ExifLibrary.ExifUndefined::set__Value(System.Object)
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern "C" void ExifUndefined_set__Value_m8_586 (ExifUndefined_t8_123 * __this, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___value;
		ExifUndefined_set_Value_m8_588(__this, ((ByteU5BU5D_t1_109*)Castclass(L_0, ByteU5BU5D_t1_109_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return;
	}
}
// System.Byte[] ExifLibrary.ExifUndefined::get_Value()
extern "C" ByteU5BU5D_t1_109* ExifUndefined_get_Value_m8_587 (ExifUndefined_t8_123 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t1_109* L_0 = (__this->___mValue_3);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifUndefined::set_Value(System.Byte[])
extern "C" void ExifUndefined_set_Value_m8_588 (ExifUndefined_t8_123 * __this, ByteU5BU5D_t1_109* ___value, const MethodInfo* method)
{
	{
		ByteU5BU5D_t1_109* L_0 = ___value;
		__this->___mValue_3 = L_0;
		return;
	}
}
// System.String ExifLibrary.ExifUndefined::ToString()
extern TypeInfo* StringBuilder_t1_247_il2cpp_TypeInfo_var;
extern "C" String_t* ExifUndefined_ToString_m8_589 (ExifUndefined_t8_123 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringBuilder_t1_247_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t1_247 * V_0 = {0};
	uint8_t V_1 = 0x0;
	ByteU5BU5D_t1_109* V_2 = {0};
	int32_t V_3 = 0;
	{
		StringBuilder_t1_247 * L_0 = (StringBuilder_t1_247 *)il2cpp_codegen_object_new (StringBuilder_t1_247_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1_12414(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		StringBuilder_t1_247 * L_1 = V_0;
		NullCheck(L_1);
		StringBuilder_Append_m1_12452(L_1, ((int32_t)91), /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_2 = (__this->___mValue_3);
		V_2 = L_2;
		V_3 = 0;
		goto IL_0036;
	}

IL_001d:
	{
		ByteU5BU5D_t1_109* L_3 = V_2;
		int32_t L_4 = V_3;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		V_1 = (*(uint8_t*)(uint8_t*)SZArrayLdElema(L_3, L_5, sizeof(uint8_t)));
		StringBuilder_t1_247 * L_6 = V_0;
		uint8_t L_7 = V_1;
		NullCheck(L_6);
		StringBuilder_Append_m1_12440(L_6, L_7, /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_8 = V_0;
		NullCheck(L_8);
		StringBuilder_Append_m1_12452(L_8, ((int32_t)32), /*hidden argument*/NULL);
		int32_t L_9 = V_3;
		V_3 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0036:
	{
		int32_t L_10 = V_3;
		ByteU5BU5D_t1_109* L_11 = V_2;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_11)->max_length)))))))
		{
			goto IL_001d;
		}
	}
	{
		StringBuilder_t1_247 * L_12 = V_0;
		StringBuilder_t1_247 * L_13 = V_0;
		NullCheck(L_13);
		int32_t L_14 = StringBuilder_get_Length_m1_12424(L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		StringBuilder_Remove_m1_12432(L_12, ((int32_t)((int32_t)L_14-(int32_t)1)), 1, /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_15 = V_0;
		NullCheck(L_15);
		StringBuilder_Append_m1_12452(L_15, ((int32_t)93), /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_16 = V_0;
		NullCheck(L_16);
		String_t* L_17 = StringBuilder_ToString_m1_12428(L_16, /*hidden argument*/NULL);
		return L_17;
	}
}
// ExifLibrary.ExifInterOperability ExifLibrary.ExifUndefined::get_Interoperability()
extern "C" ExifInterOperability_t8_113  ExifUndefined_get_Interoperability_m8_590 (ExifUndefined_t8_123 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (((ExifProperty_t8_99 *)__this)->___mTag_0);
		uint16_t L_1 = ExifTagFactory_GetTagID_m8_630(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_2 = (__this->___mValue_3);
		NullCheck(L_2);
		ByteU5BU5D_t1_109* L_3 = (__this->___mValue_3);
		ExifInterOperability_t8_113  L_4 = {0};
		ExifInterOperability__ctor_m8_496(&L_4, L_1, 7, (((int32_t)((int32_t)(((Array_t *)L_2)->max_length)))), L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Byte[] ExifLibrary.ExifUndefined::op_Implicit(ExifLibrary.ExifUndefined)
extern "C" ByteU5BU5D_t1_109* ExifUndefined_op_Implicit_m8_591 (Object_t * __this /* static, unused */, ExifUndefined_t8_123 * ___obj, const MethodInfo* method)
{
	{
		ExifUndefined_t8_123 * L_0 = ___obj;
		NullCheck(L_0);
		ByteU5BU5D_t1_109* L_1 = (L_0->___mValue_3);
		return L_1;
	}
}
// System.Void ExifLibrary.ExifSInt::.ctor(ExifLibrary.ExifTag,System.Int32)
extern "C" void ExifSInt__ctor_m8_592 (ExifSInt_t8_124 * __this, int32_t ___tag, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		ExifProperty__ctor_m8_502(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = ___value;
		__this->___mValue_3 = L_1;
		return;
	}
}
// System.Object ExifLibrary.ExifSInt::get__Value()
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern "C" Object_t * ExifSInt_get__Value_m8_593 (ExifSInt_t8_124 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ExifSInt_get_Value_m8_595(__this, /*hidden argument*/NULL);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void ExifLibrary.ExifSInt::set__Value(System.Object)
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern "C" void ExifSInt_set__Value_m8_594 (ExifSInt_t8_124 * __this, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___value;
		ExifSInt_set_Value_m8_596(__this, ((*(int32_t*)((int32_t*)UnBox (L_0, Int32_t1_3_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 ExifLibrary.ExifSInt::get_Value()
extern "C" int32_t ExifSInt_get_Value_m8_595 (ExifSInt_t8_124 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___mValue_3);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifSInt::set_Value(System.Int32)
extern "C" void ExifSInt_set_Value_m8_596 (ExifSInt_t8_124 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___mValue_3 = L_0;
		return;
	}
}
// System.String ExifLibrary.ExifSInt::ToString()
extern "C" String_t* ExifSInt_ToString_m8_597 (ExifSInt_t8_124 * __this, const MethodInfo* method)
{
	{
		int32_t* L_0 = &(__this->___mValue_3);
		String_t* L_1 = Int32_ToString_m1_101(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// ExifLibrary.ExifInterOperability ExifLibrary.ExifSInt::get_Interoperability()
extern "C" ExifInterOperability_t8_113  ExifSInt_get_Interoperability_m8_598 (ExifSInt_t8_124 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (((ExifProperty_t8_99 *)__this)->___mTag_0);
		uint16_t L_1 = ExifTagFactory_GetTagID_m8_630(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		int32_t L_2 = (__this->___mValue_3);
		ByteU5BU5D_t1_109* L_3 = BitConverterEx_GetBytes_m8_358(NULL /*static, unused*/, L_2, 0, 0, /*hidden argument*/NULL);
		ExifInterOperability_t8_113  L_4 = {0};
		ExifInterOperability__ctor_m8_496(&L_4, L_1, ((int32_t)9), 1, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Int32 ExifLibrary.ExifSInt::op_Implicit(ExifLibrary.ExifSInt)
extern "C" int32_t ExifSInt_op_Implicit_m8_599 (Object_t * __this /* static, unused */, ExifSInt_t8_124 * ___obj, const MethodInfo* method)
{
	{
		ExifSInt_t8_124 * L_0 = ___obj;
		NullCheck(L_0);
		int32_t L_1 = (L_0->___mValue_3);
		return L_1;
	}
}
// System.Void ExifLibrary.ExifSIntArray::.ctor(ExifLibrary.ExifTag,System.Int32[])
extern "C" void ExifSIntArray__ctor_m8_600 (ExifSIntArray_t8_125 * __this, int32_t ___tag, Int32U5BU5D_t1_275* ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		ExifProperty__ctor_m8_502(__this, L_0, /*hidden argument*/NULL);
		Int32U5BU5D_t1_275* L_1 = ___value;
		__this->___mValue_3 = L_1;
		return;
	}
}
// System.Object ExifLibrary.ExifSIntArray::get__Value()
extern "C" Object_t * ExifSIntArray_get__Value_m8_601 (ExifSIntArray_t8_125 * __this, const MethodInfo* method)
{
	{
		Int32U5BU5D_t1_275* L_0 = ExifSIntArray_get_Value_m8_603(__this, /*hidden argument*/NULL);
		return (Object_t *)L_0;
	}
}
// System.Void ExifLibrary.ExifSIntArray::set__Value(System.Object)
extern TypeInfo* Int32U5BU5D_t1_275_il2cpp_TypeInfo_var;
extern "C" void ExifSIntArray_set__Value_m8_602 (ExifSIntArray_t8_125 * __this, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32U5BU5D_t1_275_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(51);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___value;
		ExifSIntArray_set_Value_m8_604(__this, ((Int32U5BU5D_t1_275*)Castclass(L_0, Int32U5BU5D_t1_275_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return;
	}
}
// System.Int32[] ExifLibrary.ExifSIntArray::get_Value()
extern "C" Int32U5BU5D_t1_275* ExifSIntArray_get_Value_m8_603 (ExifSIntArray_t8_125 * __this, const MethodInfo* method)
{
	{
		Int32U5BU5D_t1_275* L_0 = (__this->___mValue_3);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifSIntArray::set_Value(System.Int32[])
extern "C" void ExifSIntArray_set_Value_m8_604 (ExifSIntArray_t8_125 * __this, Int32U5BU5D_t1_275* ___value, const MethodInfo* method)
{
	{
		Int32U5BU5D_t1_275* L_0 = ___value;
		__this->___mValue_3 = L_0;
		return;
	}
}
// System.String ExifLibrary.ExifSIntArray::ToString()
extern TypeInfo* StringBuilder_t1_247_il2cpp_TypeInfo_var;
extern "C" String_t* ExifSIntArray_ToString_m8_605 (ExifSIntArray_t8_125 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringBuilder_t1_247_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t1_247 * V_0 = {0};
	int32_t V_1 = 0;
	Int32U5BU5D_t1_275* V_2 = {0};
	int32_t V_3 = 0;
	{
		StringBuilder_t1_247 * L_0 = (StringBuilder_t1_247 *)il2cpp_codegen_object_new (StringBuilder_t1_247_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1_12414(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		StringBuilder_t1_247 * L_1 = V_0;
		NullCheck(L_1);
		StringBuilder_Append_m1_12452(L_1, ((int32_t)91), /*hidden argument*/NULL);
		Int32U5BU5D_t1_275* L_2 = (__this->___mValue_3);
		V_2 = L_2;
		V_3 = 0;
		goto IL_0036;
	}

IL_001d:
	{
		Int32U5BU5D_t1_275* L_3 = V_2;
		int32_t L_4 = V_3;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		V_1 = (*(int32_t*)(int32_t*)SZArrayLdElema(L_3, L_5, sizeof(int32_t)));
		StringBuilder_t1_247 * L_6 = V_0;
		int32_t L_7 = V_1;
		NullCheck(L_6);
		StringBuilder_Append_m1_12444(L_6, L_7, /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_8 = V_0;
		NullCheck(L_8);
		StringBuilder_Append_m1_12452(L_8, ((int32_t)32), /*hidden argument*/NULL);
		int32_t L_9 = V_3;
		V_3 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0036:
	{
		int32_t L_10 = V_3;
		Int32U5BU5D_t1_275* L_11 = V_2;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_11)->max_length)))))))
		{
			goto IL_001d;
		}
	}
	{
		StringBuilder_t1_247 * L_12 = V_0;
		StringBuilder_t1_247 * L_13 = V_0;
		NullCheck(L_13);
		int32_t L_14 = StringBuilder_get_Length_m1_12424(L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		StringBuilder_Remove_m1_12432(L_12, ((int32_t)((int32_t)L_14-(int32_t)1)), 1, /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_15 = V_0;
		NullCheck(L_15);
		StringBuilder_Append_m1_12452(L_15, ((int32_t)93), /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_16 = V_0;
		NullCheck(L_16);
		String_t* L_17 = StringBuilder_ToString_m1_12428(L_16, /*hidden argument*/NULL);
		return L_17;
	}
}
// ExifLibrary.ExifInterOperability ExifLibrary.ExifSIntArray::get_Interoperability()
extern "C" ExifInterOperability_t8_113  ExifSIntArray_get_Interoperability_m8_606 (ExifSIntArray_t8_125 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (((ExifProperty_t8_99 *)__this)->___mTag_0);
		uint16_t L_1 = ExifTagFactory_GetTagID_m8_630(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Int32U5BU5D_t1_275* L_2 = (__this->___mValue_3);
		NullCheck(L_2);
		Int32U5BU5D_t1_275* L_3 = (__this->___mValue_3);
		ByteU5BU5D_t1_109* L_4 = ExifBitConverter_GetBytes_m8_402(NULL /*static, unused*/, L_3, 0, /*hidden argument*/NULL);
		ExifInterOperability_t8_113  L_5 = {0};
		ExifInterOperability__ctor_m8_496(&L_5, L_1, ((int32_t)9), (((int32_t)((int32_t)(((Array_t *)L_2)->max_length)))), L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Int32[] ExifLibrary.ExifSIntArray::op_Implicit(ExifLibrary.ExifSIntArray)
extern "C" Int32U5BU5D_t1_275* ExifSIntArray_op_Implicit_m8_607 (Object_t * __this /* static, unused */, ExifSIntArray_t8_125 * ___obj, const MethodInfo* method)
{
	{
		ExifSIntArray_t8_125 * L_0 = ___obj;
		NullCheck(L_0);
		Int32U5BU5D_t1_275* L_1 = (L_0->___mValue_3);
		return L_1;
	}
}
// System.Void ExifLibrary.ExifSRational::.ctor(ExifLibrary.ExifTag,System.Int32,System.Int32)
extern "C" void ExifSRational__ctor_m8_608 (ExifSRational_t8_126 * __this, int32_t ___tag, int32_t ___numerator, int32_t ___denominator, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		ExifProperty__ctor_m8_502(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = ___numerator;
		int32_t L_2 = ___denominator;
		Fraction32_t8_127  L_3 = {0};
		Fraction32__ctor_m8_659(&L_3, L_1, L_2, /*hidden argument*/NULL);
		__this->___mValue_3 = L_3;
		return;
	}
}
// System.Void ExifLibrary.ExifSRational::.ctor(ExifLibrary.ExifTag,ExifLibrary.MathEx/Fraction32)
extern "C" void ExifSRational__ctor_m8_609 (ExifSRational_t8_126 * __this, int32_t ___tag, Fraction32_t8_127  ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		ExifProperty__ctor_m8_502(__this, L_0, /*hidden argument*/NULL);
		Fraction32_t8_127  L_1 = ___value;
		__this->___mValue_3 = L_1;
		return;
	}
}
// System.Object ExifLibrary.ExifSRational::get__Value()
extern TypeInfo* Fraction32_t8_127_il2cpp_TypeInfo_var;
extern "C" Object_t * ExifSRational_get__Value_m8_610 (ExifSRational_t8_126 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Fraction32_t8_127_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1974);
		s_Il2CppMethodIntialized = true;
	}
	{
		Fraction32_t8_127  L_0 = ExifSRational_get_Value_m8_612(__this, /*hidden argument*/NULL);
		Fraction32_t8_127  L_1 = L_0;
		Object_t * L_2 = Box(Fraction32_t8_127_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void ExifLibrary.ExifSRational::set__Value(System.Object)
extern TypeInfo* Fraction32_t8_127_il2cpp_TypeInfo_var;
extern "C" void ExifSRational_set__Value_m8_611 (ExifSRational_t8_126 * __this, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Fraction32_t8_127_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1974);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___value;
		ExifSRational_set_Value_m8_613(__this, ((*(Fraction32_t8_127 *)((Fraction32_t8_127 *)UnBox (L_0, Fraction32_t8_127_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		return;
	}
}
// ExifLibrary.MathEx/Fraction32 ExifLibrary.ExifSRational::get_Value()
extern "C" Fraction32_t8_127  ExifSRational_get_Value_m8_612 (ExifSRational_t8_126 * __this, const MethodInfo* method)
{
	{
		Fraction32_t8_127  L_0 = (__this->___mValue_3);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifSRational::set_Value(ExifLibrary.MathEx/Fraction32)
extern "C" void ExifSRational_set_Value_m8_613 (ExifSRational_t8_126 * __this, Fraction32_t8_127  ___value, const MethodInfo* method)
{
	{
		Fraction32_t8_127  L_0 = ___value;
		__this->___mValue_3 = L_0;
		return;
	}
}
// System.String ExifLibrary.ExifSRational::ToString()
extern "C" String_t* ExifSRational_ToString_m8_614 (ExifSRational_t8_126 * __this, const MethodInfo* method)
{
	{
		Fraction32_t8_127 * L_0 = &(__this->___mValue_3);
		String_t* L_1 = Fraction32_ToString_m8_686(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Single ExifLibrary.ExifSRational::ToFloat()
extern TypeInfo* Fraction32_t8_127_il2cpp_TypeInfo_var;
extern "C" float ExifSRational_ToFloat_m8_615 (ExifSRational_t8_126 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Fraction32_t8_127_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1974);
		s_Il2CppMethodIntialized = true;
	}
	{
		Fraction32_t8_127  L_0 = (__this->___mValue_3);
		IL2CPP_RUNTIME_CLASS_INIT(Fraction32_t8_127_il2cpp_TypeInfo_var);
		float L_1 = Fraction32_op_Explicit_m8_720(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32[] ExifLibrary.ExifSRational::ToArray()
extern TypeInfo* Int32U5BU5D_t1_275_il2cpp_TypeInfo_var;
extern "C" Int32U5BU5D_t1_275* ExifSRational_ToArray_m8_616 (ExifSRational_t8_126 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32U5BU5D_t1_275_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(51);
		s_Il2CppMethodIntialized = true;
	}
	{
		Int32U5BU5D_t1_275* L_0 = ((Int32U5BU5D_t1_275*)SZArrayNew(Int32U5BU5D_t1_275_il2cpp_TypeInfo_var, 2));
		Fraction32_t8_127 * L_1 = &(__this->___mValue_3);
		int32_t L_2 = Fraction32_get_Numerator_m8_666(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_0, 0, sizeof(int32_t))) = (int32_t)L_2;
		Int32U5BU5D_t1_275* L_3 = L_0;
		Fraction32_t8_127 * L_4 = &(__this->___mValue_3);
		int32_t L_5 = Fraction32_get_Denominator_m8_668(L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 1);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_3, 1, sizeof(int32_t))) = (int32_t)L_5;
		return L_3;
	}
}
// ExifLibrary.ExifInterOperability ExifLibrary.ExifSRational::get_Interoperability()
extern "C" ExifInterOperability_t8_113  ExifSRational_get_Interoperability_m8_617 (ExifSRational_t8_126 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (((ExifProperty_t8_99 *)__this)->___mTag_0);
		uint16_t L_1 = ExifTagFactory_GetTagID_m8_630(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Fraction32_t8_127  L_2 = (__this->___mValue_3);
		ByteU5BU5D_t1_109* L_3 = ExifBitConverter_GetBytes_m8_399(NULL /*static, unused*/, L_2, 0, /*hidden argument*/NULL);
		ExifInterOperability_t8_113  L_4 = {0};
		ExifInterOperability__ctor_m8_496(&L_4, L_1, ((int32_t)10), 1, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Single ExifLibrary.ExifSRational::op_Explicit(ExifLibrary.ExifSRational)
extern TypeInfo* Fraction32_t8_127_il2cpp_TypeInfo_var;
extern "C" float ExifSRational_op_Explicit_m8_618 (Object_t * __this /* static, unused */, ExifSRational_t8_126 * ___obj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Fraction32_t8_127_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1974);
		s_Il2CppMethodIntialized = true;
	}
	{
		ExifSRational_t8_126 * L_0 = ___obj;
		NullCheck(L_0);
		Fraction32_t8_127  L_1 = (L_0->___mValue_3);
		IL2CPP_RUNTIME_CLASS_INIT(Fraction32_t8_127_il2cpp_TypeInfo_var);
		float L_2 = Fraction32_op_Explicit_m8_720(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void ExifLibrary.ExifSRationalArray::.ctor(ExifLibrary.ExifTag,ExifLibrary.MathEx/Fraction32[])
extern "C" void ExifSRationalArray__ctor_m8_619 (ExifSRationalArray_t8_128 * __this, int32_t ___tag, Fraction32U5BU5D_t8_129* ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		ExifProperty__ctor_m8_502(__this, L_0, /*hidden argument*/NULL);
		Fraction32U5BU5D_t8_129* L_1 = ___value;
		__this->___mValue_3 = L_1;
		return;
	}
}
// System.Object ExifLibrary.ExifSRationalArray::get__Value()
extern "C" Object_t * ExifSRationalArray_get__Value_m8_620 (ExifSRationalArray_t8_128 * __this, const MethodInfo* method)
{
	{
		Fraction32U5BU5D_t8_129* L_0 = ExifSRationalArray_get_Value_m8_622(__this, /*hidden argument*/NULL);
		return (Object_t *)L_0;
	}
}
// System.Void ExifLibrary.ExifSRationalArray::set__Value(System.Object)
extern TypeInfo* Fraction32U5BU5D_t8_129_il2cpp_TypeInfo_var;
extern "C" void ExifSRationalArray_set__Value_m8_621 (ExifSRationalArray_t8_128 * __this, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Fraction32U5BU5D_t8_129_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1973);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___value;
		ExifSRationalArray_set_Value_m8_623(__this, ((Fraction32U5BU5D_t8_129*)Castclass(L_0, Fraction32U5BU5D_t8_129_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return;
	}
}
// ExifLibrary.MathEx/Fraction32[] ExifLibrary.ExifSRationalArray::get_Value()
extern "C" Fraction32U5BU5D_t8_129* ExifSRationalArray_get_Value_m8_622 (ExifSRationalArray_t8_128 * __this, const MethodInfo* method)
{
	{
		Fraction32U5BU5D_t8_129* L_0 = (__this->___mValue_3);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifSRationalArray::set_Value(ExifLibrary.MathEx/Fraction32[])
extern "C" void ExifSRationalArray_set_Value_m8_623 (ExifSRationalArray_t8_128 * __this, Fraction32U5BU5D_t8_129* ___value, const MethodInfo* method)
{
	{
		Fraction32U5BU5D_t8_129* L_0 = ___value;
		__this->___mValue_3 = L_0;
		return;
	}
}
// System.String ExifLibrary.ExifSRationalArray::ToString()
extern TypeInfo* StringBuilder_t1_247_il2cpp_TypeInfo_var;
extern "C" String_t* ExifSRationalArray_ToString_m8_624 (ExifSRationalArray_t8_128 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringBuilder_t1_247_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t1_247 * V_0 = {0};
	Fraction32_t8_127  V_1 = {0};
	Fraction32U5BU5D_t8_129* V_2 = {0};
	int32_t V_3 = 0;
	{
		StringBuilder_t1_247 * L_0 = (StringBuilder_t1_247 *)il2cpp_codegen_object_new (StringBuilder_t1_247_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1_12414(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		StringBuilder_t1_247 * L_1 = V_0;
		NullCheck(L_1);
		StringBuilder_Append_m1_12452(L_1, ((int32_t)91), /*hidden argument*/NULL);
		Fraction32U5BU5D_t8_129* L_2 = (__this->___mValue_3);
		V_2 = L_2;
		V_3 = 0;
		goto IL_0045;
	}

IL_001d:
	{
		Fraction32U5BU5D_t8_129* L_3 = V_2;
		int32_t L_4 = V_3;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		V_1 = (*(Fraction32_t8_127 *)((Fraction32_t8_127 *)(Fraction32_t8_127 *)SZArrayLdElema(L_3, L_4, sizeof(Fraction32_t8_127 ))));
		StringBuilder_t1_247 * L_5 = V_0;
		String_t* L_6 = Fraction32_ToString_m8_686((&V_1), /*hidden argument*/NULL);
		NullCheck(L_5);
		StringBuilder_Append_m1_12438(L_5, L_6, /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_7 = V_0;
		NullCheck(L_7);
		StringBuilder_Append_m1_12452(L_7, ((int32_t)32), /*hidden argument*/NULL);
		int32_t L_8 = V_3;
		V_3 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0045:
	{
		int32_t L_9 = V_3;
		Fraction32U5BU5D_t8_129* L_10 = V_2;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_10)->max_length)))))))
		{
			goto IL_001d;
		}
	}
	{
		StringBuilder_t1_247 * L_11 = V_0;
		StringBuilder_t1_247 * L_12 = V_0;
		NullCheck(L_12);
		int32_t L_13 = StringBuilder_get_Length_m1_12424(L_12, /*hidden argument*/NULL);
		NullCheck(L_11);
		StringBuilder_Remove_m1_12432(L_11, ((int32_t)((int32_t)L_13-(int32_t)1)), 1, /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_14 = V_0;
		NullCheck(L_14);
		StringBuilder_Append_m1_12452(L_14, ((int32_t)93), /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_15 = V_0;
		NullCheck(L_15);
		String_t* L_16 = StringBuilder_ToString_m1_12428(L_15, /*hidden argument*/NULL);
		return L_16;
	}
}
// ExifLibrary.ExifInterOperability ExifLibrary.ExifSRationalArray::get_Interoperability()
extern "C" ExifInterOperability_t8_113  ExifSRationalArray_get_Interoperability_m8_625 (ExifSRationalArray_t8_128 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (((ExifProperty_t8_99 *)__this)->___mTag_0);
		uint16_t L_1 = ExifTagFactory_GetTagID_m8_630(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Fraction32U5BU5D_t8_129* L_2 = (__this->___mValue_3);
		NullCheck(L_2);
		Fraction32U5BU5D_t8_129* L_3 = (__this->___mValue_3);
		ByteU5BU5D_t1_109* L_4 = ExifBitConverter_GetBytes_m8_404(NULL /*static, unused*/, L_3, 0, /*hidden argument*/NULL);
		ExifInterOperability_t8_113  L_5 = {0};
		ExifInterOperability__ctor_m8_496(&L_5, L_1, ((int32_t)10), (((int32_t)((int32_t)(((Array_t *)L_2)->max_length)))), L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Single[] ExifLibrary.ExifSRationalArray::op_Explicit(ExifLibrary.ExifSRationalArray)
extern TypeInfo* SingleU5BU5D_t1_1695_il2cpp_TypeInfo_var;
extern TypeInfo* Fraction32_t8_127_il2cpp_TypeInfo_var;
extern "C" SingleU5BU5D_t1_1695* ExifSRationalArray_op_Explicit_m8_626 (Object_t * __this /* static, unused */, ExifSRationalArray_t8_128 * ___obj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SingleU5BU5D_t1_1695_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(692);
		Fraction32_t8_127_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1974);
		s_Il2CppMethodIntialized = true;
	}
	SingleU5BU5D_t1_1695* V_0 = {0};
	int32_t V_1 = 0;
	{
		ExifSRationalArray_t8_128 * L_0 = ___obj;
		NullCheck(L_0);
		Fraction32U5BU5D_t8_129* L_1 = (L_0->___mValue_3);
		NullCheck(L_1);
		V_0 = ((SingleU5BU5D_t1_1695*)SZArrayNew(SingleU5BU5D_t1_1695_il2cpp_TypeInfo_var, (((int32_t)((int32_t)(((Array_t *)L_1)->max_length))))));
		V_1 = 0;
		goto IL_0032;
	}

IL_0015:
	{
		SingleU5BU5D_t1_1695* L_2 = V_0;
		int32_t L_3 = V_1;
		ExifSRationalArray_t8_128 * L_4 = ___obj;
		NullCheck(L_4);
		Fraction32U5BU5D_t8_129* L_5 = (L_4->___mValue_3);
		int32_t L_6 = V_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		IL2CPP_RUNTIME_CLASS_INIT(Fraction32_t8_127_il2cpp_TypeInfo_var);
		float L_7 = Fraction32_op_Explicit_m8_720(NULL /*static, unused*/, (*(Fraction32_t8_127 *)((Fraction32_t8_127 *)(Fraction32_t8_127 *)SZArrayLdElema(L_5, L_6, sizeof(Fraction32_t8_127 )))), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		*((float*)(float*)SZArrayLdElema(L_2, L_3, sizeof(float))) = (float)L_7;
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0032:
	{
		int32_t L_9 = V_1;
		ExifSRationalArray_t8_128 * L_10 = ___obj;
		NullCheck(L_10);
		Fraction32U5BU5D_t8_129* L_11 = (L_10->___mValue_3);
		NullCheck(L_11);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_11)->max_length)))))))
		{
			goto IL_0015;
		}
	}
	{
		SingleU5BU5D_t1_1695* L_12 = V_0;
		return L_12;
	}
}
// ExifLibrary.ExifProperty ExifLibrary.ExifPropertyFactory::Get(ExifLibrary.ExifInterOperability,ExifLibrary.BitConverterEx/ByteOrder,ExifLibrary.IFD)
extern "C" ExifProperty_t8_99 * ExifPropertyFactory_Get_m8_627 (Object_t * __this /* static, unused */, ExifInterOperability_t8_113  ___interOperability, int32_t ___byteOrder, int32_t ___ifd, const MethodInfo* method)
{
	{
		uint16_t L_0 = ExifInterOperability_get_TagID_m8_497((&___interOperability), /*hidden argument*/NULL);
		uint16_t L_1 = ExifInterOperability_get_TypeID_m8_498((&___interOperability), /*hidden argument*/NULL);
		uint32_t L_2 = ExifInterOperability_get_Count_m8_499((&___interOperability), /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_3 = ExifInterOperability_get_Data_m8_500((&___interOperability), /*hidden argument*/NULL);
		int32_t L_4 = ___byteOrder;
		int32_t L_5 = ___ifd;
		ExifProperty_t8_99 * L_6 = ExifPropertyFactory_Get_m8_628(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// ExifLibrary.ExifProperty ExifLibrary.ExifPropertyFactory::Get(System.UInt16,System.UInt16,System.UInt32,System.Byte[],ExifLibrary.BitConverterEx/ByteOrder,ExifLibrary.IFD)
extern TypeInfo* BitConverterEx_t8_61_il2cpp_TypeInfo_var;
extern TypeInfo* ExifEnumProperty_1_t8_338_il2cpp_TypeInfo_var;
extern TypeInfo* ExifEnumProperty_1_t8_339_il2cpp_TypeInfo_var;
extern TypeInfo* ExifEnumProperty_1_t8_340_il2cpp_TypeInfo_var;
extern TypeInfo* ExifEnumProperty_1_t8_341_il2cpp_TypeInfo_var;
extern TypeInfo* ExifEnumProperty_1_t8_342_il2cpp_TypeInfo_var;
extern TypeInfo* ExifEnumProperty_1_t8_343_il2cpp_TypeInfo_var;
extern TypeInfo* ExifDateTime_t8_100_il2cpp_TypeInfo_var;
extern TypeInfo* ExifVersion_t8_101_il2cpp_TypeInfo_var;
extern TypeInfo* ExifEnumProperty_1_t8_344_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern TypeInfo* Encoding_t1_406_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ExifEncodedString_t8_98_il2cpp_TypeInfo_var;
extern TypeInfo* ExifEnumProperty_1_t8_345_il2cpp_TypeInfo_var;
extern TypeInfo* ExifEnumProperty_1_t8_346_il2cpp_TypeInfo_var;
extern TypeInfo* ExifEnumProperty_1_t8_347_il2cpp_TypeInfo_var;
extern TypeInfo* ExifEnumProperty_1_t8_348_il2cpp_TypeInfo_var;
extern TypeInfo* ExifCircularSubjectArea_t8_104_il2cpp_TypeInfo_var;
extern TypeInfo* ExifRectangularSubjectArea_t8_105_il2cpp_TypeInfo_var;
extern TypeInfo* ExifPointSubjectArea_t8_102_il2cpp_TypeInfo_var;
extern TypeInfo* ExifEnumProperty_1_t8_349_il2cpp_TypeInfo_var;
extern TypeInfo* ExifEnumProperty_1_t8_350_il2cpp_TypeInfo_var;
extern TypeInfo* ExifEnumProperty_1_t8_351_il2cpp_TypeInfo_var;
extern TypeInfo* ExifEnumProperty_1_t8_352_il2cpp_TypeInfo_var;
extern TypeInfo* ExifEnumProperty_1_t8_353_il2cpp_TypeInfo_var;
extern TypeInfo* ExifEnumProperty_1_t8_354_il2cpp_TypeInfo_var;
extern TypeInfo* ExifEnumProperty_1_t8_355_il2cpp_TypeInfo_var;
extern TypeInfo* ExifEnumProperty_1_t8_356_il2cpp_TypeInfo_var;
extern TypeInfo* ExifEnumProperty_1_t8_357_il2cpp_TypeInfo_var;
extern TypeInfo* ExifEnumProperty_1_t8_358_il2cpp_TypeInfo_var;
extern TypeInfo* ExifEnumProperty_1_t8_359_il2cpp_TypeInfo_var;
extern TypeInfo* ExifEnumProperty_1_t8_360_il2cpp_TypeInfo_var;
extern TypeInfo* ExifEnumProperty_1_t8_361_il2cpp_TypeInfo_var;
extern TypeInfo* GPSLatitudeLongitude_t8_106_il2cpp_TypeInfo_var;
extern TypeInfo* ExifEnumProperty_1_t8_362_il2cpp_TypeInfo_var;
extern TypeInfo* ExifEnumProperty_1_t8_363_il2cpp_TypeInfo_var;
extern TypeInfo* GPSTimeStamp_t8_108_il2cpp_TypeInfo_var;
extern TypeInfo* ExifEnumProperty_1_t8_364_il2cpp_TypeInfo_var;
extern TypeInfo* ExifEnumProperty_1_t8_365_il2cpp_TypeInfo_var;
extern TypeInfo* ExifEnumProperty_1_t8_366_il2cpp_TypeInfo_var;
extern TypeInfo* ExifEnumProperty_1_t8_367_il2cpp_TypeInfo_var;
extern TypeInfo* ExifEnumProperty_1_t8_368_il2cpp_TypeInfo_var;
extern TypeInfo* ExifEnumProperty_1_t8_369_il2cpp_TypeInfo_var;
extern TypeInfo* ExifAscii_t8_116_il2cpp_TypeInfo_var;
extern TypeInfo* ExifByte_t8_114_il2cpp_TypeInfo_var;
extern TypeInfo* ExifByteArray_t8_115_il2cpp_TypeInfo_var;
extern TypeInfo* ExifUShort_t8_117_il2cpp_TypeInfo_var;
extern TypeInfo* ExifUShortArray_t8_103_il2cpp_TypeInfo_var;
extern TypeInfo* ExifUInt_t8_118_il2cpp_TypeInfo_var;
extern TypeInfo* ExifUIntArray_t8_119_il2cpp_TypeInfo_var;
extern TypeInfo* ExifURational_t8_120_il2cpp_TypeInfo_var;
extern TypeInfo* ExifURationalArray_t8_107_il2cpp_TypeInfo_var;
extern TypeInfo* ExifUndefined_t8_123_il2cpp_TypeInfo_var;
extern TypeInfo* ExifSInt_t8_124_il2cpp_TypeInfo_var;
extern TypeInfo* ExifSIntArray_t8_125_il2cpp_TypeInfo_var;
extern TypeInfo* ExifSRational_t8_126_il2cpp_TypeInfo_var;
extern TypeInfo* ExifSRationalArray_t8_128_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern const MethodInfo* ExifEnumProperty_1__ctor_m8_1974_MethodInfo_var;
extern const MethodInfo* ExifEnumProperty_1__ctor_m8_1975_MethodInfo_var;
extern const MethodInfo* ExifEnumProperty_1__ctor_m8_1976_MethodInfo_var;
extern const MethodInfo* ExifEnumProperty_1__ctor_m8_1977_MethodInfo_var;
extern const MethodInfo* ExifEnumProperty_1__ctor_m8_1978_MethodInfo_var;
extern const MethodInfo* ExifEnumProperty_1__ctor_m8_1979_MethodInfo_var;
extern const MethodInfo* ExifEnumProperty_1__ctor_m8_1980_MethodInfo_var;
extern const MethodInfo* Array_IndexOf_TisByte_t1_11_m1_15039_MethodInfo_var;
extern const MethodInfo* ExifEnumProperty_1__ctor_m8_1981_MethodInfo_var;
extern const MethodInfo* ExifEnumProperty_1__ctor_m8_1982_MethodInfo_var;
extern const MethodInfo* ExifEnumProperty_1__ctor_m8_1983_MethodInfo_var;
extern const MethodInfo* ExifEnumProperty_1__ctor_m8_1984_MethodInfo_var;
extern const MethodInfo* ExifEnumProperty_1__ctor_m8_1985_MethodInfo_var;
extern const MethodInfo* ExifEnumProperty_1__ctor_m8_1986_MethodInfo_var;
extern const MethodInfo* ExifEnumProperty_1__ctor_m8_1987_MethodInfo_var;
extern const MethodInfo* ExifEnumProperty_1__ctor_m8_1988_MethodInfo_var;
extern const MethodInfo* ExifEnumProperty_1__ctor_m8_1989_MethodInfo_var;
extern const MethodInfo* ExifEnumProperty_1__ctor_m8_1990_MethodInfo_var;
extern const MethodInfo* ExifEnumProperty_1__ctor_m8_1991_MethodInfo_var;
extern const MethodInfo* ExifEnumProperty_1__ctor_m8_1992_MethodInfo_var;
extern const MethodInfo* ExifEnumProperty_1__ctor_m8_1993_MethodInfo_var;
extern const MethodInfo* ExifEnumProperty_1__ctor_m8_1994_MethodInfo_var;
extern const MethodInfo* ExifEnumProperty_1__ctor_m8_1995_MethodInfo_var;
extern const MethodInfo* ExifEnumProperty_1__ctor_m8_1996_MethodInfo_var;
extern const MethodInfo* ExifEnumProperty_1__ctor_m8_1997_MethodInfo_var;
extern const MethodInfo* ExifEnumProperty_1__ctor_m8_1998_MethodInfo_var;
extern const MethodInfo* ExifEnumProperty_1__ctor_m8_1999_MethodInfo_var;
extern const MethodInfo* ExifEnumProperty_1__ctor_m8_2000_MethodInfo_var;
extern const MethodInfo* ExifEnumProperty_1__ctor_m8_2001_MethodInfo_var;
extern const MethodInfo* ExifEnumProperty_1__ctor_m8_2002_MethodInfo_var;
extern const MethodInfo* ExifEnumProperty_1__ctor_m8_2003_MethodInfo_var;
extern const MethodInfo* ExifEnumProperty_1__ctor_m8_2004_MethodInfo_var;
extern const MethodInfo* ExifEnumProperty_1__ctor_m8_2005_MethodInfo_var;
extern const MethodInfo* ExifEnumProperty_1__ctor_m8_2006_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral5212;
extern Il2CppCodeGenString* _stringLiteral5214;
extern Il2CppCodeGenString* _stringLiteral5213;
extern Il2CppCodeGenString* _stringLiteral5215;
extern Il2CppCodeGenString* _stringLiteral5224;
extern "C" ExifProperty_t8_99 * ExifPropertyFactory_Get_m8_628 (Object_t * __this /* static, unused */, uint16_t ___tag, uint16_t ___type, uint32_t ___count, ByteU5BU5D_t1_109* ___value, int32_t ___byteOrder, int32_t ___ifd, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		BitConverterEx_t8_61_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1970);
		ExifEnumProperty_1_t8_338_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2023);
		ExifEnumProperty_1_t8_339_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2024);
		ExifEnumProperty_1_t8_340_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2025);
		ExifEnumProperty_1_t8_341_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2026);
		ExifEnumProperty_1_t8_342_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2027);
		ExifEnumProperty_1_t8_343_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2028);
		ExifDateTime_t8_100_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2029);
		ExifVersion_t8_101_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2030);
		ExifEnumProperty_1_t8_344_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2031);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		Encoding_t1_406_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		ExifEncodedString_t8_98_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2032);
		ExifEnumProperty_1_t8_345_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2033);
		ExifEnumProperty_1_t8_346_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2034);
		ExifEnumProperty_1_t8_347_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2035);
		ExifEnumProperty_1_t8_348_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2036);
		ExifCircularSubjectArea_t8_104_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2037);
		ExifRectangularSubjectArea_t8_105_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2038);
		ExifPointSubjectArea_t8_102_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2039);
		ExifEnumProperty_1_t8_349_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2040);
		ExifEnumProperty_1_t8_350_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2041);
		ExifEnumProperty_1_t8_351_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2042);
		ExifEnumProperty_1_t8_352_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2043);
		ExifEnumProperty_1_t8_353_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2044);
		ExifEnumProperty_1_t8_354_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2045);
		ExifEnumProperty_1_t8_355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2046);
		ExifEnumProperty_1_t8_356_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2047);
		ExifEnumProperty_1_t8_357_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2048);
		ExifEnumProperty_1_t8_358_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2049);
		ExifEnumProperty_1_t8_359_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2050);
		ExifEnumProperty_1_t8_360_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2051);
		ExifEnumProperty_1_t8_361_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2052);
		GPSLatitudeLongitude_t8_106_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2053);
		ExifEnumProperty_1_t8_362_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2054);
		ExifEnumProperty_1_t8_363_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2055);
		GPSTimeStamp_t8_108_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2056);
		ExifEnumProperty_1_t8_364_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2057);
		ExifEnumProperty_1_t8_365_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2058);
		ExifEnumProperty_1_t8_366_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2059);
		ExifEnumProperty_1_t8_367_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2060);
		ExifEnumProperty_1_t8_368_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2061);
		ExifEnumProperty_1_t8_369_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2062);
		ExifAscii_t8_116_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2063);
		ExifByte_t8_114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2064);
		ExifByteArray_t8_115_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2065);
		ExifUShort_t8_117_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2066);
		ExifUShortArray_t8_103_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2067);
		ExifUInt_t8_118_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1987);
		ExifUIntArray_t8_119_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2068);
		ExifURational_t8_120_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2069);
		ExifURationalArray_t8_107_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2070);
		ExifUndefined_t8_123_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2071);
		ExifSInt_t8_124_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2072);
		ExifSIntArray_t8_125_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2073);
		ExifSRational_t8_126_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2074);
		ExifSRationalArray_t8_128_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2075);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		ExifEnumProperty_1__ctor_m8_1974_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484151);
		ExifEnumProperty_1__ctor_m8_1975_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484152);
		ExifEnumProperty_1__ctor_m8_1976_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484153);
		ExifEnumProperty_1__ctor_m8_1977_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484154);
		ExifEnumProperty_1__ctor_m8_1978_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484155);
		ExifEnumProperty_1__ctor_m8_1979_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484156);
		ExifEnumProperty_1__ctor_m8_1980_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484157);
		Array_IndexOf_TisByte_t1_11_m1_15039_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484128);
		ExifEnumProperty_1__ctor_m8_1981_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484158);
		ExifEnumProperty_1__ctor_m8_1982_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484159);
		ExifEnumProperty_1__ctor_m8_1983_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484160);
		ExifEnumProperty_1__ctor_m8_1984_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484161);
		ExifEnumProperty_1__ctor_m8_1985_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484162);
		ExifEnumProperty_1__ctor_m8_1986_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484163);
		ExifEnumProperty_1__ctor_m8_1987_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484164);
		ExifEnumProperty_1__ctor_m8_1988_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484165);
		ExifEnumProperty_1__ctor_m8_1989_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484166);
		ExifEnumProperty_1__ctor_m8_1990_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484167);
		ExifEnumProperty_1__ctor_m8_1991_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484168);
		ExifEnumProperty_1__ctor_m8_1992_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484169);
		ExifEnumProperty_1__ctor_m8_1993_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484170);
		ExifEnumProperty_1__ctor_m8_1994_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484171);
		ExifEnumProperty_1__ctor_m8_1995_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484172);
		ExifEnumProperty_1__ctor_m8_1996_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484173);
		ExifEnumProperty_1__ctor_m8_1997_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484174);
		ExifEnumProperty_1__ctor_m8_1998_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484175);
		ExifEnumProperty_1__ctor_m8_1999_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484176);
		ExifEnumProperty_1__ctor_m8_2000_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484177);
		ExifEnumProperty_1__ctor_m8_2001_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484178);
		ExifEnumProperty_1__ctor_m8_2002_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484179);
		ExifEnumProperty_1__ctor_m8_2003_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484180);
		ExifEnumProperty_1__ctor_m8_2004_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484181);
		ExifEnumProperty_1__ctor_m8_2005_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484182);
		ExifEnumProperty_1__ctor_m8_2006_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484183);
		_stringLiteral5212 = il2cpp_codegen_string_literal_from_index(5212);
		_stringLiteral5214 = il2cpp_codegen_string_literal_from_index(5214);
		_stringLiteral5213 = il2cpp_codegen_string_literal_from_index(5213);
		_stringLiteral5215 = il2cpp_codegen_string_literal_from_index(5215);
		_stringLiteral5224 = il2cpp_codegen_string_literal_from_index(5224);
		s_Il2CppMethodIntialized = true;
	}
	BitConverterEx_t8_61 * V_0 = {0};
	ByteU5BU5D_t1_109* V_1 = {0};
	ByteU5BU5D_t1_109* V_2 = {0};
	Encoding_t1_406 * V_3 = {0};
	String_t* V_4 = {0};
	int32_t V_5 = 0;
	int32_t V_6 = {0};
	int32_t G_B35_0 = 0;
	int32_t G_B34_0 = 0;
	String_t* G_B36_0 = {0};
	int32_t G_B36_1 = 0;
	{
		int32_t L_0 = ___byteOrder;
		BitConverterEx_t8_61 * L_1 = (BitConverterEx_t8_61 *)il2cpp_codegen_object_new (BitConverterEx_t8_61_il2cpp_TypeInfo_var);
		BitConverterEx__ctor_m8_340(L_1, L_0, 0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = ___ifd;
		if ((!(((uint32_t)L_2) == ((uint32_t)((int32_t)100000)))))
		{
			goto IL_00ea;
		}
	}
	{
		uint16_t L_3 = ___tag;
		if ((!(((uint32_t)L_3) == ((uint32_t)((int32_t)259)))))
		{
			goto IL_0033;
		}
	}
	{
		BitConverterEx_t8_61 * L_4 = V_0;
		ByteU5BU5D_t1_109* L_5 = ___value;
		NullCheck(L_4);
		uint16_t L_6 = BitConverterEx_ToUInt16_m8_363(L_4, L_5, 0, /*hidden argument*/NULL);
		ExifEnumProperty_1_t8_338 * L_7 = (ExifEnumProperty_1_t8_338 *)il2cpp_codegen_object_new (ExifEnumProperty_1_t8_338_il2cpp_TypeInfo_var);
		ExifEnumProperty_1__ctor_m8_1974(L_7, ((int32_t)100259), L_6, /*hidden argument*/ExifEnumProperty_1__ctor_m8_1974_MethodInfo_var);
		return L_7;
	}

IL_0033:
	{
		uint16_t L_8 = ___tag;
		if ((!(((uint32_t)L_8) == ((uint32_t)((int32_t)262)))))
		{
			goto IL_0051;
		}
	}
	{
		BitConverterEx_t8_61 * L_9 = V_0;
		ByteU5BU5D_t1_109* L_10 = ___value;
		NullCheck(L_9);
		uint16_t L_11 = BitConverterEx_ToUInt16_m8_363(L_9, L_10, 0, /*hidden argument*/NULL);
		ExifEnumProperty_1_t8_339 * L_12 = (ExifEnumProperty_1_t8_339 *)il2cpp_codegen_object_new (ExifEnumProperty_1_t8_339_il2cpp_TypeInfo_var);
		ExifEnumProperty_1__ctor_m8_1975(L_12, ((int32_t)100262), L_11, /*hidden argument*/ExifEnumProperty_1__ctor_m8_1975_MethodInfo_var);
		return L_12;
	}

IL_0051:
	{
		uint16_t L_13 = ___tag;
		if ((!(((uint32_t)L_13) == ((uint32_t)((int32_t)274)))))
		{
			goto IL_006f;
		}
	}
	{
		BitConverterEx_t8_61 * L_14 = V_0;
		ByteU5BU5D_t1_109* L_15 = ___value;
		NullCheck(L_14);
		uint16_t L_16 = BitConverterEx_ToUInt16_m8_363(L_14, L_15, 0, /*hidden argument*/NULL);
		ExifEnumProperty_1_t8_340 * L_17 = (ExifEnumProperty_1_t8_340 *)il2cpp_codegen_object_new (ExifEnumProperty_1_t8_340_il2cpp_TypeInfo_var);
		ExifEnumProperty_1__ctor_m8_1976(L_17, ((int32_t)100274), L_16, /*hidden argument*/ExifEnumProperty_1__ctor_m8_1976_MethodInfo_var);
		return L_17;
	}

IL_006f:
	{
		uint16_t L_18 = ___tag;
		if ((!(((uint32_t)L_18) == ((uint32_t)((int32_t)284)))))
		{
			goto IL_008d;
		}
	}
	{
		BitConverterEx_t8_61 * L_19 = V_0;
		ByteU5BU5D_t1_109* L_20 = ___value;
		NullCheck(L_19);
		uint16_t L_21 = BitConverterEx_ToUInt16_m8_363(L_19, L_20, 0, /*hidden argument*/NULL);
		ExifEnumProperty_1_t8_341 * L_22 = (ExifEnumProperty_1_t8_341 *)il2cpp_codegen_object_new (ExifEnumProperty_1_t8_341_il2cpp_TypeInfo_var);
		ExifEnumProperty_1__ctor_m8_1977(L_22, ((int32_t)100284), L_21, /*hidden argument*/ExifEnumProperty_1__ctor_m8_1977_MethodInfo_var);
		return L_22;
	}

IL_008d:
	{
		uint16_t L_23 = ___tag;
		if ((!(((uint32_t)L_23) == ((uint32_t)((int32_t)531)))))
		{
			goto IL_00ab;
		}
	}
	{
		BitConverterEx_t8_61 * L_24 = V_0;
		ByteU5BU5D_t1_109* L_25 = ___value;
		NullCheck(L_24);
		uint16_t L_26 = BitConverterEx_ToUInt16_m8_363(L_24, L_25, 0, /*hidden argument*/NULL);
		ExifEnumProperty_1_t8_342 * L_27 = (ExifEnumProperty_1_t8_342 *)il2cpp_codegen_object_new (ExifEnumProperty_1_t8_342_il2cpp_TypeInfo_var);
		ExifEnumProperty_1__ctor_m8_1978(L_27, ((int32_t)100531), L_26, /*hidden argument*/ExifEnumProperty_1__ctor_m8_1978_MethodInfo_var);
		return L_27;
	}

IL_00ab:
	{
		uint16_t L_28 = ___tag;
		if ((!(((uint32_t)L_28) == ((uint32_t)((int32_t)296)))))
		{
			goto IL_00c9;
		}
	}
	{
		BitConverterEx_t8_61 * L_29 = V_0;
		ByteU5BU5D_t1_109* L_30 = ___value;
		NullCheck(L_29);
		uint16_t L_31 = BitConverterEx_ToUInt16_m8_363(L_29, L_30, 0, /*hidden argument*/NULL);
		ExifEnumProperty_1_t8_343 * L_32 = (ExifEnumProperty_1_t8_343 *)il2cpp_codegen_object_new (ExifEnumProperty_1_t8_343_il2cpp_TypeInfo_var);
		ExifEnumProperty_1__ctor_m8_1979(L_32, ((int32_t)100296), L_31, /*hidden argument*/ExifEnumProperty_1__ctor_m8_1979_MethodInfo_var);
		return L_32;
	}

IL_00c9:
	{
		uint16_t L_33 = ___tag;
		if ((!(((uint32_t)L_33) == ((uint32_t)((int32_t)306)))))
		{
			goto IL_00e5;
		}
	}
	{
		ByteU5BU5D_t1_109* L_34 = ___value;
		DateTime_t1_150  L_35 = ExifBitConverter_ToDateTime_m8_387(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
		ExifDateTime_t8_100 * L_36 = (ExifDateTime_t8_100 *)il2cpp_codegen_object_new (ExifDateTime_t8_100_il2cpp_TypeInfo_var);
		ExifDateTime__ctor_m8_421(L_36, ((int32_t)100306), L_35, /*hidden argument*/NULL);
		return L_36;
	}

IL_00e5:
	{
		goto IL_07f9;
	}

IL_00ea:
	{
		int32_t L_37 = ___ifd;
		if ((!(((uint32_t)L_37) == ((uint32_t)((int32_t)200000)))))
		{
			goto IL_04f1;
		}
	}
	{
		uint16_t L_38 = ___tag;
		if ((!(((uint32_t)L_38) == ((uint32_t)((int32_t)36864)))))
		{
			goto IL_0112;
		}
	}
	{
		ByteU5BU5D_t1_109* L_39 = ___value;
		String_t* L_40 = ExifBitConverter_ToAscii_m8_384(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
		ExifVersion_t8_101 * L_41 = (ExifVersion_t8_101 *)il2cpp_codegen_object_new (ExifVersion_t8_101_il2cpp_TypeInfo_var);
		ExifVersion__ctor_m8_429(L_41, ((int32_t)236864), L_40, /*hidden argument*/NULL);
		return L_41;
	}

IL_0112:
	{
		uint16_t L_42 = ___tag;
		if ((!(((uint32_t)L_42) == ((uint32_t)((int32_t)40960)))))
		{
			goto IL_012e;
		}
	}
	{
		ByteU5BU5D_t1_109* L_43 = ___value;
		String_t* L_44 = ExifBitConverter_ToAscii_m8_384(NULL /*static, unused*/, L_43, /*hidden argument*/NULL);
		ExifVersion_t8_101 * L_45 = (ExifVersion_t8_101 *)il2cpp_codegen_object_new (ExifVersion_t8_101_il2cpp_TypeInfo_var);
		ExifVersion__ctor_m8_429(L_45, ((int32_t)240960), L_44, /*hidden argument*/NULL);
		return L_45;
	}

IL_012e:
	{
		uint16_t L_46 = ___tag;
		if ((!(((uint32_t)L_46) == ((uint32_t)((int32_t)40961)))))
		{
			goto IL_014c;
		}
	}
	{
		BitConverterEx_t8_61 * L_47 = V_0;
		ByteU5BU5D_t1_109* L_48 = ___value;
		NullCheck(L_47);
		uint16_t L_49 = BitConverterEx_ToUInt16_m8_363(L_47, L_48, 0, /*hidden argument*/NULL);
		ExifEnumProperty_1_t8_344 * L_50 = (ExifEnumProperty_1_t8_344 *)il2cpp_codegen_object_new (ExifEnumProperty_1_t8_344_il2cpp_TypeInfo_var);
		ExifEnumProperty_1__ctor_m8_1980(L_50, ((int32_t)240961), L_49, /*hidden argument*/ExifEnumProperty_1__ctor_m8_1980_MethodInfo_var);
		return L_50;
	}

IL_014c:
	{
		uint16_t L_51 = ___tag;
		if ((!(((uint32_t)L_51) == ((uint32_t)((int32_t)37510)))))
		{
			goto IL_0232;
		}
	}
	{
		V_1 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 8));
		ByteU5BU5D_t1_109* L_52 = ___value;
		NullCheck(L_52);
		V_2 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, ((int32_t)((int32_t)(((int32_t)((int32_t)(((Array_t *)L_52)->max_length))))-(int32_t)8))));
		ByteU5BU5D_t1_109* L_53 = ___value;
		ByteU5BU5D_t1_109* L_54 = V_1;
		Array_Copy_m1_1040(NULL /*static, unused*/, (Array_t *)(Array_t *)L_53, (Array_t *)(Array_t *)L_54, 8, /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_55 = ___value;
		ByteU5BU5D_t1_109* L_56 = V_2;
		ByteU5BU5D_t1_109* L_57 = ___value;
		NullCheck(L_57);
		Array_Copy_m1_1041(NULL /*static, unused*/, (Array_t *)(Array_t *)L_55, 8, (Array_t *)(Array_t *)L_56, 0, ((int32_t)((int32_t)(((int32_t)((int32_t)(((Array_t *)L_57)->max_length))))-(int32_t)8)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t1_406_il2cpp_TypeInfo_var);
		Encoding_t1_406 * L_58 = Encoding_get_UTF8_m1_12363(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_3 = L_58;
		Encoding_t1_406 * L_59 = V_3;
		ByteU5BU5D_t1_109* L_60 = V_1;
		ByteU5BU5D_t1_109* L_61 = V_1;
		NullCheck(L_61);
		NullCheck(L_59);
		String_t* L_62 = (String_t*)VirtFuncInvoker3< String_t*, ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(26 /* System.String System.Text.Encoding::GetString(System.Byte[],System.Int32,System.Int32) */, L_59, L_60, 0, (((int32_t)((int32_t)(((Array_t *)L_61)->max_length)))));
		V_4 = L_62;
		String_t* L_63 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_64 = String_op_Equality_m1_601(NULL /*static, unused*/, L_63, _stringLiteral5212, /*hidden argument*/NULL);
		if (!L_64)
		{
			goto IL_01ae;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t1_406_il2cpp_TypeInfo_var);
		Encoding_t1_406 * L_65 = Encoding_get_UTF8_m1_12363(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_3 = L_65;
		goto IL_01ed;
	}

IL_01ae:
	{
		String_t* L_66 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_67 = String_op_Equality_m1_601(NULL /*static, unused*/, L_66, _stringLiteral5214, /*hidden argument*/NULL);
		if (!L_67)
		{
			goto IL_01cf;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t1_406_il2cpp_TypeInfo_var);
		Encoding_t1_406 * L_68 = Encoding_GetEncoding_m1_12342(NULL /*static, unused*/, _stringLiteral5213, /*hidden argument*/NULL);
		V_3 = L_68;
		goto IL_01ed;
	}

IL_01cf:
	{
		String_t* L_69 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_70 = String_op_Equality_m1_601(NULL /*static, unused*/, L_69, _stringLiteral5215, /*hidden argument*/NULL);
		if (!L_70)
		{
			goto IL_01eb;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t1_406_il2cpp_TypeInfo_var);
		Encoding_t1_406 * L_71 = Encoding_get_Unicode_m1_12366(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_3 = L_71;
		goto IL_01ed;
	}

IL_01eb:
	{
		V_3 = (Encoding_t1_406 *)NULL;
	}

IL_01ed:
	{
		ByteU5BU5D_t1_109* L_72 = V_2;
		int32_t L_73 = Array_IndexOf_TisByte_t1_11_m1_15039(NULL /*static, unused*/, L_72, 0, /*hidden argument*/Array_IndexOf_TisByte_t1_11_m1_15039_MethodInfo_var);
		V_5 = L_73;
		int32_t L_74 = V_5;
		if ((!(((uint32_t)L_74) == ((uint32_t)(-1)))))
		{
			goto IL_0203;
		}
	}
	{
		ByteU5BU5D_t1_109* L_75 = V_2;
		NullCheck(L_75);
		V_5 = (((int32_t)((int32_t)(((Array_t *)L_75)->max_length))));
	}

IL_0203:
	{
		Encoding_t1_406 * L_76 = V_3;
		G_B34_0 = ((int32_t)237510);
		if (L_76)
		{
			G_B35_0 = ((int32_t)237510);
			goto IL_0221;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t1_406_il2cpp_TypeInfo_var);
		Encoding_t1_406 * L_77 = Encoding_get_UTF8_m1_12363(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_78 = V_2;
		int32_t L_79 = V_5;
		NullCheck(L_77);
		String_t* L_80 = (String_t*)VirtFuncInvoker3< String_t*, ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(26 /* System.String System.Text.Encoding::GetString(System.Byte[],System.Int32,System.Int32) */, L_77, L_78, 0, L_79);
		G_B36_0 = L_80;
		G_B36_1 = G_B34_0;
		goto IL_022b;
	}

IL_0221:
	{
		Encoding_t1_406 * L_81 = V_3;
		ByteU5BU5D_t1_109* L_82 = V_2;
		int32_t L_83 = V_5;
		NullCheck(L_81);
		String_t* L_84 = (String_t*)VirtFuncInvoker3< String_t*, ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(26 /* System.String System.Text.Encoding::GetString(System.Byte[],System.Int32,System.Int32) */, L_81, L_82, 0, L_83);
		G_B36_0 = L_84;
		G_B36_1 = G_B35_0;
	}

IL_022b:
	{
		Encoding_t1_406 * L_85 = V_3;
		ExifEncodedString_t8_98 * L_86 = (ExifEncodedString_t8_98 *)il2cpp_codegen_object_new (ExifEncodedString_t8_98_il2cpp_TypeInfo_var);
		ExifEncodedString__ctor_m8_411(L_86, G_B36_1, G_B36_0, L_85, /*hidden argument*/NULL);
		return L_86;
	}

IL_0232:
	{
		uint16_t L_87 = ___tag;
		if ((!(((uint32_t)L_87) == ((uint32_t)((int32_t)36867)))))
		{
			goto IL_024e;
		}
	}
	{
		ByteU5BU5D_t1_109* L_88 = ___value;
		DateTime_t1_150  L_89 = ExifBitConverter_ToDateTime_m8_387(NULL /*static, unused*/, L_88, /*hidden argument*/NULL);
		ExifDateTime_t8_100 * L_90 = (ExifDateTime_t8_100 *)il2cpp_codegen_object_new (ExifDateTime_t8_100_il2cpp_TypeInfo_var);
		ExifDateTime__ctor_m8_421(L_90, ((int32_t)236867), L_89, /*hidden argument*/NULL);
		return L_90;
	}

IL_024e:
	{
		uint16_t L_91 = ___tag;
		if ((!(((uint32_t)L_91) == ((uint32_t)((int32_t)36868)))))
		{
			goto IL_026a;
		}
	}
	{
		ByteU5BU5D_t1_109* L_92 = ___value;
		DateTime_t1_150  L_93 = ExifBitConverter_ToDateTime_m8_387(NULL /*static, unused*/, L_92, /*hidden argument*/NULL);
		ExifDateTime_t8_100 * L_94 = (ExifDateTime_t8_100 *)il2cpp_codegen_object_new (ExifDateTime_t8_100_il2cpp_TypeInfo_var);
		ExifDateTime__ctor_m8_421(L_94, ((int32_t)236868), L_93, /*hidden argument*/NULL);
		return L_94;
	}

IL_026a:
	{
		uint16_t L_95 = ___tag;
		if ((!(((uint32_t)L_95) == ((uint32_t)((int32_t)34850)))))
		{
			goto IL_0288;
		}
	}
	{
		BitConverterEx_t8_61 * L_96 = V_0;
		ByteU5BU5D_t1_109* L_97 = ___value;
		NullCheck(L_96);
		uint16_t L_98 = BitConverterEx_ToUInt16_m8_363(L_96, L_97, 0, /*hidden argument*/NULL);
		ExifEnumProperty_1_t8_345 * L_99 = (ExifEnumProperty_1_t8_345 *)il2cpp_codegen_object_new (ExifEnumProperty_1_t8_345_il2cpp_TypeInfo_var);
		ExifEnumProperty_1__ctor_m8_1981(L_99, ((int32_t)234850), L_98, /*hidden argument*/ExifEnumProperty_1__ctor_m8_1981_MethodInfo_var);
		return L_99;
	}

IL_0288:
	{
		uint16_t L_100 = ___tag;
		if ((!(((uint32_t)L_100) == ((uint32_t)((int32_t)37383)))))
		{
			goto IL_02a6;
		}
	}
	{
		BitConverterEx_t8_61 * L_101 = V_0;
		ByteU5BU5D_t1_109* L_102 = ___value;
		NullCheck(L_101);
		uint16_t L_103 = BitConverterEx_ToUInt16_m8_363(L_101, L_102, 0, /*hidden argument*/NULL);
		ExifEnumProperty_1_t8_346 * L_104 = (ExifEnumProperty_1_t8_346 *)il2cpp_codegen_object_new (ExifEnumProperty_1_t8_346_il2cpp_TypeInfo_var);
		ExifEnumProperty_1__ctor_m8_1982(L_104, ((int32_t)237383), L_103, /*hidden argument*/ExifEnumProperty_1__ctor_m8_1982_MethodInfo_var);
		return L_104;
	}

IL_02a6:
	{
		uint16_t L_105 = ___tag;
		if ((!(((uint32_t)L_105) == ((uint32_t)((int32_t)37384)))))
		{
			goto IL_02c4;
		}
	}
	{
		BitConverterEx_t8_61 * L_106 = V_0;
		ByteU5BU5D_t1_109* L_107 = ___value;
		NullCheck(L_106);
		uint16_t L_108 = BitConverterEx_ToUInt16_m8_363(L_106, L_107, 0, /*hidden argument*/NULL);
		ExifEnumProperty_1_t8_347 * L_109 = (ExifEnumProperty_1_t8_347 *)il2cpp_codegen_object_new (ExifEnumProperty_1_t8_347_il2cpp_TypeInfo_var);
		ExifEnumProperty_1__ctor_m8_1983(L_109, ((int32_t)237384), L_108, /*hidden argument*/ExifEnumProperty_1__ctor_m8_1983_MethodInfo_var);
		return L_109;
	}

IL_02c4:
	{
		uint16_t L_110 = ___tag;
		if ((!(((uint32_t)L_110) == ((uint32_t)((int32_t)37385)))))
		{
			goto IL_02e3;
		}
	}
	{
		BitConverterEx_t8_61 * L_111 = V_0;
		ByteU5BU5D_t1_109* L_112 = ___value;
		NullCheck(L_111);
		uint16_t L_113 = BitConverterEx_ToUInt16_m8_363(L_111, L_112, 0, /*hidden argument*/NULL);
		ExifEnumProperty_1_t8_348 * L_114 = (ExifEnumProperty_1_t8_348 *)il2cpp_codegen_object_new (ExifEnumProperty_1_t8_348_il2cpp_TypeInfo_var);
		ExifEnumProperty_1__ctor_m8_1984(L_114, ((int32_t)237385), L_113, 1, /*hidden argument*/ExifEnumProperty_1__ctor_m8_1984_MethodInfo_var);
		return L_114;
	}

IL_02e3:
	{
		uint16_t L_115 = ___tag;
		if ((!(((uint32_t)L_115) == ((uint32_t)((int32_t)37396)))))
		{
			goto IL_0338;
		}
	}
	{
		uint32_t L_116 = ___count;
		if ((!(((uint32_t)L_116) == ((uint32_t)3))))
		{
			goto IL_0309;
		}
	}
	{
		ByteU5BU5D_t1_109* L_117 = ___value;
		uint32_t L_118 = ___count;
		int32_t L_119 = ___byteOrder;
		UInt16U5BU5D_t1_1231* L_120 = ExifBitConverter_ToUShortArray_m8_390(NULL /*static, unused*/, L_117, L_118, L_119, /*hidden argument*/NULL);
		ExifCircularSubjectArea_t8_104 * L_121 = (ExifCircularSubjectArea_t8_104 *)il2cpp_codegen_object_new (ExifCircularSubjectArea_t8_104_il2cpp_TypeInfo_var);
		ExifCircularSubjectArea__ctor_m8_444(L_121, ((int32_t)237396), L_120, /*hidden argument*/NULL);
		return L_121;
	}

IL_0309:
	{
		uint32_t L_122 = ___count;
		if ((!(((uint32_t)L_122) == ((uint32_t)4))))
		{
			goto IL_0324;
		}
	}
	{
		ByteU5BU5D_t1_109* L_123 = ___value;
		uint32_t L_124 = ___count;
		int32_t L_125 = ___byteOrder;
		UInt16U5BU5D_t1_1231* L_126 = ExifBitConverter_ToUShortArray_m8_390(NULL /*static, unused*/, L_123, L_124, L_125, /*hidden argument*/NULL);
		ExifRectangularSubjectArea_t8_105 * L_127 = (ExifRectangularSubjectArea_t8_105 *)il2cpp_codegen_object_new (ExifRectangularSubjectArea_t8_105_il2cpp_TypeInfo_var);
		ExifRectangularSubjectArea__ctor_m8_448(L_127, ((int32_t)237396), L_126, /*hidden argument*/NULL);
		return L_127;
	}

IL_0324:
	{
		ByteU5BU5D_t1_109* L_128 = ___value;
		uint32_t L_129 = ___count;
		int32_t L_130 = ___byteOrder;
		UInt16U5BU5D_t1_1231* L_131 = ExifBitConverter_ToUShortArray_m8_390(NULL /*static, unused*/, L_128, L_129, L_130, /*hidden argument*/NULL);
		ExifPointSubjectArea_t8_102 * L_132 = (ExifPointSubjectArea_t8_102 *)il2cpp_codegen_object_new (ExifPointSubjectArea_t8_102_il2cpp_TypeInfo_var);
		ExifPointSubjectArea__ctor_m8_436(L_132, ((int32_t)237396), L_131, /*hidden argument*/NULL);
		return L_132;
	}

IL_0338:
	{
		uint16_t L_133 = ___tag;
		if ((!(((uint32_t)L_133) == ((uint32_t)((int32_t)41488)))))
		{
			goto IL_0357;
		}
	}
	{
		BitConverterEx_t8_61 * L_134 = V_0;
		ByteU5BU5D_t1_109* L_135 = ___value;
		NullCheck(L_134);
		uint16_t L_136 = BitConverterEx_ToUInt16_m8_363(L_134, L_135, 0, /*hidden argument*/NULL);
		ExifEnumProperty_1_t8_343 * L_137 = (ExifEnumProperty_1_t8_343 *)il2cpp_codegen_object_new (ExifEnumProperty_1_t8_343_il2cpp_TypeInfo_var);
		ExifEnumProperty_1__ctor_m8_1985(L_137, ((int32_t)241488), L_136, 1, /*hidden argument*/ExifEnumProperty_1__ctor_m8_1985_MethodInfo_var);
		return L_137;
	}

IL_0357:
	{
		uint16_t L_138 = ___tag;
		if ((!(((uint32_t)L_138) == ((uint32_t)((int32_t)41492)))))
		{
			goto IL_0376;
		}
	}
	{
		ByteU5BU5D_t1_109* L_139 = ___value;
		uint32_t L_140 = ___count;
		int32_t L_141 = ___byteOrder;
		UInt16U5BU5D_t1_1231* L_142 = ExifBitConverter_ToUShortArray_m8_390(NULL /*static, unused*/, L_139, L_140, L_141, /*hidden argument*/NULL);
		ExifPointSubjectArea_t8_102 * L_143 = (ExifPointSubjectArea_t8_102 *)il2cpp_codegen_object_new (ExifPointSubjectArea_t8_102_il2cpp_TypeInfo_var);
		ExifPointSubjectArea__ctor_m8_436(L_143, ((int32_t)241492), L_142, /*hidden argument*/NULL);
		return L_143;
	}

IL_0376:
	{
		uint16_t L_144 = ___tag;
		if ((!(((uint32_t)L_144) == ((uint32_t)((int32_t)41495)))))
		{
			goto IL_0395;
		}
	}
	{
		BitConverterEx_t8_61 * L_145 = V_0;
		ByteU5BU5D_t1_109* L_146 = ___value;
		NullCheck(L_145);
		uint16_t L_147 = BitConverterEx_ToUInt16_m8_363(L_145, L_146, 0, /*hidden argument*/NULL);
		ExifEnumProperty_1_t8_349 * L_148 = (ExifEnumProperty_1_t8_349 *)il2cpp_codegen_object_new (ExifEnumProperty_1_t8_349_il2cpp_TypeInfo_var);
		ExifEnumProperty_1__ctor_m8_1986(L_148, ((int32_t)241495), L_147, 1, /*hidden argument*/ExifEnumProperty_1__ctor_m8_1986_MethodInfo_var);
		return L_148;
	}

IL_0395:
	{
		uint16_t L_149 = ___tag;
		if ((!(((uint32_t)L_149) == ((uint32_t)((int32_t)41728)))))
		{
			goto IL_03b5;
		}
	}
	{
		BitConverterEx_t8_61 * L_150 = V_0;
		ByteU5BU5D_t1_109* L_151 = ___value;
		NullCheck(L_150);
		uint16_t L_152 = BitConverterEx_ToUInt16_m8_363(L_150, L_151, 0, /*hidden argument*/NULL);
		ExifEnumProperty_1_t8_350 * L_153 = (ExifEnumProperty_1_t8_350 *)il2cpp_codegen_object_new (ExifEnumProperty_1_t8_350_il2cpp_TypeInfo_var);
		ExifEnumProperty_1__ctor_m8_1987(L_153, ((int32_t)241728), (((int32_t)((uint8_t)L_152))), 1, /*hidden argument*/ExifEnumProperty_1__ctor_m8_1987_MethodInfo_var);
		return L_153;
	}

IL_03b5:
	{
		uint16_t L_154 = ___tag;
		if ((!(((uint32_t)L_154) == ((uint32_t)((int32_t)41729)))))
		{
			goto IL_03d5;
		}
	}
	{
		BitConverterEx_t8_61 * L_155 = V_0;
		ByteU5BU5D_t1_109* L_156 = ___value;
		NullCheck(L_155);
		uint16_t L_157 = BitConverterEx_ToUInt16_m8_363(L_155, L_156, 0, /*hidden argument*/NULL);
		ExifEnumProperty_1_t8_351 * L_158 = (ExifEnumProperty_1_t8_351 *)il2cpp_codegen_object_new (ExifEnumProperty_1_t8_351_il2cpp_TypeInfo_var);
		ExifEnumProperty_1__ctor_m8_1988(L_158, ((int32_t)241729), (((int32_t)((uint8_t)L_157))), 1, /*hidden argument*/ExifEnumProperty_1__ctor_m8_1988_MethodInfo_var);
		return L_158;
	}

IL_03d5:
	{
		uint16_t L_159 = ___tag;
		if ((!(((uint32_t)L_159) == ((uint32_t)((int32_t)41985)))))
		{
			goto IL_03f4;
		}
	}
	{
		BitConverterEx_t8_61 * L_160 = V_0;
		ByteU5BU5D_t1_109* L_161 = ___value;
		NullCheck(L_160);
		uint16_t L_162 = BitConverterEx_ToUInt16_m8_363(L_160, L_161, 0, /*hidden argument*/NULL);
		ExifEnumProperty_1_t8_352 * L_163 = (ExifEnumProperty_1_t8_352 *)il2cpp_codegen_object_new (ExifEnumProperty_1_t8_352_il2cpp_TypeInfo_var);
		ExifEnumProperty_1__ctor_m8_1989(L_163, ((int32_t)241985), L_162, 1, /*hidden argument*/ExifEnumProperty_1__ctor_m8_1989_MethodInfo_var);
		return L_163;
	}

IL_03f4:
	{
		uint16_t L_164 = ___tag;
		if ((!(((uint32_t)L_164) == ((uint32_t)((int32_t)41986)))))
		{
			goto IL_0413;
		}
	}
	{
		BitConverterEx_t8_61 * L_165 = V_0;
		ByteU5BU5D_t1_109* L_166 = ___value;
		NullCheck(L_165);
		uint16_t L_167 = BitConverterEx_ToUInt16_m8_363(L_165, L_166, 0, /*hidden argument*/NULL);
		ExifEnumProperty_1_t8_353 * L_168 = (ExifEnumProperty_1_t8_353 *)il2cpp_codegen_object_new (ExifEnumProperty_1_t8_353_il2cpp_TypeInfo_var);
		ExifEnumProperty_1__ctor_m8_1990(L_168, ((int32_t)241986), L_167, 1, /*hidden argument*/ExifEnumProperty_1__ctor_m8_1990_MethodInfo_var);
		return L_168;
	}

IL_0413:
	{
		uint16_t L_169 = ___tag;
		if ((!(((uint32_t)L_169) == ((uint32_t)((int32_t)41987)))))
		{
			goto IL_0432;
		}
	}
	{
		BitConverterEx_t8_61 * L_170 = V_0;
		ByteU5BU5D_t1_109* L_171 = ___value;
		NullCheck(L_170);
		uint16_t L_172 = BitConverterEx_ToUInt16_m8_363(L_170, L_171, 0, /*hidden argument*/NULL);
		ExifEnumProperty_1_t8_354 * L_173 = (ExifEnumProperty_1_t8_354 *)il2cpp_codegen_object_new (ExifEnumProperty_1_t8_354_il2cpp_TypeInfo_var);
		ExifEnumProperty_1__ctor_m8_1991(L_173, ((int32_t)241987), L_172, 1, /*hidden argument*/ExifEnumProperty_1__ctor_m8_1991_MethodInfo_var);
		return L_173;
	}

IL_0432:
	{
		uint16_t L_174 = ___tag;
		if ((!(((uint32_t)L_174) == ((uint32_t)((int32_t)41990)))))
		{
			goto IL_0451;
		}
	}
	{
		BitConverterEx_t8_61 * L_175 = V_0;
		ByteU5BU5D_t1_109* L_176 = ___value;
		NullCheck(L_175);
		uint16_t L_177 = BitConverterEx_ToUInt16_m8_363(L_175, L_176, 0, /*hidden argument*/NULL);
		ExifEnumProperty_1_t8_355 * L_178 = (ExifEnumProperty_1_t8_355 *)il2cpp_codegen_object_new (ExifEnumProperty_1_t8_355_il2cpp_TypeInfo_var);
		ExifEnumProperty_1__ctor_m8_1992(L_178, ((int32_t)241990), L_177, 1, /*hidden argument*/ExifEnumProperty_1__ctor_m8_1992_MethodInfo_var);
		return L_178;
	}

IL_0451:
	{
		uint16_t L_179 = ___tag;
		if ((!(((uint32_t)L_179) == ((uint32_t)((int32_t)41991)))))
		{
			goto IL_0470;
		}
	}
	{
		BitConverterEx_t8_61 * L_180 = V_0;
		ByteU5BU5D_t1_109* L_181 = ___value;
		NullCheck(L_180);
		uint16_t L_182 = BitConverterEx_ToUInt16_m8_363(L_180, L_181, 0, /*hidden argument*/NULL);
		ExifEnumProperty_1_t8_356 * L_183 = (ExifEnumProperty_1_t8_356 *)il2cpp_codegen_object_new (ExifEnumProperty_1_t8_356_il2cpp_TypeInfo_var);
		ExifEnumProperty_1__ctor_m8_1993(L_183, ((int32_t)241991), L_182, 1, /*hidden argument*/ExifEnumProperty_1__ctor_m8_1993_MethodInfo_var);
		return L_183;
	}

IL_0470:
	{
		uint16_t L_184 = ___tag;
		if ((!(((uint32_t)L_184) == ((uint32_t)((int32_t)41992)))))
		{
			goto IL_048f;
		}
	}
	{
		BitConverterEx_t8_61 * L_185 = V_0;
		ByteU5BU5D_t1_109* L_186 = ___value;
		NullCheck(L_185);
		uint16_t L_187 = BitConverterEx_ToUInt16_m8_363(L_185, L_186, 0, /*hidden argument*/NULL);
		ExifEnumProperty_1_t8_357 * L_188 = (ExifEnumProperty_1_t8_357 *)il2cpp_codegen_object_new (ExifEnumProperty_1_t8_357_il2cpp_TypeInfo_var);
		ExifEnumProperty_1__ctor_m8_1994(L_188, ((int32_t)241992), L_187, 1, /*hidden argument*/ExifEnumProperty_1__ctor_m8_1994_MethodInfo_var);
		return L_188;
	}

IL_048f:
	{
		uint16_t L_189 = ___tag;
		if ((!(((uint32_t)L_189) == ((uint32_t)((int32_t)41993)))))
		{
			goto IL_04ae;
		}
	}
	{
		BitConverterEx_t8_61 * L_190 = V_0;
		ByteU5BU5D_t1_109* L_191 = ___value;
		NullCheck(L_190);
		uint16_t L_192 = BitConverterEx_ToUInt16_m8_363(L_190, L_191, 0, /*hidden argument*/NULL);
		ExifEnumProperty_1_t8_358 * L_193 = (ExifEnumProperty_1_t8_358 *)il2cpp_codegen_object_new (ExifEnumProperty_1_t8_358_il2cpp_TypeInfo_var);
		ExifEnumProperty_1__ctor_m8_1995(L_193, ((int32_t)241993), L_192, 1, /*hidden argument*/ExifEnumProperty_1__ctor_m8_1995_MethodInfo_var);
		return L_193;
	}

IL_04ae:
	{
		uint16_t L_194 = ___tag;
		if ((!(((uint32_t)L_194) == ((uint32_t)((int32_t)41994)))))
		{
			goto IL_04cd;
		}
	}
	{
		BitConverterEx_t8_61 * L_195 = V_0;
		ByteU5BU5D_t1_109* L_196 = ___value;
		NullCheck(L_195);
		uint16_t L_197 = BitConverterEx_ToUInt16_m8_363(L_195, L_196, 0, /*hidden argument*/NULL);
		ExifEnumProperty_1_t8_359 * L_198 = (ExifEnumProperty_1_t8_359 *)il2cpp_codegen_object_new (ExifEnumProperty_1_t8_359_il2cpp_TypeInfo_var);
		ExifEnumProperty_1__ctor_m8_1996(L_198, ((int32_t)241994), L_197, 1, /*hidden argument*/ExifEnumProperty_1__ctor_m8_1996_MethodInfo_var);
		return L_198;
	}

IL_04cd:
	{
		uint16_t L_199 = ___tag;
		if ((!(((uint32_t)L_199) == ((uint32_t)((int32_t)41996)))))
		{
			goto IL_04ec;
		}
	}
	{
		BitConverterEx_t8_61 * L_200 = V_0;
		ByteU5BU5D_t1_109* L_201 = ___value;
		NullCheck(L_200);
		uint16_t L_202 = BitConverterEx_ToUInt16_m8_363(L_200, L_201, 0, /*hidden argument*/NULL);
		ExifEnumProperty_1_t8_360 * L_203 = (ExifEnumProperty_1_t8_360 *)il2cpp_codegen_object_new (ExifEnumProperty_1_t8_360_il2cpp_TypeInfo_var);
		ExifEnumProperty_1__ctor_m8_1997(L_203, ((int32_t)237382), L_202, 1, /*hidden argument*/ExifEnumProperty_1__ctor_m8_1997_MethodInfo_var);
		return L_203;
	}

IL_04ec:
	{
		goto IL_07f9;
	}

IL_04f1:
	{
		int32_t L_204 = ___ifd;
		if ((!(((uint32_t)L_204) == ((uint32_t)((int32_t)300000)))))
		{
			goto IL_06dc;
		}
	}
	{
		uint16_t L_205 = ___tag;
		if (L_205)
		{
			goto IL_0514;
		}
	}
	{
		ByteU5BU5D_t1_109* L_206 = ___value;
		String_t* L_207 = ExifBitConverter_ToString_m8_385(NULL /*static, unused*/, L_206, /*hidden argument*/NULL);
		ExifVersion_t8_101 * L_208 = (ExifVersion_t8_101 *)il2cpp_codegen_object_new (ExifVersion_t8_101_il2cpp_TypeInfo_var);
		ExifVersion__ctor_m8_429(L_208, ((int32_t)300000), L_207, /*hidden argument*/NULL);
		return L_208;
	}

IL_0514:
	{
		uint16_t L_209 = ___tag;
		if ((!(((uint32_t)L_209) == ((uint32_t)1))))
		{
			goto IL_0529;
		}
	}
	{
		ByteU5BU5D_t1_109* L_210 = ___value;
		NullCheck(L_210);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_210, 0);
		int32_t L_211 = 0;
		ExifEnumProperty_1_t8_361 * L_212 = (ExifEnumProperty_1_t8_361 *)il2cpp_codegen_object_new (ExifEnumProperty_1_t8_361_il2cpp_TypeInfo_var);
		ExifEnumProperty_1__ctor_m8_1998(L_212, ((int32_t)300001), (*(uint8_t*)(uint8_t*)SZArrayLdElema(L_210, L_211, sizeof(uint8_t))), /*hidden argument*/ExifEnumProperty_1__ctor_m8_1998_MethodInfo_var);
		return L_212;
	}

IL_0529:
	{
		uint16_t L_213 = ___tag;
		if ((!(((uint32_t)L_213) == ((uint32_t)2))))
		{
			goto IL_0544;
		}
	}
	{
		ByteU5BU5D_t1_109* L_214 = ___value;
		uint32_t L_215 = ___count;
		int32_t L_216 = ___byteOrder;
		UFraction32U5BU5D_t8_122* L_217 = ExifBitConverter_ToURationalArray_m8_393(NULL /*static, unused*/, L_214, L_215, L_216, /*hidden argument*/NULL);
		GPSLatitudeLongitude_t8_106 * L_218 = (GPSLatitudeLongitude_t8_106 *)il2cpp_codegen_object_new (GPSLatitudeLongitude_t8_106_il2cpp_TypeInfo_var);
		GPSLatitudeLongitude__ctor_m8_454(L_218, ((int32_t)300002), L_217, /*hidden argument*/NULL);
		return L_218;
	}

IL_0544:
	{
		uint16_t L_219 = ___tag;
		if ((!(((uint32_t)L_219) == ((uint32_t)3))))
		{
			goto IL_0559;
		}
	}
	{
		ByteU5BU5D_t1_109* L_220 = ___value;
		NullCheck(L_220);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_220, 0);
		int32_t L_221 = 0;
		ExifEnumProperty_1_t8_362 * L_222 = (ExifEnumProperty_1_t8_362 *)il2cpp_codegen_object_new (ExifEnumProperty_1_t8_362_il2cpp_TypeInfo_var);
		ExifEnumProperty_1__ctor_m8_1999(L_222, ((int32_t)300003), (*(uint8_t*)(uint8_t*)SZArrayLdElema(L_220, L_221, sizeof(uint8_t))), /*hidden argument*/ExifEnumProperty_1__ctor_m8_1999_MethodInfo_var);
		return L_222;
	}

IL_0559:
	{
		uint16_t L_223 = ___tag;
		if ((!(((uint32_t)L_223) == ((uint32_t)4))))
		{
			goto IL_0574;
		}
	}
	{
		ByteU5BU5D_t1_109* L_224 = ___value;
		uint32_t L_225 = ___count;
		int32_t L_226 = ___byteOrder;
		UFraction32U5BU5D_t8_122* L_227 = ExifBitConverter_ToURationalArray_m8_393(NULL /*static, unused*/, L_224, L_225, L_226, /*hidden argument*/NULL);
		GPSLatitudeLongitude_t8_106 * L_228 = (GPSLatitudeLongitude_t8_106 *)il2cpp_codegen_object_new (GPSLatitudeLongitude_t8_106_il2cpp_TypeInfo_var);
		GPSLatitudeLongitude__ctor_m8_454(L_228, ((int32_t)300004), L_227, /*hidden argument*/NULL);
		return L_228;
	}

IL_0574:
	{
		uint16_t L_229 = ___tag;
		if ((!(((uint32_t)L_229) == ((uint32_t)5))))
		{
			goto IL_0589;
		}
	}
	{
		ByteU5BU5D_t1_109* L_230 = ___value;
		NullCheck(L_230);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_230, 0);
		int32_t L_231 = 0;
		ExifEnumProperty_1_t8_363 * L_232 = (ExifEnumProperty_1_t8_363 *)il2cpp_codegen_object_new (ExifEnumProperty_1_t8_363_il2cpp_TypeInfo_var);
		ExifEnumProperty_1__ctor_m8_2000(L_232, ((int32_t)300005), (*(uint8_t*)(uint8_t*)SZArrayLdElema(L_230, L_231, sizeof(uint8_t))), /*hidden argument*/ExifEnumProperty_1__ctor_m8_2000_MethodInfo_var);
		return L_232;
	}

IL_0589:
	{
		uint16_t L_233 = ___tag;
		if ((!(((uint32_t)L_233) == ((uint32_t)7))))
		{
			goto IL_05a4;
		}
	}
	{
		ByteU5BU5D_t1_109* L_234 = ___value;
		uint32_t L_235 = ___count;
		int32_t L_236 = ___byteOrder;
		UFraction32U5BU5D_t8_122* L_237 = ExifBitConverter_ToURationalArray_m8_393(NULL /*static, unused*/, L_234, L_235, L_236, /*hidden argument*/NULL);
		GPSTimeStamp_t8_108 * L_238 = (GPSTimeStamp_t8_108 *)il2cpp_codegen_object_new (GPSTimeStamp_t8_108_il2cpp_TypeInfo_var);
		GPSTimeStamp__ctor_m8_466(L_238, ((int32_t)300007), L_237, /*hidden argument*/NULL);
		return L_238;
	}

IL_05a4:
	{
		uint16_t L_239 = ___tag;
		if ((!(((uint32_t)L_239) == ((uint32_t)((int32_t)9)))))
		{
			goto IL_05ba;
		}
	}
	{
		ByteU5BU5D_t1_109* L_240 = ___value;
		NullCheck(L_240);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_240, 0);
		int32_t L_241 = 0;
		ExifEnumProperty_1_t8_364 * L_242 = (ExifEnumProperty_1_t8_364 *)il2cpp_codegen_object_new (ExifEnumProperty_1_t8_364_il2cpp_TypeInfo_var);
		ExifEnumProperty_1__ctor_m8_2001(L_242, ((int32_t)300009), (*(uint8_t*)(uint8_t*)SZArrayLdElema(L_240, L_241, sizeof(uint8_t))), /*hidden argument*/ExifEnumProperty_1__ctor_m8_2001_MethodInfo_var);
		return L_242;
	}

IL_05ba:
	{
		uint16_t L_243 = ___tag;
		if ((!(((uint32_t)L_243) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_05d0;
		}
	}
	{
		ByteU5BU5D_t1_109* L_244 = ___value;
		NullCheck(L_244);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_244, 0);
		int32_t L_245 = 0;
		ExifEnumProperty_1_t8_365 * L_246 = (ExifEnumProperty_1_t8_365 *)il2cpp_codegen_object_new (ExifEnumProperty_1_t8_365_il2cpp_TypeInfo_var);
		ExifEnumProperty_1__ctor_m8_2002(L_246, ((int32_t)300010), (*(uint8_t*)(uint8_t*)SZArrayLdElema(L_244, L_245, sizeof(uint8_t))), /*hidden argument*/ExifEnumProperty_1__ctor_m8_2002_MethodInfo_var);
		return L_246;
	}

IL_05d0:
	{
		uint16_t L_247 = ___tag;
		if ((!(((uint32_t)L_247) == ((uint32_t)((int32_t)12)))))
		{
			goto IL_05e6;
		}
	}
	{
		ByteU5BU5D_t1_109* L_248 = ___value;
		NullCheck(L_248);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_248, 0);
		int32_t L_249 = 0;
		ExifEnumProperty_1_t8_366 * L_250 = (ExifEnumProperty_1_t8_366 *)il2cpp_codegen_object_new (ExifEnumProperty_1_t8_366_il2cpp_TypeInfo_var);
		ExifEnumProperty_1__ctor_m8_2003(L_250, ((int32_t)300012), (*(uint8_t*)(uint8_t*)SZArrayLdElema(L_248, L_249, sizeof(uint8_t))), /*hidden argument*/ExifEnumProperty_1__ctor_m8_2003_MethodInfo_var);
		return L_250;
	}

IL_05e6:
	{
		uint16_t L_251 = ___tag;
		if ((!(((uint32_t)L_251) == ((uint32_t)((int32_t)14)))))
		{
			goto IL_05fc;
		}
	}
	{
		ByteU5BU5D_t1_109* L_252 = ___value;
		NullCheck(L_252);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_252, 0);
		int32_t L_253 = 0;
		ExifEnumProperty_1_t8_367 * L_254 = (ExifEnumProperty_1_t8_367 *)il2cpp_codegen_object_new (ExifEnumProperty_1_t8_367_il2cpp_TypeInfo_var);
		ExifEnumProperty_1__ctor_m8_2004(L_254, ((int32_t)300014), (*(uint8_t*)(uint8_t*)SZArrayLdElema(L_252, L_253, sizeof(uint8_t))), /*hidden argument*/ExifEnumProperty_1__ctor_m8_2004_MethodInfo_var);
		return L_254;
	}

IL_05fc:
	{
		uint16_t L_255 = ___tag;
		if ((!(((uint32_t)L_255) == ((uint32_t)((int32_t)16)))))
		{
			goto IL_0612;
		}
	}
	{
		ByteU5BU5D_t1_109* L_256 = ___value;
		NullCheck(L_256);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_256, 0);
		int32_t L_257 = 0;
		ExifEnumProperty_1_t8_367 * L_258 = (ExifEnumProperty_1_t8_367 *)il2cpp_codegen_object_new (ExifEnumProperty_1_t8_367_il2cpp_TypeInfo_var);
		ExifEnumProperty_1__ctor_m8_2004(L_258, ((int32_t)300016), (*(uint8_t*)(uint8_t*)SZArrayLdElema(L_256, L_257, sizeof(uint8_t))), /*hidden argument*/ExifEnumProperty_1__ctor_m8_2004_MethodInfo_var);
		return L_258;
	}

IL_0612:
	{
		uint16_t L_259 = ___tag;
		if ((!(((uint32_t)L_259) == ((uint32_t)((int32_t)19)))))
		{
			goto IL_0628;
		}
	}
	{
		ByteU5BU5D_t1_109* L_260 = ___value;
		NullCheck(L_260);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_260, 0);
		int32_t L_261 = 0;
		ExifEnumProperty_1_t8_361 * L_262 = (ExifEnumProperty_1_t8_361 *)il2cpp_codegen_object_new (ExifEnumProperty_1_t8_361_il2cpp_TypeInfo_var);
		ExifEnumProperty_1__ctor_m8_1998(L_262, ((int32_t)300019), (*(uint8_t*)(uint8_t*)SZArrayLdElema(L_260, L_261, sizeof(uint8_t))), /*hidden argument*/ExifEnumProperty_1__ctor_m8_1998_MethodInfo_var);
		return L_262;
	}

IL_0628:
	{
		uint16_t L_263 = ___tag;
		if ((!(((uint32_t)L_263) == ((uint32_t)((int32_t)20)))))
		{
			goto IL_0644;
		}
	}
	{
		ByteU5BU5D_t1_109* L_264 = ___value;
		uint32_t L_265 = ___count;
		int32_t L_266 = ___byteOrder;
		UFraction32U5BU5D_t8_122* L_267 = ExifBitConverter_ToURationalArray_m8_393(NULL /*static, unused*/, L_264, L_265, L_266, /*hidden argument*/NULL);
		GPSLatitudeLongitude_t8_106 * L_268 = (GPSLatitudeLongitude_t8_106 *)il2cpp_codegen_object_new (GPSLatitudeLongitude_t8_106_il2cpp_TypeInfo_var);
		GPSLatitudeLongitude__ctor_m8_454(L_268, ((int32_t)300020), L_267, /*hidden argument*/NULL);
		return L_268;
	}

IL_0644:
	{
		uint16_t L_269 = ___tag;
		if ((!(((uint32_t)L_269) == ((uint32_t)((int32_t)21)))))
		{
			goto IL_065a;
		}
	}
	{
		ByteU5BU5D_t1_109* L_270 = ___value;
		NullCheck(L_270);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_270, 0);
		int32_t L_271 = 0;
		ExifEnumProperty_1_t8_362 * L_272 = (ExifEnumProperty_1_t8_362 *)il2cpp_codegen_object_new (ExifEnumProperty_1_t8_362_il2cpp_TypeInfo_var);
		ExifEnumProperty_1__ctor_m8_1999(L_272, ((int32_t)300021), (*(uint8_t*)(uint8_t*)SZArrayLdElema(L_270, L_271, sizeof(uint8_t))), /*hidden argument*/ExifEnumProperty_1__ctor_m8_1999_MethodInfo_var);
		return L_272;
	}

IL_065a:
	{
		uint16_t L_273 = ___tag;
		if ((!(((uint32_t)L_273) == ((uint32_t)((int32_t)22)))))
		{
			goto IL_0676;
		}
	}
	{
		ByteU5BU5D_t1_109* L_274 = ___value;
		uint32_t L_275 = ___count;
		int32_t L_276 = ___byteOrder;
		UFraction32U5BU5D_t8_122* L_277 = ExifBitConverter_ToURationalArray_m8_393(NULL /*static, unused*/, L_274, L_275, L_276, /*hidden argument*/NULL);
		GPSLatitudeLongitude_t8_106 * L_278 = (GPSLatitudeLongitude_t8_106 *)il2cpp_codegen_object_new (GPSLatitudeLongitude_t8_106_il2cpp_TypeInfo_var);
		GPSLatitudeLongitude__ctor_m8_454(L_278, ((int32_t)300022), L_277, /*hidden argument*/NULL);
		return L_278;
	}

IL_0676:
	{
		uint16_t L_279 = ___tag;
		if ((!(((uint32_t)L_279) == ((uint32_t)((int32_t)23)))))
		{
			goto IL_068c;
		}
	}
	{
		ByteU5BU5D_t1_109* L_280 = ___value;
		NullCheck(L_280);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_280, 0);
		int32_t L_281 = 0;
		ExifEnumProperty_1_t8_367 * L_282 = (ExifEnumProperty_1_t8_367 *)il2cpp_codegen_object_new (ExifEnumProperty_1_t8_367_il2cpp_TypeInfo_var);
		ExifEnumProperty_1__ctor_m8_2004(L_282, ((int32_t)300023), (*(uint8_t*)(uint8_t*)SZArrayLdElema(L_280, L_281, sizeof(uint8_t))), /*hidden argument*/ExifEnumProperty_1__ctor_m8_2004_MethodInfo_var);
		return L_282;
	}

IL_068c:
	{
		uint16_t L_283 = ___tag;
		if ((!(((uint32_t)L_283) == ((uint32_t)((int32_t)25)))))
		{
			goto IL_06a2;
		}
	}
	{
		ByteU5BU5D_t1_109* L_284 = ___value;
		NullCheck(L_284);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_284, 0);
		int32_t L_285 = 0;
		ExifEnumProperty_1_t8_368 * L_286 = (ExifEnumProperty_1_t8_368 *)il2cpp_codegen_object_new (ExifEnumProperty_1_t8_368_il2cpp_TypeInfo_var);
		ExifEnumProperty_1__ctor_m8_2005(L_286, ((int32_t)300025), (*(uint8_t*)(uint8_t*)SZArrayLdElema(L_284, L_285, sizeof(uint8_t))), /*hidden argument*/ExifEnumProperty_1__ctor_m8_2005_MethodInfo_var);
		return L_286;
	}

IL_06a2:
	{
		uint16_t L_287 = ___tag;
		if ((!(((uint32_t)L_287) == ((uint32_t)((int32_t)29)))))
		{
			goto IL_06bc;
		}
	}
	{
		ByteU5BU5D_t1_109* L_288 = ___value;
		DateTime_t1_150  L_289 = ExifBitConverter_ToDateTime_m8_386(NULL /*static, unused*/, L_288, 0, /*hidden argument*/NULL);
		ExifDateTime_t8_100 * L_290 = (ExifDateTime_t8_100 *)il2cpp_codegen_object_new (ExifDateTime_t8_100_il2cpp_TypeInfo_var);
		ExifDateTime__ctor_m8_421(L_290, ((int32_t)300029), L_289, /*hidden argument*/NULL);
		return L_290;
	}

IL_06bc:
	{
		uint16_t L_291 = ___tag;
		if ((!(((uint32_t)L_291) == ((uint32_t)((int32_t)30)))))
		{
			goto IL_06d7;
		}
	}
	{
		BitConverterEx_t8_61 * L_292 = V_0;
		ByteU5BU5D_t1_109* L_293 = ___value;
		NullCheck(L_292);
		uint16_t L_294 = BitConverterEx_ToUInt16_m8_363(L_292, L_293, 0, /*hidden argument*/NULL);
		ExifEnumProperty_1_t8_369 * L_295 = (ExifEnumProperty_1_t8_369 *)il2cpp_codegen_object_new (ExifEnumProperty_1_t8_369_il2cpp_TypeInfo_var);
		ExifEnumProperty_1__ctor_m8_2006(L_295, ((int32_t)300030), L_294, /*hidden argument*/ExifEnumProperty_1__ctor_m8_2006_MethodInfo_var);
		return L_295;
	}

IL_06d7:
	{
		goto IL_07f9;
	}

IL_06dc:
	{
		int32_t L_296 = ___ifd;
		if ((!(((uint32_t)L_296) == ((uint32_t)((int32_t)400000)))))
		{
			goto IL_071d;
		}
	}
	{
		uint16_t L_297 = ___tag;
		if ((!(((uint32_t)L_297) == ((uint32_t)1))))
		{
			goto IL_0700;
		}
	}
	{
		ByteU5BU5D_t1_109* L_298 = ___value;
		String_t* L_299 = ExifBitConverter_ToAscii_m8_384(NULL /*static, unused*/, L_298, /*hidden argument*/NULL);
		ExifAscii_t8_116 * L_300 = (ExifAscii_t8_116 *)il2cpp_codegen_object_new (ExifAscii_t8_116_il2cpp_TypeInfo_var);
		ExifAscii__ctor_m8_525(L_300, ((int32_t)400001), L_299, /*hidden argument*/NULL);
		return L_300;
	}

IL_0700:
	{
		uint16_t L_301 = ___tag;
		if ((!(((uint32_t)L_301) == ((uint32_t)2))))
		{
			goto IL_0718;
		}
	}
	{
		ByteU5BU5D_t1_109* L_302 = ___value;
		String_t* L_303 = ExifBitConverter_ToAscii_m8_384(NULL /*static, unused*/, L_302, /*hidden argument*/NULL);
		ExifVersion_t8_101 * L_304 = (ExifVersion_t8_101 *)il2cpp_codegen_object_new (ExifVersion_t8_101_il2cpp_TypeInfo_var);
		ExifVersion__ctor_m8_429(L_304, ((int32_t)400002), L_303, /*hidden argument*/NULL);
		return L_304;
	}

IL_0718:
	{
		goto IL_07f9;
	}

IL_071d:
	{
		int32_t L_305 = ___ifd;
		if ((!(((uint32_t)L_305) == ((uint32_t)((int32_t)500000)))))
		{
			goto IL_07f9;
		}
	}
	{
		uint16_t L_306 = ___tag;
		if ((!(((uint32_t)L_306) == ((uint32_t)((int32_t)259)))))
		{
			goto IL_0747;
		}
	}
	{
		BitConverterEx_t8_61 * L_307 = V_0;
		ByteU5BU5D_t1_109* L_308 = ___value;
		NullCheck(L_307);
		uint16_t L_309 = BitConverterEx_ToUInt16_m8_363(L_307, L_308, 0, /*hidden argument*/NULL);
		ExifEnumProperty_1_t8_338 * L_310 = (ExifEnumProperty_1_t8_338 *)il2cpp_codegen_object_new (ExifEnumProperty_1_t8_338_il2cpp_TypeInfo_var);
		ExifEnumProperty_1__ctor_m8_1974(L_310, ((int32_t)500259), L_309, /*hidden argument*/ExifEnumProperty_1__ctor_m8_1974_MethodInfo_var);
		return L_310;
	}

IL_0747:
	{
		uint16_t L_311 = ___tag;
		if ((!(((uint32_t)L_311) == ((uint32_t)((int32_t)262)))))
		{
			goto IL_0765;
		}
	}
	{
		BitConverterEx_t8_61 * L_312 = V_0;
		ByteU5BU5D_t1_109* L_313 = ___value;
		NullCheck(L_312);
		uint16_t L_314 = BitConverterEx_ToUInt16_m8_363(L_312, L_313, 0, /*hidden argument*/NULL);
		ExifEnumProperty_1_t8_339 * L_315 = (ExifEnumProperty_1_t8_339 *)il2cpp_codegen_object_new (ExifEnumProperty_1_t8_339_il2cpp_TypeInfo_var);
		ExifEnumProperty_1__ctor_m8_1975(L_315, ((int32_t)500262), L_314, /*hidden argument*/ExifEnumProperty_1__ctor_m8_1975_MethodInfo_var);
		return L_315;
	}

IL_0765:
	{
		uint16_t L_316 = ___tag;
		if ((!(((uint32_t)L_316) == ((uint32_t)((int32_t)274)))))
		{
			goto IL_0783;
		}
	}
	{
		BitConverterEx_t8_61 * L_317 = V_0;
		ByteU5BU5D_t1_109* L_318 = ___value;
		NullCheck(L_317);
		uint16_t L_319 = BitConverterEx_ToUInt16_m8_363(L_317, L_318, 0, /*hidden argument*/NULL);
		ExifEnumProperty_1_t8_340 * L_320 = (ExifEnumProperty_1_t8_340 *)il2cpp_codegen_object_new (ExifEnumProperty_1_t8_340_il2cpp_TypeInfo_var);
		ExifEnumProperty_1__ctor_m8_1976(L_320, ((int32_t)500274), L_319, /*hidden argument*/ExifEnumProperty_1__ctor_m8_1976_MethodInfo_var);
		return L_320;
	}

IL_0783:
	{
		uint16_t L_321 = ___tag;
		if ((!(((uint32_t)L_321) == ((uint32_t)((int32_t)284)))))
		{
			goto IL_07a1;
		}
	}
	{
		BitConverterEx_t8_61 * L_322 = V_0;
		ByteU5BU5D_t1_109* L_323 = ___value;
		NullCheck(L_322);
		uint16_t L_324 = BitConverterEx_ToUInt16_m8_363(L_322, L_323, 0, /*hidden argument*/NULL);
		ExifEnumProperty_1_t8_341 * L_325 = (ExifEnumProperty_1_t8_341 *)il2cpp_codegen_object_new (ExifEnumProperty_1_t8_341_il2cpp_TypeInfo_var);
		ExifEnumProperty_1__ctor_m8_1977(L_325, ((int32_t)500284), L_324, /*hidden argument*/ExifEnumProperty_1__ctor_m8_1977_MethodInfo_var);
		return L_325;
	}

IL_07a1:
	{
		uint16_t L_326 = ___tag;
		if ((!(((uint32_t)L_326) == ((uint32_t)((int32_t)531)))))
		{
			goto IL_07bf;
		}
	}
	{
		BitConverterEx_t8_61 * L_327 = V_0;
		ByteU5BU5D_t1_109* L_328 = ___value;
		NullCheck(L_327);
		uint16_t L_329 = BitConverterEx_ToUInt16_m8_363(L_327, L_328, 0, /*hidden argument*/NULL);
		ExifEnumProperty_1_t8_342 * L_330 = (ExifEnumProperty_1_t8_342 *)il2cpp_codegen_object_new (ExifEnumProperty_1_t8_342_il2cpp_TypeInfo_var);
		ExifEnumProperty_1__ctor_m8_1978(L_330, ((int32_t)500531), L_329, /*hidden argument*/ExifEnumProperty_1__ctor_m8_1978_MethodInfo_var);
		return L_330;
	}

IL_07bf:
	{
		uint16_t L_331 = ___tag;
		if ((!(((uint32_t)L_331) == ((uint32_t)((int32_t)296)))))
		{
			goto IL_07dd;
		}
	}
	{
		BitConverterEx_t8_61 * L_332 = V_0;
		ByteU5BU5D_t1_109* L_333 = ___value;
		NullCheck(L_332);
		uint16_t L_334 = BitConverterEx_ToUInt16_m8_363(L_332, L_333, 0, /*hidden argument*/NULL);
		ExifEnumProperty_1_t8_343 * L_335 = (ExifEnumProperty_1_t8_343 *)il2cpp_codegen_object_new (ExifEnumProperty_1_t8_343_il2cpp_TypeInfo_var);
		ExifEnumProperty_1__ctor_m8_1979(L_335, ((int32_t)500296), L_334, /*hidden argument*/ExifEnumProperty_1__ctor_m8_1979_MethodInfo_var);
		return L_335;
	}

IL_07dd:
	{
		uint16_t L_336 = ___tag;
		if ((!(((uint32_t)L_336) == ((uint32_t)((int32_t)306)))))
		{
			goto IL_07f9;
		}
	}
	{
		ByteU5BU5D_t1_109* L_337 = ___value;
		DateTime_t1_150  L_338 = ExifBitConverter_ToDateTime_m8_387(NULL /*static, unused*/, L_337, /*hidden argument*/NULL);
		ExifDateTime_t8_100 * L_339 = (ExifDateTime_t8_100 *)il2cpp_codegen_object_new (ExifDateTime_t8_100_il2cpp_TypeInfo_var);
		ExifDateTime__ctor_m8_421(L_339, ((int32_t)500306), L_338, /*hidden argument*/NULL);
		return L_339;
	}

IL_07f9:
	{
		int32_t L_340 = ___ifd;
		uint16_t L_341 = ___tag;
		int32_t L_342 = ExifTagFactory_GetExifTag_m8_629(NULL /*static, unused*/, L_340, L_341, /*hidden argument*/NULL);
		V_6 = L_342;
		uint16_t L_343 = ___type;
		if ((!(((uint32_t)L_343) == ((uint32_t)1))))
		{
			goto IL_0825;
		}
	}
	{
		uint32_t L_344 = ___count;
		if ((!(((uint32_t)L_344) == ((uint32_t)1))))
		{
			goto IL_081c;
		}
	}
	{
		int32_t L_345 = V_6;
		ByteU5BU5D_t1_109* L_346 = ___value;
		NullCheck(L_346);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_346, 0);
		int32_t L_347 = 0;
		ExifByte_t8_114 * L_348 = (ExifByte_t8_114 *)il2cpp_codegen_object_new (ExifByte_t8_114_il2cpp_TypeInfo_var);
		ExifByte__ctor_m8_509(L_348, L_345, (*(uint8_t*)(uint8_t*)SZArrayLdElema(L_346, L_347, sizeof(uint8_t))), /*hidden argument*/NULL);
		return L_348;
	}

IL_081c:
	{
		int32_t L_349 = V_6;
		ByteU5BU5D_t1_109* L_350 = ___value;
		ExifByteArray_t8_115 * L_351 = (ExifByteArray_t8_115 *)il2cpp_codegen_object_new (ExifByteArray_t8_115_il2cpp_TypeInfo_var);
		ExifByteArray__ctor_m8_517(L_351, L_349, L_350, /*hidden argument*/NULL);
		return L_351;
	}

IL_0825:
	{
		uint16_t L_352 = ___type;
		if ((!(((uint32_t)L_352) == ((uint32_t)2))))
		{
			goto IL_083a;
		}
	}
	{
		int32_t L_353 = V_6;
		ByteU5BU5D_t1_109* L_354 = ___value;
		String_t* L_355 = ExifBitConverter_ToAscii_m8_384(NULL /*static, unused*/, L_354, /*hidden argument*/NULL);
		ExifAscii_t8_116 * L_356 = (ExifAscii_t8_116 *)il2cpp_codegen_object_new (ExifAscii_t8_116_il2cpp_TypeInfo_var);
		ExifAscii__ctor_m8_525(L_356, L_353, L_355, /*hidden argument*/NULL);
		return L_356;
	}

IL_083a:
	{
		uint16_t L_357 = ___type;
		if ((!(((uint32_t)L_357) == ((uint32_t)3))))
		{
			goto IL_0869;
		}
	}
	{
		uint32_t L_358 = ___count;
		if ((!(((uint32_t)L_358) == ((uint32_t)1))))
		{
			goto IL_0858;
		}
	}
	{
		int32_t L_359 = V_6;
		BitConverterEx_t8_61 * L_360 = V_0;
		ByteU5BU5D_t1_109* L_361 = ___value;
		NullCheck(L_360);
		uint16_t L_362 = BitConverterEx_ToUInt16_m8_363(L_360, L_361, 0, /*hidden argument*/NULL);
		ExifUShort_t8_117 * L_363 = (ExifUShort_t8_117 *)il2cpp_codegen_object_new (ExifUShort_t8_117_il2cpp_TypeInfo_var);
		ExifUShort__ctor_m8_533(L_363, L_359, L_362, /*hidden argument*/NULL);
		return L_363;
	}

IL_0858:
	{
		int32_t L_364 = V_6;
		ByteU5BU5D_t1_109* L_365 = ___value;
		uint32_t L_366 = ___count;
		int32_t L_367 = ___byteOrder;
		UInt16U5BU5D_t1_1231* L_368 = ExifBitConverter_ToUShortArray_m8_390(NULL /*static, unused*/, L_365, L_366, L_367, /*hidden argument*/NULL);
		ExifUShortArray_t8_103 * L_369 = (ExifUShortArray_t8_103 *)il2cpp_codegen_object_new (ExifUShortArray_t8_103_il2cpp_TypeInfo_var);
		ExifUShortArray__ctor_m8_541(L_369, L_364, L_368, /*hidden argument*/NULL);
		return L_369;
	}

IL_0869:
	{
		uint16_t L_370 = ___type;
		if ((!(((uint32_t)L_370) == ((uint32_t)4))))
		{
			goto IL_0898;
		}
	}
	{
		uint32_t L_371 = ___count;
		if ((!(((uint32_t)L_371) == ((uint32_t)1))))
		{
			goto IL_0887;
		}
	}
	{
		int32_t L_372 = V_6;
		BitConverterEx_t8_61 * L_373 = V_0;
		ByteU5BU5D_t1_109* L_374 = ___value;
		NullCheck(L_373);
		uint32_t L_375 = BitConverterEx_ToUInt32_m8_364(L_373, L_374, 0, /*hidden argument*/NULL);
		ExifUInt_t8_118 * L_376 = (ExifUInt_t8_118 *)il2cpp_codegen_object_new (ExifUInt_t8_118_il2cpp_TypeInfo_var);
		ExifUInt__ctor_m8_549(L_376, L_372, L_375, /*hidden argument*/NULL);
		return L_376;
	}

IL_0887:
	{
		int32_t L_377 = V_6;
		ByteU5BU5D_t1_109* L_378 = ___value;
		uint32_t L_379 = ___count;
		int32_t L_380 = ___byteOrder;
		UInt32U5BU5D_t1_142* L_381 = ExifBitConverter_ToUIntArray_m8_391(NULL /*static, unused*/, L_378, L_379, L_380, /*hidden argument*/NULL);
		ExifUIntArray_t8_119 * L_382 = (ExifUIntArray_t8_119 *)il2cpp_codegen_object_new (ExifUIntArray_t8_119_il2cpp_TypeInfo_var);
		ExifUIntArray__ctor_m8_557(L_382, L_377, L_381, /*hidden argument*/NULL);
		return L_382;
	}

IL_0898:
	{
		uint16_t L_383 = ___type;
		if ((!(((uint32_t)L_383) == ((uint32_t)5))))
		{
			goto IL_08c7;
		}
	}
	{
		uint32_t L_384 = ___count;
		if ((!(((uint32_t)L_384) == ((uint32_t)1))))
		{
			goto IL_08b6;
		}
	}
	{
		int32_t L_385 = V_6;
		ByteU5BU5D_t1_109* L_386 = ___value;
		int32_t L_387 = ___byteOrder;
		UFraction32_t8_121  L_388 = ExifBitConverter_ToURational_m8_388(NULL /*static, unused*/, L_386, L_387, /*hidden argument*/NULL);
		ExifURational_t8_120 * L_389 = (ExifURational_t8_120 *)il2cpp_codegen_object_new (ExifURational_t8_120_il2cpp_TypeInfo_var);
		ExifURational__ctor_m8_566(L_389, L_385, L_388, /*hidden argument*/NULL);
		return L_389;
	}

IL_08b6:
	{
		int32_t L_390 = V_6;
		ByteU5BU5D_t1_109* L_391 = ___value;
		uint32_t L_392 = ___count;
		int32_t L_393 = ___byteOrder;
		UFraction32U5BU5D_t8_122* L_394 = ExifBitConverter_ToURationalArray_m8_393(NULL /*static, unused*/, L_391, L_392, L_393, /*hidden argument*/NULL);
		ExifURationalArray_t8_107 * L_395 = (ExifURationalArray_t8_107 *)il2cpp_codegen_object_new (ExifURationalArray_t8_107_il2cpp_TypeInfo_var);
		ExifURationalArray__ctor_m8_576(L_395, L_390, L_394, /*hidden argument*/NULL);
		return L_395;
	}

IL_08c7:
	{
		uint16_t L_396 = ___type;
		if ((!(((uint32_t)L_396) == ((uint32_t)7))))
		{
			goto IL_08d7;
		}
	}
	{
		int32_t L_397 = V_6;
		ByteU5BU5D_t1_109* L_398 = ___value;
		ExifUndefined_t8_123 * L_399 = (ExifUndefined_t8_123 *)il2cpp_codegen_object_new (ExifUndefined_t8_123_il2cpp_TypeInfo_var);
		ExifUndefined__ctor_m8_584(L_399, L_397, L_398, /*hidden argument*/NULL);
		return L_399;
	}

IL_08d7:
	{
		uint16_t L_400 = ___type;
		if ((!(((uint32_t)L_400) == ((uint32_t)((int32_t)9)))))
		{
			goto IL_0907;
		}
	}
	{
		uint32_t L_401 = ___count;
		if ((!(((uint32_t)L_401) == ((uint32_t)1))))
		{
			goto IL_08f6;
		}
	}
	{
		int32_t L_402 = V_6;
		BitConverterEx_t8_61 * L_403 = V_0;
		ByteU5BU5D_t1_109* L_404 = ___value;
		NullCheck(L_403);
		int32_t L_405 = BitConverterEx_ToInt32_m8_367(L_403, L_404, 0, /*hidden argument*/NULL);
		ExifSInt_t8_124 * L_406 = (ExifSInt_t8_124 *)il2cpp_codegen_object_new (ExifSInt_t8_124_il2cpp_TypeInfo_var);
		ExifSInt__ctor_m8_592(L_406, L_402, L_405, /*hidden argument*/NULL);
		return L_406;
	}

IL_08f6:
	{
		int32_t L_407 = V_6;
		ByteU5BU5D_t1_109* L_408 = ___value;
		uint32_t L_409 = ___count;
		int32_t L_410 = ___byteOrder;
		Int32U5BU5D_t1_275* L_411 = ExifBitConverter_ToSIntArray_m8_392(NULL /*static, unused*/, L_408, L_409, L_410, /*hidden argument*/NULL);
		ExifSIntArray_t8_125 * L_412 = (ExifSIntArray_t8_125 *)il2cpp_codegen_object_new (ExifSIntArray_t8_125_il2cpp_TypeInfo_var);
		ExifSIntArray__ctor_m8_600(L_412, L_407, L_411, /*hidden argument*/NULL);
		return L_412;
	}

IL_0907:
	{
		uint16_t L_413 = ___type;
		if ((!(((uint32_t)L_413) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_0937;
		}
	}
	{
		uint32_t L_414 = ___count;
		if ((!(((uint32_t)L_414) == ((uint32_t)1))))
		{
			goto IL_0926;
		}
	}
	{
		int32_t L_415 = V_6;
		ByteU5BU5D_t1_109* L_416 = ___value;
		int32_t L_417 = ___byteOrder;
		Fraction32_t8_127  L_418 = ExifBitConverter_ToSRational_m8_389(NULL /*static, unused*/, L_416, L_417, /*hidden argument*/NULL);
		ExifSRational_t8_126 * L_419 = (ExifSRational_t8_126 *)il2cpp_codegen_object_new (ExifSRational_t8_126_il2cpp_TypeInfo_var);
		ExifSRational__ctor_m8_609(L_419, L_415, L_418, /*hidden argument*/NULL);
		return L_419;
	}

IL_0926:
	{
		int32_t L_420 = V_6;
		ByteU5BU5D_t1_109* L_421 = ___value;
		uint32_t L_422 = ___count;
		int32_t L_423 = ___byteOrder;
		Fraction32U5BU5D_t8_129* L_424 = ExifBitConverter_ToSRationalArray_m8_394(NULL /*static, unused*/, L_421, L_422, L_423, /*hidden argument*/NULL);
		ExifSRationalArray_t8_128 * L_425 = (ExifSRationalArray_t8_128 *)il2cpp_codegen_object_new (ExifSRationalArray_t8_128_il2cpp_TypeInfo_var);
		ExifSRationalArray__ctor_m8_619(L_425, L_420, L_424, /*hidden argument*/NULL);
		return L_425;
	}

IL_0937:
	{
		ArgumentException_t1_1425 * L_426 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13260(L_426, _stringLiteral5224, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_426);
	}
}
// ExifLibrary.ExifTag ExifLibrary.ExifTagFactory::GetExifTag(ExifLibrary.IFD,System.UInt16)
extern "C" int32_t ExifTagFactory_GetExifTag_m8_629 (Object_t * __this /* static, unused */, int32_t ___ifd, uint16_t ___tagid, const MethodInfo* method)
{
	{
		int32_t L_0 = ___ifd;
		uint16_t L_1 = ___tagid;
		return (int32_t)(((int32_t)((int32_t)L_0+(int32_t)L_1)));
	}
}
// System.UInt16 ExifLibrary.ExifTagFactory::GetTagID(ExifLibrary.ExifTag)
extern "C" uint16_t ExifTagFactory_GetTagID_m8_630 (Object_t * __this /* static, unused */, int32_t ___exiftag, const MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		int32_t L_0 = ___exiftag;
		int32_t L_1 = ExifTagFactory_GetTagIFD_m8_631(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = ___exiftag;
		int32_t L_3 = V_0;
		return (((int32_t)((uint16_t)((int32_t)((int32_t)L_2-(int32_t)L_3)))));
	}
}
// ExifLibrary.IFD ExifLibrary.ExifTagFactory::GetTagIFD(ExifLibrary.ExifTag)
extern "C" int32_t ExifTagFactory_GetTagIFD_m8_631 (Object_t * __this /* static, unused */, int32_t ___tag, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		return (int32_t)(((int32_t)((int32_t)((int32_t)((int32_t)L_0/(int32_t)((int32_t)100000)))*(int32_t)((int32_t)100000))));
	}
}
// System.String ExifLibrary.ExifTagFactory::GetTagName(ExifLibrary.ExifTag)
extern const Il2CppType* ExifTag_t8_132_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* ExifTag_t8_132_il2cpp_TypeInfo_var;
extern TypeInfo* Enum_t1_24_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5225;
extern "C" String_t* ExifTagFactory_GetTagName_m8_632 (Object_t * __this /* static, unused */, int32_t ___tag, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExifTag_t8_132_0_0_0_var = il2cpp_codegen_type_from_index(1983);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		ExifTag_t8_132_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1983);
		Enum_t1_24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		_stringLiteral5225 = il2cpp_codegen_string_literal_from_index(5225);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(ExifTag_t8_132_0_0_0_var), /*hidden argument*/NULL);
		int32_t L_1 = ___tag;
		int32_t L_2 = L_1;
		Object_t * L_3 = Box(ExifTag_t8_132_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t1_24_il2cpp_TypeInfo_var);
		String_t* L_4 = Enum_GetName_m1_939(NULL /*static, unused*/, L_0, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		String_t* L_5 = V_0;
		if (L_5)
		{
			goto IL_0022;
		}
	}
	{
		return _stringLiteral5225;
	}

IL_0022:
	{
		String_t* L_6 = V_0;
		return L_6;
	}
}
// System.String ExifLibrary.ExifTagFactory::GetTagName(ExifLibrary.IFD,System.UInt16)
extern "C" String_t* ExifTagFactory_GetTagName_m8_633 (Object_t * __this /* static, unused */, int32_t ___ifd, uint16_t ___tagid, const MethodInfo* method)
{
	{
		int32_t L_0 = ___ifd;
		uint16_t L_1 = ___tagid;
		int32_t L_2 = ExifTagFactory_GetExifTag_m8_629(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		String_t* L_3 = ExifTagFactory_GetTagName_m8_632(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.String ExifLibrary.ExifTagFactory::GetTagLongName(ExifLibrary.ExifTag)
extern const Il2CppType* IFD_t8_131_0_0_0_var;
extern const Il2CppType* ExifTag_t8_132_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* IFD_t8_131_il2cpp_TypeInfo_var;
extern TypeInfo* Enum_t1_24_il2cpp_TypeInfo_var;
extern TypeInfo* ExifTag_t8_132_il2cpp_TypeInfo_var;
extern TypeInfo* StringU5BU5D_t1_238_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5225;
extern Il2CppCodeGenString* _stringLiteral259;
extern Il2CppCodeGenString* _stringLiteral267;
extern Il2CppCodeGenString* _stringLiteral106;
extern "C" String_t* ExifTagFactory_GetTagLongName_m8_634 (Object_t * __this /* static, unused */, int32_t ___tag, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IFD_t8_131_0_0_0_var = il2cpp_codegen_type_from_index(1976);
		ExifTag_t8_132_0_0_0_var = il2cpp_codegen_type_from_index(1983);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		IFD_t8_131_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1976);
		Enum_t1_24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		ExifTag_t8_132_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1983);
		StringU5BU5D_t1_238_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(35);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5225 = il2cpp_codegen_string_literal_from_index(5225);
		_stringLiteral259 = il2cpp_codegen_string_literal_from_index(259);
		_stringLiteral267 = il2cpp_codegen_string_literal_from_index(267);
		_stringLiteral106 = il2cpp_codegen_string_literal_from_index(106);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	String_t* V_1 = {0};
	String_t* V_2 = {0};
	uint16_t V_3 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(IFD_t8_131_0_0_0_var), /*hidden argument*/NULL);
		int32_t L_1 = ___tag;
		int32_t L_2 = ExifTagFactory_GetTagIFD_m8_631(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		int32_t L_3 = L_2;
		Object_t * L_4 = Box(IFD_t8_131_il2cpp_TypeInfo_var, &L_3);
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t1_24_il2cpp_TypeInfo_var);
		String_t* L_5 = Enum_GetName_m1_939(NULL /*static, unused*/, L_0, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		Type_t * L_6 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(ExifTag_t8_132_0_0_0_var), /*hidden argument*/NULL);
		int32_t L_7 = ___tag;
		int32_t L_8 = L_7;
		Object_t * L_9 = Box(ExifTag_t8_132_il2cpp_TypeInfo_var, &L_8);
		String_t* L_10 = Enum_GetName_m1_939(NULL /*static, unused*/, L_6, L_9, /*hidden argument*/NULL);
		V_1 = L_10;
		String_t* L_11 = V_1;
		if (L_11)
		{
			goto IL_003d;
		}
	}
	{
		V_1 = _stringLiteral5225;
	}

IL_003d:
	{
		int32_t L_12 = ___tag;
		uint16_t L_13 = ExifTagFactory_GetTagID_m8_630(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		V_3 = L_13;
		String_t* L_14 = UInt16_ToString_m1_336((&V_3), /*hidden argument*/NULL);
		V_2 = L_14;
		StringU5BU5D_t1_238* L_15 = ((StringU5BU5D_t1_238*)SZArrayNew(StringU5BU5D_t1_238_il2cpp_TypeInfo_var, 6));
		String_t* L_16 = V_0;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 0);
		ArrayElementTypeCheck (L_15, L_16);
		*((String_t**)(String_t**)SZArrayLdElema(L_15, 0, sizeof(String_t*))) = (String_t*)L_16;
		StringU5BU5D_t1_238* L_17 = L_15;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 1);
		ArrayElementTypeCheck (L_17, _stringLiteral259);
		*((String_t**)(String_t**)SZArrayLdElema(L_17, 1, sizeof(String_t*))) = (String_t*)_stringLiteral259;
		StringU5BU5D_t1_238* L_18 = L_17;
		String_t* L_19 = V_1;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 2);
		ArrayElementTypeCheck (L_18, L_19);
		*((String_t**)(String_t**)SZArrayLdElema(L_18, 2, sizeof(String_t*))) = (String_t*)L_19;
		StringU5BU5D_t1_238* L_20 = L_18;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 3);
		ArrayElementTypeCheck (L_20, _stringLiteral267);
		*((String_t**)(String_t**)SZArrayLdElema(L_20, 3, sizeof(String_t*))) = (String_t*)_stringLiteral267;
		StringU5BU5D_t1_238* L_21 = L_20;
		String_t* L_22 = V_2;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 4);
		ArrayElementTypeCheck (L_21, L_22);
		*((String_t**)(String_t**)SZArrayLdElema(L_21, 4, sizeof(String_t*))) = (String_t*)L_22;
		StringU5BU5D_t1_238* L_23 = L_21;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 5);
		ArrayElementTypeCheck (L_23, _stringLiteral106);
		*((String_t**)(String_t**)SZArrayLdElema(L_23, 5, sizeof(String_t*))) = (String_t*)_stringLiteral106;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_24 = String_Concat_m1_563(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		return L_24;
	}
}
// System.Void ExifLibrary.NotValidJPEGFileException::.ctor()
extern Il2CppCodeGenString* _stringLiteral5226;
extern "C" void NotValidJPEGFileException__ctor_m8_635 (NotValidJPEGFileException_t8_134 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral5226 = il2cpp_codegen_string_literal_from_index(5226);
		s_Il2CppMethodIntialized = true;
	}
	{
		Exception__ctor_m1_1238(__this, _stringLiteral5226, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ExifLibrary.NotValidJPEGFileException::.ctor(System.String)
extern "C" void NotValidJPEGFileException__ctor_m8_636 (NotValidJPEGFileException_t8_134 * __this, String_t* ___message, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message;
		Exception__ctor_m1_1238(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ExifLibrary.SectionExceeds64KBException::.ctor()
extern Il2CppCodeGenString* _stringLiteral5227;
extern "C" void SectionExceeds64KBException__ctor_m8_637 (SectionExceeds64KBException_t8_135 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral5227 = il2cpp_codegen_string_literal_from_index(5227);
		s_Il2CppMethodIntialized = true;
	}
	{
		Exception__ctor_m1_1238(__this, _stringLiteral5227, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ExifLibrary.SectionExceeds64KBException::.ctor(System.String)
extern "C" void SectionExceeds64KBException__ctor_m8_638 (SectionExceeds64KBException_t8_135 * __this, String_t* ___message, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message;
		Exception__ctor_m1_1238(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ExifLibrary.JPEGFile::.ctor()
extern "C" void JPEGFile__ctor_m8_639 (JPEGFile_t8_111 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ExifLibrary.JPEGFile::.ctor(System.IO.Stream)
extern "C" void JPEGFile__ctor_m8_640 (JPEGFile_t8_111 * __this, Stream_t1_405 * ___stream, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		Stream_t1_405 * L_0 = ___stream;
		JPEGFile_Read_m8_648(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ExifLibrary.JPEGFile::.ctor(System.String)
extern TypeInfo* FileStream_t1_146_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern "C" void JPEGFile__ctor_m8_641 (JPEGFile_t8_111 * __this, String_t* ___filename, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FileStream_t1_146_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(184);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		s_Il2CppMethodIntialized = true;
	}
	FileStream_t1_146 * V_0 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___filename;
		FileStream_t1_146 * L_1 = (FileStream_t1_146 *)il2cpp_codegen_object_new (FileStream_t1_146_il2cpp_TypeInfo_var);
		FileStream__ctor_m1_4961(L_1, L_0, 3, 1, /*hidden argument*/NULL);
		V_0 = L_1;
	}

IL_000f:
	try
	{ // begin try (depth: 1)
		FileStream_t1_146 * L_2 = V_0;
		JPEGFile_Read_m8_648(__this, L_2, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x29, FINALLY_001c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_001c;
	}

FINALLY_001c:
	{ // begin finally (depth: 1)
		{
			FileStream_t1_146 * L_3 = V_0;
			if (!L_3)
			{
				goto IL_0028;
			}
		}

IL_0022:
		{
			FileStream_t1_146 * L_4 = V_0;
			NullCheck(L_4);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_4);
		}

IL_0028:
		{
			IL2CPP_END_FINALLY(28)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(28)
	{
		IL2CPP_JUMP_TBL(0x29, IL_0029)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0029:
	{
		return;
	}
}
// System.Collections.Generic.List`1<ExifLibrary.JPEGSection> ExifLibrary.JPEGFile::get_Sections()
extern "C" List_1_t1_1906 * JPEGFile_get_Sections_m8_642 (JPEGFile_t8_111 * __this, const MethodInfo* method)
{
	{
		List_1_t1_1906 * L_0 = (__this->___U3CSectionsU3Ek__BackingField_0);
		return L_0;
	}
}
// System.Void ExifLibrary.JPEGFile::set_Sections(System.Collections.Generic.List`1<ExifLibrary.JPEGSection>)
extern "C" void JPEGFile_set_Sections_m8_643 (JPEGFile_t8_111 * __this, List_1_t1_1906 * ___value, const MethodInfo* method)
{
	{
		List_1_t1_1906 * L_0 = ___value;
		__this->___U3CSectionsU3Ek__BackingField_0 = L_0;
		return;
	}
}
// System.Byte[] ExifLibrary.JPEGFile::get_TrailingData()
extern "C" ByteU5BU5D_t1_109* JPEGFile_get_TrailingData_m8_644 (JPEGFile_t8_111 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t1_109* L_0 = (__this->___U3CTrailingDataU3Ek__BackingField_1);
		return L_0;
	}
}
// System.Void ExifLibrary.JPEGFile::set_TrailingData(System.Byte[])
extern "C" void JPEGFile_set_TrailingData_m8_645 (JPEGFile_t8_111 * __this, ByteU5BU5D_t1_109* ___value, const MethodInfo* method)
{
	{
		ByteU5BU5D_t1_109* L_0 = ___value;
		__this->___U3CTrailingDataU3Ek__BackingField_1 = L_0;
		return;
	}
}
// System.Void ExifLibrary.JPEGFile::Save(System.IO.Stream)
extern TypeInfo* SectionExceeds64KBException_t8_135_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t1_1928_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m1_15053_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1_15054_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1_15055_MethodInfo_var;
extern "C" void JPEGFile_Save_m8_646 (JPEGFile_t8_111 * __this, Stream_t1_405 * ___stream, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SectionExceeds64KBException_t8_135_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2076);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		Enumerator_t1_1928_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2077);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		List_1_GetEnumerator_m1_15053_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484184);
		Enumerator_get_Current_m1_15054_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484185);
		Enumerator_MoveNext_m1_15055_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484186);
		s_Il2CppMethodIntialized = true;
	}
	JPEGSection_t8_112 * V_0 = {0};
	Enumerator_t1_1928  V_1 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t1_1906 * L_0 = JPEGFile_get_Sections_m8_642(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Enumerator_t1_1928  L_1 = List_1_GetEnumerator_m1_15053(L_0, /*hidden argument*/List_1_GetEnumerator_m1_15053_MethodInfo_var);
		V_1 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00b5;
		}

IL_0011:
		{
			JPEGSection_t8_112 * L_2 = Enumerator_get_Current_m1_15054((&V_1), /*hidden argument*/Enumerator_get_Current_m1_15054_MethodInfo_var);
			V_0 = L_2;
			JPEGSection_t8_112 * L_3 = V_0;
			NullCheck(L_3);
			ByteU5BU5D_t1_109* L_4 = JPEGSection_get_Header_m8_654(L_3, /*hidden argument*/NULL);
			NullCheck(L_4);
			if ((((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Array_t *)L_4)->max_length))))+(int32_t)2))+(int32_t)2))) <= ((int32_t)((int32_t)65536))))
			{
				goto IL_0035;
			}
		}

IL_002f:
		{
			SectionExceeds64KBException_t8_135 * L_5 = (SectionExceeds64KBException_t8_135 *)il2cpp_codegen_object_new (SectionExceeds64KBException_t8_135_il2cpp_TypeInfo_var);
			SectionExceeds64KBException__ctor_m8_637(L_5, /*hidden argument*/NULL);
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_5);
		}

IL_0035:
		{
			Stream_t1_405 * L_6 = ___stream;
			ByteU5BU5D_t1_109* L_7 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 2));
			NullCheck(L_7);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
			*((uint8_t*)(uint8_t*)SZArrayLdElema(L_7, 0, sizeof(uint8_t))) = (uint8_t)((int32_t)255);
			ByteU5BU5D_t1_109* L_8 = L_7;
			JPEGSection_t8_112 * L_9 = V_0;
			NullCheck(L_9);
			uint8_t L_10 = JPEGSection_get_Marker_m8_652(L_9, /*hidden argument*/NULL);
			NullCheck(L_8);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
			*((uint8_t*)(uint8_t*)SZArrayLdElema(L_8, 1, sizeof(uint8_t))) = (uint8_t)L_10;
			NullCheck(L_6);
			VirtActionInvoker3< ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(24 /* System.Void System.IO.Stream::Write(System.Byte[],System.Int32,System.Int32) */, L_6, L_8, 0, 2);
			JPEGSection_t8_112 * L_11 = V_0;
			NullCheck(L_11);
			ByteU5BU5D_t1_109* L_12 = JPEGSection_get_Header_m8_654(L_11, /*hidden argument*/NULL);
			NullCheck(L_12);
			if (!(((int32_t)((int32_t)(((Array_t *)L_12)->max_length)))))
			{
				goto IL_0093;
			}
		}

IL_0061:
		{
			Stream_t1_405 * L_13 = ___stream;
			BitConverterEx_t8_61 * L_14 = BitConverterEx_get_BigEndian_m8_343(NULL /*static, unused*/, /*hidden argument*/NULL);
			JPEGSection_t8_112 * L_15 = V_0;
			NullCheck(L_15);
			ByteU5BU5D_t1_109* L_16 = JPEGSection_get_Header_m8_654(L_15, /*hidden argument*/NULL);
			NullCheck(L_16);
			NullCheck(L_14);
			ByteU5BU5D_t1_109* L_17 = BitConverterEx_GetBytes_m8_371(L_14, (((int32_t)((uint16_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Array_t *)L_16)->max_length))))+(int32_t)2))))), /*hidden argument*/NULL);
			NullCheck(L_13);
			VirtActionInvoker3< ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(24 /* System.Void System.IO.Stream::Write(System.Byte[],System.Int32,System.Int32) */, L_13, L_17, 0, 2);
			Stream_t1_405 * L_18 = ___stream;
			JPEGSection_t8_112 * L_19 = V_0;
			NullCheck(L_19);
			ByteU5BU5D_t1_109* L_20 = JPEGSection_get_Header_m8_654(L_19, /*hidden argument*/NULL);
			JPEGSection_t8_112 * L_21 = V_0;
			NullCheck(L_21);
			ByteU5BU5D_t1_109* L_22 = JPEGSection_get_Header_m8_654(L_21, /*hidden argument*/NULL);
			NullCheck(L_22);
			NullCheck(L_18);
			VirtActionInvoker3< ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(24 /* System.Void System.IO.Stream::Write(System.Byte[],System.Int32,System.Int32) */, L_18, L_20, 0, (((int32_t)((int32_t)(((Array_t *)L_22)->max_length)))));
		}

IL_0093:
		{
			JPEGSection_t8_112 * L_23 = V_0;
			NullCheck(L_23);
			ByteU5BU5D_t1_109* L_24 = JPEGSection_get_EntropyData_m8_656(L_23, /*hidden argument*/NULL);
			NullCheck(L_24);
			if (!(((int32_t)((int32_t)(((Array_t *)L_24)->max_length)))))
			{
				goto IL_00b5;
			}
		}

IL_00a0:
		{
			Stream_t1_405 * L_25 = ___stream;
			JPEGSection_t8_112 * L_26 = V_0;
			NullCheck(L_26);
			ByteU5BU5D_t1_109* L_27 = JPEGSection_get_EntropyData_m8_656(L_26, /*hidden argument*/NULL);
			JPEGSection_t8_112 * L_28 = V_0;
			NullCheck(L_28);
			ByteU5BU5D_t1_109* L_29 = JPEGSection_get_EntropyData_m8_656(L_28, /*hidden argument*/NULL);
			NullCheck(L_29);
			NullCheck(L_25);
			VirtActionInvoker3< ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(24 /* System.Void System.IO.Stream::Write(System.Byte[],System.Int32,System.Int32) */, L_25, L_27, 0, (((int32_t)((int32_t)(((Array_t *)L_29)->max_length)))));
		}

IL_00b5:
		{
			bool L_30 = Enumerator_MoveNext_m1_15055((&V_1), /*hidden argument*/Enumerator_MoveNext_m1_15055_MethodInfo_var);
			if (L_30)
			{
				goto IL_0011;
			}
		}

IL_00c1:
		{
			IL2CPP_LEAVE(0xD2, FINALLY_00c6);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_00c6;
	}

FINALLY_00c6:
	{ // begin finally (depth: 1)
		Enumerator_t1_1928  L_31 = V_1;
		Enumerator_t1_1928  L_32 = L_31;
		Object_t * L_33 = Box(Enumerator_t1_1928_il2cpp_TypeInfo_var, &L_32);
		NullCheck(L_33);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_33);
		IL2CPP_END_FINALLY(198)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(198)
	{
		IL2CPP_JUMP_TBL(0xD2, IL_00d2)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_00d2:
	{
		ByteU5BU5D_t1_109* L_34 = JPEGFile_get_TrailingData_m8_644(__this, /*hidden argument*/NULL);
		NullCheck(L_34);
		if (!(((int32_t)((int32_t)(((Array_t *)L_34)->max_length)))))
		{
			goto IL_00f4;
		}
	}
	{
		Stream_t1_405 * L_35 = ___stream;
		ByteU5BU5D_t1_109* L_36 = JPEGFile_get_TrailingData_m8_644(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_37 = JPEGFile_get_TrailingData_m8_644(__this, /*hidden argument*/NULL);
		NullCheck(L_37);
		NullCheck(L_35);
		VirtActionInvoker3< ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(24 /* System.Void System.IO.Stream::Write(System.Byte[],System.Int32,System.Int32) */, L_35, L_36, 0, (((int32_t)((int32_t)(((Array_t *)L_37)->max_length)))));
	}

IL_00f4:
	{
		return;
	}
}
// System.Void ExifLibrary.JPEGFile::Save(System.String)
extern TypeInfo* FileStream_t1_146_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern "C" void JPEGFile_Save_m8_647 (JPEGFile_t8_111 * __this, String_t* ___filename, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FileStream_t1_146_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(184);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		s_Il2CppMethodIntialized = true;
	}
	FileStream_t1_146 * V_0 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___filename;
		FileStream_t1_146 * L_1 = (FileStream_t1_146 *)il2cpp_codegen_object_new (FileStream_t1_146_il2cpp_TypeInfo_var);
		FileStream__ctor_m1_4961(L_1, L_0, 2, 2, /*hidden argument*/NULL);
		V_0 = L_1;
	}

IL_0009:
	try
	{ // begin try (depth: 1)
		FileStream_t1_146 * L_2 = V_0;
		JPEGFile_Save_m8_646(__this, L_2, /*hidden argument*/NULL);
		FileStream_t1_146 * L_3 = V_0;
		NullCheck(L_3);
		VirtActionInvoker0::Invoke(13 /* System.Void System.IO.Stream::Close() */, L_3);
		IL2CPP_LEAVE(0x28, FINALLY_001b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_001b;
	}

FINALLY_001b:
	{ // begin finally (depth: 1)
		{
			FileStream_t1_146 * L_4 = V_0;
			if (!L_4)
			{
				goto IL_0027;
			}
		}

IL_0021:
		{
			FileStream_t1_146 * L_5 = V_0;
			NullCheck(L_5);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_5);
		}

IL_0027:
		{
			IL2CPP_END_FINALLY(27)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(27)
	{
		IL2CPP_JUMP_TBL(0x28, IL_0028)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0028:
	{
		return;
	}
}
// System.Boolean ExifLibrary.JPEGFile::Read(System.IO.Stream)
extern TypeInfo* List_1_t1_1906_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern TypeInfo* JPEGSection_t8_112_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1_15056_MethodInfo_var;
extern "C" bool JPEGFile_Read_m8_648 (JPEGFile_t8_111 * __this, Stream_t1_405 * ___stream, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t1_1906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2078);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		JPEGSection_t8_112_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1975);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		List_1__ctor_m1_15056_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484187);
		s_Il2CppMethodIntialized = true;
	}
	Stream_t1_405 * V_0 = {0};
	ByteU5BU5D_t1_109* V_1 = {0};
	uint8_t V_2 = {0};
	ByteU5BU5D_t1_109* V_3 = {0};
	ByteU5BU5D_t1_109* V_4 = {0};
	int64_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	ByteU5BU5D_t1_109* V_9 = {0};
	int64_t V_10 = 0;
	int32_t V_11 = 0;
	int64_t V_12 = 0;
	int32_t V_13 = 0;
	int32_t V_14 = 0;
	int32_t V_15 = 0;
	JPEGSection_t8_112 * V_16 = {0};
	int32_t V_17 = 0;
	int32_t V_18 = 0;
	int32_t V_19 = 0;
	bool V_20 = false;
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t1_1906 * L_0 = (List_1_t1_1906 *)il2cpp_codegen_object_new (List_1_t1_1906_il2cpp_TypeInfo_var);
		List_1__ctor_m1_15056(L_0, /*hidden argument*/List_1__ctor_m1_15056_MethodInfo_var);
		JPEGFile_set_Sections_m8_643(__this, L_0, /*hidden argument*/NULL);
		Stream_t1_405 * L_1 = ___stream;
		V_0 = L_1;
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			V_1 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 2));
			Stream_t1_405 * L_2 = ___stream;
			ByteU5BU5D_t1_109* L_3 = V_1;
			NullCheck(L_2);
			int32_t L_4 = (int32_t)VirtFuncInvoker3< int32_t, ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(20 /* System.Int32 System.IO.Stream::Read(System.Byte[],System.Int32,System.Int32) */, L_2, L_3, 0, 2);
			if ((!(((uint32_t)L_4) == ((uint32_t)2))))
			{
				goto IL_003d;
			}
		}

IL_0023:
		{
			ByteU5BU5D_t1_109* L_5 = V_1;
			NullCheck(L_5);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 0);
			int32_t L_6 = 0;
			if ((((int32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_5, L_6, sizeof(uint8_t)))) == ((int32_t)((int32_t)255))))
			{
				goto IL_0045;
			}
		}

IL_0030:
		{
			ByteU5BU5D_t1_109* L_7 = V_1;
			NullCheck(L_7);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 1);
			int32_t L_8 = 1;
			if ((((int32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_7, L_8, sizeof(uint8_t)))) == ((int32_t)((int32_t)216))))
			{
				goto IL_0045;
			}
		}

IL_003d:
		{
			V_20 = 0;
			IL2CPP_LEAVE(0x303, FINALLY_02f4);
		}

IL_0045:
		{
			Stream_t1_405 * L_9 = ___stream;
			NullCheck(L_9);
			VirtFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(22 /* System.Int64 System.IO.Stream::Seek(System.Int64,System.IO.SeekOrigin) */, L_9, (((int64_t)((int64_t)0))), 0);
			goto IL_02d8;
		}

IL_0054:
		{
			Stream_t1_405 * L_10 = ___stream;
			ByteU5BU5D_t1_109* L_11 = V_1;
			NullCheck(L_10);
			int32_t L_12 = (int32_t)VirtFuncInvoker3< int32_t, ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(20 /* System.Int32 System.IO.Stream::Read(System.Byte[],System.Int32,System.Int32) */, L_10, L_11, 0, 2);
			if ((!(((uint32_t)L_12) == ((uint32_t)2))))
			{
				goto IL_0085;
			}
		}

IL_0063:
		{
			ByteU5BU5D_t1_109* L_13 = V_1;
			NullCheck(L_13);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 0);
			int32_t L_14 = 0;
			if ((!(((uint32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_13, L_14, sizeof(uint8_t)))) == ((uint32_t)((int32_t)255)))))
			{
				goto IL_0085;
			}
		}

IL_0070:
		{
			ByteU5BU5D_t1_109* L_15 = V_1;
			NullCheck(L_15);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 1);
			int32_t L_16 = 1;
			if (!(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_15, L_16, sizeof(uint8_t))))
			{
				goto IL_0085;
			}
		}

IL_0078:
		{
			ByteU5BU5D_t1_109* L_17 = V_1;
			NullCheck(L_17);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 1);
			int32_t L_18 = 1;
			if ((!(((uint32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_17, L_18, sizeof(uint8_t)))) == ((uint32_t)((int32_t)255)))))
			{
				goto IL_008d;
			}
		}

IL_0085:
		{
			V_20 = 0;
			IL2CPP_LEAVE(0x303, FINALLY_02f4);
		}

IL_008d:
		{
			ByteU5BU5D_t1_109* L_19 = V_1;
			NullCheck(L_19);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 1);
			int32_t L_20 = 1;
			V_2 = (*(uint8_t*)(uint8_t*)SZArrayLdElema(L_19, L_20, sizeof(uint8_t)));
			V_3 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 0));
			uint8_t L_21 = V_2;
			if ((((int32_t)L_21) == ((int32_t)((int32_t)216))))
			{
				goto IL_0147;
			}
		}

IL_00a3:
		{
			uint8_t L_22 = V_2;
			if ((((int32_t)L_22) == ((int32_t)((int32_t)217))))
			{
				goto IL_0147;
			}
		}

IL_00ae:
		{
			uint8_t L_23 = V_2;
			if ((((int32_t)L_23) < ((int32_t)((int32_t)208))))
			{
				goto IL_00c4;
			}
		}

IL_00b9:
		{
			uint8_t L_24 = V_2;
			if ((((int32_t)L_24) <= ((int32_t)((int32_t)215))))
			{
				goto IL_0147;
			}
		}

IL_00c4:
		{
			V_4 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 2));
			Stream_t1_405 * L_25 = ___stream;
			ByteU5BU5D_t1_109* L_26 = V_4;
			NullCheck(L_25);
			int32_t L_27 = (int32_t)VirtFuncInvoker3< int32_t, ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(20 /* System.Int32 System.IO.Stream::Read(System.Byte[],System.Int32,System.Int32) */, L_25, L_26, 0, 2);
			if ((((int32_t)L_27) == ((int32_t)2)))
			{
				goto IL_00e4;
			}
		}

IL_00dc:
		{
			V_20 = 0;
			IL2CPP_LEAVE(0x303, FINALLY_02f4);
		}

IL_00e4:
		{
			BitConverterEx_t8_61 * L_28 = BitConverterEx_get_BigEndian_m8_343(NULL /*static, unused*/, /*hidden argument*/NULL);
			ByteU5BU5D_t1_109* L_29 = V_4;
			NullCheck(L_28);
			uint16_t L_30 = BitConverterEx_ToUInt16_m8_363(L_28, L_29, 0, /*hidden argument*/NULL);
			V_5 = (((int64_t)((int64_t)L_30)));
			int64_t L_31 = V_5;
			if ((int64_t)(((int64_t)((int64_t)L_31-(int64_t)(((int64_t)((int64_t)2)))))) > INTPTR_MAX) il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_overflow_exception());
			V_3 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, (((intptr_t)((int64_t)((int64_t)L_31-(int64_t)(((int64_t)((int64_t)2)))))))));
			ByteU5BU5D_t1_109* L_32 = V_3;
			NullCheck(L_32);
			V_6 = (((int32_t)((int32_t)(((Array_t *)L_32)->max_length))));
			goto IL_013f;
		}

IL_010a:
		{
			int32_t L_33 = V_6;
			int32_t L_34 = Math_Min_m1_14218(NULL /*static, unused*/, L_33, ((int32_t)4096), /*hidden argument*/NULL);
			V_7 = L_34;
			Stream_t1_405 * L_35 = ___stream;
			ByteU5BU5D_t1_109* L_36 = V_3;
			ByteU5BU5D_t1_109* L_37 = V_3;
			NullCheck(L_37);
			int32_t L_38 = V_6;
			int32_t L_39 = V_7;
			NullCheck(L_35);
			int32_t L_40 = (int32_t)VirtFuncInvoker3< int32_t, ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(20 /* System.Int32 System.IO.Stream::Read(System.Byte[],System.Int32,System.Int32) */, L_35, L_36, ((int32_t)((int32_t)(((int32_t)((int32_t)(((Array_t *)L_37)->max_length))))-(int32_t)L_38)), L_39);
			V_8 = L_40;
			int32_t L_41 = V_8;
			if (L_41)
			{
				goto IL_0138;
			}
		}

IL_0130:
		{
			V_20 = 0;
			IL2CPP_LEAVE(0x303, FINALLY_02f4);
		}

IL_0138:
		{
			int32_t L_42 = V_6;
			int32_t L_43 = V_8;
			V_6 = ((int32_t)((int32_t)L_42-(int32_t)L_43));
		}

IL_013f:
		{
			int32_t L_44 = V_6;
			if ((((int32_t)L_44) > ((int32_t)0)))
			{
				goto IL_010a;
			}
		}

IL_0147:
		{
			V_9 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 0));
			uint8_t L_45 = V_2;
			if ((((int32_t)L_45) == ((int32_t)((int32_t)218))))
			{
				goto IL_0170;
			}
		}

IL_015a:
		{
			uint8_t L_46 = V_2;
			if ((((int32_t)L_46) < ((int32_t)((int32_t)208))))
			{
				goto IL_024c;
			}
		}

IL_0165:
		{
			uint8_t L_47 = V_2;
			if ((((int32_t)L_47) > ((int32_t)((int32_t)215))))
			{
				goto IL_024c;
			}
		}

IL_0170:
		{
			Stream_t1_405 * L_48 = ___stream;
			NullCheck(L_48);
			int64_t L_49 = (int64_t)VirtFuncInvoker0< int64_t >::Invoke(10 /* System.Int64 System.IO.Stream::get_Position() */, L_48);
			V_10 = L_49;
		}

IL_0178:
		{
			V_11 = 0;
		}

IL_017b:
		{
			Stream_t1_405 * L_50 = ___stream;
			NullCheck(L_50);
			int32_t L_51 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(21 /* System.Int32 System.IO.Stream::ReadByte() */, L_50);
			V_11 = L_51;
			int32_t L_52 = V_11;
			if ((!(((uint32_t)L_52) == ((uint32_t)(-1)))))
			{
				goto IL_0193;
			}
		}

IL_018b:
		{
			V_20 = 0;
			IL2CPP_LEAVE(0x303, FINALLY_02f4);
		}

IL_0193:
		{
			int32_t L_53 = V_11;
			if ((!(((uint32_t)(((int32_t)((uint8_t)L_53)))) == ((uint32_t)((int32_t)255)))))
			{
				goto IL_017b;
			}
		}

IL_01a0:
		{
			Stream_t1_405 * L_54 = ___stream;
			NullCheck(L_54);
			int32_t L_55 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(21 /* System.Int32 System.IO.Stream::ReadByte() */, L_54);
			V_11 = L_55;
			int32_t L_56 = V_11;
			if ((!(((uint32_t)L_56) == ((uint32_t)(-1)))))
			{
				goto IL_01b8;
			}
		}

IL_01b0:
		{
			V_20 = 0;
			IL2CPP_LEAVE(0x303, FINALLY_02f4);
		}

IL_01b8:
		{
			int32_t L_57 = V_11;
			if ((((int32_t)(((int32_t)((uint8_t)L_57)))) == ((int32_t)((int32_t)255))))
			{
				goto IL_01a0;
			}
		}

IL_01c5:
		{
			int32_t L_58 = V_11;
			if (!(((int32_t)((uint8_t)L_58))))
			{
				goto IL_0247;
			}
		}

IL_01cd:
		{
			Stream_t1_405 * L_59 = ___stream;
			NullCheck(L_59);
			VirtFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(22 /* System.Int64 System.IO.Stream::Seek(System.Int64,System.IO.SeekOrigin) */, L_59, (((int64_t)((int64_t)((int32_t)-2)))), 1);
			Stream_t1_405 * L_60 = ___stream;
			NullCheck(L_60);
			int64_t L_61 = (int64_t)VirtFuncInvoker0< int64_t >::Invoke(10 /* System.Int64 System.IO.Stream::get_Position() */, L_60);
			int64_t L_62 = V_10;
			V_12 = ((int64_t)((int64_t)L_61-(int64_t)L_62));
			Stream_t1_405 * L_63 = ___stream;
			int64_t L_64 = V_12;
			NullCheck(L_63);
			VirtFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(22 /* System.Int64 System.IO.Stream::Seek(System.Int64,System.IO.SeekOrigin) */, L_63, ((-L_64)), 1);
			int64_t L_65 = V_12;
			if ((int64_t)(L_65) > INTPTR_MAX) il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_overflow_exception());
			V_9 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, (((intptr_t)L_65))));
			ByteU5BU5D_t1_109* L_66 = V_9;
			NullCheck(L_66);
			V_13 = (((int32_t)((int32_t)(((Array_t *)L_66)->max_length))));
			goto IL_023a;
		}

IL_0203:
		{
			int32_t L_67 = V_13;
			int32_t L_68 = Math_Min_m1_14218(NULL /*static, unused*/, L_67, ((int32_t)4096), /*hidden argument*/NULL);
			V_14 = L_68;
			Stream_t1_405 * L_69 = ___stream;
			ByteU5BU5D_t1_109* L_70 = V_9;
			ByteU5BU5D_t1_109* L_71 = V_9;
			NullCheck(L_71);
			int32_t L_72 = V_13;
			int32_t L_73 = V_14;
			NullCheck(L_69);
			int32_t L_74 = (int32_t)VirtFuncInvoker3< int32_t, ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(20 /* System.Int32 System.IO.Stream::Read(System.Byte[],System.Int32,System.Int32) */, L_69, L_70, ((int32_t)((int32_t)(((int32_t)((int32_t)(((Array_t *)L_71)->max_length))))-(int32_t)L_72)), L_73);
			V_15 = L_74;
			int32_t L_75 = V_15;
			if (L_75)
			{
				goto IL_0233;
			}
		}

IL_022b:
		{
			V_20 = 0;
			IL2CPP_LEAVE(0x303, FINALLY_02f4);
		}

IL_0233:
		{
			int32_t L_76 = V_13;
			int32_t L_77 = V_15;
			V_13 = ((int32_t)((int32_t)L_76-(int32_t)L_77));
		}

IL_023a:
		{
			int32_t L_78 = V_13;
			if ((((int32_t)L_78) > ((int32_t)0)))
			{
				goto IL_0203;
			}
		}

IL_0242:
		{
			goto IL_024c;
		}

IL_0247:
		{
			goto IL_0178;
		}

IL_024c:
		{
			uint8_t L_79 = V_2;
			ByteU5BU5D_t1_109* L_80 = V_3;
			ByteU5BU5D_t1_109* L_81 = V_9;
			JPEGSection_t8_112 * L_82 = (JPEGSection_t8_112 *)il2cpp_codegen_object_new (JPEGSection_t8_112_il2cpp_TypeInfo_var);
			JPEGSection__ctor_m8_650(L_82, L_79, L_80, L_81, /*hidden argument*/NULL);
			V_16 = L_82;
			List_1_t1_1906 * L_83 = JPEGFile_get_Sections_m8_642(__this, /*hidden argument*/NULL);
			JPEGSection_t8_112 * L_84 = V_16;
			NullCheck(L_83);
			VirtActionInvoker1< JPEGSection_t8_112 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<ExifLibrary.JPEGSection>::Add(!0) */, L_83, L_84);
			uint8_t L_85 = V_2;
			if ((!(((uint32_t)L_85) == ((uint32_t)((int32_t)217)))))
			{
				goto IL_02d8;
			}
		}

IL_026f:
		{
			Stream_t1_405 * L_86 = ___stream;
			NullCheck(L_86);
			int64_t L_87 = (int64_t)VirtFuncInvoker0< int64_t >::Invoke(9 /* System.Int64 System.IO.Stream::get_Length() */, L_86);
			Stream_t1_405 * L_88 = ___stream;
			NullCheck(L_88);
			int64_t L_89 = (int64_t)VirtFuncInvoker0< int64_t >::Invoke(10 /* System.Int64 System.IO.Stream::get_Position() */, L_88);
			V_17 = (((int32_t)((int32_t)((int64_t)((int64_t)L_87-(int64_t)L_89)))));
			int32_t L_90 = V_17;
			JPEGFile_set_TrailingData_m8_645(__this, ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, L_90)), /*hidden argument*/NULL);
			goto IL_02d0;
		}

IL_0291:
		{
			int32_t L_91 = V_17;
			int32_t L_92 = Math_Min_m1_14218(NULL /*static, unused*/, L_91, ((int32_t)4096), /*hidden argument*/NULL);
			V_18 = L_92;
			Stream_t1_405 * L_93 = ___stream;
			ByteU5BU5D_t1_109* L_94 = JPEGFile_get_TrailingData_m8_644(__this, /*hidden argument*/NULL);
			ByteU5BU5D_t1_109* L_95 = JPEGFile_get_TrailingData_m8_644(__this, /*hidden argument*/NULL);
			NullCheck(L_95);
			int32_t L_96 = V_17;
			int32_t L_97 = V_18;
			NullCheck(L_93);
			int32_t L_98 = (int32_t)VirtFuncInvoker3< int32_t, ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(20 /* System.Int32 System.IO.Stream::Read(System.Byte[],System.Int32,System.Int32) */, L_93, L_94, ((int32_t)((int32_t)(((int32_t)((int32_t)(((Array_t *)L_95)->max_length))))-(int32_t)L_96)), L_97);
			V_19 = L_98;
			int32_t L_99 = V_19;
			if (L_99)
			{
				goto IL_02c9;
			}
		}

IL_02c1:
		{
			V_20 = 0;
			IL2CPP_LEAVE(0x303, FINALLY_02f4);
		}

IL_02c9:
		{
			int32_t L_100 = V_17;
			int32_t L_101 = V_19;
			V_17 = ((int32_t)((int32_t)L_100-(int32_t)L_101));
		}

IL_02d0:
		{
			int32_t L_102 = V_17;
			if ((((int32_t)L_102) > ((int32_t)0)))
			{
				goto IL_0291;
			}
		}

IL_02d8:
		{
			Stream_t1_405 * L_103 = ___stream;
			NullCheck(L_103);
			int64_t L_104 = (int64_t)VirtFuncInvoker0< int64_t >::Invoke(10 /* System.Int64 System.IO.Stream::get_Position() */, L_103);
			Stream_t1_405 * L_105 = ___stream;
			NullCheck(L_105);
			int64_t L_106 = (int64_t)VirtFuncInvoker0< int64_t >::Invoke(9 /* System.Int64 System.IO.Stream::get_Length() */, L_105);
			if ((!(((uint64_t)L_104) == ((uint64_t)L_106))))
			{
				goto IL_0054;
			}
		}

IL_02e9:
		{
			Stream_t1_405 * L_107 = ___stream;
			NullCheck(L_107);
			VirtActionInvoker0::Invoke(13 /* System.Void System.IO.Stream::Close() */, L_107);
			IL2CPP_LEAVE(0x301, FINALLY_02f4);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_02f4;
	}

FINALLY_02f4:
	{ // begin finally (depth: 1)
		{
			Stream_t1_405 * L_108 = V_0;
			if (!L_108)
			{
				goto IL_0300;
			}
		}

IL_02fa:
		{
			Stream_t1_405 * L_109 = V_0;
			NullCheck(L_109);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_109);
		}

IL_0300:
		{
			IL2CPP_END_FINALLY(756)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(756)
	{
		IL2CPP_JUMP_TBL(0x303, IL_0303)
		IL2CPP_JUMP_TBL(0x301, IL_0301)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0301:
	{
		return 1;
	}

IL_0303:
	{
		bool L_110 = V_20;
		return L_110;
	}
}
// System.Void ExifLibrary.JPEGSection::.ctor()
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern "C" void JPEGSection__ctor_m8_649 (JPEGSection_t8_112 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		JPEGSection_set_Header_m8_655(__this, ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		JPEGSection_set_EntropyData_m8_657(__this, ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void ExifLibrary.JPEGSection::.ctor(ExifLibrary.JPEGMarker,System.Byte[],System.Byte[])
extern "C" void JPEGSection__ctor_m8_650 (JPEGSection_t8_112 * __this, uint8_t ___marker, ByteU5BU5D_t1_109* ___data, ByteU5BU5D_t1_109* ___entropydata, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		uint8_t L_0 = ___marker;
		JPEGSection_set_Marker_m8_653(__this, L_0, /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_1 = ___data;
		JPEGSection_set_Header_m8_655(__this, L_1, /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_2 = ___entropydata;
		JPEGSection_set_EntropyData_m8_657(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ExifLibrary.JPEGSection::.ctor(ExifLibrary.JPEGMarker)
extern "C" void JPEGSection__ctor_m8_651 (JPEGSection_t8_112 * __this, uint8_t ___marker, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		uint8_t L_0 = ___marker;
		JPEGSection_set_Marker_m8_653(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// ExifLibrary.JPEGMarker ExifLibrary.JPEGSection::get_Marker()
extern "C" uint8_t JPEGSection_get_Marker_m8_652 (JPEGSection_t8_112 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = (__this->___U3CMarkerU3Ek__BackingField_0);
		return L_0;
	}
}
// System.Void ExifLibrary.JPEGSection::set_Marker(ExifLibrary.JPEGMarker)
extern "C" void JPEGSection_set_Marker_m8_653 (JPEGSection_t8_112 * __this, uint8_t ___value, const MethodInfo* method)
{
	{
		uint8_t L_0 = ___value;
		__this->___U3CMarkerU3Ek__BackingField_0 = L_0;
		return;
	}
}
// System.Byte[] ExifLibrary.JPEGSection::get_Header()
extern "C" ByteU5BU5D_t1_109* JPEGSection_get_Header_m8_654 (JPEGSection_t8_112 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t1_109* L_0 = (__this->___U3CHeaderU3Ek__BackingField_1);
		return L_0;
	}
}
// System.Void ExifLibrary.JPEGSection::set_Header(System.Byte[])
extern "C" void JPEGSection_set_Header_m8_655 (JPEGSection_t8_112 * __this, ByteU5BU5D_t1_109* ___value, const MethodInfo* method)
{
	{
		ByteU5BU5D_t1_109* L_0 = ___value;
		__this->___U3CHeaderU3Ek__BackingField_1 = L_0;
		return;
	}
}
// System.Byte[] ExifLibrary.JPEGSection::get_EntropyData()
extern "C" ByteU5BU5D_t1_109* JPEGSection_get_EntropyData_m8_656 (JPEGSection_t8_112 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t1_109* L_0 = (__this->___U3CEntropyDataU3Ek__BackingField_2);
		return L_0;
	}
}
// System.Void ExifLibrary.JPEGSection::set_EntropyData(System.Byte[])
extern "C" void JPEGSection_set_EntropyData_m8_657 (JPEGSection_t8_112 * __this, ByteU5BU5D_t1_109* ___value, const MethodInfo* method)
{
	{
		ByteU5BU5D_t1_109* L_0 = ___value;
		__this->___U3CEntropyDataU3Ek__BackingField_2 = L_0;
		return;
	}
}
// System.String ExifLibrary.JPEGSection::ToString()
extern TypeInfo* JPEGMarker_t8_136_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5228;
extern "C" String_t* JPEGSection_ToString_m8_658 (JPEGSection_t8_112 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		JPEGMarker_t8_136_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2079);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5228 = il2cpp_codegen_string_literal_from_index(5228);
		s_Il2CppMethodIntialized = true;
	}
	{
		uint8_t L_0 = JPEGSection_get_Marker_m8_652(__this, /*hidden argument*/NULL);
		uint8_t L_1 = L_0;
		Object_t * L_2 = Box(JPEGMarker_t8_136_il2cpp_TypeInfo_var, &L_1);
		ByteU5BU5D_t1_109* L_3 = JPEGSection_get_Header_m8_654(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = (((int32_t)((int32_t)(((Array_t *)L_3)->max_length))));
		Object_t * L_5 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_4);
		ByteU5BU5D_t1_109* L_6 = JPEGSection_get_EntropyData_m8_656(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_7 = (((int32_t)((int32_t)(((Array_t *)L_6)->max_length))));
		Object_t * L_8 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_7);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Format_m1_550(NULL /*static, unused*/, _stringLiteral5228, L_2, L_5, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Void ExifLibrary.MathEx/Fraction32::.ctor(System.Int32,System.Int32)
extern TypeInfo* Fraction32_t8_127_il2cpp_TypeInfo_var;
extern "C" void Fraction32__ctor_m8_659 (Fraction32_t8_127 * __this, int32_t ___numerator, int32_t ___denominator, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Fraction32_t8_127_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1974);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___mIsNegative_1 = 0;
		int32_t L_0 = ___numerator;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_1 = ___numerator;
		___numerator = ((-L_1));
		bool L_2 = (__this->___mIsNegative_1);
		__this->___mIsNegative_1 = ((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}

IL_0021:
	{
		int32_t L_3 = ___denominator;
		if ((((int32_t)L_3) >= ((int32_t)0)))
		{
			goto IL_003b;
		}
	}
	{
		int32_t L_4 = ___denominator;
		___denominator = ((-L_4));
		bool L_5 = (__this->___mIsNegative_1);
		__this->___mIsNegative_1 = ((((int32_t)L_5) == ((int32_t)0))? 1 : 0);
	}

IL_003b:
	{
		int32_t L_6 = ___numerator;
		__this->___mNumerator_2 = L_6;
		int32_t L_7 = ___denominator;
		__this->___mDenominator_3 = L_7;
		int32_t L_8 = (__this->___mDenominator_3);
		if (!L_8)
		{
			goto IL_0065;
		}
	}
	{
		int32_t* L_9 = &(__this->___mNumerator_2);
		int32_t* L_10 = &(__this->___mDenominator_3);
		IL2CPP_RUNTIME_CLASS_INIT(Fraction32_t8_127_il2cpp_TypeInfo_var);
		Fraction32_Reduce_m8_691(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
	}

IL_0065:
	{
		return;
	}
}
// System.Void ExifLibrary.MathEx/Fraction32::.ctor(System.Int32)
extern "C" void Fraction32__ctor_m8_660 (Fraction32_t8_127 * __this, int32_t ___numerator, const MethodInfo* method)
{
	{
		int32_t L_0 = ___numerator;
		Fraction32__ctor_m8_659(__this, L_0, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ExifLibrary.MathEx/Fraction32::.ctor(ExifLibrary.MathEx/Fraction32)
extern "C" void Fraction32__ctor_m8_661 (Fraction32_t8_127 * __this, Fraction32_t8_127  ___f, const MethodInfo* method)
{
	{
		int32_t L_0 = Fraction32_get_Numerator_m8_666((&___f), /*hidden argument*/NULL);
		int32_t L_1 = Fraction32_get_Denominator_m8_668((&___f), /*hidden argument*/NULL);
		Fraction32__ctor_m8_659(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ExifLibrary.MathEx/Fraction32::.ctor(System.Single)
extern "C" void Fraction32__ctor_m8_662 (Fraction32_t8_127 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		Fraction32__ctor_m8_663(__this, (((double)((double)L_0))), /*hidden argument*/NULL);
		return;
	}
}
// System.Void ExifLibrary.MathEx/Fraction32::.ctor(System.Double)
extern TypeInfo* Fraction32_t8_127_il2cpp_TypeInfo_var;
extern "C" void Fraction32__ctor_m8_663 (Fraction32_t8_127 * __this, double ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Fraction32_t8_127_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1974);
		s_Il2CppMethodIntialized = true;
	}
	{
		double L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(Fraction32_t8_127_il2cpp_TypeInfo_var);
		Fraction32_t8_127  L_1 = Fraction32_FromDouble_m8_689(NULL /*static, unused*/, L_0, 8, /*hidden argument*/NULL);
		Fraction32__ctor_m8_661(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ExifLibrary.MathEx/Fraction32::.ctor(System.String)
extern TypeInfo* Fraction32_t8_127_il2cpp_TypeInfo_var;
extern "C" void Fraction32__ctor_m8_664 (Fraction32_t8_127 * __this, String_t* ___s, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Fraction32_t8_127_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1974);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___s;
		IL2CPP_RUNTIME_CLASS_INIT(Fraction32_t8_127_il2cpp_TypeInfo_var);
		Fraction32_t8_127  L_1 = Fraction32_FromString_m8_690(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Fraction32__ctor_m8_661(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ExifLibrary.MathEx/Fraction32::.cctor()
extern TypeInfo* Fraction32_t8_127_il2cpp_TypeInfo_var;
extern "C" void Fraction32__cctor_m8_665 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Fraction32_t8_127_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1974);
		s_Il2CppMethodIntialized = true;
	}
	{
		Fraction32_t8_127  L_0 = {0};
		Fraction32__ctor_m8_659(&L_0, 0, 0, /*hidden argument*/NULL);
		((Fraction32_t8_127_StaticFields*)Fraction32_t8_127_il2cpp_TypeInfo_var->static_fields)->___NaN_4 = L_0;
		Fraction32_t8_127  L_1 = {0};
		Fraction32__ctor_m8_659(&L_1, (-1), 0, /*hidden argument*/NULL);
		((Fraction32_t8_127_StaticFields*)Fraction32_t8_127_il2cpp_TypeInfo_var->static_fields)->___NegativeInfinity_5 = L_1;
		Fraction32_t8_127  L_2 = {0};
		Fraction32__ctor_m8_659(&L_2, 1, 0, /*hidden argument*/NULL);
		((Fraction32_t8_127_StaticFields*)Fraction32_t8_127_il2cpp_TypeInfo_var->static_fields)->___PositiveInfinity_6 = L_2;
		return;
	}
}
// System.Int32 ExifLibrary.MathEx/Fraction32::get_Numerator()
extern "C" int32_t Fraction32_get_Numerator_m8_666 (Fraction32_t8_127 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		bool L_0 = (__this->___mIsNegative_1);
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		G_B3_0 = (-1);
		goto IL_0012;
	}

IL_0011:
	{
		G_B3_0 = 1;
	}

IL_0012:
	{
		int32_t L_1 = (__this->___mNumerator_2);
		return ((int32_t)((int32_t)G_B3_0*(int32_t)L_1));
	}
}
// System.Void ExifLibrary.MathEx/Fraction32::set_Numerator(System.Int32)
extern TypeInfo* Fraction32_t8_127_il2cpp_TypeInfo_var;
extern "C" void Fraction32_set_Numerator_m8_667 (Fraction32_t8_127 * __this, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Fraction32_t8_127_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1974);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___value;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_001c;
		}
	}
	{
		__this->___mIsNegative_1 = 1;
		int32_t L_1 = ___value;
		__this->___mNumerator_2 = ((int32_t)((int32_t)(-1)*(int32_t)L_1));
		goto IL_002a;
	}

IL_001c:
	{
		__this->___mIsNegative_1 = 0;
		int32_t L_2 = ___value;
		__this->___mNumerator_2 = L_2;
	}

IL_002a:
	{
		int32_t* L_3 = &(__this->___mNumerator_2);
		int32_t* L_4 = &(__this->___mDenominator_3);
		IL2CPP_RUNTIME_CLASS_INIT(Fraction32_t8_127_il2cpp_TypeInfo_var);
		Fraction32_Reduce_m8_691(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 ExifLibrary.MathEx/Fraction32::get_Denominator()
extern "C" int32_t Fraction32_get_Denominator_m8_668 (Fraction32_t8_127 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___mDenominator_3);
		return L_0;
	}
}
// System.Void ExifLibrary.MathEx/Fraction32::set_Denominator(System.Int32)
extern TypeInfo* Fraction32_t8_127_il2cpp_TypeInfo_var;
extern "C" void Fraction32_set_Denominator_m8_669 (Fraction32_t8_127 * __this, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Fraction32_t8_127_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1974);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___value;
		int32_t L_1 = abs(L_0);
		__this->___mDenominator_3 = L_1;
		int32_t* L_2 = &(__this->___mNumerator_2);
		int32_t* L_3 = &(__this->___mDenominator_3);
		IL2CPP_RUNTIME_CLASS_INIT(Fraction32_t8_127_il2cpp_TypeInfo_var);
		Fraction32_Reduce_m8_691(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean ExifLibrary.MathEx/Fraction32::get_IsNegative()
extern "C" bool Fraction32_get_IsNegative_m8_670 (Fraction32_t8_127 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___mIsNegative_1);
		return L_0;
	}
}
// System.Void ExifLibrary.MathEx/Fraction32::set_IsNegative(System.Boolean)
extern "C" void Fraction32_set_IsNegative_m8_671 (Fraction32_t8_127 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___mIsNegative_1 = L_0;
		return;
	}
}
// System.Boolean ExifLibrary.MathEx/Fraction32::IsNan(ExifLibrary.MathEx/Fraction32)
extern "C" bool Fraction32_IsNan_m8_672 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = Fraction32_get_Numerator_m8_666((&___f), /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		int32_t L_1 = Fraction32_get_Denominator_m8_668((&___f), /*hidden argument*/NULL);
		G_B3_0 = ((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
		goto IL_0019;
	}

IL_0018:
	{
		G_B3_0 = 0;
	}

IL_0019:
	{
		return G_B3_0;
	}
}
// System.Boolean ExifLibrary.MathEx/Fraction32::IsNegativeInfinity(ExifLibrary.MathEx/Fraction32)
extern "C" bool Fraction32_IsNegativeInfinity_m8_673 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = Fraction32_get_Numerator_m8_666((&___f), /*hidden argument*/NULL);
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0019;
		}
	}
	{
		int32_t L_1 = Fraction32_get_Denominator_m8_668((&___f), /*hidden argument*/NULL);
		G_B3_0 = ((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
		goto IL_001a;
	}

IL_0019:
	{
		G_B3_0 = 0;
	}

IL_001a:
	{
		return G_B3_0;
	}
}
// System.Boolean ExifLibrary.MathEx/Fraction32::IsPositiveInfinity(ExifLibrary.MathEx/Fraction32)
extern "C" bool Fraction32_IsPositiveInfinity_m8_674 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = Fraction32_get_Numerator_m8_666((&___f), /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_0019;
		}
	}
	{
		int32_t L_1 = Fraction32_get_Denominator_m8_668((&___f), /*hidden argument*/NULL);
		G_B3_0 = ((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
		goto IL_001a;
	}

IL_0019:
	{
		G_B3_0 = 0;
	}

IL_001a:
	{
		return G_B3_0;
	}
}
// System.Boolean ExifLibrary.MathEx/Fraction32::IsInfinity(ExifLibrary.MathEx/Fraction32)
extern "C" bool Fraction32_IsInfinity_m8_675 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f, const MethodInfo* method)
{
	{
		int32_t L_0 = Fraction32_get_Denominator_m8_668((&___f), /*hidden argument*/NULL);
		return ((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
	}
}
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::Inverse(ExifLibrary.MathEx/Fraction32)
extern "C" Fraction32_t8_127  Fraction32_Inverse_m8_676 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f, const MethodInfo* method)
{
	{
		int32_t L_0 = Fraction32_get_Denominator_m8_668((&___f), /*hidden argument*/NULL);
		int32_t L_1 = Fraction32_get_Numerator_m8_666((&___f), /*hidden argument*/NULL);
		Fraction32_t8_127  L_2 = {0};
		Fraction32__ctor_m8_659(&L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::Parse(System.String)
extern TypeInfo* Fraction32_t8_127_il2cpp_TypeInfo_var;
extern "C" Fraction32_t8_127  Fraction32_Parse_m8_677 (Object_t * __this /* static, unused */, String_t* ___s, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Fraction32_t8_127_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1974);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___s;
		IL2CPP_RUNTIME_CLASS_INIT(Fraction32_t8_127_il2cpp_TypeInfo_var);
		Fraction32_t8_127  L_1 = Fraction32_FromString_m8_690(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean ExifLibrary.MathEx/Fraction32::TryParse(System.String,ExifLibrary.MathEx/Fraction32&)
extern TypeInfo* Fraction32_t8_127_il2cpp_TypeInfo_var;
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern "C" bool Fraction32_TryParse_m8_678 (Object_t * __this /* static, unused */, String_t* ___s, Fraction32_t8_127 * ___f, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Fraction32_t8_127_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1974);
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Fraction32_t8_127 * L_0 = ___f;
			String_t* L_1 = ___s;
			IL2CPP_RUNTIME_CLASS_INIT(Fraction32_t8_127_il2cpp_TypeInfo_var);
			Fraction32_t8_127  L_2 = Fraction32_Parse_m8_677(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
			(*(Fraction32_t8_127 *)L_0) = L_2;
			V_0 = 1;
			goto IL_002c;
		}

IL_0013:
		{
			; // IL_0013: leave IL_002c
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1_33 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Object_t_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0018;
		throw e;
	}

CATCH_0018:
	{ // begin catch(System.Object)
		{
			Fraction32_t8_127 * L_3 = ___f;
			Initobj (Fraction32_t8_127_il2cpp_TypeInfo_var, L_3);
			V_0 = 0;
			goto IL_002c;
		}

IL_0027:
		{
			; // IL_0027: leave IL_002c
		}
	} // end catch (depth: 1)

IL_002c:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
// System.Void ExifLibrary.MathEx/Fraction32::Set(System.Int32,System.Int32)
extern TypeInfo* Fraction32_t8_127_il2cpp_TypeInfo_var;
extern "C" void Fraction32_Set_m8_679 (Fraction32_t8_127 * __this, int32_t ___numerator, int32_t ___denominator, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Fraction32_t8_127_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1974);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___mIsNegative_1 = 0;
		int32_t L_0 = ___numerator;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0021;
		}
	}
	{
		bool L_1 = (__this->___mIsNegative_1);
		__this->___mIsNegative_1 = ((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
		int32_t L_2 = ___numerator;
		___numerator = ((-L_2));
	}

IL_0021:
	{
		int32_t L_3 = ___denominator;
		if ((((int32_t)L_3) >= ((int32_t)0)))
		{
			goto IL_003b;
		}
	}
	{
		bool L_4 = (__this->___mIsNegative_1);
		__this->___mIsNegative_1 = ((((int32_t)L_4) == ((int32_t)0))? 1 : 0);
		int32_t L_5 = ___denominator;
		___denominator = ((-L_5));
	}

IL_003b:
	{
		int32_t L_6 = ___numerator;
		__this->___mNumerator_2 = L_6;
		int32_t L_7 = ___denominator;
		__this->___mDenominator_3 = L_7;
		int32_t* L_8 = &(__this->___mNumerator_2);
		int32_t* L_9 = &(__this->___mDenominator_3);
		IL2CPP_RUNTIME_CLASS_INIT(Fraction32_t8_127_il2cpp_TypeInfo_var);
		Fraction32_Reduce_m8_691(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean ExifLibrary.MathEx/Fraction32::Equals(System.Object)
extern TypeInfo* Fraction32_t8_127_il2cpp_TypeInfo_var;
extern "C" bool Fraction32_Equals_m8_680 (Fraction32_t8_127 * __this, Object_t * ___obj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Fraction32_t8_127_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1974);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___obj;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return 0;
	}

IL_0008:
	{
		Object_t * L_1 = ___obj;
		if (!((Object_t *)IsInstSealed(L_1, Fraction32_t8_127_il2cpp_TypeInfo_var)))
		{
			goto IL_0020;
		}
	}
	{
		Object_t * L_2 = ___obj;
		bool L_3 = Fraction32_Equals_m8_681(__this, ((*(Fraction32_t8_127 *)((Fraction32_t8_127 *)UnBox (L_2, Fraction32_t8_127_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		return L_3;
	}

IL_0020:
	{
		return 0;
	}
}
// System.Boolean ExifLibrary.MathEx/Fraction32::Equals(ExifLibrary.MathEx/Fraction32)
extern "C" bool Fraction32_Equals_m8_681 (Fraction32_t8_127 * __this, Fraction32_t8_127  ___obj, const MethodInfo* method)
{
	int32_t G_B4_0 = 0;
	{
		bool L_0 = (__this->___mIsNegative_1);
		bool L_1 = Fraction32_get_IsNegative_m8_670((&___obj), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0035;
		}
	}
	{
		int32_t L_2 = (__this->___mNumerator_2);
		int32_t L_3 = Fraction32_get_Numerator_m8_666((&___obj), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) == ((uint32_t)L_3))))
		{
			goto IL_0035;
		}
	}
	{
		int32_t L_4 = (__this->___mDenominator_3);
		int32_t L_5 = Fraction32_get_Denominator_m8_668((&___obj), /*hidden argument*/NULL);
		G_B4_0 = ((((int32_t)L_4) == ((int32_t)L_5))? 1 : 0);
		goto IL_0036;
	}

IL_0035:
	{
		G_B4_0 = 0;
	}

IL_0036:
	{
		return G_B4_0;
	}
}
// System.Int32 ExifLibrary.MathEx/Fraction32::GetHashCode()
extern "C" int32_t Fraction32_GetHashCode_m8_682 (Fraction32_t8_127 * __this, const MethodInfo* method)
{
	int32_t G_B2_0 = 0;
	int32_t G_B1_0 = 0;
	int32_t G_B3_0 = 0;
	int32_t G_B3_1 = 0;
	{
		int32_t L_0 = (__this->___mDenominator_3);
		bool L_1 = (__this->___mIsNegative_1);
		G_B1_0 = L_0;
		if (!L_1)
		{
			G_B2_0 = L_0;
			goto IL_0017;
		}
	}
	{
		G_B3_0 = (-1);
		G_B3_1 = G_B1_0;
		goto IL_0018;
	}

IL_0017:
	{
		G_B3_0 = 1;
		G_B3_1 = G_B2_0;
	}

IL_0018:
	{
		int32_t L_2 = (__this->___mNumerator_2);
		return ((int32_t)((int32_t)G_B3_1^(int32_t)((int32_t)((int32_t)G_B3_0*(int32_t)L_2))));
	}
}
// System.String ExifLibrary.MathEx/Fraction32::ToString(System.String,System.IFormatProvider)
extern TypeInfo* StringBuilder_t1_247_il2cpp_TypeInfo_var;
extern "C" String_t* Fraction32_ToString_m8_683 (Fraction32_t8_127 * __this, String_t* ___format, Object_t * ___formatProvider, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringBuilder_t1_247_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t1_247 * V_0 = {0};
	int32_t V_1 = 0;
	StringBuilder_t1_247 * G_B2_0 = {0};
	StringBuilder_t1_247 * G_B1_0 = {0};
	int32_t G_B3_0 = 0;
	StringBuilder_t1_247 * G_B3_1 = {0};
	{
		StringBuilder_t1_247 * L_0 = (StringBuilder_t1_247 *)il2cpp_codegen_object_new (StringBuilder_t1_247_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1_12414(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		StringBuilder_t1_247 * L_1 = V_0;
		bool L_2 = (__this->___mIsNegative_1);
		G_B1_0 = L_1;
		if (!L_2)
		{
			G_B2_0 = L_1;
			goto IL_0018;
		}
	}
	{
		G_B3_0 = (-1);
		G_B3_1 = G_B1_0;
		goto IL_0019;
	}

IL_0018:
	{
		G_B3_0 = 1;
		G_B3_1 = G_B2_0;
	}

IL_0019:
	{
		int32_t L_3 = (__this->___mNumerator_2);
		V_1 = ((int32_t)((int32_t)G_B3_0*(int32_t)L_3));
		String_t* L_4 = ___format;
		Object_t * L_5 = ___formatProvider;
		String_t* L_6 = Int32_ToString_m1_104((&V_1), L_4, L_5, /*hidden argument*/NULL);
		NullCheck(G_B3_1);
		StringBuilder_Append_m1_12438(G_B3_1, L_6, /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_7 = V_0;
		NullCheck(L_7);
		StringBuilder_Append_m1_12452(L_7, ((int32_t)47), /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_8 = V_0;
		int32_t* L_9 = &(__this->___mDenominator_3);
		String_t* L_10 = ___format;
		Object_t * L_11 = ___formatProvider;
		String_t* L_12 = Int32_ToString_m1_104(L_9, L_10, L_11, /*hidden argument*/NULL);
		NullCheck(L_8);
		StringBuilder_Append_m1_12438(L_8, L_12, /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_13 = V_0;
		NullCheck(L_13);
		String_t* L_14 = StringBuilder_ToString_m1_12428(L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
// System.String ExifLibrary.MathEx/Fraction32::ToString(System.String)
extern TypeInfo* StringBuilder_t1_247_il2cpp_TypeInfo_var;
extern "C" String_t* Fraction32_ToString_m8_684 (Fraction32_t8_127 * __this, String_t* ___format, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringBuilder_t1_247_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t1_247 * V_0 = {0};
	int32_t V_1 = 0;
	StringBuilder_t1_247 * G_B2_0 = {0};
	StringBuilder_t1_247 * G_B1_0 = {0};
	int32_t G_B3_0 = 0;
	StringBuilder_t1_247 * G_B3_1 = {0};
	{
		StringBuilder_t1_247 * L_0 = (StringBuilder_t1_247 *)il2cpp_codegen_object_new (StringBuilder_t1_247_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1_12414(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		StringBuilder_t1_247 * L_1 = V_0;
		bool L_2 = (__this->___mIsNegative_1);
		G_B1_0 = L_1;
		if (!L_2)
		{
			G_B2_0 = L_1;
			goto IL_0018;
		}
	}
	{
		G_B3_0 = (-1);
		G_B3_1 = G_B1_0;
		goto IL_0019;
	}

IL_0018:
	{
		G_B3_0 = 1;
		G_B3_1 = G_B2_0;
	}

IL_0019:
	{
		int32_t L_3 = (__this->___mNumerator_2);
		V_1 = ((int32_t)((int32_t)G_B3_0*(int32_t)L_3));
		String_t* L_4 = ___format;
		String_t* L_5 = Int32_ToString_m1_103((&V_1), L_4, /*hidden argument*/NULL);
		NullCheck(G_B3_1);
		StringBuilder_Append_m1_12438(G_B3_1, L_5, /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_6 = V_0;
		NullCheck(L_6);
		StringBuilder_Append_m1_12452(L_6, ((int32_t)47), /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_7 = V_0;
		int32_t* L_8 = &(__this->___mDenominator_3);
		String_t* L_9 = ___format;
		String_t* L_10 = Int32_ToString_m1_103(L_8, L_9, /*hidden argument*/NULL);
		NullCheck(L_7);
		StringBuilder_Append_m1_12438(L_7, L_10, /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_11 = V_0;
		NullCheck(L_11);
		String_t* L_12 = StringBuilder_ToString_m1_12428(L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.String ExifLibrary.MathEx/Fraction32::ToString(System.IFormatProvider)
extern TypeInfo* StringBuilder_t1_247_il2cpp_TypeInfo_var;
extern "C" String_t* Fraction32_ToString_m8_685 (Fraction32_t8_127 * __this, Object_t * ___formatProvider, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringBuilder_t1_247_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t1_247 * V_0 = {0};
	int32_t V_1 = 0;
	StringBuilder_t1_247 * G_B2_0 = {0};
	StringBuilder_t1_247 * G_B1_0 = {0};
	int32_t G_B3_0 = 0;
	StringBuilder_t1_247 * G_B3_1 = {0};
	{
		StringBuilder_t1_247 * L_0 = (StringBuilder_t1_247 *)il2cpp_codegen_object_new (StringBuilder_t1_247_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1_12414(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		StringBuilder_t1_247 * L_1 = V_0;
		bool L_2 = (__this->___mIsNegative_1);
		G_B1_0 = L_1;
		if (!L_2)
		{
			G_B2_0 = L_1;
			goto IL_0018;
		}
	}
	{
		G_B3_0 = (-1);
		G_B3_1 = G_B1_0;
		goto IL_0019;
	}

IL_0018:
	{
		G_B3_0 = 1;
		G_B3_1 = G_B2_0;
	}

IL_0019:
	{
		int32_t L_3 = (__this->___mNumerator_2);
		V_1 = ((int32_t)((int32_t)G_B3_0*(int32_t)L_3));
		Object_t * L_4 = ___formatProvider;
		String_t* L_5 = Int32_ToString_m1_102((&V_1), L_4, /*hidden argument*/NULL);
		NullCheck(G_B3_1);
		StringBuilder_Append_m1_12438(G_B3_1, L_5, /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_6 = V_0;
		NullCheck(L_6);
		StringBuilder_Append_m1_12452(L_6, ((int32_t)47), /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_7 = V_0;
		int32_t* L_8 = &(__this->___mDenominator_3);
		Object_t * L_9 = ___formatProvider;
		String_t* L_10 = Int32_ToString_m1_102(L_8, L_9, /*hidden argument*/NULL);
		NullCheck(L_7);
		StringBuilder_Append_m1_12438(L_7, L_10, /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_11 = V_0;
		NullCheck(L_11);
		String_t* L_12 = StringBuilder_ToString_m1_12428(L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.String ExifLibrary.MathEx/Fraction32::ToString()
extern TypeInfo* StringBuilder_t1_247_il2cpp_TypeInfo_var;
extern "C" String_t* Fraction32_ToString_m8_686 (Fraction32_t8_127 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringBuilder_t1_247_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t1_247 * V_0 = {0};
	int32_t V_1 = 0;
	StringBuilder_t1_247 * G_B2_0 = {0};
	StringBuilder_t1_247 * G_B1_0 = {0};
	int32_t G_B3_0 = 0;
	StringBuilder_t1_247 * G_B3_1 = {0};
	{
		StringBuilder_t1_247 * L_0 = (StringBuilder_t1_247 *)il2cpp_codegen_object_new (StringBuilder_t1_247_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1_12414(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		StringBuilder_t1_247 * L_1 = V_0;
		bool L_2 = (__this->___mIsNegative_1);
		G_B1_0 = L_1;
		if (!L_2)
		{
			G_B2_0 = L_1;
			goto IL_0018;
		}
	}
	{
		G_B3_0 = (-1);
		G_B3_1 = G_B1_0;
		goto IL_0019;
	}

IL_0018:
	{
		G_B3_0 = 1;
		G_B3_1 = G_B2_0;
	}

IL_0019:
	{
		int32_t L_3 = (__this->___mNumerator_2);
		V_1 = ((int32_t)((int32_t)G_B3_0*(int32_t)L_3));
		String_t* L_4 = Int32_ToString_m1_101((&V_1), /*hidden argument*/NULL);
		NullCheck(G_B3_1);
		StringBuilder_Append_m1_12438(G_B3_1, L_4, /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_5 = V_0;
		NullCheck(L_5);
		StringBuilder_Append_m1_12452(L_5, ((int32_t)47), /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_6 = V_0;
		int32_t* L_7 = &(__this->___mDenominator_3);
		String_t* L_8 = Int32_ToString_m1_101(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		StringBuilder_Append_m1_12438(L_6, L_8, /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_9 = V_0;
		NullCheck(L_9);
		String_t* L_10 = StringBuilder_ToString_m1_12428(L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// System.Int32 ExifLibrary.MathEx/Fraction32::CompareTo(System.Object)
extern TypeInfo* Fraction32_t8_127_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5229;
extern Il2CppCodeGenString* _stringLiteral854;
extern "C" int32_t Fraction32_CompareTo_m8_687 (Fraction32_t8_127 * __this, Object_t * ___obj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Fraction32_t8_127_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1974);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		_stringLiteral5229 = il2cpp_codegen_string_literal_from_index(5229);
		_stringLiteral854 = il2cpp_codegen_string_literal_from_index(854);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___obj;
		if (((Object_t *)IsInstSealed(L_0, Fraction32_t8_127_il2cpp_TypeInfo_var)))
		{
			goto IL_001b;
		}
	}
	{
		ArgumentException_t1_1425 * L_1 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13262(L_1, _stringLiteral5229, _stringLiteral854, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_001b:
	{
		Object_t * L_2 = ___obj;
		int32_t L_3 = Fraction32_CompareTo_m8_688(__this, ((*(Fraction32_t8_127 *)((Fraction32_t8_127 *)UnBox (L_2, Fraction32_t8_127_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Int32 ExifLibrary.MathEx/Fraction32::CompareTo(ExifLibrary.MathEx/Fraction32)
extern TypeInfo* Fraction32_t8_127_il2cpp_TypeInfo_var;
extern "C" int32_t Fraction32_CompareTo_m8_688 (Fraction32_t8_127 * __this, Fraction32_t8_127  ___obj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Fraction32_t8_127_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1974);
		s_Il2CppMethodIntialized = true;
	}
	{
		Fraction32_t8_127  L_0 = ___obj;
		IL2CPP_RUNTIME_CLASS_INIT(Fraction32_t8_127_il2cpp_TypeInfo_var);
		bool L_1 = Fraction32_op_LessThan_m8_724(NULL /*static, unused*/, (*(Fraction32_t8_127 *)__this), L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		return (-1);
	}

IL_0013:
	{
		Fraction32_t8_127  L_2 = ___obj;
		IL2CPP_RUNTIME_CLASS_INIT(Fraction32_t8_127_il2cpp_TypeInfo_var);
		bool L_3 = Fraction32_op_GreaterThan_m8_725(NULL /*static, unused*/, (*(Fraction32_t8_127 *)__this), L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0026;
		}
	}
	{
		return 1;
	}

IL_0026:
	{
		return 0;
	}
}
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::FromDouble(System.Double,System.Int32)
extern TypeInfo* Fraction32_t8_127_il2cpp_TypeInfo_var;
extern "C" Fraction32_t8_127  Fraction32_FromDouble_m8_689 (Object_t * __this /* static, unused */, double ___value, int32_t ___precision, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Fraction32_t8_127_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1974);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t G_B11_0 = 0;
	{
		double L_0 = ___value;
		bool L_1 = Double_IsNaN_m1_659(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Fraction32_t8_127_il2cpp_TypeInfo_var);
		Fraction32_t8_127  L_2 = ((Fraction32_t8_127_StaticFields*)Fraction32_t8_127_il2cpp_TypeInfo_var->static_fields)->___NaN_4;
		return L_2;
	}

IL_0011:
	{
		double L_3 = ___value;
		bool L_4 = Double_IsNegativeInfinity_m1_660(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0022;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Fraction32_t8_127_il2cpp_TypeInfo_var);
		Fraction32_t8_127  L_5 = ((Fraction32_t8_127_StaticFields*)Fraction32_t8_127_il2cpp_TypeInfo_var->static_fields)->___NegativeInfinity_5;
		return L_5;
	}

IL_0022:
	{
		double L_6 = ___value;
		bool L_7 = Double_IsPositiveInfinity_m1_661(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0033;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Fraction32_t8_127_il2cpp_TypeInfo_var);
		Fraction32_t8_127  L_8 = ((Fraction32_t8_127_StaticFields*)Fraction32_t8_127_il2cpp_TypeInfo_var->static_fields)->___PositiveInfinity_6;
		return L_8;
	}

IL_0033:
	{
		double L_9 = ___value;
		V_0 = ((((double)L_9) < ((double)(0.0)))? 1 : 0);
		bool L_10 = V_0;
		if (!L_10)
		{
			goto IL_004a;
		}
	}
	{
		double L_11 = ___value;
		___value = ((-L_11));
	}

IL_004a:
	{
		int32_t L_12 = ___precision;
		double L_13 = pow((10.0), (((double)((double)L_12))));
		V_1 = (((int32_t)((int32_t)L_13)));
		double L_14 = ___value;
		int32_t L_15 = V_1;
		V_2 = (((int32_t)((int32_t)((double)((double)L_14*(double)(((double)((double)L_15))))))));
		int32_t L_16 = V_1;
		V_3 = L_16;
		int32_t L_17 = V_2;
		int32_t L_18 = V_3;
		uint32_t L_19 = MathEx_GCD_m8_789(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		V_4 = L_19;
		int32_t L_20 = V_2;
		int32_t L_21 = V_4;
		V_2 = ((int32_t)((int32_t)L_20/(int32_t)L_21));
		int32_t L_22 = V_3;
		int32_t L_23 = V_4;
		V_3 = ((int32_t)((int32_t)L_22/(int32_t)L_23));
		bool L_24 = V_0;
		if (!L_24)
		{
			goto IL_0083;
		}
	}
	{
		G_B11_0 = (-1);
		goto IL_0084;
	}

IL_0083:
	{
		G_B11_0 = 1;
	}

IL_0084:
	{
		int32_t L_25 = V_2;
		int32_t L_26 = V_3;
		Fraction32_t8_127  L_27 = {0};
		Fraction32__ctor_m8_659(&L_27, ((int32_t)((int32_t)G_B11_0*(int32_t)L_25)), L_26, /*hidden argument*/NULL);
		return L_27;
	}
}
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::FromString(System.String)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t1_16_il2cpp_TypeInfo_var;
extern TypeInfo* Fraction32_t8_127_il2cpp_TypeInfo_var;
extern TypeInfo* FormatException_t1_592_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral7;
extern Il2CppCodeGenString* _stringLiteral5230;
extern "C" Fraction32_t8_127  Fraction32_FromString_m8_690 (Object_t * __this /* static, unused */, String_t* ___s, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		CharU5BU5D_t1_16_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(15);
		Fraction32_t8_127_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1974);
		FormatException_t1_592_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(16);
		_stringLiteral7 = il2cpp_codegen_string_literal_from_index(7);
		_stringLiteral5230 = il2cpp_codegen_string_literal_from_index(5230);
		s_Il2CppMethodIntialized = true;
	}
	StringU5BU5D_t1_238* V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	double V_3 = 0.0;
	{
		String_t* L_0 = ___s;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, _stringLiteral7, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		String_t* L_2 = ___s;
		CharU5BU5D_t1_16* L_3 = ((CharU5BU5D_t1_16*)SZArrayNew(CharU5BU5D_t1_16_il2cpp_TypeInfo_var, 1));
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_3, 0, sizeof(uint16_t))) = (uint16_t)((int32_t)47);
		NullCheck(L_2);
		StringU5BU5D_t1_238* L_4 = String_Split_m1_448(L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		V_1 = 1;
		V_2 = 1;
		StringU5BU5D_t1_238* L_5 = V_0;
		NullCheck(L_5);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Array_t *)L_5)->max_length))))) == ((uint32_t)1))))
		{
			goto IL_005c;
		}
	}
	{
		StringU5BU5D_t1_238* L_6 = V_0;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 0);
		int32_t L_7 = 0;
		bool L_8 = Int32_TryParse_m1_99(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_6, L_7, sizeof(String_t*))), (&V_1), /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0046;
		}
	}
	{
		V_2 = 1;
		goto IL_0057;
	}

IL_0046:
	{
		StringU5BU5D_t1_238* L_9 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 0);
		int32_t L_10 = 0;
		double L_11 = Double_Parse_m1_662(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_9, L_10, sizeof(String_t*))), /*hidden argument*/NULL);
		V_3 = L_11;
		double L_12 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Fraction32_t8_127_il2cpp_TypeInfo_var);
		Fraction32_t8_127  L_13 = Fraction32_FromDouble_m8_689(NULL /*static, unused*/, L_12, 8, /*hidden argument*/NULL);
		return L_13;
	}

IL_0057:
	{
		goto IL_0087;
	}

IL_005c:
	{
		StringU5BU5D_t1_238* L_14 = V_0;
		NullCheck(L_14);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Array_t *)L_14)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_007c;
		}
	}
	{
		StringU5BU5D_t1_238* L_15 = V_0;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 0);
		int32_t L_16 = 0;
		int32_t L_17 = Int32_Parse_m1_97(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_15, L_16, sizeof(String_t*))), /*hidden argument*/NULL);
		V_1 = L_17;
		StringU5BU5D_t1_238* L_18 = V_0;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 1);
		int32_t L_19 = 1;
		int32_t L_20 = Int32_Parse_m1_97(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_18, L_19, sizeof(String_t*))), /*hidden argument*/NULL);
		V_2 = L_20;
		goto IL_0087;
	}

IL_007c:
	{
		FormatException_t1_592 * L_21 = (FormatException_t1_592 *)il2cpp_codegen_object_new (FormatException_t1_592_il2cpp_TypeInfo_var);
		FormatException__ctor_m1_14100(L_21, _stringLiteral5230, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_21);
	}

IL_0087:
	{
		int32_t L_22 = V_1;
		int32_t L_23 = V_2;
		Fraction32_t8_127  L_24 = {0};
		Fraction32__ctor_m8_659(&L_24, L_22, L_23, /*hidden argument*/NULL);
		return L_24;
	}
}
// System.Void ExifLibrary.MathEx/Fraction32::Reduce(System.Int32&,System.Int32&)
extern "C" void Fraction32_Reduce_m8_691 (Object_t * __this /* static, unused */, int32_t* ___numerator, int32_t* ___denominator, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	{
		int32_t* L_0 = ___numerator;
		int32_t* L_1 = ___denominator;
		uint32_t L_2 = MathEx_GCD_m8_789(NULL /*static, unused*/, (*((int32_t*)L_0)), (*((int32_t*)L_1)), /*hidden argument*/NULL);
		V_0 = L_2;
		uint32_t L_3 = V_0;
		if (L_3)
		{
			goto IL_0012;
		}
	}
	{
		V_0 = 1;
	}

IL_0012:
	{
		int32_t* L_4 = ___numerator;
		int32_t* L_5 = ___numerator;
		uint32_t L_6 = V_0;
		*((int32_t*)(L_4)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_5))/(int32_t)L_6));
		int32_t* L_7 = ___denominator;
		int32_t* L_8 = ___denominator;
		uint32_t L_9 = V_0;
		*((int32_t*)(L_7)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_8))/(int32_t)L_9));
		return;
	}
}
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::op_Multiply(ExifLibrary.MathEx/Fraction32,System.Int32)
extern "C" Fraction32_t8_127  Fraction32_op_Multiply_m8_692 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f, int32_t ___n, const MethodInfo* method)
{
	{
		int32_t L_0 = Fraction32_get_Numerator_m8_666((&___f), /*hidden argument*/NULL);
		int32_t L_1 = ___n;
		int32_t L_2 = Fraction32_get_Denominator_m8_668((&___f), /*hidden argument*/NULL);
		int32_t L_3 = ___n;
		int32_t L_4 = abs(L_3);
		Fraction32_t8_127  L_5 = {0};
		Fraction32__ctor_m8_659(&L_5, ((int32_t)((int32_t)L_0*(int32_t)L_1)), ((int32_t)((int32_t)L_2*(int32_t)L_4)), /*hidden argument*/NULL);
		return L_5;
	}
}
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::op_Multiply(System.Int32,ExifLibrary.MathEx/Fraction32)
extern TypeInfo* Fraction32_t8_127_il2cpp_TypeInfo_var;
extern "C" Fraction32_t8_127  Fraction32_op_Multiply_m8_693 (Object_t * __this /* static, unused */, int32_t ___n, Fraction32_t8_127  ___f, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Fraction32_t8_127_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1974);
		s_Il2CppMethodIntialized = true;
	}
	{
		Fraction32_t8_127  L_0 = ___f;
		int32_t L_1 = ___n;
		IL2CPP_RUNTIME_CLASS_INIT(Fraction32_t8_127_il2cpp_TypeInfo_var);
		Fraction32_t8_127  L_2 = Fraction32_op_Multiply_m8_692(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::op_Multiply(ExifLibrary.MathEx/Fraction32,System.Single)
extern TypeInfo* Fraction32_t8_127_il2cpp_TypeInfo_var;
extern "C" Fraction32_t8_127  Fraction32_op_Multiply_m8_694 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f, float ___n, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Fraction32_t8_127_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1974);
		s_Il2CppMethodIntialized = true;
	}
	{
		Fraction32_t8_127  L_0 = ___f;
		IL2CPP_RUNTIME_CLASS_INIT(Fraction32_t8_127_il2cpp_TypeInfo_var);
		float L_1 = Fraction32_op_Explicit_m8_720(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		float L_2 = ___n;
		Fraction32_t8_127  L_3 = {0};
		Fraction32__ctor_m8_662(&L_3, ((float)((float)L_1*(float)L_2)), /*hidden argument*/NULL);
		return L_3;
	}
}
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::op_Multiply(System.Single,ExifLibrary.MathEx/Fraction32)
extern TypeInfo* Fraction32_t8_127_il2cpp_TypeInfo_var;
extern "C" Fraction32_t8_127  Fraction32_op_Multiply_m8_695 (Object_t * __this /* static, unused */, float ___n, Fraction32_t8_127  ___f, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Fraction32_t8_127_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1974);
		s_Il2CppMethodIntialized = true;
	}
	{
		Fraction32_t8_127  L_0 = ___f;
		float L_1 = ___n;
		IL2CPP_RUNTIME_CLASS_INIT(Fraction32_t8_127_il2cpp_TypeInfo_var);
		Fraction32_t8_127  L_2 = Fraction32_op_Multiply_m8_694(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::op_Multiply(ExifLibrary.MathEx/Fraction32,System.Double)
extern TypeInfo* Fraction32_t8_127_il2cpp_TypeInfo_var;
extern "C" Fraction32_t8_127  Fraction32_op_Multiply_m8_696 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f, double ___n, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Fraction32_t8_127_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1974);
		s_Il2CppMethodIntialized = true;
	}
	{
		Fraction32_t8_127  L_0 = ___f;
		IL2CPP_RUNTIME_CLASS_INIT(Fraction32_t8_127_il2cpp_TypeInfo_var);
		double L_1 = Fraction32_op_Explicit_m8_721(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		double L_2 = ___n;
		Fraction32_t8_127  L_3 = {0};
		Fraction32__ctor_m8_663(&L_3, ((double)((double)L_1*(double)L_2)), /*hidden argument*/NULL);
		return L_3;
	}
}
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::op_Multiply(System.Double,ExifLibrary.MathEx/Fraction32)
extern TypeInfo* Fraction32_t8_127_il2cpp_TypeInfo_var;
extern "C" Fraction32_t8_127  Fraction32_op_Multiply_m8_697 (Object_t * __this /* static, unused */, double ___n, Fraction32_t8_127  ___f, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Fraction32_t8_127_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1974);
		s_Il2CppMethodIntialized = true;
	}
	{
		Fraction32_t8_127  L_0 = ___f;
		double L_1 = ___n;
		IL2CPP_RUNTIME_CLASS_INIT(Fraction32_t8_127_il2cpp_TypeInfo_var);
		Fraction32_t8_127  L_2 = Fraction32_op_Multiply_m8_696(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::op_Multiply(ExifLibrary.MathEx/Fraction32,ExifLibrary.MathEx/Fraction32)
extern "C" Fraction32_t8_127  Fraction32_op_Multiply_m8_698 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f1, Fraction32_t8_127  ___f2, const MethodInfo* method)
{
	{
		int32_t L_0 = Fraction32_get_Numerator_m8_666((&___f1), /*hidden argument*/NULL);
		int32_t L_1 = Fraction32_get_Numerator_m8_666((&___f2), /*hidden argument*/NULL);
		int32_t L_2 = Fraction32_get_Denominator_m8_668((&___f1), /*hidden argument*/NULL);
		int32_t L_3 = Fraction32_get_Denominator_m8_668((&___f2), /*hidden argument*/NULL);
		Fraction32_t8_127  L_4 = {0};
		Fraction32__ctor_m8_659(&L_4, ((int32_t)((int32_t)L_0*(int32_t)L_1)), ((int32_t)((int32_t)L_2*(int32_t)L_3)), /*hidden argument*/NULL);
		return L_4;
	}
}
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::op_Division(ExifLibrary.MathEx/Fraction32,System.Int32)
extern "C" Fraction32_t8_127  Fraction32_op_Division_m8_699 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f, int32_t ___n, const MethodInfo* method)
{
	{
		int32_t L_0 = Fraction32_get_Numerator_m8_666((&___f), /*hidden argument*/NULL);
		int32_t L_1 = ___n;
		int32_t L_2 = Fraction32_get_Denominator_m8_668((&___f), /*hidden argument*/NULL);
		int32_t L_3 = ___n;
		int32_t L_4 = abs(L_3);
		Fraction32_t8_127  L_5 = {0};
		Fraction32__ctor_m8_659(&L_5, ((int32_t)((int32_t)L_0/(int32_t)L_1)), ((int32_t)((int32_t)L_2/(int32_t)L_4)), /*hidden argument*/NULL);
		return L_5;
	}
}
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::op_Division(ExifLibrary.MathEx/Fraction32,System.Single)
extern TypeInfo* Fraction32_t8_127_il2cpp_TypeInfo_var;
extern "C" Fraction32_t8_127  Fraction32_op_Division_m8_700 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f, float ___n, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Fraction32_t8_127_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1974);
		s_Il2CppMethodIntialized = true;
	}
	{
		Fraction32_t8_127  L_0 = ___f;
		IL2CPP_RUNTIME_CLASS_INIT(Fraction32_t8_127_il2cpp_TypeInfo_var);
		float L_1 = Fraction32_op_Explicit_m8_720(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		float L_2 = ___n;
		Fraction32_t8_127  L_3 = {0};
		Fraction32__ctor_m8_662(&L_3, ((float)((float)L_1/(float)L_2)), /*hidden argument*/NULL);
		return L_3;
	}
}
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::op_Division(ExifLibrary.MathEx/Fraction32,System.Double)
extern TypeInfo* Fraction32_t8_127_il2cpp_TypeInfo_var;
extern "C" Fraction32_t8_127  Fraction32_op_Division_m8_701 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f, double ___n, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Fraction32_t8_127_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1974);
		s_Il2CppMethodIntialized = true;
	}
	{
		Fraction32_t8_127  L_0 = ___f;
		IL2CPP_RUNTIME_CLASS_INIT(Fraction32_t8_127_il2cpp_TypeInfo_var);
		double L_1 = Fraction32_op_Explicit_m8_721(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		double L_2 = ___n;
		Fraction32_t8_127  L_3 = {0};
		Fraction32__ctor_m8_663(&L_3, ((double)((double)L_1/(double)L_2)), /*hidden argument*/NULL);
		return L_3;
	}
}
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::op_Division(ExifLibrary.MathEx/Fraction32,ExifLibrary.MathEx/Fraction32)
extern TypeInfo* Fraction32_t8_127_il2cpp_TypeInfo_var;
extern "C" Fraction32_t8_127  Fraction32_op_Division_m8_702 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f1, Fraction32_t8_127  ___f2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Fraction32_t8_127_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1974);
		s_Il2CppMethodIntialized = true;
	}
	{
		Fraction32_t8_127  L_0 = ___f1;
		Fraction32_t8_127  L_1 = ___f2;
		IL2CPP_RUNTIME_CLASS_INIT(Fraction32_t8_127_il2cpp_TypeInfo_var);
		Fraction32_t8_127  L_2 = Fraction32_Inverse_m8_676(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		Fraction32_t8_127  L_3 = Fraction32_op_Multiply_m8_698(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::op_Addition(ExifLibrary.MathEx/Fraction32,System.Int32)
extern TypeInfo* Fraction32_t8_127_il2cpp_TypeInfo_var;
extern "C" Fraction32_t8_127  Fraction32_op_Addition_m8_703 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f, int32_t ___n, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Fraction32_t8_127_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1974);
		s_Il2CppMethodIntialized = true;
	}
	{
		Fraction32_t8_127  L_0 = ___f;
		int32_t L_1 = ___n;
		Fraction32_t8_127  L_2 = {0};
		Fraction32__ctor_m8_659(&L_2, L_1, 1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Fraction32_t8_127_il2cpp_TypeInfo_var);
		Fraction32_t8_127  L_3 = Fraction32_op_Addition_m8_709(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::op_Addition(System.Int32,ExifLibrary.MathEx/Fraction32)
extern TypeInfo* Fraction32_t8_127_il2cpp_TypeInfo_var;
extern "C" Fraction32_t8_127  Fraction32_op_Addition_m8_704 (Object_t * __this /* static, unused */, int32_t ___n, Fraction32_t8_127  ___f, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Fraction32_t8_127_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1974);
		s_Il2CppMethodIntialized = true;
	}
	{
		Fraction32_t8_127  L_0 = ___f;
		int32_t L_1 = ___n;
		IL2CPP_RUNTIME_CLASS_INIT(Fraction32_t8_127_il2cpp_TypeInfo_var);
		Fraction32_t8_127  L_2 = Fraction32_op_Addition_m8_703(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::op_Addition(ExifLibrary.MathEx/Fraction32,System.Single)
extern TypeInfo* Fraction32_t8_127_il2cpp_TypeInfo_var;
extern "C" Fraction32_t8_127  Fraction32_op_Addition_m8_705 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f, float ___n, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Fraction32_t8_127_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1974);
		s_Il2CppMethodIntialized = true;
	}
	{
		Fraction32_t8_127  L_0 = ___f;
		IL2CPP_RUNTIME_CLASS_INIT(Fraction32_t8_127_il2cpp_TypeInfo_var);
		float L_1 = Fraction32_op_Explicit_m8_720(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		float L_2 = ___n;
		Fraction32_t8_127  L_3 = {0};
		Fraction32__ctor_m8_662(&L_3, ((float)((float)L_1+(float)L_2)), /*hidden argument*/NULL);
		return L_3;
	}
}
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::op_Addition(System.Single,ExifLibrary.MathEx/Fraction32)
extern TypeInfo* Fraction32_t8_127_il2cpp_TypeInfo_var;
extern "C" Fraction32_t8_127  Fraction32_op_Addition_m8_706 (Object_t * __this /* static, unused */, float ___n, Fraction32_t8_127  ___f, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Fraction32_t8_127_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1974);
		s_Il2CppMethodIntialized = true;
	}
	{
		Fraction32_t8_127  L_0 = ___f;
		float L_1 = ___n;
		IL2CPP_RUNTIME_CLASS_INIT(Fraction32_t8_127_il2cpp_TypeInfo_var);
		Fraction32_t8_127  L_2 = Fraction32_op_Addition_m8_705(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::op_Addition(ExifLibrary.MathEx/Fraction32,System.Double)
extern TypeInfo* Fraction32_t8_127_il2cpp_TypeInfo_var;
extern "C" Fraction32_t8_127  Fraction32_op_Addition_m8_707 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f, double ___n, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Fraction32_t8_127_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1974);
		s_Il2CppMethodIntialized = true;
	}
	{
		Fraction32_t8_127  L_0 = ___f;
		IL2CPP_RUNTIME_CLASS_INIT(Fraction32_t8_127_il2cpp_TypeInfo_var);
		double L_1 = Fraction32_op_Explicit_m8_721(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		double L_2 = ___n;
		Fraction32_t8_127  L_3 = {0};
		Fraction32__ctor_m8_663(&L_3, ((double)((double)L_1+(double)L_2)), /*hidden argument*/NULL);
		return L_3;
	}
}
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::op_Addition(System.Double,ExifLibrary.MathEx/Fraction32)
extern TypeInfo* Fraction32_t8_127_il2cpp_TypeInfo_var;
extern "C" Fraction32_t8_127  Fraction32_op_Addition_m8_708 (Object_t * __this /* static, unused */, double ___n, Fraction32_t8_127  ___f, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Fraction32_t8_127_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1974);
		s_Il2CppMethodIntialized = true;
	}
	{
		Fraction32_t8_127  L_0 = ___f;
		double L_1 = ___n;
		IL2CPP_RUNTIME_CLASS_INIT(Fraction32_t8_127_il2cpp_TypeInfo_var);
		Fraction32_t8_127  L_2 = Fraction32_op_Addition_m8_707(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::op_Addition(ExifLibrary.MathEx/Fraction32,ExifLibrary.MathEx/Fraction32)
extern "C" Fraction32_t8_127  Fraction32_op_Addition_m8_709 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f1, Fraction32_t8_127  ___f2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		int32_t L_0 = Fraction32_get_Numerator_m8_666((&___f1), /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Fraction32_get_Denominator_m8_668((&___f2), /*hidden argument*/NULL);
		V_1 = L_1;
		int32_t L_2 = Fraction32_get_Numerator_m8_666((&___f2), /*hidden argument*/NULL);
		V_2 = L_2;
		int32_t L_3 = Fraction32_get_Denominator_m8_668((&___f2), /*hidden argument*/NULL);
		V_3 = L_3;
		int32_t L_4 = V_0;
		int32_t L_5 = V_3;
		int32_t L_6 = V_2;
		int32_t L_7 = V_1;
		int32_t L_8 = V_1;
		int32_t L_9 = V_3;
		Fraction32_t8_127  L_10 = {0};
		Fraction32__ctor_m8_659(&L_10, ((int32_t)((int32_t)((int32_t)((int32_t)L_4*(int32_t)L_5))+(int32_t)((int32_t)((int32_t)L_6*(int32_t)L_7)))), ((int32_t)((int32_t)L_8*(int32_t)L_9)), /*hidden argument*/NULL);
		return L_10;
	}
}
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::op_Subtraction(ExifLibrary.MathEx/Fraction32,System.Int32)
extern TypeInfo* Fraction32_t8_127_il2cpp_TypeInfo_var;
extern "C" Fraction32_t8_127  Fraction32_op_Subtraction_m8_710 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f, int32_t ___n, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Fraction32_t8_127_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1974);
		s_Il2CppMethodIntialized = true;
	}
	{
		Fraction32_t8_127  L_0 = ___f;
		int32_t L_1 = ___n;
		Fraction32_t8_127  L_2 = {0};
		Fraction32__ctor_m8_659(&L_2, L_1, 1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Fraction32_t8_127_il2cpp_TypeInfo_var);
		Fraction32_t8_127  L_3 = Fraction32_op_Subtraction_m8_716(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::op_Subtraction(System.Int32,ExifLibrary.MathEx/Fraction32)
extern TypeInfo* Fraction32_t8_127_il2cpp_TypeInfo_var;
extern "C" Fraction32_t8_127  Fraction32_op_Subtraction_m8_711 (Object_t * __this /* static, unused */, int32_t ___n, Fraction32_t8_127  ___f, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Fraction32_t8_127_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1974);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___n;
		Fraction32_t8_127  L_1 = {0};
		Fraction32__ctor_m8_659(&L_1, L_0, 1, /*hidden argument*/NULL);
		Fraction32_t8_127  L_2 = ___f;
		IL2CPP_RUNTIME_CLASS_INIT(Fraction32_t8_127_il2cpp_TypeInfo_var);
		Fraction32_t8_127  L_3 = Fraction32_op_Subtraction_m8_716(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::op_Subtraction(ExifLibrary.MathEx/Fraction32,System.Single)
extern TypeInfo* Fraction32_t8_127_il2cpp_TypeInfo_var;
extern "C" Fraction32_t8_127  Fraction32_op_Subtraction_m8_712 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f, float ___n, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Fraction32_t8_127_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1974);
		s_Il2CppMethodIntialized = true;
	}
	{
		Fraction32_t8_127  L_0 = ___f;
		IL2CPP_RUNTIME_CLASS_INIT(Fraction32_t8_127_il2cpp_TypeInfo_var);
		float L_1 = Fraction32_op_Explicit_m8_720(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		float L_2 = ___n;
		Fraction32_t8_127  L_3 = {0};
		Fraction32__ctor_m8_662(&L_3, ((float)((float)L_1-(float)L_2)), /*hidden argument*/NULL);
		return L_3;
	}
}
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::op_Subtraction(System.Single,ExifLibrary.MathEx/Fraction32)
extern TypeInfo* Fraction32_t8_127_il2cpp_TypeInfo_var;
extern "C" Fraction32_t8_127  Fraction32_op_Subtraction_m8_713 (Object_t * __this /* static, unused */, float ___n, Fraction32_t8_127  ___f, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Fraction32_t8_127_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1974);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___n;
		Fraction32_t8_127  L_1 = {0};
		Fraction32__ctor_m8_662(&L_1, L_0, /*hidden argument*/NULL);
		Fraction32_t8_127  L_2 = ___f;
		IL2CPP_RUNTIME_CLASS_INIT(Fraction32_t8_127_il2cpp_TypeInfo_var);
		Fraction32_t8_127  L_3 = Fraction32_op_Subtraction_m8_716(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::op_Subtraction(ExifLibrary.MathEx/Fraction32,System.Double)
extern TypeInfo* Fraction32_t8_127_il2cpp_TypeInfo_var;
extern "C" Fraction32_t8_127  Fraction32_op_Subtraction_m8_714 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f, double ___n, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Fraction32_t8_127_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1974);
		s_Il2CppMethodIntialized = true;
	}
	{
		Fraction32_t8_127  L_0 = ___f;
		IL2CPP_RUNTIME_CLASS_INIT(Fraction32_t8_127_il2cpp_TypeInfo_var);
		double L_1 = Fraction32_op_Explicit_m8_721(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		double L_2 = ___n;
		Fraction32_t8_127  L_3 = {0};
		Fraction32__ctor_m8_663(&L_3, ((double)((double)L_1-(double)L_2)), /*hidden argument*/NULL);
		return L_3;
	}
}
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::op_Subtraction(System.Double,ExifLibrary.MathEx/Fraction32)
extern TypeInfo* Fraction32_t8_127_il2cpp_TypeInfo_var;
extern "C" Fraction32_t8_127  Fraction32_op_Subtraction_m8_715 (Object_t * __this /* static, unused */, double ___n, Fraction32_t8_127  ___f, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Fraction32_t8_127_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1974);
		s_Il2CppMethodIntialized = true;
	}
	{
		double L_0 = ___n;
		Fraction32_t8_127  L_1 = {0};
		Fraction32__ctor_m8_663(&L_1, L_0, /*hidden argument*/NULL);
		Fraction32_t8_127  L_2 = ___f;
		IL2CPP_RUNTIME_CLASS_INIT(Fraction32_t8_127_il2cpp_TypeInfo_var);
		Fraction32_t8_127  L_3 = Fraction32_op_Subtraction_m8_716(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::op_Subtraction(ExifLibrary.MathEx/Fraction32,ExifLibrary.MathEx/Fraction32)
extern "C" Fraction32_t8_127  Fraction32_op_Subtraction_m8_716 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f1, Fraction32_t8_127  ___f2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		int32_t L_0 = Fraction32_get_Numerator_m8_666((&___f1), /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Fraction32_get_Denominator_m8_668((&___f2), /*hidden argument*/NULL);
		V_1 = L_1;
		int32_t L_2 = Fraction32_get_Numerator_m8_666((&___f2), /*hidden argument*/NULL);
		V_2 = L_2;
		int32_t L_3 = Fraction32_get_Denominator_m8_668((&___f2), /*hidden argument*/NULL);
		V_3 = L_3;
		int32_t L_4 = V_0;
		int32_t L_5 = V_3;
		int32_t L_6 = V_2;
		int32_t L_7 = V_1;
		int32_t L_8 = V_1;
		int32_t L_9 = V_3;
		Fraction32_t8_127  L_10 = {0};
		Fraction32__ctor_m8_659(&L_10, ((int32_t)((int32_t)((int32_t)((int32_t)L_4*(int32_t)L_5))-(int32_t)((int32_t)((int32_t)L_6*(int32_t)L_7)))), ((int32_t)((int32_t)L_8*(int32_t)L_9)), /*hidden argument*/NULL);
		return L_10;
	}
}
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::op_Increment(ExifLibrary.MathEx/Fraction32)
extern TypeInfo* Fraction32_t8_127_il2cpp_TypeInfo_var;
extern "C" Fraction32_t8_127  Fraction32_op_Increment_m8_717 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Fraction32_t8_127_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1974);
		s_Il2CppMethodIntialized = true;
	}
	{
		Fraction32_t8_127  L_0 = ___f;
		Fraction32_t8_127  L_1 = {0};
		Fraction32__ctor_m8_659(&L_1, 1, 1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Fraction32_t8_127_il2cpp_TypeInfo_var);
		Fraction32_t8_127  L_2 = Fraction32_op_Addition_m8_709(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::op_Decrement(ExifLibrary.MathEx/Fraction32)
extern TypeInfo* Fraction32_t8_127_il2cpp_TypeInfo_var;
extern "C" Fraction32_t8_127  Fraction32_op_Decrement_m8_718 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Fraction32_t8_127_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1974);
		s_Il2CppMethodIntialized = true;
	}
	{
		Fraction32_t8_127  L_0 = ___f;
		Fraction32_t8_127  L_1 = {0};
		Fraction32__ctor_m8_659(&L_1, 1, 1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Fraction32_t8_127_il2cpp_TypeInfo_var);
		Fraction32_t8_127  L_2 = Fraction32_op_Subtraction_m8_716(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 ExifLibrary.MathEx/Fraction32::op_Explicit(ExifLibrary.MathEx/Fraction32)
extern "C" int32_t Fraction32_op_Explicit_m8_719 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f, const MethodInfo* method)
{
	{
		int32_t L_0 = Fraction32_get_Numerator_m8_666((&___f), /*hidden argument*/NULL);
		int32_t L_1 = Fraction32_get_Denominator_m8_668((&___f), /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_0/(int32_t)L_1));
	}
}
// System.Single ExifLibrary.MathEx/Fraction32::op_Explicit(ExifLibrary.MathEx/Fraction32)
extern "C" float Fraction32_op_Explicit_m8_720 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f, const MethodInfo* method)
{
	{
		int32_t L_0 = Fraction32_get_Numerator_m8_666((&___f), /*hidden argument*/NULL);
		int32_t L_1 = Fraction32_get_Denominator_m8_668((&___f), /*hidden argument*/NULL);
		return ((float)((float)(((float)((float)L_0)))/(float)(((float)((float)L_1)))));
	}
}
// System.Double ExifLibrary.MathEx/Fraction32::op_Explicit(ExifLibrary.MathEx/Fraction32)
extern "C" double Fraction32_op_Explicit_m8_721 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f, const MethodInfo* method)
{
	{
		int32_t L_0 = Fraction32_get_Numerator_m8_666((&___f), /*hidden argument*/NULL);
		int32_t L_1 = Fraction32_get_Denominator_m8_668((&___f), /*hidden argument*/NULL);
		return ((double)((double)(((double)((double)L_0)))/(double)(((double)((double)L_1)))));
	}
}
// System.Boolean ExifLibrary.MathEx/Fraction32::op_Equality(ExifLibrary.MathEx/Fraction32,ExifLibrary.MathEx/Fraction32)
extern "C" bool Fraction32_op_Equality_m8_722 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f1, Fraction32_t8_127  ___f2, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = Fraction32_get_Numerator_m8_666((&___f1), /*hidden argument*/NULL);
		int32_t L_1 = Fraction32_get_Numerator_m8_666((&___f2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0025;
		}
	}
	{
		int32_t L_2 = Fraction32_get_Denominator_m8_668((&___f1), /*hidden argument*/NULL);
		int32_t L_3 = Fraction32_get_Denominator_m8_668((&___f2), /*hidden argument*/NULL);
		G_B3_0 = ((((int32_t)L_2) == ((int32_t)L_3))? 1 : 0);
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		return G_B3_0;
	}
}
// System.Boolean ExifLibrary.MathEx/Fraction32::op_Inequality(ExifLibrary.MathEx/Fraction32,ExifLibrary.MathEx/Fraction32)
extern "C" bool Fraction32_op_Inequality_m8_723 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f1, Fraction32_t8_127  ___f2, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = Fraction32_get_Numerator_m8_666((&___f1), /*hidden argument*/NULL);
		int32_t L_1 = Fraction32_get_Numerator_m8_666((&___f2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_2 = Fraction32_get_Denominator_m8_668((&___f1), /*hidden argument*/NULL);
		int32_t L_3 = Fraction32_get_Denominator_m8_668((&___f2), /*hidden argument*/NULL);
		G_B3_0 = ((((int32_t)((((int32_t)L_2) == ((int32_t)L_3))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0029;
	}

IL_0028:
	{
		G_B3_0 = 1;
	}

IL_0029:
	{
		return G_B3_0;
	}
}
// System.Boolean ExifLibrary.MathEx/Fraction32::op_LessThan(ExifLibrary.MathEx/Fraction32,ExifLibrary.MathEx/Fraction32)
extern "C" bool Fraction32_op_LessThan_m8_724 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f1, Fraction32_t8_127  ___f2, const MethodInfo* method)
{
	{
		int32_t L_0 = Fraction32_get_Numerator_m8_666((&___f1), /*hidden argument*/NULL);
		int32_t L_1 = Fraction32_get_Denominator_m8_668((&___f2), /*hidden argument*/NULL);
		int32_t L_2 = Fraction32_get_Numerator_m8_666((&___f2), /*hidden argument*/NULL);
		int32_t L_3 = Fraction32_get_Denominator_m8_668((&___f1), /*hidden argument*/NULL);
		return ((((int32_t)((int32_t)((int32_t)L_0*(int32_t)L_1))) < ((int32_t)((int32_t)((int32_t)L_2*(int32_t)L_3))))? 1 : 0);
	}
}
// System.Boolean ExifLibrary.MathEx/Fraction32::op_GreaterThan(ExifLibrary.MathEx/Fraction32,ExifLibrary.MathEx/Fraction32)
extern "C" bool Fraction32_op_GreaterThan_m8_725 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f1, Fraction32_t8_127  ___f2, const MethodInfo* method)
{
	{
		int32_t L_0 = Fraction32_get_Numerator_m8_666((&___f1), /*hidden argument*/NULL);
		int32_t L_1 = Fraction32_get_Denominator_m8_668((&___f2), /*hidden argument*/NULL);
		int32_t L_2 = Fraction32_get_Numerator_m8_666((&___f2), /*hidden argument*/NULL);
		int32_t L_3 = Fraction32_get_Denominator_m8_668((&___f1), /*hidden argument*/NULL);
		return ((((int32_t)((int32_t)((int32_t)L_0*(int32_t)L_1))) > ((int32_t)((int32_t)((int32_t)L_2*(int32_t)L_3))))? 1 : 0);
	}
}
// Conversion methods for marshalling of: ExifLibrary.MathEx/Fraction32
extern "C" void Fraction32_t8_127_marshal(const Fraction32_t8_127& unmarshaled, Fraction32_t8_127_marshaled& marshaled)
{
	marshaled.___mIsNegative_1 = unmarshaled.___mIsNegative_1;
	marshaled.___mNumerator_2 = unmarshaled.___mNumerator_2;
	marshaled.___mDenominator_3 = unmarshaled.___mDenominator_3;
}
extern "C" void Fraction32_t8_127_marshal_back(const Fraction32_t8_127_marshaled& marshaled, Fraction32_t8_127& unmarshaled)
{
	unmarshaled.___mIsNegative_1 = marshaled.___mIsNegative_1;
	unmarshaled.___mNumerator_2 = marshaled.___mNumerator_2;
	unmarshaled.___mDenominator_3 = marshaled.___mDenominator_3;
}
// Conversion method for clean up from marshalling of: ExifLibrary.MathEx/Fraction32
extern "C" void Fraction32_t8_127_marshal_cleanup(Fraction32_t8_127_marshaled& marshaled)
{
}
// System.Void ExifLibrary.MathEx/UFraction32::.ctor(System.UInt32,System.UInt32)
extern TypeInfo* UFraction32_t8_121_il2cpp_TypeInfo_var;
extern "C" void UFraction32__ctor_m8_726 (UFraction32_t8_121 * __this, uint32_t ___numerator, uint32_t ___denominator, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UFraction32_t8_121_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1972);
		s_Il2CppMethodIntialized = true;
	}
	{
		uint32_t L_0 = ___numerator;
		__this->___mNumerator_1 = L_0;
		uint32_t L_1 = ___denominator;
		__this->___mDenominator_2 = L_1;
		uint32_t L_2 = (__this->___mDenominator_2);
		if (!L_2)
		{
			goto IL_002a;
		}
	}
	{
		uint32_t* L_3 = &(__this->___mNumerator_1);
		uint32_t* L_4 = &(__this->___mDenominator_2);
		IL2CPP_RUNTIME_CLASS_INIT(UFraction32_t8_121_il2cpp_TypeInfo_var);
		UFraction32_Reduce_m8_754(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
	}

IL_002a:
	{
		return;
	}
}
// System.Void ExifLibrary.MathEx/UFraction32::.ctor(System.UInt32)
extern "C" void UFraction32__ctor_m8_727 (UFraction32_t8_121 * __this, uint32_t ___numerator, const MethodInfo* method)
{
	{
		uint32_t L_0 = ___numerator;
		UFraction32__ctor_m8_726(__this, L_0, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ExifLibrary.MathEx/UFraction32::.ctor(ExifLibrary.MathEx/UFraction32)
extern "C" void UFraction32__ctor_m8_728 (UFraction32_t8_121 * __this, UFraction32_t8_121  ___f, const MethodInfo* method)
{
	{
		uint32_t L_0 = UFraction32_get_Numerator_m8_733((&___f), /*hidden argument*/NULL);
		uint32_t L_1 = UFraction32_get_Denominator_m8_735((&___f), /*hidden argument*/NULL);
		UFraction32__ctor_m8_726(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ExifLibrary.MathEx/UFraction32::.ctor(System.Single)
extern "C" void UFraction32__ctor_m8_729 (UFraction32_t8_121 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		UFraction32__ctor_m8_730(__this, (((double)((double)L_0))), /*hidden argument*/NULL);
		return;
	}
}
// System.Void ExifLibrary.MathEx/UFraction32::.ctor(System.Double)
extern TypeInfo* UFraction32_t8_121_il2cpp_TypeInfo_var;
extern "C" void UFraction32__ctor_m8_730 (UFraction32_t8_121 * __this, double ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UFraction32_t8_121_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1972);
		s_Il2CppMethodIntialized = true;
	}
	{
		double L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(UFraction32_t8_121_il2cpp_TypeInfo_var);
		UFraction32_t8_121  L_1 = UFraction32_FromDouble_m8_752(NULL /*static, unused*/, L_0, 8, /*hidden argument*/NULL);
		UFraction32__ctor_m8_728(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ExifLibrary.MathEx/UFraction32::.ctor(System.String)
extern TypeInfo* UFraction32_t8_121_il2cpp_TypeInfo_var;
extern "C" void UFraction32__ctor_m8_731 (UFraction32_t8_121 * __this, String_t* ___s, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UFraction32_t8_121_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1972);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___s;
		IL2CPP_RUNTIME_CLASS_INIT(UFraction32_t8_121_il2cpp_TypeInfo_var);
		UFraction32_t8_121  L_1 = UFraction32_FromString_m8_753(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		UFraction32__ctor_m8_728(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ExifLibrary.MathEx/UFraction32::.cctor()
extern TypeInfo* UFraction32_t8_121_il2cpp_TypeInfo_var;
extern "C" void UFraction32__cctor_m8_732 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UFraction32_t8_121_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1972);
		s_Il2CppMethodIntialized = true;
	}
	{
		UFraction32_t8_121  L_0 = {0};
		UFraction32__ctor_m8_726(&L_0, 0, 0, /*hidden argument*/NULL);
		((UFraction32_t8_121_StaticFields*)UFraction32_t8_121_il2cpp_TypeInfo_var->static_fields)->___NaN_3 = L_0;
		UFraction32_t8_121  L_1 = {0};
		UFraction32__ctor_m8_726(&L_1, 1, 0, /*hidden argument*/NULL);
		((UFraction32_t8_121_StaticFields*)UFraction32_t8_121_il2cpp_TypeInfo_var->static_fields)->___Infinity_4 = L_1;
		return;
	}
}
// System.UInt32 ExifLibrary.MathEx/UFraction32::get_Numerator()
extern "C" uint32_t UFraction32_get_Numerator_m8_733 (UFraction32_t8_121 * __this, const MethodInfo* method)
{
	{
		uint32_t L_0 = (__this->___mNumerator_1);
		return L_0;
	}
}
// System.Void ExifLibrary.MathEx/UFraction32::set_Numerator(System.UInt32)
extern TypeInfo* UFraction32_t8_121_il2cpp_TypeInfo_var;
extern "C" void UFraction32_set_Numerator_m8_734 (UFraction32_t8_121 * __this, uint32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UFraction32_t8_121_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1972);
		s_Il2CppMethodIntialized = true;
	}
	{
		uint32_t L_0 = ___value;
		__this->___mNumerator_1 = L_0;
		uint32_t* L_1 = &(__this->___mNumerator_1);
		uint32_t* L_2 = &(__this->___mDenominator_2);
		IL2CPP_RUNTIME_CLASS_INIT(UFraction32_t8_121_il2cpp_TypeInfo_var);
		UFraction32_Reduce_m8_754(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.UInt32 ExifLibrary.MathEx/UFraction32::get_Denominator()
extern "C" uint32_t UFraction32_get_Denominator_m8_735 (UFraction32_t8_121 * __this, const MethodInfo* method)
{
	{
		uint32_t L_0 = (__this->___mDenominator_2);
		return L_0;
	}
}
// System.Void ExifLibrary.MathEx/UFraction32::set_Denominator(System.UInt32)
extern TypeInfo* UFraction32_t8_121_il2cpp_TypeInfo_var;
extern "C" void UFraction32_set_Denominator_m8_736 (UFraction32_t8_121 * __this, uint32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UFraction32_t8_121_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1972);
		s_Il2CppMethodIntialized = true;
	}
	{
		uint32_t L_0 = ___value;
		__this->___mDenominator_2 = L_0;
		uint32_t* L_1 = &(__this->___mNumerator_1);
		uint32_t* L_2 = &(__this->___mDenominator_2);
		IL2CPP_RUNTIME_CLASS_INIT(UFraction32_t8_121_il2cpp_TypeInfo_var);
		UFraction32_Reduce_m8_754(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean ExifLibrary.MathEx/UFraction32::IsNan(ExifLibrary.MathEx/UFraction32)
extern "C" bool UFraction32_IsNan_m8_737 (Object_t * __this /* static, unused */, UFraction32_t8_121  ___f, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		uint32_t L_0 = UFraction32_get_Numerator_m8_733((&___f), /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		uint32_t L_1 = UFraction32_get_Denominator_m8_735((&___f), /*hidden argument*/NULL);
		G_B3_0 = ((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
		goto IL_0019;
	}

IL_0018:
	{
		G_B3_0 = 0;
	}

IL_0019:
	{
		return G_B3_0;
	}
}
// System.Boolean ExifLibrary.MathEx/UFraction32::IsInfinity(ExifLibrary.MathEx/UFraction32)
extern "C" bool UFraction32_IsInfinity_m8_738 (Object_t * __this /* static, unused */, UFraction32_t8_121  ___f, const MethodInfo* method)
{
	{
		uint32_t L_0 = UFraction32_get_Denominator_m8_735((&___f), /*hidden argument*/NULL);
		return ((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
	}
}
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::Parse(System.String)
extern TypeInfo* UFraction32_t8_121_il2cpp_TypeInfo_var;
extern "C" UFraction32_t8_121  UFraction32_Parse_m8_739 (Object_t * __this /* static, unused */, String_t* ___s, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UFraction32_t8_121_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1972);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___s;
		IL2CPP_RUNTIME_CLASS_INIT(UFraction32_t8_121_il2cpp_TypeInfo_var);
		UFraction32_t8_121  L_1 = UFraction32_FromString_m8_753(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean ExifLibrary.MathEx/UFraction32::TryParse(System.String,ExifLibrary.MathEx/UFraction32&)
extern TypeInfo* UFraction32_t8_121_il2cpp_TypeInfo_var;
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern "C" bool UFraction32_TryParse_m8_740 (Object_t * __this /* static, unused */, String_t* ___s, UFraction32_t8_121 * ___f, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UFraction32_t8_121_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1972);
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			UFraction32_t8_121 * L_0 = ___f;
			String_t* L_1 = ___s;
			IL2CPP_RUNTIME_CLASS_INIT(UFraction32_t8_121_il2cpp_TypeInfo_var);
			UFraction32_t8_121  L_2 = UFraction32_Parse_m8_739(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
			(*(UFraction32_t8_121 *)L_0) = L_2;
			V_0 = 1;
			goto IL_002c;
		}

IL_0013:
		{
			; // IL_0013: leave IL_002c
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1_33 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Object_t_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0018;
		throw e;
	}

CATCH_0018:
	{ // begin catch(System.Object)
		{
			UFraction32_t8_121 * L_3 = ___f;
			Initobj (UFraction32_t8_121_il2cpp_TypeInfo_var, L_3);
			V_0 = 0;
			goto IL_002c;
		}

IL_0027:
		{
			; // IL_0027: leave IL_002c
		}
	} // end catch (depth: 1)

IL_002c:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
// System.Void ExifLibrary.MathEx/UFraction32::Set(System.UInt32,System.UInt32)
extern TypeInfo* UFraction32_t8_121_il2cpp_TypeInfo_var;
extern "C" void UFraction32_Set_m8_741 (UFraction32_t8_121 * __this, uint32_t ___numerator, uint32_t ___denominator, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UFraction32_t8_121_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1972);
		s_Il2CppMethodIntialized = true;
	}
	{
		uint32_t L_0 = ___numerator;
		__this->___mNumerator_1 = L_0;
		uint32_t L_1 = ___denominator;
		__this->___mDenominator_2 = L_1;
		uint32_t* L_2 = &(__this->___mNumerator_1);
		uint32_t* L_3 = &(__this->___mDenominator_2);
		IL2CPP_RUNTIME_CLASS_INIT(UFraction32_t8_121_il2cpp_TypeInfo_var);
		UFraction32_Reduce_m8_754(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::Inverse(ExifLibrary.MathEx/UFraction32)
extern "C" UFraction32_t8_121  UFraction32_Inverse_m8_742 (Object_t * __this /* static, unused */, UFraction32_t8_121  ___f, const MethodInfo* method)
{
	{
		uint32_t L_0 = UFraction32_get_Denominator_m8_735((&___f), /*hidden argument*/NULL);
		uint32_t L_1 = UFraction32_get_Numerator_m8_733((&___f), /*hidden argument*/NULL);
		UFraction32_t8_121  L_2 = {0};
		UFraction32__ctor_m8_726(&L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean ExifLibrary.MathEx/UFraction32::Equals(System.Object)
extern TypeInfo* UFraction32_t8_121_il2cpp_TypeInfo_var;
extern "C" bool UFraction32_Equals_m8_743 (UFraction32_t8_121 * __this, Object_t * ___obj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UFraction32_t8_121_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1972);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___obj;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return 0;
	}

IL_0008:
	{
		Object_t * L_1 = ___obj;
		if (!((Object_t *)IsInstSealed(L_1, UFraction32_t8_121_il2cpp_TypeInfo_var)))
		{
			goto IL_0020;
		}
	}
	{
		Object_t * L_2 = ___obj;
		bool L_3 = UFraction32_Equals_m8_744(__this, ((*(UFraction32_t8_121 *)((UFraction32_t8_121 *)UnBox (L_2, UFraction32_t8_121_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		return L_3;
	}

IL_0020:
	{
		return 0;
	}
}
// System.Boolean ExifLibrary.MathEx/UFraction32::Equals(ExifLibrary.MathEx/UFraction32)
extern "C" bool UFraction32_Equals_m8_744 (UFraction32_t8_121 * __this, UFraction32_t8_121  ___obj, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		uint32_t L_0 = (__this->___mNumerator_1);
		uint32_t L_1 = UFraction32_get_Numerator_m8_733((&___obj), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0023;
		}
	}
	{
		uint32_t L_2 = (__this->___mDenominator_2);
		uint32_t L_3 = UFraction32_get_Denominator_m8_735((&___obj), /*hidden argument*/NULL);
		G_B3_0 = ((((int32_t)L_2) == ((int32_t)L_3))? 1 : 0);
		goto IL_0024;
	}

IL_0023:
	{
		G_B3_0 = 0;
	}

IL_0024:
	{
		return G_B3_0;
	}
}
// System.Int32 ExifLibrary.MathEx/UFraction32::GetHashCode()
extern "C" int32_t UFraction32_GetHashCode_m8_745 (UFraction32_t8_121 * __this, const MethodInfo* method)
{
	{
		uint32_t L_0 = (__this->___mDenominator_2);
		uint32_t L_1 = (__this->___mNumerator_1);
		return ((int32_t)((int32_t)L_0^(int32_t)L_1));
	}
}
// System.String ExifLibrary.MathEx/UFraction32::ToString(System.String,System.IFormatProvider)
extern TypeInfo* StringBuilder_t1_247_il2cpp_TypeInfo_var;
extern "C" String_t* UFraction32_ToString_m8_746 (UFraction32_t8_121 * __this, String_t* ___format, Object_t * ___formatProvider, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringBuilder_t1_247_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t1_247 * V_0 = {0};
	{
		StringBuilder_t1_247 * L_0 = (StringBuilder_t1_247 *)il2cpp_codegen_object_new (StringBuilder_t1_247_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1_12414(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		StringBuilder_t1_247 * L_1 = V_0;
		uint32_t* L_2 = &(__this->___mNumerator_1);
		String_t* L_3 = ___format;
		Object_t * L_4 = ___formatProvider;
		String_t* L_5 = UInt32_ToString_m1_179(L_2, L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_1);
		StringBuilder_Append_m1_12438(L_1, L_5, /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_6 = V_0;
		NullCheck(L_6);
		StringBuilder_Append_m1_12452(L_6, ((int32_t)47), /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_7 = V_0;
		uint32_t* L_8 = &(__this->___mDenominator_2);
		String_t* L_9 = ___format;
		Object_t * L_10 = ___formatProvider;
		String_t* L_11 = UInt32_ToString_m1_179(L_8, L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_7);
		StringBuilder_Append_m1_12438(L_7, L_11, /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_12 = V_0;
		NullCheck(L_12);
		String_t* L_13 = StringBuilder_ToString_m1_12428(L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
// System.String ExifLibrary.MathEx/UFraction32::ToString(System.String)
extern TypeInfo* StringBuilder_t1_247_il2cpp_TypeInfo_var;
extern "C" String_t* UFraction32_ToString_m8_747 (UFraction32_t8_121 * __this, String_t* ___format, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringBuilder_t1_247_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t1_247 * V_0 = {0};
	{
		StringBuilder_t1_247 * L_0 = (StringBuilder_t1_247 *)il2cpp_codegen_object_new (StringBuilder_t1_247_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1_12414(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		StringBuilder_t1_247 * L_1 = V_0;
		uint32_t* L_2 = &(__this->___mNumerator_1);
		String_t* L_3 = ___format;
		String_t* L_4 = UInt32_ToString_m1_178(L_2, L_3, /*hidden argument*/NULL);
		NullCheck(L_1);
		StringBuilder_Append_m1_12438(L_1, L_4, /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_5 = V_0;
		NullCheck(L_5);
		StringBuilder_Append_m1_12452(L_5, ((int32_t)47), /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_6 = V_0;
		uint32_t* L_7 = &(__this->___mDenominator_2);
		String_t* L_8 = ___format;
		String_t* L_9 = UInt32_ToString_m1_178(L_7, L_8, /*hidden argument*/NULL);
		NullCheck(L_6);
		StringBuilder_Append_m1_12438(L_6, L_9, /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_10 = V_0;
		NullCheck(L_10);
		String_t* L_11 = StringBuilder_ToString_m1_12428(L_10, /*hidden argument*/NULL);
		return L_11;
	}
}
// System.String ExifLibrary.MathEx/UFraction32::ToString(System.IFormatProvider)
extern TypeInfo* StringBuilder_t1_247_il2cpp_TypeInfo_var;
extern "C" String_t* UFraction32_ToString_m8_748 (UFraction32_t8_121 * __this, Object_t * ___formatProvider, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringBuilder_t1_247_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t1_247 * V_0 = {0};
	{
		StringBuilder_t1_247 * L_0 = (StringBuilder_t1_247 *)il2cpp_codegen_object_new (StringBuilder_t1_247_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1_12414(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		StringBuilder_t1_247 * L_1 = V_0;
		uint32_t* L_2 = &(__this->___mNumerator_1);
		Object_t * L_3 = ___formatProvider;
		String_t* L_4 = UInt32_ToString_m1_177(L_2, L_3, /*hidden argument*/NULL);
		NullCheck(L_1);
		StringBuilder_Append_m1_12438(L_1, L_4, /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_5 = V_0;
		NullCheck(L_5);
		StringBuilder_Append_m1_12452(L_5, ((int32_t)47), /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_6 = V_0;
		uint32_t* L_7 = &(__this->___mDenominator_2);
		Object_t * L_8 = ___formatProvider;
		String_t* L_9 = UInt32_ToString_m1_177(L_7, L_8, /*hidden argument*/NULL);
		NullCheck(L_6);
		StringBuilder_Append_m1_12438(L_6, L_9, /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_10 = V_0;
		NullCheck(L_10);
		String_t* L_11 = StringBuilder_ToString_m1_12428(L_10, /*hidden argument*/NULL);
		return L_11;
	}
}
// System.String ExifLibrary.MathEx/UFraction32::ToString()
extern TypeInfo* StringBuilder_t1_247_il2cpp_TypeInfo_var;
extern "C" String_t* UFraction32_ToString_m8_749 (UFraction32_t8_121 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringBuilder_t1_247_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t1_247 * V_0 = {0};
	{
		StringBuilder_t1_247 * L_0 = (StringBuilder_t1_247 *)il2cpp_codegen_object_new (StringBuilder_t1_247_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1_12414(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		StringBuilder_t1_247 * L_1 = V_0;
		uint32_t* L_2 = &(__this->___mNumerator_1);
		String_t* L_3 = UInt32_ToString_m1_176(L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		StringBuilder_Append_m1_12438(L_1, L_3, /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_4 = V_0;
		NullCheck(L_4);
		StringBuilder_Append_m1_12452(L_4, ((int32_t)47), /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_5 = V_0;
		uint32_t* L_6 = &(__this->___mDenominator_2);
		String_t* L_7 = UInt32_ToString_m1_176(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		StringBuilder_Append_m1_12438(L_5, L_7, /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_8 = V_0;
		NullCheck(L_8);
		String_t* L_9 = StringBuilder_ToString_m1_12428(L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Int32 ExifLibrary.MathEx/UFraction32::CompareTo(System.Object)
extern TypeInfo* UFraction32_t8_121_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5231;
extern Il2CppCodeGenString* _stringLiteral854;
extern "C" int32_t UFraction32_CompareTo_m8_750 (UFraction32_t8_121 * __this, Object_t * ___obj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UFraction32_t8_121_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1972);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		_stringLiteral5231 = il2cpp_codegen_string_literal_from_index(5231);
		_stringLiteral854 = il2cpp_codegen_string_literal_from_index(854);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___obj;
		if (((Object_t *)IsInstSealed(L_0, UFraction32_t8_121_il2cpp_TypeInfo_var)))
		{
			goto IL_001b;
		}
	}
	{
		ArgumentException_t1_1425 * L_1 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13262(L_1, _stringLiteral5231, _stringLiteral854, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_001b:
	{
		Object_t * L_2 = ___obj;
		int32_t L_3 = UFraction32_CompareTo_m8_751(__this, ((*(UFraction32_t8_121 *)((UFraction32_t8_121 *)UnBox (L_2, UFraction32_t8_121_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Int32 ExifLibrary.MathEx/UFraction32::CompareTo(ExifLibrary.MathEx/UFraction32)
extern TypeInfo* UFraction32_t8_121_il2cpp_TypeInfo_var;
extern "C" int32_t UFraction32_CompareTo_m8_751 (UFraction32_t8_121 * __this, UFraction32_t8_121  ___obj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UFraction32_t8_121_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1972);
		s_Il2CppMethodIntialized = true;
	}
	{
		UFraction32_t8_121  L_0 = ___obj;
		IL2CPP_RUNTIME_CLASS_INIT(UFraction32_t8_121_il2cpp_TypeInfo_var);
		bool L_1 = UFraction32_op_LessThan_m8_787(NULL /*static, unused*/, (*(UFraction32_t8_121 *)__this), L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		return (-1);
	}

IL_0013:
	{
		UFraction32_t8_121  L_2 = ___obj;
		IL2CPP_RUNTIME_CLASS_INIT(UFraction32_t8_121_il2cpp_TypeInfo_var);
		bool L_3 = UFraction32_op_GreaterThan_m8_788(NULL /*static, unused*/, (*(UFraction32_t8_121 *)__this), L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0026;
		}
	}
	{
		return 1;
	}

IL_0026:
	{
		return 0;
	}
}
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::FromDouble(System.Double,System.Int32)
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern TypeInfo* UFraction32_t8_121_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5232;
extern Il2CppCodeGenString* _stringLiteral61;
extern "C" UFraction32_t8_121  UFraction32_FromDouble_m8_752 (Object_t * __this /* static, unused */, double ___value, int32_t ___precision, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		UFraction32_t8_121_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1972);
		_stringLiteral5232 = il2cpp_codegen_string_literal_from_index(5232);
		_stringLiteral61 = il2cpp_codegen_string_literal_from_index(61);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	uint32_t V_1 = 0;
	uint32_t V_2 = 0;
	uint32_t V_3 = 0;
	{
		double L_0 = ___value;
		if ((!(((double)L_0) < ((double)(0.0)))))
		{
			goto IL_001f;
		}
	}
	{
		ArgumentException_t1_1425 * L_1 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13262(L_1, _stringLiteral5232, _stringLiteral61, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_001f:
	{
		double L_2 = ___value;
		bool L_3 = Double_IsNaN_m1_659(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0030;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UFraction32_t8_121_il2cpp_TypeInfo_var);
		UFraction32_t8_121  L_4 = ((UFraction32_t8_121_StaticFields*)UFraction32_t8_121_il2cpp_TypeInfo_var->static_fields)->___NaN_3;
		return L_4;
	}

IL_0030:
	{
		double L_5 = ___value;
		bool L_6 = Double_IsInfinity_m1_658(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0041;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UFraction32_t8_121_il2cpp_TypeInfo_var);
		UFraction32_t8_121  L_7 = ((UFraction32_t8_121_StaticFields*)UFraction32_t8_121_il2cpp_TypeInfo_var->static_fields)->___Infinity_4;
		return L_7;
	}

IL_0041:
	{
		int32_t L_8 = ___precision;
		double L_9 = pow((10.0), (((double)((double)L_8))));
		V_0 = (((int32_t)((uint32_t)L_9)));
		double L_10 = ___value;
		uint32_t L_11 = V_0;
		V_1 = (((int32_t)((uint32_t)((double)((double)L_10*(double)(((double)((double)(((double)((double)L_11)))))))))));
		uint32_t L_12 = V_0;
		V_2 = L_12;
		uint32_t L_13 = V_1;
		uint32_t L_14 = V_2;
		uint32_t L_15 = MathEx_GCD_m8_789(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		V_3 = L_15;
		uint32_t L_16 = V_1;
		uint32_t L_17 = V_3;
		V_1 = ((int32_t)((uint32_t)(int32_t)L_16/(uint32_t)(int32_t)L_17));
		uint32_t L_18 = V_2;
		uint32_t L_19 = V_3;
		V_2 = ((int32_t)((uint32_t)(int32_t)L_18/(uint32_t)(int32_t)L_19));
		uint32_t L_20 = V_1;
		uint32_t L_21 = V_2;
		UFraction32_t8_121  L_22 = {0};
		UFraction32__ctor_m8_726(&L_22, L_20, L_21, /*hidden argument*/NULL);
		return L_22;
	}
}
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::FromString(System.String)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t1_16_il2cpp_TypeInfo_var;
extern TypeInfo* UFraction32_t8_121_il2cpp_TypeInfo_var;
extern TypeInfo* FormatException_t1_592_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral7;
extern Il2CppCodeGenString* _stringLiteral5230;
extern "C" UFraction32_t8_121  UFraction32_FromString_m8_753 (Object_t * __this /* static, unused */, String_t* ___s, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		CharU5BU5D_t1_16_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(15);
		UFraction32_t8_121_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1972);
		FormatException_t1_592_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(16);
		_stringLiteral7 = il2cpp_codegen_string_literal_from_index(7);
		_stringLiteral5230 = il2cpp_codegen_string_literal_from_index(5230);
		s_Il2CppMethodIntialized = true;
	}
	StringU5BU5D_t1_238* V_0 = {0};
	uint32_t V_1 = 0;
	uint32_t V_2 = 0;
	double V_3 = 0.0;
	{
		String_t* L_0 = ___s;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, _stringLiteral7, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		String_t* L_2 = ___s;
		CharU5BU5D_t1_16* L_3 = ((CharU5BU5D_t1_16*)SZArrayNew(CharU5BU5D_t1_16_il2cpp_TypeInfo_var, 1));
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_3, 0, sizeof(uint16_t))) = (uint16_t)((int32_t)47);
		NullCheck(L_2);
		StringU5BU5D_t1_238* L_4 = String_Split_m1_448(L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		V_1 = 1;
		V_2 = 1;
		StringU5BU5D_t1_238* L_5 = V_0;
		NullCheck(L_5);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Array_t *)L_5)->max_length))))) == ((uint32_t)1))))
		{
			goto IL_005c;
		}
	}
	{
		StringU5BU5D_t1_238* L_6 = V_0;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 0);
		int32_t L_7 = 0;
		bool L_8 = UInt32_TryParse_m1_174(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_6, L_7, sizeof(String_t*))), (&V_1), /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0046;
		}
	}
	{
		V_2 = 1;
		goto IL_0057;
	}

IL_0046:
	{
		StringU5BU5D_t1_238* L_9 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 0);
		int32_t L_10 = 0;
		double L_11 = Double_Parse_m1_662(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_9, L_10, sizeof(String_t*))), /*hidden argument*/NULL);
		V_3 = L_11;
		double L_12 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(UFraction32_t8_121_il2cpp_TypeInfo_var);
		UFraction32_t8_121  L_13 = UFraction32_FromDouble_m8_752(NULL /*static, unused*/, L_12, 8, /*hidden argument*/NULL);
		return L_13;
	}

IL_0057:
	{
		goto IL_0087;
	}

IL_005c:
	{
		StringU5BU5D_t1_238* L_14 = V_0;
		NullCheck(L_14);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Array_t *)L_14)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_007c;
		}
	}
	{
		StringU5BU5D_t1_238* L_15 = V_0;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 0);
		int32_t L_16 = 0;
		uint32_t L_17 = UInt32_Parse_m1_170(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_15, L_16, sizeof(String_t*))), /*hidden argument*/NULL);
		V_1 = L_17;
		StringU5BU5D_t1_238* L_18 = V_0;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 1);
		int32_t L_19 = 1;
		uint32_t L_20 = UInt32_Parse_m1_170(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_18, L_19, sizeof(String_t*))), /*hidden argument*/NULL);
		V_2 = L_20;
		goto IL_0087;
	}

IL_007c:
	{
		FormatException_t1_592 * L_21 = (FormatException_t1_592 *)il2cpp_codegen_object_new (FormatException_t1_592_il2cpp_TypeInfo_var);
		FormatException__ctor_m1_14100(L_21, _stringLiteral5230, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_21);
	}

IL_0087:
	{
		uint32_t L_22 = V_1;
		uint32_t L_23 = V_2;
		UFraction32_t8_121  L_24 = {0};
		UFraction32__ctor_m8_726(&L_24, L_22, L_23, /*hidden argument*/NULL);
		return L_24;
	}
}
// System.Void ExifLibrary.MathEx/UFraction32::Reduce(System.UInt32&,System.UInt32&)
extern "C" void UFraction32_Reduce_m8_754 (Object_t * __this /* static, unused */, uint32_t* ___numerator, uint32_t* ___denominator, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	{
		uint32_t* L_0 = ___numerator;
		uint32_t* L_1 = ___denominator;
		uint32_t L_2 = MathEx_GCD_m8_789(NULL /*static, unused*/, (*((uint32_t*)L_0)), (*((uint32_t*)L_1)), /*hidden argument*/NULL);
		V_0 = L_2;
		uint32_t* L_3 = ___numerator;
		uint32_t* L_4 = ___numerator;
		uint32_t L_5 = V_0;
		*((int32_t*)(L_3)) = (int32_t)((int32_t)((uint32_t)(int32_t)(*((uint32_t*)L_4))/(uint32_t)(int32_t)L_5));
		uint32_t* L_6 = ___denominator;
		uint32_t* L_7 = ___denominator;
		uint32_t L_8 = V_0;
		*((int32_t*)(L_6)) = (int32_t)((int32_t)((uint32_t)(int32_t)(*((uint32_t*)L_7))/(uint32_t)(int32_t)L_8));
		return;
	}
}
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::op_Multiply(ExifLibrary.MathEx/UFraction32,System.UInt32)
extern "C" UFraction32_t8_121  UFraction32_op_Multiply_m8_755 (Object_t * __this /* static, unused */, UFraction32_t8_121  ___f, uint32_t ___n, const MethodInfo* method)
{
	{
		uint32_t L_0 = UFraction32_get_Numerator_m8_733((&___f), /*hidden argument*/NULL);
		uint32_t L_1 = ___n;
		uint32_t L_2 = UFraction32_get_Denominator_m8_735((&___f), /*hidden argument*/NULL);
		uint32_t L_3 = ___n;
		UFraction32_t8_121  L_4 = {0};
		UFraction32__ctor_m8_726(&L_4, ((int32_t)((int32_t)L_0*(int32_t)L_1)), ((int32_t)((int32_t)L_2*(int32_t)L_3)), /*hidden argument*/NULL);
		return L_4;
	}
}
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::op_Multiply(System.UInt32,ExifLibrary.MathEx/UFraction32)
extern TypeInfo* UFraction32_t8_121_il2cpp_TypeInfo_var;
extern "C" UFraction32_t8_121  UFraction32_op_Multiply_m8_756 (Object_t * __this /* static, unused */, uint32_t ___n, UFraction32_t8_121  ___f, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UFraction32_t8_121_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1972);
		s_Il2CppMethodIntialized = true;
	}
	{
		UFraction32_t8_121  L_0 = ___f;
		uint32_t L_1 = ___n;
		IL2CPP_RUNTIME_CLASS_INIT(UFraction32_t8_121_il2cpp_TypeInfo_var);
		UFraction32_t8_121  L_2 = UFraction32_op_Multiply_m8_755(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::op_Multiply(ExifLibrary.MathEx/UFraction32,System.Single)
extern TypeInfo* UFraction32_t8_121_il2cpp_TypeInfo_var;
extern "C" UFraction32_t8_121  UFraction32_op_Multiply_m8_757 (Object_t * __this /* static, unused */, UFraction32_t8_121  ___f, float ___n, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UFraction32_t8_121_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1972);
		s_Il2CppMethodIntialized = true;
	}
	{
		UFraction32_t8_121  L_0 = ___f;
		IL2CPP_RUNTIME_CLASS_INIT(UFraction32_t8_121_il2cpp_TypeInfo_var);
		float L_1 = UFraction32_op_Explicit_m8_783(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		float L_2 = ___n;
		UFraction32_t8_121  L_3 = {0};
		UFraction32__ctor_m8_729(&L_3, ((float)((float)L_1*(float)L_2)), /*hidden argument*/NULL);
		return L_3;
	}
}
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::op_Multiply(System.Single,ExifLibrary.MathEx/UFraction32)
extern TypeInfo* UFraction32_t8_121_il2cpp_TypeInfo_var;
extern "C" UFraction32_t8_121  UFraction32_op_Multiply_m8_758 (Object_t * __this /* static, unused */, float ___n, UFraction32_t8_121  ___f, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UFraction32_t8_121_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1972);
		s_Il2CppMethodIntialized = true;
	}
	{
		UFraction32_t8_121  L_0 = ___f;
		float L_1 = ___n;
		IL2CPP_RUNTIME_CLASS_INIT(UFraction32_t8_121_il2cpp_TypeInfo_var);
		UFraction32_t8_121  L_2 = UFraction32_op_Multiply_m8_757(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::op_Multiply(ExifLibrary.MathEx/UFraction32,System.Double)
extern TypeInfo* UFraction32_t8_121_il2cpp_TypeInfo_var;
extern "C" UFraction32_t8_121  UFraction32_op_Multiply_m8_759 (Object_t * __this /* static, unused */, UFraction32_t8_121  ___f, double ___n, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UFraction32_t8_121_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1972);
		s_Il2CppMethodIntialized = true;
	}
	{
		UFraction32_t8_121  L_0 = ___f;
		IL2CPP_RUNTIME_CLASS_INIT(UFraction32_t8_121_il2cpp_TypeInfo_var);
		double L_1 = UFraction32_op_Explicit_m8_784(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		double L_2 = ___n;
		UFraction32_t8_121  L_3 = {0};
		UFraction32__ctor_m8_730(&L_3, ((double)((double)L_1*(double)L_2)), /*hidden argument*/NULL);
		return L_3;
	}
}
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::op_Multiply(System.Double,ExifLibrary.MathEx/UFraction32)
extern TypeInfo* UFraction32_t8_121_il2cpp_TypeInfo_var;
extern "C" UFraction32_t8_121  UFraction32_op_Multiply_m8_760 (Object_t * __this /* static, unused */, double ___n, UFraction32_t8_121  ___f, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UFraction32_t8_121_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1972);
		s_Il2CppMethodIntialized = true;
	}
	{
		UFraction32_t8_121  L_0 = ___f;
		double L_1 = ___n;
		IL2CPP_RUNTIME_CLASS_INIT(UFraction32_t8_121_il2cpp_TypeInfo_var);
		UFraction32_t8_121  L_2 = UFraction32_op_Multiply_m8_759(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::op_Multiply(ExifLibrary.MathEx/UFraction32,ExifLibrary.MathEx/UFraction32)
extern "C" UFraction32_t8_121  UFraction32_op_Multiply_m8_761 (Object_t * __this /* static, unused */, UFraction32_t8_121  ___f1, UFraction32_t8_121  ___f2, const MethodInfo* method)
{
	{
		uint32_t L_0 = UFraction32_get_Numerator_m8_733((&___f1), /*hidden argument*/NULL);
		uint32_t L_1 = UFraction32_get_Numerator_m8_733((&___f2), /*hidden argument*/NULL);
		uint32_t L_2 = UFraction32_get_Denominator_m8_735((&___f1), /*hidden argument*/NULL);
		uint32_t L_3 = UFraction32_get_Denominator_m8_735((&___f2), /*hidden argument*/NULL);
		UFraction32_t8_121  L_4 = {0};
		UFraction32__ctor_m8_726(&L_4, ((int32_t)((int32_t)L_0*(int32_t)L_1)), ((int32_t)((int32_t)L_2*(int32_t)L_3)), /*hidden argument*/NULL);
		return L_4;
	}
}
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::op_Division(ExifLibrary.MathEx/UFraction32,System.UInt32)
extern "C" UFraction32_t8_121  UFraction32_op_Division_m8_762 (Object_t * __this /* static, unused */, UFraction32_t8_121  ___f, uint32_t ___n, const MethodInfo* method)
{
	{
		uint32_t L_0 = UFraction32_get_Numerator_m8_733((&___f), /*hidden argument*/NULL);
		uint32_t L_1 = ___n;
		uint32_t L_2 = UFraction32_get_Denominator_m8_735((&___f), /*hidden argument*/NULL);
		uint32_t L_3 = ___n;
		UFraction32_t8_121  L_4 = {0};
		UFraction32__ctor_m8_726(&L_4, ((int32_t)((uint32_t)(int32_t)L_0/(uint32_t)(int32_t)L_1)), ((int32_t)((uint32_t)(int32_t)L_2/(uint32_t)(int32_t)L_3)), /*hidden argument*/NULL);
		return L_4;
	}
}
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::op_Division(ExifLibrary.MathEx/UFraction32,System.Single)
extern TypeInfo* UFraction32_t8_121_il2cpp_TypeInfo_var;
extern "C" UFraction32_t8_121  UFraction32_op_Division_m8_763 (Object_t * __this /* static, unused */, UFraction32_t8_121  ___f, float ___n, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UFraction32_t8_121_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1972);
		s_Il2CppMethodIntialized = true;
	}
	{
		UFraction32_t8_121  L_0 = ___f;
		IL2CPP_RUNTIME_CLASS_INIT(UFraction32_t8_121_il2cpp_TypeInfo_var);
		float L_1 = UFraction32_op_Explicit_m8_783(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		float L_2 = ___n;
		UFraction32_t8_121  L_3 = {0};
		UFraction32__ctor_m8_729(&L_3, ((float)((float)L_1/(float)L_2)), /*hidden argument*/NULL);
		return L_3;
	}
}
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::op_Division(ExifLibrary.MathEx/UFraction32,System.Double)
extern TypeInfo* UFraction32_t8_121_il2cpp_TypeInfo_var;
extern "C" UFraction32_t8_121  UFraction32_op_Division_m8_764 (Object_t * __this /* static, unused */, UFraction32_t8_121  ___f, double ___n, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UFraction32_t8_121_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1972);
		s_Il2CppMethodIntialized = true;
	}
	{
		UFraction32_t8_121  L_0 = ___f;
		IL2CPP_RUNTIME_CLASS_INIT(UFraction32_t8_121_il2cpp_TypeInfo_var);
		double L_1 = UFraction32_op_Explicit_m8_784(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		double L_2 = ___n;
		UFraction32_t8_121  L_3 = {0};
		UFraction32__ctor_m8_730(&L_3, ((double)((double)L_1/(double)L_2)), /*hidden argument*/NULL);
		return L_3;
	}
}
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::op_Division(ExifLibrary.MathEx/UFraction32,ExifLibrary.MathEx/UFraction32)
extern TypeInfo* UFraction32_t8_121_il2cpp_TypeInfo_var;
extern "C" UFraction32_t8_121  UFraction32_op_Division_m8_765 (Object_t * __this /* static, unused */, UFraction32_t8_121  ___f1, UFraction32_t8_121  ___f2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UFraction32_t8_121_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1972);
		s_Il2CppMethodIntialized = true;
	}
	{
		UFraction32_t8_121  L_0 = ___f1;
		UFraction32_t8_121  L_1 = ___f2;
		IL2CPP_RUNTIME_CLASS_INIT(UFraction32_t8_121_il2cpp_TypeInfo_var);
		UFraction32_t8_121  L_2 = UFraction32_Inverse_m8_742(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		UFraction32_t8_121  L_3 = UFraction32_op_Multiply_m8_761(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::op_Addition(ExifLibrary.MathEx/UFraction32,System.UInt32)
extern TypeInfo* UFraction32_t8_121_il2cpp_TypeInfo_var;
extern "C" UFraction32_t8_121  UFraction32_op_Addition_m8_766 (Object_t * __this /* static, unused */, UFraction32_t8_121  ___f, uint32_t ___n, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UFraction32_t8_121_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1972);
		s_Il2CppMethodIntialized = true;
	}
	{
		UFraction32_t8_121  L_0 = ___f;
		uint32_t L_1 = ___n;
		UFraction32_t8_121  L_2 = {0};
		UFraction32__ctor_m8_726(&L_2, L_1, 1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UFraction32_t8_121_il2cpp_TypeInfo_var);
		UFraction32_t8_121  L_3 = UFraction32_op_Addition_m8_772(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::op_Addition(System.UInt32,ExifLibrary.MathEx/UFraction32)
extern TypeInfo* UFraction32_t8_121_il2cpp_TypeInfo_var;
extern "C" UFraction32_t8_121  UFraction32_op_Addition_m8_767 (Object_t * __this /* static, unused */, uint32_t ___n, UFraction32_t8_121  ___f, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UFraction32_t8_121_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1972);
		s_Il2CppMethodIntialized = true;
	}
	{
		UFraction32_t8_121  L_0 = ___f;
		uint32_t L_1 = ___n;
		IL2CPP_RUNTIME_CLASS_INIT(UFraction32_t8_121_il2cpp_TypeInfo_var);
		UFraction32_t8_121  L_2 = UFraction32_op_Addition_m8_766(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::op_Addition(ExifLibrary.MathEx/UFraction32,System.Single)
extern TypeInfo* UFraction32_t8_121_il2cpp_TypeInfo_var;
extern "C" UFraction32_t8_121  UFraction32_op_Addition_m8_768 (Object_t * __this /* static, unused */, UFraction32_t8_121  ___f, float ___n, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UFraction32_t8_121_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1972);
		s_Il2CppMethodIntialized = true;
	}
	{
		UFraction32_t8_121  L_0 = ___f;
		IL2CPP_RUNTIME_CLASS_INIT(UFraction32_t8_121_il2cpp_TypeInfo_var);
		float L_1 = UFraction32_op_Explicit_m8_783(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		float L_2 = ___n;
		UFraction32_t8_121  L_3 = {0};
		UFraction32__ctor_m8_729(&L_3, ((float)((float)L_1+(float)L_2)), /*hidden argument*/NULL);
		return L_3;
	}
}
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::op_Addition(System.Single,ExifLibrary.MathEx/UFraction32)
extern TypeInfo* UFraction32_t8_121_il2cpp_TypeInfo_var;
extern "C" UFraction32_t8_121  UFraction32_op_Addition_m8_769 (Object_t * __this /* static, unused */, float ___n, UFraction32_t8_121  ___f, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UFraction32_t8_121_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1972);
		s_Il2CppMethodIntialized = true;
	}
	{
		UFraction32_t8_121  L_0 = ___f;
		float L_1 = ___n;
		IL2CPP_RUNTIME_CLASS_INIT(UFraction32_t8_121_il2cpp_TypeInfo_var);
		UFraction32_t8_121  L_2 = UFraction32_op_Addition_m8_768(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::op_Addition(ExifLibrary.MathEx/UFraction32,System.Double)
extern TypeInfo* UFraction32_t8_121_il2cpp_TypeInfo_var;
extern "C" UFraction32_t8_121  UFraction32_op_Addition_m8_770 (Object_t * __this /* static, unused */, UFraction32_t8_121  ___f, double ___n, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UFraction32_t8_121_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1972);
		s_Il2CppMethodIntialized = true;
	}
	{
		UFraction32_t8_121  L_0 = ___f;
		IL2CPP_RUNTIME_CLASS_INIT(UFraction32_t8_121_il2cpp_TypeInfo_var);
		double L_1 = UFraction32_op_Explicit_m8_784(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		double L_2 = ___n;
		UFraction32_t8_121  L_3 = {0};
		UFraction32__ctor_m8_730(&L_3, ((double)((double)L_1+(double)L_2)), /*hidden argument*/NULL);
		return L_3;
	}
}
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::op_Addition(System.Double,ExifLibrary.MathEx/UFraction32)
extern TypeInfo* UFraction32_t8_121_il2cpp_TypeInfo_var;
extern "C" UFraction32_t8_121  UFraction32_op_Addition_m8_771 (Object_t * __this /* static, unused */, double ___n, UFraction32_t8_121  ___f, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UFraction32_t8_121_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1972);
		s_Il2CppMethodIntialized = true;
	}
	{
		UFraction32_t8_121  L_0 = ___f;
		double L_1 = ___n;
		IL2CPP_RUNTIME_CLASS_INIT(UFraction32_t8_121_il2cpp_TypeInfo_var);
		UFraction32_t8_121  L_2 = UFraction32_op_Addition_m8_770(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::op_Addition(ExifLibrary.MathEx/UFraction32,ExifLibrary.MathEx/UFraction32)
extern "C" UFraction32_t8_121  UFraction32_op_Addition_m8_772 (Object_t * __this /* static, unused */, UFraction32_t8_121  ___f1, UFraction32_t8_121  ___f2, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	uint32_t V_1 = 0;
	uint32_t V_2 = 0;
	uint32_t V_3 = 0;
	{
		uint32_t L_0 = UFraction32_get_Numerator_m8_733((&___f1), /*hidden argument*/NULL);
		V_0 = L_0;
		uint32_t L_1 = UFraction32_get_Denominator_m8_735((&___f2), /*hidden argument*/NULL);
		V_1 = L_1;
		uint32_t L_2 = UFraction32_get_Numerator_m8_733((&___f2), /*hidden argument*/NULL);
		V_2 = L_2;
		uint32_t L_3 = UFraction32_get_Denominator_m8_735((&___f2), /*hidden argument*/NULL);
		V_3 = L_3;
		uint32_t L_4 = V_0;
		uint32_t L_5 = V_3;
		uint32_t L_6 = V_2;
		uint32_t L_7 = V_1;
		uint32_t L_8 = V_1;
		uint32_t L_9 = V_3;
		UFraction32_t8_121  L_10 = {0};
		UFraction32__ctor_m8_726(&L_10, ((int32_t)((int32_t)((int32_t)((int32_t)L_4*(int32_t)L_5))+(int32_t)((int32_t)((int32_t)L_6*(int32_t)L_7)))), ((int32_t)((int32_t)L_8*(int32_t)L_9)), /*hidden argument*/NULL);
		return L_10;
	}
}
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::op_Subtraction(ExifLibrary.MathEx/UFraction32,System.UInt32)
extern TypeInfo* UFraction32_t8_121_il2cpp_TypeInfo_var;
extern "C" UFraction32_t8_121  UFraction32_op_Subtraction_m8_773 (Object_t * __this /* static, unused */, UFraction32_t8_121  ___f, uint32_t ___n, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UFraction32_t8_121_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1972);
		s_Il2CppMethodIntialized = true;
	}
	{
		UFraction32_t8_121  L_0 = ___f;
		uint32_t L_1 = ___n;
		UFraction32_t8_121  L_2 = {0};
		UFraction32__ctor_m8_726(&L_2, L_1, 1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UFraction32_t8_121_il2cpp_TypeInfo_var);
		UFraction32_t8_121  L_3 = UFraction32_op_Subtraction_m8_779(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::op_Subtraction(System.UInt32,ExifLibrary.MathEx/UFraction32)
extern TypeInfo* UFraction32_t8_121_il2cpp_TypeInfo_var;
extern "C" UFraction32_t8_121  UFraction32_op_Subtraction_m8_774 (Object_t * __this /* static, unused */, uint32_t ___n, UFraction32_t8_121  ___f, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UFraction32_t8_121_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1972);
		s_Il2CppMethodIntialized = true;
	}
	{
		uint32_t L_0 = ___n;
		UFraction32_t8_121  L_1 = {0};
		UFraction32__ctor_m8_726(&L_1, L_0, 1, /*hidden argument*/NULL);
		UFraction32_t8_121  L_2 = ___f;
		IL2CPP_RUNTIME_CLASS_INIT(UFraction32_t8_121_il2cpp_TypeInfo_var);
		UFraction32_t8_121  L_3 = UFraction32_op_Subtraction_m8_779(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::op_Subtraction(ExifLibrary.MathEx/UFraction32,System.Single)
extern TypeInfo* UFraction32_t8_121_il2cpp_TypeInfo_var;
extern "C" UFraction32_t8_121  UFraction32_op_Subtraction_m8_775 (Object_t * __this /* static, unused */, UFraction32_t8_121  ___f, float ___n, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UFraction32_t8_121_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1972);
		s_Il2CppMethodIntialized = true;
	}
	{
		UFraction32_t8_121  L_0 = ___f;
		IL2CPP_RUNTIME_CLASS_INIT(UFraction32_t8_121_il2cpp_TypeInfo_var);
		float L_1 = UFraction32_op_Explicit_m8_783(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		float L_2 = ___n;
		UFraction32_t8_121  L_3 = {0};
		UFraction32__ctor_m8_729(&L_3, ((float)((float)L_1-(float)L_2)), /*hidden argument*/NULL);
		return L_3;
	}
}
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::op_Subtraction(System.Single,ExifLibrary.MathEx/UFraction32)
extern TypeInfo* UFraction32_t8_121_il2cpp_TypeInfo_var;
extern "C" UFraction32_t8_121  UFraction32_op_Subtraction_m8_776 (Object_t * __this /* static, unused */, float ___n, UFraction32_t8_121  ___f, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UFraction32_t8_121_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1972);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___n;
		UFraction32_t8_121  L_1 = {0};
		UFraction32__ctor_m8_729(&L_1, L_0, /*hidden argument*/NULL);
		UFraction32_t8_121  L_2 = ___f;
		IL2CPP_RUNTIME_CLASS_INIT(UFraction32_t8_121_il2cpp_TypeInfo_var);
		UFraction32_t8_121  L_3 = UFraction32_op_Subtraction_m8_779(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::op_Subtraction(ExifLibrary.MathEx/UFraction32,System.Double)
extern TypeInfo* UFraction32_t8_121_il2cpp_TypeInfo_var;
extern "C" UFraction32_t8_121  UFraction32_op_Subtraction_m8_777 (Object_t * __this /* static, unused */, UFraction32_t8_121  ___f, double ___n, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UFraction32_t8_121_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1972);
		s_Il2CppMethodIntialized = true;
	}
	{
		UFraction32_t8_121  L_0 = ___f;
		IL2CPP_RUNTIME_CLASS_INIT(UFraction32_t8_121_il2cpp_TypeInfo_var);
		double L_1 = UFraction32_op_Explicit_m8_784(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		double L_2 = ___n;
		UFraction32_t8_121  L_3 = {0};
		UFraction32__ctor_m8_730(&L_3, ((double)((double)L_1-(double)L_2)), /*hidden argument*/NULL);
		return L_3;
	}
}
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::op_Subtraction(System.Double,ExifLibrary.MathEx/UFraction32)
extern TypeInfo* UFraction32_t8_121_il2cpp_TypeInfo_var;
extern "C" UFraction32_t8_121  UFraction32_op_Subtraction_m8_778 (Object_t * __this /* static, unused */, double ___n, UFraction32_t8_121  ___f, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UFraction32_t8_121_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1972);
		s_Il2CppMethodIntialized = true;
	}
	{
		double L_0 = ___n;
		UFraction32_t8_121  L_1 = {0};
		UFraction32__ctor_m8_730(&L_1, L_0, /*hidden argument*/NULL);
		UFraction32_t8_121  L_2 = ___f;
		IL2CPP_RUNTIME_CLASS_INIT(UFraction32_t8_121_il2cpp_TypeInfo_var);
		UFraction32_t8_121  L_3 = UFraction32_op_Subtraction_m8_779(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::op_Subtraction(ExifLibrary.MathEx/UFraction32,ExifLibrary.MathEx/UFraction32)
extern "C" UFraction32_t8_121  UFraction32_op_Subtraction_m8_779 (Object_t * __this /* static, unused */, UFraction32_t8_121  ___f1, UFraction32_t8_121  ___f2, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	uint32_t V_1 = 0;
	uint32_t V_2 = 0;
	uint32_t V_3 = 0;
	{
		uint32_t L_0 = UFraction32_get_Numerator_m8_733((&___f1), /*hidden argument*/NULL);
		V_0 = L_0;
		uint32_t L_1 = UFraction32_get_Denominator_m8_735((&___f2), /*hidden argument*/NULL);
		V_1 = L_1;
		uint32_t L_2 = UFraction32_get_Numerator_m8_733((&___f2), /*hidden argument*/NULL);
		V_2 = L_2;
		uint32_t L_3 = UFraction32_get_Denominator_m8_735((&___f2), /*hidden argument*/NULL);
		V_3 = L_3;
		uint32_t L_4 = V_0;
		uint32_t L_5 = V_3;
		uint32_t L_6 = V_2;
		uint32_t L_7 = V_1;
		uint32_t L_8 = V_1;
		uint32_t L_9 = V_3;
		UFraction32_t8_121  L_10 = {0};
		UFraction32__ctor_m8_726(&L_10, ((int32_t)((int32_t)((int32_t)((int32_t)L_4*(int32_t)L_5))-(int32_t)((int32_t)((int32_t)L_6*(int32_t)L_7)))), ((int32_t)((int32_t)L_8*(int32_t)L_9)), /*hidden argument*/NULL);
		return L_10;
	}
}
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::op_Increment(ExifLibrary.MathEx/UFraction32)
extern TypeInfo* UFraction32_t8_121_il2cpp_TypeInfo_var;
extern "C" UFraction32_t8_121  UFraction32_op_Increment_m8_780 (Object_t * __this /* static, unused */, UFraction32_t8_121  ___f, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UFraction32_t8_121_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1972);
		s_Il2CppMethodIntialized = true;
	}
	{
		UFraction32_t8_121  L_0 = ___f;
		UFraction32_t8_121  L_1 = {0};
		UFraction32__ctor_m8_726(&L_1, 1, 1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UFraction32_t8_121_il2cpp_TypeInfo_var);
		UFraction32_t8_121  L_2 = UFraction32_op_Addition_m8_772(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::op_Decrement(ExifLibrary.MathEx/UFraction32)
extern TypeInfo* UFraction32_t8_121_il2cpp_TypeInfo_var;
extern "C" UFraction32_t8_121  UFraction32_op_Decrement_m8_781 (Object_t * __this /* static, unused */, UFraction32_t8_121  ___f, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UFraction32_t8_121_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1972);
		s_Il2CppMethodIntialized = true;
	}
	{
		UFraction32_t8_121  L_0 = ___f;
		UFraction32_t8_121  L_1 = {0};
		UFraction32__ctor_m8_726(&L_1, 1, 1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UFraction32_t8_121_il2cpp_TypeInfo_var);
		UFraction32_t8_121  L_2 = UFraction32_op_Subtraction_m8_779(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.UInt32 ExifLibrary.MathEx/UFraction32::op_Explicit(ExifLibrary.MathEx/UFraction32)
extern "C" uint32_t UFraction32_op_Explicit_m8_782 (Object_t * __this /* static, unused */, UFraction32_t8_121  ___f, const MethodInfo* method)
{
	{
		uint32_t L_0 = UFraction32_get_Numerator_m8_733((&___f), /*hidden argument*/NULL);
		uint32_t L_1 = UFraction32_get_Denominator_m8_735((&___f), /*hidden argument*/NULL);
		return ((int32_t)((uint32_t)(int32_t)L_0/(uint32_t)(int32_t)L_1));
	}
}
// System.Single ExifLibrary.MathEx/UFraction32::op_Explicit(ExifLibrary.MathEx/UFraction32)
extern "C" float UFraction32_op_Explicit_m8_783 (Object_t * __this /* static, unused */, UFraction32_t8_121  ___f, const MethodInfo* method)
{
	{
		uint32_t L_0 = UFraction32_get_Numerator_m8_733((&___f), /*hidden argument*/NULL);
		uint32_t L_1 = UFraction32_get_Denominator_m8_735((&___f), /*hidden argument*/NULL);
		return ((float)((float)(((float)((float)(((double)((double)L_0))))))/(float)(((float)((float)(((double)((double)L_1))))))));
	}
}
// System.Double ExifLibrary.MathEx/UFraction32::op_Explicit(ExifLibrary.MathEx/UFraction32)
extern "C" double UFraction32_op_Explicit_m8_784 (Object_t * __this /* static, unused */, UFraction32_t8_121  ___f, const MethodInfo* method)
{
	{
		uint32_t L_0 = UFraction32_get_Numerator_m8_733((&___f), /*hidden argument*/NULL);
		uint32_t L_1 = UFraction32_get_Denominator_m8_735((&___f), /*hidden argument*/NULL);
		return ((double)((double)(((double)((double)(((double)((double)L_0))))))/(double)(((double)((double)(((double)((double)L_1))))))));
	}
}
// System.Boolean ExifLibrary.MathEx/UFraction32::op_Equality(ExifLibrary.MathEx/UFraction32,ExifLibrary.MathEx/UFraction32)
extern "C" bool UFraction32_op_Equality_m8_785 (Object_t * __this /* static, unused */, UFraction32_t8_121  ___f1, UFraction32_t8_121  ___f2, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		uint32_t L_0 = UFraction32_get_Numerator_m8_733((&___f1), /*hidden argument*/NULL);
		uint32_t L_1 = UFraction32_get_Numerator_m8_733((&___f2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0025;
		}
	}
	{
		uint32_t L_2 = UFraction32_get_Denominator_m8_735((&___f1), /*hidden argument*/NULL);
		uint32_t L_3 = UFraction32_get_Denominator_m8_735((&___f2), /*hidden argument*/NULL);
		G_B3_0 = ((((int32_t)L_2) == ((int32_t)L_3))? 1 : 0);
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		return G_B3_0;
	}
}
// System.Boolean ExifLibrary.MathEx/UFraction32::op_Inequality(ExifLibrary.MathEx/UFraction32,ExifLibrary.MathEx/UFraction32)
extern "C" bool UFraction32_op_Inequality_m8_786 (Object_t * __this /* static, unused */, UFraction32_t8_121  ___f1, UFraction32_t8_121  ___f2, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		uint32_t L_0 = UFraction32_get_Numerator_m8_733((&___f1), /*hidden argument*/NULL);
		uint32_t L_1 = UFraction32_get_Numerator_m8_733((&___f2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0028;
		}
	}
	{
		uint32_t L_2 = UFraction32_get_Denominator_m8_735((&___f1), /*hidden argument*/NULL);
		uint32_t L_3 = UFraction32_get_Denominator_m8_735((&___f2), /*hidden argument*/NULL);
		G_B3_0 = ((((int32_t)((((int32_t)L_2) == ((int32_t)L_3))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0029;
	}

IL_0028:
	{
		G_B3_0 = 1;
	}

IL_0029:
	{
		return G_B3_0;
	}
}
// System.Boolean ExifLibrary.MathEx/UFraction32::op_LessThan(ExifLibrary.MathEx/UFraction32,ExifLibrary.MathEx/UFraction32)
extern "C" bool UFraction32_op_LessThan_m8_787 (Object_t * __this /* static, unused */, UFraction32_t8_121  ___f1, UFraction32_t8_121  ___f2, const MethodInfo* method)
{
	{
		uint32_t L_0 = UFraction32_get_Numerator_m8_733((&___f1), /*hidden argument*/NULL);
		uint32_t L_1 = UFraction32_get_Denominator_m8_735((&___f2), /*hidden argument*/NULL);
		uint32_t L_2 = UFraction32_get_Numerator_m8_733((&___f2), /*hidden argument*/NULL);
		uint32_t L_3 = UFraction32_get_Denominator_m8_735((&___f1), /*hidden argument*/NULL);
		return ((!(((uint32_t)((int32_t)((int32_t)L_0*(int32_t)L_1))) >= ((uint32_t)((int32_t)((int32_t)L_2*(int32_t)L_3)))))? 1 : 0);
	}
}
// System.Boolean ExifLibrary.MathEx/UFraction32::op_GreaterThan(ExifLibrary.MathEx/UFraction32,ExifLibrary.MathEx/UFraction32)
extern "C" bool UFraction32_op_GreaterThan_m8_788 (Object_t * __this /* static, unused */, UFraction32_t8_121  ___f1, UFraction32_t8_121  ___f2, const MethodInfo* method)
{
	{
		uint32_t L_0 = UFraction32_get_Numerator_m8_733((&___f1), /*hidden argument*/NULL);
		uint32_t L_1 = UFraction32_get_Denominator_m8_735((&___f2), /*hidden argument*/NULL);
		uint32_t L_2 = UFraction32_get_Numerator_m8_733((&___f2), /*hidden argument*/NULL);
		uint32_t L_3 = UFraction32_get_Denominator_m8_735((&___f1), /*hidden argument*/NULL);
		return ((!(((uint32_t)((int32_t)((int32_t)L_0*(int32_t)L_1))) <= ((uint32_t)((int32_t)((int32_t)L_2*(int32_t)L_3)))))? 1 : 0);
	}
}
// System.UInt32 ExifLibrary.MathEx::GCD(System.UInt32,System.UInt32)
extern "C" uint32_t MathEx_GCD_m8_789 (Object_t * __this /* static, unused */, uint32_t ___a, uint32_t ___b, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	{
		goto IL_000f;
	}

IL_0005:
	{
		uint32_t L_0 = ___a;
		uint32_t L_1 = ___b;
		V_0 = ((int32_t)((uint32_t)(int32_t)L_0%(uint32_t)(int32_t)L_1));
		uint32_t L_2 = ___b;
		___a = L_2;
		uint32_t L_3 = V_0;
		___b = L_3;
	}

IL_000f:
	{
		uint32_t L_4 = ___b;
		if (L_4)
		{
			goto IL_0005;
		}
	}
	{
		uint32_t L_5 = ___a;
		return L_5;
	}
}
// System.Void VoxelBusters.Utility.GUIModalWindow::.ctor()
extern "C" void GUIModalWindow__ctor_m8_790 (GUIModalWindow_t8_15 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Screen_get_width_m6_145(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = Screen_get_height_m6_146(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t6_51  L_2 = {0};
		Rect__ctor_m6_295(&L_2, (0.0f), (0.0f), (((float)((float)L_0))), (((float)((float)L_1))), /*hidden argument*/NULL);
		__this->___m_windowRect_5 = L_2;
		MonoBehaviour__ctor_m6_671(__this, /*hidden argument*/NULL);
		return;
	}
}
// VoxelBusters.Utility.GUIScrollView VoxelBusters.Utility.GUIModalWindow::get_RootScrollView()
extern "C" GUIScrollView_t8_19 * GUIModalWindow_get_RootScrollView_m8_791 (GUIModalWindow_t8_15 * __this, const MethodInfo* method)
{
	{
		GUIScrollView_t8_19 * L_0 = (__this->___m_rootScrollView_4);
		return L_0;
	}
}
// UnityEngine.GUISkin VoxelBusters.Utility.GUIModalWindow::get_UISkin()
extern "C" GUISkin_t6_169 * GUIModalWindow_get_UISkin_m8_792 (GUIModalWindow_t8_15 * __this, const MethodInfo* method)
{
	{
		GUISkin_t6_169 * L_0 = (__this->___m_uiSkin_2);
		return L_0;
	}
}
// System.Void VoxelBusters.Utility.GUIModalWindow::set_UISkin(UnityEngine.GUISkin)
extern const MethodInfo* Object_Instantiate_TisGUISkin_t6_169_m6_1920_MethodInfo_var;
extern "C" void GUIModalWindow_set_UISkin_m8_793 (GUIModalWindow_t8_15 * __this, GUISkin_t6_169 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Object_Instantiate_TisGUISkin_t6_169_m6_1920_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484188);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUISkin_t6_169 * L_0 = ___value;
		bool L_1 = Object_op_Inequality_m6_722(NULL /*static, unused*/, L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		GUISkin_t6_169 * L_2 = ___value;
		GUISkin_t6_169 * L_3 = Object_Instantiate_TisGUISkin_t6_169_m6_1920(NULL /*static, unused*/, L_2, /*hidden argument*/Object_Instantiate_TisGUISkin_t6_169_m6_1920_MethodInfo_var);
		__this->___m_uiSkin_2 = L_3;
	}

IL_0018:
	{
		return;
	}
}
// System.Void VoxelBusters.Utility.GUIModalWindow::Awake()
extern "C" void GUIModalWindow_Awake_m8_794 (GUIModalWindow_t8_15 * __this, const MethodInfo* method)
{
	{
		GUISkin_t6_169 * L_0 = (__this->___m_uiSkin_2);
		GUIModalWindow_set_UISkin_m8_793(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.Utility.GUIModalWindow::Start()
extern const MethodInfo* GameObject_AddComponent_TisGUIScrollView_t8_19_m6_1918_MethodInfo_var;
extern "C" void GUIModalWindow_Start_m8_795 (GUIModalWindow_t8_15 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_AddComponent_TisGUIScrollView_t8_19_m6_1918_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484116);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t6_97 * L_0 = Component_get_gameObject_m6_725(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		GUIScrollView_t8_19 * L_1 = GameObject_AddComponent_TisGUIScrollView_t8_19_m6_1918(L_0, /*hidden argument*/GameObject_AddComponent_TisGUIScrollView_t8_19_m6_1918_MethodInfo_var);
		__this->___m_rootScrollView_4 = L_1;
		return;
	}
}
// System.Void VoxelBusters.Utility.GUIModalWindow::OnEnable()
extern "C" void GUIModalWindow_OnEnable_m8_796 (GUIModalWindow_t8_15 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void VoxelBusters.Utility.GUIModalWindow::OnDisable()
extern "C" void GUIModalWindow_OnDisable_m8_797 (GUIModalWindow_t8_15 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void VoxelBusters.Utility.GUIModalWindow::OnGUI()
extern TypeInfo* WindowFunction_t6_167_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern const MethodInfo* GUIModalWindow_GUIWindowBase_m8_799_MethodInfo_var;
extern "C" void GUIModalWindow_OnGUI_m8_798 (GUIModalWindow_t8_15 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WindowFunction_t6_167_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2080);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		GUIModalWindow_GUIWindowBase_m8_799_MethodInfo_var = il2cpp_codegen_method_info_from_index(541);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIModalWindow_SetSkin_m8_803(__this, /*hidden argument*/NULL);
		VirtActionInvoker0::Invoke(9 /* System.Void VoxelBusters.Utility.GUIModalWindow::AdjustFontBasedOnScreen() */, __this);
		VirtActionInvoker0::Invoke(10 /* System.Void VoxelBusters.Utility.GUIModalWindow::AdjustWindowBasedOnScreen() */, __this);
		int32_t L_0 = Object_GetInstanceID_m6_717(__this, /*hidden argument*/NULL);
		Rect_t6_51  L_1 = (__this->___m_windowRect_5);
		IntPtr_t L_2 = { (void*)GUIModalWindow_GUIWindowBase_m8_799_MethodInfo_var };
		WindowFunction_t6_167 * L_3 = (WindowFunction_t6_167 *)il2cpp_codegen_object_new (WindowFunction_t6_167_il2cpp_TypeInfo_var);
		WindowFunction__ctor_m6_1192(L_3, __this, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		Rect_t6_51  L_5 = GUI_Window_m6_1226(NULL /*static, unused*/, L_0, L_1, L_3, L_4, /*hidden argument*/NULL);
		__this->___m_windowRect_5 = L_5;
		GUIModalWindow_RestoreSkin_m8_804(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.Utility.GUIModalWindow::GUIWindowBase(System.Int32)
extern "C" void GUIModalWindow_GUIWindowBase_m8_799 (GUIModalWindow_t8_15 * __this, int32_t ____windowID, const MethodInfo* method)
{
	{
		VirtActionInvoker0::Invoke(8 /* System.Void VoxelBusters.Utility.GUIModalWindow::OnGUIWindow() */, __this);
		return;
	}
}
// System.Void VoxelBusters.Utility.GUIModalWindow::OnGUIWindow()
extern "C" void GUIModalWindow_OnGUIWindow_m8_800 (GUIModalWindow_t8_15 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void VoxelBusters.Utility.GUIModalWindow::AdjustFontBasedOnScreen()
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern "C" void GUIModalWindow_AdjustFontBasedOnScreen_m8_801 (GUIModalWindow_t8_15 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUISkin_t6_169 * L_0 = GUI_get_skin_m6_1202(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		GUIStyle_t6_176 * L_1 = GUISkin_get_box_m6_1352(L_0, /*hidden argument*/NULL);
		int32_t L_2 = Screen_get_width_m6_145(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		GUIStyle_set_fontSize_m6_1476(L_1, (((int32_t)((int32_t)((float)((float)(((float)((float)L_2)))*(float)(0.03f)))))), /*hidden argument*/NULL);
		GUISkin_t6_169 * L_3 = GUI_get_skin_m6_1202(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		GUIStyle_t6_176 * L_4 = GUISkin_get_button_m6_1360(L_3, /*hidden argument*/NULL);
		int32_t L_5 = Screen_get_width_m6_145(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		GUIStyle_set_fontSize_m6_1476(L_4, (((int32_t)((int32_t)((float)((float)(((float)((float)L_5)))*(float)(0.03f)))))), /*hidden argument*/NULL);
		GUISkin_t6_169 * L_6 = GUI_get_skin_m6_1202(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		GUIStyle_t6_176 * L_7 = GUISkin_get_label_m6_1354(L_6, /*hidden argument*/NULL);
		int32_t L_8 = Screen_get_width_m6_145(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		GUIStyle_set_fontSize_m6_1476(L_7, (((int32_t)((int32_t)((float)((float)(((float)((float)L_8)))*(float)(0.03f)))))), /*hidden argument*/NULL);
		GUISkin_t6_169 * L_9 = GUI_get_skin_m6_1202(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_9);
		GUIStyle_t6_176 * L_10 = GUISkin_get_toggle_m6_1362(L_9, /*hidden argument*/NULL);
		int32_t L_11 = Screen_get_width_m6_145(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_10);
		GUIStyle_set_fontSize_m6_1476(L_10, (((int32_t)((int32_t)((float)((float)(((float)((float)L_11)))*(float)(0.03f)))))), /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.Utility.GUIModalWindow::AdjustWindowBasedOnScreen()
extern "C" void GUIModalWindow_AdjustWindowBasedOnScreen_m8_802 (GUIModalWindow_t8_15 * __this, const MethodInfo* method)
{
	{
		Rect_t6_51 * L_0 = &(__this->___m_windowRect_5);
		int32_t L_1 = Screen_get_width_m6_145(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_set_width_m6_307(L_0, (((float)((float)L_1))), /*hidden argument*/NULL);
		Rect_t6_51 * L_2 = &(__this->___m_windowRect_5);
		int32_t L_3 = Screen_get_height_m6_146(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_set_height_m6_309(L_2, (((float)((float)L_3))), /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.Utility.GUIModalWindow::SetSkin()
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern "C" void GUIModalWindow_SetSkin_m8_803 (GUIModalWindow_t8_15 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUISkin_t6_169 * L_0 = GUI_get_skin_m6_1202(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___m_oldSkin_3 = L_0;
		GUISkin_t6_169 * L_1 = GUIModalWindow_get_UISkin_m8_792(__this, /*hidden argument*/NULL);
		GUI_set_skin_m6_1201(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.Utility.GUIModalWindow::RestoreSkin()
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern "C" void GUIModalWindow_RestoreSkin_m8_804 (GUIModalWindow_t8_15 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUISkin_t6_169 * L_0 = (__this->___m_oldSkin_3);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUI_set_skin_m6_1201(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Single VoxelBusters.Utility.GUIModalWindow::GetWindowWidth()
extern "C" float GUIModalWindow_GetWindowWidth_m8_805 (GUIModalWindow_t8_15 * __this, const MethodInfo* method)
{
	{
		Rect_t6_51 * L_0 = &(__this->___m_windowRect_5);
		float L_1 = Rect_get_width_m6_306(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Single VoxelBusters.Utility.GUIModalWindow::GetWindowHeight()
extern "C" float GUIModalWindow_GetWindowHeight_m8_806 (GUIModalWindow_t8_15 * __this, const MethodInfo* method)
{
	{
		Rect_t6_51 * L_0 = &(__this->___m_windowRect_5);
		float L_1 = Rect_get_height_m6_308(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void VoxelBusters.Utility.GUIScrollView::.ctor()
extern "C" void GUIScrollView__ctor_m8_807 (GUIScrollView_t8_19 * __this, const MethodInfo* method)
{
	{
		Vector2_t6_47  L_0 = Vector2_get_zero_m6_225(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___m_scrollPosition_2 = L_0;
		__this->___m_scrollSpeed_3 = (5.0f);
		Rect_t6_51  L_1 = {0};
		Rect__ctor_m6_295(&L_1, (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		__this->___m_rect_4 = L_1;
		MonoBehaviour__ctor_m6_671(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.Utility.GUIScrollView::BeginScrollView(UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C" void GUIScrollView_BeginScrollView_m8_808 (GUIScrollView_t8_19 * __this, GUIStyle_t6_176 * ____style, GUILayoutOptionU5BU5D_t6_290* ____options, const MethodInfo* method)
{
	{
		Vector2_t6_47  L_0 = (__this->___m_scrollPosition_2);
		GUIStyle_t6_176 * L_1 = ____style;
		GUILayoutOptionU5BU5D_t6_290* L_2 = ____options;
		Vector2_t6_47  L_3 = GUILayout_BeginScrollView_m6_1269(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		__this->___m_scrollPosition_2 = L_3;
		return;
	}
}
// System.Void VoxelBusters.Utility.GUIScrollView::BeginScrollView(UnityEngine.GUILayoutOption[])
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern "C" void GUIScrollView_BeginScrollView_m8_809 (GUIScrollView_t8_19 * __this, GUILayoutOptionU5BU5D_t6_290* ____options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUISkin_t6_169 * L_0 = GUI_get_skin_m6_1202(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		GUIStyle_t6_176 * L_1 = GUISkin_get_scrollView_m6_1390(L_0, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t6_290* L_2 = ____options;
		GUIScrollView_BeginScrollView_m8_808(__this, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.Utility.GUIScrollView::EndScrollView()
extern TypeInfo* GUILayoutUtility_t6_175_il2cpp_TypeInfo_var;
extern "C" void GUIScrollView_EndScrollView_m8_810 (GUIScrollView_t8_19 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t6_175_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1650);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUILayout_EndScrollView_m6_1271(NULL /*static, unused*/, /*hidden argument*/NULL);
		Event_t6_162 * L_0 = Event_get_current_m6_1150(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = Event_get_type_m6_1164(L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)7))))
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		Rect_t6_51  L_2 = GUILayoutUtility_GetLastRect_m6_1297(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___m_rect_4 = L_2;
	}

IL_0020:
	{
		return;
	}
}
// System.Void VoxelBusters.Utility.GUIScrollView::Reset()
extern "C" void GUIScrollView_Reset_m8_811 (GUIScrollView_t8_19 * __this, const MethodInfo* method)
{
	{
		Vector2_t6_47  L_0 = Vector2_get_zero_m6_225(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___m_scrollPosition_2 = L_0;
		return;
	}
}
// System.Void VoxelBusters.Utility.GUIScrollView::Update()
extern "C" void GUIScrollView_Update_m8_812 (GUIScrollView_t8_19 * __this, const MethodInfo* method)
{
	{
		GUIScrollView_UpdateScroll_m8_813(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.Utility.GUIScrollView::UpdateScroll()
extern TypeInfo* Input_t6_95_il2cpp_TypeInfo_var;
extern "C" void GUIScrollView_UpdateScroll_m8_813 (GUIScrollView_t8_19 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t6_95_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1613);
		s_Il2CppMethodIntialized = true;
	}
	Touch_t6_94  V_0 = {0};
	TouchU5BU5D_t6_282* V_1 = {0};
	int32_t V_2 = 0;
	Vector2_t6_47  V_3 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t6_95_il2cpp_TypeInfo_var);
		TouchU5BU5D_t6_282* L_0 = Input_get_touches_m6_693(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_0;
		V_2 = 0;
		goto IL_0081;
	}

IL_000d:
	{
		TouchU5BU5D_t6_282* L_1 = V_1;
		int32_t L_2 = V_2;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		V_0 = (*(Touch_t6_94 *)((Touch_t6_94 *)(Touch_t6_94 *)SZArrayLdElema(L_1, L_2, sizeof(Touch_t6_94 ))));
		Vector2_t6_47  L_3 = Touch_get_position_m6_679((&V_0), /*hidden argument*/NULL);
		V_3 = L_3;
		int32_t L_4 = Screen_get_height_m6_146(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_5 = ((&V_3)->___y_2);
		(&V_3)->___y_2 = ((float)((float)(((float)((float)L_4)))-(float)L_5));
		int32_t L_6 = Touch_get_phase_m6_681((&V_0), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)1))))
		{
			goto IL_007d;
		}
	}
	{
		Rect_t6_51 * L_7 = &(__this->___m_rect_4);
		Vector2_t6_47  L_8 = V_3;
		bool L_9 = Rect_Contains_m6_320(L_7, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_007d;
		}
	}
	{
		Vector2_t6_47  L_10 = (__this->___m_scrollPosition_2);
		Vector2_t6_47  L_11 = Touch_get_deltaPosition_m6_680((&V_0), /*hidden argument*/NULL);
		float L_12 = (__this->___m_scrollSpeed_3);
		Vector2_t6_47  L_13 = Vector2_op_Multiply_m6_231(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		Vector2_t6_47  L_14 = Vector2_op_Addition_m6_229(NULL /*static, unused*/, L_10, L_13, /*hidden argument*/NULL);
		__this->___m_scrollPosition_2 = L_14;
		goto IL_008a;
	}

IL_007d:
	{
		int32_t L_15 = V_2;
		V_2 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_0081:
	{
		int32_t L_16 = V_2;
		TouchU5BU5D_t6_282* L_17 = V_1;
		NullCheck(L_17);
		if ((((int32_t)L_16) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_17)->max_length)))))))
		{
			goto IL_000d;
		}
	}

IL_008a:
	{
		return;
	}
}
// System.Void VoxelBusters.Utility.GUIExtensions::.ctor()
extern "C" void GUIExtensions__ctor_m8_814 (GUIExtensions_t8_138 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String VoxelBusters.Utility.GUIExtensions::TextArea(System.String,UnityEngine.Rect)
extern TypeInfo* GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var;
extern "C" String_t* GUIExtensions_TextArea_m8_815 (Object_t * __this /* static, unused */, String_t* ____text, Rect_t6_51  ____normalisedBounds, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1651);
		s_Il2CppMethodIntialized = true;
	}
	Rect_t6_51  V_0 = {0};
	{
		Rect_t6_51  L_0 = ____normalisedBounds;
		Rect_t6_51  L_1 = GUIExtensions_GetScreenSpaceBounds_m8_818(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Rect_t6_51  L_2 = V_0;
		GUILayout_BeginArea_m6_1266(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		String_t* L_3 = ____text;
		if (!L_3)
		{
			goto IL_003f;
		}
	}
	{
		String_t* L_4 = ____text;
		GUILayoutOptionU5BU5D_t6_290* L_5 = ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 2));
		float L_6 = Rect_get_width_m6_306((&V_0), /*hidden argument*/NULL);
		GUILayoutOption_t6_182 * L_7 = GUILayout_Width_m6_1273(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 0);
		ArrayElementTypeCheck (L_5, L_7);
		*((GUILayoutOption_t6_182 **)(GUILayoutOption_t6_182 **)SZArrayLdElema(L_5, 0, sizeof(GUILayoutOption_t6_182 *))) = (GUILayoutOption_t6_182 *)L_7;
		GUILayoutOptionU5BU5D_t6_290* L_8 = L_5;
		float L_9 = Rect_get_height_m6_308((&V_0), /*hidden argument*/NULL);
		GUILayoutOption_t6_182 * L_10 = GUILayout_Height_m6_1275(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		ArrayElementTypeCheck (L_8, L_10);
		*((GUILayoutOption_t6_182 **)(GUILayoutOption_t6_182 **)SZArrayLdElema(L_8, 1, sizeof(GUILayoutOption_t6_182 *))) = (GUILayoutOption_t6_182 *)L_10;
		String_t* L_11 = GUILayout_TextArea_m6_1254(NULL /*static, unused*/, L_4, L_8, /*hidden argument*/NULL);
		____text = L_11;
	}

IL_003f:
	{
		GUILayout_EndArea_m6_1268(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_12 = ____text;
		return L_12;
	}
}
// System.Void VoxelBusters.Utility.GUIExtensions::Buttons(System.Collections.ArrayList,System.Action`1<System.String>,UnityEngine.Rect)
extern TypeInfo* GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var;
extern "C" void GUIExtensions_Buttons_m8_816 (Object_t * __this /* static, unused */, ArrayList_t1_170 * ____buttonsList, Action_1_t1_1917 * ____callbackOnPress, Rect_t6_51  ____normalisedBounds, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1651);
		s_Il2CppMethodIntialized = true;
	}
	Rect_t6_51  V_0 = {0};
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	GUILayoutOptionU5BU5D_t6_290* V_7 = {0};
	int32_t V_8 = 0;
	int32_t G_B4_0 = 0;
	int32_t G_B3_0 = 0;
	int32_t G_B5_0 = 0;
	int32_t G_B5_1 = 0;
	{
		ArrayList_t1_170 * L_0 = ____buttonsList;
		if (L_0)
		{
			goto IL_0007;
		}
	}
	{
		return;
	}

IL_0007:
	{
		Rect_t6_51  L_1 = ____normalisedBounds;
		Rect_t6_51  L_2 = GUIExtensions_GetScreenSpaceBounds_m8_818(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = Screen_get_height_m6_146(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = ((float)((float)(((float)((float)L_3)))*(float)(0.05f)));
		int32_t L_4 = Screen_get_height_m6_146(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = ((float)((float)(((float)((float)L_4)))*(float)(0.01f)));
		float L_5 = V_1;
		float L_6 = V_2;
		V_3 = ((float)((float)L_5+(float)L_6));
		ArrayList_t1_170 * L_7 = ____buttonsList;
		NullCheck(L_7);
		int32_t L_8 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_7);
		V_4 = L_8;
		float L_9 = Rect_get_height_m6_308((&V_0), /*hidden argument*/NULL);
		float L_10 = V_3;
		V_5 = (((int32_t)((int32_t)((float)((float)L_9/(float)L_10)))));
		int32_t L_11 = V_4;
		int32_t L_12 = V_5;
		int32_t L_13 = V_4;
		int32_t L_14 = V_5;
		G_B3_0 = ((int32_t)((int32_t)L_11/(int32_t)L_12));
		if (((int32_t)((int32_t)L_13%(int32_t)L_14)))
		{
			G_B4_0 = ((int32_t)((int32_t)L_11/(int32_t)L_12));
			goto IL_0055;
		}
	}
	{
		G_B5_0 = 0;
		G_B5_1 = G_B3_0;
		goto IL_0056;
	}

IL_0055:
	{
		G_B5_0 = 1;
		G_B5_1 = G_B4_0;
	}

IL_0056:
	{
		V_6 = ((int32_t)((int32_t)G_B5_1+(int32_t)G_B5_0));
		GUILayoutOptionU5BU5D_t6_290* L_15 = ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 1));
		float L_16 = V_1;
		GUILayoutOption_t6_182 * L_17 = GUILayout_Height_m6_1275(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 0);
		ArrayElementTypeCheck (L_15, L_17);
		*((GUILayoutOption_t6_182 **)(GUILayoutOption_t6_182 **)SZArrayLdElema(L_15, 0, sizeof(GUILayoutOption_t6_182 *))) = (GUILayoutOption_t6_182 *)L_17;
		V_7 = L_15;
		Rect_t6_51  L_18 = V_0;
		GUILayout_BeginArea_m6_1266(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		GUILayout_BeginHorizontal_m6_1260(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		V_8 = 0;
		goto IL_0099;
	}

IL_0083:
	{
		ArrayList_t1_170 * L_19 = ____buttonsList;
		Action_1_t1_1917 * L_20 = ____callbackOnPress;
		int32_t L_21 = V_8;
		int32_t L_22 = V_5;
		int32_t L_23 = V_5;
		GUILayoutOptionU5BU5D_t6_290* L_24 = V_7;
		GUIExtensions_DrawButtonsLayout_m8_817(NULL /*static, unused*/, L_19, L_20, ((int32_t)((int32_t)L_21*(int32_t)L_22)), L_23, L_24, /*hidden argument*/NULL);
		int32_t L_25 = V_8;
		V_8 = ((int32_t)((int32_t)L_25+(int32_t)1));
	}

IL_0099:
	{
		int32_t L_26 = V_8;
		int32_t L_27 = V_6;
		if ((((int32_t)L_26) < ((int32_t)L_27)))
		{
			goto IL_0083;
		}
	}
	{
		GUILayout_EndHorizontal_m6_1262(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndArea_m6_1268(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.Utility.GUIExtensions::DrawButtonsLayout(System.Collections.ArrayList,System.Action`1<System.String>,System.Int32,System.Int32,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m1_15057_MethodInfo_var;
extern "C" void GUIExtensions_DrawButtonsLayout_m8_817 (Object_t * __this /* static, unused */, ArrayList_t1_170 * ____buttonsList, Action_1_t1_1917 * ____callbackOnPress, int32_t ____startingIndex, int32_t ____buttonsPerColumn, GUILayoutOptionU5BU5D_t6_290* ____layoutOptions, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1651);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Action_1_Invoke_m1_15057_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484190);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	String_t* V_3 = {0};
	{
		ArrayList_t1_170 * L_0 = ____buttonsList;
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_0);
		V_0 = L_1;
		int32_t L_2 = ____startingIndex;
		int32_t L_3 = ____buttonsPerColumn;
		V_1 = ((int32_t)((int32_t)L_2+(int32_t)L_3));
		GUILayout_BeginVertical_m6_1263(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		int32_t L_4 = ____startingIndex;
		V_2 = L_4;
		goto IL_0052;
	}

IL_001d:
	{
		ArrayList_t1_170 * L_5 = ____buttonsList;
		int32_t L_6 = V_2;
		NullCheck(L_5);
		Object_t * L_7 = (Object_t *)VirtFuncInvoker1< Object_t *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_5, L_6);
		V_3 = ((String_t*)IsInstSealed(L_7, String_t_il2cpp_TypeInfo_var));
		GUILayout_FlexibleSpace_m6_1259(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_8 = V_3;
		GUILayoutOptionU5BU5D_t6_290* L_9 = ____layoutOptions;
		bool L_10 = GUILayout_Button_m6_1252(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0049;
		}
	}
	{
		Action_1_t1_1917 * L_11 = ____callbackOnPress;
		if (!L_11)
		{
			goto IL_0049;
		}
	}
	{
		Action_1_t1_1917 * L_12 = ____callbackOnPress;
		String_t* L_13 = V_3;
		NullCheck(L_12);
		Action_1_Invoke_m1_15057(L_12, L_13, /*hidden argument*/Action_1_Invoke_m1_15057_MethodInfo_var);
	}

IL_0049:
	{
		GUILayout_FlexibleSpace_m6_1259(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_14 = V_2;
		V_2 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_0052:
	{
		int32_t L_15 = V_2;
		int32_t L_16 = V_1;
		if ((((int32_t)L_15) >= ((int32_t)L_16)))
		{
			goto IL_0060;
		}
	}
	{
		int32_t L_17 = V_2;
		int32_t L_18 = V_0;
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_001d;
		}
	}

IL_0060:
	{
		GUILayout_EndVertical_m6_1265(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Rect VoxelBusters.Utility.GUIExtensions::GetScreenSpaceBounds(UnityEngine.Rect)
extern TypeInfo* Rect_t6_51_il2cpp_TypeInfo_var;
extern "C" Rect_t6_51  GUIExtensions_GetScreenSpaceBounds_m8_818 (Object_t * __this /* static, unused */, Rect_t6_51  ____normalisedBounds, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Rect_t6_51_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1593);
		s_Il2CppMethodIntialized = true;
	}
	Rect_t6_51  V_0 = {0};
	{
		Initobj (Rect_t6_51_il2cpp_TypeInfo_var, (&V_0));
		float L_0 = Rect_get_xMin_m6_311((&____normalisedBounds), /*hidden argument*/NULL);
		int32_t L_1 = Screen_get_width_m6_145(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_set_xMin_m6_312((&V_0), ((float)((float)L_0*(float)(((float)((float)L_1))))), /*hidden argument*/NULL);
		float L_2 = Rect_get_xMax_m6_315((&____normalisedBounds), /*hidden argument*/NULL);
		int32_t L_3 = Screen_get_width_m6_145(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_set_xMax_m6_316((&V_0), ((float)((float)L_2*(float)(((float)((float)L_3))))), /*hidden argument*/NULL);
		float L_4 = Rect_get_yMin_m6_313((&____normalisedBounds), /*hidden argument*/NULL);
		int32_t L_5 = Screen_get_height_m6_146(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_set_yMin_m6_314((&V_0), ((float)((float)L_4*(float)(((float)((float)L_5))))), /*hidden argument*/NULL);
		float L_6 = Rect_get_yMax_m6_317((&____normalisedBounds), /*hidden argument*/NULL);
		int32_t L_7 = Screen_get_height_m6_146(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_set_yMax_m6_318((&V_0), ((float)((float)L_6*(float)(((float)((float)L_7))))), /*hidden argument*/NULL);
		Rect_t6_51  L_8 = V_0;
		return L_8;
	}
}
// System.Void VoxelBusters.Utility.UnityGUILayoutUtility::.ctor()
extern "C" void UnityGUILayoutUtility__ctor_m8_819 (UnityGUILayoutUtility_t8_139 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m6_671(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean VoxelBusters.Utility.UnityGUILayoutUtility::Foldout(UnityEngine.GUIContent,System.Boolean)
extern TypeInfo* GUIStyle_t6_176_il2cpp_TypeInfo_var;
extern TypeInfo* GUIContent_t6_171_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4832;
extern Il2CppCodeGenString* _stringLiteral636;
extern Il2CppCodeGenString* _stringLiteral1117;
extern Il2CppCodeGenString* _stringLiteral5233;
extern "C" bool UnityGUILayoutUtility_Foldout_m8_820 (Object_t * __this /* static, unused */, GUIContent_t6_171 * ____label, bool ____state, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t6_176_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1648);
		GUIContent_t6_171_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1646);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1651);
		_stringLiteral4832 = il2cpp_codegen_string_literal_from_index(4832);
		_stringLiteral636 = il2cpp_codegen_string_literal_from_index(636);
		_stringLiteral1117 = il2cpp_codegen_string_literal_from_index(1117);
		_stringLiteral5233 = il2cpp_codegen_string_literal_from_index(5233);
		s_Il2CppMethodIntialized = true;
	}
	GUIStyle_t6_176 * V_0 = {0};
	GUIContent_t6_171 * V_1 = {0};
	String_t* V_2 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle_t6_176 * L_0 = GUIStyle_op_Implicit_m6_1493(NULL /*static, unused*/, _stringLiteral4832, /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_1 = (GUIStyle_t6_176 *)il2cpp_codegen_object_new (GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m6_1429(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		GUIStyle_t6_176 * L_2 = V_0;
		NullCheck(L_2);
		GUIStyle_set_richText_m6_1477(L_2, 1, /*hidden argument*/NULL);
		GUIContent_t6_171 * L_3 = ____label;
		GUIContent_t6_171 * L_4 = (GUIContent_t6_171 *)il2cpp_codegen_object_new (GUIContent_t6_171_il2cpp_TypeInfo_var);
		GUIContent__ctor_m6_1241(L_4, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		V_2 = (String_t*)NULL;
		bool L_5 = ____state;
		if (!L_5)
		{
			goto IL_0031;
		}
	}
	{
		V_2 = _stringLiteral636;
		goto IL_0037;
	}

IL_0031:
	{
		V_2 = _stringLiteral1117;
	}

IL_0037:
	{
		GUIContent_t6_171 * L_6 = V_1;
		String_t* L_7 = V_2;
		GUIContent_t6_171 * L_8 = ____label;
		NullCheck(L_8);
		String_t* L_9 = GUIContent_get_text_m6_1243(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Format_m1_549(NULL /*static, unused*/, _stringLiteral5233, L_7, L_9, /*hidden argument*/NULL);
		NullCheck(L_6);
		GUIContent_set_text_m6_1244(L_6, L_10, /*hidden argument*/NULL);
		GUILayout_BeginHorizontal_m6_1260(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		GUIContent_t6_171 * L_11 = V_1;
		GUIStyle_t6_176 * L_12 = V_0;
		bool L_13 = GUILayout_Toggle_m6_1256(NULL /*static, unused*/, 1, L_11, L_12, ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_0072;
		}
	}
	{
		bool L_14 = ____state;
		____state = ((((int32_t)L_14) == ((int32_t)0))? 1 : 0);
	}

IL_0072:
	{
		GUILayout_EndHorizontal_m6_1262(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_15 = ____state;
		return L_15;
	}
}
// System.Void VoxelBusters.Utility.UnityGUI.MENU.GUIMainMenu::.ctor()
extern "C" void GUIMainMenu__ctor_m8_821 (GUIMainMenu_t8_140 * __this, const MethodInfo* method)
{
	{
		GUIMenuBase__ctor_m8_827(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.Utility.UnityGUI.MENU.GUIMainMenu::Start()
extern const MethodInfo* Component_GetComponentsInChildren_TisGUISubMenu_t8_143_m6_1921_MethodInfo_var;
extern "C" void GUIMainMenu_Start_m8_822 (GUIMainMenu_t8_140 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponentsInChildren_TisGUISubMenu_t8_143_m6_1921_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484191);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		GUISubMenuU5BU5D_t8_142* L_0 = Component_GetComponentsInChildren_TisGUISubMenu_t8_143_m6_1921(__this, /*hidden argument*/Component_GetComponentsInChildren_TisGUISubMenu_t8_143_m6_1921_MethodInfo_var);
		__this->___m_subMenuList_10 = L_0;
		__this->___m_showingMainMenu_12 = 1;
		V_0 = 0;
		goto IL_002c;
	}

IL_001a:
	{
		GUISubMenuU5BU5D_t8_142* L_1 = (__this->___m_subMenuList_10);
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		NullCheck((*(GUISubMenu_t8_143 **)(GUISubMenu_t8_143 **)SZArrayLdElema(L_1, L_3, sizeof(GUISubMenu_t8_143 *))));
		GUISubMenu_SetActive_m8_842((*(GUISubMenu_t8_143 **)(GUISubMenu_t8_143 **)SZArrayLdElema(L_1, L_3, sizeof(GUISubMenu_t8_143 *))), 0, /*hidden argument*/NULL);
		int32_t L_4 = V_0;
		V_0 = ((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_002c:
	{
		int32_t L_5 = V_0;
		GUISubMenuU5BU5D_t8_142* L_6 = (__this->___m_subMenuList_10);
		NullCheck(L_6);
		if ((((int32_t)L_5) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_6)->max_length)))))))
		{
			goto IL_001a;
		}
	}
	{
		return;
	}
}
// System.Void VoxelBusters.Utility.UnityGUI.MENU.GUIMainMenu::Update()
extern "C" void GUIMainMenu_Update_m8_823 (GUIMainMenu_t8_140 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_showingMainMenu_12);
		if (L_0)
		{
			goto IL_001f;
		}
	}
	{
		GUISubMenu_t8_143 * L_1 = (__this->___m_activeSubMenu_11);
		NullCheck(L_1);
		bool L_2 = GUISubMenu_IsActive_m8_843(L_1, /*hidden argument*/NULL);
		__this->___m_showingMainMenu_12 = ((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}

IL_001f:
	{
		return;
	}
}
// System.Void VoxelBusters.Utility.UnityGUI.MENU.GUIMainMenu::OnGUI()
extern "C" void GUIMainMenu_OnGUI_m8_824 (GUIMainMenu_t8_140 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_showingMainMenu_12);
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		VirtActionInvoker0::Invoke(8 /* System.Void VoxelBusters.Utility.UnityGUI.MENU.GUIMainMenu::DrawMainMenu() */, __this);
	}

IL_0011:
	{
		return;
	}
}
// System.Void VoxelBusters.Utility.UnityGUI.MENU.GUIMainMenu::DrawMainMenu()
extern Il2CppCodeGenString* _stringLiteral5234;
extern "C" void GUIMainMenu_DrawMainMenu_m8_825 (GUIMainMenu_t8_140 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral5234 = il2cpp_codegen_string_literal_from_index(5234);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t G_B2_0 = 0;
	int32_t G_B1_0 = 0;
	int32_t G_B3_0 = 0;
	int32_t G_B3_1 = 0;
	{
		GUIMenuBase_DrawTitle_m8_831(__this, _stringLiteral5234, /*hidden argument*/NULL);
		GUISubMenuU5BU5D_t8_142* L_0 = (__this->___m_subMenuList_10);
		NullCheck(L_0);
		V_0 = (((int32_t)((int32_t)(((Array_t *)L_0)->max_length))));
		int32_t L_1 = V_0;
		int32_t L_2 = V_0;
		G_B1_0 = ((int32_t)((int32_t)L_1/(int32_t)5));
		if (((int32_t)((int32_t)L_2%(int32_t)5)))
		{
			G_B2_0 = ((int32_t)((int32_t)L_1/(int32_t)5));
			goto IL_0025;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 1;
		G_B3_1 = G_B2_0;
	}

IL_0026:
	{
		V_1 = ((int32_t)((int32_t)G_B3_1+(int32_t)G_B3_0));
		int32_t L_3 = V_1;
		GUIMenuBase_BeginButtonLayout_m8_833(__this, (((float)((float)L_3))), (0.8f), /*hidden argument*/NULL);
		GUISubMenuU5BU5D_t8_142* L_4 = (__this->___m_subMenuList_10);
		GUIMainMenu_DrawSubMenuColumn_m8_826(__this, L_4, 0, 5, /*hidden argument*/NULL);
		GUISubMenuU5BU5D_t8_142* L_5 = (__this->___m_subMenuList_10);
		int32_t L_6 = V_0;
		GUIMainMenu_DrawSubMenuColumn_m8_826(__this, L_5, 5, L_6, /*hidden argument*/NULL);
		GUIMenuBase_EndButtonLayout_m8_834(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.Utility.UnityGUI.MENU.GUIMainMenu::DrawSubMenuColumn(VoxelBusters.Utility.UnityGUI.MENU.GUISubMenu[],System.Int32,System.Int32)
extern TypeInfo* GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var;
extern "C" void GUIMainMenu_DrawSubMenuColumn_m8_826 (GUIMainMenu_t8_140 * __this, GUISubMenuU5BU5D_t8_142* ____subMenuList, int32_t ____startIndex, int32_t ____endIndex, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1651);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	GUISubMenu_t8_143 * V_2 = {0};
	{
		GUISubMenuU5BU5D_t8_142* L_0 = ____subMenuList;
		NullCheck(L_0);
		V_0 = (((int32_t)((int32_t)(((Array_t *)L_0)->max_length))));
		GUILayout_BeginVertical_m6_1263(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		int32_t L_1 = ____startIndex;
		V_1 = L_1;
		goto IL_0049;
	}

IL_0016:
	{
		GUISubMenuU5BU5D_t8_142* L_2 = ____subMenuList;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		V_2 = (*(GUISubMenu_t8_143 **)(GUISubMenu_t8_143 **)SZArrayLdElema(L_2, L_4, sizeof(GUISubMenu_t8_143 *)));
		GUISubMenu_t8_143 * L_5 = V_2;
		NullCheck(L_5);
		String_t* L_6 = GUISubMenu_get_SubMenuName_m8_840(L_5, /*hidden argument*/NULL);
		bool L_7 = GUIMenuBase_DrawButton_m8_835(__this, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0045;
		}
	}
	{
		GUISubMenu_t8_143 * L_8 = V_2;
		__this->___m_activeSubMenu_11 = L_8;
		GUISubMenu_t8_143 * L_9 = (__this->___m_activeSubMenu_11);
		NullCheck(L_9);
		GUISubMenu_SetActive_m8_842(L_9, 1, /*hidden argument*/NULL);
		__this->___m_showingMainMenu_12 = 0;
	}

IL_0045:
	{
		int32_t L_10 = V_1;
		V_1 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0049:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = ____endIndex;
		if ((((int32_t)L_11) >= ((int32_t)L_12)))
		{
			goto IL_0057;
		}
	}
	{
		int32_t L_13 = V_1;
		int32_t L_14 = V_0;
		if ((((int32_t)L_13) < ((int32_t)L_14)))
		{
			goto IL_0016;
		}
	}

IL_0057:
	{
		GUILayout_EndVertical_m6_1265(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.Utility.UnityGUI.MENU.GUIMenuBase::.ctor()
extern "C" void GUIMenuBase__ctor_m8_827 (GUIMenuBase_t8_141 * __this, const MethodInfo* method)
{
	{
		Rect_t6_51  L_0 = {0};
		Rect__ctor_m6_295(&L_0, (0.05f), (0.0f), (0.9f), (0.2f), /*hidden argument*/NULL);
		__this->___m_titleLayoutNormalisedRect_5 = L_0;
		Rect_t6_51  L_1 = {0};
		Rect__ctor_m6_295(&L_1, (0.05f), (0.2f), (0.9f), (0.8f), /*hidden argument*/NULL);
		__this->___m_buttonLayoutNormalisedRect_6 = L_1;
		Rect_t6_51  L_2 = {0};
		Rect__ctor_m6_295(&L_2, (0.0f), (0.8f), (1.0f), (1.0f), /*hidden argument*/NULL);
		__this->___m_resultLayoutNormalisedRect_7 = L_2;
		__this->___m_buttonColumnCount_8 = (2.0f);
		MonoBehaviour__ctor_m6_671(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.Utility.UnityGUI.MENU.GUIMenuBase::OnEnable()
extern "C" void GUIMenuBase_OnEnable_m8_828 (GUIMenuBase_t8_141 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void VoxelBusters.Utility.UnityGUI.MENU.GUIMenuBase::OnDisable()
extern "C" void GUIMenuBase_OnDisable_m8_829 (GUIMenuBase_t8_141 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void VoxelBusters.Utility.UnityGUI.MENU.GUIMenuBase::OnGUI()
extern "C" void GUIMenuBase_OnGUI_m8_830 (GUIMenuBase_t8_141 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void VoxelBusters.Utility.UnityGUI.MENU.GUIMenuBase::DrawTitle(System.String)
extern "C" void GUIMenuBase_DrawTitle_m8_831 (GUIMenuBase_t8_141 * __this, String_t* ____title, const MethodInfo* method)
{
	{
		String_t* L_0 = ____title;
		GUIMenuBase_DrawTitleWithBackButton_m8_832(__this, L_0, (String_t*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean VoxelBusters.Utility.UnityGUI.MENU.GUIMenuBase::DrawTitleWithBackButton(System.String,System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t6_176_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var;
extern "C" bool GUIMenuBase_DrawTitleWithBackButton_m8_832 (GUIMenuBase_t8_141 * __this, String_t* ____title, String_t* ____button, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		GUIStyle_t6_176_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1648);
		GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1651);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	GUIStyle_t6_176 * V_1 = {0};
	GUIStyle_t6_176 * V_2 = {0};
	{
		V_0 = 0;
		String_t* L_0 = ____button;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1_521(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_004b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUISkin_t6_169 * L_2 = GUI_get_skin_m6_1202(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		GUIStyle_t6_176 * L_3 = GUISkin_get_button_m6_1360(L_2, /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_4 = (GUIStyle_t6_176 *)il2cpp_codegen_object_new (GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m6_1429(L_4, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		GUIStyle_t6_176 * L_5 = V_1;
		NullCheck(L_5);
		GUIStyle_set_alignment_m6_1460(L_5, 4, /*hidden argument*/NULL);
		Rect_t6_51  L_6 = {0};
		Rect__ctor_m6_295(&L_6, (10.0f), (5.0f), (40.0f), (40.0f), /*hidden argument*/NULL);
		String_t* L_7 = ____button;
		GUIStyle_t6_176 * L_8 = V_1;
		bool L_9 = GUI_Button_m6_1207(NULL /*static, unused*/, L_6, L_7, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_004b;
		}
	}
	{
		V_0 = 1;
	}

IL_004b:
	{
		Rect_t6_51  L_10 = (__this->___m_titleLayoutNormalisedRect_5);
		Rect_t6_51  L_11 = GUIMenuBase_GetLayoutRect_m8_837(__this, L_10, /*hidden argument*/NULL);
		GUILayout_BeginArea_m6_1266(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUISkin_t6_169 * L_12 = GUI_get_skin_m6_1202(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_12);
		GUIStyle_t6_176 * L_13 = GUISkin_get_label_m6_1354(L_12, /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_14 = (GUIStyle_t6_176 *)il2cpp_codegen_object_new (GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m6_1429(L_14, L_13, /*hidden argument*/NULL);
		V_2 = L_14;
		GUIStyle_t6_176 * L_15 = V_2;
		NullCheck(L_15);
		GUIStyle_set_alignment_m6_1460(L_15, 1, /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_16 = V_2;
		NullCheck(L_16);
		GUIStyle_set_fontSize_m6_1476(L_16, ((int32_t)20), /*hidden argument*/NULL);
		GUILayout_Space_m6_1258(NULL /*static, unused*/, (10.0f), /*hidden argument*/NULL);
		String_t* L_17 = ____title;
		GUIStyle_t6_176 * L_18 = V_2;
		GUILayout_Label_m6_1248(NULL /*static, unused*/, L_17, L_18, ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		GUILayout_EndArea_m6_1268(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_19 = V_0;
		return L_19;
	}
}
// System.Void VoxelBusters.Utility.UnityGUI.MENU.GUIMenuBase::BeginButtonLayout(System.Single,System.Single)
extern TypeInfo* GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var;
extern "C" void GUIMenuBase_BeginButtonLayout_m8_833 (GUIMenuBase_t8_141 * __this, float ____columnCount, float ____normalisedHeight, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1651);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t6_51 * L_0 = &(__this->___m_buttonLayoutNormalisedRect_6);
		float L_1 = ____normalisedHeight;
		Rect_set_height_m6_309(L_0, L_1, /*hidden argument*/NULL);
		float L_2 = ____columnCount;
		__this->___m_buttonColumnCount_8 = L_2;
		Rect_t6_51  L_3 = (__this->___m_buttonLayoutNormalisedRect_6);
		Rect_t6_51  L_4 = GUIMenuBase_GetLayoutRect_m8_837(__this, L_3, /*hidden argument*/NULL);
		GUILayout_BeginArea_m6_1266(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		GUILayout_BeginHorizontal_m6_1260(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.Utility.UnityGUI.MENU.GUIMenuBase::EndButtonLayout()
extern "C" void GUIMenuBase_EndButtonLayout_m8_834 (GUIMenuBase_t8_141 * __this, const MethodInfo* method)
{
	{
		GUILayout_EndHorizontal_m6_1262(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndArea_m6_1268(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean VoxelBusters.Utility.UnityGUI.MENU.GUIMenuBase::DrawButton(System.String)
extern TypeInfo* GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var;
extern "C" bool GUIMenuBase_DrawButton_m8_835 (GUIMenuBase_t8_141 * __this, String_t* ____buttonName, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1651);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ____buttonName;
		GUILayoutOptionU5BU5D_t6_290* L_1 = ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 2));
		GUILayoutOption_t6_182 * L_2 = GUILayout_MinHeight_m6_1276(NULL /*static, unused*/, (40.0f), /*hidden argument*/NULL);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, L_2);
		*((GUILayoutOption_t6_182 **)(GUILayoutOption_t6_182 **)SZArrayLdElema(L_1, 0, sizeof(GUILayoutOption_t6_182 *))) = (GUILayoutOption_t6_182 *)L_2;
		GUILayoutOptionU5BU5D_t6_290* L_3 = L_1;
		int32_t L_4 = Screen_get_width_m6_145(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t6_51 * L_5 = &(__this->___m_buttonLayoutNormalisedRect_6);
		float L_6 = Rect_get_xMax_m6_315(L_5, /*hidden argument*/NULL);
		float L_7 = (__this->___m_buttonColumnCount_8);
		GUILayoutOption_t6_182 * L_8 = GUILayout_MaxWidth_m6_1274(NULL /*static, unused*/, ((float)((float)((float)((float)(((float)((float)L_4)))*(float)L_6))/(float)L_7)), /*hidden argument*/NULL);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 1);
		ArrayElementTypeCheck (L_3, L_8);
		*((GUILayoutOption_t6_182 **)(GUILayoutOption_t6_182 **)SZArrayLdElema(L_3, 1, sizeof(GUILayoutOption_t6_182 *))) = (GUILayoutOption_t6_182 *)L_8;
		bool L_9 = GUILayout_Button_m6_1252(NULL /*static, unused*/, L_0, L_3, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0041;
		}
	}
	{
		return 1;
	}

IL_0041:
	{
		return 0;
	}
}
// System.Void VoxelBusters.Utility.UnityGUI.MENU.GUIMenuBase::DrawResultLayout(System.String,System.Single)
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t6_176_il2cpp_TypeInfo_var;
extern "C" void GUIMenuBase_DrawResultLayout_m8_836 (GUIMenuBase_t8_141 * __this, String_t* ____result, float ____normalisedHeight, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		GUIStyle_t6_176_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1648);
		s_Il2CppMethodIntialized = true;
	}
	GUIStyle_t6_176 * V_0 = {0};
	{
		Rect_t6_51 * L_0 = &(__this->___m_resultLayoutNormalisedRect_7);
		float L_1 = ____normalisedHeight;
		Rect_set_height_m6_309(L_0, L_1, /*hidden argument*/NULL);
		Rect_t6_51 * L_2 = &(__this->___m_resultLayoutNormalisedRect_7);
		float L_3 = Rect_get_height_m6_308(L_2, /*hidden argument*/NULL);
		if ((!(((float)L_3) == ((float)(0.0f)))))
		{
			goto IL_0022;
		}
	}
	{
		return;
	}

IL_0022:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUISkin_t6_169 * L_4 = GUI_get_skin_m6_1202(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		GUIStyle_t6_176 * L_5 = GUISkin_get_textArea_m6_1358(L_4, /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_6 = (GUIStyle_t6_176 *)il2cpp_codegen_object_new (GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m6_1429(L_6, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		Rect_t6_51  L_7 = (__this->___m_resultLayoutNormalisedRect_7);
		Rect_t6_51  L_8 = GUIMenuBase_GetLayoutRect_m8_837(__this, L_7, /*hidden argument*/NULL);
		String_t* L_9 = ____result;
		GUIStyle_t6_176 * L_10 = V_0;
		GUI_Label_m6_1204(NULL /*static, unused*/, L_8, L_9, L_10, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Rect VoxelBusters.Utility.UnityGUI.MENU.GUIMenuBase::GetLayoutRect(UnityEngine.Rect)
extern "C" Rect_t6_51  GUIMenuBase_GetLayoutRect_m8_837 (GUIMenuBase_t8_141 * __this, Rect_t6_51  ____normalisedRect, const MethodInfo* method)
{
	Vector2_t6_47  V_0 = {0};
	Rect_t6_51  V_1 = {0};
	{
		int32_t L_0 = Screen_get_width_m6_145(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = Screen_get_height_m6_146(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2__ctor_m6_215((&V_0), (((float)((float)L_0))), (((float)((float)L_1))), /*hidden argument*/NULL);
		float L_2 = ((&V_0)->___x_1);
		float L_3 = Rect_get_xMin_m6_311((&____normalisedRect), /*hidden argument*/NULL);
		float L_4 = ((&V_0)->___y_2);
		float L_5 = Rect_get_yMin_m6_313((&____normalisedRect), /*hidden argument*/NULL);
		float L_6 = ((&V_0)->___x_1);
		float L_7 = Rect_get_xMax_m6_315((&____normalisedRect), /*hidden argument*/NULL);
		float L_8 = ((&V_0)->___y_2);
		float L_9 = Rect_get_yMax_m6_317((&____normalisedRect), /*hidden argument*/NULL);
		Rect__ctor_m6_295((&V_1), ((float)((float)L_2*(float)L_3)), ((float)((float)L_4*(float)L_5)), ((float)((float)L_6*(float)L_7)), ((float)((float)L_8*(float)L_9)), /*hidden argument*/NULL);
		Rect_t6_51  L_10 = V_1;
		return L_10;
	}
}
// System.Void VoxelBusters.Utility.UnityGUI.MENU.GUISubMenu::.ctor()
extern "C" void GUISubMenu__ctor_m8_838 (GUISubMenu_t8_143 * __this, const MethodInfo* method)
{
	{
		GUIMenuBase__ctor_m8_827(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GameObject VoxelBusters.Utility.UnityGUI.MENU.GUISubMenu::get_CachedGameObject()
extern "C" GameObject_t6_97 * GUISubMenu_get_CachedGameObject_m8_839 (GUISubMenu_t8_143 * __this, const MethodInfo* method)
{
	{
		GameObject_t6_97 * L_0 = (__this->___m_gameObject_9);
		bool L_1 = Object_op_Equality_m6_721(NULL /*static, unused*/, L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		GameObject_t6_97 * L_2 = Component_get_gameObject_m6_725(__this, /*hidden argument*/NULL);
		__this->___m_gameObject_9 = L_2;
	}

IL_001d:
	{
		GameObject_t6_97 * L_3 = (__this->___m_gameObject_9);
		return L_3;
	}
}
// System.String VoxelBusters.Utility.UnityGUI.MENU.GUISubMenu::get_SubMenuName()
extern "C" String_t* GUISubMenu_get_SubMenuName_m8_840 (GUISubMenu_t8_143 * __this, const MethodInfo* method)
{
	{
		GameObject_t6_97 * L_0 = GUISubMenu_get_CachedGameObject_m8_839(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = Object_get_name_m6_708(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void VoxelBusters.Utility.UnityGUI.MENU.GUISubMenu::OnGUI()
extern Il2CppCodeGenString* _stringLiteral2550;
extern "C" void GUISubMenu_OnGUI_m8_841 (GUISubMenu_t8_143 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral2550 = il2cpp_codegen_string_literal_from_index(2550);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = GUISubMenu_get_SubMenuName_m8_840(__this, /*hidden argument*/NULL);
		bool L_1 = GUIMenuBase_DrawTitleWithBackButton_m8_832(__this, L_0, _stringLiteral2550, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		GUISubMenu_OnPressingBackButton_m8_844(__this, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void VoxelBusters.Utility.UnityGUI.MENU.GUISubMenu::SetActive(System.Boolean)
extern "C" void GUISubMenu_SetActive_m8_842 (GUISubMenu_t8_143 * __this, bool ____newState, const MethodInfo* method)
{
	{
		GameObject_t6_97 * L_0 = GUISubMenu_get_CachedGameObject_m8_839(__this, /*hidden argument*/NULL);
		bool L_1 = ____newState;
		NullCheck(L_0);
		GameObject_SetActive_m6_742(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean VoxelBusters.Utility.UnityGUI.MENU.GUISubMenu::IsActive()
extern "C" bool GUISubMenu_IsActive_m8_843 (GUISubMenu_t8_143 * __this, const MethodInfo* method)
{
	{
		GameObject_t6_97 * L_0 = GUISubMenu_get_CachedGameObject_m8_839(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = GameObject_get_activeSelf_m6_743(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void VoxelBusters.Utility.UnityGUI.MENU.GUISubMenu::OnPressingBackButton()
extern "C" void GUISubMenu_OnPressingBackButton_m8_844 (GUISubMenu_t8_143 * __this, const MethodInfo* method)
{
	{
		GameObject_t6_97 * L_0 = Component_get_gameObject_m6_725(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		GameObject_SetActive_m6_742(L_0, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.Utility.EditorCoroutine::.ctor()
extern "C" void EditorCoroutine__ctor_m8_845 (EditorCoroutine_t8_144 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.Utility.EditorCoroutine::.ctor(System.Collections.IEnumerator)
extern "C" void EditorCoroutine__ctor_m8_846 (EditorCoroutine_t8_144 * __this, Object_t * ____enumerator, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		Object_t * L_0 = ____enumerator;
		EditorCoroutine_set_CoroutineMethod_m8_848(__this, L_0, /*hidden argument*/NULL);
		__this->___m_finishedCoroutine_0 = 0;
		return;
	}
}
// System.Collections.IEnumerator VoxelBusters.Utility.EditorCoroutine::get_CoroutineMethod()
extern "C" Object_t * EditorCoroutine_get_CoroutineMethod_m8_847 (EditorCoroutine_t8_144 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U3CCoroutineMethodU3Ek__BackingField_1);
		return L_0;
	}
}
// System.Void VoxelBusters.Utility.EditorCoroutine::set_CoroutineMethod(System.Collections.IEnumerator)
extern "C" void EditorCoroutine_set_CoroutineMethod_m8_848 (EditorCoroutine_t8_144 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		__this->___U3CCoroutineMethodU3Ek__BackingField_1 = L_0;
		return;
	}
}
// VoxelBusters.Utility.EditorCoroutine VoxelBusters.Utility.EditorCoroutine::StartCoroutine(System.Collections.IEnumerator)
extern "C" EditorCoroutine_t8_144 * EditorCoroutine_StartCoroutine_m8_849 (Object_t * __this /* static, unused */, Object_t * ____enumerator, const MethodInfo* method)
{
	EditorCoroutine_t8_144 * V_0 = {0};
	{
		V_0 = (EditorCoroutine_t8_144 *)NULL;
		EditorCoroutine_t8_144 * L_0 = V_0;
		return L_0;
	}
}
// System.Void VoxelBusters.Utility.EditorCoroutine::StopCoroutine()
extern "C" void EditorCoroutine_StopCoroutine_m8_850 (EditorCoroutine_t8_144 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void VoxelBusters.Utility.EditorCoroutine::Update()
extern TypeInfo* IEnumerator_t1_193_il2cpp_TypeInfo_var;
extern "C" void EditorCoroutine_Update_m8_851 (EditorCoroutine_t8_144 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IEnumerator_t1_193_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(144);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = EditorCoroutine_get_CoroutineMethod_m8_847(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_0);
		if (L_1)
		{
			goto IL_0016;
		}
	}
	{
		EditorCoroutine_StopCoroutine_m8_850(__this, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void VoxelBusters.Utility.EnumMaskFieldAttribute::.ctor()
extern "C" void EnumMaskFieldAttribute__ctor_m8_852 (EnumMaskFieldAttribute_t8_145 * __this, const MethodInfo* method)
{
	{
		PropertyAttribute__ctor_m6_1620(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.Utility.EnumMaskFieldAttribute::.ctor(System.Type)
extern "C" void EnumMaskFieldAttribute__ctor_m8_853 (EnumMaskFieldAttribute_t8_145 * __this, Type_t * ____targetType, const MethodInfo* method)
{
	{
		PropertyAttribute__ctor_m6_1620(__this, /*hidden argument*/NULL);
		Type_t * L_0 = ____targetType;
		EnumMaskFieldAttribute_set_TargetType_m8_855(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Type VoxelBusters.Utility.EnumMaskFieldAttribute::get_TargetType()
extern "C" Type_t * EnumMaskFieldAttribute_get_TargetType_m8_854 (EnumMaskFieldAttribute_t8_145 * __this, const MethodInfo* method)
{
	{
		Type_t * L_0 = (__this->___U3CTargetTypeU3Ek__BackingField_0);
		return L_0;
	}
}
// System.Void VoxelBusters.Utility.EnumMaskFieldAttribute::set_TargetType(System.Type)
extern "C" void EnumMaskFieldAttribute_set_TargetType_m8_855 (EnumMaskFieldAttribute_t8_145 * __this, Type_t * ___value, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___value;
		__this->___U3CTargetTypeU3Ek__BackingField_0 = L_0;
		return;
	}
}
// System.Boolean VoxelBusters.Utility.EnumMaskFieldAttribute::IsEnum()
extern "C" bool EnumMaskFieldAttribute_IsEnum_m8_856 (EnumMaskFieldAttribute_t8_145 * __this, const MethodInfo* method)
{
	{
		Type_t * L_0 = EnumMaskFieldAttribute_get_TargetType_m8_854(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(127 /* System.Boolean System.Type::get_IsEnum() */, L_0);
		return L_1;
	}
}
// System.Void VoxelBusters.Utility.InspectorButtonAttribute::.ctor()
extern "C" void InspectorButtonAttribute__ctor_m8_857 (InspectorButtonAttribute_t8_147 * __this, const MethodInfo* method)
{
	{
		PropertyAttribute__ctor_m6_1620(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.Utility.InspectorButtonAttribute::.ctor(System.String,System.String,VoxelBusters.Utility.InspectorButtonAttribute/ePosition)
extern "C" void InspectorButtonAttribute__ctor_m8_858 (InspectorButtonAttribute_t8_147 * __this, String_t* ____buttonName, String_t* ____invokeMethod, int32_t ____position, const MethodInfo* method)
{
	{
		PropertyAttribute__ctor_m6_1620(__this, /*hidden argument*/NULL);
		String_t* L_0 = ____buttonName;
		InspectorButtonAttribute_set_Name_m8_860(__this, L_0, /*hidden argument*/NULL);
		String_t* L_1 = ____invokeMethod;
		InspectorButtonAttribute_set_InvokeMethod_m8_862(__this, L_1, /*hidden argument*/NULL);
		int32_t L_2 = ____position;
		InspectorButtonAttribute_set_Position_m8_864(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.String VoxelBusters.Utility.InspectorButtonAttribute::get_Name()
extern "C" String_t* InspectorButtonAttribute_get_Name_m8_859 (InspectorButtonAttribute_t8_147 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CNameU3Ek__BackingField_0);
		return L_0;
	}
}
// System.Void VoxelBusters.Utility.InspectorButtonAttribute::set_Name(System.String)
extern "C" void InspectorButtonAttribute_set_Name_m8_860 (InspectorButtonAttribute_t8_147 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CNameU3Ek__BackingField_0 = L_0;
		return;
	}
}
// System.String VoxelBusters.Utility.InspectorButtonAttribute::get_InvokeMethod()
extern "C" String_t* InspectorButtonAttribute_get_InvokeMethod_m8_861 (InspectorButtonAttribute_t8_147 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CInvokeMethodU3Ek__BackingField_1);
		return L_0;
	}
}
// System.Void VoxelBusters.Utility.InspectorButtonAttribute::set_InvokeMethod(System.String)
extern "C" void InspectorButtonAttribute_set_InvokeMethod_m8_862 (InspectorButtonAttribute_t8_147 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CInvokeMethodU3Ek__BackingField_1 = L_0;
		return;
	}
}
// VoxelBusters.Utility.InspectorButtonAttribute/ePosition VoxelBusters.Utility.InspectorButtonAttribute::get_Position()
extern "C" int32_t InspectorButtonAttribute_get_Position_m8_863 (InspectorButtonAttribute_t8_147 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___U3CPositionU3Ek__BackingField_2);
		return L_0;
	}
}
// System.Void VoxelBusters.Utility.InspectorButtonAttribute::set_Position(VoxelBusters.Utility.InspectorButtonAttribute/ePosition)
extern "C" void InspectorButtonAttribute_set_Position_m8_864 (InspectorButtonAttribute_t8_147 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___U3CPositionU3Ek__BackingField_2 = L_0;
		return;
	}
}
// System.Void VoxelBusters.Utility.ExecuteOnValueChangeAttribute::.ctor()
extern "C" void ExecuteOnValueChangeAttribute__ctor_m8_865 (ExecuteOnValueChangeAttribute_t8_148 * __this, const MethodInfo* method)
{
	{
		PropertyAttribute__ctor_m6_1620(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.Utility.ExecuteOnValueChangeAttribute::.ctor(System.String)
extern "C" void ExecuteOnValueChangeAttribute__ctor_m8_866 (ExecuteOnValueChangeAttribute_t8_148 * __this, String_t* ____method, const MethodInfo* method)
{
	{
		PropertyAttribute__ctor_m6_1620(__this, /*hidden argument*/NULL);
		String_t* L_0 = ____method;
		ExecuteOnValueChangeAttribute_set_InvokeMethod_m8_868(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String VoxelBusters.Utility.ExecuteOnValueChangeAttribute::get_InvokeMethod()
extern "C" String_t* ExecuteOnValueChangeAttribute_get_InvokeMethod_m8_867 (ExecuteOnValueChangeAttribute_t8_148 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CInvokeMethodU3Ek__BackingField_0);
		return L_0;
	}
}
// System.Void VoxelBusters.Utility.ExecuteOnValueChangeAttribute::set_InvokeMethod(System.String)
extern "C" void ExecuteOnValueChangeAttribute_set_InvokeMethod_m8_868 (ExecuteOnValueChangeAttribute_t8_148 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CInvokeMethodU3Ek__BackingField_0 = L_0;
		return;
	}
}
// System.Void VoxelBusters.Utility.RegexAttribute::.ctor(System.String,System.String)
extern "C" void RegexAttribute__ctor_m8_869 (RegexAttribute_t8_149 * __this, String_t* ___pattern, String_t* ___helpMessage, const MethodInfo* method)
{
	{
		PropertyAttribute__ctor_m6_1620(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___pattern;
		__this->___pattern_0 = L_0;
		String_t* L_1 = ___helpMessage;
		__this->___helpMessage_1 = L_1;
		return;
	}
}
// System.Void VoxelBusters.Utility.EditorInvoke::.ctor()
extern "C" void EditorInvoke__ctor_m8_870 (EditorInvoke_t8_150 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.Utility.EditorInvoke::.cctor()
extern TypeInfo* Dictionary_2_t1_1907_il2cpp_TypeInfo_var;
extern TypeInfo* EditorInvoke_t8_150_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_15058_MethodInfo_var;
extern "C" void EditorInvoke__cctor_m8_871 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t1_1907_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2083);
		EditorInvoke_t8_150_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2084);
		Dictionary_2__ctor_m1_15058_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484192);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t1_1907 * L_0 = (Dictionary_2_t1_1907 *)il2cpp_codegen_object_new (Dictionary_2_t1_1907_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_15058(L_0, /*hidden argument*/Dictionary_2__ctor_m1_15058_MethodInfo_var);
		((EditorInvoke_t8_150_StaticFields*)EditorInvoke_t8_150_il2cpp_TypeInfo_var->static_fields)->___invokeMethodsContainer_4 = L_0;
		return;
	}
}
// System.Void VoxelBusters.Utility.EditorInvoke::Invoke(System.Action,System.Single)
extern TypeInfo* EditorInvoke_t8_150_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t1_1929_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_15059_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral5235;
extern Il2CppCodeGenString* _stringLiteral5236;
extern "C" void EditorInvoke_Invoke_m8_872 (Object_t * __this /* static, unused */, Action_t5_11 * ____method, float ____time, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		EditorInvoke_t8_150_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2084);
		Dictionary_2_t1_1929_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2082);
		Dictionary_2__ctor_m1_15059_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484193);
		_stringLiteral5235 = il2cpp_codegen_string_literal_from_index(5235);
		_stringLiteral5236 = il2cpp_codegen_string_literal_from_index(5236);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t1_1929 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(EditorInvoke_t8_150_il2cpp_TypeInfo_var);
		Dictionary_2_t1_1907 * L_0 = ((EditorInvoke_t8_150_StaticFields*)EditorInvoke_t8_150_il2cpp_TypeInfo_var->static_fields)->___invokeMethodsContainer_4;
		Action_t5_11 * L_1 = ____method;
		Dictionary_2_t1_1929 * L_2 = (Dictionary_2_t1_1929 *)il2cpp_codegen_object_new (Dictionary_2_t1_1929_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_15059(L_2, /*hidden argument*/Dictionary_2__ctor_m1_15059_MethodInfo_var);
		V_0 = L_2;
		Dictionary_2_t1_1929 * L_3 = V_0;
		NullCheck(L_3);
		VirtActionInvoker2< String_t*, float >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Single>::Add(!0,!1) */, L_3, _stringLiteral5235, (0.0f));
		Dictionary_2_t1_1929 * L_4 = V_0;
		float L_5 = ____time;
		NullCheck(L_4);
		VirtActionInvoker2< String_t*, float >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Single>::Add(!0,!1) */, L_4, _stringLiteral5236, L_5);
		Dictionary_2_t1_1929 * L_6 = V_0;
		NullCheck(L_0);
		VirtActionInvoker2< Action_t5_11 *, Dictionary_2_t1_1929 * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.Action,System.Collections.Generic.Dictionary`2<System.String,System.Single>>::set_Item(!0,!1) */, L_0, L_1, L_6);
		return;
	}
}
// System.Void VoxelBusters.Utility.EditorInvoke::InvokeRepeating(System.Action,System.Single,System.Single)
extern TypeInfo* EditorInvoke_t8_150_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t1_1929_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_15059_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral5235;
extern Il2CppCodeGenString* _stringLiteral5236;
extern Il2CppCodeGenString* _stringLiteral5237;
extern "C" void EditorInvoke_InvokeRepeating_m8_873 (Object_t * __this /* static, unused */, Action_t5_11 * ____method, float ____time, float ____repeatRate, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		EditorInvoke_t8_150_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2084);
		Dictionary_2_t1_1929_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2082);
		Dictionary_2__ctor_m1_15059_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484193);
		_stringLiteral5235 = il2cpp_codegen_string_literal_from_index(5235);
		_stringLiteral5236 = il2cpp_codegen_string_literal_from_index(5236);
		_stringLiteral5237 = il2cpp_codegen_string_literal_from_index(5237);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t1_1929 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(EditorInvoke_t8_150_il2cpp_TypeInfo_var);
		Dictionary_2_t1_1907 * L_0 = ((EditorInvoke_t8_150_StaticFields*)EditorInvoke_t8_150_il2cpp_TypeInfo_var->static_fields)->___invokeMethodsContainer_4;
		Action_t5_11 * L_1 = ____method;
		Dictionary_2_t1_1929 * L_2 = (Dictionary_2_t1_1929 *)il2cpp_codegen_object_new (Dictionary_2_t1_1929_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_15059(L_2, /*hidden argument*/Dictionary_2__ctor_m1_15059_MethodInfo_var);
		V_0 = L_2;
		Dictionary_2_t1_1929 * L_3 = V_0;
		NullCheck(L_3);
		VirtActionInvoker2< String_t*, float >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Single>::Add(!0,!1) */, L_3, _stringLiteral5235, (0.0f));
		Dictionary_2_t1_1929 * L_4 = V_0;
		float L_5 = ____time;
		NullCheck(L_4);
		VirtActionInvoker2< String_t*, float >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Single>::Add(!0,!1) */, L_4, _stringLiteral5236, L_5);
		Dictionary_2_t1_1929 * L_6 = V_0;
		float L_7 = ____repeatRate;
		NullCheck(L_6);
		VirtActionInvoker2< String_t*, float >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Single>::Add(!0,!1) */, L_6, _stringLiteral5237, L_7);
		Dictionary_2_t1_1929 * L_8 = V_0;
		NullCheck(L_0);
		VirtActionInvoker2< Action_t5_11 *, Dictionary_2_t1_1929 * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.Action,System.Collections.Generic.Dictionary`2<System.String,System.Single>>::set_Item(!0,!1) */, L_0, L_1, L_8);
		return;
	}
}
// System.Void VoxelBusters.Utility.EditorScheduler/CallbackFunction::.ctor(System.Object,System.IntPtr)
extern "C" void CallbackFunction__ctor_m8_874 (CallbackFunction_t8_151 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void VoxelBusters.Utility.EditorScheduler/CallbackFunction::Invoke()
extern "C" void CallbackFunction_Invoke_m8_875 (CallbackFunction_t8_151 * __this, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		CallbackFunction_Invoke_m8_875((CallbackFunction_t8_151 *)__this->___prev_9, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if ((__this->___m_target_2 != NULL || MethodHasParameters((MethodInfo*)(__this->___method_3.___m_value_0))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_CallbackFunction_t8_151(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.IAsyncResult VoxelBusters.Utility.EditorScheduler/CallbackFunction::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * CallbackFunction_BeginInvoke_m8_876 (CallbackFunction_t8_151 * __this, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void VoxelBusters.Utility.EditorScheduler/CallbackFunction::EndInvoke(System.IAsyncResult)
extern "C" void CallbackFunction_EndInvoke_m8_877 (CallbackFunction_t8_151 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void VoxelBusters.Utility.EditorScheduler::.ctor()
extern "C" void EditorScheduler__ctor_m8_878 (EditorScheduler_t8_152 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.Utility.EditorScheduler::.cctor()
extern "C" void EditorScheduler__cctor_m8_879 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void VoxelBusters.Utility.EditorScheduler::add_ScheduleUpdate(VoxelBusters.Utility.EditorScheduler/CallbackFunction)
extern TypeInfo* EditorScheduler_t8_152_il2cpp_TypeInfo_var;
extern TypeInfo* CallbackFunction_t8_151_il2cpp_TypeInfo_var;
extern "C" void EditorScheduler_add_ScheduleUpdate_m8_880 (Object_t * __this /* static, unused */, CallbackFunction_t8_151 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		EditorScheduler_t8_152_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2085);
		CallbackFunction_t8_151_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2086);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(EditorScheduler_t8_152_il2cpp_TypeInfo_var);
		CallbackFunction_t8_151 * L_0 = ((EditorScheduler_t8_152_StaticFields*)EditorScheduler_t8_152_il2cpp_TypeInfo_var->static_fields)->___ScheduleUpdate_0;
		CallbackFunction_t8_151 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Combine_m1_908(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((EditorScheduler_t8_152_StaticFields*)EditorScheduler_t8_152_il2cpp_TypeInfo_var->static_fields)->___ScheduleUpdate_0 = ((CallbackFunction_t8_151 *)CastclassSealed(L_2, CallbackFunction_t8_151_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void VoxelBusters.Utility.EditorScheduler::remove_ScheduleUpdate(VoxelBusters.Utility.EditorScheduler/CallbackFunction)
extern TypeInfo* EditorScheduler_t8_152_il2cpp_TypeInfo_var;
extern TypeInfo* CallbackFunction_t8_151_il2cpp_TypeInfo_var;
extern "C" void EditorScheduler_remove_ScheduleUpdate_m8_881 (Object_t * __this /* static, unused */, CallbackFunction_t8_151 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		EditorScheduler_t8_152_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2085);
		CallbackFunction_t8_151_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2086);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(EditorScheduler_t8_152_il2cpp_TypeInfo_var);
		CallbackFunction_t8_151 * L_0 = ((EditorScheduler_t8_152_StaticFields*)EditorScheduler_t8_152_il2cpp_TypeInfo_var->static_fields)->___ScheduleUpdate_0;
		CallbackFunction_t8_151 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Remove_m1_911(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((EditorScheduler_t8_152_StaticFields*)EditorScheduler_t8_152_il2cpp_TypeInfo_var->static_fields)->___ScheduleUpdate_0 = ((CallbackFunction_t8_151 *)CastclassSealed(L_2, CallbackFunction_t8_151_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void VoxelBusters.Utility.EditorScheduler::Update()
extern TypeInfo* EditorScheduler_t8_152_il2cpp_TypeInfo_var;
extern "C" void EditorScheduler_Update_m8_882 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		EditorScheduler_t8_152_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2085);
		s_Il2CppMethodIntialized = true;
	}
	DelegateU5BU5D_t1_1664* V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(EditorScheduler_t8_152_il2cpp_TypeInfo_var);
		CallbackFunction_t8_151 * L_0 = ((EditorScheduler_t8_152_StaticFields*)EditorScheduler_t8_152_il2cpp_TypeInfo_var->static_fields)->___ScheduleUpdate_0;
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(EditorScheduler_t8_152_il2cpp_TypeInfo_var);
		CallbackFunction_t8_151 * L_1 = ((EditorScheduler_t8_152_StaticFields*)EditorScheduler_t8_152_il2cpp_TypeInfo_var->static_fields)->___ScheduleUpdate_0;
		NullCheck(L_1);
		DelegateU5BU5D_t1_1664* L_2 = (DelegateU5BU5D_t1_1664*)VirtFuncInvoker0< DelegateU5BU5D_t1_1664* >::Invoke(10 /* System.Delegate[] System.MulticastDelegate::GetInvocationList() */, L_1);
		V_0 = L_2;
		DelegateU5BU5D_t1_1664* L_3 = V_0;
		NullCheck(L_3);
		V_1 = (((int32_t)((int32_t)(((Array_t *)L_3)->max_length))));
		V_2 = 0;
		goto IL_003d;
	}

IL_0021:
	{
		DelegateU5BU5D_t1_1664* L_4 = V_0;
		int32_t L_5 = V_2;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		if (!(*(Delegate_t1_22 **)(Delegate_t1_22 **)SZArrayLdElema(L_4, L_6, sizeof(Delegate_t1_22 *))))
		{
			goto IL_0039;
		}
	}
	{
		DelegateU5BU5D_t1_1664* L_7 = V_0;
		int32_t L_8 = V_2;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		NullCheck((*(Delegate_t1_22 **)(Delegate_t1_22 **)SZArrayLdElema(L_7, L_9, sizeof(Delegate_t1_22 *))));
		MethodInfo_t * L_10 = Delegate_get_Method_m1_883((*(Delegate_t1_22 **)(Delegate_t1_22 **)SZArrayLdElema(L_7, L_9, sizeof(Delegate_t1_22 *))), /*hidden argument*/NULL);
		NullCheck(L_10);
		VirtFuncInvoker2< Object_t *, Object_t *, ObjectU5BU5D_t1_272* >::Invoke(43 /* System.Object System.Reflection.MethodBase::Invoke(System.Object,System.Object[]) */, L_10, NULL, (ObjectU5BU5D_t1_272*)(ObjectU5BU5D_t1_272*)NULL);
	}

IL_0039:
	{
		int32_t L_11 = V_2;
		V_2 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_003d:
	{
		int32_t L_12 = V_2;
		int32_t L_13 = V_1;
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_0021;
		}
	}
	{
		return;
	}
}
// System.Void VoxelBusters.Utility.DownloadTextureDemo::.ctor()
extern "C" void DownloadTextureDemo__ctor_m8_883 (DownloadTextureDemo_t8_153 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m6_671(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.Utility.DownloadTextureDemo::StartDownload()
extern TypeInfo* DownloadTexture_t8_162_il2cpp_TypeInfo_var;
extern TypeInfo* Completion_t8_161_il2cpp_TypeInfo_var;
extern const MethodInfo* DownloadTextureDemo_U3CStartDownloadU3Em__4_m8_885_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral732;
extern "C" void DownloadTextureDemo_StartDownload_m8_884 (DownloadTextureDemo_t8_153 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DownloadTexture_t8_162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2087);
		Completion_t8_161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2088);
		DownloadTextureDemo_U3CStartDownloadU3Em__4_m8_885_MethodInfo_var = il2cpp_codegen_method_info_from_index(546);
		_stringLiteral732 = il2cpp_codegen_string_literal_from_index(732);
		s_Il2CppMethodIntialized = true;
	}
	URL_t8_156  V_0 = {0};
	DownloadTexture_t8_162 * V_1 = {0};
	{
		String_t* L_0 = (__this->___m_URLString_2);
		NullCheck(L_0);
		bool L_1 = String_StartsWith_m1_531(L_0, _stringLiteral732, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		String_t* L_2 = (__this->___m_URLString_2);
		URL_t8_156  L_3 = URL_URLWithString_m8_963(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0032;
	}

IL_0026:
	{
		String_t* L_4 = (__this->___m_URLString_2);
		URL_t8_156  L_5 = URL_FileURLWithPath_m8_961(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
	}

IL_0032:
	{
		URL_t8_156  L_6 = V_0;
		DownloadTexture_t8_162 * L_7 = (DownloadTexture_t8_162 *)il2cpp_codegen_object_new (DownloadTexture_t8_162_il2cpp_TypeInfo_var);
		DownloadTexture__ctor_m8_929(L_7, L_6, 1, 1, /*hidden argument*/NULL);
		V_1 = L_7;
		DownloadTexture_t8_162 * L_8 = V_1;
		IntPtr_t L_9 = { (void*)DownloadTextureDemo_U3CStartDownloadU3Em__4_m8_885_MethodInfo_var };
		Completion_t8_161 * L_10 = (Completion_t8_161 *)il2cpp_codegen_object_new (Completion_t8_161_il2cpp_TypeInfo_var);
		Completion__ctor_m8_925(L_10, __this, L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		DownloadTexture_set_OnCompletion_m8_935(L_8, L_10, /*hidden argument*/NULL);
		DownloadTexture_t8_162 * L_11 = V_1;
		NullCheck(L_11);
		Request_StartRequest_m8_900(L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.Utility.DownloadTextureDemo::<StartDownload>m__4(UnityEngine.Texture2D,System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5238;
extern "C" void DownloadTextureDemo_U3CStartDownloadU3Em__4_m8_885 (DownloadTextureDemo_t8_153 * __this, Texture2D_t6_33 * ____texture, String_t* ____error, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5238 = il2cpp_codegen_string_literal_from_index(5238);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ____error;
		String_t* L_1 = StringExtensions_GetPrintableString_m8_224(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Format_m1_548(NULL /*static, unused*/, _stringLiteral5238, L_1, /*hidden argument*/NULL);
		Debug_Log_m6_631(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		Texture2D_t6_33 * L_3 = ____texture;
		bool L_4 = Object_op_Inequality_m6_722(NULL /*static, unused*/, L_3, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0032;
		}
	}
	{
		MeshRenderer_t6_28 * L_5 = (__this->___m_renderer_3);
		NullCheck(L_5);
		Material_t6_72 * L_6 = Renderer_get_sharedMaterial_m6_142(L_5, /*hidden argument*/NULL);
		Texture2D_t6_33 * L_7 = ____texture;
		NullCheck(L_6);
		Material_set_mainTexture_m6_498(L_6, L_7, /*hidden argument*/NULL);
	}

IL_0032:
	{
		return;
	}
}
// System.Void VoxelBusters.Utility.Request/<StartAsynchronousRequest>c__Iterator4::.ctor()
extern "C" void U3CStartAsynchronousRequestU3Ec__Iterator4__ctor_m8_886 (U3CStartAsynchronousRequestU3Ec__Iterator4_t8_154 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object VoxelBusters.Utility.Request/<StartAsynchronousRequest>c__Iterator4::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CStartAsynchronousRequestU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m8_887 (U3CStartAsynchronousRequestU3Ec__Iterator4_t8_154 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_1);
		return L_0;
	}
}
// System.Object VoxelBusters.Utility.Request/<StartAsynchronousRequest>c__Iterator4::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CStartAsynchronousRequestU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m8_888 (U3CStartAsynchronousRequestU3Ec__Iterator4_t8_154 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_1);
		return L_0;
	}
}
// System.Boolean VoxelBusters.Utility.Request/<StartAsynchronousRequest>c__Iterator4::MoveNext()
extern "C" bool U3CStartAsynchronousRequestU3Ec__Iterator4_MoveNext_m8_889 (U3CStartAsynchronousRequestU3Ec__Iterator4_t8_154 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_0);
		V_0 = L_0;
		__this->___U24PC_0 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0039;
		}
	}
	{
		goto IL_0060;
	}

IL_0021:
	{
		goto IL_0039;
	}

IL_0026:
	{
		__this->___U24current_1 = NULL;
		__this->___U24PC_0 = 1;
		goto IL_0062;
	}

IL_0039:
	{
		Request_t8_155 * L_2 = (__this->___U3CU3Ef__this_2);
		NullCheck(L_2);
		WWW_t6_78 * L_3 = Request_get_WWWObject_m8_898(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		bool L_4 = WWW_get_isDone_m6_557(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0026;
		}
	}
	{
		Request_t8_155 * L_5 = (__this->___U3CU3Ef__this_2);
		NullCheck(L_5);
		VirtActionInvoker0::Invoke(4 /* System.Void VoxelBusters.Utility.Request::OnFetchingResponse() */, L_5);
		__this->___U24PC_0 = (-1);
	}

IL_0060:
	{
		return 0;
	}

IL_0062:
	{
		return 1;
	}
	// Dead block : IL_0064: ldloc.1
}
// System.Void VoxelBusters.Utility.Request/<StartAsynchronousRequest>c__Iterator4::Dispose()
extern "C" void U3CStartAsynchronousRequestU3Ec__Iterator4_Dispose_m8_890 (U3CStartAsynchronousRequestU3Ec__Iterator4_t8_154 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_0 = (-1);
		return;
	}
}
// System.Void VoxelBusters.Utility.Request/<StartAsynchronousRequest>c__Iterator4::Reset()
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern "C" void U3CStartAsynchronousRequestU3Ec__Iterator4_Reset_m8_891 (U3CStartAsynchronousRequestU3Ec__Iterator4_t8_154 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14416(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void VoxelBusters.Utility.Request::.ctor()
extern "C" void Request__ctor_m8_892 (Request_t8_155 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.Utility.Request::.ctor(VoxelBusters.Utility.URL,System.Boolean)
extern "C" void Request__ctor_m8_893 (Request_t8_155 * __this, URL_t8_156  ____URL, bool ____isAsynchronous, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		URL_t8_156  L_0 = ____URL;
		Request_set_URL_m8_897(__this, L_0, /*hidden argument*/NULL);
		bool L_1 = ____isAsynchronous;
		Request_set_IsAsynchronous_m8_895(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean VoxelBusters.Utility.Request::get_IsAsynchronous()
extern "C" bool Request_get_IsAsynchronous_m8_894 (Request_t8_155 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___U3CIsAsynchronousU3Ek__BackingField_1);
		return L_0;
	}
}
// System.Void VoxelBusters.Utility.Request::set_IsAsynchronous(System.Boolean)
extern "C" void Request_set_IsAsynchronous_m8_895 (Request_t8_155 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___U3CIsAsynchronousU3Ek__BackingField_1 = L_0;
		return;
	}
}
// VoxelBusters.Utility.URL VoxelBusters.Utility.Request::get_URL()
extern "C" URL_t8_156  Request_get_URL_m8_896 (Request_t8_155 * __this, const MethodInfo* method)
{
	{
		URL_t8_156  L_0 = (__this->___U3CURLU3Ek__BackingField_2);
		return L_0;
	}
}
// System.Void VoxelBusters.Utility.Request::set_URL(VoxelBusters.Utility.URL)
extern "C" void Request_set_URL_m8_897 (Request_t8_155 * __this, URL_t8_156  ___value, const MethodInfo* method)
{
	{
		URL_t8_156  L_0 = ___value;
		__this->___U3CURLU3Ek__BackingField_2 = L_0;
		return;
	}
}
// UnityEngine.WWW VoxelBusters.Utility.Request::get_WWWObject()
extern "C" WWW_t6_78 * Request_get_WWWObject_m8_898 (Request_t8_155 * __this, const MethodInfo* method)
{
	{
		WWW_t6_78 * L_0 = (__this->___U3CWWWObjectU3Ek__BackingField_3);
		return L_0;
	}
}
// System.Void VoxelBusters.Utility.Request::set_WWWObject(UnityEngine.WWW)
extern "C" void Request_set_WWWObject_m8_899 (Request_t8_155 * __this, WWW_t6_78 * ___value, const MethodInfo* method)
{
	{
		WWW_t6_78 * L_0 = ___value;
		__this->___U3CWWWObjectU3Ek__BackingField_3 = L_0;
		return;
	}
}
// System.Void VoxelBusters.Utility.Request::StartRequest()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Request_t8_155_il2cpp_TypeInfo_var;
extern TypeInfo* GameObject_t6_97_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisMonoBehaviour_t6_91_m6_1922_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral5239;
extern Il2CppCodeGenString* _stringLiteral5240;
extern "C" void Request_StartRequest_m8_900 (Request_t8_155 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Request_t8_155_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2089);
		GameObject_t6_97_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1770);
		GameObject_AddComponent_TisMonoBehaviour_t6_91_m6_1922_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484195);
		_stringLiteral5239 = il2cpp_codegen_string_literal_from_index(5239);
		_stringLiteral5240 = il2cpp_codegen_string_literal_from_index(5240);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t6_97 * V_0 = {0};
	URL_t8_156  V_1 = {0};
	{
		WWW_t6_78 * L_0 = Request_get_WWWObject_m8_898(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0023;
		}
	}
	{
		URL_t8_156  L_1 = Request_get_URL_m8_896(__this, /*hidden argument*/NULL);
		V_1 = L_1;
		String_t* L_2 = URL_get_URLString_m8_958((&V_1), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m1_521(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0039;
		}
	}

IL_0023:
	{
		Debug_LogError_m6_632(NULL /*static, unused*/, _stringLiteral5239, /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(5 /* System.Void VoxelBusters.Utility.Request::DidFailStartRequestWithError(System.String) */, __this, _stringLiteral5240);
		return;
	}

IL_0039:
	{
		bool L_4 = Request_get_IsAsynchronous_m8_894(__this, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0088;
		}
	}
	{
		MonoBehaviour_t6_91 * L_5 = ((Request_t8_155_StaticFields*)Request_t8_155_il2cpp_TypeInfo_var->static_fields)->___surrogateMonobehaviour_0;
		bool L_6 = Object_op_Equality_m6_721(NULL /*static, unused*/, L_5, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0072;
		}
	}
	{
		GameObject_t6_97 * L_7 = (GameObject_t6_97 *)il2cpp_codegen_object_new (GameObject_t6_97_il2cpp_TypeInfo_var);
		GameObject__ctor_m6_733(L_7, /*hidden argument*/NULL);
		V_0 = L_7;
		GameObject_t6_97 * L_8 = V_0;
		Object_DontDestroyOnLoad_m6_710(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		GameObject_t6_97 * L_9 = V_0;
		NullCheck(L_9);
		Object_set_hideFlags_m6_711(L_9, 1, /*hidden argument*/NULL);
		GameObject_t6_97 * L_10 = V_0;
		NullCheck(L_10);
		MonoBehaviour_t6_91 * L_11 = GameObject_AddComponent_TisMonoBehaviour_t6_91_m6_1922(L_10, /*hidden argument*/GameObject_AddComponent_TisMonoBehaviour_t6_91_m6_1922_MethodInfo_var);
		((Request_t8_155_StaticFields*)Request_t8_155_il2cpp_TypeInfo_var->static_fields)->___surrogateMonobehaviour_0 = L_11;
	}

IL_0072:
	{
		MonoBehaviour_t6_91 * L_12 = ((Request_t8_155_StaticFields*)Request_t8_155_il2cpp_TypeInfo_var->static_fields)->___surrogateMonobehaviour_0;
		Object_t * L_13 = Request_StartAsynchronousRequest_m8_901(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		MonoBehaviour_StartCoroutine_m6_672(L_12, L_13, /*hidden argument*/NULL);
		goto IL_00a3;
	}

IL_0088:
	{
		goto IL_008d;
	}

IL_008d:
	{
		WWW_t6_78 * L_14 = Request_get_WWWObject_m8_898(__this, /*hidden argument*/NULL);
		NullCheck(L_14);
		bool L_15 = WWW_get_isDone_m6_557(L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_008d;
		}
	}
	{
		VirtActionInvoker0::Invoke(4 /* System.Void VoxelBusters.Utility.Request::OnFetchingResponse() */, __this);
	}

IL_00a3:
	{
		return;
	}
}
// System.Collections.IEnumerator VoxelBusters.Utility.Request::StartAsynchronousRequest()
extern TypeInfo* U3CStartAsynchronousRequestU3Ec__Iterator4_t8_154_il2cpp_TypeInfo_var;
extern "C" Object_t * Request_StartAsynchronousRequest_m8_901 (Request_t8_155 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CStartAsynchronousRequestU3Ec__Iterator4_t8_154_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2090);
		s_Il2CppMethodIntialized = true;
	}
	U3CStartAsynchronousRequestU3Ec__Iterator4_t8_154 * V_0 = {0};
	{
		U3CStartAsynchronousRequestU3Ec__Iterator4_t8_154 * L_0 = (U3CStartAsynchronousRequestU3Ec__Iterator4_t8_154 *)il2cpp_codegen_object_new (U3CStartAsynchronousRequestU3Ec__Iterator4_t8_154_il2cpp_TypeInfo_var);
		U3CStartAsynchronousRequestU3Ec__Iterator4__ctor_m8_886(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CStartAsynchronousRequestU3Ec__Iterator4_t8_154 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3Ef__this_2 = __this;
		U3CStartAsynchronousRequestU3Ec__Iterator4_t8_154 * L_2 = V_0;
		return L_2;
	}
}
// System.Void VoxelBusters.Utility.Request::Abort()
extern "C" void Request_Abort_m8_902 (Request_t8_155 * __this, const MethodInfo* method)
{
	{
		WWW_t6_78 * L_0 = Request_get_WWWObject_m8_898(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		WWW_t6_78 * L_1 = Request_get_WWWObject_m8_898(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_2 = WWW_get_isDone_m6_557(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0026;
		}
	}
	{
		WWW_t6_78 * L_3 = Request_get_WWWObject_m8_898(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		WWW_Dispose_m6_544(L_3, /*hidden argument*/NULL);
	}

IL_0026:
	{
		return;
	}
}
// System.Void VoxelBusters.Utility.WebRequest/JSONResponse::.ctor(System.Object,System.IntPtr)
extern "C" void JSONResponse__ctor_m8_903 (JSONResponse_t8_157 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void VoxelBusters.Utility.WebRequest/JSONResponse::Invoke(System.Collections.IDictionary)
extern "C" void JSONResponse_Invoke_m8_904 (JSONResponse_t8_157 * __this, Object_t * ____response, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		JSONResponse_Invoke_m8_904((JSONResponse_t8_157 *)__this->___prev_9,____response, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, Object_t * ____response, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,____response,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, Object_t * ____response, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,____response,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(____response,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_JSONResponse_t8_157(Il2CppObject* delegate, Object_t * ____response)
{
	// Marshaling of parameter '____response' to native representation
	Object_t * _____response_marshaled = { 0 };
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Collections.IDictionary'."));
}
// System.IAsyncResult VoxelBusters.Utility.WebRequest/JSONResponse::BeginInvoke(System.Collections.IDictionary,System.AsyncCallback,System.Object)
extern "C" Object_t * JSONResponse_BeginInvoke_m8_905 (JSONResponse_t8_157 * __this, Object_t * ____response, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ____response;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void VoxelBusters.Utility.WebRequest/JSONResponse::EndInvoke(System.IAsyncResult)
extern "C" void JSONResponse_EndInvoke_m8_906 (JSONResponse_t8_157 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void VoxelBusters.Utility.WebRequest::.ctor(VoxelBusters.Utility.URL,System.Object,System.Boolean)
extern "C" void WebRequest__ctor_m8_907 (WebRequest_t8_158 * __this, URL_t8_156  ____URL, Object_t * ____params, bool ____isAsynchronous, const MethodInfo* method)
{
	{
		URL_t8_156  L_0 = ____URL;
		bool L_1 = ____isAsynchronous;
		Request__ctor_m8_893(__this, L_0, L_1, /*hidden argument*/NULL);
		Object_t * L_2 = ____params;
		WebRequest_set_Parameters_m8_909(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Object VoxelBusters.Utility.WebRequest::get_Parameters()
extern "C" Object_t * WebRequest_get_Parameters_m8_908 (WebRequest_t8_158 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U3CParametersU3Ek__BackingField_4);
		return L_0;
	}
}
// System.Void VoxelBusters.Utility.WebRequest::set_Parameters(System.Object)
extern "C" void WebRequest_set_Parameters_m8_909 (WebRequest_t8_158 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		__this->___U3CParametersU3Ek__BackingField_4 = L_0;
		return;
	}
}
// VoxelBusters.Utility.WebRequest/JSONResponse VoxelBusters.Utility.WebRequest::get_OnSuccess()
extern "C" JSONResponse_t8_157 * WebRequest_get_OnSuccess_m8_910 (WebRequest_t8_158 * __this, const MethodInfo* method)
{
	{
		JSONResponse_t8_157 * L_0 = (__this->___U3COnSuccessU3Ek__BackingField_5);
		return L_0;
	}
}
// System.Void VoxelBusters.Utility.WebRequest::set_OnSuccess(VoxelBusters.Utility.WebRequest/JSONResponse)
extern "C" void WebRequest_set_OnSuccess_m8_911 (WebRequest_t8_158 * __this, JSONResponse_t8_157 * ___value, const MethodInfo* method)
{
	{
		JSONResponse_t8_157 * L_0 = ___value;
		__this->___U3COnSuccessU3Ek__BackingField_5 = L_0;
		return;
	}
}
// VoxelBusters.Utility.WebRequest/JSONResponse VoxelBusters.Utility.WebRequest::get_OnFailure()
extern "C" JSONResponse_t8_157 * WebRequest_get_OnFailure_m8_912 (WebRequest_t8_158 * __this, const MethodInfo* method)
{
	{
		JSONResponse_t8_157 * L_0 = (__this->___U3COnFailureU3Ek__BackingField_6);
		return L_0;
	}
}
// System.Void VoxelBusters.Utility.WebRequest::set_OnFailure(VoxelBusters.Utility.WebRequest/JSONResponse)
extern "C" void WebRequest_set_OnFailure_m8_913 (WebRequest_t8_158 * __this, JSONResponse_t8_157 * ___value, const MethodInfo* method)
{
	{
		JSONResponse_t8_157 * L_0 = ___value;
		__this->___U3COnFailureU3Ek__BackingField_6 = L_0;
		return;
	}
}
// System.Void VoxelBusters.Utility.WebRequest::DidFailStartRequestWithError(System.String)
extern TypeInfo* Dictionary_2_t1_1839_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_14949_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral5241;
extern "C" void WebRequest_DidFailStartRequestWithError_m8_914 (WebRequest_t8_158 * __this, String_t* ____error, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t1_1839_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1603);
		Dictionary_2__ctor_m1_14949_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483789);
		_stringLiteral5241 = il2cpp_codegen_string_literal_from_index(5241);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	Dictionary_2_t1_1839 * V_1 = {0};
	{
		Dictionary_2_t1_1839 * L_0 = (Dictionary_2_t1_1839 *)il2cpp_codegen_object_new (Dictionary_2_t1_1839_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_14949(L_0, /*hidden argument*/Dictionary_2__ctor_m1_14949_MethodInfo_var);
		V_1 = L_0;
		Dictionary_2_t1_1839 * L_1 = V_1;
		String_t* L_2 = ____error;
		NullCheck(L_1);
		VirtActionInvoker2< String_t*, String_t* >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::Add(!0,!1) */, L_1, _stringLiteral5241, L_2);
		Dictionary_2_t1_1839 * L_3 = V_1;
		V_0 = L_3;
		JSONResponse_t8_157 * L_4 = WebRequest_get_OnFailure_m8_912(__this, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_002b;
		}
	}
	{
		JSONResponse_t8_157 * L_5 = WebRequest_get_OnFailure_m8_912(__this, /*hidden argument*/NULL);
		Object_t * L_6 = V_0;
		NullCheck(L_5);
		JSONResponse_Invoke_m8_904(L_5, L_6, /*hidden argument*/NULL);
	}

IL_002b:
	{
		return;
	}
}
// System.Void VoxelBusters.Utility.WebRequest::OnFetchingResponse()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionary_t1_35_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t1_1839_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_14949_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral5241;
extern "C" void WebRequest_OnFetchingResponse_m8_915 (WebRequest_t8_158 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		IDictionary_t1_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		Dictionary_2_t1_1839_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1603);
		Dictionary_2__ctor_m1_14949_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483789);
		_stringLiteral5241 = il2cpp_codegen_string_literal_from_index(5241);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	Object_t * V_1 = {0};
	Dictionary_2_t1_1839 * V_2 = {0};
	{
		WWW_t6_78 * L_0 = Request_get_WWWObject_m8_898(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = WWW_get_error_m6_554(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_IsNullOrEmpty_m1_521(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0047;
		}
	}
	{
		WWW_t6_78 * L_3 = Request_get_WWWObject_m8_898(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		String_t* L_4 = WWW_get_text_m6_550(L_3, /*hidden argument*/NULL);
		Object_t * L_5 = JSONUtility_FromJSON_m8_298(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_0 = ((Object_t *)IsInst(L_5, IDictionary_t1_35_il2cpp_TypeInfo_var));
		JSONResponse_t8_157 * L_6 = WebRequest_get_OnSuccess_m8_910(__this, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0042;
		}
	}
	{
		JSONResponse_t8_157 * L_7 = WebRequest_get_OnSuccess_m8_910(__this, /*hidden argument*/NULL);
		Object_t * L_8 = V_0;
		NullCheck(L_7);
		JSONResponse_Invoke_m8_904(L_7, L_8, /*hidden argument*/NULL);
	}

IL_0042:
	{
		goto IL_007c;
	}

IL_0047:
	{
		Dictionary_2_t1_1839 * L_9 = (Dictionary_2_t1_1839 *)il2cpp_codegen_object_new (Dictionary_2_t1_1839_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_14949(L_9, /*hidden argument*/Dictionary_2__ctor_m1_14949_MethodInfo_var);
		V_2 = L_9;
		Dictionary_2_t1_1839 * L_10 = V_2;
		WWW_t6_78 * L_11 = Request_get_WWWObject_m8_898(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		String_t* L_12 = WWW_get_error_m6_554(L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		VirtActionInvoker2< String_t*, String_t* >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::Add(!0,!1) */, L_10, _stringLiteral5241, L_12);
		Dictionary_2_t1_1839 * L_13 = V_2;
		V_1 = L_13;
		JSONResponse_t8_157 * L_14 = WebRequest_get_OnFailure_m8_912(__this, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_007c;
		}
	}
	{
		JSONResponse_t8_157 * L_15 = WebRequest_get_OnFailure_m8_912(__this, /*hidden argument*/NULL);
		Object_t * L_16 = V_1;
		NullCheck(L_15);
		JSONResponse_Invoke_m8_904(L_15, L_16, /*hidden argument*/NULL);
	}

IL_007c:
	{
		return;
	}
}
// System.Void VoxelBusters.Utility.DownloadTextAsset/Completion::.ctor(System.Object,System.IntPtr)
extern "C" void Completion__ctor_m8_916 (Completion_t8_159 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void VoxelBusters.Utility.DownloadTextAsset/Completion::Invoke(System.String,System.String)
extern "C" void Completion_Invoke_m8_917 (Completion_t8_159 * __this, String_t* ____text, String_t* ____error, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		Completion_Invoke_m8_917((Completion_t8_159 *)__this->___prev_9,____text, ____error, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, String_t* ____text, String_t* ____error, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,____text, ____error,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, String_t* ____text, String_t* ____error, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,____text, ____error,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, String_t* ____error, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(____text, ____error,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_Completion_t8_159(Il2CppObject* delegate, String_t* ____text, String_t* ____error)
{
	typedef void (STDCALL *native_function_ptr_type)(char*, char*);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '____text' to native representation
	char* _____text_marshaled = { 0 };
	_____text_marshaled = il2cpp_codegen_marshal_string(____text);

	// Marshaling of parameter '____error' to native representation
	char* _____error_marshaled = { 0 };
	_____error_marshaled = il2cpp_codegen_marshal_string(____error);

	// Native function invocation
	_il2cpp_pinvoke_func(_____text_marshaled, _____error_marshaled);

	// Marshaling cleanup of parameter '____text' native representation
	il2cpp_codegen_marshal_free(_____text_marshaled);
	_____text_marshaled = NULL;

	// Marshaling cleanup of parameter '____error' native representation
	il2cpp_codegen_marshal_free(_____error_marshaled);
	_____error_marshaled = NULL;

}
// System.IAsyncResult VoxelBusters.Utility.DownloadTextAsset/Completion::BeginInvoke(System.String,System.String,System.AsyncCallback,System.Object)
extern "C" Object_t * Completion_BeginInvoke_m8_918 (Completion_t8_159 * __this, String_t* ____text, String_t* ____error, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ____text;
	__d_args[1] = ____error;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void VoxelBusters.Utility.DownloadTextAsset/Completion::EndInvoke(System.IAsyncResult)
extern "C" void Completion_EndInvoke_m8_919 (Completion_t8_159 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void VoxelBusters.Utility.DownloadTextAsset::.ctor(VoxelBusters.Utility.URL,System.Boolean,System.Boolean)
extern TypeInfo* WWW_t6_78_il2cpp_TypeInfo_var;
extern "C" void DownloadTextAsset__ctor_m8_920 (DownloadTextAsset_t8_160 * __this, URL_t8_156  ____URL, bool ____isAsynchronous, bool ____autoFixOrientation, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WWW_t6_78_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1944);
		s_Il2CppMethodIntialized = true;
	}
	{
		URL_t8_156  L_0 = ____URL;
		bool L_1 = ____isAsynchronous;
		Request__ctor_m8_893(__this, L_0, L_1, /*hidden argument*/NULL);
		String_t* L_2 = URL_get_URLString_m8_958((&____URL), /*hidden argument*/NULL);
		WWW_t6_78 * L_3 = (WWW_t6_78 *)il2cpp_codegen_object_new (WWW_t6_78_il2cpp_TypeInfo_var);
		WWW__ctor_m6_541(L_3, L_2, /*hidden argument*/NULL);
		Request_set_WWWObject_m8_899(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// VoxelBusters.Utility.DownloadTextAsset/Completion VoxelBusters.Utility.DownloadTextAsset::get_OnCompletion()
extern "C" Completion_t8_159 * DownloadTextAsset_get_OnCompletion_m8_921 (DownloadTextAsset_t8_160 * __this, const MethodInfo* method)
{
	{
		Completion_t8_159 * L_0 = (__this->___U3COnCompletionU3Ek__BackingField_4);
		return L_0;
	}
}
// System.Void VoxelBusters.Utility.DownloadTextAsset::set_OnCompletion(VoxelBusters.Utility.DownloadTextAsset/Completion)
extern "C" void DownloadTextAsset_set_OnCompletion_m8_922 (DownloadTextAsset_t8_160 * __this, Completion_t8_159 * ___value, const MethodInfo* method)
{
	{
		Completion_t8_159 * L_0 = ___value;
		__this->___U3COnCompletionU3Ek__BackingField_4 = L_0;
		return;
	}
}
// System.Void VoxelBusters.Utility.DownloadTextAsset::DidFailStartRequestWithError(System.String)
extern "C" void DownloadTextAsset_DidFailStartRequestWithError_m8_923 (DownloadTextAsset_t8_160 * __this, String_t* ____error, const MethodInfo* method)
{
	{
		Completion_t8_159 * L_0 = DownloadTextAsset_get_OnCompletion_m8_921(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		Completion_t8_159 * L_1 = DownloadTextAsset_get_OnCompletion_m8_921(__this, /*hidden argument*/NULL);
		String_t* L_2 = ____error;
		NullCheck(L_1);
		Completion_Invoke_m8_917(L_1, (String_t*)NULL, L_2, /*hidden argument*/NULL);
	}

IL_0018:
	{
		return;
	}
}
// System.Void VoxelBusters.Utility.DownloadTextAsset::OnFetchingResponse()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5242;
extern "C" void DownloadTextAsset_OnFetchingResponse_m8_924 (DownloadTextAsset_t8_160 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5242 = il2cpp_codegen_string_literal_from_index(5242);
		s_Il2CppMethodIntialized = true;
	}
	{
		WWW_t6_78 * L_0 = Request_get_WWWObject_m8_898(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = WWW_get_error_m6_554(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m1_559(NULL /*static, unused*/, _stringLiteral5242, L_1, /*hidden argument*/NULL);
		Debug_Log_m6_631(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		WWW_t6_78 * L_3 = Request_get_WWWObject_m8_898(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		String_t* L_4 = WWW_get_error_m6_554(L_3, /*hidden argument*/NULL);
		bool L_5 = String_IsNullOrEmpty_m1_521(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0056;
		}
	}
	{
		Completion_t8_159 * L_6 = DownloadTextAsset_get_OnCompletion_m8_921(__this, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0051;
		}
	}
	{
		Completion_t8_159 * L_7 = DownloadTextAsset_get_OnCompletion_m8_921(__this, /*hidden argument*/NULL);
		WWW_t6_78 * L_8 = Request_get_WWWObject_m8_898(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		String_t* L_9 = WWW_get_text_m6_550(L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		Completion_Invoke_m8_917(L_7, L_9, (String_t*)NULL, /*hidden argument*/NULL);
	}

IL_0051:
	{
		goto IL_0078;
	}

IL_0056:
	{
		Completion_t8_159 * L_10 = DownloadTextAsset_get_OnCompletion_m8_921(__this, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0078;
		}
	}
	{
		Completion_t8_159 * L_11 = DownloadTextAsset_get_OnCompletion_m8_921(__this, /*hidden argument*/NULL);
		WWW_t6_78 * L_12 = Request_get_WWWObject_m8_898(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		String_t* L_13 = WWW_get_error_m6_554(L_12, /*hidden argument*/NULL);
		NullCheck(L_11);
		Completion_Invoke_m8_917(L_11, (String_t*)NULL, L_13, /*hidden argument*/NULL);
	}

IL_0078:
	{
		return;
	}
}
// System.Void VoxelBusters.Utility.DownloadTexture/Completion::.ctor(System.Object,System.IntPtr)
extern "C" void Completion__ctor_m8_925 (Completion_t8_161 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void VoxelBusters.Utility.DownloadTexture/Completion::Invoke(UnityEngine.Texture2D,System.String)
extern "C" void Completion_Invoke_m8_926 (Completion_t8_161 * __this, Texture2D_t6_33 * ____texture, String_t* ____error, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		Completion_Invoke_m8_926((Completion_t8_161 *)__this->___prev_9,____texture, ____error, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, Texture2D_t6_33 * ____texture, String_t* ____error, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,____texture, ____error,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, Texture2D_t6_33 * ____texture, String_t* ____error, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,____texture, ____error,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, String_t* ____error, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(____texture, ____error,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_Completion_t8_161(Il2CppObject* delegate, Texture2D_t6_33 * ____texture, String_t* ____error)
{
	// Marshaling of parameter '____texture' to native representation
	Texture2D_t6_33 * _____texture_marshaled = { 0 };
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'UnityEngine.Texture2D'."));
}
// System.IAsyncResult VoxelBusters.Utility.DownloadTexture/Completion::BeginInvoke(UnityEngine.Texture2D,System.String,System.AsyncCallback,System.Object)
extern "C" Object_t * Completion_BeginInvoke_m8_927 (Completion_t8_161 * __this, Texture2D_t6_33 * ____texture, String_t* ____error, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ____texture;
	__d_args[1] = ____error;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void VoxelBusters.Utility.DownloadTexture/Completion::EndInvoke(System.IAsyncResult)
extern "C" void Completion_EndInvoke_m8_928 (Completion_t8_161 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void VoxelBusters.Utility.DownloadTexture::.ctor(VoxelBusters.Utility.URL,System.Boolean,System.Boolean)
extern TypeInfo* WWW_t6_78_il2cpp_TypeInfo_var;
extern "C" void DownloadTexture__ctor_m8_929 (DownloadTexture_t8_162 * __this, URL_t8_156  ____URL, bool ____isAsynchronous, bool ____autoFixOrientation, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WWW_t6_78_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1944);
		s_Il2CppMethodIntialized = true;
	}
	{
		URL_t8_156  L_0 = ____URL;
		bool L_1 = ____isAsynchronous;
		Request__ctor_m8_893(__this, L_0, L_1, /*hidden argument*/NULL);
		bool L_2 = ____autoFixOrientation;
		DownloadTexture_set_AutoFixOrientation_m8_931(__this, L_2, /*hidden argument*/NULL);
		String_t* L_3 = URL_get_URLString_m8_958((&____URL), /*hidden argument*/NULL);
		WWW_t6_78 * L_4 = (WWW_t6_78 *)il2cpp_codegen_object_new (WWW_t6_78_il2cpp_TypeInfo_var);
		WWW__ctor_m6_541(L_4, L_3, /*hidden argument*/NULL);
		Request_set_WWWObject_m8_899(__this, L_4, /*hidden argument*/NULL);
		DownloadTexture_set_ScaleFactor_m8_933(__this, (1.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean VoxelBusters.Utility.DownloadTexture::get_AutoFixOrientation()
extern "C" bool DownloadTexture_get_AutoFixOrientation_m8_930 (DownloadTexture_t8_162 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___U3CAutoFixOrientationU3Ek__BackingField_4);
		return L_0;
	}
}
// System.Void VoxelBusters.Utility.DownloadTexture::set_AutoFixOrientation(System.Boolean)
extern "C" void DownloadTexture_set_AutoFixOrientation_m8_931 (DownloadTexture_t8_162 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___U3CAutoFixOrientationU3Ek__BackingField_4 = L_0;
		return;
	}
}
// System.Single VoxelBusters.Utility.DownloadTexture::get_ScaleFactor()
extern "C" float DownloadTexture_get_ScaleFactor_m8_932 (DownloadTexture_t8_162 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___U3CScaleFactorU3Ek__BackingField_5);
		return L_0;
	}
}
// System.Void VoxelBusters.Utility.DownloadTexture::set_ScaleFactor(System.Single)
extern "C" void DownloadTexture_set_ScaleFactor_m8_933 (DownloadTexture_t8_162 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___U3CScaleFactorU3Ek__BackingField_5 = L_0;
		return;
	}
}
// VoxelBusters.Utility.DownloadTexture/Completion VoxelBusters.Utility.DownloadTexture::get_OnCompletion()
extern "C" Completion_t8_161 * DownloadTexture_get_OnCompletion_m8_934 (DownloadTexture_t8_162 * __this, const MethodInfo* method)
{
	{
		Completion_t8_161 * L_0 = (__this->___U3COnCompletionU3Ek__BackingField_6);
		return L_0;
	}
}
// System.Void VoxelBusters.Utility.DownloadTexture::set_OnCompletion(VoxelBusters.Utility.DownloadTexture/Completion)
extern "C" void DownloadTexture_set_OnCompletion_m8_935 (DownloadTexture_t8_162 * __this, Completion_t8_161 * ___value, const MethodInfo* method)
{
	{
		Completion_t8_161 * L_0 = ___value;
		__this->___U3COnCompletionU3Ek__BackingField_6 = L_0;
		return;
	}
}
// System.Void VoxelBusters.Utility.DownloadTexture::DidFailStartRequestWithError(System.String)
extern "C" void DownloadTexture_DidFailStartRequestWithError_m8_936 (DownloadTexture_t8_162 * __this, String_t* ____error, const MethodInfo* method)
{
	{
		Completion_t8_161 * L_0 = DownloadTexture_get_OnCompletion_m8_934(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		Completion_t8_161 * L_1 = DownloadTexture_get_OnCompletion_m8_934(__this, /*hidden argument*/NULL);
		String_t* L_2 = ____error;
		NullCheck(L_1);
		Completion_Invoke_m8_926(L_1, (Texture2D_t6_33 *)NULL, L_2, /*hidden argument*/NULL);
	}

IL_0018:
	{
		return;
	}
}
// System.Void VoxelBusters.Utility.DownloadTexture::OnFetchingResponse()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* MemoryStream_t1_433_il2cpp_TypeInfo_var;
extern TypeInfo* UInt16_t1_14_il2cpp_TypeInfo_var;
extern TypeInfo* Orientation_t8_65_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5243;
extern Il2CppCodeGenString* _stringLiteral56;
extern Il2CppCodeGenString* _stringLiteral5244;
extern "C" void DownloadTexture_OnFetchingResponse_m8_937 (DownloadTexture_t8_162 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		MemoryStream_t1_433_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(390);
		UInt16_t1_14_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		Orientation_t8_65_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1993);
		_stringLiteral5243 = il2cpp_codegen_string_literal_from_index(5243);
		_stringLiteral56 = il2cpp_codegen_string_literal_from_index(56);
		_stringLiteral5244 = il2cpp_codegen_string_literal_from_index(5244);
		s_Il2CppMethodIntialized = true;
	}
	Texture2D_t6_33 * V_0 = {0};
	Texture2D_t6_33 * V_1 = {0};
	Stream_t1_405 * V_2 = {0};
	ExifFile_t8_110 * V_3 = {0};
	uint16_t V_4 = {0};
	uint16_t V_5 = {0};
	{
		V_0 = (Texture2D_t6_33 *)NULL;
		WWW_t6_78 * L_0 = Request_get_WWWObject_m8_898(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = WWW_get_error_m6_554(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_IsNullOrEmpty_m1_521(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0059;
		}
	}
	{
		WWW_t6_78 * L_3 = Request_get_WWWObject_m8_898(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		String_t* L_4 = WWW_get_error_m6_554(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m1_560(NULL /*static, unused*/, _stringLiteral5243, L_4, _stringLiteral56, /*hidden argument*/NULL);
		Debug_Log_m6_631(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		Completion_t8_161 * L_6 = DownloadTexture_get_OnCompletion_m8_934(__this, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0059;
		}
	}
	{
		Completion_t8_161 * L_7 = DownloadTexture_get_OnCompletion_m8_934(__this, /*hidden argument*/NULL);
		WWW_t6_78 * L_8 = Request_get_WWWObject_m8_898(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		String_t* L_9 = WWW_get_error_m6_554(L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		Completion_Invoke_m8_926(L_7, (Texture2D_t6_33 *)NULL, L_9, /*hidden argument*/NULL);
		return;
	}

IL_0059:
	{
		WWW_t6_78 * L_10 = Request_get_WWWObject_m8_898(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		Texture2D_t6_33 * L_11 = WWW_get_texture_m6_556(L_10, /*hidden argument*/NULL);
		V_1 = L_11;
		bool L_12 = DownloadTexture_get_AutoFixOrientation_m8_930(__this, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_01a3;
		}
	}
	{
		WWW_t6_78 * L_13 = Request_get_WWWObject_m8_898(__this, /*hidden argument*/NULL);
		NullCheck(L_13);
		ByteU5BU5D_t1_109* L_14 = WWW_get_bytes_m6_553(L_13, /*hidden argument*/NULL);
		MemoryStream_t1_433 * L_15 = (MemoryStream_t1_433 *)il2cpp_codegen_object_new (MemoryStream_t1_433_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m1_5046(L_15, L_14, /*hidden argument*/NULL);
		V_2 = L_15;
		Stream_t1_405 * L_16 = V_2;
		ExifFile_t8_110 * L_17 = ExifFile_Read_m8_493(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		V_3 = L_17;
		Texture2D_t6_33 * L_18 = V_1;
		float L_19 = DownloadTexture_get_ScaleFactor_m8_932(__this, /*hidden argument*/NULL);
		Texture2D_t6_33 * L_20 = TextureExtensions_Scale_m8_243(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		V_1 = L_20;
		ExifFile_t8_110 * L_21 = V_3;
		if (!L_21)
		{
			goto IL_019c;
		}
	}
	{
		ExifFile_t8_110 * L_22 = V_3;
		NullCheck(L_22);
		Dictionary_2_t1_1904 * L_23 = ExifFile_get_Properties_m8_479(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		bool L_24 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::ContainsKey(!0) */, L_23, ((int32_t)100274));
		if (!L_24)
		{
			goto IL_019c;
		}
	}
	{
		ExifFile_t8_110 * L_25 = V_3;
		NullCheck(L_25);
		Dictionary_2_t1_1904 * L_26 = ExifFile_get_Properties_m8_479(L_25, /*hidden argument*/NULL);
		NullCheck(L_26);
		ExifProperty_t8_99 * L_27 = (ExifProperty_t8_99 *)VirtFuncInvoker1< ExifProperty_t8_99 *, int32_t >::Invoke(22 /* !1 System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::get_Item(!0) */, L_26, ((int32_t)100274));
		NullCheck(L_27);
		Object_t * L_28 = ExifProperty_get_Value_m8_507(L_27, /*hidden argument*/NULL);
		V_4 = ((*(uint16_t*)((uint16_t*)UnBox (L_28, UInt16_t1_14_il2cpp_TypeInfo_var))));
		uint16_t L_29 = V_4;
		uint16_t L_30 = L_29;
		Object_t * L_31 = Box(Orientation_t8_65_il2cpp_TypeInfo_var, &L_30);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m1_556(NULL /*static, unused*/, _stringLiteral5244, L_31, /*hidden argument*/NULL);
		Debug_Log_m6_631(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		uint16_t L_33 = V_4;
		V_5 = L_33;
		uint16_t L_34 = V_5;
		if (((int32_t)((int32_t)L_34-(int32_t)1)) == 0)
		{
			goto IL_0114;
		}
		if (((int32_t)((int32_t)L_34-(int32_t)1)) == 1)
		{
			goto IL_011b;
		}
		if (((int32_t)((int32_t)L_34-(int32_t)1)) == 2)
		{
			goto IL_0129;
		}
		if (((int32_t)((int32_t)L_34-(int32_t)1)) == 3)
		{
			goto IL_0137;
		}
		if (((int32_t)((int32_t)L_34-(int32_t)1)) == 4)
		{
			goto IL_0145;
		}
		if (((int32_t)((int32_t)L_34-(int32_t)1)) == 5)
		{
			goto IL_015d;
		}
		if (((int32_t)((int32_t)L_34-(int32_t)1)) == 6)
		{
			goto IL_016e;
		}
		if (((int32_t)((int32_t)L_34-(int32_t)1)) == 7)
		{
			goto IL_0186;
		}
	}
	{
		goto IL_0197;
	}

IL_0114:
	{
		Texture2D_t6_33 * L_35 = V_1;
		V_0 = L_35;
		goto IL_0197;
	}

IL_011b:
	{
		Texture2D_t6_33 * L_36 = V_1;
		Texture2D_t6_33 * L_37 = TextureExtensions_MirrorTexture_m8_242(NULL /*static, unused*/, L_36, 1, 0, /*hidden argument*/NULL);
		V_0 = L_37;
		goto IL_0197;
	}

IL_0129:
	{
		Texture2D_t6_33 * L_38 = V_1;
		Texture2D_t6_33 * L_39 = TextureExtensions_MirrorTexture_m8_242(NULL /*static, unused*/, L_38, 1, 1, /*hidden argument*/NULL);
		V_0 = L_39;
		goto IL_0197;
	}

IL_0137:
	{
		Texture2D_t6_33 * L_40 = V_1;
		Texture2D_t6_33 * L_41 = TextureExtensions_MirrorTexture_m8_242(NULL /*static, unused*/, L_40, 0, 1, /*hidden argument*/NULL);
		V_0 = L_41;
		goto IL_0197;
	}

IL_0145:
	{
		Texture2D_t6_33 * L_42 = V_1;
		Texture2D_t6_33 * L_43 = TextureExtensions_MirrorTexture_m8_242(NULL /*static, unused*/, L_42, 1, 0, /*hidden argument*/NULL);
		Texture2D_t6_33 * L_44 = TextureExtensions_Rotate_m8_240(NULL /*static, unused*/, L_43, (-90.0f), /*hidden argument*/NULL);
		V_0 = L_44;
		goto IL_0197;
	}

IL_015d:
	{
		Texture2D_t6_33 * L_45 = V_1;
		Texture2D_t6_33 * L_46 = TextureExtensions_Rotate_m8_240(NULL /*static, unused*/, L_45, (90.0f), /*hidden argument*/NULL);
		V_0 = L_46;
		goto IL_0197;
	}

IL_016e:
	{
		Texture2D_t6_33 * L_47 = V_1;
		Texture2D_t6_33 * L_48 = TextureExtensions_MirrorTexture_m8_242(NULL /*static, unused*/, L_47, 0, 1, /*hidden argument*/NULL);
		Texture2D_t6_33 * L_49 = TextureExtensions_Rotate_m8_240(NULL /*static, unused*/, L_48, (-90.0f), /*hidden argument*/NULL);
		V_0 = L_49;
		goto IL_0197;
	}

IL_0186:
	{
		Texture2D_t6_33 * L_50 = V_1;
		Texture2D_t6_33 * L_51 = TextureExtensions_Rotate_m8_240(NULL /*static, unused*/, L_50, (-90.0f), /*hidden argument*/NULL);
		V_0 = L_51;
		goto IL_0197;
	}

IL_0197:
	{
		goto IL_019e;
	}

IL_019c:
	{
		Texture2D_t6_33 * L_52 = V_1;
		V_0 = L_52;
	}

IL_019e:
	{
		goto IL_01b2;
	}

IL_01a3:
	{
		Texture2D_t6_33 * L_53 = V_1;
		float L_54 = DownloadTexture_get_ScaleFactor_m8_932(__this, /*hidden argument*/NULL);
		Texture2D_t6_33 * L_55 = TextureExtensions_Scale_m8_243(NULL /*static, unused*/, L_53, L_54, /*hidden argument*/NULL);
		V_1 = L_55;
		Texture2D_t6_33 * L_56 = V_1;
		V_0 = L_56;
	}

IL_01b2:
	{
		Completion_t8_161 * L_57 = DownloadTexture_get_OnCompletion_m8_934(__this, /*hidden argument*/NULL);
		if (!L_57)
		{
			goto IL_01ca;
		}
	}
	{
		Completion_t8_161 * L_58 = DownloadTexture_get_OnCompletion_m8_934(__this, /*hidden argument*/NULL);
		Texture2D_t6_33 * L_59 = V_0;
		NullCheck(L_58);
		Completion_Invoke_m8_926(L_58, L_59, (String_t*)NULL, /*hidden argument*/NULL);
	}

IL_01ca:
	{
		return;
	}
}
// System.Void VoxelBusters.Utility.GETRequest::.ctor(VoxelBusters.Utility.URL,System.Object,System.Boolean)
extern "C" void GETRequest__ctor_m8_938 (GETRequest_t8_23 * __this, URL_t8_156  ____URL, Object_t * ____params, bool ____isAsynchronous, const MethodInfo* method)
{
	{
		URL_t8_156  L_0 = ____URL;
		Object_t * L_1 = ____params;
		bool L_2 = ____isAsynchronous;
		WebRequest__ctor_m8_907(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		WWW_t6_78 * L_3 = (WWW_t6_78 *)VirtFuncInvoker0< WWW_t6_78 * >::Invoke(6 /* UnityEngine.WWW VoxelBusters.Utility.GETRequest::CreateWWWObject() */, __this);
		Request_set_WWWObject_m8_899(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.WWW VoxelBusters.Utility.GETRequest::CreateWWWObject()
extern const Il2CppType* String_t_0_0_0_var;
extern TypeInfo* WWW_t6_78_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionary_t1_35_il2cpp_TypeInfo_var;
extern TypeInfo* ICollection_t1_280_il2cpp_TypeInfo_var;
extern TypeInfo* StringBuilder_t1_247_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral740;
extern Il2CppCodeGenString* _stringLiteral5245;
extern "C" WWW_t6_78 * GETRequest_CreateWWWObject_m8_939 (GETRequest_t8_23 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_0_0_0_var = il2cpp_codegen_type_from_index(14);
		WWW_t6_78_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1944);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		IDictionary_t1_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		ICollection_t1_280_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(149);
		StringBuilder_t1_247_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		_stringLiteral740 = il2cpp_codegen_string_literal_from_index(740);
		_stringLiteral5245 = il2cpp_codegen_string_literal_from_index(5245);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	String_t* V_1 = {0};
	int32_t V_2 = 0;
	StringBuilder_t1_247 * V_3 = {0};
	int32_t V_4 = 0;
	URL_t8_156  V_5 = {0};
	{
		V_0 = (Object_t *)NULL;
		URL_t8_156  L_0 = Request_get_URL_m8_896(__this, /*hidden argument*/NULL);
		V_5 = L_0;
		String_t* L_1 = URL_get_URLString_m8_958((&V_5), /*hidden argument*/NULL);
		V_1 = L_1;
		Object_t * L_2 = WebRequest_get_Parameters_m8_908(__this, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0024;
		}
	}
	{
		String_t* L_3 = V_1;
		WWW_t6_78 * L_4 = (WWW_t6_78 *)il2cpp_codegen_object_new (WWW_t6_78_il2cpp_TypeInfo_var);
		WWW__ctor_m6_541(L_4, L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_0024:
	{
		Object_t * L_5 = WebRequest_get_Parameters_m8_908(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Type_t * L_6 = Object_GetType_m1_5(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_6) == ((Object_t*)(Type_t *)L_7))))
		{
			goto IL_005a;
		}
	}
	{
		String_t* L_8 = V_1;
		Object_t * L_9 = WebRequest_get_Parameters_m8_908(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m1_560(NULL /*static, unused*/, L_8, _stringLiteral740, ((String_t*)IsInstSealed(L_9, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		WWW_t6_78 * L_11 = (WWW_t6_78 *)il2cpp_codegen_object_new (WWW_t6_78_il2cpp_TypeInfo_var);
		WWW__ctor_m6_541(L_11, L_10, /*hidden argument*/NULL);
		return L_11;
	}

IL_005a:
	{
		Object_t * L_12 = WebRequest_get_Parameters_m8_908(__this, /*hidden argument*/NULL);
		Object_t * L_13 = ((Object_t *)IsInst(L_12, IDictionary_t1_35_il2cpp_TypeInfo_var));
		V_0 = L_13;
		if (!L_13)
		{
			goto IL_00a7;
		}
	}
	{
		Object_t * L_14 = V_0;
		NullCheck(L_14);
		int32_t L_15 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.ICollection::get_Count() */, ICollection_t1_280_il2cpp_TypeInfo_var, L_14);
		V_2 = L_15;
		int32_t L_16 = V_2;
		if (L_16)
		{
			goto IL_0080;
		}
	}
	{
		String_t* L_17 = V_1;
		WWW_t6_78 * L_18 = (WWW_t6_78 *)il2cpp_codegen_object_new (WWW_t6_78_il2cpp_TypeInfo_var);
		WWW__ctor_m6_541(L_18, L_17, /*hidden argument*/NULL);
		return L_18;
	}

IL_0080:
	{
		String_t* L_19 = V_1;
		String_t* L_20 = V_1;
		NullCheck(L_20);
		int32_t L_21 = String_get_Length_m1_571(L_20, /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_22 = (StringBuilder_t1_247 *)il2cpp_codegen_object_new (StringBuilder_t1_247_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1_12418(L_22, L_19, L_21, /*hidden argument*/NULL);
		V_3 = L_22;
		V_4 = 0;
		Object_t * L_23 = V_0;
		StringBuilder_t1_247 * L_24 = V_3;
		GETRequest_AppendParameters_m8_940(__this, (String_t*)NULL, L_23, L_24, (&V_4), /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_25 = V_3;
		NullCheck(L_25);
		String_t* L_26 = StringBuilder_ToString_m1_12428(L_25, /*hidden argument*/NULL);
		WWW_t6_78 * L_27 = (WWW_t6_78 *)il2cpp_codegen_object_new (WWW_t6_78_il2cpp_TypeInfo_var);
		WWW__ctor_m6_541(L_27, L_26, /*hidden argument*/NULL);
		return L_27;
	}

IL_00a7:
	{
		Debug_LogError_m6_632(NULL /*static, unused*/, _stringLiteral5245, /*hidden argument*/NULL);
		return (WWW_t6_78 *)NULL;
	}
}
// System.Void VoxelBusters.Utility.GETRequest::AppendParameters(System.String,System.Object,System.Text.StringBuilder,System.Int32&)
extern const Il2CppType* String_t_0_0_0_var;
extern TypeInfo* IDictionary_t1_35_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionaryEnumerator_t1_863_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t1_193_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerable_t1_1677_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5246;
extern Il2CppCodeGenString* _stringLiteral2470;
extern Il2CppCodeGenString* _stringLiteral5247;
extern Il2CppCodeGenString* _stringLiteral5248;
extern "C" void GETRequest_AppendParameters_m8_940 (GETRequest_t8_23 * __this, String_t* ____key, Object_t * ____value, StringBuilder_t1_247 * ____urlBuilder, int32_t* ____paramAdded, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_0_0_0_var = il2cpp_codegen_type_from_index(14);
		IDictionary_t1_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		IDictionaryEnumerator_t1_863_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(227);
		IEnumerator_t1_193_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(144);
		IEnumerable_t1_1677_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(253);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		_stringLiteral5246 = il2cpp_codegen_string_literal_from_index(5246);
		_stringLiteral2470 = il2cpp_codegen_string_literal_from_index(2470);
		_stringLiteral5247 = il2cpp_codegen_string_literal_from_index(5247);
		_stringLiteral5248 = il2cpp_codegen_string_literal_from_index(5248);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	Object_t * V_1 = {0};
	Object_t * V_2 = {0};
	String_t* V_3 = {0};
	Object_t * V_4 = {0};
	Object_t * V_5 = {0};
	String_t* V_6 = {0};
	Object_t * V_7 = {0};
	Object_t * V_8 = {0};
	Object_t * V_9 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Object_t * L_0 = ____value;
		if (!((Object_t *)IsInst(L_0, IDictionary_t1_35_il2cpp_TypeInfo_var)))
		{
			goto IL_0090;
		}
	}
	{
		Object_t * L_1 = ____value;
		NullCheck(((Object_t *)IsInst(L_1, IDictionary_t1_35_il2cpp_TypeInfo_var)));
		Object_t * L_2 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(9 /* System.Collections.IDictionaryEnumerator System.Collections.IDictionary::GetEnumerator() */, IDictionary_t1_35_il2cpp_TypeInfo_var, ((Object_t *)IsInst(L_1, IDictionary_t1_35_il2cpp_TypeInfo_var)));
		V_0 = L_2;
		String_t* L_3 = ____key;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_IsNullOrEmpty_m1_521(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0055;
		}
	}
	{
		goto IL_0045;
	}

IL_0027:
	{
		Object_t * L_5 = V_0;
		NullCheck(L_5);
		Object_t * L_6 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(1 /* System.Object System.Collections.IDictionaryEnumerator::get_Key() */, IDictionaryEnumerator_t1_863_il2cpp_TypeInfo_var, L_5);
		V_1 = L_6;
		Object_t * L_7 = V_0;
		NullCheck(L_7);
		Object_t * L_8 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(2 /* System.Object System.Collections.IDictionaryEnumerator::get_Value() */, IDictionaryEnumerator_t1_863_il2cpp_TypeInfo_var, L_7);
		V_2 = L_8;
		Object_t * L_9 = V_1;
		NullCheck(L_9);
		String_t* L_10 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_9);
		Object_t * L_11 = V_2;
		StringBuilder_t1_247 * L_12 = ____urlBuilder;
		int32_t* L_13 = ____paramAdded;
		GETRequest_AppendParameters_m8_940(__this, L_10, L_11, L_12, L_13, /*hidden argument*/NULL);
	}

IL_0045:
	{
		Object_t * L_14 = V_0;
		NullCheck(L_14);
		bool L_15 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_14);
		if (L_15)
		{
			goto IL_0027;
		}
	}
	{
		goto IL_008f;
	}

IL_0055:
	{
		String_t* L_16 = ____key;
		V_3 = L_16;
		goto IL_0084;
	}

IL_005c:
	{
		Object_t * L_17 = V_0;
		NullCheck(L_17);
		Object_t * L_18 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(1 /* System.Object System.Collections.IDictionaryEnumerator::get_Key() */, IDictionaryEnumerator_t1_863_il2cpp_TypeInfo_var, L_17);
		V_4 = L_18;
		Object_t * L_19 = V_0;
		NullCheck(L_19);
		Object_t * L_20 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(2 /* System.Object System.Collections.IDictionaryEnumerator::get_Value() */, IDictionaryEnumerator_t1_863_il2cpp_TypeInfo_var, L_19);
		V_5 = L_20;
		String_t* L_21 = V_3;
		Object_t * L_22 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_23 = String_Format_m1_549(NULL /*static, unused*/, _stringLiteral5246, L_21, L_22, /*hidden argument*/NULL);
		Object_t * L_24 = V_5;
		StringBuilder_t1_247 * L_25 = ____urlBuilder;
		int32_t* L_26 = ____paramAdded;
		GETRequest_AppendParameters_m8_940(__this, L_23, L_24, L_25, L_26, /*hidden argument*/NULL);
	}

IL_0084:
	{
		Object_t * L_27 = V_0;
		NullCheck(L_27);
		bool L_28 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_27);
		if (L_28)
		{
			goto IL_005c;
		}
	}

IL_008f:
	{
		return;
	}

IL_0090:
	{
		Object_t * L_29 = ____value;
		if (!((Object_t *)IsInst(L_29, IEnumerable_t1_1677_il2cpp_TypeInfo_var)))
		{
			goto IL_010d;
		}
	}
	{
		Object_t * L_30 = ____value;
		NullCheck(L_30);
		Type_t * L_31 = Object_GetType_m1_5(L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_32 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_31) == ((Object_t*)(Type_t *)L_32)))
		{
			goto IL_010d;
		}
	}
	{
		String_t* L_33 = ____key;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_34 = String_Concat_m1_559(NULL /*static, unused*/, L_33, _stringLiteral2470, /*hidden argument*/NULL);
		V_6 = L_34;
		Object_t * L_35 = ____value;
		NullCheck(((Object_t *)IsInst(L_35, IEnumerable_t1_1677_il2cpp_TypeInfo_var)));
		Object_t * L_36 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t1_1677_il2cpp_TypeInfo_var, ((Object_t *)IsInst(L_35, IEnumerable_t1_1677_il2cpp_TypeInfo_var)));
		V_8 = L_36;
	}

IL_00ca:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00e5;
		}

IL_00cf:
		{
			Object_t * L_37 = V_8;
			NullCheck(L_37);
			Object_t * L_38 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_37);
			V_7 = L_38;
			String_t* L_39 = V_6;
			Object_t * L_40 = V_7;
			StringBuilder_t1_247 * L_41 = ____urlBuilder;
			int32_t* L_42 = ____paramAdded;
			GETRequest_AppendParameters_m8_940(__this, L_39, L_40, L_41, L_42, /*hidden argument*/NULL);
		}

IL_00e5:
		{
			Object_t * L_43 = V_8;
			NullCheck(L_43);
			bool L_44 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_43);
			if (L_44)
			{
				goto IL_00cf;
			}
		}

IL_00f1:
		{
			IL2CPP_LEAVE(0x10C, FINALLY_00f6);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_00f6;
	}

FINALLY_00f6:
	{ // begin finally (depth: 1)
		{
			Object_t * L_45 = V_8;
			V_9 = ((Object_t *)IsInst(L_45, IDisposable_t1_1035_il2cpp_TypeInfo_var));
			Object_t * L_46 = V_9;
			if (L_46)
			{
				goto IL_0104;
			}
		}

IL_0103:
		{
			IL2CPP_END_FINALLY(246)
		}

IL_0104:
		{
			Object_t * L_47 = V_9;
			NullCheck(L_47);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_47);
			IL2CPP_END_FINALLY(246)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(246)
	{
		IL2CPP_JUMP_TBL(0x10C, IL_010c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_010c:
	{
		return;
	}

IL_010d:
	{
		int32_t* L_48 = ____paramAdded;
		if ((*((int32_t*)L_48)))
		{
			goto IL_0128;
		}
	}
	{
		StringBuilder_t1_247 * L_49 = ____urlBuilder;
		String_t* L_50 = ____key;
		Object_t * L_51 = ____value;
		NullCheck(L_49);
		StringBuilder_AppendFormat_m1_12461(L_49, _stringLiteral5247, L_50, L_51, /*hidden argument*/NULL);
		goto IL_0136;
	}

IL_0128:
	{
		StringBuilder_t1_247 * L_52 = ____urlBuilder;
		String_t* L_53 = ____key;
		Object_t * L_54 = ____value;
		NullCheck(L_52);
		StringBuilder_AppendFormat_m1_12461(L_52, _stringLiteral5248, L_53, L_54, /*hidden argument*/NULL);
	}

IL_0136:
	{
		int32_t* L_55 = ____paramAdded;
		int32_t* L_56 = ____paramAdded;
		*((int32_t*)(L_55)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_56))+(int32_t)1));
		return;
	}
}
// VoxelBusters.Utility.GETRequest VoxelBusters.Utility.GETRequest::CreateRequest(VoxelBusters.Utility.URL,System.Object)
extern TypeInfo* GETRequest_t8_23_il2cpp_TypeInfo_var;
extern "C" GETRequest_t8_23 * GETRequest_CreateRequest_m8_941 (Object_t * __this /* static, unused */, URL_t8_156  ____URL, Object_t * ____params, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GETRequest_t8_23_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2091);
		s_Il2CppMethodIntialized = true;
	}
	GETRequest_t8_23 * V_0 = {0};
	{
		URL_t8_156  L_0 = ____URL;
		Object_t * L_1 = ____params;
		GETRequest_t8_23 * L_2 = (GETRequest_t8_23 *)il2cpp_codegen_object_new (GETRequest_t8_23_il2cpp_TypeInfo_var);
		GETRequest__ctor_m8_938(L_2, L_0, L_1, 0, /*hidden argument*/NULL);
		V_0 = L_2;
		GETRequest_t8_23 * L_3 = V_0;
		return L_3;
	}
}
// VoxelBusters.Utility.GETRequest VoxelBusters.Utility.GETRequest::CreateAsyncRequest(VoxelBusters.Utility.URL,System.Object)
extern TypeInfo* GETRequest_t8_23_il2cpp_TypeInfo_var;
extern "C" GETRequest_t8_23 * GETRequest_CreateAsyncRequest_m8_942 (Object_t * __this /* static, unused */, URL_t8_156  ____URL, Object_t * ____params, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GETRequest_t8_23_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2091);
		s_Il2CppMethodIntialized = true;
	}
	GETRequest_t8_23 * V_0 = {0};
	{
		URL_t8_156  L_0 = ____URL;
		Object_t * L_1 = ____params;
		GETRequest_t8_23 * L_2 = (GETRequest_t8_23 *)il2cpp_codegen_object_new (GETRequest_t8_23_il2cpp_TypeInfo_var);
		GETRequest__ctor_m8_938(L_2, L_0, L_1, 1, /*hidden argument*/NULL);
		V_0 = L_2;
		GETRequest_t8_23 * L_3 = V_0;
		return L_3;
	}
}
// System.Void VoxelBusters.Utility.POSTRequest::.ctor(VoxelBusters.Utility.URL,System.Object,System.Boolean)
extern "C" void POSTRequest__ctor_m8_943 (POSTRequest_t8_163 * __this, URL_t8_156  ____URL, Object_t * ____params, bool ____isAsynchronous, const MethodInfo* method)
{
	{
		URL_t8_156  L_0 = ____URL;
		Object_t * L_1 = ____params;
		bool L_2 = ____isAsynchronous;
		WebRequest__ctor_m8_907(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		WWW_t6_78 * L_3 = (WWW_t6_78 *)VirtFuncInvoker0< WWW_t6_78 * >::Invoke(6 /* UnityEngine.WWW VoxelBusters.Utility.POSTRequest::CreateWWWObject() */, __this);
		Request_set_WWWObject_m8_899(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.WWW VoxelBusters.Utility.POSTRequest::CreateWWWObject()
extern TypeInfo* WWW_t6_78_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionary_t1_35_il2cpp_TypeInfo_var;
extern TypeInfo* Encoding_t1_406_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5249;
extern Il2CppCodeGenString* _stringLiteral5250;
extern "C" WWW_t6_78 * POSTRequest_CreateWWWObject_m8_944 (POSTRequest_t8_163 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WWW_t6_78_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1944);
		IDictionary_t1_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		Encoding_t1_406_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		_stringLiteral5249 = il2cpp_codegen_string_literal_from_index(5249);
		_stringLiteral5250 = il2cpp_codegen_string_literal_from_index(5250);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	Object_t * V_1 = {0};
	String_t* V_2 = {0};
	ByteU5BU5D_t1_109* V_3 = {0};
	URL_t8_156  V_4 = {0};
	{
		URL_t8_156  L_0 = Request_get_URL_m8_896(__this, /*hidden argument*/NULL);
		V_4 = L_0;
		String_t* L_1 = URL_get_URLString_m8_958((&V_4), /*hidden argument*/NULL);
		V_0 = L_1;
		Object_t * L_2 = WebRequest_get_Parameters_m8_908(__this, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_002c;
		}
	}
	{
		Debug_LogWarning_m6_636(NULL /*static, unused*/, _stringLiteral5249, /*hidden argument*/NULL);
		String_t* L_3 = V_0;
		WWW_t6_78 * L_4 = (WWW_t6_78 *)il2cpp_codegen_object_new (WWW_t6_78_il2cpp_TypeInfo_var);
		WWW__ctor_m6_541(L_4, L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_002c:
	{
		Object_t * L_5 = WebRequest_get_Parameters_m8_908(__this, /*hidden argument*/NULL);
		V_1 = ((Object_t *)IsInst(L_5, IDictionary_t1_35_il2cpp_TypeInfo_var));
		Object_t * L_6 = V_1;
		if (L_6)
		{
			goto IL_004a;
		}
	}
	{
		Debug_LogError_m6_632(NULL /*static, unused*/, _stringLiteral5250, /*hidden argument*/NULL);
		return (WWW_t6_78 *)NULL;
	}

IL_004a:
	{
		Object_t * L_7 = V_1;
		String_t* L_8 = JSONParserExtensions_ToJSON_m8_269(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t1_406_il2cpp_TypeInfo_var);
		Encoding_t1_406 * L_9 = Encoding_get_UTF8_m1_12363(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_10 = V_2;
		NullCheck(L_9);
		ByteU5BU5D_t1_109* L_11 = (ByteU5BU5D_t1_109*)VirtFuncInvoker1< ByteU5BU5D_t1_109*, String_t* >::Invoke(11 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_9, L_10);
		V_3 = L_11;
		String_t* L_12 = V_0;
		ByteU5BU5D_t1_109* L_13 = V_3;
		WWW_t6_78 * L_14 = (WWW_t6_78 *)il2cpp_codegen_object_new (WWW_t6_78_il2cpp_TypeInfo_var);
		WWW__ctor_m6_543(L_14, L_12, L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
// VoxelBusters.Utility.POSTRequest VoxelBusters.Utility.POSTRequest::CreateRequest(VoxelBusters.Utility.URL,System.Object)
extern TypeInfo* POSTRequest_t8_163_il2cpp_TypeInfo_var;
extern "C" POSTRequest_t8_163 * POSTRequest_CreateRequest_m8_945 (Object_t * __this /* static, unused */, URL_t8_156  ____URL, Object_t * ____params, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		POSTRequest_t8_163_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2092);
		s_Il2CppMethodIntialized = true;
	}
	POSTRequest_t8_163 * V_0 = {0};
	{
		URL_t8_156  L_0 = ____URL;
		Object_t * L_1 = ____params;
		POSTRequest_t8_163 * L_2 = (POSTRequest_t8_163 *)il2cpp_codegen_object_new (POSTRequest_t8_163_il2cpp_TypeInfo_var);
		POSTRequest__ctor_m8_943(L_2, L_0, L_1, 0, /*hidden argument*/NULL);
		V_0 = L_2;
		POSTRequest_t8_163 * L_3 = V_0;
		return L_3;
	}
}
// VoxelBusters.Utility.POSTRequest VoxelBusters.Utility.POSTRequest::CreateAsyncRequest(VoxelBusters.Utility.URL,System.Object)
extern TypeInfo* POSTRequest_t8_163_il2cpp_TypeInfo_var;
extern "C" POSTRequest_t8_163 * POSTRequest_CreateAsyncRequest_m8_946 (Object_t * __this /* static, unused */, URL_t8_156  ____URL, Object_t * ____params, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		POSTRequest_t8_163_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2092);
		s_Il2CppMethodIntialized = true;
	}
	POSTRequest_t8_163 * V_0 = {0};
	{
		URL_t8_156  L_0 = ____URL;
		Object_t * L_1 = ____params;
		POSTRequest_t8_163 * L_2 = (POSTRequest_t8_163 *)il2cpp_codegen_object_new (POSTRequest_t8_163_il2cpp_TypeInfo_var);
		POSTRequest__ctor_m8_943(L_2, L_0, L_1, 0, /*hidden argument*/NULL);
		V_0 = L_2;
		POSTRequest_t8_163 * L_3 = V_0;
		return L_3;
	}
}
// System.Int32 VoxelBusters.Utility.WebResponse::get_Status()
extern "C" int32_t WebResponse_get_Status_m8_947 (WebResponse_t8_164 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___U3CStatusU3Ek__BackingField_0);
		return L_0;
	}
}
// System.Void VoxelBusters.Utility.WebResponse::set_Status(System.Int32)
extern "C" void WebResponse_set_Status_m8_948 (WebResponse_t8_164 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___U3CStatusU3Ek__BackingField_0 = L_0;
		return;
	}
}
// System.String VoxelBusters.Utility.WebResponse::get_Message()
extern "C" String_t* WebResponse_get_Message_m8_949 (WebResponse_t8_164 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CMessageU3Ek__BackingField_1);
		return L_0;
	}
}
// System.Void VoxelBusters.Utility.WebResponse::set_Message(System.String)
extern "C" void WebResponse_set_Message_m8_950 (WebResponse_t8_164 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CMessageU3Ek__BackingField_1 = L_0;
		return;
	}
}
// System.Collections.IDictionary VoxelBusters.Utility.WebResponse::get_Data()
extern "C" Object_t * WebResponse_get_Data_m8_951 (WebResponse_t8_164 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U3CDataU3Ek__BackingField_2);
		return L_0;
	}
}
// System.Void VoxelBusters.Utility.WebResponse::set_Data(System.Collections.IDictionary)
extern "C" void WebResponse_set_Data_m8_952 (WebResponse_t8_164 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		__this->___U3CDataU3Ek__BackingField_2 = L_0;
		return;
	}
}
// System.Collections.Generic.List`1<System.String> VoxelBusters.Utility.WebResponse::get_Errors()
extern "C" List_1_t1_1742 * WebResponse_get_Errors_m8_953 (WebResponse_t8_164 * __this, const MethodInfo* method)
{
	{
		List_1_t1_1742 * L_0 = (__this->___U3CErrorsU3Ek__BackingField_3);
		return L_0;
	}
}
// System.Void VoxelBusters.Utility.WebResponse::set_Errors(System.Collections.Generic.List`1<System.String>)
extern "C" void WebResponse_set_Errors_m8_954 (WebResponse_t8_164 * __this, List_1_t1_1742 * ___value, const MethodInfo* method)
{
	{
		List_1_t1_1742 * L_0 = ___value;
		__this->___U3CErrorsU3Ek__BackingField_3 = L_0;
		return;
	}
}
// VoxelBusters.Utility.WebResponse VoxelBusters.Utility.WebResponse::WebResponseOnSuccess(System.Collections.IDictionary)
extern TypeInfo* WebResponse_t8_164_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionary_t1_35_il2cpp_TypeInfo_var;
extern const MethodInfo* IDictionaryExtensions_GetIfAvailable_TisInt32_t1_3_m8_2007_MethodInfo_var;
extern const MethodInfo* IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var;
extern const MethodInfo* IDictionaryExtensions_GetIfAvailable_TisList_1_t1_1742_m8_2008_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral5251;
extern Il2CppCodeGenString* _stringLiteral5252;
extern Il2CppCodeGenString* _stringLiteral424;
extern Il2CppCodeGenString* _stringLiteral5136;
extern Il2CppCodeGenString* _stringLiteral5253;
extern "C" WebResponse_t8_164  WebResponse_WebResponseOnSuccess_m8_955 (Object_t * __this /* static, unused */, Object_t * ____jsonResponse, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WebResponse_t8_164_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2093);
		IDictionary_t1_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		IDictionaryExtensions_GetIfAvailable_TisInt32_t1_3_m8_2007_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484196);
		IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484117);
		IDictionaryExtensions_GetIfAvailable_TisList_1_t1_1742_m8_2008_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484197);
		_stringLiteral5251 = il2cpp_codegen_string_literal_from_index(5251);
		_stringLiteral5252 = il2cpp_codegen_string_literal_from_index(5252);
		_stringLiteral424 = il2cpp_codegen_string_literal_from_index(424);
		_stringLiteral5136 = il2cpp_codegen_string_literal_from_index(5136);
		_stringLiteral5253 = il2cpp_codegen_string_literal_from_index(5253);
		s_Il2CppMethodIntialized = true;
	}
	WebResponse_t8_164  V_0 = {0};
	Object_t * V_1 = {0};
	{
		Initobj (WebResponse_t8_164_il2cpp_TypeInfo_var, (&V_0));
		Object_t * L_0 = ____jsonResponse;
		int32_t L_1 = IDictionaryExtensions_GetIfAvailable_TisInt32_t1_3_m8_2007(NULL /*static, unused*/, L_0, _stringLiteral5251, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisInt32_t1_3_m8_2007_MethodInfo_var);
		WebResponse_set_Status_m8_948((&V_0), L_1, /*hidden argument*/NULL);
		Object_t * L_2 = ____jsonResponse;
		NullCheck(L_2);
		bool L_3 = (bool)InterfaceFuncInvoker1< bool, Object_t * >::Invoke(8 /* System.Boolean System.Collections.IDictionary::Contains(System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_2, _stringLiteral5252);
		if (!L_3)
		{
			goto IL_0086;
		}
	}
	{
		Object_t * L_4 = ____jsonResponse;
		NullCheck(L_4);
		Object_t * L_5 = (Object_t *)InterfaceFuncInvoker1< Object_t *, Object_t * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_4, _stringLiteral5252);
		V_1 = ((Object_t *)IsInst(L_5, IDictionary_t1_35_il2cpp_TypeInfo_var));
		Object_t * L_6 = V_1;
		NullCheck(L_6);
		bool L_7 = (bool)InterfaceFuncInvoker1< bool, Object_t * >::Invoke(8 /* System.Boolean System.Collections.IDictionary::Contains(System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_6, _stringLiteral424);
		if (!L_7)
		{
			goto IL_0086;
		}
	}
	{
		Object_t * L_8 = V_1;
		NullCheck(L_8);
		Object_t * L_9 = (Object_t *)InterfaceFuncInvoker1< Object_t *, Object_t * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_8, _stringLiteral424);
		WebResponse_set_Data_m8_952((&V_0), ((Object_t *)IsInst(L_9, IDictionary_t1_35_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		Object_t * L_10 = V_1;
		String_t* L_11 = IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(NULL /*static, unused*/, L_10, _stringLiteral5136, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var);
		WebResponse_set_Message_m8_950((&V_0), L_11, /*hidden argument*/NULL);
		Object_t * L_12 = V_1;
		List_1_t1_1742 * L_13 = IDictionaryExtensions_GetIfAvailable_TisList_1_t1_1742_m8_2008(NULL /*static, unused*/, L_12, _stringLiteral5253, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisList_1_t1_1742_m8_2008_MethodInfo_var);
		WebResponse_set_Errors_m8_954((&V_0), L_13, /*hidden argument*/NULL);
	}

IL_0086:
	{
		WebResponse_t8_164  L_14 = V_0;
		return L_14;
	}
}
// VoxelBusters.Utility.WebResponse VoxelBusters.Utility.WebResponse::WebResponseOnFail(System.Collections.IDictionary)
extern TypeInfo* WebResponse_t8_164_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t1_1742_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionary_t1_35_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1_14876_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral5241;
extern "C" WebResponse_t8_164  WebResponse_WebResponseOnFail_m8_956 (Object_t * __this /* static, unused */, Object_t * ____jsonResponse, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WebResponse_t8_164_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2093);
		List_1_t1_1742_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(37);
		IDictionary_t1_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		List_1__ctor_m1_14876_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483649);
		_stringLiteral5241 = il2cpp_codegen_string_literal_from_index(5241);
		s_Il2CppMethodIntialized = true;
	}
	WebResponse_t8_164  V_0 = {0};
	{
		Initobj (WebResponse_t8_164_il2cpp_TypeInfo_var, (&V_0));
		WebResponse_set_Status_m8_948((&V_0), 0, /*hidden argument*/NULL);
		WebResponse_set_Message_m8_950((&V_0), (String_t*)NULL, /*hidden argument*/NULL);
		WebResponse_set_Data_m8_952((&V_0), (Object_t *)NULL, /*hidden argument*/NULL);
		List_1_t1_1742 * L_0 = (List_1_t1_1742 *)il2cpp_codegen_object_new (List_1_t1_1742_il2cpp_TypeInfo_var);
		List_1__ctor_m1_14876(L_0, /*hidden argument*/List_1__ctor_m1_14876_MethodInfo_var);
		WebResponse_set_Errors_m8_954((&V_0), L_0, /*hidden argument*/NULL);
		List_1_t1_1742 * L_1 = WebResponse_get_Errors_m8_953((&V_0), /*hidden argument*/NULL);
		Object_t * L_2 = ____jsonResponse;
		NullCheck(L_2);
		Object_t * L_3 = (Object_t *)InterfaceFuncInvoker1< Object_t *, Object_t * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_2, _stringLiteral5241);
		NullCheck(L_1);
		VirtActionInvoker1< String_t* >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.String>::Add(!0) */, L_1, ((String_t*)IsInstSealed(L_3, String_t_il2cpp_TypeInfo_var)));
		WebResponse_t8_164  L_4 = V_0;
		return L_4;
	}
}
// System.Void VoxelBusters.Utility.URL::.ctor(System.String)
extern "C" void URL__ctor_m8_957 (URL_t8_156 * __this, String_t* ____URLString, const MethodInfo* method)
{
	{
		String_t* L_0 = ____URLString;
		URL_set_URLString_m8_959(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String VoxelBusters.Utility.URL::get_URLString()
extern "C" String_t* URL_get_URLString_m8_958 (URL_t8_156 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CURLStringU3Ek__BackingField_3);
		return L_0;
	}
}
// System.Void VoxelBusters.Utility.URL::set_URLString(System.String)
extern "C" void URL_set_URLString_m8_959 (URL_t8_156 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CURLStringU3Ek__BackingField_3 = L_0;
		return;
	}
}
// VoxelBusters.Utility.URL VoxelBusters.Utility.URL::FileURLWithPath(System.String,System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral740;
extern "C" URL_t8_156  URL_FileURLWithPath_m8_960 (Object_t * __this /* static, unused */, String_t* ____rootPath, String_t* ____relativePath, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral740 = il2cpp_codegen_string_literal_from_index(740);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ____rootPath;
		String_t* L_1 = ____relativePath;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m1_560(NULL /*static, unused*/, L_0, _stringLiteral740, L_1, /*hidden argument*/NULL);
		URL_t8_156  L_3 = URL_FileURLWithPath_m8_961(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// VoxelBusters.Utility.URL VoxelBusters.Utility.URL::FileURLWithPath(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3032;
extern "C" URL_t8_156  URL_FileURLWithPath_m8_961 (Object_t * __this /* static, unused */, String_t* ____path, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral3032 = il2cpp_codegen_string_literal_from_index(3032);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	{
		V_0 = (String_t*)NULL;
		String_t* L_0 = ____path;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1_521(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0030;
		}
	}
	{
		String_t* L_2 = ____path;
		NullCheck(L_2);
		bool L_3 = String_StartsWith_m1_531(L_2, _stringLiteral3032, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_002e;
		}
	}
	{
		String_t* L_4 = ____path;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m1_559(NULL /*static, unused*/, _stringLiteral3032, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_0030;
	}

IL_002e:
	{
		String_t* L_6 = ____path;
		V_0 = L_6;
	}

IL_0030:
	{
		String_t* L_7 = V_0;
		URL_t8_156  L_8 = {0};
		URL__ctor_m8_957(&L_8, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// VoxelBusters.Utility.URL VoxelBusters.Utility.URL::URLWithString(System.String,System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral740;
extern "C" URL_t8_156  URL_URLWithString_m8_962 (Object_t * __this /* static, unused */, String_t* ____rootURLString, String_t* ____relativePath, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral740 = il2cpp_codegen_string_literal_from_index(740);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ____rootURLString;
		String_t* L_1 = ____relativePath;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m1_560(NULL /*static, unused*/, L_0, _stringLiteral740, L_1, /*hidden argument*/NULL);
		URL_t8_156  L_3 = URL_URLWithString_m8_963(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// VoxelBusters.Utility.URL VoxelBusters.Utility.URL::URLWithString(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* URL_t8_156_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5254;
extern Il2CppCodeGenString* _stringLiteral5255;
extern "C" URL_t8_156  URL_URLWithString_m8_963 (Object_t * __this /* static, unused */, String_t* ____URLString, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		URL_t8_156_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2094);
		_stringLiteral5254 = il2cpp_codegen_string_literal_from_index(5254);
		_stringLiteral5255 = il2cpp_codegen_string_literal_from_index(5255);
		s_Il2CppMethodIntialized = true;
	}
	URL_t8_156  V_0 = {0};
	{
		String_t* L_0 = ____URLString;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1_521(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0015;
		}
	}
	{
		Initobj (URL_t8_156_il2cpp_TypeInfo_var, (&V_0));
		URL_t8_156  L_2 = V_0;
		return L_2;
	}

IL_0015:
	{
		String_t* L_3 = ____URLString;
		NullCheck(L_3);
		bool L_4 = String_StartsWith_m1_531(L_3, _stringLiteral5254, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0035;
		}
	}
	{
		String_t* L_5 = ____URLString;
		NullCheck(L_5);
		bool L_6 = String_StartsWith_m1_531(L_5, _stringLiteral5255, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003c;
		}
	}

IL_0035:
	{
		String_t* L_7 = ____URLString;
		URL_t8_156  L_8 = {0};
		URL__ctor_m8_957(&L_8, L_7, /*hidden argument*/NULL);
		return L_8;
	}

IL_003c:
	{
		String_t* L_9 = ____URLString;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m1_559(NULL /*static, unused*/, _stringLiteral5254, L_9, /*hidden argument*/NULL);
		____URLString = L_10;
		String_t* L_11 = ____URLString;
		URL_t8_156  L_12 = {0};
		URL__ctor_m8_957(&L_12, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Boolean VoxelBusters.Utility.URL::isFileReferenceURL()
extern Il2CppCodeGenString* _stringLiteral3032;
extern "C" bool URL_isFileReferenceURL_m8_964 (URL_t8_156 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral3032 = il2cpp_codegen_string_literal_from_index(3032);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = URL_get_URLString_m8_958(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = String_StartsWith_m1_531(L_0, _stringLiteral3032, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String VoxelBusters.Utility.URL::ToString()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5256;
extern "C" String_t* URL_ToString_m8_965 (URL_t8_156 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5256 = il2cpp_codegen_string_literal_from_index(5256);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = URL_get_URLString_m8_958(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Format_m1_548(NULL /*static, unused*/, _stringLiteral5256, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// Conversion methods for marshalling of: VoxelBusters.Utility.URL
extern "C" void URL_t8_156_marshal(const URL_t8_156& unmarshaled, URL_t8_156_marshaled& marshaled)
{
	marshaled.___U3CURLStringU3Ek__BackingField_3 = il2cpp_codegen_marshal_string(unmarshaled.___U3CURLStringU3Ek__BackingField_3);
}
extern "C" void URL_t8_156_marshal_back(const URL_t8_156_marshaled& marshaled, URL_t8_156& unmarshaled)
{
	unmarshaled.___U3CURLStringU3Ek__BackingField_3 = il2cpp_codegen_marshal_string_result(marshaled.___U3CURLStringU3Ek__BackingField_3);
}
// Conversion method for clean up from marshalling of: VoxelBusters.Utility.URL
extern "C" void URL_t8_156_marshal_cleanup(URL_t8_156_marshaled& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___U3CURLStringU3Ek__BackingField_3);
	marshaled.___U3CURLStringU3Ek__BackingField_3 = NULL;
}
// System.Void Demo::.ctor()
extern "C" void Demo__ctor_m8_966 (Demo_t8_165 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m6_671(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Demo::Start()
extern "C" void Demo_Start_m8_967 (Demo_t8_165 * __this, const MethodInfo* method)
{
	{
		Demo_PrintLogs_m8_968(__this, (Console_t8_169 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Demo::PrintLogs(VoxelBusters.DebugPRO.Console)
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5257;
extern Il2CppCodeGenString* _stringLiteral5258;
extern Il2CppCodeGenString* _stringLiteral5259;
extern Il2CppCodeGenString* _stringLiteral5260;
extern Il2CppCodeGenString* _stringLiteral5261;
extern Il2CppCodeGenString* _stringLiteral5262;
extern Il2CppCodeGenString* _stringLiteral5263;
extern Il2CppCodeGenString* _stringLiteral5264;
extern Il2CppCodeGenString* _stringLiteral5265;
extern Il2CppCodeGenString* _stringLiteral5266;
extern Il2CppCodeGenString* _stringLiteral5267;
extern Il2CppCodeGenString* _stringLiteral5268;
extern "C" void Demo_PrintLogs_m8_968 (Demo_t8_165 * __this, Console_t8_169 * ____p, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		_stringLiteral5257 = il2cpp_codegen_string_literal_from_index(5257);
		_stringLiteral5258 = il2cpp_codegen_string_literal_from_index(5258);
		_stringLiteral5259 = il2cpp_codegen_string_literal_from_index(5259);
		_stringLiteral5260 = il2cpp_codegen_string_literal_from_index(5260);
		_stringLiteral5261 = il2cpp_codegen_string_literal_from_index(5261);
		_stringLiteral5262 = il2cpp_codegen_string_literal_from_index(5262);
		_stringLiteral5263 = il2cpp_codegen_string_literal_from_index(5263);
		_stringLiteral5264 = il2cpp_codegen_string_literal_from_index(5264);
		_stringLiteral5265 = il2cpp_codegen_string_literal_from_index(5265);
		_stringLiteral5266 = il2cpp_codegen_string_literal_from_index(5266);
		_stringLiteral5267 = il2cpp_codegen_string_literal_from_index(5267);
		_stringLiteral5268 = il2cpp_codegen_string_literal_from_index(5268);
		s_Il2CppMethodIntialized = true;
	}
	{
		Debug_Log_m6_631(NULL /*static, unused*/, _stringLiteral5257, /*hidden argument*/NULL);
		Debug_Log_m6_631(NULL /*static, unused*/, _stringLiteral5258, /*hidden argument*/NULL);
		Debug_Log_m6_631(NULL /*static, unused*/, _stringLiteral5259, /*hidden argument*/NULL);
		Debug_Log_m6_631(NULL /*static, unused*/, _stringLiteral5260, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_Log_m8_1007(NULL /*static, unused*/, _stringLiteral5261, _stringLiteral5262, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		Console_Log_m8_1007(NULL /*static, unused*/, _stringLiteral5263, _stringLiteral5264, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		Console_Log_m8_1007(NULL /*static, unused*/, _stringLiteral5265, _stringLiteral5266, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		Console_Log_m8_1007(NULL /*static, unused*/, _stringLiteral5267, _stringLiteral5268, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.DebugPRO.Internal.NativeBinding::.ctor()
extern "C" void NativeBinding__ctor_m8_969 (NativeBinding_t8_166 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m6_671(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.DebugPRO.Internal.NativeBinding::debugProLogMessage(System.String,VoxelBusters.DebugPRO.Internal.eConsoleLogType,System.String)
extern "C" {void DEFAULT_CALL debugProLogMessage(char*, int32_t, char*);}
extern "C" void NativeBinding_debugProLogMessage_m8_970 (Object_t * __this /* static, unused */, String_t* ____message, int32_t ____type, String_t* ____stackTrace, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, int32_t, char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)debugProLogMessage;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'debugProLogMessage'"));
		}
	}

	// Marshaling of parameter '____message' to native representation
	char* _____message_marshaled = { 0 };
	_____message_marshaled = il2cpp_codegen_marshal_string(____message);

	// Marshaling of parameter '____type' to native representation

	// Marshaling of parameter '____stackTrace' to native representation
	char* _____stackTrace_marshaled = { 0 };
	_____stackTrace_marshaled = il2cpp_codegen_marshal_string(____stackTrace);

	// Native function invocation
	_il2cpp_pinvoke_func(_____message_marshaled, ____type, _____stackTrace_marshaled);

	// Marshaling cleanup of parameter '____message' native representation
	il2cpp_codegen_marshal_free(_____message_marshaled);
	_____message_marshaled = NULL;

	// Marshaling cleanup of parameter '____type' native representation

	// Marshaling cleanup of parameter '____stackTrace' native representation
	il2cpp_codegen_marshal_free(_____stackTrace_marshaled);
	_____stackTrace_marshaled = NULL;

}
// System.Void VoxelBusters.DebugPRO.Internal.NativeBinding::Log(VoxelBusters.DebugPRO.Internal.ConsoleLog)
extern "C" void NativeBinding_Log_m8_971 (Object_t * __this /* static, unused */, ConsoleLog_t8_170  ____log, const MethodInfo* method)
{
	{
		String_t* L_0 = ConsoleLog_get_Message_m8_1016((&____log), /*hidden argument*/NULL);
		int32_t L_1 = ConsoleLog_get_Type_m8_1018((&____log), /*hidden argument*/NULL);
		String_t* L_2 = ConsoleLog_get_StackTrace_m8_1022((&____log), /*hidden argument*/NULL);
		NativeBinding_debugProLogMessage_m8_970(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.DebugPRO.Console/<GetConsoleTag>c__AnonStorey7::.ctor()
extern "C" void U3CGetConsoleTagU3Ec__AnonStorey7__ctor_m8_972 (U3CGetConsoleTagU3Ec__AnonStorey7_t8_167 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean VoxelBusters.DebugPRO.Console/<GetConsoleTag>c__AnonStorey7::<>m__5(VoxelBusters.DebugPRO.Internal.ConsoleTag)
extern "C" bool U3CGetConsoleTagU3Ec__AnonStorey7_U3CU3Em__5_m8_973 (U3CGetConsoleTagU3Ec__AnonStorey7_t8_167 * __this, ConsoleTag_t8_171 * ____consoleTag, const MethodInfo* method)
{
	{
		ConsoleTag_t8_171 * L_0 = ____consoleTag;
		NullCheck(L_0);
		String_t* L_1 = ConsoleTag_get_Name_m8_1034(L_0, /*hidden argument*/NULL);
		String_t* L_2 = (__this->____tagName_0);
		NullCheck(L_1);
		bool L_3 = String_Equals_m1_441(L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void VoxelBusters.DebugPRO.Console/<GetIndexOfConsoleTag>c__AnonStorey8::.ctor()
extern "C" void U3CGetIndexOfConsoleTagU3Ec__AnonStorey8__ctor_m8_974 (U3CGetIndexOfConsoleTagU3Ec__AnonStorey8_t8_168 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean VoxelBusters.DebugPRO.Console/<GetIndexOfConsoleTag>c__AnonStorey8::<>m__6(VoxelBusters.DebugPRO.Internal.ConsoleTag)
extern "C" bool U3CGetIndexOfConsoleTagU3Ec__AnonStorey8_U3CU3Em__6_m8_975 (U3CGetIndexOfConsoleTagU3Ec__AnonStorey8_t8_168 * __this, ConsoleTag_t8_171 * ____consoleTag, const MethodInfo* method)
{
	{
		ConsoleTag_t8_171 * L_0 = ____consoleTag;
		NullCheck(L_0);
		String_t* L_1 = ConsoleTag_get_Name_m8_1034(L_0, /*hidden argument*/NULL);
		String_t* L_2 = (__this->____tagName_0);
		NullCheck(L_1);
		bool L_3 = String_Equals_m1_441(L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void VoxelBusters.DebugPRO.Console::.ctor()
extern TypeInfo* List_1_t1_1908_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t1_1909_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1_15060_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m1_15061_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral166;
extern "C" void Console__ctor_m8_976 (Console_t8_169 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t1_1908_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2098);
		List_1_t1_1909_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2099);
		List_1__ctor_m1_15060_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484198);
		List_1__ctor_m1_15061_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484199);
		_stringLiteral166 = il2cpp_codegen_string_literal_from_index(166);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1_1908 * L_0 = (List_1_t1_1908 *)il2cpp_codegen_object_new (List_1_t1_1908_il2cpp_TypeInfo_var);
		List_1__ctor_m1_15060(L_0, /*hidden argument*/List_1__ctor_m1_15060_MethodInfo_var);
		__this->___m_consoleLogsList_4 = L_0;
		List_1_t1_1908 * L_1 = (List_1_t1_1908 *)il2cpp_codegen_object_new (List_1_t1_1908_il2cpp_TypeInfo_var);
		List_1__ctor_m1_15060(L_1, /*hidden argument*/List_1__ctor_m1_15060_MethodInfo_var);
		__this->___m_displayableConsoleLogsList_5 = L_1;
		List_1_t1_1909 * L_2 = (List_1_t1_1909 *)il2cpp_codegen_object_new (List_1_t1_1909_il2cpp_TypeInfo_var);
		List_1__ctor_m1_15061(L_2, /*hidden argument*/List_1__ctor_m1_15061_MethodInfo_var);
		__this->___m_consoleTags_6 = L_2;
		__this->___m_clearOnPlay_7 = 1;
		__this->___m_allowedLogTypes_9 = ((int32_t)255);
		__this->___m_showInfoLogs_10 = 1;
		__this->___m_showWarningLogs_11 = 1;
		__this->___m_showErrorLogs_12 = 1;
		ScriptableObject__ctor_m6_16(__this, /*hidden argument*/NULL);
		__this->___m_infoLogsCounterStr_13 = _stringLiteral166;
		__this->___m_warningLogsCounterStr_15 = _stringLiteral166;
		__this->___m_errorLogsCounterStr_17 = _stringLiteral166;
		return;
	}
}
// System.Void VoxelBusters.DebugPRO.Console::.cctor()
extern TypeInfo* LogCallback_t6_83_il2cpp_TypeInfo_var;
extern TypeInfo* UnityDebugUtility_t8_173_il2cpp_TypeInfo_var;
extern const MethodInfo* Console_HandleUnityLog_m8_994_MethodInfo_var;
extern "C" void Console__cctor_m8_977 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LogCallback_t6_83_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1608);
		UnityDebugUtility_t8_173_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2100);
		Console_HandleUnityLog_m8_994_MethodInfo_var = il2cpp_codegen_method_info_from_index(552);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = { (void*)Console_HandleUnityLog_m8_994_MethodInfo_var };
		LogCallback_t6_83 * L_1 = (LogCallback_t6_83 *)il2cpp_codegen_object_new (LogCallback_t6_83_il2cpp_TypeInfo_var);
		LogCallback__ctor_m6_577(L_1, NULL, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UnityDebugUtility_t8_173_il2cpp_TypeInfo_var);
		UnityDebugUtility_remove_LogCallback_m8_1043(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IntPtr_t L_2 = { (void*)Console_HandleUnityLog_m8_994_MethodInfo_var };
		LogCallback_t6_83 * L_3 = (LogCallback_t6_83 *)il2cpp_codegen_object_new (LogCallback_t6_83_il2cpp_TypeInfo_var);
		LogCallback__ctor_m6_577(L_3, NULL, L_2, /*hidden argument*/NULL);
		UnityDebugUtility_add_LogCallback_m8_1042(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean VoxelBusters.DebugPRO.Console::get_IsDebugMode()
extern "C" bool Console_get_IsDebugMode_m8_978 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		bool L_0 = Debug_get_isDebugBuild_m6_638(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean VoxelBusters.DebugPRO.Console::get_ShowInfoLogs()
extern "C" bool Console_get_ShowInfoLogs_m8_979 (Console_t8_169 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_showInfoLogs_10);
		return L_0;
	}
}
// System.Void VoxelBusters.DebugPRO.Console::set_ShowInfoLogs(System.Boolean)
extern "C" void Console_set_ShowInfoLogs_m8_980 (Console_t8_169 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_showInfoLogs_10);
		bool L_1 = ___value;
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0041;
		}
	}
	{
		bool L_2 = ___value;
		__this->___m_showInfoLogs_10 = L_2;
		bool L_3 = ___value;
		if (!L_3)
		{
			goto IL_002c;
		}
	}
	{
		int32_t L_4 = (__this->___m_allowedLogTypes_9);
		__this->___m_allowedLogTypes_9 = ((int32_t)((int32_t)L_4|(int32_t)8));
		goto IL_003b;
	}

IL_002c:
	{
		int32_t L_5 = (__this->___m_allowedLogTypes_9);
		__this->___m_allowedLogTypes_9 = ((int32_t)((int32_t)L_5&(int32_t)((int32_t)-9)));
	}

IL_003b:
	{
		Console_RebuildDisplayableLogs_m8_996(__this, /*hidden argument*/NULL);
	}

IL_0041:
	{
		return;
	}
}
// System.Boolean VoxelBusters.DebugPRO.Console::get_ShowWarningLogs()
extern "C" bool Console_get_ShowWarningLogs_m8_981 (Console_t8_169 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_showWarningLogs_11);
		return L_0;
	}
}
// System.Void VoxelBusters.DebugPRO.Console::set_ShowWarningLogs(System.Boolean)
extern "C" void Console_set_ShowWarningLogs_m8_982 (Console_t8_169 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_showWarningLogs_11);
		bool L_1 = ___value;
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0041;
		}
	}
	{
		bool L_2 = ___value;
		__this->___m_showWarningLogs_11 = L_2;
		bool L_3 = ___value;
		if (!L_3)
		{
			goto IL_002c;
		}
	}
	{
		int32_t L_4 = (__this->___m_allowedLogTypes_9);
		__this->___m_allowedLogTypes_9 = ((int32_t)((int32_t)L_4|(int32_t)4));
		goto IL_003b;
	}

IL_002c:
	{
		int32_t L_5 = (__this->___m_allowedLogTypes_9);
		__this->___m_allowedLogTypes_9 = ((int32_t)((int32_t)L_5&(int32_t)((int32_t)-5)));
	}

IL_003b:
	{
		Console_RebuildDisplayableLogs_m8_996(__this, /*hidden argument*/NULL);
	}

IL_0041:
	{
		return;
	}
}
// System.Boolean VoxelBusters.DebugPRO.Console::get_ShowErrorLogs()
extern "C" bool Console_get_ShowErrorLogs_m8_983 (Console_t8_169 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_showErrorLogs_12);
		return L_0;
	}
}
// System.Void VoxelBusters.DebugPRO.Console::set_ShowErrorLogs(System.Boolean)
extern "C" void Console_set_ShowErrorLogs_m8_984 (Console_t8_169 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_showErrorLogs_12);
		bool L_1 = ___value;
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0042;
		}
	}
	{
		bool L_2 = ___value;
		__this->___m_showErrorLogs_12 = L_2;
		bool L_3 = ___value;
		if (!L_3)
		{
			goto IL_002d;
		}
	}
	{
		int32_t L_4 = (__this->___m_allowedLogTypes_9);
		__this->___m_allowedLogTypes_9 = ((int32_t)((int32_t)L_4|(int32_t)((int32_t)19)));
		goto IL_003c;
	}

IL_002d:
	{
		int32_t L_5 = (__this->___m_allowedLogTypes_9);
		__this->___m_allowedLogTypes_9 = ((int32_t)((int32_t)L_5&(int32_t)((int32_t)-20)));
	}

IL_003c:
	{
		Console_RebuildDisplayableLogs_m8_996(__this, /*hidden argument*/NULL);
	}

IL_0042:
	{
		return;
	}
}
// System.Int32 VoxelBusters.DebugPRO.Console::get_InfoLogsCounter()
extern "C" int32_t Console_get_InfoLogsCounter_m8_985 (Console_t8_169 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_infoLogsCounter_14);
		return L_0;
	}
}
// System.Void VoxelBusters.DebugPRO.Console::set_InfoLogsCounter(System.Int32)
extern Il2CppCodeGenString* _stringLiteral5269;
extern "C" void Console_set_InfoLogsCounter_m8_986 (Console_t8_169 * __this, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral5269 = il2cpp_codegen_string_literal_from_index(5269);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___value;
		int32_t L_1 = (__this->___m_infoLogsCounter_14);
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0034;
		}
	}
	{
		int32_t L_2 = ___value;
		if ((((int32_t)L_2) <= ((int32_t)((int32_t)999))))
		{
			goto IL_0027;
		}
	}
	{
		__this->___m_infoLogsCounterStr_13 = _stringLiteral5269;
		goto IL_0034;
	}

IL_0027:
	{
		String_t* L_3 = Int32_ToString_m1_101((&___value), /*hidden argument*/NULL);
		__this->___m_infoLogsCounterStr_13 = L_3;
	}

IL_0034:
	{
		int32_t L_4 = ___value;
		__this->___m_infoLogsCounter_14 = L_4;
		return;
	}
}
// System.Int32 VoxelBusters.DebugPRO.Console::get_WarningLogsCounter()
extern "C" int32_t Console_get_WarningLogsCounter_m8_987 (Console_t8_169 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_warningLogsCounter_16);
		return L_0;
	}
}
// System.Void VoxelBusters.DebugPRO.Console::set_WarningLogsCounter(System.Int32)
extern Il2CppCodeGenString* _stringLiteral5269;
extern "C" void Console_set_WarningLogsCounter_m8_988 (Console_t8_169 * __this, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral5269 = il2cpp_codegen_string_literal_from_index(5269);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___value;
		int32_t L_1 = (__this->___m_warningLogsCounter_16);
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0034;
		}
	}
	{
		int32_t L_2 = ___value;
		if ((((int32_t)L_2) <= ((int32_t)((int32_t)999))))
		{
			goto IL_0027;
		}
	}
	{
		__this->___m_warningLogsCounterStr_15 = _stringLiteral5269;
		goto IL_0034;
	}

IL_0027:
	{
		String_t* L_3 = Int32_ToString_m1_101((&___value), /*hidden argument*/NULL);
		__this->___m_warningLogsCounterStr_15 = L_3;
	}

IL_0034:
	{
		int32_t L_4 = ___value;
		__this->___m_warningLogsCounter_16 = L_4;
		return;
	}
}
// System.Int32 VoxelBusters.DebugPRO.Console::get_ErrorLogsCounter()
extern "C" int32_t Console_get_ErrorLogsCounter_m8_989 (Console_t8_169 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_errorLogsCounter_18);
		return L_0;
	}
}
// System.Void VoxelBusters.DebugPRO.Console::set_ErrorLogsCounter(System.Int32)
extern Il2CppCodeGenString* _stringLiteral5269;
extern "C" void Console_set_ErrorLogsCounter_m8_990 (Console_t8_169 * __this, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral5269 = il2cpp_codegen_string_literal_from_index(5269);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___value;
		int32_t L_1 = (__this->___m_errorLogsCounter_18);
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0034;
		}
	}
	{
		int32_t L_2 = ___value;
		if ((((int32_t)L_2) <= ((int32_t)((int32_t)999))))
		{
			goto IL_0027;
		}
	}
	{
		__this->___m_errorLogsCounterStr_17 = _stringLiteral5269;
		goto IL_0034;
	}

IL_0027:
	{
		String_t* L_3 = Int32_ToString_m1_101((&___value), /*hidden argument*/NULL);
		__this->___m_errorLogsCounterStr_17 = L_3;
	}

IL_0034:
	{
		int32_t L_4 = ___value;
		__this->___m_errorLogsCounter_18 = L_4;
		return;
	}
}
// VoxelBusters.DebugPRO.Console VoxelBusters.DebugPRO.Console::get_Instance()
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern const MethodInfo* ScriptableObject_CreateInstance_TisConsole_t8_169_m6_1923_MethodInfo_var;
extern "C" Console_t8_169 * Console_get_Instance_m8_991 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		ScriptableObject_CreateInstance_TisConsole_t8_169_m6_1923_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484201);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_t8_169 * L_0 = ((Console_t8_169_StaticFields*)Console_t8_169_il2cpp_TypeInfo_var->static_fields)->___instance_20;
		bool L_1 = Object_op_Equality_m6_721(NULL /*static, unused*/, L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		Console_t8_169 * L_2 = ScriptableObject_CreateInstance_TisConsole_t8_169_m6_1923(NULL /*static, unused*/, /*hidden argument*/ScriptableObject_CreateInstance_TisConsole_t8_169_m6_1923_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		((Console_t8_169_StaticFields*)Console_t8_169_il2cpp_TypeInfo_var->static_fields)->___instance_20 = L_2;
	}

IL_001a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_t8_169 * L_3 = ((Console_t8_169_StaticFields*)Console_t8_169_il2cpp_TypeInfo_var->static_fields)->___instance_20;
		return L_3;
	}
}
// System.Void VoxelBusters.DebugPRO.Console::OnEnable()
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern "C" void Console_OnEnable_m8_992 (Console_t8_169 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_t8_169 * L_0 = ((Console_t8_169_StaticFields*)Console_t8_169_il2cpp_TypeInfo_var->static_fields)->___instance_20;
		bool L_1 = Object_op_Equality_m6_721(NULL /*static, unused*/, L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		((Console_t8_169_StaticFields*)Console_t8_169_il2cpp_TypeInfo_var->static_fields)->___instance_20 = __this;
	}

IL_0016:
	{
		Console_RebuildDisplayableLogs_m8_996(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.DebugPRO.Console::OnDisable()
extern "C" void Console_OnDisable_m8_993 (Console_t8_169 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void VoxelBusters.DebugPRO.Console::HandleUnityLog(System.String,System.String,UnityEngine.LogType)
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5270;
extern "C" void Console_HandleUnityLog_m8_994 (Object_t * __this /* static, unused */, String_t* ____message, String_t* ____stackTrace, int32_t ____logType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		_stringLiteral5270 = il2cpp_codegen_string_literal_from_index(5270);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	{
		int32_t L_0 = ____logType;
		if ((!(((uint32_t)L_0) == ((uint32_t)3))))
		{
			goto IL_000e;
		}
	}
	{
		V_0 = 8;
		goto IL_003a;
	}

IL_000e:
	{
		int32_t L_1 = ____logType;
		if ((!(((uint32_t)L_1) == ((uint32_t)2))))
		{
			goto IL_001c;
		}
	}
	{
		V_0 = 4;
		goto IL_003a;
	}

IL_001c:
	{
		int32_t L_2 = ____logType;
		if (L_2)
		{
			goto IL_0029;
		}
	}
	{
		V_0 = 1;
		goto IL_003a;
	}

IL_0029:
	{
		int32_t L_3 = ____logType;
		if ((!(((uint32_t)L_3) == ((uint32_t)4))))
		{
			goto IL_0038;
		}
	}
	{
		V_0 = ((int32_t)16);
		goto IL_003a;
	}

IL_0038:
	{
		V_0 = 2;
	}

IL_003a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_t8_169 * L_4 = Console_get_Instance_m8_991(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_5 = Object_op_Inequality_m6_722(NULL /*static, unused*/, L_4, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_005d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_t8_169 * L_6 = Console_get_Instance_m8_991(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_7 = ____message;
		int32_t L_8 = V_0;
		NullCheck(L_6);
		Console_Log_m8_1001(L_6, _stringLiteral5270, L_7, L_8, (Object_t6_5 *)NULL, 5, /*hidden argument*/NULL);
	}

IL_005d:
	{
		return;
	}
}
// System.Void VoxelBusters.DebugPRO.Console::Clear()
extern "C" void Console_Clear_m8_995 (Console_t8_169 * __this, const MethodInfo* method)
{
	{
		List_1_t1_1908 * L_0 = (__this->___m_consoleLogsList_4);
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Clear() */, L_0);
		List_1_t1_1909 * L_1 = (__this->___m_consoleTags_6);
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleTag>::Clear() */, L_1);
		Console_RebuildDisplayableLogs_m8_996(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.DebugPRO.Console::RebuildDisplayableLogs()
extern TypeInfo* ConsoleLog_t8_170_il2cpp_TypeInfo_var;
extern "C" void Console_RebuildDisplayableLogs_m8_996 (Console_t8_169 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ConsoleLog_t8_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2096);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	ConsoleLog_t8_170  V_2 = {0};
	ConsoleLog_t8_170  V_3 = {0};
	{
		List_1_t1_1908 * L_0 = (__this->___m_displayableConsoleLogsList_5);
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Clear() */, L_0);
		Initobj (ConsoleLog_t8_170_il2cpp_TypeInfo_var, (&V_3));
		ConsoleLog_t8_170  L_1 = V_3;
		__this->___m_selectedConsoleLog_19 = L_1;
		Console_set_InfoLogsCounter_m8_986(__this, 0, /*hidden argument*/NULL);
		Console_set_WarningLogsCounter_m8_988(__this, 0, /*hidden argument*/NULL);
		Console_set_ErrorLogsCounter_m8_990(__this, 0, /*hidden argument*/NULL);
		List_1_t1_1908 * L_2 = (__this->___m_consoleLogsList_4);
		NullCheck(L_2);
		int32_t L_3 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::get_Count() */, L_2);
		V_0 = L_3;
		V_1 = 0;
		goto IL_005b;
	}

IL_0042:
	{
		List_1_t1_1908 * L_4 = (__this->___m_consoleLogsList_4);
		int32_t L_5 = V_1;
		NullCheck(L_4);
		ConsoleLog_t8_170  L_6 = (ConsoleLog_t8_170 )VirtFuncInvoker1< ConsoleLog_t8_170 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::get_Item(System.Int32) */, L_4, L_5);
		V_2 = L_6;
		ConsoleLog_t8_170  L_7 = V_2;
		Console_AddToDisplayableLogList_m8_997(__this, L_7, /*hidden argument*/NULL);
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_9 = V_1;
		int32_t L_10 = V_0;
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_0042;
		}
	}
	{
		return;
	}
}
// System.Boolean VoxelBusters.DebugPRO.Console::AddToDisplayableLogList(VoxelBusters.DebugPRO.Internal.ConsoleLog)
extern "C" bool Console_AddToDisplayableLogList_m8_997 (Console_t8_169 * __this, ConsoleLog_t8_170  ____consoleLog, const MethodInfo* method)
{
	int32_t V_0 = 0;
	ConsoleTag_t8_171 * V_1 = {0};
	{
		int32_t L_0 = ConsoleLog_get_TagID_m8_1014((&____consoleLog), /*hidden argument*/NULL);
		V_0 = L_0;
		List_1_t1_1909 * L_1 = (__this->___m_consoleTags_6);
		int32_t L_2 = V_0;
		NullCheck(L_1);
		ConsoleTag_t8_171 * L_3 = (ConsoleTag_t8_171 *)VirtFuncInvoker1< ConsoleTag_t8_171 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleTag>::get_Item(System.Int32) */, L_1, L_2);
		V_1 = L_3;
		ConsoleTag_t8_171 * L_4 = V_1;
		NullCheck(L_4);
		bool L_5 = ConsoleTag_get_IsActive_m8_1036(L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0022;
		}
	}
	{
		return 0;
	}

IL_0022:
	{
		int32_t L_6 = ConsoleLog_get_Type_m8_1018((&____consoleLog), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)8))))
		{
			goto IL_0042;
		}
	}
	{
		int32_t L_7 = Console_get_InfoLogsCounter_m8_985(__this, /*hidden argument*/NULL);
		Console_set_InfoLogsCounter_m8_986(__this, ((int32_t)((int32_t)L_7+(int32_t)1)), /*hidden argument*/NULL);
		goto IL_0070;
	}

IL_0042:
	{
		int32_t L_8 = ConsoleLog_get_Type_m8_1018((&____consoleLog), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_8) == ((uint32_t)4))))
		{
			goto IL_0062;
		}
	}
	{
		int32_t L_9 = Console_get_WarningLogsCounter_m8_987(__this, /*hidden argument*/NULL);
		Console_set_WarningLogsCounter_m8_988(__this, ((int32_t)((int32_t)L_9+(int32_t)1)), /*hidden argument*/NULL);
		goto IL_0070;
	}

IL_0062:
	{
		int32_t L_10 = Console_get_ErrorLogsCounter_m8_989(__this, /*hidden argument*/NULL);
		Console_set_ErrorLogsCounter_m8_990(__this, ((int32_t)((int32_t)L_10+(int32_t)1)), /*hidden argument*/NULL);
	}

IL_0070:
	{
		int32_t L_11 = ConsoleLog_get_Type_m8_1018((&____consoleLog), /*hidden argument*/NULL);
		int32_t L_12 = (__this->___m_allowedLogTypes_9);
		if (((int32_t)((int32_t)L_11&(int32_t)L_12)))
		{
			goto IL_0085;
		}
	}
	{
		return 0;
	}

IL_0085:
	{
		List_1_t1_1908 * L_13 = (__this->___m_displayableConsoleLogsList_5);
		ConsoleLog_t8_170  L_14 = ____consoleLog;
		NullCheck(L_13);
		VirtActionInvoker1< ConsoleLog_t8_170  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Add(!0) */, L_13, L_14);
		return 1;
	}
}
// VoxelBusters.DebugPRO.Internal.ConsoleTag VoxelBusters.DebugPRO.Console::GetConsoleTag(System.String)
extern TypeInfo* U3CGetConsoleTagU3Ec__AnonStorey7_t8_167_il2cpp_TypeInfo_var;
extern TypeInfo* Func_2_t5_19_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CGetConsoleTagU3Ec__AnonStorey7_U3CU3Em__5_m8_973_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m5_59_MethodInfo_var;
extern const MethodInfo* Enumerable_FirstOrDefault_TisConsoleTag_t8_171_m5_60_MethodInfo_var;
extern "C" ConsoleTag_t8_171 * Console_GetConsoleTag_m8_998 (Console_t8_169 * __this, String_t* ____tagName, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CGetConsoleTagU3Ec__AnonStorey7_t8_167_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2101);
		Func_2_t5_19_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2102);
		U3CGetConsoleTagU3Ec__AnonStorey7_U3CU3Em__5_m8_973_MethodInfo_var = il2cpp_codegen_method_info_from_index(554);
		Func_2__ctor_m5_59_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484203);
		Enumerable_FirstOrDefault_TisConsoleTag_t8_171_m5_60_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484204);
		s_Il2CppMethodIntialized = true;
	}
	U3CGetConsoleTagU3Ec__AnonStorey7_t8_167 * V_0 = {0};
	{
		U3CGetConsoleTagU3Ec__AnonStorey7_t8_167 * L_0 = (U3CGetConsoleTagU3Ec__AnonStorey7_t8_167 *)il2cpp_codegen_object_new (U3CGetConsoleTagU3Ec__AnonStorey7_t8_167_il2cpp_TypeInfo_var);
		U3CGetConsoleTagU3Ec__AnonStorey7__ctor_m8_972(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetConsoleTagU3Ec__AnonStorey7_t8_167 * L_1 = V_0;
		String_t* L_2 = ____tagName;
		NullCheck(L_1);
		L_1->____tagName_0 = L_2;
		List_1_t1_1909 * L_3 = (__this->___m_consoleTags_6);
		NullCheck(L_3);
		int32_t L_4 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleTag>::get_Count() */, L_3);
		if (L_4)
		{
			goto IL_001f;
		}
	}
	{
		return (ConsoleTag_t8_171 *)NULL;
	}

IL_001f:
	{
		List_1_t1_1909 * L_5 = (__this->___m_consoleTags_6);
		U3CGetConsoleTagU3Ec__AnonStorey7_t8_167 * L_6 = V_0;
		IntPtr_t L_7 = { (void*)U3CGetConsoleTagU3Ec__AnonStorey7_U3CU3Em__5_m8_973_MethodInfo_var };
		Func_2_t5_19 * L_8 = (Func_2_t5_19 *)il2cpp_codegen_object_new (Func_2_t5_19_il2cpp_TypeInfo_var);
		Func_2__ctor_m5_59(L_8, L_6, L_7, /*hidden argument*/Func_2__ctor_m5_59_MethodInfo_var);
		ConsoleTag_t8_171 * L_9 = Enumerable_FirstOrDefault_TisConsoleTag_t8_171_m5_60(NULL /*static, unused*/, L_5, L_8, /*hidden argument*/Enumerable_FirstOrDefault_TisConsoleTag_t8_171_m5_60_MethodInfo_var);
		return L_9;
	}
}
// System.Int32 VoxelBusters.DebugPRO.Console::GetIndexOfConsoleTag(System.String)
extern TypeInfo* U3CGetIndexOfConsoleTagU3Ec__AnonStorey8_t8_168_il2cpp_TypeInfo_var;
extern TypeInfo* Predicate_1_t1_1930_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CGetIndexOfConsoleTagU3Ec__AnonStorey8_U3CU3Em__6_m8_975_MethodInfo_var;
extern const MethodInfo* Predicate_1__ctor_m1_15062_MethodInfo_var;
extern const MethodInfo* List_1_FindIndex_m1_15063_MethodInfo_var;
extern "C" int32_t Console_GetIndexOfConsoleTag_m8_999 (Console_t8_169 * __this, String_t* ____tagName, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CGetIndexOfConsoleTagU3Ec__AnonStorey8_t8_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2103);
		Predicate_1_t1_1930_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2104);
		U3CGetIndexOfConsoleTagU3Ec__AnonStorey8_U3CU3Em__6_m8_975_MethodInfo_var = il2cpp_codegen_method_info_from_index(557);
		Predicate_1__ctor_m1_15062_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484206);
		List_1_FindIndex_m1_15063_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484207);
		s_Il2CppMethodIntialized = true;
	}
	U3CGetIndexOfConsoleTagU3Ec__AnonStorey8_t8_168 * V_0 = {0};
	{
		U3CGetIndexOfConsoleTagU3Ec__AnonStorey8_t8_168 * L_0 = (U3CGetIndexOfConsoleTagU3Ec__AnonStorey8_t8_168 *)il2cpp_codegen_object_new (U3CGetIndexOfConsoleTagU3Ec__AnonStorey8_t8_168_il2cpp_TypeInfo_var);
		U3CGetIndexOfConsoleTagU3Ec__AnonStorey8__ctor_m8_974(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetIndexOfConsoleTagU3Ec__AnonStorey8_t8_168 * L_1 = V_0;
		String_t* L_2 = ____tagName;
		NullCheck(L_1);
		L_1->____tagName_0 = L_2;
		List_1_t1_1909 * L_3 = (__this->___m_consoleTags_6);
		NullCheck(L_3);
		int32_t L_4 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleTag>::get_Count() */, L_3);
		if (L_4)
		{
			goto IL_001f;
		}
	}
	{
		return (-1);
	}

IL_001f:
	{
		List_1_t1_1909 * L_5 = (__this->___m_consoleTags_6);
		U3CGetIndexOfConsoleTagU3Ec__AnonStorey8_t8_168 * L_6 = V_0;
		IntPtr_t L_7 = { (void*)U3CGetIndexOfConsoleTagU3Ec__AnonStorey8_U3CU3Em__6_m8_975_MethodInfo_var };
		Predicate_1_t1_1930 * L_8 = (Predicate_1_t1_1930 *)il2cpp_codegen_object_new (Predicate_1_t1_1930_il2cpp_TypeInfo_var);
		Predicate_1__ctor_m1_15062(L_8, L_6, L_7, /*hidden argument*/Predicate_1__ctor_m1_15062_MethodInfo_var);
		NullCheck(L_5);
		int32_t L_9 = List_1_FindIndex_m1_15063(L_5, L_8, /*hidden argument*/List_1_FindIndex_m1_15063_MethodInfo_var);
		return L_9;
	}
}
// System.Boolean VoxelBusters.DebugPRO.Console::IgnoreConsoleLog(System.String)
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5270;
extern "C" bool Console_IgnoreConsoleLog_m8_1000 (Console_t8_169 * __this, String_t* ____tagName, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5270 = il2cpp_codegen_string_literal_from_index(5270);
		s_Il2CppMethodIntialized = true;
	}
	ConsoleTag_t8_171 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		bool L_0 = Console_get_IsDebugMode_m8_978(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return 1;
	}

IL_000c:
	{
		String_t* L_1 = ____tagName;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_IsNullOrEmpty_m1_521(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		____tagName = _stringLiteral5270;
	}

IL_001e:
	{
		String_t* L_3 = ____tagName;
		ConsoleTag_t8_171 * L_4 = Console_GetConsoleTag_m8_998(__this, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		ConsoleTag_t8_171 * L_5 = V_0;
		if (!L_5)
		{
			goto IL_0039;
		}
	}
	{
		ConsoleTag_t8_171 * L_6 = V_0;
		NullCheck(L_6);
		bool L_7 = ConsoleTag_get_Ignore_m8_1038(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0039;
		}
	}
	{
		return 1;
	}

IL_0039:
	{
		return 0;
	}
}
// System.Void VoxelBusters.DebugPRO.Console::Log(System.String,System.Object,VoxelBusters.DebugPRO.Internal.eConsoleLogType,UnityEngine.Object,System.Int32)
extern TypeInfo* ConsoleTag_t8_171_il2cpp_TypeInfo_var;
extern "C" void Console_Log_m8_1001 (Console_t8_169 * __this, String_t* ____tagName, Object_t * ____message, int32_t ____logType, Object_t6_5 * ____context, int32_t ____skipStackFrameCount, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ConsoleTag_t8_171_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2097);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	ConsoleTag_t8_171 * V_1 = {0};
	int32_t V_2 = 0;
	ConsoleLog_t8_170  V_3 = {0};
	{
		String_t* L_0 = ____tagName;
		bool L_1 = Console_IgnoreConsoleLog_m8_1000(__this, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		int32_t L_2 = ____skipStackFrameCount;
		____skipStackFrameCount = ((int32_t)((int32_t)L_2+(int32_t)1));
		String_t* L_3 = ____tagName;
		int32_t L_4 = Console_GetIndexOfConsoleTag_m8_999(__this, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((!(((uint32_t)L_5) == ((uint32_t)(-1)))))
		{
			goto IL_0043;
		}
	}
	{
		List_1_t1_1909 * L_6 = (__this->___m_consoleTags_6);
		String_t* L_7 = ____tagName;
		ConsoleTag_t8_171 * L_8 = (ConsoleTag_t8_171 *)il2cpp_codegen_object_new (ConsoleTag_t8_171_il2cpp_TypeInfo_var);
		ConsoleTag__ctor_m8_1033(L_8, L_7, 1, 0, /*hidden argument*/NULL);
		NullCheck(L_6);
		VirtActionInvoker1< ConsoleTag_t8_171 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleTag>::Add(!0) */, L_6, L_8);
		List_1_t1_1909 * L_9 = (__this->___m_consoleTags_6);
		NullCheck(L_9);
		int32_t L_10 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleTag>::get_Count() */, L_9);
		V_0 = ((int32_t)((int32_t)L_10-(int32_t)1));
	}

IL_0043:
	{
		List_1_t1_1909 * L_11 = (__this->___m_consoleTags_6);
		int32_t L_12 = V_0;
		NullCheck(L_11);
		ConsoleTag_t8_171 * L_13 = (ConsoleTag_t8_171 *)VirtFuncInvoker1< ConsoleTag_t8_171 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleTag>::get_Item(System.Int32) */, L_11, L_12);
		V_1 = L_13;
		ConsoleTag_t8_171 * L_14 = V_1;
		NullCheck(L_14);
		bool L_15 = ConsoleTag_get_Ignore_m8_1038(L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_005c;
		}
	}
	{
		return;
	}

IL_005c:
	{
		List_1_t1_1908 * L_16 = (__this->___m_consoleLogsList_4);
		NullCheck(L_16);
		int32_t L_17 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::get_Count() */, L_16);
		V_2 = ((int32_t)((int32_t)L_17+(int32_t)1));
		int32_t L_18 = V_2;
		int32_t L_19 = V_0;
		Object_t * L_20 = ____message;
		NullCheck(L_20);
		String_t* L_21 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_20);
		int32_t L_22 = ____logType;
		Object_t6_5 * L_23 = ____context;
		int32_t L_24 = ____skipStackFrameCount;
		ConsoleLog__ctor_m8_1011((&V_3), L_18, L_19, L_21, L_22, L_23, L_24, /*hidden argument*/NULL);
		ConsoleLog_t8_170  L_25 = V_3;
		NativeBinding_Log_m8_971(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.DebugPRO.Console::ClearConsole()
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern "C" void Console_ClearConsole_m8_1002 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_t8_169 * L_0 = Console_get_Instance_m8_991(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_1 = Object_op_Equality_m6_721(NULL /*static, unused*/, L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_t8_169 * L_2 = Console_get_Instance_m8_991(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		Console_Clear_m8_995(L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.DebugPRO.Console::AddIgnoreTag(System.String)
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern TypeInfo* ConsoleTag_t8_171_il2cpp_TypeInfo_var;
extern "C" void Console_AddIgnoreTag_m8_1003 (Object_t * __this /* static, unused */, String_t* ____tag, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		ConsoleTag_t8_171_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2097);
		s_Il2CppMethodIntialized = true;
	}
	ConsoleTag_t8_171 * V_0 = {0};
	ConsoleTag_t8_171 * V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_t8_169 * L_0 = Console_get_Instance_m8_991(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_1 = Object_op_Equality_m6_721(NULL /*static, unused*/, L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_t8_169 * L_2 = Console_get_Instance_m8_991(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_3 = ____tag;
		NullCheck(L_2);
		ConsoleTag_t8_171 * L_4 = Console_GetConsoleTag_m8_998(L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		ConsoleTag_t8_171 * L_5 = V_0;
		if (L_5)
		{
			goto IL_003e;
		}
	}
	{
		String_t* L_6 = ____tag;
		ConsoleTag_t8_171 * L_7 = (ConsoleTag_t8_171 *)il2cpp_codegen_object_new (ConsoleTag_t8_171_il2cpp_TypeInfo_var);
		ConsoleTag__ctor_m8_1033(L_7, L_6, 1, 0, /*hidden argument*/NULL);
		V_1 = L_7;
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_t8_169 * L_8 = Console_get_Instance_m8_991(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		List_1_t1_1909 * L_9 = (L_8->___m_consoleTags_6);
		ConsoleTag_t8_171 * L_10 = V_1;
		NullCheck(L_9);
		VirtActionInvoker1< ConsoleTag_t8_171 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleTag>::Add(!0) */, L_9, L_10);
		ConsoleTag_t8_171 * L_11 = V_1;
		V_0 = L_11;
	}

IL_003e:
	{
		ConsoleTag_t8_171 * L_12 = V_0;
		NullCheck(L_12);
		ConsoleTag_set_Ignore_m8_1039(L_12, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.DebugPRO.Console::RemoveIgnoreTag(System.String)
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern "C" void Console_RemoveIgnoreTag_m8_1004 (Object_t * __this /* static, unused */, String_t* ____tag, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		s_Il2CppMethodIntialized = true;
	}
	ConsoleTag_t8_171 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_t8_169 * L_0 = Console_get_Instance_m8_991(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_1 = Object_op_Equality_m6_721(NULL /*static, unused*/, L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_t8_169 * L_2 = Console_get_Instance_m8_991(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_3 = ____tag;
		NullCheck(L_2);
		ConsoleTag_t8_171 * L_4 = Console_GetConsoleTag_m8_998(L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		ConsoleTag_t8_171 * L_5 = V_0;
		if (!L_5)
		{
			goto IL_002a;
		}
	}
	{
		ConsoleTag_t8_171 * L_6 = V_0;
		NullCheck(L_6);
		ConsoleTag_set_Ignore_m8_1039(L_6, 0, /*hidden argument*/NULL);
	}

IL_002a:
	{
		return;
	}
}
// System.Void VoxelBusters.DebugPRO.Console::DrawLine(System.String,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Color,System.Single,System.Boolean)
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern "C" void Console_DrawLine_m8_1005 (Object_t * __this /* static, unused */, String_t* ____tag, Vector3_t6_48  ____start, Vector3_t6_48  ____end, Color_t6_40  ____color, float ____duration, bool ____depthTest, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_t8_169 * L_0 = Console_get_Instance_m8_991(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_1 = Object_op_Equality_m6_721(NULL /*static, unused*/, L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_t8_169 * L_2 = Console_get_Instance_m8_991(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_3 = ____tag;
		NullCheck(L_2);
		bool L_4 = Console_IgnoreConsoleLog_m8_1000(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0022;
		}
	}
	{
		return;
	}

IL_0022:
	{
		Vector3_t6_48  L_5 = ____start;
		Vector3_t6_48  L_6 = ____end;
		Color_t6_40  L_7 = ____color;
		float L_8 = ____duration;
		bool L_9 = ____depthTest;
		Debug_DrawLine_m6_626(NULL /*static, unused*/, L_5, L_6, L_7, L_8, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.DebugPRO.Console::DrawRay(System.String,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Color,System.Single,System.Boolean)
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern "C" void Console_DrawRay_m8_1006 (Object_t * __this /* static, unused */, String_t* ____tag, Vector3_t6_48  ____start, Vector3_t6_48  ____direction, Color_t6_40  ____color, float ____duration, bool ____depthTest, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_t8_169 * L_0 = Console_get_Instance_m8_991(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_1 = Object_op_Equality_m6_721(NULL /*static, unused*/, L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_t8_169 * L_2 = Console_get_Instance_m8_991(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_3 = ____tag;
		NullCheck(L_2);
		bool L_4 = Console_IgnoreConsoleLog_m8_1000(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0022;
		}
	}
	{
		return;
	}

IL_0022:
	{
		Vector3_t6_48  L_5 = ____start;
		Vector3_t6_48  L_6 = ____direction;
		Color_t6_40  L_7 = ____color;
		float L_8 = ____duration;
		bool L_9 = ____depthTest;
		Debug_DrawRay_m6_628(NULL /*static, unused*/, L_5, L_6, L_7, L_8, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.DebugPRO.Console::Log(System.String,System.Object,UnityEngine.Object)
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern "C" void Console_Log_m8_1007 (Object_t * __this /* static, unused */, String_t* ____tag, Object_t * ____message, Object_t6_5 * ____context, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_t8_169 * L_0 = Console_get_Instance_m8_991(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_1 = Object_op_Equality_m6_721(NULL /*static, unused*/, L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_t8_169 * L_2 = Console_get_Instance_m8_991(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_3 = ____tag;
		Object_t * L_4 = ____message;
		Object_t6_5 * L_5 = ____context;
		NullCheck(L_2);
		Console_Log_m8_1001(L_2, L_3, L_4, 8, L_5, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.DebugPRO.Console::LogWarning(System.String,System.Object,UnityEngine.Object)
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern "C" void Console_LogWarning_m8_1008 (Object_t * __this /* static, unused */, String_t* ____tag, Object_t * ____message, Object_t6_5 * ____context, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_t8_169 * L_0 = Console_get_Instance_m8_991(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_1 = Object_op_Equality_m6_721(NULL /*static, unused*/, L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_t8_169 * L_2 = Console_get_Instance_m8_991(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_3 = ____tag;
		Object_t * L_4 = ____message;
		Object_t6_5 * L_5 = ____context;
		NullCheck(L_2);
		Console_Log_m8_1001(L_2, L_3, L_4, 4, L_5, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.DebugPRO.Console::LogError(System.String,System.Object,UnityEngine.Object)
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern "C" void Console_LogError_m8_1009 (Object_t * __this /* static, unused */, String_t* ____tag, Object_t * ____message, Object_t6_5 * ____context, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_t8_169 * L_0 = Console_get_Instance_m8_991(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_1 = Object_op_Equality_m6_721(NULL /*static, unused*/, L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_t8_169 * L_2 = Console_get_Instance_m8_991(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_3 = ____tag;
		Object_t * L_4 = ____message;
		Object_t6_5 * L_5 = ____context;
		NullCheck(L_2);
		Console_Log_m8_1001(L_2, L_3, L_4, 1, L_5, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.DebugPRO.Console::LogException(System.String,System.Exception,UnityEngine.Object)
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern "C" void Console_LogException_m8_1010 (Object_t * __this /* static, unused */, String_t* ____tag, Exception_t1_33 * ____exception, Object_t6_5 * ____context, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_t8_169 * L_0 = Console_get_Instance_m8_991(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_1 = Object_op_Equality_m6_721(NULL /*static, unused*/, L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_t8_169 * L_2 = Console_get_Instance_m8_991(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_3 = ____tag;
		Exception_t1_33 * L_4 = ____exception;
		Object_t6_5 * L_5 = ____context;
		NullCheck(L_2);
		Console_Log_m8_1001(L_2, L_3, L_4, ((int32_t)16), L_5, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.DebugPRO.Internal.ConsoleLog::.ctor(System.Int32,System.Int32,System.String,VoxelBusters.DebugPRO.Internal.eConsoleLogType,UnityEngine.Object,System.Int32)
extern TypeInfo* StackTrace_t1_336_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4828;
extern "C" void ConsoleLog__ctor_m8_1011 (ConsoleLog_t8_170 * __this, int32_t ____logID, int32_t ____tagID, String_t* ____message, int32_t ____type, Object_t6_5 * ____context, int32_t ____skipFrames, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StackTrace_t1_336_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral4828 = il2cpp_codegen_string_literal_from_index(4828);
		s_Il2CppMethodIntialized = true;
	}
	StackTrace_t1_336 * V_0 = {0};
	StackFrame_t1_334 * V_1 = {0};
	{
		int32_t L_0 = ____logID;
		ConsoleLog_set_ID_m8_1013(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = ____tagID;
		ConsoleLog_set_TagID_m8_1015(__this, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ____message;
		ConsoleLog_set_Message_m8_1017(__this, L_2, /*hidden argument*/NULL);
		int32_t L_3 = ____type;
		ConsoleLog_set_Type_m8_1019(__this, L_3, /*hidden argument*/NULL);
		Object_t6_5 * L_4 = ____context;
		ConsoleLog_set_Context_m8_1021(__this, L_4, /*hidden argument*/NULL);
		int32_t L_5 = ____skipFrames;
		int32_t L_6 = ((int32_t)((int32_t)L_5+(int32_t)1));
		____skipFrames = L_6;
		StackTrace_t1_336 * L_7 = (StackTrace_t1_336 *)il2cpp_codegen_object_new (StackTrace_t1_336_il2cpp_TypeInfo_var);
		StackTrace__ctor_m1_3641(L_7, L_6, 1, /*hidden argument*/NULL);
		V_0 = L_7;
		StackTrace_t1_336 * L_8 = V_0;
		NullCheck(L_8);
		StackFrame_t1_334 * L_9 = (StackFrame_t1_334 *)VirtFuncInvoker1< StackFrame_t1_334 *, int32_t >::Invoke(5 /* System.Diagnostics.StackFrame System.Diagnostics.StackTrace::GetFrame(System.Int32) */, L_8, 0);
		V_1 = L_9;
		StackFrame_t1_334 * L_10 = V_1;
		NullCheck(L_10);
		String_t* L_11 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Diagnostics.StackFrame::GetFileName() */, L_10);
		__this->___m_callerFileName_6 = L_11;
		StackFrame_t1_334 * L_12 = V_1;
		NullCheck(L_12);
		int32_t L_13 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 System.Diagnostics.StackFrame::GetFileLineNumber() */, L_12);
		__this->___m_callerFileLineNumber_7 = L_13;
		StackTrace_t1_336 * L_14 = V_0;
		ConsoleLog_CollectStackTraceInfo_m8_1026(__this, L_14, /*hidden argument*/NULL);
		String_t* L_15 = ConsoleLog_get_Message_m8_1016(__this, /*hidden argument*/NULL);
		String_t* L_16 = ConsoleLog_get_StackTrace_m8_1022(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_17 = String_Concat_m1_560(NULL /*static, unused*/, L_15, _stringLiteral4828, L_16, /*hidden argument*/NULL);
		ConsoleLog_set_Description_m8_1025(__this, L_17, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 VoxelBusters.DebugPRO.Internal.ConsoleLog::get_ID()
extern "C" int32_t ConsoleLog_get_ID_m8_1012 (ConsoleLog_t8_170 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_ID_0);
		return L_0;
	}
}
// System.Void VoxelBusters.DebugPRO.Internal.ConsoleLog::set_ID(System.Int32)
extern "C" void ConsoleLog_set_ID_m8_1013 (ConsoleLog_t8_170 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_ID_0 = L_0;
		return;
	}
}
// System.Int32 VoxelBusters.DebugPRO.Internal.ConsoleLog::get_TagID()
extern "C" int32_t ConsoleLog_get_TagID_m8_1014 (ConsoleLog_t8_170 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_tagID_1);
		return L_0;
	}
}
// System.Void VoxelBusters.DebugPRO.Internal.ConsoleLog::set_TagID(System.Int32)
extern "C" void ConsoleLog_set_TagID_m8_1015 (ConsoleLog_t8_170 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_tagID_1 = L_0;
		return;
	}
}
// System.String VoxelBusters.DebugPRO.Internal.ConsoleLog::get_Message()
extern "C" String_t* ConsoleLog_get_Message_m8_1016 (ConsoleLog_t8_170 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_message_2);
		return L_0;
	}
}
// System.Void VoxelBusters.DebugPRO.Internal.ConsoleLog::set_Message(System.String)
extern "C" void ConsoleLog_set_Message_m8_1017 (ConsoleLog_t8_170 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_message_2 = L_0;
		return;
	}
}
// VoxelBusters.DebugPRO.Internal.eConsoleLogType VoxelBusters.DebugPRO.Internal.ConsoleLog::get_Type()
extern "C" int32_t ConsoleLog_get_Type_m8_1018 (ConsoleLog_t8_170 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_type_3);
		return L_0;
	}
}
// System.Void VoxelBusters.DebugPRO.Internal.ConsoleLog::set_Type(VoxelBusters.DebugPRO.Internal.eConsoleLogType)
extern "C" void ConsoleLog_set_Type_m8_1019 (ConsoleLog_t8_170 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_type_3 = L_0;
		return;
	}
}
// UnityEngine.Object VoxelBusters.DebugPRO.Internal.ConsoleLog::get_Context()
extern "C" Object_t6_5 * ConsoleLog_get_Context_m8_1020 (ConsoleLog_t8_170 * __this, const MethodInfo* method)
{
	{
		Object_t6_5 * L_0 = (__this->___U3CContextU3Ek__BackingField_8);
		return L_0;
	}
}
// System.Void VoxelBusters.DebugPRO.Internal.ConsoleLog::set_Context(UnityEngine.Object)
extern "C" void ConsoleLog_set_Context_m8_1021 (ConsoleLog_t8_170 * __this, Object_t6_5 * ___value, const MethodInfo* method)
{
	{
		Object_t6_5 * L_0 = ___value;
		__this->___U3CContextU3Ek__BackingField_8 = L_0;
		return;
	}
}
// System.String VoxelBusters.DebugPRO.Internal.ConsoleLog::get_StackTrace()
extern "C" String_t* ConsoleLog_get_StackTrace_m8_1022 (ConsoleLog_t8_170 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_stackTrace_4);
		return L_0;
	}
}
// System.Void VoxelBusters.DebugPRO.Internal.ConsoleLog::set_StackTrace(System.String)
extern "C" void ConsoleLog_set_StackTrace_m8_1023 (ConsoleLog_t8_170 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_stackTrace_4 = L_0;
		return;
	}
}
// System.String VoxelBusters.DebugPRO.Internal.ConsoleLog::get_Description()
extern "C" String_t* ConsoleLog_get_Description_m8_1024 (ConsoleLog_t8_170 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_description_5);
		return L_0;
	}
}
// System.Void VoxelBusters.DebugPRO.Internal.ConsoleLog::set_Description(System.String)
extern "C" void ConsoleLog_set_Description_m8_1025 (ConsoleLog_t8_170 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_description_5 = L_0;
		return;
	}
}
// System.Void VoxelBusters.DebugPRO.Internal.ConsoleLog::CollectStackTraceInfo(System.Diagnostics.StackTrace)
extern TypeInfo* StringBuilder_t1_247_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4252;
extern Il2CppCodeGenString* _stringLiteral5271;
extern "C" void ConsoleLog_CollectStackTraceInfo_m8_1026 (ConsoleLog_t8_170 * __this, StackTrace_t1_336 * ____stackTrace, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringBuilder_t1_247_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral4252 = il2cpp_codegen_string_literal_from_index(4252);
		_stringLiteral5271 = il2cpp_codegen_string_literal_from_index(5271);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t1_247 * V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	StackFrame_t1_334 * V_4 = {0};
	MethodBase_t1_335 * V_5 = {0};
	String_t* V_6 = {0};
	String_t* V_7 = {0};
	String_t* V_8 = {0};
	String_t* V_9 = {0};
	{
		StringBuilder_t1_247 * L_0 = (StringBuilder_t1_247 *)il2cpp_codegen_object_new (StringBuilder_t1_247_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1_12414(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		StackTrace_t1_336 * L_1 = ____stackTrace;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 System.Diagnostics.StackTrace::get_FrameCount() */, L_1);
		V_1 = L_2;
		int32_t L_3 = V_1;
		V_2 = ((int32_t)((int32_t)L_3-(int32_t)1));
		V_3 = 0;
		goto IL_009c;
	}

IL_0018:
	{
		StackTrace_t1_336 * L_4 = ____stackTrace;
		int32_t L_5 = V_3;
		NullCheck(L_4);
		StackFrame_t1_334 * L_6 = (StackFrame_t1_334 *)VirtFuncInvoker1< StackFrame_t1_334 *, int32_t >::Invoke(5 /* System.Diagnostics.StackFrame System.Diagnostics.StackTrace::GetFrame(System.Int32) */, L_4, L_5);
		V_4 = L_6;
		StackFrame_t1_334 * L_7 = V_4;
		NullCheck(L_7);
		MethodBase_t1_335 * L_8 = (MethodBase_t1_335 *)VirtFuncInvoker0< MethodBase_t1_335 * >::Invoke(8 /* System.Reflection.MethodBase System.Diagnostics.StackFrame::GetMethod() */, L_7);
		V_5 = L_8;
		MethodBase_t1_335 * L_9 = V_5;
		NullCheck(L_9);
		String_t* L_10 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_9);
		V_6 = L_10;
		MethodBase_t1_335 * L_11 = V_5;
		NullCheck(L_11);
		Type_t * L_12 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(22 /* System.Type System.Reflection.MemberInfo::get_DeclaringType() */, L_11);
		NullCheck(L_12);
		String_t* L_13 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(159 /* System.String System.Type::get_FullName() */, L_12);
		V_7 = L_13;
		StringBuilder_t1_247 * L_14 = V_0;
		String_t* L_15 = V_7;
		String_t* L_16 = V_6;
		NullCheck(L_14);
		StringBuilder_AppendFormat_m1_12461(L_14, _stringLiteral4252, L_15, L_16, /*hidden argument*/NULL);
		StackFrame_t1_334 * L_17 = V_4;
		NullCheck(L_17);
		String_t* L_18 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Diagnostics.StackFrame::GetFileName() */, L_17);
		V_8 = L_18;
		String_t* L_19 = V_8;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_20 = String_IsNullOrEmpty_m1_521(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_008a;
		}
	}
	{
		String_t* L_21 = V_8;
		String_t* L_22 = ConsoleLog_GetRelativePath_m8_1027(__this, L_21, /*hidden argument*/NULL);
		V_9 = L_22;
		StringBuilder_t1_247 * L_23 = V_0;
		String_t* L_24 = V_9;
		StackFrame_t1_334 * L_25 = V_4;
		NullCheck(L_25);
		int32_t L_26 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 System.Diagnostics.StackFrame::GetFileLineNumber() */, L_25);
		int32_t L_27 = L_26;
		Object_t * L_28 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_27);
		NullCheck(L_23);
		StringBuilder_AppendFormat_m1_12461(L_23, _stringLiteral5271, L_24, L_28, /*hidden argument*/NULL);
	}

IL_008a:
	{
		int32_t L_29 = V_3;
		int32_t L_30 = V_2;
		if ((((int32_t)L_29) >= ((int32_t)L_30)))
		{
			goto IL_0098;
		}
	}
	{
		StringBuilder_t1_247 * L_31 = V_0;
		NullCheck(L_31);
		StringBuilder_AppendLine_m1_12456(L_31, /*hidden argument*/NULL);
	}

IL_0098:
	{
		int32_t L_32 = V_3;
		V_3 = ((int32_t)((int32_t)L_32+(int32_t)1));
	}

IL_009c:
	{
		int32_t L_33 = V_3;
		int32_t L_34 = V_1;
		if ((((int32_t)L_33) < ((int32_t)L_34)))
		{
			goto IL_0018;
		}
	}
	{
		StringBuilder_t1_247 * L_35 = V_0;
		NullCheck(L_35);
		String_t* L_36 = StringBuilder_ToString_m1_12428(L_35, /*hidden argument*/NULL);
		ConsoleLog_set_StackTrace_m8_1023(__this, L_36, /*hidden argument*/NULL);
		return;
	}
}
// System.String VoxelBusters.DebugPRO.Internal.ConsoleLog::GetRelativePath(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5272;
extern "C" String_t* ConsoleLog_GetRelativePath_m8_1027 (ConsoleLog_t8_170 * __this, String_t* ____absolutePath, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5272 = il2cpp_codegen_string_literal_from_index(5272);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ____absolutePath;
		if (!L_0)
		{
			goto IL_0031;
		}
	}
	{
		String_t* L_1 = ____absolutePath;
		String_t* L_2 = Application_get_dataPath_m6_590(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_3 = String_StartsWith_m1_531(L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0031;
		}
	}
	{
		String_t* L_4 = ____absolutePath;
		String_t* L_5 = Application_get_dataPath_m6_590(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		int32_t L_6 = String_get_Length_m1_571(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		String_t* L_7 = String_Substring_m1_454(L_4, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m1_559(NULL /*static, unused*/, _stringLiteral5272, L_7, /*hidden argument*/NULL);
		return L_8;
	}

IL_0031:
	{
		String_t* L_9 = ____absolutePath;
		return L_9;
	}
}
// System.Boolean VoxelBusters.DebugPRO.Internal.ConsoleLog::IsValid()
extern "C" bool ConsoleLog_IsValid_m8_1028 (ConsoleLog_t8_170 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = ConsoleLog_get_ID_m8_1012(__this, /*hidden argument*/NULL);
		return ((((int32_t)L_0) > ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean VoxelBusters.DebugPRO.Internal.ConsoleLog::Equals(VoxelBusters.DebugPRO.Internal.ConsoleLog)
extern "C" bool ConsoleLog_Equals_m8_1029 (ConsoleLog_t8_170 * __this, ConsoleLog_t8_170  ____log, const MethodInfo* method)
{
	{
		int32_t L_0 = ConsoleLog_get_ID_m8_1012(__this, /*hidden argument*/NULL);
		int32_t L_1 = ConsoleLog_get_ID_m8_1012((&____log), /*hidden argument*/NULL);
		return ((((int32_t)L_0) == ((int32_t)L_1))? 1 : 0);
	}
}
// System.Void VoxelBusters.DebugPRO.Internal.ConsoleLog::OnSelect()
extern "C" void ConsoleLog_OnSelect_m8_1030 (ConsoleLog_t8_170 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void VoxelBusters.DebugPRO.Internal.ConsoleLog::OnPress()
extern "C" void ConsoleLog_OnPress_m8_1031 (ConsoleLog_t8_170 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// Conversion methods for marshalling of: VoxelBusters.DebugPRO.Internal.ConsoleLog
extern "C" void ConsoleLog_t8_170_marshal(const ConsoleLog_t8_170& unmarshaled, ConsoleLog_t8_170_marshaled& marshaled)
{
	Il2CppCodeGenException* ___U3CContextU3Ek__BackingField_8Exception = il2cpp_codegen_get_not_supported_exception("Cannot marshal field '<Context>k__BackingField' of type 'ConsoleLog': Reference type field marshaling is not supported.");
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)___U3CContextU3Ek__BackingField_8Exception);
}
extern "C" void ConsoleLog_t8_170_marshal_back(const ConsoleLog_t8_170_marshaled& marshaled, ConsoleLog_t8_170& unmarshaled)
{
	Il2CppCodeGenException* ___U3CContextU3Ek__BackingField_8Exception = il2cpp_codegen_get_not_supported_exception("Cannot marshal field '<Context>k__BackingField' of type 'ConsoleLog': Reference type field marshaling is not supported.");
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)___U3CContextU3Ek__BackingField_8Exception);
}
// Conversion method for clean up from marshalling of: VoxelBusters.DebugPRO.Internal.ConsoleLog
extern "C" void ConsoleLog_t8_170_marshal_cleanup(ConsoleLog_t8_170_marshaled& marshaled)
{
}
// System.Void VoxelBusters.DebugPRO.Internal.ConsoleTag::.ctor()
extern "C" void ConsoleTag__ctor_m8_1032 (ConsoleTag_t8_171 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.DebugPRO.Internal.ConsoleTag::.ctor(System.String,System.Boolean,System.Boolean)
extern "C" void ConsoleTag__ctor_m8_1033 (ConsoleTag_t8_171 * __this, String_t* ____tagName, bool ____isActive, bool ____ignore, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ____tagName;
		ConsoleTag_set_Name_m8_1035(__this, L_0, /*hidden argument*/NULL);
		bool L_1 = ____isActive;
		ConsoleTag_set_IsActive_m8_1037(__this, L_1, /*hidden argument*/NULL);
		bool L_2 = ____ignore;
		ConsoleTag_set_Ignore_m8_1039(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.String VoxelBusters.DebugPRO.Internal.ConsoleTag::get_Name()
extern "C" String_t* ConsoleTag_get_Name_m8_1034 (ConsoleTag_t8_171 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_name_0);
		return L_0;
	}
}
// System.Void VoxelBusters.DebugPRO.Internal.ConsoleTag::set_Name(System.String)
extern "C" void ConsoleTag_set_Name_m8_1035 (ConsoleTag_t8_171 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_name_0 = L_0;
		return;
	}
}
// System.Boolean VoxelBusters.DebugPRO.Internal.ConsoleTag::get_IsActive()
extern "C" bool ConsoleTag_get_IsActive_m8_1036 (ConsoleTag_t8_171 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_isActive_1);
		return L_0;
	}
}
// System.Void VoxelBusters.DebugPRO.Internal.ConsoleTag::set_IsActive(System.Boolean)
extern "C" void ConsoleTag_set_IsActive_m8_1037 (ConsoleTag_t8_171 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___m_isActive_1 = L_0;
		return;
	}
}
// System.Boolean VoxelBusters.DebugPRO.Internal.ConsoleTag::get_Ignore()
extern "C" bool ConsoleTag_get_Ignore_m8_1038 (ConsoleTag_t8_171 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_ignore_2);
		return L_0;
	}
}
// System.Void VoxelBusters.DebugPRO.Internal.ConsoleTag::set_Ignore(System.Boolean)
extern "C" void ConsoleTag_set_Ignore_m8_1039 (ConsoleTag_t8_171 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___m_ignore_2 = L_0;
		return;
	}
}
// System.Void VoxelBusters.DebugPRO.UnityDebugUtility::.ctor()
extern "C" void UnityDebugUtility__ctor_m8_1040 (UnityDebugUtility_t8_173 * __this, const MethodInfo* method)
{
	{
		ScriptableObject__ctor_m6_16(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.DebugPRO.UnityDebugUtility::.cctor()
extern "C" void UnityDebugUtility__cctor_m8_1041 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void VoxelBusters.DebugPRO.UnityDebugUtility::add_LogCallback(UnityEngine.Application/LogCallback)
extern TypeInfo* UnityDebugUtility_t8_173_il2cpp_TypeInfo_var;
extern TypeInfo* LogCallback_t6_83_il2cpp_TypeInfo_var;
extern "C" void UnityDebugUtility_add_LogCallback_m8_1042 (Object_t * __this /* static, unused */, LogCallback_t6_83 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityDebugUtility_t8_173_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2100);
		LogCallback_t6_83_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1608);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityDebugUtility_t8_173_il2cpp_TypeInfo_var);
		LogCallback_t6_83 * L_0 = ((UnityDebugUtility_t8_173_StaticFields*)UnityDebugUtility_t8_173_il2cpp_TypeInfo_var->static_fields)->___LogCallback_3;
		LogCallback_t6_83 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Combine_m1_908(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((UnityDebugUtility_t8_173_StaticFields*)UnityDebugUtility_t8_173_il2cpp_TypeInfo_var->static_fields)->___LogCallback_3 = ((LogCallback_t6_83 *)CastclassSealed(L_2, LogCallback_t6_83_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void VoxelBusters.DebugPRO.UnityDebugUtility::remove_LogCallback(UnityEngine.Application/LogCallback)
extern TypeInfo* UnityDebugUtility_t8_173_il2cpp_TypeInfo_var;
extern TypeInfo* LogCallback_t6_83_il2cpp_TypeInfo_var;
extern "C" void UnityDebugUtility_remove_LogCallback_m8_1043 (Object_t * __this /* static, unused */, LogCallback_t6_83 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityDebugUtility_t8_173_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2100);
		LogCallback_t6_83_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1608);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityDebugUtility_t8_173_il2cpp_TypeInfo_var);
		LogCallback_t6_83 * L_0 = ((UnityDebugUtility_t8_173_StaticFields*)UnityDebugUtility_t8_173_il2cpp_TypeInfo_var->static_fields)->___LogCallback_3;
		LogCallback_t6_83 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Remove_m1_911(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((UnityDebugUtility_t8_173_StaticFields*)UnityDebugUtility_t8_173_il2cpp_TypeInfo_var->static_fields)->___LogCallback_3 = ((LogCallback_t6_83 *)CastclassSealed(L_2, LogCallback_t6_83_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void VoxelBusters.DebugPRO.UnityDebugUtility::EditorUpdate()
extern TypeInfo* UnityDebugUtility_t8_173_il2cpp_TypeInfo_var;
extern TypeInfo* LogCallback_t6_83_il2cpp_TypeInfo_var;
extern const MethodInfo* ScriptableObject_CreateInstance_TisUnityDebugUtility_t8_173_m6_1924_MethodInfo_var;
extern const MethodInfo* UnityDebugUtility_HandleLog_m8_1045_MethodInfo_var;
extern "C" void UnityDebugUtility_EditorUpdate_m8_1044 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityDebugUtility_t8_173_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2100);
		LogCallback_t6_83_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1608);
		ScriptableObject_CreateInstance_TisUnityDebugUtility_t8_173_m6_1924_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484208);
		UnityDebugUtility_HandleLog_m8_1045_MethodInfo_var = il2cpp_codegen_method_info_from_index(561);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityDebugUtility_t8_173_il2cpp_TypeInfo_var);
		UnityDebugUtility_t8_173 * L_0 = ((UnityDebugUtility_t8_173_StaticFields*)UnityDebugUtility_t8_173_il2cpp_TypeInfo_var->static_fields)->___instance_2;
		bool L_1 = Object_op_Equality_m6_721(NULL /*static, unused*/, L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		UnityDebugUtility_t8_173 * L_2 = ScriptableObject_CreateInstance_TisUnityDebugUtility_t8_173_m6_1924(NULL /*static, unused*/, /*hidden argument*/ScriptableObject_CreateInstance_TisUnityDebugUtility_t8_173_m6_1924_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(UnityDebugUtility_t8_173_il2cpp_TypeInfo_var);
		((UnityDebugUtility_t8_173_StaticFields*)UnityDebugUtility_t8_173_il2cpp_TypeInfo_var->static_fields)->___instance_2 = L_2;
	}

IL_001a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityDebugUtility_t8_173_il2cpp_TypeInfo_var);
		UnityDebugUtility_t8_173 * L_3 = ((UnityDebugUtility_t8_173_StaticFields*)UnityDebugUtility_t8_173_il2cpp_TypeInfo_var->static_fields)->___instance_2;
		NullCheck(L_3);
		Object_set_hideFlags_m6_711(L_3, ((int32_t)61), /*hidden argument*/NULL);
		UnityDebugUtility_t8_173 * L_4 = ((UnityDebugUtility_t8_173_StaticFields*)UnityDebugUtility_t8_173_il2cpp_TypeInfo_var->static_fields)->___instance_2;
		IntPtr_t L_5 = { (void*)UnityDebugUtility_HandleLog_m8_1045_MethodInfo_var };
		LogCallback_t6_83 * L_6 = (LogCallback_t6_83 *)il2cpp_codegen_object_new (LogCallback_t6_83_il2cpp_TypeInfo_var);
		LogCallback__ctor_m6_577(L_6, L_4, L_5, /*hidden argument*/NULL);
		Application_remove_logMessageReceived_m6_582(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		UnityDebugUtility_t8_173 * L_7 = ((UnityDebugUtility_t8_173_StaticFields*)UnityDebugUtility_t8_173_il2cpp_TypeInfo_var->static_fields)->___instance_2;
		IntPtr_t L_8 = { (void*)UnityDebugUtility_HandleLog_m8_1045_MethodInfo_var };
		LogCallback_t6_83 * L_9 = (LogCallback_t6_83 *)il2cpp_codegen_object_new (LogCallback_t6_83_il2cpp_TypeInfo_var);
		LogCallback__ctor_m6_577(L_9, L_7, L_8, /*hidden argument*/NULL);
		Application_add_logMessageReceived_m6_581(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.DebugPRO.UnityDebugUtility::HandleLog(System.String,System.String,UnityEngine.LogType)
extern TypeInfo* UnityDebugUtility_t8_173_il2cpp_TypeInfo_var;
extern "C" void UnityDebugUtility_HandleLog_m8_1045 (UnityDebugUtility_t8_173 * __this, String_t* ____message, String_t* ____stackTrace, int32_t ____logType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityDebugUtility_t8_173_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2100);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityDebugUtility_t8_173_il2cpp_TypeInfo_var);
		LogCallback_t6_83 * L_0 = ((UnityDebugUtility_t8_173_StaticFields*)UnityDebugUtility_t8_173_il2cpp_TypeInfo_var->static_fields)->___LogCallback_3;
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityDebugUtility_t8_173_il2cpp_TypeInfo_var);
		LogCallback_t6_83 * L_1 = ((UnityDebugUtility_t8_173_StaticFields*)UnityDebugUtility_t8_173_il2cpp_TypeInfo_var->static_fields)->___LogCallback_3;
		String_t* L_2 = ____message;
		String_t* L_3 = ____stackTrace;
		int32_t L_4 = ____logType;
		NullCheck(L_1);
		LogCallback_Invoke_m6_578(L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.NPDemoBase::.ctor()
extern TypeInfo* StringU5BU5D_t1_238_il2cpp_TypeInfo_var;
extern "C" void NPDemoBase__ctor_m8_1046 (NPDemoBase_t8_174 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringU5BU5D_t1_238_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(35);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___m_additionalInfoTexts_12 = ((StringU5BU5D_t1_238*)SZArrayNew(StringU5BU5D_t1_238_il2cpp_TypeInfo_var, 0));
		DemoSubMenu__ctor_m8_130(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.NPDemoBase::Start()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5273;
extern Il2CppCodeGenString* _stringLiteral268;
extern "C" void NPDemoBase_Start_m8_1047 (NPDemoBase_t8_174 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5273 = il2cpp_codegen_string_literal_from_index(5273);
		_stringLiteral268 = il2cpp_codegen_string_literal_from_index(268);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	{
		DemoSubMenu_Start_m8_131(__this, /*hidden argument*/NULL);
		__this->___m_showThingsToKnow_10 = 1;
		GameObject_t6_97 * L_0 = Component_get_gameObject_m6_725(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = Object_get_name_m6_708(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		NullCheck(L_2);
		String_t* L_4 = String_Replace_m1_536(L_2, _stringLiteral268, L_3, /*hidden argument*/NULL);
		String_t* L_5 = V_0;
		String_t* L_6 = String_Format_m1_549(NULL /*static, unused*/, _stringLiteral5273, L_4, L_5, /*hidden argument*/NULL);
		__this->___m_featureInterfaceInfoText_11 = L_6;
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.NPDemoBase::OnGUIWindow()
extern TypeInfo* GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5274;
extern "C" void NPDemoBase_OnGUIWindow_m8_1048 (NPDemoBase_t8_174 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1651);
		_stringLiteral5274 = il2cpp_codegen_string_literal_from_index(5274);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		DemoSubMenu_OnGUIWindow_m8_133(__this, /*hidden argument*/NULL);
	}

IL_0006:
	try
	{ // begin try (depth: 1)
		{
			GUIScrollView_t8_19 * L_0 = GUIModalWindow_get_RootScrollView_m8_791(__this, /*hidden argument*/NULL);
			NullCheck(L_0);
			GUIScrollView_BeginScrollView_m8_809(L_0, ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
			bool L_1 = NPDemoBase_DisplayThingsToKnow_m8_1049(__this, /*hidden argument*/NULL);
			if (!L_1)
			{
				goto IL_0027;
			}
		}

IL_0022:
		{
			IL2CPP_LEAVE(0x64, FINALLY_0032);
		}

IL_0027:
		{
			VirtActionInvoker0::Invoke(12 /* System.Void VoxelBusters.NativePlugins.Demo.NPDemoBase::DisplayFeatureFunctionalities() */, __this);
			IL2CPP_LEAVE(0x64, FINALLY_0032);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0032;
	}

FINALLY_0032:
	{ // begin finally (depth: 1)
		{
			GUIScrollView_t8_19 * L_2 = GUIModalWindow_get_RootScrollView_m8_791(__this, /*hidden argument*/NULL);
			NullCheck(L_2);
			GUIScrollView_EndScrollView_m8_810(L_2, /*hidden argument*/NULL);
			bool L_3 = (__this->___m_showThingsToKnow_10);
			if (!L_3)
			{
				goto IL_0052;
			}
		}

IL_0048:
		{
			GUILayout_FlexibleSpace_m6_1259(NULL /*static, unused*/, /*hidden argument*/NULL);
			goto IL_0063;
		}

IL_0052:
		{
			DemoSubMenu_DrawResults_m8_136(__this, /*hidden argument*/NULL);
			VirtActionInvoker1< String_t* >::Invoke(11 /* System.Void VoxelBusters.AssetStoreProductUtility.Demo.DemoSubMenu::DrawPopButton(System.String) */, __this, _stringLiteral5274);
		}

IL_0063:
		{
			IL2CPP_END_FINALLY(50)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(50)
	{
		IL2CPP_JUMP_TBL(0x64, IL_0064)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0064:
	{
		return;
	}
}
// System.Boolean VoxelBusters.NativePlugins.Demo.NPDemoBase::DisplayThingsToKnow()
extern TypeInfo* GUIStyle_t6_176_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5275;
extern Il2CppCodeGenString* _stringLiteral5146;
extern Il2CppCodeGenString* _stringLiteral5276;
extern "C" bool NPDemoBase_DisplayThingsToKnow_m8_1049 (NPDemoBase_t8_174 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t6_176_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1648);
		GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1651);
		_stringLiteral5275 = il2cpp_codegen_string_literal_from_index(5275);
		_stringLiteral5146 = il2cpp_codegen_string_literal_from_index(5146);
		_stringLiteral5276 = il2cpp_codegen_string_literal_from_index(5276);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	StringU5BU5D_t1_238* V_1 = {0};
	int32_t V_2 = 0;
	{
		bool L_0 = (__this->___m_showThingsToKnow_10);
		if (!L_0)
		{
			goto IL_008a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle_t6_176 * L_1 = GUIStyle_op_Implicit_m6_1493(NULL /*static, unused*/, _stringLiteral5146, /*hidden argument*/NULL);
		GUILayout_Label_m6_1248(NULL /*static, unused*/, _stringLiteral5275, L_1, ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		String_t* L_2 = (__this->___m_featureInterfaceInfoText_11);
		if (!L_2)
		{
			goto IL_0041;
		}
	}
	{
		String_t* L_3 = (__this->___m_featureInterfaceInfoText_11);
		GUILayout_Box_m6_1250(NULL /*static, unused*/, L_3, ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
	}

IL_0041:
	{
		StringU5BU5D_t1_238* L_4 = (__this->___m_additionalInfoTexts_12);
		V_1 = L_4;
		V_2 = 0;
		goto IL_0063;
	}

IL_004f:
	{
		StringU5BU5D_t1_238* L_5 = V_1;
		int32_t L_6 = V_2;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		V_0 = (*(String_t**)(String_t**)SZArrayLdElema(L_5, L_7, sizeof(String_t*)));
		String_t* L_8 = V_0;
		GUILayout_Box_m6_1250(NULL /*static, unused*/, L_8, ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		int32_t L_9 = V_2;
		V_2 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0063:
	{
		int32_t L_10 = V_2;
		StringU5BU5D_t1_238* L_11 = V_1;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_11)->max_length)))))))
		{
			goto IL_004f;
		}
	}
	{
		bool L_12 = GUILayout_Button_m6_1252(NULL /*static, unused*/, _stringLiteral5276, ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0088;
		}
	}
	{
		__this->___m_showThingsToKnow_10 = 0;
	}

IL_0088:
	{
		return 1;
	}

IL_008a:
	{
		return 0;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.NPDemoBase::DisplayFeatureFunctionalities()
extern "C" void NPDemoBase_DisplayFeatureFunctionalities_m8_1050 (NPDemoBase_t8_174 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.NPDemoBase::AddExtraInfoTexts(System.String[])
extern "C" void NPDemoBase_AddExtraInfoTexts_m8_1051 (NPDemoBase_t8_174 * __this, StringU5BU5D_t1_238* ____infoTexts, const MethodInfo* method)
{
	{
		StringU5BU5D_t1_238* L_0 = ____infoTexts;
		if (L_0)
		{
			goto IL_0007;
		}
	}
	{
		return;
	}

IL_0007:
	{
		StringU5BU5D_t1_238* L_1 = ____infoTexts;
		__this->___m_additionalInfoTexts_12 = L_1;
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.NPDemoBase::SetFeatureInterfaceInfoText(System.String)
extern "C" void NPDemoBase_SetFeatureInterfaceInfoText_m8_1052 (NPDemoBase_t8_174 * __this, String_t* ____newText, const MethodInfo* method)
{
	{
		String_t* L_0 = ____newText;
		__this->___m_featureInterfaceInfoText_11 = L_0;
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.NPDisabledFeatureDemo::.ctor()
extern "C" void NPDisabledFeatureDemo__ctor_m8_1053 (NPDisabledFeatureDemo_t8_175 * __this, const MethodInfo* method)
{
	{
		DemoSubMenu__ctor_m8_130(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.NPDisabledFeatureDemo::Start()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5277;
extern "C" void NPDisabledFeatureDemo_Start_m8_1054 (NPDisabledFeatureDemo_t8_175 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5277 = il2cpp_codegen_string_literal_from_index(5277);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	{
		DemoSubMenu_Start_m8_131(__this, /*hidden argument*/NULL);
		GameObject_t6_97 * L_0 = Component_get_gameObject_m6_725(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = Object_get_name_m6_708(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Format_m1_548(NULL /*static, unused*/, _stringLiteral5277, L_2, /*hidden argument*/NULL);
		__this->___m_enableFeatureInfoText_9 = L_3;
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.NPDisabledFeatureDemo::OnGUIWindow()
extern TypeInfo* GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5274;
extern "C" void NPDisabledFeatureDemo_OnGUIWindow_m8_1055 (NPDisabledFeatureDemo_t8_175 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1651);
		_stringLiteral5274 = il2cpp_codegen_string_literal_from_index(5274);
		s_Il2CppMethodIntialized = true;
	}
	{
		DemoSubMenu_OnGUIWindow_m8_133(__this, /*hidden argument*/NULL);
		String_t* L_0 = (__this->___m_enableFeatureInfoText_9);
		GUILayout_Box_m6_1250(NULL /*static, unused*/, L_0, ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		GUILayout_FlexibleSpace_m6_1259(NULL /*static, unused*/, /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(11 /* System.Void VoxelBusters.AssetStoreProductUtility.Demo.DemoSubMenu::DrawPopButton(System.String) */, __this, _stringLiteral5274);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.AddressBookDemo::.ctor()
extern "C" void AddressBookDemo__ctor_m8_1056 (AddressBookDemo_t8_176 * __this, const MethodInfo* method)
{
	{
		__this->___m_eachRowHeight_11 = (150.0f);
		__this->___m_maxContactsToRender_12 = ((int32_t)50);
		NPDisabledFeatureDemo__ctor_m8_1053(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.BillingDemo::.ctor()
extern "C" void BillingDemo__ctor_m8_1057 (BillingDemo_t8_178 * __this, const MethodInfo* method)
{
	{
		NPDisabledFeatureDemo__ctor_m8_1053(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.GameServicesDemo::.ctor()
extern TypeInfo* StringU5BU5D_t1_238_il2cpp_TypeInfo_var;
extern "C" void GameServicesDemo__ctor_m8_1058 (GameServicesDemo_t8_179 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringU5BU5D_t1_238_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(35);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___m_maxScoreResults_11 = ((int32_t)20);
		__this->___m_curLeaderboardGIDIndex_14 = (-1);
		__this->___m_leaderboardGIDList_15 = ((StringU5BU5D_t1_238*)SZArrayNew(StringU5BU5D_t1_238_il2cpp_TypeInfo_var, 0));
		__this->___m_curAchievementGIDIndex_17 = (-1);
		__this->___m_achievementGIDList_18 = ((StringU5BU5D_t1_238*)SZArrayNew(StringU5BU5D_t1_238_il2cpp_TypeInfo_var, 0));
		NPDisabledFeatureDemo__ctor_m8_1053(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.MediaLibraryDemo::.ctor()
extern "C" void MediaLibraryDemo__ctor_m8_1059 (MediaLibraryDemo_t8_181 * __this, const MethodInfo* method)
{
	{
		NPDisabledFeatureDemo__ctor_m8_1053(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.NetworkConnectivityDemo::.ctor()
extern "C" void NetworkConnectivityDemo__ctor_m8_1060 (NetworkConnectivityDemo_t8_182 * __this, const MethodInfo* method)
{
	{
		NPDisabledFeatureDemo__ctor_m8_1053(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.NotificationDemo::.ctor()
extern TypeInfo* ArrayList_t1_170_il2cpp_TypeInfo_var;
extern "C" void NotificationDemo__ctor_m8_1061 (NotificationDemo_t8_183 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArrayList_t1_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(60);
		s_Il2CppMethodIntialized = true;
	}
	{
		ArrayList_t1_170 * L_0 = (ArrayList_t1_170 *)il2cpp_codegen_object_new (ArrayList_t1_170_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1_3049(L_0, /*hidden argument*/NULL);
		__this->___m_scheduledNotificationIDList_14 = L_0;
		NPDemoBase__ctor_m8_1046(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.NotificationDemo::Start()
extern TypeInfo* StringU5BU5D_t1_238_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5278;
extern Il2CppCodeGenString* _stringLiteral5279;
extern Il2CppCodeGenString* _stringLiteral5280;
extern Il2CppCodeGenString* _stringLiteral5281;
extern "C" void NotificationDemo_Start_m8_1062 (NotificationDemo_t8_183 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringU5BU5D_t1_238_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(35);
		_stringLiteral5278 = il2cpp_codegen_string_literal_from_index(5278);
		_stringLiteral5279 = il2cpp_codegen_string_literal_from_index(5279);
		_stringLiteral5280 = il2cpp_codegen_string_literal_from_index(5280);
		_stringLiteral5281 = il2cpp_codegen_string_literal_from_index(5281);
		s_Il2CppMethodIntialized = true;
	}
	{
		NPDemoBase_Start_m8_1047(__this, /*hidden argument*/NULL);
		StringU5BU5D_t1_238* L_0 = ((StringU5BU5D_t1_238*)SZArrayNew(StringU5BU5D_t1_238_il2cpp_TypeInfo_var, 4));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral5278);
		*((String_t**)(String_t**)SZArrayLdElema(L_0, 0, sizeof(String_t*))) = (String_t*)_stringLiteral5278;
		StringU5BU5D_t1_238* L_1 = L_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, _stringLiteral5279);
		*((String_t**)(String_t**)SZArrayLdElema(L_1, 1, sizeof(String_t*))) = (String_t*)_stringLiteral5279;
		StringU5BU5D_t1_238* L_2 = L_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 2);
		ArrayElementTypeCheck (L_2, _stringLiteral5280);
		*((String_t**)(String_t**)SZArrayLdElema(L_2, 2, sizeof(String_t*))) = (String_t*)_stringLiteral5280;
		StringU5BU5D_t1_238* L_3 = L_2;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 3);
		ArrayElementTypeCheck (L_3, _stringLiteral5281);
		*((String_t**)(String_t**)SZArrayLdElema(L_3, 3, sizeof(String_t*))) = (String_t*)_stringLiteral5281;
		NPDemoBase_AddExtraInfoTexts_m8_1051(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.NotificationDemo::OnEnable()
extern TypeInfo* RegisterForRemoteNotificationCompletion_t8_257_il2cpp_TypeInfo_var;
extern TypeInfo* ReceivedNotificationResponse_t8_258_il2cpp_TypeInfo_var;
extern const MethodInfo* NotificationDemo_DidFinishRegisterForRemoteNotificationEvent_m8_1080_MethodInfo_var;
extern const MethodInfo* NotificationDemo_DidLaunchWithLocalNotificationEvent_m8_1076_MethodInfo_var;
extern const MethodInfo* NotificationDemo_DidLaunchWithRemoteNotificationEvent_m8_1077_MethodInfo_var;
extern const MethodInfo* NotificationDemo_DidReceiveLocalNotificationEvent_m8_1078_MethodInfo_var;
extern const MethodInfo* NotificationDemo_DidReceiveRemoteNotificationEvent_m8_1079_MethodInfo_var;
extern "C" void NotificationDemo_OnEnable_m8_1063 (NotificationDemo_t8_183 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RegisterForRemoteNotificationCompletion_t8_257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2105);
		ReceivedNotificationResponse_t8_258_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2106);
		NotificationDemo_DidFinishRegisterForRemoteNotificationEvent_m8_1080_MethodInfo_var = il2cpp_codegen_method_info_from_index(562);
		NotificationDemo_DidLaunchWithLocalNotificationEvent_m8_1076_MethodInfo_var = il2cpp_codegen_method_info_from_index(563);
		NotificationDemo_DidLaunchWithRemoteNotificationEvent_m8_1077_MethodInfo_var = il2cpp_codegen_method_info_from_index(564);
		NotificationDemo_DidReceiveLocalNotificationEvent_m8_1078_MethodInfo_var = il2cpp_codegen_method_info_from_index(565);
		NotificationDemo_DidReceiveRemoteNotificationEvent_m8_1079_MethodInfo_var = il2cpp_codegen_method_info_from_index(566);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIModalWindow_OnEnable_m8_796(__this, /*hidden argument*/NULL);
		IntPtr_t L_0 = { (void*)NotificationDemo_DidFinishRegisterForRemoteNotificationEvent_m8_1080_MethodInfo_var };
		RegisterForRemoteNotificationCompletion_t8_257 * L_1 = (RegisterForRemoteNotificationCompletion_t8_257 *)il2cpp_codegen_object_new (RegisterForRemoteNotificationCompletion_t8_257_il2cpp_TypeInfo_var);
		RegisterForRemoteNotificationCompletion__ctor_m8_1423(L_1, __this, L_0, /*hidden argument*/NULL);
		NotificationService_add_DidFinishRegisterForRemoteNotificationEvent_m8_1438(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IntPtr_t L_2 = { (void*)NotificationDemo_DidLaunchWithLocalNotificationEvent_m8_1076_MethodInfo_var };
		ReceivedNotificationResponse_t8_258 * L_3 = (ReceivedNotificationResponse_t8_258 *)il2cpp_codegen_object_new (ReceivedNotificationResponse_t8_258_il2cpp_TypeInfo_var);
		ReceivedNotificationResponse__ctor_m8_1427(L_3, __this, L_2, /*hidden argument*/NULL);
		NotificationService_add_DidLaunchWithLocalNotificationEvent_m8_1440(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IntPtr_t L_4 = { (void*)NotificationDemo_DidLaunchWithRemoteNotificationEvent_m8_1077_MethodInfo_var };
		ReceivedNotificationResponse_t8_258 * L_5 = (ReceivedNotificationResponse_t8_258 *)il2cpp_codegen_object_new (ReceivedNotificationResponse_t8_258_il2cpp_TypeInfo_var);
		ReceivedNotificationResponse__ctor_m8_1427(L_5, __this, L_4, /*hidden argument*/NULL);
		NotificationService_add_DidLaunchWithRemoteNotificationEvent_m8_1442(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		IntPtr_t L_6 = { (void*)NotificationDemo_DidReceiveLocalNotificationEvent_m8_1078_MethodInfo_var };
		ReceivedNotificationResponse_t8_258 * L_7 = (ReceivedNotificationResponse_t8_258 *)il2cpp_codegen_object_new (ReceivedNotificationResponse_t8_258_il2cpp_TypeInfo_var);
		ReceivedNotificationResponse__ctor_m8_1427(L_7, __this, L_6, /*hidden argument*/NULL);
		NotificationService_add_DidReceiveLocalNotificationEvent_m8_1444(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		IntPtr_t L_8 = { (void*)NotificationDemo_DidReceiveRemoteNotificationEvent_m8_1079_MethodInfo_var };
		ReceivedNotificationResponse_t8_258 * L_9 = (ReceivedNotificationResponse_t8_258 *)il2cpp_codegen_object_new (ReceivedNotificationResponse_t8_258_il2cpp_TypeInfo_var);
		ReceivedNotificationResponse__ctor_m8_1427(L_9, __this, L_8, /*hidden argument*/NULL);
		NotificationService_add_DidReceiveRemoteNotificationEvent_m8_1446(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.NotificationDemo::OnDisable()
extern TypeInfo* RegisterForRemoteNotificationCompletion_t8_257_il2cpp_TypeInfo_var;
extern TypeInfo* ReceivedNotificationResponse_t8_258_il2cpp_TypeInfo_var;
extern const MethodInfo* NotificationDemo_DidFinishRegisterForRemoteNotificationEvent_m8_1080_MethodInfo_var;
extern const MethodInfo* NotificationDemo_DidLaunchWithLocalNotificationEvent_m8_1076_MethodInfo_var;
extern const MethodInfo* NotificationDemo_DidLaunchWithRemoteNotificationEvent_m8_1077_MethodInfo_var;
extern const MethodInfo* NotificationDemo_DidReceiveLocalNotificationEvent_m8_1078_MethodInfo_var;
extern const MethodInfo* NotificationDemo_DidReceiveRemoteNotificationEvent_m8_1079_MethodInfo_var;
extern "C" void NotificationDemo_OnDisable_m8_1064 (NotificationDemo_t8_183 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RegisterForRemoteNotificationCompletion_t8_257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2105);
		ReceivedNotificationResponse_t8_258_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2106);
		NotificationDemo_DidFinishRegisterForRemoteNotificationEvent_m8_1080_MethodInfo_var = il2cpp_codegen_method_info_from_index(562);
		NotificationDemo_DidLaunchWithLocalNotificationEvent_m8_1076_MethodInfo_var = il2cpp_codegen_method_info_from_index(563);
		NotificationDemo_DidLaunchWithRemoteNotificationEvent_m8_1077_MethodInfo_var = il2cpp_codegen_method_info_from_index(564);
		NotificationDemo_DidReceiveLocalNotificationEvent_m8_1078_MethodInfo_var = il2cpp_codegen_method_info_from_index(565);
		NotificationDemo_DidReceiveRemoteNotificationEvent_m8_1079_MethodInfo_var = il2cpp_codegen_method_info_from_index(566);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIModalWindow_OnDisable_m8_797(__this, /*hidden argument*/NULL);
		IntPtr_t L_0 = { (void*)NotificationDemo_DidFinishRegisterForRemoteNotificationEvent_m8_1080_MethodInfo_var };
		RegisterForRemoteNotificationCompletion_t8_257 * L_1 = (RegisterForRemoteNotificationCompletion_t8_257 *)il2cpp_codegen_object_new (RegisterForRemoteNotificationCompletion_t8_257_il2cpp_TypeInfo_var);
		RegisterForRemoteNotificationCompletion__ctor_m8_1423(L_1, __this, L_0, /*hidden argument*/NULL);
		NotificationService_remove_DidFinishRegisterForRemoteNotificationEvent_m8_1439(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IntPtr_t L_2 = { (void*)NotificationDemo_DidLaunchWithLocalNotificationEvent_m8_1076_MethodInfo_var };
		ReceivedNotificationResponse_t8_258 * L_3 = (ReceivedNotificationResponse_t8_258 *)il2cpp_codegen_object_new (ReceivedNotificationResponse_t8_258_il2cpp_TypeInfo_var);
		ReceivedNotificationResponse__ctor_m8_1427(L_3, __this, L_2, /*hidden argument*/NULL);
		NotificationService_remove_DidLaunchWithLocalNotificationEvent_m8_1441(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IntPtr_t L_4 = { (void*)NotificationDemo_DidLaunchWithRemoteNotificationEvent_m8_1077_MethodInfo_var };
		ReceivedNotificationResponse_t8_258 * L_5 = (ReceivedNotificationResponse_t8_258 *)il2cpp_codegen_object_new (ReceivedNotificationResponse_t8_258_il2cpp_TypeInfo_var);
		ReceivedNotificationResponse__ctor_m8_1427(L_5, __this, L_4, /*hidden argument*/NULL);
		NotificationService_remove_DidLaunchWithRemoteNotificationEvent_m8_1443(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		IntPtr_t L_6 = { (void*)NotificationDemo_DidReceiveLocalNotificationEvent_m8_1078_MethodInfo_var };
		ReceivedNotificationResponse_t8_258 * L_7 = (ReceivedNotificationResponse_t8_258 *)il2cpp_codegen_object_new (ReceivedNotificationResponse_t8_258_il2cpp_TypeInfo_var);
		ReceivedNotificationResponse__ctor_m8_1427(L_7, __this, L_6, /*hidden argument*/NULL);
		NotificationService_remove_DidReceiveLocalNotificationEvent_m8_1445(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		IntPtr_t L_8 = { (void*)NotificationDemo_DidReceiveRemoteNotificationEvent_m8_1079_MethodInfo_var };
		ReceivedNotificationResponse_t8_258 * L_9 = (ReceivedNotificationResponse_t8_258 *)il2cpp_codegen_object_new (ReceivedNotificationResponse_t8_258_il2cpp_TypeInfo_var);
		ReceivedNotificationResponse__ctor_m8_1427(L_9, __this, L_8, /*hidden argument*/NULL);
		NotificationService_remove_DidReceiveRemoteNotificationEvent_m8_1447(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.NotificationDemo::DisplayFeatureFunctionalities()
extern "C" void NotificationDemo_DisplayFeatureFunctionalities_m8_1065 (NotificationDemo_t8_183 * __this, const MethodInfo* method)
{
	{
		NPDemoBase_DisplayFeatureFunctionalities_m8_1050(__this, /*hidden argument*/NULL);
		NotificationDemo_DrawRegisterAPI_m8_1066(__this, /*hidden argument*/NULL);
		NotificationDemo_DrawScheduleNotificationAPI_m8_1067(__this, /*hidden argument*/NULL);
		NotificationDemo_DrawCancelNotificationAPI_m8_1068(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.NotificationDemo::DrawRegisterAPI()
extern TypeInfo* GUIStyle_t6_176_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5282;
extern Il2CppCodeGenString* _stringLiteral5146;
extern Il2CppCodeGenString* _stringLiteral5283;
extern Il2CppCodeGenString* _stringLiteral5284;
extern Il2CppCodeGenString* _stringLiteral5285;
extern Il2CppCodeGenString* _stringLiteral5286;
extern Il2CppCodeGenString* _stringLiteral5287;
extern Il2CppCodeGenString* _stringLiteral5288;
extern Il2CppCodeGenString* _stringLiteral5289;
extern Il2CppCodeGenString* _stringLiteral5290;
extern "C" void NotificationDemo_DrawRegisterAPI_m8_1066 (NotificationDemo_t8_183 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t6_176_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1648);
		GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1651);
		_stringLiteral5282 = il2cpp_codegen_string_literal_from_index(5282);
		_stringLiteral5146 = il2cpp_codegen_string_literal_from_index(5146);
		_stringLiteral5283 = il2cpp_codegen_string_literal_from_index(5283);
		_stringLiteral5284 = il2cpp_codegen_string_literal_from_index(5284);
		_stringLiteral5285 = il2cpp_codegen_string_literal_from_index(5285);
		_stringLiteral5286 = il2cpp_codegen_string_literal_from_index(5286);
		_stringLiteral5287 = il2cpp_codegen_string_literal_from_index(5287);
		_stringLiteral5288 = il2cpp_codegen_string_literal_from_index(5288);
		_stringLiteral5289 = il2cpp_codegen_string_literal_from_index(5289);
		_stringLiteral5290 = il2cpp_codegen_string_literal_from_index(5290);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle_t6_176 * L_0 = GUIStyle_op_Implicit_m6_1493(NULL /*static, unused*/, _stringLiteral5146, /*hidden argument*/NULL);
		GUILayout_Label_m6_1248(NULL /*static, unused*/, _stringLiteral5282, L_0, ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		GUILayout_Box_m6_1250(NULL /*static, unused*/, _stringLiteral5283, ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		GUILayout_Box_m6_1250(NULL /*static, unused*/, _stringLiteral5284, ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		GUILayout_Box_m6_1250(NULL /*static, unused*/, _stringLiteral5285, ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		bool L_1 = GUILayout_Button_m6_1252(NULL /*static, unused*/, _stringLiteral5286, ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_2 = (__this->___m_notificationType_13);
		NotificationDemo_RegisterNotificationTypes_m8_1069(__this, L_2, /*hidden argument*/NULL);
	}

IL_006b:
	{
		bool L_3 = GUILayout_Button_m6_1252(NULL /*static, unused*/, _stringLiteral5287, ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0086;
		}
	}
	{
		NotificationDemo_RegisterForRemoteNotifications_m8_1070(__this, /*hidden argument*/NULL);
	}

IL_0086:
	{
		GUILayout_Box_m6_1250(NULL /*static, unused*/, _stringLiteral5288, ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		bool L_4 = GUILayout_Button_m6_1252(NULL /*static, unused*/, _stringLiteral5289, ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_00bc;
		}
	}
	{
		NotificationDemo_UnregisterForRemoteNotifications_m8_1071(__this, /*hidden argument*/NULL);
		DemoSubMenu_AddNewResult_m8_135(__this, _stringLiteral5290, /*hidden argument*/NULL);
	}

IL_00bc:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.NotificationDemo::DrawScheduleNotificationAPI()
extern TypeInfo* GUIStyle_t6_176_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5291;
extern Il2CppCodeGenString* _stringLiteral5146;
extern Il2CppCodeGenString* _stringLiteral5292;
extern Il2CppCodeGenString* _stringLiteral5293;
extern Il2CppCodeGenString* _stringLiteral5294;
extern Il2CppCodeGenString* _stringLiteral5295;
extern "C" void NotificationDemo_DrawScheduleNotificationAPI_m8_1067 (NotificationDemo_t8_183 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t6_176_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1648);
		GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1651);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5291 = il2cpp_codegen_string_literal_from_index(5291);
		_stringLiteral5146 = il2cpp_codegen_string_literal_from_index(5146);
		_stringLiteral5292 = il2cpp_codegen_string_literal_from_index(5292);
		_stringLiteral5293 = il2cpp_codegen_string_literal_from_index(5293);
		_stringLiteral5294 = il2cpp_codegen_string_literal_from_index(5294);
		_stringLiteral5295 = il2cpp_codegen_string_literal_from_index(5295);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	String_t* V_1 = {0};
	String_t* V_2 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle_t6_176 * L_0 = GUIStyle_op_Implicit_m6_1493(NULL /*static, unused*/, _stringLiteral5146, /*hidden argument*/NULL);
		GUILayout_Label_m6_1248(NULL /*static, unused*/, _stringLiteral5291, L_0, ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		bool L_1 = GUILayout_Button_m6_1252(NULL /*static, unused*/, _stringLiteral5292, ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_005e;
		}
	}
	{
		CrossPlatformNotification_t8_259 * L_2 = NotificationDemo_CreateNotification_m8_1082(__this, (((int64_t)((int64_t)((int32_t)60)))), 0, /*hidden argument*/NULL);
		String_t* L_3 = NotificationDemo_ScheduleLocalNotification_m8_1072(__this, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		ArrayList_t1_170 * L_4 = (__this->___m_scheduledNotificationIDList_14);
		String_t* L_5 = V_0;
		NullCheck(L_4);
		VirtFuncInvoker1< int32_t, Object_t * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_4, L_5);
		String_t* L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m1_559(NULL /*static, unused*/, _stringLiteral5293, L_6, /*hidden argument*/NULL);
		DemoSubMenu_AddNewResult_m8_135(__this, L_7, /*hidden argument*/NULL);
	}

IL_005e:
	{
		bool L_8 = GUILayout_Button_m6_1252(NULL /*static, unused*/, _stringLiteral5294, ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_00a2;
		}
	}
	{
		CrossPlatformNotification_t8_259 * L_9 = NotificationDemo_CreateNotification_m8_1082(__this, (((int64_t)((int64_t)((int32_t)60)))), 1, /*hidden argument*/NULL);
		String_t* L_10 = NotificationDemo_ScheduleLocalNotification_m8_1072(__this, L_9, /*hidden argument*/NULL);
		V_1 = L_10;
		ArrayList_t1_170 * L_11 = (__this->___m_scheduledNotificationIDList_14);
		String_t* L_12 = V_1;
		NullCheck(L_11);
		VirtFuncInvoker1< int32_t, Object_t * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_11, L_12);
		String_t* L_13 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m1_559(NULL /*static, unused*/, _stringLiteral5293, L_13, /*hidden argument*/NULL);
		DemoSubMenu_AddNewResult_m8_135(__this, L_14, /*hidden argument*/NULL);
	}

IL_00a2:
	{
		bool L_15 = GUILayout_Button_m6_1252(NULL /*static, unused*/, _stringLiteral5295, ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_00e6;
		}
	}
	{
		CrossPlatformNotification_t8_259 * L_16 = NotificationDemo_CreateNotification_m8_1082(__this, (((int64_t)((int64_t)((int32_t)60)))), 2, /*hidden argument*/NULL);
		String_t* L_17 = NotificationDemo_ScheduleLocalNotification_m8_1072(__this, L_16, /*hidden argument*/NULL);
		V_2 = L_17;
		ArrayList_t1_170 * L_18 = (__this->___m_scheduledNotificationIDList_14);
		String_t* L_19 = V_2;
		NullCheck(L_18);
		VirtFuncInvoker1< int32_t, Object_t * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_18, L_19);
		String_t* L_20 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = String_Concat_m1_559(NULL /*static, unused*/, _stringLiteral5293, L_20, /*hidden argument*/NULL);
		DemoSubMenu_AddNewResult_m8_135(__this, L_21, /*hidden argument*/NULL);
	}

IL_00e6:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.NotificationDemo::DrawCancelNotificationAPI()
extern TypeInfo* GUIStyle_t6_176_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5296;
extern Il2CppCodeGenString* _stringLiteral5146;
extern Il2CppCodeGenString* _stringLiteral5297;
extern Il2CppCodeGenString* _stringLiteral5298;
extern Il2CppCodeGenString* _stringLiteral5299;
extern Il2CppCodeGenString* _stringLiteral5300;
extern Il2CppCodeGenString* _stringLiteral5301;
extern Il2CppCodeGenString* _stringLiteral5302;
extern Il2CppCodeGenString* _stringLiteral5303;
extern "C" void NotificationDemo_DrawCancelNotificationAPI_m8_1068 (NotificationDemo_t8_183 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t6_176_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1648);
		GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1651);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5296 = il2cpp_codegen_string_literal_from_index(5296);
		_stringLiteral5146 = il2cpp_codegen_string_literal_from_index(5146);
		_stringLiteral5297 = il2cpp_codegen_string_literal_from_index(5297);
		_stringLiteral5298 = il2cpp_codegen_string_literal_from_index(5298);
		_stringLiteral5299 = il2cpp_codegen_string_literal_from_index(5299);
		_stringLiteral5300 = il2cpp_codegen_string_literal_from_index(5300);
		_stringLiteral5301 = il2cpp_codegen_string_literal_from_index(5301);
		_stringLiteral5302 = il2cpp_codegen_string_literal_from_index(5302);
		_stringLiteral5303 = il2cpp_codegen_string_literal_from_index(5303);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle_t6_176 * L_0 = GUIStyle_op_Implicit_m6_1493(NULL /*static, unused*/, _stringLiteral5146, /*hidden argument*/NULL);
		GUILayout_Label_m6_1248(NULL /*static, unused*/, _stringLiteral5296, L_0, ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		bool L_1 = GUILayout_Button_m6_1252(NULL /*static, unused*/, _stringLiteral5297, ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0086;
		}
	}
	{
		ArrayList_t1_170 * L_2 = (__this->___m_scheduledNotificationIDList_14);
		NullCheck(L_2);
		int32_t L_3 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_2);
		if ((((int32_t)L_3) <= ((int32_t)0)))
		{
			goto IL_007b;
		}
	}
	{
		ArrayList_t1_170 * L_4 = (__this->___m_scheduledNotificationIDList_14);
		NullCheck(L_4);
		Object_t * L_5 = (Object_t *)VirtFuncInvoker1< Object_t *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_4, 0);
		V_0 = ((String_t*)IsInstSealed(L_5, String_t_il2cpp_TypeInfo_var));
		String_t* L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m1_559(NULL /*static, unused*/, _stringLiteral5298, L_6, /*hidden argument*/NULL);
		DemoSubMenu_AddNewResult_m8_135(__this, L_7, /*hidden argument*/NULL);
		String_t* L_8 = V_0;
		NotificationDemo_CancelLocalNotification_m8_1073(__this, L_8, /*hidden argument*/NULL);
		ArrayList_t1_170 * L_9 = (__this->___m_scheduledNotificationIDList_14);
		NullCheck(L_9);
		VirtActionInvoker1< int32_t >::Invoke(43 /* System.Void System.Collections.ArrayList::RemoveAt(System.Int32) */, L_9, 0);
		goto IL_0086;
	}

IL_007b:
	{
		DemoSubMenu_AddNewResult_m8_135(__this, _stringLiteral5299, /*hidden argument*/NULL);
	}

IL_0086:
	{
		bool L_10 = GUILayout_Button_m6_1252(NULL /*static, unused*/, _stringLiteral5300, ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_00b7;
		}
	}
	{
		ArrayList_t1_170 * L_11 = (__this->___m_scheduledNotificationIDList_14);
		NullCheck(L_11);
		VirtActionInvoker0::Invoke(31 /* System.Void System.Collections.ArrayList::Clear() */, L_11);
		NotificationDemo_CancelAllLocalNotifications_m8_1074(__this, /*hidden argument*/NULL);
		DemoSubMenu_AddNewResult_m8_135(__this, _stringLiteral5301, /*hidden argument*/NULL);
	}

IL_00b7:
	{
		bool L_12 = GUILayout_Button_m6_1252(NULL /*static, unused*/, _stringLiteral5302, ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_00dd;
		}
	}
	{
		NotificationDemo_ClearNotifications_m8_1075(__this, /*hidden argument*/NULL);
		DemoSubMenu_AddNewResult_m8_135(__this, _stringLiteral5303, /*hidden argument*/NULL);
	}

IL_00dd:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.NotificationDemo::RegisterNotificationTypes(VoxelBusters.NativePlugins.NotificationType)
extern Il2CppCodeGenString* _stringLiteral5304;
extern Il2CppCodeGenString* _stringLiteral5305;
extern Il2CppCodeGenString* _stringLiteral5306;
extern Il2CppCodeGenString* _stringLiteral5307;
extern Il2CppCodeGenString* _stringLiteral5308;
extern "C" void NotificationDemo_RegisterNotificationTypes_m8_1069 (NotificationDemo_t8_183 * __this, int32_t ____notificationTypes, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral5304 = il2cpp_codegen_string_literal_from_index(5304);
		_stringLiteral5305 = il2cpp_codegen_string_literal_from_index(5305);
		_stringLiteral5306 = il2cpp_codegen_string_literal_from_index(5306);
		_stringLiteral5307 = il2cpp_codegen_string_literal_from_index(5307);
		_stringLiteral5308 = il2cpp_codegen_string_literal_from_index(5308);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotificationService_t8_261 * L_0 = NPBinding_get_NotificationService_m8_1929(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = ____notificationTypes;
		NullCheck(L_0);
		VirtActionInvoker1< int32_t >::Invoke(7 /* System.Void VoxelBusters.NativePlugins.NotificationService::RegisterNotificationTypes(VoxelBusters.NativePlugins.NotificationType) */, L_0, L_1);
		int32_t L_2 = ____notificationTypes;
		if (L_2)
		{
			goto IL_001d;
		}
	}
	{
		DemoSubMenu_AddNewResult_m8_135(__this, _stringLiteral5304, /*hidden argument*/NULL);
		return;
	}

IL_001d:
	{
		DemoSubMenu_AddNewResult_m8_135(__this, _stringLiteral5305, /*hidden argument*/NULL);
		int32_t L_3 = ____notificationTypes;
		if (!((int32_t)((int32_t)L_3&(int32_t)1)))
		{
			goto IL_003b;
		}
	}
	{
		DemoSubMenu_AppendResult_m8_134(__this, _stringLiteral5306, /*hidden argument*/NULL);
	}

IL_003b:
	{
		int32_t L_4 = ____notificationTypes;
		if (!((int32_t)((int32_t)L_4&(int32_t)2)))
		{
			goto IL_004e;
		}
	}
	{
		DemoSubMenu_AppendResult_m8_134(__this, _stringLiteral5307, /*hidden argument*/NULL);
	}

IL_004e:
	{
		int32_t L_5 = ____notificationTypes;
		if (!((int32_t)((int32_t)L_5&(int32_t)4)))
		{
			goto IL_0061;
		}
	}
	{
		DemoSubMenu_AppendResult_m8_134(__this, _stringLiteral5308, /*hidden argument*/NULL);
	}

IL_0061:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.NotificationDemo::RegisterForRemoteNotifications()
extern "C" void NotificationDemo_RegisterForRemoteNotifications_m8_1070 (NotificationDemo_t8_183 * __this, const MethodInfo* method)
{
	{
		NotificationService_t8_261 * L_0 = NPBinding_get_NotificationService_m8_1929(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(12 /* System.Void VoxelBusters.NativePlugins.NotificationService::RegisterForRemoteNotifications() */, L_0);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.NotificationDemo::UnregisterForRemoteNotifications()
extern "C" void NotificationDemo_UnregisterForRemoteNotifications_m8_1071 (NotificationDemo_t8_183 * __this, const MethodInfo* method)
{
	{
		NotificationService_t8_261 * L_0 = NPBinding_get_NotificationService_m8_1929(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(13 /* System.Void VoxelBusters.NativePlugins.NotificationService::UnregisterForRemoteNotifications() */, L_0);
		return;
	}
}
// System.String VoxelBusters.NativePlugins.Demo.NotificationDemo::ScheduleLocalNotification(VoxelBusters.NativePlugins.CrossPlatformNotification)
extern "C" String_t* NotificationDemo_ScheduleLocalNotification_m8_1072 (NotificationDemo_t8_183 * __this, CrossPlatformNotification_t8_259 * ____notification, const MethodInfo* method)
{
	{
		NotificationService_t8_261 * L_0 = NPBinding_get_NotificationService_m8_1929(NULL /*static, unused*/, /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_1 = ____notification;
		NullCheck(L_0);
		String_t* L_2 = (String_t*)VirtFuncInvoker1< String_t*, CrossPlatformNotification_t8_259 * >::Invoke(8 /* System.String VoxelBusters.NativePlugins.NotificationService::ScheduleLocalNotification(VoxelBusters.NativePlugins.CrossPlatformNotification) */, L_0, L_1);
		return L_2;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.NotificationDemo::CancelLocalNotification(System.String)
extern "C" void NotificationDemo_CancelLocalNotification_m8_1073 (NotificationDemo_t8_183 * __this, String_t* ____notificationID, const MethodInfo* method)
{
	{
		NotificationService_t8_261 * L_0 = NPBinding_get_NotificationService_m8_1929(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = ____notificationID;
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void VoxelBusters.NativePlugins.NotificationService::CancelLocalNotification(System.String) */, L_0, L_1);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.NotificationDemo::CancelAllLocalNotifications()
extern "C" void NotificationDemo_CancelAllLocalNotifications_m8_1074 (NotificationDemo_t8_183 * __this, const MethodInfo* method)
{
	{
		NotificationService_t8_261 * L_0 = NPBinding_get_NotificationService_m8_1929(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(10 /* System.Void VoxelBusters.NativePlugins.NotificationService::CancelAllLocalNotification() */, L_0);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.NotificationDemo::ClearNotifications()
extern "C" void NotificationDemo_ClearNotifications_m8_1075 (NotificationDemo_t8_183 * __this, const MethodInfo* method)
{
	{
		NotificationService_t8_261 * L_0 = NPBinding_get_NotificationService_m8_1929(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(11 /* System.Void VoxelBusters.NativePlugins.NotificationService::ClearNotifications() */, L_0);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.NotificationDemo::DidLaunchWithLocalNotificationEvent(VoxelBusters.NativePlugins.CrossPlatformNotification)
extern Il2CppCodeGenString* _stringLiteral5309;
extern "C" void NotificationDemo_DidLaunchWithLocalNotificationEvent_m8_1076 (NotificationDemo_t8_183 * __this, CrossPlatformNotification_t8_259 * ____notification, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral5309 = il2cpp_codegen_string_literal_from_index(5309);
		s_Il2CppMethodIntialized = true;
	}
	{
		DemoSubMenu_AddNewResult_m8_135(__this, _stringLiteral5309, /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_0 = ____notification;
		NotificationDemo_AppendNotificationResult_m8_1081(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.NotificationDemo::DidLaunchWithRemoteNotificationEvent(VoxelBusters.NativePlugins.CrossPlatformNotification)
extern Il2CppCodeGenString* _stringLiteral5310;
extern "C" void NotificationDemo_DidLaunchWithRemoteNotificationEvent_m8_1077 (NotificationDemo_t8_183 * __this, CrossPlatformNotification_t8_259 * ____notification, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral5310 = il2cpp_codegen_string_literal_from_index(5310);
		s_Il2CppMethodIntialized = true;
	}
	{
		DemoSubMenu_AddNewResult_m8_135(__this, _stringLiteral5310, /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_0 = ____notification;
		NotificationDemo_AppendNotificationResult_m8_1081(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.NotificationDemo::DidReceiveLocalNotificationEvent(VoxelBusters.NativePlugins.CrossPlatformNotification)
extern Il2CppCodeGenString* _stringLiteral5311;
extern "C" void NotificationDemo_DidReceiveLocalNotificationEvent_m8_1078 (NotificationDemo_t8_183 * __this, CrossPlatformNotification_t8_259 * ____notification, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral5311 = il2cpp_codegen_string_literal_from_index(5311);
		s_Il2CppMethodIntialized = true;
	}
	{
		DemoSubMenu_AddNewResult_m8_135(__this, _stringLiteral5311, /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_0 = ____notification;
		NotificationDemo_AppendNotificationResult_m8_1081(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.NotificationDemo::DidReceiveRemoteNotificationEvent(VoxelBusters.NativePlugins.CrossPlatformNotification)
extern Il2CppCodeGenString* _stringLiteral5312;
extern "C" void NotificationDemo_DidReceiveRemoteNotificationEvent_m8_1079 (NotificationDemo_t8_183 * __this, CrossPlatformNotification_t8_259 * ____notification, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral5312 = il2cpp_codegen_string_literal_from_index(5312);
		s_Il2CppMethodIntialized = true;
	}
	{
		DemoSubMenu_AddNewResult_m8_135(__this, _stringLiteral5312, /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_0 = ____notification;
		NotificationDemo_AppendNotificationResult_m8_1081(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.NotificationDemo::DidFinishRegisterForRemoteNotificationEvent(System.String,System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5313;
extern Il2CppCodeGenString* _stringLiteral5314;
extern "C" void NotificationDemo_DidFinishRegisterForRemoteNotificationEvent_m8_1080 (NotificationDemo_t8_183 * __this, String_t* ____deviceToken, String_t* ____error, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5313 = il2cpp_codegen_string_literal_from_index(5313);
		_stringLiteral5314 = il2cpp_codegen_string_literal_from_index(5314);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ____error;
		String_t* L_1 = StringExtensions_GetPrintableString_m8_224(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Format_m1_548(NULL /*static, unused*/, _stringLiteral5313, L_1, /*hidden argument*/NULL);
		DemoSubMenu_AddNewResult_m8_135(__this, L_2, /*hidden argument*/NULL);
		String_t* L_3 = ____deviceToken;
		String_t* L_4 = String_Concat_m1_559(NULL /*static, unused*/, _stringLiteral5314, L_3, /*hidden argument*/NULL);
		DemoSubMenu_AppendResult_m8_134(__this, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.NotificationDemo::AppendNotificationResult(VoxelBusters.NativePlugins.CrossPlatformNotification)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionary_t1_35_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerable_t1_1677_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t1_193_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5315;
extern Il2CppCodeGenString* _stringLiteral5316;
extern Il2CppCodeGenString* _stringLiteral4828;
extern Il2CppCodeGenString* _stringLiteral4818;
extern Il2CppCodeGenString* _stringLiteral5317;
extern "C" void NotificationDemo_AppendNotificationResult_m8_1081 (NotificationDemo_t8_183 * __this, CrossPlatformNotification_t8_259 * ____notification, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		IDictionary_t1_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		IEnumerable_t1_1677_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(253);
		IEnumerator_t1_193_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(144);
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		_stringLiteral5315 = il2cpp_codegen_string_literal_from_index(5315);
		_stringLiteral5316 = il2cpp_codegen_string_literal_from_index(5316);
		_stringLiteral4828 = il2cpp_codegen_string_literal_from_index(4828);
		_stringLiteral4818 = il2cpp_codegen_string_literal_from_index(4818);
		_stringLiteral5317 = il2cpp_codegen_string_literal_from_index(5317);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	String_t* V_1 = {0};
	Object_t * V_2 = {0};
	String_t* V_3 = {0};
	String_t* V_4 = {0};
	Object_t * V_5 = {0};
	String_t* V_6 = {0};
	Object_t * V_7 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		CrossPlatformNotification_t8_259 * L_0 = ____notification;
		NullCheck(L_0);
		String_t* L_1 = CrossPlatformNotification_get_AlertBody_m8_1516(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		CrossPlatformNotification_t8_259 * L_2 = ____notification;
		NullCheck(L_2);
		String_t* L_3 = CrossPlatformNotification_GetNotificationID_m8_1530(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		CrossPlatformNotification_t8_259 * L_4 = ____notification;
		NullCheck(L_4);
		Object_t * L_5 = CrossPlatformNotification_get_UserInfo_m8_1522(L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		String_t* L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m1_559(NULL /*static, unused*/, _stringLiteral5315, L_6, /*hidden argument*/NULL);
		DemoSubMenu_AppendResult_m8_134(__this, L_7, /*hidden argument*/NULL);
		V_3 = (String_t*)NULL;
		Object_t * L_8 = V_2;
		if (!L_8)
		{
			goto IL_00b4;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_3 = L_9;
		Object_t * L_10 = V_2;
		NullCheck(L_10);
		Object_t * L_11 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(4 /* System.Collections.ICollection System.Collections.IDictionary::get_Keys() */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_10);
		NullCheck(L_11);
		Object_t * L_12 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t1_1677_il2cpp_TypeInfo_var, L_11);
		V_5 = L_12;
	}

IL_0041:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0088;
		}

IL_0046:
		{
			Object_t * L_13 = V_5;
			NullCheck(L_13);
			Object_t * L_14 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_13);
			V_4 = ((String_t*)CastclassSealed(L_14, String_t_il2cpp_TypeInfo_var));
			String_t* L_15 = V_3;
			V_6 = L_15;
			ObjectU5BU5D_t1_272* L_16 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 5));
			String_t* L_17 = V_6;
			NullCheck(L_16);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 0);
			ArrayElementTypeCheck (L_16, L_17);
			*((Object_t **)(Object_t **)SZArrayLdElema(L_16, 0, sizeof(Object_t *))) = (Object_t *)L_17;
			ObjectU5BU5D_t1_272* L_18 = L_16;
			String_t* L_19 = V_4;
			NullCheck(L_18);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 1);
			ArrayElementTypeCheck (L_18, L_19);
			*((Object_t **)(Object_t **)SZArrayLdElema(L_18, 1, sizeof(Object_t *))) = (Object_t *)L_19;
			ObjectU5BU5D_t1_272* L_20 = L_18;
			NullCheck(L_20);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 2);
			ArrayElementTypeCheck (L_20, _stringLiteral5316);
			*((Object_t **)(Object_t **)SZArrayLdElema(L_20, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral5316;
			ObjectU5BU5D_t1_272* L_21 = L_20;
			Object_t * L_22 = V_2;
			String_t* L_23 = V_4;
			NullCheck(L_22);
			Object_t * L_24 = (Object_t *)InterfaceFuncInvoker1< Object_t *, Object_t * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_22, L_23);
			NullCheck(L_21);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 3);
			ArrayElementTypeCheck (L_21, L_24);
			*((Object_t **)(Object_t **)SZArrayLdElema(L_21, 3, sizeof(Object_t *))) = (Object_t *)L_24;
			ObjectU5BU5D_t1_272* L_25 = L_21;
			NullCheck(L_25);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_25, 4);
			ArrayElementTypeCheck (L_25, _stringLiteral4828);
			*((Object_t **)(Object_t **)SZArrayLdElema(L_25, 4, sizeof(Object_t *))) = (Object_t *)_stringLiteral4828;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_26 = String_Concat_m1_562(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
			V_3 = L_26;
		}

IL_0088:
		{
			Object_t * L_27 = V_5;
			NullCheck(L_27);
			bool L_28 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_27);
			if (L_28)
			{
				goto IL_0046;
			}
		}

IL_0094:
		{
			IL2CPP_LEAVE(0xAF, FINALLY_0099);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0099;
	}

FINALLY_0099:
	{ // begin finally (depth: 1)
		{
			Object_t * L_29 = V_5;
			V_7 = ((Object_t *)IsInst(L_29, IDisposable_t1_1035_il2cpp_TypeInfo_var));
			Object_t * L_30 = V_7;
			if (L_30)
			{
				goto IL_00a7;
			}
		}

IL_00a6:
		{
			IL2CPP_END_FINALLY(153)
		}

IL_00a7:
		{
			Object_t * L_31 = V_7;
			NullCheck(L_31);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_31);
			IL2CPP_END_FINALLY(153)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(153)
	{
		IL2CPP_JUMP_TBL(0xAF, IL_00af)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_00af:
	{
		goto IL_00ba;
	}

IL_00b4:
	{
		V_3 = _stringLiteral4818;
	}

IL_00ba:
	{
		String_t* L_32 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_33 = String_Concat_m1_559(NULL /*static, unused*/, _stringLiteral5317, L_32, /*hidden argument*/NULL);
		DemoSubMenu_AppendResult_m8_134(__this, L_33, /*hidden argument*/NULL);
		return;
	}
}
// VoxelBusters.NativePlugins.CrossPlatformNotification VoxelBusters.NativePlugins.Demo.NotificationDemo::CreateNotification(System.Int64,VoxelBusters.NativePlugins.eNotificationRepeatInterval)
extern TypeInfo* Dictionary_2_t1_1839_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionary_t1_35_il2cpp_TypeInfo_var;
extern TypeInfo* iOSSpecificProperties_t8_266_il2cpp_TypeInfo_var;
extern TypeInfo* AndroidSpecificProperties_t8_265_il2cpp_TypeInfo_var;
extern TypeInfo* CrossPlatformNotification_t8_259_il2cpp_TypeInfo_var;
extern TypeInfo* DateTime_t1_150_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_14949_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral424;
extern Il2CppCodeGenString* _stringLiteral5114;
extern Il2CppCodeGenString* _stringLiteral5115;
extern Il2CppCodeGenString* _stringLiteral5116;
extern Il2CppCodeGenString* _stringLiteral5117;
extern Il2CppCodeGenString* _stringLiteral5118;
extern Il2CppCodeGenString* _stringLiteral5119;
extern Il2CppCodeGenString* _stringLiteral5120;
extern Il2CppCodeGenString* _stringLiteral5121;
extern "C" CrossPlatformNotification_t8_259 * NotificationDemo_CreateNotification_m8_1082 (NotificationDemo_t8_183 * __this, int64_t ____fireAfterSec, int32_t ____repeatInterval, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t1_1839_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1603);
		IDictionary_t1_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		iOSSpecificProperties_t8_266_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1946);
		AndroidSpecificProperties_t8_265_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1947);
		CrossPlatformNotification_t8_259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1948);
		DateTime_t1_150_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Dictionary_2__ctor_m1_14949_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483789);
		_stringLiteral424 = il2cpp_codegen_string_literal_from_index(424);
		_stringLiteral5114 = il2cpp_codegen_string_literal_from_index(5114);
		_stringLiteral5115 = il2cpp_codegen_string_literal_from_index(5115);
		_stringLiteral5116 = il2cpp_codegen_string_literal_from_index(5116);
		_stringLiteral5117 = il2cpp_codegen_string_literal_from_index(5117);
		_stringLiteral5118 = il2cpp_codegen_string_literal_from_index(5118);
		_stringLiteral5119 = il2cpp_codegen_string_literal_from_index(5119);
		_stringLiteral5120 = il2cpp_codegen_string_literal_from_index(5120);
		_stringLiteral5121 = il2cpp_codegen_string_literal_from_index(5121);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	iOSSpecificProperties_t8_266 * V_1 = {0};
	AndroidSpecificProperties_t8_265 * V_2 = {0};
	CrossPlatformNotification_t8_259 * V_3 = {0};
	DateTime_t1_150  V_4 = {0};
	{
		Dictionary_2_t1_1839 * L_0 = (Dictionary_2_t1_1839 *)il2cpp_codegen_object_new (Dictionary_2_t1_1839_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_14949(L_0, /*hidden argument*/Dictionary_2__ctor_m1_14949_MethodInfo_var);
		V_0 = L_0;
		Object_t * L_1 = V_0;
		NullCheck(L_1);
		InterfaceActionInvoker2< Object_t *, Object_t * >::Invoke(3 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_1, _stringLiteral424, _stringLiteral5114);
		iOSSpecificProperties_t8_266 * L_2 = (iOSSpecificProperties_t8_266 *)il2cpp_codegen_object_new (iOSSpecificProperties_t8_266_il2cpp_TypeInfo_var);
		iOSSpecificProperties__ctor_m8_1502(L_2, /*hidden argument*/NULL);
		V_1 = L_2;
		iOSSpecificProperties_t8_266 * L_3 = V_1;
		NullCheck(L_3);
		iOSSpecificProperties_set_HasAction_m8_1507(L_3, 1, /*hidden argument*/NULL);
		iOSSpecificProperties_t8_266 * L_4 = V_1;
		NullCheck(L_4);
		iOSSpecificProperties_set_AlertAction_m8_1505(L_4, _stringLiteral5115, /*hidden argument*/NULL);
		AndroidSpecificProperties_t8_265 * L_5 = (AndroidSpecificProperties_t8_265 *)il2cpp_codegen_object_new (AndroidSpecificProperties_t8_265_il2cpp_TypeInfo_var);
		AndroidSpecificProperties__ctor_m8_1488(L_5, /*hidden argument*/NULL);
		V_2 = L_5;
		AndroidSpecificProperties_t8_265 * L_6 = V_2;
		NullCheck(L_6);
		AndroidSpecificProperties_set_ContentTitle_m8_1491(L_6, _stringLiteral5116, /*hidden argument*/NULL);
		AndroidSpecificProperties_t8_265 * L_7 = V_2;
		NullCheck(L_7);
		AndroidSpecificProperties_set_TickerText_m8_1493(L_7, _stringLiteral5117, /*hidden argument*/NULL);
		AndroidSpecificProperties_t8_265 * L_8 = V_2;
		NullCheck(L_8);
		AndroidSpecificProperties_set_LargeIcon_m8_1499(L_8, _stringLiteral5118, /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_9 = (CrossPlatformNotification_t8_259 *)il2cpp_codegen_object_new (CrossPlatformNotification_t8_259_il2cpp_TypeInfo_var);
		CrossPlatformNotification__ctor_m8_1514(L_9, /*hidden argument*/NULL);
		V_3 = L_9;
		CrossPlatformNotification_t8_259 * L_10 = V_3;
		NullCheck(L_10);
		CrossPlatformNotification_set_AlertBody_m8_1517(L_10, _stringLiteral5119, /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_11 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		DateTime_t1_150  L_12 = DateTime_get_Now_m1_13826(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_4 = L_12;
		int64_t L_13 = ____fireAfterSec;
		DateTime_t1_150  L_14 = DateTime_AddSeconds_m1_13840((&V_4), (((double)((double)L_13))), /*hidden argument*/NULL);
		NullCheck(L_11);
		CrossPlatformNotification_set_FireDate_m8_1519(L_11, L_14, /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_15 = V_3;
		int32_t L_16 = ____repeatInterval;
		NullCheck(L_15);
		CrossPlatformNotification_set_RepeatInterval_m8_1521(L_15, L_16, /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_17 = V_3;
		NullCheck(L_17);
		CrossPlatformNotification_set_SoundName_m8_1525(L_17, _stringLiteral5120, /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_18 = V_3;
		Object_t * L_19 = V_0;
		NullCheck(L_18);
		CrossPlatformNotification_set_UserInfo_m8_1523(L_18, L_19, /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_20 = V_3;
		iOSSpecificProperties_t8_266 * L_21 = V_1;
		NullCheck(L_20);
		CrossPlatformNotification_set_iOSProperties_m8_1527(L_20, L_21, /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_22 = V_3;
		AndroidSpecificProperties_t8_265 * L_23 = V_2;
		NullCheck(L_22);
		CrossPlatformNotification_set_AndroidProperties_m8_1529(L_22, L_23, /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_24 = V_3;
		NullCheck(L_24);
		String_t* L_25 = CrossPlatformNotification_GetNotificationID_m8_1530(L_24, /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_26 = V_3;
		NullCheck(L_26);
		DateTime_t1_150  L_27 = CrossPlatformNotification_get_FireDate_m8_1518(L_26, /*hidden argument*/NULL);
		DateTime_t1_150  L_28 = L_27;
		Object_t * L_29 = Box(DateTime_t1_150_il2cpp_TypeInfo_var, &L_28);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_30 = String_Format_m1_549(NULL /*static, unused*/, _stringLiteral5121, L_25, L_29, /*hidden argument*/NULL);
		Debug_Log_m6_631(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_31 = V_3;
		return L_31;
	}
}
// VoxelBusters.NativePlugins.CrossPlatformNotification VoxelBusters.NativePlugins.Demo.NotificationDemo::CreateNotification(System.DateTime,VoxelBusters.NativePlugins.eNotificationRepeatInterval)
extern TypeInfo* Dictionary_2_t1_1839_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionary_t1_35_il2cpp_TypeInfo_var;
extern TypeInfo* iOSSpecificProperties_t8_266_il2cpp_TypeInfo_var;
extern TypeInfo* AndroidSpecificProperties_t8_265_il2cpp_TypeInfo_var;
extern TypeInfo* CrossPlatformNotification_t8_259_il2cpp_TypeInfo_var;
extern TypeInfo* DateTime_t1_150_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_14949_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral424;
extern Il2CppCodeGenString* _stringLiteral5114;
extern Il2CppCodeGenString* _stringLiteral5115;
extern Il2CppCodeGenString* _stringLiteral5116;
extern Il2CppCodeGenString* _stringLiteral5117;
extern Il2CppCodeGenString* _stringLiteral5118;
extern Il2CppCodeGenString* _stringLiteral5119;
extern Il2CppCodeGenString* _stringLiteral5120;
extern Il2CppCodeGenString* _stringLiteral5121;
extern "C" CrossPlatformNotification_t8_259 * NotificationDemo_CreateNotification_m8_1083 (NotificationDemo_t8_183 * __this, DateTime_t1_150  ____time, int32_t ____repeatInterval, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t1_1839_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1603);
		IDictionary_t1_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		iOSSpecificProperties_t8_266_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1946);
		AndroidSpecificProperties_t8_265_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1947);
		CrossPlatformNotification_t8_259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1948);
		DateTime_t1_150_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Dictionary_2__ctor_m1_14949_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483789);
		_stringLiteral424 = il2cpp_codegen_string_literal_from_index(424);
		_stringLiteral5114 = il2cpp_codegen_string_literal_from_index(5114);
		_stringLiteral5115 = il2cpp_codegen_string_literal_from_index(5115);
		_stringLiteral5116 = il2cpp_codegen_string_literal_from_index(5116);
		_stringLiteral5117 = il2cpp_codegen_string_literal_from_index(5117);
		_stringLiteral5118 = il2cpp_codegen_string_literal_from_index(5118);
		_stringLiteral5119 = il2cpp_codegen_string_literal_from_index(5119);
		_stringLiteral5120 = il2cpp_codegen_string_literal_from_index(5120);
		_stringLiteral5121 = il2cpp_codegen_string_literal_from_index(5121);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	iOSSpecificProperties_t8_266 * V_1 = {0};
	AndroidSpecificProperties_t8_265 * V_2 = {0};
	CrossPlatformNotification_t8_259 * V_3 = {0};
	{
		Dictionary_2_t1_1839 * L_0 = (Dictionary_2_t1_1839 *)il2cpp_codegen_object_new (Dictionary_2_t1_1839_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_14949(L_0, /*hidden argument*/Dictionary_2__ctor_m1_14949_MethodInfo_var);
		V_0 = L_0;
		Object_t * L_1 = V_0;
		NullCheck(L_1);
		InterfaceActionInvoker2< Object_t *, Object_t * >::Invoke(3 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_1, _stringLiteral424, _stringLiteral5114);
		iOSSpecificProperties_t8_266 * L_2 = (iOSSpecificProperties_t8_266 *)il2cpp_codegen_object_new (iOSSpecificProperties_t8_266_il2cpp_TypeInfo_var);
		iOSSpecificProperties__ctor_m8_1502(L_2, /*hidden argument*/NULL);
		V_1 = L_2;
		iOSSpecificProperties_t8_266 * L_3 = V_1;
		NullCheck(L_3);
		iOSSpecificProperties_set_HasAction_m8_1507(L_3, 1, /*hidden argument*/NULL);
		iOSSpecificProperties_t8_266 * L_4 = V_1;
		NullCheck(L_4);
		iOSSpecificProperties_set_AlertAction_m8_1505(L_4, _stringLiteral5115, /*hidden argument*/NULL);
		AndroidSpecificProperties_t8_265 * L_5 = (AndroidSpecificProperties_t8_265 *)il2cpp_codegen_object_new (AndroidSpecificProperties_t8_265_il2cpp_TypeInfo_var);
		AndroidSpecificProperties__ctor_m8_1488(L_5, /*hidden argument*/NULL);
		V_2 = L_5;
		AndroidSpecificProperties_t8_265 * L_6 = V_2;
		NullCheck(L_6);
		AndroidSpecificProperties_set_ContentTitle_m8_1491(L_6, _stringLiteral5116, /*hidden argument*/NULL);
		AndroidSpecificProperties_t8_265 * L_7 = V_2;
		NullCheck(L_7);
		AndroidSpecificProperties_set_TickerText_m8_1493(L_7, _stringLiteral5117, /*hidden argument*/NULL);
		AndroidSpecificProperties_t8_265 * L_8 = V_2;
		NullCheck(L_8);
		AndroidSpecificProperties_set_LargeIcon_m8_1499(L_8, _stringLiteral5118, /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_9 = (CrossPlatformNotification_t8_259 *)il2cpp_codegen_object_new (CrossPlatformNotification_t8_259_il2cpp_TypeInfo_var);
		CrossPlatformNotification__ctor_m8_1514(L_9, /*hidden argument*/NULL);
		V_3 = L_9;
		CrossPlatformNotification_t8_259 * L_10 = V_3;
		NullCheck(L_10);
		CrossPlatformNotification_set_AlertBody_m8_1517(L_10, _stringLiteral5119, /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_11 = V_3;
		DateTime_t1_150  L_12 = ____time;
		NullCheck(L_11);
		CrossPlatformNotification_set_FireDate_m8_1519(L_11, L_12, /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_13 = V_3;
		int32_t L_14 = ____repeatInterval;
		NullCheck(L_13);
		CrossPlatformNotification_set_RepeatInterval_m8_1521(L_13, L_14, /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_15 = V_3;
		NullCheck(L_15);
		CrossPlatformNotification_set_SoundName_m8_1525(L_15, _stringLiteral5120, /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_16 = V_3;
		Object_t * L_17 = V_0;
		NullCheck(L_16);
		CrossPlatformNotification_set_UserInfo_m8_1523(L_16, L_17, /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_18 = V_3;
		iOSSpecificProperties_t8_266 * L_19 = V_1;
		NullCheck(L_18);
		CrossPlatformNotification_set_iOSProperties_m8_1527(L_18, L_19, /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_20 = V_3;
		AndroidSpecificProperties_t8_265 * L_21 = V_2;
		NullCheck(L_20);
		CrossPlatformNotification_set_AndroidProperties_m8_1529(L_20, L_21, /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_22 = V_3;
		NullCheck(L_22);
		String_t* L_23 = CrossPlatformNotification_GetNotificationID_m8_1530(L_22, /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_24 = V_3;
		NullCheck(L_24);
		DateTime_t1_150  L_25 = CrossPlatformNotification_get_FireDate_m8_1518(L_24, /*hidden argument*/NULL);
		DateTime_t1_150  L_26 = L_25;
		Object_t * L_27 = Box(DateTime_t1_150_il2cpp_TypeInfo_var, &L_26);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_28 = String_Format_m1_549(NULL /*static, unused*/, _stringLiteral5121, L_23, L_27, /*hidden argument*/NULL);
		Debug_Log_m6_631(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_29 = V_3;
		return L_29;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.RemoteNotificationTest::.ctor()
extern "C" void RemoteNotificationTest__ctor_m8_1084 (RemoteNotificationTest_t8_184 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m6_671(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.RemoteNotificationTest::Start()
extern "C" void RemoteNotificationTest_Start_m8_1085 (RemoteNotificationTest_t8_184 * __this, const MethodInfo* method)
{
	{
		NotificationService_t8_261 * L_0 = NPBinding_get_NotificationService_m8_1929(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = (__this->___m_notificationType_2);
		NullCheck(L_0);
		VirtActionInvoker1< int32_t >::Invoke(7 /* System.Void VoxelBusters.NativePlugins.NotificationService::RegisterNotificationTypes(VoxelBusters.NativePlugins.NotificationType) */, L_0, L_1);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.RemoteNotificationTest::OnEnable()
extern TypeInfo* RegisterForRemoteNotificationCompletion_t8_257_il2cpp_TypeInfo_var;
extern TypeInfo* ReceivedNotificationResponse_t8_258_il2cpp_TypeInfo_var;
extern const MethodInfo* RemoteNotificationTest_DidFinishRegisterForRemoteNotificationEvent_m8_1091_MethodInfo_var;
extern const MethodInfo* RemoteNotificationTest_DidReceiveRemoteNotificationEvent_m8_1090_MethodInfo_var;
extern "C" void RemoteNotificationTest_OnEnable_m8_1086 (RemoteNotificationTest_t8_184 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RegisterForRemoteNotificationCompletion_t8_257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2105);
		ReceivedNotificationResponse_t8_258_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2106);
		RemoteNotificationTest_DidFinishRegisterForRemoteNotificationEvent_m8_1091_MethodInfo_var = il2cpp_codegen_method_info_from_index(567);
		RemoteNotificationTest_DidReceiveRemoteNotificationEvent_m8_1090_MethodInfo_var = il2cpp_codegen_method_info_from_index(568);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = { (void*)RemoteNotificationTest_DidFinishRegisterForRemoteNotificationEvent_m8_1091_MethodInfo_var };
		RegisterForRemoteNotificationCompletion_t8_257 * L_1 = (RegisterForRemoteNotificationCompletion_t8_257 *)il2cpp_codegen_object_new (RegisterForRemoteNotificationCompletion_t8_257_il2cpp_TypeInfo_var);
		RegisterForRemoteNotificationCompletion__ctor_m8_1423(L_1, __this, L_0, /*hidden argument*/NULL);
		NotificationService_add_DidFinishRegisterForRemoteNotificationEvent_m8_1438(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IntPtr_t L_2 = { (void*)RemoteNotificationTest_DidReceiveRemoteNotificationEvent_m8_1090_MethodInfo_var };
		ReceivedNotificationResponse_t8_258 * L_3 = (ReceivedNotificationResponse_t8_258 *)il2cpp_codegen_object_new (ReceivedNotificationResponse_t8_258_il2cpp_TypeInfo_var);
		ReceivedNotificationResponse__ctor_m8_1427(L_3, __this, L_2, /*hidden argument*/NULL);
		NotificationService_add_DidReceiveRemoteNotificationEvent_m8_1446(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.RemoteNotificationTest::OnDisable()
extern TypeInfo* RegisterForRemoteNotificationCompletion_t8_257_il2cpp_TypeInfo_var;
extern TypeInfo* ReceivedNotificationResponse_t8_258_il2cpp_TypeInfo_var;
extern const MethodInfo* RemoteNotificationTest_DidFinishRegisterForRemoteNotificationEvent_m8_1091_MethodInfo_var;
extern const MethodInfo* RemoteNotificationTest_DidReceiveRemoteNotificationEvent_m8_1090_MethodInfo_var;
extern "C" void RemoteNotificationTest_OnDisable_m8_1087 (RemoteNotificationTest_t8_184 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RegisterForRemoteNotificationCompletion_t8_257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2105);
		ReceivedNotificationResponse_t8_258_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2106);
		RemoteNotificationTest_DidFinishRegisterForRemoteNotificationEvent_m8_1091_MethodInfo_var = il2cpp_codegen_method_info_from_index(567);
		RemoteNotificationTest_DidReceiveRemoteNotificationEvent_m8_1090_MethodInfo_var = il2cpp_codegen_method_info_from_index(568);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = { (void*)RemoteNotificationTest_DidFinishRegisterForRemoteNotificationEvent_m8_1091_MethodInfo_var };
		RegisterForRemoteNotificationCompletion_t8_257 * L_1 = (RegisterForRemoteNotificationCompletion_t8_257 *)il2cpp_codegen_object_new (RegisterForRemoteNotificationCompletion_t8_257_il2cpp_TypeInfo_var);
		RegisterForRemoteNotificationCompletion__ctor_m8_1423(L_1, __this, L_0, /*hidden argument*/NULL);
		NotificationService_remove_DidFinishRegisterForRemoteNotificationEvent_m8_1439(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IntPtr_t L_2 = { (void*)RemoteNotificationTest_DidReceiveRemoteNotificationEvent_m8_1090_MethodInfo_var };
		ReceivedNotificationResponse_t8_258 * L_3 = (ReceivedNotificationResponse_t8_258 *)il2cpp_codegen_object_new (ReceivedNotificationResponse_t8_258_il2cpp_TypeInfo_var);
		ReceivedNotificationResponse__ctor_m8_1427(L_3, __this, L_2, /*hidden argument*/NULL);
		NotificationService_remove_DidReceiveRemoteNotificationEvent_m8_1447(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.RemoteNotificationTest::OnGUI()
extern TypeInfo* GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5318;
extern "C" void RemoteNotificationTest_OnGUI_m8_1088 (RemoteNotificationTest_t8_184 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1651);
		_stringLiteral5318 = il2cpp_codegen_string_literal_from_index(5318);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUILayoutOptionU5BU5D_t6_290* L_0 = ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 2));
		int32_t L_1 = Screen_get_width_m6_145(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t6_182 * L_2 = GUILayout_Width_m6_1273(NULL /*static, unused*/, ((float)((float)(((float)((float)L_1)))/(float)(2.0f))), /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_2);
		*((GUILayoutOption_t6_182 **)(GUILayoutOption_t6_182 **)SZArrayLdElema(L_0, 0, sizeof(GUILayoutOption_t6_182 *))) = (GUILayoutOption_t6_182 *)L_2;
		GUILayoutOptionU5BU5D_t6_290* L_3 = L_0;
		int32_t L_4 = Screen_get_height_m6_146(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t6_182 * L_5 = GUILayout_Height_m6_1275(NULL /*static, unused*/, ((float)((float)(((float)((float)L_4)))*(float)(0.2f))), /*hidden argument*/NULL);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 1);
		ArrayElementTypeCheck (L_3, L_5);
		*((GUILayoutOption_t6_182 **)(GUILayoutOption_t6_182 **)SZArrayLdElema(L_3, 1, sizeof(GUILayoutOption_t6_182 *))) = (GUILayoutOption_t6_182 *)L_5;
		bool L_6 = GUILayout_Button_m6_1252(NULL /*static, unused*/, _stringLiteral5318, L_3, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0047;
		}
	}
	{
		NotificationService_t8_261 * L_7 = NPBinding_get_NotificationService_m8_1929(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		VirtActionInvoker0::Invoke(12 /* System.Void VoxelBusters.NativePlugins.NotificationService::RegisterForRemoteNotifications() */, L_7);
	}

IL_0047:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.RemoteNotificationTest::DidReceiveLocalNotificationEvent(VoxelBusters.NativePlugins.CrossPlatformNotification)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5319;
extern "C" void RemoteNotificationTest_DidReceiveLocalNotificationEvent_m8_1089 (RemoteNotificationTest_t8_184 * __this, CrossPlatformNotification_t8_259 * ____notification, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5319 = il2cpp_codegen_string_literal_from_index(5319);
		s_Il2CppMethodIntialized = true;
	}
	{
		CrossPlatformNotification_t8_259 * L_0 = ____notification;
		NullCheck(L_0);
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String VoxelBusters.NativePlugins.CrossPlatformNotification::ToString() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m1_559(NULL /*static, unused*/, _stringLiteral5319, L_1, /*hidden argument*/NULL);
		Debug_Log_m6_631(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.RemoteNotificationTest::DidReceiveRemoteNotificationEvent(VoxelBusters.NativePlugins.CrossPlatformNotification)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5320;
extern "C" void RemoteNotificationTest_DidReceiveRemoteNotificationEvent_m8_1090 (RemoteNotificationTest_t8_184 * __this, CrossPlatformNotification_t8_259 * ____notification, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5320 = il2cpp_codegen_string_literal_from_index(5320);
		s_Il2CppMethodIntialized = true;
	}
	{
		CrossPlatformNotification_t8_259 * L_0 = ____notification;
		NullCheck(L_0);
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String VoxelBusters.NativePlugins.CrossPlatformNotification::ToString() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m1_559(NULL /*static, unused*/, _stringLiteral5320, L_1, /*hidden argument*/NULL);
		Debug_Log_m6_631(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.RemoteNotificationTest::DidFinishRegisterForRemoteNotificationEvent(System.String,System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5321;
extern Il2CppCodeGenString* _stringLiteral5322;
extern "C" void RemoteNotificationTest_DidFinishRegisterForRemoteNotificationEvent_m8_1091 (RemoteNotificationTest_t8_184 * __this, String_t* ____deviceToken, String_t* ____error, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5321 = il2cpp_codegen_string_literal_from_index(5321);
		_stringLiteral5322 = il2cpp_codegen_string_literal_from_index(5322);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ____error;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1_521(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		String_t* L_2 = ____deviceToken;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m1_559(NULL /*static, unused*/, _stringLiteral5321, L_2, /*hidden argument*/NULL);
		Debug_Log_m6_631(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		goto IL_0030;
	}

IL_0020:
	{
		String_t* L_4 = ____deviceToken;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m1_559(NULL /*static, unused*/, _stringLiteral5322, L_4, /*hidden argument*/NULL);
		Debug_Log_m6_631(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
	}

IL_0030:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.SharingDemo::.ctor()
extern TypeInfo* eShareOptionsU5BU5D_t8_186_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5323;
extern Il2CppCodeGenString* _stringLiteral5324;
extern Il2CppCodeGenString* _stringLiteral5325;
extern Il2CppCodeGenString* _stringLiteral5326;
extern Il2CppCodeGenString* _stringLiteral5327;
extern Il2CppCodeGenString* _stringLiteral5328;
extern "C" void SharingDemo__ctor_m8_1092 (SharingDemo_t8_185 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		eShareOptionsU5BU5D_t8_186_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2107);
		_stringLiteral5323 = il2cpp_codegen_string_literal_from_index(5323);
		_stringLiteral5324 = il2cpp_codegen_string_literal_from_index(5324);
		_stringLiteral5325 = il2cpp_codegen_string_literal_from_index(5325);
		_stringLiteral5326 = il2cpp_codegen_string_literal_from_index(5326);
		_stringLiteral5327 = il2cpp_codegen_string_literal_from_index(5327);
		_stringLiteral5328 = il2cpp_codegen_string_literal_from_index(5328);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___m_smsBody_10 = _stringLiteral5323;
		__this->___m_mailSubject_12 = _stringLiteral5324;
		__this->___m_plainMailBody_13 = _stringLiteral5325;
		__this->___m_htmlMailBody_14 = _stringLiteral5326;
		__this->___m_excludedOptions_18 = ((eShareOptionsU5BU5D_t8_186*)SZArrayNew(eShareOptionsU5BU5D_t8_186_il2cpp_TypeInfo_var, 0));
		__this->___m_shareMessage_19 = _stringLiteral5327;
		__this->___m_shareURL_20 = _stringLiteral5328;
		NPDisabledFeatureDemo__ctor_m8_1053(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.TwitterDemo::.ctor()
extern Il2CppCodeGenString* _stringLiteral5329;
extern Il2CppCodeGenString* _stringLiteral5330;
extern "C" void TwitterDemo__ctor_m8_1093 (TwitterDemo_t8_187 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral5329 = il2cpp_codegen_string_literal_from_index(5329);
		_stringLiteral5330 = il2cpp_codegen_string_literal_from_index(5330);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___m_shareMessage_10 = _stringLiteral5329;
		__this->___m_shareURL_11 = _stringLiteral5330;
		NPDisabledFeatureDemo__ctor_m8_1053(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.UIDemo::.ctor()
extern TypeInfo* StringU5BU5D_t1_238_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5331;
extern Il2CppCodeGenString* _stringLiteral5332;
extern Il2CppCodeGenString* _stringLiteral5333;
extern Il2CppCodeGenString* _stringLiteral2745;
extern Il2CppCodeGenString* _stringLiteral5334;
extern Il2CppCodeGenString* _stringLiteral5003;
extern "C" void UIDemo__ctor_m8_1094 (UIDemo_t8_188 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringU5BU5D_t1_238_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(35);
		_stringLiteral5331 = il2cpp_codegen_string_literal_from_index(5331);
		_stringLiteral5332 = il2cpp_codegen_string_literal_from_index(5332);
		_stringLiteral5333 = il2cpp_codegen_string_literal_from_index(5333);
		_stringLiteral2745 = il2cpp_codegen_string_literal_from_index(2745);
		_stringLiteral5334 = il2cpp_codegen_string_literal_from_index(5334);
		_stringLiteral5003 = il2cpp_codegen_string_literal_from_index(5003);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___m_title_13 = _stringLiteral5331;
		__this->___m_message_14 = _stringLiteral5332;
		__this->___m_usernamePlaceHolder_15 = _stringLiteral5333;
		__this->___m_passwordPlaceHolder_16 = _stringLiteral2745;
		__this->___m_button_17 = _stringLiteral5334;
		StringU5BU5D_t1_238* L_0 = ((StringU5BU5D_t1_238*)SZArrayNew(StringU5BU5D_t1_238_il2cpp_TypeInfo_var, 2));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral5003);
		*((String_t**)(String_t**)SZArrayLdElema(L_0, 0, sizeof(String_t*))) = (String_t*)_stringLiteral5003;
		StringU5BU5D_t1_238* L_1 = L_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, _stringLiteral5334);
		*((String_t**)(String_t**)SZArrayLdElema(L_1, 1, sizeof(String_t*))) = (String_t*)_stringLiteral5334;
		__this->___m_buttons_18 = L_1;
		NPDemoBase__ctor_m8_1046(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.UIDemo::DisplayFeatureFunctionalities()
extern TypeInfo* GUIStyle_t6_176_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5335;
extern Il2CppCodeGenString* _stringLiteral5146;
extern Il2CppCodeGenString* _stringLiteral5336;
extern Il2CppCodeGenString* _stringLiteral5337;
extern Il2CppCodeGenString* _stringLiteral5338;
extern Il2CppCodeGenString* _stringLiteral5339;
extern Il2CppCodeGenString* _stringLiteral5340;
extern Il2CppCodeGenString* _stringLiteral5341;
extern Il2CppCodeGenString* _stringLiteral5342;
extern Il2CppCodeGenString* _stringLiteral5343;
extern Il2CppCodeGenString* _stringLiteral5344;
extern "C" void UIDemo_DisplayFeatureFunctionalities_m8_1095 (UIDemo_t8_188 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t6_176_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1648);
		GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1651);
		_stringLiteral5335 = il2cpp_codegen_string_literal_from_index(5335);
		_stringLiteral5146 = il2cpp_codegen_string_literal_from_index(5146);
		_stringLiteral5336 = il2cpp_codegen_string_literal_from_index(5336);
		_stringLiteral5337 = il2cpp_codegen_string_literal_from_index(5337);
		_stringLiteral5338 = il2cpp_codegen_string_literal_from_index(5338);
		_stringLiteral5339 = il2cpp_codegen_string_literal_from_index(5339);
		_stringLiteral5340 = il2cpp_codegen_string_literal_from_index(5340);
		_stringLiteral5341 = il2cpp_codegen_string_literal_from_index(5341);
		_stringLiteral5342 = il2cpp_codegen_string_literal_from_index(5342);
		_stringLiteral5343 = il2cpp_codegen_string_literal_from_index(5343);
		_stringLiteral5344 = il2cpp_codegen_string_literal_from_index(5344);
		s_Il2CppMethodIntialized = true;
	}
	{
		NPDemoBase_DisplayFeatureFunctionalities_m8_1050(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle_t6_176 * L_0 = GUIStyle_op_Implicit_m6_1493(NULL /*static, unused*/, _stringLiteral5146, /*hidden argument*/NULL);
		GUILayout_Label_m6_1248(NULL /*static, unused*/, _stringLiteral5335, L_0, ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		bool L_1 = GUILayout_Button_m6_1252(NULL /*static, unused*/, _stringLiteral5336, ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_003b;
		}
	}
	{
		UIDemo_ShowAlertDialogWithSingleButton_m8_1096(__this, /*hidden argument*/NULL);
	}

IL_003b:
	{
		bool L_2 = GUILayout_Button_m6_1252(NULL /*static, unused*/, _stringLiteral5337, ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0056;
		}
	}
	{
		UIDemo_ShowAlertDialogWithMultipleButtons_m8_1097(__this, /*hidden argument*/NULL);
	}

IL_0056:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle_t6_176 * L_3 = GUIStyle_op_Implicit_m6_1493(NULL /*static, unused*/, _stringLiteral5146, /*hidden argument*/NULL);
		GUILayout_Label_m6_1248(NULL /*static, unused*/, _stringLiteral5338, L_3, ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		bool L_4 = GUILayout_Button_m6_1252(NULL /*static, unused*/, _stringLiteral5339, ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_008b;
		}
	}
	{
		UIDemo_ShowPlainTextPromptDialog_m8_1098(__this, /*hidden argument*/NULL);
	}

IL_008b:
	{
		bool L_5 = GUILayout_Button_m6_1252(NULL /*static, unused*/, _stringLiteral5340, ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_00a6;
		}
	}
	{
		UIDemo_ShowSecuredTextPromptDialog_m8_1099(__this, /*hidden argument*/NULL);
	}

IL_00a6:
	{
		bool L_6 = GUILayout_Button_m6_1252(NULL /*static, unused*/, _stringLiteral5341, ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_00c1;
		}
	}
	{
		UIDemo_ShowLoginPromptDialog_m8_1100(__this, /*hidden argument*/NULL);
	}

IL_00c1:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle_t6_176 * L_7 = GUIStyle_op_Implicit_m6_1493(NULL /*static, unused*/, _stringLiteral5146, /*hidden argument*/NULL);
		GUILayout_Label_m6_1248(NULL /*static, unused*/, _stringLiteral5342, L_7, ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		bool L_8 = GUILayout_Button_m6_1252(NULL /*static, unused*/, _stringLiteral5343, ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_00f6;
		}
	}
	{
		UIDemo_ShowShortDurationToast_m8_1101(__this, /*hidden argument*/NULL);
	}

IL_00f6:
	{
		bool L_9 = GUILayout_Button_m6_1252(NULL /*static, unused*/, _stringLiteral5344, ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0111;
		}
	}
	{
		UIDemo_ShowLongDurationToast_m8_1102(__this, /*hidden argument*/NULL);
	}

IL_0111:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.UIDemo::ShowAlertDialogWithSingleButton()
extern TypeInfo* AlertDialogCompletion_t8_300_il2cpp_TypeInfo_var;
extern const MethodInfo* UIDemo_U3CShowAlertDialogWithSingleButtonU3Em__7_m8_1106_MethodInfo_var;
extern "C" void UIDemo_ShowAlertDialogWithSingleButton_m8_1096 (UIDemo_t8_188 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AlertDialogCompletion_t8_300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2109);
		UIDemo_U3CShowAlertDialogWithSingleButtonU3Em__7_m8_1106_MethodInfo_var = il2cpp_codegen_method_info_from_index(569);
		s_Il2CppMethodIntialized = true;
	}
	{
		UI_t8_303 * L_0 = NPBinding_get_UI_m8_1930(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = (__this->___m_title_13);
		String_t* L_2 = (__this->___m_message_14);
		String_t* L_3 = (__this->___m_button_17);
		IntPtr_t L_4 = { (void*)UIDemo_U3CShowAlertDialogWithSingleButtonU3Em__7_m8_1106_MethodInfo_var };
		AlertDialogCompletion_t8_300 * L_5 = (AlertDialogCompletion_t8_300 *)il2cpp_codegen_object_new (AlertDialogCompletion_t8_300_il2cpp_TypeInfo_var);
		AlertDialogCompletion__ctor_m8_1748(L_5, __this, L_4, /*hidden argument*/NULL);
		NullCheck(L_0);
		UI_ShowAlertDialogWithSingleButton_m8_1765(L_0, L_1, L_2, L_3, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.UIDemo::ShowAlertDialogWithMultipleButtons()
extern TypeInfo* AlertDialogCompletion_t8_300_il2cpp_TypeInfo_var;
extern const MethodInfo* UIDemo_MultipleButtonsAlertClosed_m8_1103_MethodInfo_var;
extern "C" void UIDemo_ShowAlertDialogWithMultipleButtons_m8_1097 (UIDemo_t8_188 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AlertDialogCompletion_t8_300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2109);
		UIDemo_MultipleButtonsAlertClosed_m8_1103_MethodInfo_var = il2cpp_codegen_method_info_from_index(570);
		s_Il2CppMethodIntialized = true;
	}
	{
		UI_t8_303 * L_0 = NPBinding_get_UI_m8_1930(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = (__this->___m_title_13);
		String_t* L_2 = (__this->___m_message_14);
		StringU5BU5D_t1_238* L_3 = (__this->___m_buttons_18);
		IntPtr_t L_4 = { (void*)UIDemo_MultipleButtonsAlertClosed_m8_1103_MethodInfo_var };
		AlertDialogCompletion_t8_300 * L_5 = (AlertDialogCompletion_t8_300 *)il2cpp_codegen_object_new (AlertDialogCompletion_t8_300_il2cpp_TypeInfo_var);
		AlertDialogCompletion__ctor_m8_1748(L_5, __this, L_4, /*hidden argument*/NULL);
		NullCheck(L_0);
		UI_ShowAlertDialogWithMultipleButtons_m8_1766(L_0, L_1, L_2, L_3, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.UIDemo::ShowPlainTextPromptDialog()
extern TypeInfo* SingleFieldPromptCompletion_t8_301_il2cpp_TypeInfo_var;
extern const MethodInfo* UIDemo_SingleFieldPromptDialogClosed_m8_1104_MethodInfo_var;
extern "C" void UIDemo_ShowPlainTextPromptDialog_m8_1098 (UIDemo_t8_188 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SingleFieldPromptCompletion_t8_301_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2110);
		UIDemo_SingleFieldPromptDialogClosed_m8_1104_MethodInfo_var = il2cpp_codegen_method_info_from_index(571);
		s_Il2CppMethodIntialized = true;
	}
	{
		UI_t8_303 * L_0 = NPBinding_get_UI_m8_1930(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = (__this->___m_title_13);
		String_t* L_2 = (__this->___m_message_14);
		String_t* L_3 = (__this->___m_usernamePlaceHolder_15);
		StringU5BU5D_t1_238* L_4 = (__this->___m_buttons_18);
		IntPtr_t L_5 = { (void*)UIDemo_SingleFieldPromptDialogClosed_m8_1104_MethodInfo_var };
		SingleFieldPromptCompletion_t8_301 * L_6 = (SingleFieldPromptCompletion_t8_301 *)il2cpp_codegen_object_new (SingleFieldPromptCompletion_t8_301_il2cpp_TypeInfo_var);
		SingleFieldPromptCompletion__ctor_m8_1752(L_6, __this, L_5, /*hidden argument*/NULL);
		NullCheck(L_0);
		UI_ShowSingleFieldPromptDialogWithPlainText_m8_1772(L_0, L_1, L_2, L_3, L_4, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.UIDemo::ShowSecuredTextPromptDialog()
extern TypeInfo* SingleFieldPromptCompletion_t8_301_il2cpp_TypeInfo_var;
extern const MethodInfo* UIDemo_SingleFieldPromptDialogClosed_m8_1104_MethodInfo_var;
extern "C" void UIDemo_ShowSecuredTextPromptDialog_m8_1099 (UIDemo_t8_188 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SingleFieldPromptCompletion_t8_301_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2110);
		UIDemo_SingleFieldPromptDialogClosed_m8_1104_MethodInfo_var = il2cpp_codegen_method_info_from_index(571);
		s_Il2CppMethodIntialized = true;
	}
	{
		UI_t8_303 * L_0 = NPBinding_get_UI_m8_1930(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = (__this->___m_title_13);
		String_t* L_2 = (__this->___m_message_14);
		String_t* L_3 = (__this->___m_passwordPlaceHolder_16);
		StringU5BU5D_t1_238* L_4 = (__this->___m_buttons_18);
		IntPtr_t L_5 = { (void*)UIDemo_SingleFieldPromptDialogClosed_m8_1104_MethodInfo_var };
		SingleFieldPromptCompletion_t8_301 * L_6 = (SingleFieldPromptCompletion_t8_301 *)il2cpp_codegen_object_new (SingleFieldPromptCompletion_t8_301_il2cpp_TypeInfo_var);
		SingleFieldPromptCompletion__ctor_m8_1752(L_6, __this, L_5, /*hidden argument*/NULL);
		NullCheck(L_0);
		UI_ShowSingleFieldPromptDialogWithSecuredText_m8_1773(L_0, L_1, L_2, L_3, L_4, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.UIDemo::ShowLoginPromptDialog()
extern TypeInfo* LoginPromptCompletion_t8_302_il2cpp_TypeInfo_var;
extern const MethodInfo* UIDemo_LoginPromptDialogClosed_m8_1105_MethodInfo_var;
extern "C" void UIDemo_ShowLoginPromptDialog_m8_1100 (UIDemo_t8_188 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LoginPromptCompletion_t8_302_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2111);
		UIDemo_LoginPromptDialogClosed_m8_1105_MethodInfo_var = il2cpp_codegen_method_info_from_index(572);
		s_Il2CppMethodIntialized = true;
	}
	{
		UI_t8_303 * L_0 = NPBinding_get_UI_m8_1930(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = (__this->___m_title_13);
		String_t* L_2 = (__this->___m_message_14);
		String_t* L_3 = (__this->___m_usernamePlaceHolder_15);
		String_t* L_4 = (__this->___m_passwordPlaceHolder_16);
		StringU5BU5D_t1_238* L_5 = (__this->___m_buttons_18);
		IntPtr_t L_6 = { (void*)UIDemo_LoginPromptDialogClosed_m8_1105_MethodInfo_var };
		LoginPromptCompletion_t8_302 * L_7 = (LoginPromptCompletion_t8_302 *)il2cpp_codegen_object_new (LoginPromptCompletion_t8_302_il2cpp_TypeInfo_var);
		LoginPromptCompletion__ctor_m8_1756(L_7, __this, L_6, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker6< String_t*, String_t*, String_t*, String_t*, StringU5BU5D_t1_238*, LoginPromptCompletion_t8_302 * >::Invoke(9 /* System.Void VoxelBusters.NativePlugins.UI::ShowLoginPromptDialog(System.String,System.String,System.String,System.String,System.String[],VoxelBusters.NativePlugins.UI/LoginPromptCompletion) */, L_0, L_1, L_2, L_3, L_4, L_5, L_7);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.UIDemo::ShowShortDurationToast()
extern "C" void UIDemo_ShowShortDurationToast_m8_1101 (UIDemo_t8_188 * __this, const MethodInfo* method)
{
	{
		UI_t8_303 * L_0 = NPBinding_get_UI_m8_1930(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = (__this->___m_message_14);
		NullCheck(L_0);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(10 /* System.Void VoxelBusters.NativePlugins.UI::ShowToast(System.String,VoxelBusters.NativePlugins.eToastMessageLength) */, L_0, L_1, 0);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.UIDemo::ShowLongDurationToast()
extern "C" void UIDemo_ShowLongDurationToast_m8_1102 (UIDemo_t8_188 * __this, const MethodInfo* method)
{
	{
		UI_t8_303 * L_0 = NPBinding_get_UI_m8_1930(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = (__this->___m_message_14);
		NullCheck(L_0);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(10 /* System.Void VoxelBusters.NativePlugins.UI::ShowToast(System.String,VoxelBusters.NativePlugins.eToastMessageLength) */, L_0, L_1, 1);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.UIDemo::MultipleButtonsAlertClosed(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5345;
extern Il2CppCodeGenString* _stringLiteral5346;
extern Il2CppCodeGenString* _stringLiteral56;
extern "C" void UIDemo_MultipleButtonsAlertClosed_m8_1103 (UIDemo_t8_188 * __this, String_t* ____buttonPressed, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5345 = il2cpp_codegen_string_literal_from_index(5345);
		_stringLiteral5346 = il2cpp_codegen_string_literal_from_index(5346);
		_stringLiteral56 = il2cpp_codegen_string_literal_from_index(56);
		s_Il2CppMethodIntialized = true;
	}
	{
		DemoSubMenu_AddNewResult_m8_135(__this, _stringLiteral5345, /*hidden argument*/NULL);
		String_t* L_0 = ____buttonPressed;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m1_560(NULL /*static, unused*/, _stringLiteral5346, L_0, _stringLiteral56, /*hidden argument*/NULL);
		DemoSubMenu_AppendResult_m8_134(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.UIDemo::SingleFieldPromptDialogClosed(System.String,System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5347;
extern Il2CppCodeGenString* _stringLiteral5346;
extern Il2CppCodeGenString* _stringLiteral56;
extern Il2CppCodeGenString* _stringLiteral5348;
extern "C" void UIDemo_SingleFieldPromptDialogClosed_m8_1104 (UIDemo_t8_188 * __this, String_t* ____buttonPressed, String_t* ____input, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5347 = il2cpp_codegen_string_literal_from_index(5347);
		_stringLiteral5346 = il2cpp_codegen_string_literal_from_index(5346);
		_stringLiteral56 = il2cpp_codegen_string_literal_from_index(56);
		_stringLiteral5348 = il2cpp_codegen_string_literal_from_index(5348);
		s_Il2CppMethodIntialized = true;
	}
	{
		DemoSubMenu_AddNewResult_m8_135(__this, _stringLiteral5347, /*hidden argument*/NULL);
		String_t* L_0 = ____buttonPressed;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m1_560(NULL /*static, unused*/, _stringLiteral5346, L_0, _stringLiteral56, /*hidden argument*/NULL);
		DemoSubMenu_AppendResult_m8_134(__this, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ____input;
		String_t* L_3 = String_Concat_m1_560(NULL /*static, unused*/, _stringLiteral5348, L_2, _stringLiteral56, /*hidden argument*/NULL);
		DemoSubMenu_AppendResult_m8_134(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.UIDemo::LoginPromptDialogClosed(System.String,System.String,System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5349;
extern Il2CppCodeGenString* _stringLiteral5346;
extern Il2CppCodeGenString* _stringLiteral56;
extern Il2CppCodeGenString* _stringLiteral5350;
extern Il2CppCodeGenString* _stringLiteral5351;
extern "C" void UIDemo_LoginPromptDialogClosed_m8_1105 (UIDemo_t8_188 * __this, String_t* ____buttonPressed, String_t* ____username, String_t* ____password, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5349 = il2cpp_codegen_string_literal_from_index(5349);
		_stringLiteral5346 = il2cpp_codegen_string_literal_from_index(5346);
		_stringLiteral56 = il2cpp_codegen_string_literal_from_index(56);
		_stringLiteral5350 = il2cpp_codegen_string_literal_from_index(5350);
		_stringLiteral5351 = il2cpp_codegen_string_literal_from_index(5351);
		s_Il2CppMethodIntialized = true;
	}
	{
		DemoSubMenu_AddNewResult_m8_135(__this, _stringLiteral5349, /*hidden argument*/NULL);
		String_t* L_0 = ____buttonPressed;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m1_560(NULL /*static, unused*/, _stringLiteral5346, L_0, _stringLiteral56, /*hidden argument*/NULL);
		DemoSubMenu_AppendResult_m8_134(__this, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ____username;
		String_t* L_3 = String_Concat_m1_560(NULL /*static, unused*/, _stringLiteral5350, L_2, _stringLiteral56, /*hidden argument*/NULL);
		DemoSubMenu_AppendResult_m8_134(__this, L_3, /*hidden argument*/NULL);
		String_t* L_4 = ____password;
		String_t* L_5 = String_Concat_m1_560(NULL /*static, unused*/, _stringLiteral5351, L_4, _stringLiteral56, /*hidden argument*/NULL);
		DemoSubMenu_AppendResult_m8_134(__this, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.UIDemo::<ShowAlertDialogWithSingleButton>m__7(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5352;
extern Il2CppCodeGenString* _stringLiteral5353;
extern "C" void UIDemo_U3CShowAlertDialogWithSingleButtonU3Em__7_m8_1106 (UIDemo_t8_188 * __this, String_t* ____buttonPressed, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5352 = il2cpp_codegen_string_literal_from_index(5352);
		_stringLiteral5353 = il2cpp_codegen_string_literal_from_index(5353);
		s_Il2CppMethodIntialized = true;
	}
	{
		DemoSubMenu_AddNewResult_m8_135(__this, _stringLiteral5352, /*hidden argument*/NULL);
		String_t* L_0 = ____buttonPressed;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m1_559(NULL /*static, unused*/, _stringLiteral5353, L_0, /*hidden argument*/NULL);
		DemoSubMenu_AppendResult_m8_134(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.UtilityDemo::.ctor()
extern "C" void UtilityDemo__ctor_m8_1107 (UtilityDemo_t8_189 * __this, const MethodInfo* method)
{
	{
		__this->___m_applicationBadgeNumber_13 = 2;
		NPDemoBase__ctor_m8_1046(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.UtilityDemo::Start()
extern TypeInfo* StringU5BU5D_t1_238_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5354;
extern "C" void UtilityDemo_Start_m8_1108 (UtilityDemo_t8_189 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringU5BU5D_t1_238_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(35);
		_stringLiteral5354 = il2cpp_codegen_string_literal_from_index(5354);
		s_Il2CppMethodIntialized = true;
	}
	{
		NPDemoBase_Start_m8_1047(__this, /*hidden argument*/NULL);
		StringU5BU5D_t1_238* L_0 = ((StringU5BU5D_t1_238*)SZArrayNew(StringU5BU5D_t1_238_il2cpp_TypeInfo_var, 1));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral5354);
		*((String_t**)(String_t**)SZArrayLdElema(L_0, 0, sizeof(String_t*))) = (String_t*)_stringLiteral5354;
		NPDemoBase_AddExtraInfoTexts_m8_1051(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.UtilityDemo::DisplayFeatureFunctionalities()
extern TypeInfo* GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5355;
extern Il2CppCodeGenString* _stringLiteral5356;
extern Il2CppCodeGenString* _stringLiteral56;
extern Il2CppCodeGenString* _stringLiteral5357;
extern Il2CppCodeGenString* _stringLiteral5358;
extern Il2CppCodeGenString* _stringLiteral5359;
extern Il2CppCodeGenString* _stringLiteral5360;
extern Il2CppCodeGenString* _stringLiteral5361;
extern Il2CppCodeGenString* _stringLiteral5362;
extern Il2CppCodeGenString* _stringLiteral5363;
extern Il2CppCodeGenString* _stringLiteral5364;
extern "C" void UtilityDemo_DisplayFeatureFunctionalities_m8_1109 (UtilityDemo_t8_189 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1651);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5355 = il2cpp_codegen_string_literal_from_index(5355);
		_stringLiteral5356 = il2cpp_codegen_string_literal_from_index(5356);
		_stringLiteral56 = il2cpp_codegen_string_literal_from_index(56);
		_stringLiteral5357 = il2cpp_codegen_string_literal_from_index(5357);
		_stringLiteral5358 = il2cpp_codegen_string_literal_from_index(5358);
		_stringLiteral5359 = il2cpp_codegen_string_literal_from_index(5359);
		_stringLiteral5360 = il2cpp_codegen_string_literal_from_index(5360);
		_stringLiteral5361 = il2cpp_codegen_string_literal_from_index(5361);
		_stringLiteral5362 = il2cpp_codegen_string_literal_from_index(5362);
		_stringLiteral5363 = il2cpp_codegen_string_literal_from_index(5363);
		_stringLiteral5364 = il2cpp_codegen_string_literal_from_index(5364);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	String_t* V_1 = {0};
	{
		NPDemoBase_DisplayFeatureFunctionalities_m8_1050(__this, /*hidden argument*/NULL);
		bool L_0 = GUILayout_Button_m6_1252(NULL /*static, unused*/, _stringLiteral5355, ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0038;
		}
	}
	{
		String_t* L_1 = UtilityDemo_GetUUID_m8_1110(__this, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m1_560(NULL /*static, unused*/, _stringLiteral5356, L_2, _stringLiteral56, /*hidden argument*/NULL);
		DemoSubMenu_AddNewResult_m8_135(__this, L_3, /*hidden argument*/NULL);
	}

IL_0038:
	{
		bool L_4 = GUILayout_Button_m6_1252(NULL /*static, unused*/, _stringLiteral5357, ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0075;
		}
	}
	{
		ApplicationSettings_t8_320 * L_5 = NPSettings_get_Application_m8_1934(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		String_t* L_6 = ApplicationSettings_get_StoreIdentifier_m8_1887(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		String_t* L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m1_560(NULL /*static, unused*/, _stringLiteral5358, L_7, _stringLiteral56, /*hidden argument*/NULL);
		DemoSubMenu_AddNewResult_m8_135(__this, L_8, /*hidden argument*/NULL);
		String_t* L_9 = V_1;
		UtilityDemo_OpenStoreLink_m8_1111(__this, L_9, /*hidden argument*/NULL);
	}

IL_0075:
	{
		bool L_10 = GUILayout_Button_m6_1252(NULL /*static, unused*/, _stringLiteral5359, ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0090;
		}
	}
	{
		UtilityDemo_AskForReviewNow_m8_1112(__this, /*hidden argument*/NULL);
	}

IL_0090:
	{
		bool L_11 = GUILayout_Button_m6_1252(NULL /*static, unused*/, _stringLiteral5360, ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_00ab;
		}
	}
	{
		UtilityDemo_SetApplicationIconBadgeNumber_m8_1113(__this, /*hidden argument*/NULL);
	}

IL_00ab:
	{
		bool L_12 = GUILayout_Button_m6_1252(NULL /*static, unused*/, _stringLiteral5361, ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_00df;
		}
	}
	{
		Utility_t8_306 * L_13 = NPBinding_get_Utility_m8_1931(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_13);
		String_t* L_14 = Utility_GetBundleVersion_m8_1799(L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Concat_m1_560(NULL /*static, unused*/, _stringLiteral5362, L_14, _stringLiteral56, /*hidden argument*/NULL);
		DemoSubMenu_AddNewResult_m8_135(__this, L_15, /*hidden argument*/NULL);
	}

IL_00df:
	{
		bool L_16 = GUILayout_Button_m6_1252(NULL /*static, unused*/, _stringLiteral5363, ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_0113;
		}
	}
	{
		Utility_t8_306 * L_17 = NPBinding_get_Utility_m8_1931(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_17);
		String_t* L_18 = Utility_GetBundleIdentifier_m8_1800(L_17, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Concat_m1_560(NULL /*static, unused*/, _stringLiteral5364, L_18, _stringLiteral56, /*hidden argument*/NULL);
		DemoSubMenu_AddNewResult_m8_135(__this, L_19, /*hidden argument*/NULL);
	}

IL_0113:
	{
		return;
	}
}
// System.String VoxelBusters.NativePlugins.Demo.UtilityDemo::GetUUID()
extern "C" String_t* UtilityDemo_GetUUID_m8_1110 (UtilityDemo_t8_189 * __this, const MethodInfo* method)
{
	{
		Utility_t8_306 * L_0 = NPBinding_get_Utility_m8_1931(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(5 /* System.String VoxelBusters.NativePlugins.Utility::GetUUID() */, L_0);
		return L_1;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.UtilityDemo::OpenStoreLink(System.String)
extern "C" void UtilityDemo_OpenStoreLink_m8_1111 (UtilityDemo_t8_189 * __this, String_t* ____applicationID, const MethodInfo* method)
{
	{
		Utility_t8_306 * L_0 = NPBinding_get_Utility_m8_1931(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = ____applicationID;
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(6 /* System.Void VoxelBusters.NativePlugins.Utility::OpenStoreLink(System.String) */, L_0, L_1);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.UtilityDemo::AskForReviewNow()
extern Il2CppCodeGenString* _stringLiteral5365;
extern "C" void UtilityDemo_AskForReviewNow_m8_1112 (UtilityDemo_t8_189 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral5365 = il2cpp_codegen_string_literal_from_index(5365);
		s_Il2CppMethodIntialized = true;
	}
	{
		UtilitySettings_t8_309 * L_0 = NPSettings_get_Utility_m8_1935(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Settings_t8_310 * L_1 = UtilitySettings_get_RateMyApp_m8_1806(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_2 = Settings_get_IsEnabled_m8_1941(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		Utility_t8_306 * L_3 = NPBinding_get_Utility_m8_1931(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		RateMyApp_t8_307 * L_4 = Utility_get_RateMyApp_m8_1793(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		RateMyApp_AskForReviewNow_m8_1957(L_4, /*hidden argument*/NULL);
		goto IL_0033;
	}

IL_0028:
	{
		DemoSubMenu_AddNewResult_m8_135(__this, _stringLiteral5365, /*hidden argument*/NULL);
	}

IL_0033:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.UtilityDemo::SetApplicationIconBadgeNumber()
extern "C" void UtilityDemo_SetApplicationIconBadgeNumber_m8_1113 (UtilityDemo_t8_189 * __this, const MethodInfo* method)
{
	{
		Utility_t8_306 * L_0 = NPBinding_get_Utility_m8_1931(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = (__this->___m_applicationBadgeNumber_13);
		NullCheck(L_0);
		VirtActionInvoker1< int32_t >::Invoke(7 /* System.Void VoxelBusters.NativePlugins.Utility::SetApplicationIconBadgeNumber(System.Int32) */, L_0, L_1);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.WebViewDemo::.ctor()
extern Il2CppCodeGenString* _stringLiteral5366;
extern "C" void WebViewDemo__ctor_m8_1114 (WebViewDemo_t8_190 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral5366 = il2cpp_codegen_string_literal_from_index(5366);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___m_URLSchemeName_14 = _stringLiteral5366;
		NPDisabledFeatureDemo__ctor_m8_1053(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.Utility::.ctor()
extern "C" void Utility__ctor_m8_1115 (Utility_t8_192 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m6_671(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Demo.Utility::Start()
extern Il2CppCodeGenString* _stringLiteral5367;
extern "C" void Utility_Start_m8_1116 (Utility_t8_192 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral5367 = il2cpp_codegen_string_literal_from_index(5367);
		s_Il2CppMethodIntialized = true;
	}
	{
		Application_CaptureScreenshot_m6_589(NULL /*static, unused*/, _stringLiteral5367, /*hidden argument*/NULL);
		return;
	}
}
// System.String VoxelBusters.NativePlugins.Demo.Utility::GetScreenshotPath()
extern TypeInfo* Path_t1_437_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5367;
extern "C" String_t* Utility_GetScreenshotPath_m8_1117 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Path_t1_437_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(131);
		_stringLiteral5367 = il2cpp_codegen_string_literal_from_index(5367);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = Application_get_persistentDataPath_m6_591(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1_437_il2cpp_TypeInfo_var);
		String_t* L_1 = Path_Combine_m1_5123(NULL /*static, unused*/, L_0, _stringLiteral5367, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void VoxelBusters.NativePlugins.AddressBook/RequestAccessCompletion::.ctor(System.Object,System.IntPtr)
extern "C" void RequestAccessCompletion__ctor_m8_1118 (RequestAccessCompletion_t8_193 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void VoxelBusters.NativePlugins.AddressBook/RequestAccessCompletion::Invoke(VoxelBusters.NativePlugins.eABAuthorizationStatus,System.String)
extern "C" void RequestAccessCompletion_Invoke_m8_1119 (RequestAccessCompletion_t8_193 * __this, int32_t ____authorizationStatus, String_t* ____error, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		RequestAccessCompletion_Invoke_m8_1119((RequestAccessCompletion_t8_193 *)__this->___prev_9,____authorizationStatus, ____error, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, int32_t ____authorizationStatus, String_t* ____error, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,____authorizationStatus, ____error,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, int32_t ____authorizationStatus, String_t* ____error, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,____authorizationStatus, ____error,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_RequestAccessCompletion_t8_193(Il2CppObject* delegate, int32_t ____authorizationStatus, String_t* ____error)
{
	typedef void (STDCALL *native_function_ptr_type)(int32_t, char*);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '____authorizationStatus' to native representation

	// Marshaling of parameter '____error' to native representation
	char* _____error_marshaled = { 0 };
	_____error_marshaled = il2cpp_codegen_marshal_string(____error);

	// Native function invocation
	_il2cpp_pinvoke_func(____authorizationStatus, _____error_marshaled);

	// Marshaling cleanup of parameter '____authorizationStatus' native representation

	// Marshaling cleanup of parameter '____error' native representation
	il2cpp_codegen_marshal_free(_____error_marshaled);
	_____error_marshaled = NULL;

}
// System.IAsyncResult VoxelBusters.NativePlugins.AddressBook/RequestAccessCompletion::BeginInvoke(VoxelBusters.NativePlugins.eABAuthorizationStatus,System.String,System.AsyncCallback,System.Object)
extern TypeInfo* eABAuthorizationStatus_t8_202_il2cpp_TypeInfo_var;
extern "C" Object_t * RequestAccessCompletion_BeginInvoke_m8_1120 (RequestAccessCompletion_t8_193 * __this, int32_t ____authorizationStatus, String_t* ____error, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		eABAuthorizationStatus_t8_202_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2112);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(eABAuthorizationStatus_t8_202_il2cpp_TypeInfo_var, &____authorizationStatus);
	__d_args[1] = ____error;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void VoxelBusters.NativePlugins.AddressBook/RequestAccessCompletion::EndInvoke(System.IAsyncResult)
extern "C" void RequestAccessCompletion_EndInvoke_m8_1121 (RequestAccessCompletion_t8_193 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void VoxelBusters.NativePlugins.AddressBook/ReadContactsCompletion::.ctor(System.Object,System.IntPtr)
extern "C" void ReadContactsCompletion__ctor_m8_1122 (ReadContactsCompletion_t8_194 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void VoxelBusters.NativePlugins.AddressBook/ReadContactsCompletion::Invoke(VoxelBusters.NativePlugins.eABAuthorizationStatus,VoxelBusters.NativePlugins.AddressBookContact[])
extern "C" void ReadContactsCompletion_Invoke_m8_1123 (ReadContactsCompletion_t8_194 * __this, int32_t ____authorizationStatus, AddressBookContactU5BU5D_t8_177* ____contactList, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		ReadContactsCompletion_Invoke_m8_1123((ReadContactsCompletion_t8_194 *)__this->___prev_9,____authorizationStatus, ____contactList, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, int32_t ____authorizationStatus, AddressBookContactU5BU5D_t8_177* ____contactList, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,____authorizationStatus, ____contactList,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, int32_t ____authorizationStatus, AddressBookContactU5BU5D_t8_177* ____contactList, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,____authorizationStatus, ____contactList,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_ReadContactsCompletion_t8_194(Il2CppObject* delegate, int32_t ____authorizationStatus, AddressBookContactU5BU5D_t8_177* ____contactList)
{
	// Marshaling of parameter '____contactList' to native representation
	AddressBookContactU5BU5D_t8_177* _____contactList_marshaled = { 0 };
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'VoxelBusters.NativePlugins.AddressBookContact[]'."));
}
// System.IAsyncResult VoxelBusters.NativePlugins.AddressBook/ReadContactsCompletion::BeginInvoke(VoxelBusters.NativePlugins.eABAuthorizationStatus,VoxelBusters.NativePlugins.AddressBookContact[],System.AsyncCallback,System.Object)
extern TypeInfo* eABAuthorizationStatus_t8_202_il2cpp_TypeInfo_var;
extern "C" Object_t * ReadContactsCompletion_BeginInvoke_m8_1124 (ReadContactsCompletion_t8_194 * __this, int32_t ____authorizationStatus, AddressBookContactU5BU5D_t8_177* ____contactList, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		eABAuthorizationStatus_t8_202_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2112);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(eABAuthorizationStatus_t8_202_il2cpp_TypeInfo_var, &____authorizationStatus);
	__d_args[1] = ____contactList;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void VoxelBusters.NativePlugins.AddressBook/ReadContactsCompletion::EndInvoke(System.IAsyncResult)
extern "C" void ReadContactsCompletion_EndInvoke_m8_1125 (ReadContactsCompletion_t8_194 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void VoxelBusters.NativePlugins.AddressBook/<ReadContacts>c__AnonStorey9::.ctor()
extern "C" void U3CReadContactsU3Ec__AnonStorey9__ctor_m8_1126 (U3CReadContactsU3Ec__AnonStorey9_t8_195 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.AddressBook/<ReadContacts>c__AnonStorey9::<>m__8(VoxelBusters.NativePlugins.eABAuthorizationStatus,System.String)
extern "C" void U3CReadContactsU3Ec__AnonStorey9_U3CU3Em__8_m8_1127 (U3CReadContactsU3Ec__AnonStorey9_t8_195 * __this, int32_t ____newAuthStatus, String_t* ____error, const MethodInfo* method)
{
	{
		AddressBook_t8_196 * L_0 = (__this->___U3CU3Ef__this_1);
		int32_t L_1 = ____newAuthStatus;
		ReadContactsCompletion_t8_194 * L_2 = (__this->____onCompletion_0);
		NullCheck(L_0);
		VirtActionInvoker2< int32_t, ReadContactsCompletion_t8_194 * >::Invoke(8 /* System.Void VoxelBusters.NativePlugins.AddressBook::ReadContacts(VoxelBusters.NativePlugins.eABAuthorizationStatus,VoxelBusters.NativePlugins.AddressBook/ReadContactsCompletion) */, L_0, L_1, L_2);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.AddressBook::.ctor()
extern "C" void AddressBook__ctor_m8_1128 (AddressBook_t8_196 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m6_671(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.AddressBook::ABRequestAccessFinished(System.String)
extern "C" void AddressBook_ABRequestAccessFinished_m8_1129 (AddressBook_t8_196 * __this, String_t* ____dataStr, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.AddressBook::ABRequestAccessFinished(VoxelBusters.NativePlugins.eABAuthorizationStatus,System.String)
extern TypeInfo* eABAuthorizationStatus_t8_202_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5369;
extern "C" void AddressBook_ABRequestAccessFinished_m8_1130 (AddressBook_t8_196 * __this, int32_t ____authStatus, String_t* ____error, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		eABAuthorizationStatus_t8_202_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2112);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5369 = il2cpp_codegen_string_literal_from_index(5369);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ____authStatus;
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(eABAuthorizationStatus_t8_202_il2cpp_TypeInfo_var, &L_1);
		String_t* L_3 = ____error;
		String_t* L_4 = StringExtensions_GetPrintableString_m8_224(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Format_m1_549(NULL /*static, unused*/, _stringLiteral5369, L_2, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_Log_m8_1007(NULL /*static, unused*/, _stringLiteral5368, L_5, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		RequestAccessCompletion_t8_193 * L_6 = (__this->___RequestAccessFinishedEvent_2);
		if (!L_6)
		{
			goto IL_0039;
		}
	}
	{
		RequestAccessCompletion_t8_193 * L_7 = (__this->___RequestAccessFinishedEvent_2);
		int32_t L_8 = ____authStatus;
		String_t* L_9 = ____error;
		NullCheck(L_7);
		RequestAccessCompletion_Invoke_m8_1119(L_7, L_8, L_9, /*hidden argument*/NULL);
	}

IL_0039:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.AddressBook::ABReadContactsFinished(System.String)
extern TypeInfo* IDictionary_t1_35_il2cpp_TypeInfo_var;
extern "C" void AddressBook_ABReadContactsFinished_m8_1131 (AddressBook_t8_196 * __this, String_t* ____data, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionary_t1_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	AddressBookContactU5BU5D_t8_177* V_1 = {0};
	Object_t * V_2 = {0};
	{
		String_t* L_0 = ____data;
		Object_t * L_1 = JSONUtility_FromJSON_m8_298(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_2 = ((Object_t *)IsInst(L_1, IDictionary_t1_35_il2cpp_TypeInfo_var));
		Object_t * L_2 = V_2;
		VirtActionInvoker3< Object_t *, int32_t*, AddressBookContactU5BU5D_t8_177** >::Invoke(5 /* System.Void VoxelBusters.NativePlugins.AddressBook::ParseReadContactsResponseData(System.Collections.IDictionary,VoxelBusters.NativePlugins.eABAuthorizationStatus&,VoxelBusters.NativePlugins.AddressBookContact[]&) */, __this, L_2, (&V_0), (&V_1));
		int32_t L_3 = V_0;
		AddressBookContactU5BU5D_t8_177* L_4 = V_1;
		AddressBook_ABReadContactsFinished_m8_1132(__this, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.AddressBook::ABReadContactsFinished(VoxelBusters.NativePlugins.eABAuthorizationStatus,VoxelBusters.NativePlugins.AddressBookContact[])
extern TypeInfo* eABAuthorizationStatus_t8_202_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Console_t8_169_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5368;
extern Il2CppCodeGenString* _stringLiteral5370;
extern "C" void AddressBook_ABReadContactsFinished_m8_1132 (AddressBook_t8_196 * __this, int32_t ____authStatus, AddressBookContactU5BU5D_t8_177* ____contactsList, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		eABAuthorizationStatus_t8_202_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2112);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Console_t8_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2095);
		_stringLiteral5368 = il2cpp_codegen_string_literal_from_index(5368);
		_stringLiteral5370 = il2cpp_codegen_string_literal_from_index(5370);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ____authStatus;
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(eABAuthorizationStatus_t8_202_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Format_m1_548(NULL /*static, unused*/, _stringLiteral5370, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Console_t8_169_il2cpp_TypeInfo_var);
		Console_Log_m8_1007(NULL /*static, unused*/, _stringLiteral5368, L_3, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		ReadContactsCompletion_t8_194 * L_4 = (__this->___ReadContactsFinishedEvent_3);
		if (!L_4)
		{
			goto IL_0033;
		}
	}
	{
		ReadContactsCompletion_t8_194 * L_5 = (__this->___ReadContactsFinishedEvent_3);
		int32_t L_6 = ____authStatus;
		AddressBookContactU5BU5D_t8_177* L_7 = ____contactsList;
		NullCheck(L_5);
		ReadContactsCompletion_Invoke_m8_1123(L_5, L_6, L_7, /*hidden argument*/NULL);
	}

IL_0033:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.AddressBook::ParseReadContactsResponseData(System.Collections.IDictionary,VoxelBusters.NativePlugins.eABAuthorizationStatus&,VoxelBusters.NativePlugins.AddressBookContact[]&)
extern "C" void AddressBook_ParseReadContactsResponseData_m8_1133 (AddressBook_t8_196 * __this, Object_t * ____dataDict, int32_t* ____authStatus, AddressBookContactU5BU5D_t8_177** ____contactsList, const MethodInfo* method)
{
	{
		AddressBookContactU5BU5D_t8_177** L_0 = ____contactsList;
		*((Object_t **)(L_0)) = (Object_t *)NULL;
		int32_t* L_1 = ____authStatus;
		*((int32_t*)(L_1)) = (int32_t)2;
		return;
	}
}
// VoxelBusters.NativePlugins.eABAuthorizationStatus VoxelBusters.NativePlugins.AddressBook::GetAuthorizationStatus()
extern "C" int32_t AddressBook_GetAuthorizationStatus_m8_1134 (AddressBook_t8_196 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(0);
	}
}
// System.Void VoxelBusters.NativePlugins.AddressBook::RequestAccess(VoxelBusters.NativePlugins.AddressBook/RequestAccessCompletion)
extern "C" void AddressBook_RequestAccess_m8_1135 (AddressBook_t8_196 * __this, RequestAccessCompletion_t8_193 * ____onCompletion, const MethodInfo* method)
{
	{
		RequestAccessCompletion_t8_193 * L_0 = ____onCompletion;
		__this->___RequestAccessFinishedEvent_2 = L_0;
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.AddressBook::ReadContacts(VoxelBusters.NativePlugins.AddressBook/ReadContactsCompletion)
extern TypeInfo* U3CReadContactsU3Ec__AnonStorey9_t8_195_il2cpp_TypeInfo_var;
extern TypeInfo* RequestAccessCompletion_t8_193_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CReadContactsU3Ec__AnonStorey9_U3CU3Em__8_m8_1127_MethodInfo_var;
extern "C" void AddressBook_ReadContacts_m8_1136 (AddressBook_t8_196 * __this, ReadContactsCompletion_t8_194 * ____onCompletion, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CReadContactsU3Ec__AnonStorey9_t8_195_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2113);
		RequestAccessCompletion_t8_193_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2114);
		U3CReadContactsU3Ec__AnonStorey9_U3CU3Em__8_m8_1127_MethodInfo_var = il2cpp_codegen_method_info_from_index(573);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	U3CReadContactsU3Ec__AnonStorey9_t8_195 * V_1 = {0};
	{
		U3CReadContactsU3Ec__AnonStorey9_t8_195 * L_0 = (U3CReadContactsU3Ec__AnonStorey9_t8_195 *)il2cpp_codegen_object_new (U3CReadContactsU3Ec__AnonStorey9_t8_195_il2cpp_TypeInfo_var);
		U3CReadContactsU3Ec__AnonStorey9__ctor_m8_1126(L_0, /*hidden argument*/NULL);
		V_1 = L_0;
		U3CReadContactsU3Ec__AnonStorey9_t8_195 * L_1 = V_1;
		ReadContactsCompletion_t8_194 * L_2 = ____onCompletion;
		NullCheck(L_1);
		L_1->____onCompletion_0 = L_2;
		U3CReadContactsU3Ec__AnonStorey9_t8_195 * L_3 = V_1;
		NullCheck(L_3);
		L_3->___U3CU3Ef__this_1 = __this;
		int32_t L_4 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* VoxelBusters.NativePlugins.eABAuthorizationStatus VoxelBusters.NativePlugins.AddressBook::GetAuthorizationStatus() */, __this);
		V_0 = L_4;
		int32_t L_5 = V_0;
		if (L_5)
		{
			goto IL_0038;
		}
	}
	{
		U3CReadContactsU3Ec__AnonStorey9_t8_195 * L_6 = V_1;
		IntPtr_t L_7 = { (void*)U3CReadContactsU3Ec__AnonStorey9_U3CU3Em__8_m8_1127_MethodInfo_var };
		RequestAccessCompletion_t8_193 * L_8 = (RequestAccessCompletion_t8_193 *)il2cpp_codegen_object_new (RequestAccessCompletion_t8_193_il2cpp_TypeInfo_var);
		RequestAccessCompletion__ctor_m8_1118(L_8, L_6, L_7, /*hidden argument*/NULL);
		VirtActionInvoker1< RequestAccessCompletion_t8_193 * >::Invoke(7 /* System.Void VoxelBusters.NativePlugins.AddressBook::RequestAccess(VoxelBusters.NativePlugins.AddressBook/RequestAccessCompletion) */, __this, L_8);
		goto IL_0045;
	}

IL_0038:
	{
		int32_t L_9 = V_0;
		U3CReadContactsU3Ec__AnonStorey9_t8_195 * L_10 = V_1;
		NullCheck(L_10);
		ReadContactsCompletion_t8_194 * L_11 = (L_10->____onCompletion_0);
		VirtActionInvoker2< int32_t, ReadContactsCompletion_t8_194 * >::Invoke(8 /* System.Void VoxelBusters.NativePlugins.AddressBook::ReadContacts(VoxelBusters.NativePlugins.eABAuthorizationStatus,VoxelBusters.NativePlugins.AddressBook/ReadContactsCompletion) */, __this, L_9, L_11);
	}

IL_0045:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.AddressBook::ReadContacts(VoxelBusters.NativePlugins.eABAuthorizationStatus,VoxelBusters.NativePlugins.AddressBook/ReadContactsCompletion)
extern "C" void AddressBook_ReadContacts_m8_1137 (AddressBook_t8_196 * __this, int32_t ____status, ReadContactsCompletion_t8_194 * ____onCompletion, const MethodInfo* method)
{
	{
		ReadContactsCompletion_t8_194 * L_0 = ____onCompletion;
		__this->___ReadContactsFinishedEvent_3 = L_0;
		int32_t L_1 = ____status;
		if ((((int32_t)L_1) == ((int32_t)3)))
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_2 = ____status;
		AddressBook_ABReadContactsFinished_m8_1132(__this, L_2, (AddressBookContactU5BU5D_t8_177*)(AddressBookContactU5BU5D_t8_177*)NULL, /*hidden argument*/NULL);
		return;
	}

IL_0017:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.AddressBookContact/<GetImageAsync>c__AnonStoreyA::.ctor()
extern "C" void U3CGetImageAsyncU3Ec__AnonStoreyA__ctor_m8_1138 (U3CGetImageAsyncU3Ec__AnonStoreyA_t8_197 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.AddressBookContact/<GetImageAsync>c__AnonStoreyA::<>m__9(UnityEngine.Texture2D,System.String)
extern "C" void U3CGetImageAsyncU3Ec__AnonStoreyA_U3CU3Em__9_m8_1139 (U3CGetImageAsyncU3Ec__AnonStoreyA_t8_197 * __this, Texture2D_t6_33 * ____newTexture, String_t* ____error, const MethodInfo* method)
{
	{
		AddressBookContact_t8_198 * L_0 = (__this->___U3CU3Ef__this_1);
		Texture2D_t6_33 * L_1 = ____newTexture;
		NullCheck(L_0);
		L_0->___m_image_3 = L_1;
		AddressBookContact_t8_198 * L_2 = (__this->___U3CU3Ef__this_1);
		String_t* L_3 = ____error;
		NullCheck(L_2);
		L_2->___m_imageDownloadError_4 = L_3;
		Completion_t8_161 * L_4 = (__this->____onCompletion_0);
		if (!L_4)
		{
			goto IL_0030;
		}
	}
	{
		Completion_t8_161 * L_5 = (__this->____onCompletion_0);
		Texture2D_t6_33 * L_6 = ____newTexture;
		String_t* L_7 = ____error;
		NullCheck(L_5);
		Completion_Invoke_m8_926(L_5, L_6, L_7, /*hidden argument*/NULL);
	}

IL_0030:
	{
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.AddressBookContact::.ctor()
extern TypeInfo* StringU5BU5D_t1_238_il2cpp_TypeInfo_var;
extern "C" void AddressBookContact__ctor_m8_1140 (AddressBookContact_t8_198 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringU5BU5D_t1_238_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(35);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		AddressBookContact_set_FirstName_m8_1143(__this, (String_t*)NULL, /*hidden argument*/NULL);
		AddressBookContact_set_LastName_m8_1145(__this, (String_t*)NULL, /*hidden argument*/NULL);
		AddressBookContact_set_ImagePath_m8_1147(__this, (String_t*)NULL, /*hidden argument*/NULL);
		__this->___m_image_3 = (Texture2D_t6_33 *)NULL;
		__this->___m_imageDownloadError_4 = (String_t*)NULL;
		AddressBookContact_set_PhoneNumberList_m8_1149(__this, ((StringU5BU5D_t1_238*)SZArrayNew(StringU5BU5D_t1_238_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		AddressBookContact_set_EmailIDList_m8_1151(__this, ((StringU5BU5D_t1_238*)SZArrayNew(StringU5BU5D_t1_238_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.AddressBookContact::.ctor(VoxelBusters.NativePlugins.AddressBookContact)
extern "C" void AddressBookContact__ctor_m8_1141 (AddressBookContact_t8_198 * __this, AddressBookContact_t8_198 * ____source, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		AddressBookContact_t8_198 * L_0 = ____source;
		NullCheck(L_0);
		String_t* L_1 = AddressBookContact_get_FirstName_m8_1142(L_0, /*hidden argument*/NULL);
		AddressBookContact_set_FirstName_m8_1143(__this, L_1, /*hidden argument*/NULL);
		AddressBookContact_t8_198 * L_2 = ____source;
		NullCheck(L_2);
		String_t* L_3 = AddressBookContact_get_LastName_m8_1144(L_2, /*hidden argument*/NULL);
		AddressBookContact_set_LastName_m8_1145(__this, L_3, /*hidden argument*/NULL);
		AddressBookContact_t8_198 * L_4 = ____source;
		NullCheck(L_4);
		String_t* L_5 = AddressBookContact_get_ImagePath_m8_1146(L_4, /*hidden argument*/NULL);
		AddressBookContact_set_ImagePath_m8_1147(__this, L_5, /*hidden argument*/NULL);
		AddressBookContact_t8_198 * L_6 = ____source;
		NullCheck(L_6);
		Texture2D_t6_33 * L_7 = (L_6->___m_image_3);
		__this->___m_image_3 = L_7;
		AddressBookContact_t8_198 * L_8 = ____source;
		NullCheck(L_8);
		String_t* L_9 = (L_8->___m_imageDownloadError_4);
		__this->___m_imageDownloadError_4 = L_9;
		AddressBookContact_t8_198 * L_10 = ____source;
		NullCheck(L_10);
		StringU5BU5D_t1_238* L_11 = AddressBookContact_get_PhoneNumberList_m8_1148(L_10, /*hidden argument*/NULL);
		AddressBookContact_set_PhoneNumberList_m8_1149(__this, L_11, /*hidden argument*/NULL);
		AddressBookContact_t8_198 * L_12 = ____source;
		NullCheck(L_12);
		StringU5BU5D_t1_238* L_13 = AddressBookContact_get_EmailIDList_m8_1150(L_12, /*hidden argument*/NULL);
		AddressBookContact_set_EmailIDList_m8_1151(__this, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.String VoxelBusters.NativePlugins.AddressBookContact::get_FirstName()
extern "C" String_t* AddressBookContact_get_FirstName_m8_1142 (AddressBookContact_t8_198 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_firstName_1);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.AddressBookContact::set_FirstName(System.String)
extern "C" void AddressBookContact_set_FirstName_m8_1143 (AddressBookContact_t8_198 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_firstName_1 = L_0;
		return;
	}
}
// System.String VoxelBusters.NativePlugins.AddressBookContact::get_LastName()
extern "C" String_t* AddressBookContact_get_LastName_m8_1144 (AddressBookContact_t8_198 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_lastName_2);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.AddressBookContact::set_LastName(System.String)
extern "C" void AddressBookContact_set_LastName_m8_1145 (AddressBookContact_t8_198 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_lastName_2 = L_0;
		return;
	}
}
// System.String VoxelBusters.NativePlugins.AddressBookContact::get_ImagePath()
extern "C" String_t* AddressBookContact_get_ImagePath_m8_1146 (AddressBookContact_t8_198 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CImagePathU3Ek__BackingField_7);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.AddressBookContact::set_ImagePath(System.String)
extern "C" void AddressBookContact_set_ImagePath_m8_1147 (AddressBookContact_t8_198 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CImagePathU3Ek__BackingField_7 = L_0;
		return;
	}
}
// System.String[] VoxelBusters.NativePlugins.AddressBookContact::get_PhoneNumberList()
extern "C" StringU5BU5D_t1_238* AddressBookContact_get_PhoneNumberList_m8_1148 (AddressBookContact_t8_198 * __this, const MethodInfo* method)
{
	{
		StringU5BU5D_t1_238* L_0 = (__this->___m_phoneNumberList_5);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.AddressBookContact::set_PhoneNumberList(System.String[])
extern "C" void AddressBookContact_set_PhoneNumberList_m8_1149 (AddressBookContact_t8_198 * __this, StringU5BU5D_t1_238* ___value, const MethodInfo* method)
{
	{
		StringU5BU5D_t1_238* L_0 = ___value;
		__this->___m_phoneNumberList_5 = L_0;
		return;
	}
}
// System.String[] VoxelBusters.NativePlugins.AddressBookContact::get_EmailIDList()
extern "C" StringU5BU5D_t1_238* AddressBookContact_get_EmailIDList_m8_1150 (AddressBookContact_t8_198 * __this, const MethodInfo* method)
{
	{
		StringU5BU5D_t1_238* L_0 = (__this->___m_emailIDList_6);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.AddressBookContact::set_EmailIDList(System.String[])
extern "C" void AddressBookContact_set_EmailIDList_m8_1151 (AddressBookContact_t8_198 * __this, StringU5BU5D_t1_238* ___value, const MethodInfo* method)
{
	{
		StringU5BU5D_t1_238* L_0 = ___value;
		__this->___m_emailIDList_6 = L_0;
		return;
	}
}
// UnityEngine.Texture2D VoxelBusters.NativePlugins.AddressBookContact::GetDefaultImage()
extern TypeInfo* AddressBookContact_t8_198_il2cpp_TypeInfo_var;
extern TypeInfo* Texture2D_t6_33_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5371;
extern "C" Texture2D_t6_33 * AddressBookContact_GetDefaultImage_m8_1152 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddressBookContact_t8_198_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2115);
		Texture2D_t6_33_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1581);
		_stringLiteral5371 = il2cpp_codegen_string_literal_from_index(5371);
		s_Il2CppMethodIntialized = true;
	}
	{
		Texture2D_t6_33 * L_0 = ((AddressBookContact_t8_198_StaticFields*)AddressBookContact_t8_198_il2cpp_TypeInfo_var->static_fields)->___defaultImage_0;
		bool L_1 = Object_op_Equality_m6_721(NULL /*static, unused*/, L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0024;
		}
	}
	{
		Object_t6_5 * L_2 = Resources_Load_m6_487(NULL /*static, unused*/, _stringLiteral5371, /*hidden argument*/NULL);
		((AddressBookContact_t8_198_StaticFields*)AddressBookContact_t8_198_il2cpp_TypeInfo_var->static_fields)->___defaultImage_0 = ((Texture2D_t6_33 *)IsInstSealed(L_2, Texture2D_t6_33_il2cpp_TypeInfo_var));
	}

IL_0024:
	{
		Texture2D_t6_33 * L_3 = ((AddressBookContact_t8_198_StaticFields*)AddressBookContact_t8_198_il2cpp_TypeInfo_var->static_fields)->___defaultImage_0;
		return L_3;
	}
}
// System.Void VoxelBusters.NativePlugins.AddressBookContact::GetImageAsync(VoxelBusters.Utility.DownloadTexture/Completion)
extern TypeInfo* U3CGetImageAsyncU3Ec__AnonStoreyA_t8_197_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* DownloadTexture_t8_162_il2cpp_TypeInfo_var;
extern TypeInfo* Completion_t8_161_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CGetImageAsyncU3Ec__AnonStoreyA_U3CU3Em__9_m8_1139_MethodInfo_var;
extern "C" void AddressBookContact_GetImageAsync_m8_1153 (AddressBookContact_t8_198 * __this, Completion_t8_161 * ____onCompletion, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CGetImageAsyncU3Ec__AnonStoreyA_t8_197_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2116);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		DownloadTexture_t8_162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2087);
		Completion_t8_161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2088);
		U3CGetImageAsyncU3Ec__AnonStoreyA_U3CU3Em__9_m8_1139_MethodInfo_var = il2cpp_codegen_method_info_from_index(574);
		s_Il2CppMethodIntialized = true;
	}
	URL_t8_156  V_0 = {0};
	DownloadTexture_t8_162 * V_1 = {0};
	U3CGetImageAsyncU3Ec__AnonStoreyA_t8_197 * V_2 = {0};
	{
		U3CGetImageAsyncU3Ec__AnonStoreyA_t8_197 * L_0 = (U3CGetImageAsyncU3Ec__AnonStoreyA_t8_197 *)il2cpp_codegen_object_new (U3CGetImageAsyncU3Ec__AnonStoreyA_t8_197_il2cpp_TypeInfo_var);
		U3CGetImageAsyncU3Ec__AnonStoreyA__ctor_m8_1138(L_0, /*hidden argument*/NULL);
		V_2 = L_0;
		U3CGetImageAsyncU3Ec__AnonStoreyA_t8_197 * L_1 = V_2;
		Completion_t8_161 * L_2 = ____onCompletion;
		NullCheck(L_1);
		L_1->____onCompletion_0 = L_2;
		U3CGetImageAsyncU3Ec__AnonStoreyA_t8_197 * L_3 = V_2;
		NullCheck(L_3);
		L_3->___U3CU3Ef__this_1 = __this;
		Texture2D_t6_33 * L_4 = (__this->___m_image_3);
		bool L_5 = Object_op_Inequality_m6_722(NULL /*static, unused*/, L_4, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0030;
		}
	}
	{
		String_t* L_6 = (__this->___m_imageDownloadError_4);
		if (!L_6)
		{
			goto IL_0048;
		}
	}

IL_0030:
	{
		U3CGetImageAsyncU3Ec__AnonStoreyA_t8_197 * L_7 = V_2;
		NullCheck(L_7);
		Completion_t8_161 * L_8 = (L_7->____onCompletion_0);
		Texture2D_t6_33 * L_9 = (__this->___m_image_3);
		String_t* L_10 = (__this->___m_imageDownloadError_4);
		NullCheck(L_8);
		Completion_Invoke_m8_926(L_8, L_9, L_10, /*hidden argument*/NULL);
		return;
	}

IL_0048:
	{
		String_t* L_11 = AddressBookContact_get_ImagePath_m8_1146(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_12 = String_IsNullOrEmpty_m1_521(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_006a;
		}
	}
	{
		U3CGetImageAsyncU3Ec__AnonStoreyA_t8_197 * L_13 = V_2;
		NullCheck(L_13);
		Completion_t8_161 * L_14 = (L_13->____onCompletion_0);
		Texture2D_t6_33 * L_15 = AddressBookContact_GetDefaultImage_m8_1152(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_14);
		Completion_Invoke_m8_926(L_14, L_15, (String_t*)NULL, /*hidden argument*/NULL);
		return;
	}

IL_006a:
	{
		String_t* L_16 = AddressBookContact_get_ImagePath_m8_1146(__this, /*hidden argument*/NULL);
		URL_t8_156  L_17 = URL_FileURLWithPath_m8_961(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		V_0 = L_17;
		URL_t8_156  L_18 = V_0;
		DownloadTexture_t8_162 * L_19 = (DownloadTexture_t8_162 *)il2cpp_codegen_object_new (DownloadTexture_t8_162_il2cpp_TypeInfo_var);
		DownloadTexture__ctor_m8_929(L_19, L_18, 1, 1, /*hidden argument*/NULL);
		V_1 = L_19;
		DownloadTexture_t8_162 * L_20 = V_1;
		U3CGetImageAsyncU3Ec__AnonStoreyA_t8_197 * L_21 = V_2;
		IntPtr_t L_22 = { (void*)U3CGetImageAsyncU3Ec__AnonStoreyA_U3CU3Em__9_m8_1139_MethodInfo_var };
		Completion_t8_161 * L_23 = (Completion_t8_161 *)il2cpp_codegen_object_new (Completion_t8_161_il2cpp_TypeInfo_var);
		Completion__ctor_m8_925(L_23, L_21, L_22, /*hidden argument*/NULL);
		NullCheck(L_20);
		DownloadTexture_set_OnCompletion_m8_935(L_20, L_23, /*hidden argument*/NULL);
		DownloadTexture_t8_162 * L_24 = V_1;
		NullCheck(L_24);
		Request_StartRequest_m8_900(L_24, /*hidden argument*/NULL);
		return;
	}
}
// System.String VoxelBusters.NativePlugins.AddressBookContact::ToString()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5372;
extern "C" String_t* AddressBookContact_ToString_m8_1154 (AddressBookContact_t8_198 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5372 = il2cpp_codegen_string_literal_from_index(5372);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = AddressBookContact_get_FirstName_m8_1142(__this, /*hidden argument*/NULL);
		String_t* L_1 = AddressBookContact_get_LastName_m8_1144(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Format_m1_549(NULL /*static, unused*/, _stringLiteral5372, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.AndroidAddressBookContact::.ctor(System.Collections.IDictionary)
extern TypeInfo* ICollection_t1_280_il2cpp_TypeInfo_var;
extern TypeInfo* StringU5BU5D_t1_238_il2cpp_TypeInfo_var;
extern TypeInfo* IList_t1_262_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var;
extern const MethodInfo* IDictionaryExtensions_GetIfAvailable_TisIList_t1_262_m8_2009_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral5373;
extern Il2CppCodeGenString* _stringLiteral5374;
extern Il2CppCodeGenString* _stringLiteral5375;
extern Il2CppCodeGenString* _stringLiteral5376;
extern Il2CppCodeGenString* _stringLiteral5377;
extern "C" void AndroidAddressBookContact__ctor_m8_1155 (AndroidAddressBookContact_t8_199 * __this, Object_t * ____contactInfoJsontDict, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ICollection_t1_280_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(149);
		StringU5BU5D_t1_238_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(35);
		IList_t1_262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(76);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484117);
		IDictionaryExtensions_GetIfAvailable_TisIList_t1_262_m8_2009_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484223);
		_stringLiteral5373 = il2cpp_codegen_string_literal_from_index(5373);
		_stringLiteral5374 = il2cpp_codegen_string_literal_from_index(5374);
		_stringLiteral5375 = il2cpp_codegen_string_literal_from_index(5375);
		_stringLiteral5376 = il2cpp_codegen_string_literal_from_index(5376);
		_stringLiteral5377 = il2cpp_codegen_string_literal_from_index(5377);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	String_t* V_1 = {0};
	Object_t * V_2 = {0};
	StringU5BU5D_t1_238* V_3 = {0};
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	Object_t * V_6 = {0};
	StringU5BU5D_t1_238* V_7 = {0};
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	{
		AddressBookContact__ctor_m8_1140(__this, /*hidden argument*/NULL);
		Object_t * L_0 = ____contactInfoJsontDict;
		String_t* L_1 = IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(NULL /*static, unused*/, L_0, _stringLiteral5373, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var);
		V_0 = L_1;
		Object_t * L_2 = ____contactInfoJsontDict;
		String_t* L_3 = IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(NULL /*static, unused*/, L_2, _stringLiteral5374, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var);
		V_1 = L_3;
		String_t* L_4 = V_0;
		AddressBookContact_set_FirstName_m8_1143(__this, L_4, /*hidden argument*/NULL);
		String_t* L_5 = V_1;
		AddressBookContact_set_LastName_m8_1145(__this, L_5, /*hidden argument*/NULL);
		Object_t * L_6 = ____contactInfoJsontDict;
		String_t* L_7 = IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(NULL /*static, unused*/, L_6, _stringLiteral5375, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var);
		AddressBookContact_set_ImagePath_m8_1147(__this, L_7, /*hidden argument*/NULL);
		Object_t * L_8 = ____contactInfoJsontDict;
		Object_t * L_9 = IDictionaryExtensions_GetIfAvailable_TisIList_t1_262_m8_2009(NULL /*static, unused*/, L_8, _stringLiteral5376, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisIList_t1_262_m8_2009_MethodInfo_var);
		V_2 = L_9;
		V_3 = (StringU5BU5D_t1_238*)NULL;
		Object_t * L_10 = V_2;
		if (!L_10)
		{
			goto IL_0089;
		}
	}
	{
		Object_t * L_11 = V_2;
		NullCheck(L_11);
		int32_t L_12 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.ICollection::get_Count() */, ICollection_t1_280_il2cpp_TypeInfo_var, L_11);
		V_4 = L_12;
		int32_t L_13 = V_4;
		V_3 = ((StringU5BU5D_t1_238*)SZArrayNew(StringU5BU5D_t1_238_il2cpp_TypeInfo_var, L_13));
		V_5 = 0;
		goto IL_0080;
	}

IL_0069:
	{
		StringU5BU5D_t1_238* L_14 = V_3;
		int32_t L_15 = V_5;
		Object_t * L_16 = V_2;
		int32_t L_17 = V_5;
		NullCheck(L_16);
		Object_t * L_18 = (Object_t *)InterfaceFuncInvoker1< Object_t *, int32_t >::Invoke(2 /* System.Object System.Collections.IList::get_Item(System.Int32) */, IList_t1_262_il2cpp_TypeInfo_var, L_16, L_17);
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		ArrayElementTypeCheck (L_14, ((String_t*)CastclassSealed(L_18, String_t_il2cpp_TypeInfo_var)));
		*((String_t**)(String_t**)SZArrayLdElema(L_14, L_15, sizeof(String_t*))) = (String_t*)((String_t*)CastclassSealed(L_18, String_t_il2cpp_TypeInfo_var));
		int32_t L_19 = V_5;
		V_5 = ((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_0080:
	{
		int32_t L_20 = V_5;
		int32_t L_21 = V_4;
		if ((((int32_t)L_20) < ((int32_t)L_21)))
		{
			goto IL_0069;
		}
	}

IL_0089:
	{
		StringU5BU5D_t1_238* L_22 = V_3;
		AddressBookContact_set_PhoneNumberList_m8_1149(__this, L_22, /*hidden argument*/NULL);
		Object_t * L_23 = ____contactInfoJsontDict;
		Object_t * L_24 = IDictionaryExtensions_GetIfAvailable_TisIList_t1_262_m8_2009(NULL /*static, unused*/, L_23, _stringLiteral5377, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisIList_t1_262_m8_2009_MethodInfo_var);
		V_6 = L_24;
		V_7 = (StringU5BU5D_t1_238*)NULL;
		Object_t * L_25 = V_6;
		if (!L_25)
		{
			goto IL_00e3;
		}
	}
	{
		Object_t * L_26 = V_6;
		NullCheck(L_26);
		int32_t L_27 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.ICollection::get_Count() */, ICollection_t1_280_il2cpp_TypeInfo_var, L_26);
		V_8 = L_27;
		int32_t L_28 = V_8;
		V_7 = ((StringU5BU5D_t1_238*)SZArrayNew(StringU5BU5D_t1_238_il2cpp_TypeInfo_var, L_28));
		V_9 = 0;
		goto IL_00da;
	}

IL_00c1:
	{
		StringU5BU5D_t1_238* L_29 = V_7;
		int32_t L_30 = V_9;
		Object_t * L_31 = V_6;
		int32_t L_32 = V_9;
		NullCheck(L_31);
		Object_t * L_33 = (Object_t *)InterfaceFuncInvoker1< Object_t *, int32_t >::Invoke(2 /* System.Object System.Collections.IList::get_Item(System.Int32) */, IList_t1_262_il2cpp_TypeInfo_var, L_31, L_32);
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, L_30);
		ArrayElementTypeCheck (L_29, ((String_t*)CastclassSealed(L_33, String_t_il2cpp_TypeInfo_var)));
		*((String_t**)(String_t**)SZArrayLdElema(L_29, L_30, sizeof(String_t*))) = (String_t*)((String_t*)CastclassSealed(L_33, String_t_il2cpp_TypeInfo_var));
		int32_t L_34 = V_9;
		V_9 = ((int32_t)((int32_t)L_34+(int32_t)1));
	}

IL_00da:
	{
		int32_t L_35 = V_9;
		int32_t L_36 = V_8;
		if ((((int32_t)L_35) < ((int32_t)L_36)))
		{
			goto IL_00c1;
		}
	}

IL_00e3:
	{
		StringU5BU5D_t1_238* L_37 = V_7;
		AddressBookContact_set_EmailIDList_m8_1151(__this, L_37, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.EditorAddressBookContact::.ctor(VoxelBusters.NativePlugins.AddressBookContact)
extern "C" void EditorAddressBookContact__ctor_m8_1156 (EditorAddressBookContact_t8_200 * __this, AddressBookContact_t8_198 * ____source, const MethodInfo* method)
{
	{
		AddressBookContact_t8_198 * L_0 = ____source;
		AddressBookContact__ctor_m8_1141(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.iOSAddressBookContact::.ctor(System.Collections.IDictionary)
extern TypeInfo* ICollection_t1_280_il2cpp_TypeInfo_var;
extern TypeInfo* StringU5BU5D_t1_238_il2cpp_TypeInfo_var;
extern TypeInfo* IList_t1_262_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var;
extern const MethodInfo* IDictionaryExtensions_GetIfAvailable_TisIList_t1_262_m8_2009_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral5378;
extern Il2CppCodeGenString* _stringLiteral5379;
extern Il2CppCodeGenString* _stringLiteral5375;
extern Il2CppCodeGenString* _stringLiteral5376;
extern Il2CppCodeGenString* _stringLiteral5380;
extern "C" void iOSAddressBookContact__ctor_m8_1157 (iOSAddressBookContact_t8_201 * __this, Object_t * ____contactInfoDict, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ICollection_t1_280_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(149);
		StringU5BU5D_t1_238_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(35);
		IList_t1_262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(76);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484117);
		IDictionaryExtensions_GetIfAvailable_TisIList_t1_262_m8_2009_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484223);
		_stringLiteral5378 = il2cpp_codegen_string_literal_from_index(5378);
		_stringLiteral5379 = il2cpp_codegen_string_literal_from_index(5379);
		_stringLiteral5375 = il2cpp_codegen_string_literal_from_index(5375);
		_stringLiteral5376 = il2cpp_codegen_string_literal_from_index(5376);
		_stringLiteral5380 = il2cpp_codegen_string_literal_from_index(5380);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	StringU5BU5D_t1_238* V_1 = {0};
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	Object_t * V_4 = {0};
	StringU5BU5D_t1_238* V_5 = {0};
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	{
		AddressBookContact__ctor_m8_1140(__this, /*hidden argument*/NULL);
		Object_t * L_0 = ____contactInfoDict;
		String_t* L_1 = IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(NULL /*static, unused*/, L_0, _stringLiteral5378, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var);
		AddressBookContact_set_FirstName_m8_1143(__this, L_1, /*hidden argument*/NULL);
		Object_t * L_2 = ____contactInfoDict;
		String_t* L_3 = IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(NULL /*static, unused*/, L_2, _stringLiteral5379, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var);
		AddressBookContact_set_LastName_m8_1145(__this, L_3, /*hidden argument*/NULL);
		Object_t * L_4 = ____contactInfoDict;
		String_t* L_5 = IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(NULL /*static, unused*/, L_4, _stringLiteral5375, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var);
		AddressBookContact_set_ImagePath_m8_1147(__this, L_5, /*hidden argument*/NULL);
		Object_t * L_6 = ____contactInfoDict;
		Object_t * L_7 = IDictionaryExtensions_GetIfAvailable_TisIList_t1_262_m8_2009(NULL /*static, unused*/, L_6, _stringLiteral5376, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisIList_t1_262_m8_2009_MethodInfo_var);
		V_0 = L_7;
		V_1 = (StringU5BU5D_t1_238*)NULL;
		Object_t * L_8 = V_0;
		if (!L_8)
		{
			goto IL_007c;
		}
	}
	{
		Object_t * L_9 = V_0;
		NullCheck(L_9);
		int32_t L_10 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.ICollection::get_Count() */, ICollection_t1_280_il2cpp_TypeInfo_var, L_9);
		V_2 = L_10;
		int32_t L_11 = V_2;
		V_1 = ((StringU5BU5D_t1_238*)SZArrayNew(StringU5BU5D_t1_238_il2cpp_TypeInfo_var, L_11));
		V_3 = 0;
		goto IL_0075;
	}

IL_0062:
	{
		StringU5BU5D_t1_238* L_12 = V_1;
		int32_t L_13 = V_3;
		Object_t * L_14 = V_0;
		int32_t L_15 = V_3;
		NullCheck(L_14);
		Object_t * L_16 = (Object_t *)InterfaceFuncInvoker1< Object_t *, int32_t >::Invoke(2 /* System.Object System.Collections.IList::get_Item(System.Int32) */, IList_t1_262_il2cpp_TypeInfo_var, L_14, L_15);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		ArrayElementTypeCheck (L_12, ((String_t*)CastclassSealed(L_16, String_t_il2cpp_TypeInfo_var)));
		*((String_t**)(String_t**)SZArrayLdElema(L_12, L_13, sizeof(String_t*))) = (String_t*)((String_t*)CastclassSealed(L_16, String_t_il2cpp_TypeInfo_var));
		int32_t L_17 = V_3;
		V_3 = ((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_0075:
	{
		int32_t L_18 = V_3;
		int32_t L_19 = V_2;
		if ((((int32_t)L_18) < ((int32_t)L_19)))
		{
			goto IL_0062;
		}
	}

IL_007c:
	{
		StringU5BU5D_t1_238* L_20 = V_1;
		AddressBookContact_set_PhoneNumberList_m8_1149(__this, L_20, /*hidden argument*/NULL);
		Object_t * L_21 = ____contactInfoDict;
		Object_t * L_22 = IDictionaryExtensions_GetIfAvailable_TisIList_t1_262_m8_2009(NULL /*static, unused*/, L_21, _stringLiteral5380, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisIList_t1_262_m8_2009_MethodInfo_var);
		V_4 = L_22;
		V_5 = (StringU5BU5D_t1_238*)NULL;
		Object_t * L_23 = V_4;
		if (!L_23)
		{
			goto IL_00d6;
		}
	}
	{
		Object_t * L_24 = V_4;
		NullCheck(L_24);
		int32_t L_25 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.ICollection::get_Count() */, ICollection_t1_280_il2cpp_TypeInfo_var, L_24);
		V_6 = L_25;
		int32_t L_26 = V_6;
		V_5 = ((StringU5BU5D_t1_238*)SZArrayNew(StringU5BU5D_t1_238_il2cpp_TypeInfo_var, L_26));
		V_7 = 0;
		goto IL_00cd;
	}

IL_00b4:
	{
		StringU5BU5D_t1_238* L_27 = V_5;
		int32_t L_28 = V_7;
		Object_t * L_29 = V_4;
		int32_t L_30 = V_7;
		NullCheck(L_29);
		Object_t * L_31 = (Object_t *)InterfaceFuncInvoker1< Object_t *, int32_t >::Invoke(2 /* System.Object System.Collections.IList::get_Item(System.Int32) */, IList_t1_262_il2cpp_TypeInfo_var, L_29, L_30);
		NullCheck(L_27);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_27, L_28);
		ArrayElementTypeCheck (L_27, ((String_t*)CastclassSealed(L_31, String_t_il2cpp_TypeInfo_var)));
		*((String_t**)(String_t**)SZArrayLdElema(L_27, L_28, sizeof(String_t*))) = (String_t*)((String_t*)CastclassSealed(L_31, String_t_il2cpp_TypeInfo_var));
		int32_t L_32 = V_7;
		V_7 = ((int32_t)((int32_t)L_32+(int32_t)1));
	}

IL_00cd:
	{
		int32_t L_33 = V_7;
		int32_t L_34 = V_6;
		if ((((int32_t)L_33) < ((int32_t)L_34)))
		{
			goto IL_00b4;
		}
	}

IL_00d6:
	{
		StringU5BU5D_t1_238* L_35 = V_5;
		AddressBookContact_set_EmailIDList_m8_1151(__this, L_35, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.AndroidBillingProduct::.ctor(System.Collections.IDictionary)
extern TypeInfo* IDictionary_t1_35_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* IDictionaryExtensions_GetIfAvailable_TisInt64_t1_7_m8_2010_MethodInfo_var;
extern const MethodInfo* IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral5381;
extern Il2CppCodeGenString* _stringLiteral5382;
extern Il2CppCodeGenString* _stringLiteral786;
extern Il2CppCodeGenString* _stringLiteral5383;
extern Il2CppCodeGenString* _stringLiteral5384;
extern Il2CppCodeGenString* _stringLiteral5385;
extern Il2CppCodeGenString* _stringLiteral5386;
extern "C" void AndroidBillingProduct__ctor_m8_1158 (AndroidBillingProduct_t8_205 * __this, Object_t * ____productJsonDict, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionary_t1_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		IDictionaryExtensions_GetIfAvailable_TisInt64_t1_7_m8_2010_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484224);
		IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484117);
		_stringLiteral5381 = il2cpp_codegen_string_literal_from_index(5381);
		_stringLiteral5382 = il2cpp_codegen_string_literal_from_index(5382);
		_stringLiteral786 = il2cpp_codegen_string_literal_from_index(786);
		_stringLiteral5383 = il2cpp_codegen_string_literal_from_index(5383);
		_stringLiteral5384 = il2cpp_codegen_string_literal_from_index(5384);
		_stringLiteral5385 = il2cpp_codegen_string_literal_from_index(5385);
		_stringLiteral5386 = il2cpp_codegen_string_literal_from_index(5386);
		s_Il2CppMethodIntialized = true;
	}
	{
		MutableBillingProduct__ctor_m8_1185(__this, /*hidden argument*/NULL);
		Object_t * L_0 = ____productJsonDict;
		NullCheck(L_0);
		Object_t * L_1 = (Object_t *)InterfaceFuncInvoker1< Object_t *, Object_t * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_0, _stringLiteral5381);
		BillingProduct_set_AndroidProductID_m8_1179(__this, ((String_t*)IsInstSealed(L_1, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		Object_t * L_2 = ____productJsonDict;
		NullCheck(L_2);
		Object_t * L_3 = (Object_t *)InterfaceFuncInvoker1< Object_t *, Object_t * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_2, _stringLiteral5382);
		BillingProduct_set_Name_m8_1163(__this, ((String_t*)IsInstSealed(L_3, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		Object_t * L_4 = ____productJsonDict;
		NullCheck(L_4);
		Object_t * L_5 = (Object_t *)InterfaceFuncInvoker1< Object_t *, Object_t * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_4, _stringLiteral786);
		BillingProduct_set_Description_m8_1165(__this, ((String_t*)IsInstSealed(L_5, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		Object_t * L_6 = ____productJsonDict;
		int64_t L_7 = IDictionaryExtensions_GetIfAvailable_TisInt64_t1_7_m8_2010(NULL /*static, unused*/, L_6, _stringLiteral5383, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisInt64_t1_7_m8_2010_MethodInfo_var);
		BillingProduct_set_Price_m8_1169(__this, ((float)((float)(((float)((float)L_7)))/(float)(1000000.0f))), /*hidden argument*/NULL);
		Object_t * L_8 = ____productJsonDict;
		String_t* L_9 = IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(NULL /*static, unused*/, L_8, _stringLiteral5384, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var);
		BillingProduct_set_LocalizedPrice_m8_1171(__this, L_9, /*hidden argument*/NULL);
		Object_t * L_10 = ____productJsonDict;
		String_t* L_11 = IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(NULL /*static, unused*/, L_10, _stringLiteral5385, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var);
		BillingProduct_set_CurrencyCode_m8_1173(__this, L_11, /*hidden argument*/NULL);
		Object_t * L_12 = ____productJsonDict;
		String_t* L_13 = IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(NULL /*static, unused*/, L_12, _stringLiteral5386, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var);
		BillingProduct_set_CurrencySymbol_m8_1175(__this, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IDictionary VoxelBusters.NativePlugins.Internal.AndroidBillingProduct::CreateJSONObject(VoxelBusters.NativePlugins.BillingProduct)
extern TypeInfo* Dictionary_2_t1_1839_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionary_t1_35_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_14949_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral5381;
extern Il2CppCodeGenString* _stringLiteral5382;
extern Il2CppCodeGenString* _stringLiteral786;
extern Il2CppCodeGenString* _stringLiteral5383;
extern Il2CppCodeGenString* _stringLiteral5384;
extern Il2CppCodeGenString* _stringLiteral5385;
extern Il2CppCodeGenString* _stringLiteral5386;
extern "C" Object_t * AndroidBillingProduct_CreateJSONObject_m8_1159 (Object_t * __this /* static, unused */, BillingProduct_t8_207 * ____product, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t1_1839_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1603);
		IDictionary_t1_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		Dictionary_2__ctor_m1_14949_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483789);
		_stringLiteral5381 = il2cpp_codegen_string_literal_from_index(5381);
		_stringLiteral5382 = il2cpp_codegen_string_literal_from_index(5382);
		_stringLiteral786 = il2cpp_codegen_string_literal_from_index(786);
		_stringLiteral5383 = il2cpp_codegen_string_literal_from_index(5383);
		_stringLiteral5384 = il2cpp_codegen_string_literal_from_index(5384);
		_stringLiteral5385 = il2cpp_codegen_string_literal_from_index(5385);
		_stringLiteral5386 = il2cpp_codegen_string_literal_from_index(5386);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	float V_1 = 0.0f;
	{
		Dictionary_2_t1_1839 * L_0 = (Dictionary_2_t1_1839 *)il2cpp_codegen_object_new (Dictionary_2_t1_1839_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_14949(L_0, /*hidden argument*/Dictionary_2__ctor_m1_14949_MethodInfo_var);
		V_0 = L_0;
		Object_t * L_1 = V_0;
		BillingProduct_t8_207 * L_2 = ____product;
		NullCheck(L_2);
		String_t* L_3 = BillingProduct_get_ProductIdentifier_m8_1180(L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		InterfaceActionInvoker2< Object_t *, Object_t * >::Invoke(3 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_1, _stringLiteral5381, L_3);
		Object_t * L_4 = V_0;
		BillingProduct_t8_207 * L_5 = ____product;
		NullCheck(L_5);
		String_t* L_6 = BillingProduct_get_Name_m8_1162(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		InterfaceActionInvoker2< Object_t *, Object_t * >::Invoke(3 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_4, _stringLiteral5382, L_6);
		Object_t * L_7 = V_0;
		BillingProduct_t8_207 * L_8 = ____product;
		NullCheck(L_8);
		String_t* L_9 = BillingProduct_get_Description_m8_1164(L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		InterfaceActionInvoker2< Object_t *, Object_t * >::Invoke(3 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_7, _stringLiteral786, L_9);
		Object_t * L_10 = V_0;
		BillingProduct_t8_207 * L_11 = ____product;
		NullCheck(L_11);
		float L_12 = BillingProduct_get_Price_m8_1168(L_11, /*hidden argument*/NULL);
		V_1 = ((float)((float)L_12*(float)(1000000.0f)));
		String_t* L_13 = Single_ToString_m1_633((&V_1), /*hidden argument*/NULL);
		NullCheck(L_10);
		InterfaceActionInvoker2< Object_t *, Object_t * >::Invoke(3 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_10, _stringLiteral5383, L_13);
		Object_t * L_14 = V_0;
		BillingProduct_t8_207 * L_15 = ____product;
		NullCheck(L_15);
		String_t* L_16 = BillingProduct_get_LocalizedPrice_m8_1170(L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		InterfaceActionInvoker2< Object_t *, Object_t * >::Invoke(3 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_14, _stringLiteral5384, L_16);
		Object_t * L_17 = V_0;
		BillingProduct_t8_207 * L_18 = ____product;
		NullCheck(L_18);
		String_t* L_19 = BillingProduct_get_CurrencyCode_m8_1172(L_18, /*hidden argument*/NULL);
		NullCheck(L_17);
		InterfaceActionInvoker2< Object_t *, Object_t * >::Invoke(3 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_17, _stringLiteral5385, L_19);
		Object_t * L_20 = V_0;
		BillingProduct_t8_207 * L_21 = ____product;
		NullCheck(L_21);
		String_t* L_22 = BillingProduct_get_CurrencySymbol_m8_1174(L_21, /*hidden argument*/NULL);
		NullCheck(L_20);
		InterfaceActionInvoker2< Object_t *, Object_t * >::Invoke(3 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_20, _stringLiteral5386, L_22);
		Object_t * L_23 = V_0;
		return L_23;
	}
}
// System.Void VoxelBusters.NativePlugins.BillingProduct::.ctor()
extern "C" void BillingProduct__ctor_m8_1160 (BillingProduct_t8_207 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.BillingProduct::.ctor(VoxelBusters.NativePlugins.BillingProduct)
extern "C" void BillingProduct__ctor_m8_1161 (BillingProduct_t8_207 * __this, BillingProduct_t8_207 * ____product, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		BillingProduct_t8_207 * L_0 = ____product;
		NullCheck(L_0);
		String_t* L_1 = BillingProduct_get_Name_m8_1162(L_0, /*hidden argument*/NULL);
		BillingProduct_set_Name_m8_1163(__this, L_1, /*hidden argument*/NULL);
		BillingProduct_t8_207 * L_2 = ____product;
		NullCheck(L_2);
		String_t* L_3 = BillingProduct_get_Description_m8_1164(L_2, /*hidden argument*/NULL);
		BillingProduct_set_Description_m8_1165(__this, L_3, /*hidden argument*/NULL);
		BillingProduct_t8_207 * L_4 = ____product;
		NullCheck(L_4);
		bool L_5 = BillingProduct_get_IsConsumable_m8_1166(L_4, /*hidden argument*/NULL);
		BillingProduct_set_IsConsumable_m8_1167(__this, L_5, /*hidden argument*/NULL);
		BillingProduct_t8_207 * L_6 = ____product;
		NullCheck(L_6);
		float L_7 = BillingProduct_get_Price_m8_1168(L_6, /*hidden argument*/NULL);
		BillingProduct_set_Price_m8_1169(__this, L_7, /*hidden argument*/NULL);
		BillingProduct_t8_207 * L_8 = ____product;
		NullCheck(L_8);
		String_t* L_9 = BillingProduct_get_LocalizedPrice_m8_1170(L_8, /*hidden argument*/NULL);
		BillingProduct_set_LocalizedPrice_m8_1171(__this, L_9, /*hidden argument*/NULL);
		BillingProduct_t8_207 * L_10 = ____product;
		NullCheck(L_10);
		String_t* L_11 = BillingProduct_get_CurrencyCode_m8_1172(L_10, /*hidden argument*/NULL);
		BillingProduct_set_CurrencyCode_m8_1173(__this, L_11, /*hidden argument*/NULL);
		BillingProduct_t8_207 * L_12 = ____product;
		NullCheck(L_12);
		String_t* L_13 = BillingProduct_get_CurrencySymbol_m8_1174(L_12, /*hidden argument*/NULL);
		BillingProduct_set_CurrencySymbol_m8_1175(__this, L_13, /*hidden argument*/NULL);
		BillingProduct_t8_207 * L_14 = ____product;
		NullCheck(L_14);
		String_t* L_15 = BillingProduct_get_IOSProductID_m8_1176(L_14, /*hidden argument*/NULL);
		BillingProduct_set_IOSProductID_m8_1177(__this, L_15, /*hidden argument*/NULL);
		BillingProduct_t8_207 * L_16 = ____product;
		NullCheck(L_16);
		String_t* L_17 = BillingProduct_get_AndroidProductID_m8_1178(L_16, /*hidden argument*/NULL);
		BillingProduct_set_AndroidProductID_m8_1179(__this, L_17, /*hidden argument*/NULL);
		return;
	}
}
// System.String VoxelBusters.NativePlugins.BillingProduct::get_Name()
extern "C" String_t* BillingProduct_get_Name_m8_1162 (BillingProduct_t8_207 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_name_0);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.BillingProduct::set_Name(System.String)
extern "C" void BillingProduct_set_Name_m8_1163 (BillingProduct_t8_207 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_name_0 = L_0;
		return;
	}
}
// System.String VoxelBusters.NativePlugins.BillingProduct::get_Description()
extern "C" String_t* BillingProduct_get_Description_m8_1164 (BillingProduct_t8_207 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_description_1);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.BillingProduct::set_Description(System.String)
extern "C" void BillingProduct_set_Description_m8_1165 (BillingProduct_t8_207 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_description_1 = L_0;
		return;
	}
}
// System.Boolean VoxelBusters.NativePlugins.BillingProduct::get_IsConsumable()
extern "C" bool BillingProduct_get_IsConsumable_m8_1166 (BillingProduct_t8_207 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_isConsumable_2);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.BillingProduct::set_IsConsumable(System.Boolean)
extern "C" void BillingProduct_set_IsConsumable_m8_1167 (BillingProduct_t8_207 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___m_isConsumable_2 = L_0;
		return;
	}
}
// System.Single VoxelBusters.NativePlugins.BillingProduct::get_Price()
extern "C" float BillingProduct_get_Price_m8_1168 (BillingProduct_t8_207 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_price_3);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.BillingProduct::set_Price(System.Single)
extern "C" void BillingProduct_set_Price_m8_1169 (BillingProduct_t8_207 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_price_3 = L_0;
		return;
	}
}
// System.String VoxelBusters.NativePlugins.BillingProduct::get_LocalizedPrice()
extern "C" String_t* BillingProduct_get_LocalizedPrice_m8_1170 (BillingProduct_t8_207 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CLocalizedPriceU3Ek__BackingField_6);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.BillingProduct::set_LocalizedPrice(System.String)
extern "C" void BillingProduct_set_LocalizedPrice_m8_1171 (BillingProduct_t8_207 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CLocalizedPriceU3Ek__BackingField_6 = L_0;
		return;
	}
}
// System.String VoxelBusters.NativePlugins.BillingProduct::get_CurrencyCode()
extern "C" String_t* BillingProduct_get_CurrencyCode_m8_1172 (BillingProduct_t8_207 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CCurrencyCodeU3Ek__BackingField_7);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.BillingProduct::set_CurrencyCode(System.String)
extern "C" void BillingProduct_set_CurrencyCode_m8_1173 (BillingProduct_t8_207 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CCurrencyCodeU3Ek__BackingField_7 = L_0;
		return;
	}
}
// System.String VoxelBusters.NativePlugins.BillingProduct::get_CurrencySymbol()
extern "C" String_t* BillingProduct_get_CurrencySymbol_m8_1174 (BillingProduct_t8_207 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CCurrencySymbolU3Ek__BackingField_8);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.BillingProduct::set_CurrencySymbol(System.String)
extern "C" void BillingProduct_set_CurrencySymbol_m8_1175 (BillingProduct_t8_207 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CCurrencySymbolU3Ek__BackingField_8 = L_0;
		return;
	}
}
// System.String VoxelBusters.NativePlugins.BillingProduct::get_IOSProductID()
extern "C" String_t* BillingProduct_get_IOSProductID_m8_1176 (BillingProduct_t8_207 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_iosProductId_4);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.BillingProduct::set_IOSProductID(System.String)
extern "C" void BillingProduct_set_IOSProductID_m8_1177 (BillingProduct_t8_207 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_iosProductId_4 = L_0;
		return;
	}
}
// System.String VoxelBusters.NativePlugins.BillingProduct::get_AndroidProductID()
extern "C" String_t* BillingProduct_get_AndroidProductID_m8_1178 (BillingProduct_t8_207 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_androidProductId_5);
		return L_0;
	}
}
// System.Void VoxelBusters.NativePlugins.BillingProduct::set_AndroidProductID(System.String)
extern "C" void BillingProduct_set_AndroidProductID_m8_1179 (BillingProduct_t8_207 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_androidProductId_5 = L_0;
		return;
	}
}
// System.String VoxelBusters.NativePlugins.BillingProduct::get_ProductIdentifier()
extern "C" String_t* BillingProduct_get_ProductIdentifier_m8_1180 (BillingProduct_t8_207 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_iosProductId_4);
		return L_0;
	}
}
// VoxelBusters.NativePlugins.BillingProduct VoxelBusters.NativePlugins.BillingProduct::Create(System.String,System.Boolean,VoxelBusters.NativePlugins.PlatformID[])
extern TypeInfo* BillingProduct_t8_207_il2cpp_TypeInfo_var;
extern "C" BillingProduct_t8_207 * BillingProduct_Create_m8_1181 (Object_t * __this /* static, unused */, String_t* ____name, bool ____isConsumable, PlatformIDU5BU5D_t8_329* ____platformIDs, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		BillingProduct_t8_207_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2117);
		s_Il2CppMethodIntialized = true;
	}
	BillingProduct_t8_207 * V_0 = {0};
	PlatformID_t8_331 * V_1 = {0};
	PlatformIDU5BU5D_t8_329* V_2 = {0};
	int32_t V_3 = 0;
	{
		BillingProduct_t8_207 * L_0 = (BillingProduct_t8_207 *)il2cpp_codegen_object_new (BillingProduct_t8_207_il2cpp_TypeInfo_var);
		BillingProduct__ctor_m8_1160(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		BillingProduct_t8_207 * L_1 = V_0;
		String_t* L_2 = ____name;
		NullCheck(L_1);
		BillingProduct_set_Name_m8_1163(L_1, L_2, /*hidden argument*/NULL);
		BillingProduct_t8_207 * L_3 = V_0;
		bool L_4 = ____isConsumable;
		NullCheck(L_3);
		BillingProduct_set_IsConsumable_m8_1167(L_3, L_4, /*hidden argument*/NULL);
		PlatformIDU5BU5D_t8_329* L_5 = ____platformIDs;
		if (!L_5)
		{
			goto IL_0073;
		}
	}
	{
		PlatformIDU5BU5D_t8_329* L_6 = ____platformIDs;
		V_2 = L_6;
		V_3 = 0;
		goto IL_006a;
	}

IL_0023:
	{
		PlatformIDU5BU5D_t8_329* L_7 = V_2;
		int32_t L_8 = V_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		V_1 = (*(PlatformID_t8_331 **)(PlatformID_t8_331 **)SZArrayLdElema(L_7, L_9, sizeof(PlatformID_t8_331 *)));
		PlatformID_t8_331 * L_10 = V_1;
		if (L_10)
		{
			goto IL_0032;
		}
	}
	{
		goto IL_0066;
	}

IL_0032:
	{
		PlatformID_t8_331 * L_11 = V_1;
		NullCheck(L_11);
		int32_t L_12 = PlatformID_get_Platform_m8_1920(L_11, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_004e;
		}
	}
	{
		BillingProduct_t8_207 * L_13 = V_0;
		PlatformID_t8_331 * L_14 = V_1;
		NullCheck(L_14);
		String_t* L_15 = PlatformID_get_Value_m8_1922(L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		BillingProduct_set_IOSProductID_m8_1177(L_13, L_15, /*hidden argument*/NULL);
		goto IL_0066;
	}

IL_004e:
	{
		PlatformID_t8_331 * L_16 = V_1;
		NullCheck(L_16);
		int32_t L_17 = PlatformID_get_Platform_m8_1920(L_16, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_17) == ((uint32_t)1))))
		{
			goto IL_0066;
		}
	}
	{
		BillingProduct_t8_207 * L_18 = V_0;
		PlatformID_t8_331 * L_19 = V_1;
		NullCheck(L_19);
		String_t* L_20 = PlatformID_get_Value_m8_1922(L_19, /*hidden argument*/NULL);
		NullCheck(L_18);
		BillingProduct_set_AndroidProductID_m8_1179(L_18, L_20, /*hidden argument*/NULL);
	}

IL_0066:
	{
		int32_t L_21 = V_3;
		V_3 = ((int32_t)((int32_t)L_21+(int32_t)1));
	}

IL_006a:
	{
		int32_t L_22 = V_3;
		PlatformIDU5BU5D_t8_329* L_23 = V_2;
		NullCheck(L_23);
		if ((((int32_t)L_22) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_23)->max_length)))))))
		{
			goto IL_0023;
		}
	}

IL_0073:
	{
		BillingProduct_t8_207 * L_24 = V_0;
		return L_24;
	}
}
// System.String VoxelBusters.NativePlugins.BillingProduct::ToString()
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t1_20_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5387;
extern "C" String_t* BillingProduct_ToString_m8_1182 (BillingProduct_t8_207 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Boolean_t1_20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(54);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5387 = il2cpp_codegen_string_literal_from_index(5387);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_272* L_0 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 4));
		String_t* L_1 = BillingProduct_get_Name_m8_1162(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_1;
		ObjectU5BU5D_t1_272* L_2 = L_0;
		String_t* L_3 = BillingProduct_get_ProductIdentifier_m8_1180(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1, sizeof(Object_t *))) = (Object_t *)L_3;
		ObjectU5BU5D_t1_272* L_4 = L_2;
		bool L_5 = BillingProduct_get_IsConsumable_m8_1166(__this, /*hidden argument*/NULL);
		bool L_6 = L_5;
		Object_t * L_7 = Box(Boolean_t1_20_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 2, sizeof(Object_t *))) = (Object_t *)L_7;
		ObjectU5BU5D_t1_272* L_8 = L_4;
		String_t* L_9 = BillingProduct_get_LocalizedPrice_m8_1170(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 3);
		ArrayElementTypeCheck (L_8, L_9);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 3, sizeof(Object_t *))) = (Object_t *)L_9;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Format_m1_551(NULL /*static, unused*/, _stringLiteral5387, L_8, /*hidden argument*/NULL);
		return L_10;
	}
}
// VoxelBusters.NativePlugins.BillingProduct VoxelBusters.NativePlugins.BillingProduct::Copy()
extern "C" BillingProduct_t8_207 * BillingProduct_Copy_m8_1183 (BillingProduct_t8_207 * __this, const MethodInfo* method)
{
	{
		return (BillingProduct_t8_207 *)NULL;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.EditorBillingProduct::.ctor(VoxelBusters.NativePlugins.BillingProduct)
extern "C" void EditorBillingProduct__ctor_m8_1184 (EditorBillingProduct_t8_208 * __this, BillingProduct_t8_207 * ____product, const MethodInfo* method)
{
	{
		BillingProduct_t8_207 * L_0 = ____product;
		MutableBillingProduct__ctor_m8_1186(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.MutableBillingProduct::.ctor()
extern "C" void MutableBillingProduct__ctor_m8_1185 (MutableBillingProduct_t8_206 * __this, const MethodInfo* method)
{
	{
		BillingProduct__ctor_m8_1160(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.MutableBillingProduct::.ctor(VoxelBusters.NativePlugins.BillingProduct)
extern "C" void MutableBillingProduct__ctor_m8_1186 (MutableBillingProduct_t8_206 * __this, BillingProduct_t8_207 * ____product, const MethodInfo* method)
{
	{
		BillingProduct_t8_207 * L_0 = ____product;
		BillingProduct__ctor_m8_1161(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.MutableBillingProduct::SetIsConsumable(System.Boolean)
extern "C" void MutableBillingProduct_SetIsConsumable_m8_1187 (MutableBillingProduct_t8_206 * __this, bool ____isConsumable, const MethodInfo* method)
{
	{
		bool L_0 = ____isConsumable;
		BillingProduct_set_IsConsumable_m8_1167(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.MutableBillingProduct::SetLocalizePrice(System.String)
extern "C" void MutableBillingProduct_SetLocalizePrice_m8_1188 (MutableBillingProduct_t8_206 * __this, String_t* ____locPrice, const MethodInfo* method)
{
	{
		String_t* L_0 = ____locPrice;
		BillingProduct_set_LocalizedPrice_m8_1171(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.MutableBillingProduct::SetCurrencyCode(System.String)
extern "C" void MutableBillingProduct_SetCurrencyCode_m8_1189 (MutableBillingProduct_t8_206 * __this, String_t* ____code, const MethodInfo* method)
{
	{
		String_t* L_0 = ____code;
		BillingProduct_set_CurrencyCode_m8_1173(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.NativePlugins.Internal.MutableBillingProduct::SetCurrencySymbol(System.String)
extern "C" void MutableBillingProduct_SetCurrencySymbol_m8_1190 (MutableBillingProduct_t8_206 * __this, String_t* ____symbol, const MethodInfo* method)
{
	{
		String_t* L_0 = ____symbol;
		BillingProduct_set_CurrencySymbol_m8_1175(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
