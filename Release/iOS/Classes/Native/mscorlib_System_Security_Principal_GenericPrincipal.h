﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Security.Principal.IIdentity
struct IIdentity_t1_1374;
// System.String[]
struct StringU5BU5D_t1_238;

#include "mscorlib_System_Object.h"

// System.Security.Principal.GenericPrincipal
struct  GenericPrincipal_t1_1373  : public Object_t
{
	// System.Security.Principal.IIdentity System.Security.Principal.GenericPrincipal::m_identity
	Object_t * ___m_identity_0;
	// System.String[] System.Security.Principal.GenericPrincipal::m_roles
	StringU5BU5D_t1_238* ___m_roles_1;
};
