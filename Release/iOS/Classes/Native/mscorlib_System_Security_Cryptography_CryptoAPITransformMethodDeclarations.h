﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.CryptoAPITransform
struct CryptoAPITransform_t1_1189;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"

// System.Void System.Security.Cryptography.CryptoAPITransform::.ctor()
extern "C" void CryptoAPITransform__ctor_m1_10173 (CryptoAPITransform_t1_1189 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.CryptoAPITransform::System.IDisposable.Dispose()
extern "C" void CryptoAPITransform_System_IDisposable_Dispose_m1_10174 (CryptoAPITransform_t1_1189 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.CryptoAPITransform::get_CanReuseTransform()
extern "C" bool CryptoAPITransform_get_CanReuseTransform_m1_10175 (CryptoAPITransform_t1_1189 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.CryptoAPITransform::get_CanTransformMultipleBlocks()
extern "C" bool CryptoAPITransform_get_CanTransformMultipleBlocks_m1_10176 (CryptoAPITransform_t1_1189 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Cryptography.CryptoAPITransform::get_InputBlockSize()
extern "C" int32_t CryptoAPITransform_get_InputBlockSize_m1_10177 (CryptoAPITransform_t1_1189 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Security.Cryptography.CryptoAPITransform::get_KeyHandle()
extern "C" IntPtr_t CryptoAPITransform_get_KeyHandle_m1_10178 (CryptoAPITransform_t1_1189 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Cryptography.CryptoAPITransform::get_OutputBlockSize()
extern "C" int32_t CryptoAPITransform_get_OutputBlockSize_m1_10179 (CryptoAPITransform_t1_1189 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.CryptoAPITransform::Clear()
extern "C" void CryptoAPITransform_Clear_m1_10180 (CryptoAPITransform_t1_1189 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.CryptoAPITransform::Dispose(System.Boolean)
extern "C" void CryptoAPITransform_Dispose_m1_10181 (CryptoAPITransform_t1_1189 * __this, bool ___disposing, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Cryptography.CryptoAPITransform::TransformBlock(System.Byte[],System.Int32,System.Int32,System.Byte[],System.Int32)
extern "C" int32_t CryptoAPITransform_TransformBlock_m1_10182 (CryptoAPITransform_t1_1189 * __this, ByteU5BU5D_t1_109* ___inputBuffer, int32_t ___inputOffset, int32_t ___inputCount, ByteU5BU5D_t1_109* ___outputBuffer, int32_t ___outputOffset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.CryptoAPITransform::TransformFinalBlock(System.Byte[],System.Int32,System.Int32)
extern "C" ByteU5BU5D_t1_109* CryptoAPITransform_TransformFinalBlock_m1_10183 (CryptoAPITransform_t1_1189 * __this, ByteU5BU5D_t1_109* ___inputBuffer, int32_t ___inputOffset, int32_t ___inputCount, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.CryptoAPITransform::Reset()
extern "C" void CryptoAPITransform_Reset_m1_10184 (CryptoAPITransform_t1_1189 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
