﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Reflection.MonoGenericClass
struct MonoGenericClass_t1_485;
// System.Reflection.Emit.ConstructorBuilder
struct ConstructorBuilder_t1_477;

#include "mscorlib_System_Reflection_ConstructorInfo.h"

// System.Reflection.Emit.ConstructorOnTypeBuilderInst
struct  ConstructorOnTypeBuilderInst_t1_484  : public ConstructorInfo_t1_478
{
	// System.Reflection.MonoGenericClass System.Reflection.Emit.ConstructorOnTypeBuilderInst::instantiation
	MonoGenericClass_t1_485 * ___instantiation_2;
	// System.Reflection.Emit.ConstructorBuilder System.Reflection.Emit.ConstructorOnTypeBuilderInst::cb
	ConstructorBuilder_t1_477 * ___cb_3;
};
