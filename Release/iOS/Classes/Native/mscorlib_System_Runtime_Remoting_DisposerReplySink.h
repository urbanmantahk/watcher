﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Runtime.Remoting.Messaging.IMessageSink
struct IMessageSink_t1_874;
// System.IDisposable
struct IDisposable_t1_1035;

#include "mscorlib_System_Object.h"

// System.Runtime.Remoting.DisposerReplySink
struct  DisposerReplySink_t1_1034  : public Object_t
{
	// System.Runtime.Remoting.Messaging.IMessageSink System.Runtime.Remoting.DisposerReplySink::_next
	Object_t * ____next_0;
	// System.IDisposable System.Runtime.Remoting.DisposerReplySink::_disposable
	Object_t * ____disposable_1;
};
