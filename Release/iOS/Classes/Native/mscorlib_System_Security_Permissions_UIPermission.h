﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Security_CodeAccessPermission.h"
#include "mscorlib_System_Security_Permissions_UIPermissionWindow.h"
#include "mscorlib_System_Security_Permissions_UIPermissionClipboard.h"

// System.Security.Permissions.UIPermission
struct  UIPermission_t1_1317  : public CodeAccessPermission_t1_1268
{
	// System.Security.Permissions.UIPermissionWindow System.Security.Permissions.UIPermission::_window
	int32_t ____window_1;
	// System.Security.Permissions.UIPermissionClipboard System.Security.Permissions.UIPermission::_clipboard
	int32_t ____clipboard_2;
};
