﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Arra.h"

// System.Runtime.Serialization.Formatters.Binary.ArrayStructure
struct  ArrayStructure_t1_1045 
{
	// System.Byte System.Runtime.Serialization.Formatters.Binary.ArrayStructure::value__
	uint8_t ___value___1;
};
