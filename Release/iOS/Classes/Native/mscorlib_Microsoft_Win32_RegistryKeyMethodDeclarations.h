﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Microsoft.Win32.RegistryKey
struct RegistryKey_t1_91;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Security.AccessControl.RegistrySecurity
struct RegistrySecurity_t1_1175;
// System.String[]
struct StringU5BU5D_t1_238;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.IO.IOException
struct IOException_t1_413;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_Microsoft_Win32_RegistryHive.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_Microsoft_Win32_RegistryValueKind.h"
#include "mscorlib_Microsoft_Win32_RegistryValueOptions.h"
#include "mscorlib_Microsoft_Win32_RegistryKeyPermissionCheck.h"
#include "mscorlib_System_Security_AccessControl_AccessControlSections.h"
#include "mscorlib_System_Security_AccessControl_RegistryRights.h"

// System.Void Microsoft.Win32.RegistryKey::.ctor(Microsoft.Win32.RegistryHive)
extern "C" void RegistryKey__ctor_m1_1432 (RegistryKey_t1_91 * __this, int32_t ___hiveId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Microsoft.Win32.RegistryKey::.ctor(Microsoft.Win32.RegistryHive,System.IntPtr,System.Boolean)
extern "C" void RegistryKey__ctor_m1_1433 (RegistryKey_t1_91 * __this, int32_t ___hiveId, IntPtr_t ___keyHandle, bool ___remoteRoot, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Microsoft.Win32.RegistryKey::.ctor(System.Object,System.String,System.Boolean)
extern "C" void RegistryKey__ctor_m1_1434 (RegistryKey_t1_91 * __this, Object_t * ___data, String_t* ___keyName, bool ___writable, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Microsoft.Win32.RegistryKey::.cctor()
extern "C" void RegistryKey__cctor_m1_1435 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Microsoft.Win32.RegistryKey::System.IDisposable.Dispose()
extern "C" void RegistryKey_System_IDisposable_Dispose_m1_1436 (RegistryKey_t1_91 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Microsoft.Win32.RegistryKey::Finalize()
extern "C" void RegistryKey_Finalize_m1_1437 (RegistryKey_t1_91 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Microsoft.Win32.RegistryKey::get_Name()
extern "C" String_t* RegistryKey_get_Name_m1_1438 (RegistryKey_t1_91 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Microsoft.Win32.RegistryKey::Flush()
extern "C" void RegistryKey_Flush_m1_1439 (RegistryKey_t1_91 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Microsoft.Win32.RegistryKey::Close()
extern "C" void RegistryKey_Close_m1_1440 (RegistryKey_t1_91 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Microsoft.Win32.RegistryKey::get_SubKeyCount()
extern "C" int32_t RegistryKey_get_SubKeyCount_m1_1441 (RegistryKey_t1_91 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Microsoft.Win32.RegistryKey::get_ValueCount()
extern "C" int32_t RegistryKey_get_ValueCount_m1_1442 (RegistryKey_t1_91 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Microsoft.Win32.RegistryKey::SetValue(System.String,System.Object)
extern "C" void RegistryKey_SetValue_m1_1443 (RegistryKey_t1_91 * __this, String_t* ___name, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Microsoft.Win32.RegistryKey::SetValue(System.String,System.Object,Microsoft.Win32.RegistryValueKind)
extern "C" void RegistryKey_SetValue_m1_1444 (RegistryKey_t1_91 * __this, String_t* ___name, Object_t * ___value, int32_t ___valueKind, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Microsoft.Win32.RegistryKey Microsoft.Win32.RegistryKey::OpenSubKey(System.String)
extern "C" RegistryKey_t1_91 * RegistryKey_OpenSubKey_m1_1445 (RegistryKey_t1_91 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Microsoft.Win32.RegistryKey Microsoft.Win32.RegistryKey::OpenSubKey(System.String,System.Boolean)
extern "C" RegistryKey_t1_91 * RegistryKey_OpenSubKey_m1_1446 (RegistryKey_t1_91 * __this, String_t* ___name, bool ___writable, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Microsoft.Win32.RegistryKey::GetValue(System.String)
extern "C" Object_t * RegistryKey_GetValue_m1_1447 (RegistryKey_t1_91 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Microsoft.Win32.RegistryKey::GetValue(System.String,System.Object)
extern "C" Object_t * RegistryKey_GetValue_m1_1448 (RegistryKey_t1_91 * __this, String_t* ___name, Object_t * ___defaultValue, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Microsoft.Win32.RegistryKey::GetValue(System.String,System.Object,Microsoft.Win32.RegistryValueOptions)
extern "C" Object_t * RegistryKey_GetValue_m1_1449 (RegistryKey_t1_91 * __this, String_t* ___name, Object_t * ___defaultValue, int32_t ___options, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Microsoft.Win32.RegistryValueKind Microsoft.Win32.RegistryKey::GetValueKind(System.String)
extern "C" int32_t RegistryKey_GetValueKind_m1_1450 (RegistryKey_t1_91 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Microsoft.Win32.RegistryKey Microsoft.Win32.RegistryKey::CreateSubKey(System.String)
extern "C" RegistryKey_t1_91 * RegistryKey_CreateSubKey_m1_1451 (RegistryKey_t1_91 * __this, String_t* ___subkey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Microsoft.Win32.RegistryKey Microsoft.Win32.RegistryKey::CreateSubKey(System.String,Microsoft.Win32.RegistryKeyPermissionCheck)
extern "C" RegistryKey_t1_91 * RegistryKey_CreateSubKey_m1_1452 (RegistryKey_t1_91 * __this, String_t* ___subkey, int32_t ___permissionCheck, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Microsoft.Win32.RegistryKey Microsoft.Win32.RegistryKey::CreateSubKey(System.String,Microsoft.Win32.RegistryKeyPermissionCheck,System.Security.AccessControl.RegistrySecurity)
extern "C" RegistryKey_t1_91 * RegistryKey_CreateSubKey_m1_1453 (RegistryKey_t1_91 * __this, String_t* ___subkey, int32_t ___permissionCheck, RegistrySecurity_t1_1175 * ___registrySecurity, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Microsoft.Win32.RegistryKey::DeleteSubKey(System.String)
extern "C" void RegistryKey_DeleteSubKey_m1_1454 (RegistryKey_t1_91 * __this, String_t* ___subkey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Microsoft.Win32.RegistryKey::DeleteSubKey(System.String,System.Boolean)
extern "C" void RegistryKey_DeleteSubKey_m1_1455 (RegistryKey_t1_91 * __this, String_t* ___subkey, bool ___throwOnMissingSubKey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Microsoft.Win32.RegistryKey::DeleteSubKeyTree(System.String)
extern "C" void RegistryKey_DeleteSubKeyTree_m1_1456 (RegistryKey_t1_91 * __this, String_t* ___subkey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Microsoft.Win32.RegistryKey::DeleteValue(System.String)
extern "C" void RegistryKey_DeleteValue_m1_1457 (RegistryKey_t1_91 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Microsoft.Win32.RegistryKey::DeleteValue(System.String,System.Boolean)
extern "C" void RegistryKey_DeleteValue_m1_1458 (RegistryKey_t1_91 * __this, String_t* ___name, bool ___throwOnMissingValue, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.AccessControl.RegistrySecurity Microsoft.Win32.RegistryKey::GetAccessControl()
extern "C" RegistrySecurity_t1_1175 * RegistryKey_GetAccessControl_m1_1459 (RegistryKey_t1_91 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.AccessControl.RegistrySecurity Microsoft.Win32.RegistryKey::GetAccessControl(System.Security.AccessControl.AccessControlSections)
extern "C" RegistrySecurity_t1_1175 * RegistryKey_GetAccessControl_m1_1460 (RegistryKey_t1_91 * __this, int32_t ___includeSections, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] Microsoft.Win32.RegistryKey::GetSubKeyNames()
extern "C" StringU5BU5D_t1_238* RegistryKey_GetSubKeyNames_m1_1461 (RegistryKey_t1_91 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] Microsoft.Win32.RegistryKey::GetValueNames()
extern "C" StringU5BU5D_t1_238* RegistryKey_GetValueNames_m1_1462 (RegistryKey_t1_91 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Microsoft.Win32.RegistryKey Microsoft.Win32.RegistryKey::OpenRemoteBaseKey(Microsoft.Win32.RegistryHive,System.String)
extern "C" RegistryKey_t1_91 * RegistryKey_OpenRemoteBaseKey_m1_1463 (Object_t * __this /* static, unused */, int32_t ___hKey, String_t* ___machineName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Microsoft.Win32.RegistryKey Microsoft.Win32.RegistryKey::OpenSubKey(System.String,Microsoft.Win32.RegistryKeyPermissionCheck)
extern "C" RegistryKey_t1_91 * RegistryKey_OpenSubKey_m1_1464 (RegistryKey_t1_91 * __this, String_t* ___name, int32_t ___permissionCheck, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Microsoft.Win32.RegistryKey Microsoft.Win32.RegistryKey::OpenSubKey(System.String,Microsoft.Win32.RegistryKeyPermissionCheck,System.Security.AccessControl.RegistryRights)
extern "C" RegistryKey_t1_91 * RegistryKey_OpenSubKey_m1_1465 (RegistryKey_t1_91 * __this, String_t* ___name, int32_t ___permissionCheck, int32_t ___rights, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Microsoft.Win32.RegistryKey::SetAccessControl(System.Security.AccessControl.RegistrySecurity)
extern "C" void RegistryKey_SetAccessControl_m1_1466 (RegistryKey_t1_91 * __this, RegistrySecurity_t1_1175 * ___registrySecurity, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Microsoft.Win32.RegistryKey::ToString()
extern "C" String_t* RegistryKey_ToString_m1_1467 (RegistryKey_t1_91 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Microsoft.Win32.RegistryKey::get_IsRoot()
extern "C" bool RegistryKey_get_IsRoot_m1_1468 (RegistryKey_t1_91 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Microsoft.Win32.RegistryKey::get_IsWritable()
extern "C" bool RegistryKey_get_IsWritable_m1_1469 (RegistryKey_t1_91 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Microsoft.Win32.RegistryHive Microsoft.Win32.RegistryKey::get_Hive()
extern "C" int32_t RegistryKey_get_Hive_m1_1470 (RegistryKey_t1_91 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Microsoft.Win32.RegistryKey::get_Handle()
extern "C" Object_t * RegistryKey_get_Handle_m1_1471 (RegistryKey_t1_91 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Microsoft.Win32.RegistryKey::AssertKeyStillValid()
extern "C" void RegistryKey_AssertKeyStillValid_m1_1472 (RegistryKey_t1_91 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Microsoft.Win32.RegistryKey::AssertKeyNameNotNull(System.String)
extern "C" void RegistryKey_AssertKeyNameNotNull_m1_1473 (RegistryKey_t1_91 * __this, String_t* ___subKeyName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Microsoft.Win32.RegistryKey::AssertKeyNameLength(System.String)
extern "C" void RegistryKey_AssertKeyNameLength_m1_1474 (RegistryKey_t1_91 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Microsoft.Win32.RegistryKey::DeleteChildKeysAndValues()
extern "C" void RegistryKey_DeleteChildKeysAndValues_m1_1475 (RegistryKey_t1_91 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Microsoft.Win32.RegistryKey::DecodeString(System.Byte[])
extern "C" String_t* RegistryKey_DecodeString_m1_1476 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.IOException Microsoft.Win32.RegistryKey::CreateMarkedForDeletionException()
extern "C" IOException_t1_413 * RegistryKey_CreateMarkedForDeletionException_m1_1477 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Microsoft.Win32.RegistryKey::GetHiveName(Microsoft.Win32.RegistryHive)
extern "C" String_t* RegistryKey_GetHiveName_m1_1478 (Object_t * __this /* static, unused */, int32_t ___hive, const MethodInfo* method) IL2CPP_METHOD_ATTR;
