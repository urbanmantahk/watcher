﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Serialization.Formatter
struct Formatter_t1_1074;
// System.Object
struct Object_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Serialization.Formatter::.ctor()
extern "C" void Formatter__ctor_m1_9502 (Formatter_t1_1074 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Serialization.Formatter::GetNext(System.Int64&)
extern "C" Object_t * Formatter_GetNext_m1_9503 (Formatter_t1_1074 * __this, int64_t* ___objID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Runtime.Serialization.Formatter::Schedule(System.Object)
extern "C" int64_t Formatter_Schedule_m1_9504 (Formatter_t1_1074 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.Formatter::WriteMember(System.String,System.Object)
extern "C" void Formatter_WriteMember_m1_9505 (Formatter_t1_1074 * __this, String_t* ___memberName, Object_t * ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
