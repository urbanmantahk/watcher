﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.Achievement/ReportProgressCompletion
struct ReportProgressCompletion_t8_220;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_String.h"

// System.Void VoxelBusters.NativePlugins.Achievement/ReportProgressCompletion::.ctor(System.Object,System.IntPtr)
extern "C" void ReportProgressCompletion__ctor_m8_1246 (ReportProgressCompletion_t8_220 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Achievement/ReportProgressCompletion::Invoke(System.Boolean,System.String)
extern "C" void ReportProgressCompletion_Invoke_m8_1247 (ReportProgressCompletion_t8_220 * __this, bool ____success, String_t* ____error, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_ReportProgressCompletion_t8_220(Il2CppObject* delegate, bool ____success, String_t* ____error);
// System.IAsyncResult VoxelBusters.NativePlugins.Achievement/ReportProgressCompletion::BeginInvoke(System.Boolean,System.String,System.AsyncCallback,System.Object)
extern "C" Object_t * ReportProgressCompletion_BeginInvoke_m8_1248 (ReportProgressCompletion_t8_220 * __this, bool ____success, String_t* ____error, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Achievement/ReportProgressCompletion::EndInvoke(System.IAsyncResult)
extern "C" void ReportProgressCompletion_EndInvoke_m8_1249 (ReportProgressCompletion_t8_220 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
