﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExifLibrary.ExifEncodedString
struct ExifEncodedString_t8_98;
// System.String
struct String_t;
// System.Text.Encoding
struct Encoding_t1_406;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTag.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifInterOperability.h"

// System.Void ExifLibrary.ExifEncodedString::.ctor(ExifLibrary.ExifTag,System.String,System.Text.Encoding)
extern "C" void ExifEncodedString__ctor_m8_411 (ExifEncodedString_t8_98 * __this, int32_t ___tag, String_t* ___value, Encoding_t1_406 * ___encoding, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ExifLibrary.ExifEncodedString::get__Value()
extern "C" Object_t * ExifEncodedString_get__Value_m8_412 (ExifEncodedString_t8_98 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.ExifEncodedString::set__Value(System.Object)
extern "C" void ExifEncodedString_set__Value_m8_413 (ExifEncodedString_t8_98 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExifLibrary.ExifEncodedString::get_Value()
extern "C" String_t* ExifEncodedString_get_Value_m8_414 (ExifEncodedString_t8_98 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.ExifEncodedString::set_Value(System.String)
extern "C" void ExifEncodedString_set_Value_m8_415 (ExifEncodedString_t8_98 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.Encoding ExifLibrary.ExifEncodedString::get_Encoding()
extern "C" Encoding_t1_406 * ExifEncodedString_get_Encoding_m8_416 (ExifEncodedString_t8_98 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.ExifEncodedString::set_Encoding(System.Text.Encoding)
extern "C" void ExifEncodedString_set_Encoding_m8_417 (ExifEncodedString_t8_98 * __this, Encoding_t1_406 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExifLibrary.ExifEncodedString::ToString()
extern "C" String_t* ExifEncodedString_ToString_m8_418 (ExifEncodedString_t8_98 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.ExifInterOperability ExifLibrary.ExifEncodedString::get_Interoperability()
extern "C" ExifInterOperability_t8_113  ExifEncodedString_get_Interoperability_m8_419 (ExifEncodedString_t8_98 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExifLibrary.ExifEncodedString::op_Implicit(ExifLibrary.ExifEncodedString)
extern "C" String_t* ExifEncodedString_op_Implicit_m8_420 (Object_t * __this /* static, unused */, ExifEncodedString_t8_98 * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
