﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>
struct Dictionary_2_t1_2422;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__10.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_12.h"
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m1_21618_gshared (Enumerator_t1_2429 * __this, Dictionary_2_t1_2422 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m1_21618(__this, ___dictionary, method) (( void (*) (Enumerator_t1_2429 *, Dictionary_2_t1_2422 *, const MethodInfo*))Enumerator__ctor_m1_21618_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m1_21619_gshared (Enumerator_t1_2429 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_21619(__this, method) (( Object_t * (*) (Enumerator_t1_2429 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_21619_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m1_21620_gshared (Enumerator_t1_2429 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1_21620(__this, method) (( void (*) (Enumerator_t1_2429 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_21620_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t1_284  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_21621_gshared (Enumerator_t1_2429 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_21621(__this, method) (( DictionaryEntry_t1_284  (*) (Enumerator_t1_2429 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_21621_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_21622_gshared (Enumerator_t1_2429 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_21622(__this, method) (( Object_t * (*) (Enumerator_t1_2429 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_21622_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_21623_gshared (Enumerator_t1_2429 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_21623(__this, method) (( Object_t * (*) (Enumerator_t1_2429 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_21623_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::MoveNext()
extern "C" bool Enumerator_MoveNext_m1_21624_gshared (Enumerator_t1_2429 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1_21624(__this, method) (( bool (*) (Enumerator_t1_2429 *, const MethodInfo*))Enumerator_MoveNext_m1_21624_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Current()
extern "C" KeyValuePair_2_t1_2424  Enumerator_get_Current_m1_21625_gshared (Enumerator_t1_2429 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1_21625(__this, method) (( KeyValuePair_2_t1_2424  (*) (Enumerator_t1_2429 *, const MethodInfo*))Enumerator_get_Current_m1_21625_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m1_21626_gshared (Enumerator_t1_2429 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m1_21626(__this, method) (( Object_t * (*) (Enumerator_t1_2429 *, const MethodInfo*))Enumerator_get_CurrentKey_m1_21626_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_CurrentValue()
extern "C" int32_t Enumerator_get_CurrentValue_m1_21627_gshared (Enumerator_t1_2429 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m1_21627(__this, method) (( int32_t (*) (Enumerator_t1_2429 *, const MethodInfo*))Enumerator_get_CurrentValue_m1_21627_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::Reset()
extern "C" void Enumerator_Reset_m1_21628_gshared (Enumerator_t1_2429 * __this, const MethodInfo* method);
#define Enumerator_Reset_m1_21628(__this, method) (( void (*) (Enumerator_t1_2429 *, const MethodInfo*))Enumerator_Reset_m1_21628_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::VerifyState()
extern "C" void Enumerator_VerifyState_m1_21629_gshared (Enumerator_t1_2429 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m1_21629(__this, method) (( void (*) (Enumerator_t1_2429 *, const MethodInfo*))Enumerator_VerifyState_m1_21629_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m1_21630_gshared (Enumerator_t1_2429 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m1_21630(__this, method) (( void (*) (Enumerator_t1_2429 *, const MethodInfo*))Enumerator_VerifyCurrent_m1_21630_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::Dispose()
extern "C" void Enumerator_Dispose_m1_21631_gshared (Enumerator_t1_2429 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1_21631(__this, method) (( void (*) (Enumerator_t1_2429 *, const MethodInfo*))Enumerator_Dispose_m1_21631_gshared)(__this, method)
