﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.IO.DirectoryInfo
struct DirectoryInfo_t1_399;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1_238;
// System.Collections.ArrayList
struct ArrayList_t1_170;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime.h"
#include "mscorlib_System_IO_SearchOption.h"
#include "mscorlib_System_IO_FileAttributes.h"

// System.IO.DirectoryInfo System.IO.Directory::CreateDirectory(System.String)
extern "C" DirectoryInfo_t1_399 * Directory_CreateDirectory_m1_4754 (Object_t * __this /* static, unused */, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.DirectoryInfo System.IO.Directory::CreateDirectoriesInternal(System.String)
extern "C" DirectoryInfo_t1_399 * Directory_CreateDirectoriesInternal_m1_4755 (Object_t * __this /* static, unused */, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.Directory::Delete(System.String)
extern "C" void Directory_Delete_m1_4756 (Object_t * __this /* static, unused */, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.Directory::RecursiveDelete(System.String)
extern "C" void Directory_RecursiveDelete_m1_4757 (Object_t * __this /* static, unused */, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.Directory::Delete(System.String,System.Boolean)
extern "C" void Directory_Delete_m1_4758 (Object_t * __this /* static, unused */, String_t* ___path, bool ___recursive, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IO.Directory::Exists(System.String)
extern "C" bool Directory_Exists_m1_4759 (Object_t * __this /* static, unused */, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.IO.Directory::GetLastAccessTime(System.String)
extern "C" DateTime_t1_150  Directory_GetLastAccessTime_m1_4760 (Object_t * __this /* static, unused */, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.IO.Directory::GetLastAccessTimeUtc(System.String)
extern "C" DateTime_t1_150  Directory_GetLastAccessTimeUtc_m1_4761 (Object_t * __this /* static, unused */, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.IO.Directory::GetLastWriteTime(System.String)
extern "C" DateTime_t1_150  Directory_GetLastWriteTime_m1_4762 (Object_t * __this /* static, unused */, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.IO.Directory::GetLastWriteTimeUtc(System.String)
extern "C" DateTime_t1_150  Directory_GetLastWriteTimeUtc_m1_4763 (Object_t * __this /* static, unused */, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.IO.Directory::GetCreationTime(System.String)
extern "C" DateTime_t1_150  Directory_GetCreationTime_m1_4764 (Object_t * __this /* static, unused */, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.IO.Directory::GetCreationTimeUtc(System.String)
extern "C" DateTime_t1_150  Directory_GetCreationTimeUtc_m1_4765 (Object_t * __this /* static, unused */, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.IO.Directory::GetCurrentDirectory()
extern "C" String_t* Directory_GetCurrentDirectory_m1_4766 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.IO.Directory::GetDirectories(System.String)
extern "C" StringU5BU5D_t1_238* Directory_GetDirectories_m1_4767 (Object_t * __this /* static, unused */, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.IO.Directory::GetDirectories(System.String,System.String)
extern "C" StringU5BU5D_t1_238* Directory_GetDirectories_m1_4768 (Object_t * __this /* static, unused */, String_t* ___path, String_t* ___searchPattern, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.IO.Directory::GetDirectories(System.String,System.String,System.IO.SearchOption)
extern "C" StringU5BU5D_t1_238* Directory_GetDirectories_m1_4769 (Object_t * __this /* static, unused */, String_t* ___path, String_t* ___searchPattern, int32_t ___searchOption, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.Directory::GetDirectoriesRecurse(System.String,System.String,System.Collections.ArrayList)
extern "C" void Directory_GetDirectoriesRecurse_m1_4770 (Object_t * __this /* static, unused */, String_t* ___path, String_t* ___searchPattern, ArrayList_t1_170 * ___all, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.IO.Directory::GetDirectoryRoot(System.String)
extern "C" String_t* Directory_GetDirectoryRoot_m1_4771 (Object_t * __this /* static, unused */, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.IO.Directory::GetFiles(System.String)
extern "C" StringU5BU5D_t1_238* Directory_GetFiles_m1_4772 (Object_t * __this /* static, unused */, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.IO.Directory::GetFiles(System.String,System.String)
extern "C" StringU5BU5D_t1_238* Directory_GetFiles_m1_4773 (Object_t * __this /* static, unused */, String_t* ___path, String_t* ___searchPattern, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.IO.Directory::GetFiles(System.String,System.String,System.IO.SearchOption)
extern "C" StringU5BU5D_t1_238* Directory_GetFiles_m1_4774 (Object_t * __this /* static, unused */, String_t* ___path, String_t* ___searchPattern, int32_t ___searchOption, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.Directory::GetFilesRecurse(System.String,System.String,System.Collections.ArrayList)
extern "C" void Directory_GetFilesRecurse_m1_4775 (Object_t * __this /* static, unused */, String_t* ___path, String_t* ___searchPattern, ArrayList_t1_170 * ___all, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.IO.Directory::GetFileSystemEntries(System.String)
extern "C" StringU5BU5D_t1_238* Directory_GetFileSystemEntries_m1_4776 (Object_t * __this /* static, unused */, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.IO.Directory::GetFileSystemEntries(System.String,System.String)
extern "C" StringU5BU5D_t1_238* Directory_GetFileSystemEntries_m1_4777 (Object_t * __this /* static, unused */, String_t* ___path, String_t* ___searchPattern, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.IO.Directory::GetLogicalDrives()
extern "C" StringU5BU5D_t1_238* Directory_GetLogicalDrives_m1_4778 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IO.Directory::IsRootDirectory(System.String)
extern "C" bool Directory_IsRootDirectory_m1_4779 (Object_t * __this /* static, unused */, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.DirectoryInfo System.IO.Directory::GetParent(System.String)
extern "C" DirectoryInfo_t1_399 * Directory_GetParent_m1_4780 (Object_t * __this /* static, unused */, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.Directory::Move(System.String,System.String)
extern "C" void Directory_Move_m1_4781 (Object_t * __this /* static, unused */, String_t* ___sourceDirName, String_t* ___destDirName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.Directory::SetCreationTime(System.String,System.DateTime)
extern "C" void Directory_SetCreationTime_m1_4782 (Object_t * __this /* static, unused */, String_t* ___path, DateTime_t1_150  ___creationTime, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.Directory::SetCreationTimeUtc(System.String,System.DateTime)
extern "C" void Directory_SetCreationTimeUtc_m1_4783 (Object_t * __this /* static, unused */, String_t* ___path, DateTime_t1_150  ___creationTimeUtc, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.Directory::SetCurrentDirectory(System.String)
extern "C" void Directory_SetCurrentDirectory_m1_4784 (Object_t * __this /* static, unused */, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.Directory::SetLastAccessTime(System.String,System.DateTime)
extern "C" void Directory_SetLastAccessTime_m1_4785 (Object_t * __this /* static, unused */, String_t* ___path, DateTime_t1_150  ___lastAccessTime, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.Directory::SetLastAccessTimeUtc(System.String,System.DateTime)
extern "C" void Directory_SetLastAccessTimeUtc_m1_4786 (Object_t * __this /* static, unused */, String_t* ___path, DateTime_t1_150  ___lastAccessTimeUtc, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.Directory::SetLastWriteTime(System.String,System.DateTime)
extern "C" void Directory_SetLastWriteTime_m1_4787 (Object_t * __this /* static, unused */, String_t* ___path, DateTime_t1_150  ___lastWriteTime, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.Directory::SetLastWriteTimeUtc(System.String,System.DateTime)
extern "C" void Directory_SetLastWriteTimeUtc_m1_4788 (Object_t * __this /* static, unused */, String_t* ___path, DateTime_t1_150  ___lastWriteTimeUtc, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.Directory::CheckPathExceptions(System.String)
extern "C" void Directory_CheckPathExceptions_m1_4789 (Object_t * __this /* static, unused */, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.IO.Directory::GetFileSystemEntries(System.String,System.String,System.IO.FileAttributes,System.IO.FileAttributes)
extern "C" StringU5BU5D_t1_238* Directory_GetFileSystemEntries_m1_4790 (Object_t * __this /* static, unused */, String_t* ___path, String_t* ___searchPattern, int32_t ___mask, int32_t ___attrs, const MethodInfo* method) IL2CPP_METHOD_ATTR;
