﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.BitConverter::.cctor()
extern "C" void BitConverter__cctor_m1_13303 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.BitConverter::AmILittleEndian()
extern "C" bool BitConverter_AmILittleEndian_m1_13304 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.BitConverter::DoubleWordsAreSwapped()
extern "C" bool BitConverter_DoubleWordsAreSwapped_m1_13305 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.BitConverter::DoubleToInt64Bits(System.Double)
extern "C" int64_t BitConverter_DoubleToInt64Bits_m1_13306 (Object_t * __this /* static, unused */, double ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.BitConverter::Int64BitsToDouble(System.Int64)
extern "C" double BitConverter_Int64BitsToDouble_m1_13307 (Object_t * __this /* static, unused */, int64_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.BitConverter::InternalInt64BitsToDouble(System.Int64)
extern "C" double BitConverter_InternalInt64BitsToDouble_m1_13308 (Object_t * __this /* static, unused */, int64_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.BitConverter::GetBytes(System.Byte*,System.Int32)
extern "C" ByteU5BU5D_t1_109* BitConverter_GetBytes_m1_13309 (Object_t * __this /* static, unused */, uint8_t* ___ptr, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.BitConverter::GetBytes(System.Boolean)
extern "C" ByteU5BU5D_t1_109* BitConverter_GetBytes_m1_13310 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.BitConverter::GetBytes(System.Char)
extern "C" ByteU5BU5D_t1_109* BitConverter_GetBytes_m1_13311 (Object_t * __this /* static, unused */, uint16_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.BitConverter::GetBytes(System.Int16)
extern "C" ByteU5BU5D_t1_109* BitConverter_GetBytes_m1_13312 (Object_t * __this /* static, unused */, int16_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.BitConverter::GetBytes(System.Int32)
extern "C" ByteU5BU5D_t1_109* BitConverter_GetBytes_m1_13313 (Object_t * __this /* static, unused */, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.BitConverter::GetBytes(System.Int64)
extern "C" ByteU5BU5D_t1_109* BitConverter_GetBytes_m1_13314 (Object_t * __this /* static, unused */, int64_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.BitConverter::GetBytes(System.UInt16)
extern "C" ByteU5BU5D_t1_109* BitConverter_GetBytes_m1_13315 (Object_t * __this /* static, unused */, uint16_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.BitConverter::GetBytes(System.UInt32)
extern "C" ByteU5BU5D_t1_109* BitConverter_GetBytes_m1_13316 (Object_t * __this /* static, unused */, uint32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.BitConverter::GetBytes(System.UInt64)
extern "C" ByteU5BU5D_t1_109* BitConverter_GetBytes_m1_13317 (Object_t * __this /* static, unused */, uint64_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.BitConverter::GetBytes(System.Single)
extern "C" ByteU5BU5D_t1_109* BitConverter_GetBytes_m1_13318 (Object_t * __this /* static, unused */, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.BitConverter::GetBytes(System.Double)
extern "C" ByteU5BU5D_t1_109* BitConverter_GetBytes_m1_13319 (Object_t * __this /* static, unused */, double ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.BitConverter::PutBytes(System.Byte*,System.Byte[],System.Int32,System.Int32)
extern "C" void BitConverter_PutBytes_m1_13320 (Object_t * __this /* static, unused */, uint8_t* ___dst, ByteU5BU5D_t1_109* ___src, int32_t ___start_index, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.BitConverter::ToBoolean(System.Byte[],System.Int32)
extern "C" bool BitConverter_ToBoolean_m1_13321 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char System.BitConverter::ToChar(System.Byte[],System.Int32)
extern "C" uint16_t BitConverter_ToChar_m1_13322 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 System.BitConverter::ToInt16(System.Byte[],System.Int32)
extern "C" int16_t BitConverter_ToInt16_m1_13323 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.BitConverter::ToInt32(System.Byte[],System.Int32)
extern "C" int32_t BitConverter_ToInt32_m1_13324 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.BitConverter::ToInt64(System.Byte[],System.Int32)
extern "C" int64_t BitConverter_ToInt64_m1_13325 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 System.BitConverter::ToUInt16(System.Byte[],System.Int32)
extern "C" uint16_t BitConverter_ToUInt16_m1_13326 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 System.BitConverter::ToUInt32(System.Byte[],System.Int32)
extern "C" uint32_t BitConverter_ToUInt32_m1_13327 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 System.BitConverter::ToUInt64(System.Byte[],System.Int32)
extern "C" uint64_t BitConverter_ToUInt64_m1_13328 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single System.BitConverter::ToSingle(System.Byte[],System.Int32)
extern "C" float BitConverter_ToSingle_m1_13329 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.BitConverter::ToDouble(System.Byte[],System.Int32)
extern "C" double BitConverter_ToDouble_m1_13330 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.BitConverter::SwappableToDouble(System.Byte[],System.Int32)
extern "C" double BitConverter_SwappableToDouble_m1_13331 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.BitConverter::ToString(System.Byte[])
extern "C" String_t* BitConverter_ToString_m1_13332 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.BitConverter::ToString(System.Byte[],System.Int32)
extern "C" String_t* BitConverter_ToString_m1_13333 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.BitConverter::ToString(System.Byte[],System.Int32,System.Int32)
extern "C" String_t* BitConverter_ToString_m1_13334 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, int32_t ___length, const MethodInfo* method) IL2CPP_METHOD_ATTR;
