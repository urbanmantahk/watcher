﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Object>
struct List_1_t1_1894;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t1_1897;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1_2210;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t1_2012;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>
struct ReadOnlyCollection_1_t1_1763;
// System.Collections.Generic.IComparer`1<System.Object>
struct IComparer_1_t1_2211;
// System.Predicate`1<System.Object>
struct Predicate_1_t1_1944;
// System.Action`1<System.Object>
struct Action_1_t1_1945;
// System.Comparison`1<System.Object>
struct Comparison_1_t1_1948;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_8.h"

// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C" void List_1__ctor_m1_15034_gshared (List_1_t1_1894 * __this, const MethodInfo* method);
#define List_1__ctor_m1_15034(__this, method) (( void (*) (List_1_t1_1894 *, const MethodInfo*))List_1__ctor_m1_15034_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m1_15095_gshared (List_1_t1_1894 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m1_15095(__this, ___collection, method) (( void (*) (List_1_t1_1894 *, Object_t*, const MethodInfo*))List_1__ctor_m1_15095_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor(System.Int32)
extern "C" void List_1__ctor_m1_15097_gshared (List_1_t1_1894 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m1_15097(__this, ___capacity, method) (( void (*) (List_1_t1_1894 *, int32_t, const MethodInfo*))List_1__ctor_m1_15097_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor(T[],System.Int32)
extern "C" void List_1__ctor_m1_15099_gshared (List_1_t1_1894 * __this, ObjectU5BU5D_t1_272* ___data, int32_t ___size, const MethodInfo* method);
#define List_1__ctor_m1_15099(__this, ___data, ___size, method) (( void (*) (List_1_t1_1894 *, ObjectU5BU5D_t1_272*, int32_t, const MethodInfo*))List_1__ctor_m1_15099_gshared)(__this, ___data, ___size, method)
// System.Void System.Collections.Generic.List`1<System.Object>::.cctor()
extern "C" void List_1__cctor_m1_15101_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m1_15101(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m1_15101_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_15103_gshared (List_1_t1_1894 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_15103(__this, method) (( Object_t* (*) (List_1_t1_1894 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_15103_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m1_15105_gshared (List_1_t1_1894 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m1_15105(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_1894 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m1_15105_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m1_15107_gshared (List_1_t1_1894 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1_15107(__this, method) (( Object_t * (*) (List_1_t1_1894 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1_15107_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m1_15109_gshared (List_1_t1_1894 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m1_15109(__this, ___item, method) (( int32_t (*) (List_1_t1_1894 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m1_15109_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m1_15111_gshared (List_1_t1_1894 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m1_15111(__this, ___item, method) (( bool (*) (List_1_t1_1894 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1_15111_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m1_15113_gshared (List_1_t1_1894 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m1_15113(__this, ___item, method) (( int32_t (*) (List_1_t1_1894 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m1_15113_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m1_15115_gshared (List_1_t1_1894 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m1_15115(__this, ___index, ___item, method) (( void (*) (List_1_t1_1894 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m1_15115_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m1_15117_gshared (List_1_t1_1894 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m1_15117(__this, ___item, method) (( void (*) (List_1_t1_1894 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1_15117_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_15119_gshared (List_1_t1_1894 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_15119(__this, method) (( bool (*) (List_1_t1_1894 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_15119_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m1_15121_gshared (List_1_t1_1894 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m1_15121(__this, method) (( bool (*) (List_1_t1_1894 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m1_15121_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m1_15123_gshared (List_1_t1_1894 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m1_15123(__this, method) (( Object_t * (*) (List_1_t1_1894 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1_15123_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m1_15125_gshared (List_1_t1_1894 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m1_15125(__this, method) (( bool (*) (List_1_t1_1894 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m1_15125_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m1_15127_gshared (List_1_t1_1894 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m1_15127(__this, method) (( bool (*) (List_1_t1_1894 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m1_15127_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Object>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m1_15129_gshared (List_1_t1_1894 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m1_15129(__this, ___index, method) (( Object_t * (*) (List_1_t1_1894 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m1_15129_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m1_15131_gshared (List_1_t1_1894 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m1_15131(__this, ___index, ___value, method) (( void (*) (List_1_t1_1894 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m1_15131_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Add(T)
extern "C" void List_1_Add_m1_15133_gshared (List_1_t1_1894 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_Add_m1_15133(__this, ___item, method) (( void (*) (List_1_t1_1894 *, Object_t *, const MethodInfo*))List_1_Add_m1_15133_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m1_15135_gshared (List_1_t1_1894 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m1_15135(__this, ___newCount, method) (( void (*) (List_1_t1_1894 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m1_15135_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<System.Object>::CheckRange(System.Int32,System.Int32)
extern "C" void List_1_CheckRange_m1_15137_gshared (List_1_t1_1894 * __this, int32_t ___idx, int32_t ___count, const MethodInfo* method);
#define List_1_CheckRange_m1_15137(__this, ___idx, ___count, method) (( void (*) (List_1_t1_1894 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m1_15137_gshared)(__this, ___idx, ___count, method)
// System.Void System.Collections.Generic.List`1<System.Object>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m1_15139_gshared (List_1_t1_1894 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m1_15139(__this, ___collection, method) (( void (*) (List_1_t1_1894 *, Object_t*, const MethodInfo*))List_1_AddCollection_m1_15139_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Object>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m1_15141_gshared (List_1_t1_1894 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m1_15141(__this, ___enumerable, method) (( void (*) (List_1_t1_1894 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m1_15141_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<System.Object>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m1_15143_gshared (List_1_t1_1894 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m1_15143(__this, ___collection, method) (( void (*) (List_1_t1_1894 *, Object_t*, const MethodInfo*))List_1_AddRange_m1_15143_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Object>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t1_1763 * List_1_AsReadOnly_m1_15145_gshared (List_1_t1_1894 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m1_15145(__this, method) (( ReadOnlyCollection_1_t1_1763 * (*) (List_1_t1_1894 *, const MethodInfo*))List_1_AsReadOnly_m1_15145_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::BinarySearch(T)
extern "C" int32_t List_1_BinarySearch_m1_15147_gshared (List_1_t1_1894 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_BinarySearch_m1_15147(__this, ___item, method) (( int32_t (*) (List_1_t1_1894 *, Object_t *, const MethodInfo*))List_1_BinarySearch_m1_15147_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::BinarySearch(T,System.Collections.Generic.IComparer`1<T>)
extern "C" int32_t List_1_BinarySearch_m1_15149_gshared (List_1_t1_1894 * __this, Object_t * ___item, Object_t* ___comparer, const MethodInfo* method);
#define List_1_BinarySearch_m1_15149(__this, ___item, ___comparer, method) (( int32_t (*) (List_1_t1_1894 *, Object_t *, Object_t*, const MethodInfo*))List_1_BinarySearch_m1_15149_gshared)(__this, ___item, ___comparer, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::BinarySearch(System.Int32,System.Int32,T,System.Collections.Generic.IComparer`1<T>)
extern "C" int32_t List_1_BinarySearch_m1_15151_gshared (List_1_t1_1894 * __this, int32_t ___index, int32_t ___count, Object_t * ___item, Object_t* ___comparer, const MethodInfo* method);
#define List_1_BinarySearch_m1_15151(__this, ___index, ___count, ___item, ___comparer, method) (( int32_t (*) (List_1_t1_1894 *, int32_t, int32_t, Object_t *, Object_t*, const MethodInfo*))List_1_BinarySearch_m1_15151_gshared)(__this, ___index, ___count, ___item, ___comparer, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
extern "C" void List_1_Clear_m1_15153_gshared (List_1_t1_1894 * __this, const MethodInfo* method);
#define List_1_Clear_m1_15153(__this, method) (( void (*) (List_1_t1_1894 *, const MethodInfo*))List_1_Clear_m1_15153_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::Contains(T)
extern "C" bool List_1_Contains_m1_15155_gshared (List_1_t1_1894 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_Contains_m1_15155(__this, ___item, method) (( bool (*) (List_1_t1_1894 *, Object_t *, const MethodInfo*))List_1_Contains_m1_15155_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::CopyTo(T[])
extern "C" void List_1_CopyTo_m1_15157_gshared (List_1_t1_1894 * __this, ObjectU5BU5D_t1_272* ___array, const MethodInfo* method);
#define List_1_CopyTo_m1_15157(__this, ___array, method) (( void (*) (List_1_t1_1894 *, ObjectU5BU5D_t1_272*, const MethodInfo*))List_1_CopyTo_m1_15157_gshared)(__this, ___array, method)
// System.Void System.Collections.Generic.List`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m1_15159_gshared (List_1_t1_1894 * __this, ObjectU5BU5D_t1_272* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m1_15159(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_1894 *, ObjectU5BU5D_t1_272*, int32_t, const MethodInfo*))List_1_CopyTo_m1_15159_gshared)(__this, ___array, ___arrayIndex, method)
// System.Void System.Collections.Generic.List`1<System.Object>::CopyTo(System.Int32,T[],System.Int32,System.Int32)
extern "C" void List_1_CopyTo_m1_15161_gshared (List_1_t1_1894 * __this, int32_t ___index, ObjectU5BU5D_t1_272* ___array, int32_t ___arrayIndex, int32_t ___count, const MethodInfo* method);
#define List_1_CopyTo_m1_15161(__this, ___index, ___array, ___arrayIndex, ___count, method) (( void (*) (List_1_t1_1894 *, int32_t, ObjectU5BU5D_t1_272*, int32_t, int32_t, const MethodInfo*))List_1_CopyTo_m1_15161_gshared)(__this, ___index, ___array, ___arrayIndex, ___count, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::Exists(System.Predicate`1<T>)
extern "C" bool List_1_Exists_m1_15163_gshared (List_1_t1_1894 * __this, Predicate_1_t1_1944 * ___match, const MethodInfo* method);
#define List_1_Exists_m1_15163(__this, ___match, method) (( bool (*) (List_1_t1_1894 *, Predicate_1_t1_1944 *, const MethodInfo*))List_1_Exists_m1_15163_gshared)(__this, ___match, method)
// T System.Collections.Generic.List`1<System.Object>::Find(System.Predicate`1<T>)
extern "C" Object_t * List_1_Find_m1_15165_gshared (List_1_t1_1894 * __this, Predicate_1_t1_1944 * ___match, const MethodInfo* method);
#define List_1_Find_m1_15165(__this, ___match, method) (( Object_t * (*) (List_1_t1_1894 *, Predicate_1_t1_1944 *, const MethodInfo*))List_1_Find_m1_15165_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Object>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m1_15167_gshared (Object_t * __this /* static, unused */, Predicate_1_t1_1944 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m1_15167(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t1_1944 *, const MethodInfo*))List_1_CheckMatch_m1_15167_gshared)(__this /* static, unused */, ___match, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<System.Object>::FindAll(System.Predicate`1<T>)
extern "C" List_1_t1_1894 * List_1_FindAll_m1_15169_gshared (List_1_t1_1894 * __this, Predicate_1_t1_1944 * ___match, const MethodInfo* method);
#define List_1_FindAll_m1_15169(__this, ___match, method) (( List_1_t1_1894 * (*) (List_1_t1_1894 *, Predicate_1_t1_1944 *, const MethodInfo*))List_1_FindAll_m1_15169_gshared)(__this, ___match, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<System.Object>::FindAllStackBits(System.Predicate`1<T>)
extern "C" List_1_t1_1894 * List_1_FindAllStackBits_m1_15171_gshared (List_1_t1_1894 * __this, Predicate_1_t1_1944 * ___match, const MethodInfo* method);
#define List_1_FindAllStackBits_m1_15171(__this, ___match, method) (( List_1_t1_1894 * (*) (List_1_t1_1894 *, Predicate_1_t1_1944 *, const MethodInfo*))List_1_FindAllStackBits_m1_15171_gshared)(__this, ___match, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<System.Object>::FindAllList(System.Predicate`1<T>)
extern "C" List_1_t1_1894 * List_1_FindAllList_m1_15173_gshared (List_1_t1_1894 * __this, Predicate_1_t1_1944 * ___match, const MethodInfo* method);
#define List_1_FindAllList_m1_15173(__this, ___match, method) (( List_1_t1_1894 * (*) (List_1_t1_1894 *, Predicate_1_t1_1944 *, const MethodInfo*))List_1_FindAllList_m1_15173_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::FindIndex(System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m1_15175_gshared (List_1_t1_1894 * __this, Predicate_1_t1_1944 * ___match, const MethodInfo* method);
#define List_1_FindIndex_m1_15175(__this, ___match, method) (( int32_t (*) (List_1_t1_1894 *, Predicate_1_t1_1944 *, const MethodInfo*))List_1_FindIndex_m1_15175_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::FindIndex(System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m1_15177_gshared (List_1_t1_1894 * __this, int32_t ___startIndex, Predicate_1_t1_1944 * ___match, const MethodInfo* method);
#define List_1_FindIndex_m1_15177(__this, ___startIndex, ___match, method) (( int32_t (*) (List_1_t1_1894 *, int32_t, Predicate_1_t1_1944 *, const MethodInfo*))List_1_FindIndex_m1_15177_gshared)(__this, ___startIndex, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::FindIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m1_15179_gshared (List_1_t1_1894 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_1944 * ___match, const MethodInfo* method);
#define List_1_FindIndex_m1_15179(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_1894 *, int32_t, int32_t, Predicate_1_t1_1944 *, const MethodInfo*))List_1_FindIndex_m1_15179_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m1_15181_gshared (List_1_t1_1894 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_1944 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m1_15181(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_1894 *, int32_t, int32_t, Predicate_1_t1_1944 *, const MethodInfo*))List_1_GetIndex_m1_15181_gshared)(__this, ___startIndex, ___count, ___match, method)
// T System.Collections.Generic.List`1<System.Object>::FindLast(System.Predicate`1<T>)
extern "C" Object_t * List_1_FindLast_m1_15183_gshared (List_1_t1_1894 * __this, Predicate_1_t1_1944 * ___match, const MethodInfo* method);
#define List_1_FindLast_m1_15183(__this, ___match, method) (( Object_t * (*) (List_1_t1_1894 *, Predicate_1_t1_1944 *, const MethodInfo*))List_1_FindLast_m1_15183_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::FindLastIndex(System.Predicate`1<T>)
extern "C" int32_t List_1_FindLastIndex_m1_15185_gshared (List_1_t1_1894 * __this, Predicate_1_t1_1944 * ___match, const MethodInfo* method);
#define List_1_FindLastIndex_m1_15185(__this, ___match, method) (( int32_t (*) (List_1_t1_1894 *, Predicate_1_t1_1944 *, const MethodInfo*))List_1_FindLastIndex_m1_15185_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::FindLastIndex(System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindLastIndex_m1_15187_gshared (List_1_t1_1894 * __this, int32_t ___startIndex, Predicate_1_t1_1944 * ___match, const MethodInfo* method);
#define List_1_FindLastIndex_m1_15187(__this, ___startIndex, ___match, method) (( int32_t (*) (List_1_t1_1894 *, int32_t, Predicate_1_t1_1944 *, const MethodInfo*))List_1_FindLastIndex_m1_15187_gshared)(__this, ___startIndex, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::FindLastIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindLastIndex_m1_15189_gshared (List_1_t1_1894 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_1944 * ___match, const MethodInfo* method);
#define List_1_FindLastIndex_m1_15189(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_1894 *, int32_t, int32_t, Predicate_1_t1_1944 *, const MethodInfo*))List_1_FindLastIndex_m1_15189_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::GetLastIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetLastIndex_m1_15191_gshared (List_1_t1_1894 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_1944 * ___match, const MethodInfo* method);
#define List_1_GetLastIndex_m1_15191(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_1894 *, int32_t, int32_t, Predicate_1_t1_1944 *, const MethodInfo*))List_1_GetLastIndex_m1_15191_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Object>::ForEach(System.Action`1<T>)
extern "C" void List_1_ForEach_m1_15193_gshared (List_1_t1_1894 * __this, Action_1_t1_1945 * ___action, const MethodInfo* method);
#define List_1_ForEach_m1_15193(__this, ___action, method) (( void (*) (List_1_t1_1894 *, Action_1_t1_1945 *, const MethodInfo*))List_1_ForEach_m1_15193_gshared)(__this, ___action, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
extern "C" Enumerator_t1_1936  List_1_GetEnumerator_m1_15195_gshared (List_1_t1_1894 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m1_15195(__this, method) (( Enumerator_t1_1936  (*) (List_1_t1_1894 *, const MethodInfo*))List_1_GetEnumerator_m1_15195_gshared)(__this, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<System.Object>::GetRange(System.Int32,System.Int32)
extern "C" List_1_t1_1894 * List_1_GetRange_m1_15197_gshared (List_1_t1_1894 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_GetRange_m1_15197(__this, ___index, ___count, method) (( List_1_t1_1894 * (*) (List_1_t1_1894 *, int32_t, int32_t, const MethodInfo*))List_1_GetRange_m1_15197_gshared)(__this, ___index, ___count, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m1_15199_gshared (List_1_t1_1894 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_IndexOf_m1_15199(__this, ___item, method) (( int32_t (*) (List_1_t1_1894 *, Object_t *, const MethodInfo*))List_1_IndexOf_m1_15199_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::IndexOf(T,System.Int32)
extern "C" int32_t List_1_IndexOf_m1_15201_gshared (List_1_t1_1894 * __this, Object_t * ___item, int32_t ___index, const MethodInfo* method);
#define List_1_IndexOf_m1_15201(__this, ___item, ___index, method) (( int32_t (*) (List_1_t1_1894 *, Object_t *, int32_t, const MethodInfo*))List_1_IndexOf_m1_15201_gshared)(__this, ___item, ___index, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::IndexOf(T,System.Int32,System.Int32)
extern "C" int32_t List_1_IndexOf_m1_15203_gshared (List_1_t1_1894 * __this, Object_t * ___item, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_IndexOf_m1_15203(__this, ___item, ___index, ___count, method) (( int32_t (*) (List_1_t1_1894 *, Object_t *, int32_t, int32_t, const MethodInfo*))List_1_IndexOf_m1_15203_gshared)(__this, ___item, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m1_15205_gshared (List_1_t1_1894 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m1_15205(__this, ___start, ___delta, method) (( void (*) (List_1_t1_1894 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m1_15205_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<System.Object>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m1_15207_gshared (List_1_t1_1894 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m1_15207(__this, ___index, method) (( void (*) (List_1_t1_1894 *, int32_t, const MethodInfo*))List_1_CheckIndex_m1_15207_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m1_15209_gshared (List_1_t1_1894 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_Insert_m1_15209(__this, ___index, ___item, method) (( void (*) (List_1_t1_1894 *, int32_t, Object_t *, const MethodInfo*))List_1_Insert_m1_15209_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m1_15211_gshared (List_1_t1_1894 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m1_15211(__this, ___collection, method) (( void (*) (List_1_t1_1894 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m1_15211_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Object>::InsertRange(System.Int32,System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_InsertRange_m1_15213_gshared (List_1_t1_1894 * __this, int32_t ___index, Object_t* ___collection, const MethodInfo* method);
#define List_1_InsertRange_m1_15213(__this, ___index, ___collection, method) (( void (*) (List_1_t1_1894 *, int32_t, Object_t*, const MethodInfo*))List_1_InsertRange_m1_15213_gshared)(__this, ___index, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Object>::InsertCollection(System.Int32,System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_InsertCollection_m1_15215_gshared (List_1_t1_1894 * __this, int32_t ___index, Object_t* ___collection, const MethodInfo* method);
#define List_1_InsertCollection_m1_15215(__this, ___index, ___collection, method) (( void (*) (List_1_t1_1894 *, int32_t, Object_t*, const MethodInfo*))List_1_InsertCollection_m1_15215_gshared)(__this, ___index, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Object>::InsertEnumeration(System.Int32,System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_InsertEnumeration_m1_15217_gshared (List_1_t1_1894 * __this, int32_t ___index, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_InsertEnumeration_m1_15217(__this, ___index, ___enumerable, method) (( void (*) (List_1_t1_1894 *, int32_t, Object_t*, const MethodInfo*))List_1_InsertEnumeration_m1_15217_gshared)(__this, ___index, ___enumerable, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::LastIndexOf(T)
extern "C" int32_t List_1_LastIndexOf_m1_15219_gshared (List_1_t1_1894 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_LastIndexOf_m1_15219(__this, ___item, method) (( int32_t (*) (List_1_t1_1894 *, Object_t *, const MethodInfo*))List_1_LastIndexOf_m1_15219_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::LastIndexOf(T,System.Int32)
extern "C" int32_t List_1_LastIndexOf_m1_15221_gshared (List_1_t1_1894 * __this, Object_t * ___item, int32_t ___index, const MethodInfo* method);
#define List_1_LastIndexOf_m1_15221(__this, ___item, ___index, method) (( int32_t (*) (List_1_t1_1894 *, Object_t *, int32_t, const MethodInfo*))List_1_LastIndexOf_m1_15221_gshared)(__this, ___item, ___index, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::LastIndexOf(T,System.Int32,System.Int32)
extern "C" int32_t List_1_LastIndexOf_m1_15223_gshared (List_1_t1_1894 * __this, Object_t * ___item, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_LastIndexOf_m1_15223(__this, ___item, ___index, ___count, method) (( int32_t (*) (List_1_t1_1894 *, Object_t *, int32_t, int32_t, const MethodInfo*))List_1_LastIndexOf_m1_15223_gshared)(__this, ___item, ___index, ___count, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(T)
extern "C" bool List_1_Remove_m1_15225_gshared (List_1_t1_1894 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_Remove_m1_15225(__this, ___item, method) (( bool (*) (List_1_t1_1894 *, Object_t *, const MethodInfo*))List_1_Remove_m1_15225_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m1_15227_gshared (List_1_t1_1894 * __this, Predicate_1_t1_1944 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m1_15227(__this, ___match, method) (( int32_t (*) (List_1_t1_1894 *, Predicate_1_t1_1944 *, const MethodInfo*))List_1_RemoveAll_m1_15227_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Object>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m1_15229_gshared (List_1_t1_1894 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m1_15229(__this, ___index, method) (( void (*) (List_1_t1_1894 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1_15229_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Object>::RemoveRange(System.Int32,System.Int32)
extern "C" void List_1_RemoveRange_m1_15231_gshared (List_1_t1_1894 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_RemoveRange_m1_15231(__this, ___index, ___count, method) (( void (*) (List_1_t1_1894 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m1_15231_gshared)(__this, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Reverse()
extern "C" void List_1_Reverse_m1_15233_gshared (List_1_t1_1894 * __this, const MethodInfo* method);
#define List_1_Reverse_m1_15233(__this, method) (( void (*) (List_1_t1_1894 *, const MethodInfo*))List_1_Reverse_m1_15233_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Reverse(System.Int32,System.Int32)
extern "C" void List_1_Reverse_m1_15235_gshared (List_1_t1_1894 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_Reverse_m1_15235(__this, ___index, ___count, method) (( void (*) (List_1_t1_1894 *, int32_t, int32_t, const MethodInfo*))List_1_Reverse_m1_15235_gshared)(__this, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Sort()
extern "C" void List_1_Sort_m1_15237_gshared (List_1_t1_1894 * __this, const MethodInfo* method);
#define List_1_Sort_m1_15237(__this, method) (( void (*) (List_1_t1_1894 *, const MethodInfo*))List_1_Sort_m1_15237_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C" void List_1_Sort_m1_15239_gshared (List_1_t1_1894 * __this, Object_t* ___comparer, const MethodInfo* method);
#define List_1_Sort_m1_15239(__this, ___comparer, method) (( void (*) (List_1_t1_1894 *, Object_t*, const MethodInfo*))List_1_Sort_m1_15239_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m1_15241_gshared (List_1_t1_1894 * __this, Comparison_1_t1_1948 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m1_15241(__this, ___comparison, method) (( void (*) (List_1_t1_1894 *, Comparison_1_t1_1948 *, const MethodInfo*))List_1_Sort_m1_15241_gshared)(__this, ___comparison, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Sort(System.Int32,System.Int32,System.Collections.Generic.IComparer`1<T>)
extern "C" void List_1_Sort_m1_15243_gshared (List_1_t1_1894 * __this, int32_t ___index, int32_t ___count, Object_t* ___comparer, const MethodInfo* method);
#define List_1_Sort_m1_15243(__this, ___index, ___count, ___comparer, method) (( void (*) (List_1_t1_1894 *, int32_t, int32_t, Object_t*, const MethodInfo*))List_1_Sort_m1_15243_gshared)(__this, ___index, ___count, ___comparer, method)
// T[] System.Collections.Generic.List`1<System.Object>::ToArray()
extern "C" ObjectU5BU5D_t1_272* List_1_ToArray_m1_15035_gshared (List_1_t1_1894 * __this, const MethodInfo* method);
#define List_1_ToArray_m1_15035(__this, method) (( ObjectU5BU5D_t1_272* (*) (List_1_t1_1894 *, const MethodInfo*))List_1_ToArray_m1_15035_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::TrimExcess()
extern "C" void List_1_TrimExcess_m1_15245_gshared (List_1_t1_1894 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m1_15245(__this, method) (( void (*) (List_1_t1_1894 *, const MethodInfo*))List_1_TrimExcess_m1_15245_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::TrueForAll(System.Predicate`1<T>)
extern "C" bool List_1_TrueForAll_m1_15247_gshared (List_1_t1_1894 * __this, Predicate_1_t1_1944 * ___match, const MethodInfo* method);
#define List_1_TrueForAll_m1_15247(__this, ___match, method) (( bool (*) (List_1_t1_1894 *, Predicate_1_t1_1944 *, const MethodInfo*))List_1_TrueForAll_m1_15247_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m1_15249_gshared (List_1_t1_1894 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m1_15249(__this, method) (( int32_t (*) (List_1_t1_1894 *, const MethodInfo*))List_1_get_Capacity_m1_15249_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m1_15251_gshared (List_1_t1_1894 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m1_15251(__this, ___value, method) (( void (*) (List_1_t1_1894 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1_15251_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C" int32_t List_1_get_Count_m1_15253_gshared (List_1_t1_1894 * __this, const MethodInfo* method);
#define List_1_get_Count_m1_15253(__this, method) (( int32_t (*) (List_1_t1_1894 *, const MethodInfo*))List_1_get_Count_m1_15253_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C" Object_t * List_1_get_Item_m1_15255_gshared (List_1_t1_1894 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m1_15255(__this, ___index, method) (( Object_t * (*) (List_1_t1_1894 *, int32_t, const MethodInfo*))List_1_get_Item_m1_15255_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Object>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m1_15257_gshared (List_1_t1_1894 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_set_Item_m1_15257(__this, ___index, ___value, method) (( void (*) (List_1_t1_1894 *, int32_t, Object_t *, const MethodInfo*))List_1_set_Item_m1_15257_gshared)(__this, ___index, ___value, method)
