﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU2_0.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU2_1.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU2_2.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU2.h"

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t4_192  : public Object_t
{
};
struct U3CPrivateImplementationDetailsU3E_t4_192_StaticFields{
	// <PrivateImplementationDetails>/$ArrayType$8 <PrivateImplementationDetails>::$$field-23
	U24ArrayTypeU248_t4_189  ___U24U24fieldU2D23_0;
	// <PrivateImplementationDetails>/$ArrayType$8 <PrivateImplementationDetails>::$$field-26
	U24ArrayTypeU248_t4_189  ___U24U24fieldU2D26_1;
	// <PrivateImplementationDetails>/$ArrayType$256 <PrivateImplementationDetails>::$$field-27
	U24ArrayTypeU24256_t4_190  ___U24U24fieldU2D27_2;
	// <PrivateImplementationDetails>/$ArrayType$256 <PrivateImplementationDetails>::$$field-28
	U24ArrayTypeU24256_t4_190  ___U24U24fieldU2D28_3;
	// <PrivateImplementationDetails>/$ArrayType$1280 <PrivateImplementationDetails>::$$field-29
	U24ArrayTypeU241280_t4_191  ___U24U24fieldU2D29_4;
	// <PrivateImplementationDetails>/$ArrayType$12 <PrivateImplementationDetails>::$$field-43
	U24ArrayTypeU2412_t4_188  ___U24U24fieldU2D43_5;
	// <PrivateImplementationDetails>/$ArrayType$12 <PrivateImplementationDetails>::$$field-44
	U24ArrayTypeU2412_t4_188  ___U24U24fieldU2D44_6;
};
