﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "UnityEngine_UnityEngine_Rect.h"

// VoxelBusters.Utility.UnityGUI.MENU.GUIMenuBase
struct  GUIMenuBase_t8_141  : public MonoBehaviour_t6_91
{
	// UnityEngine.Rect VoxelBusters.Utility.UnityGUI.MENU.GUIMenuBase::m_titleLayoutNormalisedRect
	Rect_t6_51  ___m_titleLayoutNormalisedRect_5;
	// UnityEngine.Rect VoxelBusters.Utility.UnityGUI.MENU.GUIMenuBase::m_buttonLayoutNormalisedRect
	Rect_t6_51  ___m_buttonLayoutNormalisedRect_6;
	// UnityEngine.Rect VoxelBusters.Utility.UnityGUI.MENU.GUIMenuBase::m_resultLayoutNormalisedRect
	Rect_t6_51  ___m_resultLayoutNormalisedRect_7;
	// System.Single VoxelBusters.Utility.UnityGUI.MENU.GUIMenuBase::m_buttonColumnCount
	float ___m_buttonColumnCount_8;
};
