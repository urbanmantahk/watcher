﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExifLibrary.ExifEnumProperty`1<ExifLibrary.Flash>
struct ExifEnumProperty_1_t8_348;
// System.Object
struct Object_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTag.h"
#include "AssemblyU2DCSharp_ExifLibrary_Flash.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifInterOperability.h"

// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.Flash>::.ctor(ExifLibrary.ExifTag,T,System.Boolean)
extern "C" void ExifEnumProperty_1__ctor_m8_1984_gshared (ExifEnumProperty_1_t8_348 * __this, int32_t ___tag, uint16_t ___value, bool ___isbitfield, const MethodInfo* method);
#define ExifEnumProperty_1__ctor_m8_1984(__this, ___tag, ___value, ___isbitfield, method) (( void (*) (ExifEnumProperty_1_t8_348 *, int32_t, uint16_t, bool, const MethodInfo*))ExifEnumProperty_1__ctor_m8_1984_gshared)(__this, ___tag, ___value, ___isbitfield, method)
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.Flash>::.ctor(ExifLibrary.ExifTag,T)
extern "C" void ExifEnumProperty_1__ctor_m8_2162_gshared (ExifEnumProperty_1_t8_348 * __this, int32_t ___tag, uint16_t ___value, const MethodInfo* method);
#define ExifEnumProperty_1__ctor_m8_2162(__this, ___tag, ___value, method) (( void (*) (ExifEnumProperty_1_t8_348 *, int32_t, uint16_t, const MethodInfo*))ExifEnumProperty_1__ctor_m8_2162_gshared)(__this, ___tag, ___value, method)
// System.Object ExifLibrary.ExifEnumProperty`1<ExifLibrary.Flash>::get__Value()
extern "C" Object_t * ExifEnumProperty_1_get__Value_m8_2163_gshared (ExifEnumProperty_1_t8_348 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get__Value_m8_2163(__this, method) (( Object_t * (*) (ExifEnumProperty_1_t8_348 *, const MethodInfo*))ExifEnumProperty_1_get__Value_m8_2163_gshared)(__this, method)
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.Flash>::set__Value(System.Object)
extern "C" void ExifEnumProperty_1_set__Value_m8_2164_gshared (ExifEnumProperty_1_t8_348 * __this, Object_t * ___value, const MethodInfo* method);
#define ExifEnumProperty_1_set__Value_m8_2164(__this, ___value, method) (( void (*) (ExifEnumProperty_1_t8_348 *, Object_t *, const MethodInfo*))ExifEnumProperty_1_set__Value_m8_2164_gshared)(__this, ___value, method)
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.Flash>::get_Value()
extern "C" uint16_t ExifEnumProperty_1_get_Value_m8_2165_gshared (ExifEnumProperty_1_t8_348 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get_Value_m8_2165(__this, method) (( uint16_t (*) (ExifEnumProperty_1_t8_348 *, const MethodInfo*))ExifEnumProperty_1_get_Value_m8_2165_gshared)(__this, method)
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.Flash>::set_Value(T)
extern "C" void ExifEnumProperty_1_set_Value_m8_2166_gshared (ExifEnumProperty_1_t8_348 * __this, uint16_t ___value, const MethodInfo* method);
#define ExifEnumProperty_1_set_Value_m8_2166(__this, ___value, method) (( void (*) (ExifEnumProperty_1_t8_348 *, uint16_t, const MethodInfo*))ExifEnumProperty_1_set_Value_m8_2166_gshared)(__this, ___value, method)
// System.Boolean ExifLibrary.ExifEnumProperty`1<ExifLibrary.Flash>::get_IsBitField()
extern "C" bool ExifEnumProperty_1_get_IsBitField_m8_2167_gshared (ExifEnumProperty_1_t8_348 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get_IsBitField_m8_2167(__this, method) (( bool (*) (ExifEnumProperty_1_t8_348 *, const MethodInfo*))ExifEnumProperty_1_get_IsBitField_m8_2167_gshared)(__this, method)
// System.String ExifLibrary.ExifEnumProperty`1<ExifLibrary.Flash>::ToString()
extern "C" String_t* ExifEnumProperty_1_ToString_m8_2168_gshared (ExifEnumProperty_1_t8_348 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_ToString_m8_2168(__this, method) (( String_t* (*) (ExifEnumProperty_1_t8_348 *, const MethodInfo*))ExifEnumProperty_1_ToString_m8_2168_gshared)(__this, method)
// ExifLibrary.ExifInterOperability ExifLibrary.ExifEnumProperty`1<ExifLibrary.Flash>::get_Interoperability()
extern "C" ExifInterOperability_t8_113  ExifEnumProperty_1_get_Interoperability_m8_2169_gshared (ExifEnumProperty_1_t8_348 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get_Interoperability_m8_2169(__this, method) (( ExifInterOperability_t8_113  (*) (ExifEnumProperty_1_t8_348 *, const MethodInfo*))ExifEnumProperty_1_get_Interoperability_m8_2169_gshared)(__this, method)
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.Flash>::op_Implicit(ExifLibrary.ExifEnumProperty`1<T>)
extern "C" uint16_t ExifEnumProperty_1_op_Implicit_m8_2170_gshared (Object_t * __this /* static, unused */, ExifEnumProperty_1_t8_348 * ___obj, const MethodInfo* method);
#define ExifEnumProperty_1_op_Implicit_m8_2170(__this /* static, unused */, ___obj, method) (( uint16_t (*) (Object_t * /* static, unused */, ExifEnumProperty_1_t8_348 *, const MethodInfo*))ExifEnumProperty_1_op_Implicit_m8_2170_gshared)(__this /* static, unused */, ___obj, method)
