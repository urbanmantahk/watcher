﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Diagnostics.DebuggerDisplayAttribute
struct DebuggerDisplayAttribute_t1_328;
// System.String
struct String_t;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Diagnostics.DebuggerDisplayAttribute::.ctor(System.String)
extern "C" void DebuggerDisplayAttribute__ctor_m1_3588 (DebuggerDisplayAttribute_t1_328 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Diagnostics.DebuggerDisplayAttribute::get_Value()
extern "C" String_t* DebuggerDisplayAttribute_get_Value_m1_3589 (DebuggerDisplayAttribute_t1_328 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Diagnostics.DebuggerDisplayAttribute::get_Target()
extern "C" Type_t * DebuggerDisplayAttribute_get_Target_m1_3590 (DebuggerDisplayAttribute_t1_328 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.DebuggerDisplayAttribute::set_Target(System.Type)
extern "C" void DebuggerDisplayAttribute_set_Target_m1_3591 (DebuggerDisplayAttribute_t1_328 * __this, Type_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Diagnostics.DebuggerDisplayAttribute::get_TargetTypeName()
extern "C" String_t* DebuggerDisplayAttribute_get_TargetTypeName_m1_3592 (DebuggerDisplayAttribute_t1_328 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.DebuggerDisplayAttribute::set_TargetTypeName(System.String)
extern "C" void DebuggerDisplayAttribute_set_TargetTypeName_m1_3593 (DebuggerDisplayAttribute_t1_328 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Diagnostics.DebuggerDisplayAttribute::get_Type()
extern "C" String_t* DebuggerDisplayAttribute_get_Type_m1_3594 (DebuggerDisplayAttribute_t1_328 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.DebuggerDisplayAttribute::set_Type(System.String)
extern "C" void DebuggerDisplayAttribute_set_Type_m1_3595 (DebuggerDisplayAttribute_t1_328 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Diagnostics.DebuggerDisplayAttribute::get_Name()
extern "C" String_t* DebuggerDisplayAttribute_get_Name_m1_3596 (DebuggerDisplayAttribute_t1_328 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.DebuggerDisplayAttribute::set_Name(System.String)
extern "C" void DebuggerDisplayAttribute_set_Name_m1_3597 (DebuggerDisplayAttribute_t1_328 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
