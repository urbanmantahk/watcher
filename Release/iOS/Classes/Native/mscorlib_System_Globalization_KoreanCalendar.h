﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Globalization.CCGregorianEraHandler
struct CCGregorianEraHandler_t1_353;

#include "mscorlib_System_Globalization_Calendar.h"
#include "mscorlib_System_DateTime.h"

// System.Globalization.KoreanCalendar
struct  KoreanCalendar_t1_381  : public Calendar_t1_338
{
};
struct KoreanCalendar_t1_381_StaticFields{
	// System.Globalization.CCGregorianEraHandler System.Globalization.KoreanCalendar::M_EraHandler
	CCGregorianEraHandler_t1_353 * ___M_EraHandler_8;
	// System.DateTime System.Globalization.KoreanCalendar::KoreanMin
	DateTime_t1_150  ___KoreanMin_9;
	// System.DateTime System.Globalization.KoreanCalendar::KoreanMax
	DateTime_t1_150  ___KoreanMax_10;
};
