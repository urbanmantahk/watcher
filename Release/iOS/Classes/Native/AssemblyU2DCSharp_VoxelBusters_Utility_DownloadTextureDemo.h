﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.MeshRenderer
struct MeshRenderer_t6_28;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// VoxelBusters.Utility.DownloadTextureDemo
struct  DownloadTextureDemo_t8_153  : public MonoBehaviour_t6_91
{
	// System.String VoxelBusters.Utility.DownloadTextureDemo::m_URLString
	String_t* ___m_URLString_2;
	// UnityEngine.MeshRenderer VoxelBusters.Utility.DownloadTextureDemo::m_renderer
	MeshRenderer_t6_28 * ___m_renderer_3;
};
