﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.AssemblyLoadEventArgs
struct AssemblyLoadEventArgs_t1_1504;
// System.Reflection.Assembly
struct Assembly_t1_467;

#include "codegen/il2cpp-codegen.h"

// System.Void System.AssemblyLoadEventArgs::.ctor(System.Reflection.Assembly)
extern "C" void AssemblyLoadEventArgs__ctor_m1_13290 (AssemblyLoadEventArgs_t1_1504 * __this, Assembly_t1_467 * ___loadedAssembly, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.AssemblyLoadEventArgs::get_LoadedAssembly()
extern "C" Assembly_t1_467 * AssemblyLoadEventArgs_get_LoadedAssembly_m1_13291 (AssemblyLoadEventArgs_t1_1504 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
