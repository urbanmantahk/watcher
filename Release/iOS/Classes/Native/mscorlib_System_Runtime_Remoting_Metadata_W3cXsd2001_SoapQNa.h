﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object.h"

// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapQName
struct  SoapQName_t1_992  : public Object_t
{
	// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapQName::_name
	String_t* ____name_0;
	// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapQName::_key
	String_t* ____key_1;
	// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapQName::_namespace
	String_t* ____namespace_2;
};
