﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.CompilerServices.HasCopySemanticsAttribute
struct HasCopySemanticsAttribute_t1_687;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.CompilerServices.HasCopySemanticsAttribute::.ctor()
extern "C" void HasCopySemanticsAttribute__ctor_m1_7545 (HasCopySemanticsAttribute_t1_687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
