﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Type
struct Type_t;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Reflection.Emit.LocalBuilder[]
struct LocalBuilderU5BU5D_t1_517;
// System.Reflection.Emit.ILExceptionInfo[]
struct ILExceptionInfoU5BU5D_t1_518;
// System.Reflection.Emit.ILTokenInfo[]
struct ILTokenInfoU5BU5D_t1_519;
// System.Reflection.Emit.ILGenerator/LabelData[]
struct LabelDataU5BU5D_t1_520;
// System.Reflection.Emit.ILGenerator/LabelFixup[]
struct LabelFixupU5BU5D_t1_521;
// System.Reflection.Module
struct Module_t1_495;
// System.Collections.Stack
struct Stack_t1_243;
// System.Reflection.Emit.TokenGenerator
struct TokenGenerator_t1_523;
// System.Collections.ArrayList
struct ArrayList_t1_170;
// System.Reflection.Emit.SequencePointList
struct SequencePointList_t1_522;

#include "mscorlib_System_Object.h"

// System.Reflection.Emit.ILGenerator
struct  ILGenerator_t1_480  : public Object_t
{
	// System.Byte[] System.Reflection.Emit.ILGenerator::code
	ByteU5BU5D_t1_109* ___code_4;
	// System.Int32 System.Reflection.Emit.ILGenerator::code_len
	int32_t ___code_len_5;
	// System.Int32 System.Reflection.Emit.ILGenerator::max_stack
	int32_t ___max_stack_6;
	// System.Int32 System.Reflection.Emit.ILGenerator::cur_stack
	int32_t ___cur_stack_7;
	// System.Reflection.Emit.LocalBuilder[] System.Reflection.Emit.ILGenerator::locals
	LocalBuilderU5BU5D_t1_517* ___locals_8;
	// System.Reflection.Emit.ILExceptionInfo[] System.Reflection.Emit.ILGenerator::ex_handlers
	ILExceptionInfoU5BU5D_t1_518* ___ex_handlers_9;
	// System.Int32 System.Reflection.Emit.ILGenerator::num_token_fixups
	int32_t ___num_token_fixups_10;
	// System.Reflection.Emit.ILTokenInfo[] System.Reflection.Emit.ILGenerator::token_fixups
	ILTokenInfoU5BU5D_t1_519* ___token_fixups_11;
	// System.Reflection.Emit.ILGenerator/LabelData[] System.Reflection.Emit.ILGenerator::labels
	LabelDataU5BU5D_t1_520* ___labels_12;
	// System.Int32 System.Reflection.Emit.ILGenerator::num_labels
	int32_t ___num_labels_13;
	// System.Reflection.Emit.ILGenerator/LabelFixup[] System.Reflection.Emit.ILGenerator::fixups
	LabelFixupU5BU5D_t1_521* ___fixups_14;
	// System.Int32 System.Reflection.Emit.ILGenerator::num_fixups
	int32_t ___num_fixups_15;
	// System.Reflection.Module System.Reflection.Emit.ILGenerator::module
	Module_t1_495 * ___module_16;
	// System.Int32 System.Reflection.Emit.ILGenerator::cur_block
	int32_t ___cur_block_17;
	// System.Collections.Stack System.Reflection.Emit.ILGenerator::open_blocks
	Stack_t1_243 * ___open_blocks_18;
	// System.Reflection.Emit.TokenGenerator System.Reflection.Emit.ILGenerator::token_gen
	Object_t * ___token_gen_19;
	// System.Collections.ArrayList System.Reflection.Emit.ILGenerator::sequencePointLists
	ArrayList_t1_170 * ___sequencePointLists_20;
	// System.Reflection.Emit.SequencePointList System.Reflection.Emit.ILGenerator::currentSequence
	SequencePointList_t1_522 * ___currentSequence_21;
};
struct ILGenerator_t1_480_StaticFields{
	// System.Type System.Reflection.Emit.ILGenerator::void_type
	Type_t * ___void_type_3;
};
