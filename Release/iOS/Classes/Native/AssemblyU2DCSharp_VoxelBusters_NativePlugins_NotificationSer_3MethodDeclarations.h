﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.NotificationServiceIOS
struct NotificationServiceIOS_t8_262;
// System.String
struct String_t;
// VoxelBusters.NativePlugins.CrossPlatformNotification
struct CrossPlatformNotification_t8_259;
// VoxelBusters.NativePlugins.NotificationServiceSettings
struct NotificationServiceSettings_t8_270;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_NotificationTyp.h"

// System.Void VoxelBusters.NativePlugins.NotificationServiceIOS::.ctor()
extern "C" void NotificationServiceIOS__ctor_m8_1469 (NotificationServiceIOS_t8_262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationServiceIOS::ParseAppLaunchInfo(System.String,VoxelBusters.NativePlugins.CrossPlatformNotification&,VoxelBusters.NativePlugins.CrossPlatformNotification&)
extern "C" void NotificationServiceIOS_ParseAppLaunchInfo_m8_1470 (NotificationServiceIOS_t8_262 * __this, String_t* ____launchData, CrossPlatformNotification_t8_259 ** ____launchLocalNotification, CrossPlatformNotification_t8_259 ** ____launchRemoteNotification, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationServiceIOS::initNotificationService(System.String)
extern "C" void NotificationServiceIOS_initNotificationService_m8_1471 (Object_t * __this /* static, unused */, String_t* ____keyForUserInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationServiceIOS::registerNotificationTypes(System.Int32)
extern "C" void NotificationServiceIOS_registerNotificationTypes_m8_1472 (Object_t * __this /* static, unused */, int32_t ___notificationTypes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 VoxelBusters.NativePlugins.NotificationServiceIOS::enabledNotificationTypes()
extern "C" int32_t NotificationServiceIOS_enabledNotificationTypes_m8_1473 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationServiceIOS::scheduleLocalNotification(System.String)
extern "C" void NotificationServiceIOS_scheduleLocalNotification_m8_1474 (Object_t * __this /* static, unused */, String_t* ____payload, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationServiceIOS::cancelLocalNotification(System.String)
extern "C" void NotificationServiceIOS_cancelLocalNotification_m8_1475 (Object_t * __this /* static, unused */, String_t* ____notificationID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationServiceIOS::cancelAllLocalNotifications()
extern "C" void NotificationServiceIOS_cancelAllLocalNotifications_m8_1476 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationServiceIOS::clearNotifications()
extern "C" void NotificationServiceIOS_clearNotifications_m8_1477 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationServiceIOS::registerForRemoteNotifications()
extern "C" void NotificationServiceIOS_registerForRemoteNotifications_m8_1478 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationServiceIOS::unregisterForRemoteNotifications()
extern "C" void NotificationServiceIOS_unregisterForRemoteNotifications_m8_1479 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationServiceIOS::Initialise(VoxelBusters.NativePlugins.NotificationServiceSettings)
extern "C" void NotificationServiceIOS_Initialise_m8_1480 (NotificationServiceIOS_t8_262 * __this, NotificationServiceSettings_t8_270 * ____settings, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationServiceIOS::RegisterNotificationTypes(VoxelBusters.NativePlugins.NotificationType)
extern "C" void NotificationServiceIOS_RegisterNotificationTypes_m8_1481 (NotificationServiceIOS_t8_262 * __this, int32_t ____notificationTypes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.NotificationServiceIOS::ScheduleLocalNotification(VoxelBusters.NativePlugins.CrossPlatformNotification)
extern "C" String_t* NotificationServiceIOS_ScheduleLocalNotification_m8_1482 (NotificationServiceIOS_t8_262 * __this, CrossPlatformNotification_t8_259 * ____notification, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationServiceIOS::CancelLocalNotification(System.String)
extern "C" void NotificationServiceIOS_CancelLocalNotification_m8_1483 (NotificationServiceIOS_t8_262 * __this, String_t* ____notificationID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationServiceIOS::CancelAllLocalNotification()
extern "C" void NotificationServiceIOS_CancelAllLocalNotification_m8_1484 (NotificationServiceIOS_t8_262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationServiceIOS::ClearNotifications()
extern "C" void NotificationServiceIOS_ClearNotifications_m8_1485 (NotificationServiceIOS_t8_262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationServiceIOS::RegisterForRemoteNotifications()
extern "C" void NotificationServiceIOS_RegisterForRemoteNotifications_m8_1486 (NotificationServiceIOS_t8_262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationServiceIOS::UnregisterForRemoteNotifications()
extern "C" void NotificationServiceIOS_UnregisterForRemoteNotifications_m8_1487 (NotificationServiceIOS_t8_262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
