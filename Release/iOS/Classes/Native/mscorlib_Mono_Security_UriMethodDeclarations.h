﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.Uri
struct Uri_t1_237;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1_238;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_Mono_Security_UriPartial.h"

// System.Void Mono.Security.Uri::.ctor(System.String)
extern "C" void Uri__ctor_m1_2612 (Uri_t1_237 * __this, String_t* ___uriString, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Uri::.ctor(System.String,System.Boolean)
extern "C" void Uri__ctor_m1_2613 (Uri_t1_237 * __this, String_t* ___uriString, bool ___dontEscape, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Uri::.ctor(System.String,System.Boolean,System.Boolean)
extern "C" void Uri__ctor_m1_2614 (Uri_t1_237 * __this, String_t* ___uriString, bool ___dontEscape, bool ___reduce, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Uri::.ctor(Mono.Security.Uri,System.String)
extern "C" void Uri__ctor_m1_2615 (Uri_t1_237 * __this, Uri_t1_237 * ___baseUri, String_t* ___relativeUri, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Uri::.ctor(Mono.Security.Uri,System.String,System.Boolean)
extern "C" void Uri__ctor_m1_2616 (Uri_t1_237 * __this, Uri_t1_237 * ___baseUri, String_t* ___relativeUri, bool ___dontEscape, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Uri::.cctor()
extern "C" void Uri__cctor_m1_2617 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.Uri::get_AbsolutePath()
extern "C" String_t* Uri_get_AbsolutePath_m1_2618 (Uri_t1_237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.Uri::get_AbsoluteUri()
extern "C" String_t* Uri_get_AbsoluteUri_m1_2619 (Uri_t1_237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.Uri::get_Authority()
extern "C" String_t* Uri_get_Authority_m1_2620 (Uri_t1_237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.Uri::get_Fragment()
extern "C" String_t* Uri_get_Fragment_m1_2621 (Uri_t1_237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.Uri::get_Host()
extern "C" String_t* Uri_get_Host_m1_2622 (Uri_t1_237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.Uri::get_IsDefaultPort()
extern "C" bool Uri_get_IsDefaultPort_m1_2623 (Uri_t1_237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.Uri::get_IsFile()
extern "C" bool Uri_get_IsFile_m1_2624 (Uri_t1_237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.Uri::get_IsLoopback()
extern "C" bool Uri_get_IsLoopback_m1_2625 (Uri_t1_237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.Uri::get_IsUnc()
extern "C" bool Uri_get_IsUnc_m1_2626 (Uri_t1_237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.Uri::get_LocalPath()
extern "C" String_t* Uri_get_LocalPath_m1_2627 (Uri_t1_237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.Uri::get_PathAndQuery()
extern "C" String_t* Uri_get_PathAndQuery_m1_2628 (Uri_t1_237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Security.Uri::get_Port()
extern "C" int32_t Uri_get_Port_m1_2629 (Uri_t1_237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.Uri::get_Query()
extern "C" String_t* Uri_get_Query_m1_2630 (Uri_t1_237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.Uri::get_Scheme()
extern "C" String_t* Uri_get_Scheme_m1_2631 (Uri_t1_237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] Mono.Security.Uri::get_Segments()
extern "C" StringU5BU5D_t1_238* Uri_get_Segments_m1_2632 (Uri_t1_237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.Uri::get_UserEscaped()
extern "C" bool Uri_get_UserEscaped_m1_2633 (Uri_t1_237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.Uri::get_UserInfo()
extern "C" String_t* Uri_get_UserInfo_m1_2634 (Uri_t1_237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.Uri::IsIPv4Address(System.String)
extern "C" bool Uri_IsIPv4Address_m1_2635 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.Uri::IsDomainAddress(System.String)
extern "C" bool Uri_IsDomainAddress_m1_2636 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.Uri::CheckSchemeName(System.String)
extern "C" bool Uri_CheckSchemeName_m1_2637 (Object_t * __this /* static, unused */, String_t* ___schemeName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.Uri::Equals(System.Object)
extern "C" bool Uri_Equals_m1_2638 (Uri_t1_237 * __this, Object_t * ___comparant, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Security.Uri::GetHashCode()
extern "C" int32_t Uri_GetHashCode_m1_2639 (Uri_t1_237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.Uri::GetLeftPart(Mono.Security.UriPartial)
extern "C" String_t* Uri_GetLeftPart_m1_2640 (Uri_t1_237 * __this, int32_t ___part, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Security.Uri::FromHex(System.Char)
extern "C" int32_t Uri_FromHex_m1_2641 (Object_t * __this /* static, unused */, uint16_t ___digit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.Uri::HexEscape(System.Char)
extern "C" String_t* Uri_HexEscape_m1_2642 (Object_t * __this /* static, unused */, uint16_t ___character, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char Mono.Security.Uri::HexUnescape(System.String,System.Int32&)
extern "C" uint16_t Uri_HexUnescape_m1_2643 (Object_t * __this /* static, unused */, String_t* ___pattern, int32_t* ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.Uri::IsHexDigit(System.Char)
extern "C" bool Uri_IsHexDigit_m1_2644 (Object_t * __this /* static, unused */, uint16_t ___digit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.Uri::IsHexEncoding(System.String,System.Int32)
extern "C" bool Uri_IsHexEncoding_m1_2645 (Object_t * __this /* static, unused */, String_t* ___pattern, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.Uri::MakeRelative(Mono.Security.Uri)
extern "C" String_t* Uri_MakeRelative_m1_2646 (Uri_t1_237 * __this, Uri_t1_237 * ___toUri, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.Uri::ToString()
extern "C" String_t* Uri_ToString_m1_2647 (Uri_t1_237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Uri::Escape()
extern "C" void Uri_Escape_m1_2648 (Uri_t1_237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.Uri::EscapeString(System.String)
extern "C" String_t* Uri_EscapeString_m1_2649 (Object_t * __this /* static, unused */, String_t* ___str, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.Uri::EscapeString(System.String,System.Boolean,System.Boolean,System.Boolean)
extern "C" String_t* Uri_EscapeString_m1_2650 (Object_t * __this /* static, unused */, String_t* ___str, bool ___escapeReserved, bool ___escapeHex, bool ___escapeBrackets, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Uri::Parse()
extern "C" void Uri_Parse_m1_2651 (Uri_t1_237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.Uri::Unescape(System.String)
extern "C" String_t* Uri_Unescape_m1_2652 (Uri_t1_237 * __this, String_t* ___str, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.Uri::Unescape(System.String,System.Boolean)
extern "C" String_t* Uri_Unescape_m1_2653 (Uri_t1_237 * __this, String_t* ___str, bool ___excludeSharp, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Uri::ParseAsWindowsUNC(System.String)
extern "C" void Uri_ParseAsWindowsUNC_m1_2654 (Uri_t1_237 * __this, String_t* ___uriString, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Uri::ParseAsWindowsAbsoluteFilePath(System.String)
extern "C" void Uri_ParseAsWindowsAbsoluteFilePath_m1_2655 (Uri_t1_237 * __this, String_t* ___uriString, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Uri::ParseAsUnixAbsoluteFilePath(System.String)
extern "C" void Uri_ParseAsUnixAbsoluteFilePath_m1_2656 (Uri_t1_237 * __this, String_t* ___uriString, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Uri::Parse(System.String)
extern "C" void Uri_Parse_m1_2657 (Uri_t1_237 * __this, String_t* ___uriString, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.Uri::Reduce(System.String)
extern "C" String_t* Uri_Reduce_m1_2658 (Object_t * __this /* static, unused */, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.Uri::GetSchemeDelimiter(System.String)
extern "C" String_t* Uri_GetSchemeDelimiter_m1_2659 (Object_t * __this /* static, unused */, String_t* ___scheme, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Security.Uri::GetDefaultPort(System.String)
extern "C" int32_t Uri_GetDefaultPort_m1_2660 (Object_t * __this /* static, unused */, String_t* ___scheme, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.Uri::GetOpaqueWiseSchemeDelimiter()
extern "C" String_t* Uri_GetOpaqueWiseSchemeDelimiter_m1_2661 (Uri_t1_237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.Uri::IsBadFileSystemCharacter(System.Char)
extern "C" bool Uri_IsBadFileSystemCharacter_m1_2662 (Uri_t1_237 * __this, uint16_t ___ch, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.Uri::IsExcludedCharacter(System.Char)
extern "C" bool Uri_IsExcludedCharacter_m1_2663 (Object_t * __this /* static, unused */, uint16_t ___ch, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.Uri::IsPredefinedScheme(System.String)
extern "C" bool Uri_IsPredefinedScheme_m1_2664 (Object_t * __this /* static, unused */, String_t* ___scheme, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.Uri::IsReservedCharacter(System.Char)
extern "C" bool Uri_IsReservedCharacter_m1_2665 (Uri_t1_237 * __this, uint16_t ___ch, const MethodInfo* method) IL2CPP_METHOD_ATTR;
