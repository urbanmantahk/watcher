﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Text.UTF7Encoding/UTF7Encoder
struct UTF7Encoder_t1_1448;
// System.Char[]
struct CharU5BU5D_t1_16;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Text.UTF7Encoding/UTF7Encoder::.ctor(System.Boolean)
extern "C" void UTF7Encoder__ctor_m1_12513 (UTF7Encoder_t1_1448 * __this, bool ___allowOptionals, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.UTF7Encoding/UTF7Encoder::GetByteCount(System.Char[],System.Int32,System.Int32,System.Boolean)
extern "C" int32_t UTF7Encoder_GetByteCount_m1_12514 (UTF7Encoder_t1_1448 * __this, CharU5BU5D_t1_16* ___chars, int32_t ___index, int32_t ___count, bool ___flush, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.UTF7Encoding/UTF7Encoder::GetBytes(System.Char[],System.Int32,System.Int32,System.Byte[],System.Int32,System.Boolean)
extern "C" int32_t UTF7Encoder_GetBytes_m1_12515 (UTF7Encoder_t1_1448 * __this, CharU5BU5D_t1_16* ___chars, int32_t ___charIndex, int32_t ___charCount, ByteU5BU5D_t1_109* ___bytes, int32_t ___byteIndex, bool ___flush, const MethodInfo* method) IL2CPP_METHOD_ATTR;
