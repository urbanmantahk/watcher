﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Runtime.Remoting.ObjRef
struct ObjRef_t1_923;

#include "mscorlib_System_Object.h"

// System.Runtime.Remoting.Messaging.CADObjRef
struct  CADObjRef_t1_922  : public Object_t
{
	// System.Runtime.Remoting.ObjRef System.Runtime.Remoting.Messaging.CADObjRef::objref
	ObjRef_t1_923 * ___objref_0;
	// System.Int32 System.Runtime.Remoting.Messaging.CADObjRef::SourceDomain
	int32_t ___SourceDomain_1;
};
