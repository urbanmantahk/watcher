﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Policy.StrongName
struct StrongName_t1_1363;
// System.Security.Permissions.StrongNamePublicKeyBlob
struct StrongNamePublicKeyBlob_t1_1314;
// System.String
struct String_t;
// System.Version
struct Version_t1_578;
// System.Char[]
struct CharU5BU5D_t1_16;
// System.Object
struct Object_t;
// System.Security.IPermission
struct IPermission_t1_1400;
// System.Security.Policy.Evidence
struct Evidence_t1_398;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Policy.StrongName::.ctor(System.Security.Permissions.StrongNamePublicKeyBlob,System.String,System.Version)
extern "C" void StrongName__ctor_m1_11683 (StrongName_t1_1363 * __this, StrongNamePublicKeyBlob_t1_1314 * ___blob, String_t* ___name, Version_t1_578 * ___version, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Policy.StrongName::System.Security.Policy.IBuiltInEvidence.GetRequiredSize(System.Boolean)
extern "C" int32_t StrongName_System_Security_Policy_IBuiltInEvidence_GetRequiredSize_m1_11684 (StrongName_t1_1363 * __this, bool ___verbose, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Policy.StrongName::System.Security.Policy.IBuiltInEvidence.InitFromBuffer(System.Char[],System.Int32)
extern "C" int32_t StrongName_System_Security_Policy_IBuiltInEvidence_InitFromBuffer_m1_11685 (StrongName_t1_1363 * __this, CharU5BU5D_t1_16* ___buffer, int32_t ___position, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Policy.StrongName::System.Security.Policy.IBuiltInEvidence.OutputToBuffer(System.Char[],System.Int32,System.Boolean)
extern "C" int32_t StrongName_System_Security_Policy_IBuiltInEvidence_OutputToBuffer_m1_11686 (StrongName_t1_1363 * __this, CharU5BU5D_t1_16* ___buffer, int32_t ___position, bool ___verbose, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Policy.StrongName::get_Name()
extern "C" String_t* StrongName_get_Name_m1_11687 (StrongName_t1_1363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Permissions.StrongNamePublicKeyBlob System.Security.Policy.StrongName::get_PublicKey()
extern "C" StrongNamePublicKeyBlob_t1_1314 * StrongName_get_PublicKey_m1_11688 (StrongName_t1_1363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Version System.Security.Policy.StrongName::get_Version()
extern "C" Version_t1_578 * StrongName_get_Version_m1_11689 (StrongName_t1_1363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Security.Policy.StrongName::Copy()
extern "C" Object_t * StrongName_Copy_m1_11690 (StrongName_t1_1363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Policy.StrongName::CreateIdentityPermission(System.Security.Policy.Evidence)
extern "C" Object_t * StrongName_CreateIdentityPermission_m1_11691 (StrongName_t1_1363 * __this, Evidence_t1_398 * ___evidence, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Policy.StrongName::Equals(System.Object)
extern "C" bool StrongName_Equals_m1_11692 (StrongName_t1_1363 * __this, Object_t * ___o, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Policy.StrongName::GetHashCode()
extern "C" int32_t StrongName_GetHashCode_m1_11693 (StrongName_t1_1363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Policy.StrongName::ToString()
extern "C" String_t* StrongName_ToString_m1_11694 (StrongName_t1_1363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
