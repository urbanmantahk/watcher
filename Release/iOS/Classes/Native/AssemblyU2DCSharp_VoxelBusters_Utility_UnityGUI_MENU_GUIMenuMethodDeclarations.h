﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.Utility.UnityGUI.MENU.GUIMenuBase
struct GUIMenuBase_t8_141;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Rect.h"

// System.Void VoxelBusters.Utility.UnityGUI.MENU.GUIMenuBase::.ctor()
extern "C" void GUIMenuBase__ctor_m8_827 (GUIMenuBase_t8_141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.UnityGUI.MENU.GUIMenuBase::OnEnable()
extern "C" void GUIMenuBase_OnEnable_m8_828 (GUIMenuBase_t8_141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.UnityGUI.MENU.GUIMenuBase::OnDisable()
extern "C" void GUIMenuBase_OnDisable_m8_829 (GUIMenuBase_t8_141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.UnityGUI.MENU.GUIMenuBase::OnGUI()
extern "C" void GUIMenuBase_OnGUI_m8_830 (GUIMenuBase_t8_141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.UnityGUI.MENU.GUIMenuBase::DrawTitle(System.String)
extern "C" void GUIMenuBase_DrawTitle_m8_831 (GUIMenuBase_t8_141 * __this, String_t* ____title, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.Utility.UnityGUI.MENU.GUIMenuBase::DrawTitleWithBackButton(System.String,System.String)
extern "C" bool GUIMenuBase_DrawTitleWithBackButton_m8_832 (GUIMenuBase_t8_141 * __this, String_t* ____title, String_t* ____button, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.UnityGUI.MENU.GUIMenuBase::BeginButtonLayout(System.Single,System.Single)
extern "C" void GUIMenuBase_BeginButtonLayout_m8_833 (GUIMenuBase_t8_141 * __this, float ____columnCount, float ____normalisedHeight, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.UnityGUI.MENU.GUIMenuBase::EndButtonLayout()
extern "C" void GUIMenuBase_EndButtonLayout_m8_834 (GUIMenuBase_t8_141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.Utility.UnityGUI.MENU.GUIMenuBase::DrawButton(System.String)
extern "C" bool GUIMenuBase_DrawButton_m8_835 (GUIMenuBase_t8_141 * __this, String_t* ____buttonName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.UnityGUI.MENU.GUIMenuBase::DrawResultLayout(System.String,System.Single)
extern "C" void GUIMenuBase_DrawResultLayout_m8_836 (GUIMenuBase_t8_141 * __this, String_t* ____result, float ____normalisedHeight, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect VoxelBusters.Utility.UnityGUI.MENU.GUIMenuBase::GetLayoutRect(UnityEngine.Rect)
extern "C" Rect_t6_51  GUIMenuBase_GetLayoutRect_m8_837 (GUIMenuBase_t8_141 * __this, Rect_t6_51  ____normalisedRect, const MethodInfo* method) IL2CPP_METHOD_ATTR;
