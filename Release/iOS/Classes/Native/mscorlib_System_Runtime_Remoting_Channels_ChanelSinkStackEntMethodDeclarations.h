﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Channels.ChanelSinkStackEntry
struct ChanelSinkStackEntry_t1_876;
// System.Runtime.Remoting.Channels.IChannelSinkBase
struct IChannelSinkBase_t1_867;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Channels.ChanelSinkStackEntry::.ctor(System.Runtime.Remoting.Channels.IChannelSinkBase,System.Object,System.Runtime.Remoting.Channels.ChanelSinkStackEntry)
extern "C" void ChanelSinkStackEntry__ctor_m1_8063 (ChanelSinkStackEntry_t1_876 * __this, Object_t * ___sink, Object_t * ___state, ChanelSinkStackEntry_t1_876 * ___next, const MethodInfo* method) IL2CPP_METHOD_ATTR;
