﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.TypeUnloadedException
struct TypeUnloadedException_t1_1613;
// System.String
struct String_t;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;
// System.Exception
struct Exception_t1_33;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.TypeUnloadedException::.ctor()
extern "C" void TypeUnloadedException__ctor_m1_14701 (TypeUnloadedException_t1_1613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.TypeUnloadedException::.ctor(System.String)
extern "C" void TypeUnloadedException__ctor_m1_14702 (TypeUnloadedException_t1_1613 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.TypeUnloadedException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void TypeUnloadedException__ctor_m1_14703 (TypeUnloadedException_t1_1613 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.TypeUnloadedException::.ctor(System.String,System.Exception)
extern "C" void TypeUnloadedException__ctor_m1_14704 (TypeUnloadedException_t1_1613 * __this, String_t* ___message, Exception_t1_33 * ___innerException, const MethodInfo* method) IL2CPP_METHOD_ATTR;
