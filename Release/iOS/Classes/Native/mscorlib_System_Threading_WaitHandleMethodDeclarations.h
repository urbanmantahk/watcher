﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Threading.WaitHandle
struct WaitHandle_t1_917;
// System.Threading.WaitHandle[]
struct WaitHandleU5BU5D_t1_1737;
// Microsoft.Win32.SafeHandles.SafeWaitHandle
struct SafeWaitHandle_t1_89;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_TimeSpan.h"
#include "mscorlib_System_IntPtr.h"

// System.Void System.Threading.WaitHandle::.ctor()
extern "C" void WaitHandle__ctor_m1_12964 (WaitHandle_t1_917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.WaitHandle::.cctor()
extern "C" void WaitHandle__cctor_m1_12965 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.WaitHandle::System.IDisposable.Dispose()
extern "C" void WaitHandle_System_IDisposable_Dispose_m1_12966 (WaitHandle_t1_917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.WaitHandle::WaitAll_internal(System.Threading.WaitHandle[],System.Int32,System.Boolean)
extern "C" bool WaitHandle_WaitAll_internal_m1_12967 (Object_t * __this /* static, unused */, WaitHandleU5BU5D_t1_1737* ___handles, int32_t ___ms, bool ___exitContext, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.WaitHandle::CheckArray(System.Threading.WaitHandle[],System.Boolean)
extern "C" void WaitHandle_CheckArray_m1_12968 (Object_t * __this /* static, unused */, WaitHandleU5BU5D_t1_1737* ___handles, bool ___waitAll, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.WaitHandle::WaitAll(System.Threading.WaitHandle[])
extern "C" bool WaitHandle_WaitAll_m1_12969 (Object_t * __this /* static, unused */, WaitHandleU5BU5D_t1_1737* ___waitHandles, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.WaitHandle::WaitAll(System.Threading.WaitHandle[],System.Int32,System.Boolean)
extern "C" bool WaitHandle_WaitAll_m1_12970 (Object_t * __this /* static, unused */, WaitHandleU5BU5D_t1_1737* ___waitHandles, int32_t ___millisecondsTimeout, bool ___exitContext, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.WaitHandle::WaitAll(System.Threading.WaitHandle[],System.TimeSpan,System.Boolean)
extern "C" bool WaitHandle_WaitAll_m1_12971 (Object_t * __this /* static, unused */, WaitHandleU5BU5D_t1_1737* ___waitHandles, TimeSpan_t1_368  ___timeout, bool ___exitContext, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Threading.WaitHandle::WaitAny_internal(System.Threading.WaitHandle[],System.Int32,System.Boolean)
extern "C" int32_t WaitHandle_WaitAny_internal_m1_12972 (Object_t * __this /* static, unused */, WaitHandleU5BU5D_t1_1737* ___handles, int32_t ___ms, bool ___exitContext, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Threading.WaitHandle::WaitAny(System.Threading.WaitHandle[])
extern "C" int32_t WaitHandle_WaitAny_m1_12973 (Object_t * __this /* static, unused */, WaitHandleU5BU5D_t1_1737* ___waitHandles, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Threading.WaitHandle::WaitAny(System.Threading.WaitHandle[],System.Int32,System.Boolean)
extern "C" int32_t WaitHandle_WaitAny_m1_12974 (Object_t * __this /* static, unused */, WaitHandleU5BU5D_t1_1737* ___waitHandles, int32_t ___millisecondsTimeout, bool ___exitContext, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Threading.WaitHandle::WaitAny(System.Threading.WaitHandle[],System.TimeSpan)
extern "C" int32_t WaitHandle_WaitAny_m1_12975 (Object_t * __this /* static, unused */, WaitHandleU5BU5D_t1_1737* ___waitHandles, TimeSpan_t1_368  ___timeout, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Threading.WaitHandle::WaitAny(System.Threading.WaitHandle[],System.Int32)
extern "C" int32_t WaitHandle_WaitAny_m1_12976 (Object_t * __this /* static, unused */, WaitHandleU5BU5D_t1_1737* ___waitHandles, int32_t ___millisecondsTimeout, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Threading.WaitHandle::WaitAny(System.Threading.WaitHandle[],System.TimeSpan,System.Boolean)
extern "C" int32_t WaitHandle_WaitAny_m1_12977 (Object_t * __this /* static, unused */, WaitHandleU5BU5D_t1_1737* ___waitHandles, TimeSpan_t1_368  ___timeout, bool ___exitContext, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.WaitHandle::Close()
extern "C" void WaitHandle_Close_m1_12978 (WaitHandle_t1_917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Threading.WaitHandle::get_Handle()
extern "C" IntPtr_t WaitHandle_get_Handle_m1_12979 (WaitHandle_t1_917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.WaitHandle::set_Handle(System.IntPtr)
extern "C" void WaitHandle_set_Handle_m1_12980 (WaitHandle_t1_917 * __this, IntPtr_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.WaitHandle::WaitOne_internal(System.IntPtr,System.Int32,System.Boolean)
extern "C" bool WaitHandle_WaitOne_internal_m1_12981 (WaitHandle_t1_917 * __this, IntPtr_t ___handle, int32_t ___ms, bool ___exitContext, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.WaitHandle::Dispose(System.Boolean)
extern "C" void WaitHandle_Dispose_m1_12982 (WaitHandle_t1_917 * __this, bool ___explicitDisposing, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Microsoft.Win32.SafeHandles.SafeWaitHandle System.Threading.WaitHandle::get_SafeWaitHandle()
extern "C" SafeWaitHandle_t1_89 * WaitHandle_get_SafeWaitHandle_m1_12983 (WaitHandle_t1_917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.WaitHandle::set_SafeWaitHandle(Microsoft.Win32.SafeHandles.SafeWaitHandle)
extern "C" void WaitHandle_set_SafeWaitHandle_m1_12984 (WaitHandle_t1_917 * __this, SafeWaitHandle_t1_89 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.WaitHandle::SignalAndWait(System.Threading.WaitHandle,System.Threading.WaitHandle)
extern "C" bool WaitHandle_SignalAndWait_m1_12985 (Object_t * __this /* static, unused */, WaitHandle_t1_917 * ___toSignal, WaitHandle_t1_917 * ___toWaitOn, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.WaitHandle::SignalAndWait(System.Threading.WaitHandle,System.Threading.WaitHandle,System.Int32,System.Boolean)
extern "C" bool WaitHandle_SignalAndWait_m1_12986 (Object_t * __this /* static, unused */, WaitHandle_t1_917 * ___toSignal, WaitHandle_t1_917 * ___toWaitOn, int32_t ___millisecondsTimeout, bool ___exitContext, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.WaitHandle::SignalAndWait(System.Threading.WaitHandle,System.Threading.WaitHandle,System.TimeSpan,System.Boolean)
extern "C" bool WaitHandle_SignalAndWait_m1_12987 (Object_t * __this /* static, unused */, WaitHandle_t1_917 * ___toSignal, WaitHandle_t1_917 * ___toWaitOn, TimeSpan_t1_368  ___timeout, bool ___exitContext, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.WaitHandle::SignalAndWait_Internal(System.IntPtr,System.IntPtr,System.Int32,System.Boolean)
extern "C" bool WaitHandle_SignalAndWait_Internal_m1_12988 (Object_t * __this /* static, unused */, IntPtr_t ___toSignal, IntPtr_t ___toWaitOn, int32_t ___ms, bool ___exitContext, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.WaitHandle::WaitOne()
extern "C" bool WaitHandle_WaitOne_m1_12989 (WaitHandle_t1_917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.WaitHandle::WaitOne(System.Int32,System.Boolean)
extern "C" bool WaitHandle_WaitOne_m1_12990 (WaitHandle_t1_917 * __this, int32_t ___millisecondsTimeout, bool ___exitContext, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.WaitHandle::WaitOne(System.Int32)
extern "C" bool WaitHandle_WaitOne_m1_12991 (WaitHandle_t1_917 * __this, int32_t ___millisecondsTimeout, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.WaitHandle::WaitOne(System.TimeSpan)
extern "C" bool WaitHandle_WaitOne_m1_12992 (WaitHandle_t1_917 * __this, TimeSpan_t1_368  ___timeout, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.WaitHandle::WaitOne(System.TimeSpan,System.Boolean)
extern "C" bool WaitHandle_WaitOne_m1_12993 (WaitHandle_t1_917 * __this, TimeSpan_t1_368  ___timeout, bool ___exitContext, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.WaitHandle::CheckDisposed()
extern "C" void WaitHandle_CheckDisposed_m1_12994 (WaitHandle_t1_917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.WaitHandle::WaitAll(System.Threading.WaitHandle[],System.Int32)
extern "C" bool WaitHandle_WaitAll_m1_12995 (Object_t * __this /* static, unused */, WaitHandleU5BU5D_t1_1737* ___waitHandles, int32_t ___millisecondsTimeout, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.WaitHandle::WaitAll(System.Threading.WaitHandle[],System.TimeSpan)
extern "C" bool WaitHandle_WaitAll_m1_12996 (Object_t * __this /* static, unused */, WaitHandleU5BU5D_t1_1737* ___waitHandles, TimeSpan_t1_368  ___timeout, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.WaitHandle::Finalize()
extern "C" void WaitHandle_Finalize_m1_12997 (WaitHandle_t1_917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
