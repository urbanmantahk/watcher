﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Policy.UnionCodeGroup
struct UnionCodeGroup_t1_1367;
// System.Security.Policy.IMembershipCondition
struct IMembershipCondition_t1_1340;
// System.Security.Policy.PolicyStatement
struct PolicyStatement_t1_1334;
// System.Security.SecurityElement
struct SecurityElement_t1_242;
// System.Security.Policy.PolicyLevel
struct PolicyLevel_t1_1357;
// System.Security.Policy.CodeGroup
struct CodeGroup_t1_1339;
// System.Security.Policy.Evidence
struct Evidence_t1_398;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Policy.UnionCodeGroup::.ctor(System.Security.Policy.IMembershipCondition,System.Security.Policy.PolicyStatement)
extern "C" void UnionCodeGroup__ctor_m1_11727 (UnionCodeGroup_t1_1367 * __this, Object_t * ___membershipCondition, PolicyStatement_t1_1334 * ___policy, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.UnionCodeGroup::.ctor(System.Security.SecurityElement,System.Security.Policy.PolicyLevel)
extern "C" void UnionCodeGroup__ctor_m1_11728 (UnionCodeGroup_t1_1367 * __this, SecurityElement_t1_242 * ___e, PolicyLevel_t1_1357 * ___level, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.CodeGroup System.Security.Policy.UnionCodeGroup::Copy()
extern "C" CodeGroup_t1_1339 * UnionCodeGroup_Copy_m1_11729 (UnionCodeGroup_t1_1367 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.CodeGroup System.Security.Policy.UnionCodeGroup::Copy(System.Boolean)
extern "C" CodeGroup_t1_1339 * UnionCodeGroup_Copy_m1_11730 (UnionCodeGroup_t1_1367 * __this, bool ___childs, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.PolicyStatement System.Security.Policy.UnionCodeGroup::Resolve(System.Security.Policy.Evidence)
extern "C" PolicyStatement_t1_1334 * UnionCodeGroup_Resolve_m1_11731 (UnionCodeGroup_t1_1367 * __this, Evidence_t1_398 * ___evidence, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.CodeGroup System.Security.Policy.UnionCodeGroup::ResolveMatchingCodeGroups(System.Security.Policy.Evidence)
extern "C" CodeGroup_t1_1339 * UnionCodeGroup_ResolveMatchingCodeGroups_m1_11732 (UnionCodeGroup_t1_1367 * __this, Evidence_t1_398 * ___evidence, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Policy.UnionCodeGroup::get_MergeLogic()
extern "C" String_t* UnionCodeGroup_get_MergeLogic_m1_11733 (UnionCodeGroup_t1_1367 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
