﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.AccessControl.RawAcl
struct RawAcl_t1_1170;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Security.AccessControl.GenericAce
struct GenericAce_t1_1145;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.AccessControl.RawAcl::.ctor(System.Byte,System.Int32)
extern "C" void RawAcl__ctor_m1_10039 (RawAcl_t1_1170 * __this, uint8_t ___revision, int32_t ___capacity, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.RawAcl::.ctor(System.Byte[],System.Int32)
extern "C" void RawAcl__ctor_m1_10040 (RawAcl_t1_1170 * __this, ByteU5BU5D_t1_109* ___binaryForm, int32_t ___offset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.AccessControl.RawAcl::get_BinaryLength()
extern "C" int32_t RawAcl_get_BinaryLength_m1_10041 (RawAcl_t1_1170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.AccessControl.RawAcl::get_Count()
extern "C" int32_t RawAcl_get_Count_m1_10042 (RawAcl_t1_1170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.AccessControl.GenericAce System.Security.AccessControl.RawAcl::get_Item(System.Int32)
extern "C" GenericAce_t1_1145 * RawAcl_get_Item_m1_10043 (RawAcl_t1_1170 * __this, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.RawAcl::set_Item(System.Int32,System.Security.AccessControl.GenericAce)
extern "C" void RawAcl_set_Item_m1_10044 (RawAcl_t1_1170 * __this, int32_t ___index, GenericAce_t1_1145 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte System.Security.AccessControl.RawAcl::get_Revision()
extern "C" uint8_t RawAcl_get_Revision_m1_10045 (RawAcl_t1_1170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.RawAcl::GetBinaryForm(System.Byte[],System.Int32)
extern "C" void RawAcl_GetBinaryForm_m1_10046 (RawAcl_t1_1170 * __this, ByteU5BU5D_t1_109* ___binaryForm, int32_t ___offset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.RawAcl::InsertAce(System.Int32,System.Security.AccessControl.GenericAce)
extern "C" void RawAcl_InsertAce_m1_10047 (RawAcl_t1_1170 * __this, int32_t ___index, GenericAce_t1_1145 * ___ace, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.RawAcl::RemoveAce(System.Int32)
extern "C" void RawAcl_RemoveAce_m1_10048 (RawAcl_t1_1170 * __this, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
