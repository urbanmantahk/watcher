﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_EnvironmentVariableTarget.h"

// System.EnvironmentVariableTarget
struct  EnvironmentVariableTarget_t1_1546 
{
	// System.Int32 System.EnvironmentVariableTarget::value__
	int32_t ___value___1;
};
