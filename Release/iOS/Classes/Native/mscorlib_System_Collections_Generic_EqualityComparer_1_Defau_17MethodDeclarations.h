﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>
struct DefaultComparer_t1_2475;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>::.ctor()
extern "C" void DefaultComparer__ctor_m1_22441_gshared (DefaultComparer_t1_2475 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1_22441(__this, method) (( void (*) (DefaultComparer_t1_2475 *, const MethodInfo*))DefaultComparer__ctor_m1_22441_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m1_22442_gshared (DefaultComparer_t1_2475 * __this, RaycastResult_t7_31  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m1_22442(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t1_2475 *, RaycastResult_t7_31 , const MethodInfo*))DefaultComparer_GetHashCode_m1_22442_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m1_22443_gshared (DefaultComparer_t1_2475 * __this, RaycastResult_t7_31  ___x, RaycastResult_t7_31  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m1_22443(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t1_2475 *, RaycastResult_t7_31 , RaycastResult_t7_31 , const MethodInfo*))DefaultComparer_Equals_m1_22443_gshared)(__this, ___x, ___y, method)
