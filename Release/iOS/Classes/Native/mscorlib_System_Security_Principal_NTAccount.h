﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Security_Principal_IdentityReference.h"

// System.Security.Principal.NTAccount
struct  NTAccount_t1_1377  : public IdentityReference_t1_1120
{
	// System.String System.Security.Principal.NTAccount::_value
	String_t* ____value_0;
};
