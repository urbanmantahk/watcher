﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "System_Xml_Mono_Xml_Schema_XsdString.h"

// Mono.Xml.Schema.XsdBase64Binary
struct  XsdBase64Binary_t4_37  : public XsdString_t4_7
{
};
struct XsdBase64Binary_t4_37_StaticFields{
	// System.String Mono.Xml.Schema.XsdBase64Binary::ALPHABET
	String_t* ___ALPHABET_61;
	// System.Byte[] Mono.Xml.Schema.XsdBase64Binary::decodeTable
	ByteU5BU5D_t1_109* ___decodeTable_62;
};
