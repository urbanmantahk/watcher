﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.RSAOAEPKeyExchangeFormatter
struct RSAOAEPKeyExchangeFormatter_t1_1237;
// System.Security.Cryptography.AsymmetricAlgorithm
struct AsymmetricAlgorithm_t1_228;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.String
struct String_t;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t1_143;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Cryptography.RSAOAEPKeyExchangeFormatter::.ctor()
extern "C" void RSAOAEPKeyExchangeFormatter__ctor_m1_10558 (RSAOAEPKeyExchangeFormatter_t1_1237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RSAOAEPKeyExchangeFormatter::.ctor(System.Security.Cryptography.AsymmetricAlgorithm)
extern "C" void RSAOAEPKeyExchangeFormatter__ctor_m1_10559 (RSAOAEPKeyExchangeFormatter_t1_1237 * __this, AsymmetricAlgorithm_t1_228 * ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.RSAOAEPKeyExchangeFormatter::get_Parameter()
extern "C" ByteU5BU5D_t1_109* RSAOAEPKeyExchangeFormatter_get_Parameter_m1_10560 (RSAOAEPKeyExchangeFormatter_t1_1237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RSAOAEPKeyExchangeFormatter::set_Parameter(System.Byte[])
extern "C" void RSAOAEPKeyExchangeFormatter_set_Parameter_m1_10561 (RSAOAEPKeyExchangeFormatter_t1_1237 * __this, ByteU5BU5D_t1_109* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Cryptography.RSAOAEPKeyExchangeFormatter::get_Parameters()
extern "C" String_t* RSAOAEPKeyExchangeFormatter_get_Parameters_m1_10562 (RSAOAEPKeyExchangeFormatter_t1_1237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.RandomNumberGenerator System.Security.Cryptography.RSAOAEPKeyExchangeFormatter::get_Rng()
extern "C" RandomNumberGenerator_t1_143 * RSAOAEPKeyExchangeFormatter_get_Rng_m1_10563 (RSAOAEPKeyExchangeFormatter_t1_1237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RSAOAEPKeyExchangeFormatter::set_Rng(System.Security.Cryptography.RandomNumberGenerator)
extern "C" void RSAOAEPKeyExchangeFormatter_set_Rng_m1_10564 (RSAOAEPKeyExchangeFormatter_t1_1237 * __this, RandomNumberGenerator_t1_143 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.RSAOAEPKeyExchangeFormatter::CreateKeyExchange(System.Byte[])
extern "C" ByteU5BU5D_t1_109* RSAOAEPKeyExchangeFormatter_CreateKeyExchange_m1_10565 (RSAOAEPKeyExchangeFormatter_t1_1237 * __this, ByteU5BU5D_t1_109* ___rgbData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.RSAOAEPKeyExchangeFormatter::CreateKeyExchange(System.Byte[],System.Type)
extern "C" ByteU5BU5D_t1_109* RSAOAEPKeyExchangeFormatter_CreateKeyExchange_m1_10566 (RSAOAEPKeyExchangeFormatter_t1_1237 * __this, ByteU5BU5D_t1_109* ___rgbData, Type_t * ___symAlgType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RSAOAEPKeyExchangeFormatter::SetKey(System.Security.Cryptography.AsymmetricAlgorithm)
extern "C" void RSAOAEPKeyExchangeFormatter_SetKey_m1_10567 (RSAOAEPKeyExchangeFormatter_t1_1237 * __this, AsymmetricAlgorithm_t1_228 * ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
