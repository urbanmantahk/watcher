﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VoxelBusters.NativePlugins.NotificationServiceSettings/iOSSettings
struct iOSSettings_t8_268;
// VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings
struct AndroidSettings_t8_269;

#include "mscorlib_System_Object.h"

// VoxelBusters.NativePlugins.NotificationServiceSettings
struct  NotificationServiceSettings_t8_270  : public Object_t
{
	// VoxelBusters.NativePlugins.NotificationServiceSettings/iOSSettings VoxelBusters.NativePlugins.NotificationServiceSettings::m_iOS
	iOSSettings_t8_268 * ___m_iOS_0;
	// VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings VoxelBusters.NativePlugins.NotificationServiceSettings::m_android
	AndroidSettings_t8_269 * ___m_android_1;
};
