﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.BillingTransaction
struct BillingTransaction_t8_211;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eBillingTransac.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eBillingTransac_0.h"

// System.Void VoxelBusters.NativePlugins.BillingTransaction::.ctor()
extern "C" void BillingTransaction__ctor_m8_1200 (BillingTransaction_t8_211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.BillingTransaction::get_ProductIdentifier()
extern "C" String_t* BillingTransaction_get_ProductIdentifier_m8_1201 (BillingTransaction_t8_211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.BillingTransaction::set_ProductIdentifier(System.String)
extern "C" void BillingTransaction_set_ProductIdentifier_m8_1202 (BillingTransaction_t8_211 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime VoxelBusters.NativePlugins.BillingTransaction::get_TransactionDateUTC()
extern "C" DateTime_t1_150  BillingTransaction_get_TransactionDateUTC_m8_1203 (BillingTransaction_t8_211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.BillingTransaction::set_TransactionDateUTC(System.DateTime)
extern "C" void BillingTransaction_set_TransactionDateUTC_m8_1204 (BillingTransaction_t8_211 * __this, DateTime_t1_150  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime VoxelBusters.NativePlugins.BillingTransaction::get_TransactionDateLocal()
extern "C" DateTime_t1_150  BillingTransaction_get_TransactionDateLocal_m8_1205 (BillingTransaction_t8_211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.BillingTransaction::set_TransactionDateLocal(System.DateTime)
extern "C" void BillingTransaction_set_TransactionDateLocal_m8_1206 (BillingTransaction_t8_211 * __this, DateTime_t1_150  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.BillingTransaction::get_TransactionIdentifier()
extern "C" String_t* BillingTransaction_get_TransactionIdentifier_m8_1207 (BillingTransaction_t8_211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.BillingTransaction::set_TransactionIdentifier(System.String)
extern "C" void BillingTransaction_set_TransactionIdentifier_m8_1208 (BillingTransaction_t8_211 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.BillingTransaction::get_TransactionReceipt()
extern "C" String_t* BillingTransaction_get_TransactionReceipt_m8_1209 (BillingTransaction_t8_211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.BillingTransaction::set_TransactionReceipt(System.String)
extern "C" void BillingTransaction_set_TransactionReceipt_m8_1210 (BillingTransaction_t8_211 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.NativePlugins.eBillingTransactionState VoxelBusters.NativePlugins.BillingTransaction::get_TransactionState()
extern "C" int32_t BillingTransaction_get_TransactionState_m8_1211 (BillingTransaction_t8_211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.BillingTransaction::set_TransactionState(VoxelBusters.NativePlugins.eBillingTransactionState)
extern "C" void BillingTransaction_set_TransactionState_m8_1212 (BillingTransaction_t8_211 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.NativePlugins.eBillingTransactionVerificationState VoxelBusters.NativePlugins.BillingTransaction::get_VerificationState()
extern "C" int32_t BillingTransaction_get_VerificationState_m8_1213 (BillingTransaction_t8_211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.BillingTransaction::set_VerificationState(VoxelBusters.NativePlugins.eBillingTransactionVerificationState)
extern "C" void BillingTransaction_set_VerificationState_m8_1214 (BillingTransaction_t8_211 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.BillingTransaction::get_Error()
extern "C" String_t* BillingTransaction_get_Error_m8_1215 (BillingTransaction_t8_211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.BillingTransaction::set_Error(System.String)
extern "C" void BillingTransaction_set_Error_m8_1216 (BillingTransaction_t8_211 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.BillingTransaction::get_RawPurchaseData()
extern "C" String_t* BillingTransaction_get_RawPurchaseData_m8_1217 (BillingTransaction_t8_211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.BillingTransaction::set_RawPurchaseData(System.String)
extern "C" void BillingTransaction_set_RawPurchaseData_m8_1218 (BillingTransaction_t8_211 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.BillingTransaction::UpdateVerificationState(VoxelBusters.NativePlugins.eBillingTransactionVerificationState)
extern "C" void BillingTransaction_UpdateVerificationState_m8_1219 (BillingTransaction_t8_211 * __this, int32_t ____newState, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.BillingTransaction::ToString()
extern "C" String_t* BillingTransaction_ToString_m8_1220 (BillingTransaction_t8_211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
