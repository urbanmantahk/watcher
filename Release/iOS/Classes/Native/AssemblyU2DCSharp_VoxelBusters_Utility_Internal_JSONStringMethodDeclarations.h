﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_Internal_JSONString.h"

// System.Void VoxelBusters.Utility.Internal.JSONString::.ctor(System.String)
extern "C" void JSONString__ctor_m8_272 (JSONString_t8_55 * __this, String_t* ____JSONString, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.Utility.Internal.JSONString::get_Value()
extern "C" String_t* JSONString_get_Value_m8_273 (JSONString_t8_55 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.Internal.JSONString::set_Value(System.String)
extern "C" void JSONString_set_Value_m8_274 (JSONString_t8_55 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.Utility.Internal.JSONString::get_IsNullOrEmpty()
extern "C" bool JSONString_get_IsNullOrEmpty_m8_275 (JSONString_t8_55 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.Internal.JSONString::set_IsNullOrEmpty(System.Boolean)
extern "C" void JSONString_set_IsNullOrEmpty_m8_276 (JSONString_t8_55 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 VoxelBusters.Utility.Internal.JSONString::get_Length()
extern "C" int32_t JSONString_get_Length_m8_277 (JSONString_t8_55 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.Internal.JSONString::set_Length(System.Int32)
extern "C" void JSONString_set_Length_m8_278 (JSONString_t8_55 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char VoxelBusters.Utility.Internal.JSONString::get_Item(System.Int32)
extern "C" uint16_t JSONString_get_Item_m8_279 (JSONString_t8_55 * __this, int32_t ____index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void JSONString_t8_55_marshal(const JSONString_t8_55& unmarshaled, JSONString_t8_55_marshaled& marshaled);
extern "C" void JSONString_t8_55_marshal_back(const JSONString_t8_55_marshaled& marshaled, JSONString_t8_55& unmarshaled);
extern "C" void JSONString_t8_55_marshal_cleanup(JSONString_t8_55_marshaled& marshaled);
