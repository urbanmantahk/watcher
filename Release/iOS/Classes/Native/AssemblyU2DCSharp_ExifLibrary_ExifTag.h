﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTag.h"

// ExifLibrary.ExifTag
struct  ExifTag_t8_132 
{
	// System.Int32 ExifLibrary.ExifTag::value__
	int32_t ___value___1;
};
