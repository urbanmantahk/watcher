﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.MethodBody
struct MethodBody_t1_610;
// System.Collections.Generic.IList`1<System.Reflection.ExceptionHandlingClause>
struct IList_1_t1_1691;
// System.Collections.Generic.IList`1<System.Reflection.LocalVariableInfo>
struct IList_1_t1_1692;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Reflection.MethodBody::.ctor()
extern "C" void MethodBody__ctor_m1_6929 (MethodBody_t1_610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<System.Reflection.ExceptionHandlingClause> System.Reflection.MethodBody::get_ExceptionHandlingClauses()
extern "C" Object_t* MethodBody_get_ExceptionHandlingClauses_m1_6930 (MethodBody_t1_610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<System.Reflection.LocalVariableInfo> System.Reflection.MethodBody::get_LocalVariables()
extern "C" Object_t* MethodBody_get_LocalVariables_m1_6931 (MethodBody_t1_610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.MethodBody::get_InitLocals()
extern "C" bool MethodBody_get_InitLocals_m1_6932 (MethodBody_t1_610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.MethodBody::get_LocalSignatureMetadataToken()
extern "C" int32_t MethodBody_get_LocalSignatureMetadataToken_m1_6933 (MethodBody_t1_610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.MethodBody::get_MaxStackSize()
extern "C" int32_t MethodBody_get_MaxStackSize_m1_6934 (MethodBody_t1_610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Reflection.MethodBody::GetILAsByteArray()
extern "C" ByteU5BU5D_t1_109* MethodBody_GetILAsByteArray_m1_6935 (MethodBody_t1_610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
