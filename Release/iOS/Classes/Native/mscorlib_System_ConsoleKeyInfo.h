﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType.h"
#include "mscorlib_System_ConsoleKeyInfo.h"
#include "mscorlib_System_ConsoleKey.h"
#include "mscorlib_System_ConsoleModifiers.h"

// System.ConsoleKeyInfo
struct  ConsoleKeyInfo_t1_1516 
{
	// System.ConsoleKey System.ConsoleKeyInfo::key
	int32_t ___key_1;
	// System.Char System.ConsoleKeyInfo::keychar
	uint16_t ___keychar_2;
	// System.ConsoleModifiers System.ConsoleKeyInfo::modifiers
	int32_t ___modifiers_3;
};
struct ConsoleKeyInfo_t1_1516_StaticFields{
	// System.ConsoleKeyInfo System.ConsoleKeyInfo::Empty
	ConsoleKeyInfo_t1_1516  ___Empty_0;
};
// Native definition for marshalling of: System.ConsoleKeyInfo
struct ConsoleKeyInfo_t1_1516_marshaled
{
	int32_t ___key_1;
	char ___keychar_2;
	int32_t ___modifiers_3;
};
