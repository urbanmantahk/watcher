﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VoxelBusters.NativePlugins.RateMyApp
struct RateMyApp_t8_307;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// VoxelBusters.NativePlugins.Utility
struct  Utility_t8_306  : public MonoBehaviour_t6_91
{
	// VoxelBusters.NativePlugins.RateMyApp VoxelBusters.NativePlugins.Utility::m_rateMyApp
	RateMyApp_t8_307 * ___m_rateMyApp_2;
};
