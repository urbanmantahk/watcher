﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderProperty>
struct List_1_t1_1900;
// VoxelBusters.Utility.ShaderUtility/ShaderProperty
struct ShaderProperty_t8_27;

#include "mscorlib_System_ValueType.h"

// System.Collections.Generic.List`1/Enumerator<VoxelBusters.Utility.ShaderUtility/ShaderProperty>
struct  Enumerator_t1_2649 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t1_1900 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	ShaderProperty_t8_27 * ___current_3;
};
