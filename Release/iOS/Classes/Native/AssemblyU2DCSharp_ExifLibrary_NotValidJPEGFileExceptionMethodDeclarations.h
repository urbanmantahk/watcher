﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExifLibrary.NotValidJPEGFileException
struct NotValidJPEGFileException_t8_134;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void ExifLibrary.NotValidJPEGFileException::.ctor()
extern "C" void NotValidJPEGFileException__ctor_m8_635 (NotValidJPEGFileException_t8_134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.NotValidJPEGFileException::.ctor(System.String)
extern "C" void NotValidJPEGFileException__ctor_m8_636 (NotValidJPEGFileException_t8_134 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
