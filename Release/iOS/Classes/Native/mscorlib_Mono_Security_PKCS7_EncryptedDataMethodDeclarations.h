﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.PKCS7/EncryptedData
struct EncryptedData_t1_223;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// Mono.Security.ASN1
struct ASN1_t1_149;
// Mono.Security.PKCS7/ContentInfo
struct ContentInfo_t1_222;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Security.PKCS7/EncryptedData::.ctor()
extern "C" void EncryptedData__ctor_m1_2485 (EncryptedData_t1_223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.PKCS7/EncryptedData::.ctor(System.Byte[])
extern "C" void EncryptedData__ctor_m1_2486 (EncryptedData_t1_223 * __this, ByteU5BU5D_t1_109* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.PKCS7/EncryptedData::.ctor(Mono.Security.ASN1)
extern "C" void EncryptedData__ctor_m1_2487 (EncryptedData_t1_223 * __this, ASN1_t1_149 * ___asn1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.ASN1 Mono.Security.PKCS7/EncryptedData::get_ASN1()
extern "C" ASN1_t1_149 * EncryptedData_get_ASN1_m1_2488 (EncryptedData_t1_223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.PKCS7/ContentInfo Mono.Security.PKCS7/EncryptedData::get_ContentInfo()
extern "C" ContentInfo_t1_222 * EncryptedData_get_ContentInfo_m1_2489 (EncryptedData_t1_223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.PKCS7/ContentInfo Mono.Security.PKCS7/EncryptedData::get_EncryptionAlgorithm()
extern "C" ContentInfo_t1_222 * EncryptedData_get_EncryptionAlgorithm_m1_2490 (EncryptedData_t1_223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.PKCS7/EncryptedData::get_EncryptedContent()
extern "C" ByteU5BU5D_t1_109* EncryptedData_get_EncryptedContent_m1_2491 (EncryptedData_t1_223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte Mono.Security.PKCS7/EncryptedData::get_Version()
extern "C" uint8_t EncryptedData_get_Version_m1_2492 (EncryptedData_t1_223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.PKCS7/EncryptedData::set_Version(System.Byte)
extern "C" void EncryptedData_set_Version_m1_2493 (EncryptedData_t1_223 * __this, uint8_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.ASN1 Mono.Security.PKCS7/EncryptedData::GetASN1()
extern "C" ASN1_t1_149 * EncryptedData_GetASN1_m1_2494 (EncryptedData_t1_223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.PKCS7/EncryptedData::GetBytes()
extern "C" ByteU5BU5D_t1_109* EncryptedData_GetBytes_m1_2495 (EncryptedData_t1_223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
