﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Int32[]
struct Int32U5BU5D_t1_275;
// System.Object
struct Object_t;
// System.Type
struct Type_t;
// System.IFormatProvider
struct IFormatProvider_t1_455;
// System.String
struct String_t;
// System.Globalization.NumberFormatInfo
struct NumberFormatInfo_t1_361;
// System.Char[]
struct CharU5BU5D_t1_16;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Decimal.h"
#include "mscorlib_System_DateTime.h"
#include "mscorlib_System_MidpointRounding.h"
#include "mscorlib_System_Globalization_NumberStyles.h"
#include "mscorlib_System_TypeCode.h"

// System.Void System.Decimal::.ctor(System.Int32,System.Int32,System.Int32,System.Boolean,System.Byte)
extern "C" void Decimal__ctor_m1_676 (Decimal_t1_19 * __this, int32_t ___lo, int32_t ___mid, int32_t ___hi, bool ___isNegative, uint8_t ___scale, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Decimal::.ctor(System.Int32)
extern "C" void Decimal__ctor_m1_677 (Decimal_t1_19 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Decimal::.ctor(System.UInt32)
extern "C" void Decimal__ctor_m1_678 (Decimal_t1_19 * __this, uint32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Decimal::.ctor(System.Int64)
extern "C" void Decimal__ctor_m1_679 (Decimal_t1_19 * __this, int64_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Decimal::.ctor(System.UInt64)
extern "C" void Decimal__ctor_m1_680 (Decimal_t1_19 * __this, uint64_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Decimal::.ctor(System.Single)
extern "C" void Decimal__ctor_m1_681 (Decimal_t1_19 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Decimal::.ctor(System.Double)
extern "C" void Decimal__ctor_m1_682 (Decimal_t1_19 * __this, double ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Decimal::.ctor(System.Int32[])
extern "C" void Decimal__ctor_m1_683 (Decimal_t1_19 * __this, Int32U5BU5D_t1_275* ___bits, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Decimal::.cctor()
extern "C" void Decimal__cctor_m1_684 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Decimal::System.IConvertible.ToType(System.Type,System.IFormatProvider)
extern "C" Object_t * Decimal_System_IConvertible_ToType_m1_685 (Decimal_t1_19 * __this, Type_t * ___targetType, Object_t * ___provider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Decimal::System.IConvertible.ToBoolean(System.IFormatProvider)
extern "C" bool Decimal_System_IConvertible_ToBoolean_m1_686 (Decimal_t1_19 * __this, Object_t * ___provider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte System.Decimal::System.IConvertible.ToByte(System.IFormatProvider)
extern "C" uint8_t Decimal_System_IConvertible_ToByte_m1_687 (Decimal_t1_19 * __this, Object_t * ___provider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char System.Decimal::System.IConvertible.ToChar(System.IFormatProvider)
extern "C" uint16_t Decimal_System_IConvertible_ToChar_m1_688 (Decimal_t1_19 * __this, Object_t * ___provider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Decimal::System.IConvertible.ToDateTime(System.IFormatProvider)
extern "C" DateTime_t1_150  Decimal_System_IConvertible_ToDateTime_m1_689 (Decimal_t1_19 * __this, Object_t * ___provider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Decimal::System.IConvertible.ToDecimal(System.IFormatProvider)
extern "C" Decimal_t1_19  Decimal_System_IConvertible_ToDecimal_m1_690 (Decimal_t1_19 * __this, Object_t * ___provider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Decimal::System.IConvertible.ToDouble(System.IFormatProvider)
extern "C" double Decimal_System_IConvertible_ToDouble_m1_691 (Decimal_t1_19 * __this, Object_t * ___provider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 System.Decimal::System.IConvertible.ToInt16(System.IFormatProvider)
extern "C" int16_t Decimal_System_IConvertible_ToInt16_m1_692 (Decimal_t1_19 * __this, Object_t * ___provider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Decimal::System.IConvertible.ToInt32(System.IFormatProvider)
extern "C" int32_t Decimal_System_IConvertible_ToInt32_m1_693 (Decimal_t1_19 * __this, Object_t * ___provider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Decimal::System.IConvertible.ToInt64(System.IFormatProvider)
extern "C" int64_t Decimal_System_IConvertible_ToInt64_m1_694 (Decimal_t1_19 * __this, Object_t * ___provider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.SByte System.Decimal::System.IConvertible.ToSByte(System.IFormatProvider)
extern "C" int8_t Decimal_System_IConvertible_ToSByte_m1_695 (Decimal_t1_19 * __this, Object_t * ___provider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single System.Decimal::System.IConvertible.ToSingle(System.IFormatProvider)
extern "C" float Decimal_System_IConvertible_ToSingle_m1_696 (Decimal_t1_19 * __this, Object_t * ___provider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 System.Decimal::System.IConvertible.ToUInt16(System.IFormatProvider)
extern "C" uint16_t Decimal_System_IConvertible_ToUInt16_m1_697 (Decimal_t1_19 * __this, Object_t * ___provider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 System.Decimal::System.IConvertible.ToUInt32(System.IFormatProvider)
extern "C" uint32_t Decimal_System_IConvertible_ToUInt32_m1_698 (Decimal_t1_19 * __this, Object_t * ___provider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 System.Decimal::System.IConvertible.ToUInt64(System.IFormatProvider)
extern "C" uint64_t Decimal_System_IConvertible_ToUInt64_m1_699 (Decimal_t1_19 * __this, Object_t * ___provider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Decimal::FromOACurrency(System.Int64)
extern "C" Decimal_t1_19  Decimal_FromOACurrency_m1_700 (Object_t * __this /* static, unused */, int64_t ___cy, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] System.Decimal::GetBits(System.Decimal)
extern "C" Int32U5BU5D_t1_275* Decimal_GetBits_m1_701 (Object_t * __this /* static, unused */, Decimal_t1_19  ___d, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Decimal::Negate(System.Decimal)
extern "C" Decimal_t1_19  Decimal_Negate_m1_702 (Object_t * __this /* static, unused */, Decimal_t1_19  ___d, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Decimal::Add(System.Decimal,System.Decimal)
extern "C" Decimal_t1_19  Decimal_Add_m1_703 (Object_t * __this /* static, unused */, Decimal_t1_19  ___d1, Decimal_t1_19  ___d2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Decimal::Subtract(System.Decimal,System.Decimal)
extern "C" Decimal_t1_19  Decimal_Subtract_m1_704 (Object_t * __this /* static, unused */, Decimal_t1_19  ___d1, Decimal_t1_19  ___d2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Decimal::GetHashCode()
extern "C" int32_t Decimal_GetHashCode_m1_705 (Decimal_t1_19 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 System.Decimal::u64(System.Decimal)
extern "C" uint64_t Decimal_u64_m1_706 (Object_t * __this /* static, unused */, Decimal_t1_19  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Decimal::s64(System.Decimal)
extern "C" int64_t Decimal_s64_m1_707 (Object_t * __this /* static, unused */, Decimal_t1_19  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Decimal::Equals(System.Decimal,System.Decimal)
extern "C" bool Decimal_Equals_m1_708 (Object_t * __this /* static, unused */, Decimal_t1_19  ___d1, Decimal_t1_19  ___d2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Decimal::Equals(System.Object)
extern "C" bool Decimal_Equals_m1_709 (Decimal_t1_19 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Decimal::IsZero()
extern "C" bool Decimal_IsZero_m1_710 (Decimal_t1_19 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Decimal::IsNegative()
extern "C" bool Decimal_IsNegative_m1_711 (Decimal_t1_19 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Decimal::Floor(System.Decimal)
extern "C" Decimal_t1_19  Decimal_Floor_m1_712 (Object_t * __this /* static, unused */, Decimal_t1_19  ___d, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Decimal::Truncate(System.Decimal)
extern "C" Decimal_t1_19  Decimal_Truncate_m1_713 (Object_t * __this /* static, unused */, Decimal_t1_19  ___d, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Decimal::Round(System.Decimal,System.Int32)
extern "C" Decimal_t1_19  Decimal_Round_m1_714 (Object_t * __this /* static, unused */, Decimal_t1_19  ___d, int32_t ___decimals, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Decimal::Round(System.Decimal,System.Int32,System.MidpointRounding)
extern "C" Decimal_t1_19  Decimal_Round_m1_715 (Object_t * __this /* static, unused */, Decimal_t1_19  ___d, int32_t ___decimals, int32_t ___mode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Decimal::Round(System.Decimal)
extern "C" Decimal_t1_19  Decimal_Round_m1_716 (Object_t * __this /* static, unused */, Decimal_t1_19  ___d, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Decimal::Round(System.Decimal,System.MidpointRounding)
extern "C" Decimal_t1_19  Decimal_Round_m1_717 (Object_t * __this /* static, unused */, Decimal_t1_19  ___d, int32_t ___mode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Decimal::Multiply(System.Decimal,System.Decimal)
extern "C" Decimal_t1_19  Decimal_Multiply_m1_718 (Object_t * __this /* static, unused */, Decimal_t1_19  ___d1, Decimal_t1_19  ___d2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Decimal::Divide(System.Decimal,System.Decimal)
extern "C" Decimal_t1_19  Decimal_Divide_m1_719 (Object_t * __this /* static, unused */, Decimal_t1_19  ___d1, Decimal_t1_19  ___d2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Decimal::Remainder(System.Decimal,System.Decimal)
extern "C" Decimal_t1_19  Decimal_Remainder_m1_720 (Object_t * __this /* static, unused */, Decimal_t1_19  ___d1, Decimal_t1_19  ___d2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Decimal::Compare(System.Decimal,System.Decimal)
extern "C" int32_t Decimal_Compare_m1_721 (Object_t * __this /* static, unused */, Decimal_t1_19  ___d1, Decimal_t1_19  ___d2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Decimal::CompareTo(System.Object)
extern "C" int32_t Decimal_CompareTo_m1_722 (Decimal_t1_19 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Decimal::CompareTo(System.Decimal)
extern "C" int32_t Decimal_CompareTo_m1_723 (Decimal_t1_19 * __this, Decimal_t1_19  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Decimal::Equals(System.Decimal)
extern "C" bool Decimal_Equals_m1_724 (Decimal_t1_19 * __this, Decimal_t1_19  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Decimal::Ceiling(System.Decimal)
extern "C" Decimal_t1_19  Decimal_Ceiling_m1_725 (Object_t * __this /* static, unused */, Decimal_t1_19  ___d, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Decimal::Parse(System.String)
extern "C" Decimal_t1_19  Decimal_Parse_m1_726 (Object_t * __this /* static, unused */, String_t* ___s, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Decimal::Parse(System.String,System.Globalization.NumberStyles)
extern "C" Decimal_t1_19  Decimal_Parse_m1_727 (Object_t * __this /* static, unused */, String_t* ___s, int32_t ___style, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Decimal::Parse(System.String,System.IFormatProvider)
extern "C" Decimal_t1_19  Decimal_Parse_m1_728 (Object_t * __this /* static, unused */, String_t* ___s, Object_t * ___provider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Decimal::ThrowAtPos(System.Int32)
extern "C" void Decimal_ThrowAtPos_m1_729 (Object_t * __this /* static, unused */, int32_t ___pos, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Decimal::ThrowInvalidExp()
extern "C" void Decimal_ThrowInvalidExp_m1_730 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Decimal::stripStyles(System.String,System.Globalization.NumberStyles,System.Globalization.NumberFormatInfo,System.Int32&,System.Boolean&,System.Boolean&,System.Int32&,System.Boolean)
extern "C" String_t* Decimal_stripStyles_m1_731 (Object_t * __this /* static, unused */, String_t* ___s, int32_t ___style, NumberFormatInfo_t1_361 * ___nfi, int32_t* ___decPos, bool* ___isNegative, bool* ___expFlag, int32_t* ___exp, bool ___throwex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Decimal::Parse(System.String,System.Globalization.NumberStyles,System.IFormatProvider)
extern "C" Decimal_t1_19  Decimal_Parse_m1_732 (Object_t * __this /* static, unused */, String_t* ___s, int32_t ___style, Object_t * ___provider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Decimal::TryParse(System.String,System.Decimal&)
extern "C" bool Decimal_TryParse_m1_733 (Object_t * __this /* static, unused */, String_t* ___s, Decimal_t1_19 * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Decimal::TryParse(System.String,System.Globalization.NumberStyles,System.IFormatProvider,System.Decimal&)
extern "C" bool Decimal_TryParse_m1_734 (Object_t * __this /* static, unused */, String_t* ___s, int32_t ___style, Object_t * ___provider, Decimal_t1_19 * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Decimal::PerformParse(System.String,System.Globalization.NumberStyles,System.IFormatProvider,System.Decimal&,System.Boolean)
extern "C" bool Decimal_PerformParse_m1_735 (Object_t * __this /* static, unused */, String_t* ___s, int32_t ___style, Object_t * ___provider, Decimal_t1_19 * ___res, bool ___throwex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.TypeCode System.Decimal::GetTypeCode()
extern "C" int32_t Decimal_GetTypeCode_m1_736 (Decimal_t1_19 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte System.Decimal::ToByte(System.Decimal)
extern "C" uint8_t Decimal_ToByte_m1_737 (Object_t * __this /* static, unused */, Decimal_t1_19  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Decimal::ToDouble(System.Decimal)
extern "C" double Decimal_ToDouble_m1_738 (Object_t * __this /* static, unused */, Decimal_t1_19  ___d, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 System.Decimal::ToInt16(System.Decimal)
extern "C" int16_t Decimal_ToInt16_m1_739 (Object_t * __this /* static, unused */, Decimal_t1_19  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Decimal::ToInt32(System.Decimal)
extern "C" int32_t Decimal_ToInt32_m1_740 (Object_t * __this /* static, unused */, Decimal_t1_19  ___d, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Decimal::ToInt64(System.Decimal)
extern "C" int64_t Decimal_ToInt64_m1_741 (Object_t * __this /* static, unused */, Decimal_t1_19  ___d, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Decimal::ToOACurrency(System.Decimal)
extern "C" int64_t Decimal_ToOACurrency_m1_742 (Object_t * __this /* static, unused */, Decimal_t1_19  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.SByte System.Decimal::ToSByte(System.Decimal)
extern "C" int8_t Decimal_ToSByte_m1_743 (Object_t * __this /* static, unused */, Decimal_t1_19  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single System.Decimal::ToSingle(System.Decimal)
extern "C" float Decimal_ToSingle_m1_744 (Object_t * __this /* static, unused */, Decimal_t1_19  ___d, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 System.Decimal::ToUInt16(System.Decimal)
extern "C" uint16_t Decimal_ToUInt16_m1_745 (Object_t * __this /* static, unused */, Decimal_t1_19  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 System.Decimal::ToUInt32(System.Decimal)
extern "C" uint32_t Decimal_ToUInt32_m1_746 (Object_t * __this /* static, unused */, Decimal_t1_19  ___d, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 System.Decimal::ToUInt64(System.Decimal)
extern "C" uint64_t Decimal_ToUInt64_m1_747 (Object_t * __this /* static, unused */, Decimal_t1_19  ___d, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Decimal::ToString(System.String,System.IFormatProvider)
extern "C" String_t* Decimal_ToString_m1_748 (Decimal_t1_19 * __this, String_t* ___format, Object_t * ___provider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Decimal::ToString()
extern "C" String_t* Decimal_ToString_m1_749 (Decimal_t1_19 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Decimal::ToString(System.String)
extern "C" String_t* Decimal_ToString_m1_750 (Decimal_t1_19 * __this, String_t* ___format, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Decimal::ToString(System.IFormatProvider)
extern "C" String_t* Decimal_ToString_m1_751 (Decimal_t1_19 * __this, Object_t * ___provider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Decimal::decimal2UInt64(System.Decimal&,System.UInt64&)
extern "C" int32_t Decimal_decimal2UInt64_m1_752 (Object_t * __this /* static, unused */, Decimal_t1_19 * ___val, uint64_t* ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Decimal::decimal2Int64(System.Decimal&,System.Int64&)
extern "C" int32_t Decimal_decimal2Int64_m1_753 (Object_t * __this /* static, unused */, Decimal_t1_19 * ___val, int64_t* ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Decimal::decimalIncr(System.Decimal&,System.Decimal&)
extern "C" int32_t Decimal_decimalIncr_m1_754 (Object_t * __this /* static, unused */, Decimal_t1_19 * ___d1, Decimal_t1_19 * ___d2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Decimal::decimal2string(System.Decimal&,System.Int32,System.Int32,System.Char[],System.Int32,System.Int32&,System.Int32&)
extern "C" int32_t Decimal_decimal2string_m1_755 (Object_t * __this /* static, unused */, Decimal_t1_19 * ___val, int32_t ___digits, int32_t ___decimals, CharU5BU5D_t1_16* ___bufDigits, int32_t ___bufSize, int32_t* ___decPos, int32_t* ___sign, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Decimal::string2decimal(System.Decimal&,System.String,System.UInt32,System.Int32)
extern "C" int32_t Decimal_string2decimal_m1_756 (Object_t * __this /* static, unused */, Decimal_t1_19 * ___val, String_t* ___sDigits, uint32_t ___decPos, int32_t ___sign, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Decimal::decimalSetExponent(System.Decimal&,System.Int32)
extern "C" int32_t Decimal_decimalSetExponent_m1_757 (Object_t * __this /* static, unused */, Decimal_t1_19 * ___val, int32_t ___exp, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Decimal::decimal2double(System.Decimal&)
extern "C" double Decimal_decimal2double_m1_758 (Object_t * __this /* static, unused */, Decimal_t1_19 * ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Decimal::decimalFloorAndTrunc(System.Decimal&,System.Int32)
extern "C" void Decimal_decimalFloorAndTrunc_m1_759 (Object_t * __this /* static, unused */, Decimal_t1_19 * ___val, int32_t ___floorFlag, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Decimal::decimalMult(System.Decimal&,System.Decimal&)
extern "C" int32_t Decimal_decimalMult_m1_760 (Object_t * __this /* static, unused */, Decimal_t1_19 * ___pd1, Decimal_t1_19 * ___pd2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Decimal::decimalDiv(System.Decimal&,System.Decimal&,System.Decimal&)
extern "C" int32_t Decimal_decimalDiv_m1_761 (Object_t * __this /* static, unused */, Decimal_t1_19 * ___pc, Decimal_t1_19 * ___pa, Decimal_t1_19 * ___pb, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Decimal::decimalIntDiv(System.Decimal&,System.Decimal&,System.Decimal&)
extern "C" int32_t Decimal_decimalIntDiv_m1_762 (Object_t * __this /* static, unused */, Decimal_t1_19 * ___pc, Decimal_t1_19 * ___pa, Decimal_t1_19 * ___pb, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Decimal::decimalCompare(System.Decimal&,System.Decimal&)
extern "C" int32_t Decimal_decimalCompare_m1_763 (Object_t * __this /* static, unused */, Decimal_t1_19 * ___d1, Decimal_t1_19 * ___d2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Decimal::op_Addition(System.Decimal,System.Decimal)
extern "C" Decimal_t1_19  Decimal_op_Addition_m1_764 (Object_t * __this /* static, unused */, Decimal_t1_19  ___d1, Decimal_t1_19  ___d2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Decimal::op_Decrement(System.Decimal)
extern "C" Decimal_t1_19  Decimal_op_Decrement_m1_765 (Object_t * __this /* static, unused */, Decimal_t1_19  ___d, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Decimal::op_Increment(System.Decimal)
extern "C" Decimal_t1_19  Decimal_op_Increment_m1_766 (Object_t * __this /* static, unused */, Decimal_t1_19  ___d, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Decimal::op_Subtraction(System.Decimal,System.Decimal)
extern "C" Decimal_t1_19  Decimal_op_Subtraction_m1_767 (Object_t * __this /* static, unused */, Decimal_t1_19  ___d1, Decimal_t1_19  ___d2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Decimal::op_UnaryNegation(System.Decimal)
extern "C" Decimal_t1_19  Decimal_op_UnaryNegation_m1_768 (Object_t * __this /* static, unused */, Decimal_t1_19  ___d, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Decimal::op_UnaryPlus(System.Decimal)
extern "C" Decimal_t1_19  Decimal_op_UnaryPlus_m1_769 (Object_t * __this /* static, unused */, Decimal_t1_19  ___d, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Decimal::op_Multiply(System.Decimal,System.Decimal)
extern "C" Decimal_t1_19  Decimal_op_Multiply_m1_770 (Object_t * __this /* static, unused */, Decimal_t1_19  ___d1, Decimal_t1_19  ___d2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Decimal::op_Division(System.Decimal,System.Decimal)
extern "C" Decimal_t1_19  Decimal_op_Division_m1_771 (Object_t * __this /* static, unused */, Decimal_t1_19  ___d1, Decimal_t1_19  ___d2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Decimal::op_Modulus(System.Decimal,System.Decimal)
extern "C" Decimal_t1_19  Decimal_op_Modulus_m1_772 (Object_t * __this /* static, unused */, Decimal_t1_19  ___d1, Decimal_t1_19  ___d2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte System.Decimal::op_Explicit(System.Decimal)
extern "C" uint8_t Decimal_op_Explicit_m1_773 (Object_t * __this /* static, unused */, Decimal_t1_19  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.SByte System.Decimal::op_Explicit(System.Decimal)
extern "C" int8_t Decimal_op_Explicit_m1_774 (Object_t * __this /* static, unused */, Decimal_t1_19  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char System.Decimal::op_Explicit(System.Decimal)
extern "C" uint16_t Decimal_op_Explicit_m1_775 (Object_t * __this /* static, unused */, Decimal_t1_19  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 System.Decimal::op_Explicit(System.Decimal)
extern "C" int16_t Decimal_op_Explicit_m1_776 (Object_t * __this /* static, unused */, Decimal_t1_19  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 System.Decimal::op_Explicit(System.Decimal)
extern "C" uint16_t Decimal_op_Explicit_m1_777 (Object_t * __this /* static, unused */, Decimal_t1_19  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Decimal::op_Explicit(System.Decimal)
extern "C" int32_t Decimal_op_Explicit_m1_778 (Object_t * __this /* static, unused */, Decimal_t1_19  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 System.Decimal::op_Explicit(System.Decimal)
extern "C" uint32_t Decimal_op_Explicit_m1_779 (Object_t * __this /* static, unused */, Decimal_t1_19  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Decimal::op_Explicit(System.Decimal)
extern "C" int64_t Decimal_op_Explicit_m1_780 (Object_t * __this /* static, unused */, Decimal_t1_19  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 System.Decimal::op_Explicit(System.Decimal)
extern "C" uint64_t Decimal_op_Explicit_m1_781 (Object_t * __this /* static, unused */, Decimal_t1_19  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Decimal::op_Implicit(System.Byte)
extern "C" Decimal_t1_19  Decimal_op_Implicit_m1_782 (Object_t * __this /* static, unused */, uint8_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Decimal::op_Implicit(System.SByte)
extern "C" Decimal_t1_19  Decimal_op_Implicit_m1_783 (Object_t * __this /* static, unused */, int8_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Decimal::op_Implicit(System.Int16)
extern "C" Decimal_t1_19  Decimal_op_Implicit_m1_784 (Object_t * __this /* static, unused */, int16_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Decimal::op_Implicit(System.UInt16)
extern "C" Decimal_t1_19  Decimal_op_Implicit_m1_785 (Object_t * __this /* static, unused */, uint16_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Decimal::op_Implicit(System.Char)
extern "C" Decimal_t1_19  Decimal_op_Implicit_m1_786 (Object_t * __this /* static, unused */, uint16_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Decimal::op_Implicit(System.Int32)
extern "C" Decimal_t1_19  Decimal_op_Implicit_m1_787 (Object_t * __this /* static, unused */, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Decimal::op_Implicit(System.UInt32)
extern "C" Decimal_t1_19  Decimal_op_Implicit_m1_788 (Object_t * __this /* static, unused */, uint32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Decimal::op_Implicit(System.Int64)
extern "C" Decimal_t1_19  Decimal_op_Implicit_m1_789 (Object_t * __this /* static, unused */, int64_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Decimal::op_Implicit(System.UInt64)
extern "C" Decimal_t1_19  Decimal_op_Implicit_m1_790 (Object_t * __this /* static, unused */, uint64_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Decimal::op_Explicit(System.Single)
extern "C" Decimal_t1_19  Decimal_op_Explicit_m1_791 (Object_t * __this /* static, unused */, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Decimal::op_Explicit(System.Double)
extern "C" Decimal_t1_19  Decimal_op_Explicit_m1_792 (Object_t * __this /* static, unused */, double ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single System.Decimal::op_Explicit(System.Decimal)
extern "C" float Decimal_op_Explicit_m1_793 (Object_t * __this /* static, unused */, Decimal_t1_19  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Decimal::op_Explicit(System.Decimal)
extern "C" double Decimal_op_Explicit_m1_794 (Object_t * __this /* static, unused */, Decimal_t1_19  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Decimal::op_Inequality(System.Decimal,System.Decimal)
extern "C" bool Decimal_op_Inequality_m1_795 (Object_t * __this /* static, unused */, Decimal_t1_19  ___d1, Decimal_t1_19  ___d2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Decimal::op_Equality(System.Decimal,System.Decimal)
extern "C" bool Decimal_op_Equality_m1_796 (Object_t * __this /* static, unused */, Decimal_t1_19  ___d1, Decimal_t1_19  ___d2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Decimal::op_GreaterThan(System.Decimal,System.Decimal)
extern "C" bool Decimal_op_GreaterThan_m1_797 (Object_t * __this /* static, unused */, Decimal_t1_19  ___d1, Decimal_t1_19  ___d2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Decimal::op_GreaterThanOrEqual(System.Decimal,System.Decimal)
extern "C" bool Decimal_op_GreaterThanOrEqual_m1_798 (Object_t * __this /* static, unused */, Decimal_t1_19  ___d1, Decimal_t1_19  ___d2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Decimal::op_LessThan(System.Decimal,System.Decimal)
extern "C" bool Decimal_op_LessThan_m1_799 (Object_t * __this /* static, unused */, Decimal_t1_19  ___d1, Decimal_t1_19  ___d2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Decimal::op_LessThanOrEqual(System.Decimal,System.Decimal)
extern "C" bool Decimal_op_LessThanOrEqual_m1_800 (Object_t * __this /* static, unused */, Decimal_t1_19  ___d1, Decimal_t1_19  ___d2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
