﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.StrongNameManager
struct StrongNameManager_t1_234;
// System.String
struct String_t;
// System.Security.SecurityElement
struct SecurityElement_t1_242;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Reflection.AssemblyName
struct AssemblyName_t1_576;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Security.StrongNameManager::.ctor()
extern "C" void StrongNameManager__ctor_m1_2603 (StrongNameManager_t1_234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.StrongNameManager::.cctor()
extern "C" void StrongNameManager__cctor_m1_2604 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.StrongNameManager::LoadConfig(System.String)
extern "C" void StrongNameManager_LoadConfig_m1_2605 (Object_t * __this /* static, unused */, String_t* ___filename, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.StrongNameManager::LoadMapping(System.Security.SecurityElement)
extern "C" void StrongNameManager_LoadMapping_m1_2606 (Object_t * __this /* static, unused */, SecurityElement_t1_242 * ___mapping, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.StrongNameManager::LoadVerificationSettings(System.Security.SecurityElement)
extern "C" void StrongNameManager_LoadVerificationSettings_m1_2607 (Object_t * __this /* static, unused */, SecurityElement_t1_242 * ___settings, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.StrongNameManager::GetMappedPublicKey(System.Byte[])
extern "C" ByteU5BU5D_t1_109* StrongNameManager_GetMappedPublicKey_m1_2608 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___token, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.StrongNameManager::MustVerify(System.Reflection.AssemblyName)
extern "C" bool StrongNameManager_MustVerify_m1_2609 (Object_t * __this /* static, unused */, AssemblyName_t1_576 * ___an, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.StrongNameManager::ToString()
extern "C" String_t* StrongNameManager_ToString_m1_2610 (StrongNameManager_t1_234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
