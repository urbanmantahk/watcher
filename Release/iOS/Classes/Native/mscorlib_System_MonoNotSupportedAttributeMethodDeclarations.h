﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.MonoNotSupportedAttribute
struct MonoNotSupportedAttribute_t1_81;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.MonoNotSupportedAttribute::.ctor(System.String)
extern "C" void MonoNotSupportedAttribute__ctor_m1_1413 (MonoNotSupportedAttribute_t1_81 * __this, String_t* ___comment, const MethodInfo* method) IL2CPP_METHOD_ATTR;
