﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VoxelBusters.NativePlugins.Score[]
struct ScoreU5BU5D_t8_230;
// VoxelBusters.NativePlugins.Score
struct Score_t8_231;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;
// System.Object
struct Object_t;

#include "mscorlib_System_MulticastDelegate.h"
#include "mscorlib_System_Void.h"

// VoxelBusters.NativePlugins.Leaderboard/LoadScoreCompletion
struct  LoadScoreCompletion_t8_229  : public MulticastDelegate_t1_21
{
};
