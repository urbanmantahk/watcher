﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.SecurityState
struct SecurityState_t1_1408;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.SecurityState::.ctor()
extern "C" void SecurityState__ctor_m1_12158 (SecurityState_t1_1408 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.SecurityState::IsStateAvailable()
extern "C" bool SecurityState_IsStateAvailable_m1_12159 (SecurityState_t1_1408 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
