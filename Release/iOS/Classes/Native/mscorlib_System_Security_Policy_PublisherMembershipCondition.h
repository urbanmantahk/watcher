﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Security.Cryptography.X509Certificates.X509Certificate
struct X509Certificate_t1_1179;

#include "mscorlib_System_Object.h"

// System.Security.Policy.PublisherMembershipCondition
struct  PublisherMembershipCondition_t1_1360  : public Object_t
{
	// System.Int32 System.Security.Policy.PublisherMembershipCondition::version
	int32_t ___version_0;
	// System.Security.Cryptography.X509Certificates.X509Certificate System.Security.Policy.PublisherMembershipCondition::x509
	X509Certificate_t1_1179 * ___x509_1;
};
