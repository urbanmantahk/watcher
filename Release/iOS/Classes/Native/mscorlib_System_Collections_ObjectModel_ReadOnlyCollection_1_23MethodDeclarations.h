﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>
struct ReadOnlyCollection_1_t1_2361;
// System.Collections.Generic.IList`1<UnityEngine.UICharInfo>
struct IList_1_t1_1842;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Object
struct Object_t;
// UnityEngine.UICharInfo[]
struct UICharInfoU5BU5D_t6_288;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UICharInfo>
struct IEnumerator_1_t1_2818;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_UICharInfo.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C" void ReadOnlyCollection_1__ctor_m1_20522_gshared (ReadOnlyCollection_1_t1_2361 * __this, Object_t* ___list, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m1_20522(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t1_2361 *, Object_t*, const MethodInfo*))ReadOnlyCollection_1__ctor_m1_20522_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1_20523_gshared (ReadOnlyCollection_1_t1_2361 * __this, UICharInfo_t6_150  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1_20523(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t1_2361 *, UICharInfo_t6_150 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1_20523_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::System.Collections.Generic.ICollection<T>.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1_20524_gshared (ReadOnlyCollection_1_t1_2361 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1_20524(__this, method) (( void (*) (ReadOnlyCollection_1_t1_2361 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1_20524_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1_20525_gshared (ReadOnlyCollection_1_t1_2361 * __this, int32_t ___index, UICharInfo_t6_150  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1_20525(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t1_2361 *, int32_t, UICharInfo_t6_150 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1_20525_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1_20526_gshared (ReadOnlyCollection_1_t1_2361 * __this, UICharInfo_t6_150  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1_20526(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t1_2361 *, UICharInfo_t6_150 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1_20526_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1_20527_gshared (ReadOnlyCollection_1_t1_2361 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1_20527(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t1_2361 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1_20527_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C" UICharInfo_t6_150  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1_20528_gshared (ReadOnlyCollection_1_t1_2361 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1_20528(__this, ___index, method) (( UICharInfo_t6_150  (*) (ReadOnlyCollection_1_t1_2361 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1_20528_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1_20529_gshared (ReadOnlyCollection_1_t1_2361 * __this, int32_t ___index, UICharInfo_t6_150  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1_20529(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t1_2361 *, int32_t, UICharInfo_t6_150 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1_20529_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_20530_gshared (ReadOnlyCollection_1_t1_2361 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_20530(__this, method) (( bool (*) (ReadOnlyCollection_1_t1_2361 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_20530_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1_20531_gshared (ReadOnlyCollection_1_t1_2361 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1_20531(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t1_2361 *, Array_t *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1_20531_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1_20532_gshared (ReadOnlyCollection_1_t1_2361 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1_20532(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t1_2361 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1_20532_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::System.Collections.IList.Add(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m1_20533_gshared (ReadOnlyCollection_1_t1_2361 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m1_20533(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t1_2361 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m1_20533_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::System.Collections.IList.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m1_20534_gshared (ReadOnlyCollection_1_t1_2361 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m1_20534(__this, method) (( void (*) (ReadOnlyCollection_1_t1_2361 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m1_20534_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::System.Collections.IList.Contains(System.Object)
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_Contains_m1_20535_gshared (ReadOnlyCollection_1_t1_2361 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m1_20535(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t1_2361 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m1_20535_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1_20536_gshared (ReadOnlyCollection_1_t1_2361 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1_20536(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t1_2361 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1_20536_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m1_20537_gshared (ReadOnlyCollection_1_t1_2361 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m1_20537(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t1_2361 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m1_20537_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::System.Collections.IList.Remove(System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m1_20538_gshared (ReadOnlyCollection_1_t1_2361 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m1_20538(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t1_2361 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m1_20538_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::System.Collections.IList.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1_20539_gshared (ReadOnlyCollection_1_t1_2361 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1_20539(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t1_2361 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1_20539_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1_20540_gshared (ReadOnlyCollection_1_t1_2361 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1_20540(__this, method) (( bool (*) (ReadOnlyCollection_1_t1_2361 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1_20540_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1_20541_gshared (ReadOnlyCollection_1_t1_2361 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1_20541(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t1_2361 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1_20541_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::System.Collections.IList.get_IsFixedSize()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1_20542_gshared (ReadOnlyCollection_1_t1_2361 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1_20542(__this, method) (( bool (*) (ReadOnlyCollection_1_t1_2361 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1_20542_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::System.Collections.IList.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1_20543_gshared (ReadOnlyCollection_1_t1_2361 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1_20543(__this, method) (( bool (*) (ReadOnlyCollection_1_t1_2361 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1_20543_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IList_get_Item_m1_20544_gshared (ReadOnlyCollection_1_t1_2361 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m1_20544(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t1_2361 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m1_20544_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m1_20545_gshared (ReadOnlyCollection_1_t1_2361 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m1_20545(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t1_2361 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m1_20545_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::Contains(T)
extern "C" bool ReadOnlyCollection_1_Contains_m1_20546_gshared (ReadOnlyCollection_1_t1_2361 * __this, UICharInfo_t6_150  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m1_20546(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t1_2361 *, UICharInfo_t6_150 , const MethodInfo*))ReadOnlyCollection_1_Contains_m1_20546_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::CopyTo(T[],System.Int32)
extern "C" void ReadOnlyCollection_1_CopyTo_m1_20547_gshared (ReadOnlyCollection_1_t1_2361 * __this, UICharInfoU5BU5D_t6_288* ___array, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m1_20547(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t1_2361 *, UICharInfoU5BU5D_t6_288*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m1_20547_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::GetEnumerator()
extern "C" Object_t* ReadOnlyCollection_1_GetEnumerator_m1_20548_gshared (ReadOnlyCollection_1_t1_2361 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m1_20548(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t1_2361 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m1_20548_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::IndexOf(T)
extern "C" int32_t ReadOnlyCollection_1_IndexOf_m1_20549_gshared (ReadOnlyCollection_1_t1_2361 * __this, UICharInfo_t6_150  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m1_20549(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t1_2361 *, UICharInfo_t6_150 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m1_20549_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::get_Count()
extern "C" int32_t ReadOnlyCollection_1_get_Count_m1_20550_gshared (ReadOnlyCollection_1_t1_2361 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m1_20550(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t1_2361 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m1_20550_gshared)(__this, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::get_Items()
extern "C" Object_t* ReadOnlyCollection_1_get_Items_m1_20551_gshared (ReadOnlyCollection_1_t1_2361 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Items_m1_20551(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t1_2361 *, const MethodInfo*))ReadOnlyCollection_1_get_Items_m1_20551_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::get_Item(System.Int32)
extern "C" UICharInfo_t6_150  ReadOnlyCollection_1_get_Item_m1_20552_gshared (ReadOnlyCollection_1_t1_2361 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m1_20552(__this, ___index, method) (( UICharInfo_t6_150  (*) (ReadOnlyCollection_1_t1_2361 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m1_20552_gshared)(__this, ___index, method)
