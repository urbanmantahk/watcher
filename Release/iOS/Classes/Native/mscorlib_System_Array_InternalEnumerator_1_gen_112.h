﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Array
struct Array_t;

#include "mscorlib_System_ValueType.h"

// System.Array/InternalEnumerator`1<System.Security.Policy.Evidence>
struct  InternalEnumerator_1_t1_2156 
{
	// System.Array System.Array/InternalEnumerator`1::array
	Array_t * ___array_2;
	// System.Int32 System.Array/InternalEnumerator`1::idx
	int32_t ___idx_3;
};
