﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Security.Cryptography.HashAlgorithm
struct HashAlgorithm_t1_162;
// System.String
struct String_t;
// Mono.Security.Cryptography.BlockProcessor
struct BlockProcessor_t1_155;

#include "mscorlib_System_Object.h"

// Mono.Security.Cryptography.HMACAlgorithm
struct  HMACAlgorithm_t1_161  : public Object_t
{
	// System.Byte[] Mono.Security.Cryptography.HMACAlgorithm::key
	ByteU5BU5D_t1_109* ___key_0;
	// System.Byte[] Mono.Security.Cryptography.HMACAlgorithm::hash
	ByteU5BU5D_t1_109* ___hash_1;
	// System.Security.Cryptography.HashAlgorithm Mono.Security.Cryptography.HMACAlgorithm::algo
	HashAlgorithm_t1_162 * ___algo_2;
	// System.String Mono.Security.Cryptography.HMACAlgorithm::hashName
	String_t* ___hashName_3;
	// Mono.Security.Cryptography.BlockProcessor Mono.Security.Cryptography.HMACAlgorithm::block
	BlockProcessor_t1_155 * ___block_4;
};
