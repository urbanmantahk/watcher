﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Policy.Zone
struct Zone_t1_1370;
// System.Char[]
struct CharU5BU5D_t1_16;
// System.Object
struct Object_t;
// System.Security.IPermission
struct IPermission_t1_1400;
// System.Security.Policy.Evidence
struct Evidence_t1_398;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_SecurityZone.h"

// System.Void System.Security.Policy.Zone::.ctor(System.Security.SecurityZone)
extern "C" void Zone__ctor_m1_11760 (Zone_t1_1370 * __this, int32_t ___zone, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Policy.Zone::System.Security.Policy.IBuiltInEvidence.GetRequiredSize(System.Boolean)
extern "C" int32_t Zone_System_Security_Policy_IBuiltInEvidence_GetRequiredSize_m1_11761 (Zone_t1_1370 * __this, bool ___verbose, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Policy.Zone::System.Security.Policy.IBuiltInEvidence.InitFromBuffer(System.Char[],System.Int32)
extern "C" int32_t Zone_System_Security_Policy_IBuiltInEvidence_InitFromBuffer_m1_11762 (Zone_t1_1370 * __this, CharU5BU5D_t1_16* ___buffer, int32_t ___position, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Policy.Zone::System.Security.Policy.IBuiltInEvidence.OutputToBuffer(System.Char[],System.Int32,System.Boolean)
extern "C" int32_t Zone_System_Security_Policy_IBuiltInEvidence_OutputToBuffer_m1_11763 (Zone_t1_1370 * __this, CharU5BU5D_t1_16* ___buffer, int32_t ___position, bool ___verbose, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityZone System.Security.Policy.Zone::get_SecurityZone()
extern "C" int32_t Zone_get_SecurityZone_m1_11764 (Zone_t1_1370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Security.Policy.Zone::Copy()
extern "C" Object_t * Zone_Copy_m1_11765 (Zone_t1_1370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Policy.Zone::CreateIdentityPermission(System.Security.Policy.Evidence)
extern "C" Object_t * Zone_CreateIdentityPermission_m1_11766 (Zone_t1_1370 * __this, Evidence_t1_398 * ___evidence, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.Zone System.Security.Policy.Zone::CreateFromUrl(System.String)
extern "C" Zone_t1_1370 * Zone_CreateFromUrl_m1_11767 (Object_t * __this /* static, unused */, String_t* ___url, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Policy.Zone::Equals(System.Object)
extern "C" bool Zone_Equals_m1_11768 (Zone_t1_1370 * __this, Object_t * ___o, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Policy.Zone::GetHashCode()
extern "C" int32_t Zone_GetHashCode_m1_11769 (Zone_t1_1370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Policy.Zone::ToString()
extern "C" String_t* Zone_ToString_m1_11770 (Zone_t1_1370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
