﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VoxelBusters.DebugPRO.Internal.ConsoleLog[]
struct ConsoleLogU5BU5D_t8_382;

#include "mscorlib_System_Object.h"

// System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>
struct  List_1_t1_1908  : public Object_t
{
	// T[] System.Collections.Generic.List`1::_items
	ConsoleLogU5BU5D_t8_382* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
};
struct List_1_t1_1908_StaticFields{
	// T[] System.Collections.Generic.List`1::EmptyArray
	ConsoleLogU5BU5D_t8_382* ___EmptyArray_4;
};
