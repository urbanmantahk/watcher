﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Resources.Win32IconResource[]
struct Win32IconResourceU5BU5D_t1_669;

#include "mscorlib_System_Resources_Win32Resource.h"

// System.Resources.Win32GroupIconResource
struct  Win32GroupIconResource_t1_668  : public Win32Resource_t1_664
{
	// System.Resources.Win32IconResource[] System.Resources.Win32GroupIconResource::icons
	Win32IconResourceU5BU5D_t1_669* ___icons_3;
};
