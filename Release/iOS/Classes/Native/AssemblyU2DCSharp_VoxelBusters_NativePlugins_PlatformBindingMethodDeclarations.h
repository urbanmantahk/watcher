﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.PlatformBindingHelper
struct PlatformBindingHelper_t8_316;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.PlatformBindingHelper::.ctor()
extern "C" void PlatformBindingHelper__ctor_m8_1861 (PlatformBindingHelper_t8_316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.PlatformBindingHelper::Start()
extern "C" void PlatformBindingHelper_Start_m8_1862 (PlatformBindingHelper_t8_316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
