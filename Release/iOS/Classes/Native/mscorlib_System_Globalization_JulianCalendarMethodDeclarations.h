﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Globalization.JulianCalendar
struct JulianCalendar_t1_380;
// System.Int32[]
struct Int32U5BU5D_t1_275;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime.h"
#include "mscorlib_System_DayOfWeek.h"
#include "mscorlib_System_Globalization_CalendarAlgorithmType.h"

// System.Void System.Globalization.JulianCalendar::.ctor()
extern "C" void JulianCalendar__ctor_m1_4252 (JulianCalendar_t1_380 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.JulianCalendar::.cctor()
extern "C" void JulianCalendar__cctor_m1_4253 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] System.Globalization.JulianCalendar::get_Eras()
extern "C" Int32U5BU5D_t1_275* JulianCalendar_get_Eras_m1_4254 (JulianCalendar_t1_380 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.JulianCalendar::get_TwoDigitYearMax()
extern "C" int32_t JulianCalendar_get_TwoDigitYearMax_m1_4255 (JulianCalendar_t1_380 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.JulianCalendar::set_TwoDigitYearMax(System.Int32)
extern "C" void JulianCalendar_set_TwoDigitYearMax_m1_4256 (JulianCalendar_t1_380 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.JulianCalendar::M_CheckEra(System.Int32&)
extern "C" void JulianCalendar_M_CheckEra_m1_4257 (JulianCalendar_t1_380 * __this, int32_t* ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.JulianCalendar::M_CheckYE(System.Int32,System.Int32&)
extern "C" void JulianCalendar_M_CheckYE_m1_4258 (JulianCalendar_t1_380 * __this, int32_t ___year, int32_t* ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.JulianCalendar::M_CheckYME(System.Int32,System.Int32,System.Int32&)
extern "C" void JulianCalendar_M_CheckYME_m1_4259 (JulianCalendar_t1_380 * __this, int32_t ___year, int32_t ___month, int32_t* ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.JulianCalendar::M_CheckYMDE(System.Int32,System.Int32,System.Int32,System.Int32&)
extern "C" void JulianCalendar_M_CheckYMDE_m1_4260 (JulianCalendar_t1_380 * __this, int32_t ___year, int32_t ___month, int32_t ___day, int32_t* ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.JulianCalendar::AddMonths(System.DateTime,System.Int32)
extern "C" DateTime_t1_150  JulianCalendar_AddMonths_m1_4261 (JulianCalendar_t1_380 * __this, DateTime_t1_150  ___time, int32_t ___months, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.JulianCalendar::AddYears(System.DateTime,System.Int32)
extern "C" DateTime_t1_150  JulianCalendar_AddYears_m1_4262 (JulianCalendar_t1_380 * __this, DateTime_t1_150  ___time, int32_t ___years, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.JulianCalendar::GetDayOfMonth(System.DateTime)
extern "C" int32_t JulianCalendar_GetDayOfMonth_m1_4263 (JulianCalendar_t1_380 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DayOfWeek System.Globalization.JulianCalendar::GetDayOfWeek(System.DateTime)
extern "C" int32_t JulianCalendar_GetDayOfWeek_m1_4264 (JulianCalendar_t1_380 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.JulianCalendar::GetDayOfYear(System.DateTime)
extern "C" int32_t JulianCalendar_GetDayOfYear_m1_4265 (JulianCalendar_t1_380 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.JulianCalendar::GetDaysInMonth(System.Int32,System.Int32,System.Int32)
extern "C" int32_t JulianCalendar_GetDaysInMonth_m1_4266 (JulianCalendar_t1_380 * __this, int32_t ___year, int32_t ___month, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.JulianCalendar::GetDaysInYear(System.Int32,System.Int32)
extern "C" int32_t JulianCalendar_GetDaysInYear_m1_4267 (JulianCalendar_t1_380 * __this, int32_t ___year, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.JulianCalendar::GetEra(System.DateTime)
extern "C" int32_t JulianCalendar_GetEra_m1_4268 (JulianCalendar_t1_380 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.JulianCalendar::GetLeapMonth(System.Int32,System.Int32)
extern "C" int32_t JulianCalendar_GetLeapMonth_m1_4269 (JulianCalendar_t1_380 * __this, int32_t ___year, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.JulianCalendar::GetMonth(System.DateTime)
extern "C" int32_t JulianCalendar_GetMonth_m1_4270 (JulianCalendar_t1_380 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.JulianCalendar::GetMonthsInYear(System.Int32,System.Int32)
extern "C" int32_t JulianCalendar_GetMonthsInYear_m1_4271 (JulianCalendar_t1_380 * __this, int32_t ___year, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.JulianCalendar::GetYear(System.DateTime)
extern "C" int32_t JulianCalendar_GetYear_m1_4272 (JulianCalendar_t1_380 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.JulianCalendar::IsLeapDay(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" bool JulianCalendar_IsLeapDay_m1_4273 (JulianCalendar_t1_380 * __this, int32_t ___year, int32_t ___month, int32_t ___day, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.JulianCalendar::IsLeapMonth(System.Int32,System.Int32,System.Int32)
extern "C" bool JulianCalendar_IsLeapMonth_m1_4274 (JulianCalendar_t1_380 * __this, int32_t ___year, int32_t ___month, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.JulianCalendar::IsLeapYear(System.Int32,System.Int32)
extern "C" bool JulianCalendar_IsLeapYear_m1_4275 (JulianCalendar_t1_380 * __this, int32_t ___year, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.JulianCalendar::ToDateTime(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" DateTime_t1_150  JulianCalendar_ToDateTime_m1_4276 (JulianCalendar_t1_380 * __this, int32_t ___year, int32_t ___month, int32_t ___day, int32_t ___hour, int32_t ___minute, int32_t ___second, int32_t ___millisecond, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.JulianCalendar::ToFourDigitYear(System.Int32)
extern "C" int32_t JulianCalendar_ToFourDigitYear_m1_4277 (JulianCalendar_t1_380 * __this, int32_t ___year, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.CalendarAlgorithmType System.Globalization.JulianCalendar::get_AlgorithmType()
extern "C" int32_t JulianCalendar_get_AlgorithmType_m1_4278 (JulianCalendar_t1_380 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.JulianCalendar::get_MinSupportedDateTime()
extern "C" DateTime_t1_150  JulianCalendar_get_MinSupportedDateTime_m1_4279 (JulianCalendar_t1_380 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.JulianCalendar::get_MaxSupportedDateTime()
extern "C" DateTime_t1_150  JulianCalendar_get_MaxSupportedDateTime_m1_4280 (JulianCalendar_t1_380 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
