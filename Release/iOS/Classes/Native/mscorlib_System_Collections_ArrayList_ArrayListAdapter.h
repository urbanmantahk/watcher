﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.IList
struct IList_t1_262;

#include "mscorlib_System_Collections_ArrayList.h"

// System.Collections.ArrayList/ArrayListAdapter
struct  ArrayListAdapter_t1_261  : public ArrayList_t1_170
{
	// System.Collections.IList System.Collections.ArrayList/ArrayListAdapter::m_Adaptee
	Object_t * ___m_Adaptee_5;
};
