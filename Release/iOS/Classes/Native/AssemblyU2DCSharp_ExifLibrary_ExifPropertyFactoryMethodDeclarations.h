﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExifLibrary.ExifProperty
struct ExifProperty_t8_99;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifInterOperability.h"
#include "AssemblyU2DCSharp_ExifLibrary_BitConverterEx_ByteOrder.h"
#include "AssemblyU2DCSharp_ExifLibrary_IFD.h"

// ExifLibrary.ExifProperty ExifLibrary.ExifPropertyFactory::Get(ExifLibrary.ExifInterOperability,ExifLibrary.BitConverterEx/ByteOrder,ExifLibrary.IFD)
extern "C" ExifProperty_t8_99 * ExifPropertyFactory_Get_m8_627 (Object_t * __this /* static, unused */, ExifInterOperability_t8_113  ___interOperability, int32_t ___byteOrder, int32_t ___ifd, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.ExifProperty ExifLibrary.ExifPropertyFactory::Get(System.UInt16,System.UInt16,System.UInt32,System.Byte[],ExifLibrary.BitConverterEx/ByteOrder,ExifLibrary.IFD)
extern "C" ExifProperty_t8_99 * ExifPropertyFactory_Get_m8_628 (Object_t * __this /* static, unused */, uint16_t ___tag, uint16_t ___type, uint32_t ___count, ByteU5BU5D_t1_109* ___value, int32_t ___byteOrder, int32_t ___ifd, const MethodInfo* method) IL2CPP_METHOD_ATTR;
