﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Policy.IApplicationTrustManager
struct IApplicationTrustManager_t1_1332;
// System.Security.Policy.ApplicationTrustCollection
struct ApplicationTrustCollection_t1_1331;
// System.ActivationContext
struct ActivationContext_t1_717;
// System.Security.Policy.TrustManagerContext
struct TrustManagerContext_t1_1365;

#include "codegen/il2cpp-codegen.h"

// System.Security.Policy.IApplicationTrustManager System.Security.Policy.ApplicationSecurityManager::get_ApplicationTrustManager()
extern "C" Object_t * ApplicationSecurityManager_get_ApplicationTrustManager_m1_11353 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.ApplicationTrustCollection System.Security.Policy.ApplicationSecurityManager::get_UserApplicationTrusts()
extern "C" ApplicationTrustCollection_t1_1331 * ApplicationSecurityManager_get_UserApplicationTrusts_m1_11354 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Policy.ApplicationSecurityManager::DetermineApplicationTrust(System.ActivationContext,System.Security.Policy.TrustManagerContext)
extern "C" bool ApplicationSecurityManager_DetermineApplicationTrust_m1_11355 (Object_t * __this /* static, unused */, ActivationContext_t1_717 * ___activationContext, TrustManagerContext_t1_1365 * ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
