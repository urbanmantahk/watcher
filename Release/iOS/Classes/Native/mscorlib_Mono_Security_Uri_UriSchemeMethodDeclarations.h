﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_Mono_Security_Uri_UriScheme.h"

// System.Void Mono.Security.Uri/UriScheme::.ctor(System.String,System.String,System.Int32)
extern "C" void UriScheme__ctor_m1_2611 (UriScheme_t1_236 * __this, String_t* ___s, String_t* ___d, int32_t ___p, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void UriScheme_t1_236_marshal(const UriScheme_t1_236& unmarshaled, UriScheme_t1_236_marshaled& marshaled);
extern "C" void UriScheme_t1_236_marshal_back(const UriScheme_t1_236_marshaled& marshaled, UriScheme_t1_236& unmarshaled);
extern "C" void UriScheme_t1_236_marshal_cleanup(UriScheme_t1_236_marshaled& marshaled);
