﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Security_AccessControl_AccessRule.h"
#include "mscorlib_System_Guid.h"

// System.Security.AccessControl.ObjectAccessRule
struct  ObjectAccessRule_t1_1163  : public AccessRule_t1_1111
{
	// System.Guid System.Security.AccessControl.ObjectAccessRule::object_type
	Guid_t1_319  ___object_type_6;
	// System.Guid System.Security.AccessControl.ObjectAccessRule::inherited_object_type
	Guid_t1_319  ___inherited_object_type_7;
};
