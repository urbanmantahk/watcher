﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t1_2012;

#include "mscorlib_System_Object.h"

// System.Collections.Generic.CollectionDebuggerView`1<System.Object>
struct  CollectionDebuggerView_1_t1_2011  : public Object_t
{
	// System.Collections.Generic.ICollection`1<T> System.Collections.Generic.CollectionDebuggerView`1::c
	Object_t* ___c_0;
};
