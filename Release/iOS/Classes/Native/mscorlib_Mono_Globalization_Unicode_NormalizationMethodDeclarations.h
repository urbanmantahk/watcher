﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Globalization.Unicode.Normalization
struct Normalization_t1_117;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t1_247;
// System.Int32[]
struct Int32U5BU5D_t1_275;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_Mono_Globalization_Unicode_NormalizationCheck.h"
#include "mscorlib_System_IntPtr.h"

// System.Void Mono.Globalization.Unicode.Normalization::.ctor()
extern "C" void Normalization__ctor_m1_1607 (Normalization_t1_117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Globalization.Unicode.Normalization::.cctor()
extern "C" void Normalization__cctor_m1_1608 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Mono.Globalization.Unicode.Normalization::PropValue(System.Int32)
extern "C" uint32_t Normalization_PropValue_m1_1609 (Object_t * __this /* static, unused */, int32_t ___cp, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Globalization.Unicode.Normalization::CharMapIdx(System.Int32)
extern "C" int32_t Normalization_CharMapIdx_m1_1610 (Object_t * __this /* static, unused */, int32_t ___cp, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Globalization.Unicode.Normalization::GetNormalizedStringLength(System.Int32)
extern "C" int32_t Normalization_GetNormalizedStringLength_m1_1611 (Object_t * __this /* static, unused */, int32_t ___ch, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte Mono.Globalization.Unicode.Normalization::GetCombiningClass(System.Int32)
extern "C" uint8_t Normalization_GetCombiningClass_m1_1612 (Object_t * __this /* static, unused */, int32_t ___c, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Globalization.Unicode.Normalization::GetPrimaryCompositeFromMapIndex(System.Int32)
extern "C" int32_t Normalization_GetPrimaryCompositeFromMapIndex_m1_1613 (Object_t * __this /* static, unused */, int32_t ___src, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Globalization.Unicode.Normalization::GetPrimaryCompositeHelperIndex(System.Int32)
extern "C" int32_t Normalization_GetPrimaryCompositeHelperIndex_m1_1614 (Object_t * __this /* static, unused */, int32_t ___cp, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Globalization.Unicode.Normalization::GetPrimaryCompositeCharIndex(System.Object,System.Int32)
extern "C" int32_t Normalization_GetPrimaryCompositeCharIndex_m1_1615 (Object_t * __this /* static, unused */, Object_t * ___chars, int32_t ___start, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Globalization.Unicode.Normalization::Compose(System.String,System.Int32)
extern "C" String_t* Normalization_Compose_m1_1616 (Object_t * __this /* static, unused */, String_t* ___source, int32_t ___checkType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder Mono.Globalization.Unicode.Normalization::Combine(System.String,System.Int32,System.Int32)
extern "C" StringBuilder_t1_247 * Normalization_Combine_m1_1617 (Object_t * __this /* static, unused */, String_t* ___source, int32_t ___start, int32_t ___checkType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Globalization.Unicode.Normalization::CanBePrimaryComposite(System.Int32)
extern "C" bool Normalization_CanBePrimaryComposite_m1_1618 (Object_t * __this /* static, unused */, int32_t ___i, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Globalization.Unicode.Normalization::Combine(System.Text.StringBuilder,System.Int32,System.Int32)
extern "C" void Normalization_Combine_m1_1619 (Object_t * __this /* static, unused */, StringBuilder_t1_247 * ___sb, int32_t ___start, int32_t ___checkType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Globalization.Unicode.Normalization::GetPrimaryCompositeMapIndex(System.Object,System.Int32,System.Int32)
extern "C" int32_t Normalization_GetPrimaryCompositeMapIndex_m1_1620 (Object_t * __this /* static, unused */, Object_t * ___o, int32_t ___cur, int32_t ___bufferPos, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Globalization.Unicode.Normalization::Decompose(System.String,System.Int32)
extern "C" String_t* Normalization_Decompose_m1_1621 (Object_t * __this /* static, unused */, String_t* ___source, int32_t ___checkType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Globalization.Unicode.Normalization::Decompose(System.String,System.Text.StringBuilder&,System.Int32)
extern "C" void Normalization_Decompose_m1_1622 (Object_t * __this /* static, unused */, String_t* ___source, StringBuilder_t1_247 ** ___sb, int32_t ___checkType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Globalization.Unicode.Normalization::ReorderCanonical(System.String,System.Text.StringBuilder&,System.Int32)
extern "C" void Normalization_ReorderCanonical_m1_1623 (Object_t * __this /* static, unused */, String_t* ___src, StringBuilder_t1_247 ** ___sb, int32_t ___start, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Globalization.Unicode.Normalization::DecomposeChar(System.Text.StringBuilder&,System.Int32[]&,System.String,System.Int32,System.Int32&)
extern "C" void Normalization_DecomposeChar_m1_1624 (Object_t * __this /* static, unused */, StringBuilder_t1_247 ** ___sb, Int32U5BU5D_t1_275** ___buf, String_t* ___s, int32_t ___i, int32_t* ___start, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Globalization.Unicode.NormalizationCheck Mono.Globalization.Unicode.Normalization::QuickCheck(System.Char,System.Int32)
extern "C" int32_t Normalization_QuickCheck_m1_1625 (Object_t * __this /* static, unused */, uint16_t ___c, int32_t ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Globalization.Unicode.Normalization::GetCanonicalHangul(System.Int32,System.Int32[],System.Int32)
extern "C" bool Normalization_GetCanonicalHangul_m1_1626 (Object_t * __this /* static, unused */, int32_t ___s, Int32U5BU5D_t1_275* ___buf, int32_t ___bufIdx, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Globalization.Unicode.Normalization::GetCanonical(System.Int32,System.Int32[],System.Int32)
extern "C" void Normalization_GetCanonical_m1_1627 (Object_t * __this /* static, unused */, int32_t ___c, Int32U5BU5D_t1_275* ___buf, int32_t ___bufIdx, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Globalization.Unicode.Normalization::IsNormalized(System.String,System.Int32)
extern "C" bool Normalization_IsNormalized_m1_1628 (Object_t * __this /* static, unused */, String_t* ___source, int32_t ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Globalization.Unicode.Normalization::Normalize(System.String,System.Int32)
extern "C" String_t* Normalization_Normalize_m1_1629 (Object_t * __this /* static, unused */, String_t* ___source, int32_t ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Globalization.Unicode.Normalization::get_IsReady()
extern "C" bool Normalization_get_IsReady_m1_1630 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Globalization.Unicode.Normalization::load_normalization_resource(System.IntPtr&,System.IntPtr&,System.IntPtr&,System.IntPtr&,System.IntPtr&,System.IntPtr&)
extern "C" void Normalization_load_normalization_resource_m1_1631 (Object_t * __this /* static, unused */, IntPtr_t* ___props, IntPtr_t* ___mappedChars, IntPtr_t* ___charMapIndex, IntPtr_t* ___helperIndex, IntPtr_t* ___mapIdxToComposite, IntPtr_t* ___combiningClass, const MethodInfo* method) IL2CPP_METHOD_ATTR;
