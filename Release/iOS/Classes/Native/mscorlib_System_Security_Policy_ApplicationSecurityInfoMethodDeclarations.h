﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Policy.ApplicationSecurityInfo
struct ApplicationSecurityInfo_t1_1328;
// System.ActivationContext
struct ActivationContext_t1_717;
// System.Security.Policy.Evidence
struct Evidence_t1_398;
// System.ApplicationId
struct ApplicationId_t1_1329;
// System.Security.PermissionSet
struct PermissionSet_t1_563;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Policy.ApplicationSecurityInfo::.ctor(System.ActivationContext)
extern "C" void ApplicationSecurityInfo__ctor_m1_11344 (ApplicationSecurityInfo_t1_1328 * __this, ActivationContext_t1_717 * ___activationContext, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.Evidence System.Security.Policy.ApplicationSecurityInfo::get_ApplicationEvidence()
extern "C" Evidence_t1_398 * ApplicationSecurityInfo_get_ApplicationEvidence_m1_11345 (ApplicationSecurityInfo_t1_1328 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.ApplicationSecurityInfo::set_ApplicationEvidence(System.Security.Policy.Evidence)
extern "C" void ApplicationSecurityInfo_set_ApplicationEvidence_m1_11346 (ApplicationSecurityInfo_t1_1328 * __this, Evidence_t1_398 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ApplicationId System.Security.Policy.ApplicationSecurityInfo::get_ApplicationId()
extern "C" ApplicationId_t1_1329 * ApplicationSecurityInfo_get_ApplicationId_m1_11347 (ApplicationSecurityInfo_t1_1328 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.ApplicationSecurityInfo::set_ApplicationId(System.ApplicationId)
extern "C" void ApplicationSecurityInfo_set_ApplicationId_m1_11348 (ApplicationSecurityInfo_t1_1328 * __this, ApplicationId_t1_1329 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.PermissionSet System.Security.Policy.ApplicationSecurityInfo::get_DefaultRequestSet()
extern "C" PermissionSet_t1_563 * ApplicationSecurityInfo_get_DefaultRequestSet_m1_11349 (ApplicationSecurityInfo_t1_1328 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.ApplicationSecurityInfo::set_DefaultRequestSet(System.Security.PermissionSet)
extern "C" void ApplicationSecurityInfo_set_DefaultRequestSet_m1_11350 (ApplicationSecurityInfo_t1_1328 * __this, PermissionSet_t1_563 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ApplicationId System.Security.Policy.ApplicationSecurityInfo::get_DeploymentId()
extern "C" ApplicationId_t1_1329 * ApplicationSecurityInfo_get_DeploymentId_m1_11351 (ApplicationSecurityInfo_t1_1328 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.ApplicationSecurityInfo::set_DeploymentId(System.ApplicationId)
extern "C" void ApplicationSecurityInfo_set_DeploymentId_m1_11352 (ApplicationSecurityInfo_t1_1328 * __this, ApplicationId_t1_1329 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
