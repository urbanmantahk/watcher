﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.ApplicationSettings/Features
struct Features_t8_318;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.ApplicationSettings/Features::.ctor()
extern "C" void Features__ctor_m8_1866 (Features_t8_318 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.NativePlugins.ApplicationSettings/Features::get_UsesAddressBook()
extern "C" bool Features_get_UsesAddressBook_m8_1867 (Features_t8_318 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.NativePlugins.ApplicationSettings/Features::get_UsesBilling()
extern "C" bool Features_get_UsesBilling_m8_1868 (Features_t8_318 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.NativePlugins.ApplicationSettings/Features::get_UsesGameServices()
extern "C" bool Features_get_UsesGameServices_m8_1869 (Features_t8_318 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.NativePlugins.ApplicationSettings/Features::get_UsesMediaLibrary()
extern "C" bool Features_get_UsesMediaLibrary_m8_1870 (Features_t8_318 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.NativePlugins.ApplicationSettings/Features::get_UsesNetworkConnectivity()
extern "C" bool Features_get_UsesNetworkConnectivity_m8_1871 (Features_t8_318 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.NativePlugins.ApplicationSettings/Features::get_UsesNotificationService()
extern "C" bool Features_get_UsesNotificationService_m8_1872 (Features_t8_318 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.NativePlugins.ApplicationSettings/Features::get_UsesSharing()
extern "C" bool Features_get_UsesSharing_m8_1873 (Features_t8_318 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.NativePlugins.ApplicationSettings/Features::get_UsesTwitter()
extern "C" bool Features_get_UsesTwitter_m8_1874 (Features_t8_318 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.NativePlugins.ApplicationSettings/Features::get_UsesWebView()
extern "C" bool Features_get_UsesWebView_m8_1875 (Features_t8_318 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
