﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Xml.Schema.XsdIDRef
struct XsdIDRef_t4_16;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlTokenizedType.h"

// System.Void Mono.Xml.Schema.XsdIDRef::.ctor()
extern "C" void XsdIDRef__ctor_m4_25 (XsdIDRef_t4_16 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdIDRef::get_TokenizedType()
extern "C" int32_t XsdIDRef_get_TokenizedType_m4_26 (XsdIDRef_t4_16 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
