﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VoxelBusters.NativePlugins.MediaLibrary
struct MediaLibrary_t8_245;

#include "mscorlib_System_Object.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_URL.h"

// VoxelBusters.NativePlugins.MediaLibrary/<PickImageFinished>c__AnonStoreyB
struct  U3CPickImageFinishedU3Ec__AnonStoreyB_t8_244  : public Object_t
{
	// VoxelBusters.Utility.URL VoxelBusters.NativePlugins.MediaLibrary/<PickImageFinished>c__AnonStoreyB::_imagePathURL
	URL_t8_156  ____imagePathURL_0;
	// VoxelBusters.NativePlugins.MediaLibrary VoxelBusters.NativePlugins.MediaLibrary/<PickImageFinished>c__AnonStoreyB::<>f__this
	MediaLibrary_t8_245 * ___U3CU3Ef__this_1;
};
