﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Globalization_Calendar.h"
#include "mscorlib_System_DateTime.h"

// System.Globalization.HebrewCalendar
struct  HebrewCalendar_t1_373  : public Calendar_t1_338
{
};
struct HebrewCalendar_t1_373_StaticFields{
	// System.Int32 System.Globalization.HebrewCalendar::HebrewEra
	int32_t ___HebrewEra_10;
	// System.DateTime System.Globalization.HebrewCalendar::Min
	DateTime_t1_150  ___Min_11;
	// System.DateTime System.Globalization.HebrewCalendar::Max
	DateTime_t1_150  ___Max_12;
};
