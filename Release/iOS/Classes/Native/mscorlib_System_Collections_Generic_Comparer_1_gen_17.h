﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Comparer`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>
struct Comparer_1_t1_2725;

#include "mscorlib_System_Object.h"

// System.Collections.Generic.Comparer`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>
struct  Comparer_1_t1_2725  : public Object_t
{
};
struct Comparer_1_t1_2725_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1::_default
	Comparer_1_t1_2725 * ____default_0;
};
