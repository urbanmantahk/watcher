﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Security_PermissionSet.h"

// System.Security.NamedPermissionSet
struct  NamedPermissionSet_t1_1344  : public PermissionSet_t1_563
{
	// System.String System.Security.NamedPermissionSet::name
	String_t* ___name_2;
	// System.String System.Security.NamedPermissionSet::description
	String_t* ___description_3;
};
