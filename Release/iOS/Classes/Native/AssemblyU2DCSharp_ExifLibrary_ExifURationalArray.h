﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ExifLibrary.MathEx/UFraction32[]
struct UFraction32U5BU5D_t8_122;

#include "AssemblyU2DCSharp_ExifLibrary_ExifProperty.h"

// ExifLibrary.ExifURationalArray
struct  ExifURationalArray_t8_107  : public ExifProperty_t8_99
{
	// ExifLibrary.MathEx/UFraction32[] ExifLibrary.ExifURationalArray::mValue
	UFraction32U5BU5D_t8_122* ___mValue_3;
};
