﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Permissions.UrlIdentityPermission
struct UrlIdentityPermission_t1_1321;
// System.String
struct String_t;
// System.Security.IPermission
struct IPermission_t1_1400;
// System.Security.SecurityElement
struct SecurityElement_t1_242;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Permissions_PermissionState.h"

// System.Void System.Security.Permissions.UrlIdentityPermission::.ctor(System.Security.Permissions.PermissionState)
extern "C" void UrlIdentityPermission__ctor_m1_11280 (UrlIdentityPermission_t1_1321 * __this, int32_t ___state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.UrlIdentityPermission::.ctor(System.String)
extern "C" void UrlIdentityPermission__ctor_m1_11281 (UrlIdentityPermission_t1_1321 * __this, String_t* ___site, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Permissions.UrlIdentityPermission::System.Security.Permissions.IBuiltInPermission.GetTokenIndex()
extern "C" int32_t UrlIdentityPermission_System_Security_Permissions_IBuiltInPermission_GetTokenIndex_m1_11282 (UrlIdentityPermission_t1_1321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Permissions.UrlIdentityPermission::get_Url()
extern "C" String_t* UrlIdentityPermission_get_Url_m1_11283 (UrlIdentityPermission_t1_1321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.UrlIdentityPermission::set_Url(System.String)
extern "C" void UrlIdentityPermission_set_Url_m1_11284 (UrlIdentityPermission_t1_1321 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.UrlIdentityPermission::Copy()
extern "C" Object_t * UrlIdentityPermission_Copy_m1_11285 (UrlIdentityPermission_t1_1321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.UrlIdentityPermission::FromXml(System.Security.SecurityElement)
extern "C" void UrlIdentityPermission_FromXml_m1_11286 (UrlIdentityPermission_t1_1321 * __this, SecurityElement_t1_242 * ___esd, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.UrlIdentityPermission::Intersect(System.Security.IPermission)
extern "C" Object_t * UrlIdentityPermission_Intersect_m1_11287 (UrlIdentityPermission_t1_1321 * __this, Object_t * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.UrlIdentityPermission::IsSubsetOf(System.Security.IPermission)
extern "C" bool UrlIdentityPermission_IsSubsetOf_m1_11288 (UrlIdentityPermission_t1_1321 * __this, Object_t * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityElement System.Security.Permissions.UrlIdentityPermission::ToXml()
extern "C" SecurityElement_t1_242 * UrlIdentityPermission_ToXml_m1_11289 (UrlIdentityPermission_t1_1321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.UrlIdentityPermission::Union(System.Security.IPermission)
extern "C" Object_t * UrlIdentityPermission_Union_m1_11290 (UrlIdentityPermission_t1_1321 * __this, Object_t * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.UrlIdentityPermission::IsEmpty()
extern "C" bool UrlIdentityPermission_IsEmpty_m1_11291 (UrlIdentityPermission_t1_1321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Permissions.UrlIdentityPermission System.Security.Permissions.UrlIdentityPermission::Cast(System.Security.IPermission)
extern "C" UrlIdentityPermission_t1_1321 * UrlIdentityPermission_Cast_m1_11292 (UrlIdentityPermission_t1_1321 * __this, Object_t * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.UrlIdentityPermission::Match(System.String)
extern "C" bool UrlIdentityPermission_Match_m1_11293 (UrlIdentityPermission_t1_1321 * __this, String_t* ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
