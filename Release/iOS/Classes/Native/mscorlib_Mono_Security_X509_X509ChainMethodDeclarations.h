﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.X509.X509Chain
struct X509Chain_t1_152;
// Mono.Security.X509.X509CertificateCollection
struct X509CertificateCollection_t1_148;
// Mono.Security.X509.X509Certificate
struct X509Certificate_t1_151;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_Mono_Security_X509_X509ChainStatusFlags.h"

// System.Void Mono.Security.X509.X509Chain::.ctor()
extern "C" void X509Chain__ctor_m1_2305 (X509Chain_t1_152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509Chain::.ctor(Mono.Security.X509.X509CertificateCollection)
extern "C" void X509Chain__ctor_m1_2306 (X509Chain_t1_152 * __this, X509CertificateCollection_t1_148 * ___chain, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509CertificateCollection Mono.Security.X509.X509Chain::get_Chain()
extern "C" X509CertificateCollection_t1_148 * X509Chain_get_Chain_m1_2307 (X509Chain_t1_152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Certificate Mono.Security.X509.X509Chain::get_Root()
extern "C" X509Certificate_t1_151 * X509Chain_get_Root_m1_2308 (X509Chain_t1_152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509ChainStatusFlags Mono.Security.X509.X509Chain::get_Status()
extern "C" int32_t X509Chain_get_Status_m1_2309 (X509Chain_t1_152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509CertificateCollection Mono.Security.X509.X509Chain::get_TrustAnchors()
extern "C" X509CertificateCollection_t1_148 * X509Chain_get_TrustAnchors_m1_2310 (X509Chain_t1_152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509Chain::set_TrustAnchors(Mono.Security.X509.X509CertificateCollection)
extern "C" void X509Chain_set_TrustAnchors_m1_2311 (X509Chain_t1_152 * __this, X509CertificateCollection_t1_148 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509Chain::LoadCertificate(Mono.Security.X509.X509Certificate)
extern "C" void X509Chain_LoadCertificate_m1_2312 (X509Chain_t1_152 * __this, X509Certificate_t1_151 * ___x509, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509Chain::LoadCertificates(Mono.Security.X509.X509CertificateCollection)
extern "C" void X509Chain_LoadCertificates_m1_2313 (X509Chain_t1_152 * __this, X509CertificateCollection_t1_148 * ___collection, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Certificate Mono.Security.X509.X509Chain::FindByIssuerName(System.String)
extern "C" X509Certificate_t1_151 * X509Chain_FindByIssuerName_m1_2314 (X509Chain_t1_152 * __this, String_t* ___issuerName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509Chain::Build(Mono.Security.X509.X509Certificate)
extern "C" bool X509Chain_Build_m1_2315 (X509Chain_t1_152 * __this, X509Certificate_t1_151 * ___leaf, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509Chain::Reset()
extern "C" void X509Chain_Reset_m1_2316 (X509Chain_t1_152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509Chain::IsValid(Mono.Security.X509.X509Certificate)
extern "C" bool X509Chain_IsValid_m1_2317 (X509Chain_t1_152 * __this, X509Certificate_t1_151 * ___cert, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Certificate Mono.Security.X509.X509Chain::FindCertificateParent(Mono.Security.X509.X509Certificate)
extern "C" X509Certificate_t1_151 * X509Chain_FindCertificateParent_m1_2318 (X509Chain_t1_152 * __this, X509Certificate_t1_151 * ___child, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Certificate Mono.Security.X509.X509Chain::FindCertificateRoot(Mono.Security.X509.X509Certificate)
extern "C" X509Certificate_t1_151 * X509Chain_FindCertificateRoot_m1_2319 (X509Chain_t1_152 * __this, X509Certificate_t1_151 * ___potentialRoot, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509Chain::IsTrusted(Mono.Security.X509.X509Certificate)
extern "C" bool X509Chain_IsTrusted_m1_2320 (X509Chain_t1_152 * __this, X509Certificate_t1_151 * ___potentialTrusted, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509Chain::IsParent(Mono.Security.X509.X509Certificate,Mono.Security.X509.X509Certificate)
extern "C" bool X509Chain_IsParent_m1_2321 (X509Chain_t1_152 * __this, X509Certificate_t1_151 * ___child, X509Certificate_t1_151 * ___parent, const MethodInfo* method) IL2CPP_METHOD_ATTR;
