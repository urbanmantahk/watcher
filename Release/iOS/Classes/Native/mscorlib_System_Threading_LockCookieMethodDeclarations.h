﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Threading_LockCookie.h"

// System.Void System.Threading.LockCookie::.ctor(System.Int32)
extern "C" void LockCookie__ctor_m1_12683 (LockCookie_t1_1466 * __this, int32_t ___thread_id, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.LockCookie::.ctor(System.Int32,System.Int32,System.Int32)
extern "C" void LockCookie__ctor_m1_12684 (LockCookie_t1_1466 * __this, int32_t ___thread_id, int32_t ___reader_locks, int32_t ___writer_locks, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Threading.LockCookie::GetHashCode()
extern "C" int32_t LockCookie_GetHashCode_m1_12685 (LockCookie_t1_1466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.LockCookie::Equals(System.Threading.LockCookie)
extern "C" bool LockCookie_Equals_m1_12686 (LockCookie_t1_1466 * __this, LockCookie_t1_1466  ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.LockCookie::Equals(System.Object)
extern "C" bool LockCookie_Equals_m1_12687 (LockCookie_t1_1466 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.LockCookie::op_Equality(System.Threading.LockCookie,System.Threading.LockCookie)
extern "C" bool LockCookie_op_Equality_m1_12688 (Object_t * __this /* static, unused */, LockCookie_t1_1466  ___a, LockCookie_t1_1466  ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.LockCookie::op_Inequality(System.Threading.LockCookie,System.Threading.LockCookie)
extern "C" bool LockCookie_op_Inequality_m1_12689 (Object_t * __this /* static, unused */, LockCookie_t1_1466  ___a, LockCookie_t1_1466  ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
