﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_CallType.h"

// System.Runtime.Remoting.Messaging.CallType
struct  CallType_t1_953 
{
	// System.Int32 System.Runtime.Remoting.Messaging.CallType::value__
	int32_t ___value___1;
};
