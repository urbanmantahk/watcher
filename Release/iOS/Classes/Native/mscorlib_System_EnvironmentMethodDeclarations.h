﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.OperatingSystem
struct OperatingSystem_t1_1545;
// System.Version
struct Version_t1_578;
// System.String[]
struct StringU5BU5D_t1_238;
// System.Collections.Hashtable
struct Hashtable_t1_100;
// System.Collections.IDictionary
struct IDictionary_t1_35;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_PlatformID.h"
#include "mscorlib_System_Environment_SpecialFolder.h"
#include "mscorlib_System_EnvironmentVariableTarget.h"

// System.String System.Environment::get_CommandLine()
extern "C" String_t* Environment_get_CommandLine_m1_14042 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Environment::get_CurrentDirectory()
extern "C" String_t* Environment_get_CurrentDirectory_m1_14043 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Environment::set_CurrentDirectory(System.String)
extern "C" void Environment_set_CurrentDirectory_m1_14044 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Environment::get_ExitCode()
extern "C" int32_t Environment_get_ExitCode_m1_14045 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Environment::set_ExitCode(System.Int32)
extern "C" void Environment_set_ExitCode_m1_14046 (Object_t * __this /* static, unused */, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Environment::get_HasShutdownStarted()
extern "C" bool Environment_get_HasShutdownStarted_m1_14047 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Environment::get_EmbeddingHostName()
extern "C" String_t* Environment_get_EmbeddingHostName_m1_14048 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Environment::get_SocketSecurityEnabled()
extern "C" bool Environment_get_SocketSecurityEnabled_m1_14049 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Environment::get_UnityWebSecurityEnabled()
extern "C" bool Environment_get_UnityWebSecurityEnabled_m1_14050 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Environment::get_MachineName()
extern "C" String_t* Environment_get_MachineName_m1_14051 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Environment::get_NewLine()
extern "C" String_t* Environment_get_NewLine_m1_14052 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.PlatformID System.Environment::get_Platform()
extern "C" int32_t Environment_get_Platform_m1_14053 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Environment::GetOSVersionString()
extern "C" String_t* Environment_GetOSVersionString_m1_14054 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.OperatingSystem System.Environment::get_OSVersion()
extern "C" OperatingSystem_t1_1545 * Environment_get_OSVersion_m1_14055 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Environment::get_StackTrace()
extern "C" String_t* Environment_get_StackTrace_m1_14056 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Environment::get_TickCount()
extern "C" int32_t Environment_get_TickCount_m1_14057 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Environment::get_UserDomainName()
extern "C" String_t* Environment_get_UserDomainName_m1_14058 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Environment::get_UserInteractive()
extern "C" bool Environment_get_UserInteractive_m1_14059 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Environment::get_UserName()
extern "C" String_t* Environment_get_UserName_m1_14060 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Version System.Environment::get_Version()
extern "C" Version_t1_578 * Environment_get_Version_m1_14061 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Environment::get_WorkingSet()
extern "C" int64_t Environment_get_WorkingSet_m1_14062 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Environment::Exit(System.Int32)
extern "C" void Environment_Exit_m1_14063 (Object_t * __this /* static, unused */, int32_t ___exitCode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Environment::ExpandEnvironmentVariables(System.String)
extern "C" String_t* Environment_ExpandEnvironmentVariables_m1_14064 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.Environment::GetCommandLineArgs()
extern "C" StringU5BU5D_t1_238* Environment_GetCommandLineArgs_m1_14065 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Environment::internalGetEnvironmentVariable(System.String)
extern "C" String_t* Environment_internalGetEnvironmentVariable_m1_14066 (Object_t * __this /* static, unused */, String_t* ___variable, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Environment::GetEnvironmentVariable(System.String)
extern "C" String_t* Environment_GetEnvironmentVariable_m1_14067 (Object_t * __this /* static, unused */, String_t* ___variable, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Hashtable System.Environment::GetEnvironmentVariablesNoCase()
extern "C" Hashtable_t1_100 * Environment_GetEnvironmentVariablesNoCase_m1_14068 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IDictionary System.Environment::GetEnvironmentVariables()
extern "C" Object_t * Environment_GetEnvironmentVariables_m1_14069 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Environment::GetWindowsFolderPath(System.Int32)
extern "C" String_t* Environment_GetWindowsFolderPath_m1_14070 (Object_t * __this /* static, unused */, int32_t ___folder, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Environment::GetFolderPath(System.Environment/SpecialFolder)
extern "C" String_t* Environment_GetFolderPath_m1_14071 (Object_t * __this /* static, unused */, int32_t ___folder, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Environment::ReadXdgUserDir(System.String,System.String,System.String,System.String)
extern "C" String_t* Environment_ReadXdgUserDir_m1_14072 (Object_t * __this /* static, unused */, String_t* ___config_dir, String_t* ___home_dir, String_t* ___key, String_t* ___fallback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Environment::InternalGetFolderPath(System.Environment/SpecialFolder)
extern "C" String_t* Environment_InternalGetFolderPath_m1_14073 (Object_t * __this /* static, unused */, int32_t ___folder, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.Environment::GetLogicalDrives()
extern "C" StringU5BU5D_t1_238* Environment_GetLogicalDrives_m1_14074 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Environment::internalBroadcastSettingChange()
extern "C" void Environment_internalBroadcastSettingChange_m1_14075 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Environment::GetEnvironmentVariable(System.String,System.EnvironmentVariableTarget)
extern "C" String_t* Environment_GetEnvironmentVariable_m1_14076 (Object_t * __this /* static, unused */, String_t* ___variable, int32_t ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IDictionary System.Environment::GetEnvironmentVariables(System.EnvironmentVariableTarget)
extern "C" Object_t * Environment_GetEnvironmentVariables_m1_14077 (Object_t * __this /* static, unused */, int32_t ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Environment::SetEnvironmentVariable(System.String,System.String)
extern "C" void Environment_SetEnvironmentVariable_m1_14078 (Object_t * __this /* static, unused */, String_t* ___variable, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Environment::SetEnvironmentVariable(System.String,System.String,System.EnvironmentVariableTarget)
extern "C" void Environment_SetEnvironmentVariable_m1_14079 (Object_t * __this /* static, unused */, String_t* ___variable, String_t* ___value, int32_t ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Environment::InternalSetEnvironmentVariable(System.String,System.String)
extern "C" void Environment_InternalSetEnvironmentVariable_m1_14080 (Object_t * __this /* static, unused */, String_t* ___variable, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Environment::FailFast(System.String)
extern "C" void Environment_FailFast_m1_14081 (Object_t * __this /* static, unused */, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Environment::get_ProcessorCount()
extern "C" int32_t Environment_get_ProcessorCount_m1_14082 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Environment::get_IsRunningOnWindows()
extern "C" bool Environment_get_IsRunningOnWindows_m1_14083 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.Environment::GetLogicalDrivesInternal()
extern "C" StringU5BU5D_t1_238* Environment_GetLogicalDrivesInternal_m1_14084 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.Environment::GetEnvironmentVariableNames()
extern "C" StringU5BU5D_t1_238* Environment_GetEnvironmentVariableNames_m1_14085 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Environment::GetMachineConfigPath()
extern "C" String_t* Environment_GetMachineConfigPath_m1_14086 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Environment::internalGetHome()
extern "C" String_t* Environment_internalGetHome_m1_14087 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
