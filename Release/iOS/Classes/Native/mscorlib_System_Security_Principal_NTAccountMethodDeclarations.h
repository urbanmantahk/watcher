﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Principal.NTAccount
struct NTAccount_t1_1377;
// System.String
struct String_t;
// System.Object
struct Object_t;
// System.Type
struct Type_t;
// System.Security.Principal.IdentityReference
struct IdentityReference_t1_1120;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Principal.NTAccount::.ctor(System.String)
extern "C" void NTAccount__ctor_m1_11815 (NTAccount_t1_1377 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Principal.NTAccount::.ctor(System.String,System.String)
extern "C" void NTAccount__ctor_m1_11816 (NTAccount_t1_1377 * __this, String_t* ___domainName, String_t* ___accountName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Principal.NTAccount::get_Value()
extern "C" String_t* NTAccount_get_Value_m1_11817 (NTAccount_t1_1377 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Principal.NTAccount::Equals(System.Object)
extern "C" bool NTAccount_Equals_m1_11818 (NTAccount_t1_1377 * __this, Object_t * ___o, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Principal.NTAccount::GetHashCode()
extern "C" int32_t NTAccount_GetHashCode_m1_11819 (NTAccount_t1_1377 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Principal.NTAccount::IsValidTargetType(System.Type)
extern "C" bool NTAccount_IsValidTargetType_m1_11820 (NTAccount_t1_1377 * __this, Type_t * ___targetType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Principal.NTAccount::ToString()
extern "C" String_t* NTAccount_ToString_m1_11821 (NTAccount_t1_1377 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Principal.IdentityReference System.Security.Principal.NTAccount::Translate(System.Type)
extern "C" IdentityReference_t1_1120 * NTAccount_Translate_m1_11822 (NTAccount_t1_1377 * __this, Type_t * ___targetType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Principal.NTAccount::op_Equality(System.Security.Principal.NTAccount,System.Security.Principal.NTAccount)
extern "C" bool NTAccount_op_Equality_m1_11823 (Object_t * __this /* static, unused */, NTAccount_t1_1377 * ___left, NTAccount_t1_1377 * ___right, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Principal.NTAccount::op_Inequality(System.Security.Principal.NTAccount,System.Security.Principal.NTAccount)
extern "C" bool NTAccount_op_Inequality_m1_11824 (Object_t * __this /* static, unused */, NTAccount_t1_1377 * ___left, NTAccount_t1_1377 * ___right, const MethodInfo* method) IL2CPP_METHOD_ATTR;
