﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<System.Reflection.CustomAttributeTypedArgument>
struct DefaultComparer_t1_2074;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArgument.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Reflection.CustomAttributeTypedArgument>::.ctor()
extern "C" void DefaultComparer__ctor_m1_16474_gshared (DefaultComparer_t1_2074 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1_16474(__this, method) (( void (*) (DefaultComparer_t1_2074 *, const MethodInfo*))DefaultComparer__ctor_m1_16474_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Reflection.CustomAttributeTypedArgument>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m1_16475_gshared (DefaultComparer_t1_2074 * __this, CustomAttributeTypedArgument_t1_594  ___x, CustomAttributeTypedArgument_t1_594  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m1_16475(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t1_2074 *, CustomAttributeTypedArgument_t1_594 , CustomAttributeTypedArgument_t1_594 , const MethodInfo*))DefaultComparer_Compare_m1_16475_gshared)(__this, ___x, ___y, method)
