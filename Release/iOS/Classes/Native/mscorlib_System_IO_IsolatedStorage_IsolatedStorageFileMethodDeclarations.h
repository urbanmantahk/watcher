﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.IO.IsolatedStorage.IsolatedStorageFile
struct IsolatedStorageFile_t1_397;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Security.Policy.Evidence
struct Evidence_t1_398;
// System.Type
struct Type_t;
// System.Object
struct Object_t;
// System.IO.DirectoryInfo
struct DirectoryInfo_t1_399;
// System.String[]
struct StringU5BU5D_t1_238;
// System.IO.FileSystemInfo[]
struct FileSystemInfoU5BU5D_t1_1679;
// System.Security.Permissions.IsolatedStoragePermission
struct IsolatedStoragePermission_t1_1285;
// System.Security.PermissionSet
struct PermissionSet_t1_563;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_IsolatedStorage_IsolatedStorageScope.h"
#include "mscorlib_System_Security_Permissions_IsolatedStorageContainm.h"

// System.Void System.IO.IsolatedStorage.IsolatedStorageFile::.ctor(System.IO.IsolatedStorage.IsolatedStorageScope)
extern "C" void IsolatedStorageFile__ctor_m1_4604 (IsolatedStorageFile_t1_397 * __this, int32_t ___scope, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.IsolatedStorage.IsolatedStorageFile::.ctor(System.IO.IsolatedStorage.IsolatedStorageScope,System.String)
extern "C" void IsolatedStorageFile__ctor_m1_4605 (IsolatedStorageFile_t1_397 * __this, int32_t ___scope, String_t* ___location, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.IsolatedStorage.IsolatedStorageFile::.cctor()
extern "C" void IsolatedStorageFile__cctor_m1_4606 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.IO.IsolatedStorage.IsolatedStorageFile::GetEnumerator(System.IO.IsolatedStorage.IsolatedStorageScope)
extern "C" Object_t * IsolatedStorageFile_GetEnumerator_m1_4607 (Object_t * __this /* static, unused */, int32_t ___scope, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.IsolatedStorage.IsolatedStorageFile System.IO.IsolatedStorage.IsolatedStorageFile::GetStore(System.IO.IsolatedStorage.IsolatedStorageScope,System.Security.Policy.Evidence,System.Type,System.Security.Policy.Evidence,System.Type)
extern "C" IsolatedStorageFile_t1_397 * IsolatedStorageFile_GetStore_m1_4608 (Object_t * __this /* static, unused */, int32_t ___scope, Evidence_t1_398 * ___domainEvidence, Type_t * ___domainEvidenceType, Evidence_t1_398 * ___assemblyEvidence, Type_t * ___assemblyEvidenceType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.IsolatedStorage.IsolatedStorageFile System.IO.IsolatedStorage.IsolatedStorageFile::GetStore(System.IO.IsolatedStorage.IsolatedStorageScope,System.Object,System.Object)
extern "C" IsolatedStorageFile_t1_397 * IsolatedStorageFile_GetStore_m1_4609 (Object_t * __this /* static, unused */, int32_t ___scope, Object_t * ___domainIdentity, Object_t * ___assemblyIdentity, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.IsolatedStorage.IsolatedStorageFile System.IO.IsolatedStorage.IsolatedStorageFile::GetStore(System.IO.IsolatedStorage.IsolatedStorageScope,System.Type,System.Type)
extern "C" IsolatedStorageFile_t1_397 * IsolatedStorageFile_GetStore_m1_4610 (Object_t * __this /* static, unused */, int32_t ___scope, Type_t * ___domainEvidenceType, Type_t * ___assemblyEvidenceType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.IsolatedStorage.IsolatedStorageFile System.IO.IsolatedStorage.IsolatedStorageFile::GetStore(System.IO.IsolatedStorage.IsolatedStorageScope,System.Object)
extern "C" IsolatedStorageFile_t1_397 * IsolatedStorageFile_GetStore_m1_4611 (Object_t * __this /* static, unused */, int32_t ___scope, Object_t * ___applicationIdentity, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.IsolatedStorage.IsolatedStorageFile System.IO.IsolatedStorage.IsolatedStorageFile::GetStore(System.IO.IsolatedStorage.IsolatedStorageScope,System.Type)
extern "C" IsolatedStorageFile_t1_397 * IsolatedStorageFile_GetStore_m1_4612 (Object_t * __this /* static, unused */, int32_t ___scope, Type_t * ___applicationEvidenceType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.IsolatedStorage.IsolatedStorageFile System.IO.IsolatedStorage.IsolatedStorageFile::GetMachineStoreForApplication()
extern "C" IsolatedStorageFile_t1_397 * IsolatedStorageFile_GetMachineStoreForApplication_m1_4613 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.IsolatedStorage.IsolatedStorageFile System.IO.IsolatedStorage.IsolatedStorageFile::GetMachineStoreForAssembly()
extern "C" IsolatedStorageFile_t1_397 * IsolatedStorageFile_GetMachineStoreForAssembly_m1_4614 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.IsolatedStorage.IsolatedStorageFile System.IO.IsolatedStorage.IsolatedStorageFile::GetMachineStoreForDomain()
extern "C" IsolatedStorageFile_t1_397 * IsolatedStorageFile_GetMachineStoreForDomain_m1_4615 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.IsolatedStorage.IsolatedStorageFile System.IO.IsolatedStorage.IsolatedStorageFile::GetUserStoreForApplication()
extern "C" IsolatedStorageFile_t1_397 * IsolatedStorageFile_GetUserStoreForApplication_m1_4616 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.IsolatedStorage.IsolatedStorageFile System.IO.IsolatedStorage.IsolatedStorageFile::GetUserStoreForAssembly()
extern "C" IsolatedStorageFile_t1_397 * IsolatedStorageFile_GetUserStoreForAssembly_m1_4617 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.IsolatedStorage.IsolatedStorageFile System.IO.IsolatedStorage.IsolatedStorageFile::GetUserStoreForDomain()
extern "C" IsolatedStorageFile_t1_397 * IsolatedStorageFile_GetUserStoreForDomain_m1_4618 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.IsolatedStorage.IsolatedStorageFile::Remove(System.IO.IsolatedStorage.IsolatedStorageScope)
extern "C" void IsolatedStorageFile_Remove_m1_4619 (Object_t * __this /* static, unused */, int32_t ___scope, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.IO.IsolatedStorage.IsolatedStorageFile::GetIsolatedStorageRoot(System.IO.IsolatedStorage.IsolatedStorageScope)
extern "C" String_t* IsolatedStorageFile_GetIsolatedStorageRoot_m1_4620 (Object_t * __this /* static, unused */, int32_t ___scope, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.IsolatedStorage.IsolatedStorageFile::Demand(System.IO.IsolatedStorage.IsolatedStorageScope)
extern "C" void IsolatedStorageFile_Demand_m1_4621 (Object_t * __this /* static, unused */, int32_t ___scope, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Permissions.IsolatedStorageContainment System.IO.IsolatedStorage.IsolatedStorageFile::ScopeToContainment(System.IO.IsolatedStorage.IsolatedStorageScope)
extern "C" int32_t IsolatedStorageFile_ScopeToContainment_m1_4622 (Object_t * __this /* static, unused */, int32_t ___scope, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 System.IO.IsolatedStorage.IsolatedStorageFile::GetDirectorySize(System.IO.DirectoryInfo)
extern "C" uint64_t IsolatedStorageFile_GetDirectorySize_m1_4623 (Object_t * __this /* static, unused */, DirectoryInfo_t1_399 * ___di, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.IsolatedStorage.IsolatedStorageFile::Finalize()
extern "C" void IsolatedStorageFile_Finalize_m1_4624 (IsolatedStorageFile_t1_397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.IsolatedStorage.IsolatedStorageFile::PostInit()
extern "C" void IsolatedStorageFile_PostInit_m1_4625 (IsolatedStorageFile_t1_397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 System.IO.IsolatedStorage.IsolatedStorageFile::get_CurrentSize()
extern "C" uint64_t IsolatedStorageFile_get_CurrentSize_m1_4626 (IsolatedStorageFile_t1_397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 System.IO.IsolatedStorage.IsolatedStorageFile::get_MaximumSize()
extern "C" uint64_t IsolatedStorageFile_get_MaximumSize_m1_4627 (IsolatedStorageFile_t1_397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.IO.IsolatedStorage.IsolatedStorageFile::get_Root()
extern "C" String_t* IsolatedStorageFile_get_Root_m1_4628 (IsolatedStorageFile_t1_397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.IsolatedStorage.IsolatedStorageFile::Close()
extern "C" void IsolatedStorageFile_Close_m1_4629 (IsolatedStorageFile_t1_397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.IsolatedStorage.IsolatedStorageFile::CreateDirectory(System.String)
extern "C" void IsolatedStorageFile_CreateDirectory_m1_4630 (IsolatedStorageFile_t1_397 * __this, String_t* ___dir, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.IsolatedStorage.IsolatedStorageFile::DeleteDirectory(System.String)
extern "C" void IsolatedStorageFile_DeleteDirectory_m1_4631 (IsolatedStorageFile_t1_397 * __this, String_t* ___dir, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.IsolatedStorage.IsolatedStorageFile::DeleteFile(System.String)
extern "C" void IsolatedStorageFile_DeleteFile_m1_4632 (IsolatedStorageFile_t1_397 * __this, String_t* ___file, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.IsolatedStorage.IsolatedStorageFile::Dispose()
extern "C" void IsolatedStorageFile_Dispose_m1_4633 (IsolatedStorageFile_t1_397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.IO.IsolatedStorage.IsolatedStorageFile::GetDirectoryNames(System.String)
extern "C" StringU5BU5D_t1_238* IsolatedStorageFile_GetDirectoryNames_m1_4634 (IsolatedStorageFile_t1_397 * __this, String_t* ___searchPattern, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.IO.IsolatedStorage.IsolatedStorageFile::GetNames(System.IO.FileSystemInfo[])
extern "C" StringU5BU5D_t1_238* IsolatedStorageFile_GetNames_m1_4635 (IsolatedStorageFile_t1_397 * __this, FileSystemInfoU5BU5D_t1_1679* ___afsi, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.IO.IsolatedStorage.IsolatedStorageFile::GetFileNames(System.String)
extern "C" StringU5BU5D_t1_238* IsolatedStorageFile_GetFileNames_m1_4636 (IsolatedStorageFile_t1_397 * __this, String_t* ___searchPattern, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.IsolatedStorage.IsolatedStorageFile::Remove()
extern "C" void IsolatedStorageFile_Remove_m1_4637 (IsolatedStorageFile_t1_397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Permissions.IsolatedStoragePermission System.IO.IsolatedStorage.IsolatedStorageFile::GetPermission(System.Security.PermissionSet)
extern "C" IsolatedStoragePermission_t1_1285 * IsolatedStorageFile_GetPermission_m1_4638 (IsolatedStorageFile_t1_397 * __this, PermissionSet_t1_563 * ___ps, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.IO.IsolatedStorage.IsolatedStorageFile::GetNameFromIdentity(System.Object)
extern "C" String_t* IsolatedStorageFile_GetNameFromIdentity_m1_4639 (IsolatedStorageFile_t1_397 * __this, Object_t * ___identity, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.IO.IsolatedStorage.IsolatedStorageFile::GetTypeFromEvidence(System.Security.Policy.Evidence,System.Type)
extern "C" Object_t * IsolatedStorageFile_GetTypeFromEvidence_m1_4640 (Object_t * __this /* static, unused */, Evidence_t1_398 * ___e, Type_t * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.IO.IsolatedStorage.IsolatedStorageFile::GetAssemblyIdentityFromEvidence(System.Security.Policy.Evidence)
extern "C" Object_t * IsolatedStorageFile_GetAssemblyIdentityFromEvidence_m1_4641 (Object_t * __this /* static, unused */, Evidence_t1_398 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.IO.IsolatedStorage.IsolatedStorageFile::GetDomainIdentityFromEvidence(System.Security.Policy.Evidence)
extern "C" Object_t * IsolatedStorageFile_GetDomainIdentityFromEvidence_m1_4642 (Object_t * __this /* static, unused */, Evidence_t1_398 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.IsolatedStorage.IsolatedStorageFile::SaveIdentities(System.String)
extern "C" void IsolatedStorageFile_SaveIdentities_m1_4643 (IsolatedStorageFile_t1_397 * __this, String_t* ___root, const MethodInfo* method) IL2CPP_METHOD_ATTR;
