﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.AccessControl.ObjectSecurity
struct ObjectSecurity_t1_1127;
// System.Security.Principal.IdentityReference
struct IdentityReference_t1_1120;
// System.Type
struct Type_t;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.String
struct String_t;
// System.Security.AccessControl.AccessRule
struct AccessRule_t1_1111;
// System.Security.AccessControl.AuditRule
struct AuditRule_t1_1119;
// System.Runtime.InteropServices.SafeHandle
struct SafeHandle_t1_88;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_AccessControl_AccessControlSections.h"
#include "mscorlib_System_Security_AccessControl_AccessControlModifica.h"

// System.Void System.Security.AccessControl.ObjectSecurity::.ctor()
extern "C" void ObjectSecurity__ctor_m1_9988 (ObjectSecurity_t1_1127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.ObjectSecurity::.ctor(System.Boolean,System.Boolean)
extern "C" void ObjectSecurity__ctor_m1_9989 (ObjectSecurity_t1_1127 * __this, bool ___isContainer, bool ___isDS, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.AccessControl.ObjectSecurity::get_AreAccessRulesCanonical()
extern "C" bool ObjectSecurity_get_AreAccessRulesCanonical_m1_9990 (ObjectSecurity_t1_1127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.AccessControl.ObjectSecurity::get_AreAccessRulesProtected()
extern "C" bool ObjectSecurity_get_AreAccessRulesProtected_m1_9991 (ObjectSecurity_t1_1127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.AccessControl.ObjectSecurity::get_AreAuditRulesCanonical()
extern "C" bool ObjectSecurity_get_AreAuditRulesCanonical_m1_9992 (ObjectSecurity_t1_1127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.AccessControl.ObjectSecurity::get_AreAuditRulesProtected()
extern "C" bool ObjectSecurity_get_AreAuditRulesProtected_m1_9993 (ObjectSecurity_t1_1127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.AccessControl.ObjectSecurity::get_AccessRulesModified()
extern "C" bool ObjectSecurity_get_AccessRulesModified_m1_9994 (ObjectSecurity_t1_1127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.ObjectSecurity::set_AccessRulesModified(System.Boolean)
extern "C" void ObjectSecurity_set_AccessRulesModified_m1_9995 (ObjectSecurity_t1_1127 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.AccessControl.ObjectSecurity::get_AuditRulesModified()
extern "C" bool ObjectSecurity_get_AuditRulesModified_m1_9996 (ObjectSecurity_t1_1127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.ObjectSecurity::set_AuditRulesModified(System.Boolean)
extern "C" void ObjectSecurity_set_AuditRulesModified_m1_9997 (ObjectSecurity_t1_1127 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.AccessControl.ObjectSecurity::get_GroupModified()
extern "C" bool ObjectSecurity_get_GroupModified_m1_9998 (ObjectSecurity_t1_1127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.ObjectSecurity::set_GroupModified(System.Boolean)
extern "C" void ObjectSecurity_set_GroupModified_m1_9999 (ObjectSecurity_t1_1127 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.AccessControl.ObjectSecurity::get_IsContainer()
extern "C" bool ObjectSecurity_get_IsContainer_m1_10000 (ObjectSecurity_t1_1127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.AccessControl.ObjectSecurity::get_IsDS()
extern "C" bool ObjectSecurity_get_IsDS_m1_10001 (ObjectSecurity_t1_1127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.AccessControl.ObjectSecurity::get_OwnerModified()
extern "C" bool ObjectSecurity_get_OwnerModified_m1_10002 (ObjectSecurity_t1_1127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.ObjectSecurity::set_OwnerModified(System.Boolean)
extern "C" void ObjectSecurity_set_OwnerModified_m1_10003 (ObjectSecurity_t1_1127 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Principal.IdentityReference System.Security.AccessControl.ObjectSecurity::GetGroup(System.Type)
extern "C" IdentityReference_t1_1120 * ObjectSecurity_GetGroup_m1_10004 (ObjectSecurity_t1_1127 * __this, Type_t * ___targetType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Principal.IdentityReference System.Security.AccessControl.ObjectSecurity::GetOwner(System.Type)
extern "C" IdentityReference_t1_1120 * ObjectSecurity_GetOwner_m1_10005 (ObjectSecurity_t1_1127 * __this, Type_t * ___targetType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.AccessControl.ObjectSecurity::GetSecurityDescriptorBinaryForm()
extern "C" ByteU5BU5D_t1_109* ObjectSecurity_GetSecurityDescriptorBinaryForm_m1_10006 (ObjectSecurity_t1_1127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.AccessControl.ObjectSecurity::GetSecurityDescriptorSddlForm(System.Security.AccessControl.AccessControlSections)
extern "C" String_t* ObjectSecurity_GetSecurityDescriptorSddlForm_m1_10007 (ObjectSecurity_t1_1127 * __this, int32_t ___includeSections, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.AccessControl.ObjectSecurity::IsSddlConversionSupported()
extern "C" bool ObjectSecurity_IsSddlConversionSupported_m1_10008 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.AccessControl.ObjectSecurity::ModifyAccessRule(System.Security.AccessControl.AccessControlModification,System.Security.AccessControl.AccessRule,System.Boolean&)
extern "C" bool ObjectSecurity_ModifyAccessRule_m1_10009 (ObjectSecurity_t1_1127 * __this, int32_t ___modification, AccessRule_t1_1111 * ___rule, bool* ___modified, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.AccessControl.ObjectSecurity::ModifyAuditRule(System.Security.AccessControl.AccessControlModification,System.Security.AccessControl.AuditRule,System.Boolean&)
extern "C" bool ObjectSecurity_ModifyAuditRule_m1_10010 (ObjectSecurity_t1_1127 * __this, int32_t ___modification, AuditRule_t1_1119 * ___rule, bool* ___modified, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.ObjectSecurity::PurgeAccessRules(System.Security.Principal.IdentityReference)
extern "C" void ObjectSecurity_PurgeAccessRules_m1_10011 (ObjectSecurity_t1_1127 * __this, IdentityReference_t1_1120 * ___identity, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.ObjectSecurity::PurgeAuditRules(System.Security.Principal.IdentityReference)
extern "C" void ObjectSecurity_PurgeAuditRules_m1_10012 (ObjectSecurity_t1_1127 * __this, IdentityReference_t1_1120 * ___identity, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.ObjectSecurity::SetAccessRuleProtection(System.Boolean,System.Boolean)
extern "C" void ObjectSecurity_SetAccessRuleProtection_m1_10013 (ObjectSecurity_t1_1127 * __this, bool ___isProtected, bool ___preserveInheritance, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.ObjectSecurity::SetAuditRuleProtection(System.Boolean,System.Boolean)
extern "C" void ObjectSecurity_SetAuditRuleProtection_m1_10014 (ObjectSecurity_t1_1127 * __this, bool ___isProtected, bool ___preserveInheritance, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.ObjectSecurity::SetGroup(System.Security.Principal.IdentityReference)
extern "C" void ObjectSecurity_SetGroup_m1_10015 (ObjectSecurity_t1_1127 * __this, IdentityReference_t1_1120 * ___identity, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.ObjectSecurity::SetOwner(System.Security.Principal.IdentityReference)
extern "C" void ObjectSecurity_SetOwner_m1_10016 (ObjectSecurity_t1_1127 * __this, IdentityReference_t1_1120 * ___identity, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.ObjectSecurity::SetSecurityDescriptorBinaryForm(System.Byte[])
extern "C" void ObjectSecurity_SetSecurityDescriptorBinaryForm_m1_10017 (ObjectSecurity_t1_1127 * __this, ByteU5BU5D_t1_109* ___binaryForm, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.ObjectSecurity::SetSecurityDescriptorBinaryForm(System.Byte[],System.Security.AccessControl.AccessControlSections)
extern "C" void ObjectSecurity_SetSecurityDescriptorBinaryForm_m1_10018 (ObjectSecurity_t1_1127 * __this, ByteU5BU5D_t1_109* ___binaryForm, int32_t ___includeSections, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.ObjectSecurity::SetSecurityDescriptorSddlForm(System.String)
extern "C" void ObjectSecurity_SetSecurityDescriptorSddlForm_m1_10019 (ObjectSecurity_t1_1127 * __this, String_t* ___sddlForm, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.ObjectSecurity::SetSecurityDescriptorSddlForm(System.String,System.Security.AccessControl.AccessControlSections)
extern "C" void ObjectSecurity_SetSecurityDescriptorSddlForm_m1_10020 (ObjectSecurity_t1_1127 * __this, String_t* ___sddlForm, int32_t ___includeSections, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.ObjectSecurity::Persist(System.Runtime.InteropServices.SafeHandle,System.Security.AccessControl.AccessControlSections)
extern "C" void ObjectSecurity_Persist_m1_10021 (ObjectSecurity_t1_1127 * __this, SafeHandle_t1_88 * ___handle, int32_t ___includeSections, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.ObjectSecurity::Persist(System.String,System.Security.AccessControl.AccessControlSections)
extern "C" void ObjectSecurity_Persist_m1_10022 (ObjectSecurity_t1_1127 * __this, String_t* ___name, int32_t ___includeSections, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.ObjectSecurity::Persist(System.Boolean,System.String,System.Security.AccessControl.AccessControlSections)
extern "C" void ObjectSecurity_Persist_m1_10023 (ObjectSecurity_t1_1127 * __this, bool ___enableOwnershipPrivilege, String_t* ___name, int32_t ___includeSections, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.ObjectSecurity::ReadLock()
extern "C" void ObjectSecurity_ReadLock_m1_10024 (ObjectSecurity_t1_1127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.ObjectSecurity::ReadUnlock()
extern "C" void ObjectSecurity_ReadUnlock_m1_10025 (ObjectSecurity_t1_1127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.ObjectSecurity::WriteLock()
extern "C" void ObjectSecurity_WriteLock_m1_10026 (ObjectSecurity_t1_1127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.ObjectSecurity::WriteUnlock()
extern "C" void ObjectSecurity_WriteUnlock_m1_10027 (ObjectSecurity_t1_1127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
