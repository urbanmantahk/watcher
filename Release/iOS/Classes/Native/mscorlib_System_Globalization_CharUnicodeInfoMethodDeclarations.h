﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Globalization.CharUnicodeInfo
struct CharUnicodeInfo_t1_356;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Globalization_UnicodeCategory.h"

// System.Void System.Globalization.CharUnicodeInfo::.ctor()
extern "C" void CharUnicodeInfo__ctor_m1_3822 (CharUnicodeInfo_t1_356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CharUnicodeInfo::GetDecimalDigitValue(System.Char)
extern "C" int32_t CharUnicodeInfo_GetDecimalDigitValue_m1_3823 (Object_t * __this /* static, unused */, uint16_t ___ch, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CharUnicodeInfo::GetDecimalDigitValue(System.String,System.Int32)
extern "C" int32_t CharUnicodeInfo_GetDecimalDigitValue_m1_3824 (Object_t * __this /* static, unused */, String_t* ___s, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CharUnicodeInfo::GetDigitValue(System.Char)
extern "C" int32_t CharUnicodeInfo_GetDigitValue_m1_3825 (Object_t * __this /* static, unused */, uint16_t ___ch, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CharUnicodeInfo::GetDigitValue(System.String,System.Int32)
extern "C" int32_t CharUnicodeInfo_GetDigitValue_m1_3826 (Object_t * __this /* static, unused */, String_t* ___s, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Globalization.CharUnicodeInfo::GetNumericValue(System.Char)
extern "C" double CharUnicodeInfo_GetNumericValue_m1_3827 (Object_t * __this /* static, unused */, uint16_t ___ch, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Globalization.CharUnicodeInfo::GetNumericValue(System.String,System.Int32)
extern "C" double CharUnicodeInfo_GetNumericValue_m1_3828 (Object_t * __this /* static, unused */, String_t* ___s, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.UnicodeCategory System.Globalization.CharUnicodeInfo::GetUnicodeCategory(System.Char)
extern "C" int32_t CharUnicodeInfo_GetUnicodeCategory_m1_3829 (Object_t * __this /* static, unused */, uint16_t ___ch, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.UnicodeCategory System.Globalization.CharUnicodeInfo::GetUnicodeCategory(System.String,System.Int32)
extern "C" int32_t CharUnicodeInfo_GetUnicodeCategory_m1_3830 (Object_t * __this /* static, unused */, String_t* ___s, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
