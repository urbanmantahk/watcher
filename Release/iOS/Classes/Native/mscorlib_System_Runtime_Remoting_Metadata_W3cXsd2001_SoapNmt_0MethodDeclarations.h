﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNmtokens
struct SoapNmtokens_t1_986;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNmtokens::.ctor()
extern "C" void SoapNmtokens__ctor_m1_8832 (SoapNmtokens_t1_986 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNmtokens::.ctor(System.String)
extern "C" void SoapNmtokens__ctor_m1_8833 (SoapNmtokens_t1_986 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNmtokens::get_Value()
extern "C" String_t* SoapNmtokens_get_Value_m1_8834 (SoapNmtokens_t1_986 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNmtokens::set_Value(System.String)
extern "C" void SoapNmtokens_set_Value_m1_8835 (SoapNmtokens_t1_986 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNmtokens::get_XsdType()
extern "C" String_t* SoapNmtokens_get_XsdType_m1_8836 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNmtokens::GetXsdType()
extern "C" String_t* SoapNmtokens_GetXsdType_m1_8837 (SoapNmtokens_t1_986 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNmtokens System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNmtokens::Parse(System.String)
extern "C" SoapNmtokens_t1_986 * SoapNmtokens_Parse_m1_8838 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNmtokens::ToString()
extern "C" String_t* SoapNmtokens_ToString_m1_8839 (SoapNmtokens_t1_986 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
