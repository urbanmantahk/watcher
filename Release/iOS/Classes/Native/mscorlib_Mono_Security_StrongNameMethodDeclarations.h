﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.StrongName
struct StrongName_t1_232;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Security.Cryptography.RSA
struct RSA_t1_175;
// System.String
struct String_t;
// Mono.Security.StrongName/StrongNameSignature
struct StrongNameSignature_t1_230;
// System.IO.Stream
struct Stream_t1_405;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_Mono_Security_StrongName_StrongNameOptions.h"
#include "mscorlib_System_Configuration_Assemblies_AssemblyHashAlgorit.h"

// System.Void Mono.Security.StrongName::.ctor()
extern "C" void StrongName__ctor_m1_2577 (StrongName_t1_232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.StrongName::.ctor(System.Int32)
extern "C" void StrongName__ctor_m1_2578 (StrongName_t1_232 * __this, int32_t ___keySize, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.StrongName::.ctor(System.Byte[])
extern "C" void StrongName__ctor_m1_2579 (StrongName_t1_232 * __this, ByteU5BU5D_t1_109* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.StrongName::.ctor(System.Security.Cryptography.RSA)
extern "C" void StrongName__ctor_m1_2580 (StrongName_t1_232 * __this, RSA_t1_175 * ___rsa, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.StrongName::.cctor()
extern "C" void StrongName__cctor_m1_2581 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.StrongName::InvalidateCache()
extern "C" void StrongName_InvalidateCache_m1_2582 (StrongName_t1_232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.StrongName::get_CanSign()
extern "C" bool StrongName_get_CanSign_m1_2583 (StrongName_t1_232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.RSA Mono.Security.StrongName::get_RSA()
extern "C" RSA_t1_175 * StrongName_get_RSA_m1_2584 (StrongName_t1_232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.StrongName::set_RSA(System.Security.Cryptography.RSA)
extern "C" void StrongName_set_RSA_m1_2585 (StrongName_t1_232 * __this, RSA_t1_175 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.StrongName::get_PublicKey()
extern "C" ByteU5BU5D_t1_109* StrongName_get_PublicKey_m1_2586 (StrongName_t1_232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.StrongName::get_PublicKeyToken()
extern "C" ByteU5BU5D_t1_109* StrongName_get_PublicKeyToken_m1_2587 (StrongName_t1_232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.StrongName::get_TokenAlgorithm()
extern "C" String_t* StrongName_get_TokenAlgorithm_m1_2588 (StrongName_t1_232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.StrongName::set_TokenAlgorithm(System.String)
extern "C" void StrongName_set_TokenAlgorithm_m1_2589 (StrongName_t1_232 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.StrongName::GetBytes()
extern "C" ByteU5BU5D_t1_109* StrongName_GetBytes_m1_2590 (StrongName_t1_232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Mono.Security.StrongName::RVAtoPosition(System.UInt32,System.Int32,System.Byte[])
extern "C" uint32_t StrongName_RVAtoPosition_m1_2591 (StrongName_t1_232 * __this, uint32_t ___r, int32_t ___sections, ByteU5BU5D_t1_109* ___headers, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.StrongName/StrongNameSignature Mono.Security.StrongName::StrongHash(System.IO.Stream,Mono.Security.StrongName/StrongNameOptions)
extern "C" StrongNameSignature_t1_230 * StrongName_StrongHash_m1_2592 (StrongName_t1_232 * __this, Stream_t1_405 * ___stream, int32_t ___options, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.StrongName::Hash(System.String)
extern "C" ByteU5BU5D_t1_109* StrongName_Hash_m1_2593 (StrongName_t1_232 * __this, String_t* ___fileName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.StrongName::Sign(System.String)
extern "C" bool StrongName_Sign_m1_2594 (StrongName_t1_232 * __this, String_t* ___fileName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.StrongName::Verify(System.String)
extern "C" bool StrongName_Verify_m1_2595 (StrongName_t1_232 * __this, String_t* ___fileName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.StrongName::Verify(System.IO.Stream)
extern "C" bool StrongName_Verify_m1_2596 (StrongName_t1_232 * __this, Stream_t1_405 * ___stream, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.StrongName::IsAssemblyStrongnamed(System.String)
extern "C" bool StrongName_IsAssemblyStrongnamed_m1_2597 (Object_t * __this /* static, unused */, String_t* ___assemblyName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.StrongName::VerifySignature(System.Byte[],System.Int32,System.Byte[],System.Byte[])
extern "C" bool StrongName_VerifySignature_m1_2598 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___publicKey, int32_t ___algorithm, ByteU5BU5D_t1_109* ___hash, ByteU5BU5D_t1_109* ___signature, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.StrongName::Verify(System.Security.Cryptography.RSA,System.Configuration.Assemblies.AssemblyHashAlgorithm,System.Byte[],System.Byte[])
extern "C" bool StrongName_Verify_m1_2599 (Object_t * __this /* static, unused */, RSA_t1_175 * ___rsa, int32_t ___algorithm, ByteU5BU5D_t1_109* ___hash, ByteU5BU5D_t1_109* ___signature, const MethodInfo* method) IL2CPP_METHOD_ATTR;
