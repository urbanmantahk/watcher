﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Principal.IdentityNotMappedException
struct IdentityNotMappedException_t1_1375;
// System.String
struct String_t;
// System.Exception
struct Exception_t1_33;
// System.Security.Principal.IdentityReferenceCollection
struct IdentityReferenceCollection_t1_1376;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.Security.Principal.IdentityNotMappedException::.ctor()
extern "C" void IdentityNotMappedException__ctor_m1_11792 (IdentityNotMappedException_t1_1375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Principal.IdentityNotMappedException::.ctor(System.String)
extern "C" void IdentityNotMappedException__ctor_m1_11793 (IdentityNotMappedException_t1_1375 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Principal.IdentityNotMappedException::.ctor(System.String,System.Exception)
extern "C" void IdentityNotMappedException__ctor_m1_11794 (IdentityNotMappedException_t1_1375 * __this, String_t* ___message, Exception_t1_33 * ___inner, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Principal.IdentityReferenceCollection System.Security.Principal.IdentityNotMappedException::get_UnmappedIdentities()
extern "C" IdentityReferenceCollection_t1_1376 * IdentityNotMappedException_get_UnmappedIdentities_m1_11795 (IdentityNotMappedException_t1_1375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Principal.IdentityNotMappedException::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void IdentityNotMappedException_GetObjectData_m1_11796 (IdentityNotMappedException_t1_1375 * __this, SerializationInfo_t1_293 * ___serializationInfo, StreamingContext_t1_1050  ___streamingContext, const MethodInfo* method) IL2CPP_METHOD_ATTR;
