﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExifLibrary.ExifVersion
struct ExifVersion_t8_101;
// System.String
struct String_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTag.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifInterOperability.h"

// System.Void ExifLibrary.ExifVersion::.ctor(ExifLibrary.ExifTag,System.String)
extern "C" void ExifVersion__ctor_m8_429 (ExifVersion_t8_101 * __this, int32_t ___tag, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ExifLibrary.ExifVersion::get__Value()
extern "C" Object_t * ExifVersion_get__Value_m8_430 (ExifVersion_t8_101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.ExifVersion::set__Value(System.Object)
extern "C" void ExifVersion_set__Value_m8_431 (ExifVersion_t8_101 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExifLibrary.ExifVersion::get_Value()
extern "C" String_t* ExifVersion_get_Value_m8_432 (ExifVersion_t8_101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.ExifVersion::set_Value(System.String)
extern "C" void ExifVersion_set_Value_m8_433 (ExifVersion_t8_101 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExifLibrary.ExifVersion::ToString()
extern "C" String_t* ExifVersion_ToString_m8_434 (ExifVersion_t8_101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.ExifInterOperability ExifLibrary.ExifVersion::get_Interoperability()
extern "C" ExifInterOperability_t8_113  ExifVersion_get_Interoperability_m8_435 (ExifVersion_t8_101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
