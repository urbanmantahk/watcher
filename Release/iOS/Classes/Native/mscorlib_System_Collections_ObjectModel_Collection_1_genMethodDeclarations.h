﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<System.Object>
struct Collection_1_t1_1938;
// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t1_1937;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Object
struct Object_t;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1_2210;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::.ctor()
extern "C" void Collection_1__ctor_m1_15296_gshared (Collection_1_t1_1938 * __this, const MethodInfo* method);
#define Collection_1__ctor_m1_15296(__this, method) (( void (*) (Collection_1_t1_1938 *, const MethodInfo*))Collection_1__ctor_m1_15296_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1__ctor_m1_15297_gshared (Collection_1_t1_1938 * __this, Object_t* ___list, const MethodInfo* method);
#define Collection_1__ctor_m1_15297(__this, ___list, method) (( void (*) (Collection_1_t1_1938 *, Object_t*, const MethodInfo*))Collection_1__ctor_m1_15297_gshared)(__this, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_15298_gshared (Collection_1_t1_1938 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_15298(__this, method) (( bool (*) (Collection_1_t1_1938 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_15298_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m1_15299_gshared (Collection_1_t1_1938 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m1_15299(__this, ___array, ___index, method) (( void (*) (Collection_1_t1_1938 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m1_15299_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m1_15300_gshared (Collection_1_t1_1938 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m1_15300(__this, method) (( Object_t * (*) (Collection_1_t1_1938 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m1_15300_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m1_15301_gshared (Collection_1_t1_1938 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m1_15301(__this, ___value, method) (( int32_t (*) (Collection_1_t1_1938 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m1_15301_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m1_15302_gshared (Collection_1_t1_1938 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m1_15302(__this, ___value, method) (( bool (*) (Collection_1_t1_1938 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m1_15302_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m1_15303_gshared (Collection_1_t1_1938 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m1_15303(__this, ___value, method) (( int32_t (*) (Collection_1_t1_1938 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m1_15303_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m1_15304_gshared (Collection_1_t1_1938 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m1_15304(__this, ___index, ___value, method) (( void (*) (Collection_1_t1_1938 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m1_15304_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m1_15305_gshared (Collection_1_t1_1938 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m1_15305(__this, ___value, method) (( void (*) (Collection_1_t1_1938 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m1_15305_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m1_15306_gshared (Collection_1_t1_1938 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m1_15306(__this, method) (( bool (*) (Collection_1_t1_1938 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m1_15306_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m1_15307_gshared (Collection_1_t1_1938 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m1_15307(__this, method) (( Object_t * (*) (Collection_1_t1_1938 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m1_15307_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m1_15308_gshared (Collection_1_t1_1938 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m1_15308(__this, method) (( bool (*) (Collection_1_t1_1938 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m1_15308_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m1_15309_gshared (Collection_1_t1_1938 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m1_15309(__this, method) (( bool (*) (Collection_1_t1_1938 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m1_15309_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m1_15310_gshared (Collection_1_t1_1938 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m1_15310(__this, ___index, method) (( Object_t * (*) (Collection_1_t1_1938 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m1_15310_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m1_15311_gshared (Collection_1_t1_1938 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m1_15311(__this, ___index, ___value, method) (( void (*) (Collection_1_t1_1938 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m1_15311_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::Add(T)
extern "C" void Collection_1_Add_m1_15312_gshared (Collection_1_t1_1938 * __this, Object_t * ___item, const MethodInfo* method);
#define Collection_1_Add_m1_15312(__this, ___item, method) (( void (*) (Collection_1_t1_1938 *, Object_t *, const MethodInfo*))Collection_1_Add_m1_15312_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::Clear()
extern "C" void Collection_1_Clear_m1_15313_gshared (Collection_1_t1_1938 * __this, const MethodInfo* method);
#define Collection_1_Clear_m1_15313(__this, method) (( void (*) (Collection_1_t1_1938 *, const MethodInfo*))Collection_1_Clear_m1_15313_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::ClearItems()
extern "C" void Collection_1_ClearItems_m1_15314_gshared (Collection_1_t1_1938 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m1_15314(__this, method) (( void (*) (Collection_1_t1_1938 *, const MethodInfo*))Collection_1_ClearItems_m1_15314_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::Contains(T)
extern "C" bool Collection_1_Contains_m1_15315_gshared (Collection_1_t1_1938 * __this, Object_t * ___item, const MethodInfo* method);
#define Collection_1_Contains_m1_15315(__this, ___item, method) (( bool (*) (Collection_1_t1_1938 *, Object_t *, const MethodInfo*))Collection_1_Contains_m1_15315_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m1_15316_gshared (Collection_1_t1_1938 * __this, ObjectU5BU5D_t1_272* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m1_15316(__this, ___array, ___index, method) (( void (*) (Collection_1_t1_1938 *, ObjectU5BU5D_t1_272*, int32_t, const MethodInfo*))Collection_1_CopyTo_m1_15316_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Object>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m1_15317_gshared (Collection_1_t1_1938 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m1_15317(__this, method) (( Object_t* (*) (Collection_1_t1_1938 *, const MethodInfo*))Collection_1_GetEnumerator_m1_15317_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Object>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m1_15318_gshared (Collection_1_t1_1938 * __this, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m1_15318(__this, ___item, method) (( int32_t (*) (Collection_1_t1_1938 *, Object_t *, const MethodInfo*))Collection_1_IndexOf_m1_15318_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m1_15319_gshared (Collection_1_t1_1938 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define Collection_1_Insert_m1_15319(__this, ___index, ___item, method) (( void (*) (Collection_1_t1_1938 *, int32_t, Object_t *, const MethodInfo*))Collection_1_Insert_m1_15319_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m1_15320_gshared (Collection_1_t1_1938 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m1_15320(__this, ___index, ___item, method) (( void (*) (Collection_1_t1_1938 *, int32_t, Object_t *, const MethodInfo*))Collection_1_InsertItem_m1_15320_gshared)(__this, ___index, ___item, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<System.Object>::get_Items()
extern "C" Object_t* Collection_1_get_Items_m1_15321_gshared (Collection_1_t1_1938 * __this, const MethodInfo* method);
#define Collection_1_get_Items_m1_15321(__this, method) (( Object_t* (*) (Collection_1_t1_1938 *, const MethodInfo*))Collection_1_get_Items_m1_15321_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::Remove(T)
extern "C" bool Collection_1_Remove_m1_15322_gshared (Collection_1_t1_1938 * __this, Object_t * ___item, const MethodInfo* method);
#define Collection_1_Remove_m1_15322(__this, ___item, method) (( bool (*) (Collection_1_t1_1938 *, Object_t *, const MethodInfo*))Collection_1_Remove_m1_15322_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m1_15323_gshared (Collection_1_t1_1938 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m1_15323(__this, ___index, method) (( void (*) (Collection_1_t1_1938 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m1_15323_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m1_15324_gshared (Collection_1_t1_1938 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m1_15324(__this, ___index, method) (( void (*) (Collection_1_t1_1938 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m1_15324_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Object>::get_Count()
extern "C" int32_t Collection_1_get_Count_m1_15325_gshared (Collection_1_t1_1938 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m1_15325(__this, method) (( int32_t (*) (Collection_1_t1_1938 *, const MethodInfo*))Collection_1_get_Count_m1_15325_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<System.Object>::get_Item(System.Int32)
extern "C" Object_t * Collection_1_get_Item_m1_15326_gshared (Collection_1_t1_1938 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m1_15326(__this, ___index, method) (( Object_t * (*) (Collection_1_t1_1938 *, int32_t, const MethodInfo*))Collection_1_get_Item_m1_15326_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m1_15327_gshared (Collection_1_t1_1938 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_set_Item_m1_15327(__this, ___index, ___value, method) (( void (*) (Collection_1_t1_1938 *, int32_t, Object_t *, const MethodInfo*))Collection_1_set_Item_m1_15327_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m1_15328_gshared (Collection_1_t1_1938 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define Collection_1_SetItem_m1_15328(__this, ___index, ___item, method) (( void (*) (Collection_1_t1_1938 *, int32_t, Object_t *, const MethodInfo*))Collection_1_SetItem_m1_15328_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m1_15329_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m1_15329(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m1_15329_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<System.Object>::ConvertItem(System.Object)
extern "C" Object_t * Collection_1_ConvertItem_m1_15330_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m1_15330(__this /* static, unused */, ___item, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m1_15330_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m1_15331_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m1_15331(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m1_15331_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m1_15332_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m1_15332(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m1_15332_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m1_15333_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m1_15333(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m1_15333_gshared)(__this /* static, unused */, ___list, method)
