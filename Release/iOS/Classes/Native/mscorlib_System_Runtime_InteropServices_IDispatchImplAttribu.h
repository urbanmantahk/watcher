﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Attribute.h"
#include "mscorlib_System_Runtime_InteropServices_IDispatchImplType.h"

// System.Runtime.InteropServices.IDispatchImplAttribute
struct  IDispatchImplAttribute_t1_799  : public Attribute_t1_2
{
	// System.Runtime.InteropServices.IDispatchImplType System.Runtime.InteropServices.IDispatchImplAttribute::Impl
	int32_t ___Impl_0;
};
