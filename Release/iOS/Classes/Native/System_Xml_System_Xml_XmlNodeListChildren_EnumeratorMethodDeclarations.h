﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XmlNodeListChildren/Enumerator
struct Enumerator_t4_151;
// System.Xml.IHasXmlChildNode
struct IHasXmlChildNode_t4_152;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Xml.XmlNodeListChildren/Enumerator::.ctor(System.Xml.IHasXmlChildNode)
extern "C" void Enumerator__ctor_m4_665 (Enumerator_t4_151 * __this, Object_t * ___parent, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Xml.XmlNodeListChildren/Enumerator::get_Current()
extern "C" Object_t * Enumerator_get_Current_m4_666 (Enumerator_t4_151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XmlNodeListChildren/Enumerator::MoveNext()
extern "C" bool Enumerator_MoveNext_m4_667 (Enumerator_t4_151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XmlNodeListChildren/Enumerator::Reset()
extern "C" void Enumerator_Reset_m4_668 (Enumerator_t4_151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
