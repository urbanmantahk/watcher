﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ArrayList/FixedSizeListWrapper
struct FixedSizeListWrapper_t1_270;
// System.Collections.IList
struct IList_t1_262;
// System.String
struct String_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.ArrayList/FixedSizeListWrapper::.ctor(System.Collections.IList)
extern "C" void FixedSizeListWrapper__ctor_m1_3019 (FixedSizeListWrapper_t1_270 * __this, Object_t * ___innerList, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.ArrayList/FixedSizeListWrapper::get_ErrorMessage()
extern "C" String_t* FixedSizeListWrapper_get_ErrorMessage_m1_3020 (FixedSizeListWrapper_t1_270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.ArrayList/FixedSizeListWrapper::get_IsFixedSize()
extern "C" bool FixedSizeListWrapper_get_IsFixedSize_m1_3021 (FixedSizeListWrapper_t1_270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.ArrayList/FixedSizeListWrapper::Add(System.Object)
extern "C" int32_t FixedSizeListWrapper_Add_m1_3022 (FixedSizeListWrapper_t1_270 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/FixedSizeListWrapper::Clear()
extern "C" void FixedSizeListWrapper_Clear_m1_3023 (FixedSizeListWrapper_t1_270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/FixedSizeListWrapper::Insert(System.Int32,System.Object)
extern "C" void FixedSizeListWrapper_Insert_m1_3024 (FixedSizeListWrapper_t1_270 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/FixedSizeListWrapper::Remove(System.Object)
extern "C" void FixedSizeListWrapper_Remove_m1_3025 (FixedSizeListWrapper_t1_270 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/FixedSizeListWrapper::RemoveAt(System.Int32)
extern "C" void FixedSizeListWrapper_RemoveAt_m1_3026 (FixedSizeListWrapper_t1_270 * __this, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
