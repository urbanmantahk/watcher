﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Serialization.Formatters.Binary.ObjectWriter/MetadataReference
struct MetadataReference_t1_1062;
// System.Runtime.Serialization.Formatters.Binary.TypeMetadata
struct TypeMetadata_t1_1058;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectWriter/MetadataReference::.ctor(System.Runtime.Serialization.Formatters.Binary.TypeMetadata,System.Int64)
extern "C" void MetadataReference__ctor_m1_9429 (MetadataReference_t1_1062 * __this, TypeMetadata_t1_1058 * ___metadata, int64_t ___id, const MethodInfo* method) IL2CPP_METHOD_ATTR;
