﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.Emit.ConstructorBuilder
struct ConstructorBuilder_t1_477;
// System.Reflection.Emit.TypeBuilder
struct TypeBuilder_t1_481;
// System.Type[]
struct TypeU5BU5D_t1_31;
// System.Type[][]
struct TypeU5BU5DU5BU5D_t1_483;
// System.Reflection.ParameterInfo[]
struct ParameterInfoU5BU5D_t1_1685;
// System.Object
struct Object_t;
// System.Reflection.Binder
struct Binder_t1_585;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Globalization.CultureInfo
struct CultureInfo_t1_277;
// System.Type
struct Type_t;
// System.String
struct String_t;
// System.Security.PermissionSet
struct PermissionSet_t1_563;
// System.Reflection.Emit.ParameterBuilder
struct ParameterBuilder_t1_545;
// System.Reflection.Emit.ILGenerator
struct ILGenerator_t1_480;
// System.Reflection.Emit.CustomAttributeBuilder
struct CustomAttributeBuilder_t1_487;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t1_478;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Reflection.Module
struct Module_t1_495;
// System.Diagnostics.SymbolStore.ISymbolWriter
struct ISymbolWriter_t1_536;
// System.Exception
struct Exception_t1_33;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_MethodAttributes.h"
#include "mscorlib_System_Reflection_CallingConventions.h"
#include "mscorlib_System_Guid.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_Reflection_MethodImplAttributes.h"
#include "mscorlib_System_Reflection_BindingFlags.h"
#include "mscorlib_System_RuntimeMethodHandle.h"
#include "mscorlib_System_Security_Permissions_SecurityAction.h"
#include "mscorlib_System_Reflection_ParameterAttributes.h"
#include "mscorlib_System_Reflection_Emit_MethodToken.h"

// System.Void System.Reflection.Emit.ConstructorBuilder::.ctor(System.Reflection.Emit.TypeBuilder,System.Reflection.MethodAttributes,System.Reflection.CallingConventions,System.Type[],System.Type[][],System.Type[][])
extern "C" void ConstructorBuilder__ctor_m1_5518 (ConstructorBuilder_t1_477 * __this, TypeBuilder_t1_481 * ___tb, int32_t ___attributes, int32_t ___callingConvention, TypeU5BU5D_t1_31* ___parameterTypes, TypeU5BU5DU5BU5D_t1_483* ___paramModReq, TypeU5BU5DU5BU5D_t1_483* ___paramModOpt, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ConstructorBuilder::System.Runtime.InteropServices._ConstructorBuilder.GetIDsOfNames(System.Guid&,System.IntPtr,System.UInt32,System.UInt32,System.IntPtr)
extern "C" void ConstructorBuilder_System_Runtime_InteropServices__ConstructorBuilder_GetIDsOfNames_m1_5519 (ConstructorBuilder_t1_477 * __this, Guid_t1_319 * ___riid, IntPtr_t ___rgszNames, uint32_t ___cNames, uint32_t ___lcid, IntPtr_t ___rgDispId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ConstructorBuilder::System.Runtime.InteropServices._ConstructorBuilder.GetTypeInfo(System.UInt32,System.UInt32,System.IntPtr)
extern "C" void ConstructorBuilder_System_Runtime_InteropServices__ConstructorBuilder_GetTypeInfo_m1_5520 (ConstructorBuilder_t1_477 * __this, uint32_t ___iTInfo, uint32_t ___lcid, IntPtr_t ___ppTInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ConstructorBuilder::System.Runtime.InteropServices._ConstructorBuilder.GetTypeInfoCount(System.UInt32&)
extern "C" void ConstructorBuilder_System_Runtime_InteropServices__ConstructorBuilder_GetTypeInfoCount_m1_5521 (ConstructorBuilder_t1_477 * __this, uint32_t* ___pcTInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ConstructorBuilder::System.Runtime.InteropServices._ConstructorBuilder.Invoke(System.UInt32,System.Guid&,System.UInt32,System.Int16,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr)
extern "C" void ConstructorBuilder_System_Runtime_InteropServices__ConstructorBuilder_Invoke_m1_5522 (ConstructorBuilder_t1_477 * __this, uint32_t ___dispIdMember, Guid_t1_319 * ___riid, uint32_t ___lcid, int16_t ___wFlags, IntPtr_t ___pDispParams, IntPtr_t ___pVarResult, IntPtr_t ___pExcepInfo, IntPtr_t ___puArgErr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.CallingConventions System.Reflection.Emit.ConstructorBuilder::get_CallingConvention()
extern "C" int32_t ConstructorBuilder_get_CallingConvention_m1_5523 (ConstructorBuilder_t1_477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.ConstructorBuilder::get_InitLocals()
extern "C" bool ConstructorBuilder_get_InitLocals_m1_5524 (ConstructorBuilder_t1_477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ConstructorBuilder::set_InitLocals(System.Boolean)
extern "C" void ConstructorBuilder_set_InitLocals_m1_5525 (ConstructorBuilder_t1_477 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.TypeBuilder System.Reflection.Emit.ConstructorBuilder::get_TypeBuilder()
extern "C" TypeBuilder_t1_481 * ConstructorBuilder_get_TypeBuilder_m1_5526 (ConstructorBuilder_t1_477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodImplAttributes System.Reflection.Emit.ConstructorBuilder::GetMethodImplementationFlags()
extern "C" int32_t ConstructorBuilder_GetMethodImplementationFlags_m1_5527 (ConstructorBuilder_t1_477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ParameterInfo[] System.Reflection.Emit.ConstructorBuilder::GetParameters()
extern "C" ParameterInfoU5BU5D_t1_1685* ConstructorBuilder_GetParameters_m1_5528 (ConstructorBuilder_t1_477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ParameterInfo[] System.Reflection.Emit.ConstructorBuilder::GetParametersInternal()
extern "C" ParameterInfoU5BU5D_t1_1685* ConstructorBuilder_GetParametersInternal_m1_5529 (ConstructorBuilder_t1_477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.ConstructorBuilder::GetParameterCount()
extern "C" int32_t ConstructorBuilder_GetParameterCount_m1_5530 (ConstructorBuilder_t1_477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Reflection.Emit.ConstructorBuilder::Invoke(System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo)
extern "C" Object_t * ConstructorBuilder_Invoke_m1_5531 (ConstructorBuilder_t1_477 * __this, Object_t * ___obj, int32_t ___invokeAttr, Binder_t1_585 * ___binder, ObjectU5BU5D_t1_272* ___parameters, CultureInfo_t1_277 * ___culture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Reflection.Emit.ConstructorBuilder::Invoke(System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo)
extern "C" Object_t * ConstructorBuilder_Invoke_m1_5532 (ConstructorBuilder_t1_477 * __this, int32_t ___invokeAttr, Binder_t1_585 * ___binder, ObjectU5BU5D_t1_272* ___parameters, CultureInfo_t1_277 * ___culture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.RuntimeMethodHandle System.Reflection.Emit.ConstructorBuilder::get_MethodHandle()
extern "C" RuntimeMethodHandle_t1_479  ConstructorBuilder_get_MethodHandle_m1_5533 (ConstructorBuilder_t1_477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodAttributes System.Reflection.Emit.ConstructorBuilder::get_Attributes()
extern "C" int32_t ConstructorBuilder_get_Attributes_m1_5534 (ConstructorBuilder_t1_477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.ConstructorBuilder::get_ReflectedType()
extern "C" Type_t * ConstructorBuilder_get_ReflectedType_m1_5535 (ConstructorBuilder_t1_477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.ConstructorBuilder::get_DeclaringType()
extern "C" Type_t * ConstructorBuilder_get_DeclaringType_m1_5536 (ConstructorBuilder_t1_477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.ConstructorBuilder::get_ReturnType()
extern "C" Type_t * ConstructorBuilder_get_ReturnType_m1_5537 (ConstructorBuilder_t1_477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Emit.ConstructorBuilder::get_Name()
extern "C" String_t* ConstructorBuilder_get_Name_m1_5538 (ConstructorBuilder_t1_477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Emit.ConstructorBuilder::get_Signature()
extern "C" String_t* ConstructorBuilder_get_Signature_m1_5539 (ConstructorBuilder_t1_477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ConstructorBuilder::AddDeclarativeSecurity(System.Security.Permissions.SecurityAction,System.Security.PermissionSet)
extern "C" void ConstructorBuilder_AddDeclarativeSecurity_m1_5540 (ConstructorBuilder_t1_477 * __this, int32_t ___action, PermissionSet_t1_563 * ___pset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.ParameterBuilder System.Reflection.Emit.ConstructorBuilder::DefineParameter(System.Int32,System.Reflection.ParameterAttributes,System.String)
extern "C" ParameterBuilder_t1_545 * ConstructorBuilder_DefineParameter_m1_5541 (ConstructorBuilder_t1_477 * __this, int32_t ___iSequence, int32_t ___attributes, String_t* ___strParamName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.ConstructorBuilder::IsDefined(System.Type,System.Boolean)
extern "C" bool ConstructorBuilder_IsDefined_m1_5542 (ConstructorBuilder_t1_477 * __this, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Reflection.Emit.ConstructorBuilder::GetCustomAttributes(System.Boolean)
extern "C" ObjectU5BU5D_t1_272* ConstructorBuilder_GetCustomAttributes_m1_5543 (ConstructorBuilder_t1_477 * __this, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Reflection.Emit.ConstructorBuilder::GetCustomAttributes(System.Type,System.Boolean)
extern "C" ObjectU5BU5D_t1_272* ConstructorBuilder_GetCustomAttributes_m1_5544 (ConstructorBuilder_t1_477 * __this, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.ILGenerator System.Reflection.Emit.ConstructorBuilder::GetILGenerator()
extern "C" ILGenerator_t1_480 * ConstructorBuilder_GetILGenerator_m1_5545 (ConstructorBuilder_t1_477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.ILGenerator System.Reflection.Emit.ConstructorBuilder::GetILGenerator(System.Int32)
extern "C" ILGenerator_t1_480 * ConstructorBuilder_GetILGenerator_m1_5546 (ConstructorBuilder_t1_477 * __this, int32_t ___streamSize, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ConstructorBuilder::SetCustomAttribute(System.Reflection.Emit.CustomAttributeBuilder)
extern "C" void ConstructorBuilder_SetCustomAttribute_m1_5547 (ConstructorBuilder_t1_477 * __this, CustomAttributeBuilder_t1_487 * ___customBuilder, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ConstructorBuilder::SetCustomAttribute(System.Reflection.ConstructorInfo,System.Byte[])
extern "C" void ConstructorBuilder_SetCustomAttribute_m1_5548 (ConstructorBuilder_t1_477 * __this, ConstructorInfo_t1_478 * ___con, ByteU5BU5D_t1_109* ___binaryAttribute, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ConstructorBuilder::SetImplementationFlags(System.Reflection.MethodImplAttributes)
extern "C" void ConstructorBuilder_SetImplementationFlags_m1_5549 (ConstructorBuilder_t1_477 * __this, int32_t ___attributes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Module System.Reflection.Emit.ConstructorBuilder::GetModule()
extern "C" Module_t1_495 * ConstructorBuilder_GetModule_m1_5550 (ConstructorBuilder_t1_477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.MethodToken System.Reflection.Emit.ConstructorBuilder::GetToken()
extern "C" MethodToken_t1_532  ConstructorBuilder_GetToken_m1_5551 (ConstructorBuilder_t1_477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ConstructorBuilder::SetSymCustomAttribute(System.String,System.Byte[])
extern "C" void ConstructorBuilder_SetSymCustomAttribute_m1_5552 (ConstructorBuilder_t1_477 * __this, String_t* ___name, ByteU5BU5D_t1_109* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Module System.Reflection.Emit.ConstructorBuilder::get_Module()
extern "C" Module_t1_495 * ConstructorBuilder_get_Module_m1_5553 (ConstructorBuilder_t1_477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Emit.ConstructorBuilder::ToString()
extern "C" String_t* ConstructorBuilder_ToString_m1_5554 (ConstructorBuilder_t1_477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ConstructorBuilder::fixup()
extern "C" void ConstructorBuilder_fixup_m1_5555 (ConstructorBuilder_t1_477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ConstructorBuilder::GenerateDebugInfo(System.Diagnostics.SymbolStore.ISymbolWriter)
extern "C" void ConstructorBuilder_GenerateDebugInfo_m1_5556 (ConstructorBuilder_t1_477 * __this, Object_t * ___symbolWriter, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.ConstructorBuilder::get_next_table_index(System.Object,System.Int32,System.Boolean)
extern "C" int32_t ConstructorBuilder_get_next_table_index_m1_5557 (ConstructorBuilder_t1_477 * __this, Object_t * ___obj, int32_t ___table, bool ___inc, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.ConstructorBuilder::get_IsCompilerContext()
extern "C" bool ConstructorBuilder_get_IsCompilerContext_m1_5558 (ConstructorBuilder_t1_477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ConstructorBuilder::RejectIfCreated()
extern "C" void ConstructorBuilder_RejectIfCreated_m1_5559 (ConstructorBuilder_t1_477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception System.Reflection.Emit.ConstructorBuilder::not_supported()
extern "C" Exception_t1_33 * ConstructorBuilder_not_supported_m1_5560 (ConstructorBuilder_t1_477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception System.Reflection.Emit.ConstructorBuilder::not_after_created()
extern "C" Exception_t1_33 * ConstructorBuilder_not_after_created_m1_5561 (ConstructorBuilder_t1_477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception System.Reflection.Emit.ConstructorBuilder::not_created()
extern "C" Exception_t1_33 * ConstructorBuilder_not_created_m1_5562 (ConstructorBuilder_t1_477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
