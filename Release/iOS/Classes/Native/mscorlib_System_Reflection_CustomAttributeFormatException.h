﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_FormatException.h"

// System.Reflection.CustomAttributeFormatException
struct  CustomAttributeFormatException_t1_591  : public FormatException_t1_592
{
};
