﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExifLibrary.ExifPointSubjectArea
struct ExifPointSubjectArea_t8_102;
// System.UInt16[]
struct UInt16U5BU5D_t1_1231;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTag.h"

// System.Void ExifLibrary.ExifPointSubjectArea::.ctor(ExifLibrary.ExifTag,System.UInt16[])
extern "C" void ExifPointSubjectArea__ctor_m8_436 (ExifPointSubjectArea_t8_102 * __this, int32_t ___tag, UInt16U5BU5D_t1_1231* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16[] ExifLibrary.ExifPointSubjectArea::get_Value()
extern "C" UInt16U5BU5D_t1_1231* ExifPointSubjectArea_get_Value_m8_437 (ExifPointSubjectArea_t8_102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.ExifPointSubjectArea::set_Value(System.UInt16[])
extern "C" void ExifPointSubjectArea_set_Value_m8_438 (ExifPointSubjectArea_t8_102 * __this, UInt16U5BU5D_t1_1231* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 ExifLibrary.ExifPointSubjectArea::get_X()
extern "C" uint16_t ExifPointSubjectArea_get_X_m8_439 (ExifPointSubjectArea_t8_102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.ExifPointSubjectArea::set_X(System.UInt16)
extern "C" void ExifPointSubjectArea_set_X_m8_440 (ExifPointSubjectArea_t8_102 * __this, uint16_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 ExifLibrary.ExifPointSubjectArea::get_Y()
extern "C" uint16_t ExifPointSubjectArea_get_Y_m8_441 (ExifPointSubjectArea_t8_102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.ExifPointSubjectArea::set_Y(System.UInt16)
extern "C" void ExifPointSubjectArea_set_Y_m8_442 (ExifPointSubjectArea_t8_102 * __this, uint16_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExifLibrary.ExifPointSubjectArea::ToString()
extern "C" String_t* ExifPointSubjectArea_ToString_m8_443 (ExifPointSubjectArea_t8_102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
