﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.NotificationServiceSettings/iOSSettings
struct iOSSettings_t8_268;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.NotificationServiceSettings/iOSSettings::.ctor()
extern "C" void iOSSettings__ctor_m8_1539 (iOSSettings_t8_268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.NotificationServiceSettings/iOSSettings::get_UserInfoKey()
extern "C" String_t* iOSSettings_get_UserInfoKey_m8_1540 (iOSSettings_t8_268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationServiceSettings/iOSSettings::set_UserInfoKey(System.String)
extern "C" void iOSSettings_set_UserInfoKey_m8_1541 (iOSSettings_t8_268 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
