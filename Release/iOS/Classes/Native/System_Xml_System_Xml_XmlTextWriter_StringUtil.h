﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Globalization.CultureInfo
struct CultureInfo_t1_277;
// System.Globalization.CompareInfo
struct CompareInfo_t1_282;

#include "mscorlib_System_Object.h"

// System.Xml.XmlTextWriter/StringUtil
struct  StringUtil_t4_179  : public Object_t
{
};
struct StringUtil_t4_179_StaticFields{
	// System.Globalization.CultureInfo System.Xml.XmlTextWriter/StringUtil::cul
	CultureInfo_t1_277 * ___cul_0;
	// System.Globalization.CompareInfo System.Xml.XmlTextWriter/StringUtil::cmp
	CompareInfo_t1_282 * ___cmp_1;
};
