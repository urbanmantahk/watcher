﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Math.Prime.Generator.PrimeGeneratorBase
struct PrimeGeneratorBase_t1_134;
// Mono.Math.Prime.PrimalityTest
struct PrimalityTest_t1_1619;
// Mono.Math.BigInteger
struct BigInteger_t1_139;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_Mono_Math_Prime_ConfidenceFactor.h"

// System.Void Mono.Math.Prime.Generator.PrimeGeneratorBase::.ctor()
extern "C" void PrimeGeneratorBase__ctor_m1_1746 (PrimeGeneratorBase_t1_134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.Prime.ConfidenceFactor Mono.Math.Prime.Generator.PrimeGeneratorBase::get_Confidence()
extern "C" int32_t PrimeGeneratorBase_get_Confidence_m1_1747 (PrimeGeneratorBase_t1_134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.Prime.PrimalityTest Mono.Math.Prime.Generator.PrimeGeneratorBase::get_PrimalityTest()
extern "C" PrimalityTest_t1_1619 * PrimeGeneratorBase_get_PrimalityTest_m1_1748 (PrimeGeneratorBase_t1_134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Math.Prime.Generator.PrimeGeneratorBase::get_TrialDivisionBounds()
extern "C" int32_t PrimeGeneratorBase_get_TrialDivisionBounds_m1_1749 (PrimeGeneratorBase_t1_134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Math.Prime.Generator.PrimeGeneratorBase::PostTrialDivisionTests(Mono.Math.BigInteger)
extern "C" bool PrimeGeneratorBase_PostTrialDivisionTests_m1_1750 (PrimeGeneratorBase_t1_134 * __this, BigInteger_t1_139 * ___bi, const MethodInfo* method) IL2CPP_METHOD_ATTR;
