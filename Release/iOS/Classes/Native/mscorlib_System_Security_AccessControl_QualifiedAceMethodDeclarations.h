﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.AccessControl.QualifiedAce
struct QualifiedAce_t1_1123;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_AccessControl_InheritanceFlags.h"
#include "mscorlib_System_Security_AccessControl_PropagationFlags.h"
#include "mscorlib_System_Security_AccessControl_AceQualifier.h"

// System.Void System.Security.AccessControl.QualifiedAce::.ctor(System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.AceQualifier,System.Boolean,System.Byte[])
extern "C" void QualifiedAce__ctor_m1_10033 (QualifiedAce_t1_1123 * __this, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___aceQualifier, bool ___isCallback, ByteU5BU5D_t1_109* ___opaque, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.AccessControl.AceQualifier System.Security.AccessControl.QualifiedAce::get_AceQualifier()
extern "C" int32_t QualifiedAce_get_AceQualifier_m1_10034 (QualifiedAce_t1_1123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.AccessControl.QualifiedAce::get_IsCallback()
extern "C" bool QualifiedAce_get_IsCallback_m1_10035 (QualifiedAce_t1_1123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.AccessControl.QualifiedAce::get_OpaqueLength()
extern "C" int32_t QualifiedAce_get_OpaqueLength_m1_10036 (QualifiedAce_t1_1123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.AccessControl.QualifiedAce::GetOpaque()
extern "C" ByteU5BU5D_t1_109* QualifiedAce_GetOpaque_m1_10037 (QualifiedAce_t1_1123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.QualifiedAce::SetOpaque(System.Byte[])
extern "C" void QualifiedAce_SetOpaque_m1_10038 (QualifiedAce_t1_1123 * __this, ByteU5BU5D_t1_109* ___opaque, const MethodInfo* method) IL2CPP_METHOD_ATTR;
