﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "UnityEngine_UnityEngine_Vector2.h"
#include "UnityEngine_UnityEngine_Rect.h"

// VoxelBusters.Utility.GUIScrollView
struct  GUIScrollView_t8_19  : public MonoBehaviour_t6_91
{
	// UnityEngine.Vector2 VoxelBusters.Utility.GUIScrollView::m_scrollPosition
	Vector2_t6_47  ___m_scrollPosition_2;
	// System.Single VoxelBusters.Utility.GUIScrollView::m_scrollSpeed
	float ___m_scrollSpeed_3;
	// UnityEngine.Rect VoxelBusters.Utility.GUIScrollView::m_rect
	Rect_t6_51  ___m_rect_4;
};
