﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Globalization_CalendarWeekRule.h"

// System.Globalization.CalendarWeekRule
struct  CalendarWeekRule_t1_340 
{
	// System.Int32 System.Globalization.CalendarWeekRule::value__
	int32_t ___value___1;
};
