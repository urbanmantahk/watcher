﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapName
struct SoapName_t1_982;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapName::.ctor()
extern "C" void SoapName__ctor_m1_8800 (SoapName_t1_982 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapName::.ctor(System.String)
extern "C" void SoapName__ctor_m1_8801 (SoapName_t1_982 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapName::get_Value()
extern "C" String_t* SoapName_get_Value_m1_8802 (SoapName_t1_982 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapName::set_Value(System.String)
extern "C" void SoapName_set_Value_m1_8803 (SoapName_t1_982 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapName::get_XsdType()
extern "C" String_t* SoapName_get_XsdType_m1_8804 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapName::GetXsdType()
extern "C" String_t* SoapName_GetXsdType_m1_8805 (SoapName_t1_982 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapName System.Runtime.Remoting.Metadata.W3cXsd2001.SoapName::Parse(System.String)
extern "C" SoapName_t1_982 * SoapName_Parse_m1_8806 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapName::ToString()
extern "C" String_t* SoapName_ToString_m1_8807 (SoapName_t1_982 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
