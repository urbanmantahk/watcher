﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.TimeSpan/Parser
struct Parser_t1_1605;
// System.String
struct String_t;
// System.Object
struct Object_t;
// System.TimeZone
struct TimeZone_t1_1606;
// System.Globalization.DaylightTime
struct DaylightTime_t1_367;
// System.CurrentSystemTimeZone
struct CurrentSystemTimeZone_t1_1609;
// System.Int64[]
struct Int64U5BU5D_t1_1665;
// System.String[]
struct StringU5BU5D_t1_238;
// System.TimeoutException
struct TimeoutException_t1_1610;
// System.Exception
struct Exception_t1_33;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;
// System.TypeInitializationException
struct TypeInitializationException_t1_1612;
// System.TypeLoadException
struct TypeLoadException_t1_1534;
// System.TypeUnloadedException
struct TypeUnloadedException_t1_1613;
// System.UnauthorizedAccessException
struct UnauthorizedAccessException_t1_1168;
// System.UnhandledExceptionEventArgs
struct UnhandledExceptionEventArgs_t1_1614;
// System.UnitySerializationHolder
struct UnitySerializationHolder_t1_1616;
// System.Type
struct Type_t;
// System.DBNull
struct DBNull_t1_1523;
// System.Reflection.Assembly
struct Assembly_t1_467;
// System.Reflection.Module
struct Module_t1_495;
// System.Version
struct Version_t1_578;
// System.WeakReference
struct WeakReference_t1_1014;
// System.__ComObject
struct __ComObject_t1_131;
// Mono.Math.Prime.PrimalityTest
struct PrimalityTest_t1_1619;
// Mono.Math.BigInteger
struct BigInteger_t1_139;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;
// System.Reflection.MemberFilter
struct MemberFilter_t1_32;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Reflection.ModuleResolveEventHandler
struct ModuleResolveEventHandler_t1_561;
// System.ResolveEventArgs
struct ResolveEventArgs_t1_1595;
// System.Reflection.GetterAdapter
struct GetterAdapter_t1_1620;
// System.Reflection.TypeFilter
struct TypeFilter_t1_616;
// System.Runtime.InteropServices.ObjectCreationDelegate
struct ObjectCreationDelegate_t1_1621;
// System.Runtime.Remoting.Contexts.CrossContextDelegate
struct CrossContextDelegate_t1_1622;
// System.Runtime.Remoting.Messaging.HeaderHandler
struct HeaderHandler_t1_1623;
// System.Runtime.Remoting.Messaging.Header[]
struct HeaderU5BU5D_t1_927;
// System.Runtime.Remoting.Messaging.MessageSurrogateFilter
struct MessageSurrogateFilter_t1_958;
// System.Threading.ContextCallback
struct ContextCallback_t1_1624;
// System.Threading.IOCompletionCallback
struct IOCompletionCallback_t1_1625;
// System.Threading.ParameterizedThreadStart
struct ParameterizedThreadStart_t1_1626;
// System.Threading.SendOrPostCallback
struct SendOrPostCallback_t1_1627;
// System.Threading.ThreadStart
struct ThreadStart_t1_1628;
// System.Threading.TimerCallback
struct TimerCallback_t1_1488;
// System.Threading.WaitCallback
struct WaitCallback_t1_1629;
// System.Threading.WaitOrTimerCallback
struct WaitOrTimerCallback_t1_1474;
// System.AppDomainInitializer
struct AppDomainInitializer_t1_1498;
// System.AssemblyLoadEventHandler
struct AssemblyLoadEventHandler_t1_1494;
// System.AssemblyLoadEventArgs
struct AssemblyLoadEventArgs_t1_1504;
// System.ConsoleCancelEventHandler
struct ConsoleCancelEventHandler_t1_1630;
// System.ConsoleCancelEventArgs
struct ConsoleCancelEventArgs_t1_1513;
// System.CrossAppDomainDelegate
struct CrossAppDomainDelegate_t1_1631;
// System.EventHandler
struct EventHandler_t1_461;
// System.EventArgs
struct EventArgs_t1_158;
// System.ResolveEventHandler
struct ResolveEventHandler_t1_1495;
// System.UnhandledExceptionEventHandler
struct UnhandledExceptionEventHandler_t1_1496;
// <PrivateImplementationDetails>
struct U3CPrivateImplementationDetailsU3E_t1_1661;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array.h"
#include "mscorlib_System_TimeSpan_Parser.h"
#include "mscorlib_System_TimeSpan_ParserMethodDeclarations.h"
#include "mscorlib_System_Void.h"
#include "mscorlib_System_String.h"
#include "mscorlib_System_ObjectMethodDeclarations.h"
#include "mscorlib_System_StringMethodDeclarations.h"
#include "mscorlib_System_Object.h"
#include "mscorlib_System_Int32.h"
#include "mscorlib_System_Boolean.h"
#include "mscorlib_System_CharMethodDeclarations.h"
#include "mscorlib_System_Char.h"
#include "mscorlib_System_Int64.h"
#include "mscorlib_System_TimeSpan.h"
#include "mscorlib_LocaleMethodDeclarations.h"
#include "mscorlib_System_OverflowExceptionMethodDeclarations.h"
#include "mscorlib_System_FormatExceptionMethodDeclarations.h"
#include "mscorlib_System_TimeSpanMethodDeclarations.h"
#include "mscorlib_System_OverflowException.h"
#include "mscorlib_System_FormatException.h"
#include "mscorlib_System_Collections_Generic_GenericComparer_1_gen_2MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_GenericEqualityComparer__2MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_GenericComparer_1_gen_2.h"
#include "mscorlib_System_Collections_Generic_GenericEqualityComparer__2.h"
#include "mscorlib_System_MonoTouchAOTHelper.h"
#include "mscorlib_System_MonoTouchAOTHelperMethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeExceptionMethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException.h"
#include "mscorlib_System_Double.h"
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
#include "mscorlib_System_ArgumentException.h"
#include "mscorlib_System_MathMethodDeclarations.h"
#include "mscorlib_System_DoubleMethodDeclarations.h"
#include "mscorlib_System_Int64MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullExceptionMethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException.h"
#include "mscorlib_System_Text_StringBuilderMethodDeclarations.h"
#include "mscorlib_System_Int32MethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder.h"
#include "mscorlib_System_TimeZone.h"
#include "mscorlib_System_TimeZoneMethodDeclarations.h"
#include "mscorlib_System_DateTimeMethodDeclarations.h"
#include "mscorlib_System_CurrentSystemTimeZoneMethodDeclarations.h"
#include "mscorlib_System_CurrentSystemTimeZone.h"
#include "mscorlib_System_DateTime.h"
#include "mscorlib_System_Globalization_DaylightTime.h"
#include "mscorlib_System_Globalization_DaylightTimeMethodDeclarations.h"
#include "mscorlib_System_DateTimeKind.h"
#include "mscorlib_System_CurrentSystemTimeZone_TimeZoneData.h"
#include "mscorlib_System_CurrentSystemTimeZone_TimeZoneDataMethodDeclarations.h"
#include "mscorlib_System_CurrentSystemTimeZone_TimeZoneNames.h"
#include "mscorlib_System_CurrentSystemTimeZone_TimeZoneNamesMethodDeclarations.h"
#include "mscorlib_System_Collections_HashtableMethodDeclarations.h"
#include "mscorlib_System_Collections_Hashtable.h"
#include "mscorlib_System_NotSupportedExceptionMethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_NotSupportedException.h"
#include "mscorlib_System_Threading_MonitorMethodDeclarations.h"
#include "mscorlib_System_TimeoutException.h"
#include "mscorlib_System_TimeoutExceptionMethodDeclarations.h"
#include "mscorlib_System_SystemExceptionMethodDeclarations.h"
#include "mscorlib_System_ExceptionMethodDeclarations.h"
#include "mscorlib_System_SystemException.h"
#include "mscorlib_System_Exception.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Runtime_Serialization_SerializationInfo.h"
#include "mscorlib_System_TypeCode.h"
#include "mscorlib_System_TypeCodeMethodDeclarations.h"
#include "mscorlib_System_TypeInitializationException.h"
#include "mscorlib_System_TypeInitializationExceptionMethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_SerializationInfoMethodDeclarations.h"
#include "mscorlib_System_TypeLoadException.h"
#include "mscorlib_System_TypeLoadExceptionMethodDeclarations.h"
#include "mscorlib_System_TypeMethodDeclarations.h"
#include "mscorlib_System_Type.h"
#include "mscorlib_System_RuntimeTypeHandle.h"
#include "mscorlib_System_TypeUnloadedException.h"
#include "mscorlib_System_TypeUnloadedExceptionMethodDeclarations.h"
#include "mscorlib_System_UnauthorizedAccessException.h"
#include "mscorlib_System_UnauthorizedAccessExceptionMethodDeclarations.h"
#include "mscorlib_System_UnhandledExceptionEventArgs.h"
#include "mscorlib_System_UnhandledExceptionEventArgsMethodDeclarations.h"
#include "mscorlib_System_EventArgsMethodDeclarations.h"
#include "mscorlib_System_EventArgs.h"
#include "mscorlib_System_UnitySerializationHolder_UnityType.h"
#include "mscorlib_System_UnitySerializationHolder_UnityTypeMethodDeclarations.h"
#include "mscorlib_System_UnitySerializationHolder.h"
#include "mscorlib_System_UnitySerializationHolderMethodDeclarations.h"
#include "mscorlib_System_Reflection_Assembly.h"
#include "mscorlib_System_Reflection_AssemblyMethodDeclarations.h"
#include "mscorlib_System_DBNull.h"
#include "mscorlib_System_Reflection_Module.h"
#include "mscorlib_System_Reflection_ModuleMethodDeclarations.h"
#include "mscorlib_System_DBNullMethodDeclarations.h"
#include "mscorlib_System_Variant.h"
#include "mscorlib_System_VariantMethodDeclarations.h"
#include "mscorlib_System_EnumMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_MarshalMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_BStrWrapperMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_UnknownWrapperMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_DispatchWrapperMethodDeclarations.h"
#include "mscorlib_System_NotImplementedExceptionMethodDeclarations.h"
#include "mscorlib_System_Int16.h"
#include "mscorlib_System_SByte.h"
#include "mscorlib_System_Byte.h"
#include "mscorlib_System_UInt16.h"
#include "mscorlib_System_UInt32.h"
#include "mscorlib_System_UInt64.h"
#include "mscorlib_System_Single.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_Runtime_InteropServices_BStrWrapper.h"
#include "mscorlib_System_Runtime_InteropServices_UnknownWrapper.h"
#include "mscorlib_System_Runtime_InteropServices_DispatchWrapper.h"
#include "mscorlib_System_NotImplementedException.h"
#include "mscorlib_System_IntPtrMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_VarEnum.h"
#include "mscorlib_System_BRECORD.h"
#include "mscorlib_System_BRECORDMethodDeclarations.h"
#include "mscorlib_System_Version.h"
#include "mscorlib_System_VersionMethodDeclarations.h"
#include "mscorlib_System_WeakReference.h"
#include "mscorlib_System_WeakReferenceMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_GCHandleMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_GCHandle.h"
#include "mscorlib_System_Runtime_InteropServices_GCHandleType.h"
#include "mscorlib_System___ComObject.h"
#include "mscorlib_System___ComObjectMethodDeclarations.h"
#include "mscorlib_System_MarshalByRefObjectMethodDeclarations.h"
#include "mscorlib_System_MarshalByRefObject.h"
#include "mscorlib_System_Guid.h"
#include "mscorlib_System_Runtime_InteropServices_ExtensibleClassFactoMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ObjectCreationDelegaMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_COMExceptionMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ObjectCreationDelega.h"
#include "mscorlib_System_Runtime_InteropServices_COMException.h"
#include "mscorlib_System_Runtime_InteropServices_InvalidComObjectExceMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_InvalidComObjectExce.h"
#include "mscorlib_System_GuidMethodDeclarations.h"
#include "mscorlib_Mono_Math_Prime_PrimalityTest.h"
#include "mscorlib_Mono_Math_Prime_PrimalityTestMethodDeclarations.h"
#include "mscorlib_Mono_Math_Prime_ConfidenceFactor.h"
#include "mscorlib_Mono_Math_BigInteger.h"
#include "mscorlib_System_AsyncCallback.h"
#include "mscorlib_System_Reflection_MemberFilter.h"
#include "mscorlib_System_Reflection_MemberFilterMethodDeclarations.h"
#include "mscorlib_System_Reflection_MemberInfo.h"
#include "mscorlib_System_Reflection_ModuleResolveEventHandler.h"
#include "mscorlib_System_Reflection_ModuleResolveEventHandlerMethodDeclarations.h"
#include "mscorlib_System_ResolveEventArgs.h"
#include "mscorlib_System_Reflection_GetterAdapter.h"
#include "mscorlib_System_Reflection_GetterAdapterMethodDeclarations.h"
#include "mscorlib_System_Reflection_TypeFilter.h"
#include "mscorlib_System_Reflection_TypeFilterMethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_Contexts_CrossContextDelega.h"
#include "mscorlib_System_Runtime_Remoting_Contexts_CrossContextDelegaMethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_HeaderHandler.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_HeaderHandlerMethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_Header.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_MessageSurrogateF.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_MessageSurrogateFMethodDeclarations.h"
#include "mscorlib_System_Threading_ContextCallback.h"
#include "mscorlib_System_Threading_ContextCallbackMethodDeclarations.h"
#include "mscorlib_System_Threading_IOCompletionCallback.h"
#include "mscorlib_System_Threading_IOCompletionCallbackMethodDeclarations.h"
#include "mscorlib_System_Threading_NativeOverlapped.h"
#include "mscorlib_System_Threading_ParameterizedThreadStart.h"
#include "mscorlib_System_Threading_ParameterizedThreadStartMethodDeclarations.h"
#include "mscorlib_System_Threading_SendOrPostCallback.h"
#include "mscorlib_System_Threading_SendOrPostCallbackMethodDeclarations.h"
#include "mscorlib_System_Threading_ThreadStart.h"
#include "mscorlib_System_Threading_ThreadStartMethodDeclarations.h"
#include "mscorlib_System_Threading_TimerCallback.h"
#include "mscorlib_System_Threading_TimerCallbackMethodDeclarations.h"
#include "mscorlib_System_Threading_WaitCallback.h"
#include "mscorlib_System_Threading_WaitCallbackMethodDeclarations.h"
#include "mscorlib_System_Threading_WaitOrTimerCallback.h"
#include "mscorlib_System_Threading_WaitOrTimerCallbackMethodDeclarations.h"
#include "mscorlib_System_AppDomainInitializer.h"
#include "mscorlib_System_AppDomainInitializerMethodDeclarations.h"
#include "mscorlib_System_AssemblyLoadEventHandler.h"
#include "mscorlib_System_AssemblyLoadEventHandlerMethodDeclarations.h"
#include "mscorlib_System_AssemblyLoadEventArgs.h"
#include "mscorlib_System_ConsoleCancelEventHandler.h"
#include "mscorlib_System_ConsoleCancelEventHandlerMethodDeclarations.h"
#include "mscorlib_System_ConsoleCancelEventArgs.h"
#include "mscorlib_System_CrossAppDomainDelegate.h"
#include "mscorlib_System_CrossAppDomainDelegateMethodDeclarations.h"
#include "mscorlib_System_EventHandler.h"
#include "mscorlib_System_EventHandlerMethodDeclarations.h"
#include "mscorlib_System_ResolveEventHandler.h"
#include "mscorlib_System_ResolveEventHandlerMethodDeclarations.h"
#include "mscorlib_System_UnhandledExceptionEventHandler.h"
#include "mscorlib_System_UnhandledExceptionEventHandlerMethodDeclarations.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU245.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU245MethodDeclarations.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242MethodDeclarations.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241MethodDeclarations.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU244.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU244MethodDeclarations.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU243.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU243MethodDeclarations.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_0.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_0MethodDeclarations.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU243_0.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU243_0MethodDeclarations.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242_0.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242_0MethodDeclarations.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU243_1.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU243_1MethodDeclarations.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU244_0.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU244_0MethodDeclarations.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU246.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU246MethodDeclarations.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_1.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_1MethodDeclarations.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_2.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_2MethodDeclarations.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU248.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU248MethodDeclarations.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU248_0.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU248_0MethodDeclarations.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU247.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU247MethodDeclarations.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_3.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_3MethodDeclarations.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU249.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU249MethodDeclarations.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242_1.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242_1MethodDeclarations.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242_2.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242_2MethodDeclarations.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_4.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_4MethodDeclarations.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU246_0.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU246_0MethodDeclarations.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_5.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_5MethodDeclarations.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU243_2.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU243_2MethodDeclarations.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_6.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_6MethodDeclarations.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU245_0.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU245_0MethodDeclarations.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_7.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_7MethodDeclarations.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242_3.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242_3MethodDeclarations.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_8.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_8MethodDeclarations.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3EMethodDeclarations.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.TimeSpan/Parser::.ctor(System.String)
extern "C" void Parser__ctor_m1_14609 (Parser_t1_1605 * __this, String_t* ___src, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___src;
		__this->____src_0 = L_0;
		String_t* L_1 = (__this->____src_0);
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m1_571(L_1, /*hidden argument*/NULL);
		__this->____length_2 = L_2;
		return;
	}
}
// System.Boolean System.TimeSpan/Parser::get_AtEnd()
extern "C" bool Parser_get_AtEnd_m1_14610 (Parser_t1_1605 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->____cur_1);
		int32_t L_1 = (__this->____length_2);
		return ((((int32_t)((((int32_t)L_0) < ((int32_t)L_1))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.TimeSpan/Parser::ParseWhiteSpace()
extern TypeInfo* Char_t1_15_il2cpp_TypeInfo_var;
extern "C" void Parser_ParseWhiteSpace_m1_14611 (Parser_t1_1605 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Char_t1_15_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(12);
		s_Il2CppMethodIntialized = true;
	}
	{
		goto IL_0013;
	}

IL_0005:
	{
		int32_t L_0 = (__this->____cur_1);
		__this->____cur_1 = ((int32_t)((int32_t)L_0+(int32_t)1));
	}

IL_0013:
	{
		bool L_1 = Parser_get_AtEnd_m1_14610(__this, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0034;
		}
	}
	{
		String_t* L_2 = (__this->____src_0);
		int32_t L_3 = (__this->____cur_1);
		IL2CPP_RUNTIME_CLASS_INIT(Char_t1_15_il2cpp_TypeInfo_var);
		bool L_4 = Char_IsWhiteSpace_m1_399(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0005;
		}
	}

IL_0034:
	{
		return;
	}
}
// System.Boolean System.TimeSpan/Parser::ParseSign()
extern "C" bool Parser_ParseSign_m1_14612 (Parser_t1_1605 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = 0;
		bool L_0 = Parser_get_AtEnd_m1_14610(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0035;
		}
	}
	{
		String_t* L_1 = (__this->____src_0);
		int32_t L_2 = (__this->____cur_1);
		NullCheck(L_1);
		uint16_t L_3 = String_get_Chars_m1_442(L_1, L_2, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)((int32_t)45)))))
		{
			goto IL_0035;
		}
	}
	{
		V_0 = 1;
		int32_t L_4 = (__this->____cur_1);
		__this->____cur_1 = ((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_0035:
	{
		bool L_5 = V_0;
		return L_5;
	}
}
// System.Int32 System.TimeSpan/Parser::ParseInt(System.Boolean)
extern TypeInfo* Char_t1_15_il2cpp_TypeInfo_var;
extern "C" int32_t Parser_ParseInt_m1_14613 (Parser_t1_1605 * __this, bool ___optional, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Char_t1_15_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(12);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		bool L_0 = ___optional;
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		bool L_1 = Parser_get_AtEnd_m1_14610(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		return 0;
	}

IL_0013:
	{
		V_0 = 0;
		V_1 = 0;
		goto IL_0048;
	}

IL_001c:
	{
		int32_t L_2 = V_0;
		if (((int64_t)L_2 * (int64_t)((int32_t)10) < (int64_t)kIl2CppInt32Min) || ((int64_t)L_2 * (int64_t)((int32_t)10) > (int64_t)kIl2CppInt32Max))
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_overflow_exception());
		String_t* L_3 = (__this->____src_0);
		int32_t L_4 = (__this->____cur_1);
		NullCheck(L_3);
		uint16_t L_5 = String_get_Chars_m1_442(L_3, L_4, /*hidden argument*/NULL);
		if (((int64_t)((int32_t)((int32_t)L_2*(int32_t)((int32_t)10))) + (int64_t)L_5 < (int64_t)kIl2CppInt32Min) || ((int64_t)((int32_t)((int32_t)L_2*(int32_t)((int32_t)10))) + (int64_t)L_5 > (int64_t)kIl2CppInt32Max))
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_overflow_exception());
		if (((int64_t)((int32_t)((int32_t)((int32_t)((int32_t)L_2*(int32_t)((int32_t)10)))+(int32_t)L_5)) - (int64_t)((int32_t)48) < (int64_t)kIl2CppInt32Min) || ((int64_t)((int32_t)((int32_t)((int32_t)((int32_t)L_2*(int32_t)((int32_t)10)))+(int32_t)L_5)) - (int64_t)((int32_t)48) > (int64_t)kIl2CppInt32Max))
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_overflow_exception());
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_2*(int32_t)((int32_t)10)))+(int32_t)L_5))-(int32_t)((int32_t)48)));
		int32_t L_6 = (__this->____cur_1);
		__this->____cur_1 = ((int32_t)((int32_t)L_6+(int32_t)1));
		int32_t L_7 = V_1;
		V_1 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0048:
	{
		bool L_8 = Parser_get_AtEnd_m1_14610(__this, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0069;
		}
	}
	{
		String_t* L_9 = (__this->____src_0);
		int32_t L_10 = (__this->____cur_1);
		IL2CPP_RUNTIME_CLASS_INIT(Char_t1_15_il2cpp_TypeInfo_var);
		bool L_11 = Char_IsDigit_m1_375(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_001c;
		}
	}

IL_0069:
	{
		bool L_12 = ___optional;
		if (L_12)
		{
			goto IL_007c;
		}
	}
	{
		int32_t L_13 = V_1;
		if (L_13)
		{
			goto IL_007c;
		}
	}
	{
		__this->___formatError_3 = 1;
	}

IL_007c:
	{
		int32_t L_14 = V_0;
		return L_14;
	}
}
// System.Boolean System.TimeSpan/Parser::ParseOptDot()
extern "C" bool Parser_ParseOptDot_m1_14614 (Parser_t1_1605 * __this, const MethodInfo* method)
{
	{
		bool L_0 = Parser_get_AtEnd_m1_14610(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		String_t* L_1 = (__this->____src_0);
		int32_t L_2 = (__this->____cur_1);
		NullCheck(L_1);
		uint16_t L_3 = String_get_Chars_m1_442(L_1, L_2, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)((int32_t)46)))))
		{
			goto IL_0035;
		}
	}
	{
		int32_t L_4 = (__this->____cur_1);
		__this->____cur_1 = ((int32_t)((int32_t)L_4+(int32_t)1));
		return 1;
	}

IL_0035:
	{
		return 0;
	}
}
// System.Void System.TimeSpan/Parser::ParseOptColon()
extern "C" void Parser_ParseOptColon_m1_14615 (Parser_t1_1605 * __this, const MethodInfo* method)
{
	{
		bool L_0 = Parser_get_AtEnd_m1_14610(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_003d;
		}
	}
	{
		String_t* L_1 = (__this->____src_0);
		int32_t L_2 = (__this->____cur_1);
		NullCheck(L_1);
		uint16_t L_3 = String_get_Chars_m1_442(L_1, L_2, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)((int32_t)58)))))
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_4 = (__this->____cur_1);
		__this->____cur_1 = ((int32_t)((int32_t)L_4+(int32_t)1));
		goto IL_003d;
	}

IL_0036:
	{
		__this->___formatError_3 = 1;
	}

IL_003d:
	{
		return;
	}
}
// System.Int64 System.TimeSpan/Parser::ParseTicks()
extern TypeInfo* Char_t1_15_il2cpp_TypeInfo_var;
extern "C" int64_t Parser_ParseTicks_m1_14616 (Parser_t1_1605 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Char_t1_15_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(12);
		s_Il2CppMethodIntialized = true;
	}
	int64_t V_0 = 0;
	int64_t V_1 = 0;
	bool V_2 = false;
	{
		V_0 = (((int64_t)((int64_t)((int32_t)1000000))));
		V_1 = (((int64_t)((int64_t)0)));
		V_2 = 0;
		goto IL_0041;
	}

IL_0011:
	{
		int64_t L_0 = V_1;
		String_t* L_1 = (__this->____src_0);
		int32_t L_2 = (__this->____cur_1);
		NullCheck(L_1);
		uint16_t L_3 = String_get_Chars_m1_442(L_1, L_2, /*hidden argument*/NULL);
		int64_t L_4 = V_0;
		V_1 = ((int64_t)((int64_t)L_0+(int64_t)((int64_t)((int64_t)(((int64_t)((int64_t)((int32_t)((int32_t)L_3-(int32_t)((int32_t)48))))))*(int64_t)L_4))));
		int32_t L_5 = (__this->____cur_1);
		__this->____cur_1 = ((int32_t)((int32_t)L_5+(int32_t)1));
		int64_t L_6 = V_0;
		V_0 = ((int64_t)((int64_t)L_6/(int64_t)(((int64_t)((int64_t)((int32_t)10))))));
		V_2 = 1;
	}

IL_0041:
	{
		int64_t L_7 = V_0;
		if ((((int64_t)L_7) <= ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_006a;
		}
	}
	{
		bool L_8 = Parser_get_AtEnd_m1_14610(__this, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_006a;
		}
	}
	{
		String_t* L_9 = (__this->____src_0);
		int32_t L_10 = (__this->____cur_1);
		IL2CPP_RUNTIME_CLASS_INIT(Char_t1_15_il2cpp_TypeInfo_var);
		bool L_11 = Char_IsDigit_m1_375(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_0011;
		}
	}

IL_006a:
	{
		bool L_12 = V_2;
		if (L_12)
		{
			goto IL_0077;
		}
	}
	{
		__this->___formatError_3 = 1;
	}

IL_0077:
	{
		int64_t L_13 = V_1;
		return L_13;
	}
}
// System.TimeSpan System.TimeSpan/Parser::Execute()
extern TypeInfo* OverflowException_t1_1590_il2cpp_TypeInfo_var;
extern TypeInfo* FormatException_t1_592_il2cpp_TypeInfo_var;
extern TypeInfo* TimeSpan_t1_368_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3620;
extern Il2CppCodeGenString* _stringLiteral3621;
extern "C" TimeSpan_t1_368  Parser_Execute_m1_14617 (Parser_t1_1605 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		OverflowException_t1_1590_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(13);
		FormatException_t1_592_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(16);
		TimeSpan_t1_368_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(327);
		_stringLiteral3620 = il2cpp_codegen_string_literal_from_index(3620);
		_stringLiteral3621 = il2cpp_codegen_string_literal_from_index(3621);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int64_t V_5 = 0;
	int64_t V_6 = 0;
	int64_t G_B18_0 = 0;
	{
		V_2 = 0;
		Parser_ParseWhiteSpace_m1_14611(__this, /*hidden argument*/NULL);
		bool L_0 = Parser_ParseSign_m1_14612(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Parser_ParseInt_m1_14613(__this, 0, /*hidden argument*/NULL);
		V_1 = L_1;
		bool L_2 = Parser_ParseOptDot_m1_14614(__this, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		int32_t L_3 = Parser_ParseInt_m1_14613(__this, 1, /*hidden argument*/NULL);
		V_2 = L_3;
		goto IL_003e;
	}

IL_002f:
	{
		bool L_4 = Parser_get_AtEnd_m1_14610(__this, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_003e;
		}
	}
	{
		int32_t L_5 = V_1;
		V_2 = L_5;
		V_1 = 0;
	}

IL_003e:
	{
		Parser_ParseOptColon_m1_14615(__this, /*hidden argument*/NULL);
		int32_t L_6 = Parser_ParseInt_m1_14613(__this, 1, /*hidden argument*/NULL);
		V_3 = L_6;
		Parser_ParseOptColon_m1_14615(__this, /*hidden argument*/NULL);
		int32_t L_7 = Parser_ParseInt_m1_14613(__this, 1, /*hidden argument*/NULL);
		V_4 = L_7;
		bool L_8 = Parser_ParseOptDot_m1_14614(__this, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0073;
		}
	}
	{
		int64_t L_9 = Parser_ParseTicks_m1_14616(__this, /*hidden argument*/NULL);
		V_5 = L_9;
		goto IL_0077;
	}

IL_0073:
	{
		V_5 = (((int64_t)((int64_t)0)));
	}

IL_0077:
	{
		Parser_ParseWhiteSpace_m1_14611(__this, /*hidden argument*/NULL);
		bool L_10 = Parser_get_AtEnd_m1_14610(__this, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_008f;
		}
	}
	{
		__this->___formatError_3 = 1;
	}

IL_008f:
	{
		int32_t L_11 = V_2;
		if ((((int32_t)L_11) > ((int32_t)((int32_t)23))))
		{
			goto IL_00a8;
		}
	}
	{
		int32_t L_12 = V_3;
		if ((((int32_t)L_12) > ((int32_t)((int32_t)59))))
		{
			goto IL_00a8;
		}
	}
	{
		int32_t L_13 = V_4;
		if ((((int32_t)L_13) <= ((int32_t)((int32_t)59))))
		{
			goto IL_00b8;
		}
	}

IL_00a8:
	{
		String_t* L_14 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral3620, /*hidden argument*/NULL);
		OverflowException_t1_1590 * L_15 = (OverflowException_t1_1590 *)il2cpp_codegen_object_new (OverflowException_t1_1590_il2cpp_TypeInfo_var);
		OverflowException__ctor_m1_14547(L_15, L_14, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_15);
	}

IL_00b8:
	{
		bool L_16 = (__this->___formatError_3);
		if (!L_16)
		{
			goto IL_00d3;
		}
	}
	{
		String_t* L_17 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral3621, /*hidden argument*/NULL);
		FormatException_t1_592 * L_18 = (FormatException_t1_592 *)il2cpp_codegen_object_new (FormatException_t1_592_il2cpp_TypeInfo_var);
		FormatException__ctor_m1_14100(L_18, L_17, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_18);
	}

IL_00d3:
	{
		int32_t L_19 = V_1;
		int32_t L_20 = V_2;
		int32_t L_21 = V_3;
		int32_t L_22 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t1_368_il2cpp_TypeInfo_var);
		int64_t L_23 = TimeSpan_CalculateTicks_m1_14623(NULL /*static, unused*/, L_19, L_20, L_21, L_22, 0, /*hidden argument*/NULL);
		V_6 = L_23;
		bool L_24 = V_0;
		if (!L_24)
		{
			goto IL_00f3;
		}
	}
	{
		int64_t L_25 = V_6;
		if (((int64_t)L_25 >= 0 && (int64_t)(((int64_t)((uint64_t)(((uint32_t)((uint32_t)0)))))) < kIl2CppInt64Min + (int64_t)L_25) || ((int64_t)L_25 < 0 && (int64_t)(((int64_t)((uint64_t)(((uint32_t)((uint32_t)0)))))) > kIl2CppInt64Max + (int64_t)L_25))
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_overflow_exception());
		int64_t L_26 = V_5;
		if (((int64_t)L_26 >= 0 && (int64_t)((int64_t)((int64_t)(((int64_t)((uint64_t)(((uint32_t)((uint32_t)0))))))-(int64_t)L_25)) < kIl2CppInt64Min + (int64_t)L_26) || ((int64_t)L_26 < 0 && (int64_t)((int64_t)((int64_t)(((int64_t)((uint64_t)(((uint32_t)((uint32_t)0))))))-(int64_t)L_25)) > kIl2CppInt64Max + (int64_t)L_26))
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_overflow_exception());
		G_B18_0 = ((int64_t)((int64_t)((int64_t)((int64_t)(((int64_t)((uint64_t)(((uint32_t)((uint32_t)0))))))-(int64_t)L_25))-(int64_t)L_26));
		goto IL_00f8;
	}

IL_00f3:
	{
		int64_t L_27 = V_6;
		int64_t L_28 = V_5;
		if (((int64_t)L_28 >= 0 && (int64_t)L_27 > kIl2CppInt64Max - (int64_t)L_28) || ((int64_t)L_28 < 0 && (int64_t)L_27 < (int64_t)kIl2CppInt64Min - (int64_t)L_28))
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_overflow_exception());
		G_B18_0 = ((int64_t)((int64_t)L_27+(int64_t)L_28));
	}

IL_00f8:
	{
		V_6 = G_B18_0;
		int64_t L_29 = V_6;
		TimeSpan_t1_368  L_30 = {0};
		TimeSpan__ctor_m1_14618(&L_30, L_29, /*hidden argument*/NULL);
		return L_30;
	}
}
// System.Void System.TimeSpan::.ctor(System.Int64)
extern "C" void TimeSpan__ctor_m1_14618 (TimeSpan_t1_368 * __this, int64_t ___ticks, const MethodInfo* method)
{
	{
		int64_t L_0 = ___ticks;
		__this->____ticks_8 = L_0;
		return;
	}
}
// System.Void System.TimeSpan::.ctor(System.Int32,System.Int32,System.Int32)
extern TypeInfo* TimeSpan_t1_368_il2cpp_TypeInfo_var;
extern "C" void TimeSpan__ctor_m1_14619 (TimeSpan_t1_368 * __this, int32_t ___hours, int32_t ___minutes, int32_t ___seconds, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TimeSpan_t1_368_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(327);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___hours;
		int32_t L_1 = ___minutes;
		int32_t L_2 = ___seconds;
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t1_368_il2cpp_TypeInfo_var);
		int64_t L_3 = TimeSpan_CalculateTicks_m1_14623(NULL /*static, unused*/, 0, L_0, L_1, L_2, 0, /*hidden argument*/NULL);
		__this->____ticks_8 = L_3;
		return;
	}
}
// System.Void System.TimeSpan::.ctor(System.Int32,System.Int32,System.Int32,System.Int32)
extern TypeInfo* TimeSpan_t1_368_il2cpp_TypeInfo_var;
extern "C" void TimeSpan__ctor_m1_14620 (TimeSpan_t1_368 * __this, int32_t ___days, int32_t ___hours, int32_t ___minutes, int32_t ___seconds, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TimeSpan_t1_368_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(327);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___days;
		int32_t L_1 = ___hours;
		int32_t L_2 = ___minutes;
		int32_t L_3 = ___seconds;
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t1_368_il2cpp_TypeInfo_var);
		int64_t L_4 = TimeSpan_CalculateTicks_m1_14623(NULL /*static, unused*/, L_0, L_1, L_2, L_3, 0, /*hidden argument*/NULL);
		__this->____ticks_8 = L_4;
		return;
	}
}
// System.Void System.TimeSpan::.ctor(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern TypeInfo* TimeSpan_t1_368_il2cpp_TypeInfo_var;
extern "C" void TimeSpan__ctor_m1_14621 (TimeSpan_t1_368 * __this, int32_t ___days, int32_t ___hours, int32_t ___minutes, int32_t ___seconds, int32_t ___milliseconds, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TimeSpan_t1_368_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(327);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___days;
		int32_t L_1 = ___hours;
		int32_t L_2 = ___minutes;
		int32_t L_3 = ___seconds;
		int32_t L_4 = ___milliseconds;
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t1_368_il2cpp_TypeInfo_var);
		int64_t L_5 = TimeSpan_CalculateTicks_m1_14623(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		__this->____ticks_8 = L_5;
		return;
	}
}
// System.Void System.TimeSpan::.cctor()
extern TypeInfo* TimeSpan_t1_368_il2cpp_TypeInfo_var;
extern TypeInfo* MonoTouchAOTHelper_t1_1577_il2cpp_TypeInfo_var;
extern TypeInfo* GenericComparer_1_t1_1799_il2cpp_TypeInfo_var;
extern TypeInfo* GenericEqualityComparer_1_t1_1800_il2cpp_TypeInfo_var;
extern const MethodInfo* GenericComparer_1__ctor_m1_14918_MethodInfo_var;
extern const MethodInfo* GenericEqualityComparer_1__ctor_m1_14919_MethodInfo_var;
extern "C" void TimeSpan__cctor_m1_14622 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TimeSpan_t1_368_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(327);
		MonoTouchAOTHelper_t1_1577_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1080);
		GenericComparer_1_t1_1799_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1108);
		GenericEqualityComparer_1_t1_1800_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1109);
		GenericComparer_1__ctor_m1_14918_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483717);
		GenericEqualityComparer_1__ctor_m1_14919_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483718);
		s_Il2CppMethodIntialized = true;
	}
	GenericComparer_1_t1_1799 * V_0 = {0};
	GenericEqualityComparer_1_t1_1800 * V_1 = {0};
	{
		TimeSpan_t1_368  L_0 = {0};
		TimeSpan__ctor_m1_14618(&L_0, ((int64_t)std::numeric_limits<int64_t>::max()), /*hidden argument*/NULL);
		((TimeSpan_t1_368_StaticFields*)TimeSpan_t1_368_il2cpp_TypeInfo_var->static_fields)->___MaxValue_5 = L_0;
		TimeSpan_t1_368  L_1 = {0};
		TimeSpan__ctor_m1_14618(&L_1, ((int64_t)std::numeric_limits<int64_t>::min()), /*hidden argument*/NULL);
		((TimeSpan_t1_368_StaticFields*)TimeSpan_t1_368_il2cpp_TypeInfo_var->static_fields)->___MinValue_6 = L_1;
		TimeSpan_t1_368  L_2 = {0};
		TimeSpan__ctor_m1_14618(&L_2, (((int64_t)((int64_t)0))), /*hidden argument*/NULL);
		((TimeSpan_t1_368_StaticFields*)TimeSpan_t1_368_il2cpp_TypeInfo_var->static_fields)->___Zero_7 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(MonoTouchAOTHelper_t1_1577_il2cpp_TypeInfo_var);
		bool L_3 = ((MonoTouchAOTHelper_t1_1577_StaticFields*)MonoTouchAOTHelper_t1_1577_il2cpp_TypeInfo_var->static_fields)->___FalseFlag_0;
		if (!L_3)
		{
			goto IL_0048;
		}
	}
	{
		GenericComparer_1_t1_1799 * L_4 = (GenericComparer_1_t1_1799 *)il2cpp_codegen_object_new (GenericComparer_1_t1_1799_il2cpp_TypeInfo_var);
		GenericComparer_1__ctor_m1_14918(L_4, /*hidden argument*/GenericComparer_1__ctor_m1_14918_MethodInfo_var);
		V_0 = L_4;
		GenericEqualityComparer_1_t1_1800 * L_5 = (GenericEqualityComparer_1_t1_1800 *)il2cpp_codegen_object_new (GenericEqualityComparer_1_t1_1800_il2cpp_TypeInfo_var);
		GenericEqualityComparer_1__ctor_m1_14919(L_5, /*hidden argument*/GenericEqualityComparer_1__ctor_m1_14919_MethodInfo_var);
		V_1 = L_5;
	}

IL_0048:
	{
		return;
	}
}
// System.Int64 System.TimeSpan::CalculateTicks(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3611;
extern "C" int64_t TimeSpan_CalculateTicks_m1_14623 (Object_t * __this /* static, unused */, int32_t ___days, int32_t ___hours, int32_t ___minutes, int32_t ___seconds, int32_t ___milliseconds, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		_stringLiteral3611 = il2cpp_codegen_string_literal_from_index(3611);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int64_t V_2 = 0;
	bool V_3 = false;
	int64_t V_4 = 0;
	int64_t V_5 = 0;
	int64_t V_6 = 0;
	int64_t V_7 = 0;
	{
		int32_t L_0 = ___hours;
		V_0 = ((int32_t)((int32_t)L_0*(int32_t)((int32_t)3600)));
		int32_t L_1 = ___minutes;
		V_1 = ((int32_t)((int32_t)L_1*(int32_t)((int32_t)60)));
		int32_t L_2 = V_0;
		int32_t L_3 = V_1;
		int32_t L_4 = ___seconds;
		int32_t L_5 = ___milliseconds;
		V_2 = ((int64_t)((int64_t)((int64_t)((int64_t)(((int64_t)((int64_t)((int32_t)((int32_t)((int32_t)((int32_t)L_2+(int32_t)L_3))+(int32_t)L_4)))))*(int64_t)(((int64_t)((int64_t)((int32_t)1000))))))+(int64_t)(((int64_t)((int64_t)L_5)))));
		int64_t L_6 = V_2;
		V_2 = ((int64_t)((int64_t)L_6*(int64_t)(((int64_t)((int64_t)((int32_t)10000))))));
		V_3 = 0;
		int32_t L_7 = ___days;
		if ((((int32_t)L_7) <= ((int32_t)0)))
		{
			goto IL_006a;
		}
	}
	{
		int32_t L_8 = ___days;
		V_4 = ((int64_t)((int64_t)((int64_t)864000000000LL)*(int64_t)(((int64_t)((int64_t)L_8)))));
		int64_t L_9 = V_2;
		if ((((int64_t)L_9) >= ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_005a;
		}
	}
	{
		int64_t L_10 = V_2;
		V_5 = L_10;
		int64_t L_11 = V_2;
		int64_t L_12 = V_4;
		V_2 = ((int64_t)((int64_t)L_11+(int64_t)L_12));
		int64_t L_13 = V_5;
		int64_t L_14 = V_2;
		V_3 = ((((int64_t)L_13) > ((int64_t)L_14))? 1 : 0);
		goto IL_0065;
	}

IL_005a:
	{
		int64_t L_15 = V_2;
		int64_t L_16 = V_4;
		V_2 = ((int64_t)((int64_t)L_15+(int64_t)L_16));
		int64_t L_17 = V_2;
		V_3 = ((((int64_t)L_17) < ((int64_t)(((int64_t)((int64_t)0)))))? 1 : 0);
	}

IL_0065:
	{
		goto IL_00a5;
	}

IL_006a:
	{
		int32_t L_18 = ___days;
		if ((((int32_t)L_18) >= ((int32_t)0)))
		{
			goto IL_00a5;
		}
	}
	{
		int32_t L_19 = ___days;
		V_6 = ((int64_t)((int64_t)((int64_t)864000000000LL)*(int64_t)(((int64_t)((int64_t)L_19)))));
		int64_t L_20 = V_2;
		if ((((int64_t)L_20) > ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_0097;
		}
	}
	{
		int64_t L_21 = V_2;
		int64_t L_22 = V_6;
		V_2 = ((int64_t)((int64_t)L_21+(int64_t)L_22));
		int64_t L_23 = V_2;
		V_3 = ((((int64_t)L_23) > ((int64_t)(((int64_t)((int64_t)0)))))? 1 : 0);
		goto IL_00a5;
	}

IL_0097:
	{
		int64_t L_24 = V_2;
		V_7 = L_24;
		int64_t L_25 = V_2;
		int64_t L_26 = V_6;
		V_2 = ((int64_t)((int64_t)L_25+(int64_t)L_26));
		int64_t L_27 = V_2;
		int64_t L_28 = V_7;
		V_3 = ((((int64_t)L_27) > ((int64_t)L_28))? 1 : 0);
	}

IL_00a5:
	{
		bool L_29 = V_3;
		if (!L_29)
		{
			goto IL_00bb;
		}
	}
	{
		String_t* L_30 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral3611, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t1_1501 * L_31 = (ArgumentOutOfRangeException_t1_1501 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1_13274(L_31, L_30, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_31);
	}

IL_00bb:
	{
		int64_t L_32 = V_2;
		return L_32;
	}
}
// System.Int32 System.TimeSpan::get_Days()
extern "C" int32_t TimeSpan_get_Days_m1_14624 (TimeSpan_t1_368 * __this, const MethodInfo* method)
{
	{
		int64_t L_0 = (__this->____ticks_8);
		return (((int32_t)((int32_t)((int64_t)((int64_t)L_0/(int64_t)((int64_t)864000000000LL))))));
	}
}
// System.Int32 System.TimeSpan::get_Hours()
extern "C" int32_t TimeSpan_get_Hours_m1_14625 (TimeSpan_t1_368 * __this, const MethodInfo* method)
{
	{
		int64_t L_0 = (__this->____ticks_8);
		return (((int32_t)((int32_t)((int64_t)((int64_t)((int64_t)((int64_t)L_0%(int64_t)((int64_t)864000000000LL)))/(int64_t)((int64_t)36000000000LL))))));
	}
}
// System.Int32 System.TimeSpan::get_Milliseconds()
extern "C" int32_t TimeSpan_get_Milliseconds_m1_14626 (TimeSpan_t1_368 * __this, const MethodInfo* method)
{
	{
		int64_t L_0 = (__this->____ticks_8);
		return (((int32_t)((int32_t)((int64_t)((int64_t)((int64_t)((int64_t)L_0%(int64_t)(((int64_t)((int64_t)((int32_t)10000000))))))/(int64_t)(((int64_t)((int64_t)((int32_t)10000)))))))));
	}
}
// System.Int32 System.TimeSpan::get_Minutes()
extern "C" int32_t TimeSpan_get_Minutes_m1_14627 (TimeSpan_t1_368 * __this, const MethodInfo* method)
{
	{
		int64_t L_0 = (__this->____ticks_8);
		return (((int32_t)((int32_t)((int64_t)((int64_t)((int64_t)((int64_t)L_0%(int64_t)((int64_t)36000000000LL)))/(int64_t)(((int64_t)((int64_t)((int32_t)600000000)))))))));
	}
}
// System.Int32 System.TimeSpan::get_Seconds()
extern "C" int32_t TimeSpan_get_Seconds_m1_14628 (TimeSpan_t1_368 * __this, const MethodInfo* method)
{
	{
		int64_t L_0 = (__this->____ticks_8);
		return (((int32_t)((int32_t)((int64_t)((int64_t)((int64_t)((int64_t)L_0%(int64_t)(((int64_t)((int64_t)((int32_t)600000000))))))/(int64_t)(((int64_t)((int64_t)((int32_t)10000000)))))))));
	}
}
// System.Int64 System.TimeSpan::get_Ticks()
extern "C" int64_t TimeSpan_get_Ticks_m1_14629 (TimeSpan_t1_368 * __this, const MethodInfo* method)
{
	{
		int64_t L_0 = (__this->____ticks_8);
		return L_0;
	}
}
// System.Double System.TimeSpan::get_TotalDays()
extern "C" double TimeSpan_get_TotalDays_m1_14630 (TimeSpan_t1_368 * __this, const MethodInfo* method)
{
	{
		int64_t L_0 = (__this->____ticks_8);
		return ((double)((double)(((double)((double)L_0)))/(double)(864000000000.0)));
	}
}
// System.Double System.TimeSpan::get_TotalHours()
extern "C" double TimeSpan_get_TotalHours_m1_14631 (TimeSpan_t1_368 * __this, const MethodInfo* method)
{
	{
		int64_t L_0 = (__this->____ticks_8);
		return ((double)((double)(((double)((double)L_0)))/(double)(36000000000.0)));
	}
}
// System.Double System.TimeSpan::get_TotalMilliseconds()
extern "C" double TimeSpan_get_TotalMilliseconds_m1_14632 (TimeSpan_t1_368 * __this, const MethodInfo* method)
{
	{
		int64_t L_0 = (__this->____ticks_8);
		return ((double)((double)(((double)((double)L_0)))/(double)(10000.0)));
	}
}
// System.Double System.TimeSpan::get_TotalMinutes()
extern "C" double TimeSpan_get_TotalMinutes_m1_14633 (TimeSpan_t1_368 * __this, const MethodInfo* method)
{
	{
		int64_t L_0 = (__this->____ticks_8);
		return ((double)((double)(((double)((double)L_0)))/(double)(600000000.0)));
	}
}
// System.Double System.TimeSpan::get_TotalSeconds()
extern "C" double TimeSpan_get_TotalSeconds_m1_14634 (TimeSpan_t1_368 * __this, const MethodInfo* method)
{
	{
		int64_t L_0 = (__this->____ticks_8);
		return ((double)((double)(((double)((double)L_0)))/(double)(10000000.0)));
	}
}
// System.TimeSpan System.TimeSpan::Add(System.TimeSpan)
extern TypeInfo* OverflowException_t1_1590_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3612;
extern "C" TimeSpan_t1_368  TimeSpan_Add_m1_14635 (TimeSpan_t1_368 * __this, TimeSpan_t1_368  ___ts, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		OverflowException_t1_1590_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(13);
		_stringLiteral3612 = il2cpp_codegen_string_literal_from_index(3612);
		s_Il2CppMethodIntialized = true;
	}
	TimeSpan_t1_368  V_0 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			int64_t L_0 = (__this->____ticks_8);
			int64_t L_1 = TimeSpan_get_Ticks_m1_14629((&___ts), /*hidden argument*/NULL);
			if (((int64_t)L_1 >= 0 && (int64_t)L_0 > kIl2CppInt64Max - (int64_t)L_1) || ((int64_t)L_1 < 0 && (int64_t)L_0 < (int64_t)kIl2CppInt64Min - (int64_t)L_1))
				il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_overflow_exception());
			TimeSpan_t1_368  L_2 = {0};
			TimeSpan__ctor_m1_14618(&L_2, ((int64_t)((int64_t)L_0+(int64_t)L_1)), /*hidden argument*/NULL);
			V_0 = L_2;
			goto IL_0034;
		}

IL_0019:
		{
			; // IL_0019: leave IL_0034
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1_33 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (OverflowException_t1_1590_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001e;
		throw e;
	}

CATCH_001e:
	{ // begin catch(System.OverflowException)
		{
			String_t* L_3 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral3612, /*hidden argument*/NULL);
			OverflowException_t1_1590 * L_4 = (OverflowException_t1_1590 *)il2cpp_codegen_object_new (OverflowException_t1_1590_il2cpp_TypeInfo_var);
			OverflowException__ctor_m1_14547(L_4, L_3, /*hidden argument*/NULL);
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
		}

IL_002f:
		{
			goto IL_0034;
		}
	} // end catch (depth: 1)

IL_0034:
	{
		TimeSpan_t1_368  L_5 = V_0;
		return L_5;
	}
}
// System.Int32 System.TimeSpan::Compare(System.TimeSpan,System.TimeSpan)
extern "C" int32_t TimeSpan_Compare_m1_14636 (Object_t * __this /* static, unused */, TimeSpan_t1_368  ___t1, TimeSpan_t1_368  ___t2, const MethodInfo* method)
{
	{
		int64_t L_0 = ((&___t1)->____ticks_8);
		int64_t L_1 = ((&___t2)->____ticks_8);
		if ((((int64_t)L_0) >= ((int64_t)L_1)))
		{
			goto IL_0015;
		}
	}
	{
		return (-1);
	}

IL_0015:
	{
		int64_t L_2 = ((&___t1)->____ticks_8);
		int64_t L_3 = ((&___t2)->____ticks_8);
		if ((((int64_t)L_2) <= ((int64_t)L_3)))
		{
			goto IL_002a;
		}
	}
	{
		return 1;
	}

IL_002a:
	{
		return 0;
	}
}
// System.Int32 System.TimeSpan::CompareTo(System.Object)
extern TypeInfo* TimeSpan_t1_368_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3613;
extern Il2CppCodeGenString* _stringLiteral61;
extern "C" int32_t TimeSpan_CompareTo_m1_14637 (TimeSpan_t1_368 * __this, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TimeSpan_t1_368_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(327);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		_stringLiteral3613 = il2cpp_codegen_string_literal_from_index(3613);
		_stringLiteral61 = il2cpp_codegen_string_literal_from_index(61);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___value;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return 1;
	}

IL_0008:
	{
		Object_t * L_1 = ___value;
		if (((Object_t *)IsInstSealed(L_1, TimeSpan_t1_368_il2cpp_TypeInfo_var)))
		{
			goto IL_0028;
		}
	}
	{
		String_t* L_2 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral3613, /*hidden argument*/NULL);
		ArgumentException_t1_1425 * L_3 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13262(L_3, L_2, _stringLiteral61, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0028:
	{
		Object_t * L_4 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t1_368_il2cpp_TypeInfo_var);
		int32_t L_5 = TimeSpan_Compare_m1_14636(NULL /*static, unused*/, (*(TimeSpan_t1_368 *)__this), ((*(TimeSpan_t1_368 *)((TimeSpan_t1_368 *)UnBox (L_4, TimeSpan_t1_368_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Int32 System.TimeSpan::CompareTo(System.TimeSpan)
extern TypeInfo* TimeSpan_t1_368_il2cpp_TypeInfo_var;
extern "C" int32_t TimeSpan_CompareTo_m1_14638 (TimeSpan_t1_368 * __this, TimeSpan_t1_368  ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TimeSpan_t1_368_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(327);
		s_Il2CppMethodIntialized = true;
	}
	{
		TimeSpan_t1_368  L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t1_368_il2cpp_TypeInfo_var);
		int32_t L_1 = TimeSpan_Compare_m1_14636(NULL /*static, unused*/, (*(TimeSpan_t1_368 *)__this), L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.TimeSpan::Equals(System.TimeSpan)
extern "C" bool TimeSpan_Equals_m1_14639 (TimeSpan_t1_368 * __this, TimeSpan_t1_368  ___obj, const MethodInfo* method)
{
	{
		int64_t L_0 = ((&___obj)->____ticks_8);
		int64_t L_1 = (__this->____ticks_8);
		return ((((int64_t)L_0) == ((int64_t)L_1))? 1 : 0);
	}
}
// System.TimeSpan System.TimeSpan::Duration()
extern TypeInfo* OverflowException_t1_1590_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3614;
extern "C" TimeSpan_t1_368  TimeSpan_Duration_m1_14640 (TimeSpan_t1_368 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		OverflowException_t1_1590_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(13);
		_stringLiteral3614 = il2cpp_codegen_string_literal_from_index(3614);
		s_Il2CppMethodIntialized = true;
	}
	TimeSpan_t1_368  V_0 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			int64_t L_0 = (__this->____ticks_8);
			int64_t L_1 = llabs(L_0);
			TimeSpan_t1_368  L_2 = {0};
			TimeSpan__ctor_m1_14618(&L_2, L_1, /*hidden argument*/NULL);
			V_0 = L_2;
			goto IL_0031;
		}

IL_0016:
		{
			; // IL_0016: leave IL_0031
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1_33 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (OverflowException_t1_1590_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001b;
		throw e;
	}

CATCH_001b:
	{ // begin catch(System.OverflowException)
		{
			String_t* L_3 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral3614, /*hidden argument*/NULL);
			OverflowException_t1_1590 * L_4 = (OverflowException_t1_1590 *)il2cpp_codegen_object_new (OverflowException_t1_1590_il2cpp_TypeInfo_var);
			OverflowException__ctor_m1_14547(L_4, L_3, /*hidden argument*/NULL);
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
		}

IL_002c:
		{
			goto IL_0031;
		}
	} // end catch (depth: 1)

IL_0031:
	{
		TimeSpan_t1_368  L_5 = V_0;
		return L_5;
	}
}
// System.Boolean System.TimeSpan::Equals(System.Object)
extern TypeInfo* TimeSpan_t1_368_il2cpp_TypeInfo_var;
extern "C" bool TimeSpan_Equals_m1_14641 (TimeSpan_t1_368 * __this, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TimeSpan_t1_368_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(327);
		s_Il2CppMethodIntialized = true;
	}
	TimeSpan_t1_368  V_0 = {0};
	{
		Object_t * L_0 = ___value;
		if (((Object_t *)IsInstSealed(L_0, TimeSpan_t1_368_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		int64_t L_1 = (__this->____ticks_8);
		Object_t * L_2 = ___value;
		V_0 = ((*(TimeSpan_t1_368 *)((TimeSpan_t1_368 *)UnBox (L_2, TimeSpan_t1_368_il2cpp_TypeInfo_var))));
		int64_t L_3 = ((&V_0)->____ticks_8);
		return ((((int64_t)L_1) == ((int64_t)L_3))? 1 : 0);
	}
}
// System.Boolean System.TimeSpan::Equals(System.TimeSpan,System.TimeSpan)
extern "C" bool TimeSpan_Equals_m1_14642 (Object_t * __this /* static, unused */, TimeSpan_t1_368  ___t1, TimeSpan_t1_368  ___t2, const MethodInfo* method)
{
	{
		int64_t L_0 = ((&___t1)->____ticks_8);
		int64_t L_1 = ((&___t2)->____ticks_8);
		return ((((int64_t)L_0) == ((int64_t)L_1))? 1 : 0);
	}
}
// System.TimeSpan System.TimeSpan::FromDays(System.Double)
extern TypeInfo* TimeSpan_t1_368_il2cpp_TypeInfo_var;
extern "C" TimeSpan_t1_368  TimeSpan_FromDays_m1_14643 (Object_t * __this /* static, unused */, double ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TimeSpan_t1_368_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(327);
		s_Il2CppMethodIntialized = true;
	}
	{
		double L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t1_368_il2cpp_TypeInfo_var);
		TimeSpan_t1_368  L_1 = TimeSpan_From_m1_14648(NULL /*static, unused*/, L_0, ((int64_t)864000000000LL), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.TimeSpan System.TimeSpan::FromHours(System.Double)
extern TypeInfo* TimeSpan_t1_368_il2cpp_TypeInfo_var;
extern "C" TimeSpan_t1_368  TimeSpan_FromHours_m1_14644 (Object_t * __this /* static, unused */, double ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TimeSpan_t1_368_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(327);
		s_Il2CppMethodIntialized = true;
	}
	{
		double L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t1_368_il2cpp_TypeInfo_var);
		TimeSpan_t1_368  L_1 = TimeSpan_From_m1_14648(NULL /*static, unused*/, L_0, ((int64_t)36000000000LL), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.TimeSpan System.TimeSpan::FromMinutes(System.Double)
extern TypeInfo* TimeSpan_t1_368_il2cpp_TypeInfo_var;
extern "C" TimeSpan_t1_368  TimeSpan_FromMinutes_m1_14645 (Object_t * __this /* static, unused */, double ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TimeSpan_t1_368_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(327);
		s_Il2CppMethodIntialized = true;
	}
	{
		double L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t1_368_il2cpp_TypeInfo_var);
		TimeSpan_t1_368  L_1 = TimeSpan_From_m1_14648(NULL /*static, unused*/, L_0, (((int64_t)((int64_t)((int32_t)600000000)))), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.TimeSpan System.TimeSpan::FromSeconds(System.Double)
extern TypeInfo* TimeSpan_t1_368_il2cpp_TypeInfo_var;
extern "C" TimeSpan_t1_368  TimeSpan_FromSeconds_m1_14646 (Object_t * __this /* static, unused */, double ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TimeSpan_t1_368_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(327);
		s_Il2CppMethodIntialized = true;
	}
	{
		double L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t1_368_il2cpp_TypeInfo_var);
		TimeSpan_t1_368  L_1 = TimeSpan_From_m1_14648(NULL /*static, unused*/, L_0, (((int64_t)((int64_t)((int32_t)10000000)))), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.TimeSpan System.TimeSpan::FromMilliseconds(System.Double)
extern TypeInfo* TimeSpan_t1_368_il2cpp_TypeInfo_var;
extern "C" TimeSpan_t1_368  TimeSpan_FromMilliseconds_m1_14647 (Object_t * __this /* static, unused */, double ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TimeSpan_t1_368_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(327);
		s_Il2CppMethodIntialized = true;
	}
	{
		double L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t1_368_il2cpp_TypeInfo_var);
		TimeSpan_t1_368  L_1 = TimeSpan_From_m1_14648(NULL /*static, unused*/, L_0, (((int64_t)((int64_t)((int32_t)10000)))), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.TimeSpan System.TimeSpan::From(System.Double,System.Int64)
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern TypeInfo* TimeSpan_t1_368_il2cpp_TypeInfo_var;
extern TypeInfo* OverflowException_t1_1590_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3615;
extern Il2CppCodeGenString* _stringLiteral61;
extern Il2CppCodeGenString* _stringLiteral3616;
extern Il2CppCodeGenString* _stringLiteral3612;
extern "C" TimeSpan_t1_368  TimeSpan_From_m1_14648 (Object_t * __this /* static, unused */, double ___value, int64_t ___tickMultiplicator, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		TimeSpan_t1_368_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(327);
		OverflowException_t1_1590_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(13);
		_stringLiteral3615 = il2cpp_codegen_string_literal_from_index(3615);
		_stringLiteral61 = il2cpp_codegen_string_literal_from_index(61);
		_stringLiteral3616 = il2cpp_codegen_string_literal_from_index(3616);
		_stringLiteral3612 = il2cpp_codegen_string_literal_from_index(3612);
		s_Il2CppMethodIntialized = true;
	}
	int64_t V_0 = 0;
	TimeSpan_t1_368  V_1 = {0};
	TimeSpan_t1_368  V_2 = {0};
	TimeSpan_t1_368  V_3 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		double L_0 = ___value;
		bool L_1 = Double_IsNaN_m1_659(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		String_t* L_2 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral3615, /*hidden argument*/NULL);
		ArgumentException_t1_1425 * L_3 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13262(L_3, L_2, _stringLiteral61, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0020:
	{
		double L_4 = ___value;
		bool L_5 = Double_IsNegativeInfinity_m1_660(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_005e;
		}
	}
	{
		double L_6 = ___value;
		bool L_7 = Double_IsPositiveInfinity_m1_661(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_005e;
		}
	}
	{
		double L_8 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t1_368_il2cpp_TypeInfo_var);
		TimeSpan_t1_368  L_9 = ((TimeSpan_t1_368_StaticFields*)TimeSpan_t1_368_il2cpp_TypeInfo_var->static_fields)->___MinValue_6;
		V_1 = L_9;
		int64_t L_10 = TimeSpan_get_Ticks_m1_14629((&V_1), /*hidden argument*/NULL);
		if ((((double)L_8) < ((double)(((double)((double)L_10))))))
		{
			goto IL_005e;
		}
	}
	{
		double L_11 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t1_368_il2cpp_TypeInfo_var);
		TimeSpan_t1_368  L_12 = ((TimeSpan_t1_368_StaticFields*)TimeSpan_t1_368_il2cpp_TypeInfo_var->static_fields)->___MaxValue_5;
		V_2 = L_12;
		int64_t L_13 = TimeSpan_get_Ticks_m1_14629((&V_2), /*hidden argument*/NULL);
		if ((!(((double)L_11) > ((double)(((double)((double)L_13)))))))
		{
			goto IL_006e;
		}
	}

IL_005e:
	{
		String_t* L_14 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral3616, /*hidden argument*/NULL);
		OverflowException_t1_1590 * L_15 = (OverflowException_t1_1590 *)il2cpp_codegen_object_new (OverflowException_t1_1590_il2cpp_TypeInfo_var);
		OverflowException__ctor_m1_14547(L_15, L_14, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_15);
	}

IL_006e:
	try
	{ // begin try (depth: 1)
		{
			double L_16 = ___value;
			int64_t L_17 = ___tickMultiplicator;
			___value = ((double)((double)L_16*(double)(((double)((double)((int64_t)((int64_t)L_17/(int64_t)(((int64_t)((int64_t)((int32_t)10000)))))))))));
			double L_18 = ___value;
			double L_19 = bankers_round(L_18);
			if (L_19 > (double)(std::numeric_limits<int64_t>::max())) il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_overflow_exception());
			V_0 = (((int64_t)((int64_t)L_19)));
			int64_t L_20 = V_0;
			if (il2cpp_codegen_check_mul_overflow_i64((int64_t)L_20, (int64_t)(((int64_t)((int64_t)((int32_t)10000)))), kIl2CppInt64Min, kIl2CppInt64Max))
				il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_overflow_exception());
			TimeSpan_t1_368  L_21 = {0};
			TimeSpan__ctor_m1_14618(&L_21, ((int64_t)((int64_t)L_20*(int64_t)(((int64_t)((int64_t)((int32_t)10000)))))), /*hidden argument*/NULL);
			V_3 = L_21;
			goto IL_00b1;
		}

IL_0096:
		{
			; // IL_0096: leave IL_00b1
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1_33 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (OverflowException_t1_1590_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_009b;
		throw e;
	}

CATCH_009b:
	{ // begin catch(System.OverflowException)
		{
			String_t* L_22 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral3612, /*hidden argument*/NULL);
			OverflowException_t1_1590 * L_23 = (OverflowException_t1_1590 *)il2cpp_codegen_object_new (OverflowException_t1_1590_il2cpp_TypeInfo_var);
			OverflowException__ctor_m1_14547(L_23, L_22, /*hidden argument*/NULL);
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_23);
		}

IL_00ac:
		{
			goto IL_00b1;
		}
	} // end catch (depth: 1)

IL_00b1:
	{
		TimeSpan_t1_368  L_24 = V_3;
		return L_24;
	}
}
// System.TimeSpan System.TimeSpan::FromTicks(System.Int64)
extern "C" TimeSpan_t1_368  TimeSpan_FromTicks_m1_14649 (Object_t * __this /* static, unused */, int64_t ___value, const MethodInfo* method)
{
	{
		int64_t L_0 = ___value;
		TimeSpan_t1_368  L_1 = {0};
		TimeSpan__ctor_m1_14618(&L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 System.TimeSpan::GetHashCode()
extern "C" int32_t TimeSpan_GetHashCode_m1_14650 (TimeSpan_t1_368 * __this, const MethodInfo* method)
{
	{
		int64_t* L_0 = &(__this->____ticks_8);
		int32_t L_1 = Int64_GetHashCode_m1_132(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.TimeSpan System.TimeSpan::Negate()
extern TypeInfo* TimeSpan_t1_368_il2cpp_TypeInfo_var;
extern TypeInfo* OverflowException_t1_1590_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3617;
extern "C" TimeSpan_t1_368  TimeSpan_Negate_m1_14651 (TimeSpan_t1_368 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TimeSpan_t1_368_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(327);
		OverflowException_t1_1590_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(13);
		_stringLiteral3617 = il2cpp_codegen_string_literal_from_index(3617);
		s_Il2CppMethodIntialized = true;
	}
	TimeSpan_t1_368  V_0 = {0};
	{
		int64_t L_0 = (__this->____ticks_8);
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t1_368_il2cpp_TypeInfo_var);
		TimeSpan_t1_368  L_1 = ((TimeSpan_t1_368_StaticFields*)TimeSpan_t1_368_il2cpp_TypeInfo_var->static_fields)->___MinValue_6;
		V_0 = L_1;
		int64_t L_2 = ((&V_0)->____ticks_8);
		if ((!(((uint64_t)L_0) == ((uint64_t)L_2))))
		{
			goto IL_0028;
		}
	}
	{
		String_t* L_3 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral3617, /*hidden argument*/NULL);
		OverflowException_t1_1590 * L_4 = (OverflowException_t1_1590 *)il2cpp_codegen_object_new (OverflowException_t1_1590_il2cpp_TypeInfo_var);
		OverflowException__ctor_m1_14547(L_4, L_3, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
	}

IL_0028:
	{
		int64_t L_5 = (__this->____ticks_8);
		TimeSpan_t1_368  L_6 = {0};
		TimeSpan__ctor_m1_14618(&L_6, ((-L_5)), /*hidden argument*/NULL);
		return L_6;
	}
}
// System.TimeSpan System.TimeSpan::Parse(System.String)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* Parser_t1_1605_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral7;
extern "C" TimeSpan_t1_368  TimeSpan_Parse_m1_14652 (Object_t * __this /* static, unused */, String_t* ___s, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		Parser_t1_1605_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1110);
		_stringLiteral7 = il2cpp_codegen_string_literal_from_index(7);
		s_Il2CppMethodIntialized = true;
	}
	Parser_t1_1605 * V_0 = {0};
	{
		String_t* L_0 = ___s;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, _stringLiteral7, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		String_t* L_2 = ___s;
		Parser_t1_1605 * L_3 = (Parser_t1_1605 *)il2cpp_codegen_object_new (Parser_t1_1605_il2cpp_TypeInfo_var);
		Parser__ctor_m1_14609(L_3, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Parser_t1_1605 * L_4 = V_0;
		NullCheck(L_4);
		TimeSpan_t1_368  L_5 = Parser_Execute_m1_14617(L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Boolean System.TimeSpan::TryParse(System.String,System.TimeSpan&)
extern TypeInfo* TimeSpan_t1_368_il2cpp_TypeInfo_var;
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern "C" bool TimeSpan_TryParse_m1_14653 (Object_t * __this /* static, unused */, String_t* ___s, TimeSpan_t1_368 * ___result, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TimeSpan_t1_368_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(327);
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___s;
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		TimeSpan_t1_368 * L_1 = ___result;
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t1_368_il2cpp_TypeInfo_var);
		TimeSpan_t1_368  L_2 = ((TimeSpan_t1_368_StaticFields*)TimeSpan_t1_368_il2cpp_TypeInfo_var->static_fields)->___Zero_7;
		(*(TimeSpan_t1_368 *)L_1) = L_2;
		return 0;
	}

IL_0013:
	try
	{ // begin try (depth: 1)
		{
			TimeSpan_t1_368 * L_3 = ___result;
			String_t* L_4 = ___s;
			IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t1_368_il2cpp_TypeInfo_var);
			TimeSpan_t1_368  L_5 = TimeSpan_Parse_m1_14652(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
			(*(TimeSpan_t1_368 *)L_3) = L_5;
			V_0 = 1;
			goto IL_0043;
		}

IL_0026:
		{
			; // IL_0026: leave IL_0043
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1_33 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Object_t_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_002b;
		throw e;
	}

CATCH_002b:
	{ // begin catch(System.Object)
		{
			TimeSpan_t1_368 * L_6 = ___result;
			IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t1_368_il2cpp_TypeInfo_var);
			TimeSpan_t1_368  L_7 = ((TimeSpan_t1_368_StaticFields*)TimeSpan_t1_368_il2cpp_TypeInfo_var->static_fields)->___Zero_7;
			(*(TimeSpan_t1_368 *)L_6) = L_7;
			V_0 = 0;
			goto IL_0043;
		}

IL_003e:
		{
			; // IL_003e: leave IL_0043
		}
	} // end catch (depth: 1)

IL_0043:
	{
		bool L_8 = V_0;
		return L_8;
	}
}
// System.TimeSpan System.TimeSpan::Subtract(System.TimeSpan)
extern TypeInfo* OverflowException_t1_1590_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3612;
extern "C" TimeSpan_t1_368  TimeSpan_Subtract_m1_14654 (TimeSpan_t1_368 * __this, TimeSpan_t1_368  ___ts, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		OverflowException_t1_1590_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(13);
		_stringLiteral3612 = il2cpp_codegen_string_literal_from_index(3612);
		s_Il2CppMethodIntialized = true;
	}
	TimeSpan_t1_368  V_0 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			int64_t L_0 = (__this->____ticks_8);
			int64_t L_1 = TimeSpan_get_Ticks_m1_14629((&___ts), /*hidden argument*/NULL);
			if (((int64_t)L_1 >= 0 && (int64_t)L_0 < kIl2CppInt64Min + (int64_t)L_1) || ((int64_t)L_1 < 0 && (int64_t)L_0 > kIl2CppInt64Max + (int64_t)L_1))
				il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_overflow_exception());
			TimeSpan_t1_368  L_2 = {0};
			TimeSpan__ctor_m1_14618(&L_2, ((int64_t)((int64_t)L_0-(int64_t)L_1)), /*hidden argument*/NULL);
			V_0 = L_2;
			goto IL_0034;
		}

IL_0019:
		{
			; // IL_0019: leave IL_0034
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1_33 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (OverflowException_t1_1590_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001e;
		throw e;
	}

CATCH_001e:
	{ // begin catch(System.OverflowException)
		{
			String_t* L_3 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral3612, /*hidden argument*/NULL);
			OverflowException_t1_1590 * L_4 = (OverflowException_t1_1590 *)il2cpp_codegen_object_new (OverflowException_t1_1590_il2cpp_TypeInfo_var);
			OverflowException__ctor_m1_14547(L_4, L_3, /*hidden argument*/NULL);
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
		}

IL_002f:
		{
			goto IL_0034;
		}
	} // end catch (depth: 1)

IL_0034:
	{
		TimeSpan_t1_368  L_5 = V_0;
		return L_5;
	}
}
// System.String System.TimeSpan::ToString()
extern TypeInfo* StringBuilder_t1_247_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3618;
extern Il2CppCodeGenString* _stringLiteral3619;
extern "C" String_t* TimeSpan_ToString_m1_14655 (TimeSpan_t1_368 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringBuilder_t1_247_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		_stringLiteral3618 = il2cpp_codegen_string_literal_from_index(3618);
		_stringLiteral3619 = il2cpp_codegen_string_literal_from_index(3619);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t1_247 * V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		StringBuilder_t1_247 * L_0 = (StringBuilder_t1_247 *)il2cpp_codegen_object_new (StringBuilder_t1_247_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1_12415(L_0, ((int32_t)14), /*hidden argument*/NULL);
		V_0 = L_0;
		int64_t L_1 = (__this->____ticks_8);
		if ((((int64_t)L_1) >= ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_001e;
		}
	}
	{
		StringBuilder_t1_247 * L_2 = V_0;
		NullCheck(L_2);
		StringBuilder_Append_m1_12452(L_2, ((int32_t)45), /*hidden argument*/NULL);
	}

IL_001e:
	{
		int32_t L_3 = TimeSpan_get_Days_m1_14624(__this, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0044;
		}
	}
	{
		StringBuilder_t1_247 * L_4 = V_0;
		int32_t L_5 = TimeSpan_get_Days_m1_14624(__this, /*hidden argument*/NULL);
		int32_t L_6 = abs(L_5);
		NullCheck(L_4);
		StringBuilder_Append_m1_12444(L_4, L_6, /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_7 = V_0;
		NullCheck(L_7);
		StringBuilder_Append_m1_12452(L_7, ((int32_t)46), /*hidden argument*/NULL);
	}

IL_0044:
	{
		StringBuilder_t1_247 * L_8 = V_0;
		int32_t L_9 = TimeSpan_get_Hours_m1_14625(__this, /*hidden argument*/NULL);
		int32_t L_10 = abs(L_9);
		V_2 = L_10;
		String_t* L_11 = Int32_ToString_m1_103((&V_2), _stringLiteral3618, /*hidden argument*/NULL);
		NullCheck(L_8);
		StringBuilder_Append_m1_12438(L_8, L_11, /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_12 = V_0;
		NullCheck(L_12);
		StringBuilder_Append_m1_12452(L_12, ((int32_t)58), /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_13 = V_0;
		int32_t L_14 = TimeSpan_get_Minutes_m1_14627(__this, /*hidden argument*/NULL);
		int32_t L_15 = abs(L_14);
		V_3 = L_15;
		String_t* L_16 = Int32_ToString_m1_103((&V_3), _stringLiteral3618, /*hidden argument*/NULL);
		NullCheck(L_13);
		StringBuilder_Append_m1_12438(L_13, L_16, /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_17 = V_0;
		NullCheck(L_17);
		StringBuilder_Append_m1_12452(L_17, ((int32_t)58), /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_18 = V_0;
		int32_t L_19 = TimeSpan_get_Seconds_m1_14628(__this, /*hidden argument*/NULL);
		int32_t L_20 = abs(L_19);
		V_4 = L_20;
		String_t* L_21 = Int32_ToString_m1_103((&V_4), _stringLiteral3618, /*hidden argument*/NULL);
		NullCheck(L_18);
		StringBuilder_Append_m1_12438(L_18, L_21, /*hidden argument*/NULL);
		int64_t L_22 = (__this->____ticks_8);
		int64_t L_23 = llabs(((int64_t)((int64_t)L_22%(int64_t)(((int64_t)((int64_t)((int32_t)10000000)))))));
		V_1 = (((int32_t)((int32_t)L_23)));
		int32_t L_24 = V_1;
		if (!L_24)
		{
			goto IL_00ea;
		}
	}
	{
		StringBuilder_t1_247 * L_25 = V_0;
		NullCheck(L_25);
		StringBuilder_Append_m1_12452(L_25, ((int32_t)46), /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_26 = V_0;
		String_t* L_27 = Int32_ToString_m1_103((&V_1), _stringLiteral3619, /*hidden argument*/NULL);
		NullCheck(L_26);
		StringBuilder_Append_m1_12438(L_26, L_27, /*hidden argument*/NULL);
	}

IL_00ea:
	{
		StringBuilder_t1_247 * L_28 = V_0;
		NullCheck(L_28);
		String_t* L_29 = StringBuilder_ToString_m1_12428(L_28, /*hidden argument*/NULL);
		return L_29;
	}
}
// System.TimeSpan System.TimeSpan::op_Addition(System.TimeSpan,System.TimeSpan)
extern "C" TimeSpan_t1_368  TimeSpan_op_Addition_m1_14656 (Object_t * __this /* static, unused */, TimeSpan_t1_368  ___t1, TimeSpan_t1_368  ___t2, const MethodInfo* method)
{
	{
		TimeSpan_t1_368  L_0 = ___t2;
		TimeSpan_t1_368  L_1 = TimeSpan_Add_m1_14635((&___t1), L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.TimeSpan::op_Equality(System.TimeSpan,System.TimeSpan)
extern "C" bool TimeSpan_op_Equality_m1_14657 (Object_t * __this /* static, unused */, TimeSpan_t1_368  ___t1, TimeSpan_t1_368  ___t2, const MethodInfo* method)
{
	{
		int64_t L_0 = ((&___t1)->____ticks_8);
		int64_t L_1 = ((&___t2)->____ticks_8);
		return ((((int64_t)L_0) == ((int64_t)L_1))? 1 : 0);
	}
}
// System.Boolean System.TimeSpan::op_GreaterThan(System.TimeSpan,System.TimeSpan)
extern "C" bool TimeSpan_op_GreaterThan_m1_14658 (Object_t * __this /* static, unused */, TimeSpan_t1_368  ___t1, TimeSpan_t1_368  ___t2, const MethodInfo* method)
{
	{
		int64_t L_0 = ((&___t1)->____ticks_8);
		int64_t L_1 = ((&___t2)->____ticks_8);
		return ((((int64_t)L_0) > ((int64_t)L_1))? 1 : 0);
	}
}
// System.Boolean System.TimeSpan::op_GreaterThanOrEqual(System.TimeSpan,System.TimeSpan)
extern "C" bool TimeSpan_op_GreaterThanOrEqual_m1_14659 (Object_t * __this /* static, unused */, TimeSpan_t1_368  ___t1, TimeSpan_t1_368  ___t2, const MethodInfo* method)
{
	{
		int64_t L_0 = ((&___t1)->____ticks_8);
		int64_t L_1 = ((&___t2)->____ticks_8);
		return ((((int32_t)((((int64_t)L_0) < ((int64_t)L_1))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean System.TimeSpan::op_Inequality(System.TimeSpan,System.TimeSpan)
extern "C" bool TimeSpan_op_Inequality_m1_14660 (Object_t * __this /* static, unused */, TimeSpan_t1_368  ___t1, TimeSpan_t1_368  ___t2, const MethodInfo* method)
{
	{
		int64_t L_0 = ((&___t1)->____ticks_8);
		int64_t L_1 = ((&___t2)->____ticks_8);
		return ((((int32_t)((((int64_t)L_0) == ((int64_t)L_1))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean System.TimeSpan::op_LessThan(System.TimeSpan,System.TimeSpan)
extern "C" bool TimeSpan_op_LessThan_m1_14661 (Object_t * __this /* static, unused */, TimeSpan_t1_368  ___t1, TimeSpan_t1_368  ___t2, const MethodInfo* method)
{
	{
		int64_t L_0 = ((&___t1)->____ticks_8);
		int64_t L_1 = ((&___t2)->____ticks_8);
		return ((((int64_t)L_0) < ((int64_t)L_1))? 1 : 0);
	}
}
// System.Boolean System.TimeSpan::op_LessThanOrEqual(System.TimeSpan,System.TimeSpan)
extern "C" bool TimeSpan_op_LessThanOrEqual_m1_14662 (Object_t * __this /* static, unused */, TimeSpan_t1_368  ___t1, TimeSpan_t1_368  ___t2, const MethodInfo* method)
{
	{
		int64_t L_0 = ((&___t1)->____ticks_8);
		int64_t L_1 = ((&___t2)->____ticks_8);
		return ((((int32_t)((((int64_t)L_0) > ((int64_t)L_1))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.TimeSpan System.TimeSpan::op_Subtraction(System.TimeSpan,System.TimeSpan)
extern "C" TimeSpan_t1_368  TimeSpan_op_Subtraction_m1_14663 (Object_t * __this /* static, unused */, TimeSpan_t1_368  ___t1, TimeSpan_t1_368  ___t2, const MethodInfo* method)
{
	{
		TimeSpan_t1_368  L_0 = ___t2;
		TimeSpan_t1_368  L_1 = TimeSpan_Subtract_m1_14654((&___t1), L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.TimeSpan System.TimeSpan::op_UnaryNegation(System.TimeSpan)
extern "C" TimeSpan_t1_368  TimeSpan_op_UnaryNegation_m1_14664 (Object_t * __this /* static, unused */, TimeSpan_t1_368  ___t, const MethodInfo* method)
{
	{
		TimeSpan_t1_368  L_0 = TimeSpan_Negate_m1_14651((&___t), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.TimeSpan System.TimeSpan::op_UnaryPlus(System.TimeSpan)
extern "C" TimeSpan_t1_368  TimeSpan_op_UnaryPlus_m1_14665 (Object_t * __this /* static, unused */, TimeSpan_t1_368  ___t, const MethodInfo* method)
{
	{
		TimeSpan_t1_368  L_0 = ___t;
		return L_0;
	}
}
// System.Void System.TimeZone::.ctor()
extern "C" void TimeZone__ctor_m1_14666 (TimeZone_t1_1606 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.TimeZone::.cctor()
extern TypeInfo* DateTime_t1_150_il2cpp_TypeInfo_var;
extern TypeInfo* CurrentSystemTimeZone_t1_1609_il2cpp_TypeInfo_var;
extern TypeInfo* TimeZone_t1_1606_il2cpp_TypeInfo_var;
extern "C" void TimeZone__cctor_m1_14667 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DateTime_t1_150_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		CurrentSystemTimeZone_t1_1609_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1111);
		TimeZone_t1_1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		int64_t L_0 = DateTime_GetNow_m1_13825(NULL /*static, unused*/, /*hidden argument*/NULL);
		CurrentSystemTimeZone_t1_1609 * L_1 = (CurrentSystemTimeZone_t1_1609 *)il2cpp_codegen_object_new (CurrentSystemTimeZone_t1_1609_il2cpp_TypeInfo_var);
		CurrentSystemTimeZone__ctor_m1_14676(L_1, L_0, /*hidden argument*/NULL);
		((TimeZone_t1_1606_StaticFields*)TimeZone_t1_1606_il2cpp_TypeInfo_var->static_fields)->___currentTimeZone_0 = L_1;
		return;
	}
}
// System.TimeZone System.TimeZone::get_CurrentTimeZone()
extern TypeInfo* TimeZone_t1_1606_il2cpp_TypeInfo_var;
extern "C" TimeZone_t1_1606 * TimeZone_get_CurrentTimeZone_m1_14668 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TimeZone_t1_1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TimeZone_t1_1606_il2cpp_TypeInfo_var);
		TimeZone_t1_1606 * L_0 = ((TimeZone_t1_1606_StaticFields*)TimeZone_t1_1606_il2cpp_TypeInfo_var->static_fields)->___currentTimeZone_0;
		return L_0;
	}
}
// System.Boolean System.TimeZone::IsDaylightSavingTime(System.DateTime)
extern TypeInfo* TimeZone_t1_1606_il2cpp_TypeInfo_var;
extern "C" bool TimeZone_IsDaylightSavingTime_m1_14669 (TimeZone_t1_1606 * __this, DateTime_t1_150  ___time, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TimeZone_t1_1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	{
		DateTime_t1_150  L_0 = ___time;
		int32_t L_1 = DateTime_get_Year_m1_13830((&___time), /*hidden argument*/NULL);
		DaylightTime_t1_367 * L_2 = (DaylightTime_t1_367 *)VirtFuncInvoker1< DaylightTime_t1_367 *, int32_t >::Invoke(6 /* System.Globalization.DaylightTime System.TimeZone::GetDaylightChanges(System.Int32) */, __this, L_1);
		IL2CPP_RUNTIME_CLASS_INIT(TimeZone_t1_1606_il2cpp_TypeInfo_var);
		bool L_3 = TimeZone_IsDaylightSavingTime_m1_14670(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean System.TimeZone::IsDaylightSavingTime(System.DateTime,System.Globalization.DaylightTime)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3622;
extern "C" bool TimeZone_IsDaylightSavingTime_m1_14670 (Object_t * __this /* static, unused */, DateTime_t1_150  ___time, DaylightTime_t1_367 * ___daylightTimes, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		_stringLiteral3622 = il2cpp_codegen_string_literal_from_index(3622);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t1_150  V_0 = {0};
	DateTime_t1_150  V_1 = {0};
	DateTime_t1_150  V_2 = {0};
	DateTime_t1_150  V_3 = {0};
	DateTime_t1_150  V_4 = {0};
	DateTime_t1_150  V_5 = {0};
	DateTime_t1_150  V_6 = {0};
	DateTime_t1_150  V_7 = {0};
	DateTime_t1_150  V_8 = {0};
	DateTime_t1_150  V_9 = {0};
	{
		DaylightTime_t1_367 * L_0 = ___daylightTimes;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, _stringLiteral3622, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		DaylightTime_t1_367 * L_2 = ___daylightTimes;
		NullCheck(L_2);
		DateTime_t1_150  L_3 = DaylightTime_get_Start_m1_4054(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		int64_t L_4 = DateTime_get_Ticks_m1_13827((&V_0), /*hidden argument*/NULL);
		DaylightTime_t1_367 * L_5 = ___daylightTimes;
		NullCheck(L_5);
		DateTime_t1_150  L_6 = DaylightTime_get_End_m1_4055(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		int64_t L_7 = DateTime_get_Ticks_m1_13827((&V_1), /*hidden argument*/NULL);
		if ((!(((uint64_t)L_4) == ((uint64_t)L_7))))
		{
			goto IL_0034;
		}
	}
	{
		return 0;
	}

IL_0034:
	{
		DaylightTime_t1_367 * L_8 = ___daylightTimes;
		NullCheck(L_8);
		DateTime_t1_150  L_9 = DaylightTime_get_Start_m1_4054(L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		int64_t L_10 = DateTime_get_Ticks_m1_13827((&V_2), /*hidden argument*/NULL);
		DaylightTime_t1_367 * L_11 = ___daylightTimes;
		NullCheck(L_11);
		DateTime_t1_150  L_12 = DaylightTime_get_End_m1_4055(L_11, /*hidden argument*/NULL);
		V_3 = L_12;
		int64_t L_13 = DateTime_get_Ticks_m1_13827((&V_3), /*hidden argument*/NULL);
		if ((((int64_t)L_10) >= ((int64_t)L_13)))
		{
			goto IL_0092;
		}
	}
	{
		DaylightTime_t1_367 * L_14 = ___daylightTimes;
		NullCheck(L_14);
		DateTime_t1_150  L_15 = DaylightTime_get_Start_m1_4054(L_14, /*hidden argument*/NULL);
		V_4 = L_15;
		int64_t L_16 = DateTime_get_Ticks_m1_13827((&V_4), /*hidden argument*/NULL);
		int64_t L_17 = DateTime_get_Ticks_m1_13827((&___time), /*hidden argument*/NULL);
		if ((((int64_t)L_16) >= ((int64_t)L_17)))
		{
			goto IL_008d;
		}
	}
	{
		DaylightTime_t1_367 * L_18 = ___daylightTimes;
		NullCheck(L_18);
		DateTime_t1_150  L_19 = DaylightTime_get_End_m1_4055(L_18, /*hidden argument*/NULL);
		V_5 = L_19;
		int64_t L_20 = DateTime_get_Ticks_m1_13827((&V_5), /*hidden argument*/NULL);
		int64_t L_21 = DateTime_get_Ticks_m1_13827((&___time), /*hidden argument*/NULL);
		if ((((int64_t)L_20) <= ((int64_t)L_21)))
		{
			goto IL_008d;
		}
	}
	{
		return 1;
	}

IL_008d:
	{
		goto IL_0100;
	}

IL_0092:
	{
		int32_t L_22 = DateTime_get_Year_m1_13830((&___time), /*hidden argument*/NULL);
		DaylightTime_t1_367 * L_23 = ___daylightTimes;
		NullCheck(L_23);
		DateTime_t1_150  L_24 = DaylightTime_get_Start_m1_4054(L_23, /*hidden argument*/NULL);
		V_6 = L_24;
		int32_t L_25 = DateTime_get_Year_m1_13830((&V_6), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_22) == ((uint32_t)L_25))))
		{
			goto IL_0100;
		}
	}
	{
		int32_t L_26 = DateTime_get_Year_m1_13830((&___time), /*hidden argument*/NULL);
		DaylightTime_t1_367 * L_27 = ___daylightTimes;
		NullCheck(L_27);
		DateTime_t1_150  L_28 = DaylightTime_get_End_m1_4055(L_27, /*hidden argument*/NULL);
		V_7 = L_28;
		int32_t L_29 = DateTime_get_Year_m1_13830((&V_7), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_26) == ((uint32_t)L_29))))
		{
			goto IL_0100;
		}
	}
	{
		int64_t L_30 = DateTime_get_Ticks_m1_13827((&___time), /*hidden argument*/NULL);
		DaylightTime_t1_367 * L_31 = ___daylightTimes;
		NullCheck(L_31);
		DateTime_t1_150  L_32 = DaylightTime_get_End_m1_4055(L_31, /*hidden argument*/NULL);
		V_8 = L_32;
		int64_t L_33 = DateTime_get_Ticks_m1_13827((&V_8), /*hidden argument*/NULL);
		if ((((int64_t)L_30) < ((int64_t)L_33)))
		{
			goto IL_00fe;
		}
	}
	{
		int64_t L_34 = DateTime_get_Ticks_m1_13827((&___time), /*hidden argument*/NULL);
		DaylightTime_t1_367 * L_35 = ___daylightTimes;
		NullCheck(L_35);
		DateTime_t1_150  L_36 = DaylightTime_get_Start_m1_4054(L_35, /*hidden argument*/NULL);
		V_9 = L_36;
		int64_t L_37 = DateTime_get_Ticks_m1_13827((&V_9), /*hidden argument*/NULL);
		if ((((int64_t)L_34) <= ((int64_t)L_37)))
		{
			goto IL_0100;
		}
	}

IL_00fe:
	{
		return 1;
	}

IL_0100:
	{
		return 0;
	}
}
// System.DateTime System.TimeZone::ToLocalTime(System.DateTime)
extern TypeInfo* DateTime_t1_150_il2cpp_TypeInfo_var;
extern "C" DateTime_t1_150  TimeZone_ToLocalTime_m1_14671 (TimeZone_t1_1606 * __this, DateTime_t1_150  ___time, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DateTime_t1_150_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	TimeSpan_t1_368  V_0 = {0};
	DateTime_t1_150  V_1 = {0};
	DaylightTime_t1_367 * V_2 = {0};
	TimeSpan_t1_368  V_3 = {0};
	DateTime_t1_150  V_4 = {0};
	TimeSpan_t1_368  V_5 = {0};
	DateTime_t1_150  V_6 = {0};
	{
		int32_t L_0 = DateTime_get_Kind_m1_13831((&___time), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)2))))
		{
			goto IL_000f;
		}
	}
	{
		DateTime_t1_150  L_1 = ___time;
		return L_1;
	}

IL_000f:
	{
		DateTime_t1_150  L_2 = ___time;
		TimeSpan_t1_368  L_3 = (TimeSpan_t1_368 )VirtFuncInvoker1< TimeSpan_t1_368 , DateTime_t1_150  >::Invoke(7 /* System.TimeSpan System.TimeZone::GetUtcOffset(System.DateTime) */, __this, L_2);
		V_0 = L_3;
		int64_t L_4 = TimeSpan_get_Ticks_m1_14629((&V_0), /*hidden argument*/NULL);
		if ((((int64_t)L_4) <= ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_004c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		DateTime_t1_150  L_5 = ((DateTime_t1_150_StaticFields*)DateTime_t1_150_il2cpp_TypeInfo_var->static_fields)->___MaxValue_12;
		TimeSpan_t1_368  L_6 = V_0;
		DateTime_t1_150  L_7 = DateTime_op_Subtraction_m1_13910(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		DateTime_t1_150  L_8 = ___time;
		bool L_9 = DateTime_op_LessThan_m1_13907(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0047;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		DateTime_t1_150  L_10 = ((DateTime_t1_150_StaticFields*)DateTime_t1_150_il2cpp_TypeInfo_var->static_fields)->___MaxValue_12;
		DateTime_t1_150  L_11 = DateTime_SpecifyKind_m1_13849(NULL /*static, unused*/, L_10, 2, /*hidden argument*/NULL);
		return L_11;
	}

IL_0047:
	{
		goto IL_0088;
	}

IL_004c:
	{
		int64_t L_12 = TimeSpan_get_Ticks_m1_14629((&V_0), /*hidden argument*/NULL);
		if ((((int64_t)L_12) >= ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_0088;
		}
	}
	{
		int64_t L_13 = DateTime_get_Ticks_m1_13827((&___time), /*hidden argument*/NULL);
		int64_t L_14 = TimeSpan_get_Ticks_m1_14629((&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		DateTime_t1_150  L_15 = ((DateTime_t1_150_StaticFields*)DateTime_t1_150_il2cpp_TypeInfo_var->static_fields)->___MinValue_13;
		V_4 = L_15;
		int64_t L_16 = DateTime_get_Ticks_m1_13827((&V_4), /*hidden argument*/NULL);
		if ((((int64_t)((int64_t)((int64_t)L_13+(int64_t)L_14))) >= ((int64_t)L_16)))
		{
			goto IL_0088;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		DateTime_t1_150  L_17 = ((DateTime_t1_150_StaticFields*)DateTime_t1_150_il2cpp_TypeInfo_var->static_fields)->___MinValue_13;
		DateTime_t1_150  L_18 = DateTime_SpecifyKind_m1_13849(NULL /*static, unused*/, L_17, 2, /*hidden argument*/NULL);
		return L_18;
	}

IL_0088:
	{
		TimeSpan_t1_368  L_19 = V_0;
		DateTime_t1_150  L_20 = DateTime_Add_m1_13832((&___time), L_19, /*hidden argument*/NULL);
		V_1 = L_20;
		int32_t L_21 = DateTime_get_Year_m1_13830((&___time), /*hidden argument*/NULL);
		DaylightTime_t1_367 * L_22 = (DaylightTime_t1_367 *)VirtFuncInvoker1< DaylightTime_t1_367 *, int32_t >::Invoke(6 /* System.Globalization.DaylightTime System.TimeZone::GetDaylightChanges(System.Int32) */, __this, L_21);
		V_2 = L_22;
		DaylightTime_t1_367 * L_23 = V_2;
		NullCheck(L_23);
		TimeSpan_t1_368  L_24 = DaylightTime_get_Delta_m1_4056(L_23, /*hidden argument*/NULL);
		V_5 = L_24;
		int64_t L_25 = TimeSpan_get_Ticks_m1_14629((&V_5), /*hidden argument*/NULL);
		if (L_25)
		{
			goto IL_00bb;
		}
	}
	{
		DateTime_t1_150  L_26 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		DateTime_t1_150  L_27 = DateTime_SpecifyKind_m1_13849(NULL /*static, unused*/, L_26, 2, /*hidden argument*/NULL);
		return L_27;
	}

IL_00bb:
	{
		DateTime_t1_150  L_28 = V_1;
		DaylightTime_t1_367 * L_29 = V_2;
		NullCheck(L_29);
		DateTime_t1_150  L_30 = DaylightTime_get_End_m1_4055(L_29, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		bool L_31 = DateTime_op_LessThan_m1_13907(NULL /*static, unused*/, L_28, L_30, /*hidden argument*/NULL);
		if (!L_31)
		{
			goto IL_00f4;
		}
	}
	{
		DaylightTime_t1_367 * L_32 = V_2;
		NullCheck(L_32);
		DateTime_t1_150  L_33 = DaylightTime_get_End_m1_4055(L_32, /*hidden argument*/NULL);
		V_6 = L_33;
		DaylightTime_t1_367 * L_34 = V_2;
		NullCheck(L_34);
		TimeSpan_t1_368  L_35 = DaylightTime_get_Delta_m1_4056(L_34, /*hidden argument*/NULL);
		DateTime_t1_150  L_36 = DateTime_Subtract_m1_13888((&V_6), L_35, /*hidden argument*/NULL);
		DateTime_t1_150  L_37 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		bool L_38 = DateTime_op_LessThanOrEqual_m1_13908(NULL /*static, unused*/, L_36, L_37, /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_00f4;
		}
	}
	{
		DateTime_t1_150  L_39 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		DateTime_t1_150  L_40 = DateTime_SpecifyKind_m1_13849(NULL /*static, unused*/, L_39, 2, /*hidden argument*/NULL);
		return L_40;
	}

IL_00f4:
	{
		DateTime_t1_150  L_41 = V_1;
		TimeSpan_t1_368  L_42 = (TimeSpan_t1_368 )VirtFuncInvoker1< TimeSpan_t1_368 , DateTime_t1_150  >::Invoke(7 /* System.TimeSpan System.TimeZone::GetUtcOffset(System.DateTime) */, __this, L_41);
		V_3 = L_42;
		TimeSpan_t1_368  L_43 = V_3;
		DateTime_t1_150  L_44 = DateTime_Add_m1_13832((&___time), L_43, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		DateTime_t1_150  L_45 = DateTime_SpecifyKind_m1_13849(NULL /*static, unused*/, L_44, 2, /*hidden argument*/NULL);
		return L_45;
	}
}
// System.DateTime System.TimeZone::ToUniversalTime(System.DateTime)
extern TypeInfo* DateTime_t1_150_il2cpp_TypeInfo_var;
extern "C" DateTime_t1_150  TimeZone_ToUniversalTime_m1_14672 (TimeZone_t1_1606 * __this, DateTime_t1_150  ___time, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DateTime_t1_150_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	TimeSpan_t1_368  V_0 = {0};
	{
		int32_t L_0 = DateTime_get_Kind_m1_13831((&___time), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_000f;
		}
	}
	{
		DateTime_t1_150  L_1 = ___time;
		return L_1;
	}

IL_000f:
	{
		DateTime_t1_150  L_2 = ___time;
		TimeSpan_t1_368  L_3 = (TimeSpan_t1_368 )VirtFuncInvoker1< TimeSpan_t1_368 , DateTime_t1_150  >::Invoke(7 /* System.TimeSpan System.TimeZone::GetUtcOffset(System.DateTime) */, __this, L_2);
		V_0 = L_3;
		int64_t L_4 = TimeSpan_get_Ticks_m1_14629((&V_0), /*hidden argument*/NULL);
		if ((((int64_t)L_4) >= ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_004c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		DateTime_t1_150  L_5 = ((DateTime_t1_150_StaticFields*)DateTime_t1_150_il2cpp_TypeInfo_var->static_fields)->___MaxValue_12;
		TimeSpan_t1_368  L_6 = V_0;
		DateTime_t1_150  L_7 = DateTime_op_Addition_m1_13902(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		DateTime_t1_150  L_8 = ___time;
		bool L_9 = DateTime_op_LessThan_m1_13907(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0047;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		DateTime_t1_150  L_10 = ((DateTime_t1_150_StaticFields*)DateTime_t1_150_il2cpp_TypeInfo_var->static_fields)->___MaxValue_12;
		DateTime_t1_150  L_11 = DateTime_SpecifyKind_m1_13849(NULL /*static, unused*/, L_10, 1, /*hidden argument*/NULL);
		return L_11;
	}

IL_0047:
	{
		goto IL_007c;
	}

IL_004c:
	{
		int64_t L_12 = TimeSpan_get_Ticks_m1_14629((&V_0), /*hidden argument*/NULL);
		if ((((int64_t)L_12) <= ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_007c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		DateTime_t1_150  L_13 = ((DateTime_t1_150_StaticFields*)DateTime_t1_150_il2cpp_TypeInfo_var->static_fields)->___MinValue_13;
		TimeSpan_t1_368  L_14 = V_0;
		DateTime_t1_150  L_15 = DateTime_op_Addition_m1_13902(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		DateTime_t1_150  L_16 = ___time;
		bool L_17 = DateTime_op_GreaterThan_m1_13904(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_007c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		DateTime_t1_150  L_18 = ((DateTime_t1_150_StaticFields*)DateTime_t1_150_il2cpp_TypeInfo_var->static_fields)->___MinValue_13;
		DateTime_t1_150  L_19 = DateTime_SpecifyKind_m1_13849(NULL /*static, unused*/, L_18, 1, /*hidden argument*/NULL);
		return L_19;
	}

IL_007c:
	{
		int64_t L_20 = DateTime_get_Ticks_m1_13827((&___time), /*hidden argument*/NULL);
		int64_t L_21 = TimeSpan_get_Ticks_m1_14629((&V_0), /*hidden argument*/NULL);
		DateTime_t1_150  L_22 = {0};
		DateTime__ctor_m1_13784(&L_22, ((int64_t)((int64_t)L_20-(int64_t)L_21)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		DateTime_t1_150  L_23 = DateTime_SpecifyKind_m1_13849(NULL /*static, unused*/, L_22, 1, /*hidden argument*/NULL);
		return L_23;
	}
}
// System.TimeSpan System.TimeZone::GetLocalTimeDiff(System.DateTime)
extern "C" TimeSpan_t1_368  TimeZone_GetLocalTimeDiff_m1_14673 (TimeZone_t1_1606 * __this, DateTime_t1_150  ___time, const MethodInfo* method)
{
	{
		DateTime_t1_150  L_0 = ___time;
		DateTime_t1_150  L_1 = ___time;
		TimeSpan_t1_368  L_2 = (TimeSpan_t1_368 )VirtFuncInvoker1< TimeSpan_t1_368 , DateTime_t1_150  >::Invoke(7 /* System.TimeSpan System.TimeZone::GetUtcOffset(System.DateTime) */, __this, L_1);
		TimeSpan_t1_368  L_3 = TimeZone_GetLocalTimeDiff_m1_14674(__this, L_0, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.TimeSpan System.TimeZone::GetLocalTimeDiff(System.DateTime,System.TimeSpan)
extern TypeInfo* DateTime_t1_150_il2cpp_TypeInfo_var;
extern TypeInfo* TimeSpan_t1_368_il2cpp_TypeInfo_var;
extern "C" TimeSpan_t1_368  TimeZone_GetLocalTimeDiff_m1_14674 (TimeZone_t1_1606 * __this, DateTime_t1_150  ___time, TimeSpan_t1_368  ___utc_offset, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DateTime_t1_150_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		TimeSpan_t1_368_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(327);
		s_Il2CppMethodIntialized = true;
	}
	DaylightTime_t1_367 * V_0 = {0};
	DateTime_t1_150  V_1 = {0};
	TimeSpan_t1_368  V_2 = {0};
	DateTime_t1_150  V_3 = {0};
	DateTime_t1_150  V_4 = {0};
	{
		int32_t L_0 = DateTime_get_Year_m1_13830((&___time), /*hidden argument*/NULL);
		DaylightTime_t1_367 * L_1 = (DaylightTime_t1_367 *)VirtFuncInvoker1< DaylightTime_t1_367 *, int32_t >::Invoke(6 /* System.Globalization.DaylightTime System.TimeZone::GetDaylightChanges(System.Int32) */, __this, L_0);
		V_0 = L_1;
		DaylightTime_t1_367 * L_2 = V_0;
		NullCheck(L_2);
		TimeSpan_t1_368  L_3 = DaylightTime_get_Delta_m1_4056(L_2, /*hidden argument*/NULL);
		V_2 = L_3;
		int64_t L_4 = TimeSpan_get_Ticks_m1_14629((&V_2), /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0023;
		}
	}
	{
		TimeSpan_t1_368  L_5 = ___utc_offset;
		return L_5;
	}

IL_0023:
	{
		TimeSpan_t1_368  L_6 = ___utc_offset;
		DateTime_t1_150  L_7 = DateTime_Add_m1_13832((&___time), L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		DateTime_t1_150  L_8 = V_1;
		DaylightTime_t1_367 * L_9 = V_0;
		NullCheck(L_9);
		DateTime_t1_150  L_10 = DaylightTime_get_End_m1_4055(L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		bool L_11 = DateTime_op_LessThan_m1_13907(NULL /*static, unused*/, L_8, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_005e;
		}
	}
	{
		DaylightTime_t1_367 * L_12 = V_0;
		NullCheck(L_12);
		DateTime_t1_150  L_13 = DaylightTime_get_End_m1_4055(L_12, /*hidden argument*/NULL);
		V_3 = L_13;
		DaylightTime_t1_367 * L_14 = V_0;
		NullCheck(L_14);
		TimeSpan_t1_368  L_15 = DaylightTime_get_Delta_m1_4056(L_14, /*hidden argument*/NULL);
		DateTime_t1_150  L_16 = DateTime_Subtract_m1_13888((&V_3), L_15, /*hidden argument*/NULL);
		DateTime_t1_150  L_17 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		bool L_18 = DateTime_op_LessThanOrEqual_m1_13908(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_005e;
		}
	}
	{
		TimeSpan_t1_368  L_19 = ___utc_offset;
		return L_19;
	}

IL_005e:
	{
		DateTime_t1_150  L_20 = V_1;
		DaylightTime_t1_367 * L_21 = V_0;
		NullCheck(L_21);
		DateTime_t1_150  L_22 = DaylightTime_get_Start_m1_4054(L_21, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		bool L_23 = DateTime_op_GreaterThanOrEqual_m1_13905(NULL /*static, unused*/, L_20, L_22, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_009c;
		}
	}
	{
		DaylightTime_t1_367 * L_24 = V_0;
		NullCheck(L_24);
		DateTime_t1_150  L_25 = DaylightTime_get_Start_m1_4054(L_24, /*hidden argument*/NULL);
		V_4 = L_25;
		DaylightTime_t1_367 * L_26 = V_0;
		NullCheck(L_26);
		TimeSpan_t1_368  L_27 = DaylightTime_get_Delta_m1_4056(L_26, /*hidden argument*/NULL);
		DateTime_t1_150  L_28 = DateTime_Add_m1_13832((&V_4), L_27, /*hidden argument*/NULL);
		DateTime_t1_150  L_29 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		bool L_30 = DateTime_op_GreaterThan_m1_13904(NULL /*static, unused*/, L_28, L_29, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_009c;
		}
	}
	{
		TimeSpan_t1_368  L_31 = ___utc_offset;
		DaylightTime_t1_367 * L_32 = V_0;
		NullCheck(L_32);
		TimeSpan_t1_368  L_33 = DaylightTime_get_Delta_m1_4056(L_32, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t1_368_il2cpp_TypeInfo_var);
		TimeSpan_t1_368  L_34 = TimeSpan_op_Subtraction_m1_14663(NULL /*static, unused*/, L_31, L_33, /*hidden argument*/NULL);
		return L_34;
	}

IL_009c:
	{
		DateTime_t1_150  L_35 = V_1;
		TimeSpan_t1_368  L_36 = (TimeSpan_t1_368 )VirtFuncInvoker1< TimeSpan_t1_368 , DateTime_t1_150  >::Invoke(7 /* System.TimeSpan System.TimeZone::GetUtcOffset(System.DateTime) */, __this, L_35);
		return L_36;
	}
}
// System.Void System.CurrentSystemTimeZone::.ctor()
extern TypeInfo* Hashtable_t1_100_il2cpp_TypeInfo_var;
extern TypeInfo* TimeZone_t1_1606_il2cpp_TypeInfo_var;
extern "C" void CurrentSystemTimeZone__ctor_m1_14675 (CurrentSystemTimeZone_t1_1609 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Hashtable_t1_100_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(119);
		TimeZone_t1_1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	{
		Hashtable_t1_100 * L_0 = (Hashtable_t1_100 *)il2cpp_codegen_object_new (Hashtable_t1_100_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1_3282(L_0, 1, /*hidden argument*/NULL);
		__this->___m_CachedDaylightChanges_3 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(TimeZone_t1_1606_il2cpp_TypeInfo_var);
		TimeZone__ctor_m1_14666(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.CurrentSystemTimeZone::.ctor(System.Int64)
extern TypeInfo* Hashtable_t1_100_il2cpp_TypeInfo_var;
extern TypeInfo* TimeZone_t1_1606_il2cpp_TypeInfo_var;
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3623;
extern "C" void CurrentSystemTimeZone__ctor_m1_14676 (CurrentSystemTimeZone_t1_1609 * __this, int64_t ___lnow, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Hashtable_t1_100_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(119);
		TimeZone_t1_1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral3623 = il2cpp_codegen_string_literal_from_index(3623);
		s_Il2CppMethodIntialized = true;
	}
	Int64U5BU5D_t1_1665* V_0 = {0};
	StringU5BU5D_t1_238* V_1 = {0};
	DateTime_t1_150  V_2 = {0};
	DaylightTime_t1_367 * V_3 = {0};
	{
		Hashtable_t1_100 * L_0 = (Hashtable_t1_100 *)il2cpp_codegen_object_new (Hashtable_t1_100_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1_3282(L_0, 1, /*hidden argument*/NULL);
		__this->___m_CachedDaylightChanges_3 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(TimeZone_t1_1606_il2cpp_TypeInfo_var);
		TimeZone__ctor_m1_14666(__this, /*hidden argument*/NULL);
		int64_t L_1 = ___lnow;
		DateTime__ctor_m1_13784((&V_2), L_1, /*hidden argument*/NULL);
		int32_t L_2 = DateTime_get_Year_m1_13830((&V_2), /*hidden argument*/NULL);
		bool L_3 = CurrentSystemTimeZone_GetTimeZoneData_m1_14678(NULL /*static, unused*/, L_2, (&V_0), (&V_1), /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_003f;
		}
	}
	{
		String_t* L_4 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral3623, /*hidden argument*/NULL);
		NotSupportedException_t1_1583 * L_5 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14417(L_5, L_4, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_5);
	}

IL_003f:
	{
		StringU5BU5D_t1_238* L_6 = V_1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 0);
		int32_t L_7 = 0;
		String_t* L_8 = Locale_GetText_m1_1404(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_6, L_7, sizeof(String_t*))), /*hidden argument*/NULL);
		__this->___m_standardName_1 = L_8;
		StringU5BU5D_t1_238* L_9 = V_1;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 1);
		int32_t L_10 = 1;
		String_t* L_11 = Locale_GetText_m1_1404(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_9, L_10, sizeof(String_t*))), /*hidden argument*/NULL);
		__this->___m_daylightName_2 = L_11;
		Int64U5BU5D_t1_1665* L_12 = V_0;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 2);
		int32_t L_13 = 2;
		__this->___m_ticksOffset_4 = (*(int64_t*)(int64_t*)SZArrayLdElema(L_12, L_13, sizeof(int64_t)));
		Int64U5BU5D_t1_1665* L_14 = V_0;
		DaylightTime_t1_367 * L_15 = CurrentSystemTimeZone_GetDaylightTimeFromData_m1_14684(__this, L_14, /*hidden argument*/NULL);
		V_3 = L_15;
		Hashtable_t1_100 * L_16 = (__this->___m_CachedDaylightChanges_3);
		int32_t L_17 = DateTime_get_Year_m1_13830((&V_2), /*hidden argument*/NULL);
		int32_t L_18 = L_17;
		Object_t * L_19 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_18);
		DaylightTime_t1_367 * L_20 = V_3;
		NullCheck(L_16);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_16, L_19, L_20);
		DaylightTime_t1_367 * L_21 = V_3;
		CurrentSystemTimeZone_OnDeserialization_m1_14683(__this, L_21, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.CurrentSystemTimeZone::System.Runtime.Serialization.IDeserializationCallback.OnDeserialization(System.Object)
extern "C" void CurrentSystemTimeZone_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m1_14677 (CurrentSystemTimeZone_t1_1609 * __this, Object_t * ___sender, const MethodInfo* method)
{
	{
		CurrentSystemTimeZone_OnDeserialization_m1_14683(__this, (DaylightTime_t1_367 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.CurrentSystemTimeZone::GetTimeZoneData(System.Int32,System.Int64[]&,System.String[]&)
extern "C" bool CurrentSystemTimeZone_GetTimeZoneData_m1_14678 (Object_t * __this /* static, unused */, int32_t ___year, Int64U5BU5D_t1_1665** ___data, StringU5BU5D_t1_238** ___names, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef bool (*CurrentSystemTimeZone_GetTimeZoneData_m1_14678_ftn) (int32_t, Int64U5BU5D_t1_1665**, StringU5BU5D_t1_238**);
	return  ((CurrentSystemTimeZone_GetTimeZoneData_m1_14678_ftn)mscorlib::System::CurrentSystemTimeZone::GetTimeZoneData) (___year, ___data, ___names);
}
// System.String System.CurrentSystemTimeZone::get_DaylightName()
extern "C" String_t* CurrentSystemTimeZone_get_DaylightName_m1_14679 (CurrentSystemTimeZone_t1_1609 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_daylightName_2);
		return L_0;
	}
}
// System.String System.CurrentSystemTimeZone::get_StandardName()
extern "C" String_t* CurrentSystemTimeZone_get_StandardName_m1_14680 (CurrentSystemTimeZone_t1_1609 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_standardName_1);
		return L_0;
	}
}
// System.Globalization.DaylightTime System.CurrentSystemTimeZone::GetDaylightChanges(System.Int32)
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var;
extern TypeInfo* CurrentSystemTimeZone_t1_1609_il2cpp_TypeInfo_var;
extern TypeInfo* DaylightTime_t1_367_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral934;
extern Il2CppCodeGenString* _stringLiteral3624;
extern Il2CppCodeGenString* _stringLiteral3625;
extern "C" DaylightTime_t1_367 * CurrentSystemTimeZone_GetDaylightChanges_m1_14681 (CurrentSystemTimeZone_t1_1609 * __this, int32_t ___year, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		CurrentSystemTimeZone_t1_1609_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1111);
		DaylightTime_t1_367_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1112);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		_stringLiteral934 = il2cpp_codegen_string_literal_from_index(934);
		_stringLiteral3624 = il2cpp_codegen_string_literal_from_index(3624);
		_stringLiteral3625 = il2cpp_codegen_string_literal_from_index(3625);
		s_Il2CppMethodIntialized = true;
	}
	Hashtable_t1_100 * V_0 = {0};
	DaylightTime_t1_367 * V_1 = {0};
	Int64U5BU5D_t1_1665* V_2 = {0};
	StringU5BU5D_t1_238* V_3 = {0};
	DaylightTime_t1_367 * V_4 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = ___year;
		if ((((int32_t)L_0) < ((int32_t)1)))
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_1 = ___year;
		if ((((int32_t)L_1) <= ((int32_t)((int32_t)9999))))
		{
			goto IL_0032;
		}
	}

IL_0012:
	{
		int32_t L_2 = ___year;
		int32_t L_3 = L_2;
		Object_t * L_4 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_3);
		String_t* L_5 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral3624, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m1_556(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t1_1501 * L_7 = (ArgumentOutOfRangeException_t1_1501 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1_13275(L_7, _stringLiteral934, L_6, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_7);
	}

IL_0032:
	{
		int32_t L_8 = ___year;
		int32_t L_9 = ((CurrentSystemTimeZone_t1_1609_StaticFields*)CurrentSystemTimeZone_t1_1609_il2cpp_TypeInfo_var->static_fields)->___this_year_7;
		if ((!(((uint32_t)L_8) == ((uint32_t)L_9))))
		{
			goto IL_0043;
		}
	}
	{
		DaylightTime_t1_367 * L_10 = ((CurrentSystemTimeZone_t1_1609_StaticFields*)CurrentSystemTimeZone_t1_1609_il2cpp_TypeInfo_var->static_fields)->___this_year_dlt_8;
		return L_10;
	}

IL_0043:
	{
		Hashtable_t1_100 * L_11 = (__this->___m_CachedDaylightChanges_3);
		V_0 = L_11;
		Hashtable_t1_100 * L_12 = V_0;
		Monitor_Enter_m1_12697(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
	}

IL_0050:
	try
	{ // begin try (depth: 1)
		{
			Hashtable_t1_100 * L_13 = (__this->___m_CachedDaylightChanges_3);
			int32_t L_14 = ___year;
			int32_t L_15 = L_14;
			Object_t * L_16 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_15);
			NullCheck(L_13);
			Object_t * L_17 = (Object_t *)VirtFuncInvoker1< Object_t *, Object_t * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_13, L_16);
			V_1 = ((DaylightTime_t1_367 *)CastclassClass(L_17, DaylightTime_t1_367_il2cpp_TypeInfo_var));
			DaylightTime_t1_367 * L_18 = V_1;
			if (L_18)
			{
				goto IL_00b1;
			}
		}

IL_006d:
		{
			int32_t L_19 = ___year;
			bool L_20 = CurrentSystemTimeZone_GetTimeZoneData_m1_14678(NULL /*static, unused*/, L_19, (&V_2), (&V_3), /*hidden argument*/NULL);
			if (L_20)
			{
				goto IL_0097;
			}
		}

IL_007c:
		{
			int32_t L_21 = ___year;
			int32_t L_22 = L_21;
			Object_t * L_23 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_22);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_24 = String_Concat_m1_556(NULL /*static, unused*/, _stringLiteral3625, L_23, /*hidden argument*/NULL);
			String_t* L_25 = Locale_GetText_m1_1404(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
			ArgumentException_t1_1425 * L_26 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
			ArgumentException__ctor_m1_13260(L_26, L_25, /*hidden argument*/NULL);
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_26);
		}

IL_0097:
		{
			Int64U5BU5D_t1_1665* L_27 = V_2;
			DaylightTime_t1_367 * L_28 = CurrentSystemTimeZone_GetDaylightTimeFromData_m1_14684(__this, L_27, /*hidden argument*/NULL);
			V_1 = L_28;
			Hashtable_t1_100 * L_29 = (__this->___m_CachedDaylightChanges_3);
			int32_t L_30 = ___year;
			int32_t L_31 = L_30;
			Object_t * L_32 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_31);
			DaylightTime_t1_367 * L_33 = V_1;
			NullCheck(L_29);
			VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_29, L_32, L_33);
		}

IL_00b1:
		{
			DaylightTime_t1_367 * L_34 = V_1;
			V_4 = L_34;
			IL2CPP_LEAVE(0xC5, FINALLY_00be);
		}

IL_00b9:
		{
			; // IL_00b9: leave IL_00c5
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_00be;
	}

FINALLY_00be:
	{ // begin finally (depth: 1)
		Hashtable_t1_100 * L_35 = V_0;
		Monitor_Exit_m1_12698(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(190)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(190)
	{
		IL2CPP_JUMP_TBL(0xC5, IL_00c5)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_00c5:
	{
		DaylightTime_t1_367 * L_36 = V_4;
		return L_36;
	}
}
// System.TimeSpan System.CurrentSystemTimeZone::GetUtcOffset(System.DateTime)
extern "C" TimeSpan_t1_368  CurrentSystemTimeZone_GetUtcOffset_m1_14682 (CurrentSystemTimeZone_t1_1609 * __this, DateTime_t1_150  ___time, const MethodInfo* method)
{
	{
		DateTime_t1_150  L_0 = ___time;
		bool L_1 = (bool)VirtFuncInvoker1< bool, DateTime_t1_150  >::Invoke(8 /* System.Boolean System.TimeZone::IsDaylightSavingTime(System.DateTime) */, __this, L_0);
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		TimeSpan_t1_368  L_2 = (__this->___utcOffsetWithDLS_6);
		return L_2;
	}

IL_0013:
	{
		TimeSpan_t1_368  L_3 = (__this->___utcOffsetWithOutDLS_5);
		return L_3;
	}
}
// System.Void System.CurrentSystemTimeZone::OnDeserialization(System.Globalization.DaylightTime)
extern TypeInfo* DateTime_t1_150_il2cpp_TypeInfo_var;
extern TypeInfo* CurrentSystemTimeZone_t1_1609_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3625;
extern "C" void CurrentSystemTimeZone_OnDeserialization_m1_14683 (CurrentSystemTimeZone_t1_1609 * __this, DaylightTime_t1_367 * ___dlt, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DateTime_t1_150_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		CurrentSystemTimeZone_t1_1609_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1111);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		_stringLiteral3625 = il2cpp_codegen_string_literal_from_index(3625);
		s_Il2CppMethodIntialized = true;
	}
	Int64U5BU5D_t1_1665* V_0 = {0};
	StringU5BU5D_t1_238* V_1 = {0};
	DateTime_t1_150  V_2 = {0};
	DateTime_t1_150  V_3 = {0};
	TimeSpan_t1_368  V_4 = {0};
	{
		DaylightTime_t1_367 * L_0 = ___dlt;
		if (L_0)
		{
			goto IL_0058;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		DateTime_t1_150  L_1 = DateTime_get_Now_m1_13826(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_1;
		int32_t L_2 = DateTime_get_Year_m1_13830((&V_2), /*hidden argument*/NULL);
		((CurrentSystemTimeZone_t1_1609_StaticFields*)CurrentSystemTimeZone_t1_1609_il2cpp_TypeInfo_var->static_fields)->___this_year_7 = L_2;
		int32_t L_3 = ((CurrentSystemTimeZone_t1_1609_StaticFields*)CurrentSystemTimeZone_t1_1609_il2cpp_TypeInfo_var->static_fields)->___this_year_7;
		bool L_4 = CurrentSystemTimeZone_GetTimeZoneData_m1_14678(NULL /*static, unused*/, L_3, (&V_0), (&V_1), /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_004a;
		}
	}
	{
		int32_t L_5 = ((CurrentSystemTimeZone_t1_1609_StaticFields*)CurrentSystemTimeZone_t1_1609_il2cpp_TypeInfo_var->static_fields)->___this_year_7;
		int32_t L_6 = L_5;
		Object_t * L_7 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_6);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m1_556(NULL /*static, unused*/, _stringLiteral3625, L_7, /*hidden argument*/NULL);
		String_t* L_9 = Locale_GetText_m1_1404(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		ArgumentException_t1_1425 * L_10 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13260(L_10, L_9, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_10);
	}

IL_004a:
	{
		Int64U5BU5D_t1_1665* L_11 = V_0;
		DaylightTime_t1_367 * L_12 = CurrentSystemTimeZone_GetDaylightTimeFromData_m1_14684(__this, L_11, /*hidden argument*/NULL);
		___dlt = L_12;
		goto IL_006b;
	}

IL_0058:
	{
		DaylightTime_t1_367 * L_13 = ___dlt;
		NullCheck(L_13);
		DateTime_t1_150  L_14 = DaylightTime_get_Start_m1_4054(L_13, /*hidden argument*/NULL);
		V_3 = L_14;
		int32_t L_15 = DateTime_get_Year_m1_13830((&V_3), /*hidden argument*/NULL);
		((CurrentSystemTimeZone_t1_1609_StaticFields*)CurrentSystemTimeZone_t1_1609_il2cpp_TypeInfo_var->static_fields)->___this_year_7 = L_15;
	}

IL_006b:
	{
		int64_t L_16 = (__this->___m_ticksOffset_4);
		TimeSpan_t1_368  L_17 = {0};
		TimeSpan__ctor_m1_14618(&L_17, L_16, /*hidden argument*/NULL);
		__this->___utcOffsetWithOutDLS_5 = L_17;
		int64_t L_18 = (__this->___m_ticksOffset_4);
		DaylightTime_t1_367 * L_19 = ___dlt;
		NullCheck(L_19);
		TimeSpan_t1_368  L_20 = DaylightTime_get_Delta_m1_4056(L_19, /*hidden argument*/NULL);
		V_4 = L_20;
		int64_t L_21 = TimeSpan_get_Ticks_m1_14629((&V_4), /*hidden argument*/NULL);
		TimeSpan_t1_368  L_22 = {0};
		TimeSpan__ctor_m1_14618(&L_22, ((int64_t)((int64_t)L_18+(int64_t)L_21)), /*hidden argument*/NULL);
		__this->___utcOffsetWithDLS_6 = L_22;
		DaylightTime_t1_367 * L_23 = ___dlt;
		((CurrentSystemTimeZone_t1_1609_StaticFields*)CurrentSystemTimeZone_t1_1609_il2cpp_TypeInfo_var->static_fields)->___this_year_dlt_8 = L_23;
		return;
	}
}
// System.Globalization.DaylightTime System.CurrentSystemTimeZone::GetDaylightTimeFromData(System.Int64[])
extern TypeInfo* DaylightTime_t1_367_il2cpp_TypeInfo_var;
extern "C" DaylightTime_t1_367 * CurrentSystemTimeZone_GetDaylightTimeFromData_m1_14684 (CurrentSystemTimeZone_t1_1609 * __this, Int64U5BU5D_t1_1665* ___data, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DaylightTime_t1_367_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1112);
		s_Il2CppMethodIntialized = true;
	}
	{
		Int64U5BU5D_t1_1665* L_0 = ___data;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		int32_t L_1 = 0;
		DateTime_t1_150  L_2 = {0};
		DateTime__ctor_m1_13784(&L_2, (*(int64_t*)(int64_t*)SZArrayLdElema(L_0, L_1, sizeof(int64_t))), /*hidden argument*/NULL);
		Int64U5BU5D_t1_1665* L_3 = ___data;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 1);
		int32_t L_4 = 1;
		DateTime_t1_150  L_5 = {0};
		DateTime__ctor_m1_13784(&L_5, (*(int64_t*)(int64_t*)SZArrayLdElema(L_3, L_4, sizeof(int64_t))), /*hidden argument*/NULL);
		Int64U5BU5D_t1_1665* L_6 = ___data;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 3);
		int32_t L_7 = 3;
		TimeSpan_t1_368  L_8 = {0};
		TimeSpan__ctor_m1_14618(&L_8, (*(int64_t*)(int64_t*)SZArrayLdElema(L_6, L_7, sizeof(int64_t))), /*hidden argument*/NULL);
		DaylightTime_t1_367 * L_9 = (DaylightTime_t1_367 *)il2cpp_codegen_object_new (DaylightTime_t1_367_il2cpp_TypeInfo_var);
		DaylightTime__ctor_m1_4053(L_9, L_2, L_5, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Void System.TimeoutException::.ctor()
extern Il2CppCodeGenString* _stringLiteral3626;
extern "C" void TimeoutException__ctor_m1_14685 (TimeoutException_t1_1610 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral3626 = il2cpp_codegen_string_literal_from_index(3626);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral3626, /*hidden argument*/NULL);
		SystemException__ctor_m1_14605(__this, L_0, /*hidden argument*/NULL);
		Exception_set_HResult_m1_1245(__this, ((int32_t)-2146233083), /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.TimeoutException::.ctor(System.String)
extern "C" void TimeoutException__ctor_m1_14686 (TimeoutException_t1_1610 * __this, String_t* ___message, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message;
		SystemException__ctor_m1_14605(__this, L_0, /*hidden argument*/NULL);
		Exception_set_HResult_m1_1245(__this, ((int32_t)-2146233083), /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.TimeoutException::.ctor(System.String,System.Exception)
extern "C" void TimeoutException__ctor_m1_14687 (TimeoutException_t1_1610 * __this, String_t* ___message, Exception_t1_33 * ___innerException, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message;
		Exception_t1_33 * L_1 = ___innerException;
		SystemException__ctor_m1_14607(__this, L_0, L_1, /*hidden argument*/NULL);
		Exception_set_HResult_m1_1245(__this, ((int32_t)-2146233083), /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.TimeoutException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void TimeoutException__ctor_m1_14688 (TimeoutException_t1_1610 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method)
{
	{
		SerializationInfo_t1_293 * L_0 = ___info;
		StreamingContext_t1_1050  L_1 = ___context;
		SystemException__ctor_m1_14606(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.TypeInitializationException::.ctor(System.String,System.Exception)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3627;
extern "C" void TypeInitializationException__ctor_m1_14689 (TypeInitializationException_t1_1612 * __this, String_t* ___fullTypeName, Exception_t1_33 * ___innerException, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral3627 = il2cpp_codegen_string_literal_from_index(3627);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral3627, /*hidden argument*/NULL);
		String_t* L_1 = ___fullTypeName;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m1_559(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		Exception_t1_33 * L_3 = ___innerException;
		SystemException__ctor_m1_14607(__this, L_2, L_3, /*hidden argument*/NULL);
		String_t* L_4 = ___fullTypeName;
		__this->___type_name_12 = L_4;
		return;
	}
}
// System.Void System.TypeInitializationException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern Il2CppCodeGenString* _stringLiteral222;
extern "C" void TypeInitializationException__ctor_m1_14690 (TypeInitializationException_t1_1612 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral222 = il2cpp_codegen_string_literal_from_index(222);
		s_Il2CppMethodIntialized = true;
	}
	{
		SerializationInfo_t1_293 * L_0 = ___info;
		StreamingContext_t1_1050  L_1 = ___context;
		SystemException__ctor_m1_14606(__this, L_0, L_1, /*hidden argument*/NULL);
		SerializationInfo_t1_293 * L_2 = ___info;
		NullCheck(L_2);
		String_t* L_3 = SerializationInfo_GetString_m1_9654(L_2, _stringLiteral222, /*hidden argument*/NULL);
		__this->___type_name_12 = L_3;
		return;
	}
}
// System.String System.TypeInitializationException::get_TypeName()
extern "C" String_t* TypeInitializationException_get_TypeName_m1_14691 (TypeInitializationException_t1_1612 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___type_name_12);
		return L_0;
	}
}
// System.Void System.TypeInitializationException::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern Il2CppCodeGenString* _stringLiteral222;
extern "C" void TypeInitializationException_GetObjectData_m1_14692 (TypeInitializationException_t1_1612 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral222 = il2cpp_codegen_string_literal_from_index(222);
		s_Il2CppMethodIntialized = true;
	}
	{
		SerializationInfo_t1_293 * L_0 = ___info;
		StreamingContext_t1_1050  L_1 = ___context;
		Exception_GetObjectData_m1_1256(__this, L_0, L_1, /*hidden argument*/NULL);
		SerializationInfo_t1_293 * L_2 = ___info;
		String_t* L_3 = (__this->___type_name_12);
		NullCheck(L_2);
		SerializationInfo_AddValue_m1_9642(L_2, _stringLiteral222, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.TypeLoadException::.ctor()
extern Il2CppCodeGenString* _stringLiteral3628;
extern "C" void TypeLoadException__ctor_m1_14693 (TypeLoadException_t1_1534 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral3628 = il2cpp_codegen_string_literal_from_index(3628);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral3628, /*hidden argument*/NULL);
		SystemException__ctor_m1_14605(__this, L_0, /*hidden argument*/NULL);
		Exception_set_HResult_m1_1245(__this, ((int32_t)-2146233054), /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.TypeLoadException::.ctor(System.String)
extern "C" void TypeLoadException__ctor_m1_14694 (TypeLoadException_t1_1534 * __this, String_t* ___message, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message;
		SystemException__ctor_m1_14605(__this, L_0, /*hidden argument*/NULL);
		Exception_set_HResult_m1_1245(__this, ((int32_t)-2146233054), /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.TypeLoadException::.ctor(System.String,System.Exception)
extern "C" void TypeLoadException__ctor_m1_14695 (TypeLoadException_t1_1534 * __this, String_t* ___message, Exception_t1_33 * ___inner, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message;
		Exception_t1_33 * L_1 = ___inner;
		SystemException__ctor_m1_14607(__this, L_0, L_1, /*hidden argument*/NULL);
		Exception_set_HResult_m1_1245(__this, ((int32_t)-2146233054), /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.TypeLoadException::.ctor(System.String,System.String)
extern "C" void TypeLoadException__ctor_m1_14696 (TypeLoadException_t1_1534 * __this, String_t* ___className, String_t* ___assemblyName, const MethodInfo* method)
{
	{
		TypeLoadException__ctor_m1_14693(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___className;
		__this->___className_13 = L_0;
		String_t* L_1 = ___assemblyName;
		__this->___assemblyName_14 = L_1;
		return;
	}
}
// System.Void System.TypeLoadException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral135;
extern Il2CppCodeGenString* _stringLiteral3629;
extern Il2CppCodeGenString* _stringLiteral3630;
extern "C" void TypeLoadException__ctor_m1_14697 (TypeLoadException_t1_1534 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		_stringLiteral135 = il2cpp_codegen_string_literal_from_index(135);
		_stringLiteral3629 = il2cpp_codegen_string_literal_from_index(3629);
		_stringLiteral3630 = il2cpp_codegen_string_literal_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	{
		SerializationInfo_t1_293 * L_0 = ___info;
		StreamingContext_t1_1050  L_1 = ___context;
		SystemException__ctor_m1_14606(__this, L_0, L_1, /*hidden argument*/NULL);
		SerializationInfo_t1_293 * L_2 = ___info;
		if (L_2)
		{
			goto IL_0019;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_3 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_3, _stringLiteral135, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0019:
	{
		SerializationInfo_t1_293 * L_4 = ___info;
		NullCheck(L_4);
		String_t* L_5 = SerializationInfo_GetString_m1_9654(L_4, _stringLiteral3629, /*hidden argument*/NULL);
		__this->___className_13 = L_5;
		SerializationInfo_t1_293 * L_6 = ___info;
		NullCheck(L_6);
		String_t* L_7 = SerializationInfo_GetString_m1_9654(L_6, _stringLiteral3630, /*hidden argument*/NULL);
		__this->___assemblyName_14 = L_7;
		return;
	}
}
// System.String System.TypeLoadException::get_Message()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3631;
extern Il2CppCodeGenString* _stringLiteral3632;
extern "C" String_t* TypeLoadException_get_Message_m1_14698 (TypeLoadException_t1_1534 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral3631 = il2cpp_codegen_string_literal_from_index(3631);
		_stringLiteral3632 = il2cpp_codegen_string_literal_from_index(3632);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = (__this->___className_13);
		if (!L_0)
		{
			goto IL_0053;
		}
	}
	{
		String_t* L_1 = (__this->___assemblyName_14);
		if (!L_1)
		{
			goto IL_0042;
		}
	}
	{
		String_t* L_2 = (__this->___assemblyName_14);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		bool L_4 = String_op_Inequality_m1_602(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0042;
		}
	}
	{
		String_t* L_5 = (__this->___className_13);
		String_t* L_6 = (__this->___assemblyName_14);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Format_m1_549(NULL /*static, unused*/, _stringLiteral3631, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}

IL_0042:
	{
		String_t* L_8 = (__this->___className_13);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Format_m1_548(NULL /*static, unused*/, _stringLiteral3632, L_8, /*hidden argument*/NULL);
		return L_9;
	}

IL_0053:
	{
		String_t* L_10 = Exception_get_Message_m1_1249(__this, /*hidden argument*/NULL);
		return L_10;
	}
}
// System.String System.TypeLoadException::get_TypeName()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" String_t* TypeLoadException_get_TypeName_m1_14699 (TypeLoadException_t1_1534 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = (__this->___className_13);
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		return L_1;
	}

IL_0011:
	{
		String_t* L_2 = (__this->___className_13);
		return L_2;
	}
}
// System.Void System.TypeLoadException::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const Il2CppType* String_t_0_0_0_var;
extern const Il2CppType* Int32_t1_3_0_0_0_var;
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral135;
extern Il2CppCodeGenString* _stringLiteral3629;
extern Il2CppCodeGenString* _stringLiteral3630;
extern Il2CppCodeGenString* _stringLiteral3633;
extern Il2CppCodeGenString* _stringLiteral3634;
extern "C" void TypeLoadException_GetObjectData_m1_14700 (TypeLoadException_t1_1534 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_0_0_0_var = il2cpp_codegen_type_from_index(14);
		Int32_t1_3_0_0_0_var = il2cpp_codegen_type_from_index(11);
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral135 = il2cpp_codegen_string_literal_from_index(135);
		_stringLiteral3629 = il2cpp_codegen_string_literal_from_index(3629);
		_stringLiteral3630 = il2cpp_codegen_string_literal_from_index(3630);
		_stringLiteral3633 = il2cpp_codegen_string_literal_from_index(3633);
		_stringLiteral3634 = il2cpp_codegen_string_literal_from_index(3634);
		s_Il2CppMethodIntialized = true;
	}
	{
		SerializationInfo_t1_293 * L_0 = ___info;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, _stringLiteral135, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		SerializationInfo_t1_293 * L_2 = ___info;
		StreamingContext_t1_1050  L_3 = ___context;
		Exception_GetObjectData_m1_1256(__this, L_2, L_3, /*hidden argument*/NULL);
		SerializationInfo_t1_293 * L_4 = ___info;
		String_t* L_5 = (__this->___className_13);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_4);
		SerializationInfo_AddValue_m1_9624(L_4, _stringLiteral3629, L_5, L_6, /*hidden argument*/NULL);
		SerializationInfo_t1_293 * L_7 = ___info;
		String_t* L_8 = (__this->___assemblyName_14);
		Type_t * L_9 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_7);
		SerializationInfo_AddValue_m1_9624(L_7, _stringLiteral3630, L_8, L_9, /*hidden argument*/NULL);
		SerializationInfo_t1_293 * L_10 = ___info;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		Type_t * L_12 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_10);
		SerializationInfo_AddValue_m1_9624(L_10, _stringLiteral3633, L_11, L_12, /*hidden argument*/NULL);
		SerializationInfo_t1_293 * L_13 = ___info;
		int32_t L_14 = 0;
		Object_t * L_15 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_14);
		Type_t * L_16 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(Int32_t1_3_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_13);
		SerializationInfo_AddValue_m1_9624(L_13, _stringLiteral3634, L_15, L_16, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.TypeUnloadedException::.ctor()
extern Il2CppCodeGenString* _stringLiteral3635;
extern "C" void TypeUnloadedException__ctor_m1_14701 (TypeUnloadedException_t1_1613 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral3635 = il2cpp_codegen_string_literal_from_index(3635);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral3635, /*hidden argument*/NULL);
		SystemException__ctor_m1_14605(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.TypeUnloadedException::.ctor(System.String)
extern "C" void TypeUnloadedException__ctor_m1_14702 (TypeUnloadedException_t1_1613 * __this, String_t* ___message, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message;
		SystemException__ctor_m1_14605(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.TypeUnloadedException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void TypeUnloadedException__ctor_m1_14703 (TypeUnloadedException_t1_1613 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method)
{
	{
		SerializationInfo_t1_293 * L_0 = ___info;
		StreamingContext_t1_1050  L_1 = ___context;
		SystemException__ctor_m1_14606(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.TypeUnloadedException::.ctor(System.String,System.Exception)
extern "C" void TypeUnloadedException__ctor_m1_14704 (TypeUnloadedException_t1_1613 * __this, String_t* ___message, Exception_t1_33 * ___innerException, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message;
		Exception_t1_33 * L_1 = ___innerException;
		SystemException__ctor_m1_14607(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.UnauthorizedAccessException::.ctor()
extern Il2CppCodeGenString* _stringLiteral3636;
extern "C" void UnauthorizedAccessException__ctor_m1_14705 (UnauthorizedAccessException_t1_1168 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral3636 = il2cpp_codegen_string_literal_from_index(3636);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral3636, /*hidden argument*/NULL);
		SystemException__ctor_m1_14605(__this, L_0, /*hidden argument*/NULL);
		Exception_set_HResult_m1_1245(__this, ((int32_t)-2146233088), /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.UnauthorizedAccessException::.ctor(System.String)
extern "C" void UnauthorizedAccessException__ctor_m1_14706 (UnauthorizedAccessException_t1_1168 * __this, String_t* ___message, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message;
		SystemException__ctor_m1_14605(__this, L_0, /*hidden argument*/NULL);
		Exception_set_HResult_m1_1245(__this, ((int32_t)-2146233088), /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.UnauthorizedAccessException::.ctor(System.String,System.Exception)
extern "C" void UnauthorizedAccessException__ctor_m1_14707 (UnauthorizedAccessException_t1_1168 * __this, String_t* ___message, Exception_t1_33 * ___inner, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message;
		Exception_t1_33 * L_1 = ___inner;
		SystemException__ctor_m1_14607(__this, L_0, L_1, /*hidden argument*/NULL);
		Exception_set_HResult_m1_1245(__this, ((int32_t)-2146233088), /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.UnauthorizedAccessException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void UnauthorizedAccessException__ctor_m1_14708 (UnauthorizedAccessException_t1_1168 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method)
{
	{
		SerializationInfo_t1_293 * L_0 = ___info;
		StreamingContext_t1_1050  L_1 = ___context;
		SystemException__ctor_m1_14606(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.UnhandledExceptionEventArgs::.ctor(System.Object,System.Boolean)
extern TypeInfo* EventArgs_t1_158_il2cpp_TypeInfo_var;
extern "C" void UnhandledExceptionEventArgs__ctor_m1_14709 (UnhandledExceptionEventArgs_t1_1614 * __this, Object_t * ___exception, bool ___isTerminating, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		EventArgs_t1_158_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1074);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t1_158_il2cpp_TypeInfo_var);
		EventArgs__ctor_m1_14088(__this, /*hidden argument*/NULL);
		Object_t * L_0 = ___exception;
		__this->___exception_1 = L_0;
		bool L_1 = ___isTerminating;
		__this->___m_isTerminating_2 = L_1;
		return;
	}
}
// System.Object System.UnhandledExceptionEventArgs::get_ExceptionObject()
extern "C" Object_t * UnhandledExceptionEventArgs_get_ExceptionObject_m1_14710 (UnhandledExceptionEventArgs_t1_1614 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___exception_1);
		return L_0;
	}
}
// System.Boolean System.UnhandledExceptionEventArgs::get_IsTerminating()
extern "C" bool UnhandledExceptionEventArgs_get_IsTerminating_m1_14711 (UnhandledExceptionEventArgs_t1_1614 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_isTerminating_2);
		return L_0;
	}
}
// System.Void System.UnitySerializationHolder::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern Il2CppCodeGenString* _stringLiteral248;
extern Il2CppCodeGenString* _stringLiteral3637;
extern Il2CppCodeGenString* _stringLiteral1968;
extern "C" void UnitySerializationHolder__ctor_m1_14712 (UnitySerializationHolder_t1_1616 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___ctx, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral248 = il2cpp_codegen_string_literal_from_index(248);
		_stringLiteral3637 = il2cpp_codegen_string_literal_from_index(3637);
		_stringLiteral1968 = il2cpp_codegen_string_literal_from_index(1968);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		SerializationInfo_t1_293 * L_0 = ___info;
		NullCheck(L_0);
		String_t* L_1 = SerializationInfo_GetString_m1_9654(L_0, _stringLiteral248, /*hidden argument*/NULL);
		__this->____data_0 = L_1;
		SerializationInfo_t1_293 * L_2 = ___info;
		NullCheck(L_2);
		int32_t L_3 = SerializationInfo_GetInt32_m1_9650(L_2, _stringLiteral3637, /*hidden argument*/NULL);
		__this->____unityType_1 = (((int32_t)((uint8_t)L_3)));
		SerializationInfo_t1_293 * L_4 = ___info;
		NullCheck(L_4);
		String_t* L_5 = SerializationInfo_GetString_m1_9654(L_4, _stringLiteral1968, /*hidden argument*/NULL);
		__this->____assemblyName_2 = L_5;
		return;
	}
}
// System.Void System.UnitySerializationHolder::GetTypeData(System.Type,System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const Il2CppType* UnitySerializationHolder_t1_1616_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral248;
extern Il2CppCodeGenString* _stringLiteral3637;
extern Il2CppCodeGenString* _stringLiteral1968;
extern "C" void UnitySerializationHolder_GetTypeData_m1_14713 (Object_t * __this /* static, unused */, Type_t * ___instance, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___ctx, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnitySerializationHolder_t1_1616_0_0_0_var = il2cpp_codegen_type_from_index(1113);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		_stringLiteral248 = il2cpp_codegen_string_literal_from_index(248);
		_stringLiteral3637 = il2cpp_codegen_string_literal_from_index(3637);
		_stringLiteral1968 = il2cpp_codegen_string_literal_from_index(1968);
		s_Il2CppMethodIntialized = true;
	}
	{
		SerializationInfo_t1_293 * L_0 = ___info;
		Type_t * L_1 = ___instance;
		NullCheck(L_1);
		String_t* L_2 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(159 /* System.String System.Type::get_FullName() */, L_1);
		NullCheck(L_0);
		SerializationInfo_AddValue_m1_9642(L_0, _stringLiteral248, L_2, /*hidden argument*/NULL);
		SerializationInfo_t1_293 * L_3 = ___info;
		NullCheck(L_3);
		SerializationInfo_AddValue_m1_9630(L_3, _stringLiteral3637, 4, /*hidden argument*/NULL);
		SerializationInfo_t1_293 * L_4 = ___info;
		Type_t * L_5 = ___instance;
		NullCheck(L_5);
		Assembly_t1_467 * L_6 = (Assembly_t1_467 *)VirtFuncInvoker0< Assembly_t1_467 * >::Invoke(156 /* System.Reflection.Assembly System.Type::get_Assembly() */, L_5);
		NullCheck(L_6);
		String_t* L_7 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(51 /* System.String System.Reflection.Assembly::get_FullName() */, L_6);
		NullCheck(L_4);
		SerializationInfo_AddValue_m1_9642(L_4, _stringLiteral1968, L_7, /*hidden argument*/NULL);
		SerializationInfo_t1_293 * L_8 = ___info;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_9 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(UnitySerializationHolder_t1_1616_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_8);
		SerializationInfo_SetType_m1_9626(L_8, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.UnitySerializationHolder::GetDBNullData(System.DBNull,System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const Il2CppType* UnitySerializationHolder_t1_1616_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral248;
extern Il2CppCodeGenString* _stringLiteral3637;
extern Il2CppCodeGenString* _stringLiteral1968;
extern "C" void UnitySerializationHolder_GetDBNullData_m1_14714 (Object_t * __this /* static, unused */, DBNull_t1_1523 * ___instance, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___ctx, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnitySerializationHolder_t1_1616_0_0_0_var = il2cpp_codegen_type_from_index(1113);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		_stringLiteral248 = il2cpp_codegen_string_literal_from_index(248);
		_stringLiteral3637 = il2cpp_codegen_string_literal_from_index(3637);
		_stringLiteral1968 = il2cpp_codegen_string_literal_from_index(1968);
		s_Il2CppMethodIntialized = true;
	}
	{
		SerializationInfo_t1_293 * L_0 = ___info;
		NullCheck(L_0);
		SerializationInfo_AddValue_m1_9642(L_0, _stringLiteral248, NULL, /*hidden argument*/NULL);
		SerializationInfo_t1_293 * L_1 = ___info;
		NullCheck(L_1);
		SerializationInfo_AddValue_m1_9630(L_1, _stringLiteral3637, 2, /*hidden argument*/NULL);
		SerializationInfo_t1_293 * L_2 = ___info;
		DBNull_t1_1523 * L_3 = ___instance;
		NullCheck(L_3);
		Type_t * L_4 = Object_GetType_m1_5(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Assembly_t1_467 * L_5 = (Assembly_t1_467 *)VirtFuncInvoker0< Assembly_t1_467 * >::Invoke(156 /* System.Reflection.Assembly System.Type::get_Assembly() */, L_4);
		NullCheck(L_5);
		String_t* L_6 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(51 /* System.String System.Reflection.Assembly::get_FullName() */, L_5);
		NullCheck(L_2);
		SerializationInfo_AddValue_m1_9642(L_2, _stringLiteral1968, L_6, /*hidden argument*/NULL);
		SerializationInfo_t1_293 * L_7 = ___info;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(UnitySerializationHolder_t1_1616_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_7);
		SerializationInfo_SetType_m1_9626(L_7, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.UnitySerializationHolder::GetAssemblyData(System.Reflection.Assembly,System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const Il2CppType* UnitySerializationHolder_t1_1616_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral248;
extern Il2CppCodeGenString* _stringLiteral3637;
extern Il2CppCodeGenString* _stringLiteral1968;
extern "C" void UnitySerializationHolder_GetAssemblyData_m1_14715 (Object_t * __this /* static, unused */, Assembly_t1_467 * ___instance, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___ctx, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnitySerializationHolder_t1_1616_0_0_0_var = il2cpp_codegen_type_from_index(1113);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		_stringLiteral248 = il2cpp_codegen_string_literal_from_index(248);
		_stringLiteral3637 = il2cpp_codegen_string_literal_from_index(3637);
		_stringLiteral1968 = il2cpp_codegen_string_literal_from_index(1968);
		s_Il2CppMethodIntialized = true;
	}
	{
		SerializationInfo_t1_293 * L_0 = ___info;
		Assembly_t1_467 * L_1 = ___instance;
		NullCheck(L_1);
		String_t* L_2 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(51 /* System.String System.Reflection.Assembly::get_FullName() */, L_1);
		NullCheck(L_0);
		SerializationInfo_AddValue_m1_9642(L_0, _stringLiteral248, L_2, /*hidden argument*/NULL);
		SerializationInfo_t1_293 * L_3 = ___info;
		NullCheck(L_3);
		SerializationInfo_AddValue_m1_9630(L_3, _stringLiteral3637, 6, /*hidden argument*/NULL);
		SerializationInfo_t1_293 * L_4 = ___info;
		Assembly_t1_467 * L_5 = ___instance;
		NullCheck(L_5);
		String_t* L_6 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(51 /* System.String System.Reflection.Assembly::get_FullName() */, L_5);
		NullCheck(L_4);
		SerializationInfo_AddValue_m1_9642(L_4, _stringLiteral1968, L_6, /*hidden argument*/NULL);
		SerializationInfo_t1_293 * L_7 = ___info;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(UnitySerializationHolder_t1_1616_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_7);
		SerializationInfo_SetType_m1_9626(L_7, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.UnitySerializationHolder::GetModuleData(System.Reflection.Module,System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const Il2CppType* UnitySerializationHolder_t1_1616_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral248;
extern Il2CppCodeGenString* _stringLiteral3637;
extern Il2CppCodeGenString* _stringLiteral1968;
extern "C" void UnitySerializationHolder_GetModuleData_m1_14716 (Object_t * __this /* static, unused */, Module_t1_495 * ___instance, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___ctx, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnitySerializationHolder_t1_1616_0_0_0_var = il2cpp_codegen_type_from_index(1113);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		_stringLiteral248 = il2cpp_codegen_string_literal_from_index(248);
		_stringLiteral3637 = il2cpp_codegen_string_literal_from_index(3637);
		_stringLiteral1968 = il2cpp_codegen_string_literal_from_index(1968);
		s_Il2CppMethodIntialized = true;
	}
	{
		SerializationInfo_t1_293 * L_0 = ___info;
		Module_t1_495 * L_1 = ___instance;
		NullCheck(L_1);
		String_t* L_2 = Module_get_ScopeName_m1_6963(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		SerializationInfo_AddValue_m1_9642(L_0, _stringLiteral248, L_2, /*hidden argument*/NULL);
		SerializationInfo_t1_293 * L_3 = ___info;
		NullCheck(L_3);
		SerializationInfo_AddValue_m1_9630(L_3, _stringLiteral3637, 5, /*hidden argument*/NULL);
		SerializationInfo_t1_293 * L_4 = ___info;
		Module_t1_495 * L_5 = ___instance;
		NullCheck(L_5);
		Assembly_t1_467 * L_6 = Module_get_Assembly_m1_6960(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		String_t* L_7 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(51 /* System.String System.Reflection.Assembly::get_FullName() */, L_6);
		NullCheck(L_4);
		SerializationInfo_AddValue_m1_9642(L_4, _stringLiteral1968, L_7, /*hidden argument*/NULL);
		SerializationInfo_t1_293 * L_8 = ___info;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_9 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(UnitySerializationHolder_t1_1616_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_8);
		SerializationInfo_SetType_m1_9626(L_8, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.UnitySerializationHolder::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern "C" void UnitySerializationHolder_GetObjectData_m1_14717 (UnitySerializationHolder_t1_1616 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14416(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Object System.UnitySerializationHolder::GetRealObject(System.Runtime.Serialization.StreamingContext)
extern TypeInfo* DBNull_t1_1523_il2cpp_TypeInfo_var;
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3638;
extern "C" Object_t * UnitySerializationHolder_GetRealObject_m1_14718 (UnitySerializationHolder_t1_1616 * __this, StreamingContext_t1_1050  ___context, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DBNull_t1_1523_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1079);
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		_stringLiteral3638 = il2cpp_codegen_string_literal_from_index(3638);
		s_Il2CppMethodIntialized = true;
	}
	Assembly_t1_467 * V_0 = {0};
	Assembly_t1_467 * V_1 = {0};
	uint8_t V_2 = {0};
	{
		uint8_t L_0 = (__this->____unityType_1);
		V_2 = L_0;
		uint8_t L_1 = V_2;
		if (((int32_t)((int32_t)L_1-(int32_t)2)) == 0)
		{
			goto IL_0041;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)2)) == 1)
		{
			goto IL_006c;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)2)) == 2)
		{
			goto IL_0028;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)2)) == 3)
		{
			goto IL_0047;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)2)) == 4)
		{
			goto IL_0060;
		}
	}
	{
		goto IL_006c;
	}

IL_0028:
	{
		String_t* L_2 = (__this->____assemblyName_2);
		Assembly_t1_467 * L_3 = Assembly_Load_m1_6623(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Assembly_t1_467 * L_4 = V_0;
		String_t* L_5 = (__this->____data_0);
		NullCheck(L_4);
		Type_t * L_6 = (Type_t *)VirtFuncInvoker1< Type_t *, String_t* >::Invoke(69 /* System.Type System.Reflection.Assembly::GetType(System.String) */, L_4, L_5);
		return L_6;
	}

IL_0041:
	{
		IL2CPP_RUNTIME_CLASS_INIT(DBNull_t1_1523_il2cpp_TypeInfo_var);
		DBNull_t1_1523 * L_7 = ((DBNull_t1_1523_StaticFields*)DBNull_t1_1523_il2cpp_TypeInfo_var->static_fields)->___Value_0;
		return L_7;
	}

IL_0047:
	{
		String_t* L_8 = (__this->____assemblyName_2);
		Assembly_t1_467 * L_9 = Assembly_Load_m1_6623(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		Assembly_t1_467 * L_10 = V_1;
		String_t* L_11 = (__this->____data_0);
		NullCheck(L_10);
		Module_t1_495 * L_12 = (Module_t1_495 *)VirtFuncInvoker1< Module_t1_495 *, String_t* >::Invoke(47 /* System.Reflection.Module System.Reflection.Assembly::GetModule(System.String) */, L_10, L_11);
		return L_12;
	}

IL_0060:
	{
		String_t* L_13 = (__this->____data_0);
		Assembly_t1_467 * L_14 = Assembly_Load_m1_6623(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		return L_14;
	}

IL_006c:
	{
		String_t* L_15 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral3638, /*hidden argument*/NULL);
		NotSupportedException_t1_1583 * L_16 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14417(L_16, L_15, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_16);
	}
}
// System.Void System.Variant::SetValue(System.Object)
extern const Il2CppType* SByte_t1_12_0_0_0_var;
extern const Il2CppType* Byte_t1_11_0_0_0_var;
extern const Il2CppType* Int16_t1_13_0_0_0_var;
extern const Il2CppType* UInt16_t1_14_0_0_0_var;
extern const Il2CppType* Int32_t1_3_0_0_0_var;
extern const Il2CppType* UInt32_t1_8_0_0_0_var;
extern const Il2CppType* Int64_t1_7_0_0_0_var;
extern const Il2CppType* UInt64_t1_10_0_0_0_var;
extern const Il2CppType* Single_t1_17_0_0_0_var;
extern const Il2CppType* Double_t1_18_0_0_0_var;
extern const Il2CppType* String_t_0_0_0_var;
extern const Il2CppType* Boolean_t1_20_0_0_0_var;
extern const Il2CppType* BStrWrapper_t1_757_0_0_0_var;
extern const Il2CppType* UnknownWrapper_t1_842_0_0_0_var;
extern const Il2CppType* DispatchWrapper_t1_781_0_0_0_var;
extern TypeInfo* Enum_t1_24_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* SByte_t1_12_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern TypeInfo* Int16_t1_13_il2cpp_TypeInfo_var;
extern TypeInfo* UInt16_t1_14_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* UInt32_t1_8_il2cpp_TypeInfo_var;
extern TypeInfo* Int64_t1_7_il2cpp_TypeInfo_var;
extern TypeInfo* UInt64_t1_10_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t1_17_il2cpp_TypeInfo_var;
extern TypeInfo* Double_t1_18_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t1_20_il2cpp_TypeInfo_var;
extern TypeInfo* BStrWrapper_t1_757_il2cpp_TypeInfo_var;
extern TypeInfo* UnknownWrapper_t1_842_il2cpp_TypeInfo_var;
extern TypeInfo* DispatchWrapper_t1_781_il2cpp_TypeInfo_var;
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t1_33_il2cpp_TypeInfo_var;
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3639;
extern "C" void Variant_SetValue_m1_14719 (Variant_t1_1617 * __this, Object_t * ___obj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SByte_t1_12_0_0_0_var = il2cpp_codegen_type_from_index(27);
		Byte_t1_11_0_0_0_var = il2cpp_codegen_type_from_index(25);
		Int16_t1_13_0_0_0_var = il2cpp_codegen_type_from_index(28);
		UInt16_t1_14_0_0_0_var = il2cpp_codegen_type_from_index(29);
		Int32_t1_3_0_0_0_var = il2cpp_codegen_type_from_index(11);
		UInt32_t1_8_0_0_0_var = il2cpp_codegen_type_from_index(23);
		Int64_t1_7_0_0_0_var = il2cpp_codegen_type_from_index(21);
		UInt64_t1_10_0_0_0_var = il2cpp_codegen_type_from_index(24);
		Single_t1_17_0_0_0_var = il2cpp_codegen_type_from_index(47);
		Double_t1_18_0_0_0_var = il2cpp_codegen_type_from_index(48);
		String_t_0_0_0_var = il2cpp_codegen_type_from_index(14);
		Boolean_t1_20_0_0_0_var = il2cpp_codegen_type_from_index(54);
		BStrWrapper_t1_757_0_0_0_var = il2cpp_codegen_type_from_index(1114);
		UnknownWrapper_t1_842_0_0_0_var = il2cpp_codegen_type_from_index(1115);
		DispatchWrapper_t1_781_0_0_0_var = il2cpp_codegen_type_from_index(1116);
		Enum_t1_24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		SByte_t1_12_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(27);
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		Int16_t1_13_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(28);
		UInt16_t1_14_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		UInt32_t1_8_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		Int64_t1_7_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(21);
		UInt64_t1_10_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(24);
		Single_t1_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		Double_t1_18_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(48);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		Boolean_t1_20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(54);
		BStrWrapper_t1_757_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1114);
		UnknownWrapper_t1_842_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1115);
		DispatchWrapper_t1_781_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1116);
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		Exception_t1_33_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(49);
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		_stringLiteral3639 = il2cpp_codegen_string_literal_from_index(3639);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = {0};
	Exception_t1_33 * V_1 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	Variant_t1_1617 * G_B29_0 = {0};
	Variant_t1_1617 * G_B28_0 = {0};
	int32_t G_B30_0 = 0;
	Variant_t1_1617 * G_B30_1 = {0};
	{
		__this->___vt_0 = 0;
		Object_t * L_0 = ___obj;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		return;
	}

IL_000e:
	{
		Object_t * L_1 = ___obj;
		NullCheck(L_1);
		Type_t * L_2 = Object_GetType_m1_5(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Type_t * L_3 = V_0;
		NullCheck(L_3);
		bool L_4 = (bool)VirtFuncInvoker0< bool >::Invoke(127 /* System.Boolean System.Type::get_IsEnum() */, L_3);
		if (!L_4)
		{
			goto IL_0027;
		}
	}
	{
		Type_t * L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t1_24_il2cpp_TypeInfo_var);
		Type_t * L_6 = Enum_GetUnderlyingType_m1_942(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
	}

IL_0027:
	{
		Type_t * L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(SByte_t1_12_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_7) == ((Object_t*)(Type_t *)L_8))))
		{
			goto IL_0050;
		}
	}
	{
		__this->___vt_0 = ((int32_t)16);
		Object_t * L_9 = ___obj;
		__this->___cVal_12 = ((*(int8_t*)((int8_t*)UnBox (L_9, SByte_t1_12_il2cpp_TypeInfo_var))));
		goto IL_0311;
	}

IL_0050:
	{
		Type_t * L_10 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_11 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(Byte_t1_11_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_10) == ((Object_t*)(Type_t *)L_11))))
		{
			goto IL_0079;
		}
	}
	{
		__this->___vt_0 = ((int32_t)17);
		Object_t * L_12 = ___obj;
		__this->___bVal_6 = ((*(uint8_t*)((uint8_t*)UnBox (L_12, Byte_t1_11_il2cpp_TypeInfo_var))));
		goto IL_0311;
	}

IL_0079:
	{
		Type_t * L_13 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_14 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(Int16_t1_13_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_13) == ((Object_t*)(Type_t *)L_14))))
		{
			goto IL_00a1;
		}
	}
	{
		__this->___vt_0 = 2;
		Object_t * L_15 = ___obj;
		__this->___iVal_7 = ((*(int16_t*)((int16_t*)UnBox (L_15, Int16_t1_13_il2cpp_TypeInfo_var))));
		goto IL_0311;
	}

IL_00a1:
	{
		Type_t * L_16 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_17 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(UInt16_t1_14_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_16) == ((Object_t*)(Type_t *)L_17))))
		{
			goto IL_00ca;
		}
	}
	{
		__this->___vt_0 = ((int32_t)18);
		Object_t * L_18 = ___obj;
		__this->___uiVal_13 = ((*(uint16_t*)((uint16_t*)UnBox (L_18, UInt16_t1_14_il2cpp_TypeInfo_var))));
		goto IL_0311;
	}

IL_00ca:
	{
		Type_t * L_19 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_20 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(Int32_t1_3_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_19) == ((Object_t*)(Type_t *)L_20))))
		{
			goto IL_00f2;
		}
	}
	{
		__this->___vt_0 = 3;
		Object_t * L_21 = ___obj;
		__this->___lVal_5 = ((*(int32_t*)((int32_t*)UnBox (L_21, Int32_t1_3_il2cpp_TypeInfo_var))));
		goto IL_0311;
	}

IL_00f2:
	{
		Type_t * L_22 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_23 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(UInt32_t1_8_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_22) == ((Object_t*)(Type_t *)L_23))))
		{
			goto IL_011b;
		}
	}
	{
		__this->___vt_0 = ((int32_t)19);
		Object_t * L_24 = ___obj;
		__this->___ulVal_14 = ((*(uint32_t*)((uint32_t*)UnBox (L_24, UInt32_t1_8_il2cpp_TypeInfo_var))));
		goto IL_0311;
	}

IL_011b:
	{
		Type_t * L_25 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_26 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(Int64_t1_7_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_25) == ((Object_t*)(Type_t *)L_26))))
		{
			goto IL_0144;
		}
	}
	{
		__this->___vt_0 = ((int32_t)20);
		Object_t * L_27 = ___obj;
		__this->___llVal_4 = ((*(int64_t*)((int64_t*)UnBox (L_27, Int64_t1_7_il2cpp_TypeInfo_var))));
		goto IL_0311;
	}

IL_0144:
	{
		Type_t * L_28 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_29 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(UInt64_t1_10_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_28) == ((Object_t*)(Type_t *)L_29))))
		{
			goto IL_016d;
		}
	}
	{
		__this->___vt_0 = ((int32_t)21);
		Object_t * L_30 = ___obj;
		__this->___ullVal_15 = ((*(uint64_t*)((uint64_t*)UnBox (L_30, UInt64_t1_10_il2cpp_TypeInfo_var))));
		goto IL_0311;
	}

IL_016d:
	{
		Type_t * L_31 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_32 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(Single_t1_17_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_31) == ((Object_t*)(Type_t *)L_32))))
		{
			goto IL_0195;
		}
	}
	{
		__this->___vt_0 = 4;
		Object_t * L_33 = ___obj;
		__this->___fltVal_8 = ((*(float*)((float*)UnBox (L_33, Single_t1_17_il2cpp_TypeInfo_var))));
		goto IL_0311;
	}

IL_0195:
	{
		Type_t * L_34 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_35 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(Double_t1_18_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_34) == ((Object_t*)(Type_t *)L_35))))
		{
			goto IL_01bd;
		}
	}
	{
		__this->___vt_0 = 5;
		Object_t * L_36 = ___obj;
		__this->___dblVal_9 = ((*(double*)((double*)UnBox (L_36, Double_t1_18_il2cpp_TypeInfo_var))));
		goto IL_0311;
	}

IL_01bd:
	{
		Type_t * L_37 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_38 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_37) == ((Object_t*)(Type_t *)L_38))))
		{
			goto IL_01ea;
		}
	}
	{
		__this->___vt_0 = 8;
		Object_t * L_39 = ___obj;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		IntPtr_t L_40 = Marshal_StringToBSTR_m1_7824(NULL /*static, unused*/, ((String_t*)CastclassSealed(L_39, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		__this->___bstrVal_11 = L_40;
		goto IL_0311;
	}

IL_01ea:
	{
		Type_t * L_41 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_42 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(Boolean_t1_20_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_41) == ((Object_t*)(Type_t *)L_42))))
		{
			goto IL_021f;
		}
	}
	{
		__this->___vt_0 = ((int32_t)11);
		Object_t * L_43 = ___obj;
		G_B28_0 = __this;
		if (!((*(bool*)((bool*)UnBox (L_43, Boolean_t1_20_il2cpp_TypeInfo_var)))))
		{
			G_B29_0 = __this;
			goto IL_0214;
		}
	}
	{
		G_B30_0 = (-1);
		G_B30_1 = G_B28_0;
		goto IL_0215;
	}

IL_0214:
	{
		G_B30_0 = 0;
		G_B30_1 = G_B29_0;
	}

IL_0215:
	{
		G_B30_1->___lVal_5 = G_B30_0;
		goto IL_0311;
	}

IL_021f:
	{
		Type_t * L_44 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_45 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(BStrWrapper_t1_757_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_44) == ((Object_t*)(Type_t *)L_45))))
		{
			goto IL_0251;
		}
	}
	{
		__this->___vt_0 = 8;
		Object_t * L_46 = ___obj;
		NullCheck(((BStrWrapper_t1_757 *)CastclassSealed(L_46, BStrWrapper_t1_757_il2cpp_TypeInfo_var)));
		String_t* L_47 = BStrWrapper_get_WrappedObject_m1_7587(((BStrWrapper_t1_757 *)CastclassSealed(L_46, BStrWrapper_t1_757_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		IntPtr_t L_48 = Marshal_StringToBSTR_m1_7824(NULL /*static, unused*/, L_47, /*hidden argument*/NULL);
		__this->___bstrVal_11 = L_48;
		goto IL_0311;
	}

IL_0251:
	{
		Type_t * L_49 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_50 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(UnknownWrapper_t1_842_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_49) == ((Object_t*)(Type_t *)L_50))))
		{
			goto IL_0284;
		}
	}
	{
		__this->___vt_0 = ((int32_t)13);
		Object_t * L_51 = ___obj;
		NullCheck(((UnknownWrapper_t1_842 *)CastclassSealed(L_51, UnknownWrapper_t1_842_il2cpp_TypeInfo_var)));
		Object_t * L_52 = UnknownWrapper_get_WrappedObject_m1_7937(((UnknownWrapper_t1_842 *)CastclassSealed(L_51, UnknownWrapper_t1_842_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		IntPtr_t L_53 = Marshal_GetIUnknownForObject_m1_7756(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		__this->___pdispVal_18 = L_53;
		goto IL_0311;
	}

IL_0284:
	{
		Type_t * L_54 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_55 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(DispatchWrapper_t1_781_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_54) == ((Object_t*)(Type_t *)L_55))))
		{
			goto IL_02b7;
		}
	}
	{
		__this->___vt_0 = ((int32_t)9);
		Object_t * L_56 = ___obj;
		NullCheck(((DispatchWrapper_t1_781 *)CastclassSealed(L_56, DispatchWrapper_t1_781_il2cpp_TypeInfo_var)));
		Object_t * L_57 = DispatchWrapper_get_WrappedObject_m1_7634(((DispatchWrapper_t1_781 *)CastclassSealed(L_56, DispatchWrapper_t1_781_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		IntPtr_t L_58 = Marshal_GetIDispatchForObject_m1_7752(NULL /*static, unused*/, L_57, /*hidden argument*/NULL);
		__this->___pdispVal_18 = L_58;
		goto IL_0311;
	}

IL_02b7:
	try
	{ // begin try (depth: 1)
		{
			Object_t * L_59 = ___obj;
			IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
			IntPtr_t L_60 = Marshal_GetIDispatchForObject_m1_7752(NULL /*static, unused*/, L_59, /*hidden argument*/NULL);
			__this->___pdispVal_18 = L_60;
			__this->___vt_0 = ((int32_t)9);
			goto IL_0311;
		}

IL_02d0:
		{
			; // IL_02d0: leave IL_02db
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1_33 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Object_t_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_02d5;
		throw e;
	}

CATCH_02d5:
	{ // begin catch(System.Object)
		goto IL_02db;
	} // end catch (depth: 1)

IL_02db:
	try
	{ // begin try (depth: 1)
		__this->___vt_0 = ((int32_t)13);
		Object_t * L_61 = ___obj;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		IntPtr_t L_62 = Marshal_GetIUnknownForObject_m1_7756(NULL /*static, unused*/, L_61, /*hidden argument*/NULL);
		__this->___pdispVal_18 = L_62;
		goto IL_0311;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1_33 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1_33_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_02f4;
		throw e;
	}

CATCH_02f4:
	{ // begin catch(System.Exception)
		{
			V_1 = ((Exception_t1_33 *)__exception_local);
			Object_t * L_63 = ___obj;
			NullCheck(L_63);
			Type_t * L_64 = Object_GetType_m1_5(L_63, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_65 = String_Format_m1_548(NULL /*static, unused*/, _stringLiteral3639, L_64, /*hidden argument*/NULL);
			Exception_t1_33 * L_66 = V_1;
			NotImplementedException_t1_1582 * L_67 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
			NotImplementedException__ctor_m1_14414(L_67, L_65, L_66, /*hidden argument*/NULL);
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_67);
		}

IL_030c:
		{
			goto IL_0311;
		}
	} // end catch (depth: 1)

IL_0311:
	{
		return;
	}
}
// System.Object System.Variant::GetValue()
extern TypeInfo* SByte_t1_12_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern TypeInfo* Int16_t1_13_il2cpp_TypeInfo_var;
extern TypeInfo* UInt16_t1_14_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* UInt32_t1_8_il2cpp_TypeInfo_var;
extern TypeInfo* Int64_t1_7_il2cpp_TypeInfo_var;
extern TypeInfo* UInt64_t1_10_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t1_17_il2cpp_TypeInfo_var;
extern TypeInfo* Double_t1_18_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t1_20_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" Object_t * Variant_GetValue_m1_14720 (Variant_t1_1617 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SByte_t1_12_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(27);
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		Int16_t1_13_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(28);
		UInt16_t1_14_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		UInt32_t1_8_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		Int64_t1_7_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(21);
		UInt64_t1_10_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(24);
		Single_t1_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		Double_t1_18_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(48);
		Boolean_t1_20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(54);
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	int32_t V_1 = {0};
	{
		V_0 = NULL;
		int16_t L_0 = (__this->___vt_0);
		V_1 = L_0;
		int32_t L_1 = V_1;
		if (((int32_t)((int32_t)L_1-(int32_t)2)) == 0)
		{
			goto IL_0088;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)2)) == 1)
		{
			goto IL_00aa;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)2)) == 2)
		{
			goto IL_00ee;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)2)) == 3)
		{
			goto IL_00ff;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)2)) == 4)
		{
			goto IL_015e;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)2)) == 5)
		{
			goto IL_015e;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)2)) == 6)
		{
			goto IL_0127;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)2)) == 7)
		{
			goto IL_0138;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)2)) == 8)
		{
			goto IL_015e;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)2)) == 9)
		{
			goto IL_0110;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)2)) == 10)
		{
			goto IL_015e;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)2)) == 11)
		{
			goto IL_0138;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)2)) == 12)
		{
			goto IL_015e;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)2)) == 13)
		{
			goto IL_015e;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)2)) == 14)
		{
			goto IL_0066;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)2)) == 15)
		{
			goto IL_0077;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)2)) == 16)
		{
			goto IL_0099;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)2)) == 17)
		{
			goto IL_00bb;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)2)) == 18)
		{
			goto IL_00cc;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)2)) == 19)
		{
			goto IL_00dd;
		}
	}
	{
		goto IL_015e;
	}

IL_0066:
	{
		int8_t L_2 = (__this->___cVal_12);
		int8_t L_3 = L_2;
		Object_t * L_4 = Box(SByte_t1_12_il2cpp_TypeInfo_var, &L_3);
		V_0 = L_4;
		goto IL_015e;
	}

IL_0077:
	{
		uint8_t L_5 = (__this->___bVal_6);
		uint8_t L_6 = L_5;
		Object_t * L_7 = Box(Byte_t1_11_il2cpp_TypeInfo_var, &L_6);
		V_0 = L_7;
		goto IL_015e;
	}

IL_0088:
	{
		int16_t L_8 = (__this->___iVal_7);
		int16_t L_9 = L_8;
		Object_t * L_10 = Box(Int16_t1_13_il2cpp_TypeInfo_var, &L_9);
		V_0 = L_10;
		goto IL_015e;
	}

IL_0099:
	{
		uint16_t L_11 = (__this->___uiVal_13);
		uint16_t L_12 = L_11;
		Object_t * L_13 = Box(UInt16_t1_14_il2cpp_TypeInfo_var, &L_12);
		V_0 = L_13;
		goto IL_015e;
	}

IL_00aa:
	{
		int32_t L_14 = (__this->___lVal_5);
		int32_t L_15 = L_14;
		Object_t * L_16 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_15);
		V_0 = L_16;
		goto IL_015e;
	}

IL_00bb:
	{
		uint32_t L_17 = (__this->___ulVal_14);
		uint32_t L_18 = L_17;
		Object_t * L_19 = Box(UInt32_t1_8_il2cpp_TypeInfo_var, &L_18);
		V_0 = L_19;
		goto IL_015e;
	}

IL_00cc:
	{
		int64_t L_20 = (__this->___llVal_4);
		int64_t L_21 = L_20;
		Object_t * L_22 = Box(Int64_t1_7_il2cpp_TypeInfo_var, &L_21);
		V_0 = L_22;
		goto IL_015e;
	}

IL_00dd:
	{
		uint64_t L_23 = (__this->___ullVal_15);
		uint64_t L_24 = L_23;
		Object_t * L_25 = Box(UInt64_t1_10_il2cpp_TypeInfo_var, &L_24);
		V_0 = L_25;
		goto IL_015e;
	}

IL_00ee:
	{
		float L_26 = (__this->___fltVal_8);
		float L_27 = L_26;
		Object_t * L_28 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_27);
		V_0 = L_28;
		goto IL_015e;
	}

IL_00ff:
	{
		double L_29 = (__this->___dblVal_9);
		double L_30 = L_29;
		Object_t * L_31 = Box(Double_t1_18_il2cpp_TypeInfo_var, &L_30);
		V_0 = L_31;
		goto IL_015e;
	}

IL_0110:
	{
		int16_t L_32 = (__this->___boolVal_10);
		bool L_33 = ((((int32_t)((((int32_t)L_32) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		Object_t * L_34 = Box(Boolean_t1_20_il2cpp_TypeInfo_var, &L_33);
		V_0 = L_34;
		goto IL_015e;
	}

IL_0127:
	{
		IntPtr_t L_35 = (__this->___bstrVal_11);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		String_t* L_36 = Marshal_PtrToStringBSTR_m1_7794(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		V_0 = L_36;
		goto IL_015e;
	}

IL_0138:
	{
		IntPtr_t L_37 = (__this->___pdispVal_18);
		IntPtr_t L_38 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		bool L_39 = IntPtr_op_Inequality_m1_841(NULL /*static, unused*/, L_37, L_38, /*hidden argument*/NULL);
		if (!L_39)
		{
			goto IL_0159;
		}
	}
	{
		IntPtr_t L_40 = (__this->___pdispVal_18);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		Object_t * L_41 = Marshal_GetObjectForIUnknown_m1_7762(NULL /*static, unused*/, L_40, /*hidden argument*/NULL);
		V_0 = L_41;
	}

IL_0159:
	{
		goto IL_015e;
	}

IL_015e:
	{
		Object_t * L_42 = V_0;
		return L_42;
	}
}
// System.Void System.Variant::Clear()
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" void Variant_Clear_m1_14721 (Variant_t1_1617 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		s_Il2CppMethodIntialized = true;
	}
	{
		int16_t L_0 = (__this->___vt_0);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_001c;
		}
	}
	{
		IntPtr_t L_1 = (__this->___bstrVal_11);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		Marshal_FreeBSTR_m1_7724(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		goto IL_0057;
	}

IL_001c:
	{
		int16_t L_2 = (__this->___vt_0);
		if ((((int32_t)L_2) == ((int32_t)((int32_t)9))))
		{
			goto IL_0036;
		}
	}
	{
		int16_t L_3 = (__this->___vt_0);
		if ((!(((uint32_t)L_3) == ((uint32_t)((int32_t)13)))))
		{
			goto IL_0057;
		}
	}

IL_0036:
	{
		IntPtr_t L_4 = (__this->___pdispVal_18);
		IntPtr_t L_5 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		bool L_6 = IntPtr_op_Inequality_m1_841(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0057;
		}
	}
	{
		IntPtr_t L_7 = (__this->___pdispVal_18);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		Marshal_Release_m1_7817(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
	}

IL_0057:
	{
		return;
	}
}
// System.Void System.Version::.ctor()
extern "C" void Version__ctor_m1_14722 (Version_t1_578 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		Version_CheckedSet_m1_14727(__this, 2, 0, 0, (-1), (-1), /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Version::.ctor(System.String)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t1_16_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1921;
extern Il2CppCodeGenString* _stringLiteral3640;
extern "C" void Version__ctor_m1_14723 (Version_t1_578 * __this, String_t* ___version, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		CharU5BU5D_t1_16_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(15);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		_stringLiteral1921 = il2cpp_codegen_string_literal_from_index(1921);
		_stringLiteral3640 = il2cpp_codegen_string_literal_from_index(3640);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	StringU5BU5D_t1_238* V_1 = {0};
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		V_2 = (-1);
		V_3 = (-1);
		V_4 = (-1);
		V_5 = (-1);
		String_t* L_0 = ___version;
		if (L_0)
		{
			goto IL_0021;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, _stringLiteral1921, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0021:
	{
		String_t* L_2 = ___version;
		CharU5BU5D_t1_16* L_3 = ((CharU5BU5D_t1_16*)SZArrayNew(CharU5BU5D_t1_16_il2cpp_TypeInfo_var, 1));
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_3, 0, sizeof(uint16_t))) = (uint16_t)((int32_t)46);
		NullCheck(L_2);
		StringU5BU5D_t1_238* L_4 = String_Split_m1_448(L_2, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		StringU5BU5D_t1_238* L_5 = V_1;
		NullCheck(L_5);
		V_0 = (((int32_t)((int32_t)(((Array_t *)L_5)->max_length))));
		int32_t L_6 = V_0;
		if ((((int32_t)L_6) < ((int32_t)2)))
		{
			goto IL_0045;
		}
	}
	{
		int32_t L_7 = V_0;
		if ((((int32_t)L_7) <= ((int32_t)4)))
		{
			goto IL_0055;
		}
	}

IL_0045:
	{
		String_t* L_8 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral3640, /*hidden argument*/NULL);
		ArgumentException_t1_1425 * L_9 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13260(L_9, L_8, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_9);
	}

IL_0055:
	{
		int32_t L_10 = V_0;
		if ((((int32_t)L_10) <= ((int32_t)0)))
		{
			goto IL_0065;
		}
	}
	{
		StringU5BU5D_t1_238* L_11 = V_1;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 0);
		int32_t L_12 = 0;
		int32_t L_13 = Int32_Parse_m1_97(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_11, L_12, sizeof(String_t*))), /*hidden argument*/NULL);
		V_2 = L_13;
	}

IL_0065:
	{
		int32_t L_14 = V_0;
		if ((((int32_t)L_14) <= ((int32_t)1)))
		{
			goto IL_0075;
		}
	}
	{
		StringU5BU5D_t1_238* L_15 = V_1;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 1);
		int32_t L_16 = 1;
		int32_t L_17 = Int32_Parse_m1_97(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_15, L_16, sizeof(String_t*))), /*hidden argument*/NULL);
		V_3 = L_17;
	}

IL_0075:
	{
		int32_t L_18 = V_0;
		if ((((int32_t)L_18) <= ((int32_t)2)))
		{
			goto IL_0086;
		}
	}
	{
		StringU5BU5D_t1_238* L_19 = V_1;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 2);
		int32_t L_20 = 2;
		int32_t L_21 = Int32_Parse_m1_97(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_19, L_20, sizeof(String_t*))), /*hidden argument*/NULL);
		V_4 = L_21;
	}

IL_0086:
	{
		int32_t L_22 = V_0;
		if ((((int32_t)L_22) <= ((int32_t)3)))
		{
			goto IL_0097;
		}
	}
	{
		StringU5BU5D_t1_238* L_23 = V_1;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 3);
		int32_t L_24 = 3;
		int32_t L_25 = Int32_Parse_m1_97(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_23, L_24, sizeof(String_t*))), /*hidden argument*/NULL);
		V_5 = L_25;
	}

IL_0097:
	{
		int32_t L_26 = V_0;
		int32_t L_27 = V_2;
		int32_t L_28 = V_3;
		int32_t L_29 = V_4;
		int32_t L_30 = V_5;
		Version_CheckedSet_m1_14727(__this, L_26, L_27, L_28, L_29, L_30, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Version::.ctor(System.Int32,System.Int32)
extern "C" void Version__ctor_m1_14724 (Version_t1_578 * __this, int32_t ___major, int32_t ___minor, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___major;
		int32_t L_1 = ___minor;
		Version_CheckedSet_m1_14727(__this, 2, L_0, L_1, 0, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Version::.ctor(System.Int32,System.Int32,System.Int32)
extern "C" void Version__ctor_m1_14725 (Version_t1_578 * __this, int32_t ___major, int32_t ___minor, int32_t ___build, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___major;
		int32_t L_1 = ___minor;
		int32_t L_2 = ___build;
		Version_CheckedSet_m1_14727(__this, 3, L_0, L_1, L_2, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Version::.ctor(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" void Version__ctor_m1_14726 (Version_t1_578 * __this, int32_t ___major, int32_t ___minor, int32_t ___build, int32_t ___revision, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___major;
		int32_t L_1 = ___minor;
		int32_t L_2 = ___build;
		int32_t L_3 = ___revision;
		Version_CheckedSet_m1_14727(__this, 4, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Version::CheckedSet(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3641;
extern Il2CppCodeGenString* _stringLiteral3642;
extern Il2CppCodeGenString* _stringLiteral3643;
extern Il2CppCodeGenString* _stringLiteral3644;
extern "C" void Version_CheckedSet_m1_14727 (Version_t1_578 * __this, int32_t ___defined, int32_t ___major, int32_t ___minor, int32_t ___build, int32_t ___revision, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		_stringLiteral3641 = il2cpp_codegen_string_literal_from_index(3641);
		_stringLiteral3642 = il2cpp_codegen_string_literal_from_index(3642);
		_stringLiteral3643 = il2cpp_codegen_string_literal_from_index(3643);
		_stringLiteral3644 = il2cpp_codegen_string_literal_from_index(3644);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___major;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0012;
		}
	}
	{
		ArgumentOutOfRangeException_t1_1501 * L_1 = (ArgumentOutOfRangeException_t1_1501 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1_13274(L_1, _stringLiteral3641, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0012:
	{
		int32_t L_2 = ___major;
		__this->____Major_1 = L_2;
		int32_t L_3 = ___minor;
		if ((((int32_t)L_3) >= ((int32_t)0)))
		{
			goto IL_002b;
		}
	}
	{
		ArgumentOutOfRangeException_t1_1501 * L_4 = (ArgumentOutOfRangeException_t1_1501 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1_13274(L_4, _stringLiteral3642, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
	}

IL_002b:
	{
		int32_t L_5 = ___minor;
		__this->____Minor_2 = L_5;
		int32_t L_6 = ___defined;
		if ((!(((uint32_t)L_6) == ((uint32_t)2))))
		{
			goto IL_0048;
		}
	}
	{
		__this->____Build_3 = (-1);
		__this->____Revision_4 = (-1);
		return;
	}

IL_0048:
	{
		int32_t L_7 = ___build;
		if ((((int32_t)L_7) >= ((int32_t)0)))
		{
			goto IL_005b;
		}
	}
	{
		ArgumentOutOfRangeException_t1_1501 * L_8 = (ArgumentOutOfRangeException_t1_1501 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1_13274(L_8, _stringLiteral3643, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_8);
	}

IL_005b:
	{
		int32_t L_9 = ___build;
		__this->____Build_3 = L_9;
		int32_t L_10 = ___defined;
		if ((!(((uint32_t)L_10) == ((uint32_t)3))))
		{
			goto IL_0072;
		}
	}
	{
		__this->____Revision_4 = (-1);
		return;
	}

IL_0072:
	{
		int32_t L_11 = ___revision;
		if ((((int32_t)L_11) >= ((int32_t)0)))
		{
			goto IL_0085;
		}
	}
	{
		ArgumentOutOfRangeException_t1_1501 * L_12 = (ArgumentOutOfRangeException_t1_1501 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1_13274(L_12, _stringLiteral3644, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_12);
	}

IL_0085:
	{
		int32_t L_13 = ___revision;
		__this->____Revision_4 = L_13;
		return;
	}
}
// System.Int32 System.Version::get_Build()
extern "C" int32_t Version_get_Build_m1_14728 (Version_t1_578 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->____Build_3);
		return L_0;
	}
}
// System.Int32 System.Version::get_Major()
extern "C" int32_t Version_get_Major_m1_14729 (Version_t1_578 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->____Major_1);
		return L_0;
	}
}
// System.Int32 System.Version::get_Minor()
extern "C" int32_t Version_get_Minor_m1_14730 (Version_t1_578 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->____Minor_2);
		return L_0;
	}
}
// System.Int32 System.Version::get_Revision()
extern "C" int32_t Version_get_Revision_m1_14731 (Version_t1_578 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->____Revision_4);
		return L_0;
	}
}
// System.Int16 System.Version::get_MajorRevision()
extern "C" int16_t Version_get_MajorRevision_m1_14732 (Version_t1_578 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->____Revision_4);
		return (((int16_t)((int16_t)((int32_t)((int32_t)L_0>>(int32_t)((int32_t)16))))));
	}
}
// System.Int16 System.Version::get_MinorRevision()
extern "C" int16_t Version_get_MinorRevision_m1_14733 (Version_t1_578 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->____Revision_4);
		return (((int16_t)((int16_t)L_0)));
	}
}
// System.Object System.Version::Clone()
extern TypeInfo* Version_t1_578_il2cpp_TypeInfo_var;
extern "C" Object_t * Version_Clone_m1_14734 (Version_t1_578 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Version_t1_578_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(510);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (__this->____Build_3);
		if ((!(((uint32_t)L_0) == ((uint32_t)(-1)))))
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = (__this->____Major_1);
		int32_t L_2 = (__this->____Minor_2);
		Version_t1_578 * L_3 = (Version_t1_578 *)il2cpp_codegen_object_new (Version_t1_578_il2cpp_TypeInfo_var);
		Version__ctor_m1_14724(L_3, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_001e:
	{
		int32_t L_4 = (__this->____Revision_4);
		if ((!(((uint32_t)L_4) == ((uint32_t)(-1)))))
		{
			goto IL_0042;
		}
	}
	{
		int32_t L_5 = (__this->____Major_1);
		int32_t L_6 = (__this->____Minor_2);
		int32_t L_7 = (__this->____Build_3);
		Version_t1_578 * L_8 = (Version_t1_578 *)il2cpp_codegen_object_new (Version_t1_578_il2cpp_TypeInfo_var);
		Version__ctor_m1_14725(L_8, L_5, L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}

IL_0042:
	{
		int32_t L_9 = (__this->____Major_1);
		int32_t L_10 = (__this->____Minor_2);
		int32_t L_11 = (__this->____Build_3);
		int32_t L_12 = (__this->____Revision_4);
		Version_t1_578 * L_13 = (Version_t1_578 *)il2cpp_codegen_object_new (Version_t1_578_il2cpp_TypeInfo_var);
		Version__ctor_m1_14726(L_13, L_9, L_10, L_11, L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
// System.Int32 System.Version::CompareTo(System.Object)
extern TypeInfo* Version_t1_578_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3645;
extern "C" int32_t Version_CompareTo_m1_14735 (Version_t1_578 * __this, Object_t * ___version, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Version_t1_578_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(510);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		_stringLiteral3645 = il2cpp_codegen_string_literal_from_index(3645);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___version;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return 1;
	}

IL_0008:
	{
		Object_t * L_1 = ___version;
		if (((Version_t1_578 *)IsInstSealed(L_1, Version_t1_578_il2cpp_TypeInfo_var)))
		{
			goto IL_0023;
		}
	}
	{
		String_t* L_2 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral3645, /*hidden argument*/NULL);
		ArgumentException_t1_1425 * L_3 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13260(L_3, L_2, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0023:
	{
		Object_t * L_4 = ___version;
		int32_t L_5 = Version_CompareTo_m1_14737(__this, ((Version_t1_578 *)CastclassSealed(L_4, Version_t1_578_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Boolean System.Version::Equals(System.Object)
extern TypeInfo* Version_t1_578_il2cpp_TypeInfo_var;
extern "C" bool Version_Equals_m1_14736 (Version_t1_578 * __this, Object_t * ___obj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Version_t1_578_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(510);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___obj;
		bool L_1 = Version_Equals_m1_14738(__this, ((Version_t1_578 *)IsInstSealed(L_0, Version_t1_578_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 System.Version::CompareTo(System.Version)
extern "C" int32_t Version_CompareTo_m1_14737 (Version_t1_578 * __this, Version_t1_578 * ___value, const MethodInfo* method)
{
	{
		Version_t1_578 * L_0 = ___value;
		bool L_1 = Version_op_Equality_m1_14743(NULL /*static, unused*/, L_0, (Version_t1_578 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000e;
		}
	}
	{
		return 1;
	}

IL_000e:
	{
		int32_t L_2 = (__this->____Major_1);
		Version_t1_578 * L_3 = ___value;
		NullCheck(L_3);
		int32_t L_4 = (L_3->____Major_1);
		if ((((int32_t)L_2) <= ((int32_t)L_4)))
		{
			goto IL_0021;
		}
	}
	{
		return 1;
	}

IL_0021:
	{
		int32_t L_5 = (__this->____Major_1);
		Version_t1_578 * L_6 = ___value;
		NullCheck(L_6);
		int32_t L_7 = (L_6->____Major_1);
		if ((((int32_t)L_5) >= ((int32_t)L_7)))
		{
			goto IL_0034;
		}
	}
	{
		return (-1);
	}

IL_0034:
	{
		int32_t L_8 = (__this->____Minor_2);
		Version_t1_578 * L_9 = ___value;
		NullCheck(L_9);
		int32_t L_10 = (L_9->____Minor_2);
		if ((((int32_t)L_8) <= ((int32_t)L_10)))
		{
			goto IL_0047;
		}
	}
	{
		return 1;
	}

IL_0047:
	{
		int32_t L_11 = (__this->____Minor_2);
		Version_t1_578 * L_12 = ___value;
		NullCheck(L_12);
		int32_t L_13 = (L_12->____Minor_2);
		if ((((int32_t)L_11) >= ((int32_t)L_13)))
		{
			goto IL_005a;
		}
	}
	{
		return (-1);
	}

IL_005a:
	{
		int32_t L_14 = (__this->____Build_3);
		Version_t1_578 * L_15 = ___value;
		NullCheck(L_15);
		int32_t L_16 = (L_15->____Build_3);
		if ((((int32_t)L_14) <= ((int32_t)L_16)))
		{
			goto IL_006d;
		}
	}
	{
		return 1;
	}

IL_006d:
	{
		int32_t L_17 = (__this->____Build_3);
		Version_t1_578 * L_18 = ___value;
		NullCheck(L_18);
		int32_t L_19 = (L_18->____Build_3);
		if ((((int32_t)L_17) >= ((int32_t)L_19)))
		{
			goto IL_0080;
		}
	}
	{
		return (-1);
	}

IL_0080:
	{
		int32_t L_20 = (__this->____Revision_4);
		Version_t1_578 * L_21 = ___value;
		NullCheck(L_21);
		int32_t L_22 = (L_21->____Revision_4);
		if ((((int32_t)L_20) <= ((int32_t)L_22)))
		{
			goto IL_0093;
		}
	}
	{
		return 1;
	}

IL_0093:
	{
		int32_t L_23 = (__this->____Revision_4);
		Version_t1_578 * L_24 = ___value;
		NullCheck(L_24);
		int32_t L_25 = (L_24->____Revision_4);
		if ((((int32_t)L_23) >= ((int32_t)L_25)))
		{
			goto IL_00a6;
		}
	}
	{
		return (-1);
	}

IL_00a6:
	{
		return 0;
	}
}
// System.Boolean System.Version::Equals(System.Version)
extern "C" bool Version_Equals_m1_14738 (Version_t1_578 * __this, Version_t1_578 * ___obj, const MethodInfo* method)
{
	int32_t G_B6_0 = 0;
	{
		Version_t1_578 * L_0 = ___obj;
		bool L_1 = Version_op_Inequality_m1_14744(NULL /*static, unused*/, L_0, (Version_t1_578 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_004f;
		}
	}
	{
		Version_t1_578 * L_2 = ___obj;
		NullCheck(L_2);
		int32_t L_3 = (L_2->____Major_1);
		int32_t L_4 = (__this->____Major_1);
		if ((!(((uint32_t)L_3) == ((uint32_t)L_4))))
		{
			goto IL_004f;
		}
	}
	{
		Version_t1_578 * L_5 = ___obj;
		NullCheck(L_5);
		int32_t L_6 = (L_5->____Minor_2);
		int32_t L_7 = (__this->____Minor_2);
		if ((!(((uint32_t)L_6) == ((uint32_t)L_7))))
		{
			goto IL_004f;
		}
	}
	{
		Version_t1_578 * L_8 = ___obj;
		NullCheck(L_8);
		int32_t L_9 = (L_8->____Build_3);
		int32_t L_10 = (__this->____Build_3);
		if ((!(((uint32_t)L_9) == ((uint32_t)L_10))))
		{
			goto IL_004f;
		}
	}
	{
		Version_t1_578 * L_11 = ___obj;
		NullCheck(L_11);
		int32_t L_12 = (L_11->____Revision_4);
		int32_t L_13 = (__this->____Revision_4);
		G_B6_0 = ((((int32_t)L_12) == ((int32_t)L_13))? 1 : 0);
		goto IL_0050;
	}

IL_004f:
	{
		G_B6_0 = 0;
	}

IL_0050:
	{
		return G_B6_0;
	}
}
// System.Int32 System.Version::GetHashCode()
extern "C" int32_t Version_GetHashCode_m1_14739 (Version_t1_578 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->____Revision_4);
		int32_t L_1 = (__this->____Build_3);
		int32_t L_2 = (__this->____Minor_2);
		int32_t L_3 = (__this->____Major_1);
		return ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_0<<(int32_t)((int32_t)24)))|(int32_t)((int32_t)((int32_t)L_1<<(int32_t)((int32_t)16)))))|(int32_t)((int32_t)((int32_t)L_2<<(int32_t)8))))|(int32_t)L_3));
	}
}
// System.String System.Version::ToString()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral56;
extern "C" String_t* Version_ToString_m1_14740 (Version_t1_578 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral56 = il2cpp_codegen_string_literal_from_index(56);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	{
		int32_t* L_0 = &(__this->____Major_1);
		String_t* L_1 = Int32_ToString_m1_101(L_0, /*hidden argument*/NULL);
		int32_t* L_2 = &(__this->____Minor_2);
		String_t* L_3 = Int32_ToString_m1_101(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m1_560(NULL /*static, unused*/, L_1, _stringLiteral56, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = (__this->____Build_3);
		if ((((int32_t)L_5) == ((int32_t)(-1))))
		{
			goto IL_0044;
		}
	}
	{
		String_t* L_6 = V_0;
		int32_t* L_7 = &(__this->____Build_3);
		String_t* L_8 = Int32_ToString_m1_101(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m1_560(NULL /*static, unused*/, L_6, _stringLiteral56, L_8, /*hidden argument*/NULL);
		V_0 = L_9;
	}

IL_0044:
	{
		int32_t L_10 = (__this->____Revision_4);
		if ((((int32_t)L_10) == ((int32_t)(-1))))
		{
			goto IL_0067;
		}
	}
	{
		String_t* L_11 = V_0;
		int32_t* L_12 = &(__this->____Revision_4);
		String_t* L_13 = Int32_ToString_m1_101(L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m1_560(NULL /*static, unused*/, L_11, _stringLiteral56, L_13, /*hidden argument*/NULL);
		V_0 = L_14;
	}

IL_0067:
	{
		String_t* L_15 = V_0;
		return L_15;
	}
}
// System.String System.Version::ToString(System.Int32)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern TypeInfo* StringU5BU5D_t1_238_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral56;
extern Il2CppCodeGenString* _stringLiteral3646;
extern Il2CppCodeGenString* _stringLiteral3647;
extern "C" String_t* Version_ToString_m1_14741 (Version_t1_578 * __this, int32_t ___fieldCount, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		StringU5BU5D_t1_238_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(35);
		_stringLiteral56 = il2cpp_codegen_string_literal_from_index(56);
		_stringLiteral3646 = il2cpp_codegen_string_literal_from_index(3646);
		_stringLiteral3647 = il2cpp_codegen_string_literal_from_index(3647);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___fieldCount;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		return L_1;
	}

IL_000c:
	{
		int32_t L_2 = ___fieldCount;
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			goto IL_001f;
		}
	}
	{
		int32_t* L_3 = &(__this->____Major_1);
		String_t* L_4 = Int32_ToString_m1_101(L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_001f:
	{
		int32_t L_5 = ___fieldCount;
		if ((!(((uint32_t)L_5) == ((uint32_t)2))))
		{
			goto IL_0047;
		}
	}
	{
		int32_t* L_6 = &(__this->____Major_1);
		String_t* L_7 = Int32_ToString_m1_101(L_6, /*hidden argument*/NULL);
		int32_t* L_8 = &(__this->____Minor_2);
		String_t* L_9 = Int32_ToString_m1_101(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m1_560(NULL /*static, unused*/, L_7, _stringLiteral56, L_9, /*hidden argument*/NULL);
		return L_10;
	}

IL_0047:
	{
		int32_t L_11 = ___fieldCount;
		if ((!(((uint32_t)L_11) == ((uint32_t)3))))
		{
			goto IL_00b0;
		}
	}
	{
		int32_t L_12 = (__this->____Build_3);
		if ((!(((uint32_t)L_12) == ((uint32_t)(-1)))))
		{
			goto IL_006a;
		}
	}
	{
		String_t* L_13 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral3646, /*hidden argument*/NULL);
		ArgumentException_t1_1425 * L_14 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13260(L_14, L_13, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_14);
	}

IL_006a:
	{
		StringU5BU5D_t1_238* L_15 = ((StringU5BU5D_t1_238*)SZArrayNew(StringU5BU5D_t1_238_il2cpp_TypeInfo_var, 5));
		int32_t* L_16 = &(__this->____Major_1);
		String_t* L_17 = Int32_ToString_m1_101(L_16, /*hidden argument*/NULL);
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 0);
		ArrayElementTypeCheck (L_15, L_17);
		*((String_t**)(String_t**)SZArrayLdElema(L_15, 0, sizeof(String_t*))) = (String_t*)L_17;
		StringU5BU5D_t1_238* L_18 = L_15;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 1);
		ArrayElementTypeCheck (L_18, _stringLiteral56);
		*((String_t**)(String_t**)SZArrayLdElema(L_18, 1, sizeof(String_t*))) = (String_t*)_stringLiteral56;
		StringU5BU5D_t1_238* L_19 = L_18;
		int32_t* L_20 = &(__this->____Minor_2);
		String_t* L_21 = Int32_ToString_m1_101(L_20, /*hidden argument*/NULL);
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 2);
		ArrayElementTypeCheck (L_19, L_21);
		*((String_t**)(String_t**)SZArrayLdElema(L_19, 2, sizeof(String_t*))) = (String_t*)L_21;
		StringU5BU5D_t1_238* L_22 = L_19;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, 3);
		ArrayElementTypeCheck (L_22, _stringLiteral56);
		*((String_t**)(String_t**)SZArrayLdElema(L_22, 3, sizeof(String_t*))) = (String_t*)_stringLiteral56;
		StringU5BU5D_t1_238* L_23 = L_22;
		int32_t* L_24 = &(__this->____Build_3);
		String_t* L_25 = Int32_ToString_m1_101(L_24, /*hidden argument*/NULL);
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 4);
		ArrayElementTypeCheck (L_23, L_25);
		*((String_t**)(String_t**)SZArrayLdElema(L_23, 4, sizeof(String_t*))) = (String_t*)L_25;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_26 = String_Concat_m1_563(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		return L_26;
	}

IL_00b0:
	{
		int32_t L_27 = ___fieldCount;
		if ((!(((uint32_t)L_27) == ((uint32_t)4))))
		{
			goto IL_013b;
		}
	}
	{
		int32_t L_28 = (__this->____Build_3);
		if ((((int32_t)L_28) == ((int32_t)(-1))))
		{
			goto IL_00cf;
		}
	}
	{
		int32_t L_29 = (__this->____Revision_4);
		if ((!(((uint32_t)L_29) == ((uint32_t)(-1)))))
		{
			goto IL_00df;
		}
	}

IL_00cf:
	{
		String_t* L_30 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral3646, /*hidden argument*/NULL);
		ArgumentException_t1_1425 * L_31 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13260(L_31, L_30, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_31);
	}

IL_00df:
	{
		StringU5BU5D_t1_238* L_32 = ((StringU5BU5D_t1_238*)SZArrayNew(StringU5BU5D_t1_238_il2cpp_TypeInfo_var, 7));
		int32_t* L_33 = &(__this->____Major_1);
		String_t* L_34 = Int32_ToString_m1_101(L_33, /*hidden argument*/NULL);
		NullCheck(L_32);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_32, 0);
		ArrayElementTypeCheck (L_32, L_34);
		*((String_t**)(String_t**)SZArrayLdElema(L_32, 0, sizeof(String_t*))) = (String_t*)L_34;
		StringU5BU5D_t1_238* L_35 = L_32;
		NullCheck(L_35);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_35, 1);
		ArrayElementTypeCheck (L_35, _stringLiteral56);
		*((String_t**)(String_t**)SZArrayLdElema(L_35, 1, sizeof(String_t*))) = (String_t*)_stringLiteral56;
		StringU5BU5D_t1_238* L_36 = L_35;
		int32_t* L_37 = &(__this->____Minor_2);
		String_t* L_38 = Int32_ToString_m1_101(L_37, /*hidden argument*/NULL);
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, 2);
		ArrayElementTypeCheck (L_36, L_38);
		*((String_t**)(String_t**)SZArrayLdElema(L_36, 2, sizeof(String_t*))) = (String_t*)L_38;
		StringU5BU5D_t1_238* L_39 = L_36;
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, 3);
		ArrayElementTypeCheck (L_39, _stringLiteral56);
		*((String_t**)(String_t**)SZArrayLdElema(L_39, 3, sizeof(String_t*))) = (String_t*)_stringLiteral56;
		StringU5BU5D_t1_238* L_40 = L_39;
		int32_t* L_41 = &(__this->____Build_3);
		String_t* L_42 = Int32_ToString_m1_101(L_41, /*hidden argument*/NULL);
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, 4);
		ArrayElementTypeCheck (L_40, L_42);
		*((String_t**)(String_t**)SZArrayLdElema(L_40, 4, sizeof(String_t*))) = (String_t*)L_42;
		StringU5BU5D_t1_238* L_43 = L_40;
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, 5);
		ArrayElementTypeCheck (L_43, _stringLiteral56);
		*((String_t**)(String_t**)SZArrayLdElema(L_43, 5, sizeof(String_t*))) = (String_t*)_stringLiteral56;
		StringU5BU5D_t1_238* L_44 = L_43;
		int32_t* L_45 = &(__this->____Revision_4);
		String_t* L_46 = Int32_ToString_m1_101(L_45, /*hidden argument*/NULL);
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, 6);
		ArrayElementTypeCheck (L_44, L_46);
		*((String_t**)(String_t**)SZArrayLdElema(L_44, 6, sizeof(String_t*))) = (String_t*)L_46;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_47 = String_Concat_m1_563(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
		return L_47;
	}

IL_013b:
	{
		String_t* L_48 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral3647, /*hidden argument*/NULL);
		String_t* L_49 = Int32_ToString_m1_101((&___fieldCount), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_50 = String_Concat_m1_559(NULL /*static, unused*/, L_48, L_49, /*hidden argument*/NULL);
		ArgumentException_t1_1425 * L_51 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13260(L_51, L_50, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_51);
	}
}
// System.Version System.Version::CreateFromString(System.String)
extern TypeInfo* Version_t1_578_il2cpp_TypeInfo_var;
extern TypeInfo* Char_t1_15_il2cpp_TypeInfo_var;
extern "C" Version_t1_578 * Version_CreateFromString_m1_14742 (Object_t * __this /* static, unused */, String_t* ___info, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Version_t1_578_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(510);
		Char_t1_15_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(12);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	uint16_t V_7 = 0x0;
	int32_t V_8 = 0;
	{
		V_0 = 0;
		V_1 = 0;
		V_2 = 0;
		V_3 = 0;
		V_4 = 1;
		V_5 = (-1);
		String_t* L_0 = ___info;
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		Version_t1_578 * L_1 = (Version_t1_578 *)il2cpp_codegen_object_new (Version_t1_578_il2cpp_TypeInfo_var);
		Version__ctor_m1_14726(L_1, 0, 0, 0, 0, /*hidden argument*/NULL);
		return L_1;
	}

IL_001e:
	{
		V_6 = 0;
		goto IL_00c8;
	}

IL_0026:
	{
		String_t* L_2 = ___info;
		int32_t L_3 = V_6;
		NullCheck(L_2);
		uint16_t L_4 = String_get_Chars_m1_442(L_2, L_3, /*hidden argument*/NULL);
		V_7 = L_4;
		uint16_t L_5 = V_7;
		IL2CPP_RUNTIME_CLASS_INIT(Char_t1_15_il2cpp_TypeInfo_var);
		bool L_6 = Char_IsDigit_m1_374(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0062;
		}
	}
	{
		int32_t L_7 = V_5;
		if ((((int32_t)L_7) >= ((int32_t)0)))
		{
			goto IL_0050;
		}
	}
	{
		uint16_t L_8 = V_7;
		V_5 = ((int32_t)((int32_t)L_8-(int32_t)((int32_t)48)));
		goto IL_005d;
	}

IL_0050:
	{
		int32_t L_9 = V_5;
		uint16_t L_10 = V_7;
		V_5 = ((int32_t)((int32_t)((int32_t)((int32_t)L_9*(int32_t)((int32_t)10)))+(int32_t)((int32_t)((int32_t)L_10-(int32_t)((int32_t)48)))));
	}

IL_005d:
	{
		goto IL_00b5;
	}

IL_0062:
	{
		int32_t L_11 = V_5;
		if ((((int32_t)L_11) < ((int32_t)0)))
		{
			goto IL_00b5;
		}
	}
	{
		int32_t L_12 = V_4;
		V_8 = L_12;
		int32_t L_13 = V_8;
		if (((int32_t)((int32_t)L_13-(int32_t)1)) == 0)
		{
			goto IL_008c;
		}
		if (((int32_t)((int32_t)L_13-(int32_t)1)) == 1)
		{
			goto IL_0094;
		}
		if (((int32_t)((int32_t)L_13-(int32_t)1)) == 2)
		{
			goto IL_009c;
		}
		if (((int32_t)((int32_t)L_13-(int32_t)1)) == 3)
		{
			goto IL_00a4;
		}
	}
	{
		goto IL_00ac;
	}

IL_008c:
	{
		int32_t L_14 = V_5;
		V_0 = L_14;
		goto IL_00ac;
	}

IL_0094:
	{
		int32_t L_15 = V_5;
		V_1 = L_15;
		goto IL_00ac;
	}

IL_009c:
	{
		int32_t L_16 = V_5;
		V_2 = L_16;
		goto IL_00ac;
	}

IL_00a4:
	{
		int32_t L_17 = V_5;
		V_3 = L_17;
		goto IL_00ac;
	}

IL_00ac:
	{
		V_5 = (-1);
		int32_t L_18 = V_4;
		V_4 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_00b5:
	{
		int32_t L_19 = V_4;
		if ((!(((uint32_t)L_19) == ((uint32_t)5))))
		{
			goto IL_00c2;
		}
	}
	{
		goto IL_00d5;
	}

IL_00c2:
	{
		int32_t L_20 = V_6;
		V_6 = ((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_00c8:
	{
		int32_t L_21 = V_6;
		String_t* L_22 = ___info;
		NullCheck(L_22);
		int32_t L_23 = String_get_Length_m1_571(L_22, /*hidden argument*/NULL);
		if ((((int32_t)L_21) < ((int32_t)L_23)))
		{
			goto IL_0026;
		}
	}

IL_00d5:
	{
		int32_t L_24 = V_5;
		if ((((int32_t)L_24) < ((int32_t)0)))
		{
			goto IL_011f;
		}
	}
	{
		int32_t L_25 = V_4;
		V_8 = L_25;
		int32_t L_26 = V_8;
		if (((int32_t)((int32_t)L_26-(int32_t)1)) == 0)
		{
			goto IL_00ff;
		}
		if (((int32_t)((int32_t)L_26-(int32_t)1)) == 1)
		{
			goto IL_0107;
		}
		if (((int32_t)((int32_t)L_26-(int32_t)1)) == 2)
		{
			goto IL_010f;
		}
		if (((int32_t)((int32_t)L_26-(int32_t)1)) == 3)
		{
			goto IL_0117;
		}
	}
	{
		goto IL_011f;
	}

IL_00ff:
	{
		int32_t L_27 = V_5;
		V_0 = L_27;
		goto IL_011f;
	}

IL_0107:
	{
		int32_t L_28 = V_5;
		V_1 = L_28;
		goto IL_011f;
	}

IL_010f:
	{
		int32_t L_29 = V_5;
		V_2 = L_29;
		goto IL_011f;
	}

IL_0117:
	{
		int32_t L_30 = V_5;
		V_3 = L_30;
		goto IL_011f;
	}

IL_011f:
	{
		int32_t L_31 = V_0;
		int32_t L_32 = V_1;
		int32_t L_33 = V_2;
		int32_t L_34 = V_3;
		Version_t1_578 * L_35 = (Version_t1_578 *)il2cpp_codegen_object_new (Version_t1_578_il2cpp_TypeInfo_var);
		Version__ctor_m1_14726(L_35, L_31, L_32, L_33, L_34, /*hidden argument*/NULL);
		return L_35;
	}
}
// System.Boolean System.Version::op_Equality(System.Version,System.Version)
extern "C" bool Version_op_Equality_m1_14743 (Object_t * __this /* static, unused */, Version_t1_578 * ___v1, Version_t1_578 * ___v2, const MethodInfo* method)
{
	{
		Version_t1_578 * L_0 = ___v1;
		Version_t1_578 * L_1 = ___v2;
		bool L_2 = Object_Equals_m1_2(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean System.Version::op_Inequality(System.Version,System.Version)
extern "C" bool Version_op_Inequality_m1_14744 (Object_t * __this /* static, unused */, Version_t1_578 * ___v1, Version_t1_578 * ___v2, const MethodInfo* method)
{
	{
		Version_t1_578 * L_0 = ___v1;
		Version_t1_578 * L_1 = ___v2;
		bool L_2 = Object_Equals_m1_2(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return ((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean System.Version::op_GreaterThan(System.Version,System.Version)
extern "C" bool Version_op_GreaterThan_m1_14745 (Object_t * __this /* static, unused */, Version_t1_578 * ___v1, Version_t1_578 * ___v2, const MethodInfo* method)
{
	{
		Version_t1_578 * L_0 = ___v1;
		Version_t1_578 * L_1 = ___v2;
		NullCheck(L_0);
		int32_t L_2 = Version_CompareTo_m1_14737(L_0, L_1, /*hidden argument*/NULL);
		return ((((int32_t)L_2) > ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean System.Version::op_GreaterThanOrEqual(System.Version,System.Version)
extern "C" bool Version_op_GreaterThanOrEqual_m1_14746 (Object_t * __this /* static, unused */, Version_t1_578 * ___v1, Version_t1_578 * ___v2, const MethodInfo* method)
{
	{
		Version_t1_578 * L_0 = ___v1;
		Version_t1_578 * L_1 = ___v2;
		NullCheck(L_0);
		int32_t L_2 = Version_CompareTo_m1_14737(L_0, L_1, /*hidden argument*/NULL);
		return ((((int32_t)((((int32_t)L_2) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean System.Version::op_LessThan(System.Version,System.Version)
extern "C" bool Version_op_LessThan_m1_14747 (Object_t * __this /* static, unused */, Version_t1_578 * ___v1, Version_t1_578 * ___v2, const MethodInfo* method)
{
	{
		Version_t1_578 * L_0 = ___v1;
		Version_t1_578 * L_1 = ___v2;
		NullCheck(L_0);
		int32_t L_2 = Version_CompareTo_m1_14737(L_0, L_1, /*hidden argument*/NULL);
		return ((((int32_t)L_2) < ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean System.Version::op_LessThanOrEqual(System.Version,System.Version)
extern "C" bool Version_op_LessThanOrEqual_m1_14748 (Object_t * __this /* static, unused */, Version_t1_578 * ___v1, Version_t1_578 * ___v2, const MethodInfo* method)
{
	{
		Version_t1_578 * L_0 = ___v1;
		Version_t1_578 * L_1 = ___v2;
		NullCheck(L_0);
		int32_t L_2 = Version_CompareTo_m1_14737(L_0, L_1, /*hidden argument*/NULL);
		return ((((int32_t)((((int32_t)L_2) > ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.WeakReference::.ctor()
extern "C" void WeakReference__ctor_m1_14749 (WeakReference_t1_1014 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.WeakReference::.ctor(System.Object)
extern "C" void WeakReference__ctor_m1_14750 (WeakReference_t1_1014 * __this, Object_t * ___target, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___target;
		WeakReference__ctor_m1_14751(__this, L_0, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.WeakReference::.ctor(System.Object,System.Boolean)
extern "C" void WeakReference__ctor_m1_14751 (WeakReference_t1_1014 * __this, Object_t * ___target, bool ___trackResurrection, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		bool L_0 = ___trackResurrection;
		__this->___isLongReference_0 = L_0;
		Object_t * L_1 = ___target;
		WeakReference_AllocateHandle_m1_14753(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.WeakReference::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const Il2CppType* Object_t_0_0_0_var;
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral135;
extern Il2CppCodeGenString* _stringLiteral3648;
extern Il2CppCodeGenString* _stringLiteral3649;
extern "C" void WeakReference__ctor_m1_14752 (WeakReference_t1_1014 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Object_t_0_0_0_var = il2cpp_codegen_type_from_index(0);
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		_stringLiteral135 = il2cpp_codegen_string_literal_from_index(135);
		_stringLiteral3648 = il2cpp_codegen_string_literal_from_index(3648);
		_stringLiteral3649 = il2cpp_codegen_string_literal_from_index(3649);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		SerializationInfo_t1_293 * L_0 = ___info;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, _stringLiteral135, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0017:
	{
		SerializationInfo_t1_293 * L_2 = ___info;
		NullCheck(L_2);
		bool L_3 = SerializationInfo_GetBoolean_m1_9643(L_2, _stringLiteral3648, /*hidden argument*/NULL);
		__this->___isLongReference_0 = L_3;
		SerializationInfo_t1_293 * L_4 = ___info;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(Object_t_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_4);
		Object_t * L_6 = SerializationInfo_GetValue_m1_9625(L_4, _stringLiteral3649, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		Object_t * L_7 = V_0;
		WeakReference_AllocateHandle_m1_14753(__this, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.WeakReference::AllocateHandle(System.Object)
extern "C" void WeakReference_AllocateHandle_m1_14753 (WeakReference_t1_1014 * __this, Object_t * ___target, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___isLongReference_0);
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Object_t * L_1 = ___target;
		GCHandle_t1_795  L_2 = GCHandle_Alloc_m1_7657(NULL /*static, unused*/, L_1, 1, /*hidden argument*/NULL);
		__this->___gcHandle_1 = L_2;
		goto IL_002a;
	}

IL_001d:
	{
		Object_t * L_3 = ___target;
		GCHandle_t1_795  L_4 = GCHandle_Alloc_m1_7657(NULL /*static, unused*/, L_3, 0, /*hidden argument*/NULL);
		__this->___gcHandle_1 = L_4;
	}

IL_002a:
	{
		return;
	}
}
// System.Boolean System.WeakReference::get_IsAlive()
extern "C" bool WeakReference_get_IsAlive_m1_14754 (WeakReference_t1_1014 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(6 /* System.Object System.WeakReference::get_Target() */, __this);
		return ((((int32_t)((((Object_t*)(Object_t *)L_0) == ((Object_t*)(Object_t *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Object System.WeakReference::get_Target()
extern "C" Object_t * WeakReference_get_Target_m1_14755 (WeakReference_t1_1014 * __this, const MethodInfo* method)
{
	{
		GCHandle_t1_795 * L_0 = &(__this->___gcHandle_1);
		Object_t * L_1 = GCHandle_get_Target_m1_7653(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void System.WeakReference::set_Target(System.Object)
extern "C" void WeakReference_set_Target_m1_14756 (WeakReference_t1_1014 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		GCHandle_t1_795 * L_0 = &(__this->___gcHandle_1);
		Object_t * L_1 = ___value;
		GCHandle_set_Target_m1_7654(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.WeakReference::get_TrackResurrection()
extern "C" bool WeakReference_get_TrackResurrection_m1_14757 (WeakReference_t1_1014 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___isLongReference_0);
		return L_0;
	}
}
// System.Void System.WeakReference::Finalize()
extern "C" void WeakReference_Finalize_m1_14758 (WeakReference_t1_1014 * __this, const MethodInfo* method)
{
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		GCHandle_t1_795 * L_0 = &(__this->___gcHandle_1);
		GCHandle_Free_m1_7658(L_0, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x17, FINALLY_0010);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0010;
	}

FINALLY_0010:
	{ // begin finally (depth: 1)
		Object_Finalize_m1_3(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(16)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(16)
	{
		IL2CPP_JUMP_TBL(0x17, IL_0017)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0017:
	{
		return;
	}
}
// System.Void System.WeakReference::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t1_33_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral135;
extern Il2CppCodeGenString* _stringLiteral3648;
extern Il2CppCodeGenString* _stringLiteral3649;
extern "C" void WeakReference_GetObjectData_m1_14759 (WeakReference_t1_1014 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		Exception_t1_33_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(49);
		_stringLiteral135 = il2cpp_codegen_string_literal_from_index(135);
		_stringLiteral3648 = il2cpp_codegen_string_literal_from_index(3648);
		_stringLiteral3649 = il2cpp_codegen_string_literal_from_index(3649);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		SerializationInfo_t1_293 * L_0 = ___info;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, _stringLiteral135, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		SerializationInfo_t1_293 * L_2 = ___info;
		bool L_3 = (bool)VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean System.WeakReference::get_TrackResurrection() */, __this);
		NullCheck(L_2);
		SerializationInfo_AddValue_m1_9632(L_2, _stringLiteral3648, L_3, /*hidden argument*/NULL);
	}

IL_0022:
	try
	{ // begin try (depth: 1)
		SerializationInfo_t1_293 * L_4 = ___info;
		Object_t * L_5 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(6 /* System.Object System.WeakReference::get_Target() */, __this);
		NullCheck(L_4);
		SerializationInfo_AddValue_m1_9642(L_4, _stringLiteral3649, L_5, /*hidden argument*/NULL);
		goto IL_004a;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1_33 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1_33_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0038;
		throw e;
	}

CATCH_0038:
	{ // begin catch(System.Exception)
		SerializationInfo_t1_293 * L_6 = ___info;
		NullCheck(L_6);
		SerializationInfo_AddValue_m1_9642(L_6, _stringLiteral3649, NULL, /*hidden argument*/NULL);
		goto IL_004a;
	} // end catch (depth: 1)

IL_004a:
	{
		return;
	}
}
// System.Void System.__ComObject::.ctor()
extern "C" void __ComObject__ctor_m1_14760 (__ComObject_t1_131 * __this, const MethodInfo* method)
{
	{
		MarshalByRefObject__ctor_m1_1370(__this, /*hidden argument*/NULL);
		Type_t * L_0 = Object_GetType_m1_5(__this, /*hidden argument*/NULL);
		__ComObject_Initialize_m1_14766(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.__ComObject::.ctor(System.Type)
extern "C" void __ComObject__ctor_m1_14761 (__ComObject_t1_131 * __this, Type_t * ___t, const MethodInfo* method)
{
	{
		MarshalByRefObject__ctor_m1_1370(__this, /*hidden argument*/NULL);
		Type_t * L_0 = ___t;
		__ComObject_Initialize_m1_14766(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.__ComObject::.ctor(System.IntPtr)
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern "C" void __ComObject__ctor_m1_14762 (__ComObject_t1_131 * __this, IntPtr_t ___pItf, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		s_Il2CppMethodIntialized = true;
	}
	Guid_t1_319  V_0 = {0};
	int32_t V_1 = 0;
	{
		MarshalByRefObject__ctor_m1_1370(__this, /*hidden argument*/NULL);
		Guid_t1_319  L_0 = __ComObject_get_IID_IUnknown_m1_14774(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		IntPtr_t L_1 = ___pItf;
		IntPtr_t* L_2 = &(__this->___iunknown_1);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		int32_t L_3 = Marshal_QueryInterface_m1_7798(NULL /*static, unused*/, L_1, (&V_0), L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = V_1;
		Marshal_ThrowExceptionForHR_m1_7837(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.__ComObject System.__ComObject::CreateRCW(System.Type)
extern "C" __ComObject_t1_131 * __ComObject_CreateRCW_m1_14763 (Object_t * __this /* static, unused */, Type_t * ___t, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef __ComObject_t1_131 * (*__ComObject_CreateRCW_m1_14763_ftn) (Type_t *);
	return  ((__ComObject_CreateRCW_m1_14763_ftn)mscorlib::System::__ComObject::CreateRCW) (___t);
}
// System.Void System.__ComObject::ReleaseInterfaces()
extern "C" void __ComObject_ReleaseInterfaces_m1_14764 (__ComObject_t1_131 * __this, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef void (*__ComObject_ReleaseInterfaces_m1_14764_ftn) (__ComObject_t1_131 *);
	 ((__ComObject_ReleaseInterfaces_m1_14764_ftn)mscorlib::System::__ComObject::ReleaseInterfaces) (__this);
}
// System.Void System.__ComObject::Finalize()
extern "C" void __ComObject_Finalize_m1_14765 (__ComObject_t1_131 * __this, const MethodInfo* method)
{
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		__ComObject_ReleaseInterfaces_m1_14764(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m1_3(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0012:
	{
		return;
	}
}
// System.Void System.__ComObject::Initialize(System.Type)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* ExtensibleClassFactory_t1_790_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* COMException_t1_760_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3650;
extern "C" void __ComObject_Initialize_m1_14766 (__ComObject_t1_131 * __this, Type_t * ___t, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		ExtensibleClassFactory_t1_790_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(592);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		COMException_t1_760_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(597);
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		_stringLiteral3650 = il2cpp_codegen_string_literal_from_index(3650);
		s_Il2CppMethodIntialized = true;
	}
	ObjectCreationDelegate_t1_1621 * V_0 = {0};
	int32_t V_1 = 0;
	{
		IntPtr_t L_0 = (__this->___iunknown_1);
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		bool L_2 = IntPtr_op_Inequality_m1_841(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0016;
		}
	}
	{
		return;
	}

IL_0016:
	{
		Type_t * L_3 = ___t;
		IL2CPP_RUNTIME_CLASS_INIT(ExtensibleClassFactory_t1_790_il2cpp_TypeInfo_var);
		ObjectCreationDelegate_t1_1621 * L_4 = ExtensibleClassFactory_GetObjectCreationCallback_m1_7641(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		ObjectCreationDelegate_t1_1621 * L_5 = V_0;
		if (!L_5)
		{
			goto IL_005f;
		}
	}
	{
		ObjectCreationDelegate_t1_1621 * L_6 = V_0;
		IntPtr_t L_7 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		NullCheck(L_6);
		IntPtr_t L_8 = ObjectCreationDelegate_Invoke_m1_14800(L_6, L_7, /*hidden argument*/NULL);
		__this->___iunknown_1 = L_8;
		IntPtr_t L_9 = (__this->___iunknown_1);
		IntPtr_t L_10 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		bool L_11 = IntPtr_op_Equality_m1_840(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_005a;
		}
	}
	{
		Type_t * L_12 = ___t;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Format_m1_548(NULL /*static, unused*/, _stringLiteral3650, L_12, /*hidden argument*/NULL);
		COMException_t1_760 * L_14 = (COMException_t1_760 *)il2cpp_codegen_object_new (COMException_t1_760_il2cpp_TypeInfo_var);
		COMException__ctor_m1_7591(L_14, L_13, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_14);
	}

IL_005a:
	{
		goto IL_0083;
	}

IL_005f:
	{
		Type_t * L_15 = ___t;
		Guid_t1_319  L_16 = __ComObject_GetCLSID_m1_14767(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		IntPtr_t L_17 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		Guid_t1_319  L_18 = __ComObject_get_IID_IUnknown_m1_14774(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t* L_19 = &(__this->___iunknown_1);
		int32_t L_20 = __ComObject_CoCreateInstance_m1_14778(NULL /*static, unused*/, L_16, L_17, ((int32_t)21), L_18, L_19, /*hidden argument*/NULL);
		V_1 = L_20;
		int32_t L_21 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		Marshal_ThrowExceptionForHR_m1_7837(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
	}

IL_0083:
	{
		return;
	}
}
// System.Guid System.__ComObject::GetCLSID(System.Type)
extern const Il2CppType* Object_t_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* COMException_t1_760_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3651;
extern "C" Guid_t1_319  __ComObject_GetCLSID_m1_14767 (Object_t * __this /* static, unused */, Type_t * ___t, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Object_t_0_0_0_var = il2cpp_codegen_type_from_index(0);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		COMException_t1_760_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(597);
		_stringLiteral3651 = il2cpp_codegen_string_literal_from_index(3651);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = {0};
	{
		Type_t * L_0 = ___t;
		NullCheck(L_0);
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(129 /* System.Boolean System.Type::get_IsImport() */, L_0);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		Type_t * L_2 = ___t;
		NullCheck(L_2);
		Guid_t1_319  L_3 = (Guid_t1_319 )VirtFuncInvoker0< Guid_t1_319  >::Invoke(160 /* System.Guid System.Type::get_GUID() */, L_2);
		return L_3;
	}

IL_0012:
	{
		Type_t * L_4 = ___t;
		NullCheck(L_4);
		Type_t * L_5 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(158 /* System.Type System.Type::get_BaseType() */, L_4);
		V_0 = L_5;
		goto IL_0037;
	}

IL_001e:
	{
		Type_t * L_6 = V_0;
		NullCheck(L_6);
		bool L_7 = (bool)VirtFuncInvoker0< bool >::Invoke(129 /* System.Boolean System.Type::get_IsImport() */, L_6);
		if (!L_7)
		{
			goto IL_0030;
		}
	}
	{
		Type_t * L_8 = V_0;
		NullCheck(L_8);
		Guid_t1_319  L_9 = (Guid_t1_319 )VirtFuncInvoker0< Guid_t1_319  >::Invoke(160 /* System.Guid System.Type::get_GUID() */, L_8);
		return L_9;
	}

IL_0030:
	{
		Type_t * L_10 = V_0;
		NullCheck(L_10);
		Type_t * L_11 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(158 /* System.Type System.Type::get_BaseType() */, L_10);
		V_0 = L_11;
	}

IL_0037:
	{
		Type_t * L_12 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_13 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(Object_t_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_12) == ((Object_t*)(Type_t *)L_13))))
		{
			goto IL_001e;
		}
	}
	{
		Type_t * L_14 = ___t;
		NullCheck(L_14);
		String_t* L_15 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Type::ToString() */, L_14);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m1_559(NULL /*static, unused*/, _stringLiteral3651, L_15, /*hidden argument*/NULL);
		COMException_t1_760 * L_17 = (COMException_t1_760 *)il2cpp_codegen_object_new (COMException_t1_760_il2cpp_TypeInfo_var);
		COMException__ctor_m1_7591(L_17, L_16, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_17);
	}
}
// System.IntPtr System.__ComObject::GetInterfaceInternal(System.Type,System.Boolean)
extern "C" IntPtr_t __ComObject_GetInterfaceInternal_m1_14768 (__ComObject_t1_131 * __this, Type_t * ___t, bool ___throwException, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef IntPtr_t (*__ComObject_GetInterfaceInternal_m1_14768_ftn) (__ComObject_t1_131 *, Type_t *, bool);
	return  ((__ComObject_GetInterfaceInternal_m1_14768_ftn)mscorlib::System::__ComObject::GetInterfaceInternal) (__this, ___t, ___throwException);
}
// System.IntPtr System.__ComObject::GetInterface(System.Type,System.Boolean)
extern "C" IntPtr_t __ComObject_GetInterface_m1_14769 (__ComObject_t1_131 * __this, Type_t * ___t, bool ___throwException, const MethodInfo* method)
{
	{
		__ComObject_CheckIUnknown_m1_14771(__this, /*hidden argument*/NULL);
		Type_t * L_0 = ___t;
		bool L_1 = ___throwException;
		IntPtr_t L_2 = __ComObject_GetInterfaceInternal_m1_14768(__this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.IntPtr System.__ComObject::GetInterface(System.Type)
extern "C" IntPtr_t __ComObject_GetInterface_m1_14770 (__ComObject_t1_131 * __this, Type_t * ___t, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___t;
		IntPtr_t L_1 = __ComObject_GetInterface_m1_14769(__this, L_0, 1, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void System.__ComObject::CheckIUnknown()
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidComObjectException_t1_806_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3652;
extern "C" void __ComObject_CheckIUnknown_m1_14771 (__ComObject_t1_131 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		InvalidComObjectException_t1_806_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1117);
		_stringLiteral3652 = il2cpp_codegen_string_literal_from_index(3652);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = (__this->___iunknown_1);
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		bool L_2 = IntPtr_op_Equality_m1_840(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		InvalidComObjectException_t1_806 * L_3 = (InvalidComObjectException_t1_806 *)il2cpp_codegen_object_new (InvalidComObjectException_t1_806_il2cpp_TypeInfo_var);
		InvalidComObjectException__ctor_m1_7686(L_3, _stringLiteral3652, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0020:
	{
		return;
	}
}
// System.IntPtr System.__ComObject::get_IUnknown()
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidComObjectException_t1_806_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3652;
extern "C" IntPtr_t __ComObject_get_IUnknown_m1_14772 (__ComObject_t1_131 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		InvalidComObjectException_t1_806_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1117);
		_stringLiteral3652 = il2cpp_codegen_string_literal_from_index(3652);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = (__this->___iunknown_1);
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		bool L_2 = IntPtr_op_Equality_m1_840(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		InvalidComObjectException_t1_806 * L_3 = (InvalidComObjectException_t1_806 *)il2cpp_codegen_object_new (InvalidComObjectException_t1_806_il2cpp_TypeInfo_var);
		InvalidComObjectException__ctor_m1_7686(L_3, _stringLiteral3652, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0020:
	{
		IntPtr_t L_4 = (__this->___iunknown_1);
		return L_4;
	}
}
// System.IntPtr System.__ComObject::get_IDispatch()
extern const Il2CppType* IDispatch_t1_1801_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidComObjectException_t1_806_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3652;
extern "C" IntPtr_t __ComObject_get_IDispatch_m1_14773 (__ComObject_t1_131 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDispatch_t1_1801_0_0_0_var = il2cpp_codegen_type_from_index(1118);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		InvalidComObjectException_t1_806_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1117);
		_stringLiteral3652 = il2cpp_codegen_string_literal_from_index(3652);
		s_Il2CppMethodIntialized = true;
	}
	IntPtr_t V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(IDispatch_t1_1801_0_0_0_var), /*hidden argument*/NULL);
		IntPtr_t L_1 = __ComObject_GetInterface_m1_14770(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		IntPtr_t L_2 = V_0;
		IntPtr_t L_3 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		bool L_4 = IntPtr_op_Equality_m1_840(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_002c;
		}
	}
	{
		InvalidComObjectException_t1_806 * L_5 = (InvalidComObjectException_t1_806 *)il2cpp_codegen_object_new (InvalidComObjectException_t1_806_il2cpp_TypeInfo_var);
		InvalidComObjectException__ctor_m1_7686(L_5, _stringLiteral3652, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_5);
	}

IL_002c:
	{
		IntPtr_t L_6 = V_0;
		return L_6;
	}
}
// System.Guid System.__ComObject::get_IID_IUnknown()
extern Il2CppCodeGenString* _stringLiteral3653;
extern "C" Guid_t1_319  __ComObject_get_IID_IUnknown_m1_14774 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral3653 = il2cpp_codegen_string_literal_from_index(3653);
		s_Il2CppMethodIntialized = true;
	}
	{
		Guid_t1_319  L_0 = {0};
		Guid__ctor_m1_14131(&L_0, _stringLiteral3653, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Guid System.__ComObject::get_IID_IDispatch()
extern Il2CppCodeGenString* _stringLiteral3654;
extern "C" Guid_t1_319  __ComObject_get_IID_IDispatch_m1_14775 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral3654 = il2cpp_codegen_string_literal_from_index(3654);
		s_Il2CppMethodIntialized = true;
	}
	{
		Guid_t1_319  L_0 = {0};
		Guid__ctor_m1_14131(&L_0, _stringLiteral3654, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean System.__ComObject::Equals(System.Object)
extern TypeInfo* __ComObject_t1_131_il2cpp_TypeInfo_var;
extern "C" bool __ComObject_Equals_m1_14776 (__ComObject_t1_131 * __this, Object_t * ___obj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		__ComObject_t1_131_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(175);
		s_Il2CppMethodIntialized = true;
	}
	__ComObject_t1_131 * V_0 = {0};
	{
		__ComObject_CheckIUnknown_m1_14771(__this, /*hidden argument*/NULL);
		Object_t * L_0 = ___obj;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		return 0;
	}

IL_000e:
	{
		Object_t * L_1 = ___obj;
		V_0 = ((__ComObject_t1_131 *)IsInstClass(L_1, __ComObject_t1_131_il2cpp_TypeInfo_var));
		__ComObject_t1_131 * L_2 = V_0;
		if (L_2)
		{
			goto IL_001d;
		}
	}
	{
		return 0;
	}

IL_001d:
	{
		IntPtr_t L_3 = (__this->___iunknown_1);
		__ComObject_t1_131 * L_4 = V_0;
		NullCheck(L_4);
		IntPtr_t L_5 = __ComObject_get_IUnknown_m1_14772(L_4, /*hidden argument*/NULL);
		bool L_6 = IntPtr_op_Equality_m1_840(NULL /*static, unused*/, L_3, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Int32 System.__ComObject::GetHashCode()
extern "C" int32_t __ComObject_GetHashCode_m1_14777 (__ComObject_t1_131 * __this, const MethodInfo* method)
{
	{
		__ComObject_CheckIUnknown_m1_14771(__this, /*hidden argument*/NULL);
		IntPtr_t* L_0 = &(__this->___iunknown_1);
		int32_t L_1 = IntPtr_ToInt32_m1_835(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 System.__ComObject::CoCreateInstance(System.Guid,System.IntPtr,System.UInt32,System.Guid,System.IntPtr&)
extern "C" int32_t __ComObject_CoCreateInstance_m1_14778 (Object_t * __this /* static, unused */, Guid_t1_319  ___rclsid, IntPtr_t ___pUnkOuter, uint32_t ___dwClsContext, Guid_t1_319  ___riid, IntPtr_t* ___pUnk, const MethodInfo* method)
{
	typedef int32_t (STDCALL *PInvokeFunc) (Guid_t1_319 *, intptr_t, uint32_t, Guid_t1_319 *, intptr_t*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		int parameterSize = sizeof(Guid_t1_319 ) + sizeof(IntPtr_t) + sizeof(uint32_t) + sizeof(Guid_t1_319 ) + sizeof(intptr_t*);
		_il2cpp_pinvoke_func = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>("ole32.dll", "CoCreateInstance", IL2CPP_CALL_STDCALL, CHARSET_UNICODE, parameterSize, true);

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'CoCreateInstance'"));
		}
	}

	// Marshaling of parameter '___rclsid' to native representation

	// Marshaling of parameter '___pUnkOuter' to native representation

	// Marshaling of parameter '___dwClsContext' to native representation

	// Marshaling of parameter '___riid' to native representation

	// Marshaling of parameter '___pUnk' to native representation
	intptr_t ____pUnk_empty = { 0 };
	intptr_t* ____pUnk_marshaled = &____pUnk_empty;

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(&___rclsid, reinterpret_cast<intptr_t>((___pUnkOuter).___m_value_0), ___dwClsContext, &___riid, ____pUnk_marshaled);

	// Marshaling cleanup of parameter '___rclsid' native representation

	// Marshaling cleanup of parameter '___pUnkOuter' native representation

	// Marshaling cleanup of parameter '___dwClsContext' native representation

	// Marshaling cleanup of parameter '___riid' native representation

	// Marshaling of parameter '___pUnk' back from native representation
	IntPtr_t ____pUnk_result_dereferenced = { 0 };
	IntPtr_t* ____pUnk_result = &____pUnk_result_dereferenced;
	(*____pUnk_result).___m_value_0 = reinterpret_cast<void*>(*____pUnk_marshaled);
	*___pUnk = *____pUnk_result;

	return _return_value;
}
// System.Void Mono.Math.Prime.PrimalityTest::.ctor(System.Object,System.IntPtr)
extern "C" void PrimalityTest__ctor_m1_14779 (PrimalityTest_t1_1619 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Boolean Mono.Math.Prime.PrimalityTest::Invoke(Mono.Math.BigInteger,Mono.Math.Prime.ConfidenceFactor)
extern "C" bool PrimalityTest_Invoke_m1_14780 (PrimalityTest_t1_1619 * __this, BigInteger_t1_139 * ___bi, int32_t ___confidence, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		PrimalityTest_Invoke_m1_14780((PrimalityTest_t1_1619 *)__this->___prev_9,___bi, ___confidence, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Object_t *, Object_t * __this, BigInteger_t1_139 * ___bi, int32_t ___confidence, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___bi, ___confidence,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Object_t * __this, BigInteger_t1_139 * ___bi, int32_t ___confidence, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___bi, ___confidence,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef bool (*FunctionPointerType) (Object_t * __this, int32_t ___confidence, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(___bi, ___confidence,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" bool pinvoke_delegate_wrapper_PrimalityTest_t1_1619(Il2CppObject* delegate, BigInteger_t1_139 * ___bi, int32_t ___confidence)
{
	// Marshaling of parameter '___bi' to native representation
	BigInteger_t1_139 * ____bi_marshaled = { 0 };
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'Mono.Math.BigInteger'."));
}
// System.IAsyncResult Mono.Math.Prime.PrimalityTest::BeginInvoke(Mono.Math.BigInteger,Mono.Math.Prime.ConfidenceFactor,System.AsyncCallback,System.Object)
extern TypeInfo* ConfidenceFactor_t1_135_il2cpp_TypeInfo_var;
extern "C" Object_t * PrimalityTest_BeginInvoke_m1_14781 (PrimalityTest_t1_1619 * __this, BigInteger_t1_139 * ___bi, int32_t ___confidence, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ConfidenceFactor_t1_135_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1119);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___bi;
	__d_args[1] = Box(ConfidenceFactor_t1_135_il2cpp_TypeInfo_var, &___confidence);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Boolean Mono.Math.Prime.PrimalityTest::EndInvoke(System.IAsyncResult)
extern "C" bool PrimalityTest_EndInvoke_m1_14782 (PrimalityTest_t1_1619 * __this, Object_t * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Reflection.MemberFilter::.ctor(System.Object,System.IntPtr)
extern "C" void MemberFilter__ctor_m1_14783 (MemberFilter_t1_32 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Boolean System.Reflection.MemberFilter::Invoke(System.Reflection.MemberInfo,System.Object)
extern "C" bool MemberFilter_Invoke_m1_14784 (MemberFilter_t1_32 * __this, MemberInfo_t * ___m, Object_t * ___filterCriteria, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		MemberFilter_Invoke_m1_14784((MemberFilter_t1_32 *)__this->___prev_9,___m, ___filterCriteria, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Object_t *, Object_t * __this, MemberInfo_t * ___m, Object_t * ___filterCriteria, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___m, ___filterCriteria,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Object_t * __this, MemberInfo_t * ___m, Object_t * ___filterCriteria, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___m, ___filterCriteria,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef bool (*FunctionPointerType) (Object_t * __this, Object_t * ___filterCriteria, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(___m, ___filterCriteria,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" bool pinvoke_delegate_wrapper_MemberFilter_t1_32(Il2CppObject* delegate, MemberInfo_t * ___m, Object_t * ___filterCriteria)
{
	// Marshaling of parameter '___m' to native representation
	MemberInfo_t * ____m_marshaled = { 0 };
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Reflection.MemberInfo'."));
}
// System.IAsyncResult System.Reflection.MemberFilter::BeginInvoke(System.Reflection.MemberInfo,System.Object,System.AsyncCallback,System.Object)
extern "C" Object_t * MemberFilter_BeginInvoke_m1_14785 (MemberFilter_t1_32 * __this, MemberInfo_t * ___m, Object_t * ___filterCriteria, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___m;
	__d_args[1] = ___filterCriteria;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Boolean System.Reflection.MemberFilter::EndInvoke(System.IAsyncResult)
extern "C" bool MemberFilter_EndInvoke_m1_14786 (MemberFilter_t1_32 * __this, Object_t * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Reflection.ModuleResolveEventHandler::.ctor(System.Object,System.IntPtr)
extern "C" void ModuleResolveEventHandler__ctor_m1_14787 (ModuleResolveEventHandler_t1_561 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Reflection.Module System.Reflection.ModuleResolveEventHandler::Invoke(System.Object,System.ResolveEventArgs)
extern "C" Module_t1_495 * ModuleResolveEventHandler_Invoke_m1_14788 (ModuleResolveEventHandler_t1_561 * __this, Object_t * ___sender, ResolveEventArgs_t1_1595 * ___e, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		ModuleResolveEventHandler_Invoke_m1_14788((ModuleResolveEventHandler_t1_561 *)__this->___prev_9,___sender, ___e, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef Module_t1_495 * (*FunctionPointerType) (Object_t *, Object_t * __this, Object_t * ___sender, ResolveEventArgs_t1_1595 * ___e, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___sender, ___e,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef Module_t1_495 * (*FunctionPointerType) (Object_t * __this, Object_t * ___sender, ResolveEventArgs_t1_1595 * ___e, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___sender, ___e,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef Module_t1_495 * (*FunctionPointerType) (Object_t * __this, ResolveEventArgs_t1_1595 * ___e, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(___sender, ___e,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" Module_t1_495 * pinvoke_delegate_wrapper_ModuleResolveEventHandler_t1_561(Il2CppObject* delegate, Object_t * ___sender, ResolveEventArgs_t1_1595 * ___e)
{
	// Marshaling of parameter '___sender' to native representation
	Object_t * ____sender_marshaled = { 0 };
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Object'."));
}
// System.IAsyncResult System.Reflection.ModuleResolveEventHandler::BeginInvoke(System.Object,System.ResolveEventArgs,System.AsyncCallback,System.Object)
extern "C" Object_t * ModuleResolveEventHandler_BeginInvoke_m1_14789 (ModuleResolveEventHandler_t1_561 * __this, Object_t * ___sender, ResolveEventArgs_t1_1595 * ___e, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___sender;
	__d_args[1] = ___e;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Reflection.Module System.Reflection.ModuleResolveEventHandler::EndInvoke(System.IAsyncResult)
extern "C" Module_t1_495 * ModuleResolveEventHandler_EndInvoke_m1_14790 (ModuleResolveEventHandler_t1_561 * __this, Object_t * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return (Module_t1_495 *)__result;
}
// System.Void System.Reflection.GetterAdapter::.ctor(System.Object,System.IntPtr)
extern "C" void GetterAdapter__ctor_m1_14791 (GetterAdapter_t1_1620 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Object System.Reflection.GetterAdapter::Invoke(System.Object)
extern "C" Object_t * GetterAdapter_Invoke_m1_14792 (GetterAdapter_t1_1620 * __this, Object_t * ____this, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		GetterAdapter_Invoke_m1_14792((GetterAdapter_t1_1620 *)__this->___prev_9,____this, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef Object_t * (*FunctionPointerType) (Object_t *, Object_t * __this, Object_t * ____this, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,____this,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef Object_t * (*FunctionPointerType) (Object_t * __this, Object_t * ____this, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,____this,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef Object_t * (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(____this,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" Object_t * pinvoke_delegate_wrapper_GetterAdapter_t1_1620(Il2CppObject* delegate, Object_t * ____this)
{
	// Marshaling of parameter '____this' to native representation
	Object_t * _____this_marshaled = { 0 };
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Object'."));
}
// System.IAsyncResult System.Reflection.GetterAdapter::BeginInvoke(System.Object,System.AsyncCallback,System.Object)
extern "C" Object_t * GetterAdapter_BeginInvoke_m1_14793 (GetterAdapter_t1_1620 * __this, Object_t * ____this, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ____this;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Object System.Reflection.GetterAdapter::EndInvoke(System.IAsyncResult)
extern "C" Object_t * GetterAdapter_EndInvoke_m1_14794 (GetterAdapter_t1_1620 * __this, Object_t * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return (Object_t *)__result;
}
// System.Void System.Reflection.TypeFilter::.ctor(System.Object,System.IntPtr)
extern "C" void TypeFilter__ctor_m1_14795 (TypeFilter_t1_616 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Boolean System.Reflection.TypeFilter::Invoke(System.Type,System.Object)
extern "C" bool TypeFilter_Invoke_m1_14796 (TypeFilter_t1_616 * __this, Type_t * ___m, Object_t * ___filterCriteria, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		TypeFilter_Invoke_m1_14796((TypeFilter_t1_616 *)__this->___prev_9,___m, ___filterCriteria, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Object_t *, Object_t * __this, Type_t * ___m, Object_t * ___filterCriteria, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___m, ___filterCriteria,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Object_t * __this, Type_t * ___m, Object_t * ___filterCriteria, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___m, ___filterCriteria,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef bool (*FunctionPointerType) (Object_t * __this, Object_t * ___filterCriteria, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(___m, ___filterCriteria,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" bool pinvoke_delegate_wrapper_TypeFilter_t1_616(Il2CppObject* delegate, Type_t * ___m, Object_t * ___filterCriteria)
{
	// Marshaling of parameter '___m' to native representation
	Type_t * ____m_marshaled = { 0 };
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Type'."));
}
// System.IAsyncResult System.Reflection.TypeFilter::BeginInvoke(System.Type,System.Object,System.AsyncCallback,System.Object)
extern "C" Object_t * TypeFilter_BeginInvoke_m1_14797 (TypeFilter_t1_616 * __this, Type_t * ___m, Object_t * ___filterCriteria, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___m;
	__d_args[1] = ___filterCriteria;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Boolean System.Reflection.TypeFilter::EndInvoke(System.IAsyncResult)
extern "C" bool TypeFilter_EndInvoke_m1_14798 (TypeFilter_t1_616 * __this, Object_t * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Runtime.InteropServices.ObjectCreationDelegate::.ctor(System.Object,System.IntPtr)
extern "C" void ObjectCreationDelegate__ctor_m1_14799 (ObjectCreationDelegate_t1_1621 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.IntPtr System.Runtime.InteropServices.ObjectCreationDelegate::Invoke(System.IntPtr)
extern "C" IntPtr_t ObjectCreationDelegate_Invoke_m1_14800 (ObjectCreationDelegate_t1_1621 * __this, IntPtr_t ___aggregator, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		ObjectCreationDelegate_Invoke_m1_14800((ObjectCreationDelegate_t1_1621 *)__this->___prev_9,___aggregator, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef IntPtr_t (*FunctionPointerType) (Object_t *, Object_t * __this, IntPtr_t ___aggregator, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___aggregator,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef IntPtr_t (*FunctionPointerType) (Object_t * __this, IntPtr_t ___aggregator, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___aggregator,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" IntPtr_t pinvoke_delegate_wrapper_ObjectCreationDelegate_t1_1621(Il2CppObject* delegate, IntPtr_t ___aggregator)
{
	typedef intptr_t (STDCALL *native_function_ptr_type)(intptr_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___aggregator' to native representation

	// Native function invocation and marshaling of return value back from native representation
	intptr_t _return_value = _il2cpp_pinvoke_func(reinterpret_cast<intptr_t>((___aggregator).___m_value_0));
	IntPtr_t __return_value_unmarshaled;
	(__return_value_unmarshaled).___m_value_0 = reinterpret_cast<void*>(_return_value);

	// Marshaling cleanup of parameter '___aggregator' native representation

	return __return_value_unmarshaled;
}
// System.IAsyncResult System.Runtime.InteropServices.ObjectCreationDelegate::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" Object_t * ObjectCreationDelegate_BeginInvoke_m1_14801 (ObjectCreationDelegate_t1_1621 * __this, IntPtr_t ___aggregator, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___aggregator);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.IntPtr System.Runtime.InteropServices.ObjectCreationDelegate::EndInvoke(System.IAsyncResult)
extern "C" IntPtr_t ObjectCreationDelegate_EndInvoke_m1_14802 (ObjectCreationDelegate_t1_1621 * __this, Object_t * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return *(IntPtr_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Runtime.Remoting.Contexts.CrossContextDelegate::.ctor(System.Object,System.IntPtr)
extern "C" void CrossContextDelegate__ctor_m1_14803 (CrossContextDelegate_t1_1622 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void System.Runtime.Remoting.Contexts.CrossContextDelegate::Invoke()
extern "C" void CrossContextDelegate_Invoke_m1_14804 (CrossContextDelegate_t1_1622 * __this, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		CrossContextDelegate_Invoke_m1_14804((CrossContextDelegate_t1_1622 *)__this->___prev_9, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if ((__this->___m_target_2 != NULL || MethodHasParameters((MethodInfo*)(__this->___method_3.___m_value_0))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_CrossContextDelegate_t1_1622(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.IAsyncResult System.Runtime.Remoting.Contexts.CrossContextDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * CrossContextDelegate_BeginInvoke_m1_14805 (CrossContextDelegate_t1_1622 * __this, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void System.Runtime.Remoting.Contexts.CrossContextDelegate::EndInvoke(System.IAsyncResult)
extern "C" void CrossContextDelegate_EndInvoke_m1_14806 (CrossContextDelegate_t1_1622 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void System.Runtime.Remoting.Messaging.HeaderHandler::.ctor(System.Object,System.IntPtr)
extern "C" void HeaderHandler__ctor_m1_14807 (HeaderHandler_t1_1623 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Object System.Runtime.Remoting.Messaging.HeaderHandler::Invoke(System.Runtime.Remoting.Messaging.Header[])
extern "C" Object_t * HeaderHandler_Invoke_m1_14808 (HeaderHandler_t1_1623 * __this, HeaderU5BU5D_t1_927* ___headers, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		HeaderHandler_Invoke_m1_14808((HeaderHandler_t1_1623 *)__this->___prev_9,___headers, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef Object_t * (*FunctionPointerType) (Object_t *, Object_t * __this, HeaderU5BU5D_t1_927* ___headers, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___headers,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef Object_t * (*FunctionPointerType) (Object_t * __this, HeaderU5BU5D_t1_927* ___headers, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___headers,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef Object_t * (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(___headers,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" Object_t * pinvoke_delegate_wrapper_HeaderHandler_t1_1623(Il2CppObject* delegate, HeaderU5BU5D_t1_927* ___headers)
{
	// Marshaling of parameter '___headers' to native representation
	HeaderU5BU5D_t1_927* ____headers_marshaled = { 0 };
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Runtime.Remoting.Messaging.Header[]'."));
}
// System.IAsyncResult System.Runtime.Remoting.Messaging.HeaderHandler::BeginInvoke(System.Runtime.Remoting.Messaging.Header[],System.AsyncCallback,System.Object)
extern "C" Object_t * HeaderHandler_BeginInvoke_m1_14809 (HeaderHandler_t1_1623 * __this, HeaderU5BU5D_t1_927* ___headers, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___headers;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Object System.Runtime.Remoting.Messaging.HeaderHandler::EndInvoke(System.IAsyncResult)
extern "C" Object_t * HeaderHandler_EndInvoke_m1_14810 (HeaderHandler_t1_1623 * __this, Object_t * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return (Object_t *)__result;
}
// System.Void System.Runtime.Remoting.Messaging.MessageSurrogateFilter::.ctor(System.Object,System.IntPtr)
extern "C" void MessageSurrogateFilter__ctor_m1_14811 (MessageSurrogateFilter_t1_958 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Boolean System.Runtime.Remoting.Messaging.MessageSurrogateFilter::Invoke(System.String,System.Object)
extern "C" bool MessageSurrogateFilter_Invoke_m1_14812 (MessageSurrogateFilter_t1_958 * __this, String_t* ___key, Object_t * ___value, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		MessageSurrogateFilter_Invoke_m1_14812((MessageSurrogateFilter_t1_958 *)__this->___prev_9,___key, ___value, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Object_t *, Object_t * __this, String_t* ___key, Object_t * ___value, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___key, ___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Object_t * __this, String_t* ___key, Object_t * ___value, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___key, ___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef bool (*FunctionPointerType) (Object_t * __this, Object_t * ___value, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(___key, ___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" bool pinvoke_delegate_wrapper_MessageSurrogateFilter_t1_958(Il2CppObject* delegate, String_t* ___key, Object_t * ___value)
{
	// Marshaling of parameter '___value' to native representation
	Object_t * ____value_marshaled = { 0 };
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Object'."));
}
// System.IAsyncResult System.Runtime.Remoting.Messaging.MessageSurrogateFilter::BeginInvoke(System.String,System.Object,System.AsyncCallback,System.Object)
extern "C" Object_t * MessageSurrogateFilter_BeginInvoke_m1_14813 (MessageSurrogateFilter_t1_958 * __this, String_t* ___key, Object_t * ___value, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___key;
	__d_args[1] = ___value;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Boolean System.Runtime.Remoting.Messaging.MessageSurrogateFilter::EndInvoke(System.IAsyncResult)
extern "C" bool MessageSurrogateFilter_EndInvoke_m1_14814 (MessageSurrogateFilter_t1_958 * __this, Object_t * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Threading.ContextCallback::.ctor(System.Object,System.IntPtr)
extern "C" void ContextCallback__ctor_m1_14815 (ContextCallback_t1_1624 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void System.Threading.ContextCallback::Invoke(System.Object)
extern "C" void ContextCallback_Invoke_m1_14816 (ContextCallback_t1_1624 * __this, Object_t * ___state, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		ContextCallback_Invoke_m1_14816((ContextCallback_t1_1624 *)__this->___prev_9,___state, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, Object_t * ___state, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___state,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, Object_t * ___state, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___state,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(___state,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_ContextCallback_t1_1624(Il2CppObject* delegate, Object_t * ___state)
{
	// Marshaling of parameter '___state' to native representation
	Object_t * ____state_marshaled = { 0 };
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Object'."));
}
// System.IAsyncResult System.Threading.ContextCallback::BeginInvoke(System.Object,System.AsyncCallback,System.Object)
extern "C" Object_t * ContextCallback_BeginInvoke_m1_14817 (ContextCallback_t1_1624 * __this, Object_t * ___state, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___state;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void System.Threading.ContextCallback::EndInvoke(System.IAsyncResult)
extern "C" void ContextCallback_EndInvoke_m1_14818 (ContextCallback_t1_1624 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void System.Threading.IOCompletionCallback::.ctor(System.Object,System.IntPtr)
extern "C" void IOCompletionCallback__ctor_m1_14819 (IOCompletionCallback_t1_1625 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void System.Threading.IOCompletionCallback::Invoke(System.UInt32,System.UInt32,System.Threading.NativeOverlapped*)
extern "C" void IOCompletionCallback_Invoke_m1_14820 (IOCompletionCallback_t1_1625 * __this, uint32_t ___errorCode, uint32_t ___numBytes, NativeOverlapped_t1_1471 * ___pOVERLAP, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		IOCompletionCallback_Invoke_m1_14820((IOCompletionCallback_t1_1625 *)__this->___prev_9,___errorCode, ___numBytes, ___pOVERLAP, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, uint32_t ___errorCode, uint32_t ___numBytes, NativeOverlapped_t1_1471 * ___pOVERLAP, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___errorCode, ___numBytes, ___pOVERLAP,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, uint32_t ___errorCode, uint32_t ___numBytes, NativeOverlapped_t1_1471 * ___pOVERLAP, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___errorCode, ___numBytes, ___pOVERLAP,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_IOCompletionCallback_t1_1625(Il2CppObject* delegate, uint32_t ___errorCode, uint32_t ___numBytes, NativeOverlapped_t1_1471 * ___pOVERLAP)
{
	typedef void (STDCALL *native_function_ptr_type)(uint32_t, uint32_t, NativeOverlapped_t1_1471 *);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___errorCode' to native representation

	// Marshaling of parameter '___numBytes' to native representation

	// Marshaling of parameter '___pOVERLAP' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___errorCode, ___numBytes, ___pOVERLAP);

	// Marshaling cleanup of parameter '___errorCode' native representation

	// Marshaling cleanup of parameter '___numBytes' native representation

	// Marshaling cleanup of parameter '___pOVERLAP' native representation

}
// System.IAsyncResult System.Threading.IOCompletionCallback::BeginInvoke(System.UInt32,System.UInt32,System.Threading.NativeOverlapped*,System.AsyncCallback,System.Object)
extern TypeInfo* UInt32_t1_8_il2cpp_TypeInfo_var;
extern "C" Object_t * IOCompletionCallback_BeginInvoke_m1_14821 (IOCompletionCallback_t1_1625 * __this, uint32_t ___errorCode, uint32_t ___numBytes, NativeOverlapped_t1_1471 * ___pOVERLAP, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UInt32_t1_8_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = Box(UInt32_t1_8_il2cpp_TypeInfo_var, &___errorCode);
	__d_args[1] = Box(UInt32_t1_8_il2cpp_TypeInfo_var, &___numBytes);
	__d_args[2] = ___pOVERLAP;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void System.Threading.IOCompletionCallback::EndInvoke(System.IAsyncResult)
extern "C" void IOCompletionCallback_EndInvoke_m1_14822 (IOCompletionCallback_t1_1625 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void System.Threading.ParameterizedThreadStart::.ctor(System.Object,System.IntPtr)
extern "C" void ParameterizedThreadStart__ctor_m1_14823 (ParameterizedThreadStart_t1_1626 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void System.Threading.ParameterizedThreadStart::Invoke(System.Object)
extern "C" void ParameterizedThreadStart_Invoke_m1_14824 (ParameterizedThreadStart_t1_1626 * __this, Object_t * ___obj, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		ParameterizedThreadStart_Invoke_m1_14824((ParameterizedThreadStart_t1_1626 *)__this->___prev_9,___obj, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, Object_t * ___obj, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___obj,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, Object_t * ___obj, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___obj,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(___obj,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_ParameterizedThreadStart_t1_1626(Il2CppObject* delegate, Object_t * ___obj)
{
	// Marshaling of parameter '___obj' to native representation
	Object_t * ____obj_marshaled = { 0 };
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Object'."));
}
// System.IAsyncResult System.Threading.ParameterizedThreadStart::BeginInvoke(System.Object,System.AsyncCallback,System.Object)
extern "C" Object_t * ParameterizedThreadStart_BeginInvoke_m1_14825 (ParameterizedThreadStart_t1_1626 * __this, Object_t * ___obj, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___obj;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void System.Threading.ParameterizedThreadStart::EndInvoke(System.IAsyncResult)
extern "C" void ParameterizedThreadStart_EndInvoke_m1_14826 (ParameterizedThreadStart_t1_1626 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void System.Threading.SendOrPostCallback::.ctor(System.Object,System.IntPtr)
extern "C" void SendOrPostCallback__ctor_m1_14827 (SendOrPostCallback_t1_1627 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void System.Threading.SendOrPostCallback::Invoke(System.Object)
extern "C" void SendOrPostCallback_Invoke_m1_14828 (SendOrPostCallback_t1_1627 * __this, Object_t * ___state, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		SendOrPostCallback_Invoke_m1_14828((SendOrPostCallback_t1_1627 *)__this->___prev_9,___state, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, Object_t * ___state, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___state,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, Object_t * ___state, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___state,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(___state,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_SendOrPostCallback_t1_1627(Il2CppObject* delegate, Object_t * ___state)
{
	// Marshaling of parameter '___state' to native representation
	Object_t * ____state_marshaled = { 0 };
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Object'."));
}
// System.IAsyncResult System.Threading.SendOrPostCallback::BeginInvoke(System.Object,System.AsyncCallback,System.Object)
extern "C" Object_t * SendOrPostCallback_BeginInvoke_m1_14829 (SendOrPostCallback_t1_1627 * __this, Object_t * ___state, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___state;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void System.Threading.SendOrPostCallback::EndInvoke(System.IAsyncResult)
extern "C" void SendOrPostCallback_EndInvoke_m1_14830 (SendOrPostCallback_t1_1627 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void System.Threading.ThreadStart::.ctor(System.Object,System.IntPtr)
extern "C" void ThreadStart__ctor_m1_14831 (ThreadStart_t1_1628 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void System.Threading.ThreadStart::Invoke()
extern "C" void ThreadStart_Invoke_m1_14832 (ThreadStart_t1_1628 * __this, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		ThreadStart_Invoke_m1_14832((ThreadStart_t1_1628 *)__this->___prev_9, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if ((__this->___m_target_2 != NULL || MethodHasParameters((MethodInfo*)(__this->___method_3.___m_value_0))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_ThreadStart_t1_1628(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.IAsyncResult System.Threading.ThreadStart::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * ThreadStart_BeginInvoke_m1_14833 (ThreadStart_t1_1628 * __this, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void System.Threading.ThreadStart::EndInvoke(System.IAsyncResult)
extern "C" void ThreadStart_EndInvoke_m1_14834 (ThreadStart_t1_1628 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void System.Threading.TimerCallback::.ctor(System.Object,System.IntPtr)
extern "C" void TimerCallback__ctor_m1_14835 (TimerCallback_t1_1488 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void System.Threading.TimerCallback::Invoke(System.Object)
extern "C" void TimerCallback_Invoke_m1_14836 (TimerCallback_t1_1488 * __this, Object_t * ___state, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		TimerCallback_Invoke_m1_14836((TimerCallback_t1_1488 *)__this->___prev_9,___state, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, Object_t * ___state, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___state,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, Object_t * ___state, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___state,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(___state,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_TimerCallback_t1_1488(Il2CppObject* delegate, Object_t * ___state)
{
	// Marshaling of parameter '___state' to native representation
	Object_t * ____state_marshaled = { 0 };
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Object'."));
}
// System.IAsyncResult System.Threading.TimerCallback::BeginInvoke(System.Object,System.AsyncCallback,System.Object)
extern "C" Object_t * TimerCallback_BeginInvoke_m1_14837 (TimerCallback_t1_1488 * __this, Object_t * ___state, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___state;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void System.Threading.TimerCallback::EndInvoke(System.IAsyncResult)
extern "C" void TimerCallback_EndInvoke_m1_14838 (TimerCallback_t1_1488 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void System.Threading.WaitCallback::.ctor(System.Object,System.IntPtr)
extern "C" void WaitCallback__ctor_m1_14839 (WaitCallback_t1_1629 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void System.Threading.WaitCallback::Invoke(System.Object)
extern "C" void WaitCallback_Invoke_m1_14840 (WaitCallback_t1_1629 * __this, Object_t * ___state, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		WaitCallback_Invoke_m1_14840((WaitCallback_t1_1629 *)__this->___prev_9,___state, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, Object_t * ___state, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___state,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, Object_t * ___state, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___state,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(___state,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_WaitCallback_t1_1629(Il2CppObject* delegate, Object_t * ___state)
{
	// Marshaling of parameter '___state' to native representation
	Object_t * ____state_marshaled = { 0 };
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Object'."));
}
// System.IAsyncResult System.Threading.WaitCallback::BeginInvoke(System.Object,System.AsyncCallback,System.Object)
extern "C" Object_t * WaitCallback_BeginInvoke_m1_14841 (WaitCallback_t1_1629 * __this, Object_t * ___state, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___state;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void System.Threading.WaitCallback::EndInvoke(System.IAsyncResult)
extern "C" void WaitCallback_EndInvoke_m1_14842 (WaitCallback_t1_1629 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void System.Threading.WaitOrTimerCallback::.ctor(System.Object,System.IntPtr)
extern "C" void WaitOrTimerCallback__ctor_m1_14843 (WaitOrTimerCallback_t1_1474 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void System.Threading.WaitOrTimerCallback::Invoke(System.Object,System.Boolean)
extern "C" void WaitOrTimerCallback_Invoke_m1_14844 (WaitOrTimerCallback_t1_1474 * __this, Object_t * ___state, bool ___timedOut, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		WaitOrTimerCallback_Invoke_m1_14844((WaitOrTimerCallback_t1_1474 *)__this->___prev_9,___state, ___timedOut, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, Object_t * ___state, bool ___timedOut, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___state, ___timedOut,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, Object_t * ___state, bool ___timedOut, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___state, ___timedOut,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, bool ___timedOut, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(___state, ___timedOut,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_WaitOrTimerCallback_t1_1474(Il2CppObject* delegate, Object_t * ___state, bool ___timedOut)
{
	// Marshaling of parameter '___state' to native representation
	Object_t * ____state_marshaled = { 0 };
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Object'."));
}
// System.IAsyncResult System.Threading.WaitOrTimerCallback::BeginInvoke(System.Object,System.Boolean,System.AsyncCallback,System.Object)
extern TypeInfo* Boolean_t1_20_il2cpp_TypeInfo_var;
extern "C" Object_t * WaitOrTimerCallback_BeginInvoke_m1_14845 (WaitOrTimerCallback_t1_1474 * __this, Object_t * ___state, bool ___timedOut, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Boolean_t1_20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(54);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___state;
	__d_args[1] = Box(Boolean_t1_20_il2cpp_TypeInfo_var, &___timedOut);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void System.Threading.WaitOrTimerCallback::EndInvoke(System.IAsyncResult)
extern "C" void WaitOrTimerCallback_EndInvoke_m1_14846 (WaitOrTimerCallback_t1_1474 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void System.AppDomainInitializer::.ctor(System.Object,System.IntPtr)
extern "C" void AppDomainInitializer__ctor_m1_14847 (AppDomainInitializer_t1_1498 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void System.AppDomainInitializer::Invoke(System.String[])
extern "C" void AppDomainInitializer_Invoke_m1_14848 (AppDomainInitializer_t1_1498 * __this, StringU5BU5D_t1_238* ___args, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		AppDomainInitializer_Invoke_m1_14848((AppDomainInitializer_t1_1498 *)__this->___prev_9,___args, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, StringU5BU5D_t1_238* ___args, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___args,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, StringU5BU5D_t1_238* ___args, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___args,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(___args,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_AppDomainInitializer_t1_1498(Il2CppObject* delegate, StringU5BU5D_t1_238* ___args)
{
	typedef void (STDCALL *native_function_ptr_type)(char**);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___args' to native representation
	char** ____args_marshaled = { 0 };
	____args_marshaled = il2cpp_codegen_marshal_string_array(___args);

	// Native function invocation
	_il2cpp_pinvoke_func(____args_marshaled);

	// Marshaling cleanup of parameter '___args' native representation
	if (___args != NULL) il2cpp_codegen_marshal_free_string_array((void**)____args_marshaled, ((Il2CppCodeGenArray*)___args)->max_length);
	____args_marshaled = NULL;

}
// System.IAsyncResult System.AppDomainInitializer::BeginInvoke(System.String[],System.AsyncCallback,System.Object)
extern "C" Object_t * AppDomainInitializer_BeginInvoke_m1_14849 (AppDomainInitializer_t1_1498 * __this, StringU5BU5D_t1_238* ___args, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___args;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void System.AppDomainInitializer::EndInvoke(System.IAsyncResult)
extern "C" void AppDomainInitializer_EndInvoke_m1_14850 (AppDomainInitializer_t1_1498 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void System.AssemblyLoadEventHandler::.ctor(System.Object,System.IntPtr)
extern "C" void AssemblyLoadEventHandler__ctor_m1_14851 (AssemblyLoadEventHandler_t1_1494 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void System.AssemblyLoadEventHandler::Invoke(System.Object,System.AssemblyLoadEventArgs)
extern "C" void AssemblyLoadEventHandler_Invoke_m1_14852 (AssemblyLoadEventHandler_t1_1494 * __this, Object_t * ___sender, AssemblyLoadEventArgs_t1_1504 * ___args, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		AssemblyLoadEventHandler_Invoke_m1_14852((AssemblyLoadEventHandler_t1_1494 *)__this->___prev_9,___sender, ___args, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, Object_t * ___sender, AssemblyLoadEventArgs_t1_1504 * ___args, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___sender, ___args,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, Object_t * ___sender, AssemblyLoadEventArgs_t1_1504 * ___args, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___sender, ___args,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, AssemblyLoadEventArgs_t1_1504 * ___args, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(___sender, ___args,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_AssemblyLoadEventHandler_t1_1494(Il2CppObject* delegate, Object_t * ___sender, AssemblyLoadEventArgs_t1_1504 * ___args)
{
	// Marshaling of parameter '___sender' to native representation
	Object_t * ____sender_marshaled = { 0 };
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Object'."));
}
// System.IAsyncResult System.AssemblyLoadEventHandler::BeginInvoke(System.Object,System.AssemblyLoadEventArgs,System.AsyncCallback,System.Object)
extern "C" Object_t * AssemblyLoadEventHandler_BeginInvoke_m1_14853 (AssemblyLoadEventHandler_t1_1494 * __this, Object_t * ___sender, AssemblyLoadEventArgs_t1_1504 * ___args, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___sender;
	__d_args[1] = ___args;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void System.AssemblyLoadEventHandler::EndInvoke(System.IAsyncResult)
extern "C" void AssemblyLoadEventHandler_EndInvoke_m1_14854 (AssemblyLoadEventHandler_t1_1494 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void System.ConsoleCancelEventHandler::.ctor(System.Object,System.IntPtr)
extern "C" void ConsoleCancelEventHandler__ctor_m1_14855 (ConsoleCancelEventHandler_t1_1630 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void System.ConsoleCancelEventHandler::Invoke(System.Object,System.ConsoleCancelEventArgs)
extern "C" void ConsoleCancelEventHandler_Invoke_m1_14856 (ConsoleCancelEventHandler_t1_1630 * __this, Object_t * ___sender, ConsoleCancelEventArgs_t1_1513 * ___e, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		ConsoleCancelEventHandler_Invoke_m1_14856((ConsoleCancelEventHandler_t1_1630 *)__this->___prev_9,___sender, ___e, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, Object_t * ___sender, ConsoleCancelEventArgs_t1_1513 * ___e, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___sender, ___e,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, Object_t * ___sender, ConsoleCancelEventArgs_t1_1513 * ___e, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___sender, ___e,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, ConsoleCancelEventArgs_t1_1513 * ___e, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(___sender, ___e,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_ConsoleCancelEventHandler_t1_1630(Il2CppObject* delegate, Object_t * ___sender, ConsoleCancelEventArgs_t1_1513 * ___e)
{
	// Marshaling of parameter '___sender' to native representation
	Object_t * ____sender_marshaled = { 0 };
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Object'."));
}
// System.IAsyncResult System.ConsoleCancelEventHandler::BeginInvoke(System.Object,System.ConsoleCancelEventArgs,System.AsyncCallback,System.Object)
extern "C" Object_t * ConsoleCancelEventHandler_BeginInvoke_m1_14857 (ConsoleCancelEventHandler_t1_1630 * __this, Object_t * ___sender, ConsoleCancelEventArgs_t1_1513 * ___e, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___sender;
	__d_args[1] = ___e;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void System.ConsoleCancelEventHandler::EndInvoke(System.IAsyncResult)
extern "C" void ConsoleCancelEventHandler_EndInvoke_m1_14858 (ConsoleCancelEventHandler_t1_1630 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void System.CrossAppDomainDelegate::.ctor(System.Object,System.IntPtr)
extern "C" void CrossAppDomainDelegate__ctor_m1_14859 (CrossAppDomainDelegate_t1_1631 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void System.CrossAppDomainDelegate::Invoke()
extern "C" void CrossAppDomainDelegate_Invoke_m1_14860 (CrossAppDomainDelegate_t1_1631 * __this, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		CrossAppDomainDelegate_Invoke_m1_14860((CrossAppDomainDelegate_t1_1631 *)__this->___prev_9, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if ((__this->___m_target_2 != NULL || MethodHasParameters((MethodInfo*)(__this->___method_3.___m_value_0))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_CrossAppDomainDelegate_t1_1631(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.IAsyncResult System.CrossAppDomainDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * CrossAppDomainDelegate_BeginInvoke_m1_14861 (CrossAppDomainDelegate_t1_1631 * __this, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void System.CrossAppDomainDelegate::EndInvoke(System.IAsyncResult)
extern "C" void CrossAppDomainDelegate_EndInvoke_m1_14862 (CrossAppDomainDelegate_t1_1631 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void System.EventHandler::.ctor(System.Object,System.IntPtr)
extern "C" void EventHandler__ctor_m1_14863 (EventHandler_t1_461 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void System.EventHandler::Invoke(System.Object,System.EventArgs)
extern "C" void EventHandler_Invoke_m1_14864 (EventHandler_t1_461 * __this, Object_t * ___sender, EventArgs_t1_158 * ___e, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		EventHandler_Invoke_m1_14864((EventHandler_t1_461 *)__this->___prev_9,___sender, ___e, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, Object_t * ___sender, EventArgs_t1_158 * ___e, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___sender, ___e,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, Object_t * ___sender, EventArgs_t1_158 * ___e, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___sender, ___e,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, EventArgs_t1_158 * ___e, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(___sender, ___e,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_EventHandler_t1_461(Il2CppObject* delegate, Object_t * ___sender, EventArgs_t1_158 * ___e)
{
	// Marshaling of parameter '___sender' to native representation
	Object_t * ____sender_marshaled = { 0 };
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Object'."));
}
// System.IAsyncResult System.EventHandler::BeginInvoke(System.Object,System.EventArgs,System.AsyncCallback,System.Object)
extern "C" Object_t * EventHandler_BeginInvoke_m1_14865 (EventHandler_t1_461 * __this, Object_t * ___sender, EventArgs_t1_158 * ___e, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___sender;
	__d_args[1] = ___e;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void System.EventHandler::EndInvoke(System.IAsyncResult)
extern "C" void EventHandler_EndInvoke_m1_14866 (EventHandler_t1_461 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void System.ResolveEventHandler::.ctor(System.Object,System.IntPtr)
extern "C" void ResolveEventHandler__ctor_m1_14867 (ResolveEventHandler_t1_1495 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Reflection.Assembly System.ResolveEventHandler::Invoke(System.Object,System.ResolveEventArgs)
extern "C" Assembly_t1_467 * ResolveEventHandler_Invoke_m1_14868 (ResolveEventHandler_t1_1495 * __this, Object_t * ___sender, ResolveEventArgs_t1_1595 * ___args, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		ResolveEventHandler_Invoke_m1_14868((ResolveEventHandler_t1_1495 *)__this->___prev_9,___sender, ___args, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef Assembly_t1_467 * (*FunctionPointerType) (Object_t *, Object_t * __this, Object_t * ___sender, ResolveEventArgs_t1_1595 * ___args, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___sender, ___args,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef Assembly_t1_467 * (*FunctionPointerType) (Object_t * __this, Object_t * ___sender, ResolveEventArgs_t1_1595 * ___args, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___sender, ___args,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef Assembly_t1_467 * (*FunctionPointerType) (Object_t * __this, ResolveEventArgs_t1_1595 * ___args, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(___sender, ___args,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" Assembly_t1_467 * pinvoke_delegate_wrapper_ResolveEventHandler_t1_1495(Il2CppObject* delegate, Object_t * ___sender, ResolveEventArgs_t1_1595 * ___args)
{
	// Marshaling of parameter '___sender' to native representation
	Object_t * ____sender_marshaled = { 0 };
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Object'."));
}
// System.IAsyncResult System.ResolveEventHandler::BeginInvoke(System.Object,System.ResolveEventArgs,System.AsyncCallback,System.Object)
extern "C" Object_t * ResolveEventHandler_BeginInvoke_m1_14869 (ResolveEventHandler_t1_1495 * __this, Object_t * ___sender, ResolveEventArgs_t1_1595 * ___args, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___sender;
	__d_args[1] = ___args;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Reflection.Assembly System.ResolveEventHandler::EndInvoke(System.IAsyncResult)
extern "C" Assembly_t1_467 * ResolveEventHandler_EndInvoke_m1_14870 (ResolveEventHandler_t1_1495 * __this, Object_t * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return (Assembly_t1_467 *)__result;
}
// System.Void System.UnhandledExceptionEventHandler::.ctor(System.Object,System.IntPtr)
extern "C" void UnhandledExceptionEventHandler__ctor_m1_14871 (UnhandledExceptionEventHandler_t1_1496 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void System.UnhandledExceptionEventHandler::Invoke(System.Object,System.UnhandledExceptionEventArgs)
extern "C" void UnhandledExceptionEventHandler_Invoke_m1_14872 (UnhandledExceptionEventHandler_t1_1496 * __this, Object_t * ___sender, UnhandledExceptionEventArgs_t1_1614 * ___e, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		UnhandledExceptionEventHandler_Invoke_m1_14872((UnhandledExceptionEventHandler_t1_1496 *)__this->___prev_9,___sender, ___e, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, Object_t * ___sender, UnhandledExceptionEventArgs_t1_1614 * ___e, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___sender, ___e,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, Object_t * ___sender, UnhandledExceptionEventArgs_t1_1614 * ___e, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___sender, ___e,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, UnhandledExceptionEventArgs_t1_1614 * ___e, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(___sender, ___e,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_UnhandledExceptionEventHandler_t1_1496(Il2CppObject* delegate, Object_t * ___sender, UnhandledExceptionEventArgs_t1_1614 * ___e)
{
	// Marshaling of parameter '___sender' to native representation
	Object_t * ____sender_marshaled = { 0 };
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Object'."));
}
// System.IAsyncResult System.UnhandledExceptionEventHandler::BeginInvoke(System.Object,System.UnhandledExceptionEventArgs,System.AsyncCallback,System.Object)
extern "C" Object_t * UnhandledExceptionEventHandler_BeginInvoke_m1_14873 (UnhandledExceptionEventHandler_t1_1496 * __this, Object_t * ___sender, UnhandledExceptionEventArgs_t1_1614 * ___e, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___sender;
	__d_args[1] = ___e;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void System.UnhandledExceptionEventHandler::EndInvoke(System.IAsyncResult)
extern "C" void UnhandledExceptionEventHandler_EndInvoke_m1_14874 (UnhandledExceptionEventHandler_t1_1496 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void <PrivateImplementationDetails>::.ctor()
extern "C" void U3CPrivateImplementationDetailsU3E__ctor_m1_14875 (U3CPrivateImplementationDetailsU3E_t1_1661 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
