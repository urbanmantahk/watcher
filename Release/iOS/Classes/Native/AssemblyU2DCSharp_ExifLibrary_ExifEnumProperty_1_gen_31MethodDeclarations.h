﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExifLibrary.ExifEnumProperty`1<System.Object>
struct ExifEnumProperty_1_t8_378;
// System.Object
struct Object_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTag.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifInterOperability.h"

// System.Void ExifLibrary.ExifEnumProperty`1<System.Object>::.ctor(ExifLibrary.ExifTag,T,System.Boolean)
extern "C" void ExifEnumProperty_1__ctor_m8_2063_gshared (ExifEnumProperty_1_t8_378 * __this, int32_t ___tag, Object_t * ___value, bool ___isbitfield, const MethodInfo* method);
#define ExifEnumProperty_1__ctor_m8_2063(__this, ___tag, ___value, ___isbitfield, method) (( void (*) (ExifEnumProperty_1_t8_378 *, int32_t, Object_t *, bool, const MethodInfo*))ExifEnumProperty_1__ctor_m8_2063_gshared)(__this, ___tag, ___value, ___isbitfield, method)
// System.Void ExifLibrary.ExifEnumProperty`1<System.Object>::.ctor(ExifLibrary.ExifTag,T)
extern "C" void ExifEnumProperty_1__ctor_m8_2064_gshared (ExifEnumProperty_1_t8_378 * __this, int32_t ___tag, Object_t * ___value, const MethodInfo* method);
#define ExifEnumProperty_1__ctor_m8_2064(__this, ___tag, ___value, method) (( void (*) (ExifEnumProperty_1_t8_378 *, int32_t, Object_t *, const MethodInfo*))ExifEnumProperty_1__ctor_m8_2064_gshared)(__this, ___tag, ___value, method)
// System.Object ExifLibrary.ExifEnumProperty`1<System.Object>::get__Value()
extern "C" Object_t * ExifEnumProperty_1_get__Value_m8_2065_gshared (ExifEnumProperty_1_t8_378 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get__Value_m8_2065(__this, method) (( Object_t * (*) (ExifEnumProperty_1_t8_378 *, const MethodInfo*))ExifEnumProperty_1_get__Value_m8_2065_gshared)(__this, method)
// System.Void ExifLibrary.ExifEnumProperty`1<System.Object>::set__Value(System.Object)
extern "C" void ExifEnumProperty_1_set__Value_m8_2066_gshared (ExifEnumProperty_1_t8_378 * __this, Object_t * ___value, const MethodInfo* method);
#define ExifEnumProperty_1_set__Value_m8_2066(__this, ___value, method) (( void (*) (ExifEnumProperty_1_t8_378 *, Object_t *, const MethodInfo*))ExifEnumProperty_1_set__Value_m8_2066_gshared)(__this, ___value, method)
// T ExifLibrary.ExifEnumProperty`1<System.Object>::get_Value()
extern "C" Object_t * ExifEnumProperty_1_get_Value_m8_2067_gshared (ExifEnumProperty_1_t8_378 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get_Value_m8_2067(__this, method) (( Object_t * (*) (ExifEnumProperty_1_t8_378 *, const MethodInfo*))ExifEnumProperty_1_get_Value_m8_2067_gshared)(__this, method)
// System.Void ExifLibrary.ExifEnumProperty`1<System.Object>::set_Value(T)
extern "C" void ExifEnumProperty_1_set_Value_m8_2068_gshared (ExifEnumProperty_1_t8_378 * __this, Object_t * ___value, const MethodInfo* method);
#define ExifEnumProperty_1_set_Value_m8_2068(__this, ___value, method) (( void (*) (ExifEnumProperty_1_t8_378 *, Object_t *, const MethodInfo*))ExifEnumProperty_1_set_Value_m8_2068_gshared)(__this, ___value, method)
// System.Boolean ExifLibrary.ExifEnumProperty`1<System.Object>::get_IsBitField()
extern "C" bool ExifEnumProperty_1_get_IsBitField_m8_2069_gshared (ExifEnumProperty_1_t8_378 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get_IsBitField_m8_2069(__this, method) (( bool (*) (ExifEnumProperty_1_t8_378 *, const MethodInfo*))ExifEnumProperty_1_get_IsBitField_m8_2069_gshared)(__this, method)
// System.String ExifLibrary.ExifEnumProperty`1<System.Object>::ToString()
extern "C" String_t* ExifEnumProperty_1_ToString_m8_2070_gshared (ExifEnumProperty_1_t8_378 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_ToString_m8_2070(__this, method) (( String_t* (*) (ExifEnumProperty_1_t8_378 *, const MethodInfo*))ExifEnumProperty_1_ToString_m8_2070_gshared)(__this, method)
// ExifLibrary.ExifInterOperability ExifLibrary.ExifEnumProperty`1<System.Object>::get_Interoperability()
extern "C" ExifInterOperability_t8_113  ExifEnumProperty_1_get_Interoperability_m8_2071_gshared (ExifEnumProperty_1_t8_378 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get_Interoperability_m8_2071(__this, method) (( ExifInterOperability_t8_113  (*) (ExifEnumProperty_1_t8_378 *, const MethodInfo*))ExifEnumProperty_1_get_Interoperability_m8_2071_gshared)(__this, method)
// T ExifLibrary.ExifEnumProperty`1<System.Object>::op_Implicit(ExifLibrary.ExifEnumProperty`1<T>)
extern "C" Object_t * ExifEnumProperty_1_op_Implicit_m8_2072_gshared (Object_t * __this /* static, unused */, ExifEnumProperty_1_t8_378 * ___obj, const MethodInfo* method);
#define ExifEnumProperty_1_op_Implicit_m8_2072(__this /* static, unused */, ___obj, method) (( Object_t * (*) (Object_t * /* static, unused */, ExifEnumProperty_1_t8_378 *, const MethodInfo*))ExifEnumProperty_1_op_Implicit_m8_2072_gshared)(__this /* static, unused */, ___obj, method)
