﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Attribute.h"

// System.Runtime.InteropServices.ProgIdAttribute
struct  ProgIdAttribute_t1_816  : public Attribute_t1_2
{
	// System.String System.Runtime.InteropServices.ProgIdAttribute::pid
	String_t* ___pid_0;
};
