﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_Emit_MethodToken.h"

// System.Void System.Reflection.Emit.MethodToken::.ctor(System.Int32)
extern "C" void MethodToken__ctor_m1_6147 (MethodToken_t1_532 * __this, int32_t ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.MethodToken::.cctor()
extern "C" void MethodToken__cctor_m1_6148 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.MethodToken::Equals(System.Object)
extern "C" bool MethodToken_Equals_m1_6149 (MethodToken_t1_532 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.MethodToken::Equals(System.Reflection.Emit.MethodToken)
extern "C" bool MethodToken_Equals_m1_6150 (MethodToken_t1_532 * __this, MethodToken_t1_532  ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.MethodToken::GetHashCode()
extern "C" int32_t MethodToken_GetHashCode_m1_6151 (MethodToken_t1_532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.MethodToken::get_Token()
extern "C" int32_t MethodToken_get_Token_m1_6152 (MethodToken_t1_532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.MethodToken::op_Equality(System.Reflection.Emit.MethodToken,System.Reflection.Emit.MethodToken)
extern "C" bool MethodToken_op_Equality_m1_6153 (Object_t * __this /* static, unused */, MethodToken_t1_532  ___a, MethodToken_t1_532  ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.MethodToken::op_Inequality(System.Reflection.Emit.MethodToken,System.Reflection.Emit.MethodToken)
extern "C" bool MethodToken_op_Inequality_m1_6154 (Object_t * __this /* static, unused */, MethodToken_t1_532  ___a, MethodToken_t1_532  ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
