﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.PermissionSet/<GetEnumerator>c__Iterator1
struct U3CGetEnumeratorU3Ec__Iterator1_t1_1392;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.PermissionSet/<GetEnumerator>c__Iterator1::.ctor()
extern "C" void U3CGetEnumeratorU3Ec__Iterator1__ctor_m1_11962 (U3CGetEnumeratorU3Ec__Iterator1_t1_1392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Security.PermissionSet/<GetEnumerator>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CGetEnumeratorU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1_11963 (U3CGetEnumeratorU3Ec__Iterator1_t1_1392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Security.PermissionSet/<GetEnumerator>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CGetEnumeratorU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m1_11964 (U3CGetEnumeratorU3Ec__Iterator1_t1_1392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.PermissionSet/<GetEnumerator>c__Iterator1::MoveNext()
extern "C" bool U3CGetEnumeratorU3Ec__Iterator1_MoveNext_m1_11965 (U3CGetEnumeratorU3Ec__Iterator1_t1_1392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.PermissionSet/<GetEnumerator>c__Iterator1::Dispose()
extern "C" void U3CGetEnumeratorU3Ec__Iterator1_Dispose_m1_11966 (U3CGetEnumeratorU3Ec__Iterator1_t1_1392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.PermissionSet/<GetEnumerator>c__Iterator1::Reset()
extern "C" void U3CGetEnumeratorU3Ec__Iterator1_Reset_m1_11967 (U3CGetEnumeratorU3Ec__Iterator1_t1_1392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
