﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Resources.SatelliteContractVersionAttribute
struct SatelliteContractVersionAttribute_t1_660;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Resources.SatelliteContractVersionAttribute::.ctor(System.String)
extern "C" void SatelliteContractVersionAttribute__ctor_m1_7462 (SatelliteContractVersionAttribute_t1_660 * __this, String_t* ___version, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Resources.SatelliteContractVersionAttribute::get_Version()
extern "C" String_t* SatelliteContractVersionAttribute_get_Version_m1_7463 (SatelliteContractVersionAttribute_t1_660 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
