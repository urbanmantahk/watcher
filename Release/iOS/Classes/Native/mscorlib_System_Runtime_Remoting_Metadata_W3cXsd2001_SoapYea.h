﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String[]
struct StringU5BU5D_t1_238;

#include "mscorlib_System_Object.h"
#include "mscorlib_System_DateTime.h"

// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapYear
struct  SoapYear_t1_995  : public Object_t
{
	// System.Int32 System.Runtime.Remoting.Metadata.W3cXsd2001.SoapYear::_sign
	int32_t ____sign_1;
	// System.DateTime System.Runtime.Remoting.Metadata.W3cXsd2001.SoapYear::_value
	DateTime_t1_150  ____value_2;
};
struct SoapYear_t1_995_StaticFields{
	// System.String[] System.Runtime.Remoting.Metadata.W3cXsd2001.SoapYear::_datetimeFormats
	StringU5BU5D_t1_238* ____datetimeFormats_0;
};
