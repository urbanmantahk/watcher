﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExifLibrary.ExifDateTime
struct ExifDateTime_t8_100;
// System.Object
struct Object_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTag.h"
#include "mscorlib_System_DateTime.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifInterOperability.h"

// System.Void ExifLibrary.ExifDateTime::.ctor(ExifLibrary.ExifTag,System.DateTime)
extern "C" void ExifDateTime__ctor_m8_421 (ExifDateTime_t8_100 * __this, int32_t ___tag, DateTime_t1_150  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ExifLibrary.ExifDateTime::get__Value()
extern "C" Object_t * ExifDateTime_get__Value_m8_422 (ExifDateTime_t8_100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.ExifDateTime::set__Value(System.Object)
extern "C" void ExifDateTime_set__Value_m8_423 (ExifDateTime_t8_100 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime ExifLibrary.ExifDateTime::get_Value()
extern "C" DateTime_t1_150  ExifDateTime_get_Value_m8_424 (ExifDateTime_t8_100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.ExifDateTime::set_Value(System.DateTime)
extern "C" void ExifDateTime_set_Value_m8_425 (ExifDateTime_t8_100 * __this, DateTime_t1_150  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExifLibrary.ExifDateTime::ToString()
extern "C" String_t* ExifDateTime_ToString_m8_426 (ExifDateTime_t8_100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.ExifInterOperability ExifLibrary.ExifDateTime::get_Interoperability()
extern "C" ExifInterOperability_t8_113  ExifDateTime_get_Interoperability_m8_427 (ExifDateTime_t8_100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime ExifLibrary.ExifDateTime::op_Implicit(ExifLibrary.ExifDateTime)
extern "C" DateTime_t1_150  ExifDateTime_op_Implicit_m8_428 (Object_t * __this /* static, unused */, ExifDateTime_t8_100 * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
