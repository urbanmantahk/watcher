﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object[]
struct ObjectU5BU5D_t1_272;

#include "mscorlib_System_Object.h"

// System.Collections.Stack
struct  Stack_t1_243  : public Object_t
{
	// System.Object[] System.Collections.Stack::contents
	ObjectU5BU5D_t1_272* ___contents_1;
	// System.Int32 System.Collections.Stack::current
	int32_t ___current_2;
	// System.Int32 System.Collections.Stack::count
	int32_t ___count_3;
	// System.Int32 System.Collections.Stack::capacity
	int32_t ___capacity_4;
	// System.Int32 System.Collections.Stack::modCount
	int32_t ___modCount_5;
};
