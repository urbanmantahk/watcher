﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType.h"
#include "mscorlib_System_Reflection_Emit_ParameterToken.h"

// System.Reflection.Emit.ParameterToken
struct  ParameterToken_t1_546 
{
	// System.Int32 System.Reflection.Emit.ParameterToken::tokValue
	int32_t ___tokValue_0;
};
struct ParameterToken_t1_546_StaticFields{
	// System.Reflection.Emit.ParameterToken System.Reflection.Emit.ParameterToken::Empty
	ParameterToken_t1_546  ___Empty_1;
};
