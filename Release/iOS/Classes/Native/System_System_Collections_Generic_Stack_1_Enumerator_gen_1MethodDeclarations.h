﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Stack_1_Enumerator_genMethodDeclarations.h"

// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Type>::.ctor(System.Collections.Generic.Stack`1<T>)
#define Enumerator__ctor_m3_2007(__this, ___t, method) (( void (*) (Enumerator_t3_267 *, Stack_1_t3_247 *, const MethodInfo*))Enumerator__ctor_m3_1836_gshared)(__this, ___t, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Type>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3_2008(__this, method) (( void (*) (Enumerator_t3_267 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3_1837_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1/Enumerator<System.Type>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3_2009(__this, method) (( Object_t * (*) (Enumerator_t3_267 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3_1838_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Type>::Dispose()
#define Enumerator_Dispose_m3_2010(__this, method) (( void (*) (Enumerator_t3_267 *, const MethodInfo*))Enumerator_Dispose_m3_1839_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1/Enumerator<System.Type>::MoveNext()
#define Enumerator_MoveNext_m3_2011(__this, method) (( bool (*) (Enumerator_t3_267 *, const MethodInfo*))Enumerator_MoveNext_m3_1840_gshared)(__this, method)
// T System.Collections.Generic.Stack`1/Enumerator<System.Type>::get_Current()
#define Enumerator_get_Current_m3_2012(__this, method) (( Type_t * (*) (Enumerator_t3_267 *, const MethodInfo*))Enumerator_get_Current_m3_1841_gshared)(__this, method)
