﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Threading.AbandonedMutexException
struct AbandonedMutexException_t1_1456;
// System.String
struct String_t;
// System.Threading.WaitHandle
struct WaitHandle_t1_917;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;
// System.Exception
struct Exception_t1_33;
// System.Threading.Mutex
struct Mutex_t1_400;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.Threading.AbandonedMutexException::.ctor()
extern "C" void AbandonedMutexException__ctor_m1_12606 (AbandonedMutexException_t1_1456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.AbandonedMutexException::.ctor(System.String)
extern "C" void AbandonedMutexException__ctor_m1_12607 (AbandonedMutexException_t1_1456 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.AbandonedMutexException::.ctor(System.Int32,System.Threading.WaitHandle)
extern "C" void AbandonedMutexException__ctor_m1_12608 (AbandonedMutexException_t1_1456 * __this, int32_t ___location, WaitHandle_t1_917 * ___handle, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.AbandonedMutexException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void AbandonedMutexException__ctor_m1_12609 (AbandonedMutexException_t1_1456 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.AbandonedMutexException::.ctor(System.String,System.Exception)
extern "C" void AbandonedMutexException__ctor_m1_12610 (AbandonedMutexException_t1_1456 * __this, String_t* ___message, Exception_t1_33 * ___inner, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.AbandonedMutexException::.ctor(System.String,System.Int32,System.Threading.WaitHandle)
extern "C" void AbandonedMutexException__ctor_m1_12611 (AbandonedMutexException_t1_1456 * __this, String_t* ___message, int32_t ___location, WaitHandle_t1_917 * ___handle, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.AbandonedMutexException::.ctor(System.String,System.Exception,System.Int32,System.Threading.WaitHandle)
extern "C" void AbandonedMutexException__ctor_m1_12612 (AbandonedMutexException_t1_1456 * __this, String_t* ___message, Exception_t1_33 * ___inner, int32_t ___location, WaitHandle_t1_917 * ___handle, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.Mutex System.Threading.AbandonedMutexException::get_Mutex()
extern "C" Mutex_t1_400 * AbandonedMutexException_get_Mutex_m1_12613 (AbandonedMutexException_t1_1456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Threading.AbandonedMutexException::get_MutexIndex()
extern "C" int32_t AbandonedMutexException_get_MutexIndex_m1_12614 (AbandonedMutexException_t1_1456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
