﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Xml.XmlWriterSettings
struct XmlWriterSettings_t4_187;

#include "mscorlib_System_Object.h"

// System.Xml.XmlWriter
struct  XmlWriter_t4_182  : public Object_t
{
	// System.Xml.XmlWriterSettings System.Xml.XmlWriter::settings
	XmlWriterSettings_t4_187 * ___settings_0;
};
