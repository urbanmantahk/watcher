﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Permissions.SiteIdentityPermission
struct SiteIdentityPermission_t1_1311;
// System.String
struct String_t;
// System.Security.IPermission
struct IPermission_t1_1400;
// System.Security.SecurityElement
struct SecurityElement_t1_242;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Permissions_PermissionState.h"

// System.Void System.Security.Permissions.SiteIdentityPermission::.ctor(System.Security.Permissions.PermissionState)
extern "C" void SiteIdentityPermission__ctor_m1_11192 (SiteIdentityPermission_t1_1311 * __this, int32_t ___state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.SiteIdentityPermission::.ctor(System.String)
extern "C" void SiteIdentityPermission__ctor_m1_11193 (SiteIdentityPermission_t1_1311 * __this, String_t* ___site, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.SiteIdentityPermission::.cctor()
extern "C" void SiteIdentityPermission__cctor_m1_11194 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Permissions.SiteIdentityPermission::System.Security.Permissions.IBuiltInPermission.GetTokenIndex()
extern "C" int32_t SiteIdentityPermission_System_Security_Permissions_IBuiltInPermission_GetTokenIndex_m1_11195 (SiteIdentityPermission_t1_1311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Permissions.SiteIdentityPermission::get_Site()
extern "C" String_t* SiteIdentityPermission_get_Site_m1_11196 (SiteIdentityPermission_t1_1311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.SiteIdentityPermission::set_Site(System.String)
extern "C" void SiteIdentityPermission_set_Site_m1_11197 (SiteIdentityPermission_t1_1311 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.SiteIdentityPermission::Copy()
extern "C" Object_t * SiteIdentityPermission_Copy_m1_11198 (SiteIdentityPermission_t1_1311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.SiteIdentityPermission::FromXml(System.Security.SecurityElement)
extern "C" void SiteIdentityPermission_FromXml_m1_11199 (SiteIdentityPermission_t1_1311 * __this, SecurityElement_t1_242 * ___esd, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.SiteIdentityPermission::Intersect(System.Security.IPermission)
extern "C" Object_t * SiteIdentityPermission_Intersect_m1_11200 (SiteIdentityPermission_t1_1311 * __this, Object_t * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.SiteIdentityPermission::IsSubsetOf(System.Security.IPermission)
extern "C" bool SiteIdentityPermission_IsSubsetOf_m1_11201 (SiteIdentityPermission_t1_1311 * __this, Object_t * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityElement System.Security.Permissions.SiteIdentityPermission::ToXml()
extern "C" SecurityElement_t1_242 * SiteIdentityPermission_ToXml_m1_11202 (SiteIdentityPermission_t1_1311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.SiteIdentityPermission::Union(System.Security.IPermission)
extern "C" Object_t * SiteIdentityPermission_Union_m1_11203 (SiteIdentityPermission_t1_1311 * __this, Object_t * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.SiteIdentityPermission::IsEmpty()
extern "C" bool SiteIdentityPermission_IsEmpty_m1_11204 (SiteIdentityPermission_t1_1311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Permissions.SiteIdentityPermission System.Security.Permissions.SiteIdentityPermission::Cast(System.Security.IPermission)
extern "C" SiteIdentityPermission_t1_1311 * SiteIdentityPermission_Cast_m1_11205 (SiteIdentityPermission_t1_1311 * __this, Object_t * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.SiteIdentityPermission::IsValid(System.String)
extern "C" bool SiteIdentityPermission_IsValid_m1_11206 (SiteIdentityPermission_t1_1311 * __this, String_t* ___s, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.SiteIdentityPermission::Match(System.String)
extern "C" bool SiteIdentityPermission_Match_m1_11207 (SiteIdentityPermission_t1_1311 * __this, String_t* ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
