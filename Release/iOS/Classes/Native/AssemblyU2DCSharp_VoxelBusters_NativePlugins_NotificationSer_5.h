﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String[]
struct StringU5BU5D_t1_238;
// UnityEngine.Texture2D
struct Texture2D_t6_33;
// System.String
struct String_t;

#include "mscorlib_System_Object.h"

// VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings
struct  AndroidSettings_t8_269  : public Object_t
{
	// System.String[] VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings::m_senderIDs
	StringU5BU5D_t1_238* ___m_senderIDs_0;
	// System.Boolean VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings::m_needsBigStyle
	bool ___m_needsBigStyle_1;
	// System.Boolean VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings::m_allowVibration
	bool ___m_allowVibration_2;
	// UnityEngine.Texture2D VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings::m_whiteSmallIcon
	Texture2D_t6_33 * ___m_whiteSmallIcon_3;
	// UnityEngine.Texture2D VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings::m_colouredSmallIcon
	Texture2D_t6_33 * ___m_colouredSmallIcon_4;
	// System.String VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings::m_tickerTextKey
	String_t* ___m_tickerTextKey_5;
	// System.String VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings::m_contentTextKey
	String_t* ___m_contentTextKey_6;
	// System.String VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings::m_contentTitleKey
	String_t* ___m_contentTitleKey_7;
	// System.String VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings::m_userInfoKey
	String_t* ___m_userInfoKey_8;
	// System.String VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings::m_tagKey
	String_t* ___m_tagKey_9;
};
