﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType.h"
#include "mscorlib_System_Runtime_InteropServices_IDLFLAG.h"

// System.Runtime.InteropServices.IDLDESC
struct  IDLDESC_t1_783 
{
	// System.Int32 System.Runtime.InteropServices.IDLDESC::dwReserved
	int32_t ___dwReserved_0;
	// System.Runtime.InteropServices.IDLFLAG System.Runtime.InteropServices.IDLDESC::wIDLFlags
	int32_t ___wIDLFlags_1;
};
// Native definition for marshalling of: System.Runtime.InteropServices.IDLDESC
struct IDLDESC_t1_783_marshaled
{
	int32_t ___dwReserved_0;
	int32_t ___wIDLFlags_1;
};
