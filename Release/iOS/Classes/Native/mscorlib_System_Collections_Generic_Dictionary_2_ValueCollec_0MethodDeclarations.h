﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_9MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1_21307(__this, ___host, method) (( void (*) (Enumerator_t1_1849 *, Dictionary_2_t1_1829 *, const MethodInfo*))Enumerator__ctor_m1_16022_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_21308(__this, method) (( Object_t * (*) (Enumerator_t1_1849 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_16023_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1_21309(__this, method) (( void (*) (Enumerator_t1_1849 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_16024_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::Dispose()
#define Enumerator_Dispose_m1_21310(__this, method) (( void (*) (Enumerator_t1_1849 *, const MethodInfo*))Enumerator_Dispose_m1_16025_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::MoveNext()
#define Enumerator_MoveNext_m1_21311(__this, method) (( bool (*) (Enumerator_t1_1849 *, const MethodInfo*))Enumerator_MoveNext_m1_16026_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::get_Current()
#define Enumerator_get_Current_m1_21312(__this, method) (( GUIStyle_t6_176 * (*) (Enumerator_t1_1849 *, const MethodInfo*))Enumerator_get_Current_m1_16027_gshared)(__this, method)
