﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture2D
struct Texture2D_t6_33;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1_238;

#include "mscorlib_System_Object.h"

// VoxelBusters.NativePlugins.AddressBookContact
struct  AddressBookContact_t8_198  : public Object_t
{
	// System.String VoxelBusters.NativePlugins.AddressBookContact::m_firstName
	String_t* ___m_firstName_1;
	// System.String VoxelBusters.NativePlugins.AddressBookContact::m_lastName
	String_t* ___m_lastName_2;
	// UnityEngine.Texture2D VoxelBusters.NativePlugins.AddressBookContact::m_image
	Texture2D_t6_33 * ___m_image_3;
	// System.String VoxelBusters.NativePlugins.AddressBookContact::m_imageDownloadError
	String_t* ___m_imageDownloadError_4;
	// System.String[] VoxelBusters.NativePlugins.AddressBookContact::m_phoneNumberList
	StringU5BU5D_t1_238* ___m_phoneNumberList_5;
	// System.String[] VoxelBusters.NativePlugins.AddressBookContact::m_emailIDList
	StringU5BU5D_t1_238* ___m_emailIDList_6;
	// System.String VoxelBusters.NativePlugins.AddressBookContact::<ImagePath>k__BackingField
	String_t* ___U3CImagePathU3Ek__BackingField_7;
};
struct AddressBookContact_t8_198_StaticFields{
	// UnityEngine.Texture2D VoxelBusters.NativePlugins.AddressBookContact::defaultImage
	Texture2D_t6_33 * ___defaultImage_0;
};
