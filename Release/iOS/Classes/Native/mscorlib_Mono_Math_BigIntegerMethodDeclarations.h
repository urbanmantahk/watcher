﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Math.BigInteger
struct BigInteger_t1_139;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.UInt32[]
struct UInt32U5BU5D_t1_142;
// System.String
struct String_t;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t1_143;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_Mono_Math_BigInteger_Sign.h"

// System.Void Mono.Math.BigInteger::.ctor()
extern "C" void BigInteger__ctor_m1_1791 (BigInteger_t1_139 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Math.BigInteger::.ctor(Mono.Math.BigInteger/Sign,System.UInt32)
extern "C" void BigInteger__ctor_m1_1792 (BigInteger_t1_139 * __this, int32_t ___sign, uint32_t ___len, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Math.BigInteger::.ctor(Mono.Math.BigInteger)
extern "C" void BigInteger__ctor_m1_1793 (BigInteger_t1_139 * __this, BigInteger_t1_139 * ___bi, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Math.BigInteger::.ctor(Mono.Math.BigInteger,System.UInt32)
extern "C" void BigInteger__ctor_m1_1794 (BigInteger_t1_139 * __this, BigInteger_t1_139 * ___bi, uint32_t ___len, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Math.BigInteger::.ctor(System.Byte[])
extern "C" void BigInteger__ctor_m1_1795 (BigInteger_t1_139 * __this, ByteU5BU5D_t1_109* ___inData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Math.BigInteger::.ctor(System.UInt32[])
extern "C" void BigInteger__ctor_m1_1796 (BigInteger_t1_139 * __this, UInt32U5BU5D_t1_142* ___inData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Math.BigInteger::.ctor(System.UInt32)
extern "C" void BigInteger__ctor_m1_1797 (BigInteger_t1_139 * __this, uint32_t ___ui, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Math.BigInteger::.ctor(System.UInt64)
extern "C" void BigInteger__ctor_m1_1798 (BigInteger_t1_139 * __this, uint64_t ___ul, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Math.BigInteger::.cctor()
extern "C" void BigInteger__cctor_m1_1799 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger::Parse(System.String)
extern "C" BigInteger_t1_139 * BigInteger_Parse_m1_1800 (Object_t * __this /* static, unused */, String_t* ___number, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger::Add(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" BigInteger_t1_139 * BigInteger_Add_m1_1801 (Object_t * __this /* static, unused */, BigInteger_t1_139 * ___bi1, BigInteger_t1_139 * ___bi2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger::Subtract(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" BigInteger_t1_139 * BigInteger_Subtract_m1_1802 (Object_t * __this /* static, unused */, BigInteger_t1_139 * ___bi1, BigInteger_t1_139 * ___bi2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Math.BigInteger::Modulus(Mono.Math.BigInteger,System.Int32)
extern "C" int32_t BigInteger_Modulus_m1_1803 (Object_t * __this /* static, unused */, BigInteger_t1_139 * ___bi, int32_t ___i, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Mono.Math.BigInteger::Modulus(Mono.Math.BigInteger,System.UInt32)
extern "C" uint32_t BigInteger_Modulus_m1_1804 (Object_t * __this /* static, unused */, BigInteger_t1_139 * ___bi, uint32_t ___ui, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger::Modulus(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" BigInteger_t1_139 * BigInteger_Modulus_m1_1805 (Object_t * __this /* static, unused */, BigInteger_t1_139 * ___bi1, BigInteger_t1_139 * ___bi2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger::Divid(Mono.Math.BigInteger,System.Int32)
extern "C" BigInteger_t1_139 * BigInteger_Divid_m1_1806 (Object_t * __this /* static, unused */, BigInteger_t1_139 * ___bi, int32_t ___i, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger::Divid(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" BigInteger_t1_139 * BigInteger_Divid_m1_1807 (Object_t * __this /* static, unused */, BigInteger_t1_139 * ___bi1, BigInteger_t1_139 * ___bi2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger::Multiply(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" BigInteger_t1_139 * BigInteger_Multiply_m1_1808 (Object_t * __this /* static, unused */, BigInteger_t1_139 * ___bi1, BigInteger_t1_139 * ___bi2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger::Multiply(Mono.Math.BigInteger,System.Int32)
extern "C" BigInteger_t1_139 * BigInteger_Multiply_m1_1809 (Object_t * __this /* static, unused */, BigInteger_t1_139 * ___bi, int32_t ___i, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.RandomNumberGenerator Mono.Math.BigInteger::get_Rng()
extern "C" RandomNumberGenerator_t1_143 * BigInteger_get_Rng_m1_1810 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger::GenerateRandom(System.Int32,System.Security.Cryptography.RandomNumberGenerator)
extern "C" BigInteger_t1_139 * BigInteger_GenerateRandom_m1_1811 (Object_t * __this /* static, unused */, int32_t ___bits, RandomNumberGenerator_t1_143 * ___rng, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger::GenerateRandom(System.Int32)
extern "C" BigInteger_t1_139 * BigInteger_GenerateRandom_m1_1812 (Object_t * __this /* static, unused */, int32_t ___bits, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Math.BigInteger::Randomize(System.Security.Cryptography.RandomNumberGenerator)
extern "C" void BigInteger_Randomize_m1_1813 (BigInteger_t1_139 * __this, RandomNumberGenerator_t1_143 * ___rng, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Math.BigInteger::Randomize()
extern "C" void BigInteger_Randomize_m1_1814 (BigInteger_t1_139 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Math.BigInteger::BitCount()
extern "C" int32_t BigInteger_BitCount_m1_1815 (BigInteger_t1_139 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Math.BigInteger::TestBit(System.UInt32)
extern "C" bool BigInteger_TestBit_m1_1816 (BigInteger_t1_139 * __this, uint32_t ___bitNum, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Math.BigInteger::TestBit(System.Int32)
extern "C" bool BigInteger_TestBit_m1_1817 (BigInteger_t1_139 * __this, int32_t ___bitNum, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Math.BigInteger::SetBit(System.UInt32)
extern "C" void BigInteger_SetBit_m1_1818 (BigInteger_t1_139 * __this, uint32_t ___bitNum, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Math.BigInteger::ClearBit(System.UInt32)
extern "C" void BigInteger_ClearBit_m1_1819 (BigInteger_t1_139 * __this, uint32_t ___bitNum, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Math.BigInteger::SetBit(System.UInt32,System.Boolean)
extern "C" void BigInteger_SetBit_m1_1820 (BigInteger_t1_139 * __this, uint32_t ___bitNum, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Math.BigInteger::LowestSetBit()
extern "C" int32_t BigInteger_LowestSetBit_m1_1821 (BigInteger_t1_139 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Math.BigInteger::GetBytes()
extern "C" ByteU5BU5D_t1_109* BigInteger_GetBytes_m1_1822 (BigInteger_t1_139 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger/Sign Mono.Math.BigInteger::Compare(Mono.Math.BigInteger)
extern "C" int32_t BigInteger_Compare_m1_1823 (BigInteger_t1_139 * __this, BigInteger_t1_139 * ___bi, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Math.BigInteger::ToString(System.UInt32)
extern "C" String_t* BigInteger_ToString_m1_1824 (BigInteger_t1_139 * __this, uint32_t ___radix, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Math.BigInteger::ToString(System.UInt32,System.String)
extern "C" String_t* BigInteger_ToString_m1_1825 (BigInteger_t1_139 * __this, uint32_t ___radix, String_t* ___characterSet, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Math.BigInteger::Normalize()
extern "C" void BigInteger_Normalize_m1_1826 (BigInteger_t1_139 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Math.BigInteger::Clear()
extern "C" void BigInteger_Clear_m1_1827 (BigInteger_t1_139 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Math.BigInteger::GetHashCode()
extern "C" int32_t BigInteger_GetHashCode_m1_1828 (BigInteger_t1_139 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Math.BigInteger::ToString()
extern "C" String_t* BigInteger_ToString_m1_1829 (BigInteger_t1_139 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Math.BigInteger::Equals(System.Object)
extern "C" bool BigInteger_Equals_m1_1830 (BigInteger_t1_139 * __this, Object_t * ___o, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger::GCD(Mono.Math.BigInteger)
extern "C" BigInteger_t1_139 * BigInteger_GCD_m1_1831 (BigInteger_t1_139 * __this, BigInteger_t1_139 * ___bi, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger::ModInverse(Mono.Math.BigInteger)
extern "C" BigInteger_t1_139 * BigInteger_ModInverse_m1_1832 (BigInteger_t1_139 * __this, BigInteger_t1_139 * ___modulus, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger::ModPow(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" BigInteger_t1_139 * BigInteger_ModPow_m1_1833 (BigInteger_t1_139 * __this, BigInteger_t1_139 * ___exp, BigInteger_t1_139 * ___n, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Math.BigInteger::IsProbablePrime()
extern "C" bool BigInteger_IsProbablePrime_m1_1834 (BigInteger_t1_139 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger::NextHighestPrime(Mono.Math.BigInteger)
extern "C" BigInteger_t1_139 * BigInteger_NextHighestPrime_m1_1835 (Object_t * __this /* static, unused */, BigInteger_t1_139 * ___bi, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger::GeneratePseudoPrime(System.Int32)
extern "C" BigInteger_t1_139 * BigInteger_GeneratePseudoPrime_m1_1836 (Object_t * __this /* static, unused */, int32_t ___bits, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Math.BigInteger::Incr2()
extern "C" void BigInteger_Incr2_m1_1837 (BigInteger_t1_139 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger::op_Implicit(System.UInt32)
extern "C" BigInteger_t1_139 * BigInteger_op_Implicit_m1_1838 (Object_t * __this /* static, unused */, uint32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger::op_Implicit(System.Int32)
extern "C" BigInteger_t1_139 * BigInteger_op_Implicit_m1_1839 (Object_t * __this /* static, unused */, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger::op_Implicit(System.UInt64)
extern "C" BigInteger_t1_139 * BigInteger_op_Implicit_m1_1840 (Object_t * __this /* static, unused */, uint64_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger::op_Addition(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" BigInteger_t1_139 * BigInteger_op_Addition_m1_1841 (Object_t * __this /* static, unused */, BigInteger_t1_139 * ___bi1, BigInteger_t1_139 * ___bi2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger::op_Subtraction(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" BigInteger_t1_139 * BigInteger_op_Subtraction_m1_1842 (Object_t * __this /* static, unused */, BigInteger_t1_139 * ___bi1, BigInteger_t1_139 * ___bi2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Math.BigInteger::op_Modulus(Mono.Math.BigInteger,System.Int32)
extern "C" int32_t BigInteger_op_Modulus_m1_1843 (Object_t * __this /* static, unused */, BigInteger_t1_139 * ___bi, int32_t ___i, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Mono.Math.BigInteger::op_Modulus(Mono.Math.BigInteger,System.UInt32)
extern "C" uint32_t BigInteger_op_Modulus_m1_1844 (Object_t * __this /* static, unused */, BigInteger_t1_139 * ___bi, uint32_t ___ui, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger::op_Modulus(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" BigInteger_t1_139 * BigInteger_op_Modulus_m1_1845 (Object_t * __this /* static, unused */, BigInteger_t1_139 * ___bi1, BigInteger_t1_139 * ___bi2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger::op_Division(Mono.Math.BigInteger,System.Int32)
extern "C" BigInteger_t1_139 * BigInteger_op_Division_m1_1846 (Object_t * __this /* static, unused */, BigInteger_t1_139 * ___bi, int32_t ___i, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger::op_Division(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" BigInteger_t1_139 * BigInteger_op_Division_m1_1847 (Object_t * __this /* static, unused */, BigInteger_t1_139 * ___bi1, BigInteger_t1_139 * ___bi2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger::op_Multiply(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" BigInteger_t1_139 * BigInteger_op_Multiply_m1_1848 (Object_t * __this /* static, unused */, BigInteger_t1_139 * ___bi1, BigInteger_t1_139 * ___bi2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger::op_Multiply(Mono.Math.BigInteger,System.Int32)
extern "C" BigInteger_t1_139 * BigInteger_op_Multiply_m1_1849 (Object_t * __this /* static, unused */, BigInteger_t1_139 * ___bi, int32_t ___i, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger::op_LeftShift(Mono.Math.BigInteger,System.Int32)
extern "C" BigInteger_t1_139 * BigInteger_op_LeftShift_m1_1850 (Object_t * __this /* static, unused */, BigInteger_t1_139 * ___bi1, int32_t ___shiftVal, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger::op_RightShift(Mono.Math.BigInteger,System.Int32)
extern "C" BigInteger_t1_139 * BigInteger_op_RightShift_m1_1851 (Object_t * __this /* static, unused */, BigInteger_t1_139 * ___bi1, int32_t ___shiftVal, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Math.BigInteger::op_Equality(Mono.Math.BigInteger,System.UInt32)
extern "C" bool BigInteger_op_Equality_m1_1852 (Object_t * __this /* static, unused */, BigInteger_t1_139 * ___bi1, uint32_t ___ui, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Math.BigInteger::op_Inequality(Mono.Math.BigInteger,System.UInt32)
extern "C" bool BigInteger_op_Inequality_m1_1853 (Object_t * __this /* static, unused */, BigInteger_t1_139 * ___bi1, uint32_t ___ui, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Math.BigInteger::op_Equality(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" bool BigInteger_op_Equality_m1_1854 (Object_t * __this /* static, unused */, BigInteger_t1_139 * ___bi1, BigInteger_t1_139 * ___bi2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Math.BigInteger::op_Inequality(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" bool BigInteger_op_Inequality_m1_1855 (Object_t * __this /* static, unused */, BigInteger_t1_139 * ___bi1, BigInteger_t1_139 * ___bi2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Math.BigInteger::op_GreaterThan(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" bool BigInteger_op_GreaterThan_m1_1856 (Object_t * __this /* static, unused */, BigInteger_t1_139 * ___bi1, BigInteger_t1_139 * ___bi2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Math.BigInteger::op_LessThan(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" bool BigInteger_op_LessThan_m1_1857 (Object_t * __this /* static, unused */, BigInteger_t1_139 * ___bi1, BigInteger_t1_139 * ___bi2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Math.BigInteger::op_GreaterThanOrEqual(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" bool BigInteger_op_GreaterThanOrEqual_m1_1858 (Object_t * __this /* static, unused */, BigInteger_t1_139 * ___bi1, BigInteger_t1_139 * ___bi2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Math.BigInteger::op_LessThanOrEqual(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" bool BigInteger_op_LessThanOrEqual_m1_1859 (Object_t * __this /* static, unused */, BigInteger_t1_139 * ___bi1, BigInteger_t1_139 * ___bi2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
