﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Object_t;
// VoxelBusters.Utility.Request
struct Request_t8_155;

#include "mscorlib_System_Object.h"

// VoxelBusters.Utility.Request/<StartAsynchronousRequest>c__Iterator4
struct  U3CStartAsynchronousRequestU3Ec__Iterator4_t8_154  : public Object_t
{
	// System.Int32 VoxelBusters.Utility.Request/<StartAsynchronousRequest>c__Iterator4::$PC
	int32_t ___U24PC_0;
	// System.Object VoxelBusters.Utility.Request/<StartAsynchronousRequest>c__Iterator4::$current
	Object_t * ___U24current_1;
	// VoxelBusters.Utility.Request VoxelBusters.Utility.Request/<StartAsynchronousRequest>c__Iterator4::<>f__this
	Request_t8_155 * ___U3CU3Ef__this_2;
};
