﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Runtime.Remoting.Channels.IChannelSinkBase
struct IChannelSinkBase_t1_867;
// System.Object
struct Object_t;
// System.Runtime.Remoting.Channels.ChanelSinkStackEntry
struct ChanelSinkStackEntry_t1_876;

#include "mscorlib_System_Object.h"

// System.Runtime.Remoting.Channels.ChanelSinkStackEntry
struct  ChanelSinkStackEntry_t1_876  : public Object_t
{
	// System.Runtime.Remoting.Channels.IChannelSinkBase System.Runtime.Remoting.Channels.ChanelSinkStackEntry::Sink
	Object_t * ___Sink_0;
	// System.Object System.Runtime.Remoting.Channels.ChanelSinkStackEntry::State
	Object_t * ___State_1;
	// System.Runtime.Remoting.Channels.ChanelSinkStackEntry System.Runtime.Remoting.Channels.ChanelSinkStackEntry::Next
	ChanelSinkStackEntry_t1_876 * ___Next_2;
};
