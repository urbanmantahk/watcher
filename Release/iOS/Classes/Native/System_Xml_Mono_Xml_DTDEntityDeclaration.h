﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.ArrayList
struct ArrayList_t1_170;

#include "System_Xml_Mono_Xml_DTDEntityBase.h"

// Mono.Xml.DTDEntityDeclaration
struct  DTDEntityDeclaration_t4_98  : public DTDEntityBase_t4_97
{
	// System.String Mono.Xml.DTDEntityDeclaration::entityValue
	String_t* ___entityValue_15;
	// System.String Mono.Xml.DTDEntityDeclaration::notationName
	String_t* ___notationName_16;
	// System.Collections.ArrayList Mono.Xml.DTDEntityDeclaration::ReferencingEntities
	ArrayList_t1_170 * ___ReferencingEntities_17;
	// System.Boolean Mono.Xml.DTDEntityDeclaration::scanned
	bool ___scanned_18;
	// System.Boolean Mono.Xml.DTDEntityDeclaration::recursed
	bool ___recursed_19;
	// System.Boolean Mono.Xml.DTDEntityDeclaration::hasExternalReference
	bool ___hasExternalReference_20;
};
