﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.DataConverter/PackContext
struct PackContext_t1_251;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.DataConverter/PackContext::.ctor()
extern "C" void PackContext__ctor_m1_2720 (PackContext_t1_251 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.DataConverter/PackContext::Add(System.Byte[])
extern "C" void PackContext_Add_m1_2721 (PackContext_t1_251 * __this, ByteU5BU5D_t1_109* ___group, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.DataConverter/PackContext::Get()
extern "C" ByteU5BU5D_t1_109* PackContext_Get_m1_2722 (PackContext_t1_251 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
