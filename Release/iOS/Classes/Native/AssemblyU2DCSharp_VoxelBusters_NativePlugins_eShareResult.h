﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eShareResult.h"

// VoxelBusters.NativePlugins.eShareResult
struct  eShareResult_t8_286 
{
	// System.Int32 VoxelBusters.NativePlugins.eShareResult::value__
	int32_t ___value___1;
};
