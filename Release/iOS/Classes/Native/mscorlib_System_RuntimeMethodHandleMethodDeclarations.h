﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_RuntimeMethodHandle.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.RuntimeMethodHandle::.ctor(System.IntPtr)
extern "C" void RuntimeMethodHandle__ctor_m1_14568 (RuntimeMethodHandle_t1_479 * __this, IntPtr_t ___v, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.RuntimeMethodHandle::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void RuntimeMethodHandle__ctor_m1_14569 (RuntimeMethodHandle_t1_479 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.RuntimeMethodHandle::get_Value()
extern "C" IntPtr_t RuntimeMethodHandle_get_Value_m1_14570 (RuntimeMethodHandle_t1_479 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.RuntimeMethodHandle::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void RuntimeMethodHandle_GetObjectData_m1_14571 (RuntimeMethodHandle_t1_479 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.RuntimeMethodHandle::GetFunctionPointer(System.IntPtr)
extern "C" IntPtr_t RuntimeMethodHandle_GetFunctionPointer_m1_14572 (Object_t * __this /* static, unused */, IntPtr_t ___m, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.RuntimeMethodHandle::GetFunctionPointer()
extern "C" IntPtr_t RuntimeMethodHandle_GetFunctionPointer_m1_14573 (RuntimeMethodHandle_t1_479 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.RuntimeMethodHandle::Equals(System.Object)
extern "C" bool RuntimeMethodHandle_Equals_m1_14574 (RuntimeMethodHandle_t1_479 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.RuntimeMethodHandle::Equals(System.RuntimeMethodHandle)
extern "C" bool RuntimeMethodHandle_Equals_m1_14575 (RuntimeMethodHandle_t1_479 * __this, RuntimeMethodHandle_t1_479  ___handle, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.RuntimeMethodHandle::GetHashCode()
extern "C" int32_t RuntimeMethodHandle_GetHashCode_m1_14576 (RuntimeMethodHandle_t1_479 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.RuntimeMethodHandle::op_Equality(System.RuntimeMethodHandle,System.RuntimeMethodHandle)
extern "C" bool RuntimeMethodHandle_op_Equality_m1_14577 (Object_t * __this /* static, unused */, RuntimeMethodHandle_t1_479  ___left, RuntimeMethodHandle_t1_479  ___right, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.RuntimeMethodHandle::op_Inequality(System.RuntimeMethodHandle,System.RuntimeMethodHandle)
extern "C" bool RuntimeMethodHandle_op_Inequality_m1_14578 (Object_t * __this /* static, unused */, RuntimeMethodHandle_t1_479  ___left, RuntimeMethodHandle_t1_479  ___right, const MethodInfo* method) IL2CPP_METHOD_ATTR;
