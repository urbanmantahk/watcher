﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.Internal.iOSTwitterUser
struct iOSTwitterUser_t8_299;
// System.Collections.IDictionary
struct IDictionary_t1_35;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.Internal.iOSTwitterUser::.ctor(System.Collections.IDictionary)
extern "C" void iOSTwitterUser__ctor_m8_1742 (iOSTwitterUser_t8_299 * __this, Object_t * ____userJsonDict, const MethodInfo* method) IL2CPP_METHOD_ATTR;
