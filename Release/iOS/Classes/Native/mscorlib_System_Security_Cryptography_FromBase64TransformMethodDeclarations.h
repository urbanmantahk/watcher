﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.FromBase64Transform
struct FromBase64Transform_t1_1209;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Cryptography_FromBase64TransformMod.h"

// System.Void System.Security.Cryptography.FromBase64Transform::.ctor()
extern "C" void FromBase64Transform__ctor_m1_10327 (FromBase64Transform_t1_1209 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.FromBase64Transform::.ctor(System.Security.Cryptography.FromBase64TransformMode)
extern "C" void FromBase64Transform__ctor_m1_10328 (FromBase64Transform_t1_1209 * __this, int32_t ___whitespaces, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.FromBase64Transform::System.IDisposable.Dispose()
extern "C" void FromBase64Transform_System_IDisposable_Dispose_m1_10329 (FromBase64Transform_t1_1209 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.FromBase64Transform::Finalize()
extern "C" void FromBase64Transform_Finalize_m1_10330 (FromBase64Transform_t1_1209 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.FromBase64Transform::get_CanTransformMultipleBlocks()
extern "C" bool FromBase64Transform_get_CanTransformMultipleBlocks_m1_10331 (FromBase64Transform_t1_1209 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.FromBase64Transform::get_CanReuseTransform()
extern "C" bool FromBase64Transform_get_CanReuseTransform_m1_10332 (FromBase64Transform_t1_1209 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Cryptography.FromBase64Transform::get_InputBlockSize()
extern "C" int32_t FromBase64Transform_get_InputBlockSize_m1_10333 (FromBase64Transform_t1_1209 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Cryptography.FromBase64Transform::get_OutputBlockSize()
extern "C" int32_t FromBase64Transform_get_OutputBlockSize_m1_10334 (FromBase64Transform_t1_1209 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.FromBase64Transform::Clear()
extern "C" void FromBase64Transform_Clear_m1_10335 (FromBase64Transform_t1_1209 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.FromBase64Transform::Dispose(System.Boolean)
extern "C" void FromBase64Transform_Dispose_m1_10336 (FromBase64Transform_t1_1209 * __this, bool ___disposing, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte System.Security.Cryptography.FromBase64Transform::lookup(System.Byte)
extern "C" uint8_t FromBase64Transform_lookup_m1_10337 (FromBase64Transform_t1_1209 * __this, uint8_t ___input, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Cryptography.FromBase64Transform::ProcessBlock(System.Byte[],System.Int32)
extern "C" int32_t FromBase64Transform_ProcessBlock_m1_10338 (FromBase64Transform_t1_1209 * __this, ByteU5BU5D_t1_109* ___output, int32_t ___offset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.FromBase64Transform::CheckInputParameters(System.Byte[],System.Int32,System.Int32)
extern "C" void FromBase64Transform_CheckInputParameters_m1_10339 (FromBase64Transform_t1_1209 * __this, ByteU5BU5D_t1_109* ___inputBuffer, int32_t ___inputOffset, int32_t ___inputCount, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Cryptography.FromBase64Transform::TransformBlock(System.Byte[],System.Int32,System.Int32,System.Byte[],System.Int32)
extern "C" int32_t FromBase64Transform_TransformBlock_m1_10340 (FromBase64Transform_t1_1209 * __this, ByteU5BU5D_t1_109* ___inputBuffer, int32_t ___inputOffset, int32_t ___inputCount, ByteU5BU5D_t1_109* ___outputBuffer, int32_t ___outputOffset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.FromBase64Transform::TransformFinalBlock(System.Byte[],System.Int32,System.Int32)
extern "C" ByteU5BU5D_t1_109* FromBase64Transform_TransformFinalBlock_m1_10341 (FromBase64Transform_t1_1209 * __this, ByteU5BU5D_t1_109* ___inputBuffer, int32_t ___inputOffset, int32_t ___inputCount, const MethodInfo* method) IL2CPP_METHOD_ATTR;
