﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Collections_ArrayList_FixedSizeListWrapper.h"

// System.Collections.ArrayList/ReadOnlyListWrapper
struct  ReadOnlyListWrapper_t1_271  : public FixedSizeListWrapper_t1_270
{
};
