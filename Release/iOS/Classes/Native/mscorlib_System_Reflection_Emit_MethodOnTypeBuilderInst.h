﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Reflection.MonoGenericClass
struct MonoGenericClass_t1_485;
// System.Reflection.Emit.MethodBuilder
struct MethodBuilder_t1_501;
// System.Type[]
struct TypeU5BU5D_t1_31;
// System.Reflection.Emit.MethodOnTypeBuilderInst
struct MethodOnTypeBuilderInst_t1_530;

#include "mscorlib_System_Reflection_MethodInfo.h"

// System.Reflection.Emit.MethodOnTypeBuilderInst
struct  MethodOnTypeBuilderInst_t1_530  : public MethodInfo_t
{
	// System.Reflection.MonoGenericClass System.Reflection.Emit.MethodOnTypeBuilderInst::instantiation
	MonoGenericClass_t1_485 * ___instantiation_0;
	// System.Reflection.Emit.MethodBuilder System.Reflection.Emit.MethodOnTypeBuilderInst::mb
	MethodBuilder_t1_501 * ___mb_1;
	// System.Type[] System.Reflection.Emit.MethodOnTypeBuilderInst::method_arguments
	TypeU5BU5D_t1_31* ___method_arguments_2;
	// System.Reflection.Emit.MethodOnTypeBuilderInst System.Reflection.Emit.MethodOnTypeBuilderInst::generic_method_definition
	MethodOnTypeBuilderInst_t1_530 * ___generic_method_definition_3;
};
