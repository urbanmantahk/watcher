﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExifLibrary.JPEGSection
struct JPEGSection_t8_112;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ExifLibrary_JPEGMarker.h"

// System.Void ExifLibrary.JPEGSection::.ctor()
extern "C" void JPEGSection__ctor_m8_649 (JPEGSection_t8_112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.JPEGSection::.ctor(ExifLibrary.JPEGMarker,System.Byte[],System.Byte[])
extern "C" void JPEGSection__ctor_m8_650 (JPEGSection_t8_112 * __this, uint8_t ___marker, ByteU5BU5D_t1_109* ___data, ByteU5BU5D_t1_109* ___entropydata, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.JPEGSection::.ctor(ExifLibrary.JPEGMarker)
extern "C" void JPEGSection__ctor_m8_651 (JPEGSection_t8_112 * __this, uint8_t ___marker, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.JPEGMarker ExifLibrary.JPEGSection::get_Marker()
extern "C" uint8_t JPEGSection_get_Marker_m8_652 (JPEGSection_t8_112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.JPEGSection::set_Marker(ExifLibrary.JPEGMarker)
extern "C" void JPEGSection_set_Marker_m8_653 (JPEGSection_t8_112 * __this, uint8_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ExifLibrary.JPEGSection::get_Header()
extern "C" ByteU5BU5D_t1_109* JPEGSection_get_Header_m8_654 (JPEGSection_t8_112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.JPEGSection::set_Header(System.Byte[])
extern "C" void JPEGSection_set_Header_m8_655 (JPEGSection_t8_112 * __this, ByteU5BU5D_t1_109* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ExifLibrary.JPEGSection::get_EntropyData()
extern "C" ByteU5BU5D_t1_109* JPEGSection_get_EntropyData_m8_656 (JPEGSection_t8_112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.JPEGSection::set_EntropyData(System.Byte[])
extern "C" void JPEGSection_set_EntropyData_m8_657 (JPEGSection_t8_112 * __this, ByteU5BU5D_t1_109* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExifLibrary.JPEGSection::ToString()
extern "C" String_t* JPEGSection_ToString_m8_658 (JPEGSection_t8_112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
