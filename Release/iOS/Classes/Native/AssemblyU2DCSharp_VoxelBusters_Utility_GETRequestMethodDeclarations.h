﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.Utility.GETRequest
struct GETRequest_t8_23;
// System.Object
struct Object_t;
// UnityEngine.WWW
struct WWW_t6_78;
// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t1_247;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_URL.h"

// System.Void VoxelBusters.Utility.GETRequest::.ctor(VoxelBusters.Utility.URL,System.Object,System.Boolean)
extern "C" void GETRequest__ctor_m8_938 (GETRequest_t8_23 * __this, URL_t8_156  ____URL, Object_t * ____params, bool ____isAsynchronous, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWW VoxelBusters.Utility.GETRequest::CreateWWWObject()
extern "C" WWW_t6_78 * GETRequest_CreateWWWObject_m8_939 (GETRequest_t8_23 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.GETRequest::AppendParameters(System.String,System.Object,System.Text.StringBuilder,System.Int32&)
extern "C" void GETRequest_AppendParameters_m8_940 (GETRequest_t8_23 * __this, String_t* ____key, Object_t * ____value, StringBuilder_t1_247 * ____urlBuilder, int32_t* ____paramAdded, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.Utility.GETRequest VoxelBusters.Utility.GETRequest::CreateRequest(VoxelBusters.Utility.URL,System.Object)
extern "C" GETRequest_t8_23 * GETRequest_CreateRequest_m8_941 (Object_t * __this /* static, unused */, URL_t8_156  ____URL, Object_t * ____params, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.Utility.GETRequest VoxelBusters.Utility.GETRequest::CreateAsyncRequest(VoxelBusters.Utility.URL,System.Object)
extern "C" GETRequest_t8_23 * GETRequest_CreateAsyncRequest_m8_942 (Object_t * __this /* static, unused */, URL_t8_156  ____URL, Object_t * ____params, const MethodInfo* method) IL2CPP_METHOD_ATTR;
