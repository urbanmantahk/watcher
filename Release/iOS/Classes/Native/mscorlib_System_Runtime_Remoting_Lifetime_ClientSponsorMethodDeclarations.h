﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Lifetime.ClientSponsor
struct ClientSponsor_t1_905;
// System.Object
struct Object_t;
// System.MarshalByRefObject
struct MarshalByRefObject_t1_69;
// System.Runtime.Remoting.Lifetime.ILease
struct ILease_t1_907;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_TimeSpan.h"

// System.Void System.Runtime.Remoting.Lifetime.ClientSponsor::.ctor()
extern "C" void ClientSponsor__ctor_m1_8216 (ClientSponsor_t1_905 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Lifetime.ClientSponsor::.ctor(System.TimeSpan)
extern "C" void ClientSponsor__ctor_m1_8217 (ClientSponsor_t1_905 * __this, TimeSpan_t1_368  ___renewalTime, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.TimeSpan System.Runtime.Remoting.Lifetime.ClientSponsor::get_RenewalTime()
extern "C" TimeSpan_t1_368  ClientSponsor_get_RenewalTime_m1_8218 (ClientSponsor_t1_905 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Lifetime.ClientSponsor::set_RenewalTime(System.TimeSpan)
extern "C" void ClientSponsor_set_RenewalTime_m1_8219 (ClientSponsor_t1_905 * __this, TimeSpan_t1_368  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Lifetime.ClientSponsor::Close()
extern "C" void ClientSponsor_Close_m1_8220 (ClientSponsor_t1_905 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Lifetime.ClientSponsor::Finalize()
extern "C" void ClientSponsor_Finalize_m1_8221 (ClientSponsor_t1_905 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Lifetime.ClientSponsor::InitializeLifetimeService()
extern "C" Object_t * ClientSponsor_InitializeLifetimeService_m1_8222 (ClientSponsor_t1_905 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Remoting.Lifetime.ClientSponsor::Register(System.MarshalByRefObject)
extern "C" bool ClientSponsor_Register_m1_8223 (ClientSponsor_t1_905 * __this, MarshalByRefObject_t1_69 * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.TimeSpan System.Runtime.Remoting.Lifetime.ClientSponsor::Renewal(System.Runtime.Remoting.Lifetime.ILease)
extern "C" TimeSpan_t1_368  ClientSponsor_Renewal_m1_8224 (ClientSponsor_t1_905 * __this, Object_t * ___lease, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Lifetime.ClientSponsor::Unregister(System.MarshalByRefObject)
extern "C" void ClientSponsor_Unregister_m1_8225 (ClientSponsor_t1_905 * __this, MarshalByRefObject_t1_69 * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
