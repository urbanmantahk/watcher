﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Resources.NeutralResourcesLanguageAttribute
struct NeutralResourcesLanguageAttribute_t1_644;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Resources_UltimateResourceFallbackLocation.h"

// System.Void System.Resources.NeutralResourcesLanguageAttribute::.ctor(System.String)
extern "C" void NeutralResourcesLanguageAttribute__ctor_m1_7356 (NeutralResourcesLanguageAttribute_t1_644 * __this, String_t* ___cultureName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.NeutralResourcesLanguageAttribute::.ctor(System.String,System.Resources.UltimateResourceFallbackLocation)
extern "C" void NeutralResourcesLanguageAttribute__ctor_m1_7357 (NeutralResourcesLanguageAttribute_t1_644 * __this, String_t* ___cultureName, int32_t ___location, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Resources.NeutralResourcesLanguageAttribute::get_CultureName()
extern "C" String_t* NeutralResourcesLanguageAttribute_get_CultureName_m1_7358 (NeutralResourcesLanguageAttribute_t1_644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Resources.UltimateResourceFallbackLocation System.Resources.NeutralResourcesLanguageAttribute::get_Location()
extern "C" int32_t NeutralResourcesLanguageAttribute_get_Location_m1_7359 (NeutralResourcesLanguageAttribute_t1_644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
