﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;
// System.Runtime.CompilerServices.RuntimeHelpers/TryCode
struct TryCode_t1_71;
// System.Runtime.CompilerServices.RuntimeHelpers/CleanupCode
struct CleanupCode_t1_72;
// System.Delegate
struct Delegate_t1_22;
// System.RuntimeTypeHandle[]
struct RuntimeTypeHandleU5BU5D_t1_1672;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_RuntimeFieldHandle.h"
#include "mscorlib_System_RuntimeTypeHandle.h"
#include "mscorlib_System_RuntimeMethodHandle.h"
#include "mscorlib_System_ModuleHandle.h"

// System.Void System.Runtime.CompilerServices.RuntimeHelpers::InitializeArray(System.Array,System.IntPtr)
extern "C" void RuntimeHelpers_InitializeArray_m1_1386 (Object_t * __this /* static, unused */, Array_t * ___array, IntPtr_t ___fldHandle, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.CompilerServices.RuntimeHelpers::InitializeArray(System.Array,System.RuntimeFieldHandle)
extern "C" void RuntimeHelpers_InitializeArray_m1_1387 (Object_t * __this /* static, unused */, Array_t * ___array, RuntimeFieldHandle_t1_36  ___fldHandle, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.CompilerServices.RuntimeHelpers::get_OffsetToStringData()
extern "C" int32_t RuntimeHelpers_get_OffsetToStringData_m1_1388 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.CompilerServices.RuntimeHelpers::GetHashCode(System.Object)
extern "C" int32_t RuntimeHelpers_GetHashCode_m1_1389 (Object_t * __this /* static, unused */, Object_t * ___o, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.CompilerServices.RuntimeHelpers::Equals(System.Object,System.Object)
extern "C" bool RuntimeHelpers_Equals_m1_1390 (Object_t * __this /* static, unused */, Object_t * ___o1, Object_t * ___o2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.CompilerServices.RuntimeHelpers::GetObjectValue(System.Object)
extern "C" Object_t * RuntimeHelpers_GetObjectValue_m1_1391 (Object_t * __this /* static, unused */, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.CompilerServices.RuntimeHelpers::RunClassConstructor(System.IntPtr)
extern "C" void RuntimeHelpers_RunClassConstructor_m1_1392 (Object_t * __this /* static, unused */, IntPtr_t ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.CompilerServices.RuntimeHelpers::RunClassConstructor(System.RuntimeTypeHandle)
extern "C" void RuntimeHelpers_RunClassConstructor_m1_1393 (Object_t * __this /* static, unused */, RuntimeTypeHandle_t1_30  ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.CompilerServices.RuntimeHelpers::ExecuteCodeWithGuaranteedCleanup(System.Runtime.CompilerServices.RuntimeHelpers/TryCode,System.Runtime.CompilerServices.RuntimeHelpers/CleanupCode,System.Object)
extern "C" void RuntimeHelpers_ExecuteCodeWithGuaranteedCleanup_m1_1394 (Object_t * __this /* static, unused */, TryCode_t1_71 * ___code, CleanupCode_t1_72 * ___backoutCode, Object_t * ___userData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.CompilerServices.RuntimeHelpers::PrepareConstrainedRegions()
extern "C" void RuntimeHelpers_PrepareConstrainedRegions_m1_1395 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.CompilerServices.RuntimeHelpers::PrepareConstrainedRegionsNoOP()
extern "C" void RuntimeHelpers_PrepareConstrainedRegionsNoOP_m1_1396 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.CompilerServices.RuntimeHelpers::ProbeForSufficientStack()
extern "C" void RuntimeHelpers_ProbeForSufficientStack_m1_1397 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.CompilerServices.RuntimeHelpers::PrepareDelegate(System.Delegate)
extern "C" void RuntimeHelpers_PrepareDelegate_m1_1398 (Object_t * __this /* static, unused */, Delegate_t1_22 * ___d, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.CompilerServices.RuntimeHelpers::PrepareMethod(System.RuntimeMethodHandle)
extern "C" void RuntimeHelpers_PrepareMethod_m1_1399 (Object_t * __this /* static, unused */, RuntimeMethodHandle_t1_479  ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.CompilerServices.RuntimeHelpers::PrepareMethod(System.RuntimeMethodHandle,System.RuntimeTypeHandle[])
extern "C" void RuntimeHelpers_PrepareMethod_m1_1400 (Object_t * __this /* static, unused */, RuntimeMethodHandle_t1_479  ___method, RuntimeTypeHandleU5BU5D_t1_1672* ___instantiation, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.CompilerServices.RuntimeHelpers::RunModuleConstructor(System.ModuleHandle)
extern "C" void RuntimeHelpers_RunModuleConstructor_m1_1401 (Object_t * __this /* static, unused */, ModuleHandle_t1_1572  ___module, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.CompilerServices.RuntimeHelpers::RunModuleConstructor(System.IntPtr)
extern "C" void RuntimeHelpers_RunModuleConstructor_m1_1402 (Object_t * __this /* static, unused */, IntPtr_t ___module, const MethodInfo* method) IL2CPP_METHOD_ATTR;
