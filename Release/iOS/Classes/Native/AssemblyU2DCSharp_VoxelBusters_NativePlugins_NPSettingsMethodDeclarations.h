﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.NPSettings
struct NPSettings_t8_335;
// VoxelBusters.NativePlugins.ApplicationSettings
struct ApplicationSettings_t8_320;
// VoxelBusters.NativePlugins.UtilitySettings
struct UtilitySettings_t8_309;
// VoxelBusters.NativePlugins.NotificationServiceSettings
struct NotificationServiceSettings_t8_270;
// VoxelBusters.AssetStoreProductUtility.AssetStoreProduct
struct AssetStoreProduct_t8_22;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.NPSettings::.ctor()
extern "C" void NPSettings__ctor_m8_1933 (NPSettings_t8_335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.NativePlugins.ApplicationSettings VoxelBusters.NativePlugins.NPSettings::get_Application()
extern "C" ApplicationSettings_t8_320 * NPSettings_get_Application_m8_1934 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.NativePlugins.UtilitySettings VoxelBusters.NativePlugins.NPSettings::get_Utility()
extern "C" UtilitySettings_t8_309 * NPSettings_get_Utility_m8_1935 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.NativePlugins.NotificationServiceSettings VoxelBusters.NativePlugins.NPSettings::get_Notification()
extern "C" NotificationServiceSettings_t8_270 * NPSettings_get_Notification_m8_1936 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.AssetStoreProductUtility.AssetStoreProduct VoxelBusters.NativePlugins.NPSettings::get_AssetStoreProduct()
extern "C" AssetStoreProduct_t8_22 * NPSettings_get_AssetStoreProduct_m8_1937 (NPSettings_t8_335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NPSettings::Reset()
extern "C" void NPSettings_Reset_m8_1938 (NPSettings_t8_335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NPSettings::OnEnable()
extern "C" void NPSettings_OnEnable_m8_1939 (NPSettings_t8_335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
