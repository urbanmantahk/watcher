﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Services.EnterpriseServicesHelper
struct EnterpriseServicesHelper_t1_1006;
// System.Runtime.Remoting.Activation.IConstructionReturnMessage
struct IConstructionReturnMessage_t1_1703;
// System.Runtime.Remoting.Activation.IConstructionCallMessage
struct IConstructionCallMessage_t1_1702;
// System.MarshalByRefObject
struct MarshalByRefObject_t1_69;
// System.Runtime.Remoting.Proxies.RealProxy
struct RealProxy_t1_130;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"

// System.Void System.Runtime.Remoting.Services.EnterpriseServicesHelper::.ctor()
extern "C" void EnterpriseServicesHelper__ctor_m1_9024 (EnterpriseServicesHelper_t1_1006 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Activation.IConstructionReturnMessage System.Runtime.Remoting.Services.EnterpriseServicesHelper::CreateConstructionReturnMessage(System.Runtime.Remoting.Activation.IConstructionCallMessage,System.MarshalByRefObject)
extern "C" Object_t * EnterpriseServicesHelper_CreateConstructionReturnMessage_m1_9025 (Object_t * __this /* static, unused */, Object_t * ___ctorMsg, MarshalByRefObject_t1_69 * ___retObj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Services.EnterpriseServicesHelper::SwitchWrappers(System.Runtime.Remoting.Proxies.RealProxy,System.Runtime.Remoting.Proxies.RealProxy)
extern "C" void EnterpriseServicesHelper_SwitchWrappers_m1_9026 (Object_t * __this /* static, unused */, RealProxy_t1_130 * ___oldcp, RealProxy_t1_130 * ___newcp, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Services.EnterpriseServicesHelper::WrapIUnknownWithComObject(System.IntPtr)
extern "C" Object_t * EnterpriseServicesHelper_WrapIUnknownWithComObject_m1_9027 (Object_t * __this /* static, unused */, IntPtr_t ___punk, const MethodInfo* method) IL2CPP_METHOD_ATTR;
