﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.AccessControl.FileSecurity
struct FileSecurity_t1_1153;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_AccessControl_AccessControlSections.h"

// System.Void System.Security.AccessControl.FileSecurity::.ctor()
extern "C" void FileSecurity__ctor_m1_9868 (FileSecurity_t1_1153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.FileSecurity::.ctor(System.String,System.Security.AccessControl.AccessControlSections)
extern "C" void FileSecurity__ctor_m1_9869 (FileSecurity_t1_1153 * __this, String_t* ___fileName, int32_t ___includeSections, const MethodInfo* method) IL2CPP_METHOD_ATTR;
