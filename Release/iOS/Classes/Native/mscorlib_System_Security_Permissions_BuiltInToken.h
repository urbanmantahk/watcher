﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Security_Permissions_BuiltInToken.h"

// System.Security.Permissions.BuiltInToken
struct  BuiltInToken_t1_1282 
{
	// System.Int32 System.Security.Permissions.BuiltInToken::value__
	int32_t ___value___1;
};
