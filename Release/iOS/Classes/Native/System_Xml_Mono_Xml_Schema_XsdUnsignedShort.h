﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "System_Xml_Mono_Xml_Schema_XsdUnsignedInt.h"

// Mono.Xml.Schema.XsdUnsignedShort
struct  XsdUnsignedShort_t4_30  : public XsdUnsignedInt_t4_29
{
};
