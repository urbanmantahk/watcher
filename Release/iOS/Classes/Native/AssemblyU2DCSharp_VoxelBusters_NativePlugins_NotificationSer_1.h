﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Object_t;
// VoxelBusters.NativePlugins.NotificationService
struct NotificationService_t8_261;

#include "mscorlib_System_Object.h"

// VoxelBusters.NativePlugins.NotificationService/<ProcessAppLaunchInfo>c__Iterator5
struct  U3CProcessAppLaunchInfoU3Ec__Iterator5_t8_260  : public Object_t
{
	// System.Int32 VoxelBusters.NativePlugins.NotificationService/<ProcessAppLaunchInfo>c__Iterator5::$PC
	int32_t ___U24PC_0;
	// System.Object VoxelBusters.NativePlugins.NotificationService/<ProcessAppLaunchInfo>c__Iterator5::$current
	Object_t * ___U24current_1;
	// VoxelBusters.NativePlugins.NotificationService VoxelBusters.NativePlugins.NotificationService/<ProcessAppLaunchInfo>c__Iterator5::<>f__this
	NotificationService_t8_261 * ___U3CU3Ef__this_2;
};
