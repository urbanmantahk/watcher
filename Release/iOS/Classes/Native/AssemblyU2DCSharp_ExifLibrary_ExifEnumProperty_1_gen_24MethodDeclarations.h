﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSAltitudeRef>
struct ExifEnumProperty_1_t8_363;
// System.Object
struct Object_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTag.h"
#include "AssemblyU2DCSharp_ExifLibrary_GPSAltitudeRef.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifInterOperability.h"

// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSAltitudeRef>::.ctor(ExifLibrary.ExifTag,T,System.Boolean)
extern "C" void ExifEnumProperty_1__ctor_m8_2297_gshared (ExifEnumProperty_1_t8_363 * __this, int32_t ___tag, uint8_t ___value, bool ___isbitfield, const MethodInfo* method);
#define ExifEnumProperty_1__ctor_m8_2297(__this, ___tag, ___value, ___isbitfield, method) (( void (*) (ExifEnumProperty_1_t8_363 *, int32_t, uint8_t, bool, const MethodInfo*))ExifEnumProperty_1__ctor_m8_2297_gshared)(__this, ___tag, ___value, ___isbitfield, method)
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSAltitudeRef>::.ctor(ExifLibrary.ExifTag,T)
extern "C" void ExifEnumProperty_1__ctor_m8_2000_gshared (ExifEnumProperty_1_t8_363 * __this, int32_t ___tag, uint8_t ___value, const MethodInfo* method);
#define ExifEnumProperty_1__ctor_m8_2000(__this, ___tag, ___value, method) (( void (*) (ExifEnumProperty_1_t8_363 *, int32_t, uint8_t, const MethodInfo*))ExifEnumProperty_1__ctor_m8_2000_gshared)(__this, ___tag, ___value, method)
// System.Object ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSAltitudeRef>::get__Value()
extern "C" Object_t * ExifEnumProperty_1_get__Value_m8_2298_gshared (ExifEnumProperty_1_t8_363 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get__Value_m8_2298(__this, method) (( Object_t * (*) (ExifEnumProperty_1_t8_363 *, const MethodInfo*))ExifEnumProperty_1_get__Value_m8_2298_gshared)(__this, method)
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSAltitudeRef>::set__Value(System.Object)
extern "C" void ExifEnumProperty_1_set__Value_m8_2299_gshared (ExifEnumProperty_1_t8_363 * __this, Object_t * ___value, const MethodInfo* method);
#define ExifEnumProperty_1_set__Value_m8_2299(__this, ___value, method) (( void (*) (ExifEnumProperty_1_t8_363 *, Object_t *, const MethodInfo*))ExifEnumProperty_1_set__Value_m8_2299_gshared)(__this, ___value, method)
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSAltitudeRef>::get_Value()
extern "C" uint8_t ExifEnumProperty_1_get_Value_m8_2300_gshared (ExifEnumProperty_1_t8_363 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get_Value_m8_2300(__this, method) (( uint8_t (*) (ExifEnumProperty_1_t8_363 *, const MethodInfo*))ExifEnumProperty_1_get_Value_m8_2300_gshared)(__this, method)
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSAltitudeRef>::set_Value(T)
extern "C" void ExifEnumProperty_1_set_Value_m8_2301_gshared (ExifEnumProperty_1_t8_363 * __this, uint8_t ___value, const MethodInfo* method);
#define ExifEnumProperty_1_set_Value_m8_2301(__this, ___value, method) (( void (*) (ExifEnumProperty_1_t8_363 *, uint8_t, const MethodInfo*))ExifEnumProperty_1_set_Value_m8_2301_gshared)(__this, ___value, method)
// System.Boolean ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSAltitudeRef>::get_IsBitField()
extern "C" bool ExifEnumProperty_1_get_IsBitField_m8_2302_gshared (ExifEnumProperty_1_t8_363 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get_IsBitField_m8_2302(__this, method) (( bool (*) (ExifEnumProperty_1_t8_363 *, const MethodInfo*))ExifEnumProperty_1_get_IsBitField_m8_2302_gshared)(__this, method)
// System.String ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSAltitudeRef>::ToString()
extern "C" String_t* ExifEnumProperty_1_ToString_m8_2303_gshared (ExifEnumProperty_1_t8_363 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_ToString_m8_2303(__this, method) (( String_t* (*) (ExifEnumProperty_1_t8_363 *, const MethodInfo*))ExifEnumProperty_1_ToString_m8_2303_gshared)(__this, method)
// ExifLibrary.ExifInterOperability ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSAltitudeRef>::get_Interoperability()
extern "C" ExifInterOperability_t8_113  ExifEnumProperty_1_get_Interoperability_m8_2304_gshared (ExifEnumProperty_1_t8_363 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get_Interoperability_m8_2304(__this, method) (( ExifInterOperability_t8_113  (*) (ExifEnumProperty_1_t8_363 *, const MethodInfo*))ExifEnumProperty_1_get_Interoperability_m8_2304_gshared)(__this, method)
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSAltitudeRef>::op_Implicit(ExifLibrary.ExifEnumProperty`1<T>)
extern "C" uint8_t ExifEnumProperty_1_op_Implicit_m8_2305_gshared (Object_t * __this /* static, unused */, ExifEnumProperty_1_t8_363 * ___obj, const MethodInfo* method);
#define ExifEnumProperty_1_op_Implicit_m8_2305(__this /* static, unused */, ___obj, method) (( uint8_t (*) (Object_t * /* static, unused */, ExifEnumProperty_1_t8_363 *, const MethodInfo*))ExifEnumProperty_1_op_Implicit_m8_2305_gshared)(__this /* static, unused */, ___obj, method)
