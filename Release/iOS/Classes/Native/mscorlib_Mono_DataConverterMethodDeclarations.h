﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.DataConverter
struct DataConverter_t1_252;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Collections.IEnumerable
struct IEnumerable_t1_1677;
// Mono.DataConverter/PackContext
struct PackContext_t1_251;
// System.Object
struct Object_t;
// System.Collections.IList
struct IList_t1_262;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.DataConverter::.ctor()
extern "C" void DataConverter__ctor_m1_2757 (DataConverter_t1_252 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.DataConverter::.cctor()
extern "C" void DataConverter__cctor_m1_2758 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.DataConverter::GetBytes(System.Double)
extern "C" ByteU5BU5D_t1_109* DataConverter_GetBytes_m1_2759 (DataConverter_t1_252 * __this, double ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.DataConverter::GetBytes(System.Single)
extern "C" ByteU5BU5D_t1_109* DataConverter_GetBytes_m1_2760 (DataConverter_t1_252 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.DataConverter::GetBytes(System.Int32)
extern "C" ByteU5BU5D_t1_109* DataConverter_GetBytes_m1_2761 (DataConverter_t1_252 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.DataConverter::GetBytes(System.Int64)
extern "C" ByteU5BU5D_t1_109* DataConverter_GetBytes_m1_2762 (DataConverter_t1_252 * __this, int64_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.DataConverter::GetBytes(System.Int16)
extern "C" ByteU5BU5D_t1_109* DataConverter_GetBytes_m1_2763 (DataConverter_t1_252 * __this, int16_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.DataConverter::GetBytes(System.UInt16)
extern "C" ByteU5BU5D_t1_109* DataConverter_GetBytes_m1_2764 (DataConverter_t1_252 * __this, uint16_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.DataConverter::GetBytes(System.UInt32)
extern "C" ByteU5BU5D_t1_109* DataConverter_GetBytes_m1_2765 (DataConverter_t1_252 * __this, uint32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.DataConverter::GetBytes(System.UInt64)
extern "C" ByteU5BU5D_t1_109* DataConverter_GetBytes_m1_2766 (DataConverter_t1_252 * __this, uint64_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.DataConverter Mono.DataConverter::get_LittleEndian()
extern "C" DataConverter_t1_252 * DataConverter_get_LittleEndian_m1_2767 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.DataConverter Mono.DataConverter::get_BigEndian()
extern "C" DataConverter_t1_252 * DataConverter_get_BigEndian_m1_2768 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.DataConverter Mono.DataConverter::get_Native()
extern "C" DataConverter_t1_252 * DataConverter_get_Native_m1_2769 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.DataConverter::Align(System.Int32,System.Int32)
extern "C" int32_t DataConverter_Align_m1_2770 (Object_t * __this /* static, unused */, int32_t ___current, int32_t ___align, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.DataConverter::Pack(System.String,System.Object[])
extern "C" ByteU5BU5D_t1_109* DataConverter_Pack_m1_2771 (Object_t * __this /* static, unused */, String_t* ___description, ObjectU5BU5D_t1_272* ___args, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.DataConverter::PackEnumerable(System.String,System.Collections.IEnumerable)
extern "C" ByteU5BU5D_t1_109* DataConverter_PackEnumerable_m1_2772 (Object_t * __this /* static, unused */, String_t* ___description, Object_t * ___args, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.DataConverter::PackOne(Mono.DataConverter/PackContext,System.Object)
extern "C" bool DataConverter_PackOne_m1_2773 (Object_t * __this /* static, unused */, PackContext_t1_251 * ___b, Object_t * ___oarg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.DataConverter::Prepare(System.Byte[],System.Int32&,System.Int32,System.Boolean&)
extern "C" bool DataConverter_Prepare_m1_2774 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___buffer, int32_t* ___idx, int32_t ___size, bool* ___align, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IList Mono.DataConverter::Unpack(System.String,System.Byte[],System.Int32)
extern "C" Object_t * DataConverter_Unpack_m1_2775 (Object_t * __this /* static, unused */, String_t* ___description, ByteU5BU5D_t1_109* ___buffer, int32_t ___startIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.DataConverter::Check(System.Byte[],System.Int32,System.Int32)
extern "C" void DataConverter_Check_m1_2776 (DataConverter_t1_252 * __this, ByteU5BU5D_t1_109* ___dest, int32_t ___destIdx, int32_t ___size, const MethodInfo* method) IL2CPP_METHOD_ATTR;
