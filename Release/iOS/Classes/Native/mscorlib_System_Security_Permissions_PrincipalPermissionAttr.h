﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Security_Permissions_CodeAccessSecurityAttri.h"

// System.Security.Permissions.PrincipalPermissionAttribute
struct  PrincipalPermissionAttribute_t1_1298  : public CodeAccessSecurityAttribute_t1_1266
{
	// System.Boolean System.Security.Permissions.PrincipalPermissionAttribute::authenticated
	bool ___authenticated_2;
	// System.String System.Security.Permissions.PrincipalPermissionAttribute::name
	String_t* ___name_3;
	// System.String System.Security.Permissions.PrincipalPermissionAttribute::role
	String_t* ___role_4;
};
