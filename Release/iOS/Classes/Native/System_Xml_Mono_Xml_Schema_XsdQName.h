﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "System_Xml_Mono_Xml_Schema_XsdName.h"

// Mono.Xml.Schema.XsdQName
struct  XsdQName_t4_39  : public XsdName_t4_13
{
};
