﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Xml.Schema.XsdToken
struct XsdToken_t4_9;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlTokenizedType.h"

// System.Void Mono.Xml.Schema.XsdToken::.ctor()
extern "C" void XsdToken__ctor_m4_11 (XsdToken_t4_9 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdToken::get_TokenizedType()
extern "C" int32_t XsdToken_get_TokenizedType_m4_12 (XsdToken_t4_9 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
