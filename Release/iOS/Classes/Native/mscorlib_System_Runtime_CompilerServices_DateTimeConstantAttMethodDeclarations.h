﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.CompilerServices.DateTimeConstantAttribute
struct DateTimeConstantAttribute_t1_682;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.CompilerServices.DateTimeConstantAttribute::.ctor(System.Int64)
extern "C" void DateTimeConstantAttribute__ctor_m1_7535 (DateTimeConstantAttribute_t1_682 * __this, int64_t ___ticks, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Runtime.CompilerServices.DateTimeConstantAttribute::get_Ticks()
extern "C" int64_t DateTimeConstantAttribute_get_Ticks_m1_7536 (DateTimeConstantAttribute_t1_682 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.CompilerServices.DateTimeConstantAttribute::get_Value()
extern "C" Object_t * DateTimeConstantAttribute_get_Value_m1_7537 (DateTimeConstantAttribute_t1_682 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
