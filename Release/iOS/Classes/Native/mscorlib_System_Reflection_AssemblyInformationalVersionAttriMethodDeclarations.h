﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.AssemblyInformationalVersionAttribute
struct AssemblyInformationalVersionAttribute_t1_573;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Reflection.AssemblyInformationalVersionAttribute::.ctor(System.String)
extern "C" void AssemblyInformationalVersionAttribute__ctor_m1_6689 (AssemblyInformationalVersionAttribute_t1_573 * __this, String_t* ___informationalVersion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.AssemblyInformationalVersionAttribute::get_InformationalVersion()
extern "C" String_t* AssemblyInformationalVersionAttribute_get_InformationalVersion_m1_6690 (AssemblyInformationalVersionAttribute_t1_573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
