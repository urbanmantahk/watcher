﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>
struct Collection_1_t1_2362;
// System.Collections.Generic.IList`1<UnityEngine.UICharInfo>
struct IList_1_t1_1842;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Object
struct Object_t;
// UnityEngine.UICharInfo[]
struct UICharInfoU5BU5D_t6_288;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UICharInfo>
struct IEnumerator_1_t1_2818;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_UICharInfo.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::.ctor()
extern "C" void Collection_1__ctor_m1_20553_gshared (Collection_1_t1_2362 * __this, const MethodInfo* method);
#define Collection_1__ctor_m1_20553(__this, method) (( void (*) (Collection_1_t1_2362 *, const MethodInfo*))Collection_1__ctor_m1_20553_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1__ctor_m1_20554_gshared (Collection_1_t1_2362 * __this, Object_t* ___list, const MethodInfo* method);
#define Collection_1__ctor_m1_20554(__this, ___list, method) (( void (*) (Collection_1_t1_2362 *, Object_t*, const MethodInfo*))Collection_1__ctor_m1_20554_gshared)(__this, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_20555_gshared (Collection_1_t1_2362 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_20555(__this, method) (( bool (*) (Collection_1_t1_2362 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_20555_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m1_20556_gshared (Collection_1_t1_2362 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m1_20556(__this, ___array, ___index, method) (( void (*) (Collection_1_t1_2362 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m1_20556_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m1_20557_gshared (Collection_1_t1_2362 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m1_20557(__this, method) (( Object_t * (*) (Collection_1_t1_2362 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m1_20557_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m1_20558_gshared (Collection_1_t1_2362 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m1_20558(__this, ___value, method) (( int32_t (*) (Collection_1_t1_2362 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m1_20558_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m1_20559_gshared (Collection_1_t1_2362 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m1_20559(__this, ___value, method) (( bool (*) (Collection_1_t1_2362 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m1_20559_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m1_20560_gshared (Collection_1_t1_2362 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m1_20560(__this, ___value, method) (( int32_t (*) (Collection_1_t1_2362 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m1_20560_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m1_20561_gshared (Collection_1_t1_2362 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m1_20561(__this, ___index, ___value, method) (( void (*) (Collection_1_t1_2362 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m1_20561_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m1_20562_gshared (Collection_1_t1_2362 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m1_20562(__this, ___value, method) (( void (*) (Collection_1_t1_2362 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m1_20562_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m1_20563_gshared (Collection_1_t1_2362 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m1_20563(__this, method) (( bool (*) (Collection_1_t1_2362 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m1_20563_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m1_20564_gshared (Collection_1_t1_2362 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m1_20564(__this, method) (( Object_t * (*) (Collection_1_t1_2362 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m1_20564_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m1_20565_gshared (Collection_1_t1_2362 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m1_20565(__this, method) (( bool (*) (Collection_1_t1_2362 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m1_20565_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m1_20566_gshared (Collection_1_t1_2362 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m1_20566(__this, method) (( bool (*) (Collection_1_t1_2362 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m1_20566_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m1_20567_gshared (Collection_1_t1_2362 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m1_20567(__this, ___index, method) (( Object_t * (*) (Collection_1_t1_2362 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m1_20567_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m1_20568_gshared (Collection_1_t1_2362 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m1_20568(__this, ___index, ___value, method) (( void (*) (Collection_1_t1_2362 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m1_20568_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Add(T)
extern "C" void Collection_1_Add_m1_20569_gshared (Collection_1_t1_2362 * __this, UICharInfo_t6_150  ___item, const MethodInfo* method);
#define Collection_1_Add_m1_20569(__this, ___item, method) (( void (*) (Collection_1_t1_2362 *, UICharInfo_t6_150 , const MethodInfo*))Collection_1_Add_m1_20569_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Clear()
extern "C" void Collection_1_Clear_m1_20570_gshared (Collection_1_t1_2362 * __this, const MethodInfo* method);
#define Collection_1_Clear_m1_20570(__this, method) (( void (*) (Collection_1_t1_2362 *, const MethodInfo*))Collection_1_Clear_m1_20570_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::ClearItems()
extern "C" void Collection_1_ClearItems_m1_20571_gshared (Collection_1_t1_2362 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m1_20571(__this, method) (( void (*) (Collection_1_t1_2362 *, const MethodInfo*))Collection_1_ClearItems_m1_20571_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Contains(T)
extern "C" bool Collection_1_Contains_m1_20572_gshared (Collection_1_t1_2362 * __this, UICharInfo_t6_150  ___item, const MethodInfo* method);
#define Collection_1_Contains_m1_20572(__this, ___item, method) (( bool (*) (Collection_1_t1_2362 *, UICharInfo_t6_150 , const MethodInfo*))Collection_1_Contains_m1_20572_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m1_20573_gshared (Collection_1_t1_2362 * __this, UICharInfoU5BU5D_t6_288* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m1_20573(__this, ___array, ___index, method) (( void (*) (Collection_1_t1_2362 *, UICharInfoU5BU5D_t6_288*, int32_t, const MethodInfo*))Collection_1_CopyTo_m1_20573_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m1_20574_gshared (Collection_1_t1_2362 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m1_20574(__this, method) (( Object_t* (*) (Collection_1_t1_2362 *, const MethodInfo*))Collection_1_GetEnumerator_m1_20574_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m1_20575_gshared (Collection_1_t1_2362 * __this, UICharInfo_t6_150  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m1_20575(__this, ___item, method) (( int32_t (*) (Collection_1_t1_2362 *, UICharInfo_t6_150 , const MethodInfo*))Collection_1_IndexOf_m1_20575_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m1_20576_gshared (Collection_1_t1_2362 * __this, int32_t ___index, UICharInfo_t6_150  ___item, const MethodInfo* method);
#define Collection_1_Insert_m1_20576(__this, ___index, ___item, method) (( void (*) (Collection_1_t1_2362 *, int32_t, UICharInfo_t6_150 , const MethodInfo*))Collection_1_Insert_m1_20576_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m1_20577_gshared (Collection_1_t1_2362 * __this, int32_t ___index, UICharInfo_t6_150  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m1_20577(__this, ___index, ___item, method) (( void (*) (Collection_1_t1_2362 *, int32_t, UICharInfo_t6_150 , const MethodInfo*))Collection_1_InsertItem_m1_20577_gshared)(__this, ___index, ___item, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::get_Items()
extern "C" Object_t* Collection_1_get_Items_m1_20578_gshared (Collection_1_t1_2362 * __this, const MethodInfo* method);
#define Collection_1_get_Items_m1_20578(__this, method) (( Object_t* (*) (Collection_1_t1_2362 *, const MethodInfo*))Collection_1_get_Items_m1_20578_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Remove(T)
extern "C" bool Collection_1_Remove_m1_20579_gshared (Collection_1_t1_2362 * __this, UICharInfo_t6_150  ___item, const MethodInfo* method);
#define Collection_1_Remove_m1_20579(__this, ___item, method) (( bool (*) (Collection_1_t1_2362 *, UICharInfo_t6_150 , const MethodInfo*))Collection_1_Remove_m1_20579_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m1_20580_gshared (Collection_1_t1_2362 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m1_20580(__this, ___index, method) (( void (*) (Collection_1_t1_2362 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m1_20580_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m1_20581_gshared (Collection_1_t1_2362 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m1_20581(__this, ___index, method) (( void (*) (Collection_1_t1_2362 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m1_20581_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::get_Count()
extern "C" int32_t Collection_1_get_Count_m1_20582_gshared (Collection_1_t1_2362 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m1_20582(__this, method) (( int32_t (*) (Collection_1_t1_2362 *, const MethodInfo*))Collection_1_get_Count_m1_20582_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::get_Item(System.Int32)
extern "C" UICharInfo_t6_150  Collection_1_get_Item_m1_20583_gshared (Collection_1_t1_2362 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m1_20583(__this, ___index, method) (( UICharInfo_t6_150  (*) (Collection_1_t1_2362 *, int32_t, const MethodInfo*))Collection_1_get_Item_m1_20583_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m1_20584_gshared (Collection_1_t1_2362 * __this, int32_t ___index, UICharInfo_t6_150  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m1_20584(__this, ___index, ___value, method) (( void (*) (Collection_1_t1_2362 *, int32_t, UICharInfo_t6_150 , const MethodInfo*))Collection_1_set_Item_m1_20584_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m1_20585_gshared (Collection_1_t1_2362 * __this, int32_t ___index, UICharInfo_t6_150  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m1_20585(__this, ___index, ___item, method) (( void (*) (Collection_1_t1_2362 *, int32_t, UICharInfo_t6_150 , const MethodInfo*))Collection_1_SetItem_m1_20585_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m1_20586_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m1_20586(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m1_20586_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::ConvertItem(System.Object)
extern "C" UICharInfo_t6_150  Collection_1_ConvertItem_m1_20587_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m1_20587(__this /* static, unused */, ___item, method) (( UICharInfo_t6_150  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m1_20587_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m1_20588_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m1_20588(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m1_20588_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m1_20589_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m1_20589(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m1_20589_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m1_20590_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m1_20590(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m1_20590_gshared)(__this /* static, unused */, ___list, method)
