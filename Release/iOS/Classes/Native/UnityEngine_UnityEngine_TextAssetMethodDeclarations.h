﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.TextAsset
struct TextAsset_t6_68;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.String UnityEngine.TextAsset::get_text()
extern "C" String_t* TextAsset_get_text_m6_489 (TextAsset_t6_68 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.TextAsset::ToString()
extern "C" String_t* TextAsset_ToString_m6_490 (TextAsset_t6_68 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
