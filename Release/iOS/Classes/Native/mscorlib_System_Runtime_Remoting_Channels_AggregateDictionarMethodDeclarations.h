﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Channels.AggregateDictionary
struct AggregateDictionary_t1_860;
// System.Collections.IDictionary[]
struct IDictionaryU5BU5D_t1_861;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Object
struct Object_t;
// System.Collections.ICollection
struct ICollection_t1_280;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1_863;
// System.Array
struct Array_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Channels.AggregateDictionary::.ctor(System.Collections.IDictionary[])
extern "C" void AggregateDictionary__ctor_m1_7983 (AggregateDictionary_t1_860 * __this, IDictionaryU5BU5D_t1_861* ___dics, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Runtime.Remoting.Channels.AggregateDictionary::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * AggregateDictionary_System_Collections_IEnumerable_GetEnumerator_m1_7984 (AggregateDictionary_t1_860 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Remoting.Channels.AggregateDictionary::get_IsFixedSize()
extern "C" bool AggregateDictionary_get_IsFixedSize_m1_7985 (AggregateDictionary_t1_860 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Remoting.Channels.AggregateDictionary::get_IsReadOnly()
extern "C" bool AggregateDictionary_get_IsReadOnly_m1_7986 (AggregateDictionary_t1_860 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Channels.AggregateDictionary::get_Item(System.Object)
extern "C" Object_t * AggregateDictionary_get_Item_m1_7987 (AggregateDictionary_t1_860 * __this, Object_t * ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Channels.AggregateDictionary::set_Item(System.Object,System.Object)
extern "C" void AggregateDictionary_set_Item_m1_7988 (AggregateDictionary_t1_860 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ICollection System.Runtime.Remoting.Channels.AggregateDictionary::get_Keys()
extern "C" Object_t * AggregateDictionary_get_Keys_m1_7989 (AggregateDictionary_t1_860 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ICollection System.Runtime.Remoting.Channels.AggregateDictionary::get_Values()
extern "C" Object_t * AggregateDictionary_get_Values_m1_7990 (AggregateDictionary_t1_860 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Channels.AggregateDictionary::Add(System.Object,System.Object)
extern "C" void AggregateDictionary_Add_m1_7991 (AggregateDictionary_t1_860 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Channels.AggregateDictionary::Clear()
extern "C" void AggregateDictionary_Clear_m1_7992 (AggregateDictionary_t1_860 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Remoting.Channels.AggregateDictionary::Contains(System.Object)
extern "C" bool AggregateDictionary_Contains_m1_7993 (AggregateDictionary_t1_860 * __this, Object_t * ___ob, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IDictionaryEnumerator System.Runtime.Remoting.Channels.AggregateDictionary::GetEnumerator()
extern "C" Object_t * AggregateDictionary_GetEnumerator_m1_7994 (AggregateDictionary_t1_860 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Channels.AggregateDictionary::Remove(System.Object)
extern "C" void AggregateDictionary_Remove_m1_7995 (AggregateDictionary_t1_860 * __this, Object_t * ___ob, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Channels.AggregateDictionary::CopyTo(System.Array,System.Int32)
extern "C" void AggregateDictionary_CopyTo_m1_7996 (AggregateDictionary_t1_860 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.Remoting.Channels.AggregateDictionary::get_Count()
extern "C" int32_t AggregateDictionary_get_Count_m1_7997 (AggregateDictionary_t1_860 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Remoting.Channels.AggregateDictionary::get_IsSynchronized()
extern "C" bool AggregateDictionary_get_IsSynchronized_m1_7998 (AggregateDictionary_t1_860 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Channels.AggregateDictionary::get_SyncRoot()
extern "C" Object_t * AggregateDictionary_get_SyncRoot_m1_7999 (AggregateDictionary_t1_860 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
