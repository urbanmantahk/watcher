﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.NotificationService/RegisterForRemoteNotificationCompletion
struct RegisterForRemoteNotificationCompletion_t8_257;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_String.h"

// System.Void VoxelBusters.NativePlugins.NotificationService/RegisterForRemoteNotificationCompletion::.ctor(System.Object,System.IntPtr)
extern "C" void RegisterForRemoteNotificationCompletion__ctor_m8_1423 (RegisterForRemoteNotificationCompletion_t8_257 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationService/RegisterForRemoteNotificationCompletion::Invoke(System.String,System.String)
extern "C" void RegisterForRemoteNotificationCompletion_Invoke_m8_1424 (RegisterForRemoteNotificationCompletion_t8_257 * __this, String_t* ____deviceToken, String_t* ____error, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_RegisterForRemoteNotificationCompletion_t8_257(Il2CppObject* delegate, String_t* ____deviceToken, String_t* ____error);
// System.IAsyncResult VoxelBusters.NativePlugins.NotificationService/RegisterForRemoteNotificationCompletion::BeginInvoke(System.String,System.String,System.AsyncCallback,System.Object)
extern "C" Object_t * RegisterForRemoteNotificationCompletion_BeginInvoke_m8_1425 (RegisterForRemoteNotificationCompletion_t8_257 * __this, String_t* ____deviceToken, String_t* ____error, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationService/RegisterForRemoteNotificationCompletion::EndInvoke(System.IAsyncResult)
extern "C" void RegisterForRemoteNotificationCompletion_EndInvoke_m8_1426 (RegisterForRemoteNotificationCompletion_t8_257 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
