﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Text.StringBuilder
struct StringBuilder_t1_247;

#include "mscorlib_System_Object.h"

// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapHexBinary
struct  SoapHexBinary_t1_974  : public Object_t
{
	// System.Byte[] System.Runtime.Remoting.Metadata.W3cXsd2001.SoapHexBinary::_value
	ByteU5BU5D_t1_109* ____value_0;
	// System.Text.StringBuilder System.Runtime.Remoting.Metadata.W3cXsd2001.SoapHexBinary::sb
	StringBuilder_t1_247 * ___sb_1;
};
