﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.HMACSHA512
struct HMACSHA512_t1_1217;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Cryptography.HMACSHA512::.ctor()
extern "C" void HMACSHA512__ctor_m1_10371 (HMACSHA512_t1_1217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.HMACSHA512::.ctor(System.Byte[])
extern "C" void HMACSHA512__ctor_m1_10372 (HMACSHA512_t1_1217 * __this, ByteU5BU5D_t1_109* ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.HMACSHA512::.cctor()
extern "C" void HMACSHA512__cctor_m1_10373 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.HMACSHA512::get_ProduceLegacyHmacValues()
extern "C" bool HMACSHA512_get_ProduceLegacyHmacValues_m1_10374 (HMACSHA512_t1_1217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.HMACSHA512::set_ProduceLegacyHmacValues(System.Boolean)
extern "C" void HMACSHA512_set_ProduceLegacyHmacValues_m1_10375 (HMACSHA512_t1_1217 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
