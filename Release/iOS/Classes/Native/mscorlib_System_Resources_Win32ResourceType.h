﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Resources_Win32ResourceType.h"

// System.Resources.Win32ResourceType
struct  Win32ResourceType_t1_662 
{
	// System.Int32 System.Resources.Win32ResourceType::value__
	int32_t ___value___1;
};
