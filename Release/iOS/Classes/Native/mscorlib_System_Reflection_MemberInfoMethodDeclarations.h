﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Reflection.Module
struct Module_t1_495;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Guid.h"
#include "mscorlib_System_IntPtr.h"

// System.Void System.Reflection.MemberInfo::.ctor()
extern "C" void MemberInfo__ctor_m1_1229 (MemberInfo_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.MemberInfo::System.Runtime.InteropServices._MemberInfo.GetIDsOfNames(System.Guid&,System.IntPtr,System.UInt32,System.UInt32,System.IntPtr)
extern "C" void MemberInfo_System_Runtime_InteropServices__MemberInfo_GetIDsOfNames_m1_1230 (MemberInfo_t * __this, Guid_t1_319 * ___riid, IntPtr_t ___rgszNames, uint32_t ___cNames, uint32_t ___lcid, IntPtr_t ___rgDispId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.MemberInfo::System.Runtime.InteropServices._MemberInfo.GetTypeInfo(System.UInt32,System.UInt32,System.IntPtr)
extern "C" void MemberInfo_System_Runtime_InteropServices__MemberInfo_GetTypeInfo_m1_1231 (MemberInfo_t * __this, uint32_t ___iTInfo, uint32_t ___lcid, IntPtr_t ___ppTInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.MemberInfo::System.Runtime.InteropServices._MemberInfo.GetTypeInfoCount(System.UInt32&)
extern "C" void MemberInfo_System_Runtime_InteropServices__MemberInfo_GetTypeInfoCount_m1_1232 (MemberInfo_t * __this, uint32_t* ___pcTInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.MemberInfo::System.Runtime.InteropServices._MemberInfo.Invoke(System.UInt32,System.Guid&,System.UInt32,System.Int16,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr)
extern "C" void MemberInfo_System_Runtime_InteropServices__MemberInfo_Invoke_m1_1233 (MemberInfo_t * __this, uint32_t ___dispIdMember, Guid_t1_319 * ___riid, uint32_t ___lcid, int16_t ___wFlags, IntPtr_t ___pDispParams, IntPtr_t ___pVarResult, IntPtr_t ___pExcepInfo, IntPtr_t ___puArgErr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Module System.Reflection.MemberInfo::get_Module()
extern "C" Module_t1_495 * MemberInfo_get_Module_m1_1234 (MemberInfo_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.MemberInfo::get_MetadataToken()
extern "C" int32_t MemberInfo_get_MetadataToken_m1_1235 (MemberInfo_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.MemberInfo::System.Runtime.InteropServices._MemberInfo.GetType()
extern "C" Type_t * MemberInfo_System_Runtime_InteropServices__MemberInfo_GetType_m1_1236 (MemberInfo_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
