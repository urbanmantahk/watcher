﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType.h"
#include "mscorlib_System_Runtime_InteropServices_TYPEDESC.h"
#include "mscorlib_System_Runtime_InteropServices_ELEMDESC_DESCUNION.h"

// System.Runtime.InteropServices.ELEMDESC
struct  ELEMDESC_t1_785 
{
	// System.Runtime.InteropServices.TYPEDESC System.Runtime.InteropServices.ELEMDESC::tdesc
	TYPEDESC_t1_786  ___tdesc_0;
	// System.Runtime.InteropServices.ELEMDESC/DESCUNION System.Runtime.InteropServices.ELEMDESC::desc
	DESCUNION_t1_782  ___desc_1;
};
// Native definition for marshalling of: System.Runtime.InteropServices.ELEMDESC
struct ELEMDESC_t1_785_marshaled
{
	TYPEDESC_t1_786  ___tdesc_0;
	DESCUNION_t1_782_marshaled ___desc_1;
};
