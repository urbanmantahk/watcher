﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eWebviewControl.h"
#include "UnityEngine_UnityEngine_Rect.h"
#include "UnityEngine_UnityEngine_Color.h"

// VoxelBusters.NativePlugins.WebView
struct  WebView_t8_191  : public MonoBehaviour_t6_91
{
	// System.Boolean VoxelBusters.NativePlugins.WebView::m_canHide
	bool ___m_canHide_2;
	// System.Boolean VoxelBusters.NativePlugins.WebView::m_canBounce
	bool ___m_canBounce_3;
	// VoxelBusters.NativePlugins.eWebviewControlType VoxelBusters.NativePlugins.WebView::m_controlType
	int32_t ___m_controlType_4;
	// System.Boolean VoxelBusters.NativePlugins.WebView::m_showSpinnerOnLoad
	bool ___m_showSpinnerOnLoad_5;
	// System.Boolean VoxelBusters.NativePlugins.WebView::m_autoShowOnLoadFinish
	bool ___m_autoShowOnLoadFinish_6;
	// System.Boolean VoxelBusters.NativePlugins.WebView::m_scalesPageToFit
	bool ___m_scalesPageToFit_7;
	// UnityEngine.Rect VoxelBusters.NativePlugins.WebView::m_frame
	Rect_t6_51  ___m_frame_8;
	// UnityEngine.Color VoxelBusters.NativePlugins.WebView::m_backgroundColor
	Color_t6_40  ___m_backgroundColor_9;
	// System.String VoxelBusters.NativePlugins.WebView::<UniqueID>k__BackingField
	String_t* ___U3CUniqueIDU3Ek__BackingField_10;
};
