﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.InteropServices.TypeLibVarAttribute
struct TypeLibVarAttribute_t1_839;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_InteropServices_TypeLibVarFlags.h"

// System.Void System.Runtime.InteropServices.TypeLibVarAttribute::.ctor(System.Int16)
extern "C" void TypeLibVarAttribute__ctor_m1_7930 (TypeLibVarAttribute_t1_839 * __this, int16_t ___flags, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.TypeLibVarAttribute::.ctor(System.Runtime.InteropServices.TypeLibVarFlags)
extern "C" void TypeLibVarAttribute__ctor_m1_7931 (TypeLibVarAttribute_t1_839 * __this, int32_t ___flags, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.InteropServices.TypeLibVarFlags System.Runtime.InteropServices.TypeLibVarAttribute::get_Value()
extern "C" int32_t TypeLibVarAttribute_get_Value_m1_7932 (TypeLibVarAttribute_t1_839 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
