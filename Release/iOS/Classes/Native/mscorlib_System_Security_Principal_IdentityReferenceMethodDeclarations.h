﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Principal.IdentityReference
struct IdentityReference_t1_1120;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Principal.IdentityReference::.ctor()
extern "C" void IdentityReference__ctor_m1_11797 (IdentityReference_t1_1120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Principal.IdentityReference::op_Equality(System.Security.Principal.IdentityReference,System.Security.Principal.IdentityReference)
extern "C" bool IdentityReference_op_Equality_m1_11798 (Object_t * __this /* static, unused */, IdentityReference_t1_1120 * ___left, IdentityReference_t1_1120 * ___right, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Principal.IdentityReference::op_Inequality(System.Security.Principal.IdentityReference,System.Security.Principal.IdentityReference)
extern "C" bool IdentityReference_op_Inequality_m1_11799 (Object_t * __this /* static, unused */, IdentityReference_t1_1120 * ___left, IdentityReference_t1_1120 * ___right, const MethodInfo* method) IL2CPP_METHOD_ATTR;
