﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_1MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.UI.Graphic,System.Int32,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
#define Transform_1__ctor_m1_24304(__this, ___object, ___method, method) (( void (*) (Transform_1_t1_2551 *, Object_t *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m1_15751_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.UI.Graphic,System.Int32,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m1_24305(__this, ___key, ___value, method) (( DictionaryEntry_t1_284  (*) (Transform_1_t1_2551 *, Graphic_t7_75 *, int32_t, const MethodInfo*))Transform_1_Invoke_m1_15752_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.UI.Graphic,System.Int32,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m1_24306(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t1_2551 *, Graphic_t7_75 *, int32_t, AsyncCallback_t1_28 *, Object_t *, const MethodInfo*))Transform_1_BeginInvoke_m1_15753_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.UI.Graphic,System.Int32,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m1_24307(__this, ___result, method) (( DictionaryEntry_t1_284  (*) (Transform_1_t1_2551 *, Object_t *, const MethodInfo*))Transform_1_EndInvoke_m1_15754_gshared)(__this, ___result, method)
