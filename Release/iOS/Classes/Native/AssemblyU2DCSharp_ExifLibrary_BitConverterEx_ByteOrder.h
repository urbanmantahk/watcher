﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "AssemblyU2DCSharp_ExifLibrary_BitConverterEx_ByteOrder.h"

// ExifLibrary.BitConverterEx/ByteOrder
struct  ByteOrder_t8_60 
{
	// System.Int32 ExifLibrary.BitConverterEx/ByteOrder::value__
	int32_t ___value___1;
};
