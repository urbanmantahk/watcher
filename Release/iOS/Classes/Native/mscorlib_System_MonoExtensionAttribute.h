﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_MonoTODOAttribute.h"

// System.MonoExtensionAttribute
struct  MonoExtensionAttribute_t1_78  : public MonoTODOAttribute_t1_76
{
};
