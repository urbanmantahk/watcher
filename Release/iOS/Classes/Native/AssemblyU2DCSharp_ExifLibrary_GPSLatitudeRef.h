﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "AssemblyU2DCSharp_ExifLibrary_GPSLatitudeRef.h"

// ExifLibrary.GPSLatitudeRef
struct  GPSLatitudeRef_t8_86 
{
	// System.Byte ExifLibrary.GPSLatitudeRef::value__
	uint8_t ___value___1;
};
