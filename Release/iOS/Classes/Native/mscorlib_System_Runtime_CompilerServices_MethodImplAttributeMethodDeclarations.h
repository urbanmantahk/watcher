﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.CompilerServices.MethodImplAttribute
struct MethodImplAttribute_t1_41;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_CompilerServices_MethodImplOptions.h"

// System.Void System.Runtime.CompilerServices.MethodImplAttribute::.ctor()
extern "C" void MethodImplAttribute__ctor_m1_1291 (MethodImplAttribute_t1_41 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.CompilerServices.MethodImplAttribute::.ctor(System.Int16)
extern "C" void MethodImplAttribute__ctor_m1_1292 (MethodImplAttribute_t1_41 * __this, int16_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.CompilerServices.MethodImplAttribute::.ctor(System.Runtime.CompilerServices.MethodImplOptions)
extern "C" void MethodImplAttribute__ctor_m1_1293 (MethodImplAttribute_t1_41 * __this, int32_t ___methodImplOptions, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.CompilerServices.MethodImplOptions System.Runtime.CompilerServices.MethodImplAttribute::get_Value()
extern "C" int32_t MethodImplAttribute_get_Value_m1_1294 (MethodImplAttribute_t1_41 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
