﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "mscorlib_System_Object.h"

// System.Resources.ResourceWriter/TypeByNameObject
struct  TypeByNameObject_t1_657  : public Object_t
{
	// System.String System.Resources.ResourceWriter/TypeByNameObject::TypeName
	String_t* ___TypeName_0;
	// System.Byte[] System.Resources.ResourceWriter/TypeByNameObject::Value
	ByteU5BU5D_t1_109* ___Value_1;
};
