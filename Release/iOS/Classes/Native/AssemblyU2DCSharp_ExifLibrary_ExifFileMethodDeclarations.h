﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExifLibrary.ExifFile
struct ExifFile_t8_110;
// System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>
struct Dictionary_2_t1_1904;
// ExifLibrary.JPEGFile
struct JPEGFile_t8_111;
// ExifLibrary.ExifProperty
struct ExifProperty_t8_99;
// System.String
struct String_t;
// System.IO.MemoryStream
struct MemoryStream_t1_433;
// System.IO.Stream
struct Stream_t1_405;
// ExifLibrary.JPEGSection
struct JPEGSection_t8_112;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ExifLibrary_BitConverterEx_ByteOrder.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTag.h"
#include "AssemblyU2DCSharp_ExifLibrary_IFD.h"

// System.Void ExifLibrary.ExifFile::.ctor()
extern "C" void ExifFile__ctor_m8_478 (ExifFile_t8_110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,ExifLibrary.ExifProperty> ExifLibrary.ExifFile::get_Properties()
extern "C" Dictionary_2_t1_1904 * ExifFile_get_Properties_m8_479 (ExifFile_t8_110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.ExifFile::set_Properties(System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>)
extern "C" void ExifFile_set_Properties_m8_480 (ExifFile_t8_110 * __this, Dictionary_2_t1_1904 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.BitConverterEx/ByteOrder ExifLibrary.ExifFile::get_ByteOrder()
extern "C" int32_t ExifFile_get_ByteOrder_m8_481 (ExifFile_t8_110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.ExifFile::set_ByteOrder(ExifLibrary.BitConverterEx/ByteOrder)
extern "C" void ExifFile_set_ByteOrder_m8_482 (ExifFile_t8_110 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.JPEGFile ExifLibrary.ExifFile::get_Thumbnail()
extern "C" JPEGFile_t8_111 * ExifFile_get_Thumbnail_m8_483 (ExifFile_t8_110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.ExifFile::set_Thumbnail(ExifLibrary.JPEGFile)
extern "C" void ExifFile_set_Thumbnail_m8_484 (ExifFile_t8_110 * __this, JPEGFile_t8_111 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.ExifProperty ExifLibrary.ExifFile::get_Item(ExifLibrary.ExifTag)
extern "C" ExifProperty_t8_99 * ExifFile_get_Item_m8_485 (ExifFile_t8_110 * __this, int32_t ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.ExifFile::set_Item(ExifLibrary.ExifTag,ExifLibrary.ExifProperty)
extern "C" void ExifFile_set_Item_m8_486 (ExifFile_t8_110 * __this, int32_t ___key, ExifProperty_t8_99 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.ExifFile::Save(System.String)
extern "C" void ExifFile_Save_m8_487 (ExifFile_t8_110 * __this, String_t* ___filename, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.ExifFile::Save(System.String,System.Boolean)
extern "C" void ExifFile_Save_m8_488 (ExifFile_t8_110 * __this, String_t* ___filename, bool ___preserveMakerNote, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.ExifFile::ReadAPP1()
extern "C" void ExifFile_ReadAPP1_m8_489 (ExifFile_t8_110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.ExifFile::WriteApp1(System.Boolean)
extern "C" void ExifFile_WriteApp1_m8_490 (ExifFile_t8_110 * __this, bool ___preserveMakerNote, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.ExifFile::WriteIFD(System.IO.MemoryStream,System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>,ExifLibrary.IFD,System.Int64,System.Boolean)
extern "C" void ExifFile_WriteIFD_m8_491 (ExifFile_t8_110 * __this, MemoryStream_t1_433 * ___stream, Dictionary_2_t1_1904 * ___ifd, int32_t ___ifdtype, int64_t ___tiffoffset, bool ___preserveMakerNote, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.ExifFile ExifLibrary.ExifFile::Read(System.String)
extern "C" ExifFile_t8_110 * ExifFile_Read_m8_492 (Object_t * __this /* static, unused */, String_t* ___filename, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.ExifFile ExifLibrary.ExifFile::Read(System.IO.Stream)
extern "C" ExifFile_t8_110 * ExifFile_Read_m8_493 (Object_t * __this /* static, unused */, Stream_t1_405 * ___fileStream, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExifLibrary.ExifFile::<ReadAPP1>m__2(ExifLibrary.JPEGSection)
extern "C" bool ExifFile_U3CReadAPP1U3Em__2_m8_494 (Object_t * __this /* static, unused */, JPEGSection_t8_112 * ___a, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExifLibrary.ExifFile::<ReadAPP1>m__3(ExifLibrary.JPEGSection)
extern "C" bool ExifFile_U3CReadAPP1U3Em__3_m8_495 (Object_t * __this /* static, unused */, JPEGSection_t8_112 * ___a, const MethodInfo* method) IL2CPP_METHOD_ATTR;
