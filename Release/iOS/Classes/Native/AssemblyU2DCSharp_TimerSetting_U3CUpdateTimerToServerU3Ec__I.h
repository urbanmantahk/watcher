﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WWWForm
struct WWWForm_t6_79;
// UnityEngine.WWW
struct WWW_t6_78;
// Boomlagoon.JSON.JSONObject
struct JSONObject_t8_5;
// System.Action
struct Action_t5_11;
// System.Object
struct Object_t;
// TimerSetting
struct TimerSetting_t8_13;

#include "mscorlib_System_Object.h"

// TimerSetting/<UpdateTimerToServer>c__Iterator2
struct  U3CUpdateTimerToServerU3Ec__Iterator2_t8_12  : public Object_t
{
	// UnityEngine.WWWForm TimerSetting/<UpdateTimerToServer>c__Iterator2::<_f>__0
	WWWForm_t6_79 * ___U3C_fU3E__0_0;
	// UnityEngine.WWW TimerSetting/<UpdateTimerToServer>c__Iterator2::<_w>__1
	WWW_t6_78 * ___U3C_wU3E__1_1;
	// System.Single TimerSetting/<UpdateTimerToServer>c__Iterator2::<_elapse>__2
	float ___U3C_elapseU3E__2_2;
	// System.Single TimerSetting/<UpdateTimerToServer>c__Iterator2::<_time>__3
	float ___U3C_timeU3E__3_3;
	// System.Single TimerSetting/<UpdateTimerToServer>c__Iterator2::<_timeout>__4
	float ___U3C_timeoutU3E__4_4;
	// Boomlagoon.JSON.JSONObject TimerSetting/<UpdateTimerToServer>c__Iterator2::<_json>__5
	JSONObject_t8_5 * ___U3C_jsonU3E__5_5;
	// System.Action TimerSetting/<UpdateTimerToServer>c__Iterator2::_action
	Action_t5_11 * ____action_6;
	// System.Int32 TimerSetting/<UpdateTimerToServer>c__Iterator2::$PC
	int32_t ___U24PC_7;
	// System.Object TimerSetting/<UpdateTimerToServer>c__Iterator2::$current
	Object_t * ___U24current_8;
	// System.Action TimerSetting/<UpdateTimerToServer>c__Iterator2::<$>_action
	Action_t5_11 * ___U3CU24U3E_action_9;
	// TimerSetting TimerSetting/<UpdateTimerToServer>c__Iterator2::<>f__this
	TimerSetting_t8_13 * ___U3CU3Ef__this_10;
};
