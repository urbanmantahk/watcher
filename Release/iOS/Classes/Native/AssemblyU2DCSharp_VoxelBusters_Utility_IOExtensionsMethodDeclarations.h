﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.Uri
struct Uri_t3_3;
// System.IO.DirectoryInfo
struct DirectoryInfo_t1_399;
// System.IO.FileInfo
struct FileInfo_t1_422;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_FileAttributes.h"

// System.String VoxelBusters.Utility.IOExtensions::MakeRelativePath(System.String,System.String)
extern "C" String_t* IOExtensions_MakeRelativePath_m8_201 (Object_t * __this /* static, unused */, String_t* ____fromPath, String_t* ____toPath, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.Utility.IOExtensions::MakeRelativePath(System.Uri,System.String)
extern "C" String_t* IOExtensions_MakeRelativePath_m8_202 (Object_t * __this /* static, unused */, Uri_t3_3 * ____fromUri, String_t* ____toPath, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.Utility.IOExtensions::AssignPermissionRecursively(System.String,System.IO.FileAttributes)
extern "C" bool IOExtensions_AssignPermissionRecursively_m8_203 (Object_t * __this /* static, unused */, String_t* ____directoryPath, int32_t ____attribute, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.Utility.IOExtensions::AssignPermissionRecursively(System.IO.DirectoryInfo,System.IO.FileAttributes)
extern "C" bool IOExtensions_AssignPermissionRecursively_m8_204 (Object_t * __this /* static, unused */, DirectoryInfo_t1_399 * ____directoryInfo, int32_t ____attribute, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.IOExtensions::CopyFilesRecursively(System.String,System.String,System.Boolean,System.Boolean)
extern "C" void IOExtensions_CopyFilesRecursively_m8_205 (Object_t * __this /* static, unused */, String_t* ____sourceDirectory, String_t* ____destinationDirectory, bool ____excludeMetaFiles, bool ____deleteDestinationFolderIfExists, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.IOExtensions::CopyFilesRecursively(System.IO.DirectoryInfo,System.IO.DirectoryInfo,System.Boolean,System.Boolean)
extern "C" void IOExtensions_CopyFilesRecursively_m8_206 (Object_t * __this /* static, unused */, DirectoryInfo_t1_399 * ____sourceDirectoryInfo, DirectoryInfo_t1_399 * ____destinationDirectoryInfo, bool ____excludeMetaFiles, bool ____deleteDestinationFolderIfExists, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.IOExtensions::CopyFile(System.IO.FileInfo,System.String)
extern "C" void IOExtensions_CopyFile_m8_207 (Object_t * __this /* static, unused */, FileInfo_t1_422 * ____sourceFileInfo, String_t* ____destinationFolderPath, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.IOExtensions::CopyFile(System.IO.FileInfo,System.String,System.String)
extern "C" void IOExtensions_CopyFile_m8_208 (Object_t * __this /* static, unused */, FileInfo_t1_422 * ____sourceFileInfo, String_t* ____destinationFolderPath, String_t* ____destinationFileName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
