﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Permissions.IsolatedStoragePermission
struct IsolatedStoragePermission_t1_1285;
// System.Security.SecurityElement
struct SecurityElement_t1_242;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Permissions_PermissionState.h"
#include "mscorlib_System_Security_Permissions_IsolatedStorageContainm.h"

// System.Void System.Security.Permissions.IsolatedStoragePermission::.ctor(System.Security.Permissions.PermissionState)
extern "C" void IsolatedStoragePermission__ctor_m1_10938 (IsolatedStoragePermission_t1_1285 * __this, int32_t ___state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Security.Permissions.IsolatedStoragePermission::get_UserQuota()
extern "C" int64_t IsolatedStoragePermission_get_UserQuota_m1_10939 (IsolatedStoragePermission_t1_1285 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.IsolatedStoragePermission::set_UserQuota(System.Int64)
extern "C" void IsolatedStoragePermission_set_UserQuota_m1_10940 (IsolatedStoragePermission_t1_1285 * __this, int64_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Permissions.IsolatedStorageContainment System.Security.Permissions.IsolatedStoragePermission::get_UsageAllowed()
extern "C" int32_t IsolatedStoragePermission_get_UsageAllowed_m1_10941 (IsolatedStoragePermission_t1_1285 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.IsolatedStoragePermission::set_UsageAllowed(System.Security.Permissions.IsolatedStorageContainment)
extern "C" void IsolatedStoragePermission_set_UsageAllowed_m1_10942 (IsolatedStoragePermission_t1_1285 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.IsolatedStoragePermission::IsUnrestricted()
extern "C" bool IsolatedStoragePermission_IsUnrestricted_m1_10943 (IsolatedStoragePermission_t1_1285 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityElement System.Security.Permissions.IsolatedStoragePermission::ToXml()
extern "C" SecurityElement_t1_242 * IsolatedStoragePermission_ToXml_m1_10944 (IsolatedStoragePermission_t1_1285 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.IsolatedStoragePermission::FromXml(System.Security.SecurityElement)
extern "C" void IsolatedStoragePermission_FromXml_m1_10945 (IsolatedStoragePermission_t1_1285 * __this, SecurityElement_t1_242 * ___esd, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.IsolatedStoragePermission::IsEmpty()
extern "C" bool IsolatedStoragePermission_IsEmpty_m1_10946 (IsolatedStoragePermission_t1_1285 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
