﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1_14925(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t1_1805 *, String_t*, DTDNode_t4_89 *, const MethodInfo*))KeyValuePair_2__ctor_m1_15906_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode>::get_Key()
#define KeyValuePair_2_get_Key_m1_14927(__this, method) (( String_t* (*) (KeyValuePair_2_t1_1805 *, const MethodInfo*))KeyValuePair_2_get_Key_m1_15907_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1_17981(__this, ___value, method) (( void (*) (KeyValuePair_2_t1_1805 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m1_15908_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode>::get_Value()
#define KeyValuePair_2_get_Value_m1_14922(__this, method) (( DTDNode_t4_89 * (*) (KeyValuePair_2_t1_1805 *, const MethodInfo*))KeyValuePair_2_get_Value_m1_15909_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1_17982(__this, ___value, method) (( void (*) (KeyValuePair_2_t1_1805 *, DTDNode_t4_89 *, const MethodInfo*))KeyValuePair_2_set_Value_m1_15910_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode>::ToString()
#define KeyValuePair_2_ToString_m1_17983(__this, method) (( String_t* (*) (KeyValuePair_2_t1_1805 *, const MethodInfo*))KeyValuePair_2_ToString_m1_15911_gshared)(__this, method)
