﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.StringComparer
struct StringComparer_t1_1598;
// System.Globalization.CultureInfo
struct CultureInfo_t1_277;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.StringComparer::.ctor()
extern "C" void StringComparer__ctor_m1_14584 (StringComparer_t1_1598 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.StringComparer::.cctor()
extern "C" void StringComparer__cctor_m1_14585 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.StringComparer System.StringComparer::get_CurrentCulture()
extern "C" StringComparer_t1_1598 * StringComparer_get_CurrentCulture_m1_14586 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.StringComparer System.StringComparer::get_CurrentCultureIgnoreCase()
extern "C" StringComparer_t1_1598 * StringComparer_get_CurrentCultureIgnoreCase_m1_14587 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.StringComparer System.StringComparer::get_InvariantCulture()
extern "C" StringComparer_t1_1598 * StringComparer_get_InvariantCulture_m1_14588 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.StringComparer System.StringComparer::get_InvariantCultureIgnoreCase()
extern "C" StringComparer_t1_1598 * StringComparer_get_InvariantCultureIgnoreCase_m1_14589 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.StringComparer System.StringComparer::get_Ordinal()
extern "C" StringComparer_t1_1598 * StringComparer_get_Ordinal_m1_14590 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.StringComparer System.StringComparer::get_OrdinalIgnoreCase()
extern "C" StringComparer_t1_1598 * StringComparer_get_OrdinalIgnoreCase_m1_14591 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.StringComparer System.StringComparer::Create(System.Globalization.CultureInfo,System.Boolean)
extern "C" StringComparer_t1_1598 * StringComparer_Create_m1_14592 (Object_t * __this /* static, unused */, CultureInfo_t1_277 * ___culture, bool ___ignoreCase, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.StringComparer::Compare(System.Object,System.Object)
extern "C" int32_t StringComparer_Compare_m1_14593 (StringComparer_t1_1598 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.StringComparer::Equals(System.Object,System.Object)
extern "C" bool StringComparer_Equals_m1_14594 (StringComparer_t1_1598 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.StringComparer::GetHashCode(System.Object)
extern "C" int32_t StringComparer_GetHashCode_m1_14595 (StringComparer_t1_1598 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
