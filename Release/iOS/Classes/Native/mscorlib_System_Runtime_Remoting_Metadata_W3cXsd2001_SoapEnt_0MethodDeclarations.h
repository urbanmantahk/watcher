﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapEntity
struct SoapEntity_t1_972;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapEntity::.ctor()
extern "C" void SoapEntity__ctor_m1_8720 (SoapEntity_t1_972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapEntity::.ctor(System.String)
extern "C" void SoapEntity__ctor_m1_8721 (SoapEntity_t1_972 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapEntity::get_Value()
extern "C" String_t* SoapEntity_get_Value_m1_8722 (SoapEntity_t1_972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapEntity::set_Value(System.String)
extern "C" void SoapEntity_set_Value_m1_8723 (SoapEntity_t1_972 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapEntity::get_XsdType()
extern "C" String_t* SoapEntity_get_XsdType_m1_8724 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapEntity::GetXsdType()
extern "C" String_t* SoapEntity_GetXsdType_m1_8725 (SoapEntity_t1_972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapEntity System.Runtime.Remoting.Metadata.W3cXsd2001.SoapEntity::Parse(System.String)
extern "C" SoapEntity_t1_972 * SoapEntity_Parse_m1_8726 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapEntity::ToString()
extern "C" String_t* SoapEntity_ToString_m1_8727 (SoapEntity_t1_972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
