﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.Security.AccessControl.AuditRule
struct AuditRule_t1_1119;
// System.Security.Principal.IdentityReference
struct IdentityReference_t1_1120;
// System.Security.AccessControl.AuthorizationRule
struct AuthorizationRule_t1_1112;
// System.Security.AccessControl.AuthorizationRuleCollection
struct AuthorizationRuleCollection_t1_1121;
// System.Security.AccessControl.AuthorizationRule[]
struct AuthorizationRuleU5BU5D_t1_1728;
// System.Security.AccessControl.CommonAce
struct CommonAce_t1_1122;
// System.Security.Principal.SecurityIdentifier
struct SecurityIdentifier_t1_1132;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Security.AccessControl.CommonAcl
struct CommonAcl_t1_1124;
// System.Security.AccessControl.GenericAce
struct GenericAce_t1_1145;
// System.Security.AccessControl.CommonObjectSecurity
struct CommonObjectSecurity_t1_1126;
// System.Type
struct Type_t;
// System.Security.AccessControl.AccessRule
struct AccessRule_t1_1111;
// System.Security.AccessControl.CommonSecurityDescriptor
struct CommonSecurityDescriptor_t1_1130;
// System.Security.AccessControl.RawSecurityDescriptor
struct RawSecurityDescriptor_t1_1171;
// System.String
struct String_t;
// System.Security.AccessControl.SystemAcl
struct SystemAcl_t1_1133;
// System.Security.AccessControl.DiscretionaryAcl
struct DiscretionaryAcl_t1_1134;
// System.Security.AccessControl.CompoundAce
struct CompoundAce_t1_1135;
// System.Security.AccessControl.CryptoKeyAccessRule
struct CryptoKeyAccessRule_t1_1139;
// System.Security.AccessControl.CryptoKeyAuditRule
struct CryptoKeyAuditRule_t1_1140;
// System.Security.AccessControl.CryptoKeySecurity
struct CryptoKeySecurity_t1_1142;
// System.Security.AccessControl.CustomAce
struct CustomAce_t1_1144;
// System.Security.AccessControl.DirectoryObjectSecurity
struct DirectoryObjectSecurity_t1_1146;
// System.Security.AccessControl.ObjectAccessRule
struct ObjectAccessRule_t1_1163;
// System.Security.AccessControl.ObjectAuditRule
struct ObjectAuditRule_t1_1166;
// System.Security.AccessControl.DirectorySecurity
struct DirectorySecurity_t1_1147;
// System.Security.AccessControl.RawAcl
struct RawAcl_t1_1170;
// System.Security.AccessControl.EventWaitHandleAccessRule
struct EventWaitHandleAccessRule_t1_1149;
// System.Security.AccessControl.EventWaitHandleAuditRule
struct EventWaitHandleAuditRule_t1_1150;
// System.Security.AccessControl.EventWaitHandleSecurity
struct EventWaitHandleSecurity_t1_1152;
// System.Security.AccessControl.FileSecurity
struct FileSecurity_t1_1153;
// System.Security.AccessControl.FileSystemAccessRule
struct FileSystemAccessRule_t1_1154;
// System.Security.AccessControl.FileSystemAuditRule
struct FileSystemAuditRule_t1_1155;
// System.Security.AccessControl.FileSystemSecurity
struct FileSystemSecurity_t1_1148;
// System.Object
struct Object_t;
// System.Security.AccessControl.GenericAcl
struct GenericAcl_t1_1114;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Security.AccessControl.GenericAce[]
struct GenericAceU5BU5D_t1_1729;
// System.Security.AccessControl.AceEnumerator
struct AceEnumerator_t1_1113;
// System.Security.AccessControl.GenericSecurityDescriptor
struct GenericSecurityDescriptor_t1_1131;
// System.Security.AccessControl.KnownAce
struct KnownAce_t1_1136;
// System.Security.AccessControl.MutexAccessRule
struct MutexAccessRule_t1_1158;
// System.Security.AccessControl.MutexAuditRule
struct MutexAuditRule_t1_1159;
// System.Security.AccessControl.MutexSecurity
struct MutexSecurity_t1_1161;
// System.Security.AccessControl.NativeObjectSecurity/ExceptionFromErrorCode
struct ExceptionFromErrorCode_t1_1162;
// System.Exception
struct Exception_t1_33;
// System.Runtime.InteropServices.SafeHandle
struct SafeHandle_t1_88;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;
// System.Security.AccessControl.NativeObjectSecurity
struct NativeObjectSecurity_t1_1143;
// System.Security.AccessControl.ObjectAce
struct ObjectAce_t1_1164;
// System.Security.AccessControl.ObjectSecurity
struct ObjectSecurity_t1_1127;
// System.Security.AccessControl.PrivilegeNotHeldException
struct PrivilegeNotHeldException_t1_1167;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;
// System.Security.AccessControl.QualifiedAce
struct QualifiedAce_t1_1123;
// System.Security.AccessControl.RegistryAccessRule
struct RegistryAccessRule_t1_1172;
// System.Security.AccessControl.RegistryAuditRule
struct RegistryAuditRule_t1_1173;
// System.Security.AccessControl.RegistrySecurity
struct RegistrySecurity_t1_1175;
// System.Security.Cryptography.X509Certificates.X509Certificate
struct X509Certificate_t1_1179;
// System.Security.SecureString
struct SecureString_t1_1197;
// System.Security.Cryptography.AsymmetricAlgorithm
struct AsymmetricAlgorithm_t1_228;
// System.Security.Cryptography.KeySizes[]
struct KeySizesU5BU5D_t1_1182;
// System.Security.Cryptography.AsymmetricKeyExchangeDeformatter
struct AsymmetricKeyExchangeDeformatter_t1_1183;
// System.Security.Cryptography.AsymmetricKeyExchangeFormatter
struct AsymmetricKeyExchangeFormatter_t1_1184;
// System.Security.Cryptography.AsymmetricSignatureDeformatter
struct AsymmetricSignatureDeformatter_t1_1185;
// System.Security.Cryptography.HashAlgorithm
struct HashAlgorithm_t1_162;
// System.Security.Cryptography.AsymmetricSignatureFormatter
struct AsymmetricSignatureFormatter_t1_1186;
// System.Security.Cryptography.CryptoAPITransform
struct CryptoAPITransform_t1_1189;
// System.Security.Cryptography.CryptoConfig/CryptoHandler
struct CryptoHandler_t1_1190;
// System.Collections.Hashtable
struct Hashtable_t1_100;
// Mono.Xml.SmallXmlParser
struct SmallXmlParser_t1_241;
// Mono.Xml.SmallXmlParser/IAttrList
struct IAttrList_t1_1676;
// System.Security.Cryptography.CryptoConfig
struct CryptoConfig_t1_1191;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Security.Cryptography.CryptoStream
struct CryptoStream_t1_1192;
// System.IO.Stream
struct Stream_t1_405;
// System.Security.Cryptography.ICryptoTransform
struct ICryptoTransform_t1_156;
// System.Security.Cryptography.CryptographicException
struct CryptographicException_t1_1194;
// System.Security.Cryptography.CryptographicUnexpectedOperationException
struct CryptographicUnexpectedOperationException_t1_1195;
// System.Security.Cryptography.CspKeyContainerInfo
struct CspKeyContainerInfo_t1_1196;
// System.Security.Cryptography.CspParameters
struct CspParameters_t1_164;
// System.Security.Cryptography.DES
struct DES_t1_1199;
// System.Security.Cryptography.DESTransform
struct DESTransform_t1_1201;
// System.Security.Cryptography.SymmetricAlgorithm
struct SymmetricAlgorithm_t1_166;
// System.UInt32[]
struct UInt32U5BU5D_t1_142;
// System.Security.Cryptography.DESCryptoServiceProvider
struct DESCryptoServiceProvider_t1_1202;
// System.Security.Cryptography.DSA
struct DSA_t1_160;
// System.Security.Cryptography.DSACryptoServiceProvider
struct DSACryptoServiceProvider_t1_1203;
// System.EventArgs
struct EventArgs_t1_158;
// System.Security.Cryptography.DSASignatureDeformatter
struct DSASignatureDeformatter_t1_1205;
// System.Security.Cryptography.DSASignatureFormatter
struct DSASignatureFormatter_t1_1206;
// System.Security.Cryptography.DeriveBytes
struct DeriveBytes_t1_1207;
// System.Security.Cryptography.FromBase64Transform
struct FromBase64Transform_t1_1209;
// System.Security.Cryptography.HMAC
struct HMAC_t1_1210;
// Mono.Security.Cryptography.BlockProcessor
struct BlockProcessor_t1_155;
// System.Security.Cryptography.HMACMD5
struct HMACMD5_t1_1212;
// System.Security.Cryptography.HMACRIPEMD160
struct HMACRIPEMD160_t1_1213;
// System.Security.Cryptography.HMACSHA1
struct HMACSHA1_t1_1214;
// System.Security.Cryptography.HMACSHA256
struct HMACSHA256_t1_1215;
// System.Security.Cryptography.HMACSHA384
struct HMACSHA384_t1_1216;
// System.Security.Cryptography.HMACSHA512
struct HMACSHA512_t1_1217;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array.h"
#include "mscorlib_System_Security_AccessControl_AceFlags.h"
#include "mscorlib_System_Security_AccessControl_AceFlagsMethodDeclarations.h"
#include "mscorlib_System_Security_AccessControl_AceQualifier.h"
#include "mscorlib_System_Security_AccessControl_AceQualifierMethodDeclarations.h"
#include "mscorlib_System_Security_AccessControl_AceType.h"
#include "mscorlib_System_Security_AccessControl_AceTypeMethodDeclarations.h"
#include "mscorlib_System_Security_AccessControl_AuditFlags.h"
#include "mscorlib_System_Security_AccessControl_AuditFlagsMethodDeclarations.h"
#include "mscorlib_System_Security_AccessControl_AuditRule.h"
#include "mscorlib_System_Security_AccessControl_AuditRuleMethodDeclarations.h"
#include "mscorlib_System_Security_AccessControl_InheritanceFlags.h"
#include "mscorlib_System_Security_AccessControl_PropagationFlags.h"
#include "mscorlib_System_Void.h"
#include "mscorlib_System_Security_Principal_IdentityReference.h"
#include "mscorlib_System_Int32.h"
#include "mscorlib_System_Boolean.h"
#include "mscorlib_System_Security_AccessControl_AuthorizationRuleMethodDeclarations.h"
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeExceptionMethodDeclarations.h"
#include "mscorlib_System_Security_AccessControl_AuthorizationRule.h"
#include "mscorlib_System_Security_Principal_SecurityIdentifier.h"
#include "mscorlib_System_String.h"
#include "mscorlib_System_ArgumentException.h"
#include "mscorlib_System_ArgumentOutOfRangeException.h"
#include "mscorlib_System_ObjectMethodDeclarations.h"
#include "mscorlib_System_Object.h"
#include "mscorlib_System_Security_AccessControl_AuthorizationRuleColl.h"
#include "mscorlib_System_Security_AccessControl_AuthorizationRuleCollMethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Collections_ReadOnlyCollectionBaseMethodDeclarations.h"
#include "mscorlib_System_Collections_ReadOnlyCollectionBase.h"
#include "mscorlib_System_Collections_ArrayList.h"
#include "mscorlib_System_Collections_ArrayListMethodDeclarations.h"
#include "mscorlib_System_Security_AccessControl_CommonAce.h"
#include "mscorlib_System_Security_AccessControl_CommonAceMethodDeclarations.h"
#include "mscorlib_System_Byte.h"
#include "mscorlib_System_Security_AccessControl_QualifiedAceMethodDeclarations.h"
#include "mscorlib_System_Security_AccessControl_KnownAceMethodDeclarations.h"
#include "mscorlib_System_Security_AccessControl_QualifiedAce.h"
#include "mscorlib_System_Security_AccessControl_KnownAce.h"
#include "mscorlib_System_NotImplementedExceptionMethodDeclarations.h"
#include "mscorlib_System_NotImplementedException.h"
#include "mscorlib_System_Security_AccessControl_CommonAcl.h"
#include "mscorlib_System_Security_AccessControl_CommonAclMethodDeclarations.h"
#include "mscorlib_System_Security_AccessControl_GenericAclMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_0MethodDeclarations.h"
#include "mscorlib_System_Security_AccessControl_GenericAcl.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_0.h"
#include "mscorlib_System_Security_AccessControl_GenericAce.h"
#include "mscorlib_System_Security_AccessControl_CommonObjectSecurity.h"
#include "mscorlib_System_Security_AccessControl_CommonObjectSecurityMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_1MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_2MethodDeclarations.h"
#include "mscorlib_System_Security_AccessControl_ObjectSecurityMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_1.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_2.h"
#include "mscorlib_System_Security_AccessControl_ObjectSecurity.h"
#include "mscorlib_System_Type.h"
#include "mscorlib_System_Security_AccessControl_AccessRule.h"
#include "mscorlib_System_Security_AccessControl_AccessControlModifica.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_genMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_0MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_0.h"
#include "mscorlib_System_Security_AccessControl_CommonSecurityDescrip.h"
#include "mscorlib_System_Security_AccessControl_CommonSecurityDescripMethodDeclarations.h"
#include "mscorlib_System_Security_AccessControl_RawSecurityDescriptor.h"
#include "mscorlib_System_Security_AccessControl_GenericSecurityDescriMethodDeclarations.h"
#include "mscorlib_System_Security_AccessControl_GenericSecurityDescri.h"
#include "mscorlib_System_Security_AccessControl_ControlFlags.h"
#include "mscorlib_System_Security_AccessControl_SystemAcl.h"
#include "mscorlib_System_Security_AccessControl_DiscretionaryAcl.h"
#include "mscorlib_System_Security_AccessControl_CompoundAce.h"
#include "mscorlib_System_Security_AccessControl_CompoundAceMethodDeclarations.h"
#include "mscorlib_System_Security_AccessControl_CompoundAceType.h"
#include "mscorlib_System_Security_AccessControl_GenericAceMethodDeclarations.h"
#include "mscorlib_System_Security_AccessControl_CompoundAceTypeMethodDeclarations.h"
#include "mscorlib_System_Security_AccessControl_ControlFlagsMethodDeclarations.h"
#include "mscorlib_System_Security_AccessControl_CryptoKeyAccessRule.h"
#include "mscorlib_System_Security_AccessControl_CryptoKeyAccessRuleMethodDeclarations.h"
#include "mscorlib_System_Security_AccessControl_CryptoKeyRights.h"
#include "mscorlib_System_Security_AccessControl_AccessControlType.h"
#include "mscorlib_System_Security_AccessControl_AccessRuleMethodDeclarations.h"
#include "mscorlib_System_Security_Principal_SecurityIdentifierMethodDeclarations.h"
#include "mscorlib_System_Security_AccessControl_CryptoKeyAuditRule.h"
#include "mscorlib_System_Security_AccessControl_CryptoKeyAuditRuleMethodDeclarations.h"
#include "mscorlib_System_Security_AccessControl_CryptoKeyRightsMethodDeclarations.h"
#include "mscorlib_System_Security_AccessControl_CryptoKeySecurity.h"
#include "mscorlib_System_Security_AccessControl_CryptoKeySecurityMethodDeclarations.h"
#include "mscorlib_System_Security_AccessControl_NativeObjectSecurityMethodDeclarations.h"
#include "mscorlib_System_Security_AccessControl_NativeObjectSecurity.h"
#include "mscorlib_System_TypeMethodDeclarations.h"
#include "mscorlib_System_RuntimeTypeHandle.h"
#include "mscorlib_System_Security_AccessControl_CustomAce.h"
#include "mscorlib_System_Security_AccessControl_CustomAceMethodDeclarations.h"
#include "mscorlib_System_ArrayMethodDeclarations.h"
#include "mscorlib_System_ArgumentNullExceptionMethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException.h"
#include "mscorlib_System_Security_AccessControl_DirectoryObjectSecuri.h"
#include "mscorlib_System_Security_AccessControl_DirectoryObjectSecuriMethodDeclarations.h"
#include "mscorlib_System_Guid.h"
#include "mscorlib_System_Security_AccessControl_ObjectAccessRule.h"
#include "mscorlib_System_Security_AccessControl_ObjectAuditRule.h"
#include "mscorlib_System_Security_AccessControl_DirectorySecurity.h"
#include "mscorlib_System_Security_AccessControl_DirectorySecurityMethodDeclarations.h"
#include "mscorlib_System_Security_AccessControl_FileSystemSecurityMethodDeclarations.h"
#include "mscorlib_System_PlatformNotSupportedExceptionMethodDeclarations.h"
#include "mscorlib_System_Security_AccessControl_FileSystemSecurity.h"
#include "mscorlib_System_PlatformNotSupportedException.h"
#include "mscorlib_System_Security_AccessControl_AccessControlSections.h"
#include "mscorlib_System_Security_AccessControl_DiscretionaryAclMethodDeclarations.h"
#include "mscorlib_System_Security_AccessControl_RawAcl.h"
#include "mscorlib_System_Security_AccessControl_ObjectAceFlags.h"
#include "mscorlib_System_Security_AccessControl_EventWaitHandleAccess.h"
#include "mscorlib_System_Security_AccessControl_EventWaitHandleAccessMethodDeclarations.h"
#include "mscorlib_System_Security_AccessControl_EventWaitHandleRights.h"
#include "mscorlib_System_Security_AccessControl_EventWaitHandleAuditR.h"
#include "mscorlib_System_Security_AccessControl_EventWaitHandleAuditRMethodDeclarations.h"
#include "mscorlib_System_Security_Principal_IdentityReferenceMethodDeclarations.h"
#include "mscorlib_System_Security_AccessControl_EventWaitHandleRightsMethodDeclarations.h"
#include "mscorlib_System_Security_AccessControl_EventWaitHandleSecuri.h"
#include "mscorlib_System_Security_AccessControl_EventWaitHandleSecuriMethodDeclarations.h"
#include "mscorlib_System_Security_AccessControl_FileSecurity.h"
#include "mscorlib_System_Security_AccessControl_FileSecurityMethodDeclarations.h"
#include "mscorlib_System_Security_AccessControl_FileSystemAccessRule.h"
#include "mscorlib_System_Security_AccessControl_FileSystemAccessRuleMethodDeclarations.h"
#include "mscorlib_System_Security_AccessControl_FileSystemRights.h"
#include "mscorlib_System_Security_AccessControl_FileSystemAuditRule.h"
#include "mscorlib_System_Security_AccessControl_FileSystemAuditRuleMethodDeclarations.h"
#include "mscorlib_System_Security_AccessControl_FileSystemRightsMethodDeclarations.h"
#include "mscorlib_System_Security_AccessControl_ResourceType.h"
#include "mscorlib_System_Security_AccessControl_AceEnumerator.h"
#include "mscorlib_System_Security_AccessControl_AceEnumeratorMethodDeclarations.h"
#include "mscorlib_System_Security_AccessControl_InheritanceFlagsMethodDeclarations.h"
#include "mscorlib_System_Security_AccessControl_MutexAccessRule.h"
#include "mscorlib_System_Security_AccessControl_MutexAccessRuleMethodDeclarations.h"
#include "mscorlib_System_Security_AccessControl_MutexRights.h"
#include "mscorlib_System_Security_AccessControl_MutexAuditRule.h"
#include "mscorlib_System_Security_AccessControl_MutexAuditRuleMethodDeclarations.h"
#include "mscorlib_System_Security_AccessControl_MutexRightsMethodDeclarations.h"
#include "mscorlib_System_Security_AccessControl_MutexSecurity.h"
#include "mscorlib_System_Security_AccessControl_MutexSecurityMethodDeclarations.h"
#include "mscorlib_System_Security_AccessControl_NativeObjectSecurity_.h"
#include "mscorlib_System_Security_AccessControl_NativeObjectSecurity_MethodDeclarations.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_Exception.h"
#include "mscorlib_System_Runtime_InteropServices_SafeHandle.h"
#include "mscorlib_System_Runtime_InteropServices_SafeHandleMethodDeclarations.h"
#include "mscorlib_System_AsyncCallback.h"
#include "mscorlib_System_Security_AccessControl_ObjectAccessRuleMethodDeclarations.h"
#include "mscorlib_System_GuidMethodDeclarations.h"
#include "mscorlib_System_Security_AccessControl_ObjectAce.h"
#include "mscorlib_System_Security_AccessControl_ObjectAceMethodDeclarations.h"
#include "mscorlib_System_Security_AccessControl_ObjectAceFlagsMethodDeclarations.h"
#include "mscorlib_System_Security_AccessControl_ObjectAuditRuleMethodDeclarations.h"
#include "mscorlib_System_Security_AccessControl_PrivilegeNotHeldExcep.h"
#include "mscorlib_System_Security_AccessControl_PrivilegeNotHeldExcepMethodDeclarations.h"
#include "mscorlib_System_UnauthorizedAccessExceptionMethodDeclarations.h"
#include "mscorlib_System_UnauthorizedAccessException.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Runtime_Serialization_SerializationInfo.h"
#include "mscorlib_System_Security_AccessControl_PropagationFlagsMethodDeclarations.h"
#include "mscorlib_System_Security_AccessControl_RawAclMethodDeclarations.h"
#include "mscorlib_System_Security_AccessControl_RawSecurityDescriptorMethodDeclarations.h"
#include "mscorlib_System_Security_AccessControl_RegistryAccessRule.h"
#include "mscorlib_System_Security_AccessControl_RegistryAccessRuleMethodDeclarations.h"
#include "mscorlib_System_Security_AccessControl_RegistryRights.h"
#include "mscorlib_System_Security_AccessControl_RegistryAuditRule.h"
#include "mscorlib_System_Security_AccessControl_RegistryAuditRuleMethodDeclarations.h"
#include "mscorlib_System_Security_AccessControl_RegistryRightsMethodDeclarations.h"
#include "mscorlib_System_Security_AccessControl_RegistrySecurity.h"
#include "mscorlib_System_Security_AccessControl_RegistrySecurityMethodDeclarations.h"
#include "mscorlib_System_Security_AccessControl_ResourceTypeMethodDeclarations.h"
#include "mscorlib_System_Security_AccessControl_SecurityInfos.h"
#include "mscorlib_System_Security_AccessControl_SecurityInfosMethodDeclarations.h"
#include "mscorlib_System_Security_AccessControl_SystemAclMethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_X509Certificates_X509C.h"
#include "mscorlib_System_Security_Cryptography_X509Certificates_X509CMethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_X509Certificates_X509C_0.h"
#include "mscorlib_System_Security_Cryptography_X509Certificates_X509C_0MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_X509Certificates_X509K.h"
#include "mscorlib_System_IntPtrMethodDeclarations.h"
#include "mscorlib_System_NotSupportedExceptionMethodDeclarations.h"
#include "mscorlib_System_NotSupportedException.h"
#include "mscorlib_Mono_Security_X509_X509CertificateMethodDeclarations.h"
#include "mscorlib_Mono_Security_X509_X509Certificate.h"
#include "mscorlib_System_Security_SecureString.h"
#include "mscorlib_System_Runtime_Serialization_SerializationInfoMethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilderMethodDeclarations.h"
#include "mscorlib_System_ByteMethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder.h"
#include "mscorlib_Mono_Security_Authenticode_AuthenticodeDeformatterMethodDeclarations.h"
#include "mscorlib_LocaleMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_COMExceptionMethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_CryptographicExceptionMethodDeclarations.h"
#include "mscorlib_Mono_Security_Authenticode_AuthenticodeDeformatter.h"
#include "mscorlib_System_Security_SecurityException.h"
#include "mscorlib_System_Runtime_InteropServices_COMException.h"
#include "mscorlib_System_Security_Cryptography_CryptographicException.h"
#include "mscorlib_System_Runtime_InteropServices_MarshalMethodDeclarations.h"
#include "mscorlib_System_UInt32.h"
#include "mscorlib_System_Security_Cryptography_SHA1MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_HashAlgorithmMethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_SHA1.h"
#include "mscorlib_System_Security_Cryptography_HashAlgorithm.h"
#include "mscorlib_System_DateTimeMethodDeclarations.h"
#include "mscorlib_System_DateTime.h"
#include "mscorlib_System_EnvironmentMethodDeclarations.h"
#include "mscorlib_System_IO_FileMethodDeclarations.h"
#include "mscorlib_System_IO_FileStream.h"
#include "mscorlib_System_IO_FileStreamMethodDeclarations.h"
#include "mscorlib_System_Int64.h"
#include "mscorlib_System_IO_Stream.h"
#include "mscorlib_System_IO_StreamMethodDeclarations.h"
#include "mscorlib_Mono_Security_X509_X501MethodDeclarations.h"
#include "mscorlib_Mono_Security_ASN1.h"
#include "mscorlib_System_Security_Cryptography_X509Certificates_X509C_1.h"
#include "mscorlib_System_Text_EncodingMethodDeclarations.h"
#include "mscorlib_System_Text_Encoding.h"
#include "mscorlib_System_Security_SecureStringMethodDeclarations.h"
#include "mscorlib_Mono_Security_X509_PKCS12MethodDeclarations.h"
#include "mscorlib_Mono_Security_X509_X509CertificateCollectionMethodDeclarations.h"
#include "mscorlib_Mono_Security_X509_PKCS12.h"
#include "mscorlib_Mono_Security_X509_X509CertificateCollection.h"
#include "mscorlib_System_Collections_CollectionBase.h"
#include "mscorlib_System_Collections_CollectionBaseMethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_X509Certificates_X509C_1MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_X509Certificates_X509KMethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_AsymmetricAlgorithm.h"
#include "mscorlib_System_Security_Cryptography_AsymmetricAlgorithmMethodDeclarations.h"
#include "mscorlib_System_GCMethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_KeySizesMethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_KeySizes.h"
#include "mscorlib_System_Security_Cryptography_CryptoConfigMethodDeclarations.h"
#include "mscorlib_System_StringMethodDeclarations.h"
#include "mscorlib_System_ConvertMethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_AsymmetricKeyExchangeD.h"
#include "mscorlib_System_Security_Cryptography_AsymmetricKeyExchangeDMethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_AsymmetricKeyExchangeF.h"
#include "mscorlib_System_Security_Cryptography_AsymmetricKeyExchangeFMethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_AsymmetricSignatureDef.h"
#include "mscorlib_System_Security_Cryptography_AsymmetricSignatureDefMethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_AsymmetricSignatureFor.h"
#include "mscorlib_System_Security_Cryptography_AsymmetricSignatureForMethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_Base64Constants.h"
#include "mscorlib_System_Security_Cryptography_Base64ConstantsMethodDeclarations.h"
#include "mscorlib_System_Runtime_CompilerServices_RuntimeHelpersMethodDeclarations.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3EMethodDeclarations.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU246.h"
#include "mscorlib_System_RuntimeFieldHandle.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_3.h"
#include "mscorlib_System_Security_Cryptography_CipherMode.h"
#include "mscorlib_System_Security_Cryptography_CipherModeMethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_CryptoAPITransform.h"
#include "mscorlib_System_Security_Cryptography_CryptoAPITransformMethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_CryptoConfig_CryptoHan.h"
#include "mscorlib_System_Security_Cryptography_CryptoConfig_CryptoHanMethodDeclarations.h"
#include "mscorlib_System_Collections_Hashtable.h"
#include "mscorlib_System_Collections_HashtableMethodDeclarations.h"
#include "mscorlib_Mono_Xml_SmallXmlParser.h"
#include "mscorlib_System_Collections_DictionaryEntryMethodDeclarations.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"
#include "mscorlib_System_Security_Cryptography_CryptoConfig.h"
#include "mscorlib_System_Collections_CaseInsensitiveHashCodeProviderMethodDeclarations.h"
#include "mscorlib_System_Collections_CaseInsensitiveComparerMethodDeclarations.h"
#include "mscorlib_System_Collections_CaseInsensitiveHashCodeProvider.h"
#include "mscorlib_System_Collections_CaseInsensitiveComparer.h"
#include "mscorlib_System_IO_StreamReaderMethodDeclarations.h"
#include "mscorlib_Mono_Xml_SmallXmlParserMethodDeclarations.h"
#include "mscorlib_System_IO_TextReader.h"
#include "mscorlib_System_IO_StreamReader.h"
#include "mscorlib_System_Threading_MonitorMethodDeclarations.h"
#include "mscorlib_System_ActivatorMethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_CryptographicUnexpecteMethodDeclarations.h"
#include "mscorlib_System_BufferMethodDeclarations.h"
#include "mscorlib_System_Char.h"
#include "mscorlib_System_Security_Cryptography_CryptographicUnexpecte.h"
#include "mscorlib_System_OverflowExceptionMethodDeclarations.h"
#include "mscorlib_System_OverflowException.h"
#include "mscorlib_System_Security_Cryptography_CryptoStream.h"
#include "mscorlib_System_Security_Cryptography_CryptoStreamMethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_CryptoStreamMode.h"
#include "mscorlib_System_MathMethodDeclarations.h"
#include "mscorlib_System_IO_SeekOrigin.h"
#include "mscorlib_System_Security_Cryptography_CryptoStreamModeMethodDeclarations.h"
#include "mscorlib_System_SystemExceptionMethodDeclarations.h"
#include "mscorlib_System_ExceptionMethodDeclarations.h"
#include "mscorlib_System_SystemException.h"
#include "mscorlib_System_Security_Cryptography_CspKeyContainerInfo.h"
#include "mscorlib_System_Security_Cryptography_CspKeyContainerInfoMethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_CspParameters.h"
#include "mscorlib_System_Security_Cryptography_KeyNumber.h"
#include "mscorlib_System_Security_Cryptography_CspParametersMethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_CspProviderFlags.h"
#include "mscorlib_System_Security_Cryptography_CspProviderFlagsMethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_DES.h"
#include "mscorlib_System_Security_Cryptography_DESMethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_SymmetricAlgorithmMethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_SymmetricAlgorithm.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU243_1.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU249.h"
#include "mscorlib_System_Security_Cryptography_DESTransform.h"
#include "mscorlib_System_Security_Cryptography_DESTransformMethodDeclarations.h"
#include "mscorlib_Mono_Security_Cryptography_SymmetricTransformMethodDeclarations.h"
#include "mscorlib_Mono_Security_Cryptography_SymmetricTransform.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242_1.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU245.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU244_0.h"
#include "mscorlib_System_BitConverter.h"
#include "mscorlib_System_BitConverterMethodDeclarations.h"
#include "mscorlib_Mono_Security_Cryptography_KeyBuilderMethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_DESCryptoServiceProvid.h"
#include "mscorlib_System_Security_Cryptography_DESCryptoServiceProvidMethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_DSA.h"
#include "mscorlib_System_Security_Cryptography_DSAMethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_DSAParameters.h"
#include "mscorlib_Mono_Security_BitConverterLEMethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_DSACryptoServiceProvid.h"
#include "mscorlib_System_Security_Cryptography_DSACryptoServiceProvidMethodDeclarations.h"
#include "mscorlib_Mono_Security_Cryptography_DSAManagedMethodDeclarations.h"
#include "mscorlib_Mono_Security_Cryptography_DSAManaged_KeyGeneratedEMethodDeclarations.h"
#include "mscorlib_Mono_Security_Cryptography_KeyPairPersistenceMethodDeclarations.h"
#include "mscorlib_Mono_Security_Cryptography_DSAManaged.h"
#include "mscorlib_System_EventArgs.h"
#include "mscorlib_Mono_Security_Cryptography_DSAManaged_KeyGeneratedE.h"
#include "mscorlib_Mono_Security_Cryptography_KeyPairPersistence.h"
#include "mscorlib_System_Globalization_CultureInfoMethodDeclarations.h"
#include "mscorlib_System_Globalization_CultureInfo.h"
#include "mscorlib_Mono_Security_Cryptography_CryptoConvertMethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_DSAParametersMethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_DSASignatureDeformatte.h"
#include "mscorlib_System_Security_Cryptography_DSASignatureDeformatteMethodDeclarations.h"
#include "mscorlib_System_InvalidCastException.h"
#include "mscorlib_System_Security_Cryptography_DSASignatureFormatter.h"
#include "mscorlib_System_Security_Cryptography_DSASignatureFormatterMethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_DeriveBytes.h"
#include "mscorlib_System_Security_Cryptography_DeriveBytesMethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_FromBase64TransformMod.h"
#include "mscorlib_System_Security_Cryptography_FromBase64TransformModMethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_FromBase64Transform.h"
#include "mscorlib_System_Security_Cryptography_FromBase64TransformMethodDeclarations.h"
#include "mscorlib_System_FormatExceptionMethodDeclarations.h"
#include "mscorlib_System_FormatException.h"
#include "mscorlib_System_OutOfMemoryExceptionMethodDeclarations.h"
#include "mscorlib_System_OutOfMemoryException.h"
#include "mscorlib_System_ObjectDisposedExceptionMethodDeclarations.h"
#include "mscorlib_System_CharMethodDeclarations.h"
#include "mscorlib_System_ObjectDisposedException.h"
#include "mscorlib_System_Security_Cryptography_HMAC.h"
#include "mscorlib_System_Security_Cryptography_HMACMethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_KeyedHashAlgorithmMethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_KeyedHashAlgorithm.h"
#include "mscorlib_Mono_Security_Cryptography_BlockProcessor.h"
#include "mscorlib_Mono_Security_Cryptography_BlockProcessorMethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_HMACMD5.h"
#include "mscorlib_System_Security_Cryptography_HMACMD5MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_HMACRIPEMD160.h"
#include "mscorlib_System_Security_Cryptography_HMACRIPEMD160MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_HMACSHA1.h"
#include "mscorlib_System_Security_Cryptography_HMACSHA1MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_HMACSHA256.h"
#include "mscorlib_System_Security_Cryptography_HMACSHA256MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_HMACSHA384.h"
#include "mscorlib_System_Security_Cryptography_HMACSHA384MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_HMACSHA512.h"
#include "mscorlib_System_Security_Cryptography_HMACSHA512MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_KeyNumberMethodDeclarations.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Security.AccessControl.AuditRule::.ctor(System.Security.Principal.IdentityReference,System.Int32,System.Boolean,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.AuditFlags)
extern TypeInfo* SecurityIdentifier_t1_1132_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2527;
extern "C" void AuditRule__ctor_m1_9713 (AuditRule_t1_1119 * __this, IdentityReference_t1_1120 * ___identity, int32_t ___accessMask, bool ___isInherited, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___auditFlags, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SecurityIdentifier_t1_1132_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(838);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		_stringLiteral2527 = il2cpp_codegen_string_literal_from_index(2527);
		s_Il2CppMethodIntialized = true;
	}
	{
		IdentityReference_t1_1120 * L_0 = ___identity;
		int32_t L_1 = ___accessMask;
		bool L_2 = ___isInherited;
		int32_t L_3 = ___inheritanceFlags;
		int32_t L_4 = ___propagationFlags;
		AuthorizationRule__ctor_m1_9716(__this, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		IdentityReference_t1_1120 * L_5 = ___identity;
		if (((SecurityIdentifier_t1_1132 *)IsInstSealed(L_5, SecurityIdentifier_t1_1132_il2cpp_TypeInfo_var)))
		{
			goto IL_0023;
		}
	}
	{
		ArgumentException_t1_1425 * L_6 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13260(L_6, _stringLiteral2527, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_6);
	}

IL_0023:
	{
		int32_t L_7 = ___accessMask;
		if (L_7)
		{
			goto IL_002f;
		}
	}
	{
		ArgumentOutOfRangeException_t1_1501 * L_8 = (ArgumentOutOfRangeException_t1_1501 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1_13273(L_8, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_8);
	}

IL_002f:
	{
		int32_t L_9 = ___auditFlags;
		__this->___auditFlags_5 = L_9;
		return;
	}
}
// System.Security.AccessControl.AuditFlags System.Security.AccessControl.AuditRule::get_AuditFlags()
extern "C" int32_t AuditRule_get_AuditFlags_m1_9714 (AuditRule_t1_1119 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___auditFlags_5);
		return L_0;
	}
}
// System.Void System.Security.AccessControl.AuthorizationRule::.ctor()
extern "C" void AuthorizationRule__ctor_m1_9715 (AuthorizationRule_t1_1112 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.AccessControl.AuthorizationRule::.ctor(System.Security.Principal.IdentityReference,System.Int32,System.Boolean,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags)
extern TypeInfo* SecurityIdentifier_t1_1132_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2527;
extern "C" void AuthorizationRule__ctor_m1_9716 (AuthorizationRule_t1_1112 * __this, IdentityReference_t1_1120 * ___identity, int32_t ___accessMask, bool ___isInherited, int32_t ___inheritanceFlags, int32_t ___propagationFlags, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SecurityIdentifier_t1_1132_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(838);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		_stringLiteral2527 = il2cpp_codegen_string_literal_from_index(2527);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		IdentityReference_t1_1120 * L_0 = ___identity;
		if (((SecurityIdentifier_t1_1132 *)IsInstSealed(L_0, SecurityIdentifier_t1_1132_il2cpp_TypeInfo_var)))
		{
			goto IL_001c;
		}
	}
	{
		ArgumentException_t1_1425 * L_1 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13260(L_1, _stringLiteral2527, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_001c:
	{
		int32_t L_2 = ___accessMask;
		if (L_2)
		{
			goto IL_0028;
		}
	}
	{
		ArgumentOutOfRangeException_t1_1501 * L_3 = (ArgumentOutOfRangeException_t1_1501 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1_13273(L_3, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0028:
	{
		IdentityReference_t1_1120 * L_4 = ___identity;
		__this->___identity_0 = L_4;
		int32_t L_5 = ___accessMask;
		__this->___accessMask_1 = L_5;
		bool L_6 = ___isInherited;
		__this->___isInherited_2 = L_6;
		int32_t L_7 = ___inheritanceFlags;
		__this->___inheritanceFlags_3 = L_7;
		int32_t L_8 = ___propagationFlags;
		__this->___propagationFlags_4 = L_8;
		return;
	}
}
// System.Security.Principal.IdentityReference System.Security.AccessControl.AuthorizationRule::get_IdentityReference()
extern "C" IdentityReference_t1_1120 * AuthorizationRule_get_IdentityReference_m1_9717 (AuthorizationRule_t1_1112 * __this, const MethodInfo* method)
{
	{
		IdentityReference_t1_1120 * L_0 = (__this->___identity_0);
		return L_0;
	}
}
// System.Security.AccessControl.InheritanceFlags System.Security.AccessControl.AuthorizationRule::get_InheritanceFlags()
extern "C" int32_t AuthorizationRule_get_InheritanceFlags_m1_9718 (AuthorizationRule_t1_1112 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___inheritanceFlags_3);
		return L_0;
	}
}
// System.Boolean System.Security.AccessControl.AuthorizationRule::get_IsInherited()
extern "C" bool AuthorizationRule_get_IsInherited_m1_9719 (AuthorizationRule_t1_1112 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___isInherited_2);
		return L_0;
	}
}
// System.Security.AccessControl.PropagationFlags System.Security.AccessControl.AuthorizationRule::get_PropagationFlags()
extern "C" int32_t AuthorizationRule_get_PropagationFlags_m1_9720 (AuthorizationRule_t1_1112 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___propagationFlags_4);
		return L_0;
	}
}
// System.Int32 System.Security.AccessControl.AuthorizationRule::get_AccessMask()
extern "C" int32_t AuthorizationRule_get_AccessMask_m1_9721 (AuthorizationRule_t1_1112 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___accessMask_1);
		return L_0;
	}
}
// System.Void System.Security.AccessControl.AuthorizationRuleCollection::.ctor(System.Security.AccessControl.AuthorizationRule[])
extern "C" void AuthorizationRuleCollection__ctor_m1_9722 (AuthorizationRuleCollection_t1_1121 * __this, AuthorizationRuleU5BU5D_t1_1728* ___rules, const MethodInfo* method)
{
	{
		ReadOnlyCollectionBase__ctor_m1_3373(__this, /*hidden argument*/NULL);
		ArrayList_t1_170 * L_0 = ReadOnlyCollectionBase_get_InnerList_m1_3380(__this, /*hidden argument*/NULL);
		AuthorizationRuleU5BU5D_t1_1728* L_1 = ___rules;
		NullCheck(L_0);
		VirtActionInvoker1< Object_t * >::Invoke(52 /* System.Void System.Collections.ArrayList::AddRange(System.Collections.ICollection) */, L_0, (Object_t *)(Object_t *)L_1);
		return;
	}
}
// System.Security.AccessControl.AuthorizationRule System.Security.AccessControl.AuthorizationRuleCollection::get_Item(System.Int32)
extern TypeInfo* AuthorizationRule_t1_1112_il2cpp_TypeInfo_var;
extern "C" AuthorizationRule_t1_1112 * AuthorizationRuleCollection_get_Item_m1_9723 (AuthorizationRuleCollection_t1_1121 * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AuthorizationRule_t1_1112_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(839);
		s_Il2CppMethodIntialized = true;
	}
	{
		ArrayList_t1_170 * L_0 = ReadOnlyCollectionBase_get_InnerList_m1_3380(__this, /*hidden argument*/NULL);
		int32_t L_1 = ___index;
		NullCheck(L_0);
		Object_t * L_2 = (Object_t *)VirtFuncInvoker1< Object_t *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_0, L_1);
		return ((AuthorizationRule_t1_1112 *)CastclassClass(L_2, AuthorizationRule_t1_1112_il2cpp_TypeInfo_var));
	}
}
// System.Void System.Security.AccessControl.AuthorizationRuleCollection::CopyTo(System.Security.AccessControl.AuthorizationRule[],System.Int32)
extern "C" void AuthorizationRuleCollection_CopyTo_m1_9724 (AuthorizationRuleCollection_t1_1121 * __this, AuthorizationRuleU5BU5D_t1_1728* ___rules, int32_t ___index, const MethodInfo* method)
{
	{
		ArrayList_t1_170 * L_0 = ReadOnlyCollectionBase_get_InnerList_m1_3380(__this, /*hidden argument*/NULL);
		AuthorizationRuleU5BU5D_t1_1728* L_1 = ___rules;
		int32_t L_2 = ___index;
		NullCheck(L_0);
		VirtActionInvoker2< Array_t *, int32_t >::Invoke(48 /* System.Void System.Collections.ArrayList::CopyTo(System.Array,System.Int32) */, L_0, (Array_t *)(Array_t *)L_1, L_2);
		return;
	}
}
// System.Void System.Security.AccessControl.CommonAce::.ctor(System.Security.AccessControl.AceFlags,System.Security.AccessControl.AceQualifier,System.Int32,System.Security.Principal.SecurityIdentifier,System.Boolean,System.Byte[])
extern "C" void CommonAce__ctor_m1_9725 (CommonAce_t1_1122 * __this, uint8_t ___flags, int32_t ___qualifier, int32_t ___accessMask, SecurityIdentifier_t1_1132 * ___sid, bool ___isCallback, ByteU5BU5D_t1_109* ___opaque, const MethodInfo* method)
{
	{
		int32_t L_0 = ___qualifier;
		bool L_1 = ___isCallback;
		ByteU5BU5D_t1_109* L_2 = ___opaque;
		QualifiedAce__ctor_m1_10033(__this, 0, 0, L_0, L_1, L_2, /*hidden argument*/NULL);
		int32_t L_3 = ___accessMask;
		KnownAce_set_AccessMask_m1_9929(__this, L_3, /*hidden argument*/NULL);
		SecurityIdentifier_t1_1132 * L_4 = ___sid;
		KnownAce_set_SecurityIdentifier_m1_9931(__this, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 System.Security.AccessControl.CommonAce::get_BinaryLength()
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" int32_t CommonAce_get_BinaryLength_m1_9726 (CommonAce_t1_1122 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.CommonAce::GetBinaryForm(System.Byte[],System.Int32)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void CommonAce_GetBinaryForm_m1_9727 (CommonAce_t1_1122 * __this, ByteU5BU5D_t1_109* ___binaryForm, int32_t ___offset, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Int32 System.Security.AccessControl.CommonAce::MaxOpaqueLength(System.Boolean)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" int32_t CommonAce_MaxOpaqueLength_m1_9728 (Object_t * __this /* static, unused */, bool ___isCallback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.CommonAcl::.ctor(System.Boolean,System.Boolean,System.Byte)
extern "C" void CommonAcl__ctor_m1_9729 (CommonAcl_t1_1124 * __this, bool ___isContainer, bool ___isDS, uint8_t ___revision, const MethodInfo* method)
{
	{
		bool L_0 = ___isContainer;
		bool L_1 = ___isDS;
		uint8_t L_2 = ___revision;
		CommonAcl__ctor_m1_9730(__this, L_0, L_1, L_2, ((int32_t)10), /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.AccessControl.CommonAcl::.ctor(System.Boolean,System.Boolean,System.Byte,System.Int32)
extern TypeInfo* GenericAcl_t1_1114_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t1_1125_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1_14897_MethodInfo_var;
extern "C" void CommonAcl__ctor_m1_9730 (CommonAcl_t1_1124 * __this, bool ___isContainer, bool ___isDS, uint8_t ___revision, int32_t ___capacity, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GenericAcl_t1_1114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(841);
		List_1_t1_1125_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(842);
		List_1__ctor_m1_14897_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483689);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GenericAcl_t1_1114_il2cpp_TypeInfo_var);
		GenericAcl__ctor_m1_9913(__this, /*hidden argument*/NULL);
		bool L_0 = ___isContainer;
		__this->___is_container_4 = L_0;
		bool L_1 = ___isDS;
		__this->___is_ds_5 = L_1;
		uint8_t L_2 = ___revision;
		__this->___revision_6 = L_2;
		int32_t L_3 = ___capacity;
		List_1_t1_1125 * L_4 = (List_1_t1_1125 *)il2cpp_codegen_object_new (List_1_t1_1125_il2cpp_TypeInfo_var);
		List_1__ctor_m1_14897(L_4, L_3, /*hidden argument*/List_1__ctor_m1_14897_MethodInfo_var);
		__this->___list_7 = L_4;
		return;
	}
}
// System.Int32 System.Security.AccessControl.CommonAcl::get_BinaryLength()
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" int32_t CommonAcl_get_BinaryLength_m1_9731 (CommonAcl_t1_1124 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Int32 System.Security.AccessControl.CommonAcl::get_Count()
extern "C" int32_t CommonAcl_get_Count_m1_9732 (CommonAcl_t1_1124 * __this, const MethodInfo* method)
{
	{
		List_1_t1_1125 * L_0 = (__this->___list_7);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.Security.AccessControl.GenericAce>::get_Count() */, L_0);
		return L_1;
	}
}
// System.Boolean System.Security.AccessControl.CommonAcl::get_IsCanonical()
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" bool CommonAcl_get_IsCanonical_m1_9733 (CommonAcl_t1_1124 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Security.AccessControl.CommonAcl::get_IsContainer()
extern "C" bool CommonAcl_get_IsContainer_m1_9734 (CommonAcl_t1_1124 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___is_container_4);
		return L_0;
	}
}
// System.Boolean System.Security.AccessControl.CommonAcl::get_IsDS()
extern "C" bool CommonAcl_get_IsDS_m1_9735 (CommonAcl_t1_1124 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___is_ds_5);
		return L_0;
	}
}
// System.Security.AccessControl.GenericAce System.Security.AccessControl.CommonAcl::get_Item(System.Int32)
extern "C" GenericAce_t1_1145 * CommonAcl_get_Item_m1_9736 (CommonAcl_t1_1124 * __this, int32_t ___index, const MethodInfo* method)
{
	{
		List_1_t1_1125 * L_0 = (__this->___list_7);
		int32_t L_1 = ___index;
		NullCheck(L_0);
		GenericAce_t1_1145 * L_2 = (GenericAce_t1_1145 *)VirtFuncInvoker1< GenericAce_t1_1145 *, int32_t >::Invoke(31 /* T System.Collections.Generic.List`1<System.Security.AccessControl.GenericAce>::get_Item(System.Int32) */, L_0, L_1);
		return L_2;
	}
}
// System.Void System.Security.AccessControl.CommonAcl::set_Item(System.Int32,System.Security.AccessControl.GenericAce)
extern "C" void CommonAcl_set_Item_m1_9737 (CommonAcl_t1_1124 * __this, int32_t ___index, GenericAce_t1_1145 * ___value, const MethodInfo* method)
{
	{
		List_1_t1_1125 * L_0 = (__this->___list_7);
		int32_t L_1 = ___index;
		GenericAce_t1_1145 * L_2 = ___value;
		NullCheck(L_0);
		VirtActionInvoker2< int32_t, GenericAce_t1_1145 * >::Invoke(32 /* System.Void System.Collections.Generic.List`1<System.Security.AccessControl.GenericAce>::set_Item(System.Int32,T) */, L_0, L_1, L_2);
		return;
	}
}
// System.Byte System.Security.AccessControl.CommonAcl::get_Revision()
extern "C" uint8_t CommonAcl_get_Revision_m1_9738 (CommonAcl_t1_1124 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = (__this->___revision_6);
		return L_0;
	}
}
// System.Void System.Security.AccessControl.CommonAcl::GetBinaryForm(System.Byte[],System.Int32)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void CommonAcl_GetBinaryForm_m1_9739 (CommonAcl_t1_1124 * __this, ByteU5BU5D_t1_109* ___binaryForm, int32_t ___offset, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.CommonAcl::Purge(System.Security.Principal.SecurityIdentifier)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void CommonAcl_Purge_m1_9740 (CommonAcl_t1_1124 * __this, SecurityIdentifier_t1_1132 * ___sid, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.CommonAcl::RemoveInheritedAces()
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void CommonAcl_RemoveInheritedAces_m1_9741 (CommonAcl_t1_1124 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.CommonObjectSecurity::.ctor(System.Boolean)
extern TypeInfo* List_1_t1_1128_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t1_1129_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1_14898_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m1_14899_MethodInfo_var;
extern "C" void CommonObjectSecurity__ctor_m1_9742 (CommonObjectSecurity_t1_1126 * __this, bool ___isContainer, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t1_1128_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(845);
		List_1_t1_1129_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(846);
		List_1__ctor_m1_14898_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483690);
		List_1__ctor_m1_14899_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483691);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1_1128 * L_0 = (List_1_t1_1128 *)il2cpp_codegen_object_new (List_1_t1_1128_il2cpp_TypeInfo_var);
		List_1__ctor_m1_14898(L_0, /*hidden argument*/List_1__ctor_m1_14898_MethodInfo_var);
		__this->___access_rules_6 = L_0;
		List_1_t1_1129 * L_1 = (List_1_t1_1129 *)il2cpp_codegen_object_new (List_1_t1_1129_il2cpp_TypeInfo_var);
		List_1__ctor_m1_14899(L_1, /*hidden argument*/List_1__ctor_m1_14899_MethodInfo_var);
		__this->___audit_rules_7 = L_1;
		bool L_2 = ___isContainer;
		ObjectSecurity__ctor_m1_9989(__this, L_2, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Security.AccessControl.AuthorizationRuleCollection System.Security.AccessControl.CommonObjectSecurity::GetAccessRules(System.Boolean,System.Boolean,System.Type)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" AuthorizationRuleCollection_t1_1121 * CommonObjectSecurity_GetAccessRules_m1_9743 (CommonObjectSecurity_t1_1126 * __this, bool ___includeExplicit, bool ___includeInherited, Type_t * ___targetType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Security.AccessControl.AuthorizationRuleCollection System.Security.AccessControl.CommonObjectSecurity::GetAuditRules(System.Boolean,System.Boolean,System.Type)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" AuthorizationRuleCollection_t1_1121 * CommonObjectSecurity_GetAuditRules_m1_9744 (CommonObjectSecurity_t1_1126 * __this, bool ___includeExplicit, bool ___includeInherited, Type_t * ___targetType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.CommonObjectSecurity::AddAccessRule(System.Security.AccessControl.AccessRule)
extern "C" void CommonObjectSecurity_AddAccessRule_m1_9745 (CommonObjectSecurity_t1_1126 * __this, AccessRule_t1_1111 * ___rule, const MethodInfo* method)
{
	{
		List_1_t1_1128 * L_0 = (__this->___access_rules_6);
		AccessRule_t1_1111 * L_1 = ___rule;
		NullCheck(L_0);
		VirtActionInvoker1< AccessRule_t1_1111 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Security.AccessControl.AccessRule>::Add(T) */, L_0, L_1);
		ObjectSecurity_set_AccessRulesModified_m1_9995(__this, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.Security.AccessControl.CommonObjectSecurity::RemoveAccessRule(System.Security.AccessControl.AccessRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" bool CommonObjectSecurity_RemoveAccessRule_m1_9746 (CommonObjectSecurity_t1_1126 * __this, AccessRule_t1_1111 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.CommonObjectSecurity::RemoveAccessRuleAll(System.Security.AccessControl.AccessRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void CommonObjectSecurity_RemoveAccessRuleAll_m1_9747 (CommonObjectSecurity_t1_1126 * __this, AccessRule_t1_1111 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.CommonObjectSecurity::RemoveAccessRuleSpecific(System.Security.AccessControl.AccessRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void CommonObjectSecurity_RemoveAccessRuleSpecific_m1_9748 (CommonObjectSecurity_t1_1126 * __this, AccessRule_t1_1111 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.CommonObjectSecurity::ResetAccessRule(System.Security.AccessControl.AccessRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void CommonObjectSecurity_ResetAccessRule_m1_9749 (CommonObjectSecurity_t1_1126 * __this, AccessRule_t1_1111 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.CommonObjectSecurity::SetAccessRule(System.Security.AccessControl.AccessRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void CommonObjectSecurity_SetAccessRule_m1_9750 (CommonObjectSecurity_t1_1126 * __this, AccessRule_t1_1111 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Security.AccessControl.CommonObjectSecurity::ModifyAccess(System.Security.AccessControl.AccessControlModification,System.Security.AccessControl.AccessRule,System.Boolean&)
extern TypeInfo* Enumerator_t1_1782_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m1_14900_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1_14901_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1_14902_MethodInfo_var;
extern "C" bool CommonObjectSecurity_ModifyAccess_m1_9751 (CommonObjectSecurity_t1_1126 * __this, int32_t ___modification, AccessRule_t1_1111 * ___rule, bool* ___modified, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Enumerator_t1_1782_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(847);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		List_1_GetEnumerator_m1_14900_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483692);
		Enumerator_get_Current_m1_14901_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483693);
		Enumerator_MoveNext_m1_14902_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483694);
		s_Il2CppMethodIntialized = true;
	}
	AccessRule_t1_1111 * V_0 = {0};
	Enumerator_t1_1782  V_1 = {0};
	int32_t V_2 = {0};
	bool V_3 = false;
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t1_1128 * L_0 = (__this->___access_rules_6);
		NullCheck(L_0);
		Enumerator_t1_1782  L_1 = List_1_GetEnumerator_m1_14900(L_0, /*hidden argument*/List_1_GetEnumerator_m1_14900_MethodInfo_var);
		V_1 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_009d;
		}

IL_0011:
		{
			AccessRule_t1_1111 * L_2 = Enumerator_get_Current_m1_14901((&V_1), /*hidden argument*/Enumerator_get_Current_m1_14901_MethodInfo_var);
			V_0 = L_2;
			AccessRule_t1_1111 * L_3 = ___rule;
			AccessRule_t1_1111 * L_4 = V_0;
			if ((((Object_t*)(AccessRule_t1_1111 *)L_3) == ((Object_t*)(AccessRule_t1_1111 *)L_4)))
			{
				goto IL_0025;
			}
		}

IL_0020:
		{
			goto IL_009d;
		}

IL_0025:
		{
			int32_t L_5 = ___modification;
			V_2 = L_5;
			int32_t L_6 = V_2;
			if (L_6 == 0)
			{
				goto IL_004a;
			}
			if (L_6 == 1)
			{
				goto IL_0056;
			}
			if (L_6 == 2)
			{
				goto IL_0062;
			}
			if (L_6 == 3)
			{
				goto IL_006e;
			}
			if (L_6 == 4)
			{
				goto IL_007b;
			}
			if (L_6 == 5)
			{
				goto IL_0087;
			}
		}

IL_0045:
		{
			goto IL_0093;
		}

IL_004a:
		{
			AccessRule_t1_1111 * L_7 = ___rule;
			CommonObjectSecurity_AddAccessRule_m1_9745(__this, L_7, /*hidden argument*/NULL);
			goto IL_0093;
		}

IL_0056:
		{
			AccessRule_t1_1111 * L_8 = ___rule;
			CommonObjectSecurity_SetAccessRule_m1_9750(__this, L_8, /*hidden argument*/NULL);
			goto IL_0093;
		}

IL_0062:
		{
			AccessRule_t1_1111 * L_9 = ___rule;
			CommonObjectSecurity_ResetAccessRule_m1_9749(__this, L_9, /*hidden argument*/NULL);
			goto IL_0093;
		}

IL_006e:
		{
			AccessRule_t1_1111 * L_10 = ___rule;
			CommonObjectSecurity_RemoveAccessRule_m1_9746(__this, L_10, /*hidden argument*/NULL);
			goto IL_0093;
		}

IL_007b:
		{
			AccessRule_t1_1111 * L_11 = ___rule;
			CommonObjectSecurity_RemoveAccessRuleAll_m1_9747(__this, L_11, /*hidden argument*/NULL);
			goto IL_0093;
		}

IL_0087:
		{
			AccessRule_t1_1111 * L_12 = ___rule;
			CommonObjectSecurity_RemoveAccessRuleSpecific_m1_9748(__this, L_12, /*hidden argument*/NULL);
			goto IL_0093;
		}

IL_0093:
		{
			bool* L_13 = ___modified;
			*((int8_t*)(L_13)) = (int8_t)1;
			V_3 = 1;
			IL2CPP_LEAVE(0xBF, FINALLY_00ae);
		}

IL_009d:
		{
			bool L_14 = Enumerator_MoveNext_m1_14902((&V_1), /*hidden argument*/Enumerator_MoveNext_m1_14902_MethodInfo_var);
			if (L_14)
			{
				goto IL_0011;
			}
		}

IL_00a9:
		{
			IL2CPP_LEAVE(0xBA, FINALLY_00ae);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_00ae;
	}

FINALLY_00ae:
	{ // begin finally (depth: 1)
		Enumerator_t1_1782  L_15 = V_1;
		Enumerator_t1_1782  L_16 = L_15;
		Object_t * L_17 = Box(Enumerator_t1_1782_il2cpp_TypeInfo_var, &L_16);
		NullCheck(L_17);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_17);
		IL2CPP_END_FINALLY(174)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(174)
	{
		IL2CPP_JUMP_TBL(0xBF, IL_00bf)
		IL2CPP_JUMP_TBL(0xBA, IL_00ba)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_00ba:
	{
		bool* L_18 = ___modified;
		*((int8_t*)(L_18)) = (int8_t)0;
		return 0;
	}

IL_00bf:
	{
		bool L_19 = V_3;
		return L_19;
	}
}
// System.Void System.Security.AccessControl.CommonObjectSecurity::AddAuditRule(System.Security.AccessControl.AuditRule)
extern "C" void CommonObjectSecurity_AddAuditRule_m1_9752 (CommonObjectSecurity_t1_1126 * __this, AuditRule_t1_1119 * ___rule, const MethodInfo* method)
{
	{
		List_1_t1_1129 * L_0 = (__this->___audit_rules_7);
		AuditRule_t1_1119 * L_1 = ___rule;
		NullCheck(L_0);
		VirtActionInvoker1< AuditRule_t1_1119 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Security.AccessControl.AuditRule>::Add(T) */, L_0, L_1);
		ObjectSecurity_set_AuditRulesModified_m1_9997(__this, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.Security.AccessControl.CommonObjectSecurity::RemoveAuditRule(System.Security.AccessControl.AuditRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" bool CommonObjectSecurity_RemoveAuditRule_m1_9753 (CommonObjectSecurity_t1_1126 * __this, AuditRule_t1_1119 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.CommonObjectSecurity::RemoveAuditRuleAll(System.Security.AccessControl.AuditRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void CommonObjectSecurity_RemoveAuditRuleAll_m1_9754 (CommonObjectSecurity_t1_1126 * __this, AuditRule_t1_1119 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.CommonObjectSecurity::RemoveAuditRuleSpecific(System.Security.AccessControl.AuditRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void CommonObjectSecurity_RemoveAuditRuleSpecific_m1_9755 (CommonObjectSecurity_t1_1126 * __this, AuditRule_t1_1119 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.CommonObjectSecurity::SetAuditRule(System.Security.AccessControl.AuditRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void CommonObjectSecurity_SetAuditRule_m1_9756 (CommonObjectSecurity_t1_1126 * __this, AuditRule_t1_1119 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Security.AccessControl.CommonObjectSecurity::ModifyAudit(System.Security.AccessControl.AccessControlModification,System.Security.AccessControl.AuditRule,System.Boolean&)
extern TypeInfo* Enumerator_t1_1783_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m1_14903_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1_14904_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1_14905_MethodInfo_var;
extern "C" bool CommonObjectSecurity_ModifyAudit_m1_9757 (CommonObjectSecurity_t1_1126 * __this, int32_t ___modification, AuditRule_t1_1119 * ___rule, bool* ___modified, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Enumerator_t1_1783_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(848);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		List_1_GetEnumerator_m1_14903_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483695);
		Enumerator_get_Current_m1_14904_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483696);
		Enumerator_MoveNext_m1_14905_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483697);
		s_Il2CppMethodIntialized = true;
	}
	AuditRule_t1_1119 * V_0 = {0};
	Enumerator_t1_1783  V_1 = {0};
	int32_t V_2 = {0};
	bool V_3 = false;
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t1_1129 * L_0 = (__this->___audit_rules_7);
		NullCheck(L_0);
		Enumerator_t1_1783  L_1 = List_1_GetEnumerator_m1_14903(L_0, /*hidden argument*/List_1_GetEnumerator_m1_14903_MethodInfo_var);
		V_1 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0098;
		}

IL_0011:
		{
			AuditRule_t1_1119 * L_2 = Enumerator_get_Current_m1_14904((&V_1), /*hidden argument*/Enumerator_get_Current_m1_14904_MethodInfo_var);
			V_0 = L_2;
			AuditRule_t1_1119 * L_3 = ___rule;
			AuditRule_t1_1119 * L_4 = V_0;
			if ((((Object_t*)(AuditRule_t1_1119 *)L_3) == ((Object_t*)(AuditRule_t1_1119 *)L_4)))
			{
				goto IL_0025;
			}
		}

IL_0020:
		{
			goto IL_0098;
		}

IL_0025:
		{
			int32_t L_5 = ___modification;
			V_2 = L_5;
			int32_t L_6 = V_2;
			if (L_6 == 0)
			{
				goto IL_004a;
			}
			if (L_6 == 1)
			{
				goto IL_0056;
			}
			if (L_6 == 2)
			{
				goto IL_0087;
			}
			if (L_6 == 3)
			{
				goto IL_0062;
			}
			if (L_6 == 4)
			{
				goto IL_006f;
			}
			if (L_6 == 5)
			{
				goto IL_007b;
			}
		}

IL_0045:
		{
			goto IL_0087;
		}

IL_004a:
		{
			AuditRule_t1_1119 * L_7 = ___rule;
			CommonObjectSecurity_AddAuditRule_m1_9752(__this, L_7, /*hidden argument*/NULL);
			goto IL_0087;
		}

IL_0056:
		{
			AuditRule_t1_1119 * L_8 = ___rule;
			CommonObjectSecurity_SetAuditRule_m1_9756(__this, L_8, /*hidden argument*/NULL);
			goto IL_0087;
		}

IL_0062:
		{
			AuditRule_t1_1119 * L_9 = ___rule;
			CommonObjectSecurity_RemoveAuditRule_m1_9753(__this, L_9, /*hidden argument*/NULL);
			goto IL_0087;
		}

IL_006f:
		{
			AuditRule_t1_1119 * L_10 = ___rule;
			CommonObjectSecurity_RemoveAuditRuleAll_m1_9754(__this, L_10, /*hidden argument*/NULL);
			goto IL_0087;
		}

IL_007b:
		{
			AuditRule_t1_1119 * L_11 = ___rule;
			CommonObjectSecurity_RemoveAuditRuleSpecific_m1_9755(__this, L_11, /*hidden argument*/NULL);
			goto IL_0087;
		}

IL_0087:
		{
			ObjectSecurity_set_AuditRulesModified_m1_9997(__this, 1, /*hidden argument*/NULL);
			bool* L_12 = ___modified;
			*((int8_t*)(L_12)) = (int8_t)1;
			V_3 = 1;
			IL2CPP_LEAVE(0xBA, FINALLY_00a9);
		}

IL_0098:
		{
			bool L_13 = Enumerator_MoveNext_m1_14905((&V_1), /*hidden argument*/Enumerator_MoveNext_m1_14905_MethodInfo_var);
			if (L_13)
			{
				goto IL_0011;
			}
		}

IL_00a4:
		{
			IL2CPP_LEAVE(0xB5, FINALLY_00a9);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_00a9;
	}

FINALLY_00a9:
	{ // begin finally (depth: 1)
		Enumerator_t1_1783  L_14 = V_1;
		Enumerator_t1_1783  L_15 = L_14;
		Object_t * L_16 = Box(Enumerator_t1_1783_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_16);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_16);
		IL2CPP_END_FINALLY(169)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(169)
	{
		IL2CPP_JUMP_TBL(0xBA, IL_00ba)
		IL2CPP_JUMP_TBL(0xB5, IL_00b5)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_00b5:
	{
		bool* L_17 = ___modified;
		*((int8_t*)(L_17)) = (int8_t)0;
		return 0;
	}

IL_00ba:
	{
		bool L_18 = V_3;
		return L_18;
	}
}
// System.Void System.Security.AccessControl.CommonSecurityDescriptor::.ctor(System.Boolean,System.Boolean,System.Security.AccessControl.RawSecurityDescriptor)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void CommonSecurityDescriptor__ctor_m1_9758 (CommonSecurityDescriptor_t1_1130 * __this, bool ___isContainer, bool ___isDS, RawSecurityDescriptor_t1_1171 * ___rawSecurityDescriptor, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		GenericSecurityDescriptor__ctor_m1_9921(__this, /*hidden argument*/NULL);
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.CommonSecurityDescriptor::.ctor(System.Boolean,System.Boolean,System.String)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void CommonSecurityDescriptor__ctor_m1_9759 (CommonSecurityDescriptor_t1_1130 * __this, bool ___isContainer, bool ___isDS, String_t* ___sddlForm, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		GenericSecurityDescriptor__ctor_m1_9921(__this, /*hidden argument*/NULL);
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.CommonSecurityDescriptor::.ctor(System.Boolean,System.Boolean,System.Byte[],System.Int32)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void CommonSecurityDescriptor__ctor_m1_9760 (CommonSecurityDescriptor_t1_1130 * __this, bool ___isContainer, bool ___isDS, ByteU5BU5D_t1_109* ___binaryForm, int32_t ___offset, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		GenericSecurityDescriptor__ctor_m1_9921(__this, /*hidden argument*/NULL);
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.CommonSecurityDescriptor::.ctor(System.Boolean,System.Boolean,System.Security.AccessControl.ControlFlags,System.Security.Principal.SecurityIdentifier,System.Security.Principal.SecurityIdentifier,System.Security.AccessControl.SystemAcl,System.Security.AccessControl.DiscretionaryAcl)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void CommonSecurityDescriptor__ctor_m1_9761 (CommonSecurityDescriptor_t1_1130 * __this, bool ___isContainer, bool ___isDS, int32_t ___flags, SecurityIdentifier_t1_1132 * ___owner, SecurityIdentifier_t1_1132 * ___group, SystemAcl_t1_1133 * ___systemAcl, DiscretionaryAcl_t1_1134 * ___discretionaryAcl, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		GenericSecurityDescriptor__ctor_m1_9921(__this, /*hidden argument*/NULL);
		bool L_0 = ___isContainer;
		__this->___isContainer_0 = L_0;
		bool L_1 = ___isDS;
		__this->___isDS_1 = L_1;
		int32_t L_2 = ___flags;
		__this->___flags_2 = L_2;
		SecurityIdentifier_t1_1132 * L_3 = ___owner;
		__this->___owner_3 = L_3;
		SecurityIdentifier_t1_1132 * L_4 = ___group;
		__this->___group_4 = L_4;
		SystemAcl_t1_1133 * L_5 = ___systemAcl;
		__this->___systemAcl_5 = L_5;
		DiscretionaryAcl_t1_1134 * L_6 = ___discretionaryAcl;
		__this->___discretionaryAcl_6 = L_6;
		NotImplementedException_t1_1582 * L_7 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_7, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_7);
	}
}
// System.Security.AccessControl.ControlFlags System.Security.AccessControl.CommonSecurityDescriptor::get_ControlFlags()
extern "C" int32_t CommonSecurityDescriptor_get_ControlFlags_m1_9762 (CommonSecurityDescriptor_t1_1130 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___flags_2);
		return L_0;
	}
}
// System.Security.AccessControl.DiscretionaryAcl System.Security.AccessControl.CommonSecurityDescriptor::get_DiscretionaryAcl()
extern "C" DiscretionaryAcl_t1_1134 * CommonSecurityDescriptor_get_DiscretionaryAcl_m1_9763 (CommonSecurityDescriptor_t1_1130 * __this, const MethodInfo* method)
{
	{
		DiscretionaryAcl_t1_1134 * L_0 = (__this->___discretionaryAcl_6);
		return L_0;
	}
}
// System.Void System.Security.AccessControl.CommonSecurityDescriptor::set_DiscretionaryAcl(System.Security.AccessControl.DiscretionaryAcl)
extern "C" void CommonSecurityDescriptor_set_DiscretionaryAcl_m1_9764 (CommonSecurityDescriptor_t1_1130 * __this, DiscretionaryAcl_t1_1134 * ___value, const MethodInfo* method)
{
	{
		DiscretionaryAcl_t1_1134 * L_0 = ___value;
		if (L_0)
		{
			goto IL_0006;
		}
	}

IL_0006:
	{
		DiscretionaryAcl_t1_1134 * L_1 = ___value;
		__this->___discretionaryAcl_6 = L_1;
		return;
	}
}
// System.Security.Principal.SecurityIdentifier System.Security.AccessControl.CommonSecurityDescriptor::get_Group()
extern "C" SecurityIdentifier_t1_1132 * CommonSecurityDescriptor_get_Group_m1_9765 (CommonSecurityDescriptor_t1_1130 * __this, const MethodInfo* method)
{
	{
		SecurityIdentifier_t1_1132 * L_0 = (__this->___group_4);
		return L_0;
	}
}
// System.Void System.Security.AccessControl.CommonSecurityDescriptor::set_Group(System.Security.Principal.SecurityIdentifier)
extern "C" void CommonSecurityDescriptor_set_Group_m1_9766 (CommonSecurityDescriptor_t1_1130 * __this, SecurityIdentifier_t1_1132 * ___value, const MethodInfo* method)
{
	{
		SecurityIdentifier_t1_1132 * L_0 = ___value;
		__this->___group_4 = L_0;
		return;
	}
}
// System.Boolean System.Security.AccessControl.CommonSecurityDescriptor::get_IsContainer()
extern "C" bool CommonSecurityDescriptor_get_IsContainer_m1_9767 (CommonSecurityDescriptor_t1_1130 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___isContainer_0);
		return L_0;
	}
}
// System.Boolean System.Security.AccessControl.CommonSecurityDescriptor::get_IsDiscretionaryAclCanonical()
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" bool CommonSecurityDescriptor_get_IsDiscretionaryAclCanonical_m1_9768 (CommonSecurityDescriptor_t1_1130 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Security.AccessControl.CommonSecurityDescriptor::get_IsDS()
extern "C" bool CommonSecurityDescriptor_get_IsDS_m1_9769 (CommonSecurityDescriptor_t1_1130 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___isDS_1);
		return L_0;
	}
}
// System.Boolean System.Security.AccessControl.CommonSecurityDescriptor::get_IsSystemAclCanonical()
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" bool CommonSecurityDescriptor_get_IsSystemAclCanonical_m1_9770 (CommonSecurityDescriptor_t1_1130 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Security.Principal.SecurityIdentifier System.Security.AccessControl.CommonSecurityDescriptor::get_Owner()
extern "C" SecurityIdentifier_t1_1132 * CommonSecurityDescriptor_get_Owner_m1_9771 (CommonSecurityDescriptor_t1_1130 * __this, const MethodInfo* method)
{
	{
		SecurityIdentifier_t1_1132 * L_0 = (__this->___owner_3);
		return L_0;
	}
}
// System.Void System.Security.AccessControl.CommonSecurityDescriptor::set_Owner(System.Security.Principal.SecurityIdentifier)
extern "C" void CommonSecurityDescriptor_set_Owner_m1_9772 (CommonSecurityDescriptor_t1_1130 * __this, SecurityIdentifier_t1_1132 * ___value, const MethodInfo* method)
{
	{
		SecurityIdentifier_t1_1132 * L_0 = ___value;
		__this->___owner_3 = L_0;
		return;
	}
}
// System.Security.AccessControl.SystemAcl System.Security.AccessControl.CommonSecurityDescriptor::get_SystemAcl()
extern "C" SystemAcl_t1_1133 * CommonSecurityDescriptor_get_SystemAcl_m1_9773 (CommonSecurityDescriptor_t1_1130 * __this, const MethodInfo* method)
{
	{
		SystemAcl_t1_1133 * L_0 = (__this->___systemAcl_5);
		return L_0;
	}
}
// System.Void System.Security.AccessControl.CommonSecurityDescriptor::set_SystemAcl(System.Security.AccessControl.SystemAcl)
extern "C" void CommonSecurityDescriptor_set_SystemAcl_m1_9774 (CommonSecurityDescriptor_t1_1130 * __this, SystemAcl_t1_1133 * ___value, const MethodInfo* method)
{
	{
		SystemAcl_t1_1133 * L_0 = ___value;
		__this->___systemAcl_5 = L_0;
		return;
	}
}
// System.Void System.Security.AccessControl.CommonSecurityDescriptor::PurgeAccessControl(System.Security.Principal.SecurityIdentifier)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void CommonSecurityDescriptor_PurgeAccessControl_m1_9775 (CommonSecurityDescriptor_t1_1130 * __this, SecurityIdentifier_t1_1132 * ___sid, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.CommonSecurityDescriptor::PurgeAudit(System.Security.Principal.SecurityIdentifier)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void CommonSecurityDescriptor_PurgeAudit_m1_9776 (CommonSecurityDescriptor_t1_1130 * __this, SecurityIdentifier_t1_1132 * ___sid, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.CommonSecurityDescriptor::SetDiscretionaryAclProtection(System.Boolean,System.Boolean)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void CommonSecurityDescriptor_SetDiscretionaryAclProtection_m1_9777 (CommonSecurityDescriptor_t1_1130 * __this, bool ___isProtected, bool ___preserveInheritance, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.CommonSecurityDescriptor::SetSystemAclProtection(System.Boolean,System.Boolean)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void CommonSecurityDescriptor_SetSystemAclProtection_m1_9778 (CommonSecurityDescriptor_t1_1130 * __this, bool ___isProtected, bool ___preserveInheritance, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.CompoundAce::.ctor(System.Security.AccessControl.AceFlags,System.Int32,System.Security.AccessControl.CompoundAceType,System.Security.Principal.SecurityIdentifier)
extern "C" void CompoundAce__ctor_m1_9779 (CompoundAce_t1_1135 * __this, uint8_t ___flags, int32_t ___accessMask, int32_t ___compoundAceType, SecurityIdentifier_t1_1132 * ___sid, const MethodInfo* method)
{
	{
		KnownAce__ctor_m1_9927(__this, 0, 0, /*hidden argument*/NULL);
		int32_t L_0 = ___compoundAceType;
		__this->___compound_ace_type_6 = L_0;
		uint8_t L_1 = ___flags;
		GenericAce_set_AceFlags_m1_9901(__this, L_1, /*hidden argument*/NULL);
		int32_t L_2 = ___accessMask;
		KnownAce_set_AccessMask_m1_9929(__this, L_2, /*hidden argument*/NULL);
		SecurityIdentifier_t1_1132 * L_3 = ___sid;
		KnownAce_set_SecurityIdentifier_m1_9931(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 System.Security.AccessControl.CompoundAce::get_BinaryLength()
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" int32_t CompoundAce_get_BinaryLength_m1_9780 (CompoundAce_t1_1135 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Security.AccessControl.CompoundAceType System.Security.AccessControl.CompoundAce::get_CompoundAceType()
extern "C" int32_t CompoundAce_get_CompoundAceType_m1_9781 (CompoundAce_t1_1135 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___compound_ace_type_6);
		return L_0;
	}
}
// System.Void System.Security.AccessControl.CompoundAce::set_CompoundAceType(System.Security.AccessControl.CompoundAceType)
extern "C" void CompoundAce_set_CompoundAceType_m1_9782 (CompoundAce_t1_1135 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___compound_ace_type_6 = L_0;
		return;
	}
}
// System.Void System.Security.AccessControl.CompoundAce::GetBinaryForm(System.Byte[],System.Int32)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void CompoundAce_GetBinaryForm_m1_9783 (CompoundAce_t1_1135 * __this, ByteU5BU5D_t1_109* ___binaryForm, int32_t ___offset, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.CryptoKeyAccessRule::.ctor(System.Security.Principal.IdentityReference,System.Security.AccessControl.CryptoKeyRights,System.Security.AccessControl.AccessControlType)
extern "C" void CryptoKeyAccessRule__ctor_m1_9784 (CryptoKeyAccessRule_t1_1139 * __this, IdentityReference_t1_1120 * ___identity, int32_t ___cryptoKeyRights, int32_t ___type, const MethodInfo* method)
{
	{
		IdentityReference_t1_1120 * L_0 = ___identity;
		AccessRule__ctor_m1_9706(__this, L_0, 0, 0, 0, 0, 0, /*hidden argument*/NULL);
		int32_t L_1 = ___cryptoKeyRights;
		__this->___rights_6 = L_1;
		return;
	}
}
// System.Void System.Security.AccessControl.CryptoKeyAccessRule::.ctor(System.String,System.Security.AccessControl.CryptoKeyRights,System.Security.AccessControl.AccessControlType)
extern TypeInfo* SecurityIdentifier_t1_1132_il2cpp_TypeInfo_var;
extern "C" void CryptoKeyAccessRule__ctor_m1_9785 (CryptoKeyAccessRule_t1_1139 * __this, String_t* ___identity, int32_t ___cryptoKeyRights, int32_t ___type, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SecurityIdentifier_t1_1132_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(838);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___identity;
		SecurityIdentifier_t1_1132 * L_1 = (SecurityIdentifier_t1_1132 *)il2cpp_codegen_object_new (SecurityIdentifier_t1_1132_il2cpp_TypeInfo_var);
		SecurityIdentifier__ctor_m1_11825(L_1, L_0, /*hidden argument*/NULL);
		int32_t L_2 = ___cryptoKeyRights;
		int32_t L_3 = ___type;
		CryptoKeyAccessRule__ctor_m1_9784(__this, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Security.AccessControl.CryptoKeyRights System.Security.AccessControl.CryptoKeyAccessRule::get_CryptoKeyRights()
extern "C" int32_t CryptoKeyAccessRule_get_CryptoKeyRights_m1_9786 (CryptoKeyAccessRule_t1_1139 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___rights_6);
		return L_0;
	}
}
// System.Void System.Security.AccessControl.CryptoKeyAuditRule::.ctor(System.Security.Principal.IdentityReference,System.Security.AccessControl.CryptoKeyRights,System.Security.AccessControl.AuditFlags)
extern "C" void CryptoKeyAuditRule__ctor_m1_9787 (CryptoKeyAuditRule_t1_1140 * __this, IdentityReference_t1_1120 * ___identity, int32_t ___cryptoKeyRights, int32_t ___flags, const MethodInfo* method)
{
	{
		IdentityReference_t1_1120 * L_0 = ___identity;
		int32_t L_1 = ___flags;
		AuditRule__ctor_m1_9713(__this, L_0, 0, 0, 0, 0, L_1, /*hidden argument*/NULL);
		int32_t L_2 = ___cryptoKeyRights;
		__this->___rights_6 = L_2;
		return;
	}
}
// System.Void System.Security.AccessControl.CryptoKeyAuditRule::.ctor(System.String,System.Security.AccessControl.CryptoKeyRights,System.Security.AccessControl.AuditFlags)
extern TypeInfo* SecurityIdentifier_t1_1132_il2cpp_TypeInfo_var;
extern "C" void CryptoKeyAuditRule__ctor_m1_9788 (CryptoKeyAuditRule_t1_1140 * __this, String_t* ___identity, int32_t ___cryptoKeyRights, int32_t ___flags, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SecurityIdentifier_t1_1132_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(838);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___identity;
		SecurityIdentifier_t1_1132 * L_1 = (SecurityIdentifier_t1_1132 *)il2cpp_codegen_object_new (SecurityIdentifier_t1_1132_il2cpp_TypeInfo_var);
		SecurityIdentifier__ctor_m1_11825(L_1, L_0, /*hidden argument*/NULL);
		int32_t L_2 = ___cryptoKeyRights;
		int32_t L_3 = ___flags;
		CryptoKeyAuditRule__ctor_m1_9787(__this, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Security.AccessControl.CryptoKeyRights System.Security.AccessControl.CryptoKeyAuditRule::get_CryptoKeyRights()
extern "C" int32_t CryptoKeyAuditRule_get_CryptoKeyRights_m1_9789 (CryptoKeyAuditRule_t1_1140 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___rights_6);
		return L_0;
	}
}
// System.Void System.Security.AccessControl.CryptoKeySecurity::.ctor()
extern "C" void CryptoKeySecurity__ctor_m1_9790 (CryptoKeySecurity_t1_1142 * __this, const MethodInfo* method)
{
	{
		NativeObjectSecurity__ctor_m1_9959(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.AccessControl.CryptoKeySecurity::.ctor(System.Security.AccessControl.CommonSecurityDescriptor)
extern "C" void CryptoKeySecurity__ctor_m1_9791 (CryptoKeySecurity_t1_1142 * __this, CommonSecurityDescriptor_t1_1130 * ___securityDescriptor, const MethodInfo* method)
{
	{
		NativeObjectSecurity__ctor_m1_9959(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Type System.Security.AccessControl.CryptoKeySecurity::get_AccessRightType()
extern const Il2CppType* CryptoKeyRights_t1_1141_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern "C" Type_t * CryptoKeySecurity_get_AccessRightType_m1_9792 (CryptoKeySecurity_t1_1142 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CryptoKeyRights_t1_1141_0_0_0_var = il2cpp_codegen_type_from_index(849);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(CryptoKeyRights_t1_1141_0_0_0_var), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Type System.Security.AccessControl.CryptoKeySecurity::get_AccessRuleType()
extern const Il2CppType* CryptoKeyAccessRule_t1_1139_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern "C" Type_t * CryptoKeySecurity_get_AccessRuleType_m1_9793 (CryptoKeySecurity_t1_1142 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CryptoKeyAccessRule_t1_1139_0_0_0_var = il2cpp_codegen_type_from_index(850);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(CryptoKeyAccessRule_t1_1139_0_0_0_var), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Type System.Security.AccessControl.CryptoKeySecurity::get_AuditRuleType()
extern const Il2CppType* CryptoKeyAuditRule_t1_1140_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern "C" Type_t * CryptoKeySecurity_get_AuditRuleType_m1_9794 (CryptoKeySecurity_t1_1142 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CryptoKeyAuditRule_t1_1140_0_0_0_var = il2cpp_codegen_type_from_index(851);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(CryptoKeyAuditRule_t1_1140_0_0_0_var), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Security.AccessControl.AccessRule System.Security.AccessControl.CryptoKeySecurity::AccessRuleFactory(System.Security.Principal.IdentityReference,System.Int32,System.Boolean,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.AccessControlType)
extern TypeInfo* CryptoKeyAccessRule_t1_1139_il2cpp_TypeInfo_var;
extern "C" AccessRule_t1_1111 * CryptoKeySecurity_AccessRuleFactory_m1_9795 (CryptoKeySecurity_t1_1142 * __this, IdentityReference_t1_1120 * ___identityReference, int32_t ___accessMask, bool ___isInherited, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___type, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CryptoKeyAccessRule_t1_1139_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(850);
		s_Il2CppMethodIntialized = true;
	}
	{
		IdentityReference_t1_1120 * L_0 = ___identityReference;
		int32_t L_1 = ___accessMask;
		int32_t L_2 = ___type;
		CryptoKeyAccessRule_t1_1139 * L_3 = (CryptoKeyAccessRule_t1_1139 *)il2cpp_codegen_object_new (CryptoKeyAccessRule_t1_1139_il2cpp_TypeInfo_var);
		CryptoKeyAccessRule__ctor_m1_9784(L_3, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void System.Security.AccessControl.CryptoKeySecurity::AddAccessRule(System.Security.AccessControl.CryptoKeyAccessRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void CryptoKeySecurity_AddAccessRule_m1_9796 (CryptoKeySecurity_t1_1142 * __this, CryptoKeyAccessRule_t1_1139 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Security.AccessControl.CryptoKeySecurity::RemoveAccessRule(System.Security.AccessControl.CryptoKeyAccessRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" bool CryptoKeySecurity_RemoveAccessRule_m1_9797 (CryptoKeySecurity_t1_1142 * __this, CryptoKeyAccessRule_t1_1139 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.CryptoKeySecurity::RemoveAccessRuleAll(System.Security.AccessControl.CryptoKeyAccessRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void CryptoKeySecurity_RemoveAccessRuleAll_m1_9798 (CryptoKeySecurity_t1_1142 * __this, CryptoKeyAccessRule_t1_1139 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.CryptoKeySecurity::RemoveAccessRuleSpecific(System.Security.AccessControl.CryptoKeyAccessRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void CryptoKeySecurity_RemoveAccessRuleSpecific_m1_9799 (CryptoKeySecurity_t1_1142 * __this, CryptoKeyAccessRule_t1_1139 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.CryptoKeySecurity::ResetAccessRule(System.Security.AccessControl.CryptoKeyAccessRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void CryptoKeySecurity_ResetAccessRule_m1_9800 (CryptoKeySecurity_t1_1142 * __this, CryptoKeyAccessRule_t1_1139 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.CryptoKeySecurity::SetAccessRule(System.Security.AccessControl.CryptoKeyAccessRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void CryptoKeySecurity_SetAccessRule_m1_9801 (CryptoKeySecurity_t1_1142 * __this, CryptoKeyAccessRule_t1_1139 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Security.AccessControl.AuditRule System.Security.AccessControl.CryptoKeySecurity::AuditRuleFactory(System.Security.Principal.IdentityReference,System.Int32,System.Boolean,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.AuditFlags)
extern TypeInfo* CryptoKeyAuditRule_t1_1140_il2cpp_TypeInfo_var;
extern "C" AuditRule_t1_1119 * CryptoKeySecurity_AuditRuleFactory_m1_9802 (CryptoKeySecurity_t1_1142 * __this, IdentityReference_t1_1120 * ___identityReference, int32_t ___accessMask, bool ___isInherited, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___flags, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CryptoKeyAuditRule_t1_1140_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(851);
		s_Il2CppMethodIntialized = true;
	}
	{
		IdentityReference_t1_1120 * L_0 = ___identityReference;
		int32_t L_1 = ___accessMask;
		int32_t L_2 = ___flags;
		CryptoKeyAuditRule_t1_1140 * L_3 = (CryptoKeyAuditRule_t1_1140 *)il2cpp_codegen_object_new (CryptoKeyAuditRule_t1_1140_il2cpp_TypeInfo_var);
		CryptoKeyAuditRule__ctor_m1_9787(L_3, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void System.Security.AccessControl.CryptoKeySecurity::AddAuditRule(System.Security.AccessControl.CryptoKeyAuditRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void CryptoKeySecurity_AddAuditRule_m1_9803 (CryptoKeySecurity_t1_1142 * __this, CryptoKeyAuditRule_t1_1140 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Security.AccessControl.CryptoKeySecurity::RemoveAuditRule(System.Security.AccessControl.CryptoKeyAuditRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" bool CryptoKeySecurity_RemoveAuditRule_m1_9804 (CryptoKeySecurity_t1_1142 * __this, CryptoKeyAuditRule_t1_1140 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.CryptoKeySecurity::RemoveAuditRuleAll(System.Security.AccessControl.CryptoKeyAuditRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void CryptoKeySecurity_RemoveAuditRuleAll_m1_9805 (CryptoKeySecurity_t1_1142 * __this, CryptoKeyAuditRule_t1_1140 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.CryptoKeySecurity::RemoveAuditRuleSpecific(System.Security.AccessControl.CryptoKeyAuditRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void CryptoKeySecurity_RemoveAuditRuleSpecific_m1_9806 (CryptoKeySecurity_t1_1142 * __this, CryptoKeyAuditRule_t1_1140 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.CryptoKeySecurity::SetAuditRule(System.Security.AccessControl.CryptoKeyAuditRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void CryptoKeySecurity_SetAuditRule_m1_9807 (CryptoKeySecurity_t1_1142 * __this, CryptoKeyAuditRule_t1_1140 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.CustomAce::.ctor(System.Security.AccessControl.AceType,System.Security.AccessControl.AceFlags,System.Byte[])
extern "C" void CustomAce__ctor_m1_9808 (CustomAce_t1_1144 * __this, int32_t ___type, uint8_t ___flags, ByteU5BU5D_t1_109* ___opaque, const MethodInfo* method)
{
	{
		int32_t L_0 = ___type;
		GenericAce__ctor_m1_9899(__this, L_0, /*hidden argument*/NULL);
		uint8_t L_1 = ___flags;
		GenericAce_set_AceFlags_m1_9901(__this, L_1, /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_2 = ___opaque;
		CustomAce_SetOpaque_m1_9813(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 System.Security.AccessControl.CustomAce::get_BinaryLength()
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" int32_t CustomAce_get_BinaryLength_m1_9809 (CustomAce_t1_1144 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Int32 System.Security.AccessControl.CustomAce::get_OpaqueLength()
extern "C" int32_t CustomAce_get_OpaqueLength_m1_9810 (CustomAce_t1_1144 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t1_109* L_0 = (__this->___opaque_4);
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Array_t *)L_0)->max_length))));
	}
}
// System.Void System.Security.AccessControl.CustomAce::GetBinaryForm(System.Byte[],System.Int32)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void CustomAce_GetBinaryForm_m1_9811 (CustomAce_t1_1144 * __this, ByteU5BU5D_t1_109* ___binaryForm, int32_t ___offset, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Byte[] System.Security.AccessControl.CustomAce::GetOpaque()
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern "C" ByteU5BU5D_t1_109* CustomAce_GetOpaque_m1_9812 (CustomAce_t1_1144 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		s_Il2CppMethodIntialized = true;
	}
	{
		ByteU5BU5D_t1_109* L_0 = (__this->___opaque_4);
		NullCheck(L_0);
		Object_t * L_1 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(5 /* System.Object System.Array::Clone() */, L_0);
		return ((ByteU5BU5D_t1_109*)Castclass(L_1, ByteU5BU5D_t1_109_il2cpp_TypeInfo_var));
	}
}
// System.Void System.Security.AccessControl.CustomAce::SetOpaque(System.Byte[])
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2528;
extern "C" void CustomAce_SetOpaque_m1_9813 (CustomAce_t1_1144 * __this, ByteU5BU5D_t1_109* ___opaque, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		_stringLiteral2528 = il2cpp_codegen_string_literal_from_index(2528);
		s_Il2CppMethodIntialized = true;
	}
	{
		ByteU5BU5D_t1_109* L_0 = ___opaque;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, _stringLiteral2528, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		ByteU5BU5D_t1_109* L_2 = ___opaque;
		NullCheck(L_2);
		Object_t * L_3 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(5 /* System.Object System.Array::Clone() */, L_2);
		__this->___opaque_4 = ((ByteU5BU5D_t1_109*)Castclass(L_3, ByteU5BU5D_t1_109_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void System.Security.AccessControl.DirectoryObjectSecurity::.ctor()
extern "C" void DirectoryObjectSecurity__ctor_m1_9814 (DirectoryObjectSecurity_t1_1146 * __this, const MethodInfo* method)
{
	{
		ObjectSecurity__ctor_m1_9989(__this, 0, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.AccessControl.DirectoryObjectSecurity::.ctor(System.Security.AccessControl.CommonSecurityDescriptor)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2529;
extern "C" void DirectoryObjectSecurity__ctor_m1_9815 (DirectoryObjectSecurity_t1_1146 * __this, CommonSecurityDescriptor_t1_1130 * ___securityDescriptor, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		_stringLiteral2529 = il2cpp_codegen_string_literal_from_index(2529);
		s_Il2CppMethodIntialized = true;
	}
	DirectoryObjectSecurity_t1_1146 * G_B2_0 = {0};
	DirectoryObjectSecurity_t1_1146 * G_B1_0 = {0};
	int32_t G_B3_0 = 0;
	DirectoryObjectSecurity_t1_1146 * G_B3_1 = {0};
	{
		CommonSecurityDescriptor_t1_1130 * L_0 = ___securityDescriptor;
		G_B1_0 = __this;
		if (!L_0)
		{
			G_B2_0 = __this;
			goto IL_000f;
		}
	}
	{
		CommonSecurityDescriptor_t1_1130 * L_1 = ___securityDescriptor;
		NullCheck(L_1);
		bool L_2 = CommonSecurityDescriptor_get_IsContainer_m1_9767(L_1, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_2));
		G_B3_1 = G_B1_0;
		goto IL_0010;
	}

IL_000f:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
	}

IL_0010:
	{
		NullCheck(G_B3_1);
		ObjectSecurity__ctor_m1_9989(G_B3_1, G_B3_0, 1, /*hidden argument*/NULL);
		CommonSecurityDescriptor_t1_1130 * L_3 = ___securityDescriptor;
		if (L_3)
		{
			goto IL_0027;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_4 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_4, _stringLiteral2529, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
	}

IL_0027:
	{
		return;
	}
}
// System.Security.AccessControl.AccessRule System.Security.AccessControl.DirectoryObjectSecurity::AccessRuleFactory(System.Security.Principal.IdentityReference,System.Int32,System.Boolean,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.AccessControlType,System.Guid,System.Guid)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" AccessRule_t1_1111 * DirectoryObjectSecurity_AccessRuleFactory_m1_9816 (DirectoryObjectSecurity_t1_1146 * __this, IdentityReference_t1_1120 * ___identityReference, int32_t ___accessMask, bool ___isInherited, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___type, Guid_t1_319  ___objectType, Guid_t1_319  ___inheritedObjectType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Security.AccessControl.AuditRule System.Security.AccessControl.DirectoryObjectSecurity::AuditRuleFactory(System.Security.Principal.IdentityReference,System.Int32,System.Boolean,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.AuditFlags,System.Guid,System.Guid)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" AuditRule_t1_1119 * DirectoryObjectSecurity_AuditRuleFactory_m1_9817 (DirectoryObjectSecurity_t1_1146 * __this, IdentityReference_t1_1120 * ___identityReference, int32_t ___accessMask, bool ___isInherited, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___flags, Guid_t1_319  ___objectType, Guid_t1_319  ___inheritedObjectType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Security.AccessControl.AuthorizationRuleCollection System.Security.AccessControl.DirectoryObjectSecurity::GetAccessRules(System.Boolean,System.Boolean,System.Type)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" AuthorizationRuleCollection_t1_1121 * DirectoryObjectSecurity_GetAccessRules_m1_9818 (DirectoryObjectSecurity_t1_1146 * __this, bool ___includeExplicit, bool ___includeInherited, Type_t * ___targetType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Security.AccessControl.AuthorizationRuleCollection System.Security.AccessControl.DirectoryObjectSecurity::GetAuditRules(System.Boolean,System.Boolean,System.Type)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" AuthorizationRuleCollection_t1_1121 * DirectoryObjectSecurity_GetAuditRules_m1_9819 (DirectoryObjectSecurity_t1_1146 * __this, bool ___includeExplicit, bool ___includeInherited, Type_t * ___targetType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.DirectoryObjectSecurity::AddAccessRule(System.Security.AccessControl.ObjectAccessRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void DirectoryObjectSecurity_AddAccessRule_m1_9820 (DirectoryObjectSecurity_t1_1146 * __this, ObjectAccessRule_t1_1163 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.DirectoryObjectSecurity::AddAuditRule(System.Security.AccessControl.ObjectAuditRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void DirectoryObjectSecurity_AddAuditRule_m1_9821 (DirectoryObjectSecurity_t1_1146 * __this, ObjectAuditRule_t1_1166 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Security.AccessControl.DirectoryObjectSecurity::ModifyAccess(System.Security.AccessControl.AccessControlModification,System.Security.AccessControl.AccessRule,System.Boolean&)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" bool DirectoryObjectSecurity_ModifyAccess_m1_9822 (DirectoryObjectSecurity_t1_1146 * __this, int32_t ___modification, AccessRule_t1_1111 * ___rule, bool* ___modified, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Security.AccessControl.DirectoryObjectSecurity::ModifyAudit(System.Security.AccessControl.AccessControlModification,System.Security.AccessControl.AuditRule,System.Boolean&)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" bool DirectoryObjectSecurity_ModifyAudit_m1_9823 (DirectoryObjectSecurity_t1_1146 * __this, int32_t ___modification, AuditRule_t1_1119 * ___rule, bool* ___modified, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Security.AccessControl.DirectoryObjectSecurity::RemoveAccessRule(System.Security.AccessControl.ObjectAccessRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" bool DirectoryObjectSecurity_RemoveAccessRule_m1_9824 (DirectoryObjectSecurity_t1_1146 * __this, ObjectAccessRule_t1_1163 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.DirectoryObjectSecurity::RemoveAccessRuleAll(System.Security.AccessControl.ObjectAccessRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void DirectoryObjectSecurity_RemoveAccessRuleAll_m1_9825 (DirectoryObjectSecurity_t1_1146 * __this, ObjectAccessRule_t1_1163 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.DirectoryObjectSecurity::RemoveAccessRuleSpecific(System.Security.AccessControl.ObjectAccessRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void DirectoryObjectSecurity_RemoveAccessRuleSpecific_m1_9826 (DirectoryObjectSecurity_t1_1146 * __this, ObjectAccessRule_t1_1163 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Security.AccessControl.DirectoryObjectSecurity::RemoveAuditRule(System.Security.AccessControl.ObjectAuditRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" bool DirectoryObjectSecurity_RemoveAuditRule_m1_9827 (DirectoryObjectSecurity_t1_1146 * __this, ObjectAuditRule_t1_1166 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.DirectoryObjectSecurity::RemoveAuditRuleAll(System.Security.AccessControl.ObjectAuditRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void DirectoryObjectSecurity_RemoveAuditRuleAll_m1_9828 (DirectoryObjectSecurity_t1_1146 * __this, ObjectAuditRule_t1_1166 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.DirectoryObjectSecurity::RemoveAuditRuleSpecific(System.Security.AccessControl.ObjectAuditRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void DirectoryObjectSecurity_RemoveAuditRuleSpecific_m1_9829 (DirectoryObjectSecurity_t1_1146 * __this, ObjectAuditRule_t1_1166 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.DirectoryObjectSecurity::ResetAccessRule(System.Security.AccessControl.ObjectAccessRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void DirectoryObjectSecurity_ResetAccessRule_m1_9830 (DirectoryObjectSecurity_t1_1146 * __this, ObjectAccessRule_t1_1163 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.DirectoryObjectSecurity::SetAccessRule(System.Security.AccessControl.ObjectAccessRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void DirectoryObjectSecurity_SetAccessRule_m1_9831 (DirectoryObjectSecurity_t1_1146 * __this, ObjectAccessRule_t1_1163 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.DirectoryObjectSecurity::SetAuditRule(System.Security.AccessControl.ObjectAuditRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void DirectoryObjectSecurity_SetAuditRule_m1_9832 (DirectoryObjectSecurity_t1_1146 * __this, ObjectAuditRule_t1_1166 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.DirectorySecurity::.ctor()
extern TypeInfo* PlatformNotSupportedException_t1_1592_il2cpp_TypeInfo_var;
extern "C" void DirectorySecurity__ctor_m1_9833 (DirectorySecurity_t1_1147 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PlatformNotSupportedException_t1_1592_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(852);
		s_Il2CppMethodIntialized = true;
	}
	{
		FileSystemSecurity__ctor_m1_9880(__this, 1, /*hidden argument*/NULL);
		PlatformNotSupportedException_t1_1592 * L_0 = (PlatformNotSupportedException_t1_1592 *)il2cpp_codegen_object_new (PlatformNotSupportedException_t1_1592_il2cpp_TypeInfo_var);
		PlatformNotSupportedException__ctor_m1_14550(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.DirectorySecurity::.ctor(System.String,System.Security.AccessControl.AccessControlSections)
extern TypeInfo* PlatformNotSupportedException_t1_1592_il2cpp_TypeInfo_var;
extern "C" void DirectorySecurity__ctor_m1_9834 (DirectorySecurity_t1_1147 * __this, String_t* ___name, int32_t ___includeSections, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PlatformNotSupportedException_t1_1592_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(852);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___name;
		int32_t L_1 = ___includeSections;
		FileSystemSecurity__ctor_m1_9881(__this, 1, L_0, L_1, /*hidden argument*/NULL);
		PlatformNotSupportedException_t1_1592 * L_2 = (PlatformNotSupportedException_t1_1592 *)il2cpp_codegen_object_new (PlatformNotSupportedException_t1_1592_il2cpp_TypeInfo_var);
		PlatformNotSupportedException__ctor_m1_14550(L_2, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}
}
// System.Void System.Security.AccessControl.DiscretionaryAcl::.ctor(System.Boolean,System.Boolean,System.Int32)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void DiscretionaryAcl__ctor_m1_9835 (DiscretionaryAcl_t1_1134 * __this, bool ___isContainer, bool ___isDS, int32_t ___capacity, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___isContainer;
		bool L_1 = ___isDS;
		int32_t L_2 = ___capacity;
		DiscretionaryAcl__ctor_m1_9837(__this, L_0, L_1, 0, L_2, /*hidden argument*/NULL);
		NotImplementedException_t1_1582 * L_3 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_3, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}
}
// System.Void System.Security.AccessControl.DiscretionaryAcl::.ctor(System.Boolean,System.Boolean,System.Security.AccessControl.RawAcl)
extern "C" void DiscretionaryAcl__ctor_m1_9836 (DiscretionaryAcl_t1_1134 * __this, bool ___isContainer, bool ___isDS, RawAcl_t1_1170 * ___rawAcl, const MethodInfo* method)
{
	{
		bool L_0 = ___isContainer;
		bool L_1 = ___isDS;
		CommonAcl__ctor_m1_9729(__this, L_0, L_1, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.AccessControl.DiscretionaryAcl::.ctor(System.Boolean,System.Boolean,System.Byte,System.Int32)
extern "C" void DiscretionaryAcl__ctor_m1_9837 (DiscretionaryAcl_t1_1134 * __this, bool ___isContainer, bool ___isDS, uint8_t ___revision, int32_t ___capacity, const MethodInfo* method)
{
	{
		bool L_0 = ___isContainer;
		bool L_1 = ___isDS;
		uint8_t L_2 = ___revision;
		int32_t L_3 = ___capacity;
		CommonAcl__ctor_m1_9730(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.AccessControl.DiscretionaryAcl::AddAccess(System.Security.AccessControl.AccessControlType,System.Security.Principal.SecurityIdentifier,System.Int32,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void DiscretionaryAcl_AddAccess_m1_9838 (DiscretionaryAcl_t1_1134 * __this, int32_t ___accessType, SecurityIdentifier_t1_1132 * ___sid, int32_t ___accessMask, int32_t ___inheritanceFlags, int32_t ___propagationFlags, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.DiscretionaryAcl::AddAccess(System.Security.AccessControl.AccessControlType,System.Security.Principal.SecurityIdentifier,System.Int32,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.ObjectAceFlags,System.Guid,System.Guid)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void DiscretionaryAcl_AddAccess_m1_9839 (DiscretionaryAcl_t1_1134 * __this, int32_t ___accessType, SecurityIdentifier_t1_1132 * ___sid, int32_t ___accessMask, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___objectFlags, Guid_t1_319  ___objectType, Guid_t1_319  ___inheritedObjectType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Security.AccessControl.DiscretionaryAcl::RemoveAccess(System.Security.AccessControl.AccessControlType,System.Security.Principal.SecurityIdentifier,System.Int32,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" bool DiscretionaryAcl_RemoveAccess_m1_9840 (DiscretionaryAcl_t1_1134 * __this, int32_t ___accessType, SecurityIdentifier_t1_1132 * ___sid, int32_t ___accessMask, int32_t ___inheritanceFlags, int32_t ___propagationFlags, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Security.AccessControl.DiscretionaryAcl::RemoveAccess(System.Security.AccessControl.AccessControlType,System.Security.Principal.SecurityIdentifier,System.Int32,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.ObjectAceFlags,System.Guid,System.Guid)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" bool DiscretionaryAcl_RemoveAccess_m1_9841 (DiscretionaryAcl_t1_1134 * __this, int32_t ___accessType, SecurityIdentifier_t1_1132 * ___sid, int32_t ___accessMask, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___objectFlags, Guid_t1_319  ___objectType, Guid_t1_319  ___inheritedObjectType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.DiscretionaryAcl::RemoveAccessSpecific(System.Security.AccessControl.AccessControlType,System.Security.Principal.SecurityIdentifier,System.Int32,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void DiscretionaryAcl_RemoveAccessSpecific_m1_9842 (DiscretionaryAcl_t1_1134 * __this, int32_t ___accessType, SecurityIdentifier_t1_1132 * ___sid, int32_t ___accessMask, int32_t ___inheritanceFlags, int32_t ___propagationFlags, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.DiscretionaryAcl::RemoveAccessSpecific(System.Security.AccessControl.AccessControlType,System.Security.Principal.SecurityIdentifier,System.Int32,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.ObjectAceFlags,System.Guid,System.Guid)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void DiscretionaryAcl_RemoveAccessSpecific_m1_9843 (DiscretionaryAcl_t1_1134 * __this, int32_t ___accessType, SecurityIdentifier_t1_1132 * ___sid, int32_t ___accessMask, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___objectFlags, Guid_t1_319  ___objectType, Guid_t1_319  ___inheritedObjectType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.DiscretionaryAcl::SetAccess(System.Security.AccessControl.AccessControlType,System.Security.Principal.SecurityIdentifier,System.Int32,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void DiscretionaryAcl_SetAccess_m1_9844 (DiscretionaryAcl_t1_1134 * __this, int32_t ___accessType, SecurityIdentifier_t1_1132 * ___sid, int32_t ___accessMask, int32_t ___inheritanceFlags, int32_t ___propagationFlags, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.DiscretionaryAcl::SetAccess(System.Security.AccessControl.AccessControlType,System.Security.Principal.SecurityIdentifier,System.Int32,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.ObjectAceFlags,System.Guid,System.Guid)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void DiscretionaryAcl_SetAccess_m1_9845 (DiscretionaryAcl_t1_1134 * __this, int32_t ___accessType, SecurityIdentifier_t1_1132 * ___sid, int32_t ___accessMask, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___objectFlags, Guid_t1_319  ___objectType, Guid_t1_319  ___inheritedObjectType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.EventWaitHandleAccessRule::.ctor(System.Security.Principal.IdentityReference,System.Security.AccessControl.EventWaitHandleRights,System.Security.AccessControl.AccessControlType)
extern "C" void EventWaitHandleAccessRule__ctor_m1_9846 (EventWaitHandleAccessRule_t1_1149 * __this, IdentityReference_t1_1120 * ___identity, int32_t ___eventRights, int32_t ___type, const MethodInfo* method)
{
	{
		IdentityReference_t1_1120 * L_0 = ___identity;
		AccessRule__ctor_m1_9706(__this, L_0, 0, 0, 0, 0, 0, /*hidden argument*/NULL);
		int32_t L_1 = ___eventRights;
		__this->___rights_6 = L_1;
		return;
	}
}
// System.Void System.Security.AccessControl.EventWaitHandleAccessRule::.ctor(System.String,System.Security.AccessControl.EventWaitHandleRights,System.Security.AccessControl.AccessControlType)
extern TypeInfo* SecurityIdentifier_t1_1132_il2cpp_TypeInfo_var;
extern "C" void EventWaitHandleAccessRule__ctor_m1_9847 (EventWaitHandleAccessRule_t1_1149 * __this, String_t* ___identity, int32_t ___eventRights, int32_t ___type, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SecurityIdentifier_t1_1132_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(838);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___identity;
		SecurityIdentifier_t1_1132 * L_1 = (SecurityIdentifier_t1_1132 *)il2cpp_codegen_object_new (SecurityIdentifier_t1_1132_il2cpp_TypeInfo_var);
		SecurityIdentifier__ctor_m1_11825(L_1, L_0, /*hidden argument*/NULL);
		int32_t L_2 = ___eventRights;
		int32_t L_3 = ___type;
		EventWaitHandleAccessRule__ctor_m1_9846(__this, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Security.AccessControl.EventWaitHandleRights System.Security.AccessControl.EventWaitHandleAccessRule::get_EventWaitHandleRights()
extern "C" int32_t EventWaitHandleAccessRule_get_EventWaitHandleRights_m1_9848 (EventWaitHandleAccessRule_t1_1149 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___rights_6);
		return L_0;
	}
}
// System.Void System.Security.AccessControl.EventWaitHandleAuditRule::.ctor(System.Security.Principal.IdentityReference,System.Security.AccessControl.EventWaitHandleRights,System.Security.AccessControl.AuditFlags)
extern TypeInfo* ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern TypeInfo* SecurityIdentifier_t1_1132_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2530;
extern Il2CppCodeGenString* _stringLiteral2531;
extern Il2CppCodeGenString* _stringLiteral2527;
extern "C" void EventWaitHandleAuditRule__ctor_m1_9849 (EventWaitHandleAuditRule_t1_1150 * __this, IdentityReference_t1_1120 * ___identity, int32_t ___eventRights, int32_t ___flags, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		SecurityIdentifier_t1_1132_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(838);
		_stringLiteral2530 = il2cpp_codegen_string_literal_from_index(2530);
		_stringLiteral2531 = il2cpp_codegen_string_literal_from_index(2531);
		_stringLiteral2527 = il2cpp_codegen_string_literal_from_index(2527);
		s_Il2CppMethodIntialized = true;
	}
	{
		IdentityReference_t1_1120 * L_0 = ___identity;
		int32_t L_1 = ___flags;
		AuditRule__ctor_m1_9713(__this, L_0, 0, 0, 0, 0, L_1, /*hidden argument*/NULL);
		int32_t L_2 = ___eventRights;
		if ((((int32_t)L_2) < ((int32_t)2)))
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_3 = ___eventRights;
		if ((((int32_t)L_3) <= ((int32_t)((int32_t)2031619))))
		{
			goto IL_0029;
		}
	}

IL_001e:
	{
		ArgumentOutOfRangeException_t1_1501 * L_4 = (ArgumentOutOfRangeException_t1_1501 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1_13274(L_4, _stringLiteral2530, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
	}

IL_0029:
	{
		int32_t L_5 = ___flags;
		if ((((int32_t)L_5) < ((int32_t)0)))
		{
			goto IL_0037;
		}
	}
	{
		int32_t L_6 = ___flags;
		if ((((int32_t)L_6) <= ((int32_t)2)))
		{
			goto IL_0042;
		}
	}

IL_0037:
	{
		ArgumentOutOfRangeException_t1_1501 * L_7 = (ArgumentOutOfRangeException_t1_1501 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1_13274(L_7, _stringLiteral2531, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_7);
	}

IL_0042:
	{
		IdentityReference_t1_1120 * L_8 = ___identity;
		bool L_9 = IdentityReference_op_Equality_m1_11798(NULL /*static, unused*/, L_8, (IdentityReference_t1_1120 *)NULL, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0059;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_10 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_10, _stringLiteral2527, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_10);
	}

IL_0059:
	{
		int32_t L_11 = ___eventRights;
		if (L_11)
		{
			goto IL_006a;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_12 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_12, _stringLiteral2530, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_12);
	}

IL_006a:
	{
		int32_t L_13 = ___flags;
		if (L_13)
		{
			goto IL_007b;
		}
	}
	{
		ArgumentException_t1_1425 * L_14 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13260(L_14, _stringLiteral2531, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_14);
	}

IL_007b:
	{
		IdentityReference_t1_1120 * L_15 = ___identity;
		if (((SecurityIdentifier_t1_1132 *)IsInstSealed(L_15, SecurityIdentifier_t1_1132_il2cpp_TypeInfo_var)))
		{
			goto IL_0091;
		}
	}
	{
		ArgumentException_t1_1425 * L_16 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13260(L_16, _stringLiteral2527, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_16);
	}

IL_0091:
	{
		int32_t L_17 = ___eventRights;
		__this->___rights_6 = L_17;
		return;
	}
}
// System.Security.AccessControl.EventWaitHandleRights System.Security.AccessControl.EventWaitHandleAuditRule::get_EventWaitHandleRights()
extern "C" int32_t EventWaitHandleAuditRule_get_EventWaitHandleRights_m1_9850 (EventWaitHandleAuditRule_t1_1150 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___rights_6);
		return L_0;
	}
}
// System.Void System.Security.AccessControl.EventWaitHandleSecurity::.ctor()
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void EventWaitHandleSecurity__ctor_m1_9851 (EventWaitHandleSecurity_t1_1152 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NativeObjectSecurity__ctor_m1_9959(__this, /*hidden argument*/NULL);
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Type System.Security.AccessControl.EventWaitHandleSecurity::get_AccessRightType()
extern const Il2CppType* EventWaitHandleRights_t1_1151_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern "C" Type_t * EventWaitHandleSecurity_get_AccessRightType_m1_9852 (EventWaitHandleSecurity_t1_1152 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		EventWaitHandleRights_t1_1151_0_0_0_var = il2cpp_codegen_type_from_index(853);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(EventWaitHandleRights_t1_1151_0_0_0_var), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Type System.Security.AccessControl.EventWaitHandleSecurity::get_AccessRuleType()
extern const Il2CppType* EventWaitHandleAccessRule_t1_1149_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern "C" Type_t * EventWaitHandleSecurity_get_AccessRuleType_m1_9853 (EventWaitHandleSecurity_t1_1152 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		EventWaitHandleAccessRule_t1_1149_0_0_0_var = il2cpp_codegen_type_from_index(854);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(EventWaitHandleAccessRule_t1_1149_0_0_0_var), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Type System.Security.AccessControl.EventWaitHandleSecurity::get_AuditRuleType()
extern const Il2CppType* EventWaitHandleAuditRule_t1_1150_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern "C" Type_t * EventWaitHandleSecurity_get_AuditRuleType_m1_9854 (EventWaitHandleSecurity_t1_1152 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		EventWaitHandleAuditRule_t1_1150_0_0_0_var = il2cpp_codegen_type_from_index(855);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(EventWaitHandleAuditRule_t1_1150_0_0_0_var), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Security.AccessControl.AccessRule System.Security.AccessControl.EventWaitHandleSecurity::AccessRuleFactory(System.Security.Principal.IdentityReference,System.Int32,System.Boolean,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.AccessControlType)
extern TypeInfo* EventWaitHandleAccessRule_t1_1149_il2cpp_TypeInfo_var;
extern "C" AccessRule_t1_1111 * EventWaitHandleSecurity_AccessRuleFactory_m1_9855 (EventWaitHandleSecurity_t1_1152 * __this, IdentityReference_t1_1120 * ___identityReference, int32_t ___accessMask, bool ___isInherited, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___type, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		EventWaitHandleAccessRule_t1_1149_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(854);
		s_Il2CppMethodIntialized = true;
	}
	{
		IdentityReference_t1_1120 * L_0 = ___identityReference;
		int32_t L_1 = ___accessMask;
		int32_t L_2 = ___type;
		EventWaitHandleAccessRule_t1_1149 * L_3 = (EventWaitHandleAccessRule_t1_1149 *)il2cpp_codegen_object_new (EventWaitHandleAccessRule_t1_1149_il2cpp_TypeInfo_var);
		EventWaitHandleAccessRule__ctor_m1_9846(L_3, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void System.Security.AccessControl.EventWaitHandleSecurity::AddAccessRule(System.Security.AccessControl.EventWaitHandleAccessRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void EventWaitHandleSecurity_AddAccessRule_m1_9856 (EventWaitHandleSecurity_t1_1152 * __this, EventWaitHandleAccessRule_t1_1149 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Security.AccessControl.EventWaitHandleSecurity::RemoveAccessRule(System.Security.AccessControl.EventWaitHandleAccessRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" bool EventWaitHandleSecurity_RemoveAccessRule_m1_9857 (EventWaitHandleSecurity_t1_1152 * __this, EventWaitHandleAccessRule_t1_1149 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.EventWaitHandleSecurity::RemoveAccessRuleAll(System.Security.AccessControl.EventWaitHandleAccessRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void EventWaitHandleSecurity_RemoveAccessRuleAll_m1_9858 (EventWaitHandleSecurity_t1_1152 * __this, EventWaitHandleAccessRule_t1_1149 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.EventWaitHandleSecurity::RemoveAccessRuleSpecific(System.Security.AccessControl.EventWaitHandleAccessRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void EventWaitHandleSecurity_RemoveAccessRuleSpecific_m1_9859 (EventWaitHandleSecurity_t1_1152 * __this, EventWaitHandleAccessRule_t1_1149 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.EventWaitHandleSecurity::ResetAccessRule(System.Security.AccessControl.EventWaitHandleAccessRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void EventWaitHandleSecurity_ResetAccessRule_m1_9860 (EventWaitHandleSecurity_t1_1152 * __this, EventWaitHandleAccessRule_t1_1149 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.EventWaitHandleSecurity::SetAccessRule(System.Security.AccessControl.EventWaitHandleAccessRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void EventWaitHandleSecurity_SetAccessRule_m1_9861 (EventWaitHandleSecurity_t1_1152 * __this, EventWaitHandleAccessRule_t1_1149 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Security.AccessControl.AuditRule System.Security.AccessControl.EventWaitHandleSecurity::AuditRuleFactory(System.Security.Principal.IdentityReference,System.Int32,System.Boolean,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.AuditFlags)
extern TypeInfo* EventWaitHandleAuditRule_t1_1150_il2cpp_TypeInfo_var;
extern "C" AuditRule_t1_1119 * EventWaitHandleSecurity_AuditRuleFactory_m1_9862 (EventWaitHandleSecurity_t1_1152 * __this, IdentityReference_t1_1120 * ___identityReference, int32_t ___accessMask, bool ___isInherited, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___flags, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		EventWaitHandleAuditRule_t1_1150_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(855);
		s_Il2CppMethodIntialized = true;
	}
	{
		IdentityReference_t1_1120 * L_0 = ___identityReference;
		int32_t L_1 = ___accessMask;
		int32_t L_2 = ___flags;
		EventWaitHandleAuditRule_t1_1150 * L_3 = (EventWaitHandleAuditRule_t1_1150 *)il2cpp_codegen_object_new (EventWaitHandleAuditRule_t1_1150_il2cpp_TypeInfo_var);
		EventWaitHandleAuditRule__ctor_m1_9849(L_3, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void System.Security.AccessControl.EventWaitHandleSecurity::AddAuditRule(System.Security.AccessControl.EventWaitHandleAuditRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void EventWaitHandleSecurity_AddAuditRule_m1_9863 (EventWaitHandleSecurity_t1_1152 * __this, EventWaitHandleAuditRule_t1_1150 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Security.AccessControl.EventWaitHandleSecurity::RemoveAuditRule(System.Security.AccessControl.EventWaitHandleAuditRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" bool EventWaitHandleSecurity_RemoveAuditRule_m1_9864 (EventWaitHandleSecurity_t1_1152 * __this, EventWaitHandleAuditRule_t1_1150 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.EventWaitHandleSecurity::RemoveAuditRuleAll(System.Security.AccessControl.EventWaitHandleAuditRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void EventWaitHandleSecurity_RemoveAuditRuleAll_m1_9865 (EventWaitHandleSecurity_t1_1152 * __this, EventWaitHandleAuditRule_t1_1150 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.EventWaitHandleSecurity::RemoveAuditRuleSpecific(System.Security.AccessControl.EventWaitHandleAuditRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void EventWaitHandleSecurity_RemoveAuditRuleSpecific_m1_9866 (EventWaitHandleSecurity_t1_1152 * __this, EventWaitHandleAuditRule_t1_1150 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.EventWaitHandleSecurity::SetAuditRule(System.Security.AccessControl.EventWaitHandleAuditRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void EventWaitHandleSecurity_SetAuditRule_m1_9867 (EventWaitHandleSecurity_t1_1152 * __this, EventWaitHandleAuditRule_t1_1150 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.FileSecurity::.ctor()
extern TypeInfo* PlatformNotSupportedException_t1_1592_il2cpp_TypeInfo_var;
extern "C" void FileSecurity__ctor_m1_9868 (FileSecurity_t1_1153 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PlatformNotSupportedException_t1_1592_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(852);
		s_Il2CppMethodIntialized = true;
	}
	{
		FileSystemSecurity__ctor_m1_9880(__this, 0, /*hidden argument*/NULL);
		PlatformNotSupportedException_t1_1592 * L_0 = (PlatformNotSupportedException_t1_1592 *)il2cpp_codegen_object_new (PlatformNotSupportedException_t1_1592_il2cpp_TypeInfo_var);
		PlatformNotSupportedException__ctor_m1_14550(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.FileSecurity::.ctor(System.String,System.Security.AccessControl.AccessControlSections)
extern TypeInfo* PlatformNotSupportedException_t1_1592_il2cpp_TypeInfo_var;
extern "C" void FileSecurity__ctor_m1_9869 (FileSecurity_t1_1153 * __this, String_t* ___fileName, int32_t ___includeSections, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PlatformNotSupportedException_t1_1592_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(852);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___fileName;
		int32_t L_1 = ___includeSections;
		FileSystemSecurity__ctor_m1_9881(__this, 0, L_0, L_1, /*hidden argument*/NULL);
		PlatformNotSupportedException_t1_1592 * L_2 = (PlatformNotSupportedException_t1_1592 *)il2cpp_codegen_object_new (PlatformNotSupportedException_t1_1592_il2cpp_TypeInfo_var);
		PlatformNotSupportedException__ctor_m1_14550(L_2, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}
}
// System.Void System.Security.AccessControl.FileSystemAccessRule::.ctor(System.Security.Principal.IdentityReference,System.Security.AccessControl.FileSystemRights,System.Security.AccessControl.AccessControlType)
extern "C" void FileSystemAccessRule__ctor_m1_9870 (FileSystemAccessRule_t1_1154 * __this, IdentityReference_t1_1120 * ___identity, int32_t ___fileSystemRights, int32_t ___type, const MethodInfo* method)
{
	{
		IdentityReference_t1_1120 * L_0 = ___identity;
		int32_t L_1 = ___fileSystemRights;
		int32_t L_2 = ___type;
		FileSystemAccessRule__ctor_m1_9872(__this, L_0, L_1, 0, 0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.AccessControl.FileSystemAccessRule::.ctor(System.String,System.Security.AccessControl.FileSystemRights,System.Security.AccessControl.AccessControlType)
extern TypeInfo* SecurityIdentifier_t1_1132_il2cpp_TypeInfo_var;
extern "C" void FileSystemAccessRule__ctor_m1_9871 (FileSystemAccessRule_t1_1154 * __this, String_t* ___identity, int32_t ___fileSystemRights, int32_t ___type, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SecurityIdentifier_t1_1132_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(838);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___identity;
		SecurityIdentifier_t1_1132 * L_1 = (SecurityIdentifier_t1_1132 *)il2cpp_codegen_object_new (SecurityIdentifier_t1_1132_il2cpp_TypeInfo_var);
		SecurityIdentifier__ctor_m1_11825(L_1, L_0, /*hidden argument*/NULL);
		int32_t L_2 = ___fileSystemRights;
		int32_t L_3 = ___type;
		FileSystemAccessRule__ctor_m1_9872(__this, L_1, L_2, 0, 0, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.AccessControl.FileSystemAccessRule::.ctor(System.Security.Principal.IdentityReference,System.Security.AccessControl.FileSystemRights,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.AccessControlType)
extern "C" void FileSystemAccessRule__ctor_m1_9872 (FileSystemAccessRule_t1_1154 * __this, IdentityReference_t1_1120 * ___identity, int32_t ___fileSystemRights, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___type, const MethodInfo* method)
{
	{
		IdentityReference_t1_1120 * L_0 = ___identity;
		int32_t L_1 = ___fileSystemRights;
		int32_t L_2 = ___inheritanceFlags;
		int32_t L_3 = ___propagationFlags;
		int32_t L_4 = ___type;
		AccessRule__ctor_m1_9706(__this, L_0, L_1, 0, L_2, L_3, L_4, /*hidden argument*/NULL);
		int32_t L_5 = ___fileSystemRights;
		__this->___rights_6 = L_5;
		return;
	}
}
// System.Void System.Security.AccessControl.FileSystemAccessRule::.ctor(System.String,System.Security.AccessControl.FileSystemRights,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.AccessControlType)
extern TypeInfo* SecurityIdentifier_t1_1132_il2cpp_TypeInfo_var;
extern "C" void FileSystemAccessRule__ctor_m1_9873 (FileSystemAccessRule_t1_1154 * __this, String_t* ___identity, int32_t ___fileSystemRights, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___type, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SecurityIdentifier_t1_1132_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(838);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___identity;
		SecurityIdentifier_t1_1132 * L_1 = (SecurityIdentifier_t1_1132 *)il2cpp_codegen_object_new (SecurityIdentifier_t1_1132_il2cpp_TypeInfo_var);
		SecurityIdentifier__ctor_m1_11825(L_1, L_0, /*hidden argument*/NULL);
		int32_t L_2 = ___fileSystemRights;
		int32_t L_3 = ___inheritanceFlags;
		int32_t L_4 = ___propagationFlags;
		int32_t L_5 = ___type;
		FileSystemAccessRule__ctor_m1_9872(__this, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Security.AccessControl.FileSystemRights System.Security.AccessControl.FileSystemAccessRule::get_FileSystemRights()
extern "C" int32_t FileSystemAccessRule_get_FileSystemRights_m1_9874 (FileSystemAccessRule_t1_1154 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___rights_6);
		return L_0;
	}
}
// System.Void System.Security.AccessControl.FileSystemAuditRule::.ctor(System.Security.Principal.IdentityReference,System.Security.AccessControl.FileSystemRights,System.Security.AccessControl.AuditFlags)
extern "C" void FileSystemAuditRule__ctor_m1_9875 (FileSystemAuditRule_t1_1155 * __this, IdentityReference_t1_1120 * ___identity, int32_t ___fileSystemRights, int32_t ___flags, const MethodInfo* method)
{
	{
		IdentityReference_t1_1120 * L_0 = ___identity;
		int32_t L_1 = ___fileSystemRights;
		int32_t L_2 = ___flags;
		FileSystemAuditRule__ctor_m1_9877(__this, L_0, L_1, 0, 0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.AccessControl.FileSystemAuditRule::.ctor(System.String,System.Security.AccessControl.FileSystemRights,System.Security.AccessControl.AuditFlags)
extern TypeInfo* SecurityIdentifier_t1_1132_il2cpp_TypeInfo_var;
extern "C" void FileSystemAuditRule__ctor_m1_9876 (FileSystemAuditRule_t1_1155 * __this, String_t* ___identity, int32_t ___fileSystemRights, int32_t ___flags, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SecurityIdentifier_t1_1132_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(838);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___identity;
		SecurityIdentifier_t1_1132 * L_1 = (SecurityIdentifier_t1_1132 *)il2cpp_codegen_object_new (SecurityIdentifier_t1_1132_il2cpp_TypeInfo_var);
		SecurityIdentifier__ctor_m1_11825(L_1, L_0, /*hidden argument*/NULL);
		int32_t L_2 = ___fileSystemRights;
		int32_t L_3 = ___flags;
		FileSystemAuditRule__ctor_m1_9875(__this, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.AccessControl.FileSystemAuditRule::.ctor(System.Security.Principal.IdentityReference,System.Security.AccessControl.FileSystemRights,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.AuditFlags)
extern "C" void FileSystemAuditRule__ctor_m1_9877 (FileSystemAuditRule_t1_1155 * __this, IdentityReference_t1_1120 * ___identity, int32_t ___fileSystemRights, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___flags, const MethodInfo* method)
{
	{
		IdentityReference_t1_1120 * L_0 = ___identity;
		int32_t L_1 = ___inheritanceFlags;
		int32_t L_2 = ___propagationFlags;
		int32_t L_3 = ___flags;
		AuditRule__ctor_m1_9713(__this, L_0, 0, 0, L_1, L_2, L_3, /*hidden argument*/NULL);
		int32_t L_4 = ___fileSystemRights;
		__this->___rights_6 = L_4;
		return;
	}
}
// System.Void System.Security.AccessControl.FileSystemAuditRule::.ctor(System.String,System.Security.AccessControl.FileSystemRights,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.AuditFlags)
extern TypeInfo* SecurityIdentifier_t1_1132_il2cpp_TypeInfo_var;
extern "C" void FileSystemAuditRule__ctor_m1_9878 (FileSystemAuditRule_t1_1155 * __this, String_t* ___identity, int32_t ___fileSystemRights, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___flags, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SecurityIdentifier_t1_1132_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(838);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___identity;
		SecurityIdentifier_t1_1132 * L_1 = (SecurityIdentifier_t1_1132 *)il2cpp_codegen_object_new (SecurityIdentifier_t1_1132_il2cpp_TypeInfo_var);
		SecurityIdentifier__ctor_m1_11825(L_1, L_0, /*hidden argument*/NULL);
		int32_t L_2 = ___fileSystemRights;
		int32_t L_3 = ___inheritanceFlags;
		int32_t L_4 = ___propagationFlags;
		int32_t L_5 = ___flags;
		FileSystemAuditRule__ctor_m1_9877(__this, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Security.AccessControl.FileSystemRights System.Security.AccessControl.FileSystemAuditRule::get_FileSystemRights()
extern "C" int32_t FileSystemAuditRule_get_FileSystemRights_m1_9879 (FileSystemAuditRule_t1_1155 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___rights_6);
		return L_0;
	}
}
// System.Void System.Security.AccessControl.FileSystemSecurity::.ctor(System.Boolean)
extern "C" void FileSystemSecurity__ctor_m1_9880 (FileSystemSecurity_t1_1148 * __this, bool ___isContainer, const MethodInfo* method)
{
	{
		bool L_0 = ___isContainer;
		NativeObjectSecurity__ctor_m1_9960(__this, L_0, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.AccessControl.FileSystemSecurity::.ctor(System.Boolean,System.String,System.Security.AccessControl.AccessControlSections)
extern "C" void FileSystemSecurity__ctor_m1_9881 (FileSystemSecurity_t1_1148 * __this, bool ___isContainer, String_t* ___name, int32_t ___includeSections, const MethodInfo* method)
{
	{
		bool L_0 = ___isContainer;
		String_t* L_1 = ___name;
		int32_t L_2 = ___includeSections;
		NativeObjectSecurity__ctor_m1_9963(__this, L_0, 1, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Type System.Security.AccessControl.FileSystemSecurity::get_AccessRightType()
extern const Il2CppType* FileSystemRights_t1_1156_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern "C" Type_t * FileSystemSecurity_get_AccessRightType_m1_9882 (FileSystemSecurity_t1_1148 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FileSystemRights_t1_1156_0_0_0_var = il2cpp_codegen_type_from_index(856);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(FileSystemRights_t1_1156_0_0_0_var), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Type System.Security.AccessControl.FileSystemSecurity::get_AccessRuleType()
extern const Il2CppType* FileSystemAccessRule_t1_1154_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern "C" Type_t * FileSystemSecurity_get_AccessRuleType_m1_9883 (FileSystemSecurity_t1_1148 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FileSystemAccessRule_t1_1154_0_0_0_var = il2cpp_codegen_type_from_index(857);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(FileSystemAccessRule_t1_1154_0_0_0_var), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Type System.Security.AccessControl.FileSystemSecurity::get_AuditRuleType()
extern const Il2CppType* FileSystemAuditRule_t1_1155_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern "C" Type_t * FileSystemSecurity_get_AuditRuleType_m1_9884 (FileSystemSecurity_t1_1148 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FileSystemAuditRule_t1_1155_0_0_0_var = il2cpp_codegen_type_from_index(858);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(FileSystemAuditRule_t1_1155_0_0_0_var), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Security.AccessControl.AccessRule System.Security.AccessControl.FileSystemSecurity::AccessRuleFactory(System.Security.Principal.IdentityReference,System.Int32,System.Boolean,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.AccessControlType)
extern TypeInfo* FileSystemAccessRule_t1_1154_il2cpp_TypeInfo_var;
extern "C" AccessRule_t1_1111 * FileSystemSecurity_AccessRuleFactory_m1_9885 (FileSystemSecurity_t1_1148 * __this, IdentityReference_t1_1120 * ___identityReference, int32_t ___accessMask, bool ___isInherited, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___type, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FileSystemAccessRule_t1_1154_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(857);
		s_Il2CppMethodIntialized = true;
	}
	{
		IdentityReference_t1_1120 * L_0 = ___identityReference;
		int32_t L_1 = ___accessMask;
		int32_t L_2 = ___inheritanceFlags;
		int32_t L_3 = ___propagationFlags;
		int32_t L_4 = ___type;
		FileSystemAccessRule_t1_1154 * L_5 = (FileSystemAccessRule_t1_1154 *)il2cpp_codegen_object_new (FileSystemAccessRule_t1_1154_il2cpp_TypeInfo_var);
		FileSystemAccessRule__ctor_m1_9872(L_5, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void System.Security.AccessControl.FileSystemSecurity::AddAccessRule(System.Security.AccessControl.FileSystemAccessRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void FileSystemSecurity_AddAccessRule_m1_9886 (FileSystemSecurity_t1_1148 * __this, FileSystemAccessRule_t1_1154 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Security.AccessControl.FileSystemSecurity::RemoveAccessRule(System.Security.AccessControl.FileSystemAccessRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" bool FileSystemSecurity_RemoveAccessRule_m1_9887 (FileSystemSecurity_t1_1148 * __this, FileSystemAccessRule_t1_1154 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.FileSystemSecurity::RemoveAccessRuleAll(System.Security.AccessControl.FileSystemAccessRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void FileSystemSecurity_RemoveAccessRuleAll_m1_9888 (FileSystemSecurity_t1_1148 * __this, FileSystemAccessRule_t1_1154 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.FileSystemSecurity::RemoveAccessRuleSpecific(System.Security.AccessControl.FileSystemAccessRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void FileSystemSecurity_RemoveAccessRuleSpecific_m1_9889 (FileSystemSecurity_t1_1148 * __this, FileSystemAccessRule_t1_1154 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.FileSystemSecurity::ResetAccessRule(System.Security.AccessControl.FileSystemAccessRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void FileSystemSecurity_ResetAccessRule_m1_9890 (FileSystemSecurity_t1_1148 * __this, FileSystemAccessRule_t1_1154 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.FileSystemSecurity::SetAccessRule(System.Security.AccessControl.FileSystemAccessRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void FileSystemSecurity_SetAccessRule_m1_9891 (FileSystemSecurity_t1_1148 * __this, FileSystemAccessRule_t1_1154 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Security.AccessControl.AuditRule System.Security.AccessControl.FileSystemSecurity::AuditRuleFactory(System.Security.Principal.IdentityReference,System.Int32,System.Boolean,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.AuditFlags)
extern TypeInfo* FileSystemAuditRule_t1_1155_il2cpp_TypeInfo_var;
extern "C" AuditRule_t1_1119 * FileSystemSecurity_AuditRuleFactory_m1_9892 (FileSystemSecurity_t1_1148 * __this, IdentityReference_t1_1120 * ___identityReference, int32_t ___accessMask, bool ___isInherited, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___flags, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FileSystemAuditRule_t1_1155_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(858);
		s_Il2CppMethodIntialized = true;
	}
	{
		IdentityReference_t1_1120 * L_0 = ___identityReference;
		int32_t L_1 = ___accessMask;
		int32_t L_2 = ___inheritanceFlags;
		int32_t L_3 = ___propagationFlags;
		int32_t L_4 = ___flags;
		FileSystemAuditRule_t1_1155 * L_5 = (FileSystemAuditRule_t1_1155 *)il2cpp_codegen_object_new (FileSystemAuditRule_t1_1155_il2cpp_TypeInfo_var);
		FileSystemAuditRule__ctor_m1_9877(L_5, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void System.Security.AccessControl.FileSystemSecurity::AddAuditRule(System.Security.AccessControl.FileSystemAuditRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void FileSystemSecurity_AddAuditRule_m1_9893 (FileSystemSecurity_t1_1148 * __this, FileSystemAuditRule_t1_1155 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Security.AccessControl.FileSystemSecurity::RemoveAuditRule(System.Security.AccessControl.FileSystemAuditRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" bool FileSystemSecurity_RemoveAuditRule_m1_9894 (FileSystemSecurity_t1_1148 * __this, FileSystemAuditRule_t1_1155 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.FileSystemSecurity::RemoveAuditRuleAll(System.Security.AccessControl.FileSystemAuditRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void FileSystemSecurity_RemoveAuditRuleAll_m1_9895 (FileSystemSecurity_t1_1148 * __this, FileSystemAuditRule_t1_1155 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.FileSystemSecurity::RemoveAuditRuleSpecific(System.Security.AccessControl.FileSystemAuditRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void FileSystemSecurity_RemoveAuditRuleSpecific_m1_9896 (FileSystemSecurity_t1_1148 * __this, FileSystemAuditRule_t1_1155 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.FileSystemSecurity::SetAuditRule(System.Security.AccessControl.FileSystemAuditRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void FileSystemSecurity_SetAuditRule_m1_9897 (FileSystemSecurity_t1_1148 * __this, FileSystemAuditRule_t1_1155 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.GenericAce::.ctor(System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags)
extern "C" void GenericAce__ctor_m1_9898 (GenericAce_t1_1145 * __this, int32_t ___inheritanceFlags, int32_t ___propagationFlags, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___inheritanceFlags;
		__this->___inheritance_0 = L_0;
		int32_t L_1 = ___propagationFlags;
		__this->___propagation_1 = L_1;
		return;
	}
}
// System.Void System.Security.AccessControl.GenericAce::.ctor(System.Security.AccessControl.AceType)
extern TypeInfo* ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral41;
extern "C" void GenericAce__ctor_m1_9899 (GenericAce_t1_1145 * __this, int32_t ___type, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		_stringLiteral41 = il2cpp_codegen_string_literal_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___type;
		if ((((int32_t)L_0) > ((int32_t)((int32_t)16))))
		{
			goto IL_0019;
		}
	}
	{
		ArgumentOutOfRangeException_t1_1501 * L_1 = (ArgumentOutOfRangeException_t1_1501 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1_13274(L_1, _stringLiteral41, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0019:
	{
		int32_t L_2 = ___type;
		__this->___ace_type_3 = L_2;
		return;
	}
}
// System.Security.AccessControl.AceFlags System.Security.AccessControl.GenericAce::get_AceFlags()
extern "C" uint8_t GenericAce_get_AceFlags_m1_9900 (GenericAce_t1_1145 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = (__this->___aceflags_2);
		return L_0;
	}
}
// System.Void System.Security.AccessControl.GenericAce::set_AceFlags(System.Security.AccessControl.AceFlags)
extern "C" void GenericAce_set_AceFlags_m1_9901 (GenericAce_t1_1145 * __this, uint8_t ___value, const MethodInfo* method)
{
	{
		uint8_t L_0 = ___value;
		__this->___aceflags_2 = L_0;
		return;
	}
}
// System.Security.AccessControl.AceType System.Security.AccessControl.GenericAce::get_AceType()
extern "C" int32_t GenericAce_get_AceType_m1_9902 (GenericAce_t1_1145 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___ace_type_3);
		return L_0;
	}
}
// System.Security.AccessControl.AuditFlags System.Security.AccessControl.GenericAce::get_AuditFlags()
extern "C" int32_t GenericAce_get_AuditFlags_m1_9903 (GenericAce_t1_1145 * __this, const MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		V_0 = 0;
		uint8_t L_0 = (__this->___aceflags_2);
		if (!(((int32_t)((uint8_t)((int32_t)((int32_t)L_0&(int32_t)((int32_t)64)))))))
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_1 = V_0;
		V_0 = ((int32_t)((int32_t)L_1|(int32_t)1));
	}

IL_0015:
	{
		uint8_t L_2 = (__this->___aceflags_2);
		if (!(((int32_t)((uint8_t)((int32_t)((int32_t)L_2&(int32_t)((int32_t)128)))))))
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_3 = V_0;
		V_0 = ((int32_t)((int32_t)L_3|(int32_t)2));
	}

IL_002b:
	{
		int32_t L_4 = V_0;
		return L_4;
	}
}
// System.Security.AccessControl.InheritanceFlags System.Security.AccessControl.GenericAce::get_InheritanceFlags()
extern "C" int32_t GenericAce_get_InheritanceFlags_m1_9904 (GenericAce_t1_1145 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___inheritance_0);
		return L_0;
	}
}
// System.Boolean System.Security.AccessControl.GenericAce::get_IsInherited()
extern "C" bool GenericAce_get_IsInherited_m1_9905 (GenericAce_t1_1145 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Security.AccessControl.PropagationFlags System.Security.AccessControl.GenericAce::get_PropagationFlags()
extern "C" int32_t GenericAce_get_PropagationFlags_m1_9906 (GenericAce_t1_1145 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___propagation_1);
		return L_0;
	}
}
// System.Security.AccessControl.GenericAce System.Security.AccessControl.GenericAce::Copy()
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" GenericAce_t1_1145 * GenericAce_Copy_m1_9907 (GenericAce_t1_1145 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Security.AccessControl.GenericAce System.Security.AccessControl.GenericAce::CreateFromBinaryForm(System.Byte[],System.Int32)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" GenericAce_t1_1145 * GenericAce_CreateFromBinaryForm_m1_9908 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___binaryForm, int32_t ___offset, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Security.AccessControl.GenericAce::Equals(System.Object)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" bool GenericAce_Equals_m1_9909 (GenericAce_t1_1145 * __this, Object_t * ___o, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Int32 System.Security.AccessControl.GenericAce::GetHashCode()
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" int32_t GenericAce_GetHashCode_m1_9910 (GenericAce_t1_1145 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Security.AccessControl.GenericAce::op_Equality(System.Security.AccessControl.GenericAce,System.Security.AccessControl.GenericAce)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" bool GenericAce_op_Equality_m1_9911 (Object_t * __this /* static, unused */, GenericAce_t1_1145 * ___left, GenericAce_t1_1145 * ___right, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Security.AccessControl.GenericAce::op_Inequality(System.Security.AccessControl.GenericAce,System.Security.AccessControl.GenericAce)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" bool GenericAce_op_Inequality_m1_9912 (Object_t * __this /* static, unused */, GenericAce_t1_1145 * ___left, GenericAce_t1_1145 * ___right, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.GenericAcl::.ctor()
extern "C" void GenericAcl__ctor_m1_9913 (GenericAcl_t1_1114 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.AccessControl.GenericAcl::.cctor()
extern TypeInfo* GenericAcl_t1_1114_il2cpp_TypeInfo_var;
extern "C" void GenericAcl__cctor_m1_9914 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GenericAcl_t1_1114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(841);
		s_Il2CppMethodIntialized = true;
	}
	{
		((GenericAcl_t1_1114_StaticFields*)GenericAcl_t1_1114_il2cpp_TypeInfo_var->static_fields)->___AclRevision_0 = 2;
		((GenericAcl_t1_1114_StaticFields*)GenericAcl_t1_1114_il2cpp_TypeInfo_var->static_fields)->___AclRevisionDS_1 = 4;
		((GenericAcl_t1_1114_StaticFields*)GenericAcl_t1_1114_il2cpp_TypeInfo_var->static_fields)->___MaxBinaryLength_2 = ((int32_t)65536);
		return;
	}
}
// System.Void System.Security.AccessControl.GenericAcl::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern TypeInfo* GenericAceU5BU5D_t1_1729_il2cpp_TypeInfo_var;
extern "C" void GenericAcl_System_Collections_ICollection_CopyTo_m1_9915 (GenericAcl_t1_1114 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GenericAceU5BU5D_t1_1729_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(859);
		s_Il2CppMethodIntialized = true;
	}
	{
		Array_t * L_0 = ___array;
		int32_t L_1 = ___index;
		GenericAcl_CopyTo_m1_9919(__this, ((GenericAceU5BU5D_t1_1729*)Castclass(L_0, GenericAceU5BU5D_t1_1729_il2cpp_TypeInfo_var)), L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator System.Security.AccessControl.GenericAcl::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * GenericAcl_System_Collections_IEnumerable_GetEnumerator_m1_9916 (GenericAcl_t1_1114 * __this, const MethodInfo* method)
{
	{
		AceEnumerator_t1_1113 * L_0 = GenericAcl_GetEnumerator_m1_9920(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean System.Security.AccessControl.GenericAcl::get_IsSynchronized()
extern "C" bool GenericAcl_get_IsSynchronized_m1_9917 (GenericAcl_t1_1114 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Object System.Security.AccessControl.GenericAcl::get_SyncRoot()
extern "C" Object_t * GenericAcl_get_SyncRoot_m1_9918 (GenericAcl_t1_1114 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Void System.Security.AccessControl.GenericAcl::CopyTo(System.Security.AccessControl.GenericAce[],System.Int32)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral173;
extern Il2CppCodeGenString* _stringLiteral170;
extern Il2CppCodeGenString* _stringLiteral2532;
extern "C" void GenericAcl_CopyTo_m1_9919 (GenericAcl_t1_1114 * __this, GenericAceU5BU5D_t1_1729* ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		_stringLiteral173 = il2cpp_codegen_string_literal_from_index(173);
		_stringLiteral170 = il2cpp_codegen_string_literal_from_index(170);
		_stringLiteral2532 = il2cpp_codegen_string_literal_from_index(2532);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		GenericAceU5BU5D_t1_1729* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, _stringLiteral173, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		int32_t L_2 = ___index;
		if ((((int32_t)L_2) < ((int32_t)0)))
		{
			goto IL_0028;
		}
	}
	{
		GenericAceU5BU5D_t1_1729* L_3 = ___array;
		NullCheck(L_3);
		int32_t L_4 = ___index;
		int32_t L_5 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Security.AccessControl.GenericAcl::get_Count() */, __this);
		if ((((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Array_t *)L_3)->max_length))))-(int32_t)L_4))) >= ((int32_t)L_5)))
		{
			goto IL_0038;
		}
	}

IL_0028:
	{
		ArgumentOutOfRangeException_t1_1501 * L_6 = (ArgumentOutOfRangeException_t1_1501 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1_13275(L_6, _stringLiteral170, _stringLiteral2532, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_6);
	}

IL_0038:
	{
		V_0 = 0;
		goto IL_004f;
	}

IL_003f:
	{
		GenericAceU5BU5D_t1_1729* L_7 = ___array;
		int32_t L_8 = V_0;
		int32_t L_9 = ___index;
		int32_t L_10 = V_0;
		GenericAce_t1_1145 * L_11 = (GenericAce_t1_1145 *)VirtFuncInvoker1< GenericAce_t1_1145 *, int32_t >::Invoke(11 /* System.Security.AccessControl.GenericAce System.Security.AccessControl.GenericAcl::get_Item(System.Int32) */, __this, L_10);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, ((int32_t)((int32_t)L_8+(int32_t)L_9)));
		ArrayElementTypeCheck (L_7, L_11);
		*((GenericAce_t1_1145 **)(GenericAce_t1_1145 **)SZArrayLdElema(L_7, ((int32_t)((int32_t)L_8+(int32_t)L_9)), sizeof(GenericAce_t1_1145 *))) = (GenericAce_t1_1145 *)L_11;
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_004f:
	{
		int32_t L_13 = V_0;
		int32_t L_14 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Security.AccessControl.GenericAcl::get_Count() */, __this);
		if ((((int32_t)L_13) < ((int32_t)L_14)))
		{
			goto IL_003f;
		}
	}
	{
		return;
	}
}
// System.Security.AccessControl.AceEnumerator System.Security.AccessControl.GenericAcl::GetEnumerator()
extern TypeInfo* AceEnumerator_t1_1113_il2cpp_TypeInfo_var;
extern "C" AceEnumerator_t1_1113 * GenericAcl_GetEnumerator_m1_9920 (GenericAcl_t1_1114 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AceEnumerator_t1_1113_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(860);
		s_Il2CppMethodIntialized = true;
	}
	{
		AceEnumerator_t1_1113 * L_0 = (AceEnumerator_t1_1113 *)il2cpp_codegen_object_new (AceEnumerator_t1_1113_il2cpp_TypeInfo_var);
		AceEnumerator__ctor_m1_9708(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Security.AccessControl.GenericSecurityDescriptor::.ctor()
extern "C" void GenericSecurityDescriptor__ctor_m1_9921 (GenericSecurityDescriptor_t1_1131 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 System.Security.AccessControl.GenericSecurityDescriptor::get_BinaryLength()
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" int32_t GenericSecurityDescriptor_get_BinaryLength_m1_9922 (GenericSecurityDescriptor_t1_1131 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Byte System.Security.AccessControl.GenericSecurityDescriptor::get_Revision()
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" uint8_t GenericSecurityDescriptor_get_Revision_m1_9923 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.GenericSecurityDescriptor::GetBinaryForm(System.Byte[],System.Int32)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void GenericSecurityDescriptor_GetBinaryForm_m1_9924 (GenericSecurityDescriptor_t1_1131 * __this, ByteU5BU5D_t1_109* ___binaryForm, int32_t ___offset, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.String System.Security.AccessControl.GenericSecurityDescriptor::GetSddlForm(System.Security.AccessControl.AccessControlSections)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" String_t* GenericSecurityDescriptor_GetSddlForm_m1_9925 (GenericSecurityDescriptor_t1_1131 * __this, int32_t ___includeSections, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Security.AccessControl.GenericSecurityDescriptor::IsSddlConversionSupported()
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" bool GenericSecurityDescriptor_IsSddlConversionSupported_m1_9926 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.KnownAce::.ctor(System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags)
extern "C" void KnownAce__ctor_m1_9927 (KnownAce_t1_1136 * __this, int32_t ___inheritanceFlags, int32_t ___propagationFlags, const MethodInfo* method)
{
	{
		int32_t L_0 = ___inheritanceFlags;
		int32_t L_1 = ___propagationFlags;
		GenericAce__ctor_m1_9898(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 System.Security.AccessControl.KnownAce::get_AccessMask()
extern "C" int32_t KnownAce_get_AccessMask_m1_9928 (KnownAce_t1_1136 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___access_mask_4);
		return L_0;
	}
}
// System.Void System.Security.AccessControl.KnownAce::set_AccessMask(System.Int32)
extern "C" void KnownAce_set_AccessMask_m1_9929 (KnownAce_t1_1136 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___access_mask_4 = L_0;
		return;
	}
}
// System.Security.Principal.SecurityIdentifier System.Security.AccessControl.KnownAce::get_SecurityIdentifier()
extern "C" SecurityIdentifier_t1_1132 * KnownAce_get_SecurityIdentifier_m1_9930 (KnownAce_t1_1136 * __this, const MethodInfo* method)
{
	{
		SecurityIdentifier_t1_1132 * L_0 = (__this->___identifier_5);
		return L_0;
	}
}
// System.Void System.Security.AccessControl.KnownAce::set_SecurityIdentifier(System.Security.Principal.SecurityIdentifier)
extern "C" void KnownAce_set_SecurityIdentifier_m1_9931 (KnownAce_t1_1136 * __this, SecurityIdentifier_t1_1132 * ___value, const MethodInfo* method)
{
	{
		SecurityIdentifier_t1_1132 * L_0 = ___value;
		__this->___identifier_5 = L_0;
		return;
	}
}
// System.Void System.Security.AccessControl.MutexAccessRule::.ctor(System.Security.Principal.IdentityReference,System.Security.AccessControl.MutexRights,System.Security.AccessControl.AccessControlType)
extern "C" void MutexAccessRule__ctor_m1_9932 (MutexAccessRule_t1_1158 * __this, IdentityReference_t1_1120 * ___identity, int32_t ___eventRights, int32_t ___type, const MethodInfo* method)
{
	{
		IdentityReference_t1_1120 * L_0 = ___identity;
		int32_t L_1 = ___type;
		AccessRule__ctor_m1_9706(__this, L_0, 0, 0, 0, 0, L_1, /*hidden argument*/NULL);
		int32_t L_2 = ___eventRights;
		__this->___rights_6 = L_2;
		return;
	}
}
// System.Void System.Security.AccessControl.MutexAccessRule::.ctor(System.String,System.Security.AccessControl.MutexRights,System.Security.AccessControl.AccessControlType)
extern TypeInfo* SecurityIdentifier_t1_1132_il2cpp_TypeInfo_var;
extern "C" void MutexAccessRule__ctor_m1_9933 (MutexAccessRule_t1_1158 * __this, String_t* ___identity, int32_t ___eventRights, int32_t ___type, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SecurityIdentifier_t1_1132_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(838);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___identity;
		SecurityIdentifier_t1_1132 * L_1 = (SecurityIdentifier_t1_1132 *)il2cpp_codegen_object_new (SecurityIdentifier_t1_1132_il2cpp_TypeInfo_var);
		SecurityIdentifier__ctor_m1_11825(L_1, L_0, /*hidden argument*/NULL);
		int32_t L_2 = ___eventRights;
		int32_t L_3 = ___type;
		MutexAccessRule__ctor_m1_9932(__this, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Security.AccessControl.MutexRights System.Security.AccessControl.MutexAccessRule::get_MutexRights()
extern "C" int32_t MutexAccessRule_get_MutexRights_m1_9934 (MutexAccessRule_t1_1158 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___rights_6);
		return L_0;
	}
}
// System.Void System.Security.AccessControl.MutexAuditRule::.ctor(System.Security.Principal.IdentityReference,System.Security.AccessControl.MutexRights,System.Security.AccessControl.AuditFlags)
extern "C" void MutexAuditRule__ctor_m1_9935 (MutexAuditRule_t1_1159 * __this, IdentityReference_t1_1120 * ___identity, int32_t ___eventRights, int32_t ___flags, const MethodInfo* method)
{
	{
		IdentityReference_t1_1120 * L_0 = ___identity;
		int32_t L_1 = ___flags;
		AuditRule__ctor_m1_9713(__this, L_0, 0, 0, 0, 0, L_1, /*hidden argument*/NULL);
		int32_t L_2 = ___eventRights;
		__this->___rights_6 = L_2;
		return;
	}
}
// System.Security.AccessControl.MutexRights System.Security.AccessControl.MutexAuditRule::get_MutexRights()
extern "C" int32_t MutexAuditRule_get_MutexRights_m1_9936 (MutexAuditRule_t1_1159 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___rights_6);
		return L_0;
	}
}
// System.Void System.Security.AccessControl.MutexSecurity::.ctor()
extern "C" void MutexSecurity__ctor_m1_9937 (MutexSecurity_t1_1161 * __this, const MethodInfo* method)
{
	{
		NativeObjectSecurity__ctor_m1_9959(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.AccessControl.MutexSecurity::.ctor(System.String,System.Security.AccessControl.AccessControlSections)
extern "C" void MutexSecurity__ctor_m1_9938 (MutexSecurity_t1_1161 * __this, String_t* ___name, int32_t ___includeSections, const MethodInfo* method)
{
	{
		NativeObjectSecurity__ctor_m1_9959(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Type System.Security.AccessControl.MutexSecurity::get_AccessRightType()
extern const Il2CppType* MutexRights_t1_1160_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern "C" Type_t * MutexSecurity_get_AccessRightType_m1_9939 (MutexSecurity_t1_1161 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MutexRights_t1_1160_0_0_0_var = il2cpp_codegen_type_from_index(861);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(MutexRights_t1_1160_0_0_0_var), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Type System.Security.AccessControl.MutexSecurity::get_AccessRuleType()
extern const Il2CppType* MutexAccessRule_t1_1158_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern "C" Type_t * MutexSecurity_get_AccessRuleType_m1_9940 (MutexSecurity_t1_1161 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MutexAccessRule_t1_1158_0_0_0_var = il2cpp_codegen_type_from_index(862);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(MutexAccessRule_t1_1158_0_0_0_var), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Type System.Security.AccessControl.MutexSecurity::get_AuditRuleType()
extern const Il2CppType* MutexAuditRule_t1_1159_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern "C" Type_t * MutexSecurity_get_AuditRuleType_m1_9941 (MutexSecurity_t1_1161 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MutexAuditRule_t1_1159_0_0_0_var = il2cpp_codegen_type_from_index(863);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(MutexAuditRule_t1_1159_0_0_0_var), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Security.AccessControl.AccessRule System.Security.AccessControl.MutexSecurity::AccessRuleFactory(System.Security.Principal.IdentityReference,System.Int32,System.Boolean,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.AccessControlType)
extern TypeInfo* MutexAccessRule_t1_1158_il2cpp_TypeInfo_var;
extern "C" AccessRule_t1_1111 * MutexSecurity_AccessRuleFactory_m1_9942 (MutexSecurity_t1_1161 * __this, IdentityReference_t1_1120 * ___identityReference, int32_t ___accessMask, bool ___isInherited, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___type, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MutexAccessRule_t1_1158_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(862);
		s_Il2CppMethodIntialized = true;
	}
	{
		IdentityReference_t1_1120 * L_0 = ___identityReference;
		int32_t L_1 = ___accessMask;
		int32_t L_2 = ___type;
		MutexAccessRule_t1_1158 * L_3 = (MutexAccessRule_t1_1158 *)il2cpp_codegen_object_new (MutexAccessRule_t1_1158_il2cpp_TypeInfo_var);
		MutexAccessRule__ctor_m1_9932(L_3, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void System.Security.AccessControl.MutexSecurity::AddAccessRule(System.Security.AccessControl.MutexAccessRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void MutexSecurity_AddAccessRule_m1_9943 (MutexSecurity_t1_1161 * __this, MutexAccessRule_t1_1158 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Security.AccessControl.MutexSecurity::RemoveAccessRule(System.Security.AccessControl.MutexAccessRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" bool MutexSecurity_RemoveAccessRule_m1_9944 (MutexSecurity_t1_1161 * __this, MutexAccessRule_t1_1158 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.MutexSecurity::RemoveAccessRuleAll(System.Security.AccessControl.MutexAccessRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void MutexSecurity_RemoveAccessRuleAll_m1_9945 (MutexSecurity_t1_1161 * __this, MutexAccessRule_t1_1158 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.MutexSecurity::RemoveAccessRuleSpecific(System.Security.AccessControl.MutexAccessRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void MutexSecurity_RemoveAccessRuleSpecific_m1_9946 (MutexSecurity_t1_1161 * __this, MutexAccessRule_t1_1158 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.MutexSecurity::ResetAccessRule(System.Security.AccessControl.MutexAccessRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void MutexSecurity_ResetAccessRule_m1_9947 (MutexSecurity_t1_1161 * __this, MutexAccessRule_t1_1158 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.MutexSecurity::SetAccessRule(System.Security.AccessControl.MutexAccessRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void MutexSecurity_SetAccessRule_m1_9948 (MutexSecurity_t1_1161 * __this, MutexAccessRule_t1_1158 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Security.AccessControl.AuditRule System.Security.AccessControl.MutexSecurity::AuditRuleFactory(System.Security.Principal.IdentityReference,System.Int32,System.Boolean,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.AuditFlags)
extern TypeInfo* MutexAuditRule_t1_1159_il2cpp_TypeInfo_var;
extern "C" AuditRule_t1_1119 * MutexSecurity_AuditRuleFactory_m1_9949 (MutexSecurity_t1_1161 * __this, IdentityReference_t1_1120 * ___identityReference, int32_t ___accessMask, bool ___isInherited, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___flags, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MutexAuditRule_t1_1159_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(863);
		s_Il2CppMethodIntialized = true;
	}
	{
		IdentityReference_t1_1120 * L_0 = ___identityReference;
		int32_t L_1 = ___accessMask;
		int32_t L_2 = ___flags;
		MutexAuditRule_t1_1159 * L_3 = (MutexAuditRule_t1_1159 *)il2cpp_codegen_object_new (MutexAuditRule_t1_1159_il2cpp_TypeInfo_var);
		MutexAuditRule__ctor_m1_9935(L_3, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void System.Security.AccessControl.MutexSecurity::AddAuditRule(System.Security.AccessControl.MutexAuditRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void MutexSecurity_AddAuditRule_m1_9950 (MutexSecurity_t1_1161 * __this, MutexAuditRule_t1_1159 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Security.AccessControl.MutexSecurity::RemoveAuditRule(System.Security.AccessControl.MutexAuditRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" bool MutexSecurity_RemoveAuditRule_m1_9951 (MutexSecurity_t1_1161 * __this, MutexAuditRule_t1_1159 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.MutexSecurity::RemoveAuditRuleAll(System.Security.AccessControl.MutexAuditRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void MutexSecurity_RemoveAuditRuleAll_m1_9952 (MutexSecurity_t1_1161 * __this, MutexAuditRule_t1_1159 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.MutexSecurity::RemoveAuditRuleSpecific(System.Security.AccessControl.MutexAuditRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void MutexSecurity_RemoveAuditRuleSpecific_m1_9953 (MutexSecurity_t1_1161 * __this, MutexAuditRule_t1_1159 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.MutexSecurity::SetAuditRule(System.Security.AccessControl.MutexAuditRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void MutexSecurity_SetAuditRule_m1_9954 (MutexSecurity_t1_1161 * __this, MutexAuditRule_t1_1159 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.NativeObjectSecurity/ExceptionFromErrorCode::.ctor(System.Object,System.IntPtr)
extern "C" void ExceptionFromErrorCode__ctor_m1_9955 (ExceptionFromErrorCode_t1_1162 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Exception System.Security.AccessControl.NativeObjectSecurity/ExceptionFromErrorCode::Invoke(System.Int32,System.String,System.Runtime.InteropServices.SafeHandle,System.Object)
extern "C" Exception_t1_33 * ExceptionFromErrorCode_Invoke_m1_9956 (ExceptionFromErrorCode_t1_1162 * __this, int32_t ___errorCode, String_t* ___name, SafeHandle_t1_88 * ___handle, Object_t * ___context, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		ExceptionFromErrorCode_Invoke_m1_9956((ExceptionFromErrorCode_t1_1162 *)__this->___prev_9,___errorCode, ___name, ___handle, ___context, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef Exception_t1_33 * (*FunctionPointerType) (Object_t *, Object_t * __this, int32_t ___errorCode, String_t* ___name, SafeHandle_t1_88 * ___handle, Object_t * ___context, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___errorCode, ___name, ___handle, ___context,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef Exception_t1_33 * (*FunctionPointerType) (Object_t * __this, int32_t ___errorCode, String_t* ___name, SafeHandle_t1_88 * ___handle, Object_t * ___context, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___errorCode, ___name, ___handle, ___context,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" Exception_t1_33 * pinvoke_delegate_wrapper_ExceptionFromErrorCode_t1_1162(Il2CppObject* delegate, int32_t ___errorCode, String_t* ___name, SafeHandle_t1_88 * ___handle, Object_t * ___context)
{
	// Marshaling of parameter '___context' to native representation
	Object_t * ____context_marshaled = { 0 };
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Object'."));
}
// System.IAsyncResult System.Security.AccessControl.NativeObjectSecurity/ExceptionFromErrorCode::BeginInvoke(System.Int32,System.String,System.Runtime.InteropServices.SafeHandle,System.Object,System.AsyncCallback,System.Object)
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern "C" Object_t * ExceptionFromErrorCode_BeginInvoke_m1_9957 (ExceptionFromErrorCode_t1_1162 * __this, int32_t ___errorCode, String_t* ___name, SafeHandle_t1_88 * ___handle, Object_t * ___context, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[5] = {0};
	__d_args[0] = Box(Int32_t1_3_il2cpp_TypeInfo_var, &___errorCode);
	__d_args[1] = ___name;
	__d_args[2] = ___handle;
	__d_args[3] = ___context;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Exception System.Security.AccessControl.NativeObjectSecurity/ExceptionFromErrorCode::EndInvoke(System.IAsyncResult)
extern "C" Exception_t1_33 * ExceptionFromErrorCode_EndInvoke_m1_9958 (ExceptionFromErrorCode_t1_1162 * __this, Object_t * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return (Exception_t1_33 *)__result;
}
// System.Void System.Security.AccessControl.NativeObjectSecurity::.ctor()
extern "C" void NativeObjectSecurity__ctor_m1_9959 (NativeObjectSecurity_t1_1143 * __this, const MethodInfo* method)
{
	{
		CommonObjectSecurity__ctor_m1_9742(__this, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.AccessControl.NativeObjectSecurity::.ctor(System.Boolean,System.Security.AccessControl.ResourceType)
extern "C" void NativeObjectSecurity__ctor_m1_9960 (NativeObjectSecurity_t1_1143 * __this, bool ___isContainer, int32_t ___resourceType, const MethodInfo* method)
{
	{
		bool L_0 = ___isContainer;
		CommonObjectSecurity__ctor_m1_9742(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.AccessControl.NativeObjectSecurity::.ctor(System.Boolean,System.Security.AccessControl.ResourceType,System.Security.AccessControl.NativeObjectSecurity/ExceptionFromErrorCode,System.Object)
extern "C" void NativeObjectSecurity__ctor_m1_9961 (NativeObjectSecurity_t1_1143 * __this, bool ___isContainer, int32_t ___resourceType, ExceptionFromErrorCode_t1_1162 * ___exceptionFromErrorCode, Object_t * ___exceptionContext, const MethodInfo* method)
{
	{
		bool L_0 = ___isContainer;
		int32_t L_1 = ___resourceType;
		NativeObjectSecurity__ctor_m1_9960(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.AccessControl.NativeObjectSecurity::.ctor(System.Boolean,System.Security.AccessControl.ResourceType,System.Runtime.InteropServices.SafeHandle,System.Security.AccessControl.AccessControlSections)
extern "C" void NativeObjectSecurity__ctor_m1_9962 (NativeObjectSecurity_t1_1143 * __this, bool ___isContainer, int32_t ___resourceType, SafeHandle_t1_88 * ___handle, int32_t ___includeSections, const MethodInfo* method)
{
	{
		bool L_0 = ___isContainer;
		int32_t L_1 = ___resourceType;
		NativeObjectSecurity__ctor_m1_9960(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.AccessControl.NativeObjectSecurity::.ctor(System.Boolean,System.Security.AccessControl.ResourceType,System.String,System.Security.AccessControl.AccessControlSections)
extern "C" void NativeObjectSecurity__ctor_m1_9963 (NativeObjectSecurity_t1_1143 * __this, bool ___isContainer, int32_t ___resourceType, String_t* ___name, int32_t ___includeSections, const MethodInfo* method)
{
	{
		bool L_0 = ___isContainer;
		int32_t L_1 = ___resourceType;
		NativeObjectSecurity__ctor_m1_9960(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.AccessControl.NativeObjectSecurity::.ctor(System.Boolean,System.Security.AccessControl.ResourceType,System.Runtime.InteropServices.SafeHandle,System.Security.AccessControl.AccessControlSections,System.Security.AccessControl.NativeObjectSecurity/ExceptionFromErrorCode,System.Object)
extern "C" void NativeObjectSecurity__ctor_m1_9964 (NativeObjectSecurity_t1_1143 * __this, bool ___isContainer, int32_t ___resourceType, SafeHandle_t1_88 * ___handle, int32_t ___includeSections, ExceptionFromErrorCode_t1_1162 * ___exceptionFromErrorCode, Object_t * ___exceptionContext, const MethodInfo* method)
{
	{
		bool L_0 = ___isContainer;
		int32_t L_1 = ___resourceType;
		SafeHandle_t1_88 * L_2 = ___handle;
		int32_t L_3 = ___includeSections;
		NativeObjectSecurity__ctor_m1_9962(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.AccessControl.NativeObjectSecurity::.ctor(System.Boolean,System.Security.AccessControl.ResourceType,System.String,System.Security.AccessControl.AccessControlSections,System.Security.AccessControl.NativeObjectSecurity/ExceptionFromErrorCode,System.Object)
extern "C" void NativeObjectSecurity__ctor_m1_9965 (NativeObjectSecurity_t1_1143 * __this, bool ___isContainer, int32_t ___resourceType, String_t* ___name, int32_t ___includeSections, ExceptionFromErrorCode_t1_1162 * ___exceptionFromErrorCode, Object_t * ___exceptionContext, const MethodInfo* method)
{
	{
		bool L_0 = ___isContainer;
		int32_t L_1 = ___resourceType;
		String_t* L_2 = ___name;
		int32_t L_3 = ___includeSections;
		NativeObjectSecurity__ctor_m1_9963(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.AccessControl.NativeObjectSecurity::Persist(System.Runtime.InteropServices.SafeHandle,System.Security.AccessControl.AccessControlSections)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void NativeObjectSecurity_Persist_m1_9966 (NativeObjectSecurity_t1_1143 * __this, SafeHandle_t1_88 * ___handle, int32_t ___includeSections, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.NativeObjectSecurity::Persist(System.String,System.Security.AccessControl.AccessControlSections)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void NativeObjectSecurity_Persist_m1_9967 (NativeObjectSecurity_t1_1143 * __this, String_t* ___name, int32_t ___includeSections, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.NativeObjectSecurity::Persist(System.Runtime.InteropServices.SafeHandle,System.Security.AccessControl.AccessControlSections,System.Object)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void NativeObjectSecurity_Persist_m1_9968 (NativeObjectSecurity_t1_1143 * __this, SafeHandle_t1_88 * ___handle, int32_t ___includeSections, Object_t * ___exceptionContext, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.NativeObjectSecurity::Persist(System.String,System.Security.AccessControl.AccessControlSections,System.Object)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void NativeObjectSecurity_Persist_m1_9969 (NativeObjectSecurity_t1_1143 * __this, String_t* ___name, int32_t ___includeSections, Object_t * ___exceptionContext, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.ObjectAccessRule::.ctor(System.Security.Principal.IdentityReference,System.Int32,System.Boolean,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Guid,System.Guid,System.Security.AccessControl.AccessControlType)
extern "C" void ObjectAccessRule__ctor_m1_9970 (ObjectAccessRule_t1_1163 * __this, IdentityReference_t1_1120 * ___identity, int32_t ___accessMask, bool ___isInherited, int32_t ___inheritanceFlags, int32_t ___propagationFlags, Guid_t1_319  ___objectType, Guid_t1_319  ___inheritedObjectType, int32_t ___type, const MethodInfo* method)
{
	{
		IdentityReference_t1_1120 * L_0 = ___identity;
		int32_t L_1 = ___accessMask;
		bool L_2 = ___isInherited;
		int32_t L_3 = ___inheritanceFlags;
		int32_t L_4 = ___propagationFlags;
		int32_t L_5 = ___type;
		AccessRule__ctor_m1_9706(__this, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		Guid_t1_319  L_6 = ___objectType;
		__this->___object_type_6 = L_6;
		Guid_t1_319  L_7 = ___inheritedObjectType;
		__this->___inherited_object_type_7 = L_7;
		return;
	}
}
// System.Guid System.Security.AccessControl.ObjectAccessRule::get_InheritedObjectType()
extern "C" Guid_t1_319  ObjectAccessRule_get_InheritedObjectType_m1_9971 (ObjectAccessRule_t1_1163 * __this, const MethodInfo* method)
{
	{
		Guid_t1_319  L_0 = (__this->___inherited_object_type_7);
		return L_0;
	}
}
// System.Security.AccessControl.ObjectAceFlags System.Security.AccessControl.ObjectAccessRule::get_ObjectFlags()
extern TypeInfo* Guid_t1_319_il2cpp_TypeInfo_var;
extern "C" int32_t ObjectAccessRule_get_ObjectFlags_m1_9972 (ObjectAccessRule_t1_1163 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Guid_t1_319_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(208);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	{
		V_0 = 0;
		Guid_t1_319  L_0 = (__this->___object_type_6);
		IL2CPP_RUNTIME_CLASS_INIT(Guid_t1_319_il2cpp_TypeInfo_var);
		Guid_t1_319  L_1 = ((Guid_t1_319_StaticFields*)Guid_t1_319_il2cpp_TypeInfo_var->static_fields)->___Empty_11;
		bool L_2 = Guid_op_Inequality_m1_14157(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001b;
		}
	}
	{
		int32_t L_3 = V_0;
		V_0 = ((int32_t)((int32_t)L_3|(int32_t)1));
	}

IL_001b:
	{
		Guid_t1_319  L_4 = (__this->___inherited_object_type_7);
		IL2CPP_RUNTIME_CLASS_INIT(Guid_t1_319_il2cpp_TypeInfo_var);
		Guid_t1_319  L_5 = ((Guid_t1_319_StaticFields*)Guid_t1_319_il2cpp_TypeInfo_var->static_fields)->___Empty_11;
		bool L_6 = Guid_op_Inequality_m1_14157(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0034;
		}
	}
	{
		int32_t L_7 = V_0;
		V_0 = ((int32_t)((int32_t)L_7|(int32_t)2));
	}

IL_0034:
	{
		int32_t L_8 = V_0;
		return L_8;
	}
}
// System.Guid System.Security.AccessControl.ObjectAccessRule::get_ObjectType()
extern "C" Guid_t1_319  ObjectAccessRule_get_ObjectType_m1_9973 (ObjectAccessRule_t1_1163 * __this, const MethodInfo* method)
{
	{
		Guid_t1_319  L_0 = (__this->___object_type_6);
		return L_0;
	}
}
// System.Void System.Security.AccessControl.ObjectAce::.ctor(System.Security.AccessControl.AceFlags,System.Security.AccessControl.AceQualifier,System.Int32,System.Security.Principal.SecurityIdentifier,System.Security.AccessControl.ObjectAceFlags,System.Guid,System.Guid,System.Boolean,System.Byte[])
extern "C" void ObjectAce__ctor_m1_9974 (ObjectAce_t1_1164 * __this, uint8_t ___aceFlags, int32_t ___qualifier, int32_t ___accessMask, SecurityIdentifier_t1_1132 * ___sid, int32_t ___flags, Guid_t1_319  ___type, Guid_t1_319  ___inheritedType, bool ___isCallback, ByteU5BU5D_t1_109* ___opaque, const MethodInfo* method)
{
	{
		int32_t L_0 = ___qualifier;
		bool L_1 = ___isCallback;
		ByteU5BU5D_t1_109* L_2 = ___opaque;
		QualifiedAce__ctor_m1_10033(__this, 0, 0, L_0, L_1, L_2, /*hidden argument*/NULL);
		uint8_t L_3 = ___aceFlags;
		GenericAce_set_AceFlags_m1_9901(__this, L_3, /*hidden argument*/NULL);
		SecurityIdentifier_t1_1132 * L_4 = ___sid;
		KnownAce_set_SecurityIdentifier_m1_9931(__this, L_4, /*hidden argument*/NULL);
		int32_t L_5 = ___flags;
		__this->___object_ace_flags_11 = L_5;
		Guid_t1_319  L_6 = ___type;
		__this->___object_ace_type_9 = L_6;
		Guid_t1_319  L_7 = ___inheritedType;
		__this->___inherited_object_type_10 = L_7;
		return;
	}
}
// System.Int32 System.Security.AccessControl.ObjectAce::get_BinaryLength()
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" int32_t ObjectAce_get_BinaryLength_m1_9975 (ObjectAce_t1_1164 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Guid System.Security.AccessControl.ObjectAce::get_InheritedObjectAceType()
extern "C" Guid_t1_319  ObjectAce_get_InheritedObjectAceType_m1_9976 (ObjectAce_t1_1164 * __this, const MethodInfo* method)
{
	{
		Guid_t1_319  L_0 = (__this->___inherited_object_type_10);
		return L_0;
	}
}
// System.Void System.Security.AccessControl.ObjectAce::set_InheritedObjectAceType(System.Guid)
extern "C" void ObjectAce_set_InheritedObjectAceType_m1_9977 (ObjectAce_t1_1164 * __this, Guid_t1_319  ___value, const MethodInfo* method)
{
	{
		Guid_t1_319  L_0 = ___value;
		__this->___inherited_object_type_10 = L_0;
		return;
	}
}
// System.Security.AccessControl.ObjectAceFlags System.Security.AccessControl.ObjectAce::get_ObjectAceFlags()
extern "C" int32_t ObjectAce_get_ObjectAceFlags_m1_9978 (ObjectAce_t1_1164 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___object_ace_flags_11);
		return L_0;
	}
}
// System.Void System.Security.AccessControl.ObjectAce::set_ObjectAceFlags(System.Security.AccessControl.ObjectAceFlags)
extern "C" void ObjectAce_set_ObjectAceFlags_m1_9979 (ObjectAce_t1_1164 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___object_ace_flags_11 = L_0;
		return;
	}
}
// System.Guid System.Security.AccessControl.ObjectAce::get_ObjectAceType()
extern "C" Guid_t1_319  ObjectAce_get_ObjectAceType_m1_9980 (ObjectAce_t1_1164 * __this, const MethodInfo* method)
{
	{
		Guid_t1_319  L_0 = (__this->___object_ace_type_9);
		return L_0;
	}
}
// System.Void System.Security.AccessControl.ObjectAce::set_ObjectAceType(System.Guid)
extern "C" void ObjectAce_set_ObjectAceType_m1_9981 (ObjectAce_t1_1164 * __this, Guid_t1_319  ___value, const MethodInfo* method)
{
	{
		Guid_t1_319  L_0 = ___value;
		__this->___object_ace_type_9 = L_0;
		return;
	}
}
// System.Void System.Security.AccessControl.ObjectAce::GetBinaryForm(System.Byte[],System.Int32)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void ObjectAce_GetBinaryForm_m1_9982 (ObjectAce_t1_1164 * __this, ByteU5BU5D_t1_109* ___binaryForm, int32_t ___offset, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Int32 System.Security.AccessControl.ObjectAce::MaxOpaqueLength(System.Boolean)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" int32_t ObjectAce_MaxOpaqueLength_m1_9983 (Object_t * __this /* static, unused */, bool ___isCallback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.ObjectAuditRule::.ctor(System.Security.Principal.IdentityReference,System.Int32,System.Boolean,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Guid,System.Guid,System.Security.AccessControl.AuditFlags)
extern "C" void ObjectAuditRule__ctor_m1_9984 (ObjectAuditRule_t1_1166 * __this, IdentityReference_t1_1120 * ___identity, int32_t ___accessMask, bool ___isInherited, int32_t ___inheritanceFlags, int32_t ___propagationFlags, Guid_t1_319  ___objectType, Guid_t1_319  ___inheritedObjectType, int32_t ___auditFlags, const MethodInfo* method)
{
	{
		IdentityReference_t1_1120 * L_0 = ___identity;
		int32_t L_1 = ___accessMask;
		bool L_2 = ___isInherited;
		int32_t L_3 = ___inheritanceFlags;
		int32_t L_4 = ___propagationFlags;
		int32_t L_5 = ___auditFlags;
		AuditRule__ctor_m1_9713(__this, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		Guid_t1_319  L_6 = ___objectType;
		__this->___object_type_7 = L_6;
		Guid_t1_319  L_7 = ___inheritedObjectType;
		__this->___inherited_object_type_6 = L_7;
		return;
	}
}
// System.Guid System.Security.AccessControl.ObjectAuditRule::get_InheritedObjectType()
extern "C" Guid_t1_319  ObjectAuditRule_get_InheritedObjectType_m1_9985 (ObjectAuditRule_t1_1166 * __this, const MethodInfo* method)
{
	{
		Guid_t1_319  L_0 = (__this->___inherited_object_type_6);
		return L_0;
	}
}
// System.Security.AccessControl.ObjectAceFlags System.Security.AccessControl.ObjectAuditRule::get_ObjectFlags()
extern TypeInfo* Guid_t1_319_il2cpp_TypeInfo_var;
extern "C" int32_t ObjectAuditRule_get_ObjectFlags_m1_9986 (ObjectAuditRule_t1_1166 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Guid_t1_319_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(208);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	{
		V_0 = 0;
		Guid_t1_319  L_0 = (__this->___object_type_7);
		IL2CPP_RUNTIME_CLASS_INIT(Guid_t1_319_il2cpp_TypeInfo_var);
		Guid_t1_319  L_1 = ((Guid_t1_319_StaticFields*)Guid_t1_319_il2cpp_TypeInfo_var->static_fields)->___Empty_11;
		bool L_2 = Guid_op_Inequality_m1_14157(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001b;
		}
	}
	{
		int32_t L_3 = V_0;
		V_0 = ((int32_t)((int32_t)L_3|(int32_t)1));
	}

IL_001b:
	{
		Guid_t1_319  L_4 = (__this->___inherited_object_type_6);
		IL2CPP_RUNTIME_CLASS_INIT(Guid_t1_319_il2cpp_TypeInfo_var);
		Guid_t1_319  L_5 = ((Guid_t1_319_StaticFields*)Guid_t1_319_il2cpp_TypeInfo_var->static_fields)->___Empty_11;
		bool L_6 = Guid_op_Inequality_m1_14157(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0034;
		}
	}
	{
		int32_t L_7 = V_0;
		V_0 = ((int32_t)((int32_t)L_7|(int32_t)2));
	}

IL_0034:
	{
		int32_t L_8 = V_0;
		return L_8;
	}
}
// System.Guid System.Security.AccessControl.ObjectAuditRule::get_ObjectType()
extern "C" Guid_t1_319  ObjectAuditRule_get_ObjectType_m1_9987 (ObjectAuditRule_t1_1166 * __this, const MethodInfo* method)
{
	{
		Guid_t1_319  L_0 = (__this->___object_type_7);
		return L_0;
	}
}
// System.Void System.Security.AccessControl.ObjectSecurity::.ctor()
extern "C" void ObjectSecurity__ctor_m1_9988 (ObjectSecurity_t1_1127 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.AccessControl.ObjectSecurity::.ctor(System.Boolean,System.Boolean)
extern "C" void ObjectSecurity__ctor_m1_9989 (ObjectSecurity_t1_1127 * __this, bool ___isContainer, bool ___isDS, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		bool L_0 = ___isContainer;
		__this->___is_container_0 = L_0;
		bool L_1 = ___isDS;
		__this->___is_ds_1 = L_1;
		return;
	}
}
// System.Boolean System.Security.AccessControl.ObjectSecurity::get_AreAccessRulesCanonical()
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" bool ObjectSecurity_get_AreAccessRulesCanonical_m1_9990 (ObjectSecurity_t1_1127 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Security.AccessControl.ObjectSecurity::get_AreAccessRulesProtected()
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" bool ObjectSecurity_get_AreAccessRulesProtected_m1_9991 (ObjectSecurity_t1_1127 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Security.AccessControl.ObjectSecurity::get_AreAuditRulesCanonical()
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" bool ObjectSecurity_get_AreAuditRulesCanonical_m1_9992 (ObjectSecurity_t1_1127 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Security.AccessControl.ObjectSecurity::get_AreAuditRulesProtected()
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" bool ObjectSecurity_get_AreAuditRulesProtected_m1_9993 (ObjectSecurity_t1_1127 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Security.AccessControl.ObjectSecurity::get_AccessRulesModified()
extern "C" bool ObjectSecurity_get_AccessRulesModified_m1_9994 (ObjectSecurity_t1_1127 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___access_rules_modified_2);
		return L_0;
	}
}
// System.Void System.Security.AccessControl.ObjectSecurity::set_AccessRulesModified(System.Boolean)
extern "C" void ObjectSecurity_set_AccessRulesModified_m1_9995 (ObjectSecurity_t1_1127 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___access_rules_modified_2 = L_0;
		return;
	}
}
// System.Boolean System.Security.AccessControl.ObjectSecurity::get_AuditRulesModified()
extern "C" bool ObjectSecurity_get_AuditRulesModified_m1_9996 (ObjectSecurity_t1_1127 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___audit_rules_modified_3);
		return L_0;
	}
}
// System.Void System.Security.AccessControl.ObjectSecurity::set_AuditRulesModified(System.Boolean)
extern "C" void ObjectSecurity_set_AuditRulesModified_m1_9997 (ObjectSecurity_t1_1127 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___audit_rules_modified_3 = L_0;
		return;
	}
}
// System.Boolean System.Security.AccessControl.ObjectSecurity::get_GroupModified()
extern "C" bool ObjectSecurity_get_GroupModified_m1_9998 (ObjectSecurity_t1_1127 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___group_modified_4);
		return L_0;
	}
}
// System.Void System.Security.AccessControl.ObjectSecurity::set_GroupModified(System.Boolean)
extern "C" void ObjectSecurity_set_GroupModified_m1_9999 (ObjectSecurity_t1_1127 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___group_modified_4 = L_0;
		return;
	}
}
// System.Boolean System.Security.AccessControl.ObjectSecurity::get_IsContainer()
extern "C" bool ObjectSecurity_get_IsContainer_m1_10000 (ObjectSecurity_t1_1127 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___is_container_0);
		return L_0;
	}
}
// System.Boolean System.Security.AccessControl.ObjectSecurity::get_IsDS()
extern "C" bool ObjectSecurity_get_IsDS_m1_10001 (ObjectSecurity_t1_1127 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___is_ds_1);
		return L_0;
	}
}
// System.Boolean System.Security.AccessControl.ObjectSecurity::get_OwnerModified()
extern "C" bool ObjectSecurity_get_OwnerModified_m1_10002 (ObjectSecurity_t1_1127 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___owner_modified_5);
		return L_0;
	}
}
// System.Void System.Security.AccessControl.ObjectSecurity::set_OwnerModified(System.Boolean)
extern "C" void ObjectSecurity_set_OwnerModified_m1_10003 (ObjectSecurity_t1_1127 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___owner_modified_5 = L_0;
		return;
	}
}
// System.Security.Principal.IdentityReference System.Security.AccessControl.ObjectSecurity::GetGroup(System.Type)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" IdentityReference_t1_1120 * ObjectSecurity_GetGroup_m1_10004 (ObjectSecurity_t1_1127 * __this, Type_t * ___targetType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Security.Principal.IdentityReference System.Security.AccessControl.ObjectSecurity::GetOwner(System.Type)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" IdentityReference_t1_1120 * ObjectSecurity_GetOwner_m1_10005 (ObjectSecurity_t1_1127 * __this, Type_t * ___targetType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Byte[] System.Security.AccessControl.ObjectSecurity::GetSecurityDescriptorBinaryForm()
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" ByteU5BU5D_t1_109* ObjectSecurity_GetSecurityDescriptorBinaryForm_m1_10006 (ObjectSecurity_t1_1127 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.String System.Security.AccessControl.ObjectSecurity::GetSecurityDescriptorSddlForm(System.Security.AccessControl.AccessControlSections)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" String_t* ObjectSecurity_GetSecurityDescriptorSddlForm_m1_10007 (ObjectSecurity_t1_1127 * __this, int32_t ___includeSections, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Security.AccessControl.ObjectSecurity::IsSddlConversionSupported()
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" bool ObjectSecurity_IsSddlConversionSupported_m1_10008 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Security.AccessControl.ObjectSecurity::ModifyAccessRule(System.Security.AccessControl.AccessControlModification,System.Security.AccessControl.AccessRule,System.Boolean&)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" bool ObjectSecurity_ModifyAccessRule_m1_10009 (ObjectSecurity_t1_1127 * __this, int32_t ___modification, AccessRule_t1_1111 * ___rule, bool* ___modified, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Security.AccessControl.ObjectSecurity::ModifyAuditRule(System.Security.AccessControl.AccessControlModification,System.Security.AccessControl.AuditRule,System.Boolean&)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" bool ObjectSecurity_ModifyAuditRule_m1_10010 (ObjectSecurity_t1_1127 * __this, int32_t ___modification, AuditRule_t1_1119 * ___rule, bool* ___modified, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.ObjectSecurity::PurgeAccessRules(System.Security.Principal.IdentityReference)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void ObjectSecurity_PurgeAccessRules_m1_10011 (ObjectSecurity_t1_1127 * __this, IdentityReference_t1_1120 * ___identity, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.ObjectSecurity::PurgeAuditRules(System.Security.Principal.IdentityReference)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void ObjectSecurity_PurgeAuditRules_m1_10012 (ObjectSecurity_t1_1127 * __this, IdentityReference_t1_1120 * ___identity, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.ObjectSecurity::SetAccessRuleProtection(System.Boolean,System.Boolean)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void ObjectSecurity_SetAccessRuleProtection_m1_10013 (ObjectSecurity_t1_1127 * __this, bool ___isProtected, bool ___preserveInheritance, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.ObjectSecurity::SetAuditRuleProtection(System.Boolean,System.Boolean)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void ObjectSecurity_SetAuditRuleProtection_m1_10014 (ObjectSecurity_t1_1127 * __this, bool ___isProtected, bool ___preserveInheritance, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.ObjectSecurity::SetGroup(System.Security.Principal.IdentityReference)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void ObjectSecurity_SetGroup_m1_10015 (ObjectSecurity_t1_1127 * __this, IdentityReference_t1_1120 * ___identity, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.ObjectSecurity::SetOwner(System.Security.Principal.IdentityReference)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void ObjectSecurity_SetOwner_m1_10016 (ObjectSecurity_t1_1127 * __this, IdentityReference_t1_1120 * ___identity, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.ObjectSecurity::SetSecurityDescriptorBinaryForm(System.Byte[])
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void ObjectSecurity_SetSecurityDescriptorBinaryForm_m1_10017 (ObjectSecurity_t1_1127 * __this, ByteU5BU5D_t1_109* ___binaryForm, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.ObjectSecurity::SetSecurityDescriptorBinaryForm(System.Byte[],System.Security.AccessControl.AccessControlSections)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void ObjectSecurity_SetSecurityDescriptorBinaryForm_m1_10018 (ObjectSecurity_t1_1127 * __this, ByteU5BU5D_t1_109* ___binaryForm, int32_t ___includeSections, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.ObjectSecurity::SetSecurityDescriptorSddlForm(System.String)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void ObjectSecurity_SetSecurityDescriptorSddlForm_m1_10019 (ObjectSecurity_t1_1127 * __this, String_t* ___sddlForm, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.ObjectSecurity::SetSecurityDescriptorSddlForm(System.String,System.Security.AccessControl.AccessControlSections)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void ObjectSecurity_SetSecurityDescriptorSddlForm_m1_10020 (ObjectSecurity_t1_1127 * __this, String_t* ___sddlForm, int32_t ___includeSections, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.ObjectSecurity::Persist(System.Runtime.InteropServices.SafeHandle,System.Security.AccessControl.AccessControlSections)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void ObjectSecurity_Persist_m1_10021 (ObjectSecurity_t1_1127 * __this, SafeHandle_t1_88 * ___handle, int32_t ___includeSections, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.ObjectSecurity::Persist(System.String,System.Security.AccessControl.AccessControlSections)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void ObjectSecurity_Persist_m1_10022 (ObjectSecurity_t1_1127 * __this, String_t* ___name, int32_t ___includeSections, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.ObjectSecurity::Persist(System.Boolean,System.String,System.Security.AccessControl.AccessControlSections)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void ObjectSecurity_Persist_m1_10023 (ObjectSecurity_t1_1127 * __this, bool ___enableOwnershipPrivilege, String_t* ___name, int32_t ___includeSections, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.ObjectSecurity::ReadLock()
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void ObjectSecurity_ReadLock_m1_10024 (ObjectSecurity_t1_1127 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.ObjectSecurity::ReadUnlock()
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void ObjectSecurity_ReadUnlock_m1_10025 (ObjectSecurity_t1_1127 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.ObjectSecurity::WriteLock()
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void ObjectSecurity_WriteLock_m1_10026 (ObjectSecurity_t1_1127 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.ObjectSecurity::WriteUnlock()
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void ObjectSecurity_WriteUnlock_m1_10027 (ObjectSecurity_t1_1127 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.PrivilegeNotHeldException::.ctor()
extern "C" void PrivilegeNotHeldException__ctor_m1_10028 (PrivilegeNotHeldException_t1_1167 * __this, const MethodInfo* method)
{
	{
		UnauthorizedAccessException__ctor_m1_14705(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.AccessControl.PrivilegeNotHeldException::.ctor(System.String)
extern "C" void PrivilegeNotHeldException__ctor_m1_10029 (PrivilegeNotHeldException_t1_1167 * __this, String_t* ___privilege, const MethodInfo* method)
{
	{
		String_t* L_0 = ___privilege;
		UnauthorizedAccessException__ctor_m1_14706(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.AccessControl.PrivilegeNotHeldException::.ctor(System.String,System.Exception)
extern "C" void PrivilegeNotHeldException__ctor_m1_10030 (PrivilegeNotHeldException_t1_1167 * __this, String_t* ___privilege, Exception_t1_33 * ___inner, const MethodInfo* method)
{
	{
		String_t* L_0 = ___privilege;
		Exception_t1_33 * L_1 = ___inner;
		UnauthorizedAccessException__ctor_m1_14707(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String System.Security.AccessControl.PrivilegeNotHeldException::get_PrivilegeName()
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" String_t* PrivilegeNotHeldException_get_PrivilegeName_m1_10031 (PrivilegeNotHeldException_t1_1167 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.PrivilegeNotHeldException::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void PrivilegeNotHeldException_GetObjectData_m1_10032 (PrivilegeNotHeldException_t1_1167 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.QualifiedAce::.ctor(System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.AceQualifier,System.Boolean,System.Byte[])
extern "C" void QualifiedAce__ctor_m1_10033 (QualifiedAce_t1_1123 * __this, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___aceQualifier, bool ___isCallback, ByteU5BU5D_t1_109* ___opaque, const MethodInfo* method)
{
	{
		int32_t L_0 = ___inheritanceFlags;
		int32_t L_1 = ___propagationFlags;
		KnownAce__ctor_m1_9927(__this, L_0, L_1, /*hidden argument*/NULL);
		int32_t L_2 = ___aceQualifier;
		__this->___ace_qualifier_6 = L_2;
		bool L_3 = ___isCallback;
		__this->___is_callback_7 = L_3;
		ByteU5BU5D_t1_109* L_4 = ___opaque;
		QualifiedAce_SetOpaque_m1_10038(__this, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Security.AccessControl.AceQualifier System.Security.AccessControl.QualifiedAce::get_AceQualifier()
extern "C" int32_t QualifiedAce_get_AceQualifier_m1_10034 (QualifiedAce_t1_1123 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___ace_qualifier_6);
		return L_0;
	}
}
// System.Boolean System.Security.AccessControl.QualifiedAce::get_IsCallback()
extern "C" bool QualifiedAce_get_IsCallback_m1_10035 (QualifiedAce_t1_1123 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___is_callback_7);
		return L_0;
	}
}
// System.Int32 System.Security.AccessControl.QualifiedAce::get_OpaqueLength()
extern "C" int32_t QualifiedAce_get_OpaqueLength_m1_10036 (QualifiedAce_t1_1123 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t1_109* L_0 = (__this->___opaque_8);
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Array_t *)L_0)->max_length))));
	}
}
// System.Byte[] System.Security.AccessControl.QualifiedAce::GetOpaque()
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern "C" ByteU5BU5D_t1_109* QualifiedAce_GetOpaque_m1_10037 (QualifiedAce_t1_1123 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		s_Il2CppMethodIntialized = true;
	}
	{
		ByteU5BU5D_t1_109* L_0 = (__this->___opaque_8);
		NullCheck(L_0);
		Object_t * L_1 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(5 /* System.Object System.Array::Clone() */, L_0);
		return ((ByteU5BU5D_t1_109*)Castclass(L_1, ByteU5BU5D_t1_109_il2cpp_TypeInfo_var));
	}
}
// System.Void System.Security.AccessControl.QualifiedAce::SetOpaque(System.Byte[])
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2528;
extern "C" void QualifiedAce_SetOpaque_m1_10038 (QualifiedAce_t1_1123 * __this, ByteU5BU5D_t1_109* ___opaque, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		_stringLiteral2528 = il2cpp_codegen_string_literal_from_index(2528);
		s_Il2CppMethodIntialized = true;
	}
	{
		ByteU5BU5D_t1_109* L_0 = ___opaque;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, _stringLiteral2528, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		ByteU5BU5D_t1_109* L_2 = ___opaque;
		NullCheck(L_2);
		Object_t * L_3 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(5 /* System.Object System.Array::Clone() */, L_2);
		__this->___opaque_8 = ((ByteU5BU5D_t1_109*)Castclass(L_3, ByteU5BU5D_t1_109_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void System.Security.AccessControl.RawAcl::.ctor(System.Byte,System.Int32)
extern TypeInfo* GenericAcl_t1_1114_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t1_1125_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1_14897_MethodInfo_var;
extern "C" void RawAcl__ctor_m1_10039 (RawAcl_t1_1170 * __this, uint8_t ___revision, int32_t ___capacity, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GenericAcl_t1_1114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(841);
		List_1_t1_1125_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(842);
		List_1__ctor_m1_14897_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483689);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GenericAcl_t1_1114_il2cpp_TypeInfo_var);
		GenericAcl__ctor_m1_9913(__this, /*hidden argument*/NULL);
		uint8_t L_0 = ___revision;
		__this->___revision_3 = L_0;
		int32_t L_1 = ___capacity;
		List_1_t1_1125 * L_2 = (List_1_t1_1125 *)il2cpp_codegen_object_new (List_1_t1_1125_il2cpp_TypeInfo_var);
		List_1__ctor_m1_14897(L_2, L_1, /*hidden argument*/List_1__ctor_m1_14897_MethodInfo_var);
		__this->___list_4 = L_2;
		return;
	}
}
// System.Void System.Security.AccessControl.RawAcl::.ctor(System.Byte[],System.Int32)
extern "C" void RawAcl__ctor_m1_10040 (RawAcl_t1_1170 * __this, ByteU5BU5D_t1_109* ___binaryForm, int32_t ___offset, const MethodInfo* method)
{
	{
		RawAcl__ctor_m1_10039(__this, 0, ((int32_t)10), /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 System.Security.AccessControl.RawAcl::get_BinaryLength()
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" int32_t RawAcl_get_BinaryLength_m1_10041 (RawAcl_t1_1170 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Int32 System.Security.AccessControl.RawAcl::get_Count()
extern "C" int32_t RawAcl_get_Count_m1_10042 (RawAcl_t1_1170 * __this, const MethodInfo* method)
{
	{
		List_1_t1_1125 * L_0 = (__this->___list_4);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.Security.AccessControl.GenericAce>::get_Count() */, L_0);
		return L_1;
	}
}
// System.Security.AccessControl.GenericAce System.Security.AccessControl.RawAcl::get_Item(System.Int32)
extern "C" GenericAce_t1_1145 * RawAcl_get_Item_m1_10043 (RawAcl_t1_1170 * __this, int32_t ___index, const MethodInfo* method)
{
	{
		List_1_t1_1125 * L_0 = (__this->___list_4);
		int32_t L_1 = ___index;
		NullCheck(L_0);
		GenericAce_t1_1145 * L_2 = (GenericAce_t1_1145 *)VirtFuncInvoker1< GenericAce_t1_1145 *, int32_t >::Invoke(31 /* T System.Collections.Generic.List`1<System.Security.AccessControl.GenericAce>::get_Item(System.Int32) */, L_0, L_1);
		return L_2;
	}
}
// System.Void System.Security.AccessControl.RawAcl::set_Item(System.Int32,System.Security.AccessControl.GenericAce)
extern "C" void RawAcl_set_Item_m1_10044 (RawAcl_t1_1170 * __this, int32_t ___index, GenericAce_t1_1145 * ___value, const MethodInfo* method)
{
	{
		List_1_t1_1125 * L_0 = (__this->___list_4);
		int32_t L_1 = ___index;
		GenericAce_t1_1145 * L_2 = ___value;
		NullCheck(L_0);
		VirtActionInvoker2< int32_t, GenericAce_t1_1145 * >::Invoke(32 /* System.Void System.Collections.Generic.List`1<System.Security.AccessControl.GenericAce>::set_Item(System.Int32,T) */, L_0, L_1, L_2);
		return;
	}
}
// System.Byte System.Security.AccessControl.RawAcl::get_Revision()
extern "C" uint8_t RawAcl_get_Revision_m1_10045 (RawAcl_t1_1170 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = (__this->___revision_3);
		return L_0;
	}
}
// System.Void System.Security.AccessControl.RawAcl::GetBinaryForm(System.Byte[],System.Int32)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void RawAcl_GetBinaryForm_m1_10046 (RawAcl_t1_1170 * __this, ByteU5BU5D_t1_109* ___binaryForm, int32_t ___offset, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.RawAcl::InsertAce(System.Int32,System.Security.AccessControl.GenericAce)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2533;
extern "C" void RawAcl_InsertAce_m1_10047 (RawAcl_t1_1170 * __this, int32_t ___index, GenericAce_t1_1145 * ___ace, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		_stringLiteral2533 = il2cpp_codegen_string_literal_from_index(2533);
		s_Il2CppMethodIntialized = true;
	}
	{
		GenericAce_t1_1145 * L_0 = ___ace;
		bool L_1 = GenericAce_op_Equality_m1_9911(NULL /*static, unused*/, L_0, (GenericAce_t1_1145 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_2 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_2, _stringLiteral2533, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		List_1_t1_1125 * L_3 = (__this->___list_4);
		int32_t L_4 = ___index;
		GenericAce_t1_1145 * L_5 = ___ace;
		NullCheck(L_3);
		VirtActionInvoker2< int32_t, GenericAce_t1_1145 * >::Invoke(29 /* System.Void System.Collections.Generic.List`1<System.Security.AccessControl.GenericAce>::Insert(System.Int32,T) */, L_3, L_4, L_5);
		return;
	}
}
// System.Void System.Security.AccessControl.RawAcl::RemoveAce(System.Int32)
extern "C" void RawAcl_RemoveAce_m1_10048 (RawAcl_t1_1170 * __this, int32_t ___index, const MethodInfo* method)
{
	{
		List_1_t1_1125 * L_0 = (__this->___list_4);
		int32_t L_1 = ___index;
		NullCheck(L_0);
		VirtActionInvoker1< int32_t >::Invoke(30 /* System.Void System.Collections.Generic.List`1<System.Security.AccessControl.GenericAce>::RemoveAt(System.Int32) */, L_0, L_1);
		return;
	}
}
// System.Void System.Security.AccessControl.RawSecurityDescriptor::.ctor(System.String)
extern "C" void RawSecurityDescriptor__ctor_m1_10049 (RawSecurityDescriptor_t1_1171 * __this, String_t* ___sddlForm, const MethodInfo* method)
{
	{
		GenericSecurityDescriptor__ctor_m1_9921(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.AccessControl.RawSecurityDescriptor::.ctor(System.Byte[],System.Int32)
extern "C" void RawSecurityDescriptor__ctor_m1_10050 (RawSecurityDescriptor_t1_1171 * __this, ByteU5BU5D_t1_109* ___binaryForm, int32_t ___offset, const MethodInfo* method)
{
	{
		GenericSecurityDescriptor__ctor_m1_9921(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.AccessControl.RawSecurityDescriptor::.ctor(System.Security.AccessControl.ControlFlags,System.Security.Principal.SecurityIdentifier,System.Security.Principal.SecurityIdentifier,System.Security.AccessControl.RawAcl,System.Security.AccessControl.RawAcl)
extern "C" void RawSecurityDescriptor__ctor_m1_10051 (RawSecurityDescriptor_t1_1171 * __this, int32_t ___flags, SecurityIdentifier_t1_1132 * ___owner, SecurityIdentifier_t1_1132 * ___group, RawAcl_t1_1170 * ___systemAcl, RawAcl_t1_1170 * ___discretionaryAcl, const MethodInfo* method)
{
	{
		GenericSecurityDescriptor__ctor_m1_9921(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Security.AccessControl.ControlFlags System.Security.AccessControl.RawSecurityDescriptor::get_ControlFlags()
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" int32_t RawSecurityDescriptor_get_ControlFlags_m1_10052 (RawSecurityDescriptor_t1_1171 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Security.AccessControl.RawAcl System.Security.AccessControl.RawSecurityDescriptor::get_DiscretionaryAcl()
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" RawAcl_t1_1170 * RawSecurityDescriptor_get_DiscretionaryAcl_m1_10053 (RawSecurityDescriptor_t1_1171 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.RawSecurityDescriptor::set_DiscretionaryAcl(System.Security.AccessControl.RawAcl)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void RawSecurityDescriptor_set_DiscretionaryAcl_m1_10054 (RawSecurityDescriptor_t1_1171 * __this, RawAcl_t1_1170 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Security.Principal.SecurityIdentifier System.Security.AccessControl.RawSecurityDescriptor::get_Group()
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" SecurityIdentifier_t1_1132 * RawSecurityDescriptor_get_Group_m1_10055 (RawSecurityDescriptor_t1_1171 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.RawSecurityDescriptor::set_Group(System.Security.Principal.SecurityIdentifier)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void RawSecurityDescriptor_set_Group_m1_10056 (RawSecurityDescriptor_t1_1171 * __this, SecurityIdentifier_t1_1132 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Security.Principal.SecurityIdentifier System.Security.AccessControl.RawSecurityDescriptor::get_Owner()
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" SecurityIdentifier_t1_1132 * RawSecurityDescriptor_get_Owner_m1_10057 (RawSecurityDescriptor_t1_1171 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.RawSecurityDescriptor::set_Owner(System.Security.Principal.SecurityIdentifier)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void RawSecurityDescriptor_set_Owner_m1_10058 (RawSecurityDescriptor_t1_1171 * __this, SecurityIdentifier_t1_1132 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Byte System.Security.AccessControl.RawSecurityDescriptor::get_ResourceManagerControl()
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" uint8_t RawSecurityDescriptor_get_ResourceManagerControl_m1_10059 (RawSecurityDescriptor_t1_1171 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.RawSecurityDescriptor::set_ResourceManagerControl(System.Byte)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void RawSecurityDescriptor_set_ResourceManagerControl_m1_10060 (RawSecurityDescriptor_t1_1171 * __this, uint8_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Security.AccessControl.RawAcl System.Security.AccessControl.RawSecurityDescriptor::get_SystemAcl()
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" RawAcl_t1_1170 * RawSecurityDescriptor_get_SystemAcl_m1_10061 (RawSecurityDescriptor_t1_1171 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.RawSecurityDescriptor::set_SystemAcl(System.Security.AccessControl.RawAcl)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void RawSecurityDescriptor_set_SystemAcl_m1_10062 (RawSecurityDescriptor_t1_1171 * __this, RawAcl_t1_1170 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.RawSecurityDescriptor::SetFlags(System.Security.AccessControl.ControlFlags)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void RawSecurityDescriptor_SetFlags_m1_10063 (RawSecurityDescriptor_t1_1171 * __this, int32_t ___flags, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.RegistryAccessRule::.ctor(System.Security.Principal.IdentityReference,System.Security.AccessControl.RegistryRights,System.Security.AccessControl.AccessControlType)
extern "C" void RegistryAccessRule__ctor_m1_10064 (RegistryAccessRule_t1_1172 * __this, IdentityReference_t1_1120 * ___identity, int32_t ___registryRights, int32_t ___type, const MethodInfo* method)
{
	{
		IdentityReference_t1_1120 * L_0 = ___identity;
		int32_t L_1 = ___registryRights;
		int32_t L_2 = ___type;
		RegistryAccessRule__ctor_m1_10066(__this, L_0, L_1, 0, 0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.AccessControl.RegistryAccessRule::.ctor(System.String,System.Security.AccessControl.RegistryRights,System.Security.AccessControl.AccessControlType)
extern TypeInfo* SecurityIdentifier_t1_1132_il2cpp_TypeInfo_var;
extern "C" void RegistryAccessRule__ctor_m1_10065 (RegistryAccessRule_t1_1172 * __this, String_t* ___identity, int32_t ___registryRights, int32_t ___type, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SecurityIdentifier_t1_1132_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(838);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___identity;
		SecurityIdentifier_t1_1132 * L_1 = (SecurityIdentifier_t1_1132 *)il2cpp_codegen_object_new (SecurityIdentifier_t1_1132_il2cpp_TypeInfo_var);
		SecurityIdentifier__ctor_m1_11825(L_1, L_0, /*hidden argument*/NULL);
		int32_t L_2 = ___registryRights;
		int32_t L_3 = ___type;
		RegistryAccessRule__ctor_m1_10064(__this, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.AccessControl.RegistryAccessRule::.ctor(System.Security.Principal.IdentityReference,System.Security.AccessControl.RegistryRights,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.AccessControlType)
extern "C" void RegistryAccessRule__ctor_m1_10066 (RegistryAccessRule_t1_1172 * __this, IdentityReference_t1_1120 * ___identity, int32_t ___registryRights, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___type, const MethodInfo* method)
{
	{
		IdentityReference_t1_1120 * L_0 = ___identity;
		int32_t L_1 = ___inheritanceFlags;
		int32_t L_2 = ___propagationFlags;
		int32_t L_3 = ___type;
		AccessRule__ctor_m1_9706(__this, L_0, 0, 0, L_1, L_2, L_3, /*hidden argument*/NULL);
		int32_t L_4 = ___registryRights;
		__this->___rights_6 = L_4;
		return;
	}
}
// System.Void System.Security.AccessControl.RegistryAccessRule::.ctor(System.String,System.Security.AccessControl.RegistryRights,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.AccessControlType)
extern TypeInfo* SecurityIdentifier_t1_1132_il2cpp_TypeInfo_var;
extern "C" void RegistryAccessRule__ctor_m1_10067 (RegistryAccessRule_t1_1172 * __this, String_t* ___identity, int32_t ___registryRights, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___type, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SecurityIdentifier_t1_1132_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(838);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___identity;
		SecurityIdentifier_t1_1132 * L_1 = (SecurityIdentifier_t1_1132 *)il2cpp_codegen_object_new (SecurityIdentifier_t1_1132_il2cpp_TypeInfo_var);
		SecurityIdentifier__ctor_m1_11825(L_1, L_0, /*hidden argument*/NULL);
		int32_t L_2 = ___registryRights;
		int32_t L_3 = ___inheritanceFlags;
		int32_t L_4 = ___propagationFlags;
		int32_t L_5 = ___type;
		RegistryAccessRule__ctor_m1_10066(__this, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Security.AccessControl.RegistryRights System.Security.AccessControl.RegistryAccessRule::get_RegistryRights()
extern "C" int32_t RegistryAccessRule_get_RegistryRights_m1_10068 (RegistryAccessRule_t1_1172 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___rights_6);
		return L_0;
	}
}
// System.Void System.Security.AccessControl.RegistryAuditRule::.ctor(System.Security.Principal.IdentityReference,System.Security.AccessControl.RegistryRights,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.AuditFlags)
extern "C" void RegistryAuditRule__ctor_m1_10069 (RegistryAuditRule_t1_1173 * __this, IdentityReference_t1_1120 * ___identity, int32_t ___registryRights, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___flags, const MethodInfo* method)
{
	{
		IdentityReference_t1_1120 * L_0 = ___identity;
		int32_t L_1 = ___inheritanceFlags;
		int32_t L_2 = ___propagationFlags;
		int32_t L_3 = ___flags;
		AuditRule__ctor_m1_9713(__this, L_0, 0, 0, L_1, L_2, L_3, /*hidden argument*/NULL);
		int32_t L_4 = ___registryRights;
		__this->___rights_6 = L_4;
		return;
	}
}
// System.Void System.Security.AccessControl.RegistryAuditRule::.ctor(System.String,System.Security.AccessControl.RegistryRights,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.AuditFlags)
extern TypeInfo* SecurityIdentifier_t1_1132_il2cpp_TypeInfo_var;
extern "C" void RegistryAuditRule__ctor_m1_10070 (RegistryAuditRule_t1_1173 * __this, String_t* ___identity, int32_t ___registryRights, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___flags, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SecurityIdentifier_t1_1132_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(838);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___identity;
		SecurityIdentifier_t1_1132 * L_1 = (SecurityIdentifier_t1_1132 *)il2cpp_codegen_object_new (SecurityIdentifier_t1_1132_il2cpp_TypeInfo_var);
		SecurityIdentifier__ctor_m1_11825(L_1, L_0, /*hidden argument*/NULL);
		int32_t L_2 = ___registryRights;
		int32_t L_3 = ___inheritanceFlags;
		int32_t L_4 = ___propagationFlags;
		int32_t L_5 = ___flags;
		RegistryAuditRule__ctor_m1_10069(__this, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Security.AccessControl.RegistryRights System.Security.AccessControl.RegistryAuditRule::get_RegistryRights()
extern "C" int32_t RegistryAuditRule_get_RegistryRights_m1_10071 (RegistryAuditRule_t1_1173 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___rights_6);
		return L_0;
	}
}
// System.Void System.Security.AccessControl.RegistrySecurity::.ctor()
extern "C" void RegistrySecurity__ctor_m1_10072 (RegistrySecurity_t1_1175 * __this, const MethodInfo* method)
{
	{
		NativeObjectSecurity__ctor_m1_9959(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Type System.Security.AccessControl.RegistrySecurity::get_AccessRightType()
extern const Il2CppType* RegistryRights_t1_1174_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern "C" Type_t * RegistrySecurity_get_AccessRightType_m1_10073 (RegistrySecurity_t1_1175 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RegistryRights_t1_1174_0_0_0_var = il2cpp_codegen_type_from_index(864);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(RegistryRights_t1_1174_0_0_0_var), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Type System.Security.AccessControl.RegistrySecurity::get_AccessRuleType()
extern const Il2CppType* RegistryAccessRule_t1_1172_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern "C" Type_t * RegistrySecurity_get_AccessRuleType_m1_10074 (RegistrySecurity_t1_1175 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RegistryAccessRule_t1_1172_0_0_0_var = il2cpp_codegen_type_from_index(865);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(RegistryAccessRule_t1_1172_0_0_0_var), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Type System.Security.AccessControl.RegistrySecurity::get_AuditRuleType()
extern const Il2CppType* RegistryAuditRule_t1_1173_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern "C" Type_t * RegistrySecurity_get_AuditRuleType_m1_10075 (RegistrySecurity_t1_1175 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RegistryAuditRule_t1_1173_0_0_0_var = il2cpp_codegen_type_from_index(866);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(RegistryAuditRule_t1_1173_0_0_0_var), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Security.AccessControl.AccessRule System.Security.AccessControl.RegistrySecurity::AccessRuleFactory(System.Security.Principal.IdentityReference,System.Int32,System.Boolean,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.AccessControlType)
extern TypeInfo* RegistryAccessRule_t1_1172_il2cpp_TypeInfo_var;
extern "C" AccessRule_t1_1111 * RegistrySecurity_AccessRuleFactory_m1_10076 (RegistrySecurity_t1_1175 * __this, IdentityReference_t1_1120 * ___identityReference, int32_t ___accessMask, bool ___isInherited, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___type, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RegistryAccessRule_t1_1172_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(865);
		s_Il2CppMethodIntialized = true;
	}
	{
		IdentityReference_t1_1120 * L_0 = ___identityReference;
		int32_t L_1 = ___accessMask;
		int32_t L_2 = ___inheritanceFlags;
		int32_t L_3 = ___propagationFlags;
		int32_t L_4 = ___type;
		RegistryAccessRule_t1_1172 * L_5 = (RegistryAccessRule_t1_1172 *)il2cpp_codegen_object_new (RegistryAccessRule_t1_1172_il2cpp_TypeInfo_var);
		RegistryAccessRule__ctor_m1_10066(L_5, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void System.Security.AccessControl.RegistrySecurity::AddAccessRule(System.Security.AccessControl.RegistryAccessRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void RegistrySecurity_AddAccessRule_m1_10077 (RegistrySecurity_t1_1175 * __this, RegistryAccessRule_t1_1172 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.RegistrySecurity::AddAuditRule(System.Security.AccessControl.RegistryAuditRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void RegistrySecurity_AddAuditRule_m1_10078 (RegistrySecurity_t1_1175 * __this, RegistryAuditRule_t1_1173 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Security.AccessControl.AuditRule System.Security.AccessControl.RegistrySecurity::AuditRuleFactory(System.Security.Principal.IdentityReference,System.Int32,System.Boolean,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.AuditFlags)
extern TypeInfo* RegistryAuditRule_t1_1173_il2cpp_TypeInfo_var;
extern "C" AuditRule_t1_1119 * RegistrySecurity_AuditRuleFactory_m1_10079 (RegistrySecurity_t1_1175 * __this, IdentityReference_t1_1120 * ___identityReference, int32_t ___accessMask, bool ___isInherited, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___flags, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RegistryAuditRule_t1_1173_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(866);
		s_Il2CppMethodIntialized = true;
	}
	{
		IdentityReference_t1_1120 * L_0 = ___identityReference;
		int32_t L_1 = ___accessMask;
		int32_t L_2 = ___inheritanceFlags;
		int32_t L_3 = ___propagationFlags;
		int32_t L_4 = ___flags;
		RegistryAuditRule_t1_1173 * L_5 = (RegistryAuditRule_t1_1173 *)il2cpp_codegen_object_new (RegistryAuditRule_t1_1173_il2cpp_TypeInfo_var);
		RegistryAuditRule__ctor_m1_10069(L_5, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Boolean System.Security.AccessControl.RegistrySecurity::RemoveAccessRule(System.Security.AccessControl.RegistryAccessRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" bool RegistrySecurity_RemoveAccessRule_m1_10080 (RegistrySecurity_t1_1175 * __this, RegistryAccessRule_t1_1172 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.RegistrySecurity::RemoveAccessRuleAll(System.Security.AccessControl.RegistryAccessRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void RegistrySecurity_RemoveAccessRuleAll_m1_10081 (RegistrySecurity_t1_1175 * __this, RegistryAccessRule_t1_1172 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.RegistrySecurity::RemoveAccessRuleSpecific(System.Security.AccessControl.RegistryAccessRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void RegistrySecurity_RemoveAccessRuleSpecific_m1_10082 (RegistrySecurity_t1_1175 * __this, RegistryAccessRule_t1_1172 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Security.AccessControl.RegistrySecurity::RemoveAuditRule(System.Security.AccessControl.RegistryAuditRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" bool RegistrySecurity_RemoveAuditRule_m1_10083 (RegistrySecurity_t1_1175 * __this, RegistryAuditRule_t1_1173 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.RegistrySecurity::RemoveAuditRuleAll(System.Security.AccessControl.RegistryAuditRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void RegistrySecurity_RemoveAuditRuleAll_m1_10084 (RegistrySecurity_t1_1175 * __this, RegistryAuditRule_t1_1173 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.RegistrySecurity::RemoveAuditRuleSpecific(System.Security.AccessControl.RegistryAuditRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void RegistrySecurity_RemoveAuditRuleSpecific_m1_10085 (RegistrySecurity_t1_1175 * __this, RegistryAuditRule_t1_1173 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.RegistrySecurity::ResetAccessRule(System.Security.AccessControl.RegistryAccessRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void RegistrySecurity_ResetAccessRule_m1_10086 (RegistrySecurity_t1_1175 * __this, RegistryAccessRule_t1_1172 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.RegistrySecurity::SetAccessRule(System.Security.AccessControl.RegistryAccessRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void RegistrySecurity_SetAccessRule_m1_10087 (RegistrySecurity_t1_1175 * __this, RegistryAccessRule_t1_1172 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.RegistrySecurity::SetAuditRule(System.Security.AccessControl.RegistryAuditRule)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void RegistrySecurity_SetAuditRule_m1_10088 (RegistrySecurity_t1_1175 * __this, RegistryAuditRule_t1_1173 * ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.SystemAcl::.ctor(System.Boolean,System.Boolean,System.Int32)
extern "C" void SystemAcl__ctor_m1_10089 (SystemAcl_t1_1133 * __this, bool ___isContainer, bool ___isDS, int32_t ___capacity, const MethodInfo* method)
{
	{
		bool L_0 = ___isContainer;
		bool L_1 = ___isDS;
		int32_t L_2 = ___capacity;
		SystemAcl__ctor_m1_10091(__this, L_0, L_1, 0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.AccessControl.SystemAcl::.ctor(System.Boolean,System.Boolean,System.Security.AccessControl.RawAcl)
extern "C" void SystemAcl__ctor_m1_10090 (SystemAcl_t1_1133 * __this, bool ___isContainer, bool ___isDS, RawAcl_t1_1170 * ___rawAcl, const MethodInfo* method)
{
	{
		bool L_0 = ___isContainer;
		bool L_1 = ___isDS;
		SystemAcl__ctor_m1_10089(__this, L_0, L_1, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.AccessControl.SystemAcl::.ctor(System.Boolean,System.Boolean,System.Byte,System.Int32)
extern "C" void SystemAcl__ctor_m1_10091 (SystemAcl_t1_1133 * __this, bool ___isContainer, bool ___isDS, uint8_t ___revision, int32_t ___capacity, const MethodInfo* method)
{
	{
		bool L_0 = ___isContainer;
		bool L_1 = ___isDS;
		uint8_t L_2 = ___revision;
		int32_t L_3 = ___capacity;
		CommonAcl__ctor_m1_9730(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.AccessControl.SystemAcl::AddAudit(System.Security.AccessControl.AuditFlags,System.Security.Principal.SecurityIdentifier,System.Int32,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void SystemAcl_AddAudit_m1_10092 (SystemAcl_t1_1133 * __this, int32_t ___auditFlags, SecurityIdentifier_t1_1132 * ___sid, int32_t ___accessMask, int32_t ___inheritanceFlags, int32_t ___propagationFlags, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.SystemAcl::AddAudit(System.Security.AccessControl.AuditFlags,System.Security.Principal.SecurityIdentifier,System.Int32,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.ObjectAceFlags,System.Guid,System.Guid)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void SystemAcl_AddAudit_m1_10093 (SystemAcl_t1_1133 * __this, int32_t ___auditFlags, SecurityIdentifier_t1_1132 * ___sid, int32_t ___accessMask, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___objectFlags, Guid_t1_319  ___objectType, Guid_t1_319  ___inheritedObjectType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Security.AccessControl.SystemAcl::RemoveAudit(System.Security.AccessControl.AuditFlags,System.Security.Principal.SecurityIdentifier,System.Int32,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" bool SystemAcl_RemoveAudit_m1_10094 (SystemAcl_t1_1133 * __this, int32_t ___auditFlags, SecurityIdentifier_t1_1132 * ___sid, int32_t ___accessMask, int32_t ___inheritanceFlags, int32_t ___propagationFlags, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Security.AccessControl.SystemAcl::RemoveAudit(System.Security.AccessControl.AuditFlags,System.Security.Principal.SecurityIdentifier,System.Int32,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.ObjectAceFlags,System.Guid,System.Guid)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" bool SystemAcl_RemoveAudit_m1_10095 (SystemAcl_t1_1133 * __this, int32_t ___auditFlags, SecurityIdentifier_t1_1132 * ___sid, int32_t ___accessMask, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___objectFlags, Guid_t1_319  ___objectType, Guid_t1_319  ___inheritedObjectType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.SystemAcl::RemoveAuditSpecific(System.Security.AccessControl.AuditFlags,System.Security.Principal.SecurityIdentifier,System.Int32,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void SystemAcl_RemoveAuditSpecific_m1_10096 (SystemAcl_t1_1133 * __this, int32_t ___auditFlags, SecurityIdentifier_t1_1132 * ___sid, int32_t ___accessMask, int32_t ___inheritanceFlags, int32_t ___propagationFlags, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.SystemAcl::RemoveAuditSpecific(System.Security.AccessControl.AuditFlags,System.Security.Principal.SecurityIdentifier,System.Int32,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.ObjectAceFlags,System.Guid,System.Guid)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void SystemAcl_RemoveAuditSpecific_m1_10097 (SystemAcl_t1_1133 * __this, int32_t ___auditFlags, SecurityIdentifier_t1_1132 * ___sid, int32_t ___accessMask, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___objectFlags, Guid_t1_319  ___objectType, Guid_t1_319  ___inheritedObjectType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.SystemAcl::SetAudit(System.Security.AccessControl.AuditFlags,System.Security.Principal.SecurityIdentifier,System.Int32,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void SystemAcl_SetAudit_m1_10098 (SystemAcl_t1_1133 * __this, int32_t ___auditFlags, SecurityIdentifier_t1_1132 * ___sid, int32_t ___accessMask, int32_t ___inheritanceFlags, int32_t ___propagationFlags, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.AccessControl.SystemAcl::SetAudit(System.Security.AccessControl.AuditFlags,System.Security.Principal.SecurityIdentifier,System.Int32,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.ObjectAceFlags,System.Guid,System.Guid)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void SystemAcl_SetAudit_m1_10099 (SystemAcl_t1_1133 * __this, int32_t ___auditFlags, SecurityIdentifier_t1_1132 * ___sid, int32_t ___accessMask, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___objectFlags, Guid_t1_319  ___objectType, Guid_t1_319  ___inheritedObjectType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate::.ctor(System.Byte[],System.Boolean)
extern "C" void X509Certificate__ctor_m1_10100 (X509Certificate_t1_1179 * __this, ByteU5BU5D_t1_109* ___data, bool ___dates, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_0 = ___data;
		if (!L_0)
		{
			goto IL_001f;
		}
	}
	{
		ByteU5BU5D_t1_109* L_1 = ___data;
		VirtActionInvoker3< ByteU5BU5D_t1_109*, String_t*, int32_t >::Invoke(28 /* System.Void System.Security.Cryptography.X509Certificates.X509Certificate::Import(System.Byte[],System.String,System.Security.Cryptography.X509Certificates.X509KeyStorageFlags) */, __this, L_1, (String_t*)NULL, 0);
		bool L_2 = ___dates;
		__this->___hideDates_1 = ((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}

IL_001f:
	{
		return;
	}
}
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate::.ctor(System.Byte[])
extern "C" void X509Certificate__ctor_m1_10101 (X509Certificate_t1_1179 * __this, ByteU5BU5D_t1_109* ___data, const MethodInfo* method)
{
	{
		ByteU5BU5D_t1_109* L_0 = ___data;
		X509Certificate__ctor_m1_10100(__this, L_0, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate::.ctor(System.IntPtr)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1316;
extern "C" void X509Certificate__ctor_m1_10102 (X509Certificate_t1_1179 * __this, IntPtr_t ___handle, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		_stringLiteral1316 = il2cpp_codegen_string_literal_from_index(1316);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		IntPtr_t L_0 = ___handle;
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		bool L_2 = IntPtr_op_Equality_m1_840(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0021;
		}
	}
	{
		ArgumentException_t1_1425 * L_3 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13260(L_3, _stringLiteral1316, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0021:
	{
		NotSupportedException_t1_1583 * L_4 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14416(L_4, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
	}
}
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate::.ctor(System.Security.Cryptography.X509Certificates.X509Certificate)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* X509Certificate_t1_151_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2534;
extern "C" void X509Certificate__ctor_m1_10103 (X509Certificate_t1_1179 * __this, X509Certificate_t1_1179 * ___cert, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		X509Certificate_t1_151_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(226);
		_stringLiteral2534 = il2cpp_codegen_string_literal_from_index(2534);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		X509Certificate_t1_1179 * L_0 = ___cert;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, _stringLiteral2534, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0017:
	{
		X509Certificate_t1_1179 * L_2 = ___cert;
		if (!L_2)
		{
			goto IL_003d;
		}
	}
	{
		X509Certificate_t1_1179 * L_3 = ___cert;
		NullCheck(L_3);
		ByteU5BU5D_t1_109* L_4 = (ByteU5BU5D_t1_109*)VirtFuncInvoker0< ByteU5BU5D_t1_109* >::Invoke(19 /* System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate::GetRawCertData() */, L_3);
		V_0 = L_4;
		ByteU5BU5D_t1_109* L_5 = V_0;
		if (!L_5)
		{
			goto IL_0036;
		}
	}
	{
		ByteU5BU5D_t1_109* L_6 = V_0;
		X509Certificate_t1_151 * L_7 = (X509Certificate_t1_151 *)il2cpp_codegen_object_new (X509Certificate_t1_151_il2cpp_TypeInfo_var);
		X509Certificate__ctor_m1_2242(L_7, L_6, /*hidden argument*/NULL);
		__this->___x509_0 = L_7;
	}

IL_0036:
	{
		__this->___hideDates_1 = 0;
	}

IL_003d:
	{
		return;
	}
}
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate::.ctor()
extern "C" void X509Certificate__ctor_m1_10104 (X509Certificate_t1_1179 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate::.ctor(System.Byte[],System.String)
extern "C" void X509Certificate__ctor_m1_10105 (X509Certificate_t1_1179 * __this, ByteU5BU5D_t1_109* ___rawData, String_t* ___password, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_0 = ___rawData;
		String_t* L_1 = ___password;
		VirtActionInvoker3< ByteU5BU5D_t1_109*, String_t*, int32_t >::Invoke(28 /* System.Void System.Security.Cryptography.X509Certificates.X509Certificate::Import(System.Byte[],System.String,System.Security.Cryptography.X509Certificates.X509KeyStorageFlags) */, __this, L_0, L_1, 0);
		return;
	}
}
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate::.ctor(System.Byte[],System.Security.SecureString)
extern "C" void X509Certificate__ctor_m1_10106 (X509Certificate_t1_1179 * __this, ByteU5BU5D_t1_109* ___rawData, SecureString_t1_1197 * ___password, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_0 = ___rawData;
		SecureString_t1_1197 * L_1 = ___password;
		VirtActionInvoker3< ByteU5BU5D_t1_109*, SecureString_t1_1197 *, int32_t >::Invoke(29 /* System.Void System.Security.Cryptography.X509Certificates.X509Certificate::Import(System.Byte[],System.Security.SecureString,System.Security.Cryptography.X509Certificates.X509KeyStorageFlags) */, __this, L_0, L_1, 0);
		return;
	}
}
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate::.ctor(System.Byte[],System.String,System.Security.Cryptography.X509Certificates.X509KeyStorageFlags)
extern "C" void X509Certificate__ctor_m1_10107 (X509Certificate_t1_1179 * __this, ByteU5BU5D_t1_109* ___rawData, String_t* ___password, int32_t ___keyStorageFlags, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_0 = ___rawData;
		String_t* L_1 = ___password;
		int32_t L_2 = ___keyStorageFlags;
		VirtActionInvoker3< ByteU5BU5D_t1_109*, String_t*, int32_t >::Invoke(28 /* System.Void System.Security.Cryptography.X509Certificates.X509Certificate::Import(System.Byte[],System.String,System.Security.Cryptography.X509Certificates.X509KeyStorageFlags) */, __this, L_0, L_1, L_2);
		return;
	}
}
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate::.ctor(System.Byte[],System.Security.SecureString,System.Security.Cryptography.X509Certificates.X509KeyStorageFlags)
extern "C" void X509Certificate__ctor_m1_10108 (X509Certificate_t1_1179 * __this, ByteU5BU5D_t1_109* ___rawData, SecureString_t1_1197 * ___password, int32_t ___keyStorageFlags, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_0 = ___rawData;
		SecureString_t1_1197 * L_1 = ___password;
		int32_t L_2 = ___keyStorageFlags;
		VirtActionInvoker3< ByteU5BU5D_t1_109*, SecureString_t1_1197 *, int32_t >::Invoke(29 /* System.Void System.Security.Cryptography.X509Certificates.X509Certificate::Import(System.Byte[],System.Security.SecureString,System.Security.Cryptography.X509Certificates.X509KeyStorageFlags) */, __this, L_0, L_1, L_2);
		return;
	}
}
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate::.ctor(System.String)
extern "C" void X509Certificate__ctor_m1_10109 (X509Certificate_t1_1179 * __this, String_t* ___fileName, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___fileName;
		VirtActionInvoker3< String_t*, String_t*, int32_t >::Invoke(31 /* System.Void System.Security.Cryptography.X509Certificates.X509Certificate::Import(System.String,System.String,System.Security.Cryptography.X509Certificates.X509KeyStorageFlags) */, __this, L_0, (String_t*)NULL, 0);
		return;
	}
}
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate::.ctor(System.String,System.String)
extern "C" void X509Certificate__ctor_m1_10110 (X509Certificate_t1_1179 * __this, String_t* ___fileName, String_t* ___password, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___fileName;
		String_t* L_1 = ___password;
		VirtActionInvoker3< String_t*, String_t*, int32_t >::Invoke(31 /* System.Void System.Security.Cryptography.X509Certificates.X509Certificate::Import(System.String,System.String,System.Security.Cryptography.X509Certificates.X509KeyStorageFlags) */, __this, L_0, L_1, 0);
		return;
	}
}
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate::.ctor(System.String,System.Security.SecureString)
extern "C" void X509Certificate__ctor_m1_10111 (X509Certificate_t1_1179 * __this, String_t* ___fileName, SecureString_t1_1197 * ___password, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___fileName;
		SecureString_t1_1197 * L_1 = ___password;
		VirtActionInvoker3< String_t*, SecureString_t1_1197 *, int32_t >::Invoke(32 /* System.Void System.Security.Cryptography.X509Certificates.X509Certificate::Import(System.String,System.Security.SecureString,System.Security.Cryptography.X509Certificates.X509KeyStorageFlags) */, __this, L_0, L_1, 0);
		return;
	}
}
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate::.ctor(System.String,System.String,System.Security.Cryptography.X509Certificates.X509KeyStorageFlags)
extern "C" void X509Certificate__ctor_m1_10112 (X509Certificate_t1_1179 * __this, String_t* ___fileName, String_t* ___password, int32_t ___keyStorageFlags, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___fileName;
		String_t* L_1 = ___password;
		int32_t L_2 = ___keyStorageFlags;
		VirtActionInvoker3< String_t*, String_t*, int32_t >::Invoke(31 /* System.Void System.Security.Cryptography.X509Certificates.X509Certificate::Import(System.String,System.String,System.Security.Cryptography.X509Certificates.X509KeyStorageFlags) */, __this, L_0, L_1, L_2);
		return;
	}
}
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate::.ctor(System.String,System.Security.SecureString,System.Security.Cryptography.X509Certificates.X509KeyStorageFlags)
extern "C" void X509Certificate__ctor_m1_10113 (X509Certificate_t1_1179 * __this, String_t* ___fileName, SecureString_t1_1197 * ___password, int32_t ___keyStorageFlags, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___fileName;
		SecureString_t1_1197 * L_1 = ___password;
		int32_t L_2 = ___keyStorageFlags;
		VirtActionInvoker3< String_t*, SecureString_t1_1197 *, int32_t >::Invoke(32 /* System.Void System.Security.Cryptography.X509Certificates.X509Certificate::Import(System.String,System.Security.SecureString,System.Security.Cryptography.X509Certificates.X509KeyStorageFlags) */, __this, L_0, L_1, L_2);
		return;
	}
}
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const Il2CppType* ByteU5BU5D_t1_109_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2535;
extern "C" void X509Certificate__ctor_m1_10114 (X509Certificate_t1_1179 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ByteU5BU5D_t1_109_0_0_0_var = il2cpp_codegen_type_from_index(46);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		_stringLiteral2535 = il2cpp_codegen_string_literal_from_index(2535);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		SerializationInfo_t1_293 * L_0 = ___info;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(ByteU5BU5D_t1_109_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		Object_t * L_2 = SerializationInfo_GetValue_m1_9625(L_0, _stringLiteral2535, L_1, /*hidden argument*/NULL);
		V_0 = ((ByteU5BU5D_t1_109*)Castclass(L_2, ByteU5BU5D_t1_109_il2cpp_TypeInfo_var));
		ByteU5BU5D_t1_109* L_3 = V_0;
		VirtActionInvoker3< ByteU5BU5D_t1_109*, String_t*, int32_t >::Invoke(28 /* System.Void System.Security.Cryptography.X509Certificates.X509Certificate::Import(System.Byte[],System.String,System.Security.Cryptography.X509Certificates.X509KeyStorageFlags) */, __this, L_3, (String_t*)NULL, 0);
		return;
	}
}
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate::System.Runtime.Serialization.IDeserializationCallback.OnDeserialization(System.Object)
extern "C" void X509Certificate_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m1_10115 (X509Certificate_t1_1179 * __this, Object_t * ___sender, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern Il2CppCodeGenString* _stringLiteral2535;
extern "C" void X509Certificate_System_Runtime_Serialization_ISerializable_GetObjectData_m1_10116 (X509Certificate_t1_1179 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral2535 = il2cpp_codegen_string_literal_from_index(2535);
		s_Il2CppMethodIntialized = true;
	}
	{
		SerializationInfo_t1_293 * L_0 = ___info;
		X509Certificate_t1_151 * L_1 = (__this->___x509_0);
		NullCheck(L_1);
		ByteU5BU5D_t1_109* L_2 = (ByteU5BU5D_t1_109*)VirtFuncInvoker0< ByteU5BU5D_t1_109* >::Invoke(12 /* System.Byte[] Mono.Security.X509.X509Certificate::get_RawData() */, L_1);
		NullCheck(L_0);
		SerializationInfo_AddValue_m1_9642(L_0, _stringLiteral2535, (Object_t *)(Object_t *)L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.String System.Security.Cryptography.X509Certificates.X509Certificate::tostr(System.Byte[])
extern TypeInfo* StringBuilder_t1_247_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral157;
extern "C" String_t* X509Certificate_tostr_m1_10117 (X509Certificate_t1_1179 * __this, ByteU5BU5D_t1_109* ___data, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringBuilder_t1_247_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		_stringLiteral157 = il2cpp_codegen_string_literal_from_index(157);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t1_247 * V_0 = {0};
	int32_t V_1 = 0;
	{
		ByteU5BU5D_t1_109* L_0 = ___data;
		if (!L_0)
		{
			goto IL_003f;
		}
	}
	{
		StringBuilder_t1_247 * L_1 = (StringBuilder_t1_247 *)il2cpp_codegen_object_new (StringBuilder_t1_247_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1_12414(L_1, /*hidden argument*/NULL);
		V_0 = L_1;
		V_1 = 0;
		goto IL_002f;
	}

IL_0013:
	{
		StringBuilder_t1_247 * L_2 = V_0;
		ByteU5BU5D_t1_109* L_3 = ___data;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		String_t* L_5 = Byte_ToString_m1_242(((uint8_t*)(uint8_t*)SZArrayLdElema(L_3, L_4, sizeof(uint8_t))), _stringLiteral157, /*hidden argument*/NULL);
		NullCheck(L_2);
		StringBuilder_Append_m1_12438(L_2, L_5, /*hidden argument*/NULL);
		int32_t L_6 = V_1;
		V_1 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_002f:
	{
		int32_t L_7 = V_1;
		ByteU5BU5D_t1_109* L_8 = ___data;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_8)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		StringBuilder_t1_247 * L_9 = V_0;
		NullCheck(L_9);
		String_t* L_10 = StringBuilder_ToString_m1_12428(L_9, /*hidden argument*/NULL);
		return L_10;
	}

IL_003f:
	{
		return (String_t*)NULL;
	}
}
// System.Security.Cryptography.X509Certificates.X509Certificate System.Security.Cryptography.X509Certificates.X509Certificate::CreateFromCertFile(System.String)
extern TypeInfo* X509Certificate_t1_1179_il2cpp_TypeInfo_var;
extern "C" X509Certificate_t1_1179 * X509Certificate_CreateFromCertFile_m1_10118 (Object_t * __this /* static, unused */, String_t* ___filename, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		X509Certificate_t1_1179_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(867);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	{
		String_t* L_0 = ___filename;
		ByteU5BU5D_t1_109* L_1 = X509Certificate_Load_m1_10141(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		ByteU5BU5D_t1_109* L_2 = V_0;
		X509Certificate_t1_1179 * L_3 = (X509Certificate_t1_1179 *)il2cpp_codegen_object_new (X509Certificate_t1_1179_il2cpp_TypeInfo_var);
		X509Certificate__ctor_m1_10101(L_3, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Security.Cryptography.X509Certificates.X509Certificate System.Security.Cryptography.X509Certificates.X509Certificate::CreateFromSignedFile(System.String)
extern TypeInfo* AuthenticodeDeformatter_t1_147_il2cpp_TypeInfo_var;
extern TypeInfo* X509Certificate_t1_1179_il2cpp_TypeInfo_var;
extern TypeInfo* SecurityException_t1_1399_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t1_33_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* COMException_t1_760_il2cpp_TypeInfo_var;
extern TypeInfo* CryptographicException_t1_1194_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2536;
extern Il2CppCodeGenString* _stringLiteral2537;
extern "C" X509Certificate_t1_1179 * X509Certificate_CreateFromSignedFile_m1_10119 (Object_t * __this /* static, unused */, String_t* ___filename, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AuthenticodeDeformatter_t1_147_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(190);
		X509Certificate_t1_1179_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(867);
		SecurityException_t1_1399_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(138);
		Exception_t1_33_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(49);
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		COMException_t1_760_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(597);
		CryptographicException_t1_1194_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(197);
		_stringLiteral2536 = il2cpp_codegen_string_literal_from_index(2536);
		_stringLiteral2537 = il2cpp_codegen_string_literal_from_index(2537);
		s_Il2CppMethodIntialized = true;
	}
	AuthenticodeDeformatter_t1_147 * V_0 = {0};
	Exception_t1_33 * V_1 = {0};
	String_t* V_2 = {0};
	X509Certificate_t1_1179 * V_3 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			String_t* L_0 = ___filename;
			AuthenticodeDeformatter_t1_147 * L_1 = (AuthenticodeDeformatter_t1_147 *)il2cpp_codegen_object_new (AuthenticodeDeformatter_t1_147_il2cpp_TypeInfo_var);
			AuthenticodeDeformatter__ctor_m1_1871(L_1, L_0, /*hidden argument*/NULL);
			V_0 = L_1;
			AuthenticodeDeformatter_t1_147 * L_2 = V_0;
			NullCheck(L_2);
			X509Certificate_t1_151 * L_3 = AuthenticodeDeformatter_get_SigningCertificate_m1_1880(L_2, /*hidden argument*/NULL);
			if (!L_3)
			{
				goto IL_0028;
			}
		}

IL_0012:
		{
			AuthenticodeDeformatter_t1_147 * L_4 = V_0;
			NullCheck(L_4);
			X509Certificate_t1_151 * L_5 = AuthenticodeDeformatter_get_SigningCertificate_m1_1880(L_4, /*hidden argument*/NULL);
			NullCheck(L_5);
			ByteU5BU5D_t1_109* L_6 = (ByteU5BU5D_t1_109*)VirtFuncInvoker0< ByteU5BU5D_t1_109* >::Invoke(12 /* System.Byte[] Mono.Security.X509.X509Certificate::get_RawData() */, L_5);
			X509Certificate_t1_1179 * L_7 = (X509Certificate_t1_1179 *)il2cpp_codegen_object_new (X509Certificate_t1_1179_il2cpp_TypeInfo_var);
			X509Certificate__ctor_m1_10101(L_7, L_6, /*hidden argument*/NULL);
			V_3 = L_7;
			goto IL_0072;
		}

IL_0028:
		{
			goto IL_0058;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1_33 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (SecurityException_t1_1399_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_002d;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1_33_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0035;
		throw e;
	}

CATCH_002d:
	{ // begin catch(System.Security.SecurityException)
		{
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)__exception_local);
		}

IL_0030:
		{
			goto IL_0058;
		}
	} // end catch (depth: 1)

CATCH_0035:
	{ // begin catch(System.Exception)
		{
			V_1 = ((Exception_t1_33 *)__exception_local);
			ObjectU5BU5D_t1_272* L_8 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 1));
			String_t* L_9 = ___filename;
			NullCheck(L_8);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 0);
			ArrayElementTypeCheck (L_8, L_9);
			*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 0, sizeof(Object_t *))) = (Object_t *)L_9;
			String_t* L_10 = Locale_GetText_m1_1405(NULL /*static, unused*/, _stringLiteral2536, L_8, /*hidden argument*/NULL);
			V_2 = L_10;
			String_t* L_11 = V_2;
			Exception_t1_33 * L_12 = V_1;
			COMException_t1_760 * L_13 = (COMException_t1_760 *)il2cpp_codegen_object_new (COMException_t1_760_il2cpp_TypeInfo_var);
			COMException__ctor_m1_7592(L_13, L_11, L_12, /*hidden argument*/NULL);
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_13);
		}

IL_0053:
		{
			goto IL_0058;
		}
	} // end catch (depth: 1)

IL_0058:
	{
		ObjectU5BU5D_t1_272* L_14 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 1));
		String_t* L_15 = ___filename;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 0);
		ArrayElementTypeCheck (L_14, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_14, 0, sizeof(Object_t *))) = (Object_t *)L_15;
		String_t* L_16 = Locale_GetText_m1_1405(NULL /*static, unused*/, _stringLiteral2537, L_14, /*hidden argument*/NULL);
		CryptographicException_t1_1194 * L_17 = (CryptographicException_t1_1194 *)il2cpp_codegen_object_new (CryptographicException_t1_1194_il2cpp_TypeInfo_var);
		CryptographicException__ctor_m1_10222(L_17, L_16, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_17);
	}

IL_0072:
	{
		X509Certificate_t1_1179 * L_18 = V_3;
		return L_18;
	}
}
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate::InitFromHandle(System.IntPtr)
extern const Il2CppType* CertificateContext_t1_1178_0_0_0_var;
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern TypeInfo* CertificateContext_t1_1178_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern TypeInfo* X509Certificate_t1_151_il2cpp_TypeInfo_var;
extern "C" void X509Certificate_InitFromHandle_m1_10120 (X509Certificate_t1_1179 * __this, IntPtr_t ___handle, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CertificateContext_t1_1178_0_0_0_var = il2cpp_codegen_type_from_index(868);
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		CertificateContext_t1_1178_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(868);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		X509Certificate_t1_151_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(226);
		s_Il2CppMethodIntialized = true;
	}
	CertificateContext_t1_1178  V_0 = {0};
	ByteU5BU5D_t1_109* V_1 = {0};
	{
		IntPtr_t L_0 = ___handle;
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		bool L_2 = IntPtr_op_Inequality_m1_841(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0055;
		}
	}
	{
		IntPtr_t L_3 = ___handle;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(CertificateContext_t1_1178_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		Object_t * L_5 = Marshal_PtrToStructure_m1_7796(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_0 = ((*(CertificateContext_t1_1178 *)((CertificateContext_t1_1178 *)UnBox (L_5, CertificateContext_t1_1178_il2cpp_TypeInfo_var))));
		uint32_t L_6 = ((&V_0)->___cbCertEncoded_2);
		V_1 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, (((uintptr_t)L_6))));
		IntPtr_t L_7 = ((&V_0)->___pbCertEncoded_1);
		ByteU5BU5D_t1_109* L_8 = V_1;
		uint32_t L_9 = ((&V_0)->___cbCertEncoded_2);
		Marshal_Copy_m1_7713(NULL /*static, unused*/, L_7, L_8, 0, L_9, /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_10 = V_1;
		X509Certificate_t1_151 * L_11 = (X509Certificate_t1_151 *)il2cpp_codegen_object_new (X509Certificate_t1_151_il2cpp_TypeInfo_var);
		X509Certificate__ctor_m1_2242(L_11, L_10, /*hidden argument*/NULL);
		__this->___x509_0 = L_11;
	}

IL_0055:
	{
		return;
	}
}
// System.Boolean System.Security.Cryptography.X509Certificates.X509Certificate::Equals(System.Security.Cryptography.X509Certificates.X509Certificate)
extern TypeInfo* CryptographicException_t1_1194_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2538;
extern "C" bool X509Certificate_Equals_m1_10121 (X509Certificate_t1_1179 * __this, X509Certificate_t1_1179 * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CryptographicException_t1_1194_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(197);
		_stringLiteral2538 = il2cpp_codegen_string_literal_from_index(2538);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	int32_t V_1 = 0;
	int32_t G_B22_0 = 0;
	{
		X509Certificate_t1_1179 * L_0 = ___other;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return 0;
	}

IL_0008:
	{
		X509Certificate_t1_1179 * L_1 = ___other;
		NullCheck(L_1);
		X509Certificate_t1_151 * L_2 = (L_1->___x509_0);
		if (L_2)
		{
			goto IL_0030;
		}
	}
	{
		X509Certificate_t1_151 * L_3 = (__this->___x509_0);
		if (L_3)
		{
			goto IL_0020;
		}
	}
	{
		return 1;
	}

IL_0020:
	{
		String_t* L_4 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2538, /*hidden argument*/NULL);
		CryptographicException_t1_1194 * L_5 = (CryptographicException_t1_1194 *)il2cpp_codegen_object_new (CryptographicException_t1_1194_il2cpp_TypeInfo_var);
		CryptographicException__ctor_m1_10222(L_5, L_4, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_5);
	}

IL_0030:
	{
		X509Certificate_t1_1179 * L_6 = ___other;
		NullCheck(L_6);
		X509Certificate_t1_151 * L_7 = (L_6->___x509_0);
		NullCheck(L_7);
		ByteU5BU5D_t1_109* L_8 = (ByteU5BU5D_t1_109*)VirtFuncInvoker0< ByteU5BU5D_t1_109* >::Invoke(12 /* System.Byte[] Mono.Security.X509.X509Certificate::get_RawData() */, L_7);
		V_0 = L_8;
		ByteU5BU5D_t1_109* L_9 = V_0;
		if (!L_9)
		{
			goto IL_00a5;
		}
	}
	{
		X509Certificate_t1_151 * L_10 = (__this->___x509_0);
		if (L_10)
		{
			goto IL_004f;
		}
	}
	{
		return 0;
	}

IL_004f:
	{
		X509Certificate_t1_151 * L_11 = (__this->___x509_0);
		NullCheck(L_11);
		ByteU5BU5D_t1_109* L_12 = (ByteU5BU5D_t1_109*)VirtFuncInvoker0< ByteU5BU5D_t1_109* >::Invoke(12 /* System.Byte[] Mono.Security.X509.X509Certificate::get_RawData() */, L_11);
		if (L_12)
		{
			goto IL_0061;
		}
	}
	{
		return 0;
	}

IL_0061:
	{
		ByteU5BU5D_t1_109* L_13 = V_0;
		NullCheck(L_13);
		X509Certificate_t1_151 * L_14 = (__this->___x509_0);
		NullCheck(L_14);
		ByteU5BU5D_t1_109* L_15 = (ByteU5BU5D_t1_109*)VirtFuncInvoker0< ByteU5BU5D_t1_109* >::Invoke(12 /* System.Byte[] Mono.Security.X509.X509Certificate::get_RawData() */, L_14);
		NullCheck(L_15);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Array_t *)L_13)->max_length))))) == ((uint32_t)(((int32_t)((int32_t)(((Array_t *)L_15)->max_length))))))))
		{
			goto IL_00a3;
		}
	}
	{
		V_1 = 0;
		goto IL_0098;
	}

IL_007d:
	{
		ByteU5BU5D_t1_109* L_16 = V_0;
		int32_t L_17 = V_1;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		int32_t L_18 = L_17;
		X509Certificate_t1_151 * L_19 = (__this->___x509_0);
		NullCheck(L_19);
		ByteU5BU5D_t1_109* L_20 = (ByteU5BU5D_t1_109*)VirtFuncInvoker0< ByteU5BU5D_t1_109* >::Invoke(12 /* System.Byte[] Mono.Security.X509.X509Certificate::get_RawData() */, L_19);
		int32_t L_21 = V_1;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		int32_t L_22 = L_21;
		if ((((int32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_16, L_18, sizeof(uint8_t)))) == ((int32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_20, L_22, sizeof(uint8_t))))))
		{
			goto IL_0094;
		}
	}
	{
		return 0;
	}

IL_0094:
	{
		int32_t L_23 = V_1;
		V_1 = ((int32_t)((int32_t)L_23+(int32_t)1));
	}

IL_0098:
	{
		int32_t L_24 = V_1;
		ByteU5BU5D_t1_109* L_25 = V_0;
		NullCheck(L_25);
		if ((((int32_t)L_24) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_25)->max_length)))))))
		{
			goto IL_007d;
		}
	}
	{
		return 1;
	}

IL_00a3:
	{
		return 0;
	}

IL_00a5:
	{
		X509Certificate_t1_151 * L_26 = (__this->___x509_0);
		if (!L_26)
		{
			goto IL_00c0;
		}
	}
	{
		X509Certificate_t1_151 * L_27 = (__this->___x509_0);
		NullCheck(L_27);
		ByteU5BU5D_t1_109* L_28 = (ByteU5BU5D_t1_109*)VirtFuncInvoker0< ByteU5BU5D_t1_109* >::Invoke(12 /* System.Byte[] Mono.Security.X509.X509Certificate::get_RawData() */, L_27);
		G_B22_0 = ((((Object_t*)(ByteU5BU5D_t1_109*)L_28) == ((Object_t*)(Object_t *)NULL))? 1 : 0);
		goto IL_00c1;
	}

IL_00c0:
	{
		G_B22_0 = 1;
	}

IL_00c1:
	{
		return G_B22_0;
	}
}
// System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate::GetCertHash()
extern TypeInfo* CryptographicException_t1_1194_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2538;
extern "C" ByteU5BU5D_t1_109* X509Certificate_GetCertHash_m1_10122 (X509Certificate_t1_1179 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CryptographicException_t1_1194_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(197);
		_stringLiteral2538 = il2cpp_codegen_string_literal_from_index(2538);
		s_Il2CppMethodIntialized = true;
	}
	SHA1_t1_1248 * V_0 = {0};
	{
		X509Certificate_t1_151 * L_0 = (__this->___x509_0);
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2538, /*hidden argument*/NULL);
		CryptographicException_t1_1194 * L_2 = (CryptographicException_t1_1194 *)il2cpp_codegen_object_new (CryptographicException_t1_1194_il2cpp_TypeInfo_var);
		CryptographicException__ctor_m1_10222(L_2, L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001b:
	{
		ByteU5BU5D_t1_109* L_3 = (__this->___cachedCertificateHash_2);
		if (L_3)
		{
			goto IL_004e;
		}
	}
	{
		X509Certificate_t1_151 * L_4 = (__this->___x509_0);
		if (!L_4)
		{
			goto IL_004e;
		}
	}
	{
		SHA1_t1_1248 * L_5 = SHA1_Create_m1_10641(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_5;
		SHA1_t1_1248 * L_6 = V_0;
		X509Certificate_t1_151 * L_7 = (__this->___x509_0);
		NullCheck(L_7);
		ByteU5BU5D_t1_109* L_8 = (ByteU5BU5D_t1_109*)VirtFuncInvoker0< ByteU5BU5D_t1_109* >::Invoke(12 /* System.Byte[] Mono.Security.X509.X509Certificate::get_RawData() */, L_7);
		NullCheck(L_6);
		ByteU5BU5D_t1_109* L_9 = HashAlgorithm_ComputeHash_m1_10381(L_6, L_8, /*hidden argument*/NULL);
		__this->___cachedCertificateHash_2 = L_9;
	}

IL_004e:
	{
		ByteU5BU5D_t1_109* L_10 = (__this->___cachedCertificateHash_2);
		return L_10;
	}
}
// System.String System.Security.Cryptography.X509Certificates.X509Certificate::GetCertHashString()
extern "C" String_t* X509Certificate_GetCertHashString_m1_10123 (X509Certificate_t1_1179 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t1_109* L_0 = (ByteU5BU5D_t1_109*)VirtFuncInvoker0< ByteU5BU5D_t1_109* >::Invoke(7 /* System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate::GetCertHash() */, __this);
		String_t* L_1 = X509Certificate_tostr_m1_10117(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String System.Security.Cryptography.X509Certificates.X509Certificate::GetEffectiveDateString()
extern TypeInfo* CryptographicException_t1_1194_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2538;
extern "C" String_t* X509Certificate_GetEffectiveDateString_m1_10124 (X509Certificate_t1_1179 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CryptographicException_t1_1194_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(197);
		_stringLiteral2538 = il2cpp_codegen_string_literal_from_index(2538);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t1_150  V_0 = {0};
	DateTime_t1_150  V_1 = {0};
	{
		bool L_0 = (__this->___hideDates_1);
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (String_t*)NULL;
	}

IL_000d:
	{
		X509Certificate_t1_151 * L_1 = (__this->___x509_0);
		if (L_1)
		{
			goto IL_0028;
		}
	}
	{
		String_t* L_2 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2538, /*hidden argument*/NULL);
		CryptographicException_t1_1194 * L_3 = (CryptographicException_t1_1194 *)il2cpp_codegen_object_new (CryptographicException_t1_1194_il2cpp_TypeInfo_var);
		CryptographicException__ctor_m1_10222(L_3, L_2, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0028:
	{
		X509Certificate_t1_151 * L_4 = (__this->___x509_0);
		NullCheck(L_4);
		DateTime_t1_150  L_5 = (DateTime_t1_150 )VirtFuncInvoker0< DateTime_t1_150  >::Invoke(18 /* System.DateTime Mono.Security.X509.X509Certificate::get_ValidFrom() */, L_4);
		V_0 = L_5;
		DateTime_t1_150  L_6 = DateTime_ToLocalTime_m1_13900((&V_0), /*hidden argument*/NULL);
		V_1 = L_6;
		String_t* L_7 = DateTime_ToString_m1_13896((&V_1), /*hidden argument*/NULL);
		return L_7;
	}
}
// System.String System.Security.Cryptography.X509Certificates.X509Certificate::GetExpirationDateString()
extern TypeInfo* CryptographicException_t1_1194_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2538;
extern "C" String_t* X509Certificate_GetExpirationDateString_m1_10125 (X509Certificate_t1_1179 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CryptographicException_t1_1194_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(197);
		_stringLiteral2538 = il2cpp_codegen_string_literal_from_index(2538);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t1_150  V_0 = {0};
	DateTime_t1_150  V_1 = {0};
	{
		bool L_0 = (__this->___hideDates_1);
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (String_t*)NULL;
	}

IL_000d:
	{
		X509Certificate_t1_151 * L_1 = (__this->___x509_0);
		if (L_1)
		{
			goto IL_0028;
		}
	}
	{
		String_t* L_2 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2538, /*hidden argument*/NULL);
		CryptographicException_t1_1194 * L_3 = (CryptographicException_t1_1194 *)il2cpp_codegen_object_new (CryptographicException_t1_1194_il2cpp_TypeInfo_var);
		CryptographicException__ctor_m1_10222(L_3, L_2, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0028:
	{
		X509Certificate_t1_151 * L_4 = (__this->___x509_0);
		NullCheck(L_4);
		DateTime_t1_150  L_5 = (DateTime_t1_150 )VirtFuncInvoker0< DateTime_t1_150  >::Invoke(19 /* System.DateTime Mono.Security.X509.X509Certificate::get_ValidUntil() */, L_4);
		V_0 = L_5;
		DateTime_t1_150  L_6 = DateTime_ToLocalTime_m1_13900((&V_0), /*hidden argument*/NULL);
		V_1 = L_6;
		String_t* L_7 = DateTime_ToString_m1_13896((&V_1), /*hidden argument*/NULL);
		return L_7;
	}
}
// System.String System.Security.Cryptography.X509Certificates.X509Certificate::GetFormat()
extern Il2CppCodeGenString* _stringLiteral2539;
extern "C" String_t* X509Certificate_GetFormat_m1_10126 (X509Certificate_t1_1179 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral2539 = il2cpp_codegen_string_literal_from_index(2539);
		s_Il2CppMethodIntialized = true;
	}
	{
		return _stringLiteral2539;
	}
}
// System.Int32 System.Security.Cryptography.X509Certificates.X509Certificate::GetHashCode()
extern "C" int32_t X509Certificate_GetHashCode_m1_10127 (X509Certificate_t1_1179 * __this, const MethodInfo* method)
{
	{
		X509Certificate_t1_151 * L_0 = (__this->___x509_0);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		ByteU5BU5D_t1_109* L_1 = (__this->___cachedCertificateHash_2);
		if (L_1)
		{
			goto IL_001f;
		}
	}
	{
		VirtFuncInvoker0< ByteU5BU5D_t1_109* >::Invoke(7 /* System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate::GetCertHash() */, __this);
	}

IL_001f:
	{
		ByteU5BU5D_t1_109* L_2 = (__this->___cachedCertificateHash_2);
		if (!L_2)
		{
			goto IL_0064;
		}
	}
	{
		ByteU5BU5D_t1_109* L_3 = (__this->___cachedCertificateHash_2);
		NullCheck(L_3);
		if ((((int32_t)(((int32_t)((int32_t)(((Array_t *)L_3)->max_length))))) < ((int32_t)4)))
		{
			goto IL_0064;
		}
	}
	{
		ByteU5BU5D_t1_109* L_4 = (__this->___cachedCertificateHash_2);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		int32_t L_5 = 0;
		ByteU5BU5D_t1_109* L_6 = (__this->___cachedCertificateHash_2);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 1);
		int32_t L_7 = 1;
		ByteU5BU5D_t1_109* L_8 = (__this->___cachedCertificateHash_2);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		int32_t L_9 = 2;
		ByteU5BU5D_t1_109* L_10 = (__this->___cachedCertificateHash_2);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 3);
		int32_t L_11 = 3;
		return ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_4, L_5, sizeof(uint8_t)))<<(int32_t)((int32_t)24)))|(int32_t)((int32_t)((int32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_6, L_7, sizeof(uint8_t)))<<(int32_t)((int32_t)16)))))|(int32_t)((int32_t)((int32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_8, L_9, sizeof(uint8_t)))<<(int32_t)8))))|(int32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_10, L_11, sizeof(uint8_t)))));
	}

IL_0064:
	{
		return 0;
	}
}
// System.String System.Security.Cryptography.X509Certificates.X509Certificate::GetIssuerName()
extern TypeInfo* CryptographicException_t1_1194_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2538;
extern "C" String_t* X509Certificate_GetIssuerName_m1_10128 (X509Certificate_t1_1179 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CryptographicException_t1_1194_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(197);
		_stringLiteral2538 = il2cpp_codegen_string_literal_from_index(2538);
		s_Il2CppMethodIntialized = true;
	}
	{
		X509Certificate_t1_151 * L_0 = (__this->___x509_0);
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2538, /*hidden argument*/NULL);
		CryptographicException_t1_1194 * L_2 = (CryptographicException_t1_1194 *)il2cpp_codegen_object_new (CryptographicException_t1_1194_il2cpp_TypeInfo_var);
		CryptographicException__ctor_m1_10222(L_2, L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001b:
	{
		X509Certificate_t1_151 * L_3 = (__this->___x509_0);
		NullCheck(L_3);
		String_t* L_4 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(5 /* System.String Mono.Security.X509.X509Certificate::get_IssuerName() */, L_3);
		return L_4;
	}
}
// System.String System.Security.Cryptography.X509Certificates.X509Certificate::GetKeyAlgorithm()
extern TypeInfo* CryptographicException_t1_1194_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2538;
extern "C" String_t* X509Certificate_GetKeyAlgorithm_m1_10129 (X509Certificate_t1_1179 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CryptographicException_t1_1194_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(197);
		_stringLiteral2538 = il2cpp_codegen_string_literal_from_index(2538);
		s_Il2CppMethodIntialized = true;
	}
	{
		X509Certificate_t1_151 * L_0 = (__this->___x509_0);
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2538, /*hidden argument*/NULL);
		CryptographicException_t1_1194 * L_2 = (CryptographicException_t1_1194 *)il2cpp_codegen_object_new (CryptographicException_t1_1194_il2cpp_TypeInfo_var);
		CryptographicException__ctor_m1_10222(L_2, L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001b:
	{
		X509Certificate_t1_151 * L_3 = (__this->___x509_0);
		NullCheck(L_3);
		String_t* L_4 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String Mono.Security.X509.X509Certificate::get_KeyAlgorithm() */, L_3);
		return L_4;
	}
}
// System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate::GetKeyAlgorithmParameters()
extern TypeInfo* CryptographicException_t1_1194_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2538;
extern Il2CppCodeGenString* _stringLiteral2540;
extern "C" ByteU5BU5D_t1_109* X509Certificate_GetKeyAlgorithmParameters_m1_10130 (X509Certificate_t1_1179 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CryptographicException_t1_1194_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(197);
		_stringLiteral2538 = il2cpp_codegen_string_literal_from_index(2538);
		_stringLiteral2540 = il2cpp_codegen_string_literal_from_index(2540);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	{
		X509Certificate_t1_151 * L_0 = (__this->___x509_0);
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2538, /*hidden argument*/NULL);
		CryptographicException_t1_1194 * L_2 = (CryptographicException_t1_1194 *)il2cpp_codegen_object_new (CryptographicException_t1_1194_il2cpp_TypeInfo_var);
		CryptographicException__ctor_m1_10222(L_2, L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001b:
	{
		X509Certificate_t1_151 * L_3 = (__this->___x509_0);
		NullCheck(L_3);
		ByteU5BU5D_t1_109* L_4 = (ByteU5BU5D_t1_109*)VirtFuncInvoker0< ByteU5BU5D_t1_109* >::Invoke(7 /* System.Byte[] Mono.Security.X509.X509Certificate::get_KeyAlgorithmParameters() */, L_3);
		V_0 = L_4;
		ByteU5BU5D_t1_109* L_5 = V_0;
		if (L_5)
		{
			goto IL_003d;
		}
	}
	{
		String_t* L_6 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2540, /*hidden argument*/NULL);
		CryptographicException_t1_1194 * L_7 = (CryptographicException_t1_1194 *)il2cpp_codegen_object_new (CryptographicException_t1_1194_il2cpp_TypeInfo_var);
		CryptographicException__ctor_m1_10222(L_7, L_6, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_7);
	}

IL_003d:
	{
		ByteU5BU5D_t1_109* L_8 = V_0;
		return L_8;
	}
}
// System.String System.Security.Cryptography.X509Certificates.X509Certificate::GetKeyAlgorithmParametersString()
extern "C" String_t* X509Certificate_GetKeyAlgorithmParametersString_m1_10131 (X509Certificate_t1_1179 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t1_109* L_0 = (ByteU5BU5D_t1_109*)VirtFuncInvoker0< ByteU5BU5D_t1_109* >::Invoke(14 /* System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate::GetKeyAlgorithmParameters() */, __this);
		String_t* L_1 = X509Certificate_tostr_m1_10117(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String System.Security.Cryptography.X509Certificates.X509Certificate::GetName()
extern TypeInfo* CryptographicException_t1_1194_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2538;
extern "C" String_t* X509Certificate_GetName_m1_10132 (X509Certificate_t1_1179 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CryptographicException_t1_1194_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(197);
		_stringLiteral2538 = il2cpp_codegen_string_literal_from_index(2538);
		s_Il2CppMethodIntialized = true;
	}
	{
		X509Certificate_t1_151 * L_0 = (__this->___x509_0);
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2538, /*hidden argument*/NULL);
		CryptographicException_t1_1194 * L_2 = (CryptographicException_t1_1194 *)il2cpp_codegen_object_new (CryptographicException_t1_1194_il2cpp_TypeInfo_var);
		CryptographicException__ctor_m1_10222(L_2, L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001b:
	{
		X509Certificate_t1_151 * L_3 = (__this->___x509_0);
		NullCheck(L_3);
		String_t* L_4 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(17 /* System.String Mono.Security.X509.X509Certificate::get_SubjectName() */, L_3);
		return L_4;
	}
}
// System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate::GetPublicKey()
extern TypeInfo* CryptographicException_t1_1194_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2538;
extern "C" ByteU5BU5D_t1_109* X509Certificate_GetPublicKey_m1_10133 (X509Certificate_t1_1179 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CryptographicException_t1_1194_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(197);
		_stringLiteral2538 = il2cpp_codegen_string_literal_from_index(2538);
		s_Il2CppMethodIntialized = true;
	}
	{
		X509Certificate_t1_151 * L_0 = (__this->___x509_0);
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2538, /*hidden argument*/NULL);
		CryptographicException_t1_1194 * L_2 = (CryptographicException_t1_1194 *)il2cpp_codegen_object_new (CryptographicException_t1_1194_il2cpp_TypeInfo_var);
		CryptographicException__ctor_m1_10222(L_2, L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001b:
	{
		X509Certificate_t1_151 * L_3 = (__this->___x509_0);
		NullCheck(L_3);
		ByteU5BU5D_t1_109* L_4 = (ByteU5BU5D_t1_109*)VirtFuncInvoker0< ByteU5BU5D_t1_109* >::Invoke(9 /* System.Byte[] Mono.Security.X509.X509Certificate::get_PublicKey() */, L_3);
		return L_4;
	}
}
// System.String System.Security.Cryptography.X509Certificates.X509Certificate::GetPublicKeyString()
extern "C" String_t* X509Certificate_GetPublicKeyString_m1_10134 (X509Certificate_t1_1179 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t1_109* L_0 = (ByteU5BU5D_t1_109*)VirtFuncInvoker0< ByteU5BU5D_t1_109* >::Invoke(17 /* System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate::GetPublicKey() */, __this);
		String_t* L_1 = X509Certificate_tostr_m1_10117(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate::GetRawCertData()
extern TypeInfo* CryptographicException_t1_1194_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2538;
extern "C" ByteU5BU5D_t1_109* X509Certificate_GetRawCertData_m1_10135 (X509Certificate_t1_1179 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CryptographicException_t1_1194_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(197);
		_stringLiteral2538 = il2cpp_codegen_string_literal_from_index(2538);
		s_Il2CppMethodIntialized = true;
	}
	{
		X509Certificate_t1_151 * L_0 = (__this->___x509_0);
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2538, /*hidden argument*/NULL);
		CryptographicException_t1_1194 * L_2 = (CryptographicException_t1_1194 *)il2cpp_codegen_object_new (CryptographicException_t1_1194_il2cpp_TypeInfo_var);
		CryptographicException__ctor_m1_10222(L_2, L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001b:
	{
		X509Certificate_t1_151 * L_3 = (__this->___x509_0);
		NullCheck(L_3);
		ByteU5BU5D_t1_109* L_4 = (ByteU5BU5D_t1_109*)VirtFuncInvoker0< ByteU5BU5D_t1_109* >::Invoke(12 /* System.Byte[] Mono.Security.X509.X509Certificate::get_RawData() */, L_3);
		return L_4;
	}
}
// System.String System.Security.Cryptography.X509Certificates.X509Certificate::GetRawCertDataString()
extern TypeInfo* CryptographicException_t1_1194_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2538;
extern "C" String_t* X509Certificate_GetRawCertDataString_m1_10136 (X509Certificate_t1_1179 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CryptographicException_t1_1194_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(197);
		_stringLiteral2538 = il2cpp_codegen_string_literal_from_index(2538);
		s_Il2CppMethodIntialized = true;
	}
	{
		X509Certificate_t1_151 * L_0 = (__this->___x509_0);
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2538, /*hidden argument*/NULL);
		CryptographicException_t1_1194 * L_2 = (CryptographicException_t1_1194 *)il2cpp_codegen_object_new (CryptographicException_t1_1194_il2cpp_TypeInfo_var);
		CryptographicException__ctor_m1_10222(L_2, L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001b:
	{
		X509Certificate_t1_151 * L_3 = (__this->___x509_0);
		NullCheck(L_3);
		ByteU5BU5D_t1_109* L_4 = (ByteU5BU5D_t1_109*)VirtFuncInvoker0< ByteU5BU5D_t1_109* >::Invoke(12 /* System.Byte[] Mono.Security.X509.X509Certificate::get_RawData() */, L_3);
		String_t* L_5 = X509Certificate_tostr_m1_10117(__this, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate::GetSerialNumber()
extern TypeInfo* CryptographicException_t1_1194_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2538;
extern "C" ByteU5BU5D_t1_109* X509Certificate_GetSerialNumber_m1_10137 (X509Certificate_t1_1179 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CryptographicException_t1_1194_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(197);
		_stringLiteral2538 = il2cpp_codegen_string_literal_from_index(2538);
		s_Il2CppMethodIntialized = true;
	}
	{
		X509Certificate_t1_151 * L_0 = (__this->___x509_0);
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2538, /*hidden argument*/NULL);
		CryptographicException_t1_1194 * L_2 = (CryptographicException_t1_1194 *)il2cpp_codegen_object_new (CryptographicException_t1_1194_il2cpp_TypeInfo_var);
		CryptographicException__ctor_m1_10222(L_2, L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001b:
	{
		X509Certificate_t1_151 * L_3 = (__this->___x509_0);
		NullCheck(L_3);
		ByteU5BU5D_t1_109* L_4 = (ByteU5BU5D_t1_109*)VirtFuncInvoker0< ByteU5BU5D_t1_109* >::Invoke(13 /* System.Byte[] Mono.Security.X509.X509Certificate::get_SerialNumber() */, L_3);
		return L_4;
	}
}
// System.String System.Security.Cryptography.X509Certificates.X509Certificate::GetSerialNumberString()
extern "C" String_t* X509Certificate_GetSerialNumberString_m1_10138 (X509Certificate_t1_1179 * __this, const MethodInfo* method)
{
	ByteU5BU5D_t1_109* V_0 = {0};
	{
		ByteU5BU5D_t1_109* L_0 = (ByteU5BU5D_t1_109*)VirtFuncInvoker0< ByteU5BU5D_t1_109* >::Invoke(21 /* System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate::GetSerialNumber() */, __this);
		V_0 = L_0;
		ByteU5BU5D_t1_109* L_1 = V_0;
		Array_Reverse_m1_1052(NULL /*static, unused*/, (Array_t *)(Array_t *)L_1, /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_2 = V_0;
		String_t* L_3 = X509Certificate_tostr_m1_10117(__this, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.String System.Security.Cryptography.X509Certificates.X509Certificate::ToString()
extern "C" String_t* X509Certificate_ToString_m1_10139 (X509Certificate_t1_1179 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = Object_ToString_m1_7(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String System.Security.Cryptography.X509Certificates.X509Certificate::ToString(System.Boolean)
extern TypeInfo* StringBuilder_t1_247_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2541;
extern Il2CppCodeGenString* _stringLiteral2542;
extern Il2CppCodeGenString* _stringLiteral2543;
extern Il2CppCodeGenString* _stringLiteral2544;
extern Il2CppCodeGenString* _stringLiteral2545;
extern "C" String_t* X509Certificate_ToString_m1_10140 (X509Certificate_t1_1179 * __this, bool ___fVerbose, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringBuilder_t1_247_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		_stringLiteral2541 = il2cpp_codegen_string_literal_from_index(2541);
		_stringLiteral2542 = il2cpp_codegen_string_literal_from_index(2542);
		_stringLiteral2543 = il2cpp_codegen_string_literal_from_index(2543);
		_stringLiteral2544 = il2cpp_codegen_string_literal_from_index(2544);
		_stringLiteral2545 = il2cpp_codegen_string_literal_from_index(2545);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	StringBuilder_t1_247 * V_1 = {0};
	{
		bool L_0 = ___fVerbose;
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		X509Certificate_t1_151 * L_1 = (__this->___x509_0);
		if (L_1)
		{
			goto IL_0018;
		}
	}

IL_0011:
	{
		String_t* L_2 = Object_ToString_m1_7(__this, /*hidden argument*/NULL);
		return L_2;
	}

IL_0018:
	{
		String_t* L_3 = Environment_get_NewLine_m1_14052(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_3;
		StringBuilder_t1_247 * L_4 = (StringBuilder_t1_247 *)il2cpp_codegen_object_new (StringBuilder_t1_247_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1_12414(L_4, /*hidden argument*/NULL);
		V_1 = L_4;
		StringBuilder_t1_247 * L_5 = V_1;
		String_t* L_6 = V_0;
		String_t* L_7 = X509Certificate_get_Subject_m1_10143(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		StringBuilder_AppendFormat_m1_12461(L_5, _stringLiteral2541, L_6, L_7, /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_8 = V_1;
		String_t* L_9 = V_0;
		String_t* L_10 = X509Certificate_get_Issuer_m1_10142(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		StringBuilder_AppendFormat_m1_12461(L_8, _stringLiteral2542, L_9, L_10, /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_11 = V_1;
		String_t* L_12 = V_0;
		String_t* L_13 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.Security.Cryptography.X509Certificates.X509Certificate::GetEffectiveDateString() */, __this);
		NullCheck(L_11);
		StringBuilder_AppendFormat_m1_12461(L_11, _stringLiteral2543, L_12, L_13, /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_14 = V_1;
		String_t* L_15 = V_0;
		String_t* L_16 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(10 /* System.String System.Security.Cryptography.X509Certificates.X509Certificate::GetExpirationDateString() */, __this);
		NullCheck(L_14);
		StringBuilder_AppendFormat_m1_12461(L_14, _stringLiteral2544, L_15, L_16, /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_17 = V_1;
		String_t* L_18 = V_0;
		String_t* L_19 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Security.Cryptography.X509Certificates.X509Certificate::GetCertHashString() */, __this);
		NullCheck(L_17);
		StringBuilder_AppendFormat_m1_12461(L_17, _stringLiteral2545, L_18, L_19, /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_20 = V_1;
		String_t* L_21 = V_0;
		NullCheck(L_20);
		StringBuilder_Append_m1_12438(L_20, L_21, /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_22 = V_1;
		NullCheck(L_22);
		String_t* L_23 = StringBuilder_ToString_m1_12428(L_22, /*hidden argument*/NULL);
		return L_23;
	}
}
// System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate::Load(System.String)
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern "C" ByteU5BU5D_t1_109* X509Certificate_Load_m1_10141 (Object_t * __this /* static, unused */, String_t* ___fileName, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	FileStream_t1_146 * V_1 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (ByteU5BU5D_t1_109*)NULL;
		String_t* L_0 = ___fileName;
		FileStream_t1_146 * L_1 = File_OpenRead_m1_4869(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_1 = L_1;
	}

IL_0009:
	try
	{ // begin try (depth: 1)
		FileStream_t1_146 * L_2 = V_1;
		NullCheck(L_2);
		int64_t L_3 = (int64_t)VirtFuncInvoker0< int64_t >::Invoke(9 /* System.Int64 System.IO.FileStream::get_Length() */, L_2);
		if ((int64_t)(L_3) > INTPTR_MAX) il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_overflow_exception());
		V_0 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, (((intptr_t)L_3))));
		FileStream_t1_146 * L_4 = V_1;
		ByteU5BU5D_t1_109* L_5 = V_0;
		ByteU5BU5D_t1_109* L_6 = V_0;
		NullCheck(L_6);
		NullCheck(L_4);
		VirtFuncInvoker3< int32_t, ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(20 /* System.Int32 System.IO.FileStream::Read(System.Byte[],System.Int32,System.Int32) */, L_4, L_5, 0, (((int32_t)((int32_t)(((Array_t *)L_6)->max_length)))));
		FileStream_t1_146 * L_7 = V_1;
		NullCheck(L_7);
		VirtActionInvoker0::Invoke(13 /* System.Void System.IO.Stream::Close() */, L_7);
		IL2CPP_LEAVE(0x3A, FINALLY_002d);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_002d;
	}

FINALLY_002d:
	{ // begin finally (depth: 1)
		{
			FileStream_t1_146 * L_8 = V_1;
			if (!L_8)
			{
				goto IL_0039;
			}
		}

IL_0033:
		{
			FileStream_t1_146 * L_9 = V_1;
			NullCheck(L_9);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_9);
		}

IL_0039:
		{
			IL2CPP_END_FINALLY(45)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(45)
	{
		IL2CPP_JUMP_TBL(0x3A, IL_003a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_003a:
	{
		ByteU5BU5D_t1_109* L_10 = V_0;
		return L_10;
	}
}
// System.String System.Security.Cryptography.X509Certificates.X509Certificate::get_Issuer()
extern TypeInfo* CryptographicException_t1_1194_il2cpp_TypeInfo_var;
extern TypeInfo* X501_t1_188_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2538;
extern Il2CppCodeGenString* _stringLiteral167;
extern "C" String_t* X509Certificate_get_Issuer_m1_10142 (X509Certificate_t1_1179 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CryptographicException_t1_1194_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(197);
		X501_t1_188_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(229);
		_stringLiteral2538 = il2cpp_codegen_string_literal_from_index(2538);
		_stringLiteral167 = il2cpp_codegen_string_literal_from_index(167);
		s_Il2CppMethodIntialized = true;
	}
	{
		X509Certificate_t1_151 * L_0 = (__this->___x509_0);
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2538, /*hidden argument*/NULL);
		CryptographicException_t1_1194 * L_2 = (CryptographicException_t1_1194 *)il2cpp_codegen_object_new (CryptographicException_t1_1194_il2cpp_TypeInfo_var);
		CryptographicException__ctor_m1_10222(L_2, L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001b:
	{
		String_t* L_3 = (__this->___issuer_name_3);
		if (L_3)
		{
			goto IL_0043;
		}
	}
	{
		X509Certificate_t1_151 * L_4 = (__this->___x509_0);
		NullCheck(L_4);
		ASN1_t1_149 * L_5 = X509Certificate_GetIssuerName_m1_2277(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(X501_t1_188_il2cpp_TypeInfo_var);
		String_t* L_6 = X501_ToString_m1_2199(NULL /*static, unused*/, L_5, 1, _stringLiteral167, 1, /*hidden argument*/NULL);
		__this->___issuer_name_3 = L_6;
	}

IL_0043:
	{
		String_t* L_7 = (__this->___issuer_name_3);
		return L_7;
	}
}
// System.String System.Security.Cryptography.X509Certificates.X509Certificate::get_Subject()
extern TypeInfo* CryptographicException_t1_1194_il2cpp_TypeInfo_var;
extern TypeInfo* X501_t1_188_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2538;
extern Il2CppCodeGenString* _stringLiteral167;
extern "C" String_t* X509Certificate_get_Subject_m1_10143 (X509Certificate_t1_1179 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CryptographicException_t1_1194_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(197);
		X501_t1_188_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(229);
		_stringLiteral2538 = il2cpp_codegen_string_literal_from_index(2538);
		_stringLiteral167 = il2cpp_codegen_string_literal_from_index(167);
		s_Il2CppMethodIntialized = true;
	}
	{
		X509Certificate_t1_151 * L_0 = (__this->___x509_0);
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2538, /*hidden argument*/NULL);
		CryptographicException_t1_1194 * L_2 = (CryptographicException_t1_1194 *)il2cpp_codegen_object_new (CryptographicException_t1_1194_il2cpp_TypeInfo_var);
		CryptographicException__ctor_m1_10222(L_2, L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001b:
	{
		String_t* L_3 = (__this->___subject_name_4);
		if (L_3)
		{
			goto IL_0043;
		}
	}
	{
		X509Certificate_t1_151 * L_4 = (__this->___x509_0);
		NullCheck(L_4);
		ASN1_t1_149 * L_5 = X509Certificate_GetSubjectName_m1_2278(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(X501_t1_188_il2cpp_TypeInfo_var);
		String_t* L_6 = X501_ToString_m1_2199(NULL /*static, unused*/, L_5, 1, _stringLiteral167, 1, /*hidden argument*/NULL);
		__this->___subject_name_4 = L_6;
	}

IL_0043:
	{
		String_t* L_7 = (__this->___subject_name_4);
		return L_7;
	}
}
// System.IntPtr System.Security.Cryptography.X509Certificates.X509Certificate::get_Handle()
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" IntPtr_t X509Certificate_get_Handle_m1_10144 (X509Certificate_t1_1179 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		return L_0;
	}
}
// System.Boolean System.Security.Cryptography.X509Certificates.X509Certificate::Equals(System.Object)
extern TypeInfo* X509Certificate_t1_1179_il2cpp_TypeInfo_var;
extern "C" bool X509Certificate_Equals_m1_10145 (X509Certificate_t1_1179 * __this, Object_t * ___obj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		X509Certificate_t1_1179_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(867);
		s_Il2CppMethodIntialized = true;
	}
	X509Certificate_t1_1179 * V_0 = {0};
	{
		Object_t * L_0 = ___obj;
		V_0 = ((X509Certificate_t1_1179 *)IsInstClass(L_0, X509Certificate_t1_1179_il2cpp_TypeInfo_var));
		X509Certificate_t1_1179 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0015;
		}
	}
	{
		X509Certificate_t1_1179 * L_2 = V_0;
		bool L_3 = (bool)VirtFuncInvoker1< bool, X509Certificate_t1_1179 * >::Invoke(6 /* System.Boolean System.Security.Cryptography.X509Certificates.X509Certificate::Equals(System.Security.Cryptography.X509Certificates.X509Certificate) */, __this, L_2);
		return L_3;
	}

IL_0015:
	{
		return 0;
	}
}
// System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate::Export(System.Security.Cryptography.X509Certificates.X509ContentType)
extern "C" ByteU5BU5D_t1_109* X509Certificate_Export_m1_10146 (X509Certificate_t1_1179 * __this, int32_t ___contentType, const MethodInfo* method)
{
	{
		int32_t L_0 = ___contentType;
		ByteU5BU5D_t1_109* L_1 = X509Certificate_Export_m1_10149(__this, L_0, (ByteU5BU5D_t1_109*)(ByteU5BU5D_t1_109*)NULL, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate::Export(System.Security.Cryptography.X509Certificates.X509ContentType,System.String)
extern TypeInfo* Encoding_t1_406_il2cpp_TypeInfo_var;
extern "C" ByteU5BU5D_t1_109* X509Certificate_Export_m1_10147 (X509Certificate_t1_1179 * __this, int32_t ___contentType, String_t* ___password, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Encoding_t1_406_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	ByteU5BU5D_t1_109* G_B3_0 = {0};
	{
		String_t* L_0 = ___password;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		G_B3_0 = ((ByteU5BU5D_t1_109*)(NULL));
		goto IL_0017;
	}

IL_000c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t1_406_il2cpp_TypeInfo_var);
		Encoding_t1_406 * L_1 = Encoding_get_UTF8_m1_12363(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_2 = ___password;
		NullCheck(L_1);
		ByteU5BU5D_t1_109* L_3 = (ByteU5BU5D_t1_109*)VirtFuncInvoker1< ByteU5BU5D_t1_109*, String_t* >::Invoke(11 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_1, L_2);
		G_B3_0 = L_3;
	}

IL_0017:
	{
		V_0 = G_B3_0;
		int32_t L_4 = ___contentType;
		ByteU5BU5D_t1_109* L_5 = V_0;
		ByteU5BU5D_t1_109* L_6 = X509Certificate_Export_m1_10149(__this, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate::Export(System.Security.Cryptography.X509Certificates.X509ContentType,System.Security.SecureString)
extern "C" ByteU5BU5D_t1_109* X509Certificate_Export_m1_10148 (X509Certificate_t1_1179 * __this, int32_t ___contentType, SecureString_t1_1197 * ___password, const MethodInfo* method)
{
	ByteU5BU5D_t1_109* V_0 = {0};
	ByteU5BU5D_t1_109* G_B3_0 = {0};
	{
		SecureString_t1_1197 * L_0 = ___password;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		G_B3_0 = ((ByteU5BU5D_t1_109*)(NULL));
		goto IL_0012;
	}

IL_000c:
	{
		SecureString_t1_1197 * L_1 = ___password;
		NullCheck(L_1);
		ByteU5BU5D_t1_109* L_2 = SecureString_GetBuffer_m1_12010(L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
	}

IL_0012:
	{
		V_0 = G_B3_0;
		int32_t L_3 = ___contentType;
		ByteU5BU5D_t1_109* L_4 = V_0;
		ByteU5BU5D_t1_109* L_5 = X509Certificate_Export_m1_10149(__this, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate::Export(System.Security.Cryptography.X509Certificates.X509ContentType,System.Byte[])
extern TypeInfo* CryptographicException_t1_1194_il2cpp_TypeInfo_var;
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* X509ContentType_t1_1180_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2538;
extern Il2CppCodeGenString* _stringLiteral2546;
extern "C" ByteU5BU5D_t1_109* X509Certificate_Export_m1_10149 (X509Certificate_t1_1179 * __this, int32_t ___contentType, ByteU5BU5D_t1_109* ___password, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CryptographicException_t1_1194_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(197);
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		X509ContentType_t1_1180_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(869);
		_stringLiteral2538 = il2cpp_codegen_string_literal_from_index(2538);
		_stringLiteral2546 = il2cpp_codegen_string_literal_from_index(2546);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	int32_t V_1 = {0};
	ByteU5BU5D_t1_109* V_2 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		X509Certificate_t1_151 * L_0 = (__this->___x509_0);
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2538, /*hidden argument*/NULL);
		CryptographicException_t1_1194 * L_2 = (CryptographicException_t1_1194 *)il2cpp_codegen_object_new (CryptographicException_t1_1194_il2cpp_TypeInfo_var);
		CryptographicException__ctor_m1_10222(L_2, L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001b:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_3 = ___contentType;
			V_1 = L_3;
			int32_t L_4 = V_1;
			if (((int32_t)((int32_t)L_4-(int32_t)1)) == 0)
			{
				goto IL_0036;
			}
			if (((int32_t)((int32_t)L_4-(int32_t)1)) == 1)
			{
				goto IL_004d;
			}
			if (((int32_t)((int32_t)L_4-(int32_t)1)) == 2)
			{
				goto IL_0047;
			}
		}

IL_0031:
		{
			goto IL_0053;
		}

IL_0036:
		{
			X509Certificate_t1_151 * L_5 = (__this->___x509_0);
			NullCheck(L_5);
			ByteU5BU5D_t1_109* L_6 = (ByteU5BU5D_t1_109*)VirtFuncInvoker0< ByteU5BU5D_t1_109* >::Invoke(12 /* System.Byte[] Mono.Security.X509.X509Certificate::get_RawData() */, L_5);
			V_2 = L_6;
			IL2CPP_LEAVE(0x8A, FINALLY_0079);
		}

IL_0047:
		{
			NotSupportedException_t1_1583 * L_7 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
			NotSupportedException__ctor_m1_14416(L_7, /*hidden argument*/NULL);
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_7);
		}

IL_004d:
		{
			NotSupportedException_t1_1583 * L_8 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
			NotSupportedException__ctor_m1_14416(L_8, /*hidden argument*/NULL);
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_8);
		}

IL_0053:
		{
			ObjectU5BU5D_t1_272* L_9 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 1));
			int32_t L_10 = ___contentType;
			int32_t L_11 = L_10;
			Object_t * L_12 = Box(X509ContentType_t1_1180_il2cpp_TypeInfo_var, &L_11);
			NullCheck(L_9);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 0);
			ArrayElementTypeCheck (L_9, L_12);
			*((Object_t **)(Object_t **)SZArrayLdElema(L_9, 0, sizeof(Object_t *))) = (Object_t *)L_12;
			String_t* L_13 = Locale_GetText_m1_1405(NULL /*static, unused*/, _stringLiteral2546, L_9, /*hidden argument*/NULL);
			V_0 = L_13;
			String_t* L_14 = V_0;
			CryptographicException_t1_1194 * L_15 = (CryptographicException_t1_1194 *)il2cpp_codegen_object_new (CryptographicException_t1_1194_il2cpp_TypeInfo_var);
			CryptographicException__ctor_m1_10222(L_15, L_14, /*hidden argument*/NULL);
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_15);
		}

IL_0074:
		{
			IL2CPP_LEAVE(0x8A, FINALLY_0079);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0079;
	}

FINALLY_0079:
	{ // begin finally (depth: 1)
		{
			ByteU5BU5D_t1_109* L_16 = ___password;
			if (!L_16)
			{
				goto IL_0089;
			}
		}

IL_007f:
		{
			ByteU5BU5D_t1_109* L_17 = ___password;
			ByteU5BU5D_t1_109* L_18 = ___password;
			NullCheck(L_18);
			Array_Clear_m1_1037(NULL /*static, unused*/, (Array_t *)(Array_t *)L_17, 0, (((int32_t)((int32_t)(((Array_t *)L_18)->max_length)))), /*hidden argument*/NULL);
		}

IL_0089:
		{
			IL2CPP_END_FINALLY(121)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(121)
	{
		IL2CPP_JUMP_TBL(0x8A, IL_008a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_008a:
	{
		ByteU5BU5D_t1_109* L_19 = V_2;
		return L_19;
	}
}
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate::Import(System.Byte[])
extern "C" void X509Certificate_Import_m1_10150 (X509Certificate_t1_1179 * __this, ByteU5BU5D_t1_109* ___rawData, const MethodInfo* method)
{
	{
		ByteU5BU5D_t1_109* L_0 = ___rawData;
		VirtActionInvoker3< ByteU5BU5D_t1_109*, String_t*, int32_t >::Invoke(28 /* System.Void System.Security.Cryptography.X509Certificates.X509Certificate::Import(System.Byte[],System.String,System.Security.Cryptography.X509Certificates.X509KeyStorageFlags) */, __this, L_0, (String_t*)NULL, 0);
		return;
	}
}
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate::Import(System.Byte[],System.String,System.Security.Cryptography.X509Certificates.X509KeyStorageFlags)
extern TypeInfo* X509Certificate_t1_151_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t1_33_il2cpp_TypeInfo_var;
extern TypeInfo* PKCS12_t1_187_il2cpp_TypeInfo_var;
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern TypeInfo* CryptographicException_t1_1194_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2547;
extern "C" void X509Certificate_Import_m1_10151 (X509Certificate_t1_1179 * __this, ByteU5BU5D_t1_109* ___rawData, String_t* ___password, int32_t ___keyStorageFlags, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		X509Certificate_t1_151_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(226);
		Exception_t1_33_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(49);
		PKCS12_t1_187_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(220);
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		CryptographicException_t1_1194_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(197);
		_stringLiteral2547 = il2cpp_codegen_string_literal_from_index(2547);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1_33 * V_0 = {0};
	PKCS12_t1_187 * V_1 = {0};
	String_t* V_2 = {0};
	PKCS12_t1_187 * V_3 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		VirtActionInvoker0::Invoke(33 /* System.Void System.Security.Cryptography.X509Certificates.X509Certificate::Reset() */, __this);
		String_t* L_0 = ___password;
		if (L_0)
		{
			goto IL_007c;
		}
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		ByteU5BU5D_t1_109* L_1 = ___rawData;
		X509Certificate_t1_151 * L_2 = (X509Certificate_t1_151 *)il2cpp_codegen_object_new (X509Certificate_t1_151_il2cpp_TypeInfo_var);
		X509Certificate__ctor_m1_2242(L_2, L_1, /*hidden argument*/NULL);
		__this->___x509_0 = L_2;
		goto IL_0077;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1_33 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1_33_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_001d:
	{ // begin catch(System.Exception)
		{
			V_0 = ((Exception_t1_33 *)__exception_local);
		}

IL_001e:
		try
		{ // begin try (depth: 2)
			{
				ByteU5BU5D_t1_109* L_3 = ___rawData;
				PKCS12_t1_187 * L_4 = (PKCS12_t1_187 *)il2cpp_codegen_object_new (PKCS12_t1_187_il2cpp_TypeInfo_var);
				PKCS12__ctor_m1_2142(L_4, L_3, /*hidden argument*/NULL);
				V_1 = L_4;
				PKCS12_t1_187 * L_5 = V_1;
				NullCheck(L_5);
				X509CertificateCollection_t1_148 * L_6 = PKCS12_get_Certificates_m1_2153(L_5, /*hidden argument*/NULL);
				NullCheck(L_6);
				int32_t L_7 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 System.Collections.CollectionBase::get_Count() */, L_6);
				if ((((int32_t)L_7) <= ((int32_t)0)))
				{
					goto IL_004d;
				}
			}

IL_0036:
			{
				PKCS12_t1_187 * L_8 = V_1;
				NullCheck(L_8);
				X509CertificateCollection_t1_148 * L_9 = PKCS12_get_Certificates_m1_2153(L_8, /*hidden argument*/NULL);
				NullCheck(L_9);
				X509Certificate_t1_151 * L_10 = X509CertificateCollection_get_Item_m1_2292(L_9, 0, /*hidden argument*/NULL);
				__this->___x509_0 = L_10;
				goto IL_0054;
			}

IL_004d:
			{
				__this->___x509_0 = (X509Certificate_t1_151 *)NULL;
			}

IL_0054:
			{
				goto IL_0072;
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t1_33 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (Object_t_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_0059;
			throw e;
		}

CATCH_0059:
		{ // begin catch(System.Object)
			{
				String_t* L_11 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2547, /*hidden argument*/NULL);
				V_2 = L_11;
				String_t* L_12 = V_2;
				Exception_t1_33 * L_13 = V_0;
				CryptographicException_t1_1194 * L_14 = (CryptographicException_t1_1194 *)il2cpp_codegen_object_new (CryptographicException_t1_1194_il2cpp_TypeInfo_var);
				CryptographicException__ctor_m1_10223(L_14, L_12, L_13, /*hidden argument*/NULL);
				il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_14);
			}

IL_006d:
			{
				goto IL_0072;
			}
		} // end catch (depth: 2)

IL_0072:
		{
			goto IL_0077;
		}
	} // end catch (depth: 1)

IL_0077:
	{
		goto IL_00ca;
	}

IL_007c:
	try
	{ // begin try (depth: 1)
		{
			ByteU5BU5D_t1_109* L_15 = ___rawData;
			String_t* L_16 = ___password;
			PKCS12_t1_187 * L_17 = (PKCS12_t1_187 *)il2cpp_codegen_object_new (PKCS12_t1_187_il2cpp_TypeInfo_var);
			PKCS12__ctor_m1_2143(L_17, L_15, L_16, /*hidden argument*/NULL);
			V_3 = L_17;
			PKCS12_t1_187 * L_18 = V_3;
			NullCheck(L_18);
			X509CertificateCollection_t1_148 * L_19 = PKCS12_get_Certificates_m1_2153(L_18, /*hidden argument*/NULL);
			NullCheck(L_19);
			int32_t L_20 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 System.Collections.CollectionBase::get_Count() */, L_19);
			if ((((int32_t)L_20) <= ((int32_t)0)))
			{
				goto IL_00ac;
			}
		}

IL_0095:
		{
			PKCS12_t1_187 * L_21 = V_3;
			NullCheck(L_21);
			X509CertificateCollection_t1_148 * L_22 = PKCS12_get_Certificates_m1_2153(L_21, /*hidden argument*/NULL);
			NullCheck(L_22);
			X509Certificate_t1_151 * L_23 = X509CertificateCollection_get_Item_m1_2292(L_22, 0, /*hidden argument*/NULL);
			__this->___x509_0 = L_23;
			goto IL_00b3;
		}

IL_00ac:
		{
			__this->___x509_0 = (X509Certificate_t1_151 *)NULL;
		}

IL_00b3:
		{
			goto IL_00ca;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1_33 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Object_t_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_00b8;
		throw e;
	}

CATCH_00b8:
	{ // begin catch(System.Object)
		ByteU5BU5D_t1_109* L_24 = ___rawData;
		X509Certificate_t1_151 * L_25 = (X509Certificate_t1_151 *)il2cpp_codegen_object_new (X509Certificate_t1_151_il2cpp_TypeInfo_var);
		X509Certificate__ctor_m1_2242(L_25, L_24, /*hidden argument*/NULL);
		__this->___x509_0 = L_25;
		goto IL_00ca;
	} // end catch (depth: 1)

IL_00ca:
	{
		return;
	}
}
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate::Import(System.Byte[],System.Security.SecureString,System.Security.Cryptography.X509Certificates.X509KeyStorageFlags)
extern "C" void X509Certificate_Import_m1_10152 (X509Certificate_t1_1179 * __this, ByteU5BU5D_t1_109* ___rawData, SecureString_t1_1197 * ___password, int32_t ___keyStorageFlags, const MethodInfo* method)
{
	{
		ByteU5BU5D_t1_109* L_0 = ___rawData;
		int32_t L_1 = ___keyStorageFlags;
		VirtActionInvoker3< ByteU5BU5D_t1_109*, String_t*, int32_t >::Invoke(28 /* System.Void System.Security.Cryptography.X509Certificates.X509Certificate::Import(System.Byte[],System.String,System.Security.Cryptography.X509Certificates.X509KeyStorageFlags) */, __this, L_0, (String_t*)NULL, L_1);
		return;
	}
}
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate::Import(System.String)
extern "C" void X509Certificate_Import_m1_10153 (X509Certificate_t1_1179 * __this, String_t* ___fileName, const MethodInfo* method)
{
	ByteU5BU5D_t1_109* V_0 = {0};
	{
		String_t* L_0 = ___fileName;
		ByteU5BU5D_t1_109* L_1 = X509Certificate_Load_m1_10141(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		ByteU5BU5D_t1_109* L_2 = V_0;
		VirtActionInvoker3< ByteU5BU5D_t1_109*, String_t*, int32_t >::Invoke(28 /* System.Void System.Security.Cryptography.X509Certificates.X509Certificate::Import(System.Byte[],System.String,System.Security.Cryptography.X509Certificates.X509KeyStorageFlags) */, __this, L_2, (String_t*)NULL, 0);
		return;
	}
}
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate::Import(System.String,System.String,System.Security.Cryptography.X509Certificates.X509KeyStorageFlags)
extern "C" void X509Certificate_Import_m1_10154 (X509Certificate_t1_1179 * __this, String_t* ___fileName, String_t* ___password, int32_t ___keyStorageFlags, const MethodInfo* method)
{
	ByteU5BU5D_t1_109* V_0 = {0};
	{
		String_t* L_0 = ___fileName;
		ByteU5BU5D_t1_109* L_1 = X509Certificate_Load_m1_10141(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		ByteU5BU5D_t1_109* L_2 = V_0;
		String_t* L_3 = ___password;
		int32_t L_4 = ___keyStorageFlags;
		VirtActionInvoker3< ByteU5BU5D_t1_109*, String_t*, int32_t >::Invoke(28 /* System.Void System.Security.Cryptography.X509Certificates.X509Certificate::Import(System.Byte[],System.String,System.Security.Cryptography.X509Certificates.X509KeyStorageFlags) */, __this, L_2, L_3, L_4);
		return;
	}
}
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate::Import(System.String,System.Security.SecureString,System.Security.Cryptography.X509Certificates.X509KeyStorageFlags)
extern "C" void X509Certificate_Import_m1_10155 (X509Certificate_t1_1179 * __this, String_t* ___fileName, SecureString_t1_1197 * ___password, int32_t ___keyStorageFlags, const MethodInfo* method)
{
	ByteU5BU5D_t1_109* V_0 = {0};
	{
		String_t* L_0 = ___fileName;
		ByteU5BU5D_t1_109* L_1 = X509Certificate_Load_m1_10141(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		ByteU5BU5D_t1_109* L_2 = V_0;
		int32_t L_3 = ___keyStorageFlags;
		VirtActionInvoker3< ByteU5BU5D_t1_109*, String_t*, int32_t >::Invoke(28 /* System.Void System.Security.Cryptography.X509Certificates.X509Certificate::Import(System.Byte[],System.String,System.Security.Cryptography.X509Certificates.X509KeyStorageFlags) */, __this, L_2, (String_t*)NULL, L_3);
		return;
	}
}
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate::Reset()
extern "C" void X509Certificate_Reset_m1_10156 (X509Certificate_t1_1179 * __this, const MethodInfo* method)
{
	{
		__this->___x509_0 = (X509Certificate_t1_151 *)NULL;
		__this->___issuer_name_3 = (String_t*)NULL;
		__this->___subject_name_4 = (String_t*)NULL;
		__this->___hideDates_1 = 0;
		__this->___cachedCertificateHash_2 = (ByteU5BU5D_t1_109*)NULL;
		return;
	}
}
// System.Void System.Security.Cryptography.AsymmetricAlgorithm::.ctor()
extern "C" void AsymmetricAlgorithm__ctor_m1_10157 (AsymmetricAlgorithm_t1_228 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.Cryptography.AsymmetricAlgorithm::System.IDisposable.Dispose()
extern "C" void AsymmetricAlgorithm_System_IDisposable_Dispose_m1_10158 (AsymmetricAlgorithm_t1_228 * __this, const MethodInfo* method)
{
	{
		VirtActionInvoker1< bool >::Invoke(10 /* System.Void System.Security.Cryptography.AsymmetricAlgorithm::Dispose(System.Boolean) */, __this, 1);
		GC_SuppressFinalize_m1_14113(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 System.Security.Cryptography.AsymmetricAlgorithm::get_KeySize()
extern "C" int32_t AsymmetricAlgorithm_get_KeySize_m1_10159 (AsymmetricAlgorithm_t1_228 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___KeySizeValue_0);
		return L_0;
	}
}
// System.Void System.Security.Cryptography.AsymmetricAlgorithm::set_KeySize(System.Int32)
extern TypeInfo* CryptographicException_t1_1194_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2548;
extern "C" void AsymmetricAlgorithm_set_KeySize_m1_10160 (AsymmetricAlgorithm_t1_228 * __this, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CryptographicException_t1_1194_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(197);
		_stringLiteral2548 = il2cpp_codegen_string_literal_from_index(2548);
		s_Il2CppMethodIntialized = true;
	}
	{
		KeySizesU5BU5D_t1_1182* L_0 = (__this->___LegalKeySizesValue_1);
		int32_t L_1 = ___value;
		bool L_2 = KeySizes_IsLegalKeySize_m1_10398(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		String_t* L_3 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2548, /*hidden argument*/NULL);
		CryptographicException_t1_1194 * L_4 = (CryptographicException_t1_1194 *)il2cpp_codegen_object_new (CryptographicException_t1_1194_il2cpp_TypeInfo_var);
		CryptographicException__ctor_m1_10222(L_4, L_3, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
	}

IL_0021:
	{
		int32_t L_5 = ___value;
		__this->___KeySizeValue_0 = L_5;
		return;
	}
}
// System.Security.Cryptography.KeySizes[] System.Security.Cryptography.AsymmetricAlgorithm::get_LegalKeySizes()
extern "C" KeySizesU5BU5D_t1_1182* AsymmetricAlgorithm_get_LegalKeySizes_m1_10161 (AsymmetricAlgorithm_t1_228 * __this, const MethodInfo* method)
{
	{
		KeySizesU5BU5D_t1_1182* L_0 = (__this->___LegalKeySizesValue_1);
		return L_0;
	}
}
// System.Void System.Security.Cryptography.AsymmetricAlgorithm::Clear()
extern "C" void AsymmetricAlgorithm_Clear_m1_10162 (AsymmetricAlgorithm_t1_228 * __this, const MethodInfo* method)
{
	{
		VirtActionInvoker1< bool >::Invoke(10 /* System.Void System.Security.Cryptography.AsymmetricAlgorithm::Dispose(System.Boolean) */, __this, 0);
		return;
	}
}
// System.Security.Cryptography.AsymmetricAlgorithm System.Security.Cryptography.AsymmetricAlgorithm::Create()
extern Il2CppCodeGenString* _stringLiteral2549;
extern "C" AsymmetricAlgorithm_t1_228 * AsymmetricAlgorithm_Create_m1_10163 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral2549 = il2cpp_codegen_string_literal_from_index(2549);
		s_Il2CppMethodIntialized = true;
	}
	{
		AsymmetricAlgorithm_t1_228 * L_0 = AsymmetricAlgorithm_Create_m1_10164(NULL /*static, unused*/, _stringLiteral2549, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Security.Cryptography.AsymmetricAlgorithm System.Security.Cryptography.AsymmetricAlgorithm::Create(System.String)
extern TypeInfo* CryptoConfig_t1_1191_il2cpp_TypeInfo_var;
extern TypeInfo* AsymmetricAlgorithm_t1_228_il2cpp_TypeInfo_var;
extern "C" AsymmetricAlgorithm_t1_228 * AsymmetricAlgorithm_Create_m1_10164 (Object_t * __this /* static, unused */, String_t* ___algName, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CryptoConfig_t1_1191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(191);
		AsymmetricAlgorithm_t1_228_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(870);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___algName;
		IL2CPP_RUNTIME_CLASS_INIT(CryptoConfig_t1_1191_il2cpp_TypeInfo_var);
		Object_t * L_1 = CryptoConfig_CreateFromName_m1_10198(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return ((AsymmetricAlgorithm_t1_228 *)CastclassClass(L_1, AsymmetricAlgorithm_t1_228_il2cpp_TypeInfo_var));
	}
}
// System.Byte[] System.Security.Cryptography.AsymmetricAlgorithm::GetNamedParam(System.String,System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Convert_t1_1522_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2550;
extern Il2CppCodeGenString* _stringLiteral2551;
extern Il2CppCodeGenString* _stringLiteral2552;
extern "C" ByteU5BU5D_t1_109* AsymmetricAlgorithm_GetNamedParam_m1_10165 (Object_t * __this /* static, unused */, String_t* ___xml, String_t* ___param, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Convert_t1_1522_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		_stringLiteral2550 = il2cpp_codegen_string_literal_from_index(2550);
		_stringLiteral2551 = il2cpp_codegen_string_literal_from_index(2551);
		_stringLiteral2552 = il2cpp_codegen_string_literal_from_index(2552);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	int32_t V_1 = 0;
	String_t* V_2 = {0};
	int32_t V_3 = 0;
	String_t* V_4 = {0};
	{
		String_t* L_0 = ___param;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m1_560(NULL /*static, unused*/, _stringLiteral2550, L_0, _stringLiteral2551, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = ___xml;
		String_t* L_3 = V_0;
		NullCheck(L_2);
		int32_t L_4 = String_IndexOf_m1_505(L_2, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		int32_t L_5 = V_1;
		if ((!(((uint32_t)L_5) == ((uint32_t)(-1)))))
		{
			goto IL_0022;
		}
	}
	{
		return (ByteU5BU5D_t1_109*)NULL;
	}

IL_0022:
	{
		String_t* L_6 = ___param;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m1_560(NULL /*static, unused*/, _stringLiteral2552, L_6, _stringLiteral2551, /*hidden argument*/NULL);
		V_2 = L_7;
		String_t* L_8 = ___xml;
		String_t* L_9 = V_2;
		NullCheck(L_8);
		int32_t L_10 = String_IndexOf_m1_505(L_8, L_9, /*hidden argument*/NULL);
		V_3 = L_10;
		int32_t L_11 = V_3;
		if ((((int32_t)L_11) == ((int32_t)(-1))))
		{
			goto IL_0049;
		}
	}
	{
		int32_t L_12 = V_3;
		int32_t L_13 = V_1;
		if ((((int32_t)L_12) > ((int32_t)L_13)))
		{
			goto IL_004b;
		}
	}

IL_0049:
	{
		return (ByteU5BU5D_t1_109*)NULL;
	}

IL_004b:
	{
		int32_t L_14 = V_1;
		String_t* L_15 = V_0;
		NullCheck(L_15);
		int32_t L_16 = String_get_Length_m1_571(L_15, /*hidden argument*/NULL);
		V_1 = ((int32_t)((int32_t)L_14+(int32_t)L_16));
		String_t* L_17 = ___xml;
		int32_t L_18 = V_1;
		int32_t L_19 = V_3;
		int32_t L_20 = V_1;
		NullCheck(L_17);
		String_t* L_21 = String_Substring_m1_455(L_17, L_18, ((int32_t)((int32_t)L_19-(int32_t)L_20)), /*hidden argument*/NULL);
		V_4 = L_21;
		String_t* L_22 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1_1522_il2cpp_TypeInfo_var);
		ByteU5BU5D_t1_109* L_23 = Convert_FromBase64String_m1_13441(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		return L_23;
	}
}
// System.Void System.Security.Cryptography.AsymmetricKeyExchangeDeformatter::.ctor()
extern "C" void AsymmetricKeyExchangeDeformatter__ctor_m1_10166 (AsymmetricKeyExchangeDeformatter_t1_1183 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.Cryptography.AsymmetricKeyExchangeFormatter::.ctor()
extern "C" void AsymmetricKeyExchangeFormatter__ctor_m1_10167 (AsymmetricKeyExchangeFormatter_t1_1184 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.Cryptography.AsymmetricSignatureDeformatter::.ctor()
extern "C" void AsymmetricSignatureDeformatter__ctor_m1_10168 (AsymmetricSignatureDeformatter_t1_1185 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.Security.Cryptography.AsymmetricSignatureDeformatter::VerifySignature(System.Security.Cryptography.HashAlgorithm,System.Byte[])
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2553;
extern "C" bool AsymmetricSignatureDeformatter_VerifySignature_m1_10169 (AsymmetricSignatureDeformatter_t1_1185 * __this, HashAlgorithm_t1_162 * ___hash, ByteU5BU5D_t1_109* ___rgbSignature, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		_stringLiteral2553 = il2cpp_codegen_string_literal_from_index(2553);
		s_Il2CppMethodIntialized = true;
	}
	{
		HashAlgorithm_t1_162 * L_0 = ___hash;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, _stringLiteral2553, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		HashAlgorithm_t1_162 * L_2 = ___hash;
		NullCheck(L_2);
		String_t* L_3 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		VirtActionInvoker1< String_t* >::Invoke(4 /* System.Void System.Security.Cryptography.AsymmetricSignatureDeformatter::SetHashAlgorithm(System.String) */, __this, L_3);
		HashAlgorithm_t1_162 * L_4 = ___hash;
		NullCheck(L_4);
		ByteU5BU5D_t1_109* L_5 = (ByteU5BU5D_t1_109*)VirtFuncInvoker0< ByteU5BU5D_t1_109* >::Invoke(13 /* System.Byte[] System.Security.Cryptography.HashAlgorithm::get_Hash() */, L_4);
		ByteU5BU5D_t1_109* L_6 = ___rgbSignature;
		bool L_7 = (bool)VirtFuncInvoker2< bool, ByteU5BU5D_t1_109*, ByteU5BU5D_t1_109* >::Invoke(6 /* System.Boolean System.Security.Cryptography.AsymmetricSignatureDeformatter::VerifySignature(System.Byte[],System.Byte[]) */, __this, L_5, L_6);
		return L_7;
	}
}
// System.Void System.Security.Cryptography.AsymmetricSignatureFormatter::.ctor()
extern "C" void AsymmetricSignatureFormatter__ctor_m1_10170 (AsymmetricSignatureFormatter_t1_1186 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Byte[] System.Security.Cryptography.AsymmetricSignatureFormatter::CreateSignature(System.Security.Cryptography.HashAlgorithm)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2553;
extern "C" ByteU5BU5D_t1_109* AsymmetricSignatureFormatter_CreateSignature_m1_10171 (AsymmetricSignatureFormatter_t1_1186 * __this, HashAlgorithm_t1_162 * ___hash, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		_stringLiteral2553 = il2cpp_codegen_string_literal_from_index(2553);
		s_Il2CppMethodIntialized = true;
	}
	{
		HashAlgorithm_t1_162 * L_0 = ___hash;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, _stringLiteral2553, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		HashAlgorithm_t1_162 * L_2 = ___hash;
		NullCheck(L_2);
		String_t* L_3 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		VirtActionInvoker1< String_t* >::Invoke(4 /* System.Void System.Security.Cryptography.AsymmetricSignatureFormatter::SetHashAlgorithm(System.String) */, __this, L_3);
		HashAlgorithm_t1_162 * L_4 = ___hash;
		NullCheck(L_4);
		ByteU5BU5D_t1_109* L_5 = (ByteU5BU5D_t1_109*)VirtFuncInvoker0< ByteU5BU5D_t1_109* >::Invoke(13 /* System.Byte[] System.Security.Cryptography.HashAlgorithm::get_Hash() */, L_4);
		ByteU5BU5D_t1_109* L_6 = (ByteU5BU5D_t1_109*)VirtFuncInvoker1< ByteU5BU5D_t1_109*, ByteU5BU5D_t1_109* >::Invoke(6 /* System.Byte[] System.Security.Cryptography.AsymmetricSignatureFormatter::CreateSignature(System.Byte[]) */, __this, L_5);
		return L_6;
	}
}
// System.Void System.Security.Cryptography.Base64Constants::.cctor()
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern TypeInfo* Base64Constants_t1_1187_il2cpp_TypeInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t1_1661____U24U24fieldU2D33_33_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t1_1661____U24U24fieldU2D34_34_FieldInfo_var;
extern "C" void Base64Constants__cctor_m1_10172 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		Base64Constants_t1_1187_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(871);
		U3CPrivateImplementationDetailsU3E_t1_1661____U24U24fieldU2D33_33_FieldInfo_var = il2cpp_codegen_field_info_from_index(32, 33);
		U3CPrivateImplementationDetailsU3E_t1_1661____U24U24fieldU2D34_34_FieldInfo_var = il2cpp_codegen_field_info_from_index(32, 34);
		s_Il2CppMethodIntialized = true;
	}
	{
		ByteU5BU5D_t1_109* L_0 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, ((int32_t)64)));
		RuntimeHelpers_InitializeArray_m1_1387(NULL /*static, unused*/, (Array_t *)(Array_t *)L_0, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t1_1661____U24U24fieldU2D33_33_FieldInfo_var), /*hidden argument*/NULL);
		((Base64Constants_t1_1187_StaticFields*)Base64Constants_t1_1187_il2cpp_TypeInfo_var->static_fields)->___EncodeTable_0 = L_0;
		ByteU5BU5D_t1_109* L_1 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, ((int32_t)123)));
		RuntimeHelpers_InitializeArray_m1_1387(NULL /*static, unused*/, (Array_t *)(Array_t *)L_1, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t1_1661____U24U24fieldU2D34_34_FieldInfo_var), /*hidden argument*/NULL);
		((Base64Constants_t1_1187_StaticFields*)Base64Constants_t1_1187_il2cpp_TypeInfo_var->static_fields)->___DecodeTable_1 = L_1;
		return;
	}
}
// System.Void System.Security.Cryptography.CryptoAPITransform::.ctor()
extern "C" void CryptoAPITransform__ctor_m1_10173 (CryptoAPITransform_t1_1189 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		__this->___m_disposed_0 = 0;
		return;
	}
}
// System.Void System.Security.Cryptography.CryptoAPITransform::System.IDisposable.Dispose()
extern "C" void CryptoAPITransform_System_IDisposable_Dispose_m1_10174 (CryptoAPITransform_t1_1189 * __this, const MethodInfo* method)
{
	{
		CryptoAPITransform_Dispose_m1_10181(__this, 1, /*hidden argument*/NULL);
		GC_SuppressFinalize_m1_14113(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.Security.Cryptography.CryptoAPITransform::get_CanReuseTransform()
extern "C" bool CryptoAPITransform_get_CanReuseTransform_m1_10175 (CryptoAPITransform_t1_1189 * __this, const MethodInfo* method)
{
	{
		return 1;
	}
}
// System.Boolean System.Security.Cryptography.CryptoAPITransform::get_CanTransformMultipleBlocks()
extern "C" bool CryptoAPITransform_get_CanTransformMultipleBlocks_m1_10176 (CryptoAPITransform_t1_1189 * __this, const MethodInfo* method)
{
	{
		return 1;
	}
}
// System.Int32 System.Security.Cryptography.CryptoAPITransform::get_InputBlockSize()
extern "C" int32_t CryptoAPITransform_get_InputBlockSize_m1_10177 (CryptoAPITransform_t1_1189 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.IntPtr System.Security.Cryptography.CryptoAPITransform::get_KeyHandle()
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" IntPtr_t CryptoAPITransform_get_KeyHandle_m1_10178 (CryptoAPITransform_t1_1189 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		return L_0;
	}
}
// System.Int32 System.Security.Cryptography.CryptoAPITransform::get_OutputBlockSize()
extern "C" int32_t CryptoAPITransform_get_OutputBlockSize_m1_10179 (CryptoAPITransform_t1_1189 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Void System.Security.Cryptography.CryptoAPITransform::Clear()
extern "C" void CryptoAPITransform_Clear_m1_10180 (CryptoAPITransform_t1_1189 * __this, const MethodInfo* method)
{
	{
		CryptoAPITransform_Dispose_m1_10181(__this, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.Cryptography.CryptoAPITransform::Dispose(System.Boolean)
extern "C" void CryptoAPITransform_Dispose_m1_10181 (CryptoAPITransform_t1_1189 * __this, bool ___disposing, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_disposed_0);
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		bool L_1 = ___disposing;
		if (!L_1)
		{
			goto IL_0011;
		}
	}

IL_0011:
	{
		__this->___m_disposed_0 = 1;
	}

IL_0018:
	{
		return;
	}
}
// System.Int32 System.Security.Cryptography.CryptoAPITransform::TransformBlock(System.Byte[],System.Int32,System.Int32,System.Byte[],System.Int32)
extern "C" int32_t CryptoAPITransform_TransformBlock_m1_10182 (CryptoAPITransform_t1_1189 * __this, ByteU5BU5D_t1_109* ___inputBuffer, int32_t ___inputOffset, int32_t ___inputCount, ByteU5BU5D_t1_109* ___outputBuffer, int32_t ___outputOffset, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Byte[] System.Security.Cryptography.CryptoAPITransform::TransformFinalBlock(System.Byte[],System.Int32,System.Int32)
extern "C" ByteU5BU5D_t1_109* CryptoAPITransform_TransformFinalBlock_m1_10183 (CryptoAPITransform_t1_1189 * __this, ByteU5BU5D_t1_109* ___inputBuffer, int32_t ___inputOffset, int32_t ___inputCount, const MethodInfo* method)
{
	{
		return (ByteU5BU5D_t1_109*)NULL;
	}
}
// System.Void System.Security.Cryptography.CryptoAPITransform::Reset()
extern "C" void CryptoAPITransform_Reset_m1_10184 (CryptoAPITransform_t1_1189 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void System.Security.Cryptography.CryptoConfig/CryptoHandler::.ctor(System.Collections.Hashtable,System.Collections.Hashtable)
extern TypeInfo* Hashtable_t1_100_il2cpp_TypeInfo_var;
extern "C" void CryptoHandler__ctor_m1_10185 (CryptoHandler_t1_1190 * __this, Hashtable_t1_100 * ___algorithms, Hashtable_t1_100 * ___oid, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Hashtable_t1_100_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(119);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		Hashtable_t1_100 * L_0 = ___algorithms;
		__this->___algorithms_0 = L_0;
		Hashtable_t1_100 * L_1 = ___oid;
		__this->___oid_1 = L_1;
		Hashtable_t1_100 * L_2 = (Hashtable_t1_100 *)il2cpp_codegen_object_new (Hashtable_t1_100_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1_3279(L_2, /*hidden argument*/NULL);
		__this->___names_2 = L_2;
		Hashtable_t1_100 * L_3 = (Hashtable_t1_100 *)il2cpp_codegen_object_new (Hashtable_t1_100_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1_3279(L_3, /*hidden argument*/NULL);
		__this->___classnames_3 = L_3;
		return;
	}
}
// System.Void System.Security.Cryptography.CryptoConfig/CryptoHandler::OnStartParsing(Mono.Xml.SmallXmlParser)
extern "C" void CryptoHandler_OnStartParsing_m1_10186 (CryptoHandler_t1_1190 * __this, SmallXmlParser_t1_241 * ___parser, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void System.Security.Cryptography.CryptoConfig/CryptoHandler::OnEndParsing(Mono.Xml.SmallXmlParser)
extern TypeInfo* IEnumerator_t1_193_il2cpp_TypeInfo_var;
extern TypeInfo* DictionaryEntry_t1_284_il2cpp_TypeInfo_var;
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern "C" void CryptoHandler_OnEndParsing_m1_10187 (CryptoHandler_t1_1190 * __this, SmallXmlParser_t1_241 * ___parser, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IEnumerator_t1_193_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(144);
		DictionaryEntry_t1_284_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(148);
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		s_Il2CppMethodIntialized = true;
	}
	DictionaryEntry_t1_284  V_0 = {0};
	Object_t * V_1 = {0};
	Object_t * V_2 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Hashtable_t1_100 * L_0 = (__this->___names_2);
		NullCheck(L_0);
		Object_t * L_1 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(36 /* System.Collections.IDictionaryEnumerator System.Collections.Hashtable::GetEnumerator() */, L_0);
		V_1 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_004c;
		}

IL_0011:
		{
			Object_t * L_2 = V_1;
			NullCheck(L_2);
			Object_t * L_3 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_2);
			V_0 = ((*(DictionaryEntry_t1_284 *)((DictionaryEntry_t1_284 *)UnBox (L_3, DictionaryEntry_t1_284_il2cpp_TypeInfo_var))));
		}

IL_001d:
		try
		{ // begin try (depth: 2)
			Hashtable_t1_100 * L_4 = (__this->___algorithms_0);
			Object_t * L_5 = DictionaryEntry_get_Key_m1_3229((&V_0), /*hidden argument*/NULL);
			Hashtable_t1_100 * L_6 = (__this->___classnames_3);
			Object_t * L_7 = DictionaryEntry_get_Value_m1_3231((&V_0), /*hidden argument*/NULL);
			NullCheck(L_6);
			Object_t * L_8 = (Object_t *)VirtFuncInvoker1< Object_t *, Object_t * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_6, L_7);
			NullCheck(L_4);
			VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_4, L_5, L_8);
			goto IL_004c;
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t1_33 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (Object_t_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_0046;
			throw e;
		}

CATCH_0046:
		{ // begin catch(System.Object)
			goto IL_004c;
		} // end catch (depth: 2)

IL_004c:
		{
			Object_t * L_9 = V_1;
			NullCheck(L_9);
			bool L_10 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_9);
			if (L_10)
			{
				goto IL_0011;
			}
		}

IL_0057:
		{
			IL2CPP_LEAVE(0x6E, FINALLY_005c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_005c;
	}

FINALLY_005c:
	{ // begin finally (depth: 1)
		{
			Object_t * L_11 = V_1;
			V_2 = ((Object_t *)IsInst(L_11, IDisposable_t1_1035_il2cpp_TypeInfo_var));
			Object_t * L_12 = V_2;
			if (L_12)
			{
				goto IL_0067;
			}
		}

IL_0066:
		{
			IL2CPP_END_FINALLY(92)
		}

IL_0067:
		{
			Object_t * L_13 = V_2;
			NullCheck(L_13);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_13);
			IL2CPP_END_FINALLY(92)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(92)
	{
		IL2CPP_JUMP_TBL(0x6E, IL_006e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_006e:
	{
		Hashtable_t1_100 * L_14 = (__this->___names_2);
		NullCheck(L_14);
		VirtActionInvoker0::Invoke(34 /* System.Void System.Collections.Hashtable::Clear() */, L_14);
		Hashtable_t1_100 * L_15 = (__this->___classnames_3);
		NullCheck(L_15);
		VirtActionInvoker0::Invoke(34 /* System.Void System.Collections.Hashtable::Clear() */, L_15);
		return;
	}
}
// System.String System.Security.Cryptography.CryptoConfig/CryptoHandler::Get(Mono.Xml.SmallXmlParser/IAttrList,System.String)
extern TypeInfo* IAttrList_t1_1676_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" String_t* CryptoHandler_Get_m1_10188 (CryptoHandler_t1_1190 * __this, Object_t * ___attrs, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IAttrList_t1_1676_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(274);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0027;
	}

IL_0007:
	{
		Object_t * L_0 = ___attrs;
		NullCheck(L_0);
		StringU5BU5D_t1_238* L_1 = (StringU5BU5D_t1_238*)InterfaceFuncInvoker0< StringU5BU5D_t1_238* >::Invoke(5 /* System.String[] Mono.Xml.SmallXmlParser/IAttrList::get_Names() */, IAttrList_t1_1676_il2cpp_TypeInfo_var, L_0);
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		String_t* L_4 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_op_Equality_m1_601(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_1, L_3, sizeof(String_t*))), L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0023;
		}
	}
	{
		Object_t * L_6 = ___attrs;
		NullCheck(L_6);
		StringU5BU5D_t1_238* L_7 = (StringU5BU5D_t1_238*)InterfaceFuncInvoker0< StringU5BU5D_t1_238* >::Invoke(6 /* System.String[] Mono.Xml.SmallXmlParser/IAttrList::get_Values() */, IAttrList_t1_1676_il2cpp_TypeInfo_var, L_6);
		int32_t L_8 = V_0;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		return (*(String_t**)(String_t**)SZArrayLdElema(L_7, L_9, sizeof(String_t*)));
	}

IL_0023:
	{
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0027:
	{
		int32_t L_11 = V_0;
		Object_t * L_12 = ___attrs;
		NullCheck(L_12);
		StringU5BU5D_t1_238* L_13 = (StringU5BU5D_t1_238*)InterfaceFuncInvoker0< StringU5BU5D_t1_238* >::Invoke(5 /* System.String[] Mono.Xml.SmallXmlParser/IAttrList::get_Names() */, IAttrList_t1_1676_il2cpp_TypeInfo_var, L_12);
		NullCheck(L_13);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_13)->max_length)))))))
		{
			goto IL_0007;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		return L_14;
	}
}
// System.Void System.Security.Cryptography.CryptoConfig/CryptoHandler::OnStartElement(System.String,Mono.Xml.SmallXmlParser/IAttrList)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* IAttrList_t1_1676_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral706;
extern Il2CppCodeGenString* _stringLiteral2667;
extern Il2CppCodeGenString* _stringLiteral2668;
extern Il2CppCodeGenString* _stringLiteral2669;
extern Il2CppCodeGenString* _stringLiteral2670;
extern Il2CppCodeGenString* _stringLiteral2671;
extern Il2CppCodeGenString* _stringLiteral230;
extern Il2CppCodeGenString* _stringLiteral2672;
extern Il2CppCodeGenString* _stringLiteral2673;
extern Il2CppCodeGenString* _stringLiteral2674;
extern Il2CppCodeGenString* _stringLiteral2675;
extern Il2CppCodeGenString* _stringLiteral2676;
extern "C" void CryptoHandler_OnStartElement_m1_10189 (CryptoHandler_t1_1190 * __this, String_t* ___name, Object_t * ___attrs, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		IAttrList_t1_1676_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(274);
		_stringLiteral706 = il2cpp_codegen_string_literal_from_index(706);
		_stringLiteral2667 = il2cpp_codegen_string_literal_from_index(2667);
		_stringLiteral2668 = il2cpp_codegen_string_literal_from_index(2668);
		_stringLiteral2669 = il2cpp_codegen_string_literal_from_index(2669);
		_stringLiteral2670 = il2cpp_codegen_string_literal_from_index(2670);
		_stringLiteral2671 = il2cpp_codegen_string_literal_from_index(2671);
		_stringLiteral230 = il2cpp_codegen_string_literal_from_index(230);
		_stringLiteral2672 = il2cpp_codegen_string_literal_from_index(2672);
		_stringLiteral2673 = il2cpp_codegen_string_literal_from_index(2673);
		_stringLiteral2674 = il2cpp_codegen_string_literal_from_index(2674);
		_stringLiteral2675 = il2cpp_codegen_string_literal_from_index(2675);
		_stringLiteral2676 = il2cpp_codegen_string_literal_from_index(2676);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = (__this->___level_4);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_002a;
		}
		if (L_1 == 1)
		{
			goto IL_004d;
		}
		if (L_1 == 2)
		{
			goto IL_0070;
		}
		if (L_1 == 3)
		{
			goto IL_0093;
		}
		if (L_1 == 4)
		{
			goto IL_00d9;
		}
		if (L_1 == 5)
		{
			goto IL_016c;
		}
	}
	{
		goto IL_019c;
	}

IL_002a:
	{
		String_t* L_2 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m1_601(NULL /*static, unused*/, L_2, _stringLiteral706, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0048;
		}
	}
	{
		int32_t L_4 = (__this->___level_4);
		__this->___level_4 = ((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_0048:
	{
		goto IL_019c;
	}

IL_004d:
	{
		String_t* L_5 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_6 = String_op_Equality_m1_601(NULL /*static, unused*/, L_5, _stringLiteral2667, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_7 = (__this->___level_4);
		__this->___level_4 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_006b:
	{
		goto IL_019c;
	}

IL_0070:
	{
		String_t* L_8 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_9 = String_op_Equality_m1_601(NULL /*static, unused*/, L_8, _stringLiteral2668, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_008e;
		}
	}
	{
		int32_t L_10 = (__this->___level_4);
		__this->___level_4 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_008e:
	{
		goto IL_019c;
	}

IL_0093:
	{
		String_t* L_11 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_12 = String_op_Equality_m1_601(NULL /*static, unused*/, L_11, _stringLiteral2669, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_00b6;
		}
	}
	{
		int32_t L_13 = (__this->___level_4);
		__this->___level_4 = ((int32_t)((int32_t)L_13+(int32_t)1));
		goto IL_00d4;
	}

IL_00b6:
	{
		String_t* L_14 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_15 = String_op_Equality_m1_601(NULL /*static, unused*/, L_14, _stringLiteral2670, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_00d4;
		}
	}
	{
		int32_t L_16 = (__this->___level_4);
		__this->___level_4 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_00d4:
	{
		goto IL_019c;
	}

IL_00d9:
	{
		String_t* L_17 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_18 = String_op_Equality_m1_601(NULL /*static, unused*/, L_17, _stringLiteral2671, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_0111;
		}
	}
	{
		Hashtable_t1_100 * L_19 = (__this->___oid_1);
		Object_t * L_20 = ___attrs;
		String_t* L_21 = CryptoHandler_Get_m1_10188(__this, L_20, _stringLiteral230, /*hidden argument*/NULL);
		Object_t * L_22 = ___attrs;
		String_t* L_23 = CryptoHandler_Get_m1_10188(__this, L_22, _stringLiteral2672, /*hidden argument*/NULL);
		NullCheck(L_19);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_19, L_21, L_23);
		goto IL_0167;
	}

IL_0111:
	{
		String_t* L_24 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_25 = String_op_Equality_m1_601(NULL /*static, unused*/, L_24, _stringLiteral2673, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_0149;
		}
	}
	{
		Hashtable_t1_100 * L_26 = (__this->___names_2);
		Object_t * L_27 = ___attrs;
		String_t* L_28 = CryptoHandler_Get_m1_10188(__this, L_27, _stringLiteral230, /*hidden argument*/NULL);
		Object_t * L_29 = ___attrs;
		String_t* L_30 = CryptoHandler_Get_m1_10188(__this, L_29, _stringLiteral2674, /*hidden argument*/NULL);
		NullCheck(L_26);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_26, L_28, L_30);
		goto IL_0167;
	}

IL_0149:
	{
		String_t* L_31 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_32 = String_op_Equality_m1_601(NULL /*static, unused*/, L_31, _stringLiteral2675, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_0167;
		}
	}
	{
		int32_t L_33 = (__this->___level_4);
		__this->___level_4 = ((int32_t)((int32_t)L_33+(int32_t)1));
	}

IL_0167:
	{
		goto IL_019c;
	}

IL_016c:
	{
		String_t* L_34 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_35 = String_op_Equality_m1_601(NULL /*static, unused*/, L_34, _stringLiteral2676, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_0197;
		}
	}
	{
		Hashtable_t1_100 * L_36 = (__this->___classnames_3);
		Object_t * L_37 = ___attrs;
		NullCheck(L_37);
		StringU5BU5D_t1_238* L_38 = (StringU5BU5D_t1_238*)InterfaceFuncInvoker0< StringU5BU5D_t1_238* >::Invoke(5 /* System.String[] Mono.Xml.SmallXmlParser/IAttrList::get_Names() */, IAttrList_t1_1676_il2cpp_TypeInfo_var, L_37);
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 0);
		int32_t L_39 = 0;
		Object_t * L_40 = ___attrs;
		NullCheck(L_40);
		StringU5BU5D_t1_238* L_41 = (StringU5BU5D_t1_238*)InterfaceFuncInvoker0< StringU5BU5D_t1_238* >::Invoke(6 /* System.String[] Mono.Xml.SmallXmlParser/IAttrList::get_Values() */, IAttrList_t1_1676_il2cpp_TypeInfo_var, L_40);
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, 0);
		int32_t L_42 = 0;
		NullCheck(L_36);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_36, (*(String_t**)(String_t**)SZArrayLdElema(L_38, L_39, sizeof(String_t*))), (*(String_t**)(String_t**)SZArrayLdElema(L_41, L_42, sizeof(String_t*))));
	}

IL_0197:
	{
		goto IL_019c;
	}

IL_019c:
	{
		return;
	}
}
// System.Void System.Security.Cryptography.CryptoConfig/CryptoHandler::OnEndElement(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral706;
extern Il2CppCodeGenString* _stringLiteral2667;
extern Il2CppCodeGenString* _stringLiteral2668;
extern Il2CppCodeGenString* _stringLiteral2669;
extern Il2CppCodeGenString* _stringLiteral2670;
extern Il2CppCodeGenString* _stringLiteral2675;
extern "C" void CryptoHandler_OnEndElement_m1_10190 (CryptoHandler_t1_1190 * __this, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral706 = il2cpp_codegen_string_literal_from_index(706);
		_stringLiteral2667 = il2cpp_codegen_string_literal_from_index(2667);
		_stringLiteral2668 = il2cpp_codegen_string_literal_from_index(2668);
		_stringLiteral2669 = il2cpp_codegen_string_literal_from_index(2669);
		_stringLiteral2670 = il2cpp_codegen_string_literal_from_index(2670);
		_stringLiteral2675 = il2cpp_codegen_string_literal_from_index(2675);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = (__this->___level_4);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 0)
		{
			goto IL_0028;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 1)
		{
			goto IL_004b;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 2)
		{
			goto IL_006e;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 3)
		{
			goto IL_0091;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 4)
		{
			goto IL_00c4;
		}
	}
	{
		goto IL_00e7;
	}

IL_0028:
	{
		String_t* L_2 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m1_601(NULL /*static, unused*/, L_2, _stringLiteral706, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0046;
		}
	}
	{
		int32_t L_4 = (__this->___level_4);
		__this->___level_4 = ((int32_t)((int32_t)L_4-(int32_t)1));
	}

IL_0046:
	{
		goto IL_00e7;
	}

IL_004b:
	{
		String_t* L_5 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_6 = String_op_Equality_m1_601(NULL /*static, unused*/, L_5, _stringLiteral2667, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0069;
		}
	}
	{
		int32_t L_7 = (__this->___level_4);
		__this->___level_4 = ((int32_t)((int32_t)L_7-(int32_t)1));
	}

IL_0069:
	{
		goto IL_00e7;
	}

IL_006e:
	{
		String_t* L_8 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_9 = String_op_Equality_m1_601(NULL /*static, unused*/, L_8, _stringLiteral2668, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_10 = (__this->___level_4);
		__this->___level_4 = ((int32_t)((int32_t)L_10-(int32_t)1));
	}

IL_008c:
	{
		goto IL_00e7;
	}

IL_0091:
	{
		String_t* L_11 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_12 = String_op_Equality_m1_601(NULL /*static, unused*/, L_11, _stringLiteral2669, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_00b1;
		}
	}
	{
		String_t* L_13 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_14 = String_op_Equality_m1_601(NULL /*static, unused*/, L_13, _stringLiteral2670, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_00bf;
		}
	}

IL_00b1:
	{
		int32_t L_15 = (__this->___level_4);
		__this->___level_4 = ((int32_t)((int32_t)L_15-(int32_t)1));
	}

IL_00bf:
	{
		goto IL_00e7;
	}

IL_00c4:
	{
		String_t* L_16 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_17 = String_op_Equality_m1_601(NULL /*static, unused*/, L_16, _stringLiteral2675, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_00e2;
		}
	}
	{
		int32_t L_18 = (__this->___level_4);
		__this->___level_4 = ((int32_t)((int32_t)L_18-(int32_t)1));
	}

IL_00e2:
	{
		goto IL_00e7;
	}

IL_00e7:
	{
		return;
	}
}
// System.Void System.Security.Cryptography.CryptoConfig/CryptoHandler::OnProcessingInstruction(System.String,System.String)
extern "C" void CryptoHandler_OnProcessingInstruction_m1_10191 (CryptoHandler_t1_1190 * __this, String_t* ___name, String_t* ___text, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void System.Security.Cryptography.CryptoConfig/CryptoHandler::OnChars(System.String)
extern "C" void CryptoHandler_OnChars_m1_10192 (CryptoHandler_t1_1190 * __this, String_t* ___text, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void System.Security.Cryptography.CryptoConfig/CryptoHandler::OnIgnorableWhitespace(System.String)
extern "C" void CryptoHandler_OnIgnorableWhitespace_m1_10193 (CryptoHandler_t1_1190 * __this, String_t* ___text, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void System.Security.Cryptography.CryptoConfig::.ctor()
extern "C" void CryptoConfig__ctor_m1_10194 (CryptoConfig_t1_1191 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.Cryptography.CryptoConfig::.cctor()
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern TypeInfo* CryptoConfig_t1_1191_il2cpp_TypeInfo_var;
extern "C" void CryptoConfig__cctor_m1_10195 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		CryptoConfig_t1_1191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(191);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = (Object_t *)il2cpp_codegen_object_new (Object_t_il2cpp_TypeInfo_var);
		Object__ctor_m1_0(L_0, /*hidden argument*/NULL);
		((CryptoConfig_t1_1191_StaticFields*)CryptoConfig_t1_1191_il2cpp_TypeInfo_var->static_fields)->___lockObject_136 = L_0;
		return;
	}
}
// System.Void System.Security.Cryptography.CryptoConfig::Initialize()
extern TypeInfo* CaseInsensitiveHashCodeProvider_t1_278_il2cpp_TypeInfo_var;
extern TypeInfo* CaseInsensitiveComparer_t1_276_il2cpp_TypeInfo_var;
extern TypeInfo* Hashtable_t1_100_il2cpp_TypeInfo_var;
extern TypeInfo* CryptoConfig_t1_1191_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2554;
extern Il2CppCodeGenString* _stringLiteral2555;
extern Il2CppCodeGenString* _stringLiteral375;
extern Il2CppCodeGenString* _stringLiteral2556;
extern Il2CppCodeGenString* _stringLiteral2557;
extern Il2CppCodeGenString* _stringLiteral374;
extern Il2CppCodeGenString* _stringLiteral2558;
extern Il2CppCodeGenString* _stringLiteral2559;
extern Il2CppCodeGenString* _stringLiteral699;
extern Il2CppCodeGenString* _stringLiteral2560;
extern Il2CppCodeGenString* _stringLiteral2561;
extern Il2CppCodeGenString* _stringLiteral2562;
extern Il2CppCodeGenString* _stringLiteral700;
extern Il2CppCodeGenString* _stringLiteral2563;
extern Il2CppCodeGenString* _stringLiteral2564;
extern Il2CppCodeGenString* _stringLiteral2565;
extern Il2CppCodeGenString* _stringLiteral701;
extern Il2CppCodeGenString* _stringLiteral2566;
extern Il2CppCodeGenString* _stringLiteral2567;
extern Il2CppCodeGenString* _stringLiteral2568;
extern Il2CppCodeGenString* _stringLiteral2569;
extern Il2CppCodeGenString* _stringLiteral2570;
extern Il2CppCodeGenString* _stringLiteral2571;
extern Il2CppCodeGenString* _stringLiteral2549;
extern Il2CppCodeGenString* _stringLiteral2572;
extern Il2CppCodeGenString* _stringLiteral2573;
extern Il2CppCodeGenString* _stringLiteral2574;
extern Il2CppCodeGenString* _stringLiteral544;
extern Il2CppCodeGenString* _stringLiteral2575;
extern Il2CppCodeGenString* _stringLiteral2576;
extern Il2CppCodeGenString* _stringLiteral2577;
extern Il2CppCodeGenString* _stringLiteral2578;
extern Il2CppCodeGenString* _stringLiteral547;
extern Il2CppCodeGenString* _stringLiteral2579;
extern Il2CppCodeGenString* _stringLiteral2580;
extern Il2CppCodeGenString* _stringLiteral545;
extern Il2CppCodeGenString* _stringLiteral2581;
extern Il2CppCodeGenString* _stringLiteral2582;
extern Il2CppCodeGenString* _stringLiteral2583;
extern Il2CppCodeGenString* _stringLiteral2584;
extern Il2CppCodeGenString* _stringLiteral2585;
extern Il2CppCodeGenString* _stringLiteral2586;
extern Il2CppCodeGenString* _stringLiteral2587;
extern Il2CppCodeGenString* _stringLiteral2588;
extern Il2CppCodeGenString* _stringLiteral2589;
extern Il2CppCodeGenString* _stringLiteral2590;
extern Il2CppCodeGenString* _stringLiteral2591;
extern Il2CppCodeGenString* _stringLiteral2592;
extern Il2CppCodeGenString* _stringLiteral2593;
extern Il2CppCodeGenString* _stringLiteral2594;
extern Il2CppCodeGenString* _stringLiteral2595;
extern Il2CppCodeGenString* _stringLiteral2596;
extern Il2CppCodeGenString* _stringLiteral2597;
extern Il2CppCodeGenString* _stringLiteral2598;
extern Il2CppCodeGenString* _stringLiteral2599;
extern Il2CppCodeGenString* _stringLiteral2600;
extern Il2CppCodeGenString* _stringLiteral2601;
extern Il2CppCodeGenString* _stringLiteral2602;
extern Il2CppCodeGenString* _stringLiteral2603;
extern Il2CppCodeGenString* _stringLiteral2604;
extern Il2CppCodeGenString* _stringLiteral2605;
extern Il2CppCodeGenString* _stringLiteral2606;
extern Il2CppCodeGenString* _stringLiteral2607;
extern Il2CppCodeGenString* _stringLiteral2608;
extern Il2CppCodeGenString* _stringLiteral2609;
extern Il2CppCodeGenString* _stringLiteral393;
extern Il2CppCodeGenString* _stringLiteral2610;
extern Il2CppCodeGenString* _stringLiteral446;
extern Il2CppCodeGenString* _stringLiteral2611;
extern Il2CppCodeGenString* _stringLiteral2612;
extern Il2CppCodeGenString* _stringLiteral2613;
extern Il2CppCodeGenString* _stringLiteral2614;
extern Il2CppCodeGenString* _stringLiteral2615;
extern Il2CppCodeGenString* _stringLiteral2616;
extern Il2CppCodeGenString* _stringLiteral2617;
extern Il2CppCodeGenString* _stringLiteral2618;
extern Il2CppCodeGenString* _stringLiteral2619;
extern Il2CppCodeGenString* _stringLiteral2620;
extern Il2CppCodeGenString* _stringLiteral2621;
extern Il2CppCodeGenString* _stringLiteral2622;
extern Il2CppCodeGenString* _stringLiteral2623;
extern Il2CppCodeGenString* _stringLiteral2624;
extern Il2CppCodeGenString* _stringLiteral2625;
extern Il2CppCodeGenString* _stringLiteral2626;
extern Il2CppCodeGenString* _stringLiteral2627;
extern Il2CppCodeGenString* _stringLiteral2628;
extern Il2CppCodeGenString* _stringLiteral2629;
extern Il2CppCodeGenString* _stringLiteral2630;
extern Il2CppCodeGenString* _stringLiteral2631;
extern Il2CppCodeGenString* _stringLiteral2632;
extern Il2CppCodeGenString* _stringLiteral2633;
extern Il2CppCodeGenString* _stringLiteral2634;
extern Il2CppCodeGenString* _stringLiteral2635;
extern Il2CppCodeGenString* _stringLiteral2636;
extern Il2CppCodeGenString* _stringLiteral2637;
extern Il2CppCodeGenString* _stringLiteral2638;
extern Il2CppCodeGenString* _stringLiteral2639;
extern Il2CppCodeGenString* _stringLiteral2640;
extern Il2CppCodeGenString* _stringLiteral2641;
extern Il2CppCodeGenString* _stringLiteral2642;
extern Il2CppCodeGenString* _stringLiteral2643;
extern Il2CppCodeGenString* _stringLiteral2644;
extern Il2CppCodeGenString* _stringLiteral2645;
extern Il2CppCodeGenString* _stringLiteral2646;
extern Il2CppCodeGenString* _stringLiteral511;
extern Il2CppCodeGenString* _stringLiteral2647;
extern Il2CppCodeGenString* _stringLiteral497;
extern Il2CppCodeGenString* _stringLiteral2648;
extern Il2CppCodeGenString* _stringLiteral488;
extern Il2CppCodeGenString* _stringLiteral2649;
extern Il2CppCodeGenString* _stringLiteral2650;
extern Il2CppCodeGenString* _stringLiteral2651;
extern Il2CppCodeGenString* _stringLiteral2652;
extern Il2CppCodeGenString* _stringLiteral2653;
extern Il2CppCodeGenString* _stringLiteral518;
extern Il2CppCodeGenString* _stringLiteral2654;
extern Il2CppCodeGenString* _stringLiteral695;
extern Il2CppCodeGenString* _stringLiteral2655;
extern Il2CppCodeGenString* _stringLiteral2656;
extern Il2CppCodeGenString* _stringLiteral2657;
extern Il2CppCodeGenString* _stringLiteral2658;
extern Il2CppCodeGenString* _stringLiteral2659;
extern Il2CppCodeGenString* _stringLiteral2660;
extern Il2CppCodeGenString* _stringLiteral2661;
extern Il2CppCodeGenString* _stringLiteral2662;
extern "C" void CryptoConfig_Initialize_m1_10196 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CaseInsensitiveHashCodeProvider_t1_278_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(140);
		CaseInsensitiveComparer_t1_276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(141);
		Hashtable_t1_100_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(119);
		CryptoConfig_t1_1191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(191);
		_stringLiteral2554 = il2cpp_codegen_string_literal_from_index(2554);
		_stringLiteral2555 = il2cpp_codegen_string_literal_from_index(2555);
		_stringLiteral375 = il2cpp_codegen_string_literal_from_index(375);
		_stringLiteral2556 = il2cpp_codegen_string_literal_from_index(2556);
		_stringLiteral2557 = il2cpp_codegen_string_literal_from_index(2557);
		_stringLiteral374 = il2cpp_codegen_string_literal_from_index(374);
		_stringLiteral2558 = il2cpp_codegen_string_literal_from_index(2558);
		_stringLiteral2559 = il2cpp_codegen_string_literal_from_index(2559);
		_stringLiteral699 = il2cpp_codegen_string_literal_from_index(699);
		_stringLiteral2560 = il2cpp_codegen_string_literal_from_index(2560);
		_stringLiteral2561 = il2cpp_codegen_string_literal_from_index(2561);
		_stringLiteral2562 = il2cpp_codegen_string_literal_from_index(2562);
		_stringLiteral700 = il2cpp_codegen_string_literal_from_index(700);
		_stringLiteral2563 = il2cpp_codegen_string_literal_from_index(2563);
		_stringLiteral2564 = il2cpp_codegen_string_literal_from_index(2564);
		_stringLiteral2565 = il2cpp_codegen_string_literal_from_index(2565);
		_stringLiteral701 = il2cpp_codegen_string_literal_from_index(701);
		_stringLiteral2566 = il2cpp_codegen_string_literal_from_index(2566);
		_stringLiteral2567 = il2cpp_codegen_string_literal_from_index(2567);
		_stringLiteral2568 = il2cpp_codegen_string_literal_from_index(2568);
		_stringLiteral2569 = il2cpp_codegen_string_literal_from_index(2569);
		_stringLiteral2570 = il2cpp_codegen_string_literal_from_index(2570);
		_stringLiteral2571 = il2cpp_codegen_string_literal_from_index(2571);
		_stringLiteral2549 = il2cpp_codegen_string_literal_from_index(2549);
		_stringLiteral2572 = il2cpp_codegen_string_literal_from_index(2572);
		_stringLiteral2573 = il2cpp_codegen_string_literal_from_index(2573);
		_stringLiteral2574 = il2cpp_codegen_string_literal_from_index(2574);
		_stringLiteral544 = il2cpp_codegen_string_literal_from_index(544);
		_stringLiteral2575 = il2cpp_codegen_string_literal_from_index(2575);
		_stringLiteral2576 = il2cpp_codegen_string_literal_from_index(2576);
		_stringLiteral2577 = il2cpp_codegen_string_literal_from_index(2577);
		_stringLiteral2578 = il2cpp_codegen_string_literal_from_index(2578);
		_stringLiteral547 = il2cpp_codegen_string_literal_from_index(547);
		_stringLiteral2579 = il2cpp_codegen_string_literal_from_index(2579);
		_stringLiteral2580 = il2cpp_codegen_string_literal_from_index(2580);
		_stringLiteral545 = il2cpp_codegen_string_literal_from_index(545);
		_stringLiteral2581 = il2cpp_codegen_string_literal_from_index(2581);
		_stringLiteral2582 = il2cpp_codegen_string_literal_from_index(2582);
		_stringLiteral2583 = il2cpp_codegen_string_literal_from_index(2583);
		_stringLiteral2584 = il2cpp_codegen_string_literal_from_index(2584);
		_stringLiteral2585 = il2cpp_codegen_string_literal_from_index(2585);
		_stringLiteral2586 = il2cpp_codegen_string_literal_from_index(2586);
		_stringLiteral2587 = il2cpp_codegen_string_literal_from_index(2587);
		_stringLiteral2588 = il2cpp_codegen_string_literal_from_index(2588);
		_stringLiteral2589 = il2cpp_codegen_string_literal_from_index(2589);
		_stringLiteral2590 = il2cpp_codegen_string_literal_from_index(2590);
		_stringLiteral2591 = il2cpp_codegen_string_literal_from_index(2591);
		_stringLiteral2592 = il2cpp_codegen_string_literal_from_index(2592);
		_stringLiteral2593 = il2cpp_codegen_string_literal_from_index(2593);
		_stringLiteral2594 = il2cpp_codegen_string_literal_from_index(2594);
		_stringLiteral2595 = il2cpp_codegen_string_literal_from_index(2595);
		_stringLiteral2596 = il2cpp_codegen_string_literal_from_index(2596);
		_stringLiteral2597 = il2cpp_codegen_string_literal_from_index(2597);
		_stringLiteral2598 = il2cpp_codegen_string_literal_from_index(2598);
		_stringLiteral2599 = il2cpp_codegen_string_literal_from_index(2599);
		_stringLiteral2600 = il2cpp_codegen_string_literal_from_index(2600);
		_stringLiteral2601 = il2cpp_codegen_string_literal_from_index(2601);
		_stringLiteral2602 = il2cpp_codegen_string_literal_from_index(2602);
		_stringLiteral2603 = il2cpp_codegen_string_literal_from_index(2603);
		_stringLiteral2604 = il2cpp_codegen_string_literal_from_index(2604);
		_stringLiteral2605 = il2cpp_codegen_string_literal_from_index(2605);
		_stringLiteral2606 = il2cpp_codegen_string_literal_from_index(2606);
		_stringLiteral2607 = il2cpp_codegen_string_literal_from_index(2607);
		_stringLiteral2608 = il2cpp_codegen_string_literal_from_index(2608);
		_stringLiteral2609 = il2cpp_codegen_string_literal_from_index(2609);
		_stringLiteral393 = il2cpp_codegen_string_literal_from_index(393);
		_stringLiteral2610 = il2cpp_codegen_string_literal_from_index(2610);
		_stringLiteral446 = il2cpp_codegen_string_literal_from_index(446);
		_stringLiteral2611 = il2cpp_codegen_string_literal_from_index(2611);
		_stringLiteral2612 = il2cpp_codegen_string_literal_from_index(2612);
		_stringLiteral2613 = il2cpp_codegen_string_literal_from_index(2613);
		_stringLiteral2614 = il2cpp_codegen_string_literal_from_index(2614);
		_stringLiteral2615 = il2cpp_codegen_string_literal_from_index(2615);
		_stringLiteral2616 = il2cpp_codegen_string_literal_from_index(2616);
		_stringLiteral2617 = il2cpp_codegen_string_literal_from_index(2617);
		_stringLiteral2618 = il2cpp_codegen_string_literal_from_index(2618);
		_stringLiteral2619 = il2cpp_codegen_string_literal_from_index(2619);
		_stringLiteral2620 = il2cpp_codegen_string_literal_from_index(2620);
		_stringLiteral2621 = il2cpp_codegen_string_literal_from_index(2621);
		_stringLiteral2622 = il2cpp_codegen_string_literal_from_index(2622);
		_stringLiteral2623 = il2cpp_codegen_string_literal_from_index(2623);
		_stringLiteral2624 = il2cpp_codegen_string_literal_from_index(2624);
		_stringLiteral2625 = il2cpp_codegen_string_literal_from_index(2625);
		_stringLiteral2626 = il2cpp_codegen_string_literal_from_index(2626);
		_stringLiteral2627 = il2cpp_codegen_string_literal_from_index(2627);
		_stringLiteral2628 = il2cpp_codegen_string_literal_from_index(2628);
		_stringLiteral2629 = il2cpp_codegen_string_literal_from_index(2629);
		_stringLiteral2630 = il2cpp_codegen_string_literal_from_index(2630);
		_stringLiteral2631 = il2cpp_codegen_string_literal_from_index(2631);
		_stringLiteral2632 = il2cpp_codegen_string_literal_from_index(2632);
		_stringLiteral2633 = il2cpp_codegen_string_literal_from_index(2633);
		_stringLiteral2634 = il2cpp_codegen_string_literal_from_index(2634);
		_stringLiteral2635 = il2cpp_codegen_string_literal_from_index(2635);
		_stringLiteral2636 = il2cpp_codegen_string_literal_from_index(2636);
		_stringLiteral2637 = il2cpp_codegen_string_literal_from_index(2637);
		_stringLiteral2638 = il2cpp_codegen_string_literal_from_index(2638);
		_stringLiteral2639 = il2cpp_codegen_string_literal_from_index(2639);
		_stringLiteral2640 = il2cpp_codegen_string_literal_from_index(2640);
		_stringLiteral2641 = il2cpp_codegen_string_literal_from_index(2641);
		_stringLiteral2642 = il2cpp_codegen_string_literal_from_index(2642);
		_stringLiteral2643 = il2cpp_codegen_string_literal_from_index(2643);
		_stringLiteral2644 = il2cpp_codegen_string_literal_from_index(2644);
		_stringLiteral2645 = il2cpp_codegen_string_literal_from_index(2645);
		_stringLiteral2646 = il2cpp_codegen_string_literal_from_index(2646);
		_stringLiteral511 = il2cpp_codegen_string_literal_from_index(511);
		_stringLiteral2647 = il2cpp_codegen_string_literal_from_index(2647);
		_stringLiteral497 = il2cpp_codegen_string_literal_from_index(497);
		_stringLiteral2648 = il2cpp_codegen_string_literal_from_index(2648);
		_stringLiteral488 = il2cpp_codegen_string_literal_from_index(488);
		_stringLiteral2649 = il2cpp_codegen_string_literal_from_index(2649);
		_stringLiteral2650 = il2cpp_codegen_string_literal_from_index(2650);
		_stringLiteral2651 = il2cpp_codegen_string_literal_from_index(2651);
		_stringLiteral2652 = il2cpp_codegen_string_literal_from_index(2652);
		_stringLiteral2653 = il2cpp_codegen_string_literal_from_index(2653);
		_stringLiteral518 = il2cpp_codegen_string_literal_from_index(518);
		_stringLiteral2654 = il2cpp_codegen_string_literal_from_index(2654);
		_stringLiteral695 = il2cpp_codegen_string_literal_from_index(695);
		_stringLiteral2655 = il2cpp_codegen_string_literal_from_index(2655);
		_stringLiteral2656 = il2cpp_codegen_string_literal_from_index(2656);
		_stringLiteral2657 = il2cpp_codegen_string_literal_from_index(2657);
		_stringLiteral2658 = il2cpp_codegen_string_literal_from_index(2658);
		_stringLiteral2659 = il2cpp_codegen_string_literal_from_index(2659);
		_stringLiteral2660 = il2cpp_codegen_string_literal_from_index(2660);
		_stringLiteral2661 = il2cpp_codegen_string_literal_from_index(2661);
		_stringLiteral2662 = il2cpp_codegen_string_literal_from_index(2662);
		s_Il2CppMethodIntialized = true;
	}
	Hashtable_t1_100 * V_0 = {0};
	Hashtable_t1_100 * V_1 = {0};
	{
		CaseInsensitiveHashCodeProvider_t1_278 * L_0 = (CaseInsensitiveHashCodeProvider_t1_278 *)il2cpp_codegen_object_new (CaseInsensitiveHashCodeProvider_t1_278_il2cpp_TypeInfo_var);
		CaseInsensitiveHashCodeProvider__ctor_m1_3152(L_0, /*hidden argument*/NULL);
		CaseInsensitiveComparer_t1_276 * L_1 = (CaseInsensitiveComparer_t1_276 *)il2cpp_codegen_object_new (CaseInsensitiveComparer_t1_276_il2cpp_TypeInfo_var);
		CaseInsensitiveComparer__ctor_m1_3145(L_1, /*hidden argument*/NULL);
		Hashtable_t1_100 * L_2 = (Hashtable_t1_100 *)il2cpp_codegen_object_new (Hashtable_t1_100_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1_3289(L_2, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Hashtable_t1_100 * L_3 = V_0;
		NullCheck(L_3);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_3, _stringLiteral2554, _stringLiteral2555);
		Hashtable_t1_100 * L_4 = V_0;
		NullCheck(L_4);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_4, _stringLiteral375, _stringLiteral2555);
		Hashtable_t1_100 * L_5 = V_0;
		NullCheck(L_5);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_5, _stringLiteral2556, _stringLiteral2555);
		Hashtable_t1_100 * L_6 = V_0;
		NullCheck(L_6);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_6, _stringLiteral2557, _stringLiteral2555);
		Hashtable_t1_100 * L_7 = V_0;
		NullCheck(L_7);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_7, _stringLiteral374, _stringLiteral2558);
		Hashtable_t1_100 * L_8 = V_0;
		NullCheck(L_8);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_8, _stringLiteral2559, _stringLiteral2558);
		Hashtable_t1_100 * L_9 = V_0;
		NullCheck(L_9);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_9, _stringLiteral699, _stringLiteral2560);
		Hashtable_t1_100 * L_10 = V_0;
		NullCheck(L_10);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_10, _stringLiteral2561, _stringLiteral2560);
		Hashtable_t1_100 * L_11 = V_0;
		NullCheck(L_11);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_11, _stringLiteral2562, _stringLiteral2560);
		Hashtable_t1_100 * L_12 = V_0;
		NullCheck(L_12);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_12, _stringLiteral700, _stringLiteral2563);
		Hashtable_t1_100 * L_13 = V_0;
		NullCheck(L_13);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_13, _stringLiteral2564, _stringLiteral2563);
		Hashtable_t1_100 * L_14 = V_0;
		NullCheck(L_14);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_14, _stringLiteral2565, _stringLiteral2563);
		Hashtable_t1_100 * L_15 = V_0;
		NullCheck(L_15);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_15, _stringLiteral701, _stringLiteral2566);
		Hashtable_t1_100 * L_16 = V_0;
		NullCheck(L_16);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_16, _stringLiteral2567, _stringLiteral2566);
		Hashtable_t1_100 * L_17 = V_0;
		NullCheck(L_17);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_17, _stringLiteral2568, _stringLiteral2566);
		Hashtable_t1_100 * L_18 = V_0;
		NullCheck(L_18);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_18, _stringLiteral2569, _stringLiteral2570);
		Hashtable_t1_100 * L_19 = V_0;
		NullCheck(L_19);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_19, _stringLiteral2571, _stringLiteral2570);
		Hashtable_t1_100 * L_20 = V_0;
		NullCheck(L_20);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_20, _stringLiteral2549, _stringLiteral2570);
		Hashtable_t1_100 * L_21 = V_0;
		NullCheck(L_21);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_21, _stringLiteral2572, _stringLiteral2573);
		Hashtable_t1_100 * L_22 = V_0;
		NullCheck(L_22);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_22, _stringLiteral2574, _stringLiteral2573);
		Hashtable_t1_100 * L_23 = V_0;
		NullCheck(L_23);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_23, _stringLiteral544, _stringLiteral2575);
		Hashtable_t1_100 * L_24 = V_0;
		NullCheck(L_24);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_24, _stringLiteral2576, _stringLiteral2575);
		Hashtable_t1_100 * L_25 = V_0;
		NullCheck(L_25);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_25, _stringLiteral2577, _stringLiteral2578);
		Hashtable_t1_100 * L_26 = V_0;
		NullCheck(L_26);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_26, _stringLiteral547, _stringLiteral2578);
		Hashtable_t1_100 * L_27 = V_0;
		NullCheck(L_27);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_27, _stringLiteral2579, _stringLiteral2578);
		Hashtable_t1_100 * L_28 = V_0;
		NullCheck(L_28);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_28, _stringLiteral2580, _stringLiteral2578);
		Hashtable_t1_100 * L_29 = V_0;
		NullCheck(L_29);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_29, _stringLiteral545, _stringLiteral2581);
		Hashtable_t1_100 * L_30 = V_0;
		NullCheck(L_30);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_30, _stringLiteral2582, _stringLiteral2581);
		Hashtable_t1_100 * L_31 = V_0;
		NullCheck(L_31);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_31, _stringLiteral2583, _stringLiteral2584);
		Hashtable_t1_100 * L_32 = V_0;
		NullCheck(L_32);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_32, _stringLiteral2585, _stringLiteral2584);
		Hashtable_t1_100 * L_33 = V_0;
		NullCheck(L_33);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_33, _stringLiteral2586, _stringLiteral2584);
		Hashtable_t1_100 * L_34 = V_0;
		NullCheck(L_34);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_34, _stringLiteral2587, _stringLiteral2588);
		Hashtable_t1_100 * L_35 = V_0;
		NullCheck(L_35);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_35, _stringLiteral2589, _stringLiteral2588);
		Hashtable_t1_100 * L_36 = V_0;
		NullCheck(L_36);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_36, _stringLiteral2590, _stringLiteral2591);
		Hashtable_t1_100 * L_37 = V_0;
		NullCheck(L_37);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_37, _stringLiteral2592, _stringLiteral2591);
		Hashtable_t1_100 * L_38 = V_0;
		NullCheck(L_38);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_38, _stringLiteral2591, _stringLiteral2591);
		Hashtable_t1_100 * L_39 = V_0;
		NullCheck(L_39);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_39, _stringLiteral2593, _stringLiteral2594);
		Hashtable_t1_100 * L_40 = V_0;
		NullCheck(L_40);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_40, _stringLiteral2594, _stringLiteral2594);
		Hashtable_t1_100 * L_41 = V_0;
		NullCheck(L_41);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_41, _stringLiteral2595, _stringLiteral2596);
		Hashtable_t1_100 * L_42 = V_0;
		NullCheck(L_42);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_42, _stringLiteral2597, _stringLiteral2596);
		Hashtable_t1_100 * L_43 = V_0;
		NullCheck(L_43);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_43, _stringLiteral2598, _stringLiteral2596);
		Hashtable_t1_100 * L_44 = V_0;
		NullCheck(L_44);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_44, _stringLiteral2599, _stringLiteral2591);
		Hashtable_t1_100 * L_45 = V_0;
		NullCheck(L_45);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_45, _stringLiteral2600, _stringLiteral2601);
		Hashtable_t1_100 * L_46 = V_0;
		NullCheck(L_46);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_46, _stringLiteral2601, _stringLiteral2601);
		Hashtable_t1_100 * L_47 = V_0;
		NullCheck(L_47);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_47, _stringLiteral2602, _stringLiteral2603);
		Hashtable_t1_100 * L_48 = V_0;
		NullCheck(L_48);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_48, _stringLiteral2603, _stringLiteral2603);
		Hashtable_t1_100 * L_49 = V_0;
		NullCheck(L_49);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_49, _stringLiteral2604, _stringLiteral2605);
		Hashtable_t1_100 * L_50 = V_0;
		NullCheck(L_50);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_50, _stringLiteral2605, _stringLiteral2605);
		Hashtable_t1_100 * L_51 = V_0;
		NullCheck(L_51);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_51, _stringLiteral2606, _stringLiteral2607);
		Hashtable_t1_100 * L_52 = V_0;
		NullCheck(L_52);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_52, _stringLiteral2607, _stringLiteral2607);
		Hashtable_t1_100 * L_53 = V_0;
		NullCheck(L_53);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_53, _stringLiteral2608, _stringLiteral2609);
		Hashtable_t1_100 * L_54 = V_0;
		NullCheck(L_54);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_54, _stringLiteral2609, _stringLiteral2609);
		Hashtable_t1_100 * L_55 = V_0;
		NullCheck(L_55);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_55, _stringLiteral393, _stringLiteral2610);
		Hashtable_t1_100 * L_56 = V_0;
		NullCheck(L_56);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_56, _stringLiteral446, _stringLiteral2611);
		Hashtable_t1_100 * L_57 = V_0;
		NullCheck(L_57);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_57, _stringLiteral2612, _stringLiteral2555);
		Hashtable_t1_100 * L_58 = V_0;
		NullCheck(L_58);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_58, _stringLiteral2613, _stringLiteral2614);
		Hashtable_t1_100 * L_59 = V_0;
		NullCheck(L_59);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_59, _stringLiteral2615, _stringLiteral2616);
		Hashtable_t1_100 * L_60 = V_0;
		NullCheck(L_60);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_60, _stringLiteral2617, _stringLiteral2618);
		Hashtable_t1_100 * L_61 = V_0;
		NullCheck(L_61);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_61, _stringLiteral2619, _stringLiteral2620);
		Hashtable_t1_100 * L_62 = V_0;
		NullCheck(L_62);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_62, _stringLiteral2621, _stringLiteral2622);
		Hashtable_t1_100 * L_63 = V_0;
		NullCheck(L_63);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_63, _stringLiteral2623, _stringLiteral2624);
		Hashtable_t1_100 * L_64 = V_0;
		NullCheck(L_64);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_64, _stringLiteral2625, _stringLiteral2626);
		Hashtable_t1_100 * L_65 = V_0;
		NullCheck(L_65);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_65, _stringLiteral2627, _stringLiteral2628);
		Hashtable_t1_100 * L_66 = V_0;
		NullCheck(L_66);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_66, _stringLiteral2629, _stringLiteral2630);
		Hashtable_t1_100 * L_67 = V_0;
		NullCheck(L_67);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_67, _stringLiteral2631, _stringLiteral2560);
		Hashtable_t1_100 * L_68 = V_0;
		NullCheck(L_68);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_68, _stringLiteral2632, _stringLiteral2566);
		Hashtable_t1_100 * L_69 = V_0;
		NullCheck(L_69);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_69, _stringLiteral2633, _stringLiteral2605);
		Hashtable_t1_100 * L_70 = V_0;
		NullCheck(L_70);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_70, _stringLiteral2634, _stringLiteral2607);
		Hashtable_t1_100 * L_71 = V_0;
		NullCheck(L_71);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_71, _stringLiteral2635, _stringLiteral2609);
		Hashtable_t1_100 * L_72 = V_0;
		NullCheck(L_72);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_72, _stringLiteral2636, _stringLiteral2603);
		Hashtable_t1_100 * L_73 = V_0;
		NullCheck(L_73);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_73, _stringLiteral2637, _stringLiteral2638);
		Hashtable_t1_100 * L_74 = V_0;
		NullCheck(L_74);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_74, _stringLiteral2639, _stringLiteral2640);
		Hashtable_t1_100 * L_75 = V_0;
		NullCheck(L_75);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_75, _stringLiteral2641, _stringLiteral2642);
		Hashtable_t1_100 * L_76 = V_0;
		NullCheck(L_76);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_76, _stringLiteral2643, _stringLiteral2644);
		Hashtable_t1_100 * L_77 = V_0;
		NullCheck(L_77);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_77, _stringLiteral2645, _stringLiteral2646);
		Hashtable_t1_100 * L_78 = V_0;
		NullCheck(L_78);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_78, _stringLiteral511, _stringLiteral2647);
		Hashtable_t1_100 * L_79 = V_0;
		NullCheck(L_79);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_79, _stringLiteral497, _stringLiteral2648);
		Hashtable_t1_100 * L_80 = V_0;
		NullCheck(L_80);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_80, _stringLiteral488, _stringLiteral2649);
		Hashtable_t1_100 * L_81 = V_0;
		NullCheck(L_81);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_81, _stringLiteral2650, _stringLiteral2651);
		Hashtable_t1_100 * L_82 = V_0;
		NullCheck(L_82);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_82, _stringLiteral2652, _stringLiteral2653);
		CaseInsensitiveHashCodeProvider_t1_278 * L_83 = (CaseInsensitiveHashCodeProvider_t1_278 *)il2cpp_codegen_object_new (CaseInsensitiveHashCodeProvider_t1_278_il2cpp_TypeInfo_var);
		CaseInsensitiveHashCodeProvider__ctor_m1_3152(L_83, /*hidden argument*/NULL);
		CaseInsensitiveComparer_t1_276 * L_84 = (CaseInsensitiveComparer_t1_276 *)il2cpp_codegen_object_new (CaseInsensitiveComparer_t1_276_il2cpp_TypeInfo_var);
		CaseInsensitiveComparer__ctor_m1_3145(L_84, /*hidden argument*/NULL);
		Hashtable_t1_100 * L_85 = (Hashtable_t1_100 *)il2cpp_codegen_object_new (Hashtable_t1_100_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1_3289(L_85, L_83, L_84, /*hidden argument*/NULL);
		V_1 = L_85;
		Hashtable_t1_100 * L_86 = V_1;
		NullCheck(L_86);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_86, _stringLiteral2555, _stringLiteral518);
		Hashtable_t1_100 * L_87 = V_1;
		NullCheck(L_87);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_87, _stringLiteral2654, _stringLiteral518);
		Hashtable_t1_100 * L_88 = V_1;
		NullCheck(L_88);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_88, _stringLiteral375, _stringLiteral518);
		Hashtable_t1_100 * L_89 = V_1;
		NullCheck(L_89);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_89, _stringLiteral2556, _stringLiteral518);
		Hashtable_t1_100 * L_90 = V_1;
		NullCheck(L_90);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_90, _stringLiteral2558, _stringLiteral695);
		Hashtable_t1_100 * L_91 = V_1;
		NullCheck(L_91);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_91, _stringLiteral374, _stringLiteral695);
		Hashtable_t1_100 * L_92 = V_1;
		NullCheck(L_92);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_92, _stringLiteral2559, _stringLiteral695);
		Hashtable_t1_100 * L_93 = V_1;
		NullCheck(L_93);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_93, _stringLiteral2560, _stringLiteral2655);
		Hashtable_t1_100 * L_94 = V_1;
		NullCheck(L_94);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_94, _stringLiteral699, _stringLiteral2655);
		Hashtable_t1_100 * L_95 = V_1;
		NullCheck(L_95);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_95, _stringLiteral2562, _stringLiteral2655);
		Hashtable_t1_100 * L_96 = V_1;
		NullCheck(L_96);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_96, _stringLiteral2563, _stringLiteral2656);
		Hashtable_t1_100 * L_97 = V_1;
		NullCheck(L_97);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_97, _stringLiteral700, _stringLiteral2656);
		Hashtable_t1_100 * L_98 = V_1;
		NullCheck(L_98);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_98, _stringLiteral2565, _stringLiteral2656);
		Hashtable_t1_100 * L_99 = V_1;
		NullCheck(L_99);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_99, _stringLiteral2566, _stringLiteral2657);
		Hashtable_t1_100 * L_100 = V_1;
		NullCheck(L_100);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_100, _stringLiteral701, _stringLiteral2657);
		Hashtable_t1_100 * L_101 = V_1;
		NullCheck(L_101);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_101, _stringLiteral2568, _stringLiteral2657);
		Hashtable_t1_100 * L_102 = V_1;
		NullCheck(L_102);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_102, _stringLiteral2658, _stringLiteral2659);
		Hashtable_t1_100 * L_103 = V_1;
		NullCheck(L_103);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_103, _stringLiteral544, _stringLiteral2660);
		Hashtable_t1_100 * L_104 = V_1;
		NullCheck(L_104);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_104, _stringLiteral547, _stringLiteral2661);
		Hashtable_t1_100 * L_105 = V_1;
		NullCheck(L_105);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_105, _stringLiteral545, _stringLiteral2662);
		Hashtable_t1_100 * L_106 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(CryptoConfig_t1_1191_il2cpp_TypeInfo_var);
		((CryptoConfig_t1_1191_StaticFields*)CryptoConfig_t1_1191_il2cpp_TypeInfo_var->static_fields)->___algorithms_137 = L_106;
		Hashtable_t1_100 * L_107 = V_1;
		((CryptoConfig_t1_1191_StaticFields*)CryptoConfig_t1_1191_il2cpp_TypeInfo_var->static_fields)->___oid_138 = L_107;
		return;
	}
}
// System.Void System.Security.Cryptography.CryptoConfig::LoadConfig(System.String,System.Collections.Hashtable,System.Collections.Hashtable)
extern TypeInfo* StreamReader_t1_447_il2cpp_TypeInfo_var;
extern TypeInfo* CryptoHandler_t1_1190_il2cpp_TypeInfo_var;
extern TypeInfo* SmallXmlParser_t1_241_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern "C" void CryptoConfig_LoadConfig_m1_10197 (Object_t * __this /* static, unused */, String_t* ___filename, Hashtable_t1_100 * ___algorithms, Hashtable_t1_100 * ___oid, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StreamReader_t1_447_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(142);
		CryptoHandler_t1_1190_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(872);
		SmallXmlParser_t1_241_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(278);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		s_Il2CppMethodIntialized = true;
	}
	TextReader_t1_246 * V_0 = {0};
	CryptoHandler_t1_1190 * V_1 = {0};
	SmallXmlParser_t1_241 * V_2 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___filename;
		bool L_1 = File_Exists_m1_4857(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			String_t* L_2 = ___filename;
			StreamReader_t1_447 * L_3 = (StreamReader_t1_447 *)il2cpp_codegen_object_new (StreamReader_t1_447_il2cpp_TypeInfo_var);
			StreamReader__ctor_m1_5215(L_3, L_2, /*hidden argument*/NULL);
			V_0 = L_3;
		}

IL_0013:
		try
		{ // begin try (depth: 2)
			Hashtable_t1_100 * L_4 = ___algorithms;
			Hashtable_t1_100 * L_5 = ___oid;
			CryptoHandler_t1_1190 * L_6 = (CryptoHandler_t1_1190 *)il2cpp_codegen_object_new (CryptoHandler_t1_1190_il2cpp_TypeInfo_var);
			CryptoHandler__ctor_m1_10185(L_6, L_4, L_5, /*hidden argument*/NULL);
			V_1 = L_6;
			SmallXmlParser_t1_241 * L_7 = (SmallXmlParser_t1_241 *)il2cpp_codegen_object_new (SmallXmlParser_t1_241_il2cpp_TypeInfo_var);
			SmallXmlParser__ctor_m1_2694(L_7, /*hidden argument*/NULL);
			V_2 = L_7;
			SmallXmlParser_t1_241 * L_8 = V_2;
			TextReader_t1_246 * L_9 = V_0;
			CryptoHandler_t1_1190 * L_10 = V_1;
			NullCheck(L_8);
			SmallXmlParser_Parse_m1_2707(L_8, L_9, L_10, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x3B, FINALLY_002e);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1_33 *)e.ex;
			goto FINALLY_002e;
		}

FINALLY_002e:
		{ // begin finally (depth: 2)
			{
				TextReader_t1_246 * L_11 = V_0;
				if (!L_11)
				{
					goto IL_003a;
				}
			}

IL_0034:
			{
				TextReader_t1_246 * L_12 = V_0;
				NullCheck(L_12);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_12);
			}

IL_003a:
			{
				IL2CPP_END_FINALLY(46)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(46)
		{
			IL2CPP_JUMP_TBL(0x3B, IL_003b)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
		}

IL_003b:
		{
			goto IL_0046;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1_33 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Object_t_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0040;
		throw e;
	}

CATCH_0040:
	{ // begin catch(System.Object)
		goto IL_0046;
	} // end catch (depth: 1)

IL_0046:
	{
		return;
	}
}
// System.Object System.Security.Cryptography.CryptoConfig::CreateFromName(System.String)
extern TypeInfo* CryptoConfig_t1_1191_il2cpp_TypeInfo_var;
extern "C" Object_t * CryptoConfig_CreateFromName_m1_10198 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CryptoConfig_t1_1191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(191);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(CryptoConfig_t1_1191_il2cpp_TypeInfo_var);
		Object_t * L_1 = CryptoConfig_CreateFromName_m1_10199(NULL /*static, unused*/, L_0, (ObjectU5BU5D_t1_272*)(ObjectU5BU5D_t1_272*)NULL, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Object System.Security.Cryptography.CryptoConfig::CreateFromName(System.String,System.Object[])
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* CryptoConfig_t1_1191_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral230;
extern "C" Object_t * CryptoConfig_CreateFromName_m1_10199 (Object_t * __this /* static, unused */, String_t* ___name, ObjectU5BU5D_t1_272* ___args, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		CryptoConfig_t1_1191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(191);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		_stringLiteral230 = il2cpp_codegen_string_literal_from_index(230);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	Type_t * V_1 = {0};
	String_t* V_2 = {0};
	Object_t * V_3 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___name;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, _stringLiteral230, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CryptoConfig_t1_1191_il2cpp_TypeInfo_var);
		Object_t * L_2 = ((CryptoConfig_t1_1191_StaticFields*)CryptoConfig_t1_1191_il2cpp_TypeInfo_var->static_fields)->___lockObject_136;
		V_0 = L_2;
		Object_t * L_3 = V_0;
		Monitor_Enter_m1_12697(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_001d:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(CryptoConfig_t1_1191_il2cpp_TypeInfo_var);
			Hashtable_t1_100 * L_4 = ((CryptoConfig_t1_1191_StaticFields*)CryptoConfig_t1_1191_il2cpp_TypeInfo_var->static_fields)->___algorithms_137;
			if (L_4)
			{
				goto IL_002c;
			}
		}

IL_0027:
		{
			IL2CPP_RUNTIME_CLASS_INIT(CryptoConfig_t1_1191_il2cpp_TypeInfo_var);
			CryptoConfig_Initialize_m1_10196(NULL /*static, unused*/, /*hidden argument*/NULL);
		}

IL_002c:
		{
			IL2CPP_LEAVE(0x38, FINALLY_0031);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0031;
	}

FINALLY_0031:
	{ // begin finally (depth: 1)
		Object_t * L_5 = V_0;
		Monitor_Exit_m1_12698(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(49)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(49)
	{
		IL2CPP_JUMP_TBL(0x38, IL_0038)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0038:
	try
	{ // begin try (depth: 1)
		{
			V_1 = (Type_t *)NULL;
			IL2CPP_RUNTIME_CLASS_INIT(CryptoConfig_t1_1191_il2cpp_TypeInfo_var);
			Hashtable_t1_100 * L_6 = ((CryptoConfig_t1_1191_StaticFields*)CryptoConfig_t1_1191_il2cpp_TypeInfo_var->static_fields)->___algorithms_137;
			String_t* L_7 = ___name;
			NullCheck(L_6);
			Object_t * L_8 = (Object_t *)VirtFuncInvoker1< Object_t *, Object_t * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_6, L_7);
			V_2 = ((String_t*)CastclassSealed(L_8, String_t_il2cpp_TypeInfo_var));
			String_t* L_9 = V_2;
			if (L_9)
			{
				goto IL_0053;
			}
		}

IL_0051:
		{
			String_t* L_10 = ___name;
			V_2 = L_10;
		}

IL_0053:
		{
			String_t* L_11 = V_2;
			IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
			Type_t * L_12 = il2cpp_codegen_get_type((methodPointerType)&Type_GetType_m1_1128, L_11, "mscorlib, Version=2.0.5.0, Culture=neutral, PublicKeyToken=7cec85d7bea7798e");
			V_1 = L_12;
			Type_t * L_13 = V_1;
			ObjectU5BU5D_t1_272* L_14 = ___args;
			Object_t * L_15 = Activator_CreateInstance_m1_13035(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
			V_3 = L_15;
			goto IL_0079;
		}

IL_0067:
		{
			; // IL_0067: leave IL_0079
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1_33 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Object_t_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_006c;
		throw e;
	}

CATCH_006c:
	{ // begin catch(System.Object)
		{
			V_3 = NULL;
			goto IL_0079;
		}

IL_0074:
		{
			; // IL_0074: leave IL_0079
		}
	} // end catch (depth: 1)

IL_0079:
	{
		Object_t * L_16 = V_3;
		return L_16;
	}
}
// System.String System.Security.Cryptography.CryptoConfig::MapNameToOID(System.String)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* CryptoConfig_t1_1191_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral230;
extern "C" String_t* CryptoConfig_MapNameToOID_m1_10200 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		CryptoConfig_t1_1191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(191);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral230 = il2cpp_codegen_string_literal_from_index(230);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___name;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, _stringLiteral230, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CryptoConfig_t1_1191_il2cpp_TypeInfo_var);
		Object_t * L_2 = ((CryptoConfig_t1_1191_StaticFields*)CryptoConfig_t1_1191_il2cpp_TypeInfo_var->static_fields)->___lockObject_136;
		V_0 = L_2;
		Object_t * L_3 = V_0;
		Monitor_Enter_m1_12697(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_001d:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(CryptoConfig_t1_1191_il2cpp_TypeInfo_var);
			Hashtable_t1_100 * L_4 = ((CryptoConfig_t1_1191_StaticFields*)CryptoConfig_t1_1191_il2cpp_TypeInfo_var->static_fields)->___oid_138;
			if (L_4)
			{
				goto IL_002c;
			}
		}

IL_0027:
		{
			IL2CPP_RUNTIME_CLASS_INIT(CryptoConfig_t1_1191_il2cpp_TypeInfo_var);
			CryptoConfig_Initialize_m1_10196(NULL /*static, unused*/, /*hidden argument*/NULL);
		}

IL_002c:
		{
			IL2CPP_LEAVE(0x38, FINALLY_0031);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0031;
	}

FINALLY_0031:
	{ // begin finally (depth: 1)
		Object_t * L_5 = V_0;
		Monitor_Exit_m1_12698(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(49)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(49)
	{
		IL2CPP_JUMP_TBL(0x38, IL_0038)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0038:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CryptoConfig_t1_1191_il2cpp_TypeInfo_var);
		Hashtable_t1_100 * L_6 = ((CryptoConfig_t1_1191_StaticFields*)CryptoConfig_t1_1191_il2cpp_TypeInfo_var->static_fields)->___oid_138;
		String_t* L_7 = ___name;
		NullCheck(L_6);
		Object_t * L_8 = (Object_t *)VirtFuncInvoker1< Object_t *, Object_t * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_6, L_7);
		return ((String_t*)CastclassSealed(L_8, String_t_il2cpp_TypeInfo_var));
	}
}
// System.Byte[] System.Security.Cryptography.CryptoConfig::EncodeOID(System.String)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t1_16_il2cpp_TypeInfo_var;
extern TypeInfo* CryptographicUnexpectedOperationException_t1_1195_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern TypeInfo* Convert_t1_1522_il2cpp_TypeInfo_var;
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern TypeInfo* CryptoConfig_t1_1191_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral81;
extern Il2CppCodeGenString* _stringLiteral2663;
extern Il2CppCodeGenString* _stringLiteral2664;
extern Il2CppCodeGenString* _stringLiteral2665;
extern "C" ByteU5BU5D_t1_109* CryptoConfig_EncodeOID_m1_10201 (Object_t * __this /* static, unused */, String_t* ___str, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		CharU5BU5D_t1_16_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(15);
		CryptographicUnexpectedOperationException_t1_1195_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(873);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		Convert_t1_1522_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		CryptoConfig_t1_1191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(191);
		_stringLiteral81 = il2cpp_codegen_string_literal_from_index(81);
		_stringLiteral2663 = il2cpp_codegen_string_literal_from_index(2663);
		_stringLiteral2664 = il2cpp_codegen_string_literal_from_index(2664);
		_stringLiteral2665 = il2cpp_codegen_string_literal_from_index(2665);
		s_Il2CppMethodIntialized = true;
	}
	CharU5BU5D_t1_16* V_0 = {0};
	StringU5BU5D_t1_238* V_1 = {0};
	ByteU5BU5D_t1_109* V_2 = {0};
	uint8_t V_3 = 0x0;
	uint8_t V_4 = 0x0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int64_t V_7 = 0;
	ByteU5BU5D_t1_109* V_8 = {0};
	int32_t V_9 = 0;
	ByteU5BU5D_t1_109* V_10 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___str;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, _stringLiteral81, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		CharU5BU5D_t1_16* L_2 = ((CharU5BU5D_t1_16*)SZArrayNew(CharU5BU5D_t1_16_il2cpp_TypeInfo_var, 1));
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_2, 0, sizeof(uint16_t))) = (uint16_t)((int32_t)46);
		V_0 = L_2;
		String_t* L_3 = ___str;
		CharU5BU5D_t1_16* L_4 = V_0;
		NullCheck(L_3);
		StringU5BU5D_t1_238* L_5 = String_Split_m1_448(L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		StringU5BU5D_t1_238* L_6 = V_1;
		NullCheck(L_6);
		if ((((int32_t)(((int32_t)((int32_t)(((Array_t *)L_6)->max_length))))) >= ((int32_t)2)))
		{
			goto IL_003e;
		}
	}
	{
		String_t* L_7 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2663, /*hidden argument*/NULL);
		CryptographicUnexpectedOperationException_t1_1195 * L_8 = (CryptographicUnexpectedOperationException_t1_1195 *)il2cpp_codegen_object_new (CryptographicUnexpectedOperationException_t1_1195_il2cpp_TypeInfo_var);
		CryptographicUnexpectedOperationException__ctor_m1_10227(L_8, L_7, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_8);
	}

IL_003e:
	{
		String_t* L_9 = ___str;
		NullCheck(L_9);
		int32_t L_10 = String_get_Length_m1_571(L_9, /*hidden argument*/NULL);
		V_2 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, L_10));
	}

IL_004a:
	try
	{ // begin try (depth: 1)
		StringU5BU5D_t1_238* L_11 = V_1;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 0);
		int32_t L_12 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1_1522_il2cpp_TypeInfo_var);
		uint8_t L_13 = Convert_ToByte_m1_13480(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_11, L_12, sizeof(String_t*))), /*hidden argument*/NULL);
		V_3 = L_13;
		StringU5BU5D_t1_238* L_14 = V_1;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 1);
		int32_t L_15 = 1;
		uint8_t L_16 = Convert_ToByte_m1_13480(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_14, L_15, sizeof(String_t*))), /*hidden argument*/NULL);
		V_4 = L_16;
		ByteU5BU5D_t1_109* L_17 = V_2;
		uint8_t L_18 = V_3;
		uint8_t L_19 = V_4;
		uint8_t L_20 = Convert_ToByte_m1_13476(NULL /*static, unused*/, ((int32_t)((int32_t)((int32_t)((int32_t)L_18*(int32_t)((int32_t)40)))+(int32_t)L_19)), /*hidden argument*/NULL);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 2);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_17, 2, sizeof(uint8_t))) = (uint8_t)L_20;
		goto IL_0087;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1_33 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Object_t_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0071;
		throw e;
	}

CATCH_0071:
	{ // begin catch(System.Object)
		{
			String_t* L_21 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2664, /*hidden argument*/NULL);
			CryptographicUnexpectedOperationException_t1_1195 * L_22 = (CryptographicUnexpectedOperationException_t1_1195 *)il2cpp_codegen_object_new (CryptographicUnexpectedOperationException_t1_1195_il2cpp_TypeInfo_var);
			CryptographicUnexpectedOperationException__ctor_m1_10227(L_22, L_21, /*hidden argument*/NULL);
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_22);
		}

IL_0082:
		{
			goto IL_0087;
		}
	} // end catch (depth: 1)

IL_0087:
	{
		V_5 = 3;
		V_6 = 2;
		goto IL_00e3;
	}

IL_0092:
	{
		StringU5BU5D_t1_238* L_23 = V_1;
		int32_t L_24 = V_6;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_24);
		int32_t L_25 = L_24;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1_1522_il2cpp_TypeInfo_var);
		int64_t L_26 = Convert_ToInt64_m1_13609(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_23, L_25, sizeof(String_t*))), /*hidden argument*/NULL);
		V_7 = L_26;
		int64_t L_27 = V_7;
		if ((((int64_t)L_27) <= ((int64_t)(((int64_t)((int64_t)((int32_t)127)))))))
		{
			goto IL_00cd;
		}
	}
	{
		int64_t L_28 = V_7;
		IL2CPP_RUNTIME_CLASS_INIT(CryptoConfig_t1_1191_il2cpp_TypeInfo_var);
		ByteU5BU5D_t1_109* L_29 = CryptoConfig_EncodeLongNumber_m1_10202(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		V_8 = L_29;
		ByteU5BU5D_t1_109* L_30 = V_8;
		ByteU5BU5D_t1_109* L_31 = V_2;
		int32_t L_32 = V_5;
		ByteU5BU5D_t1_109* L_33 = V_8;
		NullCheck(L_33);
		Buffer_BlockCopy_m1_13338(NULL /*static, unused*/, (Array_t *)(Array_t *)L_30, 0, (Array_t *)(Array_t *)L_31, L_32, (((int32_t)((int32_t)(((Array_t *)L_33)->max_length)))), /*hidden argument*/NULL);
		int32_t L_34 = V_5;
		ByteU5BU5D_t1_109* L_35 = V_8;
		NullCheck(L_35);
		V_5 = ((int32_t)((int32_t)L_34+(int32_t)(((int32_t)((int32_t)(((Array_t *)L_35)->max_length))))));
		goto IL_00dd;
	}

IL_00cd:
	{
		ByteU5BU5D_t1_109* L_36 = V_2;
		int32_t L_37 = V_5;
		int32_t L_38 = L_37;
		V_5 = ((int32_t)((int32_t)L_38+(int32_t)1));
		int64_t L_39 = V_7;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1_1522_il2cpp_TypeInfo_var);
		uint8_t L_40 = Convert_ToByte_m1_13477(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, L_38);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_36, L_38, sizeof(uint8_t))) = (uint8_t)L_40;
	}

IL_00dd:
	{
		int32_t L_41 = V_6;
		V_6 = ((int32_t)((int32_t)L_41+(int32_t)1));
	}

IL_00e3:
	{
		int32_t L_42 = V_6;
		StringU5BU5D_t1_238* L_43 = V_1;
		NullCheck(L_43);
		if ((((int32_t)L_42) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_43)->max_length)))))))
		{
			goto IL_0092;
		}
	}
	{
		V_9 = 2;
		int32_t L_44 = V_5;
		V_10 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, L_44));
		ByteU5BU5D_t1_109* L_45 = V_10;
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_45, 0, sizeof(uint8_t))) = (uint8_t)6;
		int32_t L_46 = V_5;
		if ((((int32_t)L_46) <= ((int32_t)((int32_t)127))))
		{
			goto IL_0117;
		}
	}
	{
		String_t* L_47 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2665, /*hidden argument*/NULL);
		CryptographicUnexpectedOperationException_t1_1195 * L_48 = (CryptographicUnexpectedOperationException_t1_1195 *)il2cpp_codegen_object_new (CryptographicUnexpectedOperationException_t1_1195_il2cpp_TypeInfo_var);
		CryptographicUnexpectedOperationException__ctor_m1_10227(L_48, L_47, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_48);
	}

IL_0117:
	{
		ByteU5BU5D_t1_109* L_49 = V_10;
		int32_t L_50 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1_1522_il2cpp_TypeInfo_var);
		uint8_t L_51 = Convert_ToByte_m1_13476(NULL /*static, unused*/, ((int32_t)((int32_t)L_50-(int32_t)2)), /*hidden argument*/NULL);
		NullCheck(L_49);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_49, 1);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_49, 1, sizeof(uint8_t))) = (uint8_t)L_51;
		ByteU5BU5D_t1_109* L_52 = V_2;
		int32_t L_53 = V_9;
		ByteU5BU5D_t1_109* L_54 = V_10;
		int32_t L_55 = V_9;
		int32_t L_56 = V_5;
		int32_t L_57 = V_9;
		Buffer_BlockCopy_m1_13338(NULL /*static, unused*/, (Array_t *)(Array_t *)L_52, L_53, (Array_t *)(Array_t *)L_54, L_55, ((int32_t)((int32_t)L_56-(int32_t)L_57)), /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_58 = V_10;
		return L_58;
	}
}
// System.Byte[] System.Security.Cryptography.CryptoConfig::EncodeLongNumber(System.Int64)
extern TypeInfo* OverflowException_t1_1590_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern TypeInfo* Convert_t1_1522_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2666;
extern "C" ByteU5BU5D_t1_109* CryptoConfig_EncodeLongNumber_m1_10202 (Object_t * __this /* static, unused */, int64_t ___x, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		OverflowException_t1_1590_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(13);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		Convert_t1_1522_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		_stringLiteral2666 = il2cpp_codegen_string_literal_from_index(2666);
		s_Il2CppMethodIntialized = true;
	}
	int64_t V_0 = 0;
	int32_t V_1 = 0;
	ByteU5BU5D_t1_109* V_2 = {0};
	int32_t V_3 = 0;
	{
		int64_t L_0 = ___x;
		if ((((int64_t)L_0) > ((int64_t)(((int64_t)((int64_t)((int32_t)2147483647)))))))
		{
			goto IL_0018;
		}
	}
	{
		int64_t L_1 = ___x;
		if ((((int64_t)L_1) >= ((int64_t)(((int64_t)((int64_t)((int32_t)-2147483648)))))))
		{
			goto IL_0028;
		}
	}

IL_0018:
	{
		String_t* L_2 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2666, /*hidden argument*/NULL);
		OverflowException_t1_1590 * L_3 = (OverflowException_t1_1590 *)il2cpp_codegen_object_new (OverflowException_t1_1590_il2cpp_TypeInfo_var);
		OverflowException__ctor_m1_14547(L_3, L_2, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0028:
	{
		int64_t L_4 = ___x;
		V_0 = L_4;
		V_1 = 1;
		goto IL_0039;
	}

IL_0031:
	{
		int64_t L_5 = V_0;
		V_0 = ((int64_t)((int64_t)L_5>>(int32_t)7));
		int32_t L_6 = V_1;
		V_1 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0039:
	{
		int64_t L_7 = V_0;
		if ((((int64_t)L_7) > ((int64_t)(((int64_t)((int64_t)((int32_t)127)))))))
		{
			goto IL_0031;
		}
	}
	{
		int32_t L_8 = V_1;
		V_2 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, L_8));
		V_3 = 0;
		goto IL_007f;
	}

IL_0050:
	{
		int64_t L_9 = ___x;
		int32_t L_10 = V_3;
		V_0 = ((int64_t)((int64_t)L_9>>(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)7*(int32_t)L_10))&(int32_t)((int32_t)63)))));
		int64_t L_11 = V_0;
		V_0 = ((int64_t)((int64_t)L_11&(int64_t)(((int64_t)((int64_t)((int32_t)127))))));
		int32_t L_12 = V_3;
		if (!L_12)
		{
			goto IL_006e;
		}
	}
	{
		int64_t L_13 = V_0;
		V_0 = ((int64_t)((int64_t)L_13+(int64_t)(((int64_t)((int64_t)((int32_t)128))))));
	}

IL_006e:
	{
		ByteU5BU5D_t1_109* L_14 = V_2;
		int32_t L_15 = V_1;
		int32_t L_16 = V_3;
		int64_t L_17 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1_1522_il2cpp_TypeInfo_var);
		uint8_t L_18 = Convert_ToByte_m1_13477(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, ((int32_t)((int32_t)((int32_t)((int32_t)L_15-(int32_t)L_16))-(int32_t)1)));
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_14, ((int32_t)((int32_t)((int32_t)((int32_t)L_15-(int32_t)L_16))-(int32_t)1)), sizeof(uint8_t))) = (uint8_t)L_18;
		int32_t L_19 = V_3;
		V_3 = ((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_007f:
	{
		int32_t L_20 = V_3;
		int32_t L_21 = V_1;
		if ((((int32_t)L_20) < ((int32_t)L_21)))
		{
			goto IL_0050;
		}
	}
	{
		ByteU5BU5D_t1_109* L_22 = V_2;
		return L_22;
	}
}
// System.Void System.Security.Cryptography.CryptoStream::.ctor(System.IO.Stream,System.Security.Cryptography.ICryptoTransform,System.Security.Cryptography.CryptoStreamMode)
extern TypeInfo* Stream_t1_405_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern TypeInfo* ICryptoTransform_t1_156_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2677;
extern Il2CppCodeGenString* _stringLiteral2678;
extern "C" void CryptoStream__ctor_m1_10203 (CryptoStream_t1_1192 * __this, Stream_t1_405 * ___stream, Object_t * ___transform, int32_t ___mode, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Stream_t1_405_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(265);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		ICryptoTransform_t1_156_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(202);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		_stringLiteral2677 = il2cpp_codegen_string_literal_from_index(2677);
		_stringLiteral2678 = il2cpp_codegen_string_literal_from_index(2678);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Stream_t1_405_il2cpp_TypeInfo_var);
		Stream__ctor_m1_5158(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___mode;
		if (L_0)
		{
			goto IL_0027;
		}
	}
	{
		Stream_t1_405 * L_1 = ___stream;
		NullCheck(L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Stream_t1_405_il2cpp_TypeInfo_var);
		bool L_2 = (bool)VirtFuncInvoker0< bool >::Invoke(5 /* System.Boolean System.IO.Stream::get_CanRead() */, L_1);
		if (L_2)
		{
			goto IL_0027;
		}
	}
	{
		String_t* L_3 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2677, /*hidden argument*/NULL);
		ArgumentException_t1_1425 * L_4 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13260(L_4, L_3, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
	}

IL_0027:
	{
		int32_t L_5 = ___mode;
		if ((!(((uint32_t)L_5) == ((uint32_t)1))))
		{
			goto IL_0049;
		}
	}
	{
		Stream_t1_405 * L_6 = ___stream;
		NullCheck(L_6);
		IL2CPP_RUNTIME_CLASS_INIT(Stream_t1_405_il2cpp_TypeInfo_var);
		bool L_7 = (bool)VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.IO.Stream::get_CanWrite() */, L_6);
		if (L_7)
		{
			goto IL_0049;
		}
	}
	{
		String_t* L_8 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2678, /*hidden argument*/NULL);
		ArgumentException_t1_1425 * L_9 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13260(L_9, L_8, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_9);
	}

IL_0049:
	{
		Stream_t1_405 * L_10 = ___stream;
		__this->____stream_1 = L_10;
		Object_t * L_11 = ___transform;
		__this->____transform_2 = L_11;
		int32_t L_12 = ___mode;
		__this->____mode_3 = L_12;
		__this->____disposed_5 = 0;
		Object_t * L_13 = ___transform;
		if (!L_13)
		{
			goto IL_00c1;
		}
	}
	{
		int32_t L_14 = ___mode;
		if (L_14)
		{
			goto IL_0098;
		}
	}
	{
		Object_t * L_15 = ___transform;
		NullCheck(L_15);
		int32_t L_16 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Security.Cryptography.ICryptoTransform::get_InputBlockSize() */, ICryptoTransform_t1_156_il2cpp_TypeInfo_var, L_15);
		__this->____currentBlock_4 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, L_16));
		Object_t * L_17 = ___transform;
		NullCheck(L_17);
		int32_t L_18 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Security.Cryptography.ICryptoTransform::get_InputBlockSize() */, ICryptoTransform_t1_156_il2cpp_TypeInfo_var, L_17);
		__this->____workingBlock_14 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, L_18));
		goto IL_00c1;
	}

IL_0098:
	{
		int32_t L_19 = ___mode;
		if ((!(((uint32_t)L_19) == ((uint32_t)1))))
		{
			goto IL_00c1;
		}
	}
	{
		Object_t * L_20 = ___transform;
		NullCheck(L_20);
		int32_t L_21 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(3 /* System.Int32 System.Security.Cryptography.ICryptoTransform::get_OutputBlockSize() */, ICryptoTransform_t1_156_il2cpp_TypeInfo_var, L_20);
		__this->____currentBlock_4 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, L_21));
		Object_t * L_22 = ___transform;
		NullCheck(L_22);
		int32_t L_23 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(3 /* System.Int32 System.Security.Cryptography.ICryptoTransform::get_OutputBlockSize() */, ICryptoTransform_t1_156_il2cpp_TypeInfo_var, L_22);
		__this->____workingBlock_14 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, L_23));
	}

IL_00c1:
	{
		return;
	}
}
// System.Void System.Security.Cryptography.CryptoStream::Finalize()
extern "C" void CryptoStream_Finalize_m1_10204 (CryptoStream_t1_1192 * __this, const MethodInfo* method)
{
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		VirtActionInvoker1< bool >::Invoke(12 /* System.Void System.Security.Cryptography.CryptoStream::Dispose(System.Boolean) */, __this, 0);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m1_3(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0013:
	{
		return;
	}
}
// System.Boolean System.Security.Cryptography.CryptoStream::get_CanRead()
extern "C" bool CryptoStream_get_CanRead_m1_10205 (CryptoStream_t1_1192 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->____mode_3);
		return ((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean System.Security.Cryptography.CryptoStream::get_CanSeek()
extern "C" bool CryptoStream_get_CanSeek_m1_10206 (CryptoStream_t1_1192 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Boolean System.Security.Cryptography.CryptoStream::get_CanWrite()
extern "C" bool CryptoStream_get_CanWrite_m1_10207 (CryptoStream_t1_1192 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->____mode_3);
		return ((((int32_t)L_0) == ((int32_t)1))? 1 : 0);
	}
}
// System.Int64 System.Security.Cryptography.CryptoStream::get_Length()
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2679;
extern "C" int64_t CryptoStream_get_Length_m1_10208 (CryptoStream_t1_1192 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		_stringLiteral2679 = il2cpp_codegen_string_literal_from_index(2679);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14417(L_0, _stringLiteral2679, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Int64 System.Security.Cryptography.CryptoStream::get_Position()
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2680;
extern "C" int64_t CryptoStream_get_Position_m1_10209 (CryptoStream_t1_1192 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		_stringLiteral2680 = il2cpp_codegen_string_literal_from_index(2680);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14417(L_0, _stringLiteral2680, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.Cryptography.CryptoStream::set_Position(System.Int64)
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2680;
extern "C" void CryptoStream_set_Position_m1_10210 (CryptoStream_t1_1192 * __this, int64_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		_stringLiteral2680 = il2cpp_codegen_string_literal_from_index(2680);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14417(L_0, _stringLiteral2680, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.Cryptography.CryptoStream::Clear()
extern "C" void CryptoStream_Clear_m1_10211 (CryptoStream_t1_1192 * __this, const MethodInfo* method)
{
	{
		VirtActionInvoker1< bool >::Invoke(12 /* System.Void System.Security.Cryptography.CryptoStream::Dispose(System.Boolean) */, __this, 1);
		GC_SuppressFinalize_m1_14113(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.Cryptography.CryptoStream::Close()
extern "C" void CryptoStream_Close_m1_10212 (CryptoStream_t1_1192 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->____flushedFinalBlock_6);
		if (L_0)
		{
			goto IL_001d;
		}
	}
	{
		int32_t L_1 = (__this->____mode_3);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_001d;
		}
	}
	{
		CryptoStream_FlushFinalBlock_m1_10216(__this, /*hidden argument*/NULL);
	}

IL_001d:
	{
		Stream_t1_405 * L_2 = (__this->____stream_1);
		if (!L_2)
		{
			goto IL_0033;
		}
	}
	{
		Stream_t1_405 * L_3 = (__this->____stream_1);
		NullCheck(L_3);
		VirtActionInvoker0::Invoke(13 /* System.Void System.IO.Stream::Close() */, L_3);
	}

IL_0033:
	{
		return;
	}
}
// System.Int32 System.Security.Cryptography.CryptoStream::Read(System.Byte[],System.Int32,System.Int32)
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern TypeInfo* ICryptoTransform_t1_156_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2681;
extern Il2CppCodeGenString* _stringLiteral1230;
extern Il2CppCodeGenString* _stringLiteral2682;
extern Il2CppCodeGenString* _stringLiteral47;
extern Il2CppCodeGenString* _stringLiteral2683;
extern Il2CppCodeGenString* _stringLiteral2684;
extern "C" int32_t CryptoStream_Read_m1_10213 (CryptoStream_t1_1192 * __this, ByteU5BU5D_t1_109* ___buffer, int32_t ___offset, int32_t ___count, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		ICryptoTransform_t1_156_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(202);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		_stringLiteral2681 = il2cpp_codegen_string_literal_from_index(2681);
		_stringLiteral1230 = il2cpp_codegen_string_literal_from_index(1230);
		_stringLiteral2682 = il2cpp_codegen_string_literal_from_index(2682);
		_stringLiteral47 = il2cpp_codegen_string_literal_from_index(47);
		_stringLiteral2683 = il2cpp_codegen_string_literal_from_index(2683);
		_stringLiteral2684 = il2cpp_codegen_string_literal_from_index(2684);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	ByteU5BU5D_t1_109* V_3 = {0};
	int32_t G_B30_0 = 0;
	{
		int32_t L_0 = (__this->____mode_3);
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2681, /*hidden argument*/NULL);
		NotSupportedException_t1_1583 * L_2 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14417(L_2, L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001b:
	{
		int32_t L_3 = ___offset;
		if ((((int32_t)L_3) >= ((int32_t)0)))
		{
			goto IL_0037;
		}
	}
	{
		String_t* L_4 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2682, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t1_1501 * L_5 = (ArgumentOutOfRangeException_t1_1501 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1_13275(L_5, _stringLiteral1230, L_4, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_5);
	}

IL_0037:
	{
		int32_t L_6 = ___count;
		if ((((int32_t)L_6) >= ((int32_t)0)))
		{
			goto IL_0053;
		}
	}
	{
		String_t* L_7 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2682, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t1_1501 * L_8 = (ArgumentOutOfRangeException_t1_1501 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1_13275(L_8, _stringLiteral47, L_7, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_8);
	}

IL_0053:
	{
		int32_t L_9 = ___offset;
		ByteU5BU5D_t1_109* L_10 = ___buffer;
		NullCheck(L_10);
		int32_t L_11 = ___count;
		if ((((int32_t)L_9) <= ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Array_t *)L_10)->max_length))))-(int32_t)L_11)))))
		{
			goto IL_0073;
		}
	}
	{
		String_t* L_12 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2684, /*hidden argument*/NULL);
		ArgumentException_t1_1425 * L_13 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13262(L_13, _stringLiteral2683, L_12, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_13);
	}

IL_0073:
	{
		ByteU5BU5D_t1_109* L_14 = (__this->____workingBlock_14);
		if (L_14)
		{
			goto IL_0080;
		}
	}
	{
		return 0;
	}

IL_0080:
	{
		V_0 = 0;
		int32_t L_15 = ___count;
		if (!L_15)
		{
			goto IL_00a4;
		}
	}
	{
		int32_t L_16 = (__this->____transformedPos_12);
		int32_t L_17 = (__this->____transformedCount_13);
		if ((!(((uint32_t)L_16) == ((uint32_t)L_17))))
		{
			goto IL_00a6;
		}
	}
	{
		bool L_18 = (__this->____endOfStream_8);
		if (!L_18)
		{
			goto IL_00a6;
		}
	}

IL_00a4:
	{
		int32_t L_19 = V_0;
		return L_19;
	}

IL_00a6:
	{
		ByteU5BU5D_t1_109* L_20 = (__this->____waitingBlock_9);
		if (L_20)
		{
			goto IL_010d;
		}
	}
	{
		Object_t * L_21 = (__this->____transform_2);
		NullCheck(L_21);
		int32_t L_22 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(3 /* System.Int32 System.Security.Cryptography.ICryptoTransform::get_OutputBlockSize() */, ICryptoTransform_t1_156_il2cpp_TypeInfo_var, L_21);
		__this->____transformedBlock_11 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, ((int32_t)((int32_t)L_22<<(int32_t)2))));
		__this->____transformedPos_12 = 0;
		__this->____transformedCount_13 = 0;
		Object_t * L_23 = (__this->____transform_2);
		NullCheck(L_23);
		int32_t L_24 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Security.Cryptography.ICryptoTransform::get_InputBlockSize() */, ICryptoTransform_t1_156_il2cpp_TypeInfo_var, L_23);
		__this->____waitingBlock_9 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, L_24));
		Stream_t1_405 * L_25 = (__this->____stream_1);
		ByteU5BU5D_t1_109* L_26 = (__this->____waitingBlock_9);
		ByteU5BU5D_t1_109* L_27 = (__this->____waitingBlock_9);
		NullCheck(L_27);
		NullCheck(L_25);
		int32_t L_28 = (int32_t)VirtFuncInvoker3< int32_t, ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(20 /* System.Int32 System.IO.Stream::Read(System.Byte[],System.Int32,System.Int32) */, L_25, L_26, 0, (((int32_t)((int32_t)(((Array_t *)L_27)->max_length)))));
		__this->____waitingCount_10 = L_28;
	}

IL_010d:
	{
		goto IL_035a;
	}

IL_0112:
	{
		int32_t L_29 = (__this->____transformedCount_13);
		int32_t L_30 = (__this->____transformedPos_12);
		V_1 = ((int32_t)((int32_t)L_29-(int32_t)L_30));
		int32_t L_31 = V_1;
		Object_t * L_32 = (__this->____transform_2);
		NullCheck(L_32);
		int32_t L_33 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Security.Cryptography.ICryptoTransform::get_InputBlockSize() */, ICryptoTransform_t1_156_il2cpp_TypeInfo_var, L_32);
		if ((((int32_t)L_31) >= ((int32_t)L_33)))
		{
			goto IL_0296;
		}
	}
	{
		V_2 = 0;
		Stream_t1_405 * L_34 = (__this->____stream_1);
		ByteU5BU5D_t1_109* L_35 = (__this->____workingBlock_14);
		Object_t * L_36 = (__this->____transform_2);
		NullCheck(L_36);
		int32_t L_37 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Security.Cryptography.ICryptoTransform::get_InputBlockSize() */, ICryptoTransform_t1_156_il2cpp_TypeInfo_var, L_36);
		NullCheck(L_34);
		int32_t L_38 = (int32_t)VirtFuncInvoker3< int32_t, ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(20 /* System.Int32 System.IO.Stream::Read(System.Byte[],System.Int32,System.Int32) */, L_34, L_35, 0, L_37);
		__this->____workingCount_15 = L_38;
		int32_t L_39 = (__this->____workingCount_15);
		Object_t * L_40 = (__this->____transform_2);
		NullCheck(L_40);
		int32_t L_41 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Security.Cryptography.ICryptoTransform::get_InputBlockSize() */, ICryptoTransform_t1_156_il2cpp_TypeInfo_var, L_40);
		__this->____endOfStream_8 = ((((int32_t)L_39) < ((int32_t)L_41))? 1 : 0);
		bool L_42 = (__this->____endOfStream_8);
		if (L_42)
		{
			goto IL_01cb;
		}
	}
	{
		Object_t * L_43 = (__this->____transform_2);
		ByteU5BU5D_t1_109* L_44 = (__this->____waitingBlock_9);
		ByteU5BU5D_t1_109* L_45 = (__this->____waitingBlock_9);
		NullCheck(L_45);
		ByteU5BU5D_t1_109* L_46 = (__this->____transformedBlock_11);
		int32_t L_47 = (__this->____transformedCount_13);
		NullCheck(L_43);
		int32_t L_48 = (int32_t)InterfaceFuncInvoker5< int32_t, ByteU5BU5D_t1_109*, int32_t, int32_t, ByteU5BU5D_t1_109*, int32_t >::Invoke(4 /* System.Int32 System.Security.Cryptography.ICryptoTransform::TransformBlock(System.Byte[],System.Int32,System.Int32,System.Byte[],System.Int32) */, ICryptoTransform_t1_156_il2cpp_TypeInfo_var, L_43, L_44, 0, (((int32_t)((int32_t)(((Array_t *)L_45)->max_length)))), L_46, L_47);
		V_2 = L_48;
		ByteU5BU5D_t1_109* L_49 = (__this->____workingBlock_14);
		ByteU5BU5D_t1_109* L_50 = (__this->____waitingBlock_9);
		int32_t L_51 = (__this->____workingCount_15);
		Buffer_BlockCopy_m1_13338(NULL /*static, unused*/, (Array_t *)(Array_t *)L_49, 0, (Array_t *)(Array_t *)L_50, 0, L_51, /*hidden argument*/NULL);
		int32_t L_52 = (__this->____workingCount_15);
		__this->____waitingCount_10 = L_52;
		goto IL_0284;
	}

IL_01cb:
	{
		int32_t L_53 = (__this->____workingCount_15);
		if ((((int32_t)L_53) <= ((int32_t)0)))
		{
			goto IL_0235;
		}
	}
	{
		Object_t * L_54 = (__this->____transform_2);
		ByteU5BU5D_t1_109* L_55 = (__this->____waitingBlock_9);
		ByteU5BU5D_t1_109* L_56 = (__this->____waitingBlock_9);
		NullCheck(L_56);
		ByteU5BU5D_t1_109* L_57 = (__this->____transformedBlock_11);
		int32_t L_58 = (__this->____transformedCount_13);
		NullCheck(L_54);
		int32_t L_59 = (int32_t)InterfaceFuncInvoker5< int32_t, ByteU5BU5D_t1_109*, int32_t, int32_t, ByteU5BU5D_t1_109*, int32_t >::Invoke(4 /* System.Int32 System.Security.Cryptography.ICryptoTransform::TransformBlock(System.Byte[],System.Int32,System.Int32,System.Byte[],System.Int32) */, ICryptoTransform_t1_156_il2cpp_TypeInfo_var, L_54, L_55, 0, (((int32_t)((int32_t)(((Array_t *)L_56)->max_length)))), L_57, L_58);
		V_2 = L_59;
		ByteU5BU5D_t1_109* L_60 = (__this->____workingBlock_14);
		ByteU5BU5D_t1_109* L_61 = (__this->____waitingBlock_9);
		int32_t L_62 = (__this->____workingCount_15);
		Buffer_BlockCopy_m1_13338(NULL /*static, unused*/, (Array_t *)(Array_t *)L_60, 0, (Array_t *)(Array_t *)L_61, 0, L_62, /*hidden argument*/NULL);
		int32_t L_63 = (__this->____workingCount_15);
		__this->____waitingCount_10 = L_63;
		int32_t L_64 = V_1;
		int32_t L_65 = V_2;
		V_1 = ((int32_t)((int32_t)L_64+(int32_t)L_65));
		int32_t L_66 = (__this->____transformedCount_13);
		int32_t L_67 = V_2;
		__this->____transformedCount_13 = ((int32_t)((int32_t)L_66+(int32_t)L_67));
	}

IL_0235:
	{
		bool L_68 = (__this->____flushedFinalBlock_6);
		if (L_68)
		{
			goto IL_0284;
		}
	}
	{
		Object_t * L_69 = (__this->____transform_2);
		ByteU5BU5D_t1_109* L_70 = (__this->____waitingBlock_9);
		int32_t L_71 = (__this->____waitingCount_10);
		NullCheck(L_69);
		ByteU5BU5D_t1_109* L_72 = (ByteU5BU5D_t1_109*)InterfaceFuncInvoker3< ByteU5BU5D_t1_109*, ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(5 /* System.Byte[] System.Security.Cryptography.ICryptoTransform::TransformFinalBlock(System.Byte[],System.Int32,System.Int32) */, ICryptoTransform_t1_156_il2cpp_TypeInfo_var, L_69, L_70, 0, L_71);
		V_3 = L_72;
		ByteU5BU5D_t1_109* L_73 = V_3;
		NullCheck(L_73);
		V_2 = (((int32_t)((int32_t)(((Array_t *)L_73)->max_length))));
		ByteU5BU5D_t1_109* L_74 = V_3;
		ByteU5BU5D_t1_109* L_75 = (__this->____transformedBlock_11);
		int32_t L_76 = (__this->____transformedCount_13);
		ByteU5BU5D_t1_109* L_77 = V_3;
		NullCheck(L_77);
		Buffer_BlockCopy_m1_13338(NULL /*static, unused*/, (Array_t *)(Array_t *)L_74, 0, (Array_t *)(Array_t *)L_75, L_76, (((int32_t)((int32_t)(((Array_t *)L_77)->max_length)))), /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_78 = V_3;
		ByteU5BU5D_t1_109* L_79 = V_3;
		NullCheck(L_79);
		Array_Clear_m1_1037(NULL /*static, unused*/, (Array_t *)(Array_t *)L_78, 0, (((int32_t)((int32_t)(((Array_t *)L_79)->max_length)))), /*hidden argument*/NULL);
		__this->____flushedFinalBlock_6 = 1;
	}

IL_0284:
	{
		int32_t L_80 = V_1;
		int32_t L_81 = V_2;
		V_1 = ((int32_t)((int32_t)L_80+(int32_t)L_81));
		int32_t L_82 = (__this->____transformedCount_13);
		int32_t L_83 = V_2;
		__this->____transformedCount_13 = ((int32_t)((int32_t)L_82+(int32_t)L_83));
	}

IL_0296:
	{
		int32_t L_84 = (__this->____transformedPos_12);
		Object_t * L_85 = (__this->____transform_2);
		NullCheck(L_85);
		int32_t L_86 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(3 /* System.Int32 System.Security.Cryptography.ICryptoTransform::get_OutputBlockSize() */, ICryptoTransform_t1_156_il2cpp_TypeInfo_var, L_85);
		if ((((int32_t)L_84) <= ((int32_t)L_86)))
		{
			goto IL_02df;
		}
	}
	{
		ByteU5BU5D_t1_109* L_87 = (__this->____transformedBlock_11);
		int32_t L_88 = (__this->____transformedPos_12);
		ByteU5BU5D_t1_109* L_89 = (__this->____transformedBlock_11);
		int32_t L_90 = V_1;
		Buffer_BlockCopy_m1_13338(NULL /*static, unused*/, (Array_t *)(Array_t *)L_87, L_88, (Array_t *)(Array_t *)L_89, 0, L_90, /*hidden argument*/NULL);
		int32_t L_91 = (__this->____transformedCount_13);
		int32_t L_92 = (__this->____transformedPos_12);
		__this->____transformedCount_13 = ((int32_t)((int32_t)L_91-(int32_t)L_92));
		__this->____transformedPos_12 = 0;
	}

IL_02df:
	{
		int32_t L_93 = ___count;
		int32_t L_94 = V_1;
		if ((((int32_t)L_93) >= ((int32_t)L_94)))
		{
			goto IL_02ec;
		}
	}
	{
		int32_t L_95 = ___count;
		G_B30_0 = L_95;
		goto IL_02ed;
	}

IL_02ec:
	{
		int32_t L_96 = V_1;
		G_B30_0 = L_96;
	}

IL_02ed:
	{
		V_1 = G_B30_0;
		int32_t L_97 = V_1;
		if ((((int32_t)L_97) <= ((int32_t)0)))
		{
			goto IL_0325;
		}
	}
	{
		ByteU5BU5D_t1_109* L_98 = (__this->____transformedBlock_11);
		int32_t L_99 = (__this->____transformedPos_12);
		ByteU5BU5D_t1_109* L_100 = ___buffer;
		int32_t L_101 = ___offset;
		int32_t L_102 = V_1;
		Buffer_BlockCopy_m1_13338(NULL /*static, unused*/, (Array_t *)(Array_t *)L_98, L_99, (Array_t *)(Array_t *)L_100, L_101, L_102, /*hidden argument*/NULL);
		int32_t L_103 = (__this->____transformedPos_12);
		int32_t L_104 = V_1;
		__this->____transformedPos_12 = ((int32_t)((int32_t)L_103+(int32_t)L_104));
		int32_t L_105 = V_0;
		int32_t L_106 = V_1;
		V_0 = ((int32_t)((int32_t)L_105+(int32_t)L_106));
		int32_t L_107 = ___offset;
		int32_t L_108 = V_1;
		___offset = ((int32_t)((int32_t)L_107+(int32_t)L_108));
		int32_t L_109 = ___count;
		int32_t L_110 = V_1;
		___count = ((int32_t)((int32_t)L_109-(int32_t)L_110));
	}

IL_0325:
	{
		int32_t L_111 = V_1;
		Object_t * L_112 = (__this->____transform_2);
		NullCheck(L_112);
		int32_t L_113 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Security.Cryptography.ICryptoTransform::get_InputBlockSize() */, ICryptoTransform_t1_156_il2cpp_TypeInfo_var, L_112);
		if ((((int32_t)L_111) == ((int32_t)L_113)))
		{
			goto IL_034c;
		}
	}
	{
		int32_t L_114 = (__this->____waitingCount_10);
		Object_t * L_115 = (__this->____transform_2);
		NullCheck(L_115);
		int32_t L_116 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Security.Cryptography.ICryptoTransform::get_InputBlockSize() */, ICryptoTransform_t1_156_il2cpp_TypeInfo_var, L_115);
		if ((!(((uint32_t)L_114) == ((uint32_t)L_116))))
		{
			goto IL_0357;
		}
	}

IL_034c:
	{
		bool L_117 = (__this->____endOfStream_8);
		if (!L_117)
		{
			goto IL_035a;
		}
	}

IL_0357:
	{
		___count = 0;
	}

IL_035a:
	{
		int32_t L_118 = ___count;
		if ((((int32_t)L_118) > ((int32_t)0)))
		{
			goto IL_0112;
		}
	}
	{
		int32_t L_119 = V_0;
		return L_119;
	}
}
// System.Void System.Security.Cryptography.CryptoStream::Write(System.Byte[],System.Int32,System.Int32)
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* ICryptoTransform_t1_156_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2685;
extern Il2CppCodeGenString* _stringLiteral1230;
extern Il2CppCodeGenString* _stringLiteral2682;
extern Il2CppCodeGenString* _stringLiteral47;
extern Il2CppCodeGenString* _stringLiteral2683;
extern Il2CppCodeGenString* _stringLiteral2684;
extern Il2CppCodeGenString* _stringLiteral2686;
extern "C" void CryptoStream_Write_m1_10214 (CryptoStream_t1_1192 * __this, ByteU5BU5D_t1_109* ___buffer, int32_t ___offset, int32_t ___count, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		ICryptoTransform_t1_156_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(202);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		_stringLiteral2685 = il2cpp_codegen_string_literal_from_index(2685);
		_stringLiteral1230 = il2cpp_codegen_string_literal_from_index(1230);
		_stringLiteral2682 = il2cpp_codegen_string_literal_from_index(2682);
		_stringLiteral47 = il2cpp_codegen_string_literal_from_index(47);
		_stringLiteral2683 = il2cpp_codegen_string_literal_from_index(2683);
		_stringLiteral2684 = il2cpp_codegen_string_literal_from_index(2684);
		_stringLiteral2686 = il2cpp_codegen_string_literal_from_index(2686);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t G_B15_0 = 0;
	{
		int32_t L_0 = (__this->____mode_3);
		if ((((int32_t)L_0) == ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2685, /*hidden argument*/NULL);
		NotSupportedException_t1_1583 * L_2 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14417(L_2, L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001c:
	{
		int32_t L_3 = ___offset;
		if ((((int32_t)L_3) >= ((int32_t)0)))
		{
			goto IL_0038;
		}
	}
	{
		String_t* L_4 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2682, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t1_1501 * L_5 = (ArgumentOutOfRangeException_t1_1501 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1_13275(L_5, _stringLiteral1230, L_4, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_5);
	}

IL_0038:
	{
		int32_t L_6 = ___count;
		if ((((int32_t)L_6) >= ((int32_t)0)))
		{
			goto IL_0054;
		}
	}
	{
		String_t* L_7 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2682, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t1_1501 * L_8 = (ArgumentOutOfRangeException_t1_1501 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1_13275(L_8, _stringLiteral47, L_7, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_8);
	}

IL_0054:
	{
		int32_t L_9 = ___offset;
		ByteU5BU5D_t1_109* L_10 = ___buffer;
		NullCheck(L_10);
		int32_t L_11 = ___count;
		if ((((int32_t)L_9) <= ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Array_t *)L_10)->max_length))))-(int32_t)L_11)))))
		{
			goto IL_0074;
		}
	}
	{
		String_t* L_12 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2684, /*hidden argument*/NULL);
		ArgumentException_t1_1425 * L_13 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13262(L_13, _stringLiteral2683, L_12, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_13);
	}

IL_0074:
	{
		Stream_t1_405 * L_14 = (__this->____stream_1);
		if (L_14)
		{
			goto IL_008a;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_15 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_15, _stringLiteral2686, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_15);
	}

IL_008a:
	{
		int32_t L_16 = ___count;
		V_0 = L_16;
		int32_t L_17 = (__this->____partialCount_7);
		if ((((int32_t)L_17) <= ((int32_t)0)))
		{
			goto IL_00fc;
		}
	}
	{
		int32_t L_18 = (__this->____partialCount_7);
		Object_t * L_19 = (__this->____transform_2);
		NullCheck(L_19);
		int32_t L_20 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Security.Cryptography.ICryptoTransform::get_InputBlockSize() */, ICryptoTransform_t1_156_il2cpp_TypeInfo_var, L_19);
		if ((((int32_t)L_18) == ((int32_t)L_20)))
		{
			goto IL_00fc;
		}
	}
	{
		Object_t * L_21 = (__this->____transform_2);
		NullCheck(L_21);
		int32_t L_22 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Security.Cryptography.ICryptoTransform::get_InputBlockSize() */, ICryptoTransform_t1_156_il2cpp_TypeInfo_var, L_21);
		int32_t L_23 = (__this->____partialCount_7);
		V_1 = ((int32_t)((int32_t)L_22-(int32_t)L_23));
		int32_t L_24 = ___count;
		int32_t L_25 = V_1;
		if ((((int32_t)L_24) >= ((int32_t)L_25)))
		{
			goto IL_00ce;
		}
	}
	{
		int32_t L_26 = ___count;
		G_B15_0 = L_26;
		goto IL_00cf;
	}

IL_00ce:
	{
		int32_t L_27 = V_1;
		G_B15_0 = L_27;
	}

IL_00cf:
	{
		V_1 = G_B15_0;
		ByteU5BU5D_t1_109* L_28 = ___buffer;
		int32_t L_29 = ___offset;
		ByteU5BU5D_t1_109* L_30 = (__this->____workingBlock_14);
		int32_t L_31 = (__this->____partialCount_7);
		int32_t L_32 = V_1;
		Buffer_BlockCopy_m1_13338(NULL /*static, unused*/, (Array_t *)(Array_t *)L_28, L_29, (Array_t *)(Array_t *)L_30, L_31, L_32, /*hidden argument*/NULL);
		int32_t L_33 = (__this->____partialCount_7);
		int32_t L_34 = V_1;
		__this->____partialCount_7 = ((int32_t)((int32_t)L_33+(int32_t)L_34));
		int32_t L_35 = ___offset;
		int32_t L_36 = V_1;
		___offset = ((int32_t)((int32_t)L_35+(int32_t)L_36));
		int32_t L_37 = ___count;
		int32_t L_38 = V_1;
		___count = ((int32_t)((int32_t)L_37-(int32_t)L_38));
	}

IL_00fc:
	{
		int32_t L_39 = ___offset;
		V_2 = L_39;
		goto IL_027c;
	}

IL_0103:
	{
		int32_t L_40 = (__this->____partialCount_7);
		Object_t * L_41 = (__this->____transform_2);
		NullCheck(L_41);
		int32_t L_42 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Security.Cryptography.ICryptoTransform::get_InputBlockSize() */, ICryptoTransform_t1_156_il2cpp_TypeInfo_var, L_41);
		if ((!(((uint32_t)L_40) == ((uint32_t)L_42))))
		{
			goto IL_0153;
		}
	}
	{
		Object_t * L_43 = (__this->____transform_2);
		ByteU5BU5D_t1_109* L_44 = (__this->____workingBlock_14);
		int32_t L_45 = (__this->____partialCount_7);
		ByteU5BU5D_t1_109* L_46 = (__this->____currentBlock_4);
		NullCheck(L_43);
		int32_t L_47 = (int32_t)InterfaceFuncInvoker5< int32_t, ByteU5BU5D_t1_109*, int32_t, int32_t, ByteU5BU5D_t1_109*, int32_t >::Invoke(4 /* System.Int32 System.Security.Cryptography.ICryptoTransform::TransformBlock(System.Byte[],System.Int32,System.Int32,System.Byte[],System.Int32) */, ICryptoTransform_t1_156_il2cpp_TypeInfo_var, L_43, L_44, 0, L_45, L_46, 0);
		V_3 = L_47;
		Stream_t1_405 * L_48 = (__this->____stream_1);
		ByteU5BU5D_t1_109* L_49 = (__this->____currentBlock_4);
		int32_t L_50 = V_3;
		NullCheck(L_48);
		VirtActionInvoker3< ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(24 /* System.Void System.IO.Stream::Write(System.Byte[],System.Int32,System.Int32) */, L_48, L_49, 0, L_50);
		__this->____partialCount_7 = 0;
	}

IL_0153:
	{
		Object_t * L_51 = (__this->____transform_2);
		NullCheck(L_51);
		bool L_52 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Security.Cryptography.ICryptoTransform::get_CanTransformMultipleBlocks() */, ICryptoTransform_t1_156_il2cpp_TypeInfo_var, L_51);
		if (!L_52)
		{
			goto IL_0233;
		}
	}
	{
		int32_t L_53 = ___count;
		Object_t * L_54 = (__this->____transform_2);
		NullCheck(L_54);
		int32_t L_55 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Security.Cryptography.ICryptoTransform::get_InputBlockSize() */, ICryptoTransform_t1_156_il2cpp_TypeInfo_var, L_54);
		V_4 = ((int32_t)((int32_t)L_53&(int32_t)((~((int32_t)((int32_t)L_55-(int32_t)1))))));
		int32_t L_56 = ___count;
		Object_t * L_57 = (__this->____transform_2);
		NullCheck(L_57);
		int32_t L_58 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Security.Cryptography.ICryptoTransform::get_InputBlockSize() */, ICryptoTransform_t1_156_il2cpp_TypeInfo_var, L_57);
		V_5 = ((int32_t)((int32_t)L_56&(int32_t)((int32_t)((int32_t)L_58-(int32_t)1))));
		int32_t L_59 = V_4;
		Object_t * L_60 = (__this->____transform_2);
		NullCheck(L_60);
		int32_t L_61 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Security.Cryptography.ICryptoTransform::get_InputBlockSize() */, ICryptoTransform_t1_156_il2cpp_TypeInfo_var, L_60);
		Object_t * L_62 = (__this->____transform_2);
		NullCheck(L_62);
		int32_t L_63 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(3 /* System.Int32 System.Security.Cryptography.ICryptoTransform::get_OutputBlockSize() */, ICryptoTransform_t1_156_il2cpp_TypeInfo_var, L_62);
		V_6 = ((int32_t)((int32_t)((int32_t)((int32_t)1+(int32_t)((int32_t)((int32_t)L_59/(int32_t)L_61))))*(int32_t)L_63));
		ByteU5BU5D_t1_109* L_64 = (__this->____workingBlock_14);
		NullCheck(L_64);
		int32_t L_65 = V_6;
		if ((((int32_t)(((int32_t)((int32_t)(((Array_t *)L_64)->max_length))))) >= ((int32_t)L_65)))
		{
			goto IL_01d4;
		}
	}
	{
		ByteU5BU5D_t1_109* L_66 = (__this->____workingBlock_14);
		ByteU5BU5D_t1_109* L_67 = (__this->____workingBlock_14);
		NullCheck(L_67);
		Array_Clear_m1_1037(NULL /*static, unused*/, (Array_t *)(Array_t *)L_66, 0, (((int32_t)((int32_t)(((Array_t *)L_67)->max_length)))), /*hidden argument*/NULL);
		int32_t L_68 = V_6;
		__this->____workingBlock_14 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, L_68));
	}

IL_01d4:
	{
		int32_t L_69 = V_4;
		if ((((int32_t)L_69) <= ((int32_t)0)))
		{
			goto IL_0208;
		}
	}
	{
		Object_t * L_70 = (__this->____transform_2);
		ByteU5BU5D_t1_109* L_71 = ___buffer;
		int32_t L_72 = ___offset;
		int32_t L_73 = V_4;
		ByteU5BU5D_t1_109* L_74 = (__this->____workingBlock_14);
		NullCheck(L_70);
		int32_t L_75 = (int32_t)InterfaceFuncInvoker5< int32_t, ByteU5BU5D_t1_109*, int32_t, int32_t, ByteU5BU5D_t1_109*, int32_t >::Invoke(4 /* System.Int32 System.Security.Cryptography.ICryptoTransform::TransformBlock(System.Byte[],System.Int32,System.Int32,System.Byte[],System.Int32) */, ICryptoTransform_t1_156_il2cpp_TypeInfo_var, L_70, L_71, L_72, L_73, L_74, 0);
		V_7 = L_75;
		Stream_t1_405 * L_76 = (__this->____stream_1);
		ByteU5BU5D_t1_109* L_77 = (__this->____workingBlock_14);
		int32_t L_78 = V_7;
		NullCheck(L_76);
		VirtActionInvoker3< ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(24 /* System.Void System.IO.Stream::Write(System.Byte[],System.Int32,System.Int32) */, L_76, L_77, 0, L_78);
	}

IL_0208:
	{
		int32_t L_79 = V_5;
		if ((((int32_t)L_79) <= ((int32_t)0)))
		{
			goto IL_0223;
		}
	}
	{
		ByteU5BU5D_t1_109* L_80 = ___buffer;
		int32_t L_81 = V_0;
		int32_t L_82 = V_5;
		ByteU5BU5D_t1_109* L_83 = (__this->____workingBlock_14);
		int32_t L_84 = V_5;
		Buffer_BlockCopy_m1_13338(NULL /*static, unused*/, (Array_t *)(Array_t *)L_80, ((int32_t)((int32_t)L_81-(int32_t)L_82)), (Array_t *)(Array_t *)L_83, 0, L_84, /*hidden argument*/NULL);
	}

IL_0223:
	{
		int32_t L_85 = V_5;
		__this->____partialCount_7 = L_85;
		___count = 0;
		goto IL_027c;
	}

IL_0233:
	{
		Object_t * L_86 = (__this->____transform_2);
		NullCheck(L_86);
		int32_t L_87 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Security.Cryptography.ICryptoTransform::get_InputBlockSize() */, ICryptoTransform_t1_156_il2cpp_TypeInfo_var, L_86);
		int32_t L_88 = (__this->____partialCount_7);
		int32_t L_89 = ___count;
		int32_t L_90 = Math_Min_m1_14218(NULL /*static, unused*/, ((int32_t)((int32_t)L_87-(int32_t)L_88)), L_89, /*hidden argument*/NULL);
		V_8 = L_90;
		ByteU5BU5D_t1_109* L_91 = ___buffer;
		int32_t L_92 = V_2;
		ByteU5BU5D_t1_109* L_93 = (__this->____workingBlock_14);
		int32_t L_94 = (__this->____partialCount_7);
		int32_t L_95 = V_8;
		Buffer_BlockCopy_m1_13338(NULL /*static, unused*/, (Array_t *)(Array_t *)L_91, L_92, (Array_t *)(Array_t *)L_93, L_94, L_95, /*hidden argument*/NULL);
		int32_t L_96 = V_2;
		int32_t L_97 = V_8;
		V_2 = ((int32_t)((int32_t)L_96+(int32_t)L_97));
		int32_t L_98 = (__this->____partialCount_7);
		int32_t L_99 = V_8;
		__this->____partialCount_7 = ((int32_t)((int32_t)L_98+(int32_t)L_99));
		int32_t L_100 = ___count;
		int32_t L_101 = V_8;
		___count = ((int32_t)((int32_t)L_100-(int32_t)L_101));
	}

IL_027c:
	{
		int32_t L_102 = ___count;
		if ((((int32_t)L_102) > ((int32_t)0)))
		{
			goto IL_0103;
		}
	}
	{
		return;
	}
}
// System.Void System.Security.Cryptography.CryptoStream::Flush()
extern "C" void CryptoStream_Flush_m1_10215 (CryptoStream_t1_1192 * __this, const MethodInfo* method)
{
	{
		Stream_t1_405 * L_0 = (__this->____stream_1);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Stream_t1_405 * L_1 = (__this->____stream_1);
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(19 /* System.Void System.IO.Stream::Flush() */, L_1);
	}

IL_0016:
	{
		return;
	}
}
// System.Void System.Security.Cryptography.CryptoStream::FlushFinalBlock()
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern TypeInfo* ICryptoTransform_t1_156_il2cpp_TypeInfo_var;
extern TypeInfo* CryptoStream_t1_1192_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2687;
extern Il2CppCodeGenString* _stringLiteral2688;
extern "C" void CryptoStream_FlushFinalBlock_m1_10216 (CryptoStream_t1_1192 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		ICryptoTransform_t1_156_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(202);
		CryptoStream_t1_1192_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(266);
		_stringLiteral2687 = il2cpp_codegen_string_literal_from_index(2687);
		_stringLiteral2688 = il2cpp_codegen_string_literal_from_index(2688);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	{
		bool L_0 = (__this->____flushedFinalBlock_6);
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2687, /*hidden argument*/NULL);
		NotSupportedException_t1_1583 * L_2 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14417(L_2, L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001b:
	{
		bool L_3 = (__this->____disposed_5);
		if (!L_3)
		{
			goto IL_0036;
		}
	}
	{
		String_t* L_4 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2688, /*hidden argument*/NULL);
		NotSupportedException_t1_1583 * L_5 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14417(L_5, L_4, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_5);
	}

IL_0036:
	{
		int32_t L_6 = (__this->____mode_3);
		if ((((int32_t)L_6) == ((int32_t)1)))
		{
			goto IL_0043;
		}
	}
	{
		return;
	}

IL_0043:
	{
		__this->____flushedFinalBlock_6 = 1;
		Object_t * L_7 = (__this->____transform_2);
		ByteU5BU5D_t1_109* L_8 = (__this->____workingBlock_14);
		int32_t L_9 = (__this->____partialCount_7);
		NullCheck(L_7);
		ByteU5BU5D_t1_109* L_10 = (ByteU5BU5D_t1_109*)InterfaceFuncInvoker3< ByteU5BU5D_t1_109*, ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(5 /* System.Byte[] System.Security.Cryptography.ICryptoTransform::TransformFinalBlock(System.Byte[],System.Int32,System.Int32) */, ICryptoTransform_t1_156_il2cpp_TypeInfo_var, L_7, L_8, 0, L_9);
		V_0 = L_10;
		Stream_t1_405 * L_11 = (__this->____stream_1);
		if (!L_11)
		{
			goto IL_00a9;
		}
	}
	{
		Stream_t1_405 * L_12 = (__this->____stream_1);
		ByteU5BU5D_t1_109* L_13 = V_0;
		ByteU5BU5D_t1_109* L_14 = V_0;
		NullCheck(L_14);
		NullCheck(L_12);
		VirtActionInvoker3< ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(24 /* System.Void System.IO.Stream::Write(System.Byte[],System.Int32,System.Int32) */, L_12, L_13, 0, (((int32_t)((int32_t)(((Array_t *)L_14)->max_length)))));
		Stream_t1_405 * L_15 = (__this->____stream_1);
		if (!((CryptoStream_t1_1192 *)IsInstClass(L_15, CryptoStream_t1_1192_il2cpp_TypeInfo_var)))
		{
			goto IL_009e;
		}
	}
	{
		Stream_t1_405 * L_16 = (__this->____stream_1);
		NullCheck(((CryptoStream_t1_1192 *)IsInstClass(L_16, CryptoStream_t1_1192_il2cpp_TypeInfo_var)));
		CryptoStream_FlushFinalBlock_m1_10216(((CryptoStream_t1_1192 *)IsInstClass(L_16, CryptoStream_t1_1192_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
	}

IL_009e:
	{
		Stream_t1_405 * L_17 = (__this->____stream_1);
		NullCheck(L_17);
		VirtActionInvoker0::Invoke(19 /* System.Void System.IO.Stream::Flush() */, L_17);
	}

IL_00a9:
	{
		ByteU5BU5D_t1_109* L_18 = V_0;
		ByteU5BU5D_t1_109* L_19 = V_0;
		NullCheck(L_19);
		Array_Clear_m1_1037(NULL /*static, unused*/, (Array_t *)(Array_t *)L_18, 0, (((int32_t)((int32_t)(((Array_t *)L_19)->max_length)))), /*hidden argument*/NULL);
		return;
	}
}
// System.Int64 System.Security.Cryptography.CryptoStream::Seek(System.Int64,System.IO.SeekOrigin)
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2689;
extern "C" int64_t CryptoStream_Seek_m1_10217 (CryptoStream_t1_1192 * __this, int64_t ___offset, int32_t ___origin, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		_stringLiteral2689 = il2cpp_codegen_string_literal_from_index(2689);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14417(L_0, _stringLiteral2689, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.Cryptography.CryptoStream::SetLength(System.Int64)
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2690;
extern "C" void CryptoStream_SetLength_m1_10218 (CryptoStream_t1_1192 * __this, int64_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		_stringLiteral2690 = il2cpp_codegen_string_literal_from_index(2690);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14417(L_0, _stringLiteral2690, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.Cryptography.CryptoStream::Dispose(System.Boolean)
extern "C" void CryptoStream_Dispose_m1_10219 (CryptoStream_t1_1192 * __this, bool ___disposing, const MethodInfo* method)
{
	{
		bool L_0 = (__this->____disposed_5);
		if (L_0)
		{
			goto IL_006b;
		}
	}
	{
		__this->____disposed_5 = 1;
		ByteU5BU5D_t1_109* L_1 = (__this->____workingBlock_14);
		if (!L_1)
		{
			goto IL_0031;
		}
	}
	{
		ByteU5BU5D_t1_109* L_2 = (__this->____workingBlock_14);
		ByteU5BU5D_t1_109* L_3 = (__this->____workingBlock_14);
		NullCheck(L_3);
		Array_Clear_m1_1037(NULL /*static, unused*/, (Array_t *)(Array_t *)L_2, 0, (((int32_t)((int32_t)(((Array_t *)L_3)->max_length)))), /*hidden argument*/NULL);
	}

IL_0031:
	{
		ByteU5BU5D_t1_109* L_4 = (__this->____currentBlock_4);
		if (!L_4)
		{
			goto IL_0050;
		}
	}
	{
		ByteU5BU5D_t1_109* L_5 = (__this->____currentBlock_4);
		ByteU5BU5D_t1_109* L_6 = (__this->____currentBlock_4);
		NullCheck(L_6);
		Array_Clear_m1_1037(NULL /*static, unused*/, (Array_t *)(Array_t *)L_5, 0, (((int32_t)((int32_t)(((Array_t *)L_6)->max_length)))), /*hidden argument*/NULL);
	}

IL_0050:
	{
		bool L_7 = ___disposing;
		if (!L_7)
		{
			goto IL_006b;
		}
	}
	{
		__this->____stream_1 = (Stream_t1_405 *)NULL;
		__this->____workingBlock_14 = (ByteU5BU5D_t1_109*)NULL;
		__this->____currentBlock_4 = (ByteU5BU5D_t1_109*)NULL;
	}

IL_006b:
	{
		return;
	}
}
// System.Void System.Security.Cryptography.CryptographicException::.ctor()
extern Il2CppCodeGenString* _stringLiteral2691;
extern "C" void CryptographicException__ctor_m1_10220 (CryptographicException_t1_1194 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral2691 = il2cpp_codegen_string_literal_from_index(2691);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2691, /*hidden argument*/NULL);
		SystemException__ctor_m1_14605(__this, L_0, /*hidden argument*/NULL);
		Exception_set_HResult_m1_1245(__this, ((int32_t)-2146233296), /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.Cryptography.CryptographicException::.ctor(System.Int32)
extern "C" void CryptographicException__ctor_m1_10221 (CryptographicException_t1_1194 * __this, int32_t ___hr, const MethodInfo* method)
{
	{
		SystemException__ctor_m1_14604(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___hr;
		Exception_set_HResult_m1_1245(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.Cryptography.CryptographicException::.ctor(System.String)
extern "C" void CryptographicException__ctor_m1_10222 (CryptographicException_t1_1194 * __this, String_t* ___message, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message;
		SystemException__ctor_m1_14605(__this, L_0, /*hidden argument*/NULL);
		Exception_set_HResult_m1_1245(__this, ((int32_t)-2146233296), /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.Cryptography.CryptographicException::.ctor(System.String,System.Exception)
extern "C" void CryptographicException__ctor_m1_10223 (CryptographicException_t1_1194 * __this, String_t* ___message, Exception_t1_33 * ___inner, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message;
		Exception_t1_33 * L_1 = ___inner;
		SystemException__ctor_m1_14607(__this, L_0, L_1, /*hidden argument*/NULL);
		Exception_set_HResult_m1_1245(__this, ((int32_t)-2146233296), /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.Cryptography.CryptographicException::.ctor(System.String,System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void CryptographicException__ctor_m1_10224 (CryptographicException_t1_1194 * __this, String_t* ___format, String_t* ___insert, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___format;
		String_t* L_1 = ___insert;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Format_m1_548(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		SystemException__ctor_m1_14605(__this, L_2, /*hidden argument*/NULL);
		Exception_set_HResult_m1_1245(__this, ((int32_t)-2146233296), /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.Cryptography.CryptographicException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void CryptographicException__ctor_m1_10225 (CryptographicException_t1_1194 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method)
{
	{
		SerializationInfo_t1_293 * L_0 = ___info;
		StreamingContext_t1_1050  L_1 = ___context;
		SystemException__ctor_m1_14606(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.Cryptography.CryptographicUnexpectedOperationException::.ctor()
extern Il2CppCodeGenString* _stringLiteral2692;
extern "C" void CryptographicUnexpectedOperationException__ctor_m1_10226 (CryptographicUnexpectedOperationException_t1_1195 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral2692 = il2cpp_codegen_string_literal_from_index(2692);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2692, /*hidden argument*/NULL);
		CryptographicException__ctor_m1_10222(__this, L_0, /*hidden argument*/NULL);
		Exception_set_HResult_m1_1245(__this, ((int32_t)-2146233295), /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.Cryptography.CryptographicUnexpectedOperationException::.ctor(System.String)
extern "C" void CryptographicUnexpectedOperationException__ctor_m1_10227 (CryptographicUnexpectedOperationException_t1_1195 * __this, String_t* ___message, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message;
		CryptographicException__ctor_m1_10222(__this, L_0, /*hidden argument*/NULL);
		Exception_set_HResult_m1_1245(__this, ((int32_t)-2146233295), /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.Cryptography.CryptographicUnexpectedOperationException::.ctor(System.String,System.Exception)
extern "C" void CryptographicUnexpectedOperationException__ctor_m1_10228 (CryptographicUnexpectedOperationException_t1_1195 * __this, String_t* ___message, Exception_t1_33 * ___inner, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message;
		Exception_t1_33 * L_1 = ___inner;
		CryptographicException__ctor_m1_10223(__this, L_0, L_1, /*hidden argument*/NULL);
		Exception_set_HResult_m1_1245(__this, ((int32_t)-2146233295), /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.Cryptography.CryptographicUnexpectedOperationException::.ctor(System.String,System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void CryptographicUnexpectedOperationException__ctor_m1_10229 (CryptographicUnexpectedOperationException_t1_1195 * __this, String_t* ___format, String_t* ___insert, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___format;
		String_t* L_1 = ___insert;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Format_m1_548(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		CryptographicException__ctor_m1_10222(__this, L_2, /*hidden argument*/NULL);
		Exception_set_HResult_m1_1245(__this, ((int32_t)-2146233295), /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.Cryptography.CryptographicUnexpectedOperationException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void CryptographicUnexpectedOperationException__ctor_m1_10230 (CryptographicUnexpectedOperationException_t1_1195 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method)
{
	{
		SerializationInfo_t1_293 * L_0 = ___info;
		StreamingContext_t1_1050  L_1 = ___context;
		CryptographicException__ctor_m1_10225(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.Cryptography.CspKeyContainerInfo::.ctor(System.Security.Cryptography.CspParameters)
extern "C" void CspKeyContainerInfo__ctor_m1_10231 (CspKeyContainerInfo_t1_1196 * __this, CspParameters_t1_164 * ___parameters, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		CspParameters_t1_164 * L_0 = ___parameters;
		__this->____params_0 = L_0;
		__this->____random_1 = 1;
		return;
	}
}
// System.Boolean System.Security.Cryptography.CspKeyContainerInfo::get_Accessible()
extern "C" bool CspKeyContainerInfo_get_Accessible_m1_10232 (CspKeyContainerInfo_t1_1196 * __this, const MethodInfo* method)
{
	{
		return 1;
	}
}
// System.Security.AccessControl.CryptoKeySecurity System.Security.Cryptography.CspKeyContainerInfo::get_CryptoKeySecurity()
extern "C" CryptoKeySecurity_t1_1142 * CspKeyContainerInfo_get_CryptoKeySecurity_m1_10233 (CspKeyContainerInfo_t1_1196 * __this, const MethodInfo* method)
{
	{
		return (CryptoKeySecurity_t1_1142 *)NULL;
	}
}
// System.Boolean System.Security.Cryptography.CspKeyContainerInfo::get_Exportable()
extern "C" bool CspKeyContainerInfo_get_Exportable_m1_10234 (CspKeyContainerInfo_t1_1196 * __this, const MethodInfo* method)
{
	{
		return 1;
	}
}
// System.Boolean System.Security.Cryptography.CspKeyContainerInfo::get_HardwareDevice()
extern "C" bool CspKeyContainerInfo_get_HardwareDevice_m1_10235 (CspKeyContainerInfo_t1_1196 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.String System.Security.Cryptography.CspKeyContainerInfo::get_KeyContainerName()
extern "C" String_t* CspKeyContainerInfo_get_KeyContainerName_m1_10236 (CspKeyContainerInfo_t1_1196 * __this, const MethodInfo* method)
{
	{
		CspParameters_t1_164 * L_0 = (__this->____params_0);
		NullCheck(L_0);
		String_t* L_1 = (L_0->___KeyContainerName_1);
		return L_1;
	}
}
// System.Security.Cryptography.KeyNumber System.Security.Cryptography.CspKeyContainerInfo::get_KeyNumber()
extern "C" int32_t CspKeyContainerInfo_get_KeyNumber_m1_10237 (CspKeyContainerInfo_t1_1196 * __this, const MethodInfo* method)
{
	{
		CspParameters_t1_164 * L_0 = (__this->____params_0);
		NullCheck(L_0);
		int32_t L_1 = (L_0->___KeyNumber_2);
		return (int32_t)(L_1);
	}
}
// System.Boolean System.Security.Cryptography.CspKeyContainerInfo::get_MachineKeyStore()
extern "C" bool CspKeyContainerInfo_get_MachineKeyStore_m1_10238 (CspKeyContainerInfo_t1_1196 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Boolean System.Security.Cryptography.CspKeyContainerInfo::get_Protected()
extern "C" bool CspKeyContainerInfo_get_Protected_m1_10239 (CspKeyContainerInfo_t1_1196 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.String System.Security.Cryptography.CspKeyContainerInfo::get_ProviderName()
extern "C" String_t* CspKeyContainerInfo_get_ProviderName_m1_10240 (CspKeyContainerInfo_t1_1196 * __this, const MethodInfo* method)
{
	{
		CspParameters_t1_164 * L_0 = (__this->____params_0);
		NullCheck(L_0);
		String_t* L_1 = (L_0->___ProviderName_3);
		return L_1;
	}
}
// System.Int32 System.Security.Cryptography.CspKeyContainerInfo::get_ProviderType()
extern "C" int32_t CspKeyContainerInfo_get_ProviderType_m1_10241 (CspKeyContainerInfo_t1_1196 * __this, const MethodInfo* method)
{
	{
		CspParameters_t1_164 * L_0 = (__this->____params_0);
		NullCheck(L_0);
		int32_t L_1 = (L_0->___ProviderType_4);
		return L_1;
	}
}
// System.Boolean System.Security.Cryptography.CspKeyContainerInfo::get_RandomlyGenerated()
extern "C" bool CspKeyContainerInfo_get_RandomlyGenerated_m1_10242 (CspKeyContainerInfo_t1_1196 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->____random_1);
		return L_0;
	}
}
// System.Boolean System.Security.Cryptography.CspKeyContainerInfo::get_Removable()
extern "C" bool CspKeyContainerInfo_get_Removable_m1_10243 (CspKeyContainerInfo_t1_1196 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.String System.Security.Cryptography.CspKeyContainerInfo::get_UniqueKeyContainerName()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral313;
extern "C" String_t* CspKeyContainerInfo_get_UniqueKeyContainerName_m1_10244 (CspKeyContainerInfo_t1_1196 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral313 = il2cpp_codegen_string_literal_from_index(313);
		s_Il2CppMethodIntialized = true;
	}
	{
		CspParameters_t1_164 * L_0 = (__this->____params_0);
		NullCheck(L_0);
		String_t* L_1 = (L_0->___ProviderName_3);
		CspParameters_t1_164 * L_2 = (__this->____params_0);
		NullCheck(L_2);
		String_t* L_3 = (L_2->___KeyContainerName_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m1_560(NULL /*static, unused*/, L_1, _stringLiteral313, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void System.Security.Cryptography.CspParameters::.ctor()
extern "C" void CspParameters__ctor_m1_10245 (CspParameters_t1_164 * __this, const MethodInfo* method)
{
	{
		CspParameters__ctor_m1_10246(__this, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.Cryptography.CspParameters::.ctor(System.Int32)
extern "C" void CspParameters__ctor_m1_10246 (CspParameters_t1_164 * __this, int32_t ___dwTypeIn, const MethodInfo* method)
{
	{
		int32_t L_0 = ___dwTypeIn;
		CspParameters__ctor_m1_10247(__this, L_0, (String_t*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.Cryptography.CspParameters::.ctor(System.Int32,System.String)
extern "C" void CspParameters__ctor_m1_10247 (CspParameters_t1_164 * __this, int32_t ___dwTypeIn, String_t* ___strProviderNameIn, const MethodInfo* method)
{
	{
		int32_t L_0 = ___dwTypeIn;
		CspParameters__ctor_m1_10248(__this, L_0, (String_t*)NULL, (String_t*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.Cryptography.CspParameters::.ctor(System.Int32,System.String,System.String)
extern "C" void CspParameters__ctor_m1_10248 (CspParameters_t1_164 * __this, int32_t ___dwTypeIn, String_t* ___strProviderNameIn, String_t* ___strContainerNameIn, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___dwTypeIn;
		__this->___ProviderType_4 = L_0;
		String_t* L_1 = ___strProviderNameIn;
		__this->___ProviderName_3 = L_1;
		String_t* L_2 = ___strContainerNameIn;
		__this->___KeyContainerName_1 = L_2;
		__this->___KeyNumber_2 = (-1);
		return;
	}
}
// System.Void System.Security.Cryptography.CspParameters::.ctor(System.Int32,System.String,System.String,System.Security.AccessControl.CryptoKeySecurity,System.IntPtr)
extern "C" void CspParameters__ctor_m1_10249 (CspParameters_t1_164 * __this, int32_t ___providerType, String_t* ___providerName, String_t* ___keyContainerName, CryptoKeySecurity_t1_1142 * ___cryptoKeySecurity, IntPtr_t ___parentWindowHandle, const MethodInfo* method)
{
	{
		int32_t L_0 = ___providerType;
		String_t* L_1 = ___providerName;
		String_t* L_2 = ___keyContainerName;
		CspParameters__ctor_m1_10248(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		CryptoKeySecurity_t1_1142 * L_3 = ___cryptoKeySecurity;
		if (!L_3)
		{
			goto IL_0018;
		}
	}
	{
		CryptoKeySecurity_t1_1142 * L_4 = ___cryptoKeySecurity;
		CspParameters_set_CryptoKeySecurity_m1_10254(__this, L_4, /*hidden argument*/NULL);
	}

IL_0018:
	{
		IntPtr_t L_5 = ___parentWindowHandle;
		__this->____windowHandle_6 = L_5;
		return;
	}
}
// System.Void System.Security.Cryptography.CspParameters::.ctor(System.Int32,System.String,System.String,System.Security.AccessControl.CryptoKeySecurity,System.Security.SecureString)
extern "C" void CspParameters__ctor_m1_10250 (CspParameters_t1_164 * __this, int32_t ___providerType, String_t* ___providerName, String_t* ___keyContainerName, CryptoKeySecurity_t1_1142 * ___cryptoKeySecurity, SecureString_t1_1197 * ___keyPassword, const MethodInfo* method)
{
	{
		int32_t L_0 = ___providerType;
		String_t* L_1 = ___providerName;
		String_t* L_2 = ___keyContainerName;
		CspParameters__ctor_m1_10248(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		CryptoKeySecurity_t1_1142 * L_3 = ___cryptoKeySecurity;
		if (!L_3)
		{
			goto IL_0018;
		}
	}
	{
		CryptoKeySecurity_t1_1142 * L_4 = ___cryptoKeySecurity;
		CspParameters_set_CryptoKeySecurity_m1_10254(__this, L_4, /*hidden argument*/NULL);
	}

IL_0018:
	{
		SecureString_t1_1197 * L_5 = ___keyPassword;
		__this->____password_5 = L_5;
		return;
	}
}
// System.Security.Cryptography.CspProviderFlags System.Security.Cryptography.CspParameters::get_Flags()
extern "C" int32_t CspParameters_get_Flags_m1_10251 (CspParameters_t1_164 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->____Flags_0);
		return L_0;
	}
}
// System.Void System.Security.Cryptography.CspParameters::set_Flags(System.Security.Cryptography.CspProviderFlags)
extern "C" void CspParameters_set_Flags_m1_10252 (CspParameters_t1_164 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->____Flags_0 = L_0;
		return;
	}
}
// System.Security.AccessControl.CryptoKeySecurity System.Security.Cryptography.CspParameters::get_CryptoKeySecurity()
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" CryptoKeySecurity_t1_1142 * CspParameters_get_CryptoKeySecurity_m1_10253 (CspParameters_t1_164 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Security.Cryptography.CspParameters::set_CryptoKeySecurity(System.Security.AccessControl.CryptoKeySecurity)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void CspParameters_set_CryptoKeySecurity_m1_10254 (CspParameters_t1_164 * __this, CryptoKeySecurity_t1_1142 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Security.SecureString System.Security.Cryptography.CspParameters::get_KeyPassword()
extern "C" SecureString_t1_1197 * CspParameters_get_KeyPassword_m1_10255 (CspParameters_t1_164 * __this, const MethodInfo* method)
{
	{
		SecureString_t1_1197 * L_0 = (__this->____password_5);
		return L_0;
	}
}
// System.Void System.Security.Cryptography.CspParameters::set_KeyPassword(System.Security.SecureString)
extern "C" void CspParameters_set_KeyPassword_m1_10256 (CspParameters_t1_164 * __this, SecureString_t1_1197 * ___value, const MethodInfo* method)
{
	{
		SecureString_t1_1197 * L_0 = ___value;
		__this->____password_5 = L_0;
		return;
	}
}
// System.IntPtr System.Security.Cryptography.CspParameters::get_ParentWindowHandle()
extern "C" IntPtr_t CspParameters_get_ParentWindowHandle_m1_10257 (CspParameters_t1_164 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = (__this->____windowHandle_6);
		return L_0;
	}
}
// System.Void System.Security.Cryptography.CspParameters::set_ParentWindowHandle(System.IntPtr)
extern "C" void CspParameters_set_ParentWindowHandle_m1_10258 (CspParameters_t1_164 * __this, IntPtr_t ___value, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = ___value;
		__this->____windowHandle_6 = L_0;
		return;
	}
}
// System.Void System.Security.Cryptography.DES::.ctor()
extern TypeInfo* KeySizesU5BU5D_t1_1182_il2cpp_TypeInfo_var;
extern TypeInfo* KeySizes_t1_1219_il2cpp_TypeInfo_var;
extern "C" void DES__ctor_m1_10259 (DES_t1_1199 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		KeySizesU5BU5D_t1_1182_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(203);
		KeySizes_t1_1219_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(204);
		s_Il2CppMethodIntialized = true;
	}
	{
		SymmetricAlgorithm__ctor_m1_10724(__this, /*hidden argument*/NULL);
		((SymmetricAlgorithm_t1_166 *)__this)->___KeySizeValue_2 = ((int32_t)64);
		((SymmetricAlgorithm_t1_166 *)__this)->___BlockSizeValue_0 = ((int32_t)64);
		((SymmetricAlgorithm_t1_166 *)__this)->___FeedbackSizeValue_6 = 8;
		((SymmetricAlgorithm_t1_166 *)__this)->___LegalKeySizesValue_5 = ((KeySizesU5BU5D_t1_1182*)SZArrayNew(KeySizesU5BU5D_t1_1182_il2cpp_TypeInfo_var, 1));
		KeySizesU5BU5D_t1_1182* L_0 = (((SymmetricAlgorithm_t1_166 *)__this)->___LegalKeySizesValue_5);
		KeySizes_t1_1219 * L_1 = (KeySizes_t1_1219 *)il2cpp_codegen_object_new (KeySizes_t1_1219_il2cpp_TypeInfo_var);
		KeySizes__ctor_m1_10393(L_1, ((int32_t)64), ((int32_t)64), 0, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((KeySizes_t1_1219 **)(KeySizes_t1_1219 **)SZArrayLdElema(L_0, 0, sizeof(KeySizes_t1_1219 *))) = (KeySizes_t1_1219 *)L_1;
		((SymmetricAlgorithm_t1_166 *)__this)->___LegalBlockSizesValue_4 = ((KeySizesU5BU5D_t1_1182*)SZArrayNew(KeySizesU5BU5D_t1_1182_il2cpp_TypeInfo_var, 1));
		KeySizesU5BU5D_t1_1182* L_2 = (((SymmetricAlgorithm_t1_166 *)__this)->___LegalBlockSizesValue_4);
		KeySizes_t1_1219 * L_3 = (KeySizes_t1_1219 *)il2cpp_codegen_object_new (KeySizes_t1_1219_il2cpp_TypeInfo_var);
		KeySizes__ctor_m1_10393(L_3, ((int32_t)64), ((int32_t)64), 0, /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_3);
		*((KeySizes_t1_1219 **)(KeySizes_t1_1219 **)SZArrayLdElema(L_2, 0, sizeof(KeySizes_t1_1219 *))) = (KeySizes_t1_1219 *)L_3;
		return;
	}
}
// System.Void System.Security.Cryptography.DES::.cctor()
extern TypeInfo* ByteU5BU2CU5D_t1_1200_il2cpp_TypeInfo_var;
extern TypeInfo* DES_t1_1199_il2cpp_TypeInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t1_1661____U24U24fieldU2D35_35_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t1_1661____U24U24fieldU2D36_36_FieldInfo_var;
extern "C" void DES__cctor_m1_10260 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ByteU5BU2CU5D_t1_1200_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(874);
		DES_t1_1199_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(875);
		U3CPrivateImplementationDetailsU3E_t1_1661____U24U24fieldU2D35_35_FieldInfo_var = il2cpp_codegen_field_info_from_index(32, 35);
		U3CPrivateImplementationDetailsU3E_t1_1661____U24U24fieldU2D36_36_FieldInfo_var = il2cpp_codegen_field_info_from_index(32, 36);
		s_Il2CppMethodIntialized = true;
	}
	{
		ByteU5BU2CU5D_t1_1200* L_0 = (ByteU5BU2CU5D_t1_1200*)GenArrayNew2(ByteU5BU2CU5D_t1_1200_il2cpp_TypeInfo_var, 4, 8);
		ByteU5BU2CU5D_t1_1200* L_1 = L_0;
		RuntimeHelpers_InitializeArray_m1_1387(NULL /*static, unused*/, (Array_t *)(Array_t *)L_1, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t1_1661____U24U24fieldU2D35_35_FieldInfo_var), /*hidden argument*/NULL);
		((DES_t1_1199_StaticFields*)DES_t1_1199_il2cpp_TypeInfo_var->static_fields)->___weakKeys_11 = L_1;
		ByteU5BU2CU5D_t1_1200* L_2 = (ByteU5BU2CU5D_t1_1200*)GenArrayNew2(ByteU5BU2CU5D_t1_1200_il2cpp_TypeInfo_var, ((int32_t)12), 8);
		ByteU5BU2CU5D_t1_1200* L_3 = L_2;
		RuntimeHelpers_InitializeArray_m1_1387(NULL /*static, unused*/, (Array_t *)(Array_t *)L_3, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t1_1661____U24U24fieldU2D36_36_FieldInfo_var), /*hidden argument*/NULL);
		((DES_t1_1199_StaticFields*)DES_t1_1199_il2cpp_TypeInfo_var->static_fields)->___semiWeakKeys_12 = L_3;
		return;
	}
}
// System.Security.Cryptography.DES System.Security.Cryptography.DES::Create()
extern TypeInfo* DES_t1_1199_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2576;
extern "C" DES_t1_1199 * DES_Create_m1_10261 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DES_t1_1199_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(875);
		_stringLiteral2576 = il2cpp_codegen_string_literal_from_index(2576);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DES_t1_1199_il2cpp_TypeInfo_var);
		DES_t1_1199 * L_0 = DES_Create_m1_10262(NULL /*static, unused*/, _stringLiteral2576, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Security.Cryptography.DES System.Security.Cryptography.DES::Create(System.String)
extern TypeInfo* CryptoConfig_t1_1191_il2cpp_TypeInfo_var;
extern TypeInfo* DES_t1_1199_il2cpp_TypeInfo_var;
extern "C" DES_t1_1199 * DES_Create_m1_10262 (Object_t * __this /* static, unused */, String_t* ___algName, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CryptoConfig_t1_1191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(191);
		DES_t1_1199_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(875);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___algName;
		IL2CPP_RUNTIME_CLASS_INIT(CryptoConfig_t1_1191_il2cpp_TypeInfo_var);
		Object_t * L_1 = CryptoConfig_CreateFromName_m1_10198(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return ((DES_t1_1199 *)CastclassClass(L_1, DES_t1_1199_il2cpp_TypeInfo_var));
	}
}
// System.Boolean System.Security.Cryptography.DES::IsWeakKey(System.Byte[])
extern TypeInfo* CryptographicException_t1_1194_il2cpp_TypeInfo_var;
extern TypeInfo* DES_t1_1199_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2693;
extern Il2CppCodeGenString* _stringLiteral2694;
extern "C" bool DES_IsWeakKey_m1_10263 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___rgbKey, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CryptographicException_t1_1194_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(197);
		DES_t1_1199_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(875);
		_stringLiteral2693 = il2cpp_codegen_string_literal_from_index(2693);
		_stringLiteral2694 = il2cpp_codegen_string_literal_from_index(2694);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		ByteU5BU5D_t1_109* L_0 = ___rgbKey;
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2693, /*hidden argument*/NULL);
		CryptographicException_t1_1194 * L_2 = (CryptographicException_t1_1194 *)il2cpp_codegen_object_new (CryptographicException_t1_1194_il2cpp_TypeInfo_var);
		CryptographicException__ctor_m1_10222(L_2, L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0016:
	{
		ByteU5BU5D_t1_109* L_3 = ___rgbKey;
		NullCheck(L_3);
		if ((((int32_t)(((int32_t)((int32_t)(((Array_t *)L_3)->max_length))))) == ((int32_t)8)))
		{
			goto IL_002f;
		}
	}
	{
		String_t* L_4 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2694, /*hidden argument*/NULL);
		CryptographicException_t1_1194 * L_5 = (CryptographicException_t1_1194 *)il2cpp_codegen_object_new (CryptographicException_t1_1194_il2cpp_TypeInfo_var);
		CryptographicException__ctor_m1_10222(L_5, L_4, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_5);
	}

IL_002f:
	{
		V_0 = 0;
		goto IL_0073;
	}

IL_0036:
	{
		ByteU5BU5D_t1_109* L_6 = ___rgbKey;
		int32_t L_7 = V_0;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		V_3 = ((int32_t)((int32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_6, L_8, sizeof(uint8_t)))|(int32_t)((int32_t)17)));
		int32_t L_9 = V_3;
		if ((((int32_t)L_9) == ((int32_t)((int32_t)17))))
		{
			goto IL_0068;
		}
	}
	{
		int32_t L_10 = V_3;
		if ((((int32_t)L_10) == ((int32_t)((int32_t)31))))
		{
			goto IL_0068;
		}
	}
	{
		int32_t L_11 = V_3;
		if ((((int32_t)L_11) == ((int32_t)((int32_t)241))))
		{
			goto IL_0068;
		}
	}
	{
		int32_t L_12 = V_3;
		if ((((int32_t)L_12) == ((int32_t)((int32_t)255))))
		{
			goto IL_0068;
		}
	}
	{
		goto IL_006d;
	}

IL_0068:
	{
		goto IL_006f;
	}

IL_006d:
	{
		return 0;
	}

IL_006f:
	{
		int32_t L_13 = V_0;
		V_0 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0073:
	{
		int32_t L_14 = V_0;
		ByteU5BU5D_t1_109* L_15 = ___rgbKey;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_15)->max_length)))))))
		{
			goto IL_0036;
		}
	}
	{
		V_1 = 0;
		goto IL_00bf;
	}

IL_0083:
	{
		V_2 = 0;
		goto IL_00a9;
	}

IL_008a:
	{
		ByteU5BU5D_t1_109* L_16 = ___rgbKey;
		int32_t L_17 = V_2;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		int32_t L_18 = L_17;
		IL2CPP_RUNTIME_CLASS_INIT(DES_t1_1199_il2cpp_TypeInfo_var);
		ByteU5BU2CU5D_t1_1200* L_19 = ((DES_t1_1199_StaticFields*)DES_t1_1199_il2cpp_TypeInfo_var->static_fields)->___weakKeys_11;
		int32_t L_20 = V_1;
		int32_t L_21 = V_2;
		NullCheck(L_19);
		uint8_t L_22 = GenArrayGet2(L_19, L_20, L_21, uint8_t);;
		if ((((int32_t)((int32_t)((int32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_16, L_18, sizeof(uint8_t)))^(int32_t)L_22))) <= ((int32_t)1)))
		{
			goto IL_00a5;
		}
	}
	{
		goto IL_00b2;
	}

IL_00a5:
	{
		int32_t L_23 = V_2;
		V_2 = ((int32_t)((int32_t)L_23+(int32_t)1));
	}

IL_00a9:
	{
		int32_t L_24 = V_2;
		ByteU5BU5D_t1_109* L_25 = ___rgbKey;
		NullCheck(L_25);
		if ((((int32_t)L_24) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_25)->max_length)))))))
		{
			goto IL_008a;
		}
	}

IL_00b2:
	{
		int32_t L_26 = V_2;
		if ((!(((uint32_t)L_26) == ((uint32_t)8))))
		{
			goto IL_00bb;
		}
	}
	{
		return 1;
	}

IL_00bb:
	{
		int32_t L_27 = V_1;
		V_1 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_00bf:
	{
		int32_t L_28 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(DES_t1_1199_il2cpp_TypeInfo_var);
		ByteU5BU2CU5D_t1_1200* L_29 = ((DES_t1_1199_StaticFields*)DES_t1_1199_il2cpp_TypeInfo_var->static_fields)->___weakKeys_11;
		NullCheck(L_29);
		int32_t L_30 = Array_get_Length_m1_992(L_29, /*hidden argument*/NULL);
		if ((((int32_t)L_28) < ((int32_t)((int32_t)((int32_t)L_30>>(int32_t)3)))))
		{
			goto IL_0083;
		}
	}
	{
		return 0;
	}
}
// System.Boolean System.Security.Cryptography.DES::IsSemiWeakKey(System.Byte[])
extern TypeInfo* CryptographicException_t1_1194_il2cpp_TypeInfo_var;
extern TypeInfo* DES_t1_1199_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2693;
extern Il2CppCodeGenString* _stringLiteral2694;
extern "C" bool DES_IsSemiWeakKey_m1_10264 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___rgbKey, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CryptographicException_t1_1194_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(197);
		DES_t1_1199_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(875);
		_stringLiteral2693 = il2cpp_codegen_string_literal_from_index(2693);
		_stringLiteral2694 = il2cpp_codegen_string_literal_from_index(2694);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		ByteU5BU5D_t1_109* L_0 = ___rgbKey;
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2693, /*hidden argument*/NULL);
		CryptographicException_t1_1194 * L_2 = (CryptographicException_t1_1194 *)il2cpp_codegen_object_new (CryptographicException_t1_1194_il2cpp_TypeInfo_var);
		CryptographicException__ctor_m1_10222(L_2, L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0016:
	{
		ByteU5BU5D_t1_109* L_3 = ___rgbKey;
		NullCheck(L_3);
		if ((((int32_t)(((int32_t)((int32_t)(((Array_t *)L_3)->max_length))))) == ((int32_t)8)))
		{
			goto IL_002f;
		}
	}
	{
		String_t* L_4 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2694, /*hidden argument*/NULL);
		CryptographicException_t1_1194 * L_5 = (CryptographicException_t1_1194 *)il2cpp_codegen_object_new (CryptographicException_t1_1194_il2cpp_TypeInfo_var);
		CryptographicException__ctor_m1_10222(L_5, L_4, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_5);
	}

IL_002f:
	{
		V_0 = 0;
		goto IL_0073;
	}

IL_0036:
	{
		ByteU5BU5D_t1_109* L_6 = ___rgbKey;
		int32_t L_7 = V_0;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		V_3 = ((int32_t)((int32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_6, L_8, sizeof(uint8_t)))|(int32_t)((int32_t)17)));
		int32_t L_9 = V_3;
		if ((((int32_t)L_9) == ((int32_t)((int32_t)17))))
		{
			goto IL_0068;
		}
	}
	{
		int32_t L_10 = V_3;
		if ((((int32_t)L_10) == ((int32_t)((int32_t)31))))
		{
			goto IL_0068;
		}
	}
	{
		int32_t L_11 = V_3;
		if ((((int32_t)L_11) == ((int32_t)((int32_t)241))))
		{
			goto IL_0068;
		}
	}
	{
		int32_t L_12 = V_3;
		if ((((int32_t)L_12) == ((int32_t)((int32_t)255))))
		{
			goto IL_0068;
		}
	}
	{
		goto IL_006d;
	}

IL_0068:
	{
		goto IL_006f;
	}

IL_006d:
	{
		return 0;
	}

IL_006f:
	{
		int32_t L_13 = V_0;
		V_0 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0073:
	{
		int32_t L_14 = V_0;
		ByteU5BU5D_t1_109* L_15 = ___rgbKey;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_15)->max_length)))))))
		{
			goto IL_0036;
		}
	}
	{
		V_1 = 0;
		goto IL_00bf;
	}

IL_0083:
	{
		V_2 = 0;
		goto IL_00a9;
	}

IL_008a:
	{
		ByteU5BU5D_t1_109* L_16 = ___rgbKey;
		int32_t L_17 = V_2;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		int32_t L_18 = L_17;
		IL2CPP_RUNTIME_CLASS_INIT(DES_t1_1199_il2cpp_TypeInfo_var);
		ByteU5BU2CU5D_t1_1200* L_19 = ((DES_t1_1199_StaticFields*)DES_t1_1199_il2cpp_TypeInfo_var->static_fields)->___semiWeakKeys_12;
		int32_t L_20 = V_1;
		int32_t L_21 = V_2;
		NullCheck(L_19);
		uint8_t L_22 = GenArrayGet2(L_19, L_20, L_21, uint8_t);;
		if ((((int32_t)((int32_t)((int32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_16, L_18, sizeof(uint8_t)))^(int32_t)L_22))) <= ((int32_t)1)))
		{
			goto IL_00a5;
		}
	}
	{
		goto IL_00b2;
	}

IL_00a5:
	{
		int32_t L_23 = V_2;
		V_2 = ((int32_t)((int32_t)L_23+(int32_t)1));
	}

IL_00a9:
	{
		int32_t L_24 = V_2;
		ByteU5BU5D_t1_109* L_25 = ___rgbKey;
		NullCheck(L_25);
		if ((((int32_t)L_24) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_25)->max_length)))))))
		{
			goto IL_008a;
		}
	}

IL_00b2:
	{
		int32_t L_26 = V_2;
		if ((!(((uint32_t)L_26) == ((uint32_t)8))))
		{
			goto IL_00bb;
		}
	}
	{
		return 1;
	}

IL_00bb:
	{
		int32_t L_27 = V_1;
		V_1 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_00bf:
	{
		int32_t L_28 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(DES_t1_1199_il2cpp_TypeInfo_var);
		ByteU5BU2CU5D_t1_1200* L_29 = ((DES_t1_1199_StaticFields*)DES_t1_1199_il2cpp_TypeInfo_var->static_fields)->___semiWeakKeys_12;
		NullCheck(L_29);
		int32_t L_30 = Array_get_Length_m1_992(L_29, /*hidden argument*/NULL);
		if ((((int32_t)L_28) < ((int32_t)((int32_t)((int32_t)L_30>>(int32_t)3)))))
		{
			goto IL_0083;
		}
	}
	{
		return 0;
	}
}
// System.Byte[] System.Security.Cryptography.DES::get_Key()
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern "C" ByteU5BU5D_t1_109* DES_get_Key_m1_10265 (DES_t1_1199 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		s_Il2CppMethodIntialized = true;
	}
	{
		ByteU5BU5D_t1_109* L_0 = (((SymmetricAlgorithm_t1_166 *)__this)->___KeyValue_3);
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		VirtActionInvoker0::Invoke(27 /* System.Void System.Security.Cryptography.SymmetricAlgorithm::GenerateKey() */, __this);
	}

IL_0011:
	{
		ByteU5BU5D_t1_109* L_1 = (((SymmetricAlgorithm_t1_166 *)__this)->___KeyValue_3);
		NullCheck(L_1);
		Object_t * L_2 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(5 /* System.Object System.Array::Clone() */, L_1);
		return ((ByteU5BU5D_t1_109*)Castclass(L_2, ByteU5BU5D_t1_109_il2cpp_TypeInfo_var));
	}
}
// System.Void System.Security.Cryptography.DES::set_Key(System.Byte[])
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern TypeInfo* DES_t1_1199_il2cpp_TypeInfo_var;
extern TypeInfo* CryptographicException_t1_1194_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2695;
extern Il2CppCodeGenString* _stringLiteral2694;
extern Il2CppCodeGenString* _stringLiteral2696;
extern Il2CppCodeGenString* _stringLiteral2697;
extern "C" void DES_set_Key_m1_10266 (DES_t1_1199 * __this, ByteU5BU5D_t1_109* ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		DES_t1_1199_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(875);
		CryptographicException_t1_1194_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(197);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		_stringLiteral2695 = il2cpp_codegen_string_literal_from_index(2695);
		_stringLiteral2694 = il2cpp_codegen_string_literal_from_index(2694);
		_stringLiteral2696 = il2cpp_codegen_string_literal_from_index(2696);
		_stringLiteral2697 = il2cpp_codegen_string_literal_from_index(2697);
		s_Il2CppMethodIntialized = true;
	}
	{
		ByteU5BU5D_t1_109* L_0 = ___value;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, _stringLiteral2695, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		ByteU5BU5D_t1_109* L_2 = ___value;
		NullCheck(L_2);
		if ((((int32_t)(((int32_t)((int32_t)(((Array_t *)L_2)->max_length))))) == ((int32_t)8)))
		{
			goto IL_002a;
		}
	}
	{
		String_t* L_3 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2694, /*hidden argument*/NULL);
		ArgumentException_t1_1425 * L_4 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13260(L_4, L_3, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
	}

IL_002a:
	{
		ByteU5BU5D_t1_109* L_5 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(DES_t1_1199_il2cpp_TypeInfo_var);
		bool L_6 = DES_IsWeakKey_m1_10263(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0045;
		}
	}
	{
		String_t* L_7 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2696, /*hidden argument*/NULL);
		CryptographicException_t1_1194 * L_8 = (CryptographicException_t1_1194 *)il2cpp_codegen_object_new (CryptographicException_t1_1194_il2cpp_TypeInfo_var);
		CryptographicException__ctor_m1_10222(L_8, L_7, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_8);
	}

IL_0045:
	{
		ByteU5BU5D_t1_109* L_9 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(DES_t1_1199_il2cpp_TypeInfo_var);
		bool L_10 = DES_IsSemiWeakKey_m1_10264(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0060;
		}
	}
	{
		String_t* L_11 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2697, /*hidden argument*/NULL);
		CryptographicException_t1_1194 * L_12 = (CryptographicException_t1_1194 *)il2cpp_codegen_object_new (CryptographicException_t1_1194_il2cpp_TypeInfo_var);
		CryptographicException__ctor_m1_10222(L_12, L_11, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_12);
	}

IL_0060:
	{
		ByteU5BU5D_t1_109* L_13 = ___value;
		NullCheck(L_13);
		Object_t * L_14 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(5 /* System.Object System.Array::Clone() */, L_13);
		((SymmetricAlgorithm_t1_166 *)__this)->___KeyValue_3 = ((ByteU5BU5D_t1_109*)Castclass(L_14, ByteU5BU5D_t1_109_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void System.Security.Cryptography.DESTransform::.ctor(System.Security.Cryptography.SymmetricAlgorithm,System.Boolean,System.Byte[],System.Byte[])
extern TypeInfo* DESTransform_t1_1201_il2cpp_TypeInfo_var;
extern TypeInfo* DES_t1_1199_il2cpp_TypeInfo_var;
extern TypeInfo* CryptographicException_t1_1194_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern TypeInfo* UInt32U5BU5D_t1_142_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2698;
extern "C" void DESTransform__ctor_m1_10267 (DESTransform_t1_1201 * __this, SymmetricAlgorithm_t1_166 * ___symmAlgo, bool ___encryption, ByteU5BU5D_t1_109* ___key, ByteU5BU5D_t1_109* ___iv, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DESTransform_t1_1201_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(876);
		DES_t1_1199_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(875);
		CryptographicException_t1_1194_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(197);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		UInt32U5BU5D_t1_142_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(71);
		_stringLiteral2698 = il2cpp_codegen_string_literal_from_index(2698);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	String_t* V_1 = {0};
	{
		SymmetricAlgorithm_t1_166 * L_0 = ___symmAlgo;
		bool L_1 = ___encryption;
		ByteU5BU5D_t1_109* L_2 = ___iv;
		SymmetricTransform__ctor_m1_2071(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = (ByteU5BU5D_t1_109*)NULL;
		ByteU5BU5D_t1_109* L_3 = ___key;
		if (L_3)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DESTransform_t1_1201_il2cpp_TypeInfo_var);
		ByteU5BU5D_t1_109* L_4 = DESTransform_GetStrongKey_m1_10275(NULL /*static, unused*/, /*hidden argument*/NULL);
		___key = L_4;
		ByteU5BU5D_t1_109* L_5 = ___key;
		V_0 = L_5;
	}

IL_001b:
	{
		ByteU5BU5D_t1_109* L_6 = ___key;
		IL2CPP_RUNTIME_CLASS_INIT(DES_t1_1199_il2cpp_TypeInfo_var);
		bool L_7 = DES_IsWeakKey_m1_10263(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0031;
		}
	}
	{
		ByteU5BU5D_t1_109* L_8 = ___key;
		IL2CPP_RUNTIME_CLASS_INIT(DES_t1_1199_il2cpp_TypeInfo_var);
		bool L_9 = DES_IsSemiWeakKey_m1_10264(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0043;
		}
	}

IL_0031:
	{
		String_t* L_10 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2698, /*hidden argument*/NULL);
		V_1 = L_10;
		String_t* L_11 = V_1;
		CryptographicException_t1_1194 * L_12 = (CryptographicException_t1_1194 *)il2cpp_codegen_object_new (CryptographicException_t1_1194_il2cpp_TypeInfo_var);
		CryptographicException__ctor_m1_10222(L_12, L_11, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_12);
	}

IL_0043:
	{
		ByteU5BU5D_t1_109* L_13 = V_0;
		if (L_13)
		{
			goto IL_0055;
		}
	}
	{
		ByteU5BU5D_t1_109* L_14 = ___key;
		NullCheck(L_14);
		Object_t * L_15 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(5 /* System.Object System.Array::Clone() */, L_14);
		V_0 = ((ByteU5BU5D_t1_109*)Castclass(L_15, ByteU5BU5D_t1_109_il2cpp_TypeInfo_var));
	}

IL_0055:
	{
		IL2CPP_RUNTIME_CLASS_INIT(DESTransform_t1_1201_il2cpp_TypeInfo_var);
		int32_t L_16 = ((DESTransform_t1_1201_StaticFields*)DESTransform_t1_1201_il2cpp_TypeInfo_var->static_fields)->___KEY_BYTE_SIZE_13;
		__this->___keySchedule_16 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, ((int32_t)((int32_t)L_16*(int32_t)((int32_t)16)))));
		int32_t L_17 = ((DESTransform_t1_1201_StaticFields*)DESTransform_t1_1201_il2cpp_TypeInfo_var->static_fields)->___BLOCK_BYTE_SIZE_15;
		__this->___byteBuff_17 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, L_17));
		int32_t L_18 = ((DESTransform_t1_1201_StaticFields*)DESTransform_t1_1201_il2cpp_TypeInfo_var->static_fields)->___BLOCK_BYTE_SIZE_15;
		__this->___dwordBuff_18 = ((UInt32U5BU5D_t1_142*)SZArrayNew(UInt32U5BU5D_t1_142_il2cpp_TypeInfo_var, ((int32_t)((int32_t)L_18/(int32_t)4))));
		ByteU5BU5D_t1_109* L_19 = V_0;
		DESTransform_SetKey_m1_10272(__this, L_19, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.Cryptography.DESTransform::.cctor()
extern TypeInfo* DESTransform_t1_1201_il2cpp_TypeInfo_var;
extern TypeInfo* UInt32U5BU5D_t1_142_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t1_1661____U24U24fieldU2D37_37_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t1_1661____U24U24fieldU2D38_38_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t1_1661____U24U24fieldU2D39_39_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t1_1661____U24U24fieldU2D40_40_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t1_1661____U24U24fieldU2D41_41_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t1_1661____U24U24fieldU2D42_42_FieldInfo_var;
extern "C" void DESTransform__cctor_m1_10268 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DESTransform_t1_1201_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(876);
		UInt32U5BU5D_t1_142_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(71);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		U3CPrivateImplementationDetailsU3E_t1_1661____U24U24fieldU2D37_37_FieldInfo_var = il2cpp_codegen_field_info_from_index(32, 37);
		U3CPrivateImplementationDetailsU3E_t1_1661____U24U24fieldU2D38_38_FieldInfo_var = il2cpp_codegen_field_info_from_index(32, 38);
		U3CPrivateImplementationDetailsU3E_t1_1661____U24U24fieldU2D39_39_FieldInfo_var = il2cpp_codegen_field_info_from_index(32, 39);
		U3CPrivateImplementationDetailsU3E_t1_1661____U24U24fieldU2D40_40_FieldInfo_var = il2cpp_codegen_field_info_from_index(32, 40);
		U3CPrivateImplementationDetailsU3E_t1_1661____U24U24fieldU2D41_41_FieldInfo_var = il2cpp_codegen_field_info_from_index(32, 41);
		U3CPrivateImplementationDetailsU3E_t1_1661____U24U24fieldU2D42_42_FieldInfo_var = il2cpp_codegen_field_info_from_index(32, 42);
		s_Il2CppMethodIntialized = true;
	}
	{
		((DESTransform_t1_1201_StaticFields*)DESTransform_t1_1201_il2cpp_TypeInfo_var->static_fields)->___KEY_BIT_SIZE_12 = ((int32_t)64);
		int32_t L_0 = ((DESTransform_t1_1201_StaticFields*)DESTransform_t1_1201_il2cpp_TypeInfo_var->static_fields)->___KEY_BIT_SIZE_12;
		((DESTransform_t1_1201_StaticFields*)DESTransform_t1_1201_il2cpp_TypeInfo_var->static_fields)->___KEY_BYTE_SIZE_13 = ((int32_t)((int32_t)L_0/(int32_t)8));
		((DESTransform_t1_1201_StaticFields*)DESTransform_t1_1201_il2cpp_TypeInfo_var->static_fields)->___BLOCK_BIT_SIZE_14 = ((int32_t)64);
		int32_t L_1 = ((DESTransform_t1_1201_StaticFields*)DESTransform_t1_1201_il2cpp_TypeInfo_var->static_fields)->___BLOCK_BIT_SIZE_14;
		((DESTransform_t1_1201_StaticFields*)DESTransform_t1_1201_il2cpp_TypeInfo_var->static_fields)->___BLOCK_BYTE_SIZE_15 = ((int32_t)((int32_t)L_1/(int32_t)8));
		UInt32U5BU5D_t1_142* L_2 = ((UInt32U5BU5D_t1_142*)SZArrayNew(UInt32U5BU5D_t1_142_il2cpp_TypeInfo_var, ((int32_t)512)));
		RuntimeHelpers_InitializeArray_m1_1387(NULL /*static, unused*/, (Array_t *)(Array_t *)L_2, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t1_1661____U24U24fieldU2D37_37_FieldInfo_var), /*hidden argument*/NULL);
		((DESTransform_t1_1201_StaticFields*)DESTransform_t1_1201_il2cpp_TypeInfo_var->static_fields)->___spBoxes_19 = L_2;
		ByteU5BU5D_t1_109* L_3 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, ((int32_t)56)));
		RuntimeHelpers_InitializeArray_m1_1387(NULL /*static, unused*/, (Array_t *)(Array_t *)L_3, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t1_1661____U24U24fieldU2D38_38_FieldInfo_var), /*hidden argument*/NULL);
		((DESTransform_t1_1201_StaticFields*)DESTransform_t1_1201_il2cpp_TypeInfo_var->static_fields)->___PC1_20 = L_3;
		ByteU5BU5D_t1_109* L_4 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, ((int32_t)16)));
		RuntimeHelpers_InitializeArray_m1_1387(NULL /*static, unused*/, (Array_t *)(Array_t *)L_4, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t1_1661____U24U24fieldU2D39_39_FieldInfo_var), /*hidden argument*/NULL);
		((DESTransform_t1_1201_StaticFields*)DESTransform_t1_1201_il2cpp_TypeInfo_var->static_fields)->___leftRotTotal_21 = L_4;
		ByteU5BU5D_t1_109* L_5 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, ((int32_t)48)));
		RuntimeHelpers_InitializeArray_m1_1387(NULL /*static, unused*/, (Array_t *)(Array_t *)L_5, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t1_1661____U24U24fieldU2D40_40_FieldInfo_var), /*hidden argument*/NULL);
		((DESTransform_t1_1201_StaticFields*)DESTransform_t1_1201_il2cpp_TypeInfo_var->static_fields)->___PC2_22 = L_5;
		UInt32U5BU5D_t1_142* L_6 = ((UInt32U5BU5D_t1_142*)SZArrayNew(UInt32U5BU5D_t1_142_il2cpp_TypeInfo_var, ((int32_t)512)));
		RuntimeHelpers_InitializeArray_m1_1387(NULL /*static, unused*/, (Array_t *)(Array_t *)L_6, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t1_1661____U24U24fieldU2D41_41_FieldInfo_var), /*hidden argument*/NULL);
		((DESTransform_t1_1201_StaticFields*)DESTransform_t1_1201_il2cpp_TypeInfo_var->static_fields)->___ipTab_23 = L_6;
		UInt32U5BU5D_t1_142* L_7 = ((UInt32U5BU5D_t1_142*)SZArrayNew(UInt32U5BU5D_t1_142_il2cpp_TypeInfo_var, ((int32_t)512)));
		RuntimeHelpers_InitializeArray_m1_1387(NULL /*static, unused*/, (Array_t *)(Array_t *)L_7, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t1_1661____U24U24fieldU2D42_42_FieldInfo_var), /*hidden argument*/NULL);
		((DESTransform_t1_1201_StaticFields*)DESTransform_t1_1201_il2cpp_TypeInfo_var->static_fields)->___fpTab_24 = L_7;
		return;
	}
}
// System.UInt32 System.Security.Cryptography.DESTransform::CipherFunct(System.UInt32,System.Int32)
extern TypeInfo* DESTransform_t1_1201_il2cpp_TypeInfo_var;
extern "C" uint32_t DESTransform_CipherFunct_m1_10269 (DESTransform_t1_1201 * __this, uint32_t ___r, int32_t ___n, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DESTransform_t1_1201_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(876);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	ByteU5BU5D_t1_109* V_1 = {0};
	int32_t V_2 = 0;
	uint32_t V_3 = 0;
	{
		V_0 = 0;
		ByteU5BU5D_t1_109* L_0 = (__this->___keySchedule_16);
		V_1 = L_0;
		int32_t L_1 = ___n;
		V_2 = ((int32_t)((int32_t)L_1<<(int32_t)3));
		uint32_t L_2 = ___r;
		uint32_t L_3 = ___r;
		V_3 = ((int32_t)((int32_t)((int32_t)((uint32_t)L_2>>1))|(int32_t)((int32_t)((int32_t)L_3<<(int32_t)((int32_t)31)))));
		uint32_t L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(DESTransform_t1_1201_il2cpp_TypeInfo_var);
		UInt32U5BU5D_t1_142* L_5 = ((DESTransform_t1_1201_StaticFields*)DESTransform_t1_1201_il2cpp_TypeInfo_var->static_fields)->___spBoxes_19;
		uint32_t L_6 = V_3;
		ByteU5BU5D_t1_109* L_7 = V_1;
		int32_t L_8 = V_2;
		int32_t L_9 = L_8;
		V_2 = ((int32_t)((int32_t)L_9+(int32_t)1));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_9);
		int32_t L_10 = L_9;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, (((uintptr_t)((int32_t)((int32_t)0+(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((uint32_t)L_6>>((int32_t)26)))^(int32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_7, L_10, sizeof(uint8_t)))))&(int32_t)((int32_t)63))))))));
		uintptr_t L_11 = (((uintptr_t)((int32_t)((int32_t)0+(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((uint32_t)L_6>>((int32_t)26)))^(int32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_7, L_10, sizeof(uint8_t)))))&(int32_t)((int32_t)63)))))));
		V_0 = ((int32_t)((int32_t)L_4|(int32_t)(*(uint32_t*)(uint32_t*)SZArrayLdElema(L_5, L_11, sizeof(uint32_t)))));
		uint32_t L_12 = V_0;
		UInt32U5BU5D_t1_142* L_13 = ((DESTransform_t1_1201_StaticFields*)DESTransform_t1_1201_il2cpp_TypeInfo_var->static_fields)->___spBoxes_19;
		uint32_t L_14 = V_3;
		ByteU5BU5D_t1_109* L_15 = V_1;
		int32_t L_16 = V_2;
		int32_t L_17 = L_16;
		V_2 = ((int32_t)((int32_t)L_17+(int32_t)1));
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_17);
		int32_t L_18 = L_17;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, (((uintptr_t)((int32_t)((int32_t)((int32_t)64)+(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((uint32_t)L_14>>((int32_t)22)))^(int32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_15, L_18, sizeof(uint8_t)))))&(int32_t)((int32_t)63))))))));
		uintptr_t L_19 = (((uintptr_t)((int32_t)((int32_t)((int32_t)64)+(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((uint32_t)L_14>>((int32_t)22)))^(int32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_15, L_18, sizeof(uint8_t)))))&(int32_t)((int32_t)63)))))));
		V_0 = ((int32_t)((int32_t)L_12|(int32_t)(*(uint32_t*)(uint32_t*)SZArrayLdElema(L_13, L_19, sizeof(uint32_t)))));
		uint32_t L_20 = V_0;
		UInt32U5BU5D_t1_142* L_21 = ((DESTransform_t1_1201_StaticFields*)DESTransform_t1_1201_il2cpp_TypeInfo_var->static_fields)->___spBoxes_19;
		uint32_t L_22 = V_3;
		ByteU5BU5D_t1_109* L_23 = V_1;
		int32_t L_24 = V_2;
		int32_t L_25 = L_24;
		V_2 = ((int32_t)((int32_t)L_25+(int32_t)1));
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_25);
		int32_t L_26 = L_25;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, (((uintptr_t)((int32_t)((int32_t)((int32_t)128)+(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((uint32_t)L_22>>((int32_t)18)))^(int32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_23, L_26, sizeof(uint8_t)))))&(int32_t)((int32_t)63))))))));
		uintptr_t L_27 = (((uintptr_t)((int32_t)((int32_t)((int32_t)128)+(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((uint32_t)L_22>>((int32_t)18)))^(int32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_23, L_26, sizeof(uint8_t)))))&(int32_t)((int32_t)63)))))));
		V_0 = ((int32_t)((int32_t)L_20|(int32_t)(*(uint32_t*)(uint32_t*)SZArrayLdElema(L_21, L_27, sizeof(uint32_t)))));
		uint32_t L_28 = V_0;
		UInt32U5BU5D_t1_142* L_29 = ((DESTransform_t1_1201_StaticFields*)DESTransform_t1_1201_il2cpp_TypeInfo_var->static_fields)->___spBoxes_19;
		uint32_t L_30 = V_3;
		ByteU5BU5D_t1_109* L_31 = V_1;
		int32_t L_32 = V_2;
		int32_t L_33 = L_32;
		V_2 = ((int32_t)((int32_t)L_33+(int32_t)1));
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, L_33);
		int32_t L_34 = L_33;
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, (((uintptr_t)((int32_t)((int32_t)((int32_t)192)+(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((uint32_t)L_30>>((int32_t)14)))^(int32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_31, L_34, sizeof(uint8_t)))))&(int32_t)((int32_t)63))))))));
		uintptr_t L_35 = (((uintptr_t)((int32_t)((int32_t)((int32_t)192)+(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((uint32_t)L_30>>((int32_t)14)))^(int32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_31, L_34, sizeof(uint8_t)))))&(int32_t)((int32_t)63)))))));
		V_0 = ((int32_t)((int32_t)L_28|(int32_t)(*(uint32_t*)(uint32_t*)SZArrayLdElema(L_29, L_35, sizeof(uint32_t)))));
		uint32_t L_36 = V_0;
		UInt32U5BU5D_t1_142* L_37 = ((DESTransform_t1_1201_StaticFields*)DESTransform_t1_1201_il2cpp_TypeInfo_var->static_fields)->___spBoxes_19;
		uint32_t L_38 = V_3;
		ByteU5BU5D_t1_109* L_39 = V_1;
		int32_t L_40 = V_2;
		int32_t L_41 = L_40;
		V_2 = ((int32_t)((int32_t)L_41+(int32_t)1));
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, L_41);
		int32_t L_42 = L_41;
		NullCheck(L_37);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_37, (((uintptr_t)((int32_t)((int32_t)((int32_t)256)+(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((uint32_t)L_38>>((int32_t)10)))^(int32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_39, L_42, sizeof(uint8_t)))))&(int32_t)((int32_t)63))))))));
		uintptr_t L_43 = (((uintptr_t)((int32_t)((int32_t)((int32_t)256)+(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((uint32_t)L_38>>((int32_t)10)))^(int32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_39, L_42, sizeof(uint8_t)))))&(int32_t)((int32_t)63)))))));
		V_0 = ((int32_t)((int32_t)L_36|(int32_t)(*(uint32_t*)(uint32_t*)SZArrayLdElema(L_37, L_43, sizeof(uint32_t)))));
		uint32_t L_44 = V_0;
		UInt32U5BU5D_t1_142* L_45 = ((DESTransform_t1_1201_StaticFields*)DESTransform_t1_1201_il2cpp_TypeInfo_var->static_fields)->___spBoxes_19;
		uint32_t L_46 = V_3;
		ByteU5BU5D_t1_109* L_47 = V_1;
		int32_t L_48 = V_2;
		int32_t L_49 = L_48;
		V_2 = ((int32_t)((int32_t)L_49+(int32_t)1));
		NullCheck(L_47);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_47, L_49);
		int32_t L_50 = L_49;
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, (((uintptr_t)((int32_t)((int32_t)((int32_t)320)+(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((uint32_t)L_46>>6))^(int32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_47, L_50, sizeof(uint8_t)))))&(int32_t)((int32_t)63))))))));
		uintptr_t L_51 = (((uintptr_t)((int32_t)((int32_t)((int32_t)320)+(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((uint32_t)L_46>>6))^(int32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_47, L_50, sizeof(uint8_t)))))&(int32_t)((int32_t)63)))))));
		V_0 = ((int32_t)((int32_t)L_44|(int32_t)(*(uint32_t*)(uint32_t*)SZArrayLdElema(L_45, L_51, sizeof(uint32_t)))));
		uint32_t L_52 = V_0;
		UInt32U5BU5D_t1_142* L_53 = ((DESTransform_t1_1201_StaticFields*)DESTransform_t1_1201_il2cpp_TypeInfo_var->static_fields)->___spBoxes_19;
		uint32_t L_54 = V_3;
		ByteU5BU5D_t1_109* L_55 = V_1;
		int32_t L_56 = V_2;
		int32_t L_57 = L_56;
		V_2 = ((int32_t)((int32_t)L_57+(int32_t)1));
		NullCheck(L_55);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_55, L_57);
		int32_t L_58 = L_57;
		NullCheck(L_53);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_53, (((uintptr_t)((int32_t)((int32_t)((int32_t)384)+(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((uint32_t)L_54>>2))^(int32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_55, L_58, sizeof(uint8_t)))))&(int32_t)((int32_t)63))))))));
		uintptr_t L_59 = (((uintptr_t)((int32_t)((int32_t)((int32_t)384)+(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((uint32_t)L_54>>2))^(int32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_55, L_58, sizeof(uint8_t)))))&(int32_t)((int32_t)63)))))));
		V_0 = ((int32_t)((int32_t)L_52|(int32_t)(*(uint32_t*)(uint32_t*)SZArrayLdElema(L_53, L_59, sizeof(uint32_t)))));
		uint32_t L_60 = ___r;
		uint32_t L_61 = ___r;
		V_3 = ((int32_t)((int32_t)((int32_t)((int32_t)L_60<<(int32_t)1))|(int32_t)((int32_t)((uint32_t)L_61>>((int32_t)31)))));
		uint32_t L_62 = V_0;
		UInt32U5BU5D_t1_142* L_63 = ((DESTransform_t1_1201_StaticFields*)DESTransform_t1_1201_il2cpp_TypeInfo_var->static_fields)->___spBoxes_19;
		uint32_t L_64 = V_3;
		ByteU5BU5D_t1_109* L_65 = V_1;
		int32_t L_66 = V_2;
		NullCheck(L_65);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_65, L_66);
		int32_t L_67 = L_66;
		NullCheck(L_63);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_63, (((uintptr_t)((int32_t)((int32_t)((int32_t)448)+(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_64^(int32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_65, L_67, sizeof(uint8_t)))))&(int32_t)((int32_t)63))))))));
		uintptr_t L_68 = (((uintptr_t)((int32_t)((int32_t)((int32_t)448)+(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_64^(int32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_65, L_67, sizeof(uint8_t)))))&(int32_t)((int32_t)63)))))));
		V_0 = ((int32_t)((int32_t)L_62|(int32_t)(*(uint32_t*)(uint32_t*)SZArrayLdElema(L_63, L_68, sizeof(uint32_t)))));
		uint32_t L_69 = V_0;
		return L_69;
	}
}
// System.Void System.Security.Cryptography.DESTransform::Permutation(System.Byte[],System.Byte[],System.UInt32[],System.Boolean)
extern TypeInfo* BitConverter_t1_1508_il2cpp_TypeInfo_var;
extern TypeInfo* DESTransform_t1_1201_il2cpp_TypeInfo_var;
extern "C" void DESTransform_Permutation_m1_10270 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___input, ByteU5BU5D_t1_109* ___output, UInt32U5BU5D_t1_142* ___permTab, bool ___preSwap, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		BitConverter_t1_1508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(260);
		DESTransform_t1_1201_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(876);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	uint32_t V_2 = 0;
	uint32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	{
		bool L_0 = ___preSwap;
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t1_1508_il2cpp_TypeInfo_var);
		bool L_1 = ((BitConverter_t1_1508_StaticFields*)BitConverter_t1_1508_il2cpp_TypeInfo_var->static_fields)->___IsLittleEndian_1;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		ByteU5BU5D_t1_109* L_2 = ___input;
		IL2CPP_RUNTIME_CLASS_INIT(DESTransform_t1_1201_il2cpp_TypeInfo_var);
		DESTransform_BSwap_m1_10271(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_0016:
	{
		ByteU5BU5D_t1_109* L_3 = ___input;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		int32_t L_4 = 0;
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_3, L_4, sizeof(uint8_t)))>>(int32_t)4))<<(int32_t)1));
		ByteU5BU5D_t1_109* L_5 = ___input;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 0);
		int32_t L_6 = 0;
		V_1 = ((int32_t)((int32_t)((int32_t)32)+(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_5, L_6, sizeof(uint8_t)))&(int32_t)((int32_t)15)))<<(int32_t)1))));
		UInt32U5BU5D_t1_142* L_7 = ___permTab;
		int32_t L_8 = V_0;
		int32_t L_9 = L_8;
		V_0 = ((int32_t)((int32_t)L_9+(int32_t)1));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_9);
		int32_t L_10 = L_9;
		UInt32U5BU5D_t1_142* L_11 = ___permTab;
		int32_t L_12 = V_1;
		int32_t L_13 = L_12;
		V_1 = ((int32_t)((int32_t)L_13+(int32_t)1));
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_13);
		int32_t L_14 = L_13;
		V_2 = ((int32_t)((int32_t)(*(uint32_t*)(uint32_t*)SZArrayLdElema(L_7, L_10, sizeof(uint32_t)))|(int32_t)(*(uint32_t*)(uint32_t*)SZArrayLdElema(L_11, L_14, sizeof(uint32_t)))));
		UInt32U5BU5D_t1_142* L_15 = ___permTab;
		int32_t L_16 = V_0;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		int32_t L_17 = L_16;
		UInt32U5BU5D_t1_142* L_18 = ___permTab;
		int32_t L_19 = V_1;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_19);
		int32_t L_20 = L_19;
		V_3 = ((int32_t)((int32_t)(*(uint32_t*)(uint32_t*)SZArrayLdElema(L_15, L_17, sizeof(uint32_t)))|(int32_t)(*(uint32_t*)(uint32_t*)SZArrayLdElema(L_18, L_20, sizeof(uint32_t)))));
		IL2CPP_RUNTIME_CLASS_INIT(DESTransform_t1_1201_il2cpp_TypeInfo_var);
		int32_t L_21 = ((DESTransform_t1_1201_StaticFields*)DESTransform_t1_1201_il2cpp_TypeInfo_var->static_fields)->___BLOCK_BYTE_SIZE_15;
		V_4 = ((int32_t)((int32_t)L_21<<(int32_t)1));
		V_5 = 2;
		V_6 = 1;
		goto IL_009f;
	}

IL_0056:
	{
		ByteU5BU5D_t1_109* L_22 = ___input;
		int32_t L_23 = V_6;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
		int32_t L_24 = L_23;
		V_7 = (*(uint8_t*)(uint8_t*)SZArrayLdElema(L_22, L_24, sizeof(uint8_t)));
		int32_t L_25 = V_5;
		int32_t L_26 = V_7;
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_25<<(int32_t)5))+(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_26>>(int32_t)4))<<(int32_t)1))));
		int32_t L_27 = V_5;
		int32_t L_28 = V_7;
		V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_27+(int32_t)1))<<(int32_t)5))+(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_28&(int32_t)((int32_t)15)))<<(int32_t)1))));
		uint32_t L_29 = V_2;
		UInt32U5BU5D_t1_142* L_30 = ___permTab;
		int32_t L_31 = V_0;
		int32_t L_32 = L_31;
		V_0 = ((int32_t)((int32_t)L_32+(int32_t)1));
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, L_32);
		int32_t L_33 = L_32;
		UInt32U5BU5D_t1_142* L_34 = ___permTab;
		int32_t L_35 = V_1;
		int32_t L_36 = L_35;
		V_1 = ((int32_t)((int32_t)L_36+(int32_t)1));
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, L_36);
		int32_t L_37 = L_36;
		V_2 = ((int32_t)((int32_t)L_29|(int32_t)((int32_t)((int32_t)(*(uint32_t*)(uint32_t*)SZArrayLdElema(L_30, L_33, sizeof(uint32_t)))|(int32_t)(*(uint32_t*)(uint32_t*)SZArrayLdElema(L_34, L_37, sizeof(uint32_t)))))));
		uint32_t L_38 = V_3;
		UInt32U5BU5D_t1_142* L_39 = ___permTab;
		int32_t L_40 = V_0;
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, L_40);
		int32_t L_41 = L_40;
		UInt32U5BU5D_t1_142* L_42 = ___permTab;
		int32_t L_43 = V_1;
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, L_43);
		int32_t L_44 = L_43;
		V_3 = ((int32_t)((int32_t)L_38|(int32_t)((int32_t)((int32_t)(*(uint32_t*)(uint32_t*)SZArrayLdElema(L_39, L_41, sizeof(uint32_t)))|(int32_t)(*(uint32_t*)(uint32_t*)SZArrayLdElema(L_42, L_44, sizeof(uint32_t)))))));
		int32_t L_45 = V_5;
		V_5 = ((int32_t)((int32_t)L_45+(int32_t)2));
		int32_t L_46 = V_6;
		V_6 = ((int32_t)((int32_t)L_46+(int32_t)1));
	}

IL_009f:
	{
		int32_t L_47 = V_5;
		int32_t L_48 = V_4;
		if ((((int32_t)L_47) < ((int32_t)L_48)))
		{
			goto IL_0056;
		}
	}
	{
		bool L_49 = ___preSwap;
		if (L_49)
		{
			goto IL_00b8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t1_1508_il2cpp_TypeInfo_var);
		bool L_50 = ((BitConverter_t1_1508_StaticFields*)BitConverter_t1_1508_il2cpp_TypeInfo_var->static_fields)->___IsLittleEndian_1;
		if (L_50)
		{
			goto IL_00f5;
		}
	}

IL_00b8:
	{
		ByteU5BU5D_t1_109* L_51 = ___output;
		uint32_t L_52 = V_2;
		NullCheck(L_51);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_51, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_51, 0, sizeof(uint8_t))) = (uint8_t)(((int32_t)((uint8_t)L_52)));
		ByteU5BU5D_t1_109* L_53 = ___output;
		uint32_t L_54 = V_2;
		NullCheck(L_53);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_53, 1);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_53, 1, sizeof(uint8_t))) = (uint8_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)L_54>>8)))));
		ByteU5BU5D_t1_109* L_55 = ___output;
		uint32_t L_56 = V_2;
		NullCheck(L_55);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_55, 2);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_55, 2, sizeof(uint8_t))) = (uint8_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)L_56>>((int32_t)16))))));
		ByteU5BU5D_t1_109* L_57 = ___output;
		uint32_t L_58 = V_2;
		NullCheck(L_57);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_57, 3);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_57, 3, sizeof(uint8_t))) = (uint8_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)L_58>>((int32_t)24))))));
		ByteU5BU5D_t1_109* L_59 = ___output;
		uint32_t L_60 = V_3;
		NullCheck(L_59);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_59, 4);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_59, 4, sizeof(uint8_t))) = (uint8_t)(((int32_t)((uint8_t)L_60)));
		ByteU5BU5D_t1_109* L_61 = ___output;
		uint32_t L_62 = V_3;
		NullCheck(L_61);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_61, 5);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_61, 5, sizeof(uint8_t))) = (uint8_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)L_62>>8)))));
		ByteU5BU5D_t1_109* L_63 = ___output;
		uint32_t L_64 = V_3;
		NullCheck(L_63);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_63, 6);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_63, 6, sizeof(uint8_t))) = (uint8_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)L_64>>((int32_t)16))))));
		ByteU5BU5D_t1_109* L_65 = ___output;
		uint32_t L_66 = V_3;
		NullCheck(L_65);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_65, 7);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_65, 7, sizeof(uint8_t))) = (uint8_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)L_66>>((int32_t)24))))));
		goto IL_012d;
	}

IL_00f5:
	{
		ByteU5BU5D_t1_109* L_67 = ___output;
		uint32_t L_68 = V_2;
		NullCheck(L_67);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_67, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_67, 0, sizeof(uint8_t))) = (uint8_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)L_68>>((int32_t)24))))));
		ByteU5BU5D_t1_109* L_69 = ___output;
		uint32_t L_70 = V_2;
		NullCheck(L_69);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_69, 1);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_69, 1, sizeof(uint8_t))) = (uint8_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)L_70>>((int32_t)16))))));
		ByteU5BU5D_t1_109* L_71 = ___output;
		uint32_t L_72 = V_2;
		NullCheck(L_71);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_71, 2);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_71, 2, sizeof(uint8_t))) = (uint8_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)L_72>>8)))));
		ByteU5BU5D_t1_109* L_73 = ___output;
		uint32_t L_74 = V_2;
		NullCheck(L_73);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_73, 3);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_73, 3, sizeof(uint8_t))) = (uint8_t)(((int32_t)((uint8_t)L_74)));
		ByteU5BU5D_t1_109* L_75 = ___output;
		uint32_t L_76 = V_3;
		NullCheck(L_75);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_75, 4);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_75, 4, sizeof(uint8_t))) = (uint8_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)L_76>>((int32_t)24))))));
		ByteU5BU5D_t1_109* L_77 = ___output;
		uint32_t L_78 = V_3;
		NullCheck(L_77);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_77, 5);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_77, 5, sizeof(uint8_t))) = (uint8_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)L_78>>((int32_t)16))))));
		ByteU5BU5D_t1_109* L_79 = ___output;
		uint32_t L_80 = V_3;
		NullCheck(L_79);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_79, 6);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_79, 6, sizeof(uint8_t))) = (uint8_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)L_80>>8)))));
		ByteU5BU5D_t1_109* L_81 = ___output;
		uint32_t L_82 = V_3;
		NullCheck(L_81);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_81, 7);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_81, 7, sizeof(uint8_t))) = (uint8_t)(((int32_t)((uint8_t)L_82)));
	}

IL_012d:
	{
		return;
	}
}
// System.Void System.Security.Cryptography.DESTransform::BSwap(System.Byte[])
extern "C" void DESTransform_BSwap_m1_10271 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___byteBuff, const MethodInfo* method)
{
	uint8_t V_0 = 0x0;
	{
		ByteU5BU5D_t1_109* L_0 = ___byteBuff;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		int32_t L_1 = 0;
		V_0 = (*(uint8_t*)(uint8_t*)SZArrayLdElema(L_0, L_1, sizeof(uint8_t)));
		ByteU5BU5D_t1_109* L_2 = ___byteBuff;
		ByteU5BU5D_t1_109* L_3 = ___byteBuff;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 3);
		int32_t L_4 = 3;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_2, 0, sizeof(uint8_t))) = (uint8_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_3, L_4, sizeof(uint8_t)));
		ByteU5BU5D_t1_109* L_5 = ___byteBuff;
		uint8_t L_6 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 3);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_5, 3, sizeof(uint8_t))) = (uint8_t)L_6;
		ByteU5BU5D_t1_109* L_7 = ___byteBuff;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 1);
		int32_t L_8 = 1;
		V_0 = (*(uint8_t*)(uint8_t*)SZArrayLdElema(L_7, L_8, sizeof(uint8_t)));
		ByteU5BU5D_t1_109* L_9 = ___byteBuff;
		ByteU5BU5D_t1_109* L_10 = ___byteBuff;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		int32_t L_11 = 2;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 1);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_9, 1, sizeof(uint8_t))) = (uint8_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_10, L_11, sizeof(uint8_t)));
		ByteU5BU5D_t1_109* L_12 = ___byteBuff;
		uint8_t L_13 = V_0;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 2);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_12, 2, sizeof(uint8_t))) = (uint8_t)L_13;
		ByteU5BU5D_t1_109* L_14 = ___byteBuff;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 4);
		int32_t L_15 = 4;
		V_0 = (*(uint8_t*)(uint8_t*)SZArrayLdElema(L_14, L_15, sizeof(uint8_t)));
		ByteU5BU5D_t1_109* L_16 = ___byteBuff;
		ByteU5BU5D_t1_109* L_17 = ___byteBuff;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 7);
		int32_t L_18 = 7;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 4);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_16, 4, sizeof(uint8_t))) = (uint8_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_17, L_18, sizeof(uint8_t)));
		ByteU5BU5D_t1_109* L_19 = ___byteBuff;
		uint8_t L_20 = V_0;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 7);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_19, 7, sizeof(uint8_t))) = (uint8_t)L_20;
		ByteU5BU5D_t1_109* L_21 = ___byteBuff;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 5);
		int32_t L_22 = 5;
		V_0 = (*(uint8_t*)(uint8_t*)SZArrayLdElema(L_21, L_22, sizeof(uint8_t)));
		ByteU5BU5D_t1_109* L_23 = ___byteBuff;
		ByteU5BU5D_t1_109* L_24 = ___byteBuff;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 6);
		int32_t L_25 = 6;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 5);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_23, 5, sizeof(uint8_t))) = (uint8_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_24, L_25, sizeof(uint8_t)));
		ByteU5BU5D_t1_109* L_26 = ___byteBuff;
		uint8_t L_27 = V_0;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, 6);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_26, 6, sizeof(uint8_t))) = (uint8_t)L_27;
		return;
	}
}
// System.Void System.Security.Cryptography.DESTransform::SetKey(System.Byte[])
extern TypeInfo* DESTransform_t1_1201_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern "C" void DESTransform_SetKey_m1_10272 (DESTransform_t1_1201 * __this, ByteU5BU5D_t1_109* ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DESTransform_t1_1201_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(876);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	ByteU5BU5D_t1_109* V_1 = {0};
	ByteU5BU5D_t1_109* V_2 = {0};
	int32_t V_3 = 0;
	uint8_t V_4 = 0x0;
	ByteU5BU5D_t1_109* V_5 = {0};
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	int32_t V_11 = 0;
	int32_t V_12 = 0;
	uint8_t V_13 = 0x0;
	ByteU5BU5D_t1_109* V_14 = {0};
	int32_t V_15 = 0;
	ByteU5BU5D_t1_109* G_B7_0 = {0};
	int32_t G_B7_1 = 0;
	ByteU5BU5D_t1_109* G_B7_2 = {0};
	ByteU5BU5D_t1_109* G_B6_0 = {0};
	int32_t G_B6_1 = 0;
	ByteU5BU5D_t1_109* G_B6_2 = {0};
	int32_t G_B8_0 = 0;
	ByteU5BU5D_t1_109* G_B8_1 = {0};
	int32_t G_B8_2 = 0;
	ByteU5BU5D_t1_109* G_B8_3 = {0};
	ByteU5BU5D_t1_109* G_B13_0 = {0};
	int32_t G_B13_1 = 0;
	ByteU5BU5D_t1_109* G_B13_2 = {0};
	ByteU5BU5D_t1_109* G_B12_0 = {0};
	int32_t G_B12_1 = 0;
	ByteU5BU5D_t1_109* G_B12_2 = {0};
	int32_t G_B14_0 = 0;
	ByteU5BU5D_t1_109* G_B14_1 = {0};
	int32_t G_B14_2 = 0;
	ByteU5BU5D_t1_109* G_B14_3 = {0};
	{
		ByteU5BU5D_t1_109* L_0 = (__this->___keySchedule_16);
		ByteU5BU5D_t1_109* L_1 = (__this->___keySchedule_16);
		NullCheck(L_1);
		Array_Clear_m1_1037(NULL /*static, unused*/, (Array_t *)(Array_t *)L_0, 0, (((int32_t)((int32_t)(((Array_t *)L_1)->max_length)))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DESTransform_t1_1201_il2cpp_TypeInfo_var);
		ByteU5BU5D_t1_109* L_2 = ((DESTransform_t1_1201_StaticFields*)DESTransform_t1_1201_il2cpp_TypeInfo_var->static_fields)->___PC1_20;
		NullCheck(L_2);
		V_0 = (((int32_t)((int32_t)(((Array_t *)L_2)->max_length))));
		int32_t L_3 = V_0;
		V_1 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, L_3));
		int32_t L_4 = V_0;
		V_2 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, L_4));
		V_3 = 0;
		ByteU5BU5D_t1_109* L_5 = ((DESTransform_t1_1201_StaticFields*)DESTransform_t1_1201_il2cpp_TypeInfo_var->static_fields)->___PC1_20;
		V_5 = L_5;
		V_6 = 0;
		goto IL_0062;
	}

IL_003b:
	{
		ByteU5BU5D_t1_109* L_6 = V_5;
		int32_t L_7 = V_6;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		V_4 = (*(uint8_t*)(uint8_t*)SZArrayLdElema(L_6, L_8, sizeof(uint8_t)));
		ByteU5BU5D_t1_109* L_9 = V_1;
		int32_t L_10 = V_3;
		int32_t L_11 = L_10;
		V_3 = ((int32_t)((int32_t)L_11+(int32_t)1));
		ByteU5BU5D_t1_109* L_12 = ___key;
		uint8_t L_13 = V_4;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, ((int32_t)((int32_t)L_13>>(int32_t)3)));
		int32_t L_14 = ((int32_t)((int32_t)L_13>>(int32_t)3));
		uint8_t L_15 = V_4;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_11);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_9, L_11, sizeof(uint8_t))) = (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((int32_t)((int32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_12, L_14, sizeof(uint8_t)))>>(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)7^(int32_t)((int32_t)((int32_t)L_15&(int32_t)7))))&(int32_t)((int32_t)31)))))&(int32_t)1)))));
		int32_t L_16 = V_6;
		V_6 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0062:
	{
		int32_t L_17 = V_6;
		ByteU5BU5D_t1_109* L_18 = V_5;
		NullCheck(L_18);
		if ((((int32_t)L_17) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_18)->max_length)))))))
		{
			goto IL_003b;
		}
	}
	{
		V_8 = 0;
		goto IL_0167;
	}

IL_0075:
	{
		int32_t L_19 = V_0;
		V_9 = ((int32_t)((int32_t)L_19>>(int32_t)1));
		V_7 = 0;
		goto IL_00b0;
	}

IL_0082:
	{
		int32_t L_20 = V_7;
		IL2CPP_RUNTIME_CLASS_INIT(DESTransform_t1_1201_il2cpp_TypeInfo_var);
		ByteU5BU5D_t1_109* L_21 = ((DESTransform_t1_1201_StaticFields*)DESTransform_t1_1201_il2cpp_TypeInfo_var->static_fields)->___leftRotTotal_21;
		int32_t L_22 = V_8;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, L_22);
		int32_t L_23 = L_22;
		V_10 = ((int32_t)((int32_t)L_20+(int32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_21, L_23, sizeof(uint8_t)))));
		ByteU5BU5D_t1_109* L_24 = V_2;
		int32_t L_25 = V_7;
		ByteU5BU5D_t1_109* L_26 = V_1;
		int32_t L_27 = V_10;
		int32_t L_28 = V_9;
		G_B6_0 = L_26;
		G_B6_1 = L_25;
		G_B6_2 = L_24;
		if ((((int32_t)L_27) >= ((int32_t)L_28)))
		{
			G_B7_0 = L_26;
			G_B7_1 = L_25;
			G_B7_2 = L_24;
			goto IL_00a3;
		}
	}
	{
		int32_t L_29 = V_10;
		G_B8_0 = L_29;
		G_B8_1 = G_B6_0;
		G_B8_2 = G_B6_1;
		G_B8_3 = G_B6_2;
		goto IL_00a8;
	}

IL_00a3:
	{
		int32_t L_30 = V_10;
		int32_t L_31 = V_9;
		G_B8_0 = ((int32_t)((int32_t)L_30-(int32_t)L_31));
		G_B8_1 = G_B7_0;
		G_B8_2 = G_B7_1;
		G_B8_3 = G_B7_2;
	}

IL_00a8:
	{
		NullCheck(G_B8_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B8_1, G_B8_0);
		int32_t L_32 = G_B8_0;
		NullCheck(G_B8_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B8_3, G_B8_2);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(G_B8_3, G_B8_2, sizeof(uint8_t))) = (uint8_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(G_B8_1, L_32, sizeof(uint8_t)));
		int32_t L_33 = V_7;
		V_7 = ((int32_t)((int32_t)L_33+(int32_t)1));
	}

IL_00b0:
	{
		int32_t L_34 = V_7;
		int32_t L_35 = V_9;
		if ((((int32_t)L_34) < ((int32_t)L_35)))
		{
			goto IL_0082;
		}
	}
	{
		int32_t L_36 = V_9;
		V_7 = L_36;
		goto IL_00ef;
	}

IL_00c2:
	{
		int32_t L_37 = V_7;
		IL2CPP_RUNTIME_CLASS_INIT(DESTransform_t1_1201_il2cpp_TypeInfo_var);
		ByteU5BU5D_t1_109* L_38 = ((DESTransform_t1_1201_StaticFields*)DESTransform_t1_1201_il2cpp_TypeInfo_var->static_fields)->___leftRotTotal_21;
		int32_t L_39 = V_8;
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, L_39);
		int32_t L_40 = L_39;
		V_11 = ((int32_t)((int32_t)L_37+(int32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_38, L_40, sizeof(uint8_t)))));
		ByteU5BU5D_t1_109* L_41 = V_2;
		int32_t L_42 = V_7;
		ByteU5BU5D_t1_109* L_43 = V_1;
		int32_t L_44 = V_11;
		int32_t L_45 = V_0;
		G_B12_0 = L_43;
		G_B12_1 = L_42;
		G_B12_2 = L_41;
		if ((((int32_t)L_44) >= ((int32_t)L_45)))
		{
			G_B13_0 = L_43;
			G_B13_1 = L_42;
			G_B13_2 = L_41;
			goto IL_00e2;
		}
	}
	{
		int32_t L_46 = V_11;
		G_B14_0 = L_46;
		G_B14_1 = G_B12_0;
		G_B14_2 = G_B12_1;
		G_B14_3 = G_B12_2;
		goto IL_00e7;
	}

IL_00e2:
	{
		int32_t L_47 = V_11;
		int32_t L_48 = V_9;
		G_B14_0 = ((int32_t)((int32_t)L_47-(int32_t)L_48));
		G_B14_1 = G_B13_0;
		G_B14_2 = G_B13_1;
		G_B14_3 = G_B13_2;
	}

IL_00e7:
	{
		NullCheck(G_B14_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B14_1, G_B14_0);
		int32_t L_49 = G_B14_0;
		NullCheck(G_B14_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B14_3, G_B14_2);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(G_B14_3, G_B14_2, sizeof(uint8_t))) = (uint8_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(G_B14_1, L_49, sizeof(uint8_t)));
		int32_t L_50 = V_7;
		V_7 = ((int32_t)((int32_t)L_50+(int32_t)1));
	}

IL_00ef:
	{
		int32_t L_51 = V_7;
		int32_t L_52 = V_0;
		if ((((int32_t)L_51) < ((int32_t)L_52)))
		{
			goto IL_00c2;
		}
	}
	{
		int32_t L_53 = V_8;
		IL2CPP_RUNTIME_CLASS_INIT(DESTransform_t1_1201_il2cpp_TypeInfo_var);
		int32_t L_54 = ((DESTransform_t1_1201_StaticFields*)DESTransform_t1_1201_il2cpp_TypeInfo_var->static_fields)->___KEY_BYTE_SIZE_13;
		V_12 = ((int32_t)((int32_t)L_53*(int32_t)L_54));
		V_7 = 0;
		ByteU5BU5D_t1_109* L_55 = ((DESTransform_t1_1201_StaticFields*)DESTransform_t1_1201_il2cpp_TypeInfo_var->static_fields)->___PC2_22;
		V_14 = L_55;
		V_15 = 0;
		goto IL_0156;
	}

IL_0113:
	{
		ByteU5BU5D_t1_109* L_56 = V_14;
		int32_t L_57 = V_15;
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, L_57);
		int32_t L_58 = L_57;
		V_13 = (*(uint8_t*)(uint8_t*)SZArrayLdElema(L_56, L_58, sizeof(uint8_t)));
		ByteU5BU5D_t1_109* L_59 = V_2;
		uint8_t L_60 = V_13;
		NullCheck(L_59);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_59, L_60);
		uint8_t L_61 = L_60;
		if (!(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_59, L_61, sizeof(uint8_t))))
		{
			goto IL_014a;
		}
	}
	{
		ByteU5BU5D_t1_109* L_62 = (__this->___keySchedule_16);
		int32_t L_63 = V_12;
		int32_t L_64 = V_7;
		NullCheck(L_62);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_62, ((int32_t)((int32_t)L_63+(int32_t)((int32_t)((int32_t)L_64/(int32_t)6)))));
		uint8_t* L_65 = ((uint8_t*)(uint8_t*)SZArrayLdElema(L_62, ((int32_t)((int32_t)L_63+(int32_t)((int32_t)((int32_t)L_64/(int32_t)6)))), sizeof(uint8_t)));
		int32_t L_66 = V_7;
		*((int8_t*)(L_65)) = (int8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)(*((uint8_t*)L_65))|(int32_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((int32_t)128)>>(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_66%(int32_t)6))+(int32_t)2))&(int32_t)((int32_t)31)))))))))))));
	}

IL_014a:
	{
		int32_t L_67 = V_7;
		V_7 = ((int32_t)((int32_t)L_67+(int32_t)1));
		int32_t L_68 = V_15;
		V_15 = ((int32_t)((int32_t)L_68+(int32_t)1));
	}

IL_0156:
	{
		int32_t L_69 = V_15;
		ByteU5BU5D_t1_109* L_70 = V_14;
		NullCheck(L_70);
		if ((((int32_t)L_69) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_70)->max_length)))))))
		{
			goto IL_0113;
		}
	}
	{
		int32_t L_71 = V_8;
		V_8 = ((int32_t)((int32_t)L_71+(int32_t)1));
	}

IL_0167:
	{
		int32_t L_72 = V_8;
		IL2CPP_RUNTIME_CLASS_INIT(DESTransform_t1_1201_il2cpp_TypeInfo_var);
		int32_t L_73 = ((DESTransform_t1_1201_StaticFields*)DESTransform_t1_1201_il2cpp_TypeInfo_var->static_fields)->___KEY_BYTE_SIZE_13;
		if ((((int32_t)L_72) < ((int32_t)((int32_t)((int32_t)L_73*(int32_t)2)))))
		{
			goto IL_0075;
		}
	}
	{
		return;
	}
}
// System.Void System.Security.Cryptography.DESTransform::ProcessBlock(System.Byte[],System.Byte[])
extern TypeInfo* DESTransform_t1_1201_il2cpp_TypeInfo_var;
extern "C" void DESTransform_ProcessBlock_m1_10273 (DESTransform_t1_1201 * __this, ByteU5BU5D_t1_109* ___input, ByteU5BU5D_t1_109* ___output, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DESTransform_t1_1201_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(876);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	uint32_t V_1 = 0;
	uint32_t V_2 = 0;
	uint32_t V_3 = 0;
	{
		ByteU5BU5D_t1_109* L_0 = ___input;
		UInt32U5BU5D_t1_142* L_1 = (__this->___dwordBuff_18);
		IL2CPP_RUNTIME_CLASS_INIT(DESTransform_t1_1201_il2cpp_TypeInfo_var);
		int32_t L_2 = ((DESTransform_t1_1201_StaticFields*)DESTransform_t1_1201_il2cpp_TypeInfo_var->static_fields)->___BLOCK_BYTE_SIZE_15;
		Buffer_BlockCopy_m1_13338(NULL /*static, unused*/, (Array_t *)(Array_t *)L_0, 0, (Array_t *)(Array_t *)L_1, 0, L_2, /*hidden argument*/NULL);
		bool L_3 = (((SymmetricTransform_t1_176 *)__this)->___encrypt_1);
		if (!L_3)
		{
			goto IL_00fe;
		}
	}
	{
		UInt32U5BU5D_t1_142* L_4 = (__this->___dwordBuff_18);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		int32_t L_5 = 0;
		V_0 = (*(uint32_t*)(uint32_t*)SZArrayLdElema(L_4, L_5, sizeof(uint32_t)));
		UInt32U5BU5D_t1_142* L_6 = (__this->___dwordBuff_18);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 1);
		int32_t L_7 = 1;
		V_1 = (*(uint32_t*)(uint32_t*)SZArrayLdElema(L_6, L_7, sizeof(uint32_t)));
		uint32_t L_8 = V_0;
		uint32_t L_9 = V_1;
		uint32_t L_10 = DESTransform_CipherFunct_m1_10269(__this, L_9, 0, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_8^(int32_t)L_10));
		uint32_t L_11 = V_1;
		uint32_t L_12 = V_0;
		uint32_t L_13 = DESTransform_CipherFunct_m1_10269(__this, L_12, 1, /*hidden argument*/NULL);
		V_1 = ((int32_t)((int32_t)L_11^(int32_t)L_13));
		uint32_t L_14 = V_0;
		uint32_t L_15 = V_1;
		uint32_t L_16 = DESTransform_CipherFunct_m1_10269(__this, L_15, 2, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_14^(int32_t)L_16));
		uint32_t L_17 = V_1;
		uint32_t L_18 = V_0;
		uint32_t L_19 = DESTransform_CipherFunct_m1_10269(__this, L_18, 3, /*hidden argument*/NULL);
		V_1 = ((int32_t)((int32_t)L_17^(int32_t)L_19));
		uint32_t L_20 = V_0;
		uint32_t L_21 = V_1;
		uint32_t L_22 = DESTransform_CipherFunct_m1_10269(__this, L_21, 4, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_20^(int32_t)L_22));
		uint32_t L_23 = V_1;
		uint32_t L_24 = V_0;
		uint32_t L_25 = DESTransform_CipherFunct_m1_10269(__this, L_24, 5, /*hidden argument*/NULL);
		V_1 = ((int32_t)((int32_t)L_23^(int32_t)L_25));
		uint32_t L_26 = V_0;
		uint32_t L_27 = V_1;
		uint32_t L_28 = DESTransform_CipherFunct_m1_10269(__this, L_27, 6, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_26^(int32_t)L_28));
		uint32_t L_29 = V_1;
		uint32_t L_30 = V_0;
		uint32_t L_31 = DESTransform_CipherFunct_m1_10269(__this, L_30, 7, /*hidden argument*/NULL);
		V_1 = ((int32_t)((int32_t)L_29^(int32_t)L_31));
		uint32_t L_32 = V_0;
		uint32_t L_33 = V_1;
		uint32_t L_34 = DESTransform_CipherFunct_m1_10269(__this, L_33, 8, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_32^(int32_t)L_34));
		uint32_t L_35 = V_1;
		uint32_t L_36 = V_0;
		uint32_t L_37 = DESTransform_CipherFunct_m1_10269(__this, L_36, ((int32_t)9), /*hidden argument*/NULL);
		V_1 = ((int32_t)((int32_t)L_35^(int32_t)L_37));
		uint32_t L_38 = V_0;
		uint32_t L_39 = V_1;
		uint32_t L_40 = DESTransform_CipherFunct_m1_10269(__this, L_39, ((int32_t)10), /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_38^(int32_t)L_40));
		uint32_t L_41 = V_1;
		uint32_t L_42 = V_0;
		uint32_t L_43 = DESTransform_CipherFunct_m1_10269(__this, L_42, ((int32_t)11), /*hidden argument*/NULL);
		V_1 = ((int32_t)((int32_t)L_41^(int32_t)L_43));
		uint32_t L_44 = V_0;
		uint32_t L_45 = V_1;
		uint32_t L_46 = DESTransform_CipherFunct_m1_10269(__this, L_45, ((int32_t)12), /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_44^(int32_t)L_46));
		uint32_t L_47 = V_1;
		uint32_t L_48 = V_0;
		uint32_t L_49 = DESTransform_CipherFunct_m1_10269(__this, L_48, ((int32_t)13), /*hidden argument*/NULL);
		V_1 = ((int32_t)((int32_t)L_47^(int32_t)L_49));
		uint32_t L_50 = V_0;
		uint32_t L_51 = V_1;
		uint32_t L_52 = DESTransform_CipherFunct_m1_10269(__this, L_51, ((int32_t)14), /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_50^(int32_t)L_52));
		uint32_t L_53 = V_1;
		uint32_t L_54 = V_0;
		uint32_t L_55 = DESTransform_CipherFunct_m1_10269(__this, L_54, ((int32_t)15), /*hidden argument*/NULL);
		V_1 = ((int32_t)((int32_t)L_53^(int32_t)L_55));
		UInt32U5BU5D_t1_142* L_56 = (__this->___dwordBuff_18);
		uint32_t L_57 = V_1;
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, 0);
		*((uint32_t*)(uint32_t*)SZArrayLdElema(L_56, 0, sizeof(uint32_t))) = (uint32_t)L_57;
		UInt32U5BU5D_t1_142* L_58 = (__this->___dwordBuff_18);
		uint32_t L_59 = V_0;
		NullCheck(L_58);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_58, 1);
		*((uint32_t*)(uint32_t*)SZArrayLdElema(L_58, 1, sizeof(uint32_t))) = (uint32_t)L_59;
		goto IL_01d9;
	}

IL_00fe:
	{
		UInt32U5BU5D_t1_142* L_60 = (__this->___dwordBuff_18);
		NullCheck(L_60);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_60, 0);
		int32_t L_61 = 0;
		V_2 = (*(uint32_t*)(uint32_t*)SZArrayLdElema(L_60, L_61, sizeof(uint32_t)));
		UInt32U5BU5D_t1_142* L_62 = (__this->___dwordBuff_18);
		NullCheck(L_62);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_62, 1);
		int32_t L_63 = 1;
		V_3 = (*(uint32_t*)(uint32_t*)SZArrayLdElema(L_62, L_63, sizeof(uint32_t)));
		uint32_t L_64 = V_2;
		uint32_t L_65 = V_3;
		uint32_t L_66 = DESTransform_CipherFunct_m1_10269(__this, L_65, ((int32_t)15), /*hidden argument*/NULL);
		V_2 = ((int32_t)((int32_t)L_64^(int32_t)L_66));
		uint32_t L_67 = V_3;
		uint32_t L_68 = V_2;
		uint32_t L_69 = DESTransform_CipherFunct_m1_10269(__this, L_68, ((int32_t)14), /*hidden argument*/NULL);
		V_3 = ((int32_t)((int32_t)L_67^(int32_t)L_69));
		uint32_t L_70 = V_2;
		uint32_t L_71 = V_3;
		uint32_t L_72 = DESTransform_CipherFunct_m1_10269(__this, L_71, ((int32_t)13), /*hidden argument*/NULL);
		V_2 = ((int32_t)((int32_t)L_70^(int32_t)L_72));
		uint32_t L_73 = V_3;
		uint32_t L_74 = V_2;
		uint32_t L_75 = DESTransform_CipherFunct_m1_10269(__this, L_74, ((int32_t)12), /*hidden argument*/NULL);
		V_3 = ((int32_t)((int32_t)L_73^(int32_t)L_75));
		uint32_t L_76 = V_2;
		uint32_t L_77 = V_3;
		uint32_t L_78 = DESTransform_CipherFunct_m1_10269(__this, L_77, ((int32_t)11), /*hidden argument*/NULL);
		V_2 = ((int32_t)((int32_t)L_76^(int32_t)L_78));
		uint32_t L_79 = V_3;
		uint32_t L_80 = V_2;
		uint32_t L_81 = DESTransform_CipherFunct_m1_10269(__this, L_80, ((int32_t)10), /*hidden argument*/NULL);
		V_3 = ((int32_t)((int32_t)L_79^(int32_t)L_81));
		uint32_t L_82 = V_2;
		uint32_t L_83 = V_3;
		uint32_t L_84 = DESTransform_CipherFunct_m1_10269(__this, L_83, ((int32_t)9), /*hidden argument*/NULL);
		V_2 = ((int32_t)((int32_t)L_82^(int32_t)L_84));
		uint32_t L_85 = V_3;
		uint32_t L_86 = V_2;
		uint32_t L_87 = DESTransform_CipherFunct_m1_10269(__this, L_86, 8, /*hidden argument*/NULL);
		V_3 = ((int32_t)((int32_t)L_85^(int32_t)L_87));
		uint32_t L_88 = V_2;
		uint32_t L_89 = V_3;
		uint32_t L_90 = DESTransform_CipherFunct_m1_10269(__this, L_89, 7, /*hidden argument*/NULL);
		V_2 = ((int32_t)((int32_t)L_88^(int32_t)L_90));
		uint32_t L_91 = V_3;
		uint32_t L_92 = V_2;
		uint32_t L_93 = DESTransform_CipherFunct_m1_10269(__this, L_92, 6, /*hidden argument*/NULL);
		V_3 = ((int32_t)((int32_t)L_91^(int32_t)L_93));
		uint32_t L_94 = V_2;
		uint32_t L_95 = V_3;
		uint32_t L_96 = DESTransform_CipherFunct_m1_10269(__this, L_95, 5, /*hidden argument*/NULL);
		V_2 = ((int32_t)((int32_t)L_94^(int32_t)L_96));
		uint32_t L_97 = V_3;
		uint32_t L_98 = V_2;
		uint32_t L_99 = DESTransform_CipherFunct_m1_10269(__this, L_98, 4, /*hidden argument*/NULL);
		V_3 = ((int32_t)((int32_t)L_97^(int32_t)L_99));
		uint32_t L_100 = V_2;
		uint32_t L_101 = V_3;
		uint32_t L_102 = DESTransform_CipherFunct_m1_10269(__this, L_101, 3, /*hidden argument*/NULL);
		V_2 = ((int32_t)((int32_t)L_100^(int32_t)L_102));
		uint32_t L_103 = V_3;
		uint32_t L_104 = V_2;
		uint32_t L_105 = DESTransform_CipherFunct_m1_10269(__this, L_104, 2, /*hidden argument*/NULL);
		V_3 = ((int32_t)((int32_t)L_103^(int32_t)L_105));
		uint32_t L_106 = V_2;
		uint32_t L_107 = V_3;
		uint32_t L_108 = DESTransform_CipherFunct_m1_10269(__this, L_107, 1, /*hidden argument*/NULL);
		V_2 = ((int32_t)((int32_t)L_106^(int32_t)L_108));
		uint32_t L_109 = V_3;
		uint32_t L_110 = V_2;
		uint32_t L_111 = DESTransform_CipherFunct_m1_10269(__this, L_110, 0, /*hidden argument*/NULL);
		V_3 = ((int32_t)((int32_t)L_109^(int32_t)L_111));
		UInt32U5BU5D_t1_142* L_112 = (__this->___dwordBuff_18);
		uint32_t L_113 = V_3;
		NullCheck(L_112);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_112, 0);
		*((uint32_t*)(uint32_t*)SZArrayLdElema(L_112, 0, sizeof(uint32_t))) = (uint32_t)L_113;
		UInt32U5BU5D_t1_142* L_114 = (__this->___dwordBuff_18);
		uint32_t L_115 = V_2;
		NullCheck(L_114);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_114, 1);
		*((uint32_t*)(uint32_t*)SZArrayLdElema(L_114, 1, sizeof(uint32_t))) = (uint32_t)L_115;
	}

IL_01d9:
	{
		UInt32U5BU5D_t1_142* L_116 = (__this->___dwordBuff_18);
		ByteU5BU5D_t1_109* L_117 = ___output;
		IL2CPP_RUNTIME_CLASS_INIT(DESTransform_t1_1201_il2cpp_TypeInfo_var);
		int32_t L_118 = ((DESTransform_t1_1201_StaticFields*)DESTransform_t1_1201_il2cpp_TypeInfo_var->static_fields)->___BLOCK_BYTE_SIZE_15;
		Buffer_BlockCopy_m1_13338(NULL /*static, unused*/, (Array_t *)(Array_t *)L_116, 0, (Array_t *)(Array_t *)L_117, 0, L_118, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.Cryptography.DESTransform::ECB(System.Byte[],System.Byte[])
extern TypeInfo* DESTransform_t1_1201_il2cpp_TypeInfo_var;
extern "C" void DESTransform_ECB_m1_10274 (DESTransform_t1_1201 * __this, ByteU5BU5D_t1_109* ___input, ByteU5BU5D_t1_109* ___output, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DESTransform_t1_1201_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(876);
		s_Il2CppMethodIntialized = true;
	}
	{
		ByteU5BU5D_t1_109* L_0 = ___input;
		ByteU5BU5D_t1_109* L_1 = ___output;
		IL2CPP_RUNTIME_CLASS_INIT(DESTransform_t1_1201_il2cpp_TypeInfo_var);
		UInt32U5BU5D_t1_142* L_2 = ((DESTransform_t1_1201_StaticFields*)DESTransform_t1_1201_il2cpp_TypeInfo_var->static_fields)->___ipTab_23;
		DESTransform_Permutation_m1_10270(NULL /*static, unused*/, L_0, L_1, L_2, 0, /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_3 = ___output;
		ByteU5BU5D_t1_109* L_4 = (__this->___byteBuff_17);
		DESTransform_ProcessBlock_m1_10273(__this, L_3, L_4, /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_5 = (__this->___byteBuff_17);
		ByteU5BU5D_t1_109* L_6 = ___output;
		UInt32U5BU5D_t1_142* L_7 = ((DESTransform_t1_1201_StaticFields*)DESTransform_t1_1201_il2cpp_TypeInfo_var->static_fields)->___fpTab_24;
		DESTransform_Permutation_m1_10270(NULL /*static, unused*/, L_5, L_6, L_7, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Byte[] System.Security.Cryptography.DESTransform::GetStrongKey()
extern TypeInfo* DESTransform_t1_1201_il2cpp_TypeInfo_var;
extern TypeInfo* DES_t1_1199_il2cpp_TypeInfo_var;
extern "C" ByteU5BU5D_t1_109* DESTransform_GetStrongKey_m1_10275 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DESTransform_t1_1201_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(876);
		DES_t1_1199_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(875);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(DESTransform_t1_1201_il2cpp_TypeInfo_var);
		int32_t L_0 = ((DESTransform_t1_1201_StaticFields*)DESTransform_t1_1201_il2cpp_TypeInfo_var->static_fields)->___KEY_BYTE_SIZE_13;
		ByteU5BU5D_t1_109* L_1 = KeyBuilder_Key_m1_1915(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_001b;
	}

IL_0010:
	{
		IL2CPP_RUNTIME_CLASS_INIT(DESTransform_t1_1201_il2cpp_TypeInfo_var);
		int32_t L_2 = ((DESTransform_t1_1201_StaticFields*)DESTransform_t1_1201_il2cpp_TypeInfo_var->static_fields)->___KEY_BYTE_SIZE_13;
		ByteU5BU5D_t1_109* L_3 = KeyBuilder_Key_m1_1915(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
	}

IL_001b:
	{
		ByteU5BU5D_t1_109* L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(DES_t1_1199_il2cpp_TypeInfo_var);
		bool L_5 = DES_IsWeakKey_m1_10263(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0010;
		}
	}
	{
		ByteU5BU5D_t1_109* L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(DES_t1_1199_il2cpp_TypeInfo_var);
		bool L_7 = DES_IsSemiWeakKey_m1_10264(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0010;
		}
	}
	{
		ByteU5BU5D_t1_109* L_8 = V_0;
		return L_8;
	}
}
// System.Void System.Security.Cryptography.DESCryptoServiceProvider::.ctor()
extern TypeInfo* DES_t1_1199_il2cpp_TypeInfo_var;
extern "C" void DESCryptoServiceProvider__ctor_m1_10276 (DESCryptoServiceProvider_t1_1202 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DES_t1_1199_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(875);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DES_t1_1199_il2cpp_TypeInfo_var);
		DES__ctor_m1_10259(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.DESCryptoServiceProvider::CreateDecryptor(System.Byte[],System.Byte[])
extern TypeInfo* DESTransform_t1_1201_il2cpp_TypeInfo_var;
extern "C" Object_t * DESCryptoServiceProvider_CreateDecryptor_m1_10277 (DESCryptoServiceProvider_t1_1202 * __this, ByteU5BU5D_t1_109* ___rgbKey, ByteU5BU5D_t1_109* ___rgbIV, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DESTransform_t1_1201_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(876);
		s_Il2CppMethodIntialized = true;
	}
	{
		ByteU5BU5D_t1_109* L_0 = ___rgbKey;
		ByteU5BU5D_t1_109* L_1 = ___rgbIV;
		DESTransform_t1_1201 * L_2 = (DESTransform_t1_1201 *)il2cpp_codegen_object_new (DESTransform_t1_1201_il2cpp_TypeInfo_var);
		DESTransform__ctor_m1_10267(L_2, __this, 0, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.DESCryptoServiceProvider::CreateEncryptor(System.Byte[],System.Byte[])
extern TypeInfo* DESTransform_t1_1201_il2cpp_TypeInfo_var;
extern "C" Object_t * DESCryptoServiceProvider_CreateEncryptor_m1_10278 (DESCryptoServiceProvider_t1_1202 * __this, ByteU5BU5D_t1_109* ___rgbKey, ByteU5BU5D_t1_109* ___rgbIV, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DESTransform_t1_1201_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(876);
		s_Il2CppMethodIntialized = true;
	}
	{
		ByteU5BU5D_t1_109* L_0 = ___rgbKey;
		ByteU5BU5D_t1_109* L_1 = ___rgbIV;
		DESTransform_t1_1201 * L_2 = (DESTransform_t1_1201 *)il2cpp_codegen_object_new (DESTransform_t1_1201_il2cpp_TypeInfo_var);
		DESTransform__ctor_m1_10267(L_2, __this, 1, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void System.Security.Cryptography.DESCryptoServiceProvider::GenerateIV()
extern TypeInfo* DESTransform_t1_1201_il2cpp_TypeInfo_var;
extern "C" void DESCryptoServiceProvider_GenerateIV_m1_10279 (DESCryptoServiceProvider_t1_1202 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DESTransform_t1_1201_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(876);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DESTransform_t1_1201_il2cpp_TypeInfo_var);
		int32_t L_0 = ((DESTransform_t1_1201_StaticFields*)DESTransform_t1_1201_il2cpp_TypeInfo_var->static_fields)->___BLOCK_BYTE_SIZE_15;
		ByteU5BU5D_t1_109* L_1 = KeyBuilder_IV_m1_1916(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		((SymmetricAlgorithm_t1_166 *)__this)->___IVValue_1 = L_1;
		return;
	}
}
// System.Void System.Security.Cryptography.DESCryptoServiceProvider::GenerateKey()
extern TypeInfo* DESTransform_t1_1201_il2cpp_TypeInfo_var;
extern "C" void DESCryptoServiceProvider_GenerateKey_m1_10280 (DESCryptoServiceProvider_t1_1202 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DESTransform_t1_1201_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(876);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DESTransform_t1_1201_il2cpp_TypeInfo_var);
		ByteU5BU5D_t1_109* L_0 = DESTransform_GetStrongKey_m1_10275(NULL /*static, unused*/, /*hidden argument*/NULL);
		((SymmetricAlgorithm_t1_166 *)__this)->___KeyValue_3 = L_0;
		return;
	}
}
// System.Void System.Security.Cryptography.DSA::.ctor()
extern "C" void DSA__ctor_m1_10281 (DSA_t1_160 * __this, const MethodInfo* method)
{
	{
		AsymmetricAlgorithm__ctor_m1_10157(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Security.Cryptography.DSA System.Security.Cryptography.DSA::Create()
extern Il2CppCodeGenString* _stringLiteral2574;
extern "C" DSA_t1_160 * DSA_Create_m1_10282 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral2574 = il2cpp_codegen_string_literal_from_index(2574);
		s_Il2CppMethodIntialized = true;
	}
	{
		DSA_t1_160 * L_0 = DSA_Create_m1_10283(NULL /*static, unused*/, _stringLiteral2574, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Security.Cryptography.DSA System.Security.Cryptography.DSA::Create(System.String)
extern TypeInfo* CryptoConfig_t1_1191_il2cpp_TypeInfo_var;
extern TypeInfo* DSA_t1_160_il2cpp_TypeInfo_var;
extern "C" DSA_t1_160 * DSA_Create_m1_10283 (Object_t * __this /* static, unused */, String_t* ___algName, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CryptoConfig_t1_1191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(191);
		DSA_t1_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(200);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___algName;
		IL2CPP_RUNTIME_CLASS_INIT(CryptoConfig_t1_1191_il2cpp_TypeInfo_var);
		Object_t * L_1 = CryptoConfig_CreateFromName_m1_10198(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return ((DSA_t1_160 *)CastclassClass(L_1, DSA_t1_160_il2cpp_TypeInfo_var));
	}
}
// System.Void System.Security.Cryptography.DSA::ZeroizePrivateKey(System.Security.Cryptography.DSAParameters)
extern "C" void DSA_ZeroizePrivateKey_m1_10284 (DSA_t1_160 * __this, DSAParameters_t1_1204  ___parameters, const MethodInfo* method)
{
	{
		ByteU5BU5D_t1_109* L_0 = ((&___parameters)->___X_6);
		if (!L_0)
		{
			goto IL_0022;
		}
	}
	{
		ByteU5BU5D_t1_109* L_1 = ((&___parameters)->___X_6);
		ByteU5BU5D_t1_109* L_2 = ((&___parameters)->___X_6);
		NullCheck(L_2);
		Array_Clear_m1_1037(NULL /*static, unused*/, (Array_t *)(Array_t *)L_1, 0, (((int32_t)((int32_t)(((Array_t *)L_2)->max_length)))), /*hidden argument*/NULL);
	}

IL_0022:
	{
		return;
	}
}
// System.Void System.Security.Cryptography.DSA::FromXmlString(System.String)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* DSAParameters_t1_1204_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2699;
extern Il2CppCodeGenString* _stringLiteral2700;
extern Il2CppCodeGenString* _stringLiteral2701;
extern Il2CppCodeGenString* _stringLiteral130;
extern Il2CppCodeGenString* _stringLiteral2702;
extern Il2CppCodeGenString* _stringLiteral2703;
extern Il2CppCodeGenString* _stringLiteral2704;
extern Il2CppCodeGenString* _stringLiteral2705;
extern Il2CppCodeGenString* _stringLiteral2706;
extern "C" void DSA_FromXmlString_m1_10285 (DSA_t1_160 * __this, String_t* ___xmlString, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		DSAParameters_t1_1204_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(198);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		_stringLiteral2699 = il2cpp_codegen_string_literal_from_index(2699);
		_stringLiteral2700 = il2cpp_codegen_string_literal_from_index(2700);
		_stringLiteral2701 = il2cpp_codegen_string_literal_from_index(2701);
		_stringLiteral130 = il2cpp_codegen_string_literal_from_index(130);
		_stringLiteral2702 = il2cpp_codegen_string_literal_from_index(2702);
		_stringLiteral2703 = il2cpp_codegen_string_literal_from_index(2703);
		_stringLiteral2704 = il2cpp_codegen_string_literal_from_index(2704);
		_stringLiteral2705 = il2cpp_codegen_string_literal_from_index(2705);
		_stringLiteral2706 = il2cpp_codegen_string_literal_from_index(2706);
		s_Il2CppMethodIntialized = true;
	}
	DSAParameters_t1_1204  V_0 = {0};
	ByteU5BU5D_t1_109* V_1 = {0};
	ByteU5BU5D_t1_109* V_2 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___xmlString;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, _stringLiteral2699, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		Initobj (DSAParameters_t1_1204_il2cpp_TypeInfo_var, (&V_0));
	}

IL_0019:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			{
				String_t* L_2 = ___xmlString;
				ByteU5BU5D_t1_109* L_3 = AsymmetricAlgorithm_GetNamedParam_m1_10165(NULL /*static, unused*/, L_2, _stringLiteral2700, /*hidden argument*/NULL);
				(&V_0)->___P_3 = L_3;
				String_t* L_4 = ___xmlString;
				ByteU5BU5D_t1_109* L_5 = AsymmetricAlgorithm_GetNamedParam_m1_10165(NULL /*static, unused*/, L_4, _stringLiteral2701, /*hidden argument*/NULL);
				(&V_0)->___Q_4 = L_5;
				String_t* L_6 = ___xmlString;
				ByteU5BU5D_t1_109* L_7 = AsymmetricAlgorithm_GetNamedParam_m1_10165(NULL /*static, unused*/, L_6, _stringLiteral130, /*hidden argument*/NULL);
				(&V_0)->___G_1 = L_7;
				String_t* L_8 = ___xmlString;
				ByteU5BU5D_t1_109* L_9 = AsymmetricAlgorithm_GetNamedParam_m1_10165(NULL /*static, unused*/, L_8, _stringLiteral2702, /*hidden argument*/NULL);
				(&V_0)->___J_2 = L_9;
				String_t* L_10 = ___xmlString;
				ByteU5BU5D_t1_109* L_11 = AsymmetricAlgorithm_GetNamedParam_m1_10165(NULL /*static, unused*/, L_10, _stringLiteral2703, /*hidden argument*/NULL);
				(&V_0)->___Y_7 = L_11;
				String_t* L_12 = ___xmlString;
				ByteU5BU5D_t1_109* L_13 = AsymmetricAlgorithm_GetNamedParam_m1_10165(NULL /*static, unused*/, L_12, _stringLiteral2704, /*hidden argument*/NULL);
				(&V_0)->___X_6 = L_13;
				String_t* L_14 = ___xmlString;
				ByteU5BU5D_t1_109* L_15 = AsymmetricAlgorithm_GetNamedParam_m1_10165(NULL /*static, unused*/, L_14, _stringLiteral2705, /*hidden argument*/NULL);
				(&V_0)->___Seed_5 = L_15;
				String_t* L_16 = ___xmlString;
				ByteU5BU5D_t1_109* L_17 = AsymmetricAlgorithm_GetNamedParam_m1_10165(NULL /*static, unused*/, L_16, _stringLiteral2706, /*hidden argument*/NULL);
				V_1 = L_17;
				ByteU5BU5D_t1_109* L_18 = V_1;
				if (!L_18)
				{
					goto IL_00ca;
				}
			}

IL_00a9:
			{
				V_2 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 4));
				ByteU5BU5D_t1_109* L_19 = V_1;
				ByteU5BU5D_t1_109* L_20 = V_2;
				ByteU5BU5D_t1_109* L_21 = V_1;
				NullCheck(L_21);
				Buffer_BlockCopy_m1_13338(NULL /*static, unused*/, (Array_t *)(Array_t *)L_19, 0, (Array_t *)(Array_t *)L_20, 0, (((int32_t)((int32_t)(((Array_t *)L_21)->max_length)))), /*hidden argument*/NULL);
				ByteU5BU5D_t1_109* L_22 = V_2;
				int32_t L_23 = BitConverterLE_ToInt32_m1_2466(NULL /*static, unused*/, L_22, 0, /*hidden argument*/NULL);
				(&V_0)->___Counter_0 = L_23;
			}

IL_00ca:
			{
				DSAParameters_t1_1204  L_24 = V_0;
				VirtActionInvoker1< DSAParameters_t1_1204  >::Invoke(15 /* System.Void System.Security.Cryptography.DSA::ImportParameters(System.Security.Cryptography.DSAParameters) */, __this, L_24);
				IL2CPP_LEAVE(0xED, FINALLY_00e5);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t1_33 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (Object_t_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_00d6;
			throw e;
		}

CATCH_00d6:
		{ // begin catch(System.Object)
			{
				DSAParameters_t1_1204  L_25 = V_0;
				DSA_ZeroizePrivateKey_m1_10284(__this, L_25, /*hidden argument*/NULL);
				il2cpp_codegen_raise_exception((Il2CppCodeGenException*)__exception_local);
			}

IL_00e0:
			{
				IL2CPP_LEAVE(0xED, FINALLY_00e5);
			}
		} // end catch (depth: 2)
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_00e5;
	}

FINALLY_00e5:
	{ // begin finally (depth: 1)
		DSAParameters_t1_1204  L_26 = V_0;
		DSA_ZeroizePrivateKey_m1_10284(__this, L_26, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(229)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(229)
	{
		IL2CPP_JUMP_TBL(0xED, IL_00ed)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_00ed:
	{
		return;
	}
}
// System.String System.Security.Cryptography.DSA::ToXmlString(System.Boolean)
extern TypeInfo* StringBuilder_t1_247_il2cpp_TypeInfo_var;
extern TypeInfo* Convert_t1_1522_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2707;
extern Il2CppCodeGenString* _stringLiteral459;
extern Il2CppCodeGenString* _stringLiteral460;
extern Il2CppCodeGenString* _stringLiteral461;
extern Il2CppCodeGenString* _stringLiteral462;
extern Il2CppCodeGenString* _stringLiteral2708;
extern Il2CppCodeGenString* _stringLiteral2709;
extern Il2CppCodeGenString* _stringLiteral2710;
extern Il2CppCodeGenString* _stringLiteral2711;
extern Il2CppCodeGenString* _stringLiteral2712;
extern Il2CppCodeGenString* _stringLiteral2713;
extern Il2CppCodeGenString* _stringLiteral2714;
extern Il2CppCodeGenString* _stringLiteral2715;
extern Il2CppCodeGenString* _stringLiteral2716;
extern Il2CppCodeGenString* _stringLiteral2717;
extern Il2CppCodeGenString* _stringLiteral2718;
extern Il2CppCodeGenString* _stringLiteral2719;
extern Il2CppCodeGenString* _stringLiteral2720;
extern Il2CppCodeGenString* _stringLiteral2704;
extern Il2CppCodeGenString* _stringLiteral2721;
extern "C" String_t* DSA_ToXmlString_m1_10286 (DSA_t1_160 * __this, bool ___includePrivateParameters, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringBuilder_t1_247_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		Convert_t1_1522_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		_stringLiteral2707 = il2cpp_codegen_string_literal_from_index(2707);
		_stringLiteral459 = il2cpp_codegen_string_literal_from_index(459);
		_stringLiteral460 = il2cpp_codegen_string_literal_from_index(460);
		_stringLiteral461 = il2cpp_codegen_string_literal_from_index(461);
		_stringLiteral462 = il2cpp_codegen_string_literal_from_index(462);
		_stringLiteral2708 = il2cpp_codegen_string_literal_from_index(2708);
		_stringLiteral2709 = il2cpp_codegen_string_literal_from_index(2709);
		_stringLiteral2710 = il2cpp_codegen_string_literal_from_index(2710);
		_stringLiteral2711 = il2cpp_codegen_string_literal_from_index(2711);
		_stringLiteral2712 = il2cpp_codegen_string_literal_from_index(2712);
		_stringLiteral2713 = il2cpp_codegen_string_literal_from_index(2713);
		_stringLiteral2714 = il2cpp_codegen_string_literal_from_index(2714);
		_stringLiteral2715 = il2cpp_codegen_string_literal_from_index(2715);
		_stringLiteral2716 = il2cpp_codegen_string_literal_from_index(2716);
		_stringLiteral2717 = il2cpp_codegen_string_literal_from_index(2717);
		_stringLiteral2718 = il2cpp_codegen_string_literal_from_index(2718);
		_stringLiteral2719 = il2cpp_codegen_string_literal_from_index(2719);
		_stringLiteral2720 = il2cpp_codegen_string_literal_from_index(2720);
		_stringLiteral2704 = il2cpp_codegen_string_literal_from_index(2704);
		_stringLiteral2721 = il2cpp_codegen_string_literal_from_index(2721);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t1_247 * V_0 = {0};
	DSAParameters_t1_1204  V_1 = {0};
	ByteU5BU5D_t1_109* V_2 = {0};
	int32_t V_3 = 0;
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		StringBuilder_t1_247 * L_0 = (StringBuilder_t1_247 *)il2cpp_codegen_object_new (StringBuilder_t1_247_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1_12414(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = ___includePrivateParameters;
		DSAParameters_t1_1204  L_2 = (DSAParameters_t1_1204 )VirtFuncInvoker1< DSAParameters_t1_1204 , bool >::Invoke(14 /* System.Security.Cryptography.DSAParameters System.Security.Cryptography.DSA::ExportParameters(System.Boolean) */, __this, L_1);
		V_1 = L_2;
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		{
			StringBuilder_t1_247 * L_3 = V_0;
			NullCheck(L_3);
			StringBuilder_Append_m1_12438(L_3, _stringLiteral2707, /*hidden argument*/NULL);
			StringBuilder_t1_247 * L_4 = V_0;
			NullCheck(L_4);
			StringBuilder_Append_m1_12438(L_4, _stringLiteral459, /*hidden argument*/NULL);
			StringBuilder_t1_247 * L_5 = V_0;
			ByteU5BU5D_t1_109* L_6 = ((&V_1)->___P_3);
			IL2CPP_RUNTIME_CLASS_INIT(Convert_t1_1522_il2cpp_TypeInfo_var);
			String_t* L_7 = Convert_ToBase64String_m1_13445(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
			NullCheck(L_5);
			StringBuilder_Append_m1_12438(L_5, L_7, /*hidden argument*/NULL);
			StringBuilder_t1_247 * L_8 = V_0;
			NullCheck(L_8);
			StringBuilder_Append_m1_12438(L_8, _stringLiteral460, /*hidden argument*/NULL);
			StringBuilder_t1_247 * L_9 = V_0;
			NullCheck(L_9);
			StringBuilder_Append_m1_12438(L_9, _stringLiteral461, /*hidden argument*/NULL);
			StringBuilder_t1_247 * L_10 = V_0;
			ByteU5BU5D_t1_109* L_11 = ((&V_1)->___Q_4);
			String_t* L_12 = Convert_ToBase64String_m1_13445(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
			NullCheck(L_10);
			StringBuilder_Append_m1_12438(L_10, L_12, /*hidden argument*/NULL);
			StringBuilder_t1_247 * L_13 = V_0;
			NullCheck(L_13);
			StringBuilder_Append_m1_12438(L_13, _stringLiteral462, /*hidden argument*/NULL);
			StringBuilder_t1_247 * L_14 = V_0;
			NullCheck(L_14);
			StringBuilder_Append_m1_12438(L_14, _stringLiteral2708, /*hidden argument*/NULL);
			StringBuilder_t1_247 * L_15 = V_0;
			ByteU5BU5D_t1_109* L_16 = ((&V_1)->___G_1);
			String_t* L_17 = Convert_ToBase64String_m1_13445(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
			NullCheck(L_15);
			StringBuilder_Append_m1_12438(L_15, L_17, /*hidden argument*/NULL);
			StringBuilder_t1_247 * L_18 = V_0;
			NullCheck(L_18);
			StringBuilder_Append_m1_12438(L_18, _stringLiteral2709, /*hidden argument*/NULL);
			StringBuilder_t1_247 * L_19 = V_0;
			NullCheck(L_19);
			StringBuilder_Append_m1_12438(L_19, _stringLiteral2710, /*hidden argument*/NULL);
			StringBuilder_t1_247 * L_20 = V_0;
			ByteU5BU5D_t1_109* L_21 = ((&V_1)->___Y_7);
			String_t* L_22 = Convert_ToBase64String_m1_13445(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
			NullCheck(L_20);
			StringBuilder_Append_m1_12438(L_20, L_22, /*hidden argument*/NULL);
			StringBuilder_t1_247 * L_23 = V_0;
			NullCheck(L_23);
			StringBuilder_Append_m1_12438(L_23, _stringLiteral2711, /*hidden argument*/NULL);
			ByteU5BU5D_t1_109* L_24 = ((&V_1)->___J_2);
			if (!L_24)
			{
				goto IL_00fd;
			}
		}

IL_00d2:
		{
			StringBuilder_t1_247 * L_25 = V_0;
			NullCheck(L_25);
			StringBuilder_Append_m1_12438(L_25, _stringLiteral2712, /*hidden argument*/NULL);
			StringBuilder_t1_247 * L_26 = V_0;
			ByteU5BU5D_t1_109* L_27 = ((&V_1)->___J_2);
			IL2CPP_RUNTIME_CLASS_INIT(Convert_t1_1522_il2cpp_TypeInfo_var);
			String_t* L_28 = Convert_ToBase64String_m1_13445(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
			NullCheck(L_26);
			StringBuilder_Append_m1_12438(L_26, L_28, /*hidden argument*/NULL);
			StringBuilder_t1_247 * L_29 = V_0;
			NullCheck(L_29);
			StringBuilder_Append_m1_12438(L_29, _stringLiteral2713, /*hidden argument*/NULL);
		}

IL_00fd:
		{
			ByteU5BU5D_t1_109* L_30 = ((&V_1)->___Seed_5);
			if (!L_30)
			{
				goto IL_019c;
			}
		}

IL_0109:
		{
			StringBuilder_t1_247 * L_31 = V_0;
			NullCheck(L_31);
			StringBuilder_Append_m1_12438(L_31, _stringLiteral2714, /*hidden argument*/NULL);
			StringBuilder_t1_247 * L_32 = V_0;
			ByteU5BU5D_t1_109* L_33 = ((&V_1)->___Seed_5);
			IL2CPP_RUNTIME_CLASS_INIT(Convert_t1_1522_il2cpp_TypeInfo_var);
			String_t* L_34 = Convert_ToBase64String_m1_13445(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
			NullCheck(L_32);
			StringBuilder_Append_m1_12438(L_32, L_34, /*hidden argument*/NULL);
			StringBuilder_t1_247 * L_35 = V_0;
			NullCheck(L_35);
			StringBuilder_Append_m1_12438(L_35, _stringLiteral2715, /*hidden argument*/NULL);
			StringBuilder_t1_247 * L_36 = V_0;
			NullCheck(L_36);
			StringBuilder_Append_m1_12438(L_36, _stringLiteral2716, /*hidden argument*/NULL);
			int32_t L_37 = ((&V_1)->___Counter_0);
			if (!L_37)
			{
				goto IL_0184;
			}
		}

IL_014c:
		{
			int32_t L_38 = ((&V_1)->___Counter_0);
			ByteU5BU5D_t1_109* L_39 = BitConverterLE_GetBytes_m1_2453(NULL /*static, unused*/, L_38, /*hidden argument*/NULL);
			V_2 = L_39;
			ByteU5BU5D_t1_109* L_40 = V_2;
			NullCheck(L_40);
			V_3 = (((int32_t)((int32_t)(((Array_t *)L_40)->max_length))));
			goto IL_0166;
		}

IL_0162:
		{
			int32_t L_41 = V_3;
			V_3 = ((int32_t)((int32_t)L_41-(int32_t)1));
		}

IL_0166:
		{
			ByteU5BU5D_t1_109* L_42 = V_2;
			int32_t L_43 = V_3;
			NullCheck(L_42);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_42, ((int32_t)((int32_t)L_43-(int32_t)1)));
			int32_t L_44 = ((int32_t)((int32_t)L_43-(int32_t)1));
			if (!(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_42, L_44, sizeof(uint8_t))))
			{
				goto IL_0162;
			}
		}

IL_0170:
		{
			StringBuilder_t1_247 * L_45 = V_0;
			ByteU5BU5D_t1_109* L_46 = V_2;
			int32_t L_47 = V_3;
			IL2CPP_RUNTIME_CLASS_INIT(Convert_t1_1522_il2cpp_TypeInfo_var);
			String_t* L_48 = Convert_ToBase64String_m1_13446(NULL /*static, unused*/, L_46, 0, L_47, /*hidden argument*/NULL);
			NullCheck(L_45);
			StringBuilder_Append_m1_12438(L_45, L_48, /*hidden argument*/NULL);
			goto IL_0190;
		}

IL_0184:
		{
			StringBuilder_t1_247 * L_49 = V_0;
			NullCheck(L_49);
			StringBuilder_Append_m1_12438(L_49, _stringLiteral2717, /*hidden argument*/NULL);
		}

IL_0190:
		{
			StringBuilder_t1_247 * L_50 = V_0;
			NullCheck(L_50);
			StringBuilder_Append_m1_12438(L_50, _stringLiteral2718, /*hidden argument*/NULL);
		}

IL_019c:
		{
			ByteU5BU5D_t1_109* L_51 = ((&V_1)->___X_6);
			if (!L_51)
			{
				goto IL_01d8;
			}
		}

IL_01a8:
		{
			StringBuilder_t1_247 * L_52 = V_0;
			NullCheck(L_52);
			StringBuilder_Append_m1_12438(L_52, _stringLiteral2719, /*hidden argument*/NULL);
			StringBuilder_t1_247 * L_53 = V_0;
			ByteU5BU5D_t1_109* L_54 = ((&V_1)->___X_6);
			IL2CPP_RUNTIME_CLASS_INIT(Convert_t1_1522_il2cpp_TypeInfo_var);
			String_t* L_55 = Convert_ToBase64String_m1_13445(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
			NullCheck(L_53);
			StringBuilder_Append_m1_12438(L_53, L_55, /*hidden argument*/NULL);
			StringBuilder_t1_247 * L_56 = V_0;
			NullCheck(L_56);
			StringBuilder_Append_m1_12438(L_56, _stringLiteral2720, /*hidden argument*/NULL);
			goto IL_01e9;
		}

IL_01d8:
		{
			bool L_57 = ___includePrivateParameters;
			if (!L_57)
			{
				goto IL_01e9;
			}
		}

IL_01de:
		{
			ArgumentNullException_t1_1500 * L_58 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
			ArgumentNullException__ctor_m1_13269(L_58, _stringLiteral2704, /*hidden argument*/NULL);
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_58);
		}

IL_01e9:
		{
			StringBuilder_t1_247 * L_59 = V_0;
			NullCheck(L_59);
			StringBuilder_Append_m1_12438(L_59, _stringLiteral2721, /*hidden argument*/NULL);
			goto IL_0209;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1_33 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Object_t_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_01fa;
		throw e;
	}

CATCH_01fa:
	{ // begin catch(System.Object)
		{
			DSAParameters_t1_1204  L_60 = V_1;
			DSA_ZeroizePrivateKey_m1_10284(__this, L_60, /*hidden argument*/NULL);
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)__exception_local);
		}

IL_0204:
		{
			goto IL_0209;
		}
	} // end catch (depth: 1)

IL_0209:
	{
		StringBuilder_t1_247 * L_61 = V_0;
		NullCheck(L_61);
		String_t* L_62 = StringBuilder_ToString_m1_12428(L_61, /*hidden argument*/NULL);
		return L_62;
	}
}
// System.Void System.Security.Cryptography.DSACryptoServiceProvider::.ctor()
extern "C" void DSACryptoServiceProvider__ctor_m1_10287 (DSACryptoServiceProvider_t1_1203 * __this, const MethodInfo* method)
{
	{
		DSACryptoServiceProvider__ctor_m1_10290(__this, ((int32_t)1024), (CspParameters_t1_164 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.Cryptography.DSACryptoServiceProvider::.ctor(System.Security.Cryptography.CspParameters)
extern "C" void DSACryptoServiceProvider__ctor_m1_10288 (DSACryptoServiceProvider_t1_1203 * __this, CspParameters_t1_164 * ___parameters, const MethodInfo* method)
{
	{
		CspParameters_t1_164 * L_0 = ___parameters;
		DSACryptoServiceProvider__ctor_m1_10290(__this, ((int32_t)1024), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.Cryptography.DSACryptoServiceProvider::.ctor(System.Int32)
extern "C" void DSACryptoServiceProvider__ctor_m1_10289 (DSACryptoServiceProvider_t1_1203 * __this, int32_t ___dwKeySize, const MethodInfo* method)
{
	{
		int32_t L_0 = ___dwKeySize;
		DSACryptoServiceProvider__ctor_m1_10290(__this, L_0, (CspParameters_t1_164 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.Cryptography.DSACryptoServiceProvider::.ctor(System.Int32,System.Security.Cryptography.CspParameters)
extern TypeInfo* KeySizesU5BU5D_t1_1182_il2cpp_TypeInfo_var;
extern TypeInfo* KeySizes_t1_1219_il2cpp_TypeInfo_var;
extern TypeInfo* DSAManaged_t1_159_il2cpp_TypeInfo_var;
extern TypeInfo* KeyGeneratedEventHandler_t1_157_il2cpp_TypeInfo_var;
extern TypeInfo* CspParameters_t1_164_il2cpp_TypeInfo_var;
extern TypeInfo* DSACryptoServiceProvider_t1_1203_il2cpp_TypeInfo_var;
extern TypeInfo* KeyPairPersistence_t1_163_il2cpp_TypeInfo_var;
extern const MethodInfo* DSACryptoServiceProvider_OnKeyGenerated_m1_10312_MethodInfo_var;
extern "C" void DSACryptoServiceProvider__ctor_m1_10290 (DSACryptoServiceProvider_t1_1203 * __this, int32_t ___dwKeySize, CspParameters_t1_164 * ___parameters, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		KeySizesU5BU5D_t1_1182_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(203);
		KeySizes_t1_1219_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(204);
		DSAManaged_t1_159_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(877);
		KeyGeneratedEventHandler_t1_157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(205);
		CspParameters_t1_164_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(209);
		DSACryptoServiceProvider_t1_1203_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(252);
		KeyPairPersistence_t1_163_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(207);
		DSACryptoServiceProvider_OnKeyGenerated_m1_10312_MethodInfo_var = il2cpp_codegen_method_info_from_index(50);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___privateKeyExportable_6 = 1;
		DSA__ctor_m1_10281(__this, /*hidden argument*/NULL);
		((AsymmetricAlgorithm_t1_228 *)__this)->___LegalKeySizesValue_1 = ((KeySizesU5BU5D_t1_1182*)SZArrayNew(KeySizesU5BU5D_t1_1182_il2cpp_TypeInfo_var, 1));
		KeySizesU5BU5D_t1_1182* L_0 = (((AsymmetricAlgorithm_t1_228 *)__this)->___LegalKeySizesValue_1);
		KeySizes_t1_1219 * L_1 = (KeySizes_t1_1219 *)il2cpp_codegen_object_new (KeySizes_t1_1219_il2cpp_TypeInfo_var);
		KeySizes__ctor_m1_10393(L_1, ((int32_t)512), ((int32_t)1024), ((int32_t)64), /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((KeySizes_t1_1219 **)(KeySizes_t1_1219 **)SZArrayLdElema(L_0, 0, sizeof(KeySizes_t1_1219 *))) = (KeySizes_t1_1219 *)L_1;
		int32_t L_2 = ___dwKeySize;
		VirtActionInvoker1< int32_t >::Invoke(7 /* System.Void System.Security.Cryptography.AsymmetricAlgorithm::set_KeySize(System.Int32) */, __this, L_2);
		int32_t L_3 = ___dwKeySize;
		DSAManaged_t1_159 * L_4 = (DSAManaged_t1_159 *)il2cpp_codegen_object_new (DSAManaged_t1_159_il2cpp_TypeInfo_var);
		DSAManaged__ctor_m1_1929(L_4, L_3, /*hidden argument*/NULL);
		__this->___dsa_8 = L_4;
		DSAManaged_t1_159 * L_5 = (__this->___dsa_8);
		IntPtr_t L_6 = { (void*)DSACryptoServiceProvider_OnKeyGenerated_m1_10312_MethodInfo_var };
		KeyGeneratedEventHandler_t1_157 * L_7 = (KeyGeneratedEventHandler_t1_157 *)il2cpp_codegen_object_new (KeyGeneratedEventHandler_t1_157_il2cpp_TypeInfo_var);
		KeyGeneratedEventHandler__ctor_m1_1924(L_7, __this, L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		DSAManaged_add_KeyGenerated_m1_1930(L_5, L_7, /*hidden argument*/NULL);
		CspParameters_t1_164 * L_8 = ___parameters;
		__this->___persistKey_4 = ((((int32_t)((((Object_t*)(CspParameters_t1_164 *)L_8) == ((Object_t*)(Object_t *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		CspParameters_t1_164 * L_9 = ___parameters;
		if (L_9)
		{
			goto IL_00a1;
		}
	}
	{
		CspParameters_t1_164 * L_10 = (CspParameters_t1_164 *)il2cpp_codegen_object_new (CspParameters_t1_164_il2cpp_TypeInfo_var);
		CspParameters__ctor_m1_10246(L_10, ((int32_t)13), /*hidden argument*/NULL);
		___parameters = L_10;
		IL2CPP_RUNTIME_CLASS_INIT(DSACryptoServiceProvider_t1_1203_il2cpp_TypeInfo_var);
		bool L_11 = ((DSACryptoServiceProvider_t1_1203_StaticFields*)DSACryptoServiceProvider_t1_1203_il2cpp_TypeInfo_var->static_fields)->___useMachineKeyStore_9;
		if (!L_11)
		{
			goto IL_0090;
		}
	}
	{
		CspParameters_t1_164 * L_12 = ___parameters;
		CspParameters_t1_164 * L_13 = L_12;
		NullCheck(L_13);
		int32_t L_14 = CspParameters_get_Flags_m1_10251(L_13, /*hidden argument*/NULL);
		NullCheck(L_13);
		CspParameters_set_Flags_m1_10252(L_13, ((int32_t)((int32_t)L_14|(int32_t)1)), /*hidden argument*/NULL);
	}

IL_0090:
	{
		CspParameters_t1_164 * L_15 = ___parameters;
		KeyPairPersistence_t1_163 * L_16 = (KeyPairPersistence_t1_163 *)il2cpp_codegen_object_new (KeyPairPersistence_t1_163_il2cpp_TypeInfo_var);
		KeyPairPersistence__ctor_m1_1961(L_16, L_15, /*hidden argument*/NULL);
		__this->___store_3 = L_16;
		goto IL_00e1;
	}

IL_00a1:
	{
		CspParameters_t1_164 * L_17 = ___parameters;
		KeyPairPersistence_t1_163 * L_18 = (KeyPairPersistence_t1_163 *)il2cpp_codegen_object_new (KeyPairPersistence_t1_163_il2cpp_TypeInfo_var);
		KeyPairPersistence__ctor_m1_1961(L_18, L_17, /*hidden argument*/NULL);
		__this->___store_3 = L_18;
		KeyPairPersistence_t1_163 * L_19 = (__this->___store_3);
		NullCheck(L_19);
		KeyPairPersistence_Load_m1_1968(L_19, /*hidden argument*/NULL);
		KeyPairPersistence_t1_163 * L_20 = (__this->___store_3);
		NullCheck(L_20);
		String_t* L_21 = KeyPairPersistence_get_KeyValue_m1_1965(L_20, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_00e1;
		}
	}
	{
		__this->___persisted_5 = 1;
		KeyPairPersistence_t1_163 * L_22 = (__this->___store_3);
		NullCheck(L_22);
		String_t* L_23 = KeyPairPersistence_get_KeyValue_m1_1965(L_22, /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(11 /* System.Void System.Security.Cryptography.DSA::FromXmlString(System.String) */, __this, L_23);
	}

IL_00e1:
	{
		return;
	}
}
// System.Void System.Security.Cryptography.DSACryptoServiceProvider::.cctor()
extern "C" void DSACryptoServiceProvider__cctor_m1_10291 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void System.Security.Cryptography.DSACryptoServiceProvider::Finalize()
extern "C" void DSACryptoServiceProvider_Finalize_m1_10292 (DSACryptoServiceProvider_t1_1203 * __this, const MethodInfo* method)
{
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DSACryptoServiceProvider_Dispose_m1_10311(__this, 0, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m1_3(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0013:
	{
		return;
	}
}
// System.String System.Security.Cryptography.DSACryptoServiceProvider::get_KeyExchangeAlgorithm()
extern "C" String_t* DSACryptoServiceProvider_get_KeyExchangeAlgorithm_m1_10293 (DSACryptoServiceProvider_t1_1203 * __this, const MethodInfo* method)
{
	{
		return (String_t*)NULL;
	}
}
// System.Int32 System.Security.Cryptography.DSACryptoServiceProvider::get_KeySize()
extern "C" int32_t DSACryptoServiceProvider_get_KeySize_m1_10294 (DSACryptoServiceProvider_t1_1203 * __this, const MethodInfo* method)
{
	{
		DSAManaged_t1_159 * L_0 = (__this->___dsa_8);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 Mono.Security.Cryptography.DSAManaged::get_KeySize() */, L_0);
		return L_1;
	}
}
// System.Boolean System.Security.Cryptography.DSACryptoServiceProvider::get_PersistKeyInCsp()
extern "C" bool DSACryptoServiceProvider_get_PersistKeyInCsp_m1_10295 (DSACryptoServiceProvider_t1_1203 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___persistKey_4);
		return L_0;
	}
}
// System.Void System.Security.Cryptography.DSACryptoServiceProvider::set_PersistKeyInCsp(System.Boolean)
extern "C" void DSACryptoServiceProvider_set_PersistKeyInCsp_m1_10296 (DSACryptoServiceProvider_t1_1203 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___persistKey_4 = L_0;
		return;
	}
}
// System.Boolean System.Security.Cryptography.DSACryptoServiceProvider::get_PublicOnly()
extern "C" bool DSACryptoServiceProvider_get_PublicOnly_m1_10297 (DSACryptoServiceProvider_t1_1203 * __this, const MethodInfo* method)
{
	{
		DSAManaged_t1_159 * L_0 = (__this->___dsa_8);
		NullCheck(L_0);
		bool L_1 = DSAManaged_get_PublicOnly_m1_1940(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String System.Security.Cryptography.DSACryptoServiceProvider::get_SignatureAlgorithm()
extern Il2CppCodeGenString* _stringLiteral393;
extern "C" String_t* DSACryptoServiceProvider_get_SignatureAlgorithm_m1_10298 (DSACryptoServiceProvider_t1_1203 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral393 = il2cpp_codegen_string_literal_from_index(393);
		s_Il2CppMethodIntialized = true;
	}
	{
		return _stringLiteral393;
	}
}
// System.Boolean System.Security.Cryptography.DSACryptoServiceProvider::get_UseMachineKeyStore()
extern TypeInfo* DSACryptoServiceProvider_t1_1203_il2cpp_TypeInfo_var;
extern "C" bool DSACryptoServiceProvider_get_UseMachineKeyStore_m1_10299 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DSACryptoServiceProvider_t1_1203_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(252);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DSACryptoServiceProvider_t1_1203_il2cpp_TypeInfo_var);
		bool L_0 = ((DSACryptoServiceProvider_t1_1203_StaticFields*)DSACryptoServiceProvider_t1_1203_il2cpp_TypeInfo_var->static_fields)->___useMachineKeyStore_9;
		return L_0;
	}
}
// System.Void System.Security.Cryptography.DSACryptoServiceProvider::set_UseMachineKeyStore(System.Boolean)
extern TypeInfo* DSACryptoServiceProvider_t1_1203_il2cpp_TypeInfo_var;
extern "C" void DSACryptoServiceProvider_set_UseMachineKeyStore_m1_10300 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DSACryptoServiceProvider_t1_1203_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(252);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(DSACryptoServiceProvider_t1_1203_il2cpp_TypeInfo_var);
		((DSACryptoServiceProvider_t1_1203_StaticFields*)DSACryptoServiceProvider_t1_1203_il2cpp_TypeInfo_var->static_fields)->___useMachineKeyStore_9 = L_0;
		return;
	}
}
// System.Security.Cryptography.DSAParameters System.Security.Cryptography.DSACryptoServiceProvider::ExportParameters(System.Boolean)
extern TypeInfo* CryptographicException_t1_1194_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2722;
extern "C" DSAParameters_t1_1204  DSACryptoServiceProvider_ExportParameters_m1_10301 (DSACryptoServiceProvider_t1_1203 * __this, bool ___includePrivateParameters, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CryptographicException_t1_1194_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(197);
		_stringLiteral2722 = il2cpp_codegen_string_literal_from_index(2722);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___includePrivateParameters;
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		bool L_1 = (__this->___privateKeyExportable_6);
		if (L_1)
		{
			goto IL_0021;
		}
	}
	{
		String_t* L_2 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2722, /*hidden argument*/NULL);
		CryptographicException_t1_1194 * L_3 = (CryptographicException_t1_1194 *)il2cpp_codegen_object_new (CryptographicException_t1_1194_il2cpp_TypeInfo_var);
		CryptographicException__ctor_m1_10222(L_3, L_2, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0021:
	{
		DSAManaged_t1_159 * L_4 = (__this->___dsa_8);
		bool L_5 = ___includePrivateParameters;
		NullCheck(L_4);
		DSAParameters_t1_1204  L_6 = (DSAParameters_t1_1204 )VirtFuncInvoker1< DSAParameters_t1_1204 , bool >::Invoke(14 /* System.Security.Cryptography.DSAParameters Mono.Security.Cryptography.DSAManaged::ExportParameters(System.Boolean) */, L_4, L_5);
		return L_6;
	}
}
// System.Void System.Security.Cryptography.DSACryptoServiceProvider::ImportParameters(System.Security.Cryptography.DSAParameters)
extern "C" void DSACryptoServiceProvider_ImportParameters_m1_10302 (DSACryptoServiceProvider_t1_1203 * __this, DSAParameters_t1_1204  ___parameters, const MethodInfo* method)
{
	{
		DSAManaged_t1_159 * L_0 = (__this->___dsa_8);
		DSAParameters_t1_1204  L_1 = ___parameters;
		NullCheck(L_0);
		VirtActionInvoker1< DSAParameters_t1_1204  >::Invoke(15 /* System.Void Mono.Security.Cryptography.DSAManaged::ImportParameters(System.Security.Cryptography.DSAParameters) */, L_0, L_1);
		return;
	}
}
// System.Byte[] System.Security.Cryptography.DSACryptoServiceProvider::CreateSignature(System.Byte[])
extern "C" ByteU5BU5D_t1_109* DSACryptoServiceProvider_CreateSignature_m1_10303 (DSACryptoServiceProvider_t1_1203 * __this, ByteU5BU5D_t1_109* ___rgbHash, const MethodInfo* method)
{
	{
		DSAManaged_t1_159 * L_0 = (__this->___dsa_8);
		ByteU5BU5D_t1_109* L_1 = ___rgbHash;
		NullCheck(L_0);
		ByteU5BU5D_t1_109* L_2 = (ByteU5BU5D_t1_109*)VirtFuncInvoker1< ByteU5BU5D_t1_109*, ByteU5BU5D_t1_109* >::Invoke(13 /* System.Byte[] Mono.Security.Cryptography.DSAManaged::CreateSignature(System.Byte[]) */, L_0, L_1);
		return L_2;
	}
}
// System.Byte[] System.Security.Cryptography.DSACryptoServiceProvider::SignData(System.Byte[])
extern "C" ByteU5BU5D_t1_109* DSACryptoServiceProvider_SignData_m1_10304 (DSACryptoServiceProvider_t1_1203 * __this, ByteU5BU5D_t1_109* ___buffer, const MethodInfo* method)
{
	HashAlgorithm_t1_162 * V_0 = {0};
	ByteU5BU5D_t1_109* V_1 = {0};
	{
		SHA1_t1_1248 * L_0 = SHA1_Create_m1_10641(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		HashAlgorithm_t1_162 * L_1 = V_0;
		ByteU5BU5D_t1_109* L_2 = ___buffer;
		NullCheck(L_1);
		ByteU5BU5D_t1_109* L_3 = HashAlgorithm_ComputeHash_m1_10381(L_1, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		DSAManaged_t1_159 * L_4 = (__this->___dsa_8);
		ByteU5BU5D_t1_109* L_5 = V_1;
		NullCheck(L_4);
		ByteU5BU5D_t1_109* L_6 = (ByteU5BU5D_t1_109*)VirtFuncInvoker1< ByteU5BU5D_t1_109*, ByteU5BU5D_t1_109* >::Invoke(13 /* System.Byte[] Mono.Security.Cryptography.DSAManaged::CreateSignature(System.Byte[]) */, L_4, L_5);
		return L_6;
	}
}
// System.Byte[] System.Security.Cryptography.DSACryptoServiceProvider::SignData(System.Byte[],System.Int32,System.Int32)
extern "C" ByteU5BU5D_t1_109* DSACryptoServiceProvider_SignData_m1_10305 (DSACryptoServiceProvider_t1_1203 * __this, ByteU5BU5D_t1_109* ___buffer, int32_t ___offset, int32_t ___count, const MethodInfo* method)
{
	HashAlgorithm_t1_162 * V_0 = {0};
	ByteU5BU5D_t1_109* V_1 = {0};
	{
		SHA1_t1_1248 * L_0 = SHA1_Create_m1_10641(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		HashAlgorithm_t1_162 * L_1 = V_0;
		ByteU5BU5D_t1_109* L_2 = ___buffer;
		int32_t L_3 = ___offset;
		int32_t L_4 = ___count;
		NullCheck(L_1);
		ByteU5BU5D_t1_109* L_5 = HashAlgorithm_ComputeHash_m1_10382(L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		DSAManaged_t1_159 * L_6 = (__this->___dsa_8);
		ByteU5BU5D_t1_109* L_7 = V_1;
		NullCheck(L_6);
		ByteU5BU5D_t1_109* L_8 = (ByteU5BU5D_t1_109*)VirtFuncInvoker1< ByteU5BU5D_t1_109*, ByteU5BU5D_t1_109* >::Invoke(13 /* System.Byte[] Mono.Security.Cryptography.DSAManaged::CreateSignature(System.Byte[]) */, L_6, L_7);
		return L_8;
	}
}
// System.Byte[] System.Security.Cryptography.DSACryptoServiceProvider::SignData(System.IO.Stream)
extern "C" ByteU5BU5D_t1_109* DSACryptoServiceProvider_SignData_m1_10306 (DSACryptoServiceProvider_t1_1203 * __this, Stream_t1_405 * ___inputStream, const MethodInfo* method)
{
	HashAlgorithm_t1_162 * V_0 = {0};
	ByteU5BU5D_t1_109* V_1 = {0};
	{
		SHA1_t1_1248 * L_0 = SHA1_Create_m1_10641(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		HashAlgorithm_t1_162 * L_1 = V_0;
		Stream_t1_405 * L_2 = ___inputStream;
		NullCheck(L_1);
		ByteU5BU5D_t1_109* L_3 = HashAlgorithm_ComputeHash_m1_10383(L_1, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		DSAManaged_t1_159 * L_4 = (__this->___dsa_8);
		ByteU5BU5D_t1_109* L_5 = V_1;
		NullCheck(L_4);
		ByteU5BU5D_t1_109* L_6 = (ByteU5BU5D_t1_109*)VirtFuncInvoker1< ByteU5BU5D_t1_109*, ByteU5BU5D_t1_109* >::Invoke(13 /* System.Byte[] Mono.Security.Cryptography.DSAManaged::CreateSignature(System.Byte[]) */, L_4, L_5);
		return L_6;
	}
}
// System.Byte[] System.Security.Cryptography.DSACryptoServiceProvider::SignHash(System.Byte[],System.String)
extern TypeInfo* CultureInfo_t1_277_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* CryptographicException_t1_1194_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral375;
extern Il2CppCodeGenString* _stringLiteral2723;
extern "C" ByteU5BU5D_t1_109* DSACryptoServiceProvider_SignHash_m1_10307 (DSACryptoServiceProvider_t1_1203 * __this, ByteU5BU5D_t1_109* ___rgbHash, String_t* ___str, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CultureInfo_t1_277_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(31);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		CryptographicException_t1_1194_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(197);
		_stringLiteral375 = il2cpp_codegen_string_literal_from_index(375);
		_stringLiteral2723 = il2cpp_codegen_string_literal_from_index(2723);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___str;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t1_277_il2cpp_TypeInfo_var);
		CultureInfo_t1_277 * L_1 = CultureInfo_get_InvariantCulture_m1_3911(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		int32_t L_2 = String_Compare_m1_465(NULL /*static, unused*/, L_0, _stringLiteral375, 1, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0026;
		}
	}
	{
		String_t* L_3 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2723, /*hidden argument*/NULL);
		CryptographicException_t1_1194 * L_4 = (CryptographicException_t1_1194 *)il2cpp_codegen_object_new (CryptographicException_t1_1194_il2cpp_TypeInfo_var);
		CryptographicException__ctor_m1_10222(L_4, L_3, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
	}

IL_0026:
	{
		DSAManaged_t1_159 * L_5 = (__this->___dsa_8);
		ByteU5BU5D_t1_109* L_6 = ___rgbHash;
		NullCheck(L_5);
		ByteU5BU5D_t1_109* L_7 = (ByteU5BU5D_t1_109*)VirtFuncInvoker1< ByteU5BU5D_t1_109*, ByteU5BU5D_t1_109* >::Invoke(13 /* System.Byte[] Mono.Security.Cryptography.DSAManaged::CreateSignature(System.Byte[]) */, L_5, L_6);
		return L_7;
	}
}
// System.Boolean System.Security.Cryptography.DSACryptoServiceProvider::VerifyData(System.Byte[],System.Byte[])
extern "C" bool DSACryptoServiceProvider_VerifyData_m1_10308 (DSACryptoServiceProvider_t1_1203 * __this, ByteU5BU5D_t1_109* ___rgbData, ByteU5BU5D_t1_109* ___rgbSignature, const MethodInfo* method)
{
	HashAlgorithm_t1_162 * V_0 = {0};
	ByteU5BU5D_t1_109* V_1 = {0};
	{
		SHA1_t1_1248 * L_0 = SHA1_Create_m1_10641(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		HashAlgorithm_t1_162 * L_1 = V_0;
		ByteU5BU5D_t1_109* L_2 = ___rgbData;
		NullCheck(L_1);
		ByteU5BU5D_t1_109* L_3 = HashAlgorithm_ComputeHash_m1_10381(L_1, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		DSAManaged_t1_159 * L_4 = (__this->___dsa_8);
		ByteU5BU5D_t1_109* L_5 = V_1;
		ByteU5BU5D_t1_109* L_6 = ___rgbSignature;
		NullCheck(L_4);
		bool L_7 = (bool)VirtFuncInvoker2< bool, ByteU5BU5D_t1_109*, ByteU5BU5D_t1_109* >::Invoke(16 /* System.Boolean Mono.Security.Cryptography.DSAManaged::VerifySignature(System.Byte[],System.Byte[]) */, L_4, L_5, L_6);
		return L_7;
	}
}
// System.Boolean System.Security.Cryptography.DSACryptoServiceProvider::VerifyHash(System.Byte[],System.String,System.Byte[])
extern TypeInfo* CultureInfo_t1_277_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* CryptographicException_t1_1194_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral375;
extern Il2CppCodeGenString* _stringLiteral2723;
extern "C" bool DSACryptoServiceProvider_VerifyHash_m1_10309 (DSACryptoServiceProvider_t1_1203 * __this, ByteU5BU5D_t1_109* ___rgbHash, String_t* ___str, ByteU5BU5D_t1_109* ___rgbSignature, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CultureInfo_t1_277_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(31);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		CryptographicException_t1_1194_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(197);
		_stringLiteral375 = il2cpp_codegen_string_literal_from_index(375);
		_stringLiteral2723 = il2cpp_codegen_string_literal_from_index(2723);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___str;
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		___str = _stringLiteral375;
	}

IL_000d:
	{
		String_t* L_1 = ___str;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t1_277_il2cpp_TypeInfo_var);
		CultureInfo_t1_277 * L_2 = CultureInfo_get_InvariantCulture_m1_3911(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		int32_t L_3 = String_Compare_m1_465(NULL /*static, unused*/, L_1, _stringLiteral375, 1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0033;
		}
	}
	{
		String_t* L_4 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2723, /*hidden argument*/NULL);
		CryptographicException_t1_1194 * L_5 = (CryptographicException_t1_1194 *)il2cpp_codegen_object_new (CryptographicException_t1_1194_il2cpp_TypeInfo_var);
		CryptographicException__ctor_m1_10222(L_5, L_4, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_5);
	}

IL_0033:
	{
		DSAManaged_t1_159 * L_6 = (__this->___dsa_8);
		ByteU5BU5D_t1_109* L_7 = ___rgbHash;
		ByteU5BU5D_t1_109* L_8 = ___rgbSignature;
		NullCheck(L_6);
		bool L_9 = (bool)VirtFuncInvoker2< bool, ByteU5BU5D_t1_109*, ByteU5BU5D_t1_109* >::Invoke(16 /* System.Boolean Mono.Security.Cryptography.DSAManaged::VerifySignature(System.Byte[],System.Byte[]) */, L_6, L_7, L_8);
		return L_9;
	}
}
// System.Boolean System.Security.Cryptography.DSACryptoServiceProvider::VerifySignature(System.Byte[],System.Byte[])
extern "C" bool DSACryptoServiceProvider_VerifySignature_m1_10310 (DSACryptoServiceProvider_t1_1203 * __this, ByteU5BU5D_t1_109* ___rgbHash, ByteU5BU5D_t1_109* ___rgbSignature, const MethodInfo* method)
{
	{
		DSAManaged_t1_159 * L_0 = (__this->___dsa_8);
		ByteU5BU5D_t1_109* L_1 = ___rgbHash;
		ByteU5BU5D_t1_109* L_2 = ___rgbSignature;
		NullCheck(L_0);
		bool L_3 = (bool)VirtFuncInvoker2< bool, ByteU5BU5D_t1_109*, ByteU5BU5D_t1_109* >::Invoke(16 /* System.Boolean Mono.Security.Cryptography.DSAManaged::VerifySignature(System.Byte[],System.Byte[]) */, L_0, L_1, L_2);
		return L_3;
	}
}
// System.Void System.Security.Cryptography.DSACryptoServiceProvider::Dispose(System.Boolean)
extern "C" void DSACryptoServiceProvider_Dispose_m1_10311 (DSACryptoServiceProvider_t1_1203 * __this, bool ___disposing, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_disposed_7);
		if (L_0)
		{
			goto IL_0049;
		}
	}
	{
		bool L_1 = (__this->___persisted_5);
		if (!L_1)
		{
			goto IL_002c;
		}
	}
	{
		bool L_2 = (__this->___persistKey_4);
		if (L_2)
		{
			goto IL_002c;
		}
	}
	{
		KeyPairPersistence_t1_163 * L_3 = (__this->___store_3);
		NullCheck(L_3);
		KeyPairPersistence_Remove_m1_1970(L_3, /*hidden argument*/NULL);
	}

IL_002c:
	{
		DSAManaged_t1_159 * L_4 = (__this->___dsa_8);
		if (!L_4)
		{
			goto IL_0042;
		}
	}
	{
		DSAManaged_t1_159 * L_5 = (__this->___dsa_8);
		NullCheck(L_5);
		AsymmetricAlgorithm_Clear_m1_10162(L_5, /*hidden argument*/NULL);
	}

IL_0042:
	{
		__this->___m_disposed_7 = 1;
	}

IL_0049:
	{
		return;
	}
}
// System.Void System.Security.Cryptography.DSACryptoServiceProvider::OnKeyGenerated(System.Object,System.EventArgs)
extern "C" void DSACryptoServiceProvider_OnKeyGenerated_m1_10312 (DSACryptoServiceProvider_t1_1203 * __this, Object_t * ___sender, EventArgs_t1_158 * ___e, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___persistKey_4);
		if (!L_0)
		{
			goto IL_0047;
		}
	}
	{
		bool L_1 = (__this->___persisted_5);
		if (L_1)
		{
			goto IL_0047;
		}
	}
	{
		KeyPairPersistence_t1_163 * L_2 = (__this->___store_3);
		DSAManaged_t1_159 * L_3 = (__this->___dsa_8);
		NullCheck(L_3);
		bool L_4 = DSAManaged_get_PublicOnly_m1_1940(L_3, /*hidden argument*/NULL);
		String_t* L_5 = (String_t*)VirtFuncInvoker1< String_t*, bool >::Invoke(12 /* System.String System.Security.Cryptography.DSA::ToXmlString(System.Boolean) */, __this, ((((int32_t)L_4) == ((int32_t)0))? 1 : 0));
		NullCheck(L_2);
		KeyPairPersistence_set_KeyValue_m1_1966(L_2, L_5, /*hidden argument*/NULL);
		KeyPairPersistence_t1_163 * L_6 = (__this->___store_3);
		NullCheck(L_6);
		KeyPairPersistence_Save_m1_1969(L_6, /*hidden argument*/NULL);
		__this->___persisted_5 = 1;
	}

IL_0047:
	{
		return;
	}
}
// System.Security.Cryptography.CspKeyContainerInfo System.Security.Cryptography.DSACryptoServiceProvider::get_CspKeyContainerInfo()
extern "C" CspKeyContainerInfo_t1_1196 * DSACryptoServiceProvider_get_CspKeyContainerInfo_m1_10313 (DSACryptoServiceProvider_t1_1203 * __this, const MethodInfo* method)
{
	{
		return (CspKeyContainerInfo_t1_1196 *)NULL;
	}
}
// System.Byte[] System.Security.Cryptography.DSACryptoServiceProvider::ExportCspBlob(System.Boolean)
extern "C" ByteU5BU5D_t1_109* DSACryptoServiceProvider_ExportCspBlob_m1_10314 (DSACryptoServiceProvider_t1_1203 * __this, bool ___includePrivateParameters, const MethodInfo* method)
{
	ByteU5BU5D_t1_109* V_0 = {0};
	{
		V_0 = (ByteU5BU5D_t1_109*)NULL;
		bool L_0 = ___includePrivateParameters;
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		ByteU5BU5D_t1_109* L_1 = CryptoConvert_ToCapiPrivateKeyBlob_m1_1896(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_001b;
	}

IL_0014:
	{
		ByteU5BU5D_t1_109* L_2 = CryptoConvert_ToCapiPublicKeyBlob_m1_1902(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		V_0 = L_2;
	}

IL_001b:
	{
		ByteU5BU5D_t1_109* L_3 = V_0;
		return L_3;
	}
}
// System.Void System.Security.Cryptography.DSACryptoServiceProvider::ImportCspBlob(System.Byte[])
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* DSACryptoServiceProvider_t1_1203_il2cpp_TypeInfo_var;
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2724;
extern "C" void DSACryptoServiceProvider_ImportCspBlob_m1_10315 (DSACryptoServiceProvider_t1_1203 * __this, ByteU5BU5D_t1_109* ___keyBlob, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		DSACryptoServiceProvider_t1_1203_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(252);
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		_stringLiteral2724 = il2cpp_codegen_string_literal_from_index(2724);
		s_Il2CppMethodIntialized = true;
	}
	DSA_t1_160 * V_0 = {0};
	DSAParameters_t1_1204  V_1 = {0};
	DSAParameters_t1_1204  V_2 = {0};
	DSAParameters_t1_1204  V_3 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		ByteU5BU5D_t1_109* L_0 = ___keyBlob;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, _stringLiteral2724, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		ByteU5BU5D_t1_109* L_2 = ___keyBlob;
		DSA_t1_160 * L_3 = CryptoConvert_FromCapiKeyBlobDSA_m1_1905(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		DSA_t1_160 * L_4 = V_0;
		if (!((DSACryptoServiceProvider_t1_1203 *)IsInstSealed(L_4, DSACryptoServiceProvider_t1_1203_il2cpp_TypeInfo_var)))
		{
			goto IL_0044;
		}
	}
	{
		DSA_t1_160 * L_5 = V_0;
		DSA_t1_160 * L_6 = V_0;
		NullCheck(((DSACryptoServiceProvider_t1_1203 *)IsInstSealed(L_6, DSACryptoServiceProvider_t1_1203_il2cpp_TypeInfo_var)));
		bool L_7 = DSACryptoServiceProvider_get_PublicOnly_m1_10297(((DSACryptoServiceProvider_t1_1203 *)IsInstSealed(L_6, DSACryptoServiceProvider_t1_1203_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		NullCheck(L_5);
		DSAParameters_t1_1204  L_8 = (DSAParameters_t1_1204 )VirtFuncInvoker1< DSAParameters_t1_1204 , bool >::Invoke(14 /* System.Security.Cryptography.DSAParameters System.Security.Cryptography.DSA::ExportParameters(System.Boolean) */, L_5, ((((int32_t)L_7) == ((int32_t)0))? 1 : 0));
		V_1 = L_8;
		DSAParameters_t1_1204  L_9 = V_1;
		DSACryptoServiceProvider_ImportParameters_m1_10302(__this, L_9, /*hidden argument*/NULL);
		goto IL_006d;
	}

IL_0044:
	try
	{ // begin try (depth: 1)
		DSA_t1_160 * L_10 = V_0;
		NullCheck(L_10);
		DSAParameters_t1_1204  L_11 = (DSAParameters_t1_1204 )VirtFuncInvoker1< DSAParameters_t1_1204 , bool >::Invoke(14 /* System.Security.Cryptography.DSAParameters System.Security.Cryptography.DSA::ExportParameters(System.Boolean) */, L_10, 1);
		V_2 = L_11;
		DSAParameters_t1_1204  L_12 = V_2;
		DSACryptoServiceProvider_ImportParameters_m1_10302(__this, L_12, /*hidden argument*/NULL);
		goto IL_006d;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1_33 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Object_t_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0058;
		throw e;
	}

CATCH_0058:
	{ // begin catch(System.Object)
		DSA_t1_160 * L_13 = V_0;
		NullCheck(L_13);
		DSAParameters_t1_1204  L_14 = (DSAParameters_t1_1204 )VirtFuncInvoker1< DSAParameters_t1_1204 , bool >::Invoke(14 /* System.Security.Cryptography.DSAParameters System.Security.Cryptography.DSA::ExportParameters(System.Boolean) */, L_13, 0);
		V_3 = L_14;
		DSAParameters_t1_1204  L_15 = V_3;
		DSACryptoServiceProvider_ImportParameters_m1_10302(__this, L_15, /*hidden argument*/NULL);
		goto IL_006d;
	} // end catch (depth: 1)

IL_006d:
	{
		return;
	}
}
// Conversion methods for marshalling of: System.Security.Cryptography.DSAParameters
extern "C" void DSAParameters_t1_1204_marshal(const DSAParameters_t1_1204& unmarshaled, DSAParameters_t1_1204_marshaled& marshaled)
{
	marshaled.___Counter_0 = unmarshaled.___Counter_0;
	marshaled.___G_1 = il2cpp_codegen_marshal_array<uint8_t>((Il2CppCodeGenArray*)unmarshaled.___G_1);
	marshaled.___J_2 = il2cpp_codegen_marshal_array<uint8_t>((Il2CppCodeGenArray*)unmarshaled.___J_2);
	marshaled.___P_3 = il2cpp_codegen_marshal_array<uint8_t>((Il2CppCodeGenArray*)unmarshaled.___P_3);
	marshaled.___Q_4 = il2cpp_codegen_marshal_array<uint8_t>((Il2CppCodeGenArray*)unmarshaled.___Q_4);
	marshaled.___Seed_5 = il2cpp_codegen_marshal_array<uint8_t>((Il2CppCodeGenArray*)unmarshaled.___Seed_5);
	marshaled.___X_6 = il2cpp_codegen_marshal_array<uint8_t>((Il2CppCodeGenArray*)unmarshaled.___X_6);
	marshaled.___Y_7 = il2cpp_codegen_marshal_array<uint8_t>((Il2CppCodeGenArray*)unmarshaled.___Y_7);
}
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern "C" void DSAParameters_t1_1204_marshal_back(const DSAParameters_t1_1204_marshaled& marshaled, DSAParameters_t1_1204& unmarshaled)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		s_Il2CppMethodIntialized = true;
	}
	unmarshaled.___Counter_0 = marshaled.___Counter_0;
	unmarshaled.___G_1 = (ByteU5BU5D_t1_109*)il2cpp_codegen_marshal_array_result(Byte_t1_11_il2cpp_TypeInfo_var, marshaled.___G_1, 1);
	unmarshaled.___J_2 = (ByteU5BU5D_t1_109*)il2cpp_codegen_marshal_array_result(Byte_t1_11_il2cpp_TypeInfo_var, marshaled.___J_2, 1);
	unmarshaled.___P_3 = (ByteU5BU5D_t1_109*)il2cpp_codegen_marshal_array_result(Byte_t1_11_il2cpp_TypeInfo_var, marshaled.___P_3, 1);
	unmarshaled.___Q_4 = (ByteU5BU5D_t1_109*)il2cpp_codegen_marshal_array_result(Byte_t1_11_il2cpp_TypeInfo_var, marshaled.___Q_4, 1);
	unmarshaled.___Seed_5 = (ByteU5BU5D_t1_109*)il2cpp_codegen_marshal_array_result(Byte_t1_11_il2cpp_TypeInfo_var, marshaled.___Seed_5, 1);
	unmarshaled.___X_6 = (ByteU5BU5D_t1_109*)il2cpp_codegen_marshal_array_result(Byte_t1_11_il2cpp_TypeInfo_var, marshaled.___X_6, 1);
	unmarshaled.___Y_7 = (ByteU5BU5D_t1_109*)il2cpp_codegen_marshal_array_result(Byte_t1_11_il2cpp_TypeInfo_var, marshaled.___Y_7, 1);
}
// Conversion method for clean up from marshalling of: System.Security.Cryptography.DSAParameters
extern "C" void DSAParameters_t1_1204_marshal_cleanup(DSAParameters_t1_1204_marshaled& marshaled)
{
}
// System.Void System.Security.Cryptography.DSASignatureDeformatter::.ctor()
extern "C" void DSASignatureDeformatter__ctor_m1_10316 (DSASignatureDeformatter_t1_1205 * __this, const MethodInfo* method)
{
	{
		AsymmetricSignatureDeformatter__ctor_m1_10168(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.Cryptography.DSASignatureDeformatter::.ctor(System.Security.Cryptography.AsymmetricAlgorithm)
extern "C" void DSASignatureDeformatter__ctor_m1_10317 (DSASignatureDeformatter_t1_1205 * __this, AsymmetricAlgorithm_t1_228 * ___key, const MethodInfo* method)
{
	{
		AsymmetricSignatureDeformatter__ctor_m1_10168(__this, /*hidden argument*/NULL);
		AsymmetricAlgorithm_t1_228 * L_0 = ___key;
		VirtActionInvoker1< AsymmetricAlgorithm_t1_228 * >::Invoke(5 /* System.Void System.Security.Cryptography.DSASignatureDeformatter::SetKey(System.Security.Cryptography.AsymmetricAlgorithm) */, __this, L_0);
		return;
	}
}
// System.Void System.Security.Cryptography.DSASignatureDeformatter::SetHashAlgorithm(System.String)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidCastException_t1_1558_il2cpp_TypeInfo_var;
extern TypeInfo* CryptographicUnexpectedOperationException_t1_1195_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2725;
extern Il2CppCodeGenString* _stringLiteral2726;
extern "C" void DSASignatureDeformatter_SetHashAlgorithm_m1_10318 (DSASignatureDeformatter_t1_1205 * __this, String_t* ___strName, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		InvalidCastException_t1_1558_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(26);
		CryptographicUnexpectedOperationException_t1_1195_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(873);
		_stringLiteral2725 = il2cpp_codegen_string_literal_from_index(2725);
		_stringLiteral2726 = il2cpp_codegen_string_literal_from_index(2726);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___strName;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, _stringLiteral2725, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		String_t* L_2 = ___strName;
		SHA1_Create_m1_10642(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		goto IL_0033;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1_33 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t1_1558_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		{
			String_t* L_3 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2726, /*hidden argument*/NULL);
			CryptographicUnexpectedOperationException_t1_1195 * L_4 = (CryptographicUnexpectedOperationException_t1_1195 *)il2cpp_codegen_object_new (CryptographicUnexpectedOperationException_t1_1195_il2cpp_TypeInfo_var);
			CryptographicUnexpectedOperationException__ctor_m1_10227(L_4, L_3, /*hidden argument*/NULL);
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
		}

IL_002e:
		{
			goto IL_0033;
		}
	} // end catch (depth: 1)

IL_0033:
	{
		return;
	}
}
// System.Void System.Security.Cryptography.DSASignatureDeformatter::SetKey(System.Security.Cryptography.AsymmetricAlgorithm)
extern TypeInfo* DSA_t1_160_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral793;
extern "C" void DSASignatureDeformatter_SetKey_m1_10319 (DSASignatureDeformatter_t1_1205 * __this, AsymmetricAlgorithm_t1_228 * ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DSA_t1_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(200);
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		_stringLiteral793 = il2cpp_codegen_string_literal_from_index(793);
		s_Il2CppMethodIntialized = true;
	}
	{
		AsymmetricAlgorithm_t1_228 * L_0 = ___key;
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		AsymmetricAlgorithm_t1_228 * L_1 = ___key;
		__this->___dsa_0 = ((DSA_t1_160 *)CastclassClass(L_1, DSA_t1_160_il2cpp_TypeInfo_var));
		goto IL_0022;
	}

IL_0017:
	{
		ArgumentNullException_t1_1500 * L_2 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_2, _stringLiteral793, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0022:
	{
		return;
	}
}
// System.Boolean System.Security.Cryptography.DSASignatureDeformatter::VerifySignature(System.Byte[],System.Byte[])
extern TypeInfo* CryptographicUnexpectedOperationException_t1_1195_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2727;
extern "C" bool DSASignatureDeformatter_VerifySignature_m1_10320 (DSASignatureDeformatter_t1_1205 * __this, ByteU5BU5D_t1_109* ___rgbHash, ByteU5BU5D_t1_109* ___rgbSignature, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CryptographicUnexpectedOperationException_t1_1195_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(873);
		_stringLiteral2727 = il2cpp_codegen_string_literal_from_index(2727);
		s_Il2CppMethodIntialized = true;
	}
	{
		DSA_t1_160 * L_0 = (__this->___dsa_0);
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2727, /*hidden argument*/NULL);
		CryptographicUnexpectedOperationException_t1_1195 * L_2 = (CryptographicUnexpectedOperationException_t1_1195 *)il2cpp_codegen_object_new (CryptographicUnexpectedOperationException_t1_1195_il2cpp_TypeInfo_var);
		CryptographicUnexpectedOperationException__ctor_m1_10227(L_2, L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001b:
	{
		DSA_t1_160 * L_3 = (__this->___dsa_0);
		ByteU5BU5D_t1_109* L_4 = ___rgbHash;
		ByteU5BU5D_t1_109* L_5 = ___rgbSignature;
		NullCheck(L_3);
		bool L_6 = (bool)VirtFuncInvoker2< bool, ByteU5BU5D_t1_109*, ByteU5BU5D_t1_109* >::Invoke(16 /* System.Boolean System.Security.Cryptography.DSA::VerifySignature(System.Byte[],System.Byte[]) */, L_3, L_4, L_5);
		return L_6;
	}
}
// System.Void System.Security.Cryptography.DSASignatureFormatter::.ctor()
extern "C" void DSASignatureFormatter__ctor_m1_10321 (DSASignatureFormatter_t1_1206 * __this, const MethodInfo* method)
{
	{
		AsymmetricSignatureFormatter__ctor_m1_10170(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.Cryptography.DSASignatureFormatter::.ctor(System.Security.Cryptography.AsymmetricAlgorithm)
extern "C" void DSASignatureFormatter__ctor_m1_10322 (DSASignatureFormatter_t1_1206 * __this, AsymmetricAlgorithm_t1_228 * ___key, const MethodInfo* method)
{
	{
		AsymmetricSignatureFormatter__ctor_m1_10170(__this, /*hidden argument*/NULL);
		AsymmetricAlgorithm_t1_228 * L_0 = ___key;
		VirtActionInvoker1< AsymmetricAlgorithm_t1_228 * >::Invoke(5 /* System.Void System.Security.Cryptography.DSASignatureFormatter::SetKey(System.Security.Cryptography.AsymmetricAlgorithm) */, __this, L_0);
		return;
	}
}
// System.Byte[] System.Security.Cryptography.DSASignatureFormatter::CreateSignature(System.Byte[])
extern TypeInfo* CryptographicUnexpectedOperationException_t1_1195_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2727;
extern "C" ByteU5BU5D_t1_109* DSASignatureFormatter_CreateSignature_m1_10323 (DSASignatureFormatter_t1_1206 * __this, ByteU5BU5D_t1_109* ___rgbHash, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CryptographicUnexpectedOperationException_t1_1195_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(873);
		_stringLiteral2727 = il2cpp_codegen_string_literal_from_index(2727);
		s_Il2CppMethodIntialized = true;
	}
	{
		DSA_t1_160 * L_0 = (__this->___dsa_0);
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2727, /*hidden argument*/NULL);
		CryptographicUnexpectedOperationException_t1_1195 * L_2 = (CryptographicUnexpectedOperationException_t1_1195 *)il2cpp_codegen_object_new (CryptographicUnexpectedOperationException_t1_1195_il2cpp_TypeInfo_var);
		CryptographicUnexpectedOperationException__ctor_m1_10227(L_2, L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001b:
	{
		DSA_t1_160 * L_3 = (__this->___dsa_0);
		ByteU5BU5D_t1_109* L_4 = ___rgbHash;
		NullCheck(L_3);
		ByteU5BU5D_t1_109* L_5 = (ByteU5BU5D_t1_109*)VirtFuncInvoker1< ByteU5BU5D_t1_109*, ByteU5BU5D_t1_109* >::Invoke(13 /* System.Byte[] System.Security.Cryptography.DSA::CreateSignature(System.Byte[]) */, L_3, L_4);
		return L_5;
	}
}
// System.Void System.Security.Cryptography.DSASignatureFormatter::SetHashAlgorithm(System.String)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidCastException_t1_1558_il2cpp_TypeInfo_var;
extern TypeInfo* CryptographicUnexpectedOperationException_t1_1195_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2725;
extern Il2CppCodeGenString* _stringLiteral2726;
extern "C" void DSASignatureFormatter_SetHashAlgorithm_m1_10324 (DSASignatureFormatter_t1_1206 * __this, String_t* ___strName, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		InvalidCastException_t1_1558_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(26);
		CryptographicUnexpectedOperationException_t1_1195_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(873);
		_stringLiteral2725 = il2cpp_codegen_string_literal_from_index(2725);
		_stringLiteral2726 = il2cpp_codegen_string_literal_from_index(2726);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___strName;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, _stringLiteral2725, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		String_t* L_2 = ___strName;
		SHA1_Create_m1_10642(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		goto IL_0033;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1_33 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t1_1558_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		{
			String_t* L_3 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2726, /*hidden argument*/NULL);
			CryptographicUnexpectedOperationException_t1_1195 * L_4 = (CryptographicUnexpectedOperationException_t1_1195 *)il2cpp_codegen_object_new (CryptographicUnexpectedOperationException_t1_1195_il2cpp_TypeInfo_var);
			CryptographicUnexpectedOperationException__ctor_m1_10227(L_4, L_3, /*hidden argument*/NULL);
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
		}

IL_002e:
		{
			goto IL_0033;
		}
	} // end catch (depth: 1)

IL_0033:
	{
		return;
	}
}
// System.Void System.Security.Cryptography.DSASignatureFormatter::SetKey(System.Security.Cryptography.AsymmetricAlgorithm)
extern TypeInfo* DSA_t1_160_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral793;
extern "C" void DSASignatureFormatter_SetKey_m1_10325 (DSASignatureFormatter_t1_1206 * __this, AsymmetricAlgorithm_t1_228 * ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DSA_t1_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(200);
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		_stringLiteral793 = il2cpp_codegen_string_literal_from_index(793);
		s_Il2CppMethodIntialized = true;
	}
	{
		AsymmetricAlgorithm_t1_228 * L_0 = ___key;
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		AsymmetricAlgorithm_t1_228 * L_1 = ___key;
		__this->___dsa_0 = ((DSA_t1_160 *)CastclassClass(L_1, DSA_t1_160_il2cpp_TypeInfo_var));
		goto IL_0022;
	}

IL_0017:
	{
		ArgumentNullException_t1_1500 * L_2 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_2, _stringLiteral793, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0022:
	{
		return;
	}
}
// System.Void System.Security.Cryptography.DeriveBytes::.ctor()
extern "C" void DeriveBytes__ctor_m1_10326 (DeriveBytes_t1_1207 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.Cryptography.FromBase64Transform::.ctor()
extern "C" void FromBase64Transform__ctor_m1_10327 (FromBase64Transform_t1_1209 * __this, const MethodInfo* method)
{
	{
		FromBase64Transform__ctor_m1_10328(__this, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.Cryptography.FromBase64Transform::.ctor(System.Security.Cryptography.FromBase64TransformMode)
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern "C" void FromBase64Transform__ctor_m1_10328 (FromBase64Transform_t1_1209 * __this, int32_t ___whitespaces, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___whitespaces;
		__this->___mode_1 = L_0;
		__this->___accumulator_2 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 4));
		__this->___accPtr_3 = 0;
		__this->___m_disposed_4 = 0;
		return;
	}
}
// System.Void System.Security.Cryptography.FromBase64Transform::System.IDisposable.Dispose()
extern "C" void FromBase64Transform_System_IDisposable_Dispose_m1_10329 (FromBase64Transform_t1_1209 * __this, const MethodInfo* method)
{
	{
		VirtActionInvoker1< bool >::Invoke(12 /* System.Void System.Security.Cryptography.FromBase64Transform::Dispose(System.Boolean) */, __this, 1);
		GC_SuppressFinalize_m1_14113(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.Cryptography.FromBase64Transform::Finalize()
extern "C" void FromBase64Transform_Finalize_m1_10330 (FromBase64Transform_t1_1209 * __this, const MethodInfo* method)
{
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		VirtActionInvoker1< bool >::Invoke(12 /* System.Void System.Security.Cryptography.FromBase64Transform::Dispose(System.Boolean) */, __this, 0);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m1_3(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0013:
	{
		return;
	}
}
// System.Boolean System.Security.Cryptography.FromBase64Transform::get_CanTransformMultipleBlocks()
extern "C" bool FromBase64Transform_get_CanTransformMultipleBlocks_m1_10331 (FromBase64Transform_t1_1209 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Boolean System.Security.Cryptography.FromBase64Transform::get_CanReuseTransform()
extern "C" bool FromBase64Transform_get_CanReuseTransform_m1_10332 (FromBase64Transform_t1_1209 * __this, const MethodInfo* method)
{
	{
		return 1;
	}
}
// System.Int32 System.Security.Cryptography.FromBase64Transform::get_InputBlockSize()
extern "C" int32_t FromBase64Transform_get_InputBlockSize_m1_10333 (FromBase64Transform_t1_1209 * __this, const MethodInfo* method)
{
	{
		return 1;
	}
}
// System.Int32 System.Security.Cryptography.FromBase64Transform::get_OutputBlockSize()
extern "C" int32_t FromBase64Transform_get_OutputBlockSize_m1_10334 (FromBase64Transform_t1_1209 * __this, const MethodInfo* method)
{
	{
		return 3;
	}
}
// System.Void System.Security.Cryptography.FromBase64Transform::Clear()
extern "C" void FromBase64Transform_Clear_m1_10335 (FromBase64Transform_t1_1209 * __this, const MethodInfo* method)
{
	{
		VirtActionInvoker1< bool >::Invoke(12 /* System.Void System.Security.Cryptography.FromBase64Transform::Dispose(System.Boolean) */, __this, 1);
		return;
	}
}
// System.Void System.Security.Cryptography.FromBase64Transform::Dispose(System.Boolean)
extern "C" void FromBase64Transform_Dispose_m1_10336 (FromBase64Transform_t1_1209 * __this, bool ___disposing, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_disposed_4);
		if (L_0)
		{
			goto IL_003e;
		}
	}
	{
		ByteU5BU5D_t1_109* L_1 = (__this->___accumulator_2);
		if (!L_1)
		{
			goto IL_002a;
		}
	}
	{
		ByteU5BU5D_t1_109* L_2 = (__this->___accumulator_2);
		ByteU5BU5D_t1_109* L_3 = (__this->___accumulator_2);
		NullCheck(L_3);
		Array_Clear_m1_1037(NULL /*static, unused*/, (Array_t *)(Array_t *)L_2, 0, (((int32_t)((int32_t)(((Array_t *)L_3)->max_length)))), /*hidden argument*/NULL);
	}

IL_002a:
	{
		bool L_4 = ___disposing;
		if (!L_4)
		{
			goto IL_0037;
		}
	}
	{
		__this->___accumulator_2 = (ByteU5BU5D_t1_109*)NULL;
	}

IL_0037:
	{
		__this->___m_disposed_4 = 1;
	}

IL_003e:
	{
		return;
	}
}
// System.Byte System.Security.Cryptography.FromBase64Transform::lookup(System.Byte)
extern TypeInfo* FormatException_t1_592_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2728;
extern "C" uint8_t FromBase64Transform_lookup_m1_10337 (FromBase64Transform_t1_1209 * __this, uint8_t ___input, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormatException_t1_592_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(16);
		_stringLiteral2728 = il2cpp_codegen_string_literal_from_index(2728);
		s_Il2CppMethodIntialized = true;
	}
	uint8_t V_0 = 0x0;
	{
		uint8_t L_0 = ___input;
		ByteU5BU5D_t1_109* L_1 = (__this->___lookupTable_5);
		NullCheck(L_1);
		if ((((int32_t)L_0) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_1)->max_length)))))))
		{
			goto IL_001e;
		}
	}
	{
		String_t* L_2 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2728, /*hidden argument*/NULL);
		FormatException_t1_592 * L_3 = (FormatException_t1_592 *)il2cpp_codegen_object_new (FormatException_t1_592_il2cpp_TypeInfo_var);
		FormatException__ctor_m1_14100(L_3, L_2, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_001e:
	{
		ByteU5BU5D_t1_109* L_4 = (__this->___lookupTable_5);
		uint8_t L_5 = ___input;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		uint8_t L_6 = L_5;
		V_0 = (*(uint8_t*)(uint8_t*)SZArrayLdElema(L_4, L_6, sizeof(uint8_t)));
		uint8_t L_7 = V_0;
		if ((!(((uint32_t)L_7) == ((uint32_t)((int32_t)255)))))
		{
			goto IL_0042;
		}
	}
	{
		String_t* L_8 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2728, /*hidden argument*/NULL);
		FormatException_t1_592 * L_9 = (FormatException_t1_592 *)il2cpp_codegen_object_new (FormatException_t1_592_il2cpp_TypeInfo_var);
		FormatException__ctor_m1_14100(L_9, L_8, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_9);
	}

IL_0042:
	{
		uint8_t L_10 = V_0;
		return L_10;
	}
}
// System.Int32 System.Security.Cryptography.FromBase64Transform::ProcessBlock(System.Byte[],System.Int32)
extern TypeInfo* Base64Constants_t1_1187_il2cpp_TypeInfo_var;
extern "C" int32_t FromBase64Transform_ProcessBlock_m1_10338 (FromBase64Transform_t1_1209 * __this, ByteU5BU5D_t1_109* ___output, int32_t ___offset, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Base64Constants_t1_1187_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(871);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	{
		V_0 = 0;
		ByteU5BU5D_t1_109* L_0 = (__this->___accumulator_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 3);
		int32_t L_1 = 3;
		if ((!(((uint32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_0, L_1, sizeof(uint8_t)))) == ((uint32_t)((int32_t)61)))))
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_2 = V_0;
		V_0 = ((int32_t)((int32_t)L_2+(int32_t)1));
	}

IL_0015:
	{
		ByteU5BU5D_t1_109* L_3 = (__this->___accumulator_2);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 2);
		int32_t L_4 = 2;
		if ((!(((uint32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_3, L_4, sizeof(uint8_t)))) == ((uint32_t)((int32_t)61)))))
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_5 = V_0;
		V_0 = ((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_0028:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Base64Constants_t1_1187_il2cpp_TypeInfo_var);
		ByteU5BU5D_t1_109* L_6 = ((Base64Constants_t1_1187_StaticFields*)Base64Constants_t1_1187_il2cpp_TypeInfo_var->static_fields)->___DecodeTable_1;
		__this->___lookupTable_5 = L_6;
		int32_t L_7 = V_0;
		V_5 = L_7;
		int32_t L_8 = V_5;
		if (L_8 == 0)
		{
			goto IL_004e;
		}
		if (L_8 == 1)
		{
			goto IL_00ba;
		}
		if (L_8 == 2)
		{
			goto IL_0107;
		}
	}
	{
		goto IL_0135;
	}

IL_004e:
	{
		ByteU5BU5D_t1_109* L_9 = (__this->___accumulator_2);
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 0);
		int32_t L_10 = 0;
		uint8_t L_11 = FromBase64Transform_lookup_m1_10337(__this, (*(uint8_t*)(uint8_t*)SZArrayLdElema(L_9, L_10, sizeof(uint8_t))), /*hidden argument*/NULL);
		V_1 = L_11;
		ByteU5BU5D_t1_109* L_12 = (__this->___accumulator_2);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 1);
		int32_t L_13 = 1;
		uint8_t L_14 = FromBase64Transform_lookup_m1_10337(__this, (*(uint8_t*)(uint8_t*)SZArrayLdElema(L_12, L_13, sizeof(uint8_t))), /*hidden argument*/NULL);
		V_2 = L_14;
		ByteU5BU5D_t1_109* L_15 = (__this->___accumulator_2);
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 2);
		int32_t L_16 = 2;
		uint8_t L_17 = FromBase64Transform_lookup_m1_10337(__this, (*(uint8_t*)(uint8_t*)SZArrayLdElema(L_15, L_16, sizeof(uint8_t))), /*hidden argument*/NULL);
		V_3 = L_17;
		ByteU5BU5D_t1_109* L_18 = (__this->___accumulator_2);
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 3);
		int32_t L_19 = 3;
		uint8_t L_20 = FromBase64Transform_lookup_m1_10337(__this, (*(uint8_t*)(uint8_t*)SZArrayLdElema(L_18, L_19, sizeof(uint8_t))), /*hidden argument*/NULL);
		V_4 = L_20;
		ByteU5BU5D_t1_109* L_21 = ___output;
		int32_t L_22 = ___offset;
		int32_t L_23 = L_22;
		___offset = ((int32_t)((int32_t)L_23+(int32_t)1));
		int32_t L_24 = V_1;
		int32_t L_25 = V_2;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, L_23);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_21, L_23, sizeof(uint8_t))) = (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((int32_t)((int32_t)L_24<<(int32_t)2))|(int32_t)((int32_t)((int32_t)L_25>>(int32_t)4)))))));
		ByteU5BU5D_t1_109* L_26 = ___output;
		int32_t L_27 = ___offset;
		int32_t L_28 = L_27;
		___offset = ((int32_t)((int32_t)L_28+(int32_t)1));
		int32_t L_29 = V_2;
		int32_t L_30 = V_3;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, L_28);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_26, L_28, sizeof(uint8_t))) = (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((int32_t)((int32_t)L_29<<(int32_t)4))|(int32_t)((int32_t)((int32_t)L_30>>(int32_t)2)))))));
		ByteU5BU5D_t1_109* L_31 = ___output;
		int32_t L_32 = ___offset;
		int32_t L_33 = V_3;
		int32_t L_34 = V_4;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, L_32);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_31, L_32, sizeof(uint8_t))) = (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((int32_t)((int32_t)L_33<<(int32_t)6))|(int32_t)L_34)))));
		goto IL_0135;
	}

IL_00ba:
	{
		ByteU5BU5D_t1_109* L_35 = (__this->___accumulator_2);
		NullCheck(L_35);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_35, 0);
		int32_t L_36 = 0;
		uint8_t L_37 = FromBase64Transform_lookup_m1_10337(__this, (*(uint8_t*)(uint8_t*)SZArrayLdElema(L_35, L_36, sizeof(uint8_t))), /*hidden argument*/NULL);
		V_1 = L_37;
		ByteU5BU5D_t1_109* L_38 = (__this->___accumulator_2);
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 1);
		int32_t L_39 = 1;
		uint8_t L_40 = FromBase64Transform_lookup_m1_10337(__this, (*(uint8_t*)(uint8_t*)SZArrayLdElema(L_38, L_39, sizeof(uint8_t))), /*hidden argument*/NULL);
		V_2 = L_40;
		ByteU5BU5D_t1_109* L_41 = (__this->___accumulator_2);
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, 2);
		int32_t L_42 = 2;
		uint8_t L_43 = FromBase64Transform_lookup_m1_10337(__this, (*(uint8_t*)(uint8_t*)SZArrayLdElema(L_41, L_42, sizeof(uint8_t))), /*hidden argument*/NULL);
		V_3 = L_43;
		ByteU5BU5D_t1_109* L_44 = ___output;
		int32_t L_45 = ___offset;
		int32_t L_46 = L_45;
		___offset = ((int32_t)((int32_t)L_46+(int32_t)1));
		int32_t L_47 = V_1;
		int32_t L_48 = V_2;
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, L_46);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_44, L_46, sizeof(uint8_t))) = (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((int32_t)((int32_t)L_47<<(int32_t)2))|(int32_t)((int32_t)((int32_t)L_48>>(int32_t)4)))))));
		ByteU5BU5D_t1_109* L_49 = ___output;
		int32_t L_50 = ___offset;
		int32_t L_51 = V_2;
		int32_t L_52 = V_3;
		NullCheck(L_49);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_49, L_50);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_49, L_50, sizeof(uint8_t))) = (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((int32_t)((int32_t)L_51<<(int32_t)4))|(int32_t)((int32_t)((int32_t)L_52>>(int32_t)2)))))));
		goto IL_0135;
	}

IL_0107:
	{
		ByteU5BU5D_t1_109* L_53 = (__this->___accumulator_2);
		NullCheck(L_53);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_53, 0);
		int32_t L_54 = 0;
		uint8_t L_55 = FromBase64Transform_lookup_m1_10337(__this, (*(uint8_t*)(uint8_t*)SZArrayLdElema(L_53, L_54, sizeof(uint8_t))), /*hidden argument*/NULL);
		V_1 = L_55;
		ByteU5BU5D_t1_109* L_56 = (__this->___accumulator_2);
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, 1);
		int32_t L_57 = 1;
		uint8_t L_58 = FromBase64Transform_lookup_m1_10337(__this, (*(uint8_t*)(uint8_t*)SZArrayLdElema(L_56, L_57, sizeof(uint8_t))), /*hidden argument*/NULL);
		V_2 = L_58;
		ByteU5BU5D_t1_109* L_59 = ___output;
		int32_t L_60 = ___offset;
		int32_t L_61 = V_1;
		int32_t L_62 = V_2;
		NullCheck(L_59);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_59, L_60);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_59, L_60, sizeof(uint8_t))) = (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((int32_t)((int32_t)L_61<<(int32_t)2))|(int32_t)((int32_t)((int32_t)L_62>>(int32_t)4)))))));
		goto IL_0135;
	}

IL_0135:
	{
		int32_t L_63 = V_0;
		return ((int32_t)((int32_t)3-(int32_t)L_63));
	}
}
// System.Void System.Security.Cryptography.FromBase64Transform::CheckInputParameters(System.Byte[],System.Int32,System.Int32)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* OutOfMemoryException_t1_1557_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern TypeInfo* OverflowException_t1_1590_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral476;
extern Il2CppCodeGenString* _stringLiteral477;
extern Il2CppCodeGenString* _stringLiteral51;
extern Il2CppCodeGenString* _stringLiteral2729;
extern Il2CppCodeGenString* _stringLiteral479;
extern Il2CppCodeGenString* _stringLiteral2730;
extern "C" void FromBase64Transform_CheckInputParameters_m1_10339 (FromBase64Transform_t1_1209 * __this, ByteU5BU5D_t1_109* ___inputBuffer, int32_t ___inputOffset, int32_t ___inputCount, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		OutOfMemoryException_t1_1557_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(596);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		OverflowException_t1_1590_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(13);
		_stringLiteral476 = il2cpp_codegen_string_literal_from_index(476);
		_stringLiteral477 = il2cpp_codegen_string_literal_from_index(477);
		_stringLiteral51 = il2cpp_codegen_string_literal_from_index(51);
		_stringLiteral2729 = il2cpp_codegen_string_literal_from_index(2729);
		_stringLiteral479 = il2cpp_codegen_string_literal_from_index(479);
		_stringLiteral2730 = il2cpp_codegen_string_literal_from_index(2730);
		s_Il2CppMethodIntialized = true;
	}
	{
		ByteU5BU5D_t1_109* L_0 = ___inputBuffer;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, _stringLiteral476, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		int32_t L_2 = ___inputOffset;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0028;
		}
	}
	{
		ArgumentOutOfRangeException_t1_1501 * L_3 = (ArgumentOutOfRangeException_t1_1501 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1_13275(L_3, _stringLiteral477, _stringLiteral51, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0028:
	{
		int32_t L_4 = ___inputCount;
		ByteU5BU5D_t1_109* L_5 = ___inputBuffer;
		NullCheck(L_5);
		if ((((int32_t)L_4) <= ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_5)->max_length)))))))
		{
			goto IL_004b;
		}
	}
	{
		String_t* L_6 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral479, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m1_559(NULL /*static, unused*/, _stringLiteral2729, L_6, /*hidden argument*/NULL);
		OutOfMemoryException_t1_1557 * L_8 = (OutOfMemoryException_t1_1557 *)il2cpp_codegen_object_new (OutOfMemoryException_t1_1557_il2cpp_TypeInfo_var);
		OutOfMemoryException__ctor_m1_14543(L_8, L_7, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_8);
	}

IL_004b:
	{
		int32_t L_9 = ___inputOffset;
		ByteU5BU5D_t1_109* L_10 = ___inputBuffer;
		NullCheck(L_10);
		int32_t L_11 = ___inputCount;
		if ((((int32_t)L_9) <= ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Array_t *)L_10)->max_length))))-(int32_t)L_11)))))
		{
			goto IL_006b;
		}
	}
	{
		String_t* L_12 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral479, /*hidden argument*/NULL);
		ArgumentException_t1_1425 * L_13 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13262(L_13, _stringLiteral477, L_12, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_13);
	}

IL_006b:
	{
		int32_t L_14 = ___inputCount;
		if ((((int32_t)L_14) >= ((int32_t)0)))
		{
			goto IL_007d;
		}
	}
	{
		OverflowException_t1_1590 * L_15 = (OverflowException_t1_1590 *)il2cpp_codegen_object_new (OverflowException_t1_1590_il2cpp_TypeInfo_var);
		OverflowException__ctor_m1_14547(L_15, _stringLiteral2730, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_15);
	}

IL_007d:
	{
		return;
	}
}
// System.Int32 System.Security.Cryptography.FromBase64Transform::TransformBlock(System.Byte[],System.Int32,System.Int32,System.Byte[],System.Int32)
extern TypeInfo* ObjectDisposedException_t1_1588_il2cpp_TypeInfo_var;
extern TypeInfo* FormatException_t1_592_il2cpp_TypeInfo_var;
extern TypeInfo* Char_t1_15_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2731;
extern Il2CppCodeGenString* _stringLiteral481;
extern "C" int32_t FromBase64Transform_TransformBlock_m1_10340 (FromBase64Transform_t1_1209 * __this, ByteU5BU5D_t1_109* ___inputBuffer, int32_t ___inputOffset, int32_t ___inputCount, ByteU5BU5D_t1_109* ___outputBuffer, int32_t ___outputOffset, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectDisposedException_t1_1588_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(136);
		FormatException_t1_592_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(16);
		Char_t1_15_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(12);
		_stringLiteral2731 = il2cpp_codegen_string_literal_from_index(2731);
		_stringLiteral481 = il2cpp_codegen_string_literal_from_index(481);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	uint8_t V_1 = 0x0;
	int32_t V_2 = 0;
	{
		bool L_0 = (__this->___m_disposed_4);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		ObjectDisposedException_t1_1588 * L_1 = (ObjectDisposedException_t1_1588 *)il2cpp_codegen_object_new (ObjectDisposedException_t1_1588_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1_14523(L_1, _stringLiteral2731, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		ByteU5BU5D_t1_109* L_2 = ___inputBuffer;
		int32_t L_3 = ___inputOffset;
		int32_t L_4 = ___inputCount;
		FromBase64Transform_CheckInputParameters_m1_10339(__this, L_2, L_3, L_4, /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_5 = ___outputBuffer;
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		int32_t L_6 = ___outputOffset;
		if ((((int32_t)L_6) >= ((int32_t)0)))
		{
			goto IL_0039;
		}
	}

IL_002e:
	{
		FormatException_t1_592 * L_7 = (FormatException_t1_592 *)il2cpp_codegen_object_new (FormatException_t1_592_il2cpp_TypeInfo_var);
		FormatException__ctor_m1_14100(L_7, _stringLiteral481, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_7);
	}

IL_0039:
	{
		V_0 = 0;
		goto IL_00ce;
	}

IL_0040:
	{
		int32_t L_8 = (__this->___accPtr_3);
		if ((((int32_t)L_8) >= ((int32_t)4)))
		{
			goto IL_00a3;
		}
	}
	{
		ByteU5BU5D_t1_109* L_9 = ___inputBuffer;
		int32_t L_10 = ___inputOffset;
		int32_t L_11 = L_10;
		___inputOffset = ((int32_t)((int32_t)L_11+(int32_t)1));
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_11);
		int32_t L_12 = L_11;
		V_1 = (*(uint8_t*)(uint8_t*)SZArrayLdElema(L_9, L_12, sizeof(uint8_t)));
		int32_t L_13 = (__this->___mode_1);
		if (L_13)
		{
			goto IL_008a;
		}
	}
	{
		uint8_t L_14 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Char_t1_15_il2cpp_TypeInfo_var);
		bool L_15 = Char_IsWhiteSpace_m1_398(NULL /*static, unused*/, (((int32_t)((uint16_t)L_14))), /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_0085;
		}
	}
	{
		ByteU5BU5D_t1_109* L_16 = (__this->___accumulator_2);
		int32_t L_17 = (__this->___accPtr_3);
		int32_t L_18 = L_17;
		V_2 = L_18;
		__this->___accPtr_3 = ((int32_t)((int32_t)L_18+(int32_t)1));
		int32_t L_19 = V_2;
		uint8_t L_20 = V_1;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_19);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_16, L_19, sizeof(uint8_t))) = (uint8_t)L_20;
	}

IL_0085:
	{
		goto IL_00a3;
	}

IL_008a:
	{
		ByteU5BU5D_t1_109* L_21 = (__this->___accumulator_2);
		int32_t L_22 = (__this->___accPtr_3);
		int32_t L_23 = L_22;
		V_2 = L_23;
		__this->___accPtr_3 = ((int32_t)((int32_t)L_23+(int32_t)1));
		int32_t L_24 = V_2;
		uint8_t L_25 = V_1;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, L_24);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_21, L_24, sizeof(uint8_t))) = (uint8_t)L_25;
	}

IL_00a3:
	{
		int32_t L_26 = (__this->___accPtr_3);
		if ((!(((uint32_t)L_26) == ((uint32_t)4))))
		{
			goto IL_00c9;
		}
	}
	{
		int32_t L_27 = V_0;
		ByteU5BU5D_t1_109* L_28 = ___outputBuffer;
		int32_t L_29 = ___outputOffset;
		int32_t L_30 = FromBase64Transform_ProcessBlock_m1_10338(__this, L_28, L_29, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_27+(int32_t)L_30));
		int32_t L_31 = ___outputOffset;
		___outputOffset = ((int32_t)((int32_t)L_31+(int32_t)3));
		__this->___accPtr_3 = 0;
	}

IL_00c9:
	{
		int32_t L_32 = ___inputCount;
		___inputCount = ((int32_t)((int32_t)L_32-(int32_t)1));
	}

IL_00ce:
	{
		int32_t L_33 = ___inputCount;
		if ((((int32_t)L_33) > ((int32_t)0)))
		{
			goto IL_0040;
		}
	}
	{
		int32_t L_34 = V_0;
		return L_34;
	}
}
// System.Byte[] System.Security.Cryptography.FromBase64Transform::TransformFinalBlock(System.Byte[],System.Int32,System.Int32)
extern TypeInfo* ObjectDisposedException_t1_1588_il2cpp_TypeInfo_var;
extern TypeInfo* Char_t1_15_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2731;
extern "C" ByteU5BU5D_t1_109* FromBase64Transform_TransformFinalBlock_m1_10341 (FromBase64Transform_t1_1209 * __this, ByteU5BU5D_t1_109* ___inputBuffer, int32_t ___inputOffset, int32_t ___inputCount, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectDisposedException_t1_1588_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(136);
		Char_t1_15_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(12);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		_stringLiteral2731 = il2cpp_codegen_string_literal_from_index(2731);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	uint16_t V_6 = 0x0;
	int32_t V_7 = 0;
	ByteU5BU5D_t1_109* V_8 = {0};
	{
		bool L_0 = (__this->___m_disposed_4);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		ObjectDisposedException_t1_1588 * L_1 = (ObjectDisposedException_t1_1588 *)il2cpp_codegen_object_new (ObjectDisposedException_t1_1588_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1_14523(L_1, _stringLiteral2731, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		ByteU5BU5D_t1_109* L_2 = ___inputBuffer;
		int32_t L_3 = ___inputOffset;
		int32_t L_4 = ___inputCount;
		FromBase64Transform_CheckInputParameters_m1_10339(__this, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_0 = 0;
		V_1 = 0;
		int32_t L_5 = (__this->___mode_1);
		if (L_5)
		{
			goto IL_00c2;
		}
	}
	{
		int32_t L_6 = ___inputOffset;
		V_2 = L_6;
		V_3 = 0;
		goto IL_0051;
	}

IL_0037:
	{
		ByteU5BU5D_t1_109* L_7 = ___inputBuffer;
		int32_t L_8 = V_2;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		IL2CPP_RUNTIME_CLASS_INIT(Char_t1_15_il2cpp_TypeInfo_var);
		bool L_10 = Char_IsWhiteSpace_m1_398(NULL /*static, unused*/, (((int32_t)((uint16_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_7, L_9, sizeof(uint8_t)))))), /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0049;
		}
	}
	{
		int32_t L_11 = V_0;
		V_0 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_0049:
	{
		int32_t L_12 = V_2;
		V_2 = ((int32_t)((int32_t)L_12+(int32_t)1));
		int32_t L_13 = V_3;
		V_3 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0051:
	{
		int32_t L_14 = V_3;
		int32_t L_15 = ___inputCount;
		if ((((int32_t)L_14) < ((int32_t)L_15)))
		{
			goto IL_0037;
		}
	}
	{
		int32_t L_16 = V_0;
		int32_t L_17 = ___inputCount;
		if ((!(((uint32_t)L_16) == ((uint32_t)L_17))))
		{
			goto IL_0066;
		}
	}
	{
		return ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 0));
	}

IL_0066:
	{
		int32_t L_18 = ___inputOffset;
		int32_t L_19 = ___inputCount;
		V_4 = ((int32_t)((int32_t)((int32_t)((int32_t)L_18+(int32_t)L_19))-(int32_t)1));
		int32_t L_20 = ___inputCount;
		int32_t L_21 = Math_Min_m1_14218(NULL /*static, unused*/, 2, L_20, /*hidden argument*/NULL);
		V_5 = L_21;
		goto IL_00b5;
	}

IL_007b:
	{
		ByteU5BU5D_t1_109* L_22 = ___inputBuffer;
		int32_t L_23 = V_4;
		int32_t L_24 = L_23;
		V_4 = ((int32_t)((int32_t)L_24-(int32_t)1));
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_24);
		int32_t L_25 = L_24;
		V_6 = (((int32_t)((uint16_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_22, L_25, sizeof(uint8_t))))));
		uint16_t L_26 = V_6;
		if ((!(((uint32_t)L_26) == ((uint32_t)((int32_t)61)))))
		{
			goto IL_009f;
		}
	}
	{
		int32_t L_27 = V_1;
		V_1 = ((int32_t)((int32_t)L_27+(int32_t)1));
		int32_t L_28 = V_5;
		V_5 = ((int32_t)((int32_t)L_28-(int32_t)1));
		goto IL_00b5;
	}

IL_009f:
	{
		uint16_t L_29 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Char_t1_15_il2cpp_TypeInfo_var);
		bool L_30 = Char_IsWhiteSpace_m1_398(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_00b0;
		}
	}
	{
		goto IL_00b5;
	}

IL_00b0:
	{
		goto IL_00bd;
	}

IL_00b5:
	{
		int32_t L_31 = V_5;
		if ((((int32_t)L_31) > ((int32_t)0)))
		{
			goto IL_007b;
		}
	}

IL_00bd:
	{
		goto IL_00e6;
	}

IL_00c2:
	{
		ByteU5BU5D_t1_109* L_32 = ___inputBuffer;
		int32_t L_33 = ___inputOffset;
		int32_t L_34 = ___inputCount;
		NullCheck(L_32);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_32, ((int32_t)((int32_t)((int32_t)((int32_t)L_33+(int32_t)L_34))-(int32_t)1)));
		int32_t L_35 = ((int32_t)((int32_t)((int32_t)((int32_t)L_33+(int32_t)L_34))-(int32_t)1));
		if ((!(((uint32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_32, L_35, sizeof(uint8_t)))) == ((uint32_t)((int32_t)61)))))
		{
			goto IL_00d4;
		}
	}
	{
		int32_t L_36 = V_1;
		V_1 = ((int32_t)((int32_t)L_36+(int32_t)1));
	}

IL_00d4:
	{
		ByteU5BU5D_t1_109* L_37 = ___inputBuffer;
		int32_t L_38 = ___inputOffset;
		int32_t L_39 = ___inputCount;
		NullCheck(L_37);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_37, ((int32_t)((int32_t)((int32_t)((int32_t)L_38+(int32_t)L_39))-(int32_t)2)));
		int32_t L_40 = ((int32_t)((int32_t)((int32_t)((int32_t)L_38+(int32_t)L_39))-(int32_t)2));
		if ((!(((uint32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_37, L_40, sizeof(uint8_t)))) == ((uint32_t)((int32_t)61)))))
		{
			goto IL_00e6;
		}
	}
	{
		int32_t L_41 = V_1;
		V_1 = ((int32_t)((int32_t)L_41+(int32_t)1));
	}

IL_00e6:
	{
		int32_t L_42 = ___inputCount;
		if ((((int32_t)L_42) >= ((int32_t)4)))
		{
			goto IL_0132;
		}
	}
	{
		int32_t L_43 = V_1;
		if ((((int32_t)L_43) >= ((int32_t)2)))
		{
			goto IL_0132;
		}
	}
	{
		int32_t L_44 = (__this->___accPtr_3);
		if ((((int32_t)L_44) <= ((int32_t)2)))
		{
			goto IL_0113;
		}
	}
	{
		ByteU5BU5D_t1_109* L_45 = (__this->___accumulator_2);
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, 3);
		int32_t L_46 = 3;
		if ((!(((uint32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_45, L_46, sizeof(uint8_t)))) == ((uint32_t)((int32_t)61)))))
		{
			goto IL_0113;
		}
	}
	{
		int32_t L_47 = V_1;
		V_1 = ((int32_t)((int32_t)L_47+(int32_t)1));
	}

IL_0113:
	{
		int32_t L_48 = (__this->___accPtr_3);
		if ((((int32_t)L_48) <= ((int32_t)1)))
		{
			goto IL_0132;
		}
	}
	{
		ByteU5BU5D_t1_109* L_49 = (__this->___accumulator_2);
		NullCheck(L_49);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_49, 2);
		int32_t L_50 = 2;
		if ((!(((uint32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_49, L_50, sizeof(uint8_t)))) == ((uint32_t)((int32_t)61)))))
		{
			goto IL_0132;
		}
	}
	{
		int32_t L_51 = V_1;
		V_1 = ((int32_t)((int32_t)L_51+(int32_t)1));
	}

IL_0132:
	{
		int32_t L_52 = (__this->___accPtr_3);
		int32_t L_53 = ___inputCount;
		int32_t L_54 = V_0;
		int32_t L_55 = V_1;
		V_7 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_52+(int32_t)L_53))-(int32_t)L_54))>>(int32_t)2))*(int32_t)3))-(int32_t)L_55));
		int32_t L_56 = V_7;
		if ((((int32_t)L_56) > ((int32_t)0)))
		{
			goto IL_0153;
		}
	}
	{
		return ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 0));
	}

IL_0153:
	{
		int32_t L_57 = V_7;
		V_8 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, L_57));
		ByteU5BU5D_t1_109* L_58 = ___inputBuffer;
		int32_t L_59 = ___inputOffset;
		int32_t L_60 = ___inputCount;
		ByteU5BU5D_t1_109* L_61 = V_8;
		VirtFuncInvoker5< int32_t, ByteU5BU5D_t1_109*, int32_t, int32_t, ByteU5BU5D_t1_109*, int32_t >::Invoke(9 /* System.Int32 System.Security.Cryptography.FromBase64Transform::TransformBlock(System.Byte[],System.Int32,System.Int32,System.Byte[],System.Int32) */, __this, L_58, L_59, L_60, L_61, 0);
		ByteU5BU5D_t1_109* L_62 = V_8;
		return L_62;
	}
}
// System.Void System.Security.Cryptography.HMAC::.ctor()
extern "C" void HMAC__ctor_m1_10342 (HMAC_t1_1210 * __this, const MethodInfo* method)
{
	{
		KeyedHashAlgorithm__ctor_m1_10399(__this, /*hidden argument*/NULL);
		__this->____disposed_5 = 0;
		__this->____blockSizeValue_9 = ((int32_t)64);
		return;
	}
}
// System.Int32 System.Security.Cryptography.HMAC::get_BlockSizeValue()
extern "C" int32_t HMAC_get_BlockSizeValue_m1_10343 (HMAC_t1_1210 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->____blockSizeValue_9);
		return L_0;
	}
}
// System.Void System.Security.Cryptography.HMAC::set_BlockSizeValue(System.Int32)
extern "C" void HMAC_set_BlockSizeValue_m1_10344 (HMAC_t1_1210 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->____blockSizeValue_9 = L_0;
		return;
	}
}
// System.String System.Security.Cryptography.HMAC::get_HashName()
extern "C" String_t* HMAC_get_HashName_m1_10345 (HMAC_t1_1210 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->____hashName_6);
		return L_0;
	}
}
// System.Void System.Security.Cryptography.HMAC::set_HashName(System.String)
extern "C" void HMAC_set_HashName_m1_10346 (HMAC_t1_1210 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->____hashName_6 = L_0;
		String_t* L_1 = (__this->____hashName_6);
		HashAlgorithm_t1_162 * L_2 = HashAlgorithm_Create_m1_10385(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		__this->____algo_7 = L_2;
		return;
	}
}
// System.Byte[] System.Security.Cryptography.HMAC::get_Key()
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern "C" ByteU5BU5D_t1_109* HMAC_get_Key_m1_10347 (HMAC_t1_1210 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		s_Il2CppMethodIntialized = true;
	}
	{
		ByteU5BU5D_t1_109* L_0 = KeyedHashAlgorithm_get_Key_m1_10401(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Object_t * L_1 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(5 /* System.Object System.Array::Clone() */, L_0);
		return ((ByteU5BU5D_t1_109*)Castclass(L_1, ByteU5BU5D_t1_109_il2cpp_TypeInfo_var));
	}
}
// System.Void System.Security.Cryptography.HMAC::set_Key(System.Byte[])
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern "C" void HMAC_set_Key_m1_10348 (HMAC_t1_1210 * __this, ByteU5BU5D_t1_109* ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		s_Il2CppMethodIntialized = true;
	}
	{
		ByteU5BU5D_t1_109* L_0 = ___value;
		if (!L_0)
		{
			goto IL_0027;
		}
	}
	{
		ByteU5BU5D_t1_109* L_1 = ___value;
		NullCheck(L_1);
		if ((((int32_t)(((int32_t)((int32_t)(((Array_t *)L_1)->max_length))))) <= ((int32_t)((int32_t)64))))
		{
			goto IL_0027;
		}
	}
	{
		HashAlgorithm_t1_162 * L_2 = (__this->____algo_7);
		ByteU5BU5D_t1_109* L_3 = ___value;
		NullCheck(L_2);
		ByteU5BU5D_t1_109* L_4 = HashAlgorithm_ComputeHash_m1_10381(L_2, L_3, /*hidden argument*/NULL);
		KeyedHashAlgorithm_set_Key_m1_10402(__this, L_4, /*hidden argument*/NULL);
		goto IL_0038;
	}

IL_0027:
	{
		ByteU5BU5D_t1_109* L_5 = ___value;
		NullCheck(L_5);
		Object_t * L_6 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(5 /* System.Object System.Array::Clone() */, L_5);
		KeyedHashAlgorithm_set_Key_m1_10402(__this, ((ByteU5BU5D_t1_109*)Castclass(L_6, ByteU5BU5D_t1_109_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
	}

IL_0038:
	{
		return;
	}
}
// Mono.Security.Cryptography.BlockProcessor System.Security.Cryptography.HMAC::get_Block()
extern TypeInfo* BlockProcessor_t1_155_il2cpp_TypeInfo_var;
extern "C" BlockProcessor_t1_155 * HMAC_get_Block_m1_10349 (HMAC_t1_1210 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		BlockProcessor_t1_155_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(206);
		s_Il2CppMethodIntialized = true;
	}
	{
		BlockProcessor_t1_155 * L_0 = (__this->____block_8);
		if (L_0)
		{
			goto IL_0024;
		}
	}
	{
		HashAlgorithm_t1_162 * L_1 = (__this->____algo_7);
		int32_t L_2 = HMAC_get_BlockSizeValue_m1_10343(__this, /*hidden argument*/NULL);
		BlockProcessor_t1_155 * L_3 = (BlockProcessor_t1_155 *)il2cpp_codegen_object_new (BlockProcessor_t1_155_il2cpp_TypeInfo_var);
		BlockProcessor__ctor_m1_1918(L_3, L_1, ((int32_t)((int32_t)L_2>>(int32_t)3)), /*hidden argument*/NULL);
		__this->____block_8 = L_3;
	}

IL_0024:
	{
		BlockProcessor_t1_155 * L_4 = (__this->____block_8);
		return L_4;
	}
}
// System.Byte[] System.Security.Cryptography.HMAC::KeySetup(System.Byte[],System.Byte)
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern "C" ByteU5BU5D_t1_109* HMAC_KeySetup_m1_10350 (HMAC_t1_1210 * __this, ByteU5BU5D_t1_109* ___key, uint8_t ___padding, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		int32_t L_0 = HMAC_get_BlockSizeValue_m1_10343(__this, /*hidden argument*/NULL);
		V_0 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, L_0));
		V_1 = 0;
		goto IL_0020;
	}

IL_0013:
	{
		ByteU5BU5D_t1_109* L_1 = V_0;
		int32_t L_2 = V_1;
		ByteU5BU5D_t1_109* L_3 = ___key;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		uint8_t L_6 = ___padding;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_1, L_2, sizeof(uint8_t))) = (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_3, L_5, sizeof(uint8_t)))^(int32_t)L_6)))));
		int32_t L_7 = V_1;
		V_1 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_8 = V_1;
		ByteU5BU5D_t1_109* L_9 = ___key;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_9)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		ByteU5BU5D_t1_109* L_10 = ___key;
		NullCheck(L_10);
		V_2 = (((int32_t)((int32_t)(((Array_t *)L_10)->max_length))));
		goto IL_003a;
	}

IL_0032:
	{
		ByteU5BU5D_t1_109* L_11 = V_0;
		int32_t L_12 = V_2;
		uint8_t L_13 = ___padding;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_11, L_12, sizeof(uint8_t))) = (uint8_t)L_13;
		int32_t L_14 = V_2;
		V_2 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_003a:
	{
		int32_t L_15 = V_2;
		int32_t L_16 = HMAC_get_BlockSizeValue_m1_10343(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_15) < ((int32_t)L_16)))
		{
			goto IL_0032;
		}
	}
	{
		ByteU5BU5D_t1_109* L_17 = V_0;
		return L_17;
	}
}
// System.Void System.Security.Cryptography.HMAC::Dispose(System.Boolean)
extern "C" void HMAC_Dispose_m1_10351 (HMAC_t1_1210 * __this, bool ___disposing, const MethodInfo* method)
{
	{
		bool L_0 = (__this->____disposed_5);
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		bool L_1 = ___disposing;
		KeyedHashAlgorithm_Dispose_m1_10403(__this, L_1, /*hidden argument*/NULL);
	}

IL_0012:
	{
		return;
	}
}
// System.Void System.Security.Cryptography.HMAC::HashCore(System.Byte[],System.Int32,System.Int32)
extern TypeInfo* ObjectDisposedException_t1_1588_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2592;
extern "C" void HMAC_HashCore_m1_10352 (HMAC_t1_1210 * __this, ByteU5BU5D_t1_109* ___rgb, int32_t ___ib, int32_t ___cb, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectDisposedException_t1_1588_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(136);
		_stringLiteral2592 = il2cpp_codegen_string_literal_from_index(2592);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (__this->____disposed_5);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		ObjectDisposedException_t1_1588 * L_1 = (ObjectDisposedException_t1_1588 *)il2cpp_codegen_object_new (ObjectDisposedException_t1_1588_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1_14523(L_1, _stringLiteral2592, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		int32_t L_2 = (((HashAlgorithm_t1_162 *)__this)->___State_2);
		if (L_2)
		{
			goto IL_002e;
		}
	}
	{
		VirtActionInvoker0::Invoke(17 /* System.Void System.Security.Cryptography.HMAC::Initialize() */, __this);
		((HashAlgorithm_t1_162 *)__this)->___State_2 = 1;
	}

IL_002e:
	{
		BlockProcessor_t1_155 * L_3 = HMAC_get_Block_m1_10349(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_4 = ___rgb;
		int32_t L_5 = ___ib;
		int32_t L_6 = ___cb;
		NullCheck(L_3);
		BlockProcessor_Core_m1_1922(L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Byte[] System.Security.Cryptography.HMAC::HashFinal()
extern TypeInfo* ObjectDisposedException_t1_1588_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2732;
extern "C" ByteU5BU5D_t1_109* HMAC_HashFinal_m1_10353 (HMAC_t1_1210 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectDisposedException_t1_1588_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(136);
		_stringLiteral2732 = il2cpp_codegen_string_literal_from_index(2732);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	ByteU5BU5D_t1_109* V_1 = {0};
	ByteU5BU5D_t1_109* V_2 = {0};
	{
		bool L_0 = (__this->____disposed_5);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		ObjectDisposedException_t1_1588 * L_1 = (ObjectDisposedException_t1_1588 *)il2cpp_codegen_object_new (ObjectDisposedException_t1_1588_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1_14523(L_1, _stringLiteral2732, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		((HashAlgorithm_t1_162 *)__this)->___State_2 = 0;
		BlockProcessor_t1_155 * L_2 = HMAC_get_Block_m1_10349(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		BlockProcessor_Final_m1_1923(L_2, /*hidden argument*/NULL);
		HashAlgorithm_t1_162 * L_3 = (__this->____algo_7);
		NullCheck(L_3);
		ByteU5BU5D_t1_109* L_4 = (ByteU5BU5D_t1_109*)VirtFuncInvoker0< ByteU5BU5D_t1_109* >::Invoke(13 /* System.Byte[] System.Security.Cryptography.HashAlgorithm::get_Hash() */, L_3);
		V_0 = L_4;
		ByteU5BU5D_t1_109* L_5 = (ByteU5BU5D_t1_109*)VirtFuncInvoker0< ByteU5BU5D_t1_109* >::Invoke(21 /* System.Byte[] System.Security.Cryptography.HMAC::get_Key() */, __this);
		ByteU5BU5D_t1_109* L_6 = HMAC_KeySetup_m1_10350(__this, L_5, ((int32_t)92), /*hidden argument*/NULL);
		V_1 = L_6;
		HashAlgorithm_t1_162 * L_7 = (__this->____algo_7);
		NullCheck(L_7);
		VirtActionInvoker0::Invoke(17 /* System.Void System.Security.Cryptography.HashAlgorithm::Initialize() */, L_7);
		HashAlgorithm_t1_162 * L_8 = (__this->____algo_7);
		ByteU5BU5D_t1_109* L_9 = V_1;
		ByteU5BU5D_t1_109* L_10 = V_1;
		NullCheck(L_10);
		ByteU5BU5D_t1_109* L_11 = V_1;
		NullCheck(L_8);
		VirtFuncInvoker5< int32_t, ByteU5BU5D_t1_109*, int32_t, int32_t, ByteU5BU5D_t1_109*, int32_t >::Invoke(9 /* System.Int32 System.Security.Cryptography.HashAlgorithm::TransformBlock(System.Byte[],System.Int32,System.Int32,System.Byte[],System.Int32) */, L_8, L_9, 0, (((int32_t)((int32_t)(((Array_t *)L_10)->max_length)))), L_11, 0);
		HashAlgorithm_t1_162 * L_12 = (__this->____algo_7);
		ByteU5BU5D_t1_109* L_13 = V_0;
		ByteU5BU5D_t1_109* L_14 = V_0;
		NullCheck(L_14);
		NullCheck(L_12);
		VirtFuncInvoker3< ByteU5BU5D_t1_109*, ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(10 /* System.Byte[] System.Security.Cryptography.HashAlgorithm::TransformFinalBlock(System.Byte[],System.Int32,System.Int32) */, L_12, L_13, 0, (((int32_t)((int32_t)(((Array_t *)L_14)->max_length)))));
		HashAlgorithm_t1_162 * L_15 = (__this->____algo_7);
		NullCheck(L_15);
		ByteU5BU5D_t1_109* L_16 = (ByteU5BU5D_t1_109*)VirtFuncInvoker0< ByteU5BU5D_t1_109* >::Invoke(13 /* System.Byte[] System.Security.Cryptography.HashAlgorithm::get_Hash() */, L_15);
		V_2 = L_16;
		HashAlgorithm_t1_162 * L_17 = (__this->____algo_7);
		NullCheck(L_17);
		VirtActionInvoker0::Invoke(17 /* System.Void System.Security.Cryptography.HashAlgorithm::Initialize() */, L_17);
		ByteU5BU5D_t1_109* L_18 = V_1;
		ByteU5BU5D_t1_109* L_19 = V_1;
		NullCheck(L_19);
		Array_Clear_m1_1037(NULL /*static, unused*/, (Array_t *)(Array_t *)L_18, 0, (((int32_t)((int32_t)(((Array_t *)L_19)->max_length)))), /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_20 = V_0;
		ByteU5BU5D_t1_109* L_21 = V_0;
		NullCheck(L_21);
		Array_Clear_m1_1037(NULL /*static, unused*/, (Array_t *)(Array_t *)L_20, 0, (((int32_t)((int32_t)(((Array_t *)L_21)->max_length)))), /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_22 = V_2;
		return L_22;
	}
}
// System.Void System.Security.Cryptography.HMAC::Initialize()
extern TypeInfo* ObjectDisposedException_t1_1588_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2732;
extern "C" void HMAC_Initialize_m1_10354 (HMAC_t1_1210 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectDisposedException_t1_1588_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(136);
		_stringLiteral2732 = il2cpp_codegen_string_literal_from_index(2732);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	{
		bool L_0 = (__this->____disposed_5);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		ObjectDisposedException_t1_1588 * L_1 = (ObjectDisposedException_t1_1588 *)il2cpp_codegen_object_new (ObjectDisposedException_t1_1588_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1_14523(L_1, _stringLiteral2732, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		((HashAlgorithm_t1_162 *)__this)->___State_2 = 0;
		BlockProcessor_t1_155 * L_2 = HMAC_get_Block_m1_10349(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		BlockProcessor_Initialize_m1_1920(L_2, /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_3 = (ByteU5BU5D_t1_109*)VirtFuncInvoker0< ByteU5BU5D_t1_109* >::Invoke(21 /* System.Byte[] System.Security.Cryptography.HMAC::get_Key() */, __this);
		ByteU5BU5D_t1_109* L_4 = HMAC_KeySetup_m1_10350(__this, L_3, ((int32_t)54), /*hidden argument*/NULL);
		V_0 = L_4;
		HashAlgorithm_t1_162 * L_5 = (__this->____algo_7);
		NullCheck(L_5);
		VirtActionInvoker0::Invoke(17 /* System.Void System.Security.Cryptography.HashAlgorithm::Initialize() */, L_5);
		BlockProcessor_t1_155 * L_6 = HMAC_get_Block_m1_10349(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_7 = V_0;
		NullCheck(L_6);
		BlockProcessor_Core_m1_1921(L_6, L_7, /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_8 = V_0;
		ByteU5BU5D_t1_109* L_9 = V_0;
		NullCheck(L_9);
		Array_Clear_m1_1037(NULL /*static, unused*/, (Array_t *)(Array_t *)L_8, 0, (((int32_t)((int32_t)(((Array_t *)L_9)->max_length)))), /*hidden argument*/NULL);
		return;
	}
}
// System.Security.Cryptography.HMAC System.Security.Cryptography.HMAC::Create()
extern Il2CppCodeGenString* _stringLiteral2599;
extern "C" HMAC_t1_1210 * HMAC_Create_m1_10355 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral2599 = il2cpp_codegen_string_literal_from_index(2599);
		s_Il2CppMethodIntialized = true;
	}
	{
		HMAC_t1_1210 * L_0 = HMAC_Create_m1_10356(NULL /*static, unused*/, _stringLiteral2599, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Security.Cryptography.HMAC System.Security.Cryptography.HMAC::Create(System.String)
extern TypeInfo* CryptoConfig_t1_1191_il2cpp_TypeInfo_var;
extern TypeInfo* HMAC_t1_1210_il2cpp_TypeInfo_var;
extern "C" HMAC_t1_1210 * HMAC_Create_m1_10356 (Object_t * __this /* static, unused */, String_t* ___algorithmName, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CryptoConfig_t1_1191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(191);
		HMAC_t1_1210_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(878);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___algorithmName;
		IL2CPP_RUNTIME_CLASS_INIT(CryptoConfig_t1_1191_il2cpp_TypeInfo_var);
		Object_t * L_1 = CryptoConfig_CreateFromName_m1_10198(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return ((HMAC_t1_1210 *)CastclassClass(L_1, HMAC_t1_1210_il2cpp_TypeInfo_var));
	}
}
// System.Void System.Security.Cryptography.HMACMD5::.ctor()
extern "C" void HMACMD5__ctor_m1_10357 (HMACMD5_t1_1212 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t1_109* L_0 = KeyBuilder_Key_m1_1915(NULL /*static, unused*/, 8, /*hidden argument*/NULL);
		HMACMD5__ctor_m1_10358(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.Cryptography.HMACMD5::.ctor(System.Byte[])
extern Il2CppCodeGenString* _stringLiteral374;
extern "C" void HMACMD5__ctor_m1_10358 (HMACMD5_t1_1212 * __this, ByteU5BU5D_t1_109* ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral374 = il2cpp_codegen_string_literal_from_index(374);
		s_Il2CppMethodIntialized = true;
	}
	{
		HMAC__ctor_m1_10342(__this, /*hidden argument*/NULL);
		HMAC_set_HashName_m1_10346(__this, _stringLiteral374, /*hidden argument*/NULL);
		((HashAlgorithm_t1_162 *)__this)->___HashSizeValue_1 = ((int32_t)128);
		ByteU5BU5D_t1_109* L_0 = ___key;
		VirtActionInvoker1< ByteU5BU5D_t1_109* >::Invoke(22 /* System.Void System.Security.Cryptography.HMAC::set_Key(System.Byte[]) */, __this, L_0);
		return;
	}
}
// System.Void System.Security.Cryptography.HMACRIPEMD160::.ctor()
extern "C" void HMACRIPEMD160__ctor_m1_10359 (HMACRIPEMD160_t1_1213 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t1_109* L_0 = KeyBuilder_Key_m1_1915(NULL /*static, unused*/, 8, /*hidden argument*/NULL);
		HMACRIPEMD160__ctor_m1_10360(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.Cryptography.HMACRIPEMD160::.ctor(System.Byte[])
extern Il2CppCodeGenString* _stringLiteral2595;
extern "C" void HMACRIPEMD160__ctor_m1_10360 (HMACRIPEMD160_t1_1213 * __this, ByteU5BU5D_t1_109* ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral2595 = il2cpp_codegen_string_literal_from_index(2595);
		s_Il2CppMethodIntialized = true;
	}
	{
		HMAC__ctor_m1_10342(__this, /*hidden argument*/NULL);
		HMAC_set_HashName_m1_10346(__this, _stringLiteral2595, /*hidden argument*/NULL);
		((HashAlgorithm_t1_162 *)__this)->___HashSizeValue_1 = ((int32_t)160);
		ByteU5BU5D_t1_109* L_0 = ___key;
		VirtActionInvoker1< ByteU5BU5D_t1_109* >::Invoke(22 /* System.Void System.Security.Cryptography.HMAC::set_Key(System.Byte[]) */, __this, L_0);
		return;
	}
}
// System.Void System.Security.Cryptography.HMACSHA1::.ctor()
extern "C" void HMACSHA1__ctor_m1_10361 (HMACSHA1_t1_1214 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t1_109* L_0 = KeyBuilder_Key_m1_1915(NULL /*static, unused*/, 8, /*hidden argument*/NULL);
		HMACSHA1__ctor_m1_10362(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.Cryptography.HMACSHA1::.ctor(System.Byte[])
extern Il2CppCodeGenString* _stringLiteral375;
extern "C" void HMACSHA1__ctor_m1_10362 (HMACSHA1_t1_1214 * __this, ByteU5BU5D_t1_109* ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral375 = il2cpp_codegen_string_literal_from_index(375);
		s_Il2CppMethodIntialized = true;
	}
	{
		HMAC__ctor_m1_10342(__this, /*hidden argument*/NULL);
		HMAC_set_HashName_m1_10346(__this, _stringLiteral375, /*hidden argument*/NULL);
		((HashAlgorithm_t1_162 *)__this)->___HashSizeValue_1 = ((int32_t)160);
		ByteU5BU5D_t1_109* L_0 = ___key;
		VirtActionInvoker1< ByteU5BU5D_t1_109* >::Invoke(22 /* System.Void System.Security.Cryptography.HMAC::set_Key(System.Byte[]) */, __this, L_0);
		return;
	}
}
// System.Void System.Security.Cryptography.HMACSHA1::.ctor(System.Byte[],System.Boolean)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2556;
extern Il2CppCodeGenString* _stringLiteral2733;
extern Il2CppCodeGenString* _stringLiteral2734;
extern "C" void HMACSHA1__ctor_m1_10363 (HMACSHA1_t1_1214 * __this, ByteU5BU5D_t1_109* ___key, bool ___useManagedSha1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral2556 = il2cpp_codegen_string_literal_from_index(2556);
		_stringLiteral2733 = il2cpp_codegen_string_literal_from_index(2733);
		_stringLiteral2734 = il2cpp_codegen_string_literal_from_index(2734);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B2_0 = {0};
	HMACSHA1_t1_1214 * G_B2_1 = {0};
	String_t* G_B1_0 = {0};
	HMACSHA1_t1_1214 * G_B1_1 = {0};
	String_t* G_B3_0 = {0};
	String_t* G_B3_1 = {0};
	HMACSHA1_t1_1214 * G_B3_2 = {0};
	{
		HMAC__ctor_m1_10342(__this, /*hidden argument*/NULL);
		bool L_0 = ___useManagedSha1;
		G_B1_0 = _stringLiteral2556;
		G_B1_1 = __this;
		if (!L_0)
		{
			G_B2_0 = _stringLiteral2556;
			G_B2_1 = __this;
			goto IL_001c;
		}
	}
	{
		G_B3_0 = _stringLiteral2733;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_0021;
	}

IL_001c:
	{
		G_B3_0 = _stringLiteral2734;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m1_559(NULL /*static, unused*/, G_B3_1, G_B3_0, /*hidden argument*/NULL);
		NullCheck(G_B3_2);
		HMAC_set_HashName_m1_10346(G_B3_2, L_1, /*hidden argument*/NULL);
		((HashAlgorithm_t1_162 *)__this)->___HashSizeValue_1 = ((int32_t)160);
		ByteU5BU5D_t1_109* L_2 = ___key;
		VirtActionInvoker1< ByteU5BU5D_t1_109* >::Invoke(22 /* System.Void System.Security.Cryptography.HMAC::set_Key(System.Byte[]) */, __this, L_2);
		return;
	}
}
// System.Void System.Security.Cryptography.HMACSHA256::.ctor()
extern "C" void HMACSHA256__ctor_m1_10364 (HMACSHA256_t1_1215 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t1_109* L_0 = KeyBuilder_Key_m1_1915(NULL /*static, unused*/, 8, /*hidden argument*/NULL);
		HMACSHA256__ctor_m1_10365(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.Cryptography.HMACSHA256::.ctor(System.Byte[])
extern Il2CppCodeGenString* _stringLiteral699;
extern "C" void HMACSHA256__ctor_m1_10365 (HMACSHA256_t1_1215 * __this, ByteU5BU5D_t1_109* ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral699 = il2cpp_codegen_string_literal_from_index(699);
		s_Il2CppMethodIntialized = true;
	}
	{
		HMAC__ctor_m1_10342(__this, /*hidden argument*/NULL);
		HMAC_set_HashName_m1_10346(__this, _stringLiteral699, /*hidden argument*/NULL);
		((HashAlgorithm_t1_162 *)__this)->___HashSizeValue_1 = ((int32_t)256);
		ByteU5BU5D_t1_109* L_0 = ___key;
		VirtActionInvoker1< ByteU5BU5D_t1_109* >::Invoke(22 /* System.Void System.Security.Cryptography.HMAC::set_Key(System.Byte[]) */, __this, L_0);
		return;
	}
}
// System.Void System.Security.Cryptography.HMACSHA384::.ctor()
extern TypeInfo* HMACSHA384_t1_1216_il2cpp_TypeInfo_var;
extern "C" void HMACSHA384__ctor_m1_10366 (HMACSHA384_t1_1216 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HMACSHA384_t1_1216_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(879);
		s_Il2CppMethodIntialized = true;
	}
	{
		ByteU5BU5D_t1_109* L_0 = KeyBuilder_Key_m1_1915(NULL /*static, unused*/, 8, /*hidden argument*/NULL);
		HMACSHA384__ctor_m1_10367(__this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(HMACSHA384_t1_1216_il2cpp_TypeInfo_var);
		bool L_1 = ((HMACSHA384_t1_1216_StaticFields*)HMACSHA384_t1_1216_il2cpp_TypeInfo_var->static_fields)->___legacy_mode_10;
		HMACSHA384_set_ProduceLegacyHmacValues_m1_10370(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.Cryptography.HMACSHA384::.ctor(System.Byte[])
extern TypeInfo* HMACSHA384_t1_1216_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral700;
extern "C" void HMACSHA384__ctor_m1_10367 (HMACSHA384_t1_1216 * __this, ByteU5BU5D_t1_109* ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HMACSHA384_t1_1216_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(879);
		_stringLiteral700 = il2cpp_codegen_string_literal_from_index(700);
		s_Il2CppMethodIntialized = true;
	}
	{
		HMAC__ctor_m1_10342(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(HMACSHA384_t1_1216_il2cpp_TypeInfo_var);
		bool L_0 = ((HMACSHA384_t1_1216_StaticFields*)HMACSHA384_t1_1216_il2cpp_TypeInfo_var->static_fields)->___legacy_mode_10;
		HMACSHA384_set_ProduceLegacyHmacValues_m1_10370(__this, L_0, /*hidden argument*/NULL);
		HMAC_set_HashName_m1_10346(__this, _stringLiteral700, /*hidden argument*/NULL);
		((HashAlgorithm_t1_162 *)__this)->___HashSizeValue_1 = ((int32_t)384);
		ByteU5BU5D_t1_109* L_1 = ___key;
		VirtActionInvoker1< ByteU5BU5D_t1_109* >::Invoke(22 /* System.Void System.Security.Cryptography.HMAC::set_Key(System.Byte[]) */, __this, L_1);
		return;
	}
}
// System.Void System.Security.Cryptography.HMACSHA384::.cctor()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* HMACSHA384_t1_1216_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2735;
extern Il2CppCodeGenString* _stringLiteral121;
extern "C" void HMACSHA384__cctor_m1_10368 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		HMACSHA384_t1_1216_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(879);
		_stringLiteral2735 = il2cpp_codegen_string_literal_from_index(2735);
		_stringLiteral121 = il2cpp_codegen_string_literal_from_index(121);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = Environment_GetEnvironmentVariable_m1_14067(NULL /*static, unused*/, _stringLiteral2735, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_op_Equality_m1_601(NULL /*static, unused*/, L_0, _stringLiteral121, /*hidden argument*/NULL);
		((HMACSHA384_t1_1216_StaticFields*)HMACSHA384_t1_1216_il2cpp_TypeInfo_var->static_fields)->___legacy_mode_10 = L_1;
		return;
	}
}
// System.Boolean System.Security.Cryptography.HMACSHA384::get_ProduceLegacyHmacValues()
extern "C" bool HMACSHA384_get_ProduceLegacyHmacValues_m1_10369 (HMACSHA384_t1_1216 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___legacy_11);
		return L_0;
	}
}
// System.Void System.Security.Cryptography.HMACSHA384::set_ProduceLegacyHmacValues(System.Boolean)
extern "C" void HMACSHA384_set_ProduceLegacyHmacValues_m1_10370 (HMACSHA384_t1_1216 * __this, bool ___value, const MethodInfo* method)
{
	HMACSHA384_t1_1216 * G_B2_0 = {0};
	HMACSHA384_t1_1216 * G_B1_0 = {0};
	int32_t G_B3_0 = 0;
	HMACSHA384_t1_1216 * G_B3_1 = {0};
	{
		bool L_0 = ___value;
		__this->___legacy_11 = L_0;
		bool L_1 = (__this->___legacy_11);
		G_B1_0 = __this;
		if (!L_1)
		{
			G_B2_0 = __this;
			goto IL_001a;
		}
	}
	{
		G_B3_0 = ((int32_t)64);
		G_B3_1 = G_B1_0;
		goto IL_001f;
	}

IL_001a:
	{
		G_B3_0 = ((int32_t)128);
		G_B3_1 = G_B2_0;
	}

IL_001f:
	{
		NullCheck(G_B3_1);
		HMAC_set_BlockSizeValue_m1_10344(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.Cryptography.HMACSHA512::.ctor()
extern TypeInfo* HMACSHA512_t1_1217_il2cpp_TypeInfo_var;
extern "C" void HMACSHA512__ctor_m1_10371 (HMACSHA512_t1_1217 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HMACSHA512_t1_1217_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(880);
		s_Il2CppMethodIntialized = true;
	}
	{
		ByteU5BU5D_t1_109* L_0 = KeyBuilder_Key_m1_1915(NULL /*static, unused*/, 8, /*hidden argument*/NULL);
		HMACSHA512__ctor_m1_10372(__this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(HMACSHA512_t1_1217_il2cpp_TypeInfo_var);
		bool L_1 = ((HMACSHA512_t1_1217_StaticFields*)HMACSHA512_t1_1217_il2cpp_TypeInfo_var->static_fields)->___legacy_mode_10;
		HMACSHA512_set_ProduceLegacyHmacValues_m1_10375(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.Cryptography.HMACSHA512::.ctor(System.Byte[])
extern TypeInfo* HMACSHA512_t1_1217_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral701;
extern "C" void HMACSHA512__ctor_m1_10372 (HMACSHA512_t1_1217 * __this, ByteU5BU5D_t1_109* ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HMACSHA512_t1_1217_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(880);
		_stringLiteral701 = il2cpp_codegen_string_literal_from_index(701);
		s_Il2CppMethodIntialized = true;
	}
	{
		HMAC__ctor_m1_10342(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(HMACSHA512_t1_1217_il2cpp_TypeInfo_var);
		bool L_0 = ((HMACSHA512_t1_1217_StaticFields*)HMACSHA512_t1_1217_il2cpp_TypeInfo_var->static_fields)->___legacy_mode_10;
		HMACSHA512_set_ProduceLegacyHmacValues_m1_10375(__this, L_0, /*hidden argument*/NULL);
		HMAC_set_HashName_m1_10346(__this, _stringLiteral701, /*hidden argument*/NULL);
		((HashAlgorithm_t1_162 *)__this)->___HashSizeValue_1 = ((int32_t)512);
		ByteU5BU5D_t1_109* L_1 = ___key;
		VirtActionInvoker1< ByteU5BU5D_t1_109* >::Invoke(22 /* System.Void System.Security.Cryptography.HMAC::set_Key(System.Byte[]) */, __this, L_1);
		return;
	}
}
// System.Void System.Security.Cryptography.HMACSHA512::.cctor()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* HMACSHA512_t1_1217_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2735;
extern Il2CppCodeGenString* _stringLiteral121;
extern "C" void HMACSHA512__cctor_m1_10373 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		HMACSHA512_t1_1217_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(880);
		_stringLiteral2735 = il2cpp_codegen_string_literal_from_index(2735);
		_stringLiteral121 = il2cpp_codegen_string_literal_from_index(121);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = Environment_GetEnvironmentVariable_m1_14067(NULL /*static, unused*/, _stringLiteral2735, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_op_Equality_m1_601(NULL /*static, unused*/, L_0, _stringLiteral121, /*hidden argument*/NULL);
		((HMACSHA512_t1_1217_StaticFields*)HMACSHA512_t1_1217_il2cpp_TypeInfo_var->static_fields)->___legacy_mode_10 = L_1;
		return;
	}
}
// System.Boolean System.Security.Cryptography.HMACSHA512::get_ProduceLegacyHmacValues()
extern "C" bool HMACSHA512_get_ProduceLegacyHmacValues_m1_10374 (HMACSHA512_t1_1217 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___legacy_11);
		return L_0;
	}
}
// System.Void System.Security.Cryptography.HMACSHA512::set_ProduceLegacyHmacValues(System.Boolean)
extern "C" void HMACSHA512_set_ProduceLegacyHmacValues_m1_10375 (HMACSHA512_t1_1217 * __this, bool ___value, const MethodInfo* method)
{
	HMACSHA512_t1_1217 * G_B2_0 = {0};
	HMACSHA512_t1_1217 * G_B1_0 = {0};
	int32_t G_B3_0 = 0;
	HMACSHA512_t1_1217 * G_B3_1 = {0};
	{
		bool L_0 = ___value;
		__this->___legacy_11 = L_0;
		bool L_1 = (__this->___legacy_11);
		G_B1_0 = __this;
		if (!L_1)
		{
			G_B2_0 = __this;
			goto IL_001a;
		}
	}
	{
		G_B3_0 = ((int32_t)64);
		G_B3_1 = G_B1_0;
		goto IL_001f;
	}

IL_001a:
	{
		G_B3_0 = ((int32_t)128);
		G_B3_1 = G_B2_0;
	}

IL_001f:
	{
		NullCheck(G_B3_1);
		HMAC_set_BlockSizeValue_m1_10344(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Security.Cryptography.HashAlgorithm::.ctor()
extern "C" void HashAlgorithm__ctor_m1_10376 (HashAlgorithm_t1_162 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		__this->___disposed_3 = 0;
		return;
	}
}
// System.Void System.Security.Cryptography.HashAlgorithm::System.IDisposable.Dispose()
extern "C" void HashAlgorithm_System_IDisposable_Dispose_m1_10377 (HashAlgorithm_t1_162 * __this, const MethodInfo* method)
{
	{
		VirtActionInvoker1< bool >::Invoke(18 /* System.Void System.Security.Cryptography.HashAlgorithm::Dispose(System.Boolean) */, __this, 1);
		GC_SuppressFinalize_m1_14113(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.Security.Cryptography.HashAlgorithm::get_CanTransformMultipleBlocks()
extern "C" bool HashAlgorithm_get_CanTransformMultipleBlocks_m1_10378 (HashAlgorithm_t1_162 * __this, const MethodInfo* method)
{
	{
		return 1;
	}
}
// System.Boolean System.Security.Cryptography.HashAlgorithm::get_CanReuseTransform()
extern "C" bool HashAlgorithm_get_CanReuseTransform_m1_10379 (HashAlgorithm_t1_162 * __this, const MethodInfo* method)
{
	{
		return 1;
	}
}
// System.Void System.Security.Cryptography.HashAlgorithm::Clear()
extern "C" void HashAlgorithm_Clear_m1_10380 (HashAlgorithm_t1_162 * __this, const MethodInfo* method)
{
	{
		VirtActionInvoker1< bool >::Invoke(18 /* System.Void System.Security.Cryptography.HashAlgorithm::Dispose(System.Boolean) */, __this, 1);
		return;
	}
}
// System.Byte[] System.Security.Cryptography.HashAlgorithm::ComputeHash(System.Byte[])
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1219;
extern "C" ByteU5BU5D_t1_109* HashAlgorithm_ComputeHash_m1_10381 (HashAlgorithm_t1_162 * __this, ByteU5BU5D_t1_109* ___buffer, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		_stringLiteral1219 = il2cpp_codegen_string_literal_from_index(1219);
		s_Il2CppMethodIntialized = true;
	}
	{
		ByteU5BU5D_t1_109* L_0 = ___buffer;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, _stringLiteral1219, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		ByteU5BU5D_t1_109* L_2 = ___buffer;
		ByteU5BU5D_t1_109* L_3 = ___buffer;
		NullCheck(L_3);
		ByteU5BU5D_t1_109* L_4 = HashAlgorithm_ComputeHash_m1_10382(__this, L_2, 0, (((int32_t)((int32_t)(((Array_t *)L_3)->max_length)))), /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Byte[] System.Security.Cryptography.HashAlgorithm::ComputeHash(System.Byte[],System.Int32,System.Int32)
extern TypeInfo* ObjectDisposedException_t1_1588_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2736;
extern Il2CppCodeGenString* _stringLiteral1219;
extern Il2CppCodeGenString* _stringLiteral1230;
extern Il2CppCodeGenString* _stringLiteral51;
extern Il2CppCodeGenString* _stringLiteral47;
extern Il2CppCodeGenString* _stringLiteral2737;
extern Il2CppCodeGenString* _stringLiteral479;
extern "C" ByteU5BU5D_t1_109* HashAlgorithm_ComputeHash_m1_10382 (HashAlgorithm_t1_162 * __this, ByteU5BU5D_t1_109* ___buffer, int32_t ___offset, int32_t ___count, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectDisposedException_t1_1588_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(136);
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		_stringLiteral2736 = il2cpp_codegen_string_literal_from_index(2736);
		_stringLiteral1219 = il2cpp_codegen_string_literal_from_index(1219);
		_stringLiteral1230 = il2cpp_codegen_string_literal_from_index(1230);
		_stringLiteral51 = il2cpp_codegen_string_literal_from_index(51);
		_stringLiteral47 = il2cpp_codegen_string_literal_from_index(47);
		_stringLiteral2737 = il2cpp_codegen_string_literal_from_index(2737);
		_stringLiteral479 = il2cpp_codegen_string_literal_from_index(479);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (__this->___disposed_3);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		ObjectDisposedException_t1_1588 * L_1 = (ObjectDisposedException_t1_1588 *)il2cpp_codegen_object_new (ObjectDisposedException_t1_1588_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1_14523(L_1, _stringLiteral2736, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		ByteU5BU5D_t1_109* L_2 = ___buffer;
		if (L_2)
		{
			goto IL_0027;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_3 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_3, _stringLiteral1219, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0027:
	{
		int32_t L_4 = ___offset;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_003e;
		}
	}
	{
		ArgumentOutOfRangeException_t1_1501 * L_5 = (ArgumentOutOfRangeException_t1_1501 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1_13275(L_5, _stringLiteral1230, _stringLiteral51, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_5);
	}

IL_003e:
	{
		int32_t L_6 = ___count;
		if ((((int32_t)L_6) >= ((int32_t)0)))
		{
			goto IL_0055;
		}
	}
	{
		ArgumentException_t1_1425 * L_7 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13262(L_7, _stringLiteral47, _stringLiteral51, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_7);
	}

IL_0055:
	{
		int32_t L_8 = ___offset;
		ByteU5BU5D_t1_109* L_9 = ___buffer;
		NullCheck(L_9);
		int32_t L_10 = ___count;
		if ((((int32_t)L_8) <= ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Array_t *)L_9)->max_length))))-(int32_t)L_10)))))
		{
			goto IL_0075;
		}
	}
	{
		String_t* L_11 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral479, /*hidden argument*/NULL);
		ArgumentException_t1_1425 * L_12 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13262(L_12, _stringLiteral2737, L_11, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_12);
	}

IL_0075:
	{
		ByteU5BU5D_t1_109* L_13 = ___buffer;
		int32_t L_14 = ___offset;
		int32_t L_15 = ___count;
		VirtActionInvoker3< ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(14 /* System.Void System.Security.Cryptography.HashAlgorithm::HashCore(System.Byte[],System.Int32,System.Int32) */, __this, L_13, L_14, L_15);
		ByteU5BU5D_t1_109* L_16 = (ByteU5BU5D_t1_109*)VirtFuncInvoker0< ByteU5BU5D_t1_109* >::Invoke(15 /* System.Byte[] System.Security.Cryptography.HashAlgorithm::HashFinal() */, __this);
		__this->___HashValue_0 = L_16;
		VirtActionInvoker0::Invoke(17 /* System.Void System.Security.Cryptography.HashAlgorithm::Initialize() */, __this);
		ByteU5BU5D_t1_109* L_17 = (__this->___HashValue_0);
		return L_17;
	}
}
// System.Byte[] System.Security.Cryptography.HashAlgorithm::ComputeHash(System.IO.Stream)
extern TypeInfo* ObjectDisposedException_t1_1588_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2736;
extern "C" ByteU5BU5D_t1_109* HashAlgorithm_ComputeHash_m1_10383 (HashAlgorithm_t1_162 * __this, Stream_t1_405 * ___inputStream, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectDisposedException_t1_1588_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(136);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		_stringLiteral2736 = il2cpp_codegen_string_literal_from_index(2736);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	int32_t V_1 = 0;
	{
		bool L_0 = (__this->___disposed_3);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		ObjectDisposedException_t1_1588 * L_1 = (ObjectDisposedException_t1_1588 *)il2cpp_codegen_object_new (ObjectDisposedException_t1_1588_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1_14523(L_1, _stringLiteral2736, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		V_0 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, ((int32_t)4096)));
		Stream_t1_405 * L_2 = ___inputStream;
		ByteU5BU5D_t1_109* L_3 = V_0;
		NullCheck(L_2);
		int32_t L_4 = (int32_t)VirtFuncInvoker3< int32_t, ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(20 /* System.Int32 System.IO.Stream::Read(System.Byte[],System.Int32,System.Int32) */, L_2, L_3, 0, ((int32_t)4096));
		V_1 = L_4;
		goto IL_004b;
	}

IL_0034:
	{
		ByteU5BU5D_t1_109* L_5 = V_0;
		int32_t L_6 = V_1;
		VirtActionInvoker3< ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(14 /* System.Void System.Security.Cryptography.HashAlgorithm::HashCore(System.Byte[],System.Int32,System.Int32) */, __this, L_5, 0, L_6);
		Stream_t1_405 * L_7 = ___inputStream;
		ByteU5BU5D_t1_109* L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = (int32_t)VirtFuncInvoker3< int32_t, ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(20 /* System.Int32 System.IO.Stream::Read(System.Byte[],System.Int32,System.Int32) */, L_7, L_8, 0, ((int32_t)4096));
		V_1 = L_9;
	}

IL_004b:
	{
		int32_t L_10 = V_1;
		if ((((int32_t)L_10) > ((int32_t)0)))
		{
			goto IL_0034;
		}
	}
	{
		ByteU5BU5D_t1_109* L_11 = (ByteU5BU5D_t1_109*)VirtFuncInvoker0< ByteU5BU5D_t1_109* >::Invoke(15 /* System.Byte[] System.Security.Cryptography.HashAlgorithm::HashFinal() */, __this);
		__this->___HashValue_0 = L_11;
		VirtActionInvoker0::Invoke(17 /* System.Void System.Security.Cryptography.HashAlgorithm::Initialize() */, __this);
		ByteU5BU5D_t1_109* L_12 = (__this->___HashValue_0);
		return L_12;
	}
}
// System.Security.Cryptography.HashAlgorithm System.Security.Cryptography.HashAlgorithm::Create()
extern Il2CppCodeGenString* _stringLiteral2557;
extern "C" HashAlgorithm_t1_162 * HashAlgorithm_Create_m1_10384 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral2557 = il2cpp_codegen_string_literal_from_index(2557);
		s_Il2CppMethodIntialized = true;
	}
	{
		HashAlgorithm_t1_162 * L_0 = HashAlgorithm_Create_m1_10385(NULL /*static, unused*/, _stringLiteral2557, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Security.Cryptography.HashAlgorithm System.Security.Cryptography.HashAlgorithm::Create(System.String)
extern TypeInfo* CryptoConfig_t1_1191_il2cpp_TypeInfo_var;
extern TypeInfo* HashAlgorithm_t1_162_il2cpp_TypeInfo_var;
extern "C" HashAlgorithm_t1_162 * HashAlgorithm_Create_m1_10385 (Object_t * __this /* static, unused */, String_t* ___hashName, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CryptoConfig_t1_1191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(191);
		HashAlgorithm_t1_162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(881);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___hashName;
		IL2CPP_RUNTIME_CLASS_INIT(CryptoConfig_t1_1191_il2cpp_TypeInfo_var);
		Object_t * L_1 = CryptoConfig_CreateFromName_m1_10198(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return ((HashAlgorithm_t1_162 *)CastclassClass(L_1, HashAlgorithm_t1_162_il2cpp_TypeInfo_var));
	}
}
// System.Byte[] System.Security.Cryptography.HashAlgorithm::get_Hash()
extern TypeInfo* CryptographicUnexpectedOperationException_t1_1195_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2738;
extern "C" ByteU5BU5D_t1_109* HashAlgorithm_get_Hash_m1_10386 (HashAlgorithm_t1_162 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CryptographicUnexpectedOperationException_t1_1195_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(873);
		_stringLiteral2738 = il2cpp_codegen_string_literal_from_index(2738);
		s_Il2CppMethodIntialized = true;
	}
	{
		ByteU5BU5D_t1_109* L_0 = (__this->___HashValue_0);
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2738, /*hidden argument*/NULL);
		CryptographicUnexpectedOperationException_t1_1195 * L_2 = (CryptographicUnexpectedOperationException_t1_1195 *)il2cpp_codegen_object_new (CryptographicUnexpectedOperationException_t1_1195_il2cpp_TypeInfo_var);
		CryptographicUnexpectedOperationException__ctor_m1_10227(L_2, L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001b:
	{
		ByteU5BU5D_t1_109* L_3 = (__this->___HashValue_0);
		return L_3;
	}
}
// System.Int32 System.Security.Cryptography.HashAlgorithm::get_HashSize()
extern "C" int32_t HashAlgorithm_get_HashSize_m1_10387 (HashAlgorithm_t1_162 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___HashSizeValue_1);
		return L_0;
	}
}
// System.Void System.Security.Cryptography.HashAlgorithm::Dispose(System.Boolean)
extern "C" void HashAlgorithm_Dispose_m1_10388 (HashAlgorithm_t1_162 * __this, bool ___disposing, const MethodInfo* method)
{
	{
		__this->___disposed_3 = 1;
		return;
	}
}
// System.Int32 System.Security.Cryptography.HashAlgorithm::get_InputBlockSize()
extern "C" int32_t HashAlgorithm_get_InputBlockSize_m1_10389 (HashAlgorithm_t1_162 * __this, const MethodInfo* method)
{
	{
		return 1;
	}
}
// System.Int32 System.Security.Cryptography.HashAlgorithm::get_OutputBlockSize()
extern "C" int32_t HashAlgorithm_get_OutputBlockSize_m1_10390 (HashAlgorithm_t1_162 * __this, const MethodInfo* method)
{
	{
		return 1;
	}
}
// System.Int32 System.Security.Cryptography.HashAlgorithm::TransformBlock(System.Byte[],System.Int32,System.Int32,System.Byte[],System.Int32)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral476;
extern Il2CppCodeGenString* _stringLiteral477;
extern Il2CppCodeGenString* _stringLiteral51;
extern Il2CppCodeGenString* _stringLiteral478;
extern Il2CppCodeGenString* _stringLiteral482;
extern Il2CppCodeGenString* _stringLiteral2739;
extern Il2CppCodeGenString* _stringLiteral479;
extern "C" int32_t HashAlgorithm_TransformBlock_m1_10391 (HashAlgorithm_t1_162 * __this, ByteU5BU5D_t1_109* ___inputBuffer, int32_t ___inputOffset, int32_t ___inputCount, ByteU5BU5D_t1_109* ___outputBuffer, int32_t ___outputOffset, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		_stringLiteral476 = il2cpp_codegen_string_literal_from_index(476);
		_stringLiteral477 = il2cpp_codegen_string_literal_from_index(477);
		_stringLiteral51 = il2cpp_codegen_string_literal_from_index(51);
		_stringLiteral478 = il2cpp_codegen_string_literal_from_index(478);
		_stringLiteral482 = il2cpp_codegen_string_literal_from_index(482);
		_stringLiteral2739 = il2cpp_codegen_string_literal_from_index(2739);
		_stringLiteral479 = il2cpp_codegen_string_literal_from_index(479);
		s_Il2CppMethodIntialized = true;
	}
	{
		ByteU5BU5D_t1_109* L_0 = ___inputBuffer;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, _stringLiteral476, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		int32_t L_2 = ___inputOffset;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0028;
		}
	}
	{
		ArgumentOutOfRangeException_t1_1501 * L_3 = (ArgumentOutOfRangeException_t1_1501 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1_13275(L_3, _stringLiteral477, _stringLiteral51, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0028:
	{
		int32_t L_4 = ___inputCount;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_003a;
		}
	}
	{
		ArgumentException_t1_1425 * L_5 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13260(L_5, _stringLiteral478, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_5);
	}

IL_003a:
	{
		int32_t L_6 = ___inputOffset;
		if ((((int32_t)L_6) < ((int32_t)0)))
		{
			goto IL_004c;
		}
	}
	{
		int32_t L_7 = ___inputOffset;
		ByteU5BU5D_t1_109* L_8 = ___inputBuffer;
		NullCheck(L_8);
		int32_t L_9 = ___inputCount;
		if ((((int32_t)L_7) <= ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Array_t *)L_8)->max_length))))-(int32_t)L_9)))))
		{
			goto IL_0057;
		}
	}

IL_004c:
	{
		ArgumentException_t1_1425 * L_10 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13260(L_10, _stringLiteral476, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_10);
	}

IL_0057:
	{
		ByteU5BU5D_t1_109* L_11 = ___outputBuffer;
		if (!L_11)
		{
			goto IL_0098;
		}
	}
	{
		int32_t L_12 = ___outputOffset;
		if ((((int32_t)L_12) >= ((int32_t)0)))
		{
			goto IL_0076;
		}
	}
	{
		ArgumentOutOfRangeException_t1_1501 * L_13 = (ArgumentOutOfRangeException_t1_1501 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1_13275(L_13, _stringLiteral482, _stringLiteral51, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_13);
	}

IL_0076:
	{
		int32_t L_14 = ___outputOffset;
		ByteU5BU5D_t1_109* L_15 = ___outputBuffer;
		NullCheck(L_15);
		int32_t L_16 = ___inputCount;
		if ((((int32_t)L_14) <= ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Array_t *)L_15)->max_length))))-(int32_t)L_16)))))
		{
			goto IL_0098;
		}
	}
	{
		String_t* L_17 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral479, /*hidden argument*/NULL);
		ArgumentException_t1_1425 * L_18 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13262(L_18, _stringLiteral2739, L_17, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_18);
	}

IL_0098:
	{
		ByteU5BU5D_t1_109* L_19 = ___inputBuffer;
		int32_t L_20 = ___inputOffset;
		int32_t L_21 = ___inputCount;
		VirtActionInvoker3< ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(14 /* System.Void System.Security.Cryptography.HashAlgorithm::HashCore(System.Byte[],System.Int32,System.Int32) */, __this, L_19, L_20, L_21);
		ByteU5BU5D_t1_109* L_22 = ___outputBuffer;
		if (!L_22)
		{
			goto IL_00b4;
		}
	}
	{
		ByteU5BU5D_t1_109* L_23 = ___inputBuffer;
		int32_t L_24 = ___inputOffset;
		ByteU5BU5D_t1_109* L_25 = ___outputBuffer;
		int32_t L_26 = ___outputOffset;
		int32_t L_27 = ___inputCount;
		Buffer_BlockCopy_m1_13338(NULL /*static, unused*/, (Array_t *)(Array_t *)L_23, L_24, (Array_t *)(Array_t *)L_25, L_26, L_27, /*hidden argument*/NULL);
	}

IL_00b4:
	{
		int32_t L_28 = ___inputCount;
		return L_28;
	}
}
// System.Byte[] System.Security.Cryptography.HashAlgorithm::TransformFinalBlock(System.Byte[],System.Int32,System.Int32)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral476;
extern Il2CppCodeGenString* _stringLiteral478;
extern Il2CppCodeGenString* _stringLiteral2740;
extern Il2CppCodeGenString* _stringLiteral479;
extern "C" ByteU5BU5D_t1_109* HashAlgorithm_TransformFinalBlock_m1_10392 (HashAlgorithm_t1_162 * __this, ByteU5BU5D_t1_109* ___inputBuffer, int32_t ___inputOffset, int32_t ___inputCount, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		_stringLiteral476 = il2cpp_codegen_string_literal_from_index(476);
		_stringLiteral478 = il2cpp_codegen_string_literal_from_index(478);
		_stringLiteral2740 = il2cpp_codegen_string_literal_from_index(2740);
		_stringLiteral479 = il2cpp_codegen_string_literal_from_index(479);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	{
		ByteU5BU5D_t1_109* L_0 = ___inputBuffer;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, _stringLiteral476, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		int32_t L_2 = ___inputCount;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0023;
		}
	}
	{
		ArgumentException_t1_1425 * L_3 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13260(L_3, _stringLiteral478, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0023:
	{
		int32_t L_4 = ___inputOffset;
		ByteU5BU5D_t1_109* L_5 = ___inputBuffer;
		NullCheck(L_5);
		int32_t L_6 = ___inputCount;
		if ((((int32_t)L_4) <= ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Array_t *)L_5)->max_length))))-(int32_t)L_6)))))
		{
			goto IL_0043;
		}
	}
	{
		String_t* L_7 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral479, /*hidden argument*/NULL);
		ArgumentException_t1_1425 * L_8 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13262(L_8, _stringLiteral2740, L_7, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_8);
	}

IL_0043:
	{
		int32_t L_9 = ___inputCount;
		V_0 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, L_9));
		ByteU5BU5D_t1_109* L_10 = ___inputBuffer;
		int32_t L_11 = ___inputOffset;
		ByteU5BU5D_t1_109* L_12 = V_0;
		int32_t L_13 = ___inputCount;
		Buffer_BlockCopy_m1_13338(NULL /*static, unused*/, (Array_t *)(Array_t *)L_10, L_11, (Array_t *)(Array_t *)L_12, 0, L_13, /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_14 = ___inputBuffer;
		int32_t L_15 = ___inputOffset;
		int32_t L_16 = ___inputCount;
		VirtActionInvoker3< ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(14 /* System.Void System.Security.Cryptography.HashAlgorithm::HashCore(System.Byte[],System.Int32,System.Int32) */, __this, L_14, L_15, L_16);
		ByteU5BU5D_t1_109* L_17 = (ByteU5BU5D_t1_109*)VirtFuncInvoker0< ByteU5BU5D_t1_109* >::Invoke(15 /* System.Byte[] System.Security.Cryptography.HashAlgorithm::HashFinal() */, __this);
		__this->___HashValue_0 = L_17;
		VirtActionInvoker0::Invoke(17 /* System.Void System.Security.Cryptography.HashAlgorithm::Initialize() */, __this);
		ByteU5BU5D_t1_109* L_18 = V_0;
		return L_18;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
