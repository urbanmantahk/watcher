﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedList`2/<GetEnumerator>c__Iterator1<System.Int32,ExifLibrary.IFD>
struct U3CGetEnumeratorU3Ec__Iterator1_t3_288;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_21.h"

// System.Void System.Collections.Generic.SortedList`2/<GetEnumerator>c__Iterator1<System.Int32,ExifLibrary.IFD>::.ctor()
extern "C" void U3CGetEnumeratorU3Ec__Iterator1__ctor_m3_2242_gshared (U3CGetEnumeratorU3Ec__Iterator1_t3_288 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator1__ctor_m3_2242(__this, method) (( void (*) (U3CGetEnumeratorU3Ec__Iterator1_t3_288 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator1__ctor_m3_2242_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.SortedList`2/<GetEnumerator>c__Iterator1<System.Int32,ExifLibrary.IFD>::System.Collections.Generic.IEnumerator<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_Current()
extern "C" KeyValuePair_2_t1_2681  U3CGetEnumeratorU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_Current_m3_2243_gshared (U3CGetEnumeratorU3Ec__Iterator1_t3_288 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_Current_m3_2243(__this, method) (( KeyValuePair_2_t1_2681  (*) (U3CGetEnumeratorU3Ec__Iterator1_t3_288 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_Current_m3_2243_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2/<GetEnumerator>c__Iterator1<System.Int32,ExifLibrary.IFD>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CGetEnumeratorU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m3_2244_gshared (U3CGetEnumeratorU3Ec__Iterator1_t3_288 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m3_2244(__this, method) (( Object_t * (*) (U3CGetEnumeratorU3Ec__Iterator1_t3_288 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m3_2244_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedList`2/<GetEnumerator>c__Iterator1<System.Int32,ExifLibrary.IFD>::MoveNext()
extern "C" bool U3CGetEnumeratorU3Ec__Iterator1_MoveNext_m3_2245_gshared (U3CGetEnumeratorU3Ec__Iterator1_t3_288 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator1_MoveNext_m3_2245(__this, method) (( bool (*) (U3CGetEnumeratorU3Ec__Iterator1_t3_288 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator1_MoveNext_m3_2245_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/<GetEnumerator>c__Iterator1<System.Int32,ExifLibrary.IFD>::Dispose()
extern "C" void U3CGetEnumeratorU3Ec__Iterator1_Dispose_m3_2246_gshared (U3CGetEnumeratorU3Ec__Iterator1_t3_288 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator1_Dispose_m3_2246(__this, method) (( void (*) (U3CGetEnumeratorU3Ec__Iterator1_t3_288 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator1_Dispose_m3_2246_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/<GetEnumerator>c__Iterator1<System.Int32,ExifLibrary.IFD>::Reset()
extern "C" void U3CGetEnumeratorU3Ec__Iterator1_Reset_m3_2247_gshared (U3CGetEnumeratorU3Ec__Iterator1_t3_288 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator1_Reset_m3_2247(__this, method) (( void (*) (U3CGetEnumeratorU3Ec__Iterator1_t3_288 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator1_Reset_m3_2247_gshared)(__this, method)
