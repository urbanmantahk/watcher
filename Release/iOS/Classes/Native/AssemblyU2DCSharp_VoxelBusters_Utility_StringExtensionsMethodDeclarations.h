﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.String VoxelBusters.Utility.StringExtensions::GetPrintableString(System.String)
extern "C" String_t* StringExtensions_GetPrintableString_m8_224 (Object_t * __this /* static, unused */, String_t* ____string, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.Utility.StringExtensions::Contains(System.String,System.String,System.Boolean)
extern "C" bool StringExtensions_Contains_m8_225 (Object_t * __this /* static, unused */, String_t* ____string, String_t* ____stringToCheck, bool ____ignoreCase, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.Utility.StringExtensions::ToBase64(System.String)
extern "C" String_t* StringExtensions_ToBase64_m8_226 (Object_t * __this /* static, unused */, String_t* ____string, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.Utility.StringExtensions::FromBase64(System.String)
extern "C" String_t* StringExtensions_FromBase64_m8_227 (Object_t * __this /* static, unused */, String_t* ____string, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.Utility.StringExtensions::StringBetween(System.String,System.String,System.String,System.Boolean)
extern "C" String_t* StringExtensions_StringBetween_m8_228 (Object_t * __this /* static, unused */, String_t* ____string, String_t* ____startString, String_t* ____endString, bool ____ignoreCase, const MethodInfo* method) IL2CPP_METHOD_ATTR;
