﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "System_Xml_System_Xml_XmlCharacterData.h"

// System.Xml.XmlWhitespace
struct  XmlWhitespace_t4_186  : public XmlCharacterData_t4_125
{
};
