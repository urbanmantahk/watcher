﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.StencilMaterial/MatEntry
struct MatEntry_t7_126;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;
// System.Object
struct Object_t;

#include "mscorlib_System_MulticastDelegate.h"
#include "mscorlib_System_Void.h"

// System.Action`1<UnityEngine.UI.StencilMaterial/MatEntry>
struct  Action_1_t1_2593  : public MulticastDelegate_t1_21
{
};
