﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNcName
struct SoapNcName_t1_983;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNcName::.ctor()
extern "C" void SoapNcName__ctor_m1_8808 (SoapNcName_t1_983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNcName::.ctor(System.String)
extern "C" void SoapNcName__ctor_m1_8809 (SoapNcName_t1_983 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNcName::get_Value()
extern "C" String_t* SoapNcName_get_Value_m1_8810 (SoapNcName_t1_983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNcName::set_Value(System.String)
extern "C" void SoapNcName_set_Value_m1_8811 (SoapNcName_t1_983 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNcName::get_XsdType()
extern "C" String_t* SoapNcName_get_XsdType_m1_8812 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNcName::GetXsdType()
extern "C" String_t* SoapNcName_GetXsdType_m1_8813 (SoapNcName_t1_983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNcName System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNcName::Parse(System.String)
extern "C" SoapNcName_t1_983 * SoapNcName_Parse_m1_8814 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNcName::ToString()
extern "C" String_t* SoapNcName_ToString_m1_8815 (SoapNcName_t1_983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
