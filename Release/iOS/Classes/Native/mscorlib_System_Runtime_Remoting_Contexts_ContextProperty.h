﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Object
struct Object_t;

#include "mscorlib_System_Object.h"

// System.Runtime.Remoting.Contexts.ContextProperty
struct  ContextProperty_t1_898  : public Object_t
{
	// System.String System.Runtime.Remoting.Contexts.ContextProperty::name
	String_t* ___name_0;
	// System.Object System.Runtime.Remoting.Contexts.ContextProperty::prop
	Object_t * ___prop_1;
};
