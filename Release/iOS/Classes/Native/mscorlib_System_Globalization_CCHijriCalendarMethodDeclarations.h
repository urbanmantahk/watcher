﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Globalization.CCHijriCalendar
struct CCHijriCalendar_t1_350;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Globalization.CCHijriCalendar::.ctor()
extern "C" void CCHijriCalendar__ctor_m1_3768 (CCHijriCalendar_t1_350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.CCHijriCalendar::is_leap_year(System.Int32)
extern "C" bool CCHijriCalendar_is_leap_year_m1_3769 (Object_t * __this /* static, unused */, int32_t ___year, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCHijriCalendar::fixed_from_dmy(System.Int32,System.Int32,System.Int32)
extern "C" int32_t CCHijriCalendar_fixed_from_dmy_m1_3770 (Object_t * __this /* static, unused */, int32_t ___day, int32_t ___month, int32_t ___year, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCHijriCalendar::year_from_fixed(System.Int32)
extern "C" int32_t CCHijriCalendar_year_from_fixed_m1_3771 (Object_t * __this /* static, unused */, int32_t ___date, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.CCHijriCalendar::my_from_fixed(System.Int32&,System.Int32&,System.Int32)
extern "C" void CCHijriCalendar_my_from_fixed_m1_3772 (Object_t * __this /* static, unused */, int32_t* ___month, int32_t* ___year, int32_t ___date, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.CCHijriCalendar::dmy_from_fixed(System.Int32&,System.Int32&,System.Int32&,System.Int32)
extern "C" void CCHijriCalendar_dmy_from_fixed_m1_3773 (Object_t * __this /* static, unused */, int32_t* ___day, int32_t* ___month, int32_t* ___year, int32_t ___date, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCHijriCalendar::month_from_fixed(System.Int32)
extern "C" int32_t CCHijriCalendar_month_from_fixed_m1_3774 (Object_t * __this /* static, unused */, int32_t ___date, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCHijriCalendar::day_from_fixed(System.Int32)
extern "C" int32_t CCHijriCalendar_day_from_fixed_m1_3775 (Object_t * __this /* static, unused */, int32_t ___date, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCHijriCalendar::date_difference(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" int32_t CCHijriCalendar_date_difference_m1_3776 (Object_t * __this /* static, unused */, int32_t ___dayA, int32_t ___monthA, int32_t ___yearA, int32_t ___dayB, int32_t ___monthB, int32_t ___yearB, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCHijriCalendar::day_number(System.Int32,System.Int32,System.Int32)
extern "C" int32_t CCHijriCalendar_day_number_m1_3777 (Object_t * __this /* static, unused */, int32_t ___day, int32_t ___month, int32_t ___year, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCHijriCalendar::days_remaining(System.Int32,System.Int32,System.Int32)
extern "C" int32_t CCHijriCalendar_days_remaining_m1_3778 (Object_t * __this /* static, unused */, int32_t ___day, int32_t ___month, int32_t ___year, const MethodInfo* method) IL2CPP_METHOD_ATTR;
