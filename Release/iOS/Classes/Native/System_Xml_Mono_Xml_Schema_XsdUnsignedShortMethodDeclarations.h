﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Xml.Schema.XsdUnsignedShort
struct XsdUnsignedShort_t4_30;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Xml.Schema.XsdUnsignedShort::.ctor()
extern "C" void XsdUnsignedShort__ctor_m4_45 (XsdUnsignedShort_t4_30 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
