﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.IDictionary`2<System.String,Boomlagoon.JSON.JSONValue>
struct IDictionary_2_t1_1899;
// System.Text.RegularExpressions.Regex
struct Regex_t3_11;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "mscorlib_System_Object.h"

// Boomlagoon.JSON.JSONObject
struct  JSONObject_t8_5  : public Object_t
{
	// System.Collections.Generic.IDictionary`2<System.String,Boomlagoon.JSON.JSONValue> Boomlagoon.JSON.JSONObject::values
	Object_t* ___values_0;
};
struct JSONObject_t8_5_StaticFields{
	// System.Text.RegularExpressions.Regex Boomlagoon.JSON.JSONObject::unicodeRegex
	Regex_t3_11 * ___unicodeRegex_1;
	// System.Byte[] Boomlagoon.JSON.JSONObject::unicodeBytes
	ByteU5BU5D_t1_109* ___unicodeBytes_2;
};
