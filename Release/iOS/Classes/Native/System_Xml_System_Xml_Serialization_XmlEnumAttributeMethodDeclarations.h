﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.Serialization.XmlEnumAttribute
struct XmlEnumAttribute_t4_78;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Xml.Serialization.XmlEnumAttribute::.ctor(System.String)
extern "C" void XmlEnumAttribute__ctor_m4_114 (XmlEnumAttribute_t4_78 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
