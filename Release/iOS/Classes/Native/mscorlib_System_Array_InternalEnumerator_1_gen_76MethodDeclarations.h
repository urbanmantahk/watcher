﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_76.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArgument.h"

// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_16299_gshared (InternalEnumerator_1_t1_2068 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1_16299(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1_2068 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m1_16299_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_16300_gshared (InternalEnumerator_1_t1_2068 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_16300(__this, method) (( void (*) (InternalEnumerator_1_t1_2068 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_16300_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_16301_gshared (InternalEnumerator_1_t1_2068 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_16301(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1_2068 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_16301_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_16302_gshared (InternalEnumerator_1_t1_2068 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1_16302(__this, method) (( void (*) (InternalEnumerator_1_t1_2068 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1_16302_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_16303_gshared (InternalEnumerator_1_t1_2068 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1_16303(__this, method) (( bool (*) (InternalEnumerator_1_t1_2068 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1_16303_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::get_Current()
extern "C" CustomAttributeTypedArgument_t1_594  InternalEnumerator_1_get_Current_m1_16304_gshared (InternalEnumerator_1_t1_2068 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1_16304(__this, method) (( CustomAttributeTypedArgument_t1_594  (*) (InternalEnumerator_1_t1_2068 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1_16304_gshared)(__this, method)
