﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_4MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1_15788(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t1_1997 *, String_t*, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m1_15677_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>::get_Key()
#define KeyValuePair_2_get_Key_m1_15789(__this, method) (( String_t* (*) (KeyValuePair_2_t1_1997 *, const MethodInfo*))KeyValuePair_2_get_Key_m1_15678_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1_15790(__this, ___value, method) (( void (*) (KeyValuePair_2_t1_1997 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m1_15679_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>::get_Value()
#define KeyValuePair_2_get_Value_m1_15791(__this, method) (( int32_t (*) (KeyValuePair_2_t1_1997 *, const MethodInfo*))KeyValuePair_2_get_Value_m1_15680_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1_15792(__this, ___value, method) (( void (*) (KeyValuePair_2_t1_1997 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m1_15681_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>::ToString()
#define KeyValuePair_2_ToString_m1_15793(__this, method) (( String_t* (*) (KeyValuePair_2_t1_1997 *, const MethodInfo*))KeyValuePair_2_ToString_m1_15682_gshared)(__this, method)
