﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Contexts.CrossContextChannel/ContextRestoreSink
struct ContextRestoreSink_t1_899;
// System.Runtime.Remoting.Messaging.IMessageSink
struct IMessageSink_t1_874;
// System.Runtime.Remoting.Contexts.Context
struct Context_t1_891;
// System.Runtime.Remoting.Messaging.IMessage
struct IMessage_t1_875;
// System.Runtime.Remoting.Messaging.IMessageCtrl
struct IMessageCtrl_t1_920;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Contexts.CrossContextChannel/ContextRestoreSink::.ctor(System.Runtime.Remoting.Messaging.IMessageSink,System.Runtime.Remoting.Contexts.Context,System.Runtime.Remoting.Messaging.IMessage)
extern "C" void ContextRestoreSink__ctor_m1_8181 (ContextRestoreSink_t1_899 * __this, Object_t * ___next, Context_t1_891 * ___context, Object_t * ___call, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Messaging.IMessage System.Runtime.Remoting.Contexts.CrossContextChannel/ContextRestoreSink::SyncProcessMessage(System.Runtime.Remoting.Messaging.IMessage)
extern "C" Object_t * ContextRestoreSink_SyncProcessMessage_m1_8182 (ContextRestoreSink_t1_899 * __this, Object_t * ___msg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Messaging.IMessageCtrl System.Runtime.Remoting.Contexts.CrossContextChannel/ContextRestoreSink::AsyncProcessMessage(System.Runtime.Remoting.Messaging.IMessage,System.Runtime.Remoting.Messaging.IMessageSink)
extern "C" Object_t * ContextRestoreSink_AsyncProcessMessage_m1_8183 (ContextRestoreSink_t1_899 * __this, Object_t * ___msg, Object_t * ___replySink, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Messaging.IMessageSink System.Runtime.Remoting.Contexts.CrossContextChannel/ContextRestoreSink::get_NextSink()
extern "C" Object_t * ContextRestoreSink_get_NextSink_m1_8184 (ContextRestoreSink_t1_899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
