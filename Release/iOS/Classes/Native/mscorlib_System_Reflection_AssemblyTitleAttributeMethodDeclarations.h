﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.AssemblyTitleAttribute
struct AssemblyTitleAttribute_t1_582;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Reflection.AssemblyTitleAttribute::.ctor(System.String)
extern "C" void AssemblyTitleAttribute__ctor_m1_6740 (AssemblyTitleAttribute_t1_582 * __this, String_t* ___title, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.AssemblyTitleAttribute::get_Title()
extern "C" String_t* AssemblyTitleAttribute_get_Title_m1_6741 (AssemblyTitleAttribute_t1_582 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
