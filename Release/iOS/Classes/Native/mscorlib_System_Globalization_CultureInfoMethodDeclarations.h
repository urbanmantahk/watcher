﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Globalization.CultureInfo
struct CultureInfo_t1_277;
// System.String
struct String_t;
// System.Globalization.Calendar
struct Calendar_t1_338;
// System.Globalization.Calendar[]
struct CalendarU5BU5D_t1_363;
// System.Globalization.TextInfo
struct TextInfo_t1_124;
// System.Object
struct Object_t;
// System.Globalization.CultureInfo[]
struct CultureInfoU5BU5D_t1_1678;
// System.Globalization.CompareInfo
struct CompareInfo_t1_282;
// System.Globalization.NumberFormatInfo
struct NumberFormatInfo_t1_361;
// System.Globalization.DateTimeFormatInfo
struct DateTimeFormatInfo_t1_362;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Globalization_CultureTypes.h"

// System.Void System.Globalization.CultureInfo::.ctor(System.Int32)
extern "C" void CultureInfo__ctor_m1_3903 (CultureInfo_t1_277 * __this, int32_t ___culture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.CultureInfo::.ctor(System.Int32,System.Boolean)
extern "C" void CultureInfo__ctor_m1_3904 (CultureInfo_t1_277 * __this, int32_t ___culture, bool ___useUserOverride, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.CultureInfo::.ctor(System.Int32,System.Boolean,System.Boolean)
extern "C" void CultureInfo__ctor_m1_3905 (CultureInfo_t1_277 * __this, int32_t ___culture, bool ___useUserOverride, bool ___read_only, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.CultureInfo::.ctor(System.String)
extern "C" void CultureInfo__ctor_m1_3906 (CultureInfo_t1_277 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.CultureInfo::.ctor(System.String,System.Boolean)
extern "C" void CultureInfo__ctor_m1_3907 (CultureInfo_t1_277 * __this, String_t* ___name, bool ___useUserOverride, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.CultureInfo::.ctor(System.String,System.Boolean,System.Boolean)
extern "C" void CultureInfo__ctor_m1_3908 (CultureInfo_t1_277 * __this, String_t* ___name, bool ___useUserOverride, bool ___read_only, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.CultureInfo::.ctor()
extern "C" void CultureInfo__ctor_m1_3909 (CultureInfo_t1_277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.CultureInfo::.cctor()
extern "C" void CultureInfo__cctor_m1_3910 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.CultureInfo System.Globalization.CultureInfo::get_InvariantCulture()
extern "C" CultureInfo_t1_277 * CultureInfo_get_InvariantCulture_m1_3911 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.CultureInfo System.Globalization.CultureInfo::CreateSpecificCulture(System.String)
extern "C" CultureInfo_t1_277 * CultureInfo_CreateSpecificCulture_m1_3912 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.CultureInfo System.Globalization.CultureInfo::get_CurrentCulture()
extern "C" CultureInfo_t1_277 * CultureInfo_get_CurrentCulture_m1_3913 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.CultureInfo System.Globalization.CultureInfo::get_CurrentUICulture()
extern "C" CultureInfo_t1_277 * CultureInfo_get_CurrentUICulture_m1_3914 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.CultureInfo System.Globalization.CultureInfo::ConstructCurrentCulture()
extern "C" CultureInfo_t1_277 * CultureInfo_ConstructCurrentCulture_m1_3915 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.CultureInfo System.Globalization.CultureInfo::ConstructCurrentUICulture()
extern "C" CultureInfo_t1_277 * CultureInfo_ConstructCurrentUICulture_m1_3916 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.CultureInfo::get_Territory()
extern "C" String_t* CultureInfo_get_Territory_m1_3917 (CultureInfo_t1_277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CultureInfo::get_LCID()
extern "C" int32_t CultureInfo_get_LCID_m1_3918 (CultureInfo_t1_277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.CultureInfo::get_Name()
extern "C" String_t* CultureInfo_get_Name_m1_3919 (CultureInfo_t1_277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.CultureInfo::get_NativeName()
extern "C" String_t* CultureInfo_get_NativeName_m1_3920 (CultureInfo_t1_277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.Calendar System.Globalization.CultureInfo::get_Calendar()
extern "C" Calendar_t1_338 * CultureInfo_get_Calendar_m1_3921 (CultureInfo_t1_277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.Calendar[] System.Globalization.CultureInfo::get_OptionalCalendars()
extern "C" CalendarU5BU5D_t1_363* CultureInfo_get_OptionalCalendars_m1_3922 (CultureInfo_t1_277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.CultureInfo System.Globalization.CultureInfo::get_Parent()
extern "C" CultureInfo_t1_277 * CultureInfo_get_Parent_m1_3923 (CultureInfo_t1_277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.TextInfo System.Globalization.CultureInfo::get_TextInfo()
extern "C" TextInfo_t1_124 * CultureInfo_get_TextInfo_m1_3924 (CultureInfo_t1_277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.CultureInfo::get_ThreeLetterISOLanguageName()
extern "C" String_t* CultureInfo_get_ThreeLetterISOLanguageName_m1_3925 (CultureInfo_t1_277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.CultureInfo::get_ThreeLetterWindowsLanguageName()
extern "C" String_t* CultureInfo_get_ThreeLetterWindowsLanguageName_m1_3926 (CultureInfo_t1_277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.CultureInfo::get_TwoLetterISOLanguageName()
extern "C" String_t* CultureInfo_get_TwoLetterISOLanguageName_m1_3927 (CultureInfo_t1_277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.CultureInfo::get_UseUserOverride()
extern "C" bool CultureInfo_get_UseUserOverride_m1_3928 (CultureInfo_t1_277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.CultureInfo::get_IcuName()
extern "C" String_t* CultureInfo_get_IcuName_m1_3929 (CultureInfo_t1_277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.CultureInfo::ClearCachedData()
extern "C" void CultureInfo_ClearCachedData_m1_3930 (CultureInfo_t1_277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Globalization.CultureInfo::Clone()
extern "C" Object_t * CultureInfo_Clone_m1_3931 (CultureInfo_t1_277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.CultureInfo::Equals(System.Object)
extern "C" bool CultureInfo_Equals_m1_3932 (CultureInfo_t1_277 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.CultureInfo[] System.Globalization.CultureInfo::GetCultures(System.Globalization.CultureTypes)
extern "C" CultureInfoU5BU5D_t1_1678* CultureInfo_GetCultures_m1_3933 (Object_t * __this /* static, unused */, int32_t ___types, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CultureInfo::GetHashCode()
extern "C" int32_t CultureInfo_GetHashCode_m1_3934 (CultureInfo_t1_277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.CultureInfo System.Globalization.CultureInfo::ReadOnly(System.Globalization.CultureInfo)
extern "C" CultureInfo_t1_277 * CultureInfo_ReadOnly_m1_3935 (Object_t * __this /* static, unused */, CultureInfo_t1_277 * ___ci, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.CultureInfo::ToString()
extern "C" String_t* CultureInfo_ToString_m1_3936 (CultureInfo_t1_277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.CompareInfo System.Globalization.CultureInfo::get_CompareInfo()
extern "C" CompareInfo_t1_282 * CultureInfo_get_CompareInfo_m1_3937 (CultureInfo_t1_277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.CultureInfo::IsIDNeutralCulture(System.Int32)
extern "C" bool CultureInfo_IsIDNeutralCulture_m1_3938 (Object_t * __this /* static, unused */, int32_t ___lcid, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.CultureInfo::get_IsNeutralCulture()
extern "C" bool CultureInfo_get_IsNeutralCulture_m1_3939 (CultureInfo_t1_277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.CultureInfo::CheckNeutral()
extern "C" void CultureInfo_CheckNeutral_m1_3940 (CultureInfo_t1_277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.NumberFormatInfo System.Globalization.CultureInfo::get_NumberFormat()
extern "C" NumberFormatInfo_t1_361 * CultureInfo_get_NumberFormat_m1_3941 (CultureInfo_t1_277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.CultureInfo::set_NumberFormat(System.Globalization.NumberFormatInfo)
extern "C" void CultureInfo_set_NumberFormat_m1_3942 (CultureInfo_t1_277 * __this, NumberFormatInfo_t1_361 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.DateTimeFormatInfo System.Globalization.CultureInfo::get_DateTimeFormat()
extern "C" DateTimeFormatInfo_t1_362 * CultureInfo_get_DateTimeFormat_m1_3943 (CultureInfo_t1_277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.CultureInfo::set_DateTimeFormat(System.Globalization.DateTimeFormatInfo)
extern "C" void CultureInfo_set_DateTimeFormat_m1_3944 (CultureInfo_t1_277 * __this, DateTimeFormatInfo_t1_362 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.CultureInfo::get_DisplayName()
extern "C" String_t* CultureInfo_get_DisplayName_m1_3945 (CultureInfo_t1_277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.CultureInfo::get_EnglishName()
extern "C" String_t* CultureInfo_get_EnglishName_m1_3946 (CultureInfo_t1_277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.CultureInfo System.Globalization.CultureInfo::get_InstalledUICulture()
extern "C" CultureInfo_t1_277 * CultureInfo_get_InstalledUICulture_m1_3947 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.CultureInfo::get_IsReadOnly()
extern "C" bool CultureInfo_get_IsReadOnly_m1_3948 (CultureInfo_t1_277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Globalization.CultureInfo::GetFormat(System.Type)
extern "C" Object_t * CultureInfo_GetFormat_m1_3949 (CultureInfo_t1_277 * __this, Type_t * ___formatType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.CultureInfo::Construct()
extern "C" void CultureInfo_Construct_m1_3950 (CultureInfo_t1_277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.CultureInfo::ConstructInternalLocaleFromName(System.String)
extern "C" bool CultureInfo_ConstructInternalLocaleFromName_m1_3951 (CultureInfo_t1_277 * __this, String_t* ___locale, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.CultureInfo::ConstructInternalLocaleFromLcid(System.Int32)
extern "C" bool CultureInfo_ConstructInternalLocaleFromLcid_m1_3952 (CultureInfo_t1_277 * __this, int32_t ___lcid, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.CultureInfo::ConstructInternalLocaleFromSpecificName(System.Globalization.CultureInfo,System.String)
extern "C" bool CultureInfo_ConstructInternalLocaleFromSpecificName_m1_3953 (Object_t * __this /* static, unused */, CultureInfo_t1_277 * ___ci, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.CultureInfo::ConstructInternalLocaleFromCurrentLocale(System.Globalization.CultureInfo)
extern "C" bool CultureInfo_ConstructInternalLocaleFromCurrentLocale_m1_3954 (Object_t * __this /* static, unused */, CultureInfo_t1_277 * ___ci, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.CultureInfo::construct_internal_locale_from_lcid(System.Int32)
extern "C" bool CultureInfo_construct_internal_locale_from_lcid_m1_3955 (CultureInfo_t1_277 * __this, int32_t ___lcid, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.CultureInfo::construct_internal_locale_from_name(System.String)
extern "C" bool CultureInfo_construct_internal_locale_from_name_m1_3956 (CultureInfo_t1_277 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.CultureInfo::construct_internal_locale_from_specific_name(System.Globalization.CultureInfo,System.String)
extern "C" bool CultureInfo_construct_internal_locale_from_specific_name_m1_3957 (Object_t * __this /* static, unused */, CultureInfo_t1_277 * ___ci, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.CultureInfo::construct_internal_locale_from_current_locale(System.Globalization.CultureInfo)
extern "C" bool CultureInfo_construct_internal_locale_from_current_locale_m1_3958 (Object_t * __this /* static, unused */, CultureInfo_t1_277 * ___ci, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.CultureInfo[] System.Globalization.CultureInfo::internal_get_cultures(System.Boolean,System.Boolean,System.Boolean)
extern "C" CultureInfoU5BU5D_t1_1678* CultureInfo_internal_get_cultures_m1_3959 (Object_t * __this /* static, unused */, bool ___neutral, bool ___specific, bool ___installed, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.CultureInfo::construct_datetime_format()
extern "C" void CultureInfo_construct_datetime_format_m1_3960 (CultureInfo_t1_277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.CultureInfo::construct_number_format()
extern "C" void CultureInfo_construct_number_format_m1_3961 (CultureInfo_t1_277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.CultureInfo::internal_is_lcid_neutral(System.Int32,System.Boolean&)
extern "C" bool CultureInfo_internal_is_lcid_neutral_m1_3962 (Object_t * __this /* static, unused */, int32_t ___lcid, bool* ___is_neutral, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.CultureInfo::ConstructInvariant(System.Boolean)
extern "C" void CultureInfo_ConstructInvariant_m1_3963 (CultureInfo_t1_277 * __this, bool ___read_only, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.TextInfo System.Globalization.CultureInfo::CreateTextInfo(System.Boolean)
extern "C" TextInfo_t1_124 * CultureInfo_CreateTextInfo_m1_3964 (CultureInfo_t1_277 * __this, bool ___readOnly, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.CultureInfo::insert_into_shared_tables(System.Globalization.CultureInfo)
extern "C" void CultureInfo_insert_into_shared_tables_m1_3965 (Object_t * __this /* static, unused */, CultureInfo_t1_277 * ___c, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.CultureInfo System.Globalization.CultureInfo::GetCultureInfo(System.Int32)
extern "C" CultureInfo_t1_277 * CultureInfo_GetCultureInfo_m1_3966 (Object_t * __this /* static, unused */, int32_t ___culture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.CultureInfo System.Globalization.CultureInfo::GetCultureInfo(System.String)
extern "C" CultureInfo_t1_277 * CultureInfo_GetCultureInfo_m1_3967 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.CultureInfo System.Globalization.CultureInfo::GetCultureInfo(System.String,System.String)
extern "C" CultureInfo_t1_277 * CultureInfo_GetCultureInfo_m1_3968 (Object_t * __this /* static, unused */, String_t* ___name, String_t* ___altName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.CultureInfo System.Globalization.CultureInfo::GetCultureInfoByIetfLanguageTag(System.String)
extern "C" CultureInfo_t1_277 * CultureInfo_GetCultureInfoByIetfLanguageTag_m1_3969 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.CultureInfo System.Globalization.CultureInfo::CreateCulture(System.String,System.Boolean)
extern "C" CultureInfo_t1_277 * CultureInfo_CreateCulture_m1_3970 (Object_t * __this /* static, unused */, String_t* ___name, bool ___reference, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.CultureInfo::ConstructCalendars()
extern "C" void CultureInfo_ConstructCalendars_m1_3971 (CultureInfo_t1_277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
