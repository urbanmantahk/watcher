﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_TYPEDESC.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_ELEMDESC_DE.h"

// System.Runtime.InteropServices.ComTypes.ELEMDESC
struct  ELEMDESC_t1_729 
{
	// System.Runtime.InteropServices.ComTypes.TYPEDESC System.Runtime.InteropServices.ComTypes.ELEMDESC::tdesc
	TYPEDESC_t1_730  ___tdesc_0;
	// System.Runtime.InteropServices.ComTypes.ELEMDESC/DESCUNION System.Runtime.InteropServices.ComTypes.ELEMDESC::desc
	DESCUNION_t1_726  ___desc_1;
};
// Native definition for marshalling of: System.Runtime.InteropServices.ComTypes.ELEMDESC
struct ELEMDESC_t1_729_marshaled
{
	TYPEDESC_t1_730  ___tdesc_0;
	DESCUNION_t1_726_marshaled ___desc_1;
};
