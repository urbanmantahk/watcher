﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Text.Encoding/ForwardingEncoder
struct ForwardingEncoder_t1_1436;
// System.Text.Encoding
struct Encoding_t1_406;
// System.Char[]
struct CharU5BU5D_t1_16;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Text.Encoding/ForwardingEncoder::.ctor(System.Text.Encoding)
extern "C" void ForwardingEncoder__ctor_m1_12306 (ForwardingEncoder_t1_1436 * __this, Encoding_t1_406 * ___enc, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.Encoding/ForwardingEncoder::GetByteCount(System.Char[],System.Int32,System.Int32,System.Boolean)
extern "C" int32_t ForwardingEncoder_GetByteCount_m1_12307 (ForwardingEncoder_t1_1436 * __this, CharU5BU5D_t1_16* ___chars, int32_t ___index, int32_t ___count, bool ___flush, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.Encoding/ForwardingEncoder::GetBytes(System.Char[],System.Int32,System.Int32,System.Byte[],System.Int32,System.Boolean)
extern "C" int32_t ForwardingEncoder_GetBytes_m1_12308 (ForwardingEncoder_t1_1436 * __this, CharU5BU5D_t1_16* ___chars, int32_t ___charIndex, int32_t ___charCount, ByteU5BU5D_t1_109* ___bytes, int32_t ___byteCount, bool ___flush, const MethodInfo* method) IL2CPP_METHOD_ATTR;
