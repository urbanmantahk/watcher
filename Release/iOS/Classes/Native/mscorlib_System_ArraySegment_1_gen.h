﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object[]
struct ObjectU5BU5D_t1_272;

#include "mscorlib_System_ValueType.h"

// System.ArraySegment`1<System.Object>
struct  ArraySegment_1_t1_2160 
{
	// T[] System.ArraySegment`1::array
	ObjectU5BU5D_t1_272* ___array_0;
	// System.Int32 System.ArraySegment`1::offset
	int32_t ___offset_1;
	// System.Int32 System.ArraySegment`1::count
	int32_t ___count_2;
};
