﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Serialization.Formatters.InternalRM
struct InternalRM_t1_1068;
// System.Object[]
struct ObjectU5BU5D_t1_272;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Serialization.Formatters.InternalRM::.ctor()
extern "C" void InternalRM__ctor_m1_9460 (InternalRM_t1_1068 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.Formatters.InternalRM::InfoSoap(System.Object[])
extern "C" void InternalRM_InfoSoap_m1_9461 (Object_t * __this /* static, unused */, ObjectU5BU5D_t1_272* ___messages, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Serialization.Formatters.InternalRM::SoapCheckEnabled()
extern "C" bool InternalRM_SoapCheckEnabled_m1_9462 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
