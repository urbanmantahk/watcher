﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Messaging.CADMethodReturnMessage
struct CADMethodReturnMessage_t1_881;
// System.Runtime.Remoting.Messaging.IMethodReturnMessage
struct IMethodReturnMessage_t1_1718;
// System.Runtime.Remoting.Messaging.IMessage
struct IMessage_t1_875;
// System.Collections.ArrayList
struct ArrayList_t1_170;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Object
struct Object_t;
// System.Exception
struct Exception_t1_33;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Messaging.CADMethodReturnMessage::.ctor(System.Runtime.Remoting.Messaging.IMethodReturnMessage)
extern "C" void CADMethodReturnMessage__ctor_m1_8315 (CADMethodReturnMessage_t1_881 * __this, Object_t * ___retMsg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Messaging.CADMethodReturnMessage System.Runtime.Remoting.Messaging.CADMethodReturnMessage::Create(System.Runtime.Remoting.Messaging.IMessage)
extern "C" CADMethodReturnMessage_t1_881 * CADMethodReturnMessage_Create_m1_8316 (Object_t * __this /* static, unused */, Object_t * ___callMsg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList System.Runtime.Remoting.Messaging.CADMethodReturnMessage::GetArguments()
extern "C" ArrayList_t1_170 * CADMethodReturnMessage_GetArguments_m1_8317 (CADMethodReturnMessage_t1_881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Runtime.Remoting.Messaging.CADMethodReturnMessage::GetArgs(System.Collections.ArrayList)
extern "C" ObjectU5BU5D_t1_272* CADMethodReturnMessage_GetArgs_m1_8318 (CADMethodReturnMessage_t1_881 * __this, ArrayList_t1_170 * ___args, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Messaging.CADMethodReturnMessage::GetReturnValue(System.Collections.ArrayList)
extern "C" Object_t * CADMethodReturnMessage_GetReturnValue_m1_8319 (CADMethodReturnMessage_t1_881 * __this, ArrayList_t1_170 * ___args, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception System.Runtime.Remoting.Messaging.CADMethodReturnMessage::GetException(System.Collections.ArrayList)
extern "C" Exception_t1_33 * CADMethodReturnMessage_GetException_m1_8320 (CADMethodReturnMessage_t1_881 * __this, ArrayList_t1_170 * ___args, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.Remoting.Messaging.CADMethodReturnMessage::get_PropertiesCount()
extern "C" int32_t CADMethodReturnMessage_get_PropertiesCount_m1_8321 (CADMethodReturnMessage_t1_881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
