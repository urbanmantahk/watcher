﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Security_SecurityCriticalScope.h"

// System.Security.SecurityCriticalScope
struct  SecurityCriticalScope_t1_1397 
{
	// System.Int32 System.Security.SecurityCriticalScope::value__
	int32_t ___value___1;
};
