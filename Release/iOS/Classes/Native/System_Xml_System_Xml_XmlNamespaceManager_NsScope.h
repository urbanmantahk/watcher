﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_ValueType.h"

// System.Xml.XmlNamespaceManager/NsScope
struct  NsScope_t4_142 
{
	// System.Int32 System.Xml.XmlNamespaceManager/NsScope::DeclCount
	int32_t ___DeclCount_0;
	// System.String System.Xml.XmlNamespaceManager/NsScope::DefaultNamespace
	String_t* ___DefaultNamespace_1;
};
// Native definition for marshalling of: System.Xml.XmlNamespaceManager/NsScope
struct NsScope_t4_142_marshaled
{
	int32_t ___DeclCount_0;
	char* ___DefaultNamespace_1;
};
