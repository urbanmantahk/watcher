﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ExifLibrary.MathEx/Fraction32[]
struct Fraction32U5BU5D_t8_129;

#include "AssemblyU2DCSharp_ExifLibrary_ExifProperty.h"

// ExifLibrary.ExifSRationalArray
struct  ExifSRationalArray_t8_128  : public ExifProperty_t8_99
{
	// ExifLibrary.MathEx/Fraction32[] ExifLibrary.ExifSRationalArray::mValue
	Fraction32U5BU5D_t8_129* ___mValue_3;
};
