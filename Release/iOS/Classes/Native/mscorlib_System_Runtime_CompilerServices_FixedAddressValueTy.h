﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Attribute.h"

// System.Runtime.CompilerServices.FixedAddressValueTypeAttribute
struct  FixedAddressValueTypeAttribute_t1_686  : public Attribute_t1_2
{
};
