﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.MediaLibrary/<SaveScreenshotToGallery>c__AnonStoreyC
struct U3CSaveScreenshotToGalleryU3Ec__AnonStoreyC_t8_246;
// UnityEngine.Texture2D
struct Texture2D_t6_33;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.MediaLibrary/<SaveScreenshotToGallery>c__AnonStoreyC::.ctor()
extern "C" void U3CSaveScreenshotToGalleryU3Ec__AnonStoreyC__ctor_m8_1365 (U3CSaveScreenshotToGalleryU3Ec__AnonStoreyC_t8_246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.MediaLibrary/<SaveScreenshotToGallery>c__AnonStoreyC::<>m__D(UnityEngine.Texture2D)
extern "C" void U3CSaveScreenshotToGalleryU3Ec__AnonStoreyC_U3CU3Em__D_m8_1366 (U3CSaveScreenshotToGalleryU3Ec__AnonStoreyC_t8_246 * __this, Texture2D_t6_33 * ____texture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
