﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.Schema.XmlSchemaSimpleType
struct XmlSchemaSimpleType_t4_63;
// System.String
struct String_t;
// System.Xml.Schema.XmlSchemaSimpleTypeContent
struct XmlSchemaSimpleTypeContent_t4_69;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Xml.Schema.XmlSchemaSimpleType::.ctor()
extern "C" void XmlSchemaSimpleType__ctor_m4_96 (XmlSchemaSimpleType_t4_63 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlSchemaSimpleType::.cctor()
extern "C" void XmlSchemaSimpleType__cctor_m4_97 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::BuildSchemaType(System.String,System.String)
extern "C" XmlSchemaSimpleType_t4_63 * XmlSchemaSimpleType_BuildSchemaType_m4_98 (Object_t * __this /* static, unused */, String_t* ___name, String_t* ___baseName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::BuildSchemaType(System.String,System.String,System.Boolean,System.Boolean)
extern "C" XmlSchemaSimpleType_t4_63 * XmlSchemaSimpleType_BuildSchemaType_m4_99 (Object_t * __this /* static, unused */, String_t* ___name, String_t* ___baseName, bool ___xdt, bool ___baseXdt, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlSchemaSimpleType::set_Content(System.Xml.Schema.XmlSchemaSimpleTypeContent)
extern "C" void XmlSchemaSimpleType_set_Content_m4_100 (XmlSchemaSimpleType_t4_63 * __this, XmlSchemaSimpleTypeContent_t4_69 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
