﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Threading_ThreadPriority.h"

// System.Threading.ThreadPriority
struct  ThreadPriority_t1_1481 
{
	// System.Int32 System.Threading.ThreadPriority::value__
	int32_t ___value___1;
};
