﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapIdrefs
struct SoapIdrefs_t1_977;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapIdrefs::.ctor()
extern "C" void SoapIdrefs__ctor_m1_8758 (SoapIdrefs_t1_977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapIdrefs::.ctor(System.String)
extern "C" void SoapIdrefs__ctor_m1_8759 (SoapIdrefs_t1_977 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapIdrefs::get_Value()
extern "C" String_t* SoapIdrefs_get_Value_m1_8760 (SoapIdrefs_t1_977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapIdrefs::set_Value(System.String)
extern "C" void SoapIdrefs_set_Value_m1_8761 (SoapIdrefs_t1_977 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapIdrefs::get_XsdType()
extern "C" String_t* SoapIdrefs_get_XsdType_m1_8762 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapIdrefs::GetXsdType()
extern "C" String_t* SoapIdrefs_GetXsdType_m1_8763 (SoapIdrefs_t1_977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapIdrefs System.Runtime.Remoting.Metadata.W3cXsd2001.SoapIdrefs::Parse(System.String)
extern "C" SoapIdrefs_t1_977 * SoapIdrefs_Parse_m1_8764 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapIdrefs::ToString()
extern "C" String_t* SoapIdrefs_ToString_m1_8765 (SoapIdrefs_t1_977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
