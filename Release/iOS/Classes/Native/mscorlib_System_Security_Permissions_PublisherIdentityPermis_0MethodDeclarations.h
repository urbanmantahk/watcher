﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Permissions.PublisherIdentityPermissionAttribute
struct PublisherIdentityPermissionAttribute_t1_1300;
// System.String
struct String_t;
// System.Security.IPermission
struct IPermission_t1_1400;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Permissions_SecurityAction.h"

// System.Void System.Security.Permissions.PublisherIdentityPermissionAttribute::.ctor(System.Security.Permissions.SecurityAction)
extern "C" void PublisherIdentityPermissionAttribute__ctor_m1_11075 (PublisherIdentityPermissionAttribute_t1_1300 * __this, int32_t ___action, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Permissions.PublisherIdentityPermissionAttribute::get_CertFile()
extern "C" String_t* PublisherIdentityPermissionAttribute_get_CertFile_m1_11076 (PublisherIdentityPermissionAttribute_t1_1300 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.PublisherIdentityPermissionAttribute::set_CertFile(System.String)
extern "C" void PublisherIdentityPermissionAttribute_set_CertFile_m1_11077 (PublisherIdentityPermissionAttribute_t1_1300 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Permissions.PublisherIdentityPermissionAttribute::get_SignedFile()
extern "C" String_t* PublisherIdentityPermissionAttribute_get_SignedFile_m1_11078 (PublisherIdentityPermissionAttribute_t1_1300 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.PublisherIdentityPermissionAttribute::set_SignedFile(System.String)
extern "C" void PublisherIdentityPermissionAttribute_set_SignedFile_m1_11079 (PublisherIdentityPermissionAttribute_t1_1300 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Permissions.PublisherIdentityPermissionAttribute::get_X509Certificate()
extern "C" String_t* PublisherIdentityPermissionAttribute_get_X509Certificate_m1_11080 (PublisherIdentityPermissionAttribute_t1_1300 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.PublisherIdentityPermissionAttribute::set_X509Certificate(System.String)
extern "C" void PublisherIdentityPermissionAttribute_set_X509Certificate_m1_11081 (PublisherIdentityPermissionAttribute_t1_1300 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.PublisherIdentityPermissionAttribute::CreatePermission()
extern "C" Object_t * PublisherIdentityPermissionAttribute_CreatePermission_m1_11082 (PublisherIdentityPermissionAttribute_t1_1300 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
