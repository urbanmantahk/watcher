﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.Achievement
struct Achievement_t8_221;
// System.String
struct String_t;
// VoxelBusters.NativePlugins.Achievement/ReportProgressCompletion
struct ReportProgressCompletion_t8_220;
// VoxelBusters.NativePlugins.AchievementDescription
struct AchievementDescription_t8_225;
// System.Collections.IDictionary
struct IDictionary_t1_35;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime.h"

// System.Void VoxelBusters.NativePlugins.Achievement::.ctor()
extern "C" void Achievement__ctor_m8_1250 (Achievement_t8_221 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Achievement::.ctor(System.String,System.String,System.Int32,System.Boolean,System.DateTime)
extern "C" void Achievement__ctor_m8_1251 (Achievement_t8_221 * __this, String_t* ____globalIdentifier, String_t* ____identifier, int32_t ____pointsScored, bool ____completed, DateTime_t1_150  ____reportedDate, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Achievement::.ctor(System.String,System.String,System.Int32)
extern "C" void Achievement__ctor_m8_1252 (Achievement_t8_221 * __this, String_t* ____globalIdentifier, String_t* ____identifier, int32_t ____pointsScored, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Achievement::add_ReportProgressFinishedEvent(VoxelBusters.NativePlugins.Achievement/ReportProgressCompletion)
extern "C" void Achievement_add_ReportProgressFinishedEvent_m8_1253 (Achievement_t8_221 * __this, ReportProgressCompletion_t8_220 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Achievement::remove_ReportProgressFinishedEvent(VoxelBusters.NativePlugins.Achievement/ReportProgressCompletion)
extern "C" void Achievement_remove_ReportProgressFinishedEvent_m8_1254 (Achievement_t8_221 * __this, ReportProgressCompletion_t8_220 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.Achievement::get_GlobalIdentifier()
extern "C" String_t* Achievement_get_GlobalIdentifier_m8_1255 (Achievement_t8_221 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Achievement::set_GlobalIdentifier(System.String)
extern "C" void Achievement_set_GlobalIdentifier_m8_1256 (Achievement_t8_221 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double VoxelBusters.NativePlugins.Achievement::get_PercentageCompleted()
extern "C" double Achievement_get_PercentageCompleted_m8_1257 (Achievement_t8_221 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.NativePlugins.AchievementDescription VoxelBusters.NativePlugins.Achievement::get_Description()
extern "C" AchievementDescription_t8_225 * Achievement_get_Description_m8_1258 (Achievement_t8_221 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Achievement::ReportProgress(VoxelBusters.NativePlugins.Achievement/ReportProgressCompletion)
extern "C" void Achievement_ReportProgress_m8_1259 (Achievement_t8_221 * __this, ReportProgressCompletion_t8_220 * ____onCompletion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.Achievement::ToString()
extern "C" String_t* Achievement_ToString_m8_1260 (Achievement_t8_221 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Achievement::ReportProgressFinished(System.Collections.IDictionary)
extern "C" void Achievement_ReportProgressFinished_m8_1261 (Achievement_t8_221 * __this, Object_t * ____dataDict, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Achievement::ReportProgressFinished(System.Boolean,System.String)
extern "C" void Achievement_ReportProgressFinished_m8_1262 (Achievement_t8_221 * __this, bool ____success, String_t* ____error, const MethodInfo* method) IL2CPP_METHOD_ATTR;
