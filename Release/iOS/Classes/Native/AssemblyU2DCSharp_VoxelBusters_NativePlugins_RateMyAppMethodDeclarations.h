﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.RateMyApp
struct RateMyApp_t8_307;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.RateMyApp::.ctor()
extern "C" void RateMyApp__ctor_m8_1950 (RateMyApp_t8_307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.RateMyApp::Awake()
extern "C" void RateMyApp_Awake_m8_1951 (RateMyApp_t8_307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.RateMyApp::Start()
extern "C" void RateMyApp_Start_m8_1952 (RateMyApp_t8_307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.RateMyApp::OnApplicationPause(System.Boolean)
extern "C" void RateMyApp_OnApplicationPause_m8_1953 (RateMyApp_t8_307 * __this, bool ____pauseStatus, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.RateMyApp::TrackApplicationUsage()
extern "C" void RateMyApp_TrackApplicationUsage_m8_1954 (RateMyApp_t8_307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 VoxelBusters.NativePlugins.RateMyApp::GetAppUsageCount()
extern "C" int32_t RateMyApp_GetAppUsageCount_m8_1955 (RateMyApp_t8_307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.RateMyApp::AskForReview()
extern "C" void RateMyApp_AskForReview_m8_1956 (RateMyApp_t8_307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.RateMyApp::AskForReviewNow()
extern "C" void RateMyApp_AskForReviewNow_m8_1957 (RateMyApp_t8_307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.NativePlugins.RateMyApp::CanAskForReview()
extern "C" bool RateMyApp_CanAskForReview_m8_1958 (RateMyApp_t8_307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.NativePlugins.RateMyApp::IsEligibleToShowPrompt(System.Int32)
extern "C" bool RateMyApp_IsEligibleToShowPrompt_m8_1959 (RateMyApp_t8_307 * __this, int32_t ____appUsageCountUntilNow, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.RateMyApp::ShowRateMeDialog()
extern "C" void RateMyApp_ShowRateMeDialog_m8_1960 (RateMyApp_t8_307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.RateMyApp::OnPressingRemingMeLater()
extern "C" void RateMyApp_OnPressingRemingMeLater_m8_1961 (RateMyApp_t8_307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.RateMyApp::OnPressingRateMyApp()
extern "C" void RateMyApp_OnPressingRateMyApp_m8_1962 (RateMyApp_t8_307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.RateMyApp::OnPressingDontShow()
extern "C" void RateMyApp_OnPressingDontShow_m8_1963 (RateMyApp_t8_307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.RateMyApp::<ShowRateMeDialog>m__15(System.String)
extern "C" void RateMyApp_U3CShowRateMeDialogU3Em__15_m8_1964 (RateMyApp_t8_307 * __this, String_t* ____buttonName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
