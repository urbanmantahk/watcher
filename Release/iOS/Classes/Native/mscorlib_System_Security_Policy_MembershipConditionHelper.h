﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object.h"

// System.Security.Policy.MembershipConditionHelper
struct  MembershipConditionHelper_t1_1352  : public Object_t
{
};
struct MembershipConditionHelper_t1_1352_StaticFields{
	// System.String System.Security.Policy.MembershipConditionHelper::XmlTag
	String_t* ___XmlTag_0;
};
