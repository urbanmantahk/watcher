﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Reflection.Emit.TypeBuilder
struct TypeBuilder_t1_481;
// System.Type[]
struct TypeU5BU5D_t1_31;
// System.Collections.Hashtable
struct Hashtable_t1_100;

#include "mscorlib_System_MonoType.h"
#include "mscorlib_System_Reflection_BindingFlags.h"

// System.Reflection.MonoGenericClass
struct  MonoGenericClass_t1_485  : public MonoType_t
{
	// System.Reflection.Emit.TypeBuilder System.Reflection.MonoGenericClass::generic_type
	TypeBuilder_t1_481 * ___generic_type_10;
	// System.Type[] System.Reflection.MonoGenericClass::type_arguments
	TypeU5BU5D_t1_31* ___type_arguments_11;
	// System.Boolean System.Reflection.MonoGenericClass::initialized
	bool ___initialized_12;
	// System.Collections.Hashtable System.Reflection.MonoGenericClass::fields
	Hashtable_t1_100 * ___fields_13;
	// System.Collections.Hashtable System.Reflection.MonoGenericClass::ctors
	Hashtable_t1_100 * ___ctors_14;
	// System.Collections.Hashtable System.Reflection.MonoGenericClass::methods
	Hashtable_t1_100 * ___methods_15;
	// System.Int32 System.Reflection.MonoGenericClass::event_count
	int32_t ___event_count_16;
};
