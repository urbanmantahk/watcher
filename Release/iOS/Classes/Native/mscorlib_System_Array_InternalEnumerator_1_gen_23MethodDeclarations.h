﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_23.h"
#include "mscorlib_System_RuntimeTypeHandle.h"

// System.Void System.Array/InternalEnumerator`1<System.RuntimeTypeHandle>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_15554_gshared (InternalEnumerator_1_t1_1974 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1_15554(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1_1974 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m1_15554_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.RuntimeTypeHandle>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_15555_gshared (InternalEnumerator_1_t1_1974 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_15555(__this, method) (( void (*) (InternalEnumerator_1_t1_1974 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_15555_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.RuntimeTypeHandle>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_15556_gshared (InternalEnumerator_1_t1_1974 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_15556(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1_1974 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_15556_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.RuntimeTypeHandle>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_15557_gshared (InternalEnumerator_1_t1_1974 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1_15557(__this, method) (( void (*) (InternalEnumerator_1_t1_1974 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1_15557_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.RuntimeTypeHandle>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_15558_gshared (InternalEnumerator_1_t1_1974 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1_15558(__this, method) (( bool (*) (InternalEnumerator_1_t1_1974 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1_15558_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.RuntimeTypeHandle>::get_Current()
extern "C" RuntimeTypeHandle_t1_30  InternalEnumerator_1_get_Current_m1_15559_gshared (InternalEnumerator_1_t1_1974 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1_15559(__this, method) (( RuntimeTypeHandle_t1_30  (*) (InternalEnumerator_1_t1_1974 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1_15559_gshared)(__this, method)
