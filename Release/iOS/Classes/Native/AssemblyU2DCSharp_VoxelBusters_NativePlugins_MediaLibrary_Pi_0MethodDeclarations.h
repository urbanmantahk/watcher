﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.MediaLibrary/PickVideoCompletion
struct PickVideoCompletion_t8_242;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_ePickVideoFinis.h"

// System.Void VoxelBusters.NativePlugins.MediaLibrary/PickVideoCompletion::.ctor(System.Object,System.IntPtr)
extern "C" void PickVideoCompletion__ctor_m8_1355 (PickVideoCompletion_t8_242 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.MediaLibrary/PickVideoCompletion::Invoke(VoxelBusters.NativePlugins.ePickVideoFinishReason)
extern "C" void PickVideoCompletion_Invoke_m8_1356 (PickVideoCompletion_t8_242 * __this, int32_t ____reason, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_PickVideoCompletion_t8_242(Il2CppObject* delegate, int32_t ____reason);
// System.IAsyncResult VoxelBusters.NativePlugins.MediaLibrary/PickVideoCompletion::BeginInvoke(VoxelBusters.NativePlugins.ePickVideoFinishReason,System.AsyncCallback,System.Object)
extern "C" Object_t * PickVideoCompletion_BeginInvoke_m8_1357 (PickVideoCompletion_t8_242 * __this, int32_t ____reason, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.MediaLibrary/PickVideoCompletion::EndInvoke(System.IAsyncResult)
extern "C" void PickVideoCompletion_EndInvoke_m8_1358 (PickVideoCompletion_t8_242 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
