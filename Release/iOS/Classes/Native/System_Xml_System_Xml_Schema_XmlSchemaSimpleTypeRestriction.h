﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleTypeContent.h"
#include "mscorlib_System_Globalization_NumberStyles.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaFacet_Facet.h"

// System.Xml.Schema.XmlSchemaSimpleTypeRestriction
struct  XmlSchemaSimpleTypeRestriction_t4_72  : public XmlSchemaSimpleTypeContent_t4_69
{
};
struct XmlSchemaSimpleTypeRestriction_t4_72_StaticFields{
	// System.Globalization.NumberStyles System.Xml.Schema.XmlSchemaSimpleTypeRestriction::lengthStyle
	int32_t ___lengthStyle_3;
	// System.Xml.Schema.XmlSchemaFacet/Facet System.Xml.Schema.XmlSchemaSimpleTypeRestriction::listFacets
	int32_t ___listFacets_4;
};
