﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t1_31;
// System.Reflection.Emit.MethodBuilder[]
struct MethodBuilderU5BU5D_t1_502;
// System.Reflection.Emit.ConstructorBuilder[]
struct ConstructorBuilderU5BU5D_t1_555;
// System.Reflection.Emit.PropertyBuilder[]
struct PropertyBuilderU5BU5D_t1_556;
// System.Reflection.Emit.FieldBuilder[]
struct FieldBuilderU5BU5D_t1_534;
// System.Reflection.Emit.EventBuilder[]
struct EventBuilderU5BU5D_t1_557;
// System.Reflection.Emit.CustomAttributeBuilder[]
struct CustomAttributeBuilderU5BU5D_t1_469;
// System.Reflection.Emit.TypeBuilder[]
struct TypeBuilderU5BU5D_t1_533;
// System.Reflection.Emit.ModuleBuilder
struct ModuleBuilder_t1_475;
// System.Reflection.Emit.GenericTypeParameterBuilder[]
struct GenericTypeParameterBuilderU5BU5D_t1_529;
// System.Reflection.Emit.RefEmitPermissionSet[]
struct RefEmitPermissionSetU5BU5D_t1_473;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t1_92;

#include "mscorlib_System_Type.h"
#include "mscorlib_System_Reflection_TypeAttributes.h"
#include "mscorlib_System_Reflection_Emit_PackingSize.h"
#include "mscorlib_System_IntPtr.h"

// System.Reflection.Emit.TypeBuilder
struct  TypeBuilder_t1_481  : public Type_t
{
	// System.String System.Reflection.Emit.TypeBuilder::tname
	String_t* ___tname_9;
	// System.String System.Reflection.Emit.TypeBuilder::nspace
	String_t* ___nspace_10;
	// System.Type System.Reflection.Emit.TypeBuilder::parent
	Type_t * ___parent_11;
	// System.Type System.Reflection.Emit.TypeBuilder::nesting_type
	Type_t * ___nesting_type_12;
	// System.Type[] System.Reflection.Emit.TypeBuilder::interfaces
	TypeU5BU5D_t1_31* ___interfaces_13;
	// System.Int32 System.Reflection.Emit.TypeBuilder::num_methods
	int32_t ___num_methods_14;
	// System.Reflection.Emit.MethodBuilder[] System.Reflection.Emit.TypeBuilder::methods
	MethodBuilderU5BU5D_t1_502* ___methods_15;
	// System.Reflection.Emit.ConstructorBuilder[] System.Reflection.Emit.TypeBuilder::ctors
	ConstructorBuilderU5BU5D_t1_555* ___ctors_16;
	// System.Reflection.Emit.PropertyBuilder[] System.Reflection.Emit.TypeBuilder::properties
	PropertyBuilderU5BU5D_t1_556* ___properties_17;
	// System.Int32 System.Reflection.Emit.TypeBuilder::num_fields
	int32_t ___num_fields_18;
	// System.Reflection.Emit.FieldBuilder[] System.Reflection.Emit.TypeBuilder::fields
	FieldBuilderU5BU5D_t1_534* ___fields_19;
	// System.Reflection.Emit.EventBuilder[] System.Reflection.Emit.TypeBuilder::events
	EventBuilderU5BU5D_t1_557* ___events_20;
	// System.Reflection.Emit.CustomAttributeBuilder[] System.Reflection.Emit.TypeBuilder::cattrs
	CustomAttributeBuilderU5BU5D_t1_469* ___cattrs_21;
	// System.Reflection.Emit.TypeBuilder[] System.Reflection.Emit.TypeBuilder::subtypes
	TypeBuilderU5BU5D_t1_533* ___subtypes_22;
	// System.Reflection.TypeAttributes System.Reflection.Emit.TypeBuilder::attrs
	int32_t ___attrs_23;
	// System.Int32 System.Reflection.Emit.TypeBuilder::table_idx
	int32_t ___table_idx_24;
	// System.Reflection.Emit.ModuleBuilder System.Reflection.Emit.TypeBuilder::pmodule
	ModuleBuilder_t1_475 * ___pmodule_25;
	// System.Int32 System.Reflection.Emit.TypeBuilder::class_size
	int32_t ___class_size_26;
	// System.Reflection.Emit.PackingSize System.Reflection.Emit.TypeBuilder::packing_size
	int32_t ___packing_size_27;
	// System.IntPtr System.Reflection.Emit.TypeBuilder::generic_container
	IntPtr_t ___generic_container_28;
	// System.Reflection.Emit.GenericTypeParameterBuilder[] System.Reflection.Emit.TypeBuilder::generic_params
	GenericTypeParameterBuilderU5BU5D_t1_529* ___generic_params_29;
	// System.Reflection.Emit.RefEmitPermissionSet[] System.Reflection.Emit.TypeBuilder::permissions
	RefEmitPermissionSetU5BU5D_t1_473* ___permissions_30;
	// System.Type System.Reflection.Emit.TypeBuilder::created
	Type_t * ___created_31;
	// System.String System.Reflection.Emit.TypeBuilder::fullname
	String_t* ___fullname_32;
	// System.Boolean System.Reflection.Emit.TypeBuilder::createTypeCalled
	bool ___createTypeCalled_33;
	// System.Type System.Reflection.Emit.TypeBuilder::underlying_type
	Type_t * ___underlying_type_34;
};
struct TypeBuilder_t1_481_StaticFields{
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Reflection.Emit.TypeBuilder::<>f__switch$map1E
	Dictionary_2_t1_92 * ___U3CU3Ef__switchU24map1E_35;
};
