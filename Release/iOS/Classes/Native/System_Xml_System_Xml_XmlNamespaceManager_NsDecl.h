﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_ValueType.h"

// System.Xml.XmlNamespaceManager/NsDecl
struct  NsDecl_t4_141 
{
	// System.String System.Xml.XmlNamespaceManager/NsDecl::Prefix
	String_t* ___Prefix_0;
	// System.String System.Xml.XmlNamespaceManager/NsDecl::Uri
	String_t* ___Uri_1;
};
// Native definition for marshalling of: System.Xml.XmlNamespaceManager/NsDecl
struct NsDecl_t4_141_marshaled
{
	char* ___Prefix_0;
	char* ___Uri_1;
};
