﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.AssetStoreProductUtility.Demo.DemoSubMenu
struct DemoSubMenu_t8_18;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.AssetStoreProductUtility.Demo.DemoSubMenu::.ctor()
extern "C" void DemoSubMenu__ctor_m8_130 (DemoSubMenu_t8_18 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.AssetStoreProductUtility.Demo.DemoSubMenu::Start()
extern "C" void DemoSubMenu_Start_m8_131 (DemoSubMenu_t8_18 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.AssetStoreProductUtility.Demo.DemoSubMenu::DrawPopButton(System.String)
extern "C" void DemoSubMenu_DrawPopButton_m8_132 (DemoSubMenu_t8_18 * __this, String_t* ____popTitle, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.AssetStoreProductUtility.Demo.DemoSubMenu::OnGUIWindow()
extern "C" void DemoSubMenu_OnGUIWindow_m8_133 (DemoSubMenu_t8_18 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.AssetStoreProductUtility.Demo.DemoSubMenu::AppendResult(System.String)
extern "C" void DemoSubMenu_AppendResult_m8_134 (DemoSubMenu_t8_18 * __this, String_t* ____result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.AssetStoreProductUtility.Demo.DemoSubMenu::AddNewResult(System.String)
extern "C" void DemoSubMenu_AddNewResult_m8_135 (DemoSubMenu_t8_18 * __this, String_t* ____result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.AssetStoreProductUtility.Demo.DemoSubMenu::DrawResults()
extern "C" void DemoSubMenu_DrawResults_m8_136 (DemoSubMenu_t8_18 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
