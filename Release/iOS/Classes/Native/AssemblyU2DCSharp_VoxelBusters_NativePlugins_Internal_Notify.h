﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_VoxelBusters_Utility_ExecuteOnValueChangeA.h"

// VoxelBusters.NativePlugins.Internal.NotifyNPSettingsOnValueChangeAttribute
struct  NotifyNPSettingsOnValueChangeAttribute_t8_321  : public ExecuteOnValueChangeAttribute_t8_148
{
};
