﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "System_Xml_System_Xml_XmlTextWriter_XmlDeclState.h"

// System.Xml.XmlTextWriter/XmlDeclState
struct  XmlDeclState_t4_180 
{
	// System.Int32 System.Xml.XmlTextWriter/XmlDeclState::value__
	int32_t ___value___1;
};
