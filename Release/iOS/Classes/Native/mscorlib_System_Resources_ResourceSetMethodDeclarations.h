﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Resources.ResourceSet
struct ResourceSet_t1_655;
// System.Resources.IResourceReader
struct IResourceReader_t1_656;
// System.IO.Stream
struct Stream_t1_405;
// System.IO.UnmanagedMemoryStream
struct UnmanagedMemoryStream_t1_460;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Type
struct Type_t;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1_863;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Resources.ResourceSet::.ctor()
extern "C" void ResourceSet__ctor_m1_7422 (ResourceSet_t1_655 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.ResourceSet::.ctor(System.Resources.IResourceReader)
extern "C" void ResourceSet__ctor_m1_7423 (ResourceSet_t1_655 * __this, Object_t * ___reader, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.ResourceSet::.ctor(System.IO.Stream)
extern "C" void ResourceSet__ctor_m1_7424 (ResourceSet_t1_655 * __this, Stream_t1_405 * ___stream, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.ResourceSet::.ctor(System.IO.UnmanagedMemoryStream)
extern "C" void ResourceSet__ctor_m1_7425 (ResourceSet_t1_655 * __this, UnmanagedMemoryStream_t1_460 * ___stream, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.ResourceSet::.ctor(System.String)
extern "C" void ResourceSet__ctor_m1_7426 (ResourceSet_t1_655 * __this, String_t* ___fileName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Resources.ResourceSet::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ResourceSet_System_Collections_IEnumerable_GetEnumerator_m1_7427 (ResourceSet_t1_655 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.ResourceSet::Close()
extern "C" void ResourceSet_Close_m1_7428 (ResourceSet_t1_655 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.ResourceSet::Dispose()
extern "C" void ResourceSet_Dispose_m1_7429 (ResourceSet_t1_655 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.ResourceSet::Dispose(System.Boolean)
extern "C" void ResourceSet_Dispose_m1_7430 (ResourceSet_t1_655 * __this, bool ___disposing, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Resources.ResourceSet::GetDefaultReader()
extern "C" Type_t * ResourceSet_GetDefaultReader_m1_7431 (ResourceSet_t1_655 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Resources.ResourceSet::GetDefaultWriter()
extern "C" Type_t * ResourceSet_GetDefaultWriter_m1_7432 (ResourceSet_t1_655 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IDictionaryEnumerator System.Resources.ResourceSet::GetEnumerator()
extern "C" Object_t * ResourceSet_GetEnumerator_m1_7433 (ResourceSet_t1_655 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Resources.ResourceSet::GetObjectInternal(System.String,System.Boolean)
extern "C" Object_t * ResourceSet_GetObjectInternal_m1_7434 (ResourceSet_t1_655 * __this, String_t* ___name, bool ___ignoreCase, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Resources.ResourceSet::GetObject(System.String)
extern "C" Object_t * ResourceSet_GetObject_m1_7435 (ResourceSet_t1_655 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Resources.ResourceSet::GetObject(System.String,System.Boolean)
extern "C" Object_t * ResourceSet_GetObject_m1_7436 (ResourceSet_t1_655 * __this, String_t* ___name, bool ___ignoreCase, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Resources.ResourceSet::GetStringInternal(System.String,System.Boolean)
extern "C" String_t* ResourceSet_GetStringInternal_m1_7437 (ResourceSet_t1_655 * __this, String_t* ___name, bool ___ignoreCase, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Resources.ResourceSet::GetString(System.String)
extern "C" String_t* ResourceSet_GetString_m1_7438 (ResourceSet_t1_655 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Resources.ResourceSet::GetString(System.String,System.Boolean)
extern "C" String_t* ResourceSet_GetString_m1_7439 (ResourceSet_t1_655 * __this, String_t* ___name, bool ___ignoreCase, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.ResourceSet::ReadResources()
extern "C" void ResourceSet_ReadResources_m1_7440 (ResourceSet_t1_655 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.UnmanagedMemoryStream System.Resources.ResourceSet::GetStream(System.String,System.Boolean)
extern "C" UnmanagedMemoryStream_t1_460 * ResourceSet_GetStream_m1_7441 (ResourceSet_t1_655 * __this, String_t* ___name, bool ___ignoreCase, const MethodInfo* method) IL2CPP_METHOD_ATTR;
