﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Reflection.Assembly
struct Assembly_t1_467;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "mscorlib_System_Object.h"

// System.Security.Policy.Hash
struct  Hash_t1_1350  : public Object_t
{
	// System.Reflection.Assembly System.Security.Policy.Hash::assembly
	Assembly_t1_467 * ___assembly_0;
	// System.Byte[] System.Security.Policy.Hash::data
	ByteU5BU5D_t1_109* ___data_1;
	// System.Byte[] System.Security.Policy.Hash::_md5
	ByteU5BU5D_t1_109* ____md5_2;
	// System.Byte[] System.Security.Policy.Hash::_sha1
	ByteU5BU5D_t1_109* ____sha1_3;
};
