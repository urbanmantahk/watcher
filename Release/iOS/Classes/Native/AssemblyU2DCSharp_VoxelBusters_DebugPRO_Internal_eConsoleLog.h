﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "AssemblyU2DCSharp_VoxelBusters_DebugPRO_Internal_eConsoleLog.h"

// VoxelBusters.DebugPRO.Internal.eConsoleLogType
struct  eConsoleLogType_t8_172 
{
	// System.Int32 VoxelBusters.DebugPRO.Internal.eConsoleLogType::value__
	int32_t ___value___1;
};
