﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.CrossPlatformNotification/AndroidSpecificProperties
struct AndroidSpecificProperties_t8_265;
// System.Collections.IDictionary
struct IDictionary_t1_35;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.CrossPlatformNotification/AndroidSpecificProperties::.ctor()
extern "C" void AndroidSpecificProperties__ctor_m8_1488 (AndroidSpecificProperties_t8_265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.CrossPlatformNotification/AndroidSpecificProperties::.ctor(System.Collections.IDictionary)
extern "C" void AndroidSpecificProperties__ctor_m8_1489 (AndroidSpecificProperties_t8_265 * __this, Object_t * ____jsonDict, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.CrossPlatformNotification/AndroidSpecificProperties::get_ContentTitle()
extern "C" String_t* AndroidSpecificProperties_get_ContentTitle_m8_1490 (AndroidSpecificProperties_t8_265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.CrossPlatformNotification/AndroidSpecificProperties::set_ContentTitle(System.String)
extern "C" void AndroidSpecificProperties_set_ContentTitle_m8_1491 (AndroidSpecificProperties_t8_265 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.CrossPlatformNotification/AndroidSpecificProperties::get_TickerText()
extern "C" String_t* AndroidSpecificProperties_get_TickerText_m8_1492 (AndroidSpecificProperties_t8_265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.CrossPlatformNotification/AndroidSpecificProperties::set_TickerText(System.String)
extern "C" void AndroidSpecificProperties_set_TickerText_m8_1493 (AndroidSpecificProperties_t8_265 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.CrossPlatformNotification/AndroidSpecificProperties::get_Tag()
extern "C" String_t* AndroidSpecificProperties_get_Tag_m8_1494 (AndroidSpecificProperties_t8_265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.CrossPlatformNotification/AndroidSpecificProperties::set_Tag(System.String)
extern "C" void AndroidSpecificProperties_set_Tag_m8_1495 (AndroidSpecificProperties_t8_265 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.CrossPlatformNotification/AndroidSpecificProperties::get_CustomSound()
extern "C" String_t* AndroidSpecificProperties_get_CustomSound_m8_1496 (AndroidSpecificProperties_t8_265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.CrossPlatformNotification/AndroidSpecificProperties::set_CustomSound(System.String)
extern "C" void AndroidSpecificProperties_set_CustomSound_m8_1497 (AndroidSpecificProperties_t8_265 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.CrossPlatformNotification/AndroidSpecificProperties::get_LargeIcon()
extern "C" String_t* AndroidSpecificProperties_get_LargeIcon_m8_1498 (AndroidSpecificProperties_t8_265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.CrossPlatformNotification/AndroidSpecificProperties::set_LargeIcon(System.String)
extern "C" void AndroidSpecificProperties_set_LargeIcon_m8_1499 (AndroidSpecificProperties_t8_265 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IDictionary VoxelBusters.NativePlugins.CrossPlatformNotification/AndroidSpecificProperties::JSONObject()
extern "C" Object_t * AndroidSpecificProperties_JSONObject_m8_1500 (AndroidSpecificProperties_t8_265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.CrossPlatformNotification/AndroidSpecificProperties::ToString()
extern "C" String_t* AndroidSpecificProperties_ToString_m8_1501 (AndroidSpecificProperties_t8_265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
