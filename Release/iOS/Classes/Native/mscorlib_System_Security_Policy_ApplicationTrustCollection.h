﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.ArrayList
struct ArrayList_t1_170;

#include "mscorlib_System_Object.h"

// System.Security.Policy.ApplicationTrustCollection
struct  ApplicationTrustCollection_t1_1331  : public Object_t
{
	// System.Collections.ArrayList System.Security.Policy.ApplicationTrustCollection::_list
	ArrayList_t1_170 * ____list_0;
};
