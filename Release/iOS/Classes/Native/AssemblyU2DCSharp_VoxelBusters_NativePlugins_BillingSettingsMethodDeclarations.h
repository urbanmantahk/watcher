﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.BillingSettings/AndroidSettings
struct AndroidSettings_t8_215;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.BillingSettings/AndroidSettings::.ctor()
extern "C" void AndroidSettings__ctor_m8_1227 (AndroidSettings_t8_215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.BillingSettings/AndroidSettings::get_PublicKey()
extern "C" String_t* AndroidSettings_get_PublicKey_m8_1228 (AndroidSettings_t8_215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.BillingSettings/AndroidSettings::set_PublicKey(System.String)
extern "C" void AndroidSettings_set_PublicKey_m8_1229 (AndroidSettings_t8_215 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
