﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.Utility.WebRequest
struct WebRequest_t8_158;
// System.Object
struct Object_t;
// VoxelBusters.Utility.WebRequest/JSONResponse
struct JSONResponse_t8_157;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_URL.h"

// System.Void VoxelBusters.Utility.WebRequest::.ctor(VoxelBusters.Utility.URL,System.Object,System.Boolean)
extern "C" void WebRequest__ctor_m8_907 (WebRequest_t8_158 * __this, URL_t8_156  ____URL, Object_t * ____params, bool ____isAsynchronous, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VoxelBusters.Utility.WebRequest::get_Parameters()
extern "C" Object_t * WebRequest_get_Parameters_m8_908 (WebRequest_t8_158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.WebRequest::set_Parameters(System.Object)
extern "C" void WebRequest_set_Parameters_m8_909 (WebRequest_t8_158 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.Utility.WebRequest/JSONResponse VoxelBusters.Utility.WebRequest::get_OnSuccess()
extern "C" JSONResponse_t8_157 * WebRequest_get_OnSuccess_m8_910 (WebRequest_t8_158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.WebRequest::set_OnSuccess(VoxelBusters.Utility.WebRequest/JSONResponse)
extern "C" void WebRequest_set_OnSuccess_m8_911 (WebRequest_t8_158 * __this, JSONResponse_t8_157 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.Utility.WebRequest/JSONResponse VoxelBusters.Utility.WebRequest::get_OnFailure()
extern "C" JSONResponse_t8_157 * WebRequest_get_OnFailure_m8_912 (WebRequest_t8_158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.WebRequest::set_OnFailure(VoxelBusters.Utility.WebRequest/JSONResponse)
extern "C" void WebRequest_set_OnFailure_m8_913 (WebRequest_t8_158 * __this, JSONResponse_t8_157 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.WebRequest::DidFailStartRequestWithError(System.String)
extern "C" void WebRequest_DidFailStartRequestWithError_m8_914 (WebRequest_t8_158 * __this, String_t* ____error, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.WebRequest::OnFetchingResponse()
extern "C" void WebRequest_OnFetchingResponse_m8_915 (WebRequest_t8_158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
