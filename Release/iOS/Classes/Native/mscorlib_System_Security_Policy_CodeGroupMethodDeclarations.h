﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Policy.CodeGroup
struct CodeGroup_t1_1339;
// System.Security.Policy.IMembershipCondition
struct IMembershipCondition_t1_1340;
// System.Security.Policy.PolicyStatement
struct PolicyStatement_t1_1334;
// System.Security.SecurityElement
struct SecurityElement_t1_242;
// System.Security.Policy.PolicyLevel
struct PolicyLevel_t1_1357;
// System.String
struct String_t;
// System.Collections.IList
struct IList_t1_262;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Policy.CodeGroup::.ctor(System.Security.Policy.IMembershipCondition,System.Security.Policy.PolicyStatement)
extern "C" void CodeGroup__ctor_m1_11405 (CodeGroup_t1_1339 * __this, Object_t * ___membershipCondition, PolicyStatement_t1_1334 * ___policy, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.CodeGroup::.ctor(System.Security.SecurityElement,System.Security.Policy.PolicyLevel)
extern "C" void CodeGroup__ctor_m1_11406 (CodeGroup_t1_1339 * __this, SecurityElement_t1_242 * ___e, PolicyLevel_t1_1357 * ___level, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.PolicyStatement System.Security.Policy.CodeGroup::get_PolicyStatement()
extern "C" PolicyStatement_t1_1334 * CodeGroup_get_PolicyStatement_m1_11407 (CodeGroup_t1_1339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.CodeGroup::set_PolicyStatement(System.Security.Policy.PolicyStatement)
extern "C" void CodeGroup_set_PolicyStatement_m1_11408 (CodeGroup_t1_1339 * __this, PolicyStatement_t1_1334 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Policy.CodeGroup::get_Description()
extern "C" String_t* CodeGroup_get_Description_m1_11409 (CodeGroup_t1_1339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.CodeGroup::set_Description(System.String)
extern "C" void CodeGroup_set_Description_m1_11410 (CodeGroup_t1_1339 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.IMembershipCondition System.Security.Policy.CodeGroup::get_MembershipCondition()
extern "C" Object_t * CodeGroup_get_MembershipCondition_m1_11411 (CodeGroup_t1_1339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.CodeGroup::set_MembershipCondition(System.Security.Policy.IMembershipCondition)
extern "C" void CodeGroup_set_MembershipCondition_m1_11412 (CodeGroup_t1_1339 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Policy.CodeGroup::get_Name()
extern "C" String_t* CodeGroup_get_Name_m1_11413 (CodeGroup_t1_1339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.CodeGroup::set_Name(System.String)
extern "C" void CodeGroup_set_Name_m1_11414 (CodeGroup_t1_1339 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IList System.Security.Policy.CodeGroup::get_Children()
extern "C" Object_t * CodeGroup_get_Children_m1_11415 (CodeGroup_t1_1339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.CodeGroup::set_Children(System.Collections.IList)
extern "C" void CodeGroup_set_Children_m1_11416 (CodeGroup_t1_1339 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Policy.CodeGroup::get_AttributeString()
extern "C" String_t* CodeGroup_get_AttributeString_m1_11417 (CodeGroup_t1_1339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Policy.CodeGroup::get_PermissionSetName()
extern "C" String_t* CodeGroup_get_PermissionSetName_m1_11418 (CodeGroup_t1_1339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.CodeGroup::AddChild(System.Security.Policy.CodeGroup)
extern "C" void CodeGroup_AddChild_m1_11419 (CodeGroup_t1_1339 * __this, CodeGroup_t1_1339 * ___group, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Policy.CodeGroup::Equals(System.Object)
extern "C" bool CodeGroup_Equals_m1_11420 (CodeGroup_t1_1339 * __this, Object_t * ___o, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Policy.CodeGroup::Equals(System.Security.Policy.CodeGroup,System.Boolean)
extern "C" bool CodeGroup_Equals_m1_11421 (CodeGroup_t1_1339 * __this, CodeGroup_t1_1339 * ___cg, bool ___compareChildren, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.CodeGroup::RemoveChild(System.Security.Policy.CodeGroup)
extern "C" void CodeGroup_RemoveChild_m1_11422 (CodeGroup_t1_1339 * __this, CodeGroup_t1_1339 * ___group, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Policy.CodeGroup::GetHashCode()
extern "C" int32_t CodeGroup_GetHashCode_m1_11423 (CodeGroup_t1_1339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.CodeGroup::FromXml(System.Security.SecurityElement)
extern "C" void CodeGroup_FromXml_m1_11424 (CodeGroup_t1_1339 * __this, SecurityElement_t1_242 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.CodeGroup::FromXml(System.Security.SecurityElement,System.Security.Policy.PolicyLevel)
extern "C" void CodeGroup_FromXml_m1_11425 (CodeGroup_t1_1339 * __this, SecurityElement_t1_242 * ___e, PolicyLevel_t1_1357 * ___level, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.CodeGroup::ParseXml(System.Security.SecurityElement,System.Security.Policy.PolicyLevel)
extern "C" void CodeGroup_ParseXml_m1_11426 (CodeGroup_t1_1339 * __this, SecurityElement_t1_242 * ___e, PolicyLevel_t1_1357 * ___level, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityElement System.Security.Policy.CodeGroup::ToXml()
extern "C" SecurityElement_t1_242 * CodeGroup_ToXml_m1_11427 (CodeGroup_t1_1339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityElement System.Security.Policy.CodeGroup::ToXml(System.Security.Policy.PolicyLevel)
extern "C" SecurityElement_t1_242 * CodeGroup_ToXml_m1_11428 (CodeGroup_t1_1339 * __this, PolicyLevel_t1_1357 * ___level, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.CodeGroup::CreateXml(System.Security.SecurityElement,System.Security.Policy.PolicyLevel)
extern "C" void CodeGroup_CreateXml_m1_11429 (CodeGroup_t1_1339 * __this, SecurityElement_t1_242 * ___element, PolicyLevel_t1_1357 * ___level, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.CodeGroup System.Security.Policy.CodeGroup::CreateFromXml(System.Security.SecurityElement,System.Security.Policy.PolicyLevel)
extern "C" CodeGroup_t1_1339 * CodeGroup_CreateFromXml_m1_11430 (Object_t * __this /* static, unused */, SecurityElement_t1_242 * ___se, PolicyLevel_t1_1357 * ___level, const MethodInfo* method) IL2CPP_METHOD_ATTR;
