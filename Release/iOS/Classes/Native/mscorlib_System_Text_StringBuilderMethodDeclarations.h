﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Text.StringBuilder
struct StringBuilder_t1_247;
// System.String
struct String_t;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;
// System.Char[]
struct CharU5BU5D_t1_16;
// System.Object
struct Object_t;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.IFormatProvider
struct IFormatProvider_t1_455;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Decimal.h"

// System.Void System.Text.StringBuilder::.ctor(System.String,System.Int32,System.Int32,System.Int32)
extern "C" void StringBuilder__ctor_m1_12412 (StringBuilder_t1_247 * __this, String_t* ___value, int32_t ___startIndex, int32_t ___length, int32_t ___capacity, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.StringBuilder::.ctor(System.String,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" void StringBuilder__ctor_m1_12413 (StringBuilder_t1_247 * __this, String_t* ___value, int32_t ___startIndex, int32_t ___length, int32_t ___capacity, int32_t ___maxCapacity, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.StringBuilder::.ctor()
extern "C" void StringBuilder__ctor_m1_12414 (StringBuilder_t1_247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.StringBuilder::.ctor(System.Int32)
extern "C" void StringBuilder__ctor_m1_12415 (StringBuilder_t1_247 * __this, int32_t ___capacity, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.StringBuilder::.ctor(System.Int32,System.Int32)
extern "C" void StringBuilder__ctor_m1_12416 (StringBuilder_t1_247 * __this, int32_t ___capacity, int32_t ___maxCapacity, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.StringBuilder::.ctor(System.String)
extern "C" void StringBuilder__ctor_m1_12417 (StringBuilder_t1_247 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.StringBuilder::.ctor(System.String,System.Int32)
extern "C" void StringBuilder__ctor_m1_12418 (StringBuilder_t1_247 * __this, String_t* ___value, int32_t ___capacity, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.StringBuilder::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void StringBuilder__ctor_m1_12419 (StringBuilder_t1_247 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.StringBuilder::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void StringBuilder_System_Runtime_Serialization_ISerializable_GetObjectData_m1_12420 (StringBuilder_t1_247 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.StringBuilder::get_MaxCapacity()
extern "C" int32_t StringBuilder_get_MaxCapacity_m1_12421 (StringBuilder_t1_247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.StringBuilder::get_Capacity()
extern "C" int32_t StringBuilder_get_Capacity_m1_12422 (StringBuilder_t1_247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.StringBuilder::set_Capacity(System.Int32)
extern "C" void StringBuilder_set_Capacity_m1_12423 (StringBuilder_t1_247 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.StringBuilder::get_Length()
extern "C" int32_t StringBuilder_get_Length_m1_12424 (StringBuilder_t1_247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.StringBuilder::set_Length(System.Int32)
extern "C" void StringBuilder_set_Length_m1_12425 (StringBuilder_t1_247 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char System.Text.StringBuilder::get_Chars(System.Int32)
extern "C" uint16_t StringBuilder_get_Chars_m1_12426 (StringBuilder_t1_247 * __this, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.StringBuilder::set_Chars(System.Int32,System.Char)
extern "C" void StringBuilder_set_Chars_m1_12427 (StringBuilder_t1_247 * __this, int32_t ___index, uint16_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Text.StringBuilder::ToString()
extern "C" String_t* StringBuilder_ToString_m1_12428 (StringBuilder_t1_247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Text.StringBuilder::ToString(System.Int32,System.Int32)
extern "C" String_t* StringBuilder_ToString_m1_12429 (StringBuilder_t1_247 * __this, int32_t ___startIndex, int32_t ___length, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.StringBuilder::EnsureCapacity(System.Int32)
extern "C" int32_t StringBuilder_EnsureCapacity_m1_12430 (StringBuilder_t1_247 * __this, int32_t ___capacity, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Text.StringBuilder::Equals(System.Text.StringBuilder)
extern "C" bool StringBuilder_Equals_m1_12431 (StringBuilder_t1_247 * __this, StringBuilder_t1_247 * ___sb, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Remove(System.Int32,System.Int32)
extern "C" StringBuilder_t1_247 * StringBuilder_Remove_m1_12432 (StringBuilder_t1_247 * __this, int32_t ___startIndex, int32_t ___length, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Replace(System.Char,System.Char)
extern "C" StringBuilder_t1_247 * StringBuilder_Replace_m1_12433 (StringBuilder_t1_247 * __this, uint16_t ___oldChar, uint16_t ___newChar, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Replace(System.Char,System.Char,System.Int32,System.Int32)
extern "C" StringBuilder_t1_247 * StringBuilder_Replace_m1_12434 (StringBuilder_t1_247 * __this, uint16_t ___oldChar, uint16_t ___newChar, int32_t ___startIndex, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Replace(System.String,System.String)
extern "C" StringBuilder_t1_247 * StringBuilder_Replace_m1_12435 (StringBuilder_t1_247 * __this, String_t* ___oldValue, String_t* ___newValue, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Replace(System.String,System.String,System.Int32,System.Int32)
extern "C" StringBuilder_t1_247 * StringBuilder_Replace_m1_12436 (StringBuilder_t1_247 * __this, String_t* ___oldValue, String_t* ___newValue, int32_t ___startIndex, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.Char[])
extern "C" StringBuilder_t1_247 * StringBuilder_Append_m1_12437 (StringBuilder_t1_247 * __this, CharU5BU5D_t1_16* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.String)
extern "C" StringBuilder_t1_247 * StringBuilder_Append_m1_12438 (StringBuilder_t1_247 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.Boolean)
extern "C" StringBuilder_t1_247 * StringBuilder_Append_m1_12439 (StringBuilder_t1_247 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.Byte)
extern "C" StringBuilder_t1_247 * StringBuilder_Append_m1_12440 (StringBuilder_t1_247 * __this, uint8_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.Decimal)
extern "C" StringBuilder_t1_247 * StringBuilder_Append_m1_12441 (StringBuilder_t1_247 * __this, Decimal_t1_19  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.Double)
extern "C" StringBuilder_t1_247 * StringBuilder_Append_m1_12442 (StringBuilder_t1_247 * __this, double ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.Int16)
extern "C" StringBuilder_t1_247 * StringBuilder_Append_m1_12443 (StringBuilder_t1_247 * __this, int16_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.Int32)
extern "C" StringBuilder_t1_247 * StringBuilder_Append_m1_12444 (StringBuilder_t1_247 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.Int64)
extern "C" StringBuilder_t1_247 * StringBuilder_Append_m1_12445 (StringBuilder_t1_247 * __this, int64_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.Object)
extern "C" StringBuilder_t1_247 * StringBuilder_Append_m1_12446 (StringBuilder_t1_247 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.SByte)
extern "C" StringBuilder_t1_247 * StringBuilder_Append_m1_12447 (StringBuilder_t1_247 * __this, int8_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.Single)
extern "C" StringBuilder_t1_247 * StringBuilder_Append_m1_12448 (StringBuilder_t1_247 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.UInt16)
extern "C" StringBuilder_t1_247 * StringBuilder_Append_m1_12449 (StringBuilder_t1_247 * __this, uint16_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.UInt32)
extern "C" StringBuilder_t1_247 * StringBuilder_Append_m1_12450 (StringBuilder_t1_247 * __this, uint32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.UInt64)
extern "C" StringBuilder_t1_247 * StringBuilder_Append_m1_12451 (StringBuilder_t1_247 * __this, uint64_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.Char)
extern "C" StringBuilder_t1_247 * StringBuilder_Append_m1_12452 (StringBuilder_t1_247 * __this, uint16_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.Char,System.Int32)
extern "C" StringBuilder_t1_247 * StringBuilder_Append_m1_12453 (StringBuilder_t1_247 * __this, uint16_t ___value, int32_t ___repeatCount, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.Char[],System.Int32,System.Int32)
extern "C" StringBuilder_t1_247 * StringBuilder_Append_m1_12454 (StringBuilder_t1_247 * __this, CharU5BU5D_t1_16* ___value, int32_t ___startIndex, int32_t ___charCount, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.String,System.Int32,System.Int32)
extern "C" StringBuilder_t1_247 * StringBuilder_Append_m1_12455 (StringBuilder_t1_247 * __this, String_t* ___value, int32_t ___startIndex, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::AppendLine()
extern "C" StringBuilder_t1_247 * StringBuilder_AppendLine_m1_12456 (StringBuilder_t1_247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::AppendLine(System.String)
extern "C" StringBuilder_t1_247 * StringBuilder_AppendLine_m1_12457 (StringBuilder_t1_247 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::AppendFormat(System.String,System.Object[])
extern "C" StringBuilder_t1_247 * StringBuilder_AppendFormat_m1_12458 (StringBuilder_t1_247 * __this, String_t* ___format, ObjectU5BU5D_t1_272* ___args, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::AppendFormat(System.IFormatProvider,System.String,System.Object[])
extern "C" StringBuilder_t1_247 * StringBuilder_AppendFormat_m1_12459 (StringBuilder_t1_247 * __this, Object_t * ___provider, String_t* ___format, ObjectU5BU5D_t1_272* ___args, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::AppendFormat(System.String,System.Object)
extern "C" StringBuilder_t1_247 * StringBuilder_AppendFormat_m1_12460 (StringBuilder_t1_247 * __this, String_t* ___format, Object_t * ___arg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::AppendFormat(System.String,System.Object,System.Object)
extern "C" StringBuilder_t1_247 * StringBuilder_AppendFormat_m1_12461 (StringBuilder_t1_247 * __this, String_t* ___format, Object_t * ___arg0, Object_t * ___arg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::AppendFormat(System.String,System.Object,System.Object,System.Object)
extern "C" StringBuilder_t1_247 * StringBuilder_AppendFormat_m1_12462 (StringBuilder_t1_247 * __this, String_t* ___format, Object_t * ___arg0, Object_t * ___arg1, Object_t * ___arg2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Insert(System.Int32,System.Char[])
extern "C" StringBuilder_t1_247 * StringBuilder_Insert_m1_12463 (StringBuilder_t1_247 * __this, int32_t ___index, CharU5BU5D_t1_16* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Insert(System.Int32,System.String)
extern "C" StringBuilder_t1_247 * StringBuilder_Insert_m1_12464 (StringBuilder_t1_247 * __this, int32_t ___index, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Insert(System.Int32,System.Boolean)
extern "C" StringBuilder_t1_247 * StringBuilder_Insert_m1_12465 (StringBuilder_t1_247 * __this, int32_t ___index, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Insert(System.Int32,System.Byte)
extern "C" StringBuilder_t1_247 * StringBuilder_Insert_m1_12466 (StringBuilder_t1_247 * __this, int32_t ___index, uint8_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Insert(System.Int32,System.Char)
extern "C" StringBuilder_t1_247 * StringBuilder_Insert_m1_12467 (StringBuilder_t1_247 * __this, int32_t ___index, uint16_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Insert(System.Int32,System.Decimal)
extern "C" StringBuilder_t1_247 * StringBuilder_Insert_m1_12468 (StringBuilder_t1_247 * __this, int32_t ___index, Decimal_t1_19  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Insert(System.Int32,System.Double)
extern "C" StringBuilder_t1_247 * StringBuilder_Insert_m1_12469 (StringBuilder_t1_247 * __this, int32_t ___index, double ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Insert(System.Int32,System.Int16)
extern "C" StringBuilder_t1_247 * StringBuilder_Insert_m1_12470 (StringBuilder_t1_247 * __this, int32_t ___index, int16_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Insert(System.Int32,System.Int32)
extern "C" StringBuilder_t1_247 * StringBuilder_Insert_m1_12471 (StringBuilder_t1_247 * __this, int32_t ___index, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Insert(System.Int32,System.Int64)
extern "C" StringBuilder_t1_247 * StringBuilder_Insert_m1_12472 (StringBuilder_t1_247 * __this, int32_t ___index, int64_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Insert(System.Int32,System.Object)
extern "C" StringBuilder_t1_247 * StringBuilder_Insert_m1_12473 (StringBuilder_t1_247 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Insert(System.Int32,System.SByte)
extern "C" StringBuilder_t1_247 * StringBuilder_Insert_m1_12474 (StringBuilder_t1_247 * __this, int32_t ___index, int8_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Insert(System.Int32,System.Single)
extern "C" StringBuilder_t1_247 * StringBuilder_Insert_m1_12475 (StringBuilder_t1_247 * __this, int32_t ___index, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Insert(System.Int32,System.UInt16)
extern "C" StringBuilder_t1_247 * StringBuilder_Insert_m1_12476 (StringBuilder_t1_247 * __this, int32_t ___index, uint16_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Insert(System.Int32,System.UInt32)
extern "C" StringBuilder_t1_247 * StringBuilder_Insert_m1_12477 (StringBuilder_t1_247 * __this, int32_t ___index, uint32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Insert(System.Int32,System.UInt64)
extern "C" StringBuilder_t1_247 * StringBuilder_Insert_m1_12478 (StringBuilder_t1_247 * __this, int32_t ___index, uint64_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Insert(System.Int32,System.String,System.Int32)
extern "C" StringBuilder_t1_247 * StringBuilder_Insert_m1_12479 (StringBuilder_t1_247 * __this, int32_t ___index, String_t* ___value, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Insert(System.Int32,System.Char[],System.Int32,System.Int32)
extern "C" StringBuilder_t1_247 * StringBuilder_Insert_m1_12480 (StringBuilder_t1_247 * __this, int32_t ___index, CharU5BU5D_t1_16* ___value, int32_t ___startIndex, int32_t ___charCount, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.StringBuilder::InternalEnsureCapacity(System.Int32)
extern "C" void StringBuilder_InternalEnsureCapacity_m1_12481 (StringBuilder_t1_247 * __this, int32_t ___size, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.StringBuilder::CopyTo(System.Int32,System.Char[],System.Int32,System.Int32)
extern "C" void StringBuilder_CopyTo_m1_12482 (StringBuilder_t1_247 * __this, int32_t ___sourceIndex, CharU5BU5D_t1_16* ___destination, int32_t ___destinationIndex, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
