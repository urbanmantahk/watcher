﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VoxelBusters.DebugPRO.UnityDebugUtility
struct UnityDebugUtility_t8_173;
// UnityEngine.Application/LogCallback
struct LogCallback_t6_83;

#include "UnityEngine_UnityEngine_ScriptableObject.h"

// VoxelBusters.DebugPRO.UnityDebugUtility
struct  UnityDebugUtility_t8_173  : public ScriptableObject_t6_15
{
};
struct UnityDebugUtility_t8_173_StaticFields{
	// VoxelBusters.DebugPRO.UnityDebugUtility VoxelBusters.DebugPRO.UnityDebugUtility::instance
	UnityDebugUtility_t8_173 * ___instance_2;
	// UnityEngine.Application/LogCallback VoxelBusters.DebugPRO.UnityDebugUtility::LogCallback
	LogCallback_t6_83 * ___LogCallback_3;
};
