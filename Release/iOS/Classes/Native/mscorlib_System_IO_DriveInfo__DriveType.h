﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_IO_DriveInfo__DriveType.h"

// System.IO.DriveInfo/_DriveType
struct  _DriveType_t1_414 
{
	// System.Int32 System.IO.DriveInfo/_DriveType::value__
	int32_t ___value___1;
};
