﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object.h"

// VoxelBusters.NativePlugins.GameServicesSettings/iOSSettings
struct  iOSSettings_t8_237  : public Object_t
{
	// System.Boolean VoxelBusters.NativePlugins.GameServicesSettings/iOSSettings::m_showDefaultAchievementCompletionBanner
	bool ___m_showDefaultAchievementCompletionBanner_0;
};
