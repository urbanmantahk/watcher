﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object.h"
#include "mscorlib_System_Security_AccessControl_InheritanceFlags.h"
#include "mscorlib_System_Security_AccessControl_PropagationFlags.h"
#include "mscorlib_System_Security_AccessControl_AceFlags.h"
#include "mscorlib_System_Security_AccessControl_AceType.h"

// System.Security.AccessControl.GenericAce
struct  GenericAce_t1_1145  : public Object_t
{
	// System.Security.AccessControl.InheritanceFlags System.Security.AccessControl.GenericAce::inheritance
	int32_t ___inheritance_0;
	// System.Security.AccessControl.PropagationFlags System.Security.AccessControl.GenericAce::propagation
	int32_t ___propagation_1;
	// System.Security.AccessControl.AceFlags System.Security.AccessControl.GenericAce::aceflags
	uint8_t ___aceflags_2;
	// System.Security.AccessControl.AceType System.Security.AccessControl.GenericAce::ace_type
	int32_t ___ace_type_3;
};
