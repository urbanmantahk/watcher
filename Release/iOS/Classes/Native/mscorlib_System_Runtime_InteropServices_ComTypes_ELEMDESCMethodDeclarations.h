﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

extern "C" void ELEMDESC_t1_729_marshal(const ELEMDESC_t1_729& unmarshaled, ELEMDESC_t1_729_marshaled& marshaled);
extern "C" void ELEMDESC_t1_729_marshal_back(const ELEMDESC_t1_729_marshaled& marshaled, ELEMDESC_t1_729& unmarshaled);
extern "C" void ELEMDESC_t1_729_marshal_cleanup(ELEMDESC_t1_729_marshaled& marshaled);
