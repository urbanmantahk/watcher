﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapQName
struct SoapQName_t1_992;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapQName::.ctor()
extern "C" void SoapQName__ctor_m1_8880 (SoapQName_t1_992 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapQName::.ctor(System.String)
extern "C" void SoapQName__ctor_m1_8881 (SoapQName_t1_992 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapQName::.ctor(System.String,System.String)
extern "C" void SoapQName__ctor_m1_8882 (SoapQName_t1_992 * __this, String_t* ___key, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapQName::.ctor(System.String,System.String,System.String)
extern "C" void SoapQName__ctor_m1_8883 (SoapQName_t1_992 * __this, String_t* ___key, String_t* ___name, String_t* ___namespaceValue, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapQName::get_Key()
extern "C" String_t* SoapQName_get_Key_m1_8884 (SoapQName_t1_992 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapQName::set_Key(System.String)
extern "C" void SoapQName_set_Key_m1_8885 (SoapQName_t1_992 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapQName::get_Name()
extern "C" String_t* SoapQName_get_Name_m1_8886 (SoapQName_t1_992 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapQName::set_Name(System.String)
extern "C" void SoapQName_set_Name_m1_8887 (SoapQName_t1_992 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapQName::get_Namespace()
extern "C" String_t* SoapQName_get_Namespace_m1_8888 (SoapQName_t1_992 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapQName::set_Namespace(System.String)
extern "C" void SoapQName_set_Namespace_m1_8889 (SoapQName_t1_992 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapQName::get_XsdType()
extern "C" String_t* SoapQName_get_XsdType_m1_8890 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapQName::GetXsdType()
extern "C" String_t* SoapQName_GetXsdType_m1_8891 (SoapQName_t1_992 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapQName System.Runtime.Remoting.Metadata.W3cXsd2001.SoapQName::Parse(System.String)
extern "C" SoapQName_t1_992 * SoapQName_Parse_m1_8892 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapQName::ToString()
extern "C" String_t* SoapQName_ToString_m1_8893 (SoapQName_t1_992 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
