﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.Internal.ShareImageUtility
struct ShareImageUtility_t8_280;
// System.String
struct String_t;
// UnityEngine.Texture2D
struct Texture2D_t6_33;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.Internal.ShareImageUtility::.ctor()
extern "C" void ShareImageUtility__ctor_m8_1707 (ShareImageUtility_t8_280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.NativePlugins.Internal.ShareImageUtility::get_ImageAsyncDownloadInProgress()
extern "C" bool ShareImageUtility_get_ImageAsyncDownloadInProgress_m8_1708 (ShareImageUtility_t8_280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Internal.ShareImageUtility::set_ImageAsyncDownloadInProgress(System.Boolean)
extern "C" void ShareImageUtility_set_ImageAsyncDownloadInProgress_m8_1709 (ShareImageUtility_t8_280 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Internal.ShareImageUtility::AttachScreenShot()
extern "C" void ShareImageUtility_AttachScreenShot_m8_1710 (ShareImageUtility_t8_280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Internal.ShareImageUtility::AttachImageAtPath(System.String)
extern "C" void ShareImageUtility_AttachImageAtPath_m8_1711 (ShareImageUtility_t8_280 * __this, String_t* ____imagePath, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Internal.ShareImageUtility::StopAsyncRequests()
extern "C" void ShareImageUtility_StopAsyncRequests_m8_1712 (ShareImageUtility_t8_280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Internal.ShareImageUtility::<AttachScreenShot>m__13(UnityEngine.Texture2D)
extern "C" void ShareImageUtility_U3CAttachScreenShotU3Em__13_m8_1713 (ShareImageUtility_t8_280 * __this, Texture2D_t6_33 * ____texture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Internal.ShareImageUtility::<AttachImageAtPath>m__14(UnityEngine.Texture2D,System.String)
extern "C" void ShareImageUtility_U3CAttachImageAtPathU3Em__14_m8_1714 (ShareImageUtility_t8_280 * __this, Texture2D_t6_33 * ____texture, String_t* ____error, const MethodInfo* method) IL2CPP_METHOD_ATTR;
