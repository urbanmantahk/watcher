﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Serialization.Formatters.SoapFault
struct SoapFault_t1_1071;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;
// System.String
struct String_t;
// System.Runtime.Serialization.Formatters.ServerFault
struct ServerFault_t1_1070;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.Runtime.Serialization.Formatters.SoapFault::.ctor()
extern "C" void SoapFault__ctor_m1_9477 (SoapFault_t1_1071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.Formatters.SoapFault::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void SoapFault__ctor_m1_9478 (SoapFault_t1_1071 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.Formatters.SoapFault::.ctor(System.String,System.String,System.String,System.Runtime.Serialization.Formatters.ServerFault)
extern "C" void SoapFault__ctor_m1_9479 (SoapFault_t1_1071 * __this, String_t* ___faultCode, String_t* ___faultString, String_t* ___faultActor, ServerFault_t1_1070 * ___serverFault, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Serialization.Formatters.SoapFault::get_Detail()
extern "C" Object_t * SoapFault_get_Detail_m1_9480 (SoapFault_t1_1071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.Formatters.SoapFault::set_Detail(System.Object)
extern "C" void SoapFault_set_Detail_m1_9481 (SoapFault_t1_1071 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Serialization.Formatters.SoapFault::get_FaultActor()
extern "C" String_t* SoapFault_get_FaultActor_m1_9482 (SoapFault_t1_1071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.Formatters.SoapFault::set_FaultActor(System.String)
extern "C" void SoapFault_set_FaultActor_m1_9483 (SoapFault_t1_1071 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Serialization.Formatters.SoapFault::get_FaultCode()
extern "C" String_t* SoapFault_get_FaultCode_m1_9484 (SoapFault_t1_1071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.Formatters.SoapFault::set_FaultCode(System.String)
extern "C" void SoapFault_set_FaultCode_m1_9485 (SoapFault_t1_1071 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Serialization.Formatters.SoapFault::get_FaultString()
extern "C" String_t* SoapFault_get_FaultString_m1_9486 (SoapFault_t1_1071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.Formatters.SoapFault::set_FaultString(System.String)
extern "C" void SoapFault_set_FaultString_m1_9487 (SoapFault_t1_1071 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.Formatters.SoapFault::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void SoapFault_GetObjectData_m1_9488 (SoapFault_t1_1071 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
