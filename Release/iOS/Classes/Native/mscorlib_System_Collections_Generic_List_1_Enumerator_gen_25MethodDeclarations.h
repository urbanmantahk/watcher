﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.UILineInfo>
struct List_1_t1_1826;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25.h"
#include "UnityEngine_UnityEngine_UILineInfo.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m1_20706_gshared (Enumerator_t1_2371 * __this, List_1_t1_1826 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m1_20706(__this, ___l, method) (( void (*) (Enumerator_t1_2371 *, List_1_t1_1826 *, const MethodInfo*))Enumerator__ctor_m1_20706_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m1_20707_gshared (Enumerator_t1_2371 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1_20707(__this, method) (( void (*) (Enumerator_t1_2371 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_20707_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m1_20708_gshared (Enumerator_t1_2371 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_20708(__this, method) (( Object_t * (*) (Enumerator_t1_2371 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_20708_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::Dispose()
extern "C" void Enumerator_Dispose_m1_20709_gshared (Enumerator_t1_2371 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1_20709(__this, method) (( void (*) (Enumerator_t1_2371 *, const MethodInfo*))Enumerator_Dispose_m1_20709_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::VerifyState()
extern "C" void Enumerator_VerifyState_m1_20710_gshared (Enumerator_t1_2371 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m1_20710(__this, method) (( void (*) (Enumerator_t1_2371 *, const MethodInfo*))Enumerator_VerifyState_m1_20710_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::MoveNext()
extern "C" bool Enumerator_MoveNext_m1_20711_gshared (Enumerator_t1_2371 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1_20711(__this, method) (( bool (*) (Enumerator_t1_2371 *, const MethodInfo*))Enumerator_MoveNext_m1_20711_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::get_Current()
extern "C" UILineInfo_t6_151  Enumerator_get_Current_m1_20712_gshared (Enumerator_t1_2371 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1_20712(__this, method) (( UILineInfo_t6_151  (*) (Enumerator_t1_2371 *, const MethodInfo*))Enumerator_get_Current_m1_20712_gshared)(__this, method)
