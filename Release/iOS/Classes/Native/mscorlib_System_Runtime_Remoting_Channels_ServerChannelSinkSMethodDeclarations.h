﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Channels.ServerChannelSinkStack
struct ServerChannelSinkStack_t1_885;
// System.IO.Stream
struct Stream_t1_405;
// System.Runtime.Remoting.Messaging.IMessage
struct IMessage_t1_875;
// System.Runtime.Remoting.Channels.ITransportHeaders
struct ITransportHeaders_t1_1711;
// System.Object
struct Object_t;
// System.Runtime.Remoting.Channels.IServerChannelSink
struct IServerChannelSink_t1_1706;
// System.IAsyncResult
struct IAsyncResult_t1_27;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Channels.ServerChannelSinkStack::.ctor()
extern "C" void ServerChannelSinkStack__ctor_m1_8100 (ServerChannelSinkStack_t1_885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream System.Runtime.Remoting.Channels.ServerChannelSinkStack::GetResponseStream(System.Runtime.Remoting.Messaging.IMessage,System.Runtime.Remoting.Channels.ITransportHeaders)
extern "C" Stream_t1_405 * ServerChannelSinkStack_GetResponseStream_m1_8101 (ServerChannelSinkStack_t1_885 * __this, Object_t * ___msg, Object_t * ___headers, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Channels.ServerChannelSinkStack::Pop(System.Runtime.Remoting.Channels.IServerChannelSink)
extern "C" Object_t * ServerChannelSinkStack_Pop_m1_8102 (ServerChannelSinkStack_t1_885 * __this, Object_t * ___sink, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Channels.ServerChannelSinkStack::Push(System.Runtime.Remoting.Channels.IServerChannelSink,System.Object)
extern "C" void ServerChannelSinkStack_Push_m1_8103 (ServerChannelSinkStack_t1_885 * __this, Object_t * ___sink, Object_t * ___state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Channels.ServerChannelSinkStack::ServerCallback(System.IAsyncResult)
extern "C" void ServerChannelSinkStack_ServerCallback_m1_8104 (ServerChannelSinkStack_t1_885 * __this, Object_t * ___ar, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Channels.ServerChannelSinkStack::Store(System.Runtime.Remoting.Channels.IServerChannelSink,System.Object)
extern "C" void ServerChannelSinkStack_Store_m1_8105 (ServerChannelSinkStack_t1_885 * __this, Object_t * ___sink, Object_t * ___state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Channels.ServerChannelSinkStack::StoreAndDispatch(System.Runtime.Remoting.Channels.IServerChannelSink,System.Object)
extern "C" void ServerChannelSinkStack_StoreAndDispatch_m1_8106 (ServerChannelSinkStack_t1_885 * __this, Object_t * ___sink, Object_t * ___state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Channels.ServerChannelSinkStack::AsyncProcessResponse(System.Runtime.Remoting.Messaging.IMessage,System.Runtime.Remoting.Channels.ITransportHeaders,System.IO.Stream)
extern "C" void ServerChannelSinkStack_AsyncProcessResponse_m1_8107 (ServerChannelSinkStack_t1_885 * __this, Object_t * ___msg, Object_t * ___headers, Stream_t1_405 * ___stream, const MethodInfo* method) IL2CPP_METHOD_ATTR;
