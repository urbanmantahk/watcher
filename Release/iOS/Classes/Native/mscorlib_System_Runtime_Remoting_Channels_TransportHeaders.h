﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Hashtable
struct Hashtable_t1_100;

#include "mscorlib_System_Object.h"

// System.Runtime.Remoting.Channels.TransportHeaders
struct  TransportHeaders_t1_890  : public Object_t
{
	// System.Collections.Hashtable System.Runtime.Remoting.Channels.TransportHeaders::hash_table
	Hashtable_t1_100 * ___hash_table_0;
};
