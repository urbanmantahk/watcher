﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Exception
struct Exception_t1_33;
// System.String
struct String_t;
// System.Runtime.InteropServices.SafeHandle
struct SafeHandle_t1_88;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;

#include "mscorlib_System_MulticastDelegate.h"
#include "mscorlib_System_Int32.h"

// System.Security.AccessControl.NativeObjectSecurity/ExceptionFromErrorCode
struct  ExceptionFromErrorCode_t1_1162  : public MulticastDelegate_t1_21
{
};
