﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.Utility.Request
struct Request_t8_155;
// UnityEngine.WWW
struct WWW_t6_78;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_URL.h"

// System.Void VoxelBusters.Utility.Request::.ctor()
extern "C" void Request__ctor_m8_892 (Request_t8_155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.Request::.ctor(VoxelBusters.Utility.URL,System.Boolean)
extern "C" void Request__ctor_m8_893 (Request_t8_155 * __this, URL_t8_156  ____URL, bool ____isAsynchronous, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.Utility.Request::get_IsAsynchronous()
extern "C" bool Request_get_IsAsynchronous_m8_894 (Request_t8_155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.Request::set_IsAsynchronous(System.Boolean)
extern "C" void Request_set_IsAsynchronous_m8_895 (Request_t8_155 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.Utility.URL VoxelBusters.Utility.Request::get_URL()
extern "C" URL_t8_156  Request_get_URL_m8_896 (Request_t8_155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.Request::set_URL(VoxelBusters.Utility.URL)
extern "C" void Request_set_URL_m8_897 (Request_t8_155 * __this, URL_t8_156  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWW VoxelBusters.Utility.Request::get_WWWObject()
extern "C" WWW_t6_78 * Request_get_WWWObject_m8_898 (Request_t8_155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.Request::set_WWWObject(UnityEngine.WWW)
extern "C" void Request_set_WWWObject_m8_899 (Request_t8_155 * __this, WWW_t6_78 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.Request::StartRequest()
extern "C" void Request_StartRequest_m8_900 (Request_t8_155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator VoxelBusters.Utility.Request::StartAsynchronousRequest()
extern "C" Object_t * Request_StartAsynchronousRequest_m8_901 (Request_t8_155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.Request::Abort()
extern "C" void Request_Abort_m8_902 (Request_t8_155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
