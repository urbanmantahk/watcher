﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedList`2/Enumerator<System.Int32,ExifLibrary.IFD>
struct Enumerator_t3_286;
// System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>
struct SortedList_2_t3_248;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_SortedList_2_EnumeratorMod_0.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.SortedList`2/Enumerator<System.Int32,ExifLibrary.IFD>::.ctor(System.Collections.Generic.SortedList`2<TKey,TValue>,System.Collections.Generic.SortedList`2/EnumeratorMode<TKey,TValue>)
extern "C" void Enumerator__ctor_m3_2233_gshared (Enumerator_t3_286 * __this, SortedList_2_t3_248 * ___host, int32_t ___mode, const MethodInfo* method);
#define Enumerator__ctor_m3_2233(__this, ___host, ___mode, method) (( void (*) (Enumerator_t3_286 *, SortedList_2_t3_248 *, int32_t, const MethodInfo*))Enumerator__ctor_m3_2233_gshared)(__this, ___host, ___mode, method)
// System.Void System.Collections.Generic.SortedList`2/Enumerator<System.Int32,ExifLibrary.IFD>::.cctor()
extern "C" void Enumerator__cctor_m3_2234_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Enumerator__cctor_m3_2234(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))Enumerator__cctor_m3_2234_gshared)(__this /* static, unused */, method)
// System.Void System.Collections.Generic.SortedList`2/Enumerator<System.Int32,ExifLibrary.IFD>::Reset()
extern "C" void Enumerator_Reset_m3_2235_gshared (Enumerator_t3_286 * __this, const MethodInfo* method);
#define Enumerator_Reset_m3_2235(__this, method) (( void (*) (Enumerator_t3_286 *, const MethodInfo*))Enumerator_Reset_m3_2235_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedList`2/Enumerator<System.Int32,ExifLibrary.IFD>::MoveNext()
extern "C" bool Enumerator_MoveNext_m3_2236_gshared (Enumerator_t3_286 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3_2236(__this, method) (( bool (*) (Enumerator_t3_286 *, const MethodInfo*))Enumerator_MoveNext_m3_2236_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.SortedList`2/Enumerator<System.Int32,ExifLibrary.IFD>::get_Entry()
extern "C" DictionaryEntry_t1_284  Enumerator_get_Entry_m3_2237_gshared (Enumerator_t3_286 * __this, const MethodInfo* method);
#define Enumerator_get_Entry_m3_2237(__this, method) (( DictionaryEntry_t1_284  (*) (Enumerator_t3_286 *, const MethodInfo*))Enumerator_get_Entry_m3_2237_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2/Enumerator<System.Int32,ExifLibrary.IFD>::get_Key()
extern "C" Object_t * Enumerator_get_Key_m3_2238_gshared (Enumerator_t3_286 * __this, const MethodInfo* method);
#define Enumerator_get_Key_m3_2238(__this, method) (( Object_t * (*) (Enumerator_t3_286 *, const MethodInfo*))Enumerator_get_Key_m3_2238_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2/Enumerator<System.Int32,ExifLibrary.IFD>::get_Value()
extern "C" Object_t * Enumerator_get_Value_m3_2239_gshared (Enumerator_t3_286 * __this, const MethodInfo* method);
#define Enumerator_get_Value_m3_2239(__this, method) (( Object_t * (*) (Enumerator_t3_286 *, const MethodInfo*))Enumerator_get_Value_m3_2239_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2/Enumerator<System.Int32,ExifLibrary.IFD>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m3_2240_gshared (Enumerator_t3_286 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3_2240(__this, method) (( Object_t * (*) (Enumerator_t3_286 *, const MethodInfo*))Enumerator_get_Current_m3_2240_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2/Enumerator<System.Int32,ExifLibrary.IFD>::Clone()
extern "C" Object_t * Enumerator_Clone_m3_2241_gshared (Enumerator_t3_286 * __this, const MethodInfo* method);
#define Enumerator_Clone_m3_2241(__this, method) (( Object_t * (*) (Enumerator_t3_286 *, const MethodInfo*))Enumerator_Clone_m3_2241_gshared)(__this, method)
