﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

extern "C" void DESCUNION_t1_782_marshal(const DESCUNION_t1_782& unmarshaled, DESCUNION_t1_782_marshaled& marshaled);
extern "C" void DESCUNION_t1_782_marshal_back(const DESCUNION_t1_782_marshaled& marshaled, DESCUNION_t1_782& unmarshaled);
extern "C" void DESCUNION_t1_782_marshal_cleanup(DESCUNION_t1_782_marshaled& marshaled);
