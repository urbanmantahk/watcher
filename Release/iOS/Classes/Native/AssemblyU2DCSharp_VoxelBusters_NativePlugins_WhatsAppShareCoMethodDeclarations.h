﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.WhatsAppShareComposer
struct WhatsAppShareComposer_t8_285;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// UnityEngine.Texture2D
struct Texture2D_t6_33;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.WhatsAppShareComposer::.ctor()
extern "C" void WhatsAppShareComposer__ctor_m8_1686 (WhatsAppShareComposer_t8_285 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.WhatsAppShareComposer::get_Text()
extern "C" String_t* WhatsAppShareComposer_get_Text_m8_1687 (WhatsAppShareComposer_t8_285 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.WhatsAppShareComposer::set_Text(System.String)
extern "C" void WhatsAppShareComposer_set_Text_m8_1688 (WhatsAppShareComposer_t8_285 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] VoxelBusters.NativePlugins.WhatsAppShareComposer::get_ImageData()
extern "C" ByteU5BU5D_t1_109* WhatsAppShareComposer_get_ImageData_m8_1689 (WhatsAppShareComposer_t8_285 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.WhatsAppShareComposer::set_ImageData(System.Byte[])
extern "C" void WhatsAppShareComposer_set_ImageData_m8_1690 (WhatsAppShareComposer_t8_285 * __this, ByteU5BU5D_t1_109* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.NativePlugins.WhatsAppShareComposer::get_IsReadyToShowView()
extern "C" bool WhatsAppShareComposer_get_IsReadyToShowView_m8_1691 (WhatsAppShareComposer_t8_285 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.WhatsAppShareComposer::AttachImage(UnityEngine.Texture2D)
extern "C" void WhatsAppShareComposer_AttachImage_m8_1692 (WhatsAppShareComposer_t8_285 * __this, Texture2D_t6_33 * ____texture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
