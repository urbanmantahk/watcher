﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

// System.UInt32 ExifLibrary.MathEx::GCD(System.UInt32,System.UInt32)
extern "C" uint32_t MathEx_GCD_m8_789 (Object_t * __this /* static, unused */, uint32_t ___a, uint32_t ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
