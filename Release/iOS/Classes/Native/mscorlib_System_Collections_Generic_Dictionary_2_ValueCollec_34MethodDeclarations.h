﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_8MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,VoxelBusters.NativePlugins.WebView>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m1_27809(__this, ___dictionary, method) (( void (*) (ValueCollection_t1_2761 *, Dictionary_2_t1_1912 *, const MethodInfo*))ValueCollection__ctor_m1_16008_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,VoxelBusters.NativePlugins.WebView>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1_27810(__this, ___item, method) (( void (*) (ValueCollection_t1_2761 *, WebView_t8_191 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1_16009_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,VoxelBusters.NativePlugins.WebView>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1_27811(__this, method) (( void (*) (ValueCollection_t1_2761 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1_16010_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,VoxelBusters.NativePlugins.WebView>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1_27812(__this, ___item, method) (( bool (*) (ValueCollection_t1_2761 *, WebView_t8_191 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1_16011_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,VoxelBusters.NativePlugins.WebView>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1_27813(__this, ___item, method) (( bool (*) (ValueCollection_t1_2761 *, WebView_t8_191 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1_16012_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,VoxelBusters.NativePlugins.WebView>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1_27814(__this, method) (( Object_t* (*) (ValueCollection_t1_2761 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1_16013_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,VoxelBusters.NativePlugins.WebView>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m1_27815(__this, ___array, ___index, method) (( void (*) (ValueCollection_t1_2761 *, Array_t *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m1_16014_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.String,VoxelBusters.NativePlugins.WebView>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1_27816(__this, method) (( Object_t * (*) (ValueCollection_t1_2761 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1_16015_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,VoxelBusters.NativePlugins.WebView>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1_27817(__this, method) (( bool (*) (ValueCollection_t1_2761 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1_16016_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,VoxelBusters.NativePlugins.WebView>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1_27818(__this, method) (( bool (*) (ValueCollection_t1_2761 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1_16017_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.String,VoxelBusters.NativePlugins.WebView>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m1_27819(__this, method) (( Object_t * (*) (ValueCollection_t1_2761 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1_16018_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,VoxelBusters.NativePlugins.WebView>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m1_27820(__this, ___array, ___index, method) (( void (*) (ValueCollection_t1_2761 *, WebViewU5BU5D_t8_385*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1_16019_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,VoxelBusters.NativePlugins.WebView>::GetEnumerator()
#define ValueCollection_GetEnumerator_m1_27821(__this, method) (( Enumerator_t1_2903  (*) (ValueCollection_t1_2761 *, const MethodInfo*))ValueCollection_GetEnumerator_m1_16020_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.String,VoxelBusters.NativePlugins.WebView>::get_Count()
#define ValueCollection_get_Count_m1_27822(__this, method) (( int32_t (*) (ValueCollection_t1_2761 *, const MethodInfo*))ValueCollection_get_Count_m1_16021_gshared)(__this, method)
