﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Serialization.FormatterServices
struct FormatterServices_t1_1076;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Object
struct Object_t;
// System.Reflection.MemberInfo[]
struct MemberInfoU5BU5D_t1_1054;
// System.Type
struct Type_t;
// System.Collections.ArrayList
struct ArrayList_t1_170;
// System.Reflection.Assembly
struct Assembly_t1_467;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_TypeFilterL.h"

// System.Void System.Runtime.Serialization.FormatterServices::.ctor()
extern "C" void FormatterServices__ctor_m1_9524 (FormatterServices_t1_1076 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Runtime.Serialization.FormatterServices::GetObjectData(System.Object,System.Reflection.MemberInfo[])
extern "C" ObjectU5BU5D_t1_272* FormatterServices_GetObjectData_m1_9525 (Object_t * __this /* static, unused */, Object_t * ___obj, MemberInfoU5BU5D_t1_1054* ___members, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MemberInfo[] System.Runtime.Serialization.FormatterServices::GetSerializableMembers(System.Type)
extern "C" MemberInfoU5BU5D_t1_1054* FormatterServices_GetSerializableMembers_m1_9526 (Object_t * __this /* static, unused */, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MemberInfo[] System.Runtime.Serialization.FormatterServices::GetSerializableMembers(System.Type,System.Runtime.Serialization.StreamingContext)
extern "C" MemberInfoU5BU5D_t1_1054* FormatterServices_GetSerializableMembers_m1_9527 (Object_t * __this /* static, unused */, Type_t * ___type, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.FormatterServices::GetFields(System.Type,System.Type,System.Collections.ArrayList)
extern "C" void FormatterServices_GetFields_m1_9528 (Object_t * __this /* static, unused */, Type_t * ___reflectedType, Type_t * ___type, ArrayList_t1_170 * ___fields, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Runtime.Serialization.FormatterServices::GetTypeFromAssembly(System.Reflection.Assembly,System.String)
extern "C" Type_t * FormatterServices_GetTypeFromAssembly_m1_9529 (Object_t * __this /* static, unused */, Assembly_t1_467 * ___assem, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Serialization.FormatterServices::GetUninitializedObject(System.Type)
extern "C" Object_t * FormatterServices_GetUninitializedObject_m1_9530 (Object_t * __this /* static, unused */, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Serialization.FormatterServices::PopulateObjectMembers(System.Object,System.Reflection.MemberInfo[],System.Object[])
extern "C" Object_t * FormatterServices_PopulateObjectMembers_m1_9531 (Object_t * __this /* static, unused */, Object_t * ___obj, MemberInfoU5BU5D_t1_1054* ___members, ObjectU5BU5D_t1_272* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.FormatterServices::CheckTypeSecurity(System.Type,System.Runtime.Serialization.Formatters.TypeFilterLevel)
extern "C" void FormatterServices_CheckTypeSecurity_m1_9532 (Object_t * __this /* static, unused */, Type_t * ___t, int32_t ___securityLevel, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.FormatterServices::CheckNotAssignable(System.Type,System.Type)
extern "C" void FormatterServices_CheckNotAssignable_m1_9533 (Object_t * __this /* static, unused */, Type_t * ___basetype, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Serialization.FormatterServices::GetSafeUninitializedObject(System.Type)
extern "C" Object_t * FormatterServices_GetSafeUninitializedObject_m1_9534 (Object_t * __this /* static, unused */, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
