﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_IO_IsolatedStorage_IsolatedStorageScope.h"

// System.IO.IsolatedStorage.IsolatedStorageScope
struct  IsolatedStorageScope_t1_403 
{
	// System.Int32 System.IO.IsolatedStorage.IsolatedStorageScope::value__
	int32_t ___value___1;
};
