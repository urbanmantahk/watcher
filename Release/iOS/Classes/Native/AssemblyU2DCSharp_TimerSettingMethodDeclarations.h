﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TimerSetting
struct TimerSetting_t8_13;
// System.String
struct String_t;
// VoxelBusters.NativePlugins.CrossPlatformNotification
struct CrossPlatformNotification_t8_259;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Action
struct Action_t5_11;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eNotificationRe.h"

// System.Void TimerSetting::.ctor()
extern "C" void TimerSetting__ctor_m8_103 (TimerSetting_t8_13 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimerSetting::Start()
extern "C" void TimerSetting_Start_m8_104 (TimerSetting_t8_13 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TimerSetting::CheckChangable()
extern "C" bool TimerSetting_CheckChangable_m8_105 (TimerSetting_t8_13 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimerSetting::UpdateAMHour()
extern "C" void TimerSetting_UpdateAMHour_m8_106 (TimerSetting_t8_13 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimerSetting::UpdateAMMins()
extern "C" void TimerSetting_UpdateAMMins_m8_107 (TimerSetting_t8_13 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimerSetting::UpdatePMHour()
extern "C" void TimerSetting_UpdatePMHour_m8_108 (TimerSetting_t8_13 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimerSetting::UpdatePMMins()
extern "C" void TimerSetting_UpdatePMMins_m8_109 (TimerSetting_t8_13 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimerSetting::DisplayTimerSet()
extern "C" void TimerSetting_DisplayTimerSet_m8_110 (TimerSetting_t8_13 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimerSetting::LoadAMTimer()
extern "C" void TimerSetting_LoadAMTimer_m8_111 (TimerSetting_t8_13 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimerSetting::LoadPMTimer()
extern "C" void TimerSetting_LoadPMTimer_m8_112 (TimerSetting_t8_13 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimerSetting::SetTimer()
extern "C" void TimerSetting_SetTimer_m8_113 (TimerSetting_t8_13 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimerSetting::CancelAllLocalNotifications()
extern "C" void TimerSetting_CancelAllLocalNotifications_m8_114 (TimerSetting_t8_13 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String TimerSetting::ScheduleLocalNotification(VoxelBusters.NativePlugins.CrossPlatformNotification)
extern "C" String_t* TimerSetting_ScheduleLocalNotification_m8_115 (TimerSetting_t8_13 * __this, CrossPlatformNotification_t8_259 * ____notification, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.NativePlugins.CrossPlatformNotification TimerSetting::CreateNotification(System.String,VoxelBusters.NativePlugins.eNotificationRepeatInterval)
extern "C" CrossPlatformNotification_t8_259 * TimerSetting_CreateNotification_m8_116 (TimerSetting_t8_13 * __this, String_t* ____time, int32_t ____repeatInterval, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator TimerSetting::UpdateTimerToServer(System.Action)
extern "C" Object_t * TimerSetting_UpdateTimerToServer_m8_117 (TimerSetting_t8_13 * __this, Action_t5_11 * ____action, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimerSetting::DisplayError(System.String)
extern "C" void TimerSetting_DisplayError_m8_118 (TimerSetting_t8_13 * __this, String_t* ____msg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimerSetting::Logout()
extern "C" void TimerSetting_Logout_m8_119 (TimerSetting_t8_13 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimerSetting::<SetTimer>m__1()
extern "C" void TimerSetting_U3CSetTimerU3Em__1_m8_120 (TimerSetting_t8_13 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
