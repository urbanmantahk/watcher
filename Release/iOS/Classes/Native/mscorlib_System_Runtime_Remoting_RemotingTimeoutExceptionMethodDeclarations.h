﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.RemotingTimeoutException
struct RemotingTimeoutException_t1_1029;
// System.String
struct String_t;
// System.Exception
struct Exception_t1_33;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.Runtime.Remoting.RemotingTimeoutException::.ctor()
extern "C" void RemotingTimeoutException__ctor_m1_9238 (RemotingTimeoutException_t1_1029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.RemotingTimeoutException::.ctor(System.String)
extern "C" void RemotingTimeoutException__ctor_m1_9239 (RemotingTimeoutException_t1_1029 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.RemotingTimeoutException::.ctor(System.String,System.Exception)
extern "C" void RemotingTimeoutException__ctor_m1_9240 (RemotingTimeoutException_t1_1029 * __this, String_t* ___message, Exception_t1_33 * ___InnerException, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.RemotingTimeoutException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void RemotingTimeoutException__ctor_m1_9241 (RemotingTimeoutException_t1_1029 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
