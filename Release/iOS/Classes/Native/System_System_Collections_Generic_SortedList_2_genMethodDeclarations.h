﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>
struct SortedList_2_t3_248;
// System.Collections.Generic.IComparer`1<System.Int32>
struct IComparer_1_t1_2682;
// System.Object
struct Object_t;
// System.Collections.ICollection
struct ICollection_t1_280;
// System.Collections.Generic.ICollection`1<System.Int32>
struct ICollection_1_t1_2771;
// System.Collections.Generic.ICollection`1<ExifLibrary.IFD>
struct ICollection_1_t1_2880;
// System.Collections.Generic.KeyValuePair`2<System.Int32,ExifLibrary.IFD>[]
struct KeyValuePair_2U5BU5D_t1_2680;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,ExifLibrary.IFD>>
struct IEnumerator_1_t1_2881;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1_863;
// System.Array
struct Array_t;
// System.Collections.Generic.IList`1<System.Int32>
struct IList_1_t1_1922;
// System.Collections.Generic.IList`1<ExifLibrary.IFD>
struct IList_1_t1_1923;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_21.h"
#include "AssemblyU2DCSharp_ExifLibrary_IFD.h"
#include "System_System_Collections_Generic_SortedList_2_EnumeratorMod_0.h"

// System.Void System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::.ctor()
extern "C" void SortedList_2__ctor_m3_1809_gshared (SortedList_2_t3_248 * __this, const MethodInfo* method);
#define SortedList_2__ctor_m3_1809(__this, method) (( void (*) (SortedList_2_t3_248 *, const MethodInfo*))SortedList_2__ctor_m3_1809_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::.ctor(System.Int32,System.Collections.Generic.IComparer`1<TKey>)
extern "C" void SortedList_2__ctor_m3_2121_gshared (SortedList_2_t3_248 * __this, int32_t ___capacity, Object_t* ___comparer, const MethodInfo* method);
#define SortedList_2__ctor_m3_2121(__this, ___capacity, ___comparer, method) (( void (*) (SortedList_2_t3_248 *, int32_t, Object_t*, const MethodInfo*))SortedList_2__ctor_m3_2121_gshared)(__this, ___capacity, ___comparer, method)
// System.Void System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::.cctor()
extern "C" void SortedList_2__cctor_m3_2122_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define SortedList_2__cctor_m3_2122(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))SortedList_2__cctor_m3_2122_gshared)(__this /* static, unused */, method)
// System.Boolean System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool SortedList_2_System_Collections_ICollection_get_IsSynchronized_m3_2123_gshared (SortedList_2_t3_248 * __this, const MethodInfo* method);
#define SortedList_2_System_Collections_ICollection_get_IsSynchronized_m3_2123(__this, method) (( bool (*) (SortedList_2_t3_248 *, const MethodInfo*))SortedList_2_System_Collections_ICollection_get_IsSynchronized_m3_2123_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * SortedList_2_System_Collections_ICollection_get_SyncRoot_m3_2124_gshared (SortedList_2_t3_248 * __this, const MethodInfo* method);
#define SortedList_2_System_Collections_ICollection_get_SyncRoot_m3_2124(__this, method) (( Object_t * (*) (SortedList_2_t3_248 *, const MethodInfo*))SortedList_2_System_Collections_ICollection_get_SyncRoot_m3_2124_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::System.Collections.IDictionary.get_IsFixedSize()
extern "C" bool SortedList_2_System_Collections_IDictionary_get_IsFixedSize_m3_2125_gshared (SortedList_2_t3_248 * __this, const MethodInfo* method);
#define SortedList_2_System_Collections_IDictionary_get_IsFixedSize_m3_2125(__this, method) (( bool (*) (SortedList_2_t3_248 *, const MethodInfo*))SortedList_2_System_Collections_IDictionary_get_IsFixedSize_m3_2125_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::System.Collections.IDictionary.get_IsReadOnly()
extern "C" bool SortedList_2_System_Collections_IDictionary_get_IsReadOnly_m3_2126_gshared (SortedList_2_t3_248 * __this, const MethodInfo* method);
#define SortedList_2_System_Collections_IDictionary_get_IsReadOnly_m3_2126(__this, method) (( bool (*) (SortedList_2_t3_248 *, const MethodInfo*))SortedList_2_System_Collections_IDictionary_get_IsReadOnly_m3_2126_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * SortedList_2_System_Collections_IDictionary_get_Item_m3_2127_gshared (SortedList_2_t3_248 * __this, Object_t * ___key, const MethodInfo* method);
#define SortedList_2_System_Collections_IDictionary_get_Item_m3_2127(__this, ___key, method) (( Object_t * (*) (SortedList_2_t3_248 *, Object_t *, const MethodInfo*))SortedList_2_System_Collections_IDictionary_get_Item_m3_2127_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void SortedList_2_System_Collections_IDictionary_set_Item_m3_2128_gshared (SortedList_2_t3_248 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define SortedList_2_System_Collections_IDictionary_set_Item_m3_2128(__this, ___key, ___value, method) (( void (*) (SortedList_2_t3_248 *, Object_t *, Object_t *, const MethodInfo*))SortedList_2_System_Collections_IDictionary_set_Item_m3_2128_gshared)(__this, ___key, ___value, method)
// System.Collections.ICollection System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::System.Collections.IDictionary.get_Keys()
extern "C" Object_t * SortedList_2_System_Collections_IDictionary_get_Keys_m3_2129_gshared (SortedList_2_t3_248 * __this, const MethodInfo* method);
#define SortedList_2_System_Collections_IDictionary_get_Keys_m3_2129(__this, method) (( Object_t * (*) (SortedList_2_t3_248 *, const MethodInfo*))SortedList_2_System_Collections_IDictionary_get_Keys_m3_2129_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::System.Collections.IDictionary.get_Values()
extern "C" Object_t * SortedList_2_System_Collections_IDictionary_get_Values_m3_2130_gshared (SortedList_2_t3_248 * __this, const MethodInfo* method);
#define SortedList_2_System_Collections_IDictionary_get_Values_m3_2130(__this, method) (( Object_t * (*) (SortedList_2_t3_248 *, const MethodInfo*))SortedList_2_System_Collections_IDictionary_get_Values_m3_2130_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C" Object_t* SortedList_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3_2131_gshared (SortedList_2_t3_248 * __this, const MethodInfo* method);
#define SortedList_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3_2131(__this, method) (( Object_t* (*) (SortedList_2_t3_248 *, const MethodInfo*))SortedList_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3_2131_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C" Object_t* SortedList_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m3_2132_gshared (SortedList_2_t3_248 * __this, const MethodInfo* method);
#define SortedList_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m3_2132(__this, method) (( Object_t* (*) (SortedList_2_t3_248 *, const MethodInfo*))SortedList_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m3_2132_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3_2133_gshared (SortedList_2_t3_248 * __this, const MethodInfo* method);
#define SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3_2133(__this, method) (( bool (*) (SortedList_2_t3_248 *, const MethodInfo*))SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3_2133_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Clear()
extern "C" void SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Clear_m3_2134_gshared (SortedList_2_t3_248 * __this, const MethodInfo* method);
#define SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Clear_m3_2134(__this, method) (( void (*) (SortedList_2_t3_248 *, const MethodInfo*))SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Clear_m3_2134_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3_2135_gshared (SortedList_2_t3_248 * __this, KeyValuePair_2U5BU5D_t1_2680* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3_2135(__this, ___array, ___arrayIndex, method) (( void (*) (SortedList_2_t3_248 *, KeyValuePair_2U5BU5D_t1_2680*, int32_t, const MethodInfo*))SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3_2135_gshared)(__this, ___array, ___arrayIndex, method)
// System.Void System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3_2136_gshared (SortedList_2_t3_248 * __this, KeyValuePair_2_t1_2681  ___keyValuePair, const MethodInfo* method);
#define SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3_2136(__this, ___keyValuePair, method) (( void (*) (SortedList_2_t3_248 *, KeyValuePair_2_t1_2681 , const MethodInfo*))SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3_2136_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3_2137_gshared (SortedList_2_t3_248 * __this, KeyValuePair_2_t1_2681  ___keyValuePair, const MethodInfo* method);
#define SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3_2137(__this, ___keyValuePair, method) (( bool (*) (SortedList_2_t3_248 *, KeyValuePair_2_t1_2681 , const MethodInfo*))SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3_2137_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3_2138_gshared (SortedList_2_t3_248 * __this, KeyValuePair_2_t1_2681  ___keyValuePair, const MethodInfo* method);
#define SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3_2138(__this, ___keyValuePair, method) (( bool (*) (SortedList_2_t3_248 *, KeyValuePair_2_t1_2681 , const MethodInfo*))SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3_2138_gshared)(__this, ___keyValuePair, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* SortedList_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3_2139_gshared (SortedList_2_t3_248 * __this, const MethodInfo* method);
#define SortedList_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3_2139(__this, method) (( Object_t* (*) (SortedList_2_t3_248 *, const MethodInfo*))SortedList_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3_2139_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * SortedList_2_System_Collections_IEnumerable_GetEnumerator_m3_2140_gshared (SortedList_2_t3_248 * __this, const MethodInfo* method);
#define SortedList_2_System_Collections_IEnumerable_GetEnumerator_m3_2140(__this, method) (( Object_t * (*) (SortedList_2_t3_248 *, const MethodInfo*))SortedList_2_System_Collections_IEnumerable_GetEnumerator_m3_2140_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void SortedList_2_System_Collections_IDictionary_Add_m3_2141_gshared (SortedList_2_t3_248 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define SortedList_2_System_Collections_IDictionary_Add_m3_2141(__this, ___key, ___value, method) (( void (*) (SortedList_2_t3_248 *, Object_t *, Object_t *, const MethodInfo*))SortedList_2_System_Collections_IDictionary_Add_m3_2141_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool SortedList_2_System_Collections_IDictionary_Contains_m3_2142_gshared (SortedList_2_t3_248 * __this, Object_t * ___key, const MethodInfo* method);
#define SortedList_2_System_Collections_IDictionary_Contains_m3_2142(__this, ___key, method) (( bool (*) (SortedList_2_t3_248 *, Object_t *, const MethodInfo*))SortedList_2_System_Collections_IDictionary_Contains_m3_2142_gshared)(__this, ___key, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * SortedList_2_System_Collections_IDictionary_GetEnumerator_m3_2143_gshared (SortedList_2_t3_248 * __this, const MethodInfo* method);
#define SortedList_2_System_Collections_IDictionary_GetEnumerator_m3_2143(__this, method) (( Object_t * (*) (SortedList_2_t3_248 *, const MethodInfo*))SortedList_2_System_Collections_IDictionary_GetEnumerator_m3_2143_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void SortedList_2_System_Collections_IDictionary_Remove_m3_2144_gshared (SortedList_2_t3_248 * __this, Object_t * ___key, const MethodInfo* method);
#define SortedList_2_System_Collections_IDictionary_Remove_m3_2144(__this, ___key, method) (( void (*) (SortedList_2_t3_248 *, Object_t *, const MethodInfo*))SortedList_2_System_Collections_IDictionary_Remove_m3_2144_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void SortedList_2_System_Collections_ICollection_CopyTo_m3_2145_gshared (SortedList_2_t3_248 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define SortedList_2_System_Collections_ICollection_CopyTo_m3_2145(__this, ___array, ___arrayIndex, method) (( void (*) (SortedList_2_t3_248 *, Array_t *, int32_t, const MethodInfo*))SortedList_2_System_Collections_ICollection_CopyTo_m3_2145_gshared)(__this, ___array, ___arrayIndex, method)
// System.Int32 System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::get_Count()
extern "C" int32_t SortedList_2_get_Count_m3_2146_gshared (SortedList_2_t3_248 * __this, const MethodInfo* method);
#define SortedList_2_get_Count_m3_2146(__this, method) (( int32_t (*) (SortedList_2_t3_248 *, const MethodInfo*))SortedList_2_get_Count_m3_2146_gshared)(__this, method)
// TValue System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::get_Item(TKey)
extern "C" int32_t SortedList_2_get_Item_m3_2147_gshared (SortedList_2_t3_248 * __this, int32_t ___key, const MethodInfo* method);
#define SortedList_2_get_Item_m3_2147(__this, ___key, method) (( int32_t (*) (SortedList_2_t3_248 *, int32_t, const MethodInfo*))SortedList_2_get_Item_m3_2147_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::set_Item(TKey,TValue)
extern "C" void SortedList_2_set_Item_m3_2148_gshared (SortedList_2_t3_248 * __this, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define SortedList_2_set_Item_m3_2148(__this, ___key, ___value, method) (( void (*) (SortedList_2_t3_248 *, int32_t, int32_t, const MethodInfo*))SortedList_2_set_Item_m3_2148_gshared)(__this, ___key, ___value, method)
// System.Int32 System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::get_Capacity()
extern "C" int32_t SortedList_2_get_Capacity_m3_2149_gshared (SortedList_2_t3_248 * __this, const MethodInfo* method);
#define SortedList_2_get_Capacity_m3_2149(__this, method) (( int32_t (*) (SortedList_2_t3_248 *, const MethodInfo*))SortedList_2_get_Capacity_m3_2149_gshared)(__this, method)
// System.Collections.Generic.IList`1<TKey> System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::get_Keys()
extern "C" Object_t* SortedList_2_get_Keys_m3_1810_gshared (SortedList_2_t3_248 * __this, const MethodInfo* method);
#define SortedList_2_get_Keys_m3_1810(__this, method) (( Object_t* (*) (SortedList_2_t3_248 *, const MethodInfo*))SortedList_2_get_Keys_m3_1810_gshared)(__this, method)
// System.Collections.Generic.IList`1<TValue> System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::get_Values()
extern "C" Object_t* SortedList_2_get_Values_m3_1811_gshared (SortedList_2_t3_248 * __this, const MethodInfo* method);
#define SortedList_2_get_Values_m3_1811(__this, method) (( Object_t* (*) (SortedList_2_t3_248 *, const MethodInfo*))SortedList_2_get_Values_m3_1811_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::Add(TKey,TValue)
extern "C" void SortedList_2_Add_m3_2150_gshared (SortedList_2_t3_248 * __this, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define SortedList_2_Add_m3_2150(__this, ___key, ___value, method) (( void (*) (SortedList_2_t3_248 *, int32_t, int32_t, const MethodInfo*))SortedList_2_Add_m3_2150_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::ContainsKey(TKey)
extern "C" bool SortedList_2_ContainsKey_m3_2151_gshared (SortedList_2_t3_248 * __this, int32_t ___key, const MethodInfo* method);
#define SortedList_2_ContainsKey_m3_2151(__this, ___key, method) (( bool (*) (SortedList_2_t3_248 *, int32_t, const MethodInfo*))SortedList_2_ContainsKey_m3_2151_gshared)(__this, ___key, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::GetEnumerator()
extern "C" Object_t* SortedList_2_GetEnumerator_m3_2152_gshared (SortedList_2_t3_248 * __this, const MethodInfo* method);
#define SortedList_2_GetEnumerator_m3_2152(__this, method) (( Object_t* (*) (SortedList_2_t3_248 *, const MethodInfo*))SortedList_2_GetEnumerator_m3_2152_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::Remove(TKey)
extern "C" bool SortedList_2_Remove_m3_2153_gshared (SortedList_2_t3_248 * __this, int32_t ___key, const MethodInfo* method);
#define SortedList_2_Remove_m3_2153(__this, ___key, method) (( bool (*) (SortedList_2_t3_248 *, int32_t, const MethodInfo*))SortedList_2_Remove_m3_2153_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::Clear()
extern "C" void SortedList_2_Clear_m3_2154_gshared (SortedList_2_t3_248 * __this, const MethodInfo* method);
#define SortedList_2_Clear_m3_2154(__this, method) (( void (*) (SortedList_2_t3_248 *, const MethodInfo*))SortedList_2_Clear_m3_2154_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::RemoveAt(System.Int32)
extern "C" void SortedList_2_RemoveAt_m3_1812_gshared (SortedList_2_t3_248 * __this, int32_t ___index, const MethodInfo* method);
#define SortedList_2_RemoveAt_m3_1812(__this, ___index, method) (( void (*) (SortedList_2_t3_248 *, int32_t, const MethodInfo*))SortedList_2_RemoveAt_m3_1812_gshared)(__this, ___index, method)
// System.Int32 System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::IndexOfKey(TKey)
extern "C" int32_t SortedList_2_IndexOfKey_m3_2155_gshared (SortedList_2_t3_248 * __this, int32_t ___key, const MethodInfo* method);
#define SortedList_2_IndexOfKey_m3_2155(__this, ___key, method) (( int32_t (*) (SortedList_2_t3_248 *, int32_t, const MethodInfo*))SortedList_2_IndexOfKey_m3_2155_gshared)(__this, ___key, method)
// System.Int32 System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::IndexOfValue(TValue)
extern "C" int32_t SortedList_2_IndexOfValue_m3_2156_gshared (SortedList_2_t3_248 * __this, int32_t ___value, const MethodInfo* method);
#define SortedList_2_IndexOfValue_m3_2156(__this, ___value, method) (( int32_t (*) (SortedList_2_t3_248 *, int32_t, const MethodInfo*))SortedList_2_IndexOfValue_m3_2156_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::TryGetValue(TKey,TValue&)
extern "C" bool SortedList_2_TryGetValue_m3_2157_gshared (SortedList_2_t3_248 * __this, int32_t ___key, int32_t* ___value, const MethodInfo* method);
#define SortedList_2_TryGetValue_m3_2157(__this, ___key, ___value, method) (( bool (*) (SortedList_2_t3_248 *, int32_t, int32_t*, const MethodInfo*))SortedList_2_TryGetValue_m3_2157_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::EnsureCapacity(System.Int32,System.Int32)
extern "C" void SortedList_2_EnsureCapacity_m3_2158_gshared (SortedList_2_t3_248 * __this, int32_t ___n, int32_t ___free, const MethodInfo* method);
#define SortedList_2_EnsureCapacity_m3_2158(__this, ___n, ___free, method) (( void (*) (SortedList_2_t3_248 *, int32_t, int32_t, const MethodInfo*))SortedList_2_EnsureCapacity_m3_2158_gshared)(__this, ___n, ___free, method)
// System.Void System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::PutImpl(TKey,TValue,System.Boolean)
extern "C" void SortedList_2_PutImpl_m3_2159_gshared (SortedList_2_t3_248 * __this, int32_t ___key, int32_t ___value, bool ___overwrite, const MethodInfo* method);
#define SortedList_2_PutImpl_m3_2159(__this, ___key, ___value, ___overwrite, method) (( void (*) (SortedList_2_t3_248 *, int32_t, int32_t, bool, const MethodInfo*))SortedList_2_PutImpl_m3_2159_gshared)(__this, ___key, ___value, ___overwrite, method)
// System.Void System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::Init(System.Collections.Generic.IComparer`1<TKey>,System.Int32,System.Boolean)
extern "C" void SortedList_2_Init_m3_2160_gshared (SortedList_2_t3_248 * __this, Object_t* ___comparer, int32_t ___capacity, bool ___forceSize, const MethodInfo* method);
#define SortedList_2_Init_m3_2160(__this, ___comparer, ___capacity, ___forceSize, method) (( void (*) (SortedList_2_t3_248 *, Object_t*, int32_t, bool, const MethodInfo*))SortedList_2_Init_m3_2160_gshared)(__this, ___comparer, ___capacity, ___forceSize, method)
// System.Void System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::CopyToArray(System.Array,System.Int32,System.Collections.Generic.SortedList`2/EnumeratorMode<TKey,TValue>)
extern "C" void SortedList_2_CopyToArray_m3_2161_gshared (SortedList_2_t3_248 * __this, Array_t * ___arr, int32_t ___i, int32_t ___mode, const MethodInfo* method);
#define SortedList_2_CopyToArray_m3_2161(__this, ___arr, ___i, ___mode, method) (( void (*) (SortedList_2_t3_248 *, Array_t *, int32_t, int32_t, const MethodInfo*))SortedList_2_CopyToArray_m3_2161_gshared)(__this, ___arr, ___i, ___mode, method)
// System.Int32 System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::Find(TKey)
extern "C" int32_t SortedList_2_Find_m3_2162_gshared (SortedList_2_t3_248 * __this, int32_t ___key, const MethodInfo* method);
#define SortedList_2_Find_m3_2162(__this, ___key, method) (( int32_t (*) (SortedList_2_t3_248 *, int32_t, const MethodInfo*))SortedList_2_Find_m3_2162_gshared)(__this, ___key, method)
// TKey System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::ToKey(System.Object)
extern "C" int32_t SortedList_2_ToKey_m3_2163_gshared (SortedList_2_t3_248 * __this, Object_t * ___key, const MethodInfo* method);
#define SortedList_2_ToKey_m3_2163(__this, ___key, method) (( int32_t (*) (SortedList_2_t3_248 *, Object_t *, const MethodInfo*))SortedList_2_ToKey_m3_2163_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::ToValue(System.Object)
extern "C" int32_t SortedList_2_ToValue_m3_2164_gshared (SortedList_2_t3_248 * __this, Object_t * ___value, const MethodInfo* method);
#define SortedList_2_ToValue_m3_2164(__this, ___value, method) (( int32_t (*) (SortedList_2_t3_248 *, Object_t *, const MethodInfo*))SortedList_2_ToValue_m3_2164_gshared)(__this, ___value, method)
// TKey System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::KeyAt(System.Int32)
extern "C" int32_t SortedList_2_KeyAt_m3_2165_gshared (SortedList_2_t3_248 * __this, int32_t ___index, const MethodInfo* method);
#define SortedList_2_KeyAt_m3_2165(__this, ___index, method) (( int32_t (*) (SortedList_2_t3_248 *, int32_t, const MethodInfo*))SortedList_2_KeyAt_m3_2165_gshared)(__this, ___index, method)
// TValue System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::ValueAt(System.Int32)
extern "C" int32_t SortedList_2_ValueAt_m3_2166_gshared (SortedList_2_t3_248 * __this, int32_t ___index, const MethodInfo* method);
#define SortedList_2_ValueAt_m3_2166(__this, ___index, method) (( int32_t (*) (SortedList_2_t3_248 *, int32_t, const MethodInfo*))SortedList_2_ValueAt_m3_2166_gshared)(__this, ___index, method)
