﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Xml.DictionaryBase/<>c__Iterator3
struct U3CU3Ec__Iterator3_t4_88;
// Mono.Xml.DTDNode
struct DTDNode_t4_89;
// System.Object
struct Object_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Collections.Generic.IEnumerator`1<Mono.Xml.DTDNode>
struct IEnumerator_1_t1_1807;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Xml.DictionaryBase/<>c__Iterator3::.ctor()
extern "C" void U3CU3Ec__Iterator3__ctor_m4_150 (U3CU3Ec__Iterator3_t4_88 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Xml.DTDNode Mono.Xml.DictionaryBase/<>c__Iterator3::System.Collections.Generic.IEnumerator<Mono.Xml.DTDNode>.get_Current()
extern "C" DTDNode_t4_89 * U3CU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CMono_Xml_DTDNodeU3E_get_Current_m4_151 (U3CU3Ec__Iterator3_t4_88 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Mono.Xml.DictionaryBase/<>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m4_152 (U3CU3Ec__Iterator3_t4_88 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Mono.Xml.DictionaryBase/<>c__Iterator3::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * U3CU3Ec__Iterator3_System_Collections_IEnumerable_GetEnumerator_m4_153 (U3CU3Ec__Iterator3_t4_88 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<Mono.Xml.DTDNode> Mono.Xml.DictionaryBase/<>c__Iterator3::System.Collections.Generic.IEnumerable<Mono.Xml.DTDNode>.GetEnumerator()
extern "C" Object_t* U3CU3Ec__Iterator3_System_Collections_Generic_IEnumerableU3CMono_Xml_DTDNodeU3E_GetEnumerator_m4_154 (U3CU3Ec__Iterator3_t4_88 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Xml.DictionaryBase/<>c__Iterator3::MoveNext()
extern "C" bool U3CU3Ec__Iterator3_MoveNext_m4_155 (U3CU3Ec__Iterator3_t4_88 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Xml.DictionaryBase/<>c__Iterator3::Dispose()
extern "C" void U3CU3Ec__Iterator3_Dispose_m4_156 (U3CU3Ec__Iterator3_t4_88 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Xml.DictionaryBase/<>c__Iterator3::Reset()
extern "C" void U3CU3Ec__Iterator3_Reset_m4_157 (U3CU3Ec__Iterator3_t4_88 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
