﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.Emit.MethodBuilder
struct MethodBuilder_t1_501;
// System.Reflection.Emit.TypeBuilder
struct TypeBuilder_t1_481;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t1_31;
// System.Type[][]
struct TypeU5BU5DU5BU5D_t1_483;
// System.Reflection.ICustomAttributeProvider
struct ICustomAttributeProvider_t1_1686;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Reflection.ParameterInfo[]
struct ParameterInfoU5BU5D_t1_1685;
// System.Reflection.Module
struct Module_t1_495;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Object
struct Object_t;
// System.Reflection.Binder
struct Binder_t1_585;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Globalization.CultureInfo
struct CultureInfo_t1_277;
// System.Reflection.Emit.ILGenerator
struct ILGenerator_t1_480;
// System.Reflection.Emit.ParameterBuilder
struct ParameterBuilder_t1_545;
// System.Diagnostics.SymbolStore.ISymbolWriter
struct ISymbolWriter_t1_536;
// System.Reflection.Emit.CustomAttributeBuilder
struct CustomAttributeBuilder_t1_487;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t1_478;
// System.Security.PermissionSet
struct PermissionSet_t1_563;
// System.Reflection.Emit.UnmanagedMarshal
struct UnmanagedMarshal_t1_505;
// System.Exception
struct Exception_t1_33;
// System.Reflection.Emit.GenericTypeParameterBuilder[]
struct GenericTypeParameterBuilderU5BU5D_t1_529;
// System.String[]
struct StringU5BU5D_t1_238;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_MethodAttributes.h"
#include "mscorlib_System_Reflection_CallingConventions.h"
#include "mscorlib_System_Runtime_InteropServices_CallingConvention.h"
#include "mscorlib_System_Runtime_InteropServices_CharSet.h"
#include "mscorlib_System_Guid.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_RuntimeMethodHandle.h"
#include "mscorlib_System_Reflection_Emit_MethodToken.h"
#include "mscorlib_System_Reflection_MethodImplAttributes.h"
#include "mscorlib_System_Reflection_BindingFlags.h"
#include "mscorlib_System_Reflection_ParameterAttributes.h"
#include "mscorlib_System_Security_Permissions_SecurityAction.h"

// System.Void System.Reflection.Emit.MethodBuilder::.ctor(System.Reflection.Emit.TypeBuilder,System.String,System.Reflection.MethodAttributes,System.Reflection.CallingConventions,System.Type,System.Type[],System.Type[],System.Type[],System.Type[][],System.Type[][])
extern "C" void MethodBuilder__ctor_m1_6052 (MethodBuilder_t1_501 * __this, TypeBuilder_t1_481 * ___tb, String_t* ___name, int32_t ___attributes, int32_t ___callingConvention, Type_t * ___returnType, TypeU5BU5D_t1_31* ___returnModReq, TypeU5BU5D_t1_31* ___returnModOpt, TypeU5BU5D_t1_31* ___parameterTypes, TypeU5BU5DU5BU5D_t1_483* ___paramModReq, TypeU5BU5DU5BU5D_t1_483* ___paramModOpt, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.MethodBuilder::.ctor(System.Reflection.Emit.TypeBuilder,System.String,System.Reflection.MethodAttributes,System.Reflection.CallingConventions,System.Type,System.Type[],System.Type[],System.Type[],System.Type[][],System.Type[][],System.String,System.String,System.Runtime.InteropServices.CallingConvention,System.Runtime.InteropServices.CharSet)
extern "C" void MethodBuilder__ctor_m1_6053 (MethodBuilder_t1_501 * __this, TypeBuilder_t1_481 * ___tb, String_t* ___name, int32_t ___attributes, int32_t ___callingConvention, Type_t * ___returnType, TypeU5BU5D_t1_31* ___returnModReq, TypeU5BU5D_t1_31* ___returnModOpt, TypeU5BU5D_t1_31* ___parameterTypes, TypeU5BU5DU5BU5D_t1_483* ___paramModReq, TypeU5BU5DU5BU5D_t1_483* ___paramModOpt, String_t* ___dllName, String_t* ___entryName, int32_t ___nativeCConv, int32_t ___nativeCharset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.MethodBuilder::System.Runtime.InteropServices._MethodBuilder.GetIDsOfNames(System.Guid&,System.IntPtr,System.UInt32,System.UInt32,System.IntPtr)
extern "C" void MethodBuilder_System_Runtime_InteropServices__MethodBuilder_GetIDsOfNames_m1_6054 (MethodBuilder_t1_501 * __this, Guid_t1_319 * ___riid, IntPtr_t ___rgszNames, uint32_t ___cNames, uint32_t ___lcid, IntPtr_t ___rgDispId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.MethodBuilder::System.Runtime.InteropServices._MethodBuilder.GetTypeInfo(System.UInt32,System.UInt32,System.IntPtr)
extern "C" void MethodBuilder_System_Runtime_InteropServices__MethodBuilder_GetTypeInfo_m1_6055 (MethodBuilder_t1_501 * __this, uint32_t ___iTInfo, uint32_t ___lcid, IntPtr_t ___ppTInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.MethodBuilder::System.Runtime.InteropServices._MethodBuilder.GetTypeInfoCount(System.UInt32&)
extern "C" void MethodBuilder_System_Runtime_InteropServices__MethodBuilder_GetTypeInfoCount_m1_6056 (MethodBuilder_t1_501 * __this, uint32_t* ___pcTInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.MethodBuilder::System.Runtime.InteropServices._MethodBuilder.Invoke(System.UInt32,System.Guid&,System.UInt32,System.Int16,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr)
extern "C" void MethodBuilder_System_Runtime_InteropServices__MethodBuilder_Invoke_m1_6057 (MethodBuilder_t1_501 * __this, uint32_t ___dispIdMember, Guid_t1_319 * ___riid, uint32_t ___lcid, int16_t ___wFlags, IntPtr_t ___pDispParams, IntPtr_t ___pVarResult, IntPtr_t ___pExcepInfo, IntPtr_t ___puArgErr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.MethodBuilder::get_ContainsGenericParameters()
extern "C" bool MethodBuilder_get_ContainsGenericParameters_m1_6058 (MethodBuilder_t1_501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.MethodBuilder::get_InitLocals()
extern "C" bool MethodBuilder_get_InitLocals_m1_6059 (MethodBuilder_t1_501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.MethodBuilder::set_InitLocals(System.Boolean)
extern "C" void MethodBuilder_set_InitLocals_m1_6060 (MethodBuilder_t1_501 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.TypeBuilder System.Reflection.Emit.MethodBuilder::get_TypeBuilder()
extern "C" TypeBuilder_t1_481 * MethodBuilder_get_TypeBuilder_m1_6061 (MethodBuilder_t1_501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.RuntimeMethodHandle System.Reflection.Emit.MethodBuilder::get_MethodHandle()
extern "C" RuntimeMethodHandle_t1_479  MethodBuilder_get_MethodHandle_m1_6062 (MethodBuilder_t1_501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.MethodBuilder::get_ReturnType()
extern "C" Type_t * MethodBuilder_get_ReturnType_m1_6063 (MethodBuilder_t1_501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.MethodBuilder::get_ReflectedType()
extern "C" Type_t * MethodBuilder_get_ReflectedType_m1_6064 (MethodBuilder_t1_501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.MethodBuilder::get_DeclaringType()
extern "C" Type_t * MethodBuilder_get_DeclaringType_m1_6065 (MethodBuilder_t1_501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Emit.MethodBuilder::get_Name()
extern "C" String_t* MethodBuilder_get_Name_m1_6066 (MethodBuilder_t1_501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodAttributes System.Reflection.Emit.MethodBuilder::get_Attributes()
extern "C" int32_t MethodBuilder_get_Attributes_m1_6067 (MethodBuilder_t1_501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ICustomAttributeProvider System.Reflection.Emit.MethodBuilder::get_ReturnTypeCustomAttributes()
extern "C" Object_t * MethodBuilder_get_ReturnTypeCustomAttributes_m1_6068 (MethodBuilder_t1_501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.CallingConventions System.Reflection.Emit.MethodBuilder::get_CallingConvention()
extern "C" int32_t MethodBuilder_get_CallingConvention_m1_6069 (MethodBuilder_t1_501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Emit.MethodBuilder::get_Signature()
extern "C" String_t* MethodBuilder_get_Signature_m1_6070 (MethodBuilder_t1_501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.MethodBuilder::set_BestFitMapping(System.Boolean)
extern "C" void MethodBuilder_set_BestFitMapping_m1_6071 (MethodBuilder_t1_501 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.MethodBuilder::set_ThrowOnUnmappableChar(System.Boolean)
extern "C" void MethodBuilder_set_ThrowOnUnmappableChar_m1_6072 (MethodBuilder_t1_501 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.MethodBuilder::set_ExactSpelling(System.Boolean)
extern "C" void MethodBuilder_set_ExactSpelling_m1_6073 (MethodBuilder_t1_501 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.MethodBuilder::set_SetLastError(System.Boolean)
extern "C" void MethodBuilder_set_SetLastError_m1_6074 (MethodBuilder_t1_501 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.MethodToken System.Reflection.Emit.MethodBuilder::GetToken()
extern "C" MethodToken_t1_532  MethodBuilder_GetToken_m1_6075 (MethodBuilder_t1_501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Reflection.Emit.MethodBuilder::GetBaseDefinition()
extern "C" MethodInfo_t * MethodBuilder_GetBaseDefinition_m1_6076 (MethodBuilder_t1_501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodImplAttributes System.Reflection.Emit.MethodBuilder::GetMethodImplementationFlags()
extern "C" int32_t MethodBuilder_GetMethodImplementationFlags_m1_6077 (MethodBuilder_t1_501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ParameterInfo[] System.Reflection.Emit.MethodBuilder::GetParameters()
extern "C" ParameterInfoU5BU5D_t1_1685* MethodBuilder_GetParameters_m1_6078 (MethodBuilder_t1_501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.MethodBuilder::GetParameterCount()
extern "C" int32_t MethodBuilder_GetParameterCount_m1_6079 (MethodBuilder_t1_501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Module System.Reflection.Emit.MethodBuilder::GetModule()
extern "C" Module_t1_495 * MethodBuilder_GetModule_m1_6080 (MethodBuilder_t1_501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.MethodBuilder::CreateMethodBody(System.Byte[],System.Int32)
extern "C" void MethodBuilder_CreateMethodBody_m1_6081 (MethodBuilder_t1_501 * __this, ByteU5BU5D_t1_109* ___il, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Reflection.Emit.MethodBuilder::Invoke(System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo)
extern "C" Object_t * MethodBuilder_Invoke_m1_6082 (MethodBuilder_t1_501 * __this, Object_t * ___obj, int32_t ___invokeAttr, Binder_t1_585 * ___binder, ObjectU5BU5D_t1_272* ___parameters, CultureInfo_t1_277 * ___culture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.MethodBuilder::IsDefined(System.Type,System.Boolean)
extern "C" bool MethodBuilder_IsDefined_m1_6083 (MethodBuilder_t1_501 * __this, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Reflection.Emit.MethodBuilder::GetCustomAttributes(System.Boolean)
extern "C" ObjectU5BU5D_t1_272* MethodBuilder_GetCustomAttributes_m1_6084 (MethodBuilder_t1_501 * __this, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Reflection.Emit.MethodBuilder::GetCustomAttributes(System.Type,System.Boolean)
extern "C" ObjectU5BU5D_t1_272* MethodBuilder_GetCustomAttributes_m1_6085 (MethodBuilder_t1_501 * __this, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.ILGenerator System.Reflection.Emit.MethodBuilder::GetILGenerator()
extern "C" ILGenerator_t1_480 * MethodBuilder_GetILGenerator_m1_6086 (MethodBuilder_t1_501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.ILGenerator System.Reflection.Emit.MethodBuilder::GetILGenerator(System.Int32)
extern "C" ILGenerator_t1_480 * MethodBuilder_GetILGenerator_m1_6087 (MethodBuilder_t1_501 * __this, int32_t ___size, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.ParameterBuilder System.Reflection.Emit.MethodBuilder::DefineParameter(System.Int32,System.Reflection.ParameterAttributes,System.String)
extern "C" ParameterBuilder_t1_545 * MethodBuilder_DefineParameter_m1_6088 (MethodBuilder_t1_501 * __this, int32_t ___position, int32_t ___attributes, String_t* ___strParamName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.MethodBuilder::check_override()
extern "C" void MethodBuilder_check_override_m1_6089 (MethodBuilder_t1_501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.MethodBuilder::fixup()
extern "C" void MethodBuilder_fixup_m1_6090 (MethodBuilder_t1_501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.MethodBuilder::GenerateDebugInfo(System.Diagnostics.SymbolStore.ISymbolWriter)
extern "C" void MethodBuilder_GenerateDebugInfo_m1_6091 (MethodBuilder_t1_501 * __this, Object_t * ___symbolWriter, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.MethodBuilder::SetCustomAttribute(System.Reflection.Emit.CustomAttributeBuilder)
extern "C" void MethodBuilder_SetCustomAttribute_m1_6092 (MethodBuilder_t1_501 * __this, CustomAttributeBuilder_t1_487 * ___customBuilder, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.MethodBuilder::SetCustomAttribute(System.Reflection.ConstructorInfo,System.Byte[])
extern "C" void MethodBuilder_SetCustomAttribute_m1_6093 (MethodBuilder_t1_501 * __this, ConstructorInfo_t1_478 * ___con, ByteU5BU5D_t1_109* ___binaryAttribute, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.MethodBuilder::SetImplementationFlags(System.Reflection.MethodImplAttributes)
extern "C" void MethodBuilder_SetImplementationFlags_m1_6094 (MethodBuilder_t1_501 * __this, int32_t ___attributes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.MethodBuilder::AddDeclarativeSecurity(System.Security.Permissions.SecurityAction,System.Security.PermissionSet)
extern "C" void MethodBuilder_AddDeclarativeSecurity_m1_6095 (MethodBuilder_t1_501 * __this, int32_t ___action, PermissionSet_t1_563 * ___pset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.MethodBuilder::SetMarshal(System.Reflection.Emit.UnmanagedMarshal)
extern "C" void MethodBuilder_SetMarshal_m1_6096 (MethodBuilder_t1_501 * __this, UnmanagedMarshal_t1_505 * ___unmanagedMarshal, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.MethodBuilder::SetSymCustomAttribute(System.String,System.Byte[])
extern "C" void MethodBuilder_SetSymCustomAttribute_m1_6097 (MethodBuilder_t1_501 * __this, String_t* ___name, ByteU5BU5D_t1_109* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Emit.MethodBuilder::ToString()
extern "C" String_t* MethodBuilder_ToString_m1_6098 (MethodBuilder_t1_501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.MethodBuilder::Equals(System.Object)
extern "C" bool MethodBuilder_Equals_m1_6099 (MethodBuilder_t1_501 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.MethodBuilder::GetHashCode()
extern "C" int32_t MethodBuilder_GetHashCode_m1_6100 (MethodBuilder_t1_501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.MethodBuilder::get_next_table_index(System.Object,System.Int32,System.Boolean)
extern "C" int32_t MethodBuilder_get_next_table_index_m1_6101 (MethodBuilder_t1_501 * __this, Object_t * ___obj, int32_t ___table, bool ___inc, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.MethodBuilder::set_override(System.Reflection.MethodInfo)
extern "C" void MethodBuilder_set_override_m1_6102 (MethodBuilder_t1_501 * __this, MethodInfo_t * ___mdecl, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.MethodBuilder::RejectIfCreated()
extern "C" void MethodBuilder_RejectIfCreated_m1_6103 (MethodBuilder_t1_501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception System.Reflection.Emit.MethodBuilder::NotSupported()
extern "C" Exception_t1_33 * MethodBuilder_NotSupported_m1_6104 (MethodBuilder_t1_501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Reflection.Emit.MethodBuilder::MakeGenericMethod(System.Type[])
extern "C" MethodInfo_t * MethodBuilder_MakeGenericMethod_m1_6105 (MethodBuilder_t1_501 * __this, TypeU5BU5D_t1_31* ___typeArguments, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.MethodBuilder::get_IsGenericMethodDefinition()
extern "C" bool MethodBuilder_get_IsGenericMethodDefinition_m1_6106 (MethodBuilder_t1_501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.MethodBuilder::get_IsGenericMethod()
extern "C" bool MethodBuilder_get_IsGenericMethod_m1_6107 (MethodBuilder_t1_501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Reflection.Emit.MethodBuilder::GetGenericMethodDefinition()
extern "C" MethodInfo_t * MethodBuilder_GetGenericMethodDefinition_m1_6108 (MethodBuilder_t1_501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type[] System.Reflection.Emit.MethodBuilder::GetGenericArguments()
extern "C" TypeU5BU5D_t1_31* MethodBuilder_GetGenericArguments_m1_6109 (MethodBuilder_t1_501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.GenericTypeParameterBuilder[] System.Reflection.Emit.MethodBuilder::DefineGenericParameters(System.String[])
extern "C" GenericTypeParameterBuilderU5BU5D_t1_529* MethodBuilder_DefineGenericParameters_m1_6110 (MethodBuilder_t1_501 * __this, StringU5BU5D_t1_238* ___names, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.MethodBuilder::SetReturnType(System.Type)
extern "C" void MethodBuilder_SetReturnType_m1_6111 (MethodBuilder_t1_501 * __this, Type_t * ___returnType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.MethodBuilder::SetParameters(System.Type[])
extern "C" void MethodBuilder_SetParameters_m1_6112 (MethodBuilder_t1_501 * __this, TypeU5BU5D_t1_31* ___parameterTypes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.MethodBuilder::SetSignature(System.Type,System.Type[],System.Type[],System.Type[],System.Type[][],System.Type[][])
extern "C" void MethodBuilder_SetSignature_m1_6113 (MethodBuilder_t1_501 * __this, Type_t * ___returnType, TypeU5BU5D_t1_31* ___returnTypeRequiredCustomModifiers, TypeU5BU5D_t1_31* ___returnTypeOptionalCustomModifiers, TypeU5BU5D_t1_31* ___parameterTypes, TypeU5BU5DU5BU5D_t1_483* ___parameterTypeRequiredCustomModifiers, TypeU5BU5DU5BU5D_t1_483* ___parameterTypeOptionalCustomModifiers, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Module System.Reflection.Emit.MethodBuilder::get_Module()
extern "C" Module_t1_495 * MethodBuilder_get_Module_m1_6114 (MethodBuilder_t1_501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
