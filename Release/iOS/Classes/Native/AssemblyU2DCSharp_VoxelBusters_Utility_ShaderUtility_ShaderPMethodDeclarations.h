﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.Utility.ShaderUtility/ShaderProperty
struct ShaderProperty_t8_27;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_ShaderUtility_eShader.h"

// System.Void VoxelBusters.Utility.ShaderUtility/ShaderProperty::.ctor()
extern "C" void ShaderProperty__ctor_m8_171 (ShaderProperty_t8_27 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.Utility.ShaderUtility/ShaderProperty::get_Name()
extern "C" String_t* ShaderProperty_get_Name_m8_172 (ShaderProperty_t8_27 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.ShaderUtility/ShaderProperty::set_Name(System.String)
extern "C" void ShaderProperty_set_Name_m8_173 (ShaderProperty_t8_27 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.Utility.ShaderUtility/eShaderPropertyType VoxelBusters.Utility.ShaderUtility/ShaderProperty::get_Type()
extern "C" int32_t ShaderProperty_get_Type_m8_174 (ShaderProperty_t8_27 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.ShaderUtility/ShaderProperty::set_Type(VoxelBusters.Utility.ShaderUtility/eShaderPropertyType)
extern "C" void ShaderProperty_set_Type_m8_175 (ShaderProperty_t8_27 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
