﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Activation.RemoteActivationAttribute
struct RemoteActivationAttribute_t1_856;
// System.Collections.IList
struct IList_t1_262;
// System.Runtime.Remoting.Contexts.Context
struct Context_t1_891;
// System.Runtime.Remoting.Activation.IConstructionCallMessage
struct IConstructionCallMessage_t1_1702;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Activation.RemoteActivationAttribute::.ctor()
extern "C" void RemoteActivationAttribute__ctor_m1_7967 (RemoteActivationAttribute_t1_856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Activation.RemoteActivationAttribute::.ctor(System.Collections.IList)
extern "C" void RemoteActivationAttribute__ctor_m1_7968 (RemoteActivationAttribute_t1_856 * __this, Object_t * ___contextProperties, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Remoting.Activation.RemoteActivationAttribute::IsContextOK(System.Runtime.Remoting.Contexts.Context,System.Runtime.Remoting.Activation.IConstructionCallMessage)
extern "C" bool RemoteActivationAttribute_IsContextOK_m1_7969 (RemoteActivationAttribute_t1_856 * __this, Context_t1_891 * ___ctx, Object_t * ___ctor, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Activation.RemoteActivationAttribute::GetPropertiesForNewContext(System.Runtime.Remoting.Activation.IConstructionCallMessage)
extern "C" void RemoteActivationAttribute_GetPropertiesForNewContext_m1_7970 (RemoteActivationAttribute_t1_856 * __this, Object_t * ___ctor, const MethodInfo* method) IL2CPP_METHOD_ATTR;
