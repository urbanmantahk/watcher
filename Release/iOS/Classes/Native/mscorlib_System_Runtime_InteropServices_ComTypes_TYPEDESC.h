﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType.h"
#include "mscorlib_System_IntPtr.h"

// System.Runtime.InteropServices.ComTypes.TYPEDESC
struct  TYPEDESC_t1_730 
{
	// System.IntPtr System.Runtime.InteropServices.ComTypes.TYPEDESC::lpValue
	IntPtr_t ___lpValue_0;
	// System.Int16 System.Runtime.InteropServices.ComTypes.TYPEDESC::vt
	int16_t ___vt_1;
};
