﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType.h"
#include "mscorlib_System_Guid.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_SYSKIND.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_LIBFLAGS.h"

// System.Runtime.InteropServices.ComTypes.TYPELIBATTR
struct  TYPELIBATTR_t1_746 
{
	// System.Guid System.Runtime.InteropServices.ComTypes.TYPELIBATTR::guid
	Guid_t1_319  ___guid_0;
	// System.Int32 System.Runtime.InteropServices.ComTypes.TYPELIBATTR::lcid
	int32_t ___lcid_1;
	// System.Runtime.InteropServices.ComTypes.SYSKIND System.Runtime.InteropServices.ComTypes.TYPELIBATTR::syskind
	int32_t ___syskind_2;
	// System.Int16 System.Runtime.InteropServices.ComTypes.TYPELIBATTR::wMajorVerNum
	int16_t ___wMajorVerNum_3;
	// System.Int16 System.Runtime.InteropServices.ComTypes.TYPELIBATTR::wMinorVerNum
	int16_t ___wMinorVerNum_4;
	// System.Runtime.InteropServices.ComTypes.LIBFLAGS System.Runtime.InteropServices.ComTypes.TYPELIBATTR::wLibFlags
	int32_t ___wLibFlags_5;
};
// Native definition for marshalling of: System.Runtime.InteropServices.ComTypes.TYPELIBATTR
struct TYPELIBATTR_t1_746_marshaled
{
	Guid_t1_319  ___guid_0;
	int32_t ___lcid_1;
	int32_t ___syskind_2;
	int16_t ___wMajorVerNum_3;
	int16_t ___wMinorVerNum_4;
	int32_t ___wLibFlags_5;
};
