﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Attribute.h"
#include "mscorlib_System_Runtime_InteropServices_TypeLibTypeFlags.h"

// System.Runtime.InteropServices.TypeLibTypeAttribute
struct  TypeLibTypeAttribute_t1_837  : public Attribute_t1_2
{
	// System.Runtime.InteropServices.TypeLibTypeFlags System.Runtime.InteropServices.TypeLibTypeAttribute::flags
	int32_t ___flags_0;
};
