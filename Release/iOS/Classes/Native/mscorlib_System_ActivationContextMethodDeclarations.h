﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.ActivationContext
struct ActivationContext_t1_717;
// System.ApplicationIdentity
struct ApplicationIdentity_t1_718;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;
// System.String[]
struct StringU5BU5D_t1_238;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_ActivationContext_ContextForm.h"

// System.Void System.ActivationContext::.ctor(System.ApplicationIdentity)
extern "C" void ActivationContext__ctor_m1_13006 (ActivationContext_t1_717 * __this, ApplicationIdentity_t1_718 * ___identity, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ActivationContext::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void ActivationContext_System_Runtime_Serialization_ISerializable_GetObjectData_m1_13007 (ActivationContext_t1_717 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ActivationContext::Finalize()
extern "C" void ActivationContext_Finalize_m1_13008 (ActivationContext_t1_717 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ActivationContext/ContextForm System.ActivationContext::get_Form()
extern "C" int32_t ActivationContext_get_Form_m1_13009 (ActivationContext_t1_717 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ApplicationIdentity System.ActivationContext::get_Identity()
extern "C" ApplicationIdentity_t1_718 * ActivationContext_get_Identity_m1_13010 (ActivationContext_t1_717 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ActivationContext System.ActivationContext::CreatePartialActivationContext(System.ApplicationIdentity)
extern "C" ActivationContext_t1_717 * ActivationContext_CreatePartialActivationContext_m1_13011 (Object_t * __this /* static, unused */, ApplicationIdentity_t1_718 * ___identity, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ActivationContext System.ActivationContext::CreatePartialActivationContext(System.ApplicationIdentity,System.String[])
extern "C" ActivationContext_t1_717 * ActivationContext_CreatePartialActivationContext_m1_13012 (Object_t * __this /* static, unused */, ApplicationIdentity_t1_718 * ___identity, StringU5BU5D_t1_238* ___manifestPaths, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ActivationContext::Dispose()
extern "C" void ActivationContext_Dispose_m1_13013 (ActivationContext_t1_717 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ActivationContext::Dispose(System.Boolean)
extern "C" void ActivationContext_Dispose_m1_13014 (ActivationContext_t1_717 * __this, bool ___disposing, const MethodInfo* method) IL2CPP_METHOD_ATTR;
