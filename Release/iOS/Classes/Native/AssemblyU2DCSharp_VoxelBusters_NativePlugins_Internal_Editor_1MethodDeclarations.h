﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.Internal.EditorBillingTransaction
struct EditorBillingTransaction_t8_212;
// System.String
struct String_t;
// System.Collections.IDictionary
struct IDictionary_t1_35;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eBillingTransac.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eBillingTransac_0.h"

// System.Void VoxelBusters.NativePlugins.Internal.EditorBillingTransaction::.ctor(System.String,System.DateTime,System.String,System.String,VoxelBusters.NativePlugins.eBillingTransactionState,VoxelBusters.NativePlugins.eBillingTransactionVerificationState,System.String)
extern "C" void EditorBillingTransaction__ctor_m8_1221 (EditorBillingTransaction_t8_212 * __this, String_t* ____productID, DateTime_t1_150  ____timeUTC, String_t* ____transactionID, String_t* ____receipt, int32_t ____transactionState, int32_t ____verificationState, String_t* ____error, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IDictionary VoxelBusters.NativePlugins.Internal.EditorBillingTransaction::ToJSONObject()
extern "C" Object_t * EditorBillingTransaction_ToJSONObject_m8_1222 (EditorBillingTransaction_t8_212 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
