﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>
struct Collection_1_t1_2293;
// System.Collections.Generic.IList`1<UnityEngine.Vector2>
struct IList_1_t1_2292;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Object
struct Object_t;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t6_279;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector2>
struct IEnumerator_1_t1_2803;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector2.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::.ctor()
extern "C" void Collection_1__ctor_m1_19255_gshared (Collection_1_t1_2293 * __this, const MethodInfo* method);
#define Collection_1__ctor_m1_19255(__this, method) (( void (*) (Collection_1_t1_2293 *, const MethodInfo*))Collection_1__ctor_m1_19255_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1__ctor_m1_19256_gshared (Collection_1_t1_2293 * __this, Object_t* ___list, const MethodInfo* method);
#define Collection_1__ctor_m1_19256(__this, ___list, method) (( void (*) (Collection_1_t1_2293 *, Object_t*, const MethodInfo*))Collection_1__ctor_m1_19256_gshared)(__this, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_19257_gshared (Collection_1_t1_2293 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_19257(__this, method) (( bool (*) (Collection_1_t1_2293 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_19257_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m1_19258_gshared (Collection_1_t1_2293 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m1_19258(__this, ___array, ___index, method) (( void (*) (Collection_1_t1_2293 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m1_19258_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m1_19259_gshared (Collection_1_t1_2293 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m1_19259(__this, method) (( Object_t * (*) (Collection_1_t1_2293 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m1_19259_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m1_19260_gshared (Collection_1_t1_2293 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m1_19260(__this, ___value, method) (( int32_t (*) (Collection_1_t1_2293 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m1_19260_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m1_19261_gshared (Collection_1_t1_2293 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m1_19261(__this, ___value, method) (( bool (*) (Collection_1_t1_2293 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m1_19261_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m1_19262_gshared (Collection_1_t1_2293 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m1_19262(__this, ___value, method) (( int32_t (*) (Collection_1_t1_2293 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m1_19262_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m1_19263_gshared (Collection_1_t1_2293 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m1_19263(__this, ___index, ___value, method) (( void (*) (Collection_1_t1_2293 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m1_19263_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m1_19264_gshared (Collection_1_t1_2293 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m1_19264(__this, ___value, method) (( void (*) (Collection_1_t1_2293 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m1_19264_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m1_19265_gshared (Collection_1_t1_2293 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m1_19265(__this, method) (( bool (*) (Collection_1_t1_2293 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m1_19265_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m1_19266_gshared (Collection_1_t1_2293 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m1_19266(__this, method) (( Object_t * (*) (Collection_1_t1_2293 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m1_19266_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m1_19267_gshared (Collection_1_t1_2293 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m1_19267(__this, method) (( bool (*) (Collection_1_t1_2293 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m1_19267_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m1_19268_gshared (Collection_1_t1_2293 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m1_19268(__this, method) (( bool (*) (Collection_1_t1_2293 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m1_19268_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m1_19269_gshared (Collection_1_t1_2293 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m1_19269(__this, ___index, method) (( Object_t * (*) (Collection_1_t1_2293 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m1_19269_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m1_19270_gshared (Collection_1_t1_2293 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m1_19270(__this, ___index, ___value, method) (( void (*) (Collection_1_t1_2293 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m1_19270_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::Add(T)
extern "C" void Collection_1_Add_m1_19271_gshared (Collection_1_t1_2293 * __this, Vector2_t6_47  ___item, const MethodInfo* method);
#define Collection_1_Add_m1_19271(__this, ___item, method) (( void (*) (Collection_1_t1_2293 *, Vector2_t6_47 , const MethodInfo*))Collection_1_Add_m1_19271_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::Clear()
extern "C" void Collection_1_Clear_m1_19272_gshared (Collection_1_t1_2293 * __this, const MethodInfo* method);
#define Collection_1_Clear_m1_19272(__this, method) (( void (*) (Collection_1_t1_2293 *, const MethodInfo*))Collection_1_Clear_m1_19272_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::ClearItems()
extern "C" void Collection_1_ClearItems_m1_19273_gshared (Collection_1_t1_2293 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m1_19273(__this, method) (( void (*) (Collection_1_t1_2293 *, const MethodInfo*))Collection_1_ClearItems_m1_19273_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::Contains(T)
extern "C" bool Collection_1_Contains_m1_19274_gshared (Collection_1_t1_2293 * __this, Vector2_t6_47  ___item, const MethodInfo* method);
#define Collection_1_Contains_m1_19274(__this, ___item, method) (( bool (*) (Collection_1_t1_2293 *, Vector2_t6_47 , const MethodInfo*))Collection_1_Contains_m1_19274_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m1_19275_gshared (Collection_1_t1_2293 * __this, Vector2U5BU5D_t6_279* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m1_19275(__this, ___array, ___index, method) (( void (*) (Collection_1_t1_2293 *, Vector2U5BU5D_t6_279*, int32_t, const MethodInfo*))Collection_1_CopyTo_m1_19275_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m1_19276_gshared (Collection_1_t1_2293 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m1_19276(__this, method) (( Object_t* (*) (Collection_1_t1_2293 *, const MethodInfo*))Collection_1_GetEnumerator_m1_19276_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m1_19277_gshared (Collection_1_t1_2293 * __this, Vector2_t6_47  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m1_19277(__this, ___item, method) (( int32_t (*) (Collection_1_t1_2293 *, Vector2_t6_47 , const MethodInfo*))Collection_1_IndexOf_m1_19277_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m1_19278_gshared (Collection_1_t1_2293 * __this, int32_t ___index, Vector2_t6_47  ___item, const MethodInfo* method);
#define Collection_1_Insert_m1_19278(__this, ___index, ___item, method) (( void (*) (Collection_1_t1_2293 *, int32_t, Vector2_t6_47 , const MethodInfo*))Collection_1_Insert_m1_19278_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m1_19279_gshared (Collection_1_t1_2293 * __this, int32_t ___index, Vector2_t6_47  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m1_19279(__this, ___index, ___item, method) (( void (*) (Collection_1_t1_2293 *, int32_t, Vector2_t6_47 , const MethodInfo*))Collection_1_InsertItem_m1_19279_gshared)(__this, ___index, ___item, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::get_Items()
extern "C" Object_t* Collection_1_get_Items_m1_19280_gshared (Collection_1_t1_2293 * __this, const MethodInfo* method);
#define Collection_1_get_Items_m1_19280(__this, method) (( Object_t* (*) (Collection_1_t1_2293 *, const MethodInfo*))Collection_1_get_Items_m1_19280_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::Remove(T)
extern "C" bool Collection_1_Remove_m1_19281_gshared (Collection_1_t1_2293 * __this, Vector2_t6_47  ___item, const MethodInfo* method);
#define Collection_1_Remove_m1_19281(__this, ___item, method) (( bool (*) (Collection_1_t1_2293 *, Vector2_t6_47 , const MethodInfo*))Collection_1_Remove_m1_19281_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m1_19282_gshared (Collection_1_t1_2293 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m1_19282(__this, ___index, method) (( void (*) (Collection_1_t1_2293 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m1_19282_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m1_19283_gshared (Collection_1_t1_2293 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m1_19283(__this, ___index, method) (( void (*) (Collection_1_t1_2293 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m1_19283_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::get_Count()
extern "C" int32_t Collection_1_get_Count_m1_19284_gshared (Collection_1_t1_2293 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m1_19284(__this, method) (( int32_t (*) (Collection_1_t1_2293 *, const MethodInfo*))Collection_1_get_Count_m1_19284_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::get_Item(System.Int32)
extern "C" Vector2_t6_47  Collection_1_get_Item_m1_19285_gshared (Collection_1_t1_2293 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m1_19285(__this, ___index, method) (( Vector2_t6_47  (*) (Collection_1_t1_2293 *, int32_t, const MethodInfo*))Collection_1_get_Item_m1_19285_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m1_19286_gshared (Collection_1_t1_2293 * __this, int32_t ___index, Vector2_t6_47  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m1_19286(__this, ___index, ___value, method) (( void (*) (Collection_1_t1_2293 *, int32_t, Vector2_t6_47 , const MethodInfo*))Collection_1_set_Item_m1_19286_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m1_19287_gshared (Collection_1_t1_2293 * __this, int32_t ___index, Vector2_t6_47  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m1_19287(__this, ___index, ___item, method) (( void (*) (Collection_1_t1_2293 *, int32_t, Vector2_t6_47 , const MethodInfo*))Collection_1_SetItem_m1_19287_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m1_19288_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m1_19288(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m1_19288_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::ConvertItem(System.Object)
extern "C" Vector2_t6_47  Collection_1_ConvertItem_m1_19289_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m1_19289(__this /* static, unused */, ___item, method) (( Vector2_t6_47  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m1_19289_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m1_19290_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m1_19290(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m1_19290_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m1_19291_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m1_19291(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m1_19291_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m1_19292_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m1_19292(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m1_19292_gshared)(__this /* static, unused */, ___list, method)
