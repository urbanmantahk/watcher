﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Resources.Win32IconResource
struct Win32IconResource_t1_666;
// System.Resources.ICONDIRENTRY
struct ICONDIRENTRY_t1_667;
// System.IO.Stream
struct Stream_t1_405;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Resources.Win32IconResource::.ctor(System.Int32,System.Int32,System.Resources.ICONDIRENTRY)
extern "C" void Win32IconResource__ctor_m1_7480 (Win32IconResource_t1_666 * __this, int32_t ___id, int32_t ___language, ICONDIRENTRY_t1_667 * ___icon, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Resources.ICONDIRENTRY System.Resources.Win32IconResource::get_Icon()
extern "C" ICONDIRENTRY_t1_667 * Win32IconResource_get_Icon_m1_7481 (Win32IconResource_t1_666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.Win32IconResource::WriteTo(System.IO.Stream)
extern "C" void Win32IconResource_WriteTo_m1_7482 (Win32IconResource_t1_666 * __this, Stream_t1_405 * ___s, const MethodInfo* method) IL2CPP_METHOD_ATTR;
