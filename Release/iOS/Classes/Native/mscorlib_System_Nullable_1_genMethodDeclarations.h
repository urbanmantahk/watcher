﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Object_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen.h"
#include "mscorlib_System_DateTime.h"

// System.Void System.Nullable`1<System.DateTime>::.ctor(T)
extern "C" void Nullable_1__ctor_m1_14884_gshared (Nullable_1_t1_371 * __this, DateTime_t1_150  ___value, const MethodInfo* method);
#define Nullable_1__ctor_m1_14884(__this, ___value, method) (( void (*) (Nullable_1_t1_371 *, DateTime_t1_150 , const MethodInfo*))Nullable_1__ctor_m1_14884_gshared)(__this, ___value, method)
// System.Boolean System.Nullable`1<System.DateTime>::get_HasValue()
extern "C" bool Nullable_1_get_HasValue_m1_14883_gshared (Nullable_1_t1_371 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m1_14883(__this, method) (( bool (*) (Nullable_1_t1_371 *, const MethodInfo*))Nullable_1_get_HasValue_m1_14883_gshared)(__this, method)
// T System.Nullable`1<System.DateTime>::get_Value()
extern "C" DateTime_t1_150  Nullable_1_get_Value_m1_14885_gshared (Nullable_1_t1_371 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m1_14885(__this, method) (( DateTime_t1_150  (*) (Nullable_1_t1_371 *, const MethodInfo*))Nullable_1_get_Value_m1_14885_gshared)(__this, method)
// System.Boolean System.Nullable`1<System.DateTime>::Equals(System.Object)
extern "C" bool Nullable_1_Equals_m1_16121_gshared (Nullable_1_t1_371 * __this, Object_t * ___other, const MethodInfo* method);
#define Nullable_1_Equals_m1_16121(__this, ___other, method) (( bool (*) (Nullable_1_t1_371 *, Object_t *, const MethodInfo*))Nullable_1_Equals_m1_16121_gshared)(__this, ___other, method)
// System.Boolean System.Nullable`1<System.DateTime>::Equals(System.Nullable`1<T>)
extern "C" bool Nullable_1_Equals_m1_16122_gshared (Nullable_1_t1_371 * __this, Nullable_1_t1_371  ___other, const MethodInfo* method);
#define Nullable_1_Equals_m1_16122(__this, ___other, method) (( bool (*) (Nullable_1_t1_371 *, Nullable_1_t1_371 , const MethodInfo*))Nullable_1_Equals_m1_16122_gshared)(__this, ___other, method)
// System.Int32 System.Nullable`1<System.DateTime>::GetHashCode()
extern "C" int32_t Nullable_1_GetHashCode_m1_16123_gshared (Nullable_1_t1_371 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m1_16123(__this, method) (( int32_t (*) (Nullable_1_t1_371 *, const MethodInfo*))Nullable_1_GetHashCode_m1_16123_gshared)(__this, method)
// T System.Nullable`1<System.DateTime>::GetValueOrDefault()
extern "C" DateTime_t1_150  Nullable_1_GetValueOrDefault_m1_16124_gshared (Nullable_1_t1_371 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m1_16124(__this, method) (( DateTime_t1_150  (*) (Nullable_1_t1_371 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m1_16124_gshared)(__this, method)
// T System.Nullable`1<System.DateTime>::GetValueOrDefault(T)
extern "C" DateTime_t1_150  Nullable_1_GetValueOrDefault_m1_16125_gshared (Nullable_1_t1_371 * __this, DateTime_t1_150  ___defaultValue, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m1_16125(__this, ___defaultValue, method) (( DateTime_t1_150  (*) (Nullable_1_t1_371 *, DateTime_t1_150 , const MethodInfo*))Nullable_1_GetValueOrDefault_m1_16125_gshared)(__this, ___defaultValue, method)
// System.String System.Nullable`1<System.DateTime>::ToString()
extern "C" String_t* Nullable_1_ToString_m1_16126_gshared (Nullable_1_t1_371 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m1_16126(__this, method) (( String_t* (*) (Nullable_1_t1_371 *, const MethodInfo*))Nullable_1_ToString_m1_16126_gshared)(__this, method)
// System.Object System.Nullable`1<System.DateTime>::Box(System.Nullable`1<T>)
extern "C" Object_t * Nullable_1_Box_m1_16127_gshared (Object_t * __this /* static, unused */, Nullable_1_t1_371  ___o, const MethodInfo* method);
#define Nullable_1_Box_m1_16127(__this /* static, unused */, ___o, method) (( Object_t * (*) (Object_t * /* static, unused */, Nullable_1_t1_371 , const MethodInfo*))Nullable_1_Box_m1_16127_gshared)(__this /* static, unused */, ___o, method)
// System.Nullable`1<T> System.Nullable`1<System.DateTime>::Unbox(System.Object)
extern "C" Nullable_1_t1_371  Nullable_1_Unbox_m1_16128_gshared (Object_t * __this /* static, unused */, Object_t * ___o, const MethodInfo* method);
#define Nullable_1_Unbox_m1_16128(__this /* static, unused */, ___o, method) (( Nullable_1_t1_371  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Nullable_1_Unbox_m1_16128_gshared)(__this /* static, unused */, ___o, method)
// System.Nullable`1<T> System.Nullable`1<System.DateTime>::op_Implicit(T)
extern "C" Nullable_1_t1_371  Nullable_1_op_Implicit_m1_16129_gshared (Object_t * __this /* static, unused */, DateTime_t1_150  ___value, const MethodInfo* method);
#define Nullable_1_op_Implicit_m1_16129(__this /* static, unused */, ___value, method) (( Nullable_1_t1_371  (*) (Object_t * /* static, unused */, DateTime_t1_150 , const MethodInfo*))Nullable_1_op_Implicit_m1_16129_gshared)(__this /* static, unused */, ___value, method)
// T System.Nullable`1<System.DateTime>::op_Explicit(System.Nullable`1<T>)
extern "C" DateTime_t1_150  Nullable_1_op_Explicit_m1_16130_gshared (Object_t * __this /* static, unused */, Nullable_1_t1_371  ___value, const MethodInfo* method);
#define Nullable_1_op_Explicit_m1_16130(__this /* static, unused */, ___value, method) (( DateTime_t1_150  (*) (Object_t * /* static, unused */, Nullable_1_t1_371 , const MethodInfo*))Nullable_1_op_Explicit_m1_16130_gshared)(__this /* static, unused */, ___value, method)
