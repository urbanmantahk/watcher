﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.ControlCharacters
struct ControlCharacters_t1_1521;

#include "codegen/il2cpp-codegen.h"

// System.Void System.ControlCharacters::.ctor()
extern "C" void ControlCharacters__ctor_m1_13436 (ControlCharacters_t1_1521 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
