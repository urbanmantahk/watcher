﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.AudioSettings/AudioConfigurationChangeHandler
struct AudioConfigurationChangeHandler_t6_124;

#include "mscorlib_System_Object.h"

// UnityEngine.AudioSettings
struct  AudioSettings_t6_125  : public Object_t
{
};
struct AudioSettings_t6_125_StaticFields{
	// UnityEngine.AudioSettings/AudioConfigurationChangeHandler UnityEngine.AudioSettings::OnAudioConfigurationChanged
	AudioConfigurationChangeHandler_t6_124 * ___OnAudioConfigurationChanged_0;
};
