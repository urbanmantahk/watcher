﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Gradient
struct Gradient_t6_42;
struct Gradient_t6_42_marshaled;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Gradient::.ctor()
extern "C" void Gradient__ctor_m6_189 (Gradient_t6_42 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gradient::Init()
extern "C" void Gradient_Init_m6_190 (Gradient_t6_42 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gradient::Cleanup()
extern "C" void Gradient_Cleanup_m6_191 (Gradient_t6_42 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gradient::Finalize()
extern "C" void Gradient_Finalize_m6_192 (Gradient_t6_42 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void Gradient_t6_42_marshal(const Gradient_t6_42& unmarshaled, Gradient_t6_42_marshaled& marshaled);
extern "C" void Gradient_t6_42_marshal_back(const Gradient_t6_42_marshaled& marshaled, Gradient_t6_42& unmarshaled);
extern "C" void Gradient_t6_42_marshal_cleanup(Gradient_t6_42_marshaled& marshaled);
