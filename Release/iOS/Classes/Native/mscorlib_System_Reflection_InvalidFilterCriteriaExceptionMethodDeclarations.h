﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.InvalidFilterCriteriaException
struct InvalidFilterCriteriaException_t1_604;
// System.String
struct String_t;
// System.Exception
struct Exception_t1_33;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.Reflection.InvalidFilterCriteriaException::.ctor()
extern "C" void InvalidFilterCriteriaException__ctor_m1_6874 (InvalidFilterCriteriaException_t1_604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.InvalidFilterCriteriaException::.ctor(System.String)
extern "C" void InvalidFilterCriteriaException__ctor_m1_6875 (InvalidFilterCriteriaException_t1_604 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.InvalidFilterCriteriaException::.ctor(System.String,System.Exception)
extern "C" void InvalidFilterCriteriaException__ctor_m1_6876 (InvalidFilterCriteriaException_t1_604 * __this, String_t* ___message, Exception_t1_33 * ___inner, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.InvalidFilterCriteriaException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void InvalidFilterCriteriaException__ctor_m1_6877 (InvalidFilterCriteriaException_t1_604 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
