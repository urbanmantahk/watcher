﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.CspKeyContainerInfo
struct CspKeyContainerInfo_t1_1196;
// System.Security.Cryptography.CspParameters
struct CspParameters_t1_164;
// System.Security.AccessControl.CryptoKeySecurity
struct CryptoKeySecurity_t1_1142;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Cryptography_KeyNumber.h"

// System.Void System.Security.Cryptography.CspKeyContainerInfo::.ctor(System.Security.Cryptography.CspParameters)
extern "C" void CspKeyContainerInfo__ctor_m1_10231 (CspKeyContainerInfo_t1_1196 * __this, CspParameters_t1_164 * ___parameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.CspKeyContainerInfo::get_Accessible()
extern "C" bool CspKeyContainerInfo_get_Accessible_m1_10232 (CspKeyContainerInfo_t1_1196 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.AccessControl.CryptoKeySecurity System.Security.Cryptography.CspKeyContainerInfo::get_CryptoKeySecurity()
extern "C" CryptoKeySecurity_t1_1142 * CspKeyContainerInfo_get_CryptoKeySecurity_m1_10233 (CspKeyContainerInfo_t1_1196 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.CspKeyContainerInfo::get_Exportable()
extern "C" bool CspKeyContainerInfo_get_Exportable_m1_10234 (CspKeyContainerInfo_t1_1196 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.CspKeyContainerInfo::get_HardwareDevice()
extern "C" bool CspKeyContainerInfo_get_HardwareDevice_m1_10235 (CspKeyContainerInfo_t1_1196 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Cryptography.CspKeyContainerInfo::get_KeyContainerName()
extern "C" String_t* CspKeyContainerInfo_get_KeyContainerName_m1_10236 (CspKeyContainerInfo_t1_1196 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.KeyNumber System.Security.Cryptography.CspKeyContainerInfo::get_KeyNumber()
extern "C" int32_t CspKeyContainerInfo_get_KeyNumber_m1_10237 (CspKeyContainerInfo_t1_1196 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.CspKeyContainerInfo::get_MachineKeyStore()
extern "C" bool CspKeyContainerInfo_get_MachineKeyStore_m1_10238 (CspKeyContainerInfo_t1_1196 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.CspKeyContainerInfo::get_Protected()
extern "C" bool CspKeyContainerInfo_get_Protected_m1_10239 (CspKeyContainerInfo_t1_1196 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Cryptography.CspKeyContainerInfo::get_ProviderName()
extern "C" String_t* CspKeyContainerInfo_get_ProviderName_m1_10240 (CspKeyContainerInfo_t1_1196 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Cryptography.CspKeyContainerInfo::get_ProviderType()
extern "C" int32_t CspKeyContainerInfo_get_ProviderType_m1_10241 (CspKeyContainerInfo_t1_1196 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.CspKeyContainerInfo::get_RandomlyGenerated()
extern "C" bool CspKeyContainerInfo_get_RandomlyGenerated_m1_10242 (CspKeyContainerInfo_t1_1196 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.CspKeyContainerInfo::get_Removable()
extern "C" bool CspKeyContainerInfo_get_Removable_m1_10243 (CspKeyContainerInfo_t1_1196 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Cryptography.CspKeyContainerInfo::get_UniqueKeyContainerName()
extern "C" String_t* CspKeyContainerInfo_get_UniqueKeyContainerName_m1_10244 (CspKeyContainerInfo_t1_1196 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
