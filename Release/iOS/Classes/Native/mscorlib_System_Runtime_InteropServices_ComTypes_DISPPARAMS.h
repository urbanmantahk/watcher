﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType.h"
#include "mscorlib_System_IntPtr.h"

// System.Runtime.InteropServices.ComTypes.DISPPARAMS
struct  DISPPARAMS_t1_725 
{
	// System.IntPtr System.Runtime.InteropServices.ComTypes.DISPPARAMS::rgvarg
	IntPtr_t ___rgvarg_0;
	// System.IntPtr System.Runtime.InteropServices.ComTypes.DISPPARAMS::rgdispidNamedArgs
	IntPtr_t ___rgdispidNamedArgs_1;
	// System.Int32 System.Runtime.InteropServices.ComTypes.DISPPARAMS::cArgs
	int32_t ___cArgs_2;
	// System.Int32 System.Runtime.InteropServices.ComTypes.DISPPARAMS::cNamedArgs
	int32_t ___cNamedArgs_3;
};
