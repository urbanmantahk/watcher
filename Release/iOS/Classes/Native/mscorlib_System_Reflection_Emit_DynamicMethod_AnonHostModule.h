﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Reflection.Module
struct Module_t1_495;

#include "mscorlib_System_Object.h"

// System.Reflection.Emit.DynamicMethod/AnonHostModuleHolder
struct  AnonHostModuleHolder_t1_494  : public Object_t
{
};
struct AnonHostModuleHolder_t1_494_StaticFields{
	// System.Reflection.Module System.Reflection.Emit.DynamicMethod/AnonHostModuleHolder::anon_host_module
	Module_t1_495 * ___anon_host_module_0;
};
