﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.SByte[]
struct SByteU5BU5D_t1_1450;

#include "mscorlib_System_Text_Encoding.h"

// System.Text.UTF7Encoding
struct  UTF7Encoding_t1_1449  : public Encoding_t1_406
{
	// System.Boolean System.Text.UTF7Encoding::allowOptionals
	bool ___allowOptionals_31;
};
struct UTF7Encoding_t1_1449_StaticFields{
	// System.Byte[] System.Text.UTF7Encoding::encodingRules
	ByteU5BU5D_t1_109* ___encodingRules_32;
	// System.SByte[] System.Text.UTF7Encoding::base64Values
	SByteU5BU5D_t1_1450* ___base64Values_33;
};
