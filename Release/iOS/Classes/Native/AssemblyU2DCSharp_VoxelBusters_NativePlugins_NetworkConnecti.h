﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object.h"

// VoxelBusters.NativePlugins.NetworkConnectivitySettings/EditorSettings
struct  EditorSettings_t8_254  : public Object_t
{
	// System.Int32 VoxelBusters.NativePlugins.NetworkConnectivitySettings/EditorSettings::m_timeOutPeriod
	int32_t ___m_timeOutPeriod_0;
	// System.Int32 VoxelBusters.NativePlugins.NetworkConnectivitySettings/EditorSettings::m_maxRetryCount
	int32_t ___m_maxRetryCount_1;
	// System.Single VoxelBusters.NativePlugins.NetworkConnectivitySettings/EditorSettings::m_timeGapBetweenPolling
	float ___m_timeGapBetweenPolling_2;
};
