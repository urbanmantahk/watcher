﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Policy.TrustManagerContext
struct TrustManagerContext_t1_1365;
// System.ApplicationIdentity
struct ApplicationIdentity_t1_718;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Policy_TrustManagerUIContext.h"

// System.Void System.Security.Policy.TrustManagerContext::.ctor()
extern "C" void TrustManagerContext__ctor_m1_11713 (TrustManagerContext_t1_1365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.TrustManagerContext::.ctor(System.Security.Policy.TrustManagerUIContext)
extern "C" void TrustManagerContext__ctor_m1_11714 (TrustManagerContext_t1_1365 * __this, int32_t ___uiContext, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Policy.TrustManagerContext::get_IgnorePersistedDecision()
extern "C" bool TrustManagerContext_get_IgnorePersistedDecision_m1_11715 (TrustManagerContext_t1_1365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.TrustManagerContext::set_IgnorePersistedDecision(System.Boolean)
extern "C" void TrustManagerContext_set_IgnorePersistedDecision_m1_11716 (TrustManagerContext_t1_1365 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Policy.TrustManagerContext::get_KeepAlive()
extern "C" bool TrustManagerContext_get_KeepAlive_m1_11717 (TrustManagerContext_t1_1365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.TrustManagerContext::set_KeepAlive(System.Boolean)
extern "C" void TrustManagerContext_set_KeepAlive_m1_11718 (TrustManagerContext_t1_1365 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Policy.TrustManagerContext::get_NoPrompt()
extern "C" bool TrustManagerContext_get_NoPrompt_m1_11719 (TrustManagerContext_t1_1365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.TrustManagerContext::set_NoPrompt(System.Boolean)
extern "C" void TrustManagerContext_set_NoPrompt_m1_11720 (TrustManagerContext_t1_1365 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Policy.TrustManagerContext::get_Persist()
extern "C" bool TrustManagerContext_get_Persist_m1_11721 (TrustManagerContext_t1_1365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.TrustManagerContext::set_Persist(System.Boolean)
extern "C" void TrustManagerContext_set_Persist_m1_11722 (TrustManagerContext_t1_1365 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ApplicationIdentity System.Security.Policy.TrustManagerContext::get_PreviousApplicationIdentity()
extern "C" ApplicationIdentity_t1_718 * TrustManagerContext_get_PreviousApplicationIdentity_m1_11723 (TrustManagerContext_t1_1365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.TrustManagerContext::set_PreviousApplicationIdentity(System.ApplicationIdentity)
extern "C" void TrustManagerContext_set_PreviousApplicationIdentity_m1_11724 (TrustManagerContext_t1_1365 * __this, ApplicationIdentity_t1_718 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.TrustManagerUIContext System.Security.Policy.TrustManagerContext::get_UIContext()
extern "C" int32_t TrustManagerContext_get_UIContext_m1_11725 (TrustManagerContext_t1_1365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.TrustManagerContext::set_UIContext(System.Security.Policy.TrustManagerUIContext)
extern "C" void TrustManagerContext_set_UIContext_m1_11726 (TrustManagerContext_t1_1365 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
