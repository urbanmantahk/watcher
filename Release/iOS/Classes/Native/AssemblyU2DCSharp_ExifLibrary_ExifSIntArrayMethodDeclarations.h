﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExifLibrary.ExifSIntArray
struct ExifSIntArray_t8_125;
// System.Int32[]
struct Int32U5BU5D_t1_275;
// System.Object
struct Object_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTag.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifInterOperability.h"

// System.Void ExifLibrary.ExifSIntArray::.ctor(ExifLibrary.ExifTag,System.Int32[])
extern "C" void ExifSIntArray__ctor_m8_600 (ExifSIntArray_t8_125 * __this, int32_t ___tag, Int32U5BU5D_t1_275* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ExifLibrary.ExifSIntArray::get__Value()
extern "C" Object_t * ExifSIntArray_get__Value_m8_601 (ExifSIntArray_t8_125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.ExifSIntArray::set__Value(System.Object)
extern "C" void ExifSIntArray_set__Value_m8_602 (ExifSIntArray_t8_125 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] ExifLibrary.ExifSIntArray::get_Value()
extern "C" Int32U5BU5D_t1_275* ExifSIntArray_get_Value_m8_603 (ExifSIntArray_t8_125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.ExifSIntArray::set_Value(System.Int32[])
extern "C" void ExifSIntArray_set_Value_m8_604 (ExifSIntArray_t8_125 * __this, Int32U5BU5D_t1_275* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExifLibrary.ExifSIntArray::ToString()
extern "C" String_t* ExifSIntArray_ToString_m8_605 (ExifSIntArray_t8_125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.ExifInterOperability ExifLibrary.ExifSIntArray::get_Interoperability()
extern "C" ExifInterOperability_t8_113  ExifSIntArray_get_Interoperability_m8_606 (ExifSIntArray_t8_125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] ExifLibrary.ExifSIntArray::op_Implicit(ExifLibrary.ExifSIntArray)
extern "C" Int32U5BU5D_t1_275* ExifSIntArray_op_Implicit_m8_607 (Object_t * __this /* static, unused */, ExifSIntArray_t8_125 * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
