﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.Emit.PointerType
struct PointerType_t1_492;
// System.Type
struct Type_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Reflection.Emit.PointerType::.ctor(System.Type)
extern "C" void PointerType__ctor_m1_5669 (PointerType_t1_492 * __this, Type_t * ___elementType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.PointerType::IsPointerImpl()
extern "C" bool PointerType_IsPointerImpl_m1_5670 (PointerType_t1_492 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.PointerType::get_BaseType()
extern "C" Type_t * PointerType_get_BaseType_m1_5671 (PointerType_t1_492 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Emit.PointerType::FormatName(System.String)
extern "C" String_t* PointerType_FormatName_m1_5672 (PointerType_t1_492 * __this, String_t* ___elementName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
