﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "AssemblyU2DCSharp_ExifLibrary_GPSMeasureMode.h"

// ExifLibrary.GPSMeasureMode
struct  GPSMeasureMode_t8_90 
{
	// System.Byte ExifLibrary.GPSMeasureMode::value__
	uint8_t ___value___1;
};
