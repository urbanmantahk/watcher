﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.CompilerServices.DependencyAttribute
struct DependencyAttribute_t1_684;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_CompilerServices_LoadHint.h"

// System.Void System.Runtime.CompilerServices.DependencyAttribute::.ctor(System.String,System.Runtime.CompilerServices.LoadHint)
extern "C" void DependencyAttribute__ctor_m1_7540 (DependencyAttribute_t1_684 * __this, String_t* ___dependentAssemblyArgument, int32_t ___loadHintArgument, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.CompilerServices.DependencyAttribute::get_DependentAssembly()
extern "C" String_t* DependencyAttribute_get_DependentAssembly_m1_7541 (DependencyAttribute_t1_684 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.CompilerServices.LoadHint System.Runtime.CompilerServices.DependencyAttribute::get_LoadHint()
extern "C" int32_t DependencyAttribute_get_LoadHint_m1_7542 (DependencyAttribute_t1_684 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
