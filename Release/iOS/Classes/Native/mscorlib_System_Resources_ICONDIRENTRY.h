﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "mscorlib_System_Object.h"

// System.Resources.ICONDIRENTRY
struct  ICONDIRENTRY_t1_667  : public Object_t
{
	// System.Byte System.Resources.ICONDIRENTRY::bWidth
	uint8_t ___bWidth_0;
	// System.Byte System.Resources.ICONDIRENTRY::bHeight
	uint8_t ___bHeight_1;
	// System.Byte System.Resources.ICONDIRENTRY::bColorCount
	uint8_t ___bColorCount_2;
	// System.Byte System.Resources.ICONDIRENTRY::bReserved
	uint8_t ___bReserved_3;
	// System.Int16 System.Resources.ICONDIRENTRY::wPlanes
	int16_t ___wPlanes_4;
	// System.Int16 System.Resources.ICONDIRENTRY::wBitCount
	int16_t ___wBitCount_5;
	// System.Int32 System.Resources.ICONDIRENTRY::dwBytesInRes
	int32_t ___dwBytesInRes_6;
	// System.Int32 System.Resources.ICONDIRENTRY::dwImageOffset
	int32_t ___dwImageOffset_7;
	// System.Byte[] System.Resources.ICONDIRENTRY::image
	ByteU5BU5D_t1_109* ___image_8;
};
