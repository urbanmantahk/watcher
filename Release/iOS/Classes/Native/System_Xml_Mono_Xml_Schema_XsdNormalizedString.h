﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "System_Xml_Mono_Xml_Schema_XsdString.h"

// Mono.Xml.Schema.XsdNormalizedString
struct  XsdNormalizedString_t4_8  : public XsdString_t4_7
{
};
