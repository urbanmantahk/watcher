﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t1_1837;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_18.h"
#include "UnityEngine_UnityEngine_Color32.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m1_19402_gshared (Enumerator_t1_2301 * __this, List_1_t1_1837 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m1_19402(__this, ___l, method) (( void (*) (Enumerator_t1_2301 *, List_1_t1_1837 *, const MethodInfo*))Enumerator__ctor_m1_19402_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m1_19403_gshared (Enumerator_t1_2301 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1_19403(__this, method) (( void (*) (Enumerator_t1_2301 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_19403_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m1_19404_gshared (Enumerator_t1_2301 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_19404(__this, method) (( Object_t * (*) (Enumerator_t1_2301 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_19404_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::Dispose()
extern "C" void Enumerator_Dispose_m1_19405_gshared (Enumerator_t1_2301 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1_19405(__this, method) (( void (*) (Enumerator_t1_2301 *, const MethodInfo*))Enumerator_Dispose_m1_19405_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::VerifyState()
extern "C" void Enumerator_VerifyState_m1_19406_gshared (Enumerator_t1_2301 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m1_19406(__this, method) (( void (*) (Enumerator_t1_2301 *, const MethodInfo*))Enumerator_VerifyState_m1_19406_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::MoveNext()
extern "C" bool Enumerator_MoveNext_m1_19407_gshared (Enumerator_t1_2301 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1_19407(__this, method) (( bool (*) (Enumerator_t1_2301 *, const MethodInfo*))Enumerator_MoveNext_m1_19407_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::get_Current()
extern "C" Color32_t6_49  Enumerator_get_Current_m1_19408_gshared (Enumerator_t1_2301 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1_19408(__this, method) (( Color32_t6_49  (*) (Enumerator_t1_2301 *, const MethodInfo*))Enumerator_get_Current_m1_19408_gshared)(__this, method)
