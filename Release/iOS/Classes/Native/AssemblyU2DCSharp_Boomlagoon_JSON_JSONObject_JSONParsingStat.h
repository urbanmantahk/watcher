﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "AssemblyU2DCSharp_Boomlagoon_JSON_JSONObject_JSONParsingStat.h"

// Boomlagoon.JSON.JSONObject/JSONParsingState
struct  JSONParsingState_t8_7 
{
	// System.Int32 Boomlagoon.JSON.JSONObject/JSONParsingState::value__
	int32_t ___value___1;
};
