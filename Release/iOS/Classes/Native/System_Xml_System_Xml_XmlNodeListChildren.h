﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Xml.IHasXmlChildNode
struct IHasXmlChildNode_t4_152;

#include "System_Xml_System_Xml_XmlNodeList.h"

// System.Xml.XmlNodeListChildren
struct  XmlNodeListChildren_t4_148  : public XmlNodeList_t4_147
{
	// System.Xml.IHasXmlChildNode System.Xml.XmlNodeListChildren::parent
	Object_t * ___parent_0;
};
