﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Single>
struct ShimEnumerator_t1_2716;
// System.Collections.Generic.Dictionary`2<System.Object,System.Single>
struct Dictionary_2_t1_2704;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Single>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m1_27090_gshared (ShimEnumerator_t1_2716 * __this, Dictionary_2_t1_2704 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m1_27090(__this, ___host, method) (( void (*) (ShimEnumerator_t1_2716 *, Dictionary_2_t1_2704 *, const MethodInfo*))ShimEnumerator__ctor_m1_27090_gshared)(__this, ___host, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Single>::Dispose()
extern "C" void ShimEnumerator_Dispose_m1_27091_gshared (ShimEnumerator_t1_2716 * __this, const MethodInfo* method);
#define ShimEnumerator_Dispose_m1_27091(__this, method) (( void (*) (ShimEnumerator_t1_2716 *, const MethodInfo*))ShimEnumerator_Dispose_m1_27091_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Single>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m1_27092_gshared (ShimEnumerator_t1_2716 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m1_27092(__this, method) (( bool (*) (ShimEnumerator_t1_2716 *, const MethodInfo*))ShimEnumerator_MoveNext_m1_27092_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Single>::get_Entry()
extern "C" DictionaryEntry_t1_284  ShimEnumerator_get_Entry_m1_27093_gshared (ShimEnumerator_t1_2716 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m1_27093(__this, method) (( DictionaryEntry_t1_284  (*) (ShimEnumerator_t1_2716 *, const MethodInfo*))ShimEnumerator_get_Entry_m1_27093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Single>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m1_27094_gshared (ShimEnumerator_t1_2716 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m1_27094(__this, method) (( Object_t * (*) (ShimEnumerator_t1_2716 *, const MethodInfo*))ShimEnumerator_get_Key_m1_27094_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Single>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m1_27095_gshared (ShimEnumerator_t1_2716 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m1_27095(__this, method) (( Object_t * (*) (ShimEnumerator_t1_2716 *, const MethodInfo*))ShimEnumerator_get_Value_m1_27095_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Single>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m1_27096_gshared (ShimEnumerator_t1_2716 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m1_27096(__this, method) (( Object_t * (*) (ShimEnumerator_t1_2716 *, const MethodInfo*))ShimEnumerator_get_Current_m1_27096_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Single>::Reset()
extern "C" void ShimEnumerator_Reset_m1_27097_gshared (ShimEnumerator_t1_2716 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m1_27097(__this, method) (( void (*) (ShimEnumerator_t1_2716 *, const MethodInfo*))ShimEnumerator_Reset_m1_27097_gshared)(__this, method)
