﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "mscorlib_System_Object.h"
#include "AssemblyU2DCSharp_ExifLibrary_JPEGMarker.h"

// ExifLibrary.JPEGSection
struct  JPEGSection_t8_112  : public Object_t
{
	// ExifLibrary.JPEGMarker ExifLibrary.JPEGSection::<Marker>k__BackingField
	uint8_t ___U3CMarkerU3Ek__BackingField_0;
	// System.Byte[] ExifLibrary.JPEGSection::<Header>k__BackingField
	ByteU5BU5D_t1_109* ___U3CHeaderU3Ek__BackingField_1;
	// System.Byte[] ExifLibrary.JPEGSection::<EntropyData>k__BackingField
	ByteU5BU5D_t1_109* ___U3CEntropyDataU3Ek__BackingField_2;
};
