﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.ArrayList
struct ArrayList_t1_170;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// VoxelBusters.Utility.Demo.JSONDemo
struct  JSONDemo_t8_49  : public MonoBehaviour_t6_91
{
	// System.String VoxelBusters.Utility.Demo.JSONDemo::m_JSONObject
	String_t* ___m_JSONObject_2;
	// System.String VoxelBusters.Utility.Demo.JSONDemo::m_result
	String_t* ___m_result_3;
	// System.Collections.ArrayList VoxelBusters.Utility.Demo.JSONDemo::m_GUIButtons
	ArrayList_t1_170 * ___m_GUIButtons_4;
};
