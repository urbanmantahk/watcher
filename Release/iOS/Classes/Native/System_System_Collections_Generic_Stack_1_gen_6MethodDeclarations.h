﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Stack_1_gen_1MethodDeclarations.h"

// System.Void System.Collections.Generic.Stack`1<UnityEngine.UI.LayoutRebuilder>::.ctor()
#define Stack_1__ctor_m3_2037(__this, method) (( void (*) (Stack_1_t3_272 *, const MethodInfo*))Stack_1__ctor_m3_1816_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<UnityEngine.UI.LayoutRebuilder>::System.Collections.ICollection.get_IsSynchronized()
#define Stack_1_System_Collections_ICollection_get_IsSynchronized_m3_2038(__this, method) (( bool (*) (Stack_1_t3_272 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_IsSynchronized_m3_1818_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1<UnityEngine.UI.LayoutRebuilder>::System.Collections.ICollection.get_SyncRoot()
#define Stack_1_System_Collections_ICollection_get_SyncRoot_m3_2039(__this, method) (( Object_t * (*) (Stack_1_t3_272 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_SyncRoot_m3_1820_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<UnityEngine.UI.LayoutRebuilder>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Stack_1_System_Collections_ICollection_CopyTo_m3_2040(__this, ___dest, ___idx, method) (( void (*) (Stack_1_t3_272 *, Array_t *, int32_t, const MethodInfo*))Stack_1_System_Collections_ICollection_CopyTo_m3_1822_gshared)(__this, ___dest, ___idx, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<UnityEngine.UI.LayoutRebuilder>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3_2041(__this, method) (( Object_t* (*) (Stack_1_t3_272 *, const MethodInfo*))Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3_1824_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<UnityEngine.UI.LayoutRebuilder>::System.Collections.IEnumerable.GetEnumerator()
#define Stack_1_System_Collections_IEnumerable_GetEnumerator_m3_2042(__this, method) (( Object_t * (*) (Stack_1_t3_272 *, const MethodInfo*))Stack_1_System_Collections_IEnumerable_GetEnumerator_m3_1826_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<UnityEngine.UI.LayoutRebuilder>::Contains(T)
#define Stack_1_Contains_m3_2043(__this, ___t, method) (( bool (*) (Stack_1_t3_272 *, LayoutRebuilder_t7_150 *, const MethodInfo*))Stack_1_Contains_m3_1827_gshared)(__this, ___t, method)
// T System.Collections.Generic.Stack`1<UnityEngine.UI.LayoutRebuilder>::Peek()
#define Stack_1_Peek_m3_2044(__this, method) (( LayoutRebuilder_t7_150 * (*) (Stack_1_t3_272 *, const MethodInfo*))Stack_1_Peek_m3_1829_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<UnityEngine.UI.LayoutRebuilder>::Pop()
#define Stack_1_Pop_m3_2045(__this, method) (( LayoutRebuilder_t7_150 * (*) (Stack_1_t3_272 *, const MethodInfo*))Stack_1_Pop_m3_1830_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<UnityEngine.UI.LayoutRebuilder>::Push(T)
#define Stack_1_Push_m3_2046(__this, ___t, method) (( void (*) (Stack_1_t3_272 *, LayoutRebuilder_t7_150 *, const MethodInfo*))Stack_1_Push_m3_1831_gshared)(__this, ___t, method)
// System.Int32 System.Collections.Generic.Stack`1<UnityEngine.UI.LayoutRebuilder>::get_Count()
#define Stack_1_get_Count_m3_2047(__this, method) (( int32_t (*) (Stack_1_t3_272 *, const MethodInfo*))Stack_1_get_Count_m3_1833_gshared)(__this, method)
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<UnityEngine.UI.LayoutRebuilder>::GetEnumerator()
#define Stack_1_GetEnumerator_m3_2048(__this, method) (( Enumerator_t3_292  (*) (Stack_1_t3_272 *, const MethodInfo*))Stack_1_GetEnumerator_m3_1835_gshared)(__this, method)
