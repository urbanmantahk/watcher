﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>
struct ReadOnlyCollection_1_t1_2302;
// System.Collections.Generic.IList`1<UnityEngine.Color32>
struct IList_1_t1_2303;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Object
struct Object_t;
// UnityEngine.Color32[]
struct Color32U5BU5D_t6_280;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Color32>
struct IEnumerator_1_t1_2807;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color32.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C" void ReadOnlyCollection_1__ctor_m1_19409_gshared (ReadOnlyCollection_1_t1_2302 * __this, Object_t* ___list, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m1_19409(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t1_2302 *, Object_t*, const MethodInfo*))ReadOnlyCollection_1__ctor_m1_19409_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1_19410_gshared (ReadOnlyCollection_1_t1_2302 * __this, Color32_t6_49  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1_19410(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t1_2302 *, Color32_t6_49 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1_19410_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::System.Collections.Generic.ICollection<T>.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1_19411_gshared (ReadOnlyCollection_1_t1_2302 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1_19411(__this, method) (( void (*) (ReadOnlyCollection_1_t1_2302 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1_19411_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1_19412_gshared (ReadOnlyCollection_1_t1_2302 * __this, int32_t ___index, Color32_t6_49  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1_19412(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t1_2302 *, int32_t, Color32_t6_49 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1_19412_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1_19413_gshared (ReadOnlyCollection_1_t1_2302 * __this, Color32_t6_49  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1_19413(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t1_2302 *, Color32_t6_49 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1_19413_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1_19414_gshared (ReadOnlyCollection_1_t1_2302 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1_19414(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t1_2302 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1_19414_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C" Color32_t6_49  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1_19415_gshared (ReadOnlyCollection_1_t1_2302 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1_19415(__this, ___index, method) (( Color32_t6_49  (*) (ReadOnlyCollection_1_t1_2302 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1_19415_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1_19416_gshared (ReadOnlyCollection_1_t1_2302 * __this, int32_t ___index, Color32_t6_49  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1_19416(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t1_2302 *, int32_t, Color32_t6_49 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1_19416_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_19417_gshared (ReadOnlyCollection_1_t1_2302 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_19417(__this, method) (( bool (*) (ReadOnlyCollection_1_t1_2302 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_19417_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1_19418_gshared (ReadOnlyCollection_1_t1_2302 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1_19418(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t1_2302 *, Array_t *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1_19418_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1_19419_gshared (ReadOnlyCollection_1_t1_2302 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1_19419(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t1_2302 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1_19419_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::System.Collections.IList.Add(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m1_19420_gshared (ReadOnlyCollection_1_t1_2302 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m1_19420(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t1_2302 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m1_19420_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::System.Collections.IList.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m1_19421_gshared (ReadOnlyCollection_1_t1_2302 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m1_19421(__this, method) (( void (*) (ReadOnlyCollection_1_t1_2302 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m1_19421_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::System.Collections.IList.Contains(System.Object)
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_Contains_m1_19422_gshared (ReadOnlyCollection_1_t1_2302 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m1_19422(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t1_2302 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m1_19422_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1_19423_gshared (ReadOnlyCollection_1_t1_2302 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1_19423(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t1_2302 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1_19423_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m1_19424_gshared (ReadOnlyCollection_1_t1_2302 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m1_19424(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t1_2302 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m1_19424_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::System.Collections.IList.Remove(System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m1_19425_gshared (ReadOnlyCollection_1_t1_2302 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m1_19425(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t1_2302 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m1_19425_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::System.Collections.IList.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1_19426_gshared (ReadOnlyCollection_1_t1_2302 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1_19426(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t1_2302 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1_19426_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1_19427_gshared (ReadOnlyCollection_1_t1_2302 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1_19427(__this, method) (( bool (*) (ReadOnlyCollection_1_t1_2302 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1_19427_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1_19428_gshared (ReadOnlyCollection_1_t1_2302 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1_19428(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t1_2302 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1_19428_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::System.Collections.IList.get_IsFixedSize()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1_19429_gshared (ReadOnlyCollection_1_t1_2302 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1_19429(__this, method) (( bool (*) (ReadOnlyCollection_1_t1_2302 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1_19429_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::System.Collections.IList.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1_19430_gshared (ReadOnlyCollection_1_t1_2302 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1_19430(__this, method) (( bool (*) (ReadOnlyCollection_1_t1_2302 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1_19430_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IList_get_Item_m1_19431_gshared (ReadOnlyCollection_1_t1_2302 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m1_19431(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t1_2302 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m1_19431_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m1_19432_gshared (ReadOnlyCollection_1_t1_2302 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m1_19432(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t1_2302 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m1_19432_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::Contains(T)
extern "C" bool ReadOnlyCollection_1_Contains_m1_19433_gshared (ReadOnlyCollection_1_t1_2302 * __this, Color32_t6_49  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m1_19433(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t1_2302 *, Color32_t6_49 , const MethodInfo*))ReadOnlyCollection_1_Contains_m1_19433_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::CopyTo(T[],System.Int32)
extern "C" void ReadOnlyCollection_1_CopyTo_m1_19434_gshared (ReadOnlyCollection_1_t1_2302 * __this, Color32U5BU5D_t6_280* ___array, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m1_19434(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t1_2302 *, Color32U5BU5D_t6_280*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m1_19434_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::GetEnumerator()
extern "C" Object_t* ReadOnlyCollection_1_GetEnumerator_m1_19435_gshared (ReadOnlyCollection_1_t1_2302 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m1_19435(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t1_2302 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m1_19435_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::IndexOf(T)
extern "C" int32_t ReadOnlyCollection_1_IndexOf_m1_19436_gshared (ReadOnlyCollection_1_t1_2302 * __this, Color32_t6_49  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m1_19436(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t1_2302 *, Color32_t6_49 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m1_19436_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::get_Count()
extern "C" int32_t ReadOnlyCollection_1_get_Count_m1_19437_gshared (ReadOnlyCollection_1_t1_2302 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m1_19437(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t1_2302 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m1_19437_gshared)(__this, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::get_Items()
extern "C" Object_t* ReadOnlyCollection_1_get_Items_m1_19438_gshared (ReadOnlyCollection_1_t1_2302 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Items_m1_19438(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t1_2302 *, const MethodInfo*))ReadOnlyCollection_1_get_Items_m1_19438_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>::get_Item(System.Int32)
extern "C" Color32_t6_49  ReadOnlyCollection_1_get_Item_m1_19439_gshared (ReadOnlyCollection_1_t1_2302 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m1_19439(__this, ___index, method) (( Color32_t6_49  (*) (ReadOnlyCollection_1_t1_2302 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m1_19439_gshared)(__this, ___index, method)
