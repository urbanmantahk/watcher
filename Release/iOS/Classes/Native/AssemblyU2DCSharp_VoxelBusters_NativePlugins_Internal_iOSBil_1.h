﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_BillingTransact.h"

// VoxelBusters.NativePlugins.Internal.iOSBillingTransaction
struct  iOSBillingTransaction_t8_214  : public BillingTransaction_t8_211
{
};
