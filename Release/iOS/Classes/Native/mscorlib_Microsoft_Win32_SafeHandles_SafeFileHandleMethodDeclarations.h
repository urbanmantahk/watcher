﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Microsoft.Win32.SafeHandles.SafeFileHandle
struct SafeFileHandle_t1_85;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"

// System.Void Microsoft.Win32.SafeHandles.SafeFileHandle::.ctor(System.IntPtr,System.Boolean)
extern "C" void SafeFileHandle__ctor_m1_1418 (SafeFileHandle_t1_85 * __this, IntPtr_t ___preexistingHandle, bool ___ownsHandle, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Microsoft.Win32.SafeHandles.SafeFileHandle::.ctor()
extern "C" void SafeFileHandle__ctor_m1_1419 (SafeFileHandle_t1_85 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Microsoft.Win32.SafeHandles.SafeFileHandle::ReleaseHandle()
extern "C" bool SafeFileHandle_ReleaseHandle_m1_1420 (SafeFileHandle_t1_85 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
