﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "System_Xml_Mono_Xml_Schema_XsdToken.h"

// Mono.Xml.Schema.XsdLanguage
struct  XsdLanguage_t4_10  : public XsdToken_t4_9
{
};
