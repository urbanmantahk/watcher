﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.RSAOAEPKeyExchangeDeformatter
struct RSAOAEPKeyExchangeDeformatter_t1_1236;
// System.Security.Cryptography.AsymmetricAlgorithm
struct AsymmetricAlgorithm_t1_228;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Cryptography.RSAOAEPKeyExchangeDeformatter::.ctor()
extern "C" void RSAOAEPKeyExchangeDeformatter__ctor_m1_10552 (RSAOAEPKeyExchangeDeformatter_t1_1236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RSAOAEPKeyExchangeDeformatter::.ctor(System.Security.Cryptography.AsymmetricAlgorithm)
extern "C" void RSAOAEPKeyExchangeDeformatter__ctor_m1_10553 (RSAOAEPKeyExchangeDeformatter_t1_1236 * __this, AsymmetricAlgorithm_t1_228 * ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Cryptography.RSAOAEPKeyExchangeDeformatter::get_Parameters()
extern "C" String_t* RSAOAEPKeyExchangeDeformatter_get_Parameters_m1_10554 (RSAOAEPKeyExchangeDeformatter_t1_1236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RSAOAEPKeyExchangeDeformatter::set_Parameters(System.String)
extern "C" void RSAOAEPKeyExchangeDeformatter_set_Parameters_m1_10555 (RSAOAEPKeyExchangeDeformatter_t1_1236 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.RSAOAEPKeyExchangeDeformatter::DecryptKeyExchange(System.Byte[])
extern "C" ByteU5BU5D_t1_109* RSAOAEPKeyExchangeDeformatter_DecryptKeyExchange_m1_10556 (RSAOAEPKeyExchangeDeformatter_t1_1236 * __this, ByteU5BU5D_t1_109* ___rgbData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RSAOAEPKeyExchangeDeformatter::SetKey(System.Security.Cryptography.AsymmetricAlgorithm)
extern "C" void RSAOAEPKeyExchangeDeformatter_SetKey_m1_10557 (RSAOAEPKeyExchangeDeformatter_t1_1236 * __this, AsymmetricAlgorithm_t1_228 * ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
