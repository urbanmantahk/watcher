﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_iOSBil_0.h"

// VoxelBusters.NativePlugins.Internal.iOSBillingTransaction/SKPaymentTransactionState
struct  SKPaymentTransactionState_t8_213 
{
	// System.Int32 VoxelBusters.NativePlugins.Internal.iOSBillingTransaction/SKPaymentTransactionState::value__
	int32_t ___value___1;
};
