﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XmlNode/EmptyNodeList
struct EmptyNodeList_t4_146;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Xml.XmlNode
struct XmlNode_t4_116;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Xml.XmlNode/EmptyNodeList::.ctor()
extern "C" void EmptyNodeList__ctor_m4_617 (EmptyNodeList_t4_146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XmlNode/EmptyNodeList::.cctor()
extern "C" void EmptyNodeList__cctor_m4_618 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Xml.XmlNode/EmptyNodeList::get_Count()
extern "C" int32_t EmptyNodeList_get_Count_m4_619 (EmptyNodeList_t4_146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Xml.XmlNode/EmptyNodeList::GetEnumerator()
extern "C" Object_t * EmptyNodeList_GetEnumerator_m4_620 (EmptyNodeList_t4_146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNode System.Xml.XmlNode/EmptyNodeList::Item(System.Int32)
extern "C" XmlNode_t4_116 * EmptyNodeList_Item_m4_621 (EmptyNodeList_t4_146 * __this, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
