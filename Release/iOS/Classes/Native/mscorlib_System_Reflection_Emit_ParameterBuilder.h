﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Reflection.MethodBase
struct MethodBase_t1_335;
// System.String
struct String_t;
// System.Reflection.Emit.CustomAttributeBuilder[]
struct CustomAttributeBuilderU5BU5D_t1_469;
// System.Reflection.Emit.UnmanagedMarshal
struct UnmanagedMarshal_t1_505;
// System.Object
struct Object_t;

#include "mscorlib_System_Object.h"
#include "mscorlib_System_Reflection_ParameterAttributes.h"

// System.Reflection.Emit.ParameterBuilder
struct  ParameterBuilder_t1_545  : public Object_t
{
	// System.Reflection.MethodBase System.Reflection.Emit.ParameterBuilder::methodb
	MethodBase_t1_335 * ___methodb_0;
	// System.String System.Reflection.Emit.ParameterBuilder::name
	String_t* ___name_1;
	// System.Reflection.Emit.CustomAttributeBuilder[] System.Reflection.Emit.ParameterBuilder::cattrs
	CustomAttributeBuilderU5BU5D_t1_469* ___cattrs_2;
	// System.Reflection.Emit.UnmanagedMarshal System.Reflection.Emit.ParameterBuilder::marshal_info
	UnmanagedMarshal_t1_505 * ___marshal_info_3;
	// System.Reflection.ParameterAttributes System.Reflection.Emit.ParameterBuilder::attrs
	int32_t ___attrs_4;
	// System.Int32 System.Reflection.Emit.ParameterBuilder::position
	int32_t ___position_5;
	// System.Int32 System.Reflection.Emit.ParameterBuilder::table_idx
	int32_t ___table_idx_6;
	// System.Object System.Reflection.Emit.ParameterBuilder::def_value
	Object_t * ___def_value_7;
};
