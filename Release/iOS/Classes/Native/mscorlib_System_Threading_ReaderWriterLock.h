﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Threading.LockQueue
struct LockQueue_t1_1467;
// System.Collections.Hashtable
struct Hashtable_t1_100;

#include "mscorlib_System_Runtime_ConstrainedExecution_CriticalFinaliz.h"

// System.Threading.ReaderWriterLock
struct  ReaderWriterLock_t1_1468  : public CriticalFinalizerObject_t1_713
{
	// System.Int32 System.Threading.ReaderWriterLock::seq_num
	int32_t ___seq_num_0;
	// System.Int32 System.Threading.ReaderWriterLock::state
	int32_t ___state_1;
	// System.Int32 System.Threading.ReaderWriterLock::readers
	int32_t ___readers_2;
	// System.Threading.LockQueue System.Threading.ReaderWriterLock::writer_queue
	LockQueue_t1_1467 * ___writer_queue_3;
	// System.Collections.Hashtable System.Threading.ReaderWriterLock::reader_locks
	Hashtable_t1_100 * ___reader_locks_4;
	// System.Int32 System.Threading.ReaderWriterLock::writer_lock_owner
	int32_t ___writer_lock_owner_5;
};
