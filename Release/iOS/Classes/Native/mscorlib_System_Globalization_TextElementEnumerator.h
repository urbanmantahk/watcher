﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object.h"

// System.Globalization.TextElementEnumerator
struct  TextElementEnumerator_t1_389  : public Object_t
{
	// System.Int32 System.Globalization.TextElementEnumerator::index
	int32_t ___index_0;
	// System.Int32 System.Globalization.TextElementEnumerator::elementindex
	int32_t ___elementindex_1;
	// System.Int32 System.Globalization.TextElementEnumerator::startpos
	int32_t ___startpos_2;
	// System.String System.Globalization.TextElementEnumerator::str
	String_t* ___str_3;
	// System.String System.Globalization.TextElementEnumerator::element
	String_t* ___element_4;
};
