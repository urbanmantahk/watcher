﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Messaging.MonoMethodMessage
struct MonoMethodMessage_t1_919;
// System.Reflection.MethodBase
struct MethodBase_t1_335;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Type
struct Type_t;
// System.String
struct String_t;
// System.Runtime.Remoting.Identity
struct Identity_t1_943;
// System.Reflection.MonoMethod
struct MonoMethod_t;
// System.Collections.IDictionary
struct IDictionary_t1_35;
// System.Runtime.Remoting.Messaging.LogicalCallContext
struct LogicalCallContext_t1_941;
// System.Object
struct Object_t;
// System.Exception
struct Exception_t1_33;
// System.Runtime.Remoting.Messaging.AsyncResult
struct AsyncResult_t1_916;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_CallType.h"

// System.Void System.Runtime.Remoting.Messaging.MonoMethodMessage::.ctor(System.Reflection.MethodBase,System.Object[])
extern "C" void MonoMethodMessage__ctor_m1_8573 (MonoMethodMessage_t1_919 * __this, MethodBase_t1_335 * ___method, ObjectU5BU5D_t1_272* ___out_args, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Messaging.MonoMethodMessage::.ctor(System.Type,System.String,System.Object[])
extern "C" void MonoMethodMessage__ctor_m1_8574 (MonoMethodMessage_t1_919 * __this, Type_t * ___type, String_t* ___method_name, ObjectU5BU5D_t1_272* ___in_args, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Identity System.Runtime.Remoting.Messaging.MonoMethodMessage::System.Runtime.Remoting.Messaging.IInternalMessage.get_TargetIdentity()
extern "C" Identity_t1_943 * MonoMethodMessage_System_Runtime_Remoting_Messaging_IInternalMessage_get_TargetIdentity_m1_8575 (MonoMethodMessage_t1_919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Messaging.MonoMethodMessage::System.Runtime.Remoting.Messaging.IInternalMessage.set_TargetIdentity(System.Runtime.Remoting.Identity)
extern "C" void MonoMethodMessage_System_Runtime_Remoting_Messaging_IInternalMessage_set_TargetIdentity_m1_8576 (MonoMethodMessage_t1_919 * __this, Identity_t1_943 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Messaging.MonoMethodMessage::InitMessage(System.Reflection.MonoMethod,System.Object[])
extern "C" void MonoMethodMessage_InitMessage_m1_8577 (MonoMethodMessage_t1_919 * __this, MonoMethod_t * ___method, ObjectU5BU5D_t1_272* ___out_args, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IDictionary System.Runtime.Remoting.Messaging.MonoMethodMessage::get_Properties()
extern "C" Object_t * MonoMethodMessage_get_Properties_m1_8578 (MonoMethodMessage_t1_919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.Remoting.Messaging.MonoMethodMessage::get_ArgCount()
extern "C" int32_t MonoMethodMessage_get_ArgCount_m1_8579 (MonoMethodMessage_t1_919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Runtime.Remoting.Messaging.MonoMethodMessage::get_Args()
extern "C" ObjectU5BU5D_t1_272* MonoMethodMessage_get_Args_m1_8580 (MonoMethodMessage_t1_919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Remoting.Messaging.MonoMethodMessage::get_HasVarArgs()
extern "C" bool MonoMethodMessage_get_HasVarArgs_m1_8581 (MonoMethodMessage_t1_919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Messaging.LogicalCallContext System.Runtime.Remoting.Messaging.MonoMethodMessage::get_LogicalCallContext()
extern "C" LogicalCallContext_t1_941 * MonoMethodMessage_get_LogicalCallContext_m1_8582 (MonoMethodMessage_t1_919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Messaging.MonoMethodMessage::set_LogicalCallContext(System.Runtime.Remoting.Messaging.LogicalCallContext)
extern "C" void MonoMethodMessage_set_LogicalCallContext_m1_8583 (MonoMethodMessage_t1_919 * __this, LogicalCallContext_t1_941 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodBase System.Runtime.Remoting.Messaging.MonoMethodMessage::get_MethodBase()
extern "C" MethodBase_t1_335 * MonoMethodMessage_get_MethodBase_m1_8584 (MonoMethodMessage_t1_919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Messaging.MonoMethodMessage::get_MethodName()
extern "C" String_t* MonoMethodMessage_get_MethodName_m1_8585 (MonoMethodMessage_t1_919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Messaging.MonoMethodMessage::get_MethodSignature()
extern "C" Object_t * MonoMethodMessage_get_MethodSignature_m1_8586 (MonoMethodMessage_t1_919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Messaging.MonoMethodMessage::get_TypeName()
extern "C" String_t* MonoMethodMessage_get_TypeName_m1_8587 (MonoMethodMessage_t1_919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Messaging.MonoMethodMessage::get_Uri()
extern "C" String_t* MonoMethodMessage_get_Uri_m1_8588 (MonoMethodMessage_t1_919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Messaging.MonoMethodMessage::set_Uri(System.String)
extern "C" void MonoMethodMessage_set_Uri_m1_8589 (MonoMethodMessage_t1_919 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Messaging.MonoMethodMessage::GetArg(System.Int32)
extern "C" Object_t * MonoMethodMessage_GetArg_m1_8590 (MonoMethodMessage_t1_919 * __this, int32_t ___arg_num, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Messaging.MonoMethodMessage::GetArgName(System.Int32)
extern "C" String_t* MonoMethodMessage_GetArgName_m1_8591 (MonoMethodMessage_t1_919 * __this, int32_t ___arg_num, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.Remoting.Messaging.MonoMethodMessage::get_InArgCount()
extern "C" int32_t MonoMethodMessage_get_InArgCount_m1_8592 (MonoMethodMessage_t1_919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Runtime.Remoting.Messaging.MonoMethodMessage::get_InArgs()
extern "C" ObjectU5BU5D_t1_272* MonoMethodMessage_get_InArgs_m1_8593 (MonoMethodMessage_t1_919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Messaging.MonoMethodMessage::GetInArg(System.Int32)
extern "C" Object_t * MonoMethodMessage_GetInArg_m1_8594 (MonoMethodMessage_t1_919 * __this, int32_t ___arg_num, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Messaging.MonoMethodMessage::GetInArgName(System.Int32)
extern "C" String_t* MonoMethodMessage_GetInArgName_m1_8595 (MonoMethodMessage_t1_919 * __this, int32_t ___arg_num, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception System.Runtime.Remoting.Messaging.MonoMethodMessage::get_Exception()
extern "C" Exception_t1_33 * MonoMethodMessage_get_Exception_m1_8596 (MonoMethodMessage_t1_919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.Remoting.Messaging.MonoMethodMessage::get_OutArgCount()
extern "C" int32_t MonoMethodMessage_get_OutArgCount_m1_8597 (MonoMethodMessage_t1_919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Runtime.Remoting.Messaging.MonoMethodMessage::get_OutArgs()
extern "C" ObjectU5BU5D_t1_272* MonoMethodMessage_get_OutArgs_m1_8598 (MonoMethodMessage_t1_919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Messaging.MonoMethodMessage::get_ReturnValue()
extern "C" Object_t * MonoMethodMessage_get_ReturnValue_m1_8599 (MonoMethodMessage_t1_919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Messaging.MonoMethodMessage::GetOutArg(System.Int32)
extern "C" Object_t * MonoMethodMessage_GetOutArg_m1_8600 (MonoMethodMessage_t1_919 * __this, int32_t ___arg_num, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Messaging.MonoMethodMessage::GetOutArgName(System.Int32)
extern "C" String_t* MonoMethodMessage_GetOutArgName_m1_8601 (MonoMethodMessage_t1_919 * __this, int32_t ___arg_num, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Remoting.Messaging.MonoMethodMessage::get_IsAsync()
extern "C" bool MonoMethodMessage_get_IsAsync_m1_8602 (MonoMethodMessage_t1_919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Messaging.AsyncResult System.Runtime.Remoting.Messaging.MonoMethodMessage::get_AsyncResult()
extern "C" AsyncResult_t1_916 * MonoMethodMessage_get_AsyncResult_m1_8603 (MonoMethodMessage_t1_919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Messaging.CallType System.Runtime.Remoting.Messaging.MonoMethodMessage::get_CallType()
extern "C" int32_t MonoMethodMessage_get_CallType_m1_8604 (MonoMethodMessage_t1_919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Remoting.Messaging.MonoMethodMessage::NeedsOutProcessing(System.Int32&)
extern "C" bool MonoMethodMessage_NeedsOutProcessing_m1_8605 (MonoMethodMessage_t1_919 * __this, int32_t* ___outCount, const MethodInfo* method) IL2CPP_METHOD_ATTR;
