﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Object_t;

#include "mscorlib_System_Object.h"

// System.Runtime.Remoting.RemotingServices/CACD
struct  CACD_t1_1026  : public Object_t
{
	// System.Object System.Runtime.Remoting.RemotingServices/CACD::d
	Object_t * ___d_0;
	// System.Object System.Runtime.Remoting.RemotingServices/CACD::c
	Object_t * ___c_1;
};
