﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.CspParameters
struct CspParameters_t1_164;
// System.String
struct String_t;
// System.Security.AccessControl.CryptoKeySecurity
struct CryptoKeySecurity_t1_1142;
// System.Security.SecureString
struct SecureString_t1_1197;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_Security_Cryptography_CspProviderFlags.h"

// System.Void System.Security.Cryptography.CspParameters::.ctor()
extern "C" void CspParameters__ctor_m1_10245 (CspParameters_t1_164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.CspParameters::.ctor(System.Int32)
extern "C" void CspParameters__ctor_m1_10246 (CspParameters_t1_164 * __this, int32_t ___dwTypeIn, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.CspParameters::.ctor(System.Int32,System.String)
extern "C" void CspParameters__ctor_m1_10247 (CspParameters_t1_164 * __this, int32_t ___dwTypeIn, String_t* ___strProviderNameIn, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.CspParameters::.ctor(System.Int32,System.String,System.String)
extern "C" void CspParameters__ctor_m1_10248 (CspParameters_t1_164 * __this, int32_t ___dwTypeIn, String_t* ___strProviderNameIn, String_t* ___strContainerNameIn, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.CspParameters::.ctor(System.Int32,System.String,System.String,System.Security.AccessControl.CryptoKeySecurity,System.IntPtr)
extern "C" void CspParameters__ctor_m1_10249 (CspParameters_t1_164 * __this, int32_t ___providerType, String_t* ___providerName, String_t* ___keyContainerName, CryptoKeySecurity_t1_1142 * ___cryptoKeySecurity, IntPtr_t ___parentWindowHandle, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.CspParameters::.ctor(System.Int32,System.String,System.String,System.Security.AccessControl.CryptoKeySecurity,System.Security.SecureString)
extern "C" void CspParameters__ctor_m1_10250 (CspParameters_t1_164 * __this, int32_t ___providerType, String_t* ___providerName, String_t* ___keyContainerName, CryptoKeySecurity_t1_1142 * ___cryptoKeySecurity, SecureString_t1_1197 * ___keyPassword, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.CspProviderFlags System.Security.Cryptography.CspParameters::get_Flags()
extern "C" int32_t CspParameters_get_Flags_m1_10251 (CspParameters_t1_164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.CspParameters::set_Flags(System.Security.Cryptography.CspProviderFlags)
extern "C" void CspParameters_set_Flags_m1_10252 (CspParameters_t1_164 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.AccessControl.CryptoKeySecurity System.Security.Cryptography.CspParameters::get_CryptoKeySecurity()
extern "C" CryptoKeySecurity_t1_1142 * CspParameters_get_CryptoKeySecurity_m1_10253 (CspParameters_t1_164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.CspParameters::set_CryptoKeySecurity(System.Security.AccessControl.CryptoKeySecurity)
extern "C" void CspParameters_set_CryptoKeySecurity_m1_10254 (CspParameters_t1_164 * __this, CryptoKeySecurity_t1_1142 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecureString System.Security.Cryptography.CspParameters::get_KeyPassword()
extern "C" SecureString_t1_1197 * CspParameters_get_KeyPassword_m1_10255 (CspParameters_t1_164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.CspParameters::set_KeyPassword(System.Security.SecureString)
extern "C" void CspParameters_set_KeyPassword_m1_10256 (CspParameters_t1_164 * __this, SecureString_t1_1197 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Security.Cryptography.CspParameters::get_ParentWindowHandle()
extern "C" IntPtr_t CspParameters_get_ParentWindowHandle_m1_10257 (CspParameters_t1_164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.CspParameters::set_ParentWindowHandle(System.IntPtr)
extern "C" void CspParameters_set_ParentWindowHandle_m1_10258 (CspParameters_t1_164 * __this, IntPtr_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
