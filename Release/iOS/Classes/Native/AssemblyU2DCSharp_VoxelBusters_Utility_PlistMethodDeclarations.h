﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.Utility.Plist
struct Plist_t8_51;
// System.String
struct String_t;
// System.Object
struct Object_t;
// System.Xml.XmlNode
struct XmlNode_t4_116;
// System.Collections.IList
struct IList_t1_262;
// System.Collections.IDictionary
struct IDictionary_t1_35;
// System.Xml.XmlWriter
struct XmlWriter_t4_182;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.Utility.Plist::.ctor()
extern "C" void Plist__ctor_m8_319 (Plist_t8_51 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.Utility.Plist VoxelBusters.Utility.Plist::LoadPlistAtPath(System.String)
extern "C" Plist_t8_51 * Plist_LoadPlistAtPath_m8_320 (Object_t * __this /* static, unused */, String_t* ____filePath, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.Utility.Plist VoxelBusters.Utility.Plist::Load(System.String)
extern "C" Plist_t8_51 * Plist_Load_m8_321 (Object_t * __this /* static, unused */, String_t* ____plistTextContents, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.Plist::ParsePlistText(System.String)
extern "C" void Plist_ParsePlistText_m8_322 (Plist_t8_51 * __this, String_t* ____text, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VoxelBusters.Utility.Plist::ParseValueNode(System.Xml.XmlNode)
extern "C" Object_t * Plist_ParseValueNode_m8_323 (Plist_t8_51 * __this, XmlNode_t4_116 * ____valNode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.Plist::ParseListNode(System.Xml.XmlNode,System.Collections.IList&)
extern "C" void Plist_ParseListNode_m8_324 (Plist_t8_51 * __this, XmlNode_t4_116 * ____listNode, Object_t ** ____parsedList, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.Plist::ParseDictionaryNode(System.Xml.XmlNode,System.Collections.IDictionary&)
extern "C" void Plist_ParseDictionaryNode_m8_325 (Plist_t8_51 * __this, XmlNode_t4_116 * ____dictNode, Object_t ** ____parsedDict, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VoxelBusters.Utility.Plist::GetKeyPathValue(System.String)
extern "C" Object_t * Plist_GetKeyPathValue_m8_326 (Plist_t8_51 * __this, String_t* ____keyPath, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.Plist::AddValue(System.String,System.Object)
extern "C" void Plist_AddValue_m8_327 (Plist_t8_51 * __this, String_t* ____keyPath, Object_t * ____newValue, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.Plist::Save(System.String)
extern "C" void Plist_Save_m8_328 (Plist_t8_51 * __this, String_t* ____saveToPath, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.Plist::WriteXMLNode(System.Xml.XmlWriter,System.Object)
extern "C" void Plist_WriteXMLNode_m8_329 (Plist_t8_51 * __this, XmlWriter_t4_182 * ____xmlWriter, Object_t * ____value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.Plist::WriteXMLBoolNode(System.Xml.XmlWriter,System.Boolean)
extern "C" void Plist_WriteXMLBoolNode_m8_330 (Plist_t8_51 * __this, XmlWriter_t4_182 * ____xmlWriter, bool ____value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.Plist::WriteXMLIntegerNode(System.Xml.XmlWriter,System.Int32)
extern "C" void Plist_WriteXMLIntegerNode_m8_331 (Plist_t8_51 * __this, XmlWriter_t4_182 * ____xmlWriter, int32_t ____value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.Plist::WriteXMLStringNode(System.Xml.XmlWriter,System.String)
extern "C" void Plist_WriteXMLStringNode_m8_332 (Plist_t8_51 * __this, XmlWriter_t4_182 * ____xmlWriter, String_t* ____value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.Plist::WriteXMLListNode(System.Xml.XmlWriter,System.Collections.IList)
extern "C" void Plist_WriteXMLListNode_m8_333 (Plist_t8_51 * __this, XmlWriter_t4_182 * ____xmlWriter, Object_t * ____listValue, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.Plist::WriteXMLDictionaryNode(System.Xml.XmlWriter,System.Collections.IDictionary)
extern "C" void Plist_WriteXMLDictionaryNode_m8_334 (Plist_t8_51 * __this, XmlWriter_t4_182 * ____xmlWriter, Object_t * ____dictValue, const MethodInfo* method) IL2CPP_METHOD_ATTR;
