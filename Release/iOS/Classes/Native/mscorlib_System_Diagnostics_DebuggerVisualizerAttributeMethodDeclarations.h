﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Diagnostics.DebuggerVisualizerAttribute
struct DebuggerVisualizerAttribute_t1_333;
// System.String
struct String_t;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Diagnostics.DebuggerVisualizerAttribute::.ctor(System.String)
extern "C" void DebuggerVisualizerAttribute__ctor_m1_3608 (DebuggerVisualizerAttribute_t1_333 * __this, String_t* ___visualizerTypeName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.DebuggerVisualizerAttribute::.ctor(System.Type)
extern "C" void DebuggerVisualizerAttribute__ctor_m1_3609 (DebuggerVisualizerAttribute_t1_333 * __this, Type_t * ___visualizer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.DebuggerVisualizerAttribute::.ctor(System.String,System.String)
extern "C" void DebuggerVisualizerAttribute__ctor_m1_3610 (DebuggerVisualizerAttribute_t1_333 * __this, String_t* ___visualizerTypeName, String_t* ___visualizerObjectSourceTypeName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.DebuggerVisualizerAttribute::.ctor(System.String,System.Type)
extern "C" void DebuggerVisualizerAttribute__ctor_m1_3611 (DebuggerVisualizerAttribute_t1_333 * __this, String_t* ___visualizerTypeName, Type_t * ___visualizerObjectSource, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.DebuggerVisualizerAttribute::.ctor(System.Type,System.String)
extern "C" void DebuggerVisualizerAttribute__ctor_m1_3612 (DebuggerVisualizerAttribute_t1_333 * __this, Type_t * ___visualizer, String_t* ___visualizerObjectSourceTypeName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.DebuggerVisualizerAttribute::.ctor(System.Type,System.Type)
extern "C" void DebuggerVisualizerAttribute__ctor_m1_3613 (DebuggerVisualizerAttribute_t1_333 * __this, Type_t * ___visualizer, Type_t * ___visualizerObjectSource, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Diagnostics.DebuggerVisualizerAttribute::get_Description()
extern "C" String_t* DebuggerVisualizerAttribute_get_Description_m1_3614 (DebuggerVisualizerAttribute_t1_333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.DebuggerVisualizerAttribute::set_Description(System.String)
extern "C" void DebuggerVisualizerAttribute_set_Description_m1_3615 (DebuggerVisualizerAttribute_t1_333 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Diagnostics.DebuggerVisualizerAttribute::get_Target()
extern "C" Type_t * DebuggerVisualizerAttribute_get_Target_m1_3616 (DebuggerVisualizerAttribute_t1_333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.DebuggerVisualizerAttribute::set_Target(System.Type)
extern "C" void DebuggerVisualizerAttribute_set_Target_m1_3617 (DebuggerVisualizerAttribute_t1_333 * __this, Type_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Diagnostics.DebuggerVisualizerAttribute::get_TargetTypeName()
extern "C" String_t* DebuggerVisualizerAttribute_get_TargetTypeName_m1_3618 (DebuggerVisualizerAttribute_t1_333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.DebuggerVisualizerAttribute::set_TargetTypeName(System.String)
extern "C" void DebuggerVisualizerAttribute_set_TargetTypeName_m1_3619 (DebuggerVisualizerAttribute_t1_333 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Diagnostics.DebuggerVisualizerAttribute::get_VisualizerObjectSourceTypeName()
extern "C" String_t* DebuggerVisualizerAttribute_get_VisualizerObjectSourceTypeName_m1_3620 (DebuggerVisualizerAttribute_t1_333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Diagnostics.DebuggerVisualizerAttribute::get_VisualizerTypeName()
extern "C" String_t* DebuggerVisualizerAttribute_get_VisualizerTypeName_m1_3621 (DebuggerVisualizerAttribute_t1_333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
