﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Object_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen_0.h"
#include "mscorlib_System_TimeSpan.h"

// System.Void System.Nullable`1<System.TimeSpan>::.ctor(T)
extern "C" void Nullable_1__ctor_m1_14912_gshared (Nullable_1_t1_1740 * __this, TimeSpan_t1_368  ___value, const MethodInfo* method);
#define Nullable_1__ctor_m1_14912(__this, ___value, method) (( void (*) (Nullable_1_t1_1740 *, TimeSpan_t1_368 , const MethodInfo*))Nullable_1__ctor_m1_14912_gshared)(__this, ___value, method)
// System.Boolean System.Nullable`1<System.TimeSpan>::get_HasValue()
extern "C" bool Nullable_1_get_HasValue_m1_14913_gshared (Nullable_1_t1_1740 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m1_14913(__this, method) (( bool (*) (Nullable_1_t1_1740 *, const MethodInfo*))Nullable_1_get_HasValue_m1_14913_gshared)(__this, method)
// T System.Nullable`1<System.TimeSpan>::get_Value()
extern "C" TimeSpan_t1_368  Nullable_1_get_Value_m1_14914_gshared (Nullable_1_t1_1740 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m1_14914(__this, method) (( TimeSpan_t1_368  (*) (Nullable_1_t1_1740 *, const MethodInfo*))Nullable_1_get_Value_m1_14914_gshared)(__this, method)
// System.Boolean System.Nullable`1<System.TimeSpan>::Equals(System.Object)
extern "C" bool Nullable_1_Equals_m1_17601_gshared (Nullable_1_t1_1740 * __this, Object_t * ___other, const MethodInfo* method);
#define Nullable_1_Equals_m1_17601(__this, ___other, method) (( bool (*) (Nullable_1_t1_1740 *, Object_t *, const MethodInfo*))Nullable_1_Equals_m1_17601_gshared)(__this, ___other, method)
// System.Boolean System.Nullable`1<System.TimeSpan>::Equals(System.Nullable`1<T>)
extern "C" bool Nullable_1_Equals_m1_17602_gshared (Nullable_1_t1_1740 * __this, Nullable_1_t1_1740  ___other, const MethodInfo* method);
#define Nullable_1_Equals_m1_17602(__this, ___other, method) (( bool (*) (Nullable_1_t1_1740 *, Nullable_1_t1_1740 , const MethodInfo*))Nullable_1_Equals_m1_17602_gshared)(__this, ___other, method)
// System.Int32 System.Nullable`1<System.TimeSpan>::GetHashCode()
extern "C" int32_t Nullable_1_GetHashCode_m1_17603_gshared (Nullable_1_t1_1740 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m1_17603(__this, method) (( int32_t (*) (Nullable_1_t1_1740 *, const MethodInfo*))Nullable_1_GetHashCode_m1_17603_gshared)(__this, method)
// T System.Nullable`1<System.TimeSpan>::GetValueOrDefault()
extern "C" TimeSpan_t1_368  Nullable_1_GetValueOrDefault_m1_17604_gshared (Nullable_1_t1_1740 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m1_17604(__this, method) (( TimeSpan_t1_368  (*) (Nullable_1_t1_1740 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m1_17604_gshared)(__this, method)
// T System.Nullable`1<System.TimeSpan>::GetValueOrDefault(T)
extern "C" TimeSpan_t1_368  Nullable_1_GetValueOrDefault_m1_17605_gshared (Nullable_1_t1_1740 * __this, TimeSpan_t1_368  ___defaultValue, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m1_17605(__this, ___defaultValue, method) (( TimeSpan_t1_368  (*) (Nullable_1_t1_1740 *, TimeSpan_t1_368 , const MethodInfo*))Nullable_1_GetValueOrDefault_m1_17605_gshared)(__this, ___defaultValue, method)
// System.String System.Nullable`1<System.TimeSpan>::ToString()
extern "C" String_t* Nullable_1_ToString_m1_17606_gshared (Nullable_1_t1_1740 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m1_17606(__this, method) (( String_t* (*) (Nullable_1_t1_1740 *, const MethodInfo*))Nullable_1_ToString_m1_17606_gshared)(__this, method)
// System.Object System.Nullable`1<System.TimeSpan>::Box(System.Nullable`1<T>)
extern "C" Object_t * Nullable_1_Box_m1_17607_gshared (Object_t * __this /* static, unused */, Nullable_1_t1_1740  ___o, const MethodInfo* method);
#define Nullable_1_Box_m1_17607(__this /* static, unused */, ___o, method) (( Object_t * (*) (Object_t * /* static, unused */, Nullable_1_t1_1740 , const MethodInfo*))Nullable_1_Box_m1_17607_gshared)(__this /* static, unused */, ___o, method)
// System.Nullable`1<T> System.Nullable`1<System.TimeSpan>::Unbox(System.Object)
extern "C" Nullable_1_t1_1740  Nullable_1_Unbox_m1_17608_gshared (Object_t * __this /* static, unused */, Object_t * ___o, const MethodInfo* method);
#define Nullable_1_Unbox_m1_17608(__this /* static, unused */, ___o, method) (( Nullable_1_t1_1740  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Nullable_1_Unbox_m1_17608_gshared)(__this /* static, unused */, ___o, method)
// System.Nullable`1<T> System.Nullable`1<System.TimeSpan>::op_Implicit(T)
extern "C" Nullable_1_t1_1740  Nullable_1_op_Implicit_m1_17609_gshared (Object_t * __this /* static, unused */, TimeSpan_t1_368  ___value, const MethodInfo* method);
#define Nullable_1_op_Implicit_m1_17609(__this /* static, unused */, ___value, method) (( Nullable_1_t1_1740  (*) (Object_t * /* static, unused */, TimeSpan_t1_368 , const MethodInfo*))Nullable_1_op_Implicit_m1_17609_gshared)(__this /* static, unused */, ___value, method)
// T System.Nullable`1<System.TimeSpan>::op_Explicit(System.Nullable`1<T>)
extern "C" TimeSpan_t1_368  Nullable_1_op_Explicit_m1_17610_gshared (Object_t * __this /* static, unused */, Nullable_1_t1_1740  ___value, const MethodInfo* method);
#define Nullable_1_op_Explicit_m1_17610(__this /* static, unused */, ___value, method) (( TimeSpan_t1_368  (*) (Object_t * /* static, unused */, Nullable_1_t1_1740 , const MethodInfo*))Nullable_1_op_Explicit_m1_17610_gshared)(__this /* static, unused */, ___value, method)
