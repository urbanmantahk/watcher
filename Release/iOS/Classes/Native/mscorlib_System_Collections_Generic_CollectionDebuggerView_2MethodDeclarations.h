﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.CollectionDebuggerView`2<System.Object,System.Object>
struct CollectionDebuggerView_2_t1_2013;
// System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct ICollection_1_t1_2014;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t1_2186;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Generic.CollectionDebuggerView`2<System.Object,System.Object>::.ctor(System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<T,U>>)
extern "C" void CollectionDebuggerView_2__ctor_m1_15898_gshared (CollectionDebuggerView_2_t1_2013 * __this, Object_t* ___col, const MethodInfo* method);
#define CollectionDebuggerView_2__ctor_m1_15898(__this, ___col, method) (( void (*) (CollectionDebuggerView_2_t1_2013 *, Object_t*, const MethodInfo*))CollectionDebuggerView_2__ctor_m1_15898_gshared)(__this, ___col, method)
// System.Collections.Generic.KeyValuePair`2<T,U>[] System.Collections.Generic.CollectionDebuggerView`2<System.Object,System.Object>::get_Items()
extern "C" KeyValuePair_2U5BU5D_t1_2186* CollectionDebuggerView_2_get_Items_m1_15899_gshared (CollectionDebuggerView_2_t1_2013 * __this, const MethodInfo* method);
#define CollectionDebuggerView_2_get_Items_m1_15899(__this, method) (( KeyValuePair_2U5BU5D_t1_2186* (*) (CollectionDebuggerView_2_t1_2013 *, const MethodInfo*))CollectionDebuggerView_2_get_Items_m1_15899_gshared)(__this, method)
