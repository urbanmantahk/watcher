﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Threading.AutoResetEvent
struct AutoResetEvent_t1_1460;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Threading.AutoResetEvent::.ctor(System.Boolean)
extern "C" void AutoResetEvent__ctor_m1_12623 (AutoResetEvent_t1_1460 * __this, bool ___initialState, const MethodInfo* method) IL2CPP_METHOD_ATTR;
