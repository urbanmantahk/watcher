﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.AsymmetricSignatureFormatter
struct AsymmetricSignatureFormatter_t1_1186;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Security.Cryptography.HashAlgorithm
struct HashAlgorithm_t1_162;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Cryptography.AsymmetricSignatureFormatter::.ctor()
extern "C" void AsymmetricSignatureFormatter__ctor_m1_10170 (AsymmetricSignatureFormatter_t1_1186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.AsymmetricSignatureFormatter::CreateSignature(System.Security.Cryptography.HashAlgorithm)
extern "C" ByteU5BU5D_t1_109* AsymmetricSignatureFormatter_CreateSignature_m1_10171 (AsymmetricSignatureFormatter_t1_1186 * __this, HashAlgorithm_t1_162 * ___hash, const MethodInfo* method) IL2CPP_METHOD_ATTR;
