﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.BillingProduct
struct BillingProduct_t8_207;
// System.String
struct String_t;
// VoxelBusters.NativePlugins.PlatformID[]
struct PlatformIDU5BU5D_t8_329;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.BillingProduct::.ctor()
extern "C" void BillingProduct__ctor_m8_1160 (BillingProduct_t8_207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.BillingProduct::.ctor(VoxelBusters.NativePlugins.BillingProduct)
extern "C" void BillingProduct__ctor_m8_1161 (BillingProduct_t8_207 * __this, BillingProduct_t8_207 * ____product, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.BillingProduct::get_Name()
extern "C" String_t* BillingProduct_get_Name_m8_1162 (BillingProduct_t8_207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.BillingProduct::set_Name(System.String)
extern "C" void BillingProduct_set_Name_m8_1163 (BillingProduct_t8_207 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.BillingProduct::get_Description()
extern "C" String_t* BillingProduct_get_Description_m8_1164 (BillingProduct_t8_207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.BillingProduct::set_Description(System.String)
extern "C" void BillingProduct_set_Description_m8_1165 (BillingProduct_t8_207 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.NativePlugins.BillingProduct::get_IsConsumable()
extern "C" bool BillingProduct_get_IsConsumable_m8_1166 (BillingProduct_t8_207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.BillingProduct::set_IsConsumable(System.Boolean)
extern "C" void BillingProduct_set_IsConsumable_m8_1167 (BillingProduct_t8_207 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single VoxelBusters.NativePlugins.BillingProduct::get_Price()
extern "C" float BillingProduct_get_Price_m8_1168 (BillingProduct_t8_207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.BillingProduct::set_Price(System.Single)
extern "C" void BillingProduct_set_Price_m8_1169 (BillingProduct_t8_207 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.BillingProduct::get_LocalizedPrice()
extern "C" String_t* BillingProduct_get_LocalizedPrice_m8_1170 (BillingProduct_t8_207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.BillingProduct::set_LocalizedPrice(System.String)
extern "C" void BillingProduct_set_LocalizedPrice_m8_1171 (BillingProduct_t8_207 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.BillingProduct::get_CurrencyCode()
extern "C" String_t* BillingProduct_get_CurrencyCode_m8_1172 (BillingProduct_t8_207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.BillingProduct::set_CurrencyCode(System.String)
extern "C" void BillingProduct_set_CurrencyCode_m8_1173 (BillingProduct_t8_207 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.BillingProduct::get_CurrencySymbol()
extern "C" String_t* BillingProduct_get_CurrencySymbol_m8_1174 (BillingProduct_t8_207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.BillingProduct::set_CurrencySymbol(System.String)
extern "C" void BillingProduct_set_CurrencySymbol_m8_1175 (BillingProduct_t8_207 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.BillingProduct::get_IOSProductID()
extern "C" String_t* BillingProduct_get_IOSProductID_m8_1176 (BillingProduct_t8_207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.BillingProduct::set_IOSProductID(System.String)
extern "C" void BillingProduct_set_IOSProductID_m8_1177 (BillingProduct_t8_207 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.BillingProduct::get_AndroidProductID()
extern "C" String_t* BillingProduct_get_AndroidProductID_m8_1178 (BillingProduct_t8_207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.BillingProduct::set_AndroidProductID(System.String)
extern "C" void BillingProduct_set_AndroidProductID_m8_1179 (BillingProduct_t8_207 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.BillingProduct::get_ProductIdentifier()
extern "C" String_t* BillingProduct_get_ProductIdentifier_m8_1180 (BillingProduct_t8_207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.NativePlugins.BillingProduct VoxelBusters.NativePlugins.BillingProduct::Create(System.String,System.Boolean,VoxelBusters.NativePlugins.PlatformID[])
extern "C" BillingProduct_t8_207 * BillingProduct_Create_m8_1181 (Object_t * __this /* static, unused */, String_t* ____name, bool ____isConsumable, PlatformIDU5BU5D_t8_329* ____platformIDs, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.BillingProduct::ToString()
extern "C" String_t* BillingProduct_ToString_m8_1182 (BillingProduct_t8_207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.NativePlugins.BillingProduct VoxelBusters.NativePlugins.BillingProduct::Copy()
extern "C" BillingProduct_t8_207 * BillingProduct_Copy_m8_1183 (BillingProduct_t8_207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
