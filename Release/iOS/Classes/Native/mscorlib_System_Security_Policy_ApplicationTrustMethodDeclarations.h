﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Policy.ApplicationTrust
struct ApplicationTrust_t1_1333;
// System.ApplicationIdentity
struct ApplicationIdentity_t1_718;
// System.Security.PermissionSet
struct PermissionSet_t1_563;
// System.Collections.Generic.IEnumerable`1<System.Security.Policy.StrongName>
struct IEnumerable_1_t1_1731;
// System.Security.Policy.PolicyStatement
struct PolicyStatement_t1_1334;
// System.Object
struct Object_t;
// System.Security.SecurityElement
struct SecurityElement_t1_242;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Policy.ApplicationTrust::.ctor()
extern "C" void ApplicationTrust__ctor_m1_11356 (ApplicationTrust_t1_1333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.ApplicationTrust::.ctor(System.ApplicationIdentity)
extern "C" void ApplicationTrust__ctor_m1_11357 (ApplicationTrust_t1_1333 * __this, ApplicationIdentity_t1_718 * ___applicationIdentity, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.ApplicationTrust::.ctor(System.Security.PermissionSet,System.Collections.Generic.IEnumerable`1<System.Security.Policy.StrongName>)
extern "C" void ApplicationTrust__ctor_m1_11358 (ApplicationTrust_t1_1333 * __this, PermissionSet_t1_563 * ___defaultGrantSet, Object_t* ___fullTrustAssemblies, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ApplicationIdentity System.Security.Policy.ApplicationTrust::get_ApplicationIdentity()
extern "C" ApplicationIdentity_t1_718 * ApplicationTrust_get_ApplicationIdentity_m1_11359 (ApplicationTrust_t1_1333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.ApplicationTrust::set_ApplicationIdentity(System.ApplicationIdentity)
extern "C" void ApplicationTrust_set_ApplicationIdentity_m1_11360 (ApplicationTrust_t1_1333 * __this, ApplicationIdentity_t1_718 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.PolicyStatement System.Security.Policy.ApplicationTrust::get_DefaultGrantSet()
extern "C" PolicyStatement_t1_1334 * ApplicationTrust_get_DefaultGrantSet_m1_11361 (ApplicationTrust_t1_1333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.ApplicationTrust::set_DefaultGrantSet(System.Security.Policy.PolicyStatement)
extern "C" void ApplicationTrust_set_DefaultGrantSet_m1_11362 (ApplicationTrust_t1_1333 * __this, PolicyStatement_t1_1334 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Security.Policy.ApplicationTrust::get_ExtraInfo()
extern "C" Object_t * ApplicationTrust_get_ExtraInfo_m1_11363 (ApplicationTrust_t1_1333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.ApplicationTrust::set_ExtraInfo(System.Object)
extern "C" void ApplicationTrust_set_ExtraInfo_m1_11364 (ApplicationTrust_t1_1333 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Policy.ApplicationTrust::get_IsApplicationTrustedToRun()
extern "C" bool ApplicationTrust_get_IsApplicationTrustedToRun_m1_11365 (ApplicationTrust_t1_1333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.ApplicationTrust::set_IsApplicationTrustedToRun(System.Boolean)
extern "C" void ApplicationTrust_set_IsApplicationTrustedToRun_m1_11366 (ApplicationTrust_t1_1333 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Policy.ApplicationTrust::get_Persist()
extern "C" bool ApplicationTrust_get_Persist_m1_11367 (ApplicationTrust_t1_1333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.ApplicationTrust::set_Persist(System.Boolean)
extern "C" void ApplicationTrust_set_Persist_m1_11368 (ApplicationTrust_t1_1333 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.ApplicationTrust::FromXml(System.Security.SecurityElement)
extern "C" void ApplicationTrust_FromXml_m1_11369 (ApplicationTrust_t1_1333 * __this, SecurityElement_t1_242 * ___element, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityElement System.Security.Policy.ApplicationTrust::ToXml()
extern "C" SecurityElement_t1_242 * ApplicationTrust_ToXml_m1_11370 (ApplicationTrust_t1_1333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.PolicyStatement System.Security.Policy.ApplicationTrust::GetDefaultGrantSet()
extern "C" PolicyStatement_t1_1334 * ApplicationTrust_GetDefaultGrantSet_m1_11371 (ApplicationTrust_t1_1333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
