﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_ShareI.h"

// VoxelBusters.NativePlugins.WhatsAppShareComposer
struct  WhatsAppShareComposer_t8_285  : public ShareImageUtility_t8_280
{
	// System.String VoxelBusters.NativePlugins.WhatsAppShareComposer::<Text>k__BackingField
	String_t* ___U3CTextU3Ek__BackingField_3;
	// System.Byte[] VoxelBusters.NativePlugins.WhatsAppShareComposer::<ImageData>k__BackingField
	ByteU5BU5D_t1_109* ___U3CImageDataU3Ek__BackingField_4;
};
