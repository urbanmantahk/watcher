﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Xml.XmlTextReader
struct XmlTextReader_t4_177;
// Mono.Xml2.XmlTextReader
struct XmlTextReader_t4_169;
// System.Collections.Generic.Stack`1<System.String>
struct Stack_1_t3_17;

#include "System_Xml_System_Xml_XmlReader.h"

// System.Xml.XmlTextReader
struct  XmlTextReader_t4_177  : public XmlReader_t4_160
{
	// System.Xml.XmlTextReader System.Xml.XmlTextReader::entity
	XmlTextReader_t4_177 * ___entity_2;
	// Mono.Xml2.XmlTextReader System.Xml.XmlTextReader::source
	XmlTextReader_t4_169 * ___source_3;
	// System.Boolean System.Xml.XmlTextReader::entityInsideAttribute
	bool ___entityInsideAttribute_4;
	// System.Boolean System.Xml.XmlTextReader::insideAttribute
	bool ___insideAttribute_5;
	// System.Collections.Generic.Stack`1<System.String> System.Xml.XmlTextReader::entityNameStack
	Stack_1_t3_17 * ___entityNameStack_6;
};
