﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.AccessControl.RegistryAuditRule
struct RegistryAuditRule_t1_1173;
// System.Security.Principal.IdentityReference
struct IdentityReference_t1_1120;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_AccessControl_RegistryRights.h"
#include "mscorlib_System_Security_AccessControl_InheritanceFlags.h"
#include "mscorlib_System_Security_AccessControl_PropagationFlags.h"
#include "mscorlib_System_Security_AccessControl_AuditFlags.h"

// System.Void System.Security.AccessControl.RegistryAuditRule::.ctor(System.Security.Principal.IdentityReference,System.Security.AccessControl.RegistryRights,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.AuditFlags)
extern "C" void RegistryAuditRule__ctor_m1_10069 (RegistryAuditRule_t1_1173 * __this, IdentityReference_t1_1120 * ___identity, int32_t ___registryRights, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___flags, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.RegistryAuditRule::.ctor(System.String,System.Security.AccessControl.RegistryRights,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.AuditFlags)
extern "C" void RegistryAuditRule__ctor_m1_10070 (RegistryAuditRule_t1_1173 * __this, String_t* ___identity, int32_t ___registryRights, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___flags, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.AccessControl.RegistryRights System.Security.AccessControl.RegistryAuditRule::get_RegistryRights()
extern "C" int32_t RegistryAuditRule_get_RegistryRights_m1_10071 (RegistryAuditRule_t1_1173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
