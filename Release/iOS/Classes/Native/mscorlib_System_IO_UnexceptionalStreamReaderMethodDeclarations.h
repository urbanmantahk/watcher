﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.IO.UnexceptionalStreamReader
struct UnexceptionalStreamReader_t1_457;
// System.IO.Stream
struct Stream_t1_405;
// System.Text.Encoding
struct Encoding_t1_406;
// System.String
struct String_t;
// System.Char[]
struct CharU5BU5D_t1_16;

#include "codegen/il2cpp-codegen.h"

// System.Void System.IO.UnexceptionalStreamReader::.ctor(System.IO.Stream)
extern "C" void UnexceptionalStreamReader__ctor_m1_5399 (UnexceptionalStreamReader_t1_457 * __this, Stream_t1_405 * ___stream, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.UnexceptionalStreamReader::.ctor(System.IO.Stream,System.Boolean)
extern "C" void UnexceptionalStreamReader__ctor_m1_5400 (UnexceptionalStreamReader_t1_457 * __this, Stream_t1_405 * ___stream, bool ___detect_encoding_from_bytemarks, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.UnexceptionalStreamReader::.ctor(System.IO.Stream,System.Text.Encoding)
extern "C" void UnexceptionalStreamReader__ctor_m1_5401 (UnexceptionalStreamReader_t1_457 * __this, Stream_t1_405 * ___stream, Encoding_t1_406 * ___encoding, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.UnexceptionalStreamReader::.ctor(System.IO.Stream,System.Text.Encoding,System.Boolean)
extern "C" void UnexceptionalStreamReader__ctor_m1_5402 (UnexceptionalStreamReader_t1_457 * __this, Stream_t1_405 * ___stream, Encoding_t1_406 * ___encoding, bool ___detect_encoding_from_bytemarks, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.UnexceptionalStreamReader::.ctor(System.IO.Stream,System.Text.Encoding,System.Boolean,System.Int32)
extern "C" void UnexceptionalStreamReader__ctor_m1_5403 (UnexceptionalStreamReader_t1_457 * __this, Stream_t1_405 * ___stream, Encoding_t1_406 * ___encoding, bool ___detect_encoding_from_bytemarks, int32_t ___buffer_size, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.UnexceptionalStreamReader::.ctor(System.String)
extern "C" void UnexceptionalStreamReader__ctor_m1_5404 (UnexceptionalStreamReader_t1_457 * __this, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.UnexceptionalStreamReader::.ctor(System.String,System.Boolean)
extern "C" void UnexceptionalStreamReader__ctor_m1_5405 (UnexceptionalStreamReader_t1_457 * __this, String_t* ___path, bool ___detect_encoding_from_bytemarks, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.UnexceptionalStreamReader::.ctor(System.String,System.Text.Encoding)
extern "C" void UnexceptionalStreamReader__ctor_m1_5406 (UnexceptionalStreamReader_t1_457 * __this, String_t* ___path, Encoding_t1_406 * ___encoding, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.UnexceptionalStreamReader::.ctor(System.String,System.Text.Encoding,System.Boolean)
extern "C" void UnexceptionalStreamReader__ctor_m1_5407 (UnexceptionalStreamReader_t1_457 * __this, String_t* ___path, Encoding_t1_406 * ___encoding, bool ___detect_encoding_from_bytemarks, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.UnexceptionalStreamReader::.ctor(System.String,System.Text.Encoding,System.Boolean,System.Int32)
extern "C" void UnexceptionalStreamReader__ctor_m1_5408 (UnexceptionalStreamReader_t1_457 * __this, String_t* ___path, Encoding_t1_406 * ___encoding, bool ___detect_encoding_from_bytemarks, int32_t ___buffer_size, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.UnexceptionalStreamReader::.cctor()
extern "C" void UnexceptionalStreamReader__cctor_m1_5409 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.IO.UnexceptionalStreamReader::Peek()
extern "C" int32_t UnexceptionalStreamReader_Peek_m1_5410 (UnexceptionalStreamReader_t1_457 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.IO.UnexceptionalStreamReader::Read()
extern "C" int32_t UnexceptionalStreamReader_Read_m1_5411 (UnexceptionalStreamReader_t1_457 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.IO.UnexceptionalStreamReader::Read(System.Char[],System.Int32,System.Int32)
extern "C" int32_t UnexceptionalStreamReader_Read_m1_5412 (UnexceptionalStreamReader_t1_457 * __this, CharU5BU5D_t1_16* ___dest_buffer, int32_t ___index, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IO.UnexceptionalStreamReader::CheckEOL(System.Char)
extern "C" bool UnexceptionalStreamReader_CheckEOL_m1_5413 (UnexceptionalStreamReader_t1_457 * __this, uint16_t ___current, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.IO.UnexceptionalStreamReader::ReadLine()
extern "C" String_t* UnexceptionalStreamReader_ReadLine_m1_5414 (UnexceptionalStreamReader_t1_457 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.IO.UnexceptionalStreamReader::ReadToEnd()
extern "C" String_t* UnexceptionalStreamReader_ReadToEnd_m1_5415 (UnexceptionalStreamReader_t1_457 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
