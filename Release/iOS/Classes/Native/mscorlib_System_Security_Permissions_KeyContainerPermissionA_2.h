﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Security_Permissions_CodeAccessSecurityAttri.h"
#include "mscorlib_System_Security_Permissions_KeyContainerPermissionF.h"

// System.Security.Permissions.KeyContainerPermissionAttribute
struct  KeyContainerPermissionAttribute_t1_1292  : public CodeAccessSecurityAttribute_t1_1266
{
	// System.Security.Permissions.KeyContainerPermissionFlags System.Security.Permissions.KeyContainerPermissionAttribute::_flags
	int32_t ____flags_2;
	// System.String System.Security.Permissions.KeyContainerPermissionAttribute::_containerName
	String_t* ____containerName_3;
	// System.Int32 System.Security.Permissions.KeyContainerPermissionAttribute::_spec
	int32_t ____spec_4;
	// System.String System.Security.Permissions.KeyContainerPermissionAttribute::_store
	String_t* ____store_5;
	// System.String System.Security.Permissions.KeyContainerPermissionAttribute::_providerName
	String_t* ____providerName_6;
	// System.Int32 System.Security.Permissions.KeyContainerPermissionAttribute::_type
	int32_t ____type_7;
};
