﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Permissions.SecurityPermissionAttribute
struct SecurityPermissionAttribute_t1_1309;
// System.Security.IPermission
struct IPermission_t1_1400;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Permissions_SecurityAction.h"
#include "mscorlib_System_Security_Permissions_SecurityPermissionFlag.h"

// System.Void System.Security.Permissions.SecurityPermissionAttribute::.ctor(System.Security.Permissions.SecurityAction)
extern "C" void SecurityPermissionAttribute__ctor_m1_11160 (SecurityPermissionAttribute_t1_1309 * __this, int32_t ___action, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.SecurityPermissionAttribute::get_Assertion()
extern "C" bool SecurityPermissionAttribute_get_Assertion_m1_11161 (SecurityPermissionAttribute_t1_1309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.SecurityPermissionAttribute::set_Assertion(System.Boolean)
extern "C" void SecurityPermissionAttribute_set_Assertion_m1_11162 (SecurityPermissionAttribute_t1_1309 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.SecurityPermissionAttribute::get_BindingRedirects()
extern "C" bool SecurityPermissionAttribute_get_BindingRedirects_m1_11163 (SecurityPermissionAttribute_t1_1309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.SecurityPermissionAttribute::set_BindingRedirects(System.Boolean)
extern "C" void SecurityPermissionAttribute_set_BindingRedirects_m1_11164 (SecurityPermissionAttribute_t1_1309 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.SecurityPermissionAttribute::get_ControlAppDomain()
extern "C" bool SecurityPermissionAttribute_get_ControlAppDomain_m1_11165 (SecurityPermissionAttribute_t1_1309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.SecurityPermissionAttribute::set_ControlAppDomain(System.Boolean)
extern "C" void SecurityPermissionAttribute_set_ControlAppDomain_m1_11166 (SecurityPermissionAttribute_t1_1309 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.SecurityPermissionAttribute::get_ControlDomainPolicy()
extern "C" bool SecurityPermissionAttribute_get_ControlDomainPolicy_m1_11167 (SecurityPermissionAttribute_t1_1309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.SecurityPermissionAttribute::set_ControlDomainPolicy(System.Boolean)
extern "C" void SecurityPermissionAttribute_set_ControlDomainPolicy_m1_11168 (SecurityPermissionAttribute_t1_1309 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.SecurityPermissionAttribute::get_ControlEvidence()
extern "C" bool SecurityPermissionAttribute_get_ControlEvidence_m1_11169 (SecurityPermissionAttribute_t1_1309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.SecurityPermissionAttribute::set_ControlEvidence(System.Boolean)
extern "C" void SecurityPermissionAttribute_set_ControlEvidence_m1_11170 (SecurityPermissionAttribute_t1_1309 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.SecurityPermissionAttribute::get_ControlPolicy()
extern "C" bool SecurityPermissionAttribute_get_ControlPolicy_m1_11171 (SecurityPermissionAttribute_t1_1309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.SecurityPermissionAttribute::set_ControlPolicy(System.Boolean)
extern "C" void SecurityPermissionAttribute_set_ControlPolicy_m1_11172 (SecurityPermissionAttribute_t1_1309 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.SecurityPermissionAttribute::get_ControlPrincipal()
extern "C" bool SecurityPermissionAttribute_get_ControlPrincipal_m1_11173 (SecurityPermissionAttribute_t1_1309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.SecurityPermissionAttribute::set_ControlPrincipal(System.Boolean)
extern "C" void SecurityPermissionAttribute_set_ControlPrincipal_m1_11174 (SecurityPermissionAttribute_t1_1309 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.SecurityPermissionAttribute::get_ControlThread()
extern "C" bool SecurityPermissionAttribute_get_ControlThread_m1_11175 (SecurityPermissionAttribute_t1_1309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.SecurityPermissionAttribute::set_ControlThread(System.Boolean)
extern "C" void SecurityPermissionAttribute_set_ControlThread_m1_11176 (SecurityPermissionAttribute_t1_1309 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.SecurityPermissionAttribute::get_Execution()
extern "C" bool SecurityPermissionAttribute_get_Execution_m1_11177 (SecurityPermissionAttribute_t1_1309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.SecurityPermissionAttribute::set_Execution(System.Boolean)
extern "C" void SecurityPermissionAttribute_set_Execution_m1_11178 (SecurityPermissionAttribute_t1_1309 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.SecurityPermissionAttribute::get_Infrastructure()
extern "C" bool SecurityPermissionAttribute_get_Infrastructure_m1_11179 (SecurityPermissionAttribute_t1_1309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.SecurityPermissionAttribute::set_Infrastructure(System.Boolean)
extern "C" void SecurityPermissionAttribute_set_Infrastructure_m1_11180 (SecurityPermissionAttribute_t1_1309 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.SecurityPermissionAttribute::get_RemotingConfiguration()
extern "C" bool SecurityPermissionAttribute_get_RemotingConfiguration_m1_11181 (SecurityPermissionAttribute_t1_1309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.SecurityPermissionAttribute::set_RemotingConfiguration(System.Boolean)
extern "C" void SecurityPermissionAttribute_set_RemotingConfiguration_m1_11182 (SecurityPermissionAttribute_t1_1309 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.SecurityPermissionAttribute::get_SerializationFormatter()
extern "C" bool SecurityPermissionAttribute_get_SerializationFormatter_m1_11183 (SecurityPermissionAttribute_t1_1309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.SecurityPermissionAttribute::set_SerializationFormatter(System.Boolean)
extern "C" void SecurityPermissionAttribute_set_SerializationFormatter_m1_11184 (SecurityPermissionAttribute_t1_1309 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.SecurityPermissionAttribute::get_SkipVerification()
extern "C" bool SecurityPermissionAttribute_get_SkipVerification_m1_11185 (SecurityPermissionAttribute_t1_1309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.SecurityPermissionAttribute::set_SkipVerification(System.Boolean)
extern "C" void SecurityPermissionAttribute_set_SkipVerification_m1_11186 (SecurityPermissionAttribute_t1_1309 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.SecurityPermissionAttribute::get_UnmanagedCode()
extern "C" bool SecurityPermissionAttribute_get_UnmanagedCode_m1_11187 (SecurityPermissionAttribute_t1_1309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.SecurityPermissionAttribute::set_UnmanagedCode(System.Boolean)
extern "C" void SecurityPermissionAttribute_set_UnmanagedCode_m1_11188 (SecurityPermissionAttribute_t1_1309 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.SecurityPermissionAttribute::CreatePermission()
extern "C" Object_t * SecurityPermissionAttribute_CreatePermission_m1_11189 (SecurityPermissionAttribute_t1_1309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Permissions.SecurityPermissionFlag System.Security.Permissions.SecurityPermissionAttribute::get_Flags()
extern "C" int32_t SecurityPermissionAttribute_get_Flags_m1_11190 (SecurityPermissionAttribute_t1_1309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.SecurityPermissionAttribute::set_Flags(System.Security.Permissions.SecurityPermissionFlag)
extern "C" void SecurityPermissionAttribute_set_Flags_m1_11191 (SecurityPermissionAttribute_t1_1309 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
