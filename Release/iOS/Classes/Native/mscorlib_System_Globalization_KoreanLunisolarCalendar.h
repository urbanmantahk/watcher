﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Globalization.CCEastAsianLunisolarEraHandler
struct CCEastAsianLunisolarEraHandler_t1_355;

#include "mscorlib_System_Globalization_EastAsianLunisolarCalendar.h"
#include "mscorlib_System_DateTime.h"

// System.Globalization.KoreanLunisolarCalendar
struct  KoreanLunisolarCalendar_t1_382  : public EastAsianLunisolarCalendar_t1_358
{
};
struct KoreanLunisolarCalendar_t1_382_StaticFields{
	// System.Globalization.CCEastAsianLunisolarEraHandler System.Globalization.KoreanLunisolarCalendar::era_handler
	CCEastAsianLunisolarEraHandler_t1_355 * ___era_handler_9;
	// System.DateTime System.Globalization.KoreanLunisolarCalendar::KoreanMin
	DateTime_t1_150  ___KoreanMin_10;
	// System.DateTime System.Globalization.KoreanLunisolarCalendar::KoreanMax
	DateTime_t1_150  ___KoreanMax_11;
};
