﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UICharInfo>
struct DefaultComparer_t1_2366;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_UICharInfo.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UICharInfo>::.ctor()
extern "C" void DefaultComparer__ctor_m1_20602_gshared (DefaultComparer_t1_2366 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1_20602(__this, method) (( void (*) (DefaultComparer_t1_2366 *, const MethodInfo*))DefaultComparer__ctor_m1_20602_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UICharInfo>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m1_20603_gshared (DefaultComparer_t1_2366 * __this, UICharInfo_t6_150  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m1_20603(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t1_2366 *, UICharInfo_t6_150 , const MethodInfo*))DefaultComparer_GetHashCode_m1_20603_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UICharInfo>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m1_20604_gshared (DefaultComparer_t1_2366 * __this, UICharInfo_t6_150  ___x, UICharInfo_t6_150  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m1_20604(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t1_2366 *, UICharInfo_t6_150 , UICharInfo_t6_150 , const MethodInfo*))DefaultComparer_Equals_m1_20604_gshared)(__this, ___x, ___y, method)
