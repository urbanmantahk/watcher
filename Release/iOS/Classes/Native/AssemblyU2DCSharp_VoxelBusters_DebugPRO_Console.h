﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>
struct List_1_t1_1908;
// System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleTag>
struct List_1_t1_1909;
// VoxelBusters.DebugPRO.Console
struct Console_t8_169;

#include "UnityEngine_UnityEngine_ScriptableObject.h"
#include "AssemblyU2DCSharp_VoxelBusters_DebugPRO_Internal_eConsoleLog.h"
#include "AssemblyU2DCSharp_VoxelBusters_DebugPRO_Internal_ConsoleLog.h"

// VoxelBusters.DebugPRO.Console
struct  Console_t8_169  : public ScriptableObject_t6_15
{
	// System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog> VoxelBusters.DebugPRO.Console::m_consoleLogsList
	List_1_t1_1908 * ___m_consoleLogsList_4;
	// System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog> VoxelBusters.DebugPRO.Console::m_displayableConsoleLogsList
	List_1_t1_1908 * ___m_displayableConsoleLogsList_5;
	// System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleTag> VoxelBusters.DebugPRO.Console::m_consoleTags
	List_1_t1_1909 * ___m_consoleTags_6;
	// System.Boolean VoxelBusters.DebugPRO.Console::m_clearOnPlay
	bool ___m_clearOnPlay_7;
	// System.Boolean VoxelBusters.DebugPRO.Console::m_errorPause
	bool ___m_errorPause_8;
	// VoxelBusters.DebugPRO.Internal.eConsoleLogType VoxelBusters.DebugPRO.Console::m_allowedLogTypes
	int32_t ___m_allowedLogTypes_9;
	// System.Boolean VoxelBusters.DebugPRO.Console::m_showInfoLogs
	bool ___m_showInfoLogs_10;
	// System.Boolean VoxelBusters.DebugPRO.Console::m_showWarningLogs
	bool ___m_showWarningLogs_11;
	// System.Boolean VoxelBusters.DebugPRO.Console::m_showErrorLogs
	bool ___m_showErrorLogs_12;
	// System.String VoxelBusters.DebugPRO.Console::m_infoLogsCounterStr
	String_t* ___m_infoLogsCounterStr_13;
	// System.Int32 VoxelBusters.DebugPRO.Console::m_infoLogsCounter
	int32_t ___m_infoLogsCounter_14;
	// System.String VoxelBusters.DebugPRO.Console::m_warningLogsCounterStr
	String_t* ___m_warningLogsCounterStr_15;
	// System.Int32 VoxelBusters.DebugPRO.Console::m_warningLogsCounter
	int32_t ___m_warningLogsCounter_16;
	// System.String VoxelBusters.DebugPRO.Console::m_errorLogsCounterStr
	String_t* ___m_errorLogsCounterStr_17;
	// System.Int32 VoxelBusters.DebugPRO.Console::m_errorLogsCounter
	int32_t ___m_errorLogsCounter_18;
	// VoxelBusters.DebugPRO.Internal.ConsoleLog VoxelBusters.DebugPRO.Console::m_selectedConsoleLog
	ConsoleLog_t8_170  ___m_selectedConsoleLog_19;
};
struct Console_t8_169_StaticFields{
	// VoxelBusters.DebugPRO.Console VoxelBusters.DebugPRO.Console::instance
	Console_t8_169 * ___instance_20;
};
