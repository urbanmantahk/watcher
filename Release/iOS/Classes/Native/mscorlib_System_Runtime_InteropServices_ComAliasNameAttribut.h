﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Attribute.h"

// System.Runtime.InteropServices.ComAliasNameAttribute
struct  ComAliasNameAttribute_t1_767  : public Attribute_t1_2
{
	// System.String System.Runtime.InteropServices.ComAliasNameAttribute::val
	String_t* ___val_0;
};
