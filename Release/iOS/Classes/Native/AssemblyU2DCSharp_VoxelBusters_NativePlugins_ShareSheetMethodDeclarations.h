﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.ShareSheet
struct ShareSheet_t8_289;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// VoxelBusters.NativePlugins.eShareOptions[]
struct eShareOptionsU5BU5D_t8_186;
// UnityEngine.Texture2D
struct Texture2D_t6_33;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.ShareSheet::.ctor()
extern "C" void ShareSheet__ctor_m8_1693 (ShareSheet_t8_289 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.ShareSheet::get_Text()
extern "C" String_t* ShareSheet_get_Text_m8_1694 (ShareSheet_t8_289 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.ShareSheet::set_Text(System.String)
extern "C" void ShareSheet_set_Text_m8_1695 (ShareSheet_t8_289 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.ShareSheet::get_URL()
extern "C" String_t* ShareSheet_get_URL_m8_1696 (ShareSheet_t8_289 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.ShareSheet::set_URL(System.String)
extern "C" void ShareSheet_set_URL_m8_1697 (ShareSheet_t8_289 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] VoxelBusters.NativePlugins.ShareSheet::get_ImageData()
extern "C" ByteU5BU5D_t1_109* ShareSheet_get_ImageData_m8_1698 (ShareSheet_t8_289 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.ShareSheet::set_ImageData(System.Byte[])
extern "C" void ShareSheet_set_ImageData_m8_1699 (ShareSheet_t8_289 * __this, ByteU5BU5D_t1_109* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.NativePlugins.eShareOptions[] VoxelBusters.NativePlugins.ShareSheet::get_ExcludedShareOptions()
extern "C" eShareOptionsU5BU5D_t8_186* ShareSheet_get_ExcludedShareOptions_m8_1700 (ShareSheet_t8_289 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.ShareSheet::set_ExcludedShareOptions(VoxelBusters.NativePlugins.eShareOptions[])
extern "C" void ShareSheet_set_ExcludedShareOptions_m8_1701 (ShareSheet_t8_289 * __this, eShareOptionsU5BU5D_t8_186* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.NativePlugins.ShareSheet::get_IsReadyToShowView()
extern "C" bool ShareSheet_get_IsReadyToShowView_m8_1702 (ShareSheet_t8_289 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.ShareSheet::AttachImage(UnityEngine.Texture2D)
extern "C" void ShareSheet_AttachImage_m8_1703 (ShareSheet_t8_289 * __this, Texture2D_t6_33 * ____texture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
