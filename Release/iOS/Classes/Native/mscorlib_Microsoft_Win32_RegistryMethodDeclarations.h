﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Microsoft.Win32.RegistryKey
struct RegistryKey_t1_91;
// System.String
struct String_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_Microsoft_Win32_RegistryValueKind.h"

// System.Void Microsoft.Win32.Registry::.cctor()
extern "C" void Registry__cctor_m1_1427 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Microsoft.Win32.RegistryKey Microsoft.Win32.Registry::ToKey(System.String,System.Boolean)
extern "C" RegistryKey_t1_91 * Registry_ToKey_m1_1428 (Object_t * __this /* static, unused */, String_t* ___keyName, bool ___setting, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Microsoft.Win32.Registry::SetValue(System.String,System.String,System.Object)
extern "C" void Registry_SetValue_m1_1429 (Object_t * __this /* static, unused */, String_t* ___keyName, String_t* ___valueName, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Microsoft.Win32.Registry::SetValue(System.String,System.String,System.Object,Microsoft.Win32.RegistryValueKind)
extern "C" void Registry_SetValue_m1_1430 (Object_t * __this /* static, unused */, String_t* ___keyName, String_t* ___valueName, Object_t * ___value, int32_t ___valueKind, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Microsoft.Win32.Registry::GetValue(System.String,System.String,System.Object)
extern "C" Object_t * Registry_GetValue_m1_1431 (Object_t * __this /* static, unused */, String_t* ___keyName, String_t* ___valueName, Object_t * ___defaultValue, const MethodInfo* method) IL2CPP_METHOD_ATTR;
