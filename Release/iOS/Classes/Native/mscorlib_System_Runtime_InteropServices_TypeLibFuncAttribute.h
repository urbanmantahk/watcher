﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Attribute.h"
#include "mscorlib_System_Runtime_InteropServices_TypeLibFuncFlags.h"

// System.Runtime.InteropServices.TypeLibFuncAttribute
struct  TypeLibFuncAttribute_t1_833  : public Attribute_t1_2
{
	// System.Runtime.InteropServices.TypeLibFuncFlags System.Runtime.InteropServices.TypeLibFuncAttribute::flags
	int32_t ___flags_0;
};
