﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.GenericComparer`1<System.DateTimeOffset>
struct GenericComparer_1_t1_1794;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTimeOffset.h"

// System.Void System.Collections.Generic.GenericComparer`1<System.DateTimeOffset>::.ctor()
extern "C" void GenericComparer_1__ctor_m1_14910_gshared (GenericComparer_1_t1_1794 * __this, const MethodInfo* method);
#define GenericComparer_1__ctor_m1_14910(__this, method) (( void (*) (GenericComparer_1_t1_1794 *, const MethodInfo*))GenericComparer_1__ctor_m1_14910_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericComparer`1<System.DateTimeOffset>::Compare(T,T)
extern "C" int32_t GenericComparer_1_Compare_m1_17584_gshared (GenericComparer_1_t1_1794 * __this, DateTimeOffset_t1_1527  ___x, DateTimeOffset_t1_1527  ___y, const MethodInfo* method);
#define GenericComparer_1_Compare_m1_17584(__this, ___x, ___y, method) (( int32_t (*) (GenericComparer_1_t1_1794 *, DateTimeOffset_t1_1527 , DateTimeOffset_t1_1527 , const MethodInfo*))GenericComparer_1_Compare_m1_17584_gshared)(__this, ___x, ___y, method)
