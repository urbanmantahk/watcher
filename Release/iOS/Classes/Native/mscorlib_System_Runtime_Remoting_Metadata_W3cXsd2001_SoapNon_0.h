﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object.h"
#include "mscorlib_System_Decimal.h"

// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNonPositiveInteger
struct  SoapNonPositiveInteger_t1_988  : public Object_t
{
	// System.Decimal System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNonPositiveInteger::_value
	Decimal_t1_19  ____value_0;
};
