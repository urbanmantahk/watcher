﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.IDictionary
struct IDictionary_t1_35;
// VoxelBusters.NativePlugins.CrossPlatformNotification/iOSSpecificProperties
struct iOSSpecificProperties_t8_266;
// VoxelBusters.NativePlugins.CrossPlatformNotification/AndroidSpecificProperties
struct AndroidSpecificProperties_t8_265;

#include "mscorlib_System_Object.h"
#include "mscorlib_System_DateTime.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eNotificationRe.h"

// VoxelBusters.NativePlugins.CrossPlatformNotification
struct  CrossPlatformNotification_t8_259  : public Object_t
{
	// System.String VoxelBusters.NativePlugins.CrossPlatformNotification::<AlertBody>k__BackingField
	String_t* ___U3CAlertBodyU3Ek__BackingField_8;
	// System.DateTime VoxelBusters.NativePlugins.CrossPlatformNotification::<FireDate>k__BackingField
	DateTime_t1_150  ___U3CFireDateU3Ek__BackingField_9;
	// VoxelBusters.NativePlugins.eNotificationRepeatInterval VoxelBusters.NativePlugins.CrossPlatformNotification::<RepeatInterval>k__BackingField
	int32_t ___U3CRepeatIntervalU3Ek__BackingField_10;
	// System.Collections.IDictionary VoxelBusters.NativePlugins.CrossPlatformNotification::<UserInfo>k__BackingField
	Object_t * ___U3CUserInfoU3Ek__BackingField_11;
	// System.String VoxelBusters.NativePlugins.CrossPlatformNotification::<SoundName>k__BackingField
	String_t* ___U3CSoundNameU3Ek__BackingField_12;
	// VoxelBusters.NativePlugins.CrossPlatformNotification/iOSSpecificProperties VoxelBusters.NativePlugins.CrossPlatformNotification::<iOSProperties>k__BackingField
	iOSSpecificProperties_t8_266 * ___U3CiOSPropertiesU3Ek__BackingField_13;
	// VoxelBusters.NativePlugins.CrossPlatformNotification/AndroidSpecificProperties VoxelBusters.NativePlugins.CrossPlatformNotification::<AndroidProperties>k__BackingField
	AndroidSpecificProperties_t8_265 * ___U3CAndroidPropertiesU3Ek__BackingField_14;
};
