﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedList`2/ListValues/<IEnumerable.GetEnumerator>c__Iterator3<System.Int32,ExifLibrary.IFD>
struct GetEnumeratorU3Ec__Iterator3_t3_283;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Generic.SortedList`2/ListValues/<IEnumerable.GetEnumerator>c__Iterator3<System.Int32,ExifLibrary.IFD>::.ctor()
extern "C" void GetEnumeratorU3Ec__Iterator3__ctor_m3_2215_gshared (GetEnumeratorU3Ec__Iterator3_t3_283 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator3__ctor_m3_2215(__this, method) (( void (*) (GetEnumeratorU3Ec__Iterator3_t3_283 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator3__ctor_m3_2215_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2/ListValues/<IEnumerable.GetEnumerator>c__Iterator3<System.Int32,ExifLibrary.IFD>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * GetEnumeratorU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3_2216_gshared (GetEnumeratorU3Ec__Iterator3_t3_283 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3_2216(__this, method) (( Object_t * (*) (GetEnumeratorU3Ec__Iterator3_t3_283 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3_2216_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2/ListValues/<IEnumerable.GetEnumerator>c__Iterator3<System.Int32,ExifLibrary.IFD>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * GetEnumeratorU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m3_2217_gshared (GetEnumeratorU3Ec__Iterator3_t3_283 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m3_2217(__this, method) (( Object_t * (*) (GetEnumeratorU3Ec__Iterator3_t3_283 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m3_2217_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedList`2/ListValues/<IEnumerable.GetEnumerator>c__Iterator3<System.Int32,ExifLibrary.IFD>::MoveNext()
extern "C" bool GetEnumeratorU3Ec__Iterator3_MoveNext_m3_2218_gshared (GetEnumeratorU3Ec__Iterator3_t3_283 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator3_MoveNext_m3_2218(__this, method) (( bool (*) (GetEnumeratorU3Ec__Iterator3_t3_283 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator3_MoveNext_m3_2218_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/ListValues/<IEnumerable.GetEnumerator>c__Iterator3<System.Int32,ExifLibrary.IFD>::Dispose()
extern "C" void GetEnumeratorU3Ec__Iterator3_Dispose_m3_2219_gshared (GetEnumeratorU3Ec__Iterator3_t3_283 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator3_Dispose_m3_2219(__this, method) (( void (*) (GetEnumeratorU3Ec__Iterator3_t3_283 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator3_Dispose_m3_2219_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/ListValues/<IEnumerable.GetEnumerator>c__Iterator3<System.Int32,ExifLibrary.IFD>::Reset()
extern "C" void GetEnumeratorU3Ec__Iterator3_Reset_m3_2220_gshared (GetEnumeratorU3Ec__Iterator3_t3_283 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator3_Reset_m3_2220(__this, method) (( void (*) (GetEnumeratorU3Ec__Iterator3_t3_283 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator3_Reset_m3_2220_gshared)(__this, method)
