﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_Emit_StringToken.h"

// System.Void System.Reflection.Emit.StringToken::.ctor(System.Int32)
extern "C" void StringToken__ctor_m1_6397 (StringToken_t1_554 * __this, int32_t ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.StringToken::.cctor()
extern "C" void StringToken__cctor_m1_6398 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.StringToken::Equals(System.Object)
extern "C" bool StringToken_Equals_m1_6399 (StringToken_t1_554 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.StringToken::Equals(System.Reflection.Emit.StringToken)
extern "C" bool StringToken_Equals_m1_6400 (StringToken_t1_554 * __this, StringToken_t1_554  ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.StringToken::GetHashCode()
extern "C" int32_t StringToken_GetHashCode_m1_6401 (StringToken_t1_554 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.StringToken::get_Token()
extern "C" int32_t StringToken_get_Token_m1_6402 (StringToken_t1_554 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.StringToken::op_Equality(System.Reflection.Emit.StringToken,System.Reflection.Emit.StringToken)
extern "C" bool StringToken_op_Equality_m1_6403 (Object_t * __this /* static, unused */, StringToken_t1_554  ___a, StringToken_t1_554  ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.StringToken::op_Inequality(System.Reflection.Emit.StringToken,System.Reflection.Emit.StringToken)
extern "C" bool StringToken_op_Inequality_m1_6404 (Object_t * __this /* static, unused */, StringToken_t1_554  ___a, StringToken_t1_554  ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
