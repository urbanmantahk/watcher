﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>
struct Dictionary_2_t1_2422;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t1_1980;
// System.Collections.Generic.IDictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>
struct IDictionary_2_t1_2834;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t1_2012;
// System.Collections.Generic.ICollection`1<UnityEngine.TextEditor/TextEditOp>
struct ICollection_1_t1_2835;
// System.Collections.ICollection
struct ICollection_t1_280;
// System.Object
struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>[]
struct KeyValuePair_2U5BU5D_t1_2833;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>
struct IEnumerator_1_t1_2836;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1_863;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.TextEditor/TextEditOp>
struct KeyCollection_t1_2427;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,UnityEngine.TextEditor/TextEditOp>
struct ValueCollection_t1_2431;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_12.h"
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__10.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor()
extern "C" void Dictionary_2__ctor_m1_21469_gshared (Dictionary_2_t1_2422 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m1_21469(__this, method) (( void (*) (Dictionary_2_t1_2422 *, const MethodInfo*))Dictionary_2__ctor_m1_21469_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m1_21471_gshared (Dictionary_2_t1_2422 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m1_21471(__this, ___comparer, method) (( void (*) (Dictionary_2_t1_2422 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m1_21471_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C" void Dictionary_2__ctor_m1_21473_gshared (Dictionary_2_t1_2422 * __this, Object_t* ___dictionary, const MethodInfo* method);
#define Dictionary_2__ctor_m1_21473(__this, ___dictionary, method) (( void (*) (Dictionary_2_t1_2422 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m1_21473_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m1_21475_gshared (Dictionary_2_t1_2422 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m1_21475(__this, ___capacity, method) (( void (*) (Dictionary_2_t1_2422 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m1_21475_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m1_21477_gshared (Dictionary_2_t1_2422 * __this, Object_t* ___dictionary, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m1_21477(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t1_2422 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m1_21477_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m1_21479_gshared (Dictionary_2_t1_2422 * __this, int32_t ___capacity, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m1_21479(__this, ___capacity, ___comparer, method) (( void (*) (Dictionary_2_t1_2422 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2__ctor_m1_21479_gshared)(__this, ___capacity, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m1_21481_gshared (Dictionary_2_t1_2422 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m1_21481(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1_2422 *, SerializationInfo_t1_293 *, StreamingContext_t1_1050 , const MethodInfo*))Dictionary_2__ctor_m1_21481_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1_21483_gshared (Dictionary_2_t1_2422 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1_21483(__this, method) (( Object_t* (*) (Dictionary_2_t1_2422 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1_21483_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1_21485_gshared (Dictionary_2_t1_2422 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1_21485(__this, method) (( Object_t* (*) (Dictionary_2_t1_2422 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1_21485_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.get_Keys()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Keys_m1_21487_gshared (Dictionary_2_t1_2422 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m1_21487(__this, method) (( Object_t * (*) (Dictionary_2_t1_2422 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m1_21487_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.get_Values()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Values_m1_21489_gshared (Dictionary_2_t1_2422 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m1_21489(__this, method) (( Object_t * (*) (Dictionary_2_t1_2422 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m1_21489_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.get_IsFixedSize()
extern "C" bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1_21491_gshared (Dictionary_2_t1_2422 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1_21491(__this, method) (( bool (*) (Dictionary_2_t1_2422 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1_21491_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1_21493_gshared (Dictionary_2_t1_2422 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1_21493(__this, method) (( bool (*) (Dictionary_2_t1_2422 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1_21493_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m1_21495_gshared (Dictionary_2_t1_2422 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m1_21495(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t1_2422 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m1_21495_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m1_21497_gshared (Dictionary_2_t1_2422 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m1_21497(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1_2422 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m1_21497_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m1_21499_gshared (Dictionary_2_t1_2422 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m1_21499(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1_2422 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m1_21499_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m1_21501_gshared (Dictionary_2_t1_2422 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m1_21501(__this, ___key, method) (( bool (*) (Dictionary_2_t1_2422 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m1_21501_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m1_21503_gshared (Dictionary_2_t1_2422 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m1_21503(__this, ___key, method) (( void (*) (Dictionary_2_t1_2422 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m1_21503_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1_21505_gshared (Dictionary_2_t1_2422 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1_21505(__this, method) (( bool (*) (Dictionary_2_t1_2422 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1_21505_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1_21507_gshared (Dictionary_2_t1_2422 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1_21507(__this, method) (( Object_t * (*) (Dictionary_2_t1_2422 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1_21507_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1_21509_gshared (Dictionary_2_t1_2422 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1_21509(__this, method) (( bool (*) (Dictionary_2_t1_2422 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1_21509_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1_21511_gshared (Dictionary_2_t1_2422 * __this, KeyValuePair_2_t1_2424  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1_21511(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t1_2422 *, KeyValuePair_2_t1_2424 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1_21511_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1_21513_gshared (Dictionary_2_t1_2422 * __this, KeyValuePair_2_t1_2424  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1_21513(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1_2422 *, KeyValuePair_2_t1_2424 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1_21513_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1_21515_gshared (Dictionary_2_t1_2422 * __this, KeyValuePair_2U5BU5D_t1_2833* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1_21515(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1_2422 *, KeyValuePair_2U5BU5D_t1_2833*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1_21515_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1_21517_gshared (Dictionary_2_t1_2422 * __this, KeyValuePair_2_t1_2424  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1_21517(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1_2422 *, KeyValuePair_2_t1_2424 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1_21517_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m1_21519_gshared (Dictionary_2_t1_2422 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m1_21519(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1_2422 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m1_21519_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1_21521_gshared (Dictionary_2_t1_2422 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1_21521(__this, method) (( Object_t * (*) (Dictionary_2_t1_2422 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1_21521_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1_21523_gshared (Dictionary_2_t1_2422 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1_21523(__this, method) (( Object_t* (*) (Dictionary_2_t1_2422 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1_21523_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1_21525_gshared (Dictionary_2_t1_2422 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1_21525(__this, method) (( Object_t * (*) (Dictionary_2_t1_2422 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1_21525_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m1_21527_gshared (Dictionary_2_t1_2422 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m1_21527(__this, method) (( int32_t (*) (Dictionary_2_t1_2422 *, const MethodInfo*))Dictionary_2_get_Count_m1_21527_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Item(TKey)
extern "C" int32_t Dictionary_2_get_Item_m1_21529_gshared (Dictionary_2_t1_2422 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m1_21529(__this, ___key, method) (( int32_t (*) (Dictionary_2_t1_2422 *, Object_t *, const MethodInfo*))Dictionary_2_get_Item_m1_21529_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m1_21531_gshared (Dictionary_2_t1_2422 * __this, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m1_21531(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1_2422 *, Object_t *, int32_t, const MethodInfo*))Dictionary_2_set_Item_m1_21531_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m1_21533_gshared (Dictionary_2_t1_2422 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m1_21533(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t1_2422 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m1_21533_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m1_21535_gshared (Dictionary_2_t1_2422 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m1_21535(__this, ___size, method) (( void (*) (Dictionary_2_t1_2422 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m1_21535_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m1_21537_gshared (Dictionary_2_t1_2422 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m1_21537(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1_2422 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m1_21537_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t1_2424  Dictionary_2_make_pair_m1_21539_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m1_21539(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t1_2424  (*) (Object_t * /* static, unused */, Object_t *, int32_t, const MethodInfo*))Dictionary_2_make_pair_m1_21539_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::pick_key(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_key_m1_21541_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m1_21541(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, int32_t, const MethodInfo*))Dictionary_2_pick_key_m1_21541_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::pick_value(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_value_m1_21543_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m1_21543(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, Object_t *, int32_t, const MethodInfo*))Dictionary_2_pick_value_m1_21543_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m1_21545_gshared (Dictionary_2_t1_2422 * __this, KeyValuePair_2U5BU5D_t1_2833* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m1_21545(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1_2422 *, KeyValuePair_2U5BU5D_t1_2833*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m1_21545_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::Resize()
extern "C" void Dictionary_2_Resize_m1_21547_gshared (Dictionary_2_t1_2422 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m1_21547(__this, method) (( void (*) (Dictionary_2_t1_2422 *, const MethodInfo*))Dictionary_2_Resize_m1_21547_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m1_21549_gshared (Dictionary_2_t1_2422 * __this, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_Add_m1_21549(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1_2422 *, Object_t *, int32_t, const MethodInfo*))Dictionary_2_Add_m1_21549_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Comparer()
extern "C" Object_t* Dictionary_2_get_Comparer_m1_21551_gshared (Dictionary_2_t1_2422 * __this, const MethodInfo* method);
#define Dictionary_2_get_Comparer_m1_21551(__this, method) (( Object_t* (*) (Dictionary_2_t1_2422 *, const MethodInfo*))Dictionary_2_get_Comparer_m1_21551_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::Clear()
extern "C" void Dictionary_2_Clear_m1_21553_gshared (Dictionary_2_t1_2422 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m1_21553(__this, method) (( void (*) (Dictionary_2_t1_2422 *, const MethodInfo*))Dictionary_2_Clear_m1_21553_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m1_21555_gshared (Dictionary_2_t1_2422 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m1_21555(__this, ___key, method) (( bool (*) (Dictionary_2_t1_2422 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsKey_m1_21555_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m1_21557_gshared (Dictionary_2_t1_2422 * __this, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m1_21557(__this, ___value, method) (( bool (*) (Dictionary_2_t1_2422 *, int32_t, const MethodInfo*))Dictionary_2_ContainsValue_m1_21557_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m1_21559_gshared (Dictionary_2_t1_2422 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m1_21559(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1_2422 *, SerializationInfo_t1_293 *, StreamingContext_t1_1050 , const MethodInfo*))Dictionary_2_GetObjectData_m1_21559_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m1_21561_gshared (Dictionary_2_t1_2422 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m1_21561(__this, ___sender, method) (( void (*) (Dictionary_2_t1_2422 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m1_21561_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m1_21563_gshared (Dictionary_2_t1_2422 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m1_21563(__this, ___key, method) (( bool (*) (Dictionary_2_t1_2422 *, Object_t *, const MethodInfo*))Dictionary_2_Remove_m1_21563_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m1_21565_gshared (Dictionary_2_t1_2422 * __this, Object_t * ___key, int32_t* ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m1_21565(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t1_2422 *, Object_t *, int32_t*, const MethodInfo*))Dictionary_2_TryGetValue_m1_21565_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Keys()
extern "C" KeyCollection_t1_2427 * Dictionary_2_get_Keys_m1_21567_gshared (Dictionary_2_t1_2422 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m1_21567(__this, method) (( KeyCollection_t1_2427 * (*) (Dictionary_2_t1_2422 *, const MethodInfo*))Dictionary_2_get_Keys_m1_21567_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Values()
extern "C" ValueCollection_t1_2431 * Dictionary_2_get_Values_m1_21569_gshared (Dictionary_2_t1_2422 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m1_21569(__this, method) (( ValueCollection_t1_2431 * (*) (Dictionary_2_t1_2422 *, const MethodInfo*))Dictionary_2_get_Values_m1_21569_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::ToTKey(System.Object)
extern "C" Object_t * Dictionary_2_ToTKey_m1_21571_gshared (Dictionary_2_t1_2422 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m1_21571(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t1_2422 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m1_21571_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::ToTValue(System.Object)
extern "C" int32_t Dictionary_2_ToTValue_m1_21573_gshared (Dictionary_2_t1_2422 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m1_21573(__this, ___value, method) (( int32_t (*) (Dictionary_2_t1_2422 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m1_21573_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m1_21575_gshared (Dictionary_2_t1_2422 * __this, KeyValuePair_2_t1_2424  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m1_21575(__this, ___pair, method) (( bool (*) (Dictionary_2_t1_2422 *, KeyValuePair_2_t1_2424 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m1_21575_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::GetEnumerator()
extern "C" Enumerator_t1_2429  Dictionary_2_GetEnumerator_m1_21577_gshared (Dictionary_2_t1_2422 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m1_21577(__this, method) (( Enumerator_t1_2429  (*) (Dictionary_2_t1_2422 *, const MethodInfo*))Dictionary_2_GetEnumerator_m1_21577_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t1_284  Dictionary_2_U3CCopyToU3Em__0_m1_21579_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m1_21579(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t1_284  (*) (Object_t * /* static, unused */, Object_t *, int32_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m1_21579_gshared)(__this /* static, unused */, ___key, ___value, method)
