﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// VoxelBusters.NativePlugins.WebView
struct WebView_t8_191;

#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Demo_NPDisabled.h"

// VoxelBusters.NativePlugins.Demo.WebViewDemo
struct  WebViewDemo_t8_190  : public NPDisabledFeatureDemo_t8_175
{
	// System.String VoxelBusters.NativePlugins.Demo.WebViewDemo::m_url
	String_t* ___m_url_10;
	// System.String VoxelBusters.NativePlugins.Demo.WebViewDemo::m_HTMLString
	String_t* ___m_HTMLString_11;
	// System.String VoxelBusters.NativePlugins.Demo.WebViewDemo::m_javaScript
	String_t* ___m_javaScript_12;
	// System.String VoxelBusters.NativePlugins.Demo.WebViewDemo::m_evalString
	String_t* ___m_evalString_13;
	// System.String VoxelBusters.NativePlugins.Demo.WebViewDemo::m_URLSchemeName
	String_t* ___m_URLSchemeName_14;
	// VoxelBusters.NativePlugins.WebView VoxelBusters.NativePlugins.Demo.WebViewDemo::m_webview
	WebView_t8_191 * ___m_webview_15;
};
