﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.Internal.GameServicesIDHandler
struct GameServicesIDHandler_t8_322;
// VoxelBusters.NativePlugins.IDContainer[]
struct IDContainerU5BU5D_t8_239;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.Internal.GameServicesIDHandler::.ctor()
extern "C" void GameServicesIDHandler__ctor_m8_1889 (GameServicesIDHandler_t8_322 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Internal.GameServicesIDHandler::.cctor()
extern "C" void GameServicesIDHandler__cctor_m8_1890 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Internal.GameServicesIDHandler::SetLeaderboardIDCollection(VoxelBusters.NativePlugins.IDContainer[])
extern "C" void GameServicesIDHandler_SetLeaderboardIDCollection_m8_1891 (Object_t * __this /* static, unused */, IDContainerU5BU5D_t8_239* ____newCollection, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.Internal.GameServicesIDHandler::GetLeaderboardID(System.String)
extern "C" String_t* GameServicesIDHandler_GetLeaderboardID_m8_1892 (Object_t * __this /* static, unused */, String_t* ____globalID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.Internal.GameServicesIDHandler::GetLeaderboardGID(System.String)
extern "C" String_t* GameServicesIDHandler_GetLeaderboardGID_m8_1893 (Object_t * __this /* static, unused */, String_t* ____platformID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Internal.GameServicesIDHandler::SetAchievementIDCollection(VoxelBusters.NativePlugins.IDContainer[])
extern "C" void GameServicesIDHandler_SetAchievementIDCollection_m8_1894 (Object_t * __this /* static, unused */, IDContainerU5BU5D_t8_239* ____newCollection, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.Internal.GameServicesIDHandler::GetAchievementID(System.String)
extern "C" String_t* GameServicesIDHandler_GetAchievementID_m8_1895 (Object_t * __this /* static, unused */, String_t* ____globalID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.Internal.GameServicesIDHandler::GetAchievementGID(System.String)
extern "C" String_t* GameServicesIDHandler_GetAchievementGID_m8_1896 (Object_t * __this /* static, unused */, String_t* ____platformID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.Internal.GameServicesIDHandler::FindCurrentPlatformIDFromCollection(VoxelBusters.NativePlugins.IDContainer[],System.String)
extern "C" String_t* GameServicesIDHandler_FindCurrentPlatformIDFromCollection_m8_1897 (Object_t * __this /* static, unused */, IDContainerU5BU5D_t8_239* ____collection, String_t* ____globalID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.Internal.GameServicesIDHandler::FindGlobalIDFromCollection(VoxelBusters.NativePlugins.IDContainer[],System.String)
extern "C" String_t* GameServicesIDHandler_FindGlobalIDFromCollection_m8_1898 (Object_t * __this /* static, unused */, IDContainerU5BU5D_t8_239* ____collection, String_t* ____platformID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
