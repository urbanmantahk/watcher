﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.X509.X520/Oid
struct Oid_t1_211;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Security.X509.X520/Oid::.ctor(System.String)
extern "C" void Oid__ctor_m1_2411 (Oid_t1_211 * __this, String_t* ___oid, const MethodInfo* method) IL2CPP_METHOD_ATTR;
