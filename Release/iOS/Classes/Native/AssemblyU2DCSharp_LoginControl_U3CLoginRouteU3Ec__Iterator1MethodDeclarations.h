﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LoginControl/<LoginRoute>c__Iterator1
struct U3CLoginRouteU3Ec__Iterator1_t8_10;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void LoginControl/<LoginRoute>c__Iterator1::.ctor()
extern "C" void U3CLoginRouteU3Ec__Iterator1__ctor_m8_86 (U3CLoginRouteU3Ec__Iterator1_t8_10 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LoginControl/<LoginRoute>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CLoginRouteU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m8_87 (U3CLoginRouteU3Ec__Iterator1_t8_10 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LoginControl/<LoginRoute>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CLoginRouteU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m8_88 (U3CLoginRouteU3Ec__Iterator1_t8_10 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LoginControl/<LoginRoute>c__Iterator1::MoveNext()
extern "C" bool U3CLoginRouteU3Ec__Iterator1_MoveNext_m8_89 (U3CLoginRouteU3Ec__Iterator1_t8_10 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginControl/<LoginRoute>c__Iterator1::Dispose()
extern "C" void U3CLoginRouteU3Ec__Iterator1_Dispose_m8_90 (U3CLoginRouteU3Ec__Iterator1_t8_10 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginControl/<LoginRoute>c__Iterator1::Reset()
extern "C" void U3CLoginRouteU3Ec__Iterator1_Reset_m8_91 (U3CLoginRouteU3Ec__Iterator1_t8_10 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
