﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Resources.ResourceWriter
struct ResourceWriter_t1_658;
// System.IO.Stream
struct Stream_t1_405;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Object
struct Object_t;
// System.IO.BinaryWriter
struct BinaryWriter_t1_408;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Resources.ResourceWriter::.ctor(System.IO.Stream)
extern "C" void ResourceWriter__ctor_m1_7443 (ResourceWriter_t1_658 * __this, Stream_t1_405 * ___stream, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.ResourceWriter::.ctor(System.String)
extern "C" void ResourceWriter__ctor_m1_7444 (ResourceWriter_t1_658 * __this, String_t* ___fileName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.ResourceWriter::AddResource(System.String,System.Byte[])
extern "C" void ResourceWriter_AddResource_m1_7445 (ResourceWriter_t1_658 * __this, String_t* ___name, ByteU5BU5D_t1_109* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.ResourceWriter::AddResource(System.String,System.Object)
extern "C" void ResourceWriter_AddResource_m1_7446 (ResourceWriter_t1_658 * __this, String_t* ___name, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.ResourceWriter::AddResource(System.String,System.String)
extern "C" void ResourceWriter_AddResource_m1_7447 (ResourceWriter_t1_658 * __this, String_t* ___name, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.ResourceWriter::Close()
extern "C" void ResourceWriter_Close_m1_7448 (ResourceWriter_t1_658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.ResourceWriter::Dispose()
extern "C" void ResourceWriter_Dispose_m1_7449 (ResourceWriter_t1_658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.ResourceWriter::Dispose(System.Boolean)
extern "C" void ResourceWriter_Dispose_m1_7450 (ResourceWriter_t1_658 * __this, bool ___disposing, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.ResourceWriter::AddResourceData(System.String,System.String,System.Byte[])
extern "C" void ResourceWriter_AddResourceData_m1_7451 (ResourceWriter_t1_658 * __this, String_t* ___name, String_t* ___typeName, ByteU5BU5D_t1_109* ___serializedData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.ResourceWriter::Generate()
extern "C" void ResourceWriter_Generate_m1_7452 (ResourceWriter_t1_658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Resources.ResourceWriter::GetHash(System.String)
extern "C" int32_t ResourceWriter_GetHash_m1_7453 (ResourceWriter_t1_658 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.ResourceWriter::Write7BitEncodedInt(System.IO.BinaryWriter,System.Int32)
extern "C" void ResourceWriter_Write7BitEncodedInt_m1_7454 (ResourceWriter_t1_658 * __this, BinaryWriter_t1_408 * ___writer, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream System.Resources.ResourceWriter::get_Stream()
extern "C" Stream_t1_405 * ResourceWriter_get_Stream_m1_7455 (ResourceWriter_t1_658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
