﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "System_Xml_Mono_Xml_Schema_XsdDecimal.h"

// Mono.Xml.Schema.XsdInteger
struct  XsdInteger_t4_22  : public XsdDecimal_t4_21
{
};
