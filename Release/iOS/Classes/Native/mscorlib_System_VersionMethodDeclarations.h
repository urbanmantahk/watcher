﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Version
struct Version_t1_578;
// System.String
struct String_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Version::.ctor()
extern "C" void Version__ctor_m1_14722 (Version_t1_578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Version::.ctor(System.String)
extern "C" void Version__ctor_m1_14723 (Version_t1_578 * __this, String_t* ___version, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Version::.ctor(System.Int32,System.Int32)
extern "C" void Version__ctor_m1_14724 (Version_t1_578 * __this, int32_t ___major, int32_t ___minor, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Version::.ctor(System.Int32,System.Int32,System.Int32)
extern "C" void Version__ctor_m1_14725 (Version_t1_578 * __this, int32_t ___major, int32_t ___minor, int32_t ___build, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Version::.ctor(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" void Version__ctor_m1_14726 (Version_t1_578 * __this, int32_t ___major, int32_t ___minor, int32_t ___build, int32_t ___revision, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Version::CheckedSet(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" void Version_CheckedSet_m1_14727 (Version_t1_578 * __this, int32_t ___defined, int32_t ___major, int32_t ___minor, int32_t ___build, int32_t ___revision, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Version::get_Build()
extern "C" int32_t Version_get_Build_m1_14728 (Version_t1_578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Version::get_Major()
extern "C" int32_t Version_get_Major_m1_14729 (Version_t1_578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Version::get_Minor()
extern "C" int32_t Version_get_Minor_m1_14730 (Version_t1_578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Version::get_Revision()
extern "C" int32_t Version_get_Revision_m1_14731 (Version_t1_578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 System.Version::get_MajorRevision()
extern "C" int16_t Version_get_MajorRevision_m1_14732 (Version_t1_578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 System.Version::get_MinorRevision()
extern "C" int16_t Version_get_MinorRevision_m1_14733 (Version_t1_578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Version::Clone()
extern "C" Object_t * Version_Clone_m1_14734 (Version_t1_578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Version::CompareTo(System.Object)
extern "C" int32_t Version_CompareTo_m1_14735 (Version_t1_578 * __this, Object_t * ___version, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Version::Equals(System.Object)
extern "C" bool Version_Equals_m1_14736 (Version_t1_578 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Version::CompareTo(System.Version)
extern "C" int32_t Version_CompareTo_m1_14737 (Version_t1_578 * __this, Version_t1_578 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Version::Equals(System.Version)
extern "C" bool Version_Equals_m1_14738 (Version_t1_578 * __this, Version_t1_578 * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Version::GetHashCode()
extern "C" int32_t Version_GetHashCode_m1_14739 (Version_t1_578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Version::ToString()
extern "C" String_t* Version_ToString_m1_14740 (Version_t1_578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Version::ToString(System.Int32)
extern "C" String_t* Version_ToString_m1_14741 (Version_t1_578 * __this, int32_t ___fieldCount, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Version System.Version::CreateFromString(System.String)
extern "C" Version_t1_578 * Version_CreateFromString_m1_14742 (Object_t * __this /* static, unused */, String_t* ___info, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Version::op_Equality(System.Version,System.Version)
extern "C" bool Version_op_Equality_m1_14743 (Object_t * __this /* static, unused */, Version_t1_578 * ___v1, Version_t1_578 * ___v2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Version::op_Inequality(System.Version,System.Version)
extern "C" bool Version_op_Inequality_m1_14744 (Object_t * __this /* static, unused */, Version_t1_578 * ___v1, Version_t1_578 * ___v2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Version::op_GreaterThan(System.Version,System.Version)
extern "C" bool Version_op_GreaterThan_m1_14745 (Object_t * __this /* static, unused */, Version_t1_578 * ___v1, Version_t1_578 * ___v2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Version::op_GreaterThanOrEqual(System.Version,System.Version)
extern "C" bool Version_op_GreaterThanOrEqual_m1_14746 (Object_t * __this /* static, unused */, Version_t1_578 * ___v1, Version_t1_578 * ___v2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Version::op_LessThan(System.Version,System.Version)
extern "C" bool Version_op_LessThan_m1_14747 (Object_t * __this /* static, unused */, Version_t1_578 * ___v1, Version_t1_578 * ___v2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Version::op_LessThanOrEqual(System.Version,System.Version)
extern "C" bool Version_op_LessThanOrEqual_m1_14748 (Object_t * __this /* static, unused */, Version_t1_578 * ___v1, Version_t1_578 * ___v2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
