﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Metadata.SoapTypeAttribute
struct SoapTypeAttribute_t1_1002;
// System.String
struct String_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Remoting_Metadata_SoapOption.h"
#include "mscorlib_System_Runtime_Remoting_Metadata_XmlFieldOrderOptio.h"

// System.Void System.Runtime.Remoting.Metadata.SoapTypeAttribute::.ctor()
extern "C" void SoapTypeAttribute__ctor_m1_8965 (SoapTypeAttribute_t1_1002 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Metadata.SoapOption System.Runtime.Remoting.Metadata.SoapTypeAttribute::get_SoapOptions()
extern "C" int32_t SoapTypeAttribute_get_SoapOptions_m1_8966 (SoapTypeAttribute_t1_1002 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.SoapTypeAttribute::set_SoapOptions(System.Runtime.Remoting.Metadata.SoapOption)
extern "C" void SoapTypeAttribute_set_SoapOptions_m1_8967 (SoapTypeAttribute_t1_1002 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Remoting.Metadata.SoapTypeAttribute::get_UseAttribute()
extern "C" bool SoapTypeAttribute_get_UseAttribute_m1_8968 (SoapTypeAttribute_t1_1002 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.SoapTypeAttribute::set_UseAttribute(System.Boolean)
extern "C" void SoapTypeAttribute_set_UseAttribute_m1_8969 (SoapTypeAttribute_t1_1002 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.SoapTypeAttribute::get_XmlElementName()
extern "C" String_t* SoapTypeAttribute_get_XmlElementName_m1_8970 (SoapTypeAttribute_t1_1002 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.SoapTypeAttribute::set_XmlElementName(System.String)
extern "C" void SoapTypeAttribute_set_XmlElementName_m1_8971 (SoapTypeAttribute_t1_1002 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Metadata.XmlFieldOrderOption System.Runtime.Remoting.Metadata.SoapTypeAttribute::get_XmlFieldOrder()
extern "C" int32_t SoapTypeAttribute_get_XmlFieldOrder_m1_8972 (SoapTypeAttribute_t1_1002 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.SoapTypeAttribute::set_XmlFieldOrder(System.Runtime.Remoting.Metadata.XmlFieldOrderOption)
extern "C" void SoapTypeAttribute_set_XmlFieldOrder_m1_8973 (SoapTypeAttribute_t1_1002 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.SoapTypeAttribute::get_XmlNamespace()
extern "C" String_t* SoapTypeAttribute_get_XmlNamespace_m1_8974 (SoapTypeAttribute_t1_1002 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.SoapTypeAttribute::set_XmlNamespace(System.String)
extern "C" void SoapTypeAttribute_set_XmlNamespace_m1_8975 (SoapTypeAttribute_t1_1002 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.SoapTypeAttribute::get_XmlTypeName()
extern "C" String_t* SoapTypeAttribute_get_XmlTypeName_m1_8976 (SoapTypeAttribute_t1_1002 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.SoapTypeAttribute::set_XmlTypeName(System.String)
extern "C" void SoapTypeAttribute_set_XmlTypeName_m1_8977 (SoapTypeAttribute_t1_1002 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.SoapTypeAttribute::get_XmlTypeNamespace()
extern "C" String_t* SoapTypeAttribute_get_XmlTypeNamespace_m1_8978 (SoapTypeAttribute_t1_1002 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.SoapTypeAttribute::set_XmlTypeNamespace(System.String)
extern "C" void SoapTypeAttribute_set_XmlTypeNamespace_m1_8979 (SoapTypeAttribute_t1_1002 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Remoting.Metadata.SoapTypeAttribute::get_IsInteropXmlElement()
extern "C" bool SoapTypeAttribute_get_IsInteropXmlElement_m1_8980 (SoapTypeAttribute_t1_1002 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Remoting.Metadata.SoapTypeAttribute::get_IsInteropXmlType()
extern "C" bool SoapTypeAttribute_get_IsInteropXmlType_m1_8981 (SoapTypeAttribute_t1_1002 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.SoapTypeAttribute::SetReflectionObject(System.Object)
extern "C" void SoapTypeAttribute_SetReflectionObject_m1_8982 (SoapTypeAttribute_t1_1002 * __this, Object_t * ___reflectionObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
