﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_ConsoleModifiers.h"

// System.ConsoleModifiers
struct  ConsoleModifiers_t1_1517 
{
	// System.Int32 System.ConsoleModifiers::value__
	int32_t ___value___1;
};
