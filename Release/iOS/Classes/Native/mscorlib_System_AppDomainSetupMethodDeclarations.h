﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.AppDomainSetup
struct AppDomainSetup_t1_1497;
// System.Runtime.Hosting.ActivationArguments
struct ActivationArguments_t1_716;
// System.ActivationContext
struct ActivationContext_t1_717;
// System.String
struct String_t;
// System.AppDomainInitializer
struct AppDomainInitializer_t1_1498;
// System.String[]
struct StringU5BU5D_t1_238;
// System.Security.Policy.ApplicationTrust
struct ApplicationTrust_t1_1333;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_LoaderOptimization.h"

// System.Void System.AppDomainSetup::.ctor()
extern "C" void AppDomainSetup__ctor_m1_13191 (AppDomainSetup_t1_1497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomainSetup::.ctor(System.AppDomainSetup)
extern "C" void AppDomainSetup__ctor_m1_13192 (AppDomainSetup_t1_1497 * __this, AppDomainSetup_t1_1497 * ___setup, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomainSetup::.ctor(System.Runtime.Hosting.ActivationArguments)
extern "C" void AppDomainSetup__ctor_m1_13193 (AppDomainSetup_t1_1497 * __this, ActivationArguments_t1_716 * ___activationArguments, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomainSetup::.ctor(System.ActivationContext)
extern "C" void AppDomainSetup__ctor_m1_13194 (AppDomainSetup_t1_1497 * __this, ActivationContext_t1_717 * ___activationContext, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.AppDomainSetup::GetAppBase(System.String)
extern "C" String_t* AppDomainSetup_GetAppBase_m1_13195 (Object_t * __this /* static, unused */, String_t* ___appBase, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.AppDomainSetup::get_ApplicationBase()
extern "C" String_t* AppDomainSetup_get_ApplicationBase_m1_13196 (AppDomainSetup_t1_1497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomainSetup::set_ApplicationBase(System.String)
extern "C" void AppDomainSetup_set_ApplicationBase_m1_13197 (AppDomainSetup_t1_1497 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.AppDomainSetup::get_ApplicationName()
extern "C" String_t* AppDomainSetup_get_ApplicationName_m1_13198 (AppDomainSetup_t1_1497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomainSetup::set_ApplicationName(System.String)
extern "C" void AppDomainSetup_set_ApplicationName_m1_13199 (AppDomainSetup_t1_1497 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.AppDomainSetup::get_CachePath()
extern "C" String_t* AppDomainSetup_get_CachePath_m1_13200 (AppDomainSetup_t1_1497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomainSetup::set_CachePath(System.String)
extern "C" void AppDomainSetup_set_CachePath_m1_13201 (AppDomainSetup_t1_1497 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.AppDomainSetup::get_ConfigurationFile()
extern "C" String_t* AppDomainSetup_get_ConfigurationFile_m1_13202 (AppDomainSetup_t1_1497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomainSetup::set_ConfigurationFile(System.String)
extern "C" void AppDomainSetup_set_ConfigurationFile_m1_13203 (AppDomainSetup_t1_1497 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.AppDomainSetup::get_DisallowPublisherPolicy()
extern "C" bool AppDomainSetup_get_DisallowPublisherPolicy_m1_13204 (AppDomainSetup_t1_1497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomainSetup::set_DisallowPublisherPolicy(System.Boolean)
extern "C" void AppDomainSetup_set_DisallowPublisherPolicy_m1_13205 (AppDomainSetup_t1_1497 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.AppDomainSetup::get_DynamicBase()
extern "C" String_t* AppDomainSetup_get_DynamicBase_m1_13206 (AppDomainSetup_t1_1497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomainSetup::set_DynamicBase(System.String)
extern "C" void AppDomainSetup_set_DynamicBase_m1_13207 (AppDomainSetup_t1_1497 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.AppDomainSetup::get_LicenseFile()
extern "C" String_t* AppDomainSetup_get_LicenseFile_m1_13208 (AppDomainSetup_t1_1497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomainSetup::set_LicenseFile(System.String)
extern "C" void AppDomainSetup_set_LicenseFile_m1_13209 (AppDomainSetup_t1_1497 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.LoaderOptimization System.AppDomainSetup::get_LoaderOptimization()
extern "C" int32_t AppDomainSetup_get_LoaderOptimization_m1_13210 (AppDomainSetup_t1_1497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomainSetup::set_LoaderOptimization(System.LoaderOptimization)
extern "C" void AppDomainSetup_set_LoaderOptimization_m1_13211 (AppDomainSetup_t1_1497 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.AppDomainSetup::get_PrivateBinPath()
extern "C" String_t* AppDomainSetup_get_PrivateBinPath_m1_13212 (AppDomainSetup_t1_1497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomainSetup::set_PrivateBinPath(System.String)
extern "C" void AppDomainSetup_set_PrivateBinPath_m1_13213 (AppDomainSetup_t1_1497 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.AppDomainSetup::get_PrivateBinPathProbe()
extern "C" String_t* AppDomainSetup_get_PrivateBinPathProbe_m1_13214 (AppDomainSetup_t1_1497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomainSetup::set_PrivateBinPathProbe(System.String)
extern "C" void AppDomainSetup_set_PrivateBinPathProbe_m1_13215 (AppDomainSetup_t1_1497 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.AppDomainSetup::get_ShadowCopyDirectories()
extern "C" String_t* AppDomainSetup_get_ShadowCopyDirectories_m1_13216 (AppDomainSetup_t1_1497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomainSetup::set_ShadowCopyDirectories(System.String)
extern "C" void AppDomainSetup_set_ShadowCopyDirectories_m1_13217 (AppDomainSetup_t1_1497 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.AppDomainSetup::get_ShadowCopyFiles()
extern "C" String_t* AppDomainSetup_get_ShadowCopyFiles_m1_13218 (AppDomainSetup_t1_1497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomainSetup::set_ShadowCopyFiles(System.String)
extern "C" void AppDomainSetup_set_ShadowCopyFiles_m1_13219 (AppDomainSetup_t1_1497 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.AppDomainSetup::get_DisallowBindingRedirects()
extern "C" bool AppDomainSetup_get_DisallowBindingRedirects_m1_13220 (AppDomainSetup_t1_1497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomainSetup::set_DisallowBindingRedirects(System.Boolean)
extern "C" void AppDomainSetup_set_DisallowBindingRedirects_m1_13221 (AppDomainSetup_t1_1497 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.AppDomainSetup::get_DisallowCodeDownload()
extern "C" bool AppDomainSetup_get_DisallowCodeDownload_m1_13222 (AppDomainSetup_t1_1497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomainSetup::set_DisallowCodeDownload(System.Boolean)
extern "C" void AppDomainSetup_set_DisallowCodeDownload_m1_13223 (AppDomainSetup_t1_1497 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Hosting.ActivationArguments System.AppDomainSetup::get_ActivationArguments()
extern "C" ActivationArguments_t1_716 * AppDomainSetup_get_ActivationArguments_m1_13224 (AppDomainSetup_t1_1497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomainSetup::set_ActivationArguments(System.Runtime.Hosting.ActivationArguments)
extern "C" void AppDomainSetup_set_ActivationArguments_m1_13225 (AppDomainSetup_t1_1497 * __this, ActivationArguments_t1_716 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.AppDomainInitializer System.AppDomainSetup::get_AppDomainInitializer()
extern "C" AppDomainInitializer_t1_1498 * AppDomainSetup_get_AppDomainInitializer_m1_13226 (AppDomainSetup_t1_1497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomainSetup::set_AppDomainInitializer(System.AppDomainInitializer)
extern "C" void AppDomainSetup_set_AppDomainInitializer_m1_13227 (AppDomainSetup_t1_1497 * __this, AppDomainInitializer_t1_1498 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.AppDomainSetup::get_AppDomainInitializerArguments()
extern "C" StringU5BU5D_t1_238* AppDomainSetup_get_AppDomainInitializerArguments_m1_13228 (AppDomainSetup_t1_1497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomainSetup::set_AppDomainInitializerArguments(System.String[])
extern "C" void AppDomainSetup_set_AppDomainInitializerArguments_m1_13229 (AppDomainSetup_t1_1497 * __this, StringU5BU5D_t1_238* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.ApplicationTrust System.AppDomainSetup::get_ApplicationTrust()
extern "C" ApplicationTrust_t1_1333 * AppDomainSetup_get_ApplicationTrust_m1_13230 (AppDomainSetup_t1_1497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomainSetup::set_ApplicationTrust(System.Security.Policy.ApplicationTrust)
extern "C" void AppDomainSetup_set_ApplicationTrust_m1_13231 (AppDomainSetup_t1_1497 * __this, ApplicationTrust_t1_1333 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.AppDomainSetup::get_DisallowApplicationBaseProbing()
extern "C" bool AppDomainSetup_get_DisallowApplicationBaseProbing_m1_13232 (AppDomainSetup_t1_1497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomainSetup::set_DisallowApplicationBaseProbing(System.Boolean)
extern "C" void AppDomainSetup_set_DisallowApplicationBaseProbing_m1_13233 (AppDomainSetup_t1_1497 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.AppDomainSetup::GetConfigurationBytes()
extern "C" ByteU5BU5D_t1_109* AppDomainSetup_GetConfigurationBytes_m1_13234 (AppDomainSetup_t1_1497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomainSetup::SetConfigurationBytes(System.Byte[])
extern "C" void AppDomainSetup_SetConfigurationBytes_m1_13235 (AppDomainSetup_t1_1497 * __this, ByteU5BU5D_t1_109* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
