﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>
struct ShimEnumerator_t1_2435;
// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>
struct Dictionary_2_t1_2422;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m1_21668_gshared (ShimEnumerator_t1_2435 * __this, Dictionary_2_t1_2422 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m1_21668(__this, ___host, method) (( void (*) (ShimEnumerator_t1_2435 *, Dictionary_2_t1_2422 *, const MethodInfo*))ShimEnumerator__ctor_m1_21668_gshared)(__this, ___host, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::Dispose()
extern "C" void ShimEnumerator_Dispose_m1_21669_gshared (ShimEnumerator_t1_2435 * __this, const MethodInfo* method);
#define ShimEnumerator_Dispose_m1_21669(__this, method) (( void (*) (ShimEnumerator_t1_2435 *, const MethodInfo*))ShimEnumerator_Dispose_m1_21669_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m1_21670_gshared (ShimEnumerator_t1_2435 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m1_21670(__this, method) (( bool (*) (ShimEnumerator_t1_2435 *, const MethodInfo*))ShimEnumerator_MoveNext_m1_21670_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Entry()
extern "C" DictionaryEntry_t1_284  ShimEnumerator_get_Entry_m1_21671_gshared (ShimEnumerator_t1_2435 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m1_21671(__this, method) (( DictionaryEntry_t1_284  (*) (ShimEnumerator_t1_2435 *, const MethodInfo*))ShimEnumerator_get_Entry_m1_21671_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m1_21672_gshared (ShimEnumerator_t1_2435 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m1_21672(__this, method) (( Object_t * (*) (ShimEnumerator_t1_2435 *, const MethodInfo*))ShimEnumerator_get_Key_m1_21672_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m1_21673_gshared (ShimEnumerator_t1_2435 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m1_21673(__this, method) (( Object_t * (*) (ShimEnumerator_t1_2435 *, const MethodInfo*))ShimEnumerator_get_Value_m1_21673_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m1_21674_gshared (ShimEnumerator_t1_2435 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m1_21674(__this, method) (( Object_t * (*) (ShimEnumerator_t1_2435 *, const MethodInfo*))ShimEnumerator_get_Current_m1_21674_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::Reset()
extern "C" void ShimEnumerator_Reset_m1_21675_gshared (ShimEnumerator_t1_2435 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m1_21675(__this, method) (( void (*) (ShimEnumerator_t1_2435 *, const MethodInfo*))ShimEnumerator_Reset_m1_21675_gshared)(__this, method)
