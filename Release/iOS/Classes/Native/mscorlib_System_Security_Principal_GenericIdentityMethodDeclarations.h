﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Principal.GenericIdentity
struct GenericIdentity_t1_1372;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Principal.GenericIdentity::.ctor(System.String,System.String)
extern "C" void GenericIdentity__ctor_m1_11784 (GenericIdentity_t1_1372 * __this, String_t* ___name, String_t* ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Principal.GenericIdentity::.ctor(System.String)
extern "C" void GenericIdentity__ctor_m1_11785 (GenericIdentity_t1_1372 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Principal.GenericIdentity::get_AuthenticationType()
extern "C" String_t* GenericIdentity_get_AuthenticationType_m1_11786 (GenericIdentity_t1_1372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Principal.GenericIdentity::get_Name()
extern "C" String_t* GenericIdentity_get_Name_m1_11787 (GenericIdentity_t1_1372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Principal.GenericIdentity::get_IsAuthenticated()
extern "C" bool GenericIdentity_get_IsAuthenticated_m1_11788 (GenericIdentity_t1_1372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
