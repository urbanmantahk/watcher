﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct List_1_t1_2185;
// System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerable_1_t1_2784;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t1_2186;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t1_2777;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct ICollection_1_t1_2014;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct ReadOnlyCollection_1_t1_2188;
// System.Collections.Generic.IComparer`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IComparer_1_t1_2785;
// System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Predicate_1_t1_2195;
// System.Action`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Action_1_t1_2196;
// System.Comparison`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Comparison_1_t1_2197;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_14.h"

// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor()
extern "C" void List_1__ctor_m1_17714_gshared (List_1_t1_2185 * __this, const MethodInfo* method);
#define List_1__ctor_m1_17714(__this, method) (( void (*) (List_1_t1_2185 *, const MethodInfo*))List_1__ctor_m1_17714_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m1_17716_gshared (List_1_t1_2185 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m1_17716(__this, ___collection, method) (( void (*) (List_1_t1_2185 *, Object_t*, const MethodInfo*))List_1__ctor_m1_17716_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Int32)
extern "C" void List_1__ctor_m1_17718_gshared (List_1_t1_2185 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m1_17718(__this, ___capacity, method) (( void (*) (List_1_t1_2185 *, int32_t, const MethodInfo*))List_1__ctor_m1_17718_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(T[],System.Int32)
extern "C" void List_1__ctor_m1_17720_gshared (List_1_t1_2185 * __this, KeyValuePair_2U5BU5D_t1_2186* ___data, int32_t ___size, const MethodInfo* method);
#define List_1__ctor_m1_17720(__this, ___data, ___size, method) (( void (*) (List_1_t1_2185 *, KeyValuePair_2U5BU5D_t1_2186*, int32_t, const MethodInfo*))List_1__ctor_m1_17720_gshared)(__this, ___data, ___size, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.cctor()
extern "C" void List_1__cctor_m1_17722_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m1_17722(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m1_17722_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_17724_gshared (List_1_t1_2185 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_17724(__this, method) (( Object_t* (*) (List_1_t1_2185 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_17724_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m1_17726_gshared (List_1_t1_2185 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m1_17726(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_2185 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m1_17726_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m1_17728_gshared (List_1_t1_2185 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1_17728(__this, method) (( Object_t * (*) (List_1_t1_2185 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1_17728_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m1_17730_gshared (List_1_t1_2185 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m1_17730(__this, ___item, method) (( int32_t (*) (List_1_t1_2185 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m1_17730_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m1_17732_gshared (List_1_t1_2185 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m1_17732(__this, ___item, method) (( bool (*) (List_1_t1_2185 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1_17732_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m1_17734_gshared (List_1_t1_2185 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m1_17734(__this, ___item, method) (( int32_t (*) (List_1_t1_2185 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m1_17734_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m1_17736_gshared (List_1_t1_2185 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m1_17736(__this, ___index, ___item, method) (( void (*) (List_1_t1_2185 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m1_17736_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m1_17738_gshared (List_1_t1_2185 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m1_17738(__this, ___item, method) (( void (*) (List_1_t1_2185 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1_17738_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_17740_gshared (List_1_t1_2185 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_17740(__this, method) (( bool (*) (List_1_t1_2185 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_17740_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m1_17742_gshared (List_1_t1_2185 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m1_17742(__this, method) (( bool (*) (List_1_t1_2185 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m1_17742_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m1_17744_gshared (List_1_t1_2185 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m1_17744(__this, method) (( Object_t * (*) (List_1_t1_2185 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1_17744_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m1_17746_gshared (List_1_t1_2185 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m1_17746(__this, method) (( bool (*) (List_1_t1_2185 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m1_17746_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m1_17748_gshared (List_1_t1_2185 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m1_17748(__this, method) (( bool (*) (List_1_t1_2185 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m1_17748_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m1_17750_gshared (List_1_t1_2185 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m1_17750(__this, ___index, method) (( Object_t * (*) (List_1_t1_2185 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m1_17750_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m1_17752_gshared (List_1_t1_2185 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m1_17752(__this, ___index, ___value, method) (( void (*) (List_1_t1_2185 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m1_17752_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Add(T)
extern "C" void List_1_Add_m1_17753_gshared (List_1_t1_2185 * __this, KeyValuePair_2_t1_2015  ___item, const MethodInfo* method);
#define List_1_Add_m1_17753(__this, ___item, method) (( void (*) (List_1_t1_2185 *, KeyValuePair_2_t1_2015 , const MethodInfo*))List_1_Add_m1_17753_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m1_17755_gshared (List_1_t1_2185 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m1_17755(__this, ___newCount, method) (( void (*) (List_1_t1_2185 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m1_17755_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::CheckRange(System.Int32,System.Int32)
extern "C" void List_1_CheckRange_m1_17757_gshared (List_1_t1_2185 * __this, int32_t ___idx, int32_t ___count, const MethodInfo* method);
#define List_1_CheckRange_m1_17757(__this, ___idx, ___count, method) (( void (*) (List_1_t1_2185 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m1_17757_gshared)(__this, ___idx, ___count, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m1_17759_gshared (List_1_t1_2185 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m1_17759(__this, ___collection, method) (( void (*) (List_1_t1_2185 *, Object_t*, const MethodInfo*))List_1_AddCollection_m1_17759_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m1_17761_gshared (List_1_t1_2185 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m1_17761(__this, ___enumerable, method) (( void (*) (List_1_t1_2185 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m1_17761_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m1_17763_gshared (List_1_t1_2185 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m1_17763(__this, ___collection, method) (( void (*) (List_1_t1_2185 *, Object_t*, const MethodInfo*))List_1_AddRange_m1_17763_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t1_2188 * List_1_AsReadOnly_m1_17765_gshared (List_1_t1_2185 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m1_17765(__this, method) (( ReadOnlyCollection_1_t1_2188 * (*) (List_1_t1_2185 *, const MethodInfo*))List_1_AsReadOnly_m1_17765_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::BinarySearch(T)
extern "C" int32_t List_1_BinarySearch_m1_17767_gshared (List_1_t1_2185 * __this, KeyValuePair_2_t1_2015  ___item, const MethodInfo* method);
#define List_1_BinarySearch_m1_17767(__this, ___item, method) (( int32_t (*) (List_1_t1_2185 *, KeyValuePair_2_t1_2015 , const MethodInfo*))List_1_BinarySearch_m1_17767_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::BinarySearch(T,System.Collections.Generic.IComparer`1<T>)
extern "C" int32_t List_1_BinarySearch_m1_17769_gshared (List_1_t1_2185 * __this, KeyValuePair_2_t1_2015  ___item, Object_t* ___comparer, const MethodInfo* method);
#define List_1_BinarySearch_m1_17769(__this, ___item, ___comparer, method) (( int32_t (*) (List_1_t1_2185 *, KeyValuePair_2_t1_2015 , Object_t*, const MethodInfo*))List_1_BinarySearch_m1_17769_gshared)(__this, ___item, ___comparer, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::BinarySearch(System.Int32,System.Int32,T,System.Collections.Generic.IComparer`1<T>)
extern "C" int32_t List_1_BinarySearch_m1_17771_gshared (List_1_t1_2185 * __this, int32_t ___index, int32_t ___count, KeyValuePair_2_t1_2015  ___item, Object_t* ___comparer, const MethodInfo* method);
#define List_1_BinarySearch_m1_17771(__this, ___index, ___count, ___item, ___comparer, method) (( int32_t (*) (List_1_t1_2185 *, int32_t, int32_t, KeyValuePair_2_t1_2015 , Object_t*, const MethodInfo*))List_1_BinarySearch_m1_17771_gshared)(__this, ___index, ___count, ___item, ___comparer, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Clear()
extern "C" void List_1_Clear_m1_17773_gshared (List_1_t1_2185 * __this, const MethodInfo* method);
#define List_1_Clear_m1_17773(__this, method) (( void (*) (List_1_t1_2185 *, const MethodInfo*))List_1_Clear_m1_17773_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Contains(T)
extern "C" bool List_1_Contains_m1_17775_gshared (List_1_t1_2185 * __this, KeyValuePair_2_t1_2015  ___item, const MethodInfo* method);
#define List_1_Contains_m1_17775(__this, ___item, method) (( bool (*) (List_1_t1_2185 *, KeyValuePair_2_t1_2015 , const MethodInfo*))List_1_Contains_m1_17775_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::CopyTo(T[])
extern "C" void List_1_CopyTo_m1_17777_gshared (List_1_t1_2185 * __this, KeyValuePair_2U5BU5D_t1_2186* ___array, const MethodInfo* method);
#define List_1_CopyTo_m1_17777(__this, ___array, method) (( void (*) (List_1_t1_2185 *, KeyValuePair_2U5BU5D_t1_2186*, const MethodInfo*))List_1_CopyTo_m1_17777_gshared)(__this, ___array, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m1_17779_gshared (List_1_t1_2185 * __this, KeyValuePair_2U5BU5D_t1_2186* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m1_17779(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_2185 *, KeyValuePair_2U5BU5D_t1_2186*, int32_t, const MethodInfo*))List_1_CopyTo_m1_17779_gshared)(__this, ___array, ___arrayIndex, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::CopyTo(System.Int32,T[],System.Int32,System.Int32)
extern "C" void List_1_CopyTo_m1_17781_gshared (List_1_t1_2185 * __this, int32_t ___index, KeyValuePair_2U5BU5D_t1_2186* ___array, int32_t ___arrayIndex, int32_t ___count, const MethodInfo* method);
#define List_1_CopyTo_m1_17781(__this, ___index, ___array, ___arrayIndex, ___count, method) (( void (*) (List_1_t1_2185 *, int32_t, KeyValuePair_2U5BU5D_t1_2186*, int32_t, int32_t, const MethodInfo*))List_1_CopyTo_m1_17781_gshared)(__this, ___index, ___array, ___arrayIndex, ___count, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Exists(System.Predicate`1<T>)
extern "C" bool List_1_Exists_m1_17783_gshared (List_1_t1_2185 * __this, Predicate_1_t1_2195 * ___match, const MethodInfo* method);
#define List_1_Exists_m1_17783(__this, ___match, method) (( bool (*) (List_1_t1_2185 *, Predicate_1_t1_2195 *, const MethodInfo*))List_1_Exists_m1_17783_gshared)(__this, ___match, method)
// T System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Find(System.Predicate`1<T>)
extern "C" KeyValuePair_2_t1_2015  List_1_Find_m1_17785_gshared (List_1_t1_2185 * __this, Predicate_1_t1_2195 * ___match, const MethodInfo* method);
#define List_1_Find_m1_17785(__this, ___match, method) (( KeyValuePair_2_t1_2015  (*) (List_1_t1_2185 *, Predicate_1_t1_2195 *, const MethodInfo*))List_1_Find_m1_17785_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m1_17787_gshared (Object_t * __this /* static, unused */, Predicate_1_t1_2195 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m1_17787(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t1_2195 *, const MethodInfo*))List_1_CheckMatch_m1_17787_gshared)(__this /* static, unused */, ___match, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::FindAll(System.Predicate`1<T>)
extern "C" List_1_t1_2185 * List_1_FindAll_m1_17789_gshared (List_1_t1_2185 * __this, Predicate_1_t1_2195 * ___match, const MethodInfo* method);
#define List_1_FindAll_m1_17789(__this, ___match, method) (( List_1_t1_2185 * (*) (List_1_t1_2185 *, Predicate_1_t1_2195 *, const MethodInfo*))List_1_FindAll_m1_17789_gshared)(__this, ___match, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::FindAllStackBits(System.Predicate`1<T>)
extern "C" List_1_t1_2185 * List_1_FindAllStackBits_m1_17791_gshared (List_1_t1_2185 * __this, Predicate_1_t1_2195 * ___match, const MethodInfo* method);
#define List_1_FindAllStackBits_m1_17791(__this, ___match, method) (( List_1_t1_2185 * (*) (List_1_t1_2185 *, Predicate_1_t1_2195 *, const MethodInfo*))List_1_FindAllStackBits_m1_17791_gshared)(__this, ___match, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::FindAllList(System.Predicate`1<T>)
extern "C" List_1_t1_2185 * List_1_FindAllList_m1_17793_gshared (List_1_t1_2185 * __this, Predicate_1_t1_2195 * ___match, const MethodInfo* method);
#define List_1_FindAllList_m1_17793(__this, ___match, method) (( List_1_t1_2185 * (*) (List_1_t1_2185 *, Predicate_1_t1_2195 *, const MethodInfo*))List_1_FindAllList_m1_17793_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::FindIndex(System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m1_17795_gshared (List_1_t1_2185 * __this, Predicate_1_t1_2195 * ___match, const MethodInfo* method);
#define List_1_FindIndex_m1_17795(__this, ___match, method) (( int32_t (*) (List_1_t1_2185 *, Predicate_1_t1_2195 *, const MethodInfo*))List_1_FindIndex_m1_17795_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::FindIndex(System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m1_17797_gshared (List_1_t1_2185 * __this, int32_t ___startIndex, Predicate_1_t1_2195 * ___match, const MethodInfo* method);
#define List_1_FindIndex_m1_17797(__this, ___startIndex, ___match, method) (( int32_t (*) (List_1_t1_2185 *, int32_t, Predicate_1_t1_2195 *, const MethodInfo*))List_1_FindIndex_m1_17797_gshared)(__this, ___startIndex, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::FindIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m1_17799_gshared (List_1_t1_2185 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_2195 * ___match, const MethodInfo* method);
#define List_1_FindIndex_m1_17799(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_2185 *, int32_t, int32_t, Predicate_1_t1_2195 *, const MethodInfo*))List_1_FindIndex_m1_17799_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m1_17801_gshared (List_1_t1_2185 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_2195 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m1_17801(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_2185 *, int32_t, int32_t, Predicate_1_t1_2195 *, const MethodInfo*))List_1_GetIndex_m1_17801_gshared)(__this, ___startIndex, ___count, ___match, method)
// T System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::FindLast(System.Predicate`1<T>)
extern "C" KeyValuePair_2_t1_2015  List_1_FindLast_m1_17803_gshared (List_1_t1_2185 * __this, Predicate_1_t1_2195 * ___match, const MethodInfo* method);
#define List_1_FindLast_m1_17803(__this, ___match, method) (( KeyValuePair_2_t1_2015  (*) (List_1_t1_2185 *, Predicate_1_t1_2195 *, const MethodInfo*))List_1_FindLast_m1_17803_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::FindLastIndex(System.Predicate`1<T>)
extern "C" int32_t List_1_FindLastIndex_m1_17805_gshared (List_1_t1_2185 * __this, Predicate_1_t1_2195 * ___match, const MethodInfo* method);
#define List_1_FindLastIndex_m1_17805(__this, ___match, method) (( int32_t (*) (List_1_t1_2185 *, Predicate_1_t1_2195 *, const MethodInfo*))List_1_FindLastIndex_m1_17805_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::FindLastIndex(System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindLastIndex_m1_17807_gshared (List_1_t1_2185 * __this, int32_t ___startIndex, Predicate_1_t1_2195 * ___match, const MethodInfo* method);
#define List_1_FindLastIndex_m1_17807(__this, ___startIndex, ___match, method) (( int32_t (*) (List_1_t1_2185 *, int32_t, Predicate_1_t1_2195 *, const MethodInfo*))List_1_FindLastIndex_m1_17807_gshared)(__this, ___startIndex, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::FindLastIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindLastIndex_m1_17809_gshared (List_1_t1_2185 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_2195 * ___match, const MethodInfo* method);
#define List_1_FindLastIndex_m1_17809(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_2185 *, int32_t, int32_t, Predicate_1_t1_2195 *, const MethodInfo*))List_1_FindLastIndex_m1_17809_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetLastIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetLastIndex_m1_17811_gshared (List_1_t1_2185 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_2195 * ___match, const MethodInfo* method);
#define List_1_GetLastIndex_m1_17811(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_2185 *, int32_t, int32_t, Predicate_1_t1_2195 *, const MethodInfo*))List_1_GetLastIndex_m1_17811_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ForEach(System.Action`1<T>)
extern "C" void List_1_ForEach_m1_17813_gshared (List_1_t1_2185 * __this, Action_1_t1_2196 * ___action, const MethodInfo* method);
#define List_1_ForEach_m1_17813(__this, ___action, method) (( void (*) (List_1_t1_2185 *, Action_1_t1_2196 *, const MethodInfo*))List_1_ForEach_m1_17813_gshared)(__this, ___action, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetEnumerator()
extern "C" Enumerator_t1_2187  List_1_GetEnumerator_m1_17814_gshared (List_1_t1_2185 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m1_17814(__this, method) (( Enumerator_t1_2187  (*) (List_1_t1_2185 *, const MethodInfo*))List_1_GetEnumerator_m1_17814_gshared)(__this, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetRange(System.Int32,System.Int32)
extern "C" List_1_t1_2185 * List_1_GetRange_m1_17816_gshared (List_1_t1_2185 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_GetRange_m1_17816(__this, ___index, ___count, method) (( List_1_t1_2185 * (*) (List_1_t1_2185 *, int32_t, int32_t, const MethodInfo*))List_1_GetRange_m1_17816_gshared)(__this, ___index, ___count, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m1_17818_gshared (List_1_t1_2185 * __this, KeyValuePair_2_t1_2015  ___item, const MethodInfo* method);
#define List_1_IndexOf_m1_17818(__this, ___item, method) (( int32_t (*) (List_1_t1_2185 *, KeyValuePair_2_t1_2015 , const MethodInfo*))List_1_IndexOf_m1_17818_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::IndexOf(T,System.Int32)
extern "C" int32_t List_1_IndexOf_m1_17820_gshared (List_1_t1_2185 * __this, KeyValuePair_2_t1_2015  ___item, int32_t ___index, const MethodInfo* method);
#define List_1_IndexOf_m1_17820(__this, ___item, ___index, method) (( int32_t (*) (List_1_t1_2185 *, KeyValuePair_2_t1_2015 , int32_t, const MethodInfo*))List_1_IndexOf_m1_17820_gshared)(__this, ___item, ___index, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::IndexOf(T,System.Int32,System.Int32)
extern "C" int32_t List_1_IndexOf_m1_17822_gshared (List_1_t1_2185 * __this, KeyValuePair_2_t1_2015  ___item, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_IndexOf_m1_17822(__this, ___item, ___index, ___count, method) (( int32_t (*) (List_1_t1_2185 *, KeyValuePair_2_t1_2015 , int32_t, int32_t, const MethodInfo*))List_1_IndexOf_m1_17822_gshared)(__this, ___item, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m1_17824_gshared (List_1_t1_2185 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m1_17824(__this, ___start, ___delta, method) (( void (*) (List_1_t1_2185 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m1_17824_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m1_17826_gshared (List_1_t1_2185 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m1_17826(__this, ___index, method) (( void (*) (List_1_t1_2185 *, int32_t, const MethodInfo*))List_1_CheckIndex_m1_17826_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m1_17828_gshared (List_1_t1_2185 * __this, int32_t ___index, KeyValuePair_2_t1_2015  ___item, const MethodInfo* method);
#define List_1_Insert_m1_17828(__this, ___index, ___item, method) (( void (*) (List_1_t1_2185 *, int32_t, KeyValuePair_2_t1_2015 , const MethodInfo*))List_1_Insert_m1_17828_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m1_17830_gshared (List_1_t1_2185 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m1_17830(__this, ___collection, method) (( void (*) (List_1_t1_2185 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m1_17830_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::InsertRange(System.Int32,System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_InsertRange_m1_17832_gshared (List_1_t1_2185 * __this, int32_t ___index, Object_t* ___collection, const MethodInfo* method);
#define List_1_InsertRange_m1_17832(__this, ___index, ___collection, method) (( void (*) (List_1_t1_2185 *, int32_t, Object_t*, const MethodInfo*))List_1_InsertRange_m1_17832_gshared)(__this, ___index, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::InsertCollection(System.Int32,System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_InsertCollection_m1_17834_gshared (List_1_t1_2185 * __this, int32_t ___index, Object_t* ___collection, const MethodInfo* method);
#define List_1_InsertCollection_m1_17834(__this, ___index, ___collection, method) (( void (*) (List_1_t1_2185 *, int32_t, Object_t*, const MethodInfo*))List_1_InsertCollection_m1_17834_gshared)(__this, ___index, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::InsertEnumeration(System.Int32,System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_InsertEnumeration_m1_17836_gshared (List_1_t1_2185 * __this, int32_t ___index, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_InsertEnumeration_m1_17836(__this, ___index, ___enumerable, method) (( void (*) (List_1_t1_2185 *, int32_t, Object_t*, const MethodInfo*))List_1_InsertEnumeration_m1_17836_gshared)(__this, ___index, ___enumerable, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::LastIndexOf(T)
extern "C" int32_t List_1_LastIndexOf_m1_17838_gshared (List_1_t1_2185 * __this, KeyValuePair_2_t1_2015  ___item, const MethodInfo* method);
#define List_1_LastIndexOf_m1_17838(__this, ___item, method) (( int32_t (*) (List_1_t1_2185 *, KeyValuePair_2_t1_2015 , const MethodInfo*))List_1_LastIndexOf_m1_17838_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::LastIndexOf(T,System.Int32)
extern "C" int32_t List_1_LastIndexOf_m1_17840_gshared (List_1_t1_2185 * __this, KeyValuePair_2_t1_2015  ___item, int32_t ___index, const MethodInfo* method);
#define List_1_LastIndexOf_m1_17840(__this, ___item, ___index, method) (( int32_t (*) (List_1_t1_2185 *, KeyValuePair_2_t1_2015 , int32_t, const MethodInfo*))List_1_LastIndexOf_m1_17840_gshared)(__this, ___item, ___index, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::LastIndexOf(T,System.Int32,System.Int32)
extern "C" int32_t List_1_LastIndexOf_m1_17842_gshared (List_1_t1_2185 * __this, KeyValuePair_2_t1_2015  ___item, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_LastIndexOf_m1_17842(__this, ___item, ___index, ___count, method) (( int32_t (*) (List_1_t1_2185 *, KeyValuePair_2_t1_2015 , int32_t, int32_t, const MethodInfo*))List_1_LastIndexOf_m1_17842_gshared)(__this, ___item, ___index, ___count, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Remove(T)
extern "C" bool List_1_Remove_m1_17844_gshared (List_1_t1_2185 * __this, KeyValuePair_2_t1_2015  ___item, const MethodInfo* method);
#define List_1_Remove_m1_17844(__this, ___item, method) (( bool (*) (List_1_t1_2185 *, KeyValuePair_2_t1_2015 , const MethodInfo*))List_1_Remove_m1_17844_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m1_17846_gshared (List_1_t1_2185 * __this, Predicate_1_t1_2195 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m1_17846(__this, ___match, method) (( int32_t (*) (List_1_t1_2185 *, Predicate_1_t1_2195 *, const MethodInfo*))List_1_RemoveAll_m1_17846_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m1_17848_gshared (List_1_t1_2185 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m1_17848(__this, ___index, method) (( void (*) (List_1_t1_2185 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1_17848_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::RemoveRange(System.Int32,System.Int32)
extern "C" void List_1_RemoveRange_m1_17850_gshared (List_1_t1_2185 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_RemoveRange_m1_17850(__this, ___index, ___count, method) (( void (*) (List_1_t1_2185 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m1_17850_gshared)(__this, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Reverse()
extern "C" void List_1_Reverse_m1_17852_gshared (List_1_t1_2185 * __this, const MethodInfo* method);
#define List_1_Reverse_m1_17852(__this, method) (( void (*) (List_1_t1_2185 *, const MethodInfo*))List_1_Reverse_m1_17852_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Reverse(System.Int32,System.Int32)
extern "C" void List_1_Reverse_m1_17854_gshared (List_1_t1_2185 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_Reverse_m1_17854(__this, ___index, ___count, method) (( void (*) (List_1_t1_2185 *, int32_t, int32_t, const MethodInfo*))List_1_Reverse_m1_17854_gshared)(__this, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Sort()
extern "C" void List_1_Sort_m1_17856_gshared (List_1_t1_2185 * __this, const MethodInfo* method);
#define List_1_Sort_m1_17856(__this, method) (( void (*) (List_1_t1_2185 *, const MethodInfo*))List_1_Sort_m1_17856_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C" void List_1_Sort_m1_17858_gshared (List_1_t1_2185 * __this, Object_t* ___comparer, const MethodInfo* method);
#define List_1_Sort_m1_17858(__this, ___comparer, method) (( void (*) (List_1_t1_2185 *, Object_t*, const MethodInfo*))List_1_Sort_m1_17858_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m1_17860_gshared (List_1_t1_2185 * __this, Comparison_1_t1_2197 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m1_17860(__this, ___comparison, method) (( void (*) (List_1_t1_2185 *, Comparison_1_t1_2197 *, const MethodInfo*))List_1_Sort_m1_17860_gshared)(__this, ___comparison, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Sort(System.Int32,System.Int32,System.Collections.Generic.IComparer`1<T>)
extern "C" void List_1_Sort_m1_17862_gshared (List_1_t1_2185 * __this, int32_t ___index, int32_t ___count, Object_t* ___comparer, const MethodInfo* method);
#define List_1_Sort_m1_17862(__this, ___index, ___count, ___comparer, method) (( void (*) (List_1_t1_2185 *, int32_t, int32_t, Object_t*, const MethodInfo*))List_1_Sort_m1_17862_gshared)(__this, ___index, ___count, ___comparer, method)
// T[] System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ToArray()
extern "C" KeyValuePair_2U5BU5D_t1_2186* List_1_ToArray_m1_17864_gshared (List_1_t1_2185 * __this, const MethodInfo* method);
#define List_1_ToArray_m1_17864(__this, method) (( KeyValuePair_2U5BU5D_t1_2186* (*) (List_1_t1_2185 *, const MethodInfo*))List_1_ToArray_m1_17864_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::TrimExcess()
extern "C" void List_1_TrimExcess_m1_17866_gshared (List_1_t1_2185 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m1_17866(__this, method) (( void (*) (List_1_t1_2185 *, const MethodInfo*))List_1_TrimExcess_m1_17866_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::TrueForAll(System.Predicate`1<T>)
extern "C" bool List_1_TrueForAll_m1_17868_gshared (List_1_t1_2185 * __this, Predicate_1_t1_2195 * ___match, const MethodInfo* method);
#define List_1_TrueForAll_m1_17868(__this, ___match, method) (( bool (*) (List_1_t1_2185 *, Predicate_1_t1_2195 *, const MethodInfo*))List_1_TrueForAll_m1_17868_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m1_17870_gshared (List_1_t1_2185 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m1_17870(__this, method) (( int32_t (*) (List_1_t1_2185 *, const MethodInfo*))List_1_get_Capacity_m1_17870_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m1_17872_gshared (List_1_t1_2185 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m1_17872(__this, ___value, method) (( void (*) (List_1_t1_2185 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1_17872_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Count()
extern "C" int32_t List_1_get_Count_m1_17874_gshared (List_1_t1_2185 * __this, const MethodInfo* method);
#define List_1_get_Count_m1_17874(__this, method) (( int32_t (*) (List_1_t1_2185 *, const MethodInfo*))List_1_get_Count_m1_17874_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Item(System.Int32)
extern "C" KeyValuePair_2_t1_2015  List_1_get_Item_m1_17876_gshared (List_1_t1_2185 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m1_17876(__this, ___index, method) (( KeyValuePair_2_t1_2015  (*) (List_1_t1_2185 *, int32_t, const MethodInfo*))List_1_get_Item_m1_17876_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m1_17878_gshared (List_1_t1_2185 * __this, int32_t ___index, KeyValuePair_2_t1_2015  ___value, const MethodInfo* method);
#define List_1_set_Item_m1_17878(__this, ___index, ___value, method) (( void (*) (List_1_t1_2185 *, int32_t, KeyValuePair_2_t1_2015 , const MethodInfo*))List_1_set_Item_m1_17878_gshared)(__this, ___index, ___value, method)
