﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

extern "C" void STATSTG_t1_741_marshal(const STATSTG_t1_741& unmarshaled, STATSTG_t1_741_marshaled& marshaled);
extern "C" void STATSTG_t1_741_marshal_back(const STATSTG_t1_741_marshaled& marshaled, STATSTG_t1_741& unmarshaled);
extern "C" void STATSTG_t1_741_marshal_cleanup(STATSTG_t1_741_marshaled& marshaled);
