﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XmlImplementation
struct XmlImplementation_t4_130;
// System.Xml.XmlNameTable
struct XmlNameTable_t4_67;
// System.Xml.XmlDocument
struct XmlDocument_t4_123;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Xml.XmlImplementation::.ctor()
extern "C" void XmlImplementation__ctor_m4_531 (XmlImplementation_t4_130 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XmlImplementation::.ctor(System.Xml.XmlNameTable)
extern "C" void XmlImplementation__ctor_m4_532 (XmlImplementation_t4_130 * __this, XmlNameTable_t4_67 * ___nameTable, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlDocument System.Xml.XmlImplementation::CreateDocument()
extern "C" XmlDocument_t4_123 * XmlImplementation_CreateDocument_m4_533 (XmlImplementation_t4_130 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
