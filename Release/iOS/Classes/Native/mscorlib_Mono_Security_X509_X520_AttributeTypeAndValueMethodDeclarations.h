﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.X509.X520/AttributeTypeAndValue
struct AttributeTypeAndValue_t1_200;
// System.String
struct String_t;
// Mono.Security.ASN1
struct ASN1_t1_149;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Security.X509.X520/AttributeTypeAndValue::.ctor(System.String,System.Int32)
extern "C" void AttributeTypeAndValue__ctor_m1_2391 (AttributeTypeAndValue_t1_200 * __this, String_t* ___oid, int32_t ___upperBound, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X520/AttributeTypeAndValue::.ctor(System.String,System.Int32,System.Byte)
extern "C" void AttributeTypeAndValue__ctor_m1_2392 (AttributeTypeAndValue_t1_200 * __this, String_t* ___oid, int32_t ___upperBound, uint8_t ___encoding, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.X509.X520/AttributeTypeAndValue::get_Value()
extern "C" String_t* AttributeTypeAndValue_get_Value_m1_2393 (AttributeTypeAndValue_t1_200 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X520/AttributeTypeAndValue::set_Value(System.String)
extern "C" void AttributeTypeAndValue_set_Value_m1_2394 (AttributeTypeAndValue_t1_200 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.ASN1 Mono.Security.X509.X520/AttributeTypeAndValue::get_ASN1()
extern "C" ASN1_t1_149 * AttributeTypeAndValue_get_ASN1_m1_2395 (AttributeTypeAndValue_t1_200 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.ASN1 Mono.Security.X509.X520/AttributeTypeAndValue::GetASN1(System.Byte)
extern "C" ASN1_t1_149 * AttributeTypeAndValue_GetASN1_m1_2396 (AttributeTypeAndValue_t1_200 * __this, uint8_t ___encoding, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.ASN1 Mono.Security.X509.X520/AttributeTypeAndValue::GetASN1()
extern "C" ASN1_t1_149 * AttributeTypeAndValue_GetASN1_m1_2397 (AttributeTypeAndValue_t1_200 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.X509.X520/AttributeTypeAndValue::GetBytes(System.Byte)
extern "C" ByteU5BU5D_t1_109* AttributeTypeAndValue_GetBytes_m1_2398 (AttributeTypeAndValue_t1_200 * __this, uint8_t ___encoding, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.X509.X520/AttributeTypeAndValue::GetBytes()
extern "C" ByteU5BU5D_t1_109* AttributeTypeAndValue_GetBytes_m1_2399 (AttributeTypeAndValue_t1_200 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte Mono.Security.X509.X520/AttributeTypeAndValue::SelectBestEncoding()
extern "C" uint8_t AttributeTypeAndValue_SelectBestEncoding_m1_2400 (AttributeTypeAndValue_t1_200 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
