﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.RuntimeSecurityFrame
struct RuntimeSecurityFrame_t1_1402;
// System.Array
struct Array_t;
// System.Reflection.Assembly
struct Assembly_t1_467;
// System.AppDomain
struct AppDomain_t1_1403;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Security.PermissionSet
struct PermissionSet_t1_563;
// System.String
struct String_t;
// System.Collections.ArrayList
struct ArrayList_t1_170;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_SecurityFrame.h"

// System.Void System.Security.SecurityFrame::.ctor(System.Security.RuntimeSecurityFrame)
extern "C" void SecurityFrame__ctor_m1_12100 (SecurityFrame_t1_1404 * __this, RuntimeSecurityFrame_t1_1402 * ___frame, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityFrame::.ctor(System.Int32)
extern "C" void SecurityFrame__ctor_m1_12101 (SecurityFrame_t1_1404 * __this, int32_t ___skip, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.RuntimeSecurityFrame System.Security.SecurityFrame::_GetSecurityFrame(System.Int32)
extern "C" RuntimeSecurityFrame_t1_1402 * SecurityFrame__GetSecurityFrame_m1_12102 (Object_t * __this /* static, unused */, int32_t ___skip, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Array System.Security.SecurityFrame::_GetSecurityStack(System.Int32)
extern "C" Array_t * SecurityFrame__GetSecurityStack_m1_12103 (Object_t * __this /* static, unused */, int32_t ___skip, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityFrame::InitFromRuntimeFrame(System.Security.RuntimeSecurityFrame)
extern "C" void SecurityFrame_InitFromRuntimeFrame_m1_12104 (SecurityFrame_t1_1404 * __this, RuntimeSecurityFrame_t1_1402 * ___frame, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.Security.SecurityFrame::get_Assembly()
extern "C" Assembly_t1_467 * SecurityFrame_get_Assembly_m1_12105 (SecurityFrame_t1_1404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.AppDomain System.Security.SecurityFrame::get_Domain()
extern "C" AppDomain_t1_1403 * SecurityFrame_get_Domain_m1_12106 (SecurityFrame_t1_1404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Security.SecurityFrame::get_Method()
extern "C" MethodInfo_t * SecurityFrame_get_Method_m1_12107 (SecurityFrame_t1_1404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.PermissionSet System.Security.SecurityFrame::get_Assert()
extern "C" PermissionSet_t1_563 * SecurityFrame_get_Assert_m1_12108 (SecurityFrame_t1_1404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.PermissionSet System.Security.SecurityFrame::get_Deny()
extern "C" PermissionSet_t1_563 * SecurityFrame_get_Deny_m1_12109 (SecurityFrame_t1_1404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.PermissionSet System.Security.SecurityFrame::get_PermitOnly()
extern "C" PermissionSet_t1_563 * SecurityFrame_get_PermitOnly_m1_12110 (SecurityFrame_t1_1404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.SecurityFrame::get_HasStackModifiers()
extern "C" bool SecurityFrame_get_HasStackModifiers_m1_12111 (SecurityFrame_t1_1404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.SecurityFrame::Equals(System.Security.SecurityFrame)
extern "C" bool SecurityFrame_Equals_m1_12112 (SecurityFrame_t1_1404 * __this, SecurityFrame_t1_1404  ___sf, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.SecurityFrame::ToString()
extern "C" String_t* SecurityFrame_ToString_m1_12113 (SecurityFrame_t1_1404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList System.Security.SecurityFrame::GetStack(System.Int32)
extern "C" ArrayList_t1_170 * SecurityFrame_GetStack_m1_12114 (Object_t * __this /* static, unused */, int32_t ___skipFrames, const MethodInfo* method) IL2CPP_METHOD_ATTR;
