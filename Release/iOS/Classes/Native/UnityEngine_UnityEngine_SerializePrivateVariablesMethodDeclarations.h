﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.SerializePrivateVariables
struct SerializePrivateVariables_t6_69;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.SerializePrivateVariables::.ctor()
extern "C" void SerializePrivateVariables__ctor_m6_491 (SerializePrivateVariables_t6_69 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
