﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Type
struct Type_t;

#include "mscorlib_System_Attribute.h"

// System.Runtime.CompilerServices.TypeForwardedToAttribute
struct  TypeForwardedToAttribute_t1_55  : public Attribute_t1_2
{
	// System.Type System.Runtime.CompilerServices.TypeForwardedToAttribute::destination
	Type_t * ___destination_0;
};
