﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Type
struct Type_t;
// System.Runtime.Remoting.Messaging.ObjRefSurrogate
struct ObjRefSurrogate_t1_956;
// System.Runtime.Remoting.Messaging.RemotingSurrogate
struct RemotingSurrogate_t1_955;
// System.Object
struct Object_t;
// System.Runtime.Remoting.Messaging.MessageSurrogateFilter
struct MessageSurrogateFilter_t1_958;
// System.Runtime.Serialization.ISurrogateSelector
struct ISurrogateSelector_t1_959;

#include "mscorlib_System_Object.h"

// System.Runtime.Remoting.Messaging.RemotingSurrogateSelector
struct  RemotingSurrogateSelector_t1_957  : public Object_t
{
	// System.Object System.Runtime.Remoting.Messaging.RemotingSurrogateSelector::_rootObj
	Object_t * ____rootObj_3;
	// System.Runtime.Remoting.Messaging.MessageSurrogateFilter System.Runtime.Remoting.Messaging.RemotingSurrogateSelector::_filter
	MessageSurrogateFilter_t1_958 * ____filter_4;
	// System.Runtime.Serialization.ISurrogateSelector System.Runtime.Remoting.Messaging.RemotingSurrogateSelector::_next
	Object_t * ____next_5;
};
struct RemotingSurrogateSelector_t1_957_StaticFields{
	// System.Type System.Runtime.Remoting.Messaging.RemotingSurrogateSelector::s_cachedTypeObjRef
	Type_t * ___s_cachedTypeObjRef_0;
	// System.Runtime.Remoting.Messaging.ObjRefSurrogate System.Runtime.Remoting.Messaging.RemotingSurrogateSelector::_objRefSurrogate
	ObjRefSurrogate_t1_956 * ____objRefSurrogate_1;
	// System.Runtime.Remoting.Messaging.RemotingSurrogate System.Runtime.Remoting.Messaging.RemotingSurrogateSelector::_objRemotingSurrogate
	RemotingSurrogate_t1_955 * ____objRemotingSurrogate_2;
};
