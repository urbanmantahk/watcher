﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.InteropServices.PrimaryInteropAssemblyAttribute
struct PrimaryInteropAssemblyAttribute_t1_815;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.InteropServices.PrimaryInteropAssemblyAttribute::.ctor(System.Int32,System.Int32)
extern "C" void PrimaryInteropAssemblyAttribute__ctor_m1_7870 (PrimaryInteropAssemblyAttribute_t1_815 * __this, int32_t ___major, int32_t ___minor, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.InteropServices.PrimaryInteropAssemblyAttribute::get_MajorVersion()
extern "C" int32_t PrimaryInteropAssemblyAttribute_get_MajorVersion_m1_7871 (PrimaryInteropAssemblyAttribute_t1_815 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.InteropServices.PrimaryInteropAssemblyAttribute::get_MinorVersion()
extern "C" int32_t PrimaryInteropAssemblyAttribute_get_MinorVersion_m1_7872 (PrimaryInteropAssemblyAttribute_t1_815 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
