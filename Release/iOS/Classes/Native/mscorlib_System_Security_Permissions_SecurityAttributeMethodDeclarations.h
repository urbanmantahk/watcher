﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Permissions.SecurityAttribute
struct SecurityAttribute_t1_46;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Permissions_SecurityAction.h"

// System.Void System.Security.Permissions.SecurityAttribute::.ctor(System.Security.Permissions.SecurityAction)
extern "C" void SecurityAttribute__ctor_m1_1302 (SecurityAttribute_t1_46 * __this, int32_t ___action, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.SecurityAttribute::get_Unrestricted()
extern "C" bool SecurityAttribute_get_Unrestricted_m1_1303 (SecurityAttribute_t1_46 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.SecurityAttribute::set_Unrestricted(System.Boolean)
extern "C" void SecurityAttribute_set_Unrestricted_m1_1304 (SecurityAttribute_t1_46 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Permissions.SecurityAction System.Security.Permissions.SecurityAttribute::get_Action()
extern "C" int32_t SecurityAttribute_get_Action_m1_1305 (SecurityAttribute_t1_46 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.SecurityAttribute::set_Action(System.Security.Permissions.SecurityAction)
extern "C" void SecurityAttribute_set_Action_m1_1306 (SecurityAttribute_t1_46 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
