﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.InteropServices.SafeArrayRankMismatchException
struct SafeArrayRankMismatchException_t1_824;
// System.String
struct String_t;
// System.Exception
struct Exception_t1_33;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.Runtime.InteropServices.SafeArrayRankMismatchException::.ctor()
extern "C" void SafeArrayRankMismatchException__ctor_m1_7896 (SafeArrayRankMismatchException_t1_824 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.SafeArrayRankMismatchException::.ctor(System.String)
extern "C" void SafeArrayRankMismatchException__ctor_m1_7897 (SafeArrayRankMismatchException_t1_824 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.SafeArrayRankMismatchException::.ctor(System.String,System.Exception)
extern "C" void SafeArrayRankMismatchException__ctor_m1_7898 (SafeArrayRankMismatchException_t1_824 * __this, String_t* ___message, Exception_t1_33 * ___inner, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.SafeArrayRankMismatchException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void SafeArrayRankMismatchException__ctor_m1_7899 (SafeArrayRankMismatchException_t1_824 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
