﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Security_Permissions_IsolatedStoragePermissi_0.h"

// System.Security.Permissions.IsolatedStorageFilePermissionAttribute
struct  IsolatedStorageFilePermissionAttribute_t1_1286  : public IsolatedStoragePermissionAttribute_t1_1287
{
};
