﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.MediaLibrarySettings
struct MediaLibrarySettings_t8_253;
// VoxelBusters.NativePlugins.MediaLibrarySettings/AndroidSettings
struct AndroidSettings_t8_252;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.MediaLibrarySettings::.ctor()
extern "C" void MediaLibrarySettings__ctor_m8_1397 (MediaLibrarySettings_t8_253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.NativePlugins.MediaLibrarySettings/AndroidSettings VoxelBusters.NativePlugins.MediaLibrarySettings::get_Android()
extern "C" AndroidSettings_t8_252 * MediaLibrarySettings_get_Android_m8_1398 (MediaLibrarySettings_t8_253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.MediaLibrarySettings::set_Android(VoxelBusters.NativePlugins.MediaLibrarySettings/AndroidSettings)
extern "C" void MediaLibrarySettings_set_Android_m8_1399 (MediaLibrarySettings_t8_253 * __this, AndroidSettings_t8_252 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
