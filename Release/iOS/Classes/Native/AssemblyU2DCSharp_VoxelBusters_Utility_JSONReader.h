﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_Internal_JSONString.h"

// VoxelBusters.Utility.JSONReader
struct  JSONReader_t8_56  : public Object_t
{
	// VoxelBusters.Utility.Internal.JSONString VoxelBusters.Utility.JSONReader::<JSONString>k__BackingField
	JSONString_t8_55  ___U3CJSONStringU3Ek__BackingField_0;
};
