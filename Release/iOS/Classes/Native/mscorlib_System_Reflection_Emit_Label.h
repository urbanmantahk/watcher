﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType.h"

// System.Reflection.Emit.Label
struct  Label_t1_513 
{
	// System.Int32 System.Reflection.Emit.Label::label
	int32_t ___label_0;
};
