﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Xml.SmallXmlParserException
struct SmallXmlParserException_t1_249;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Xml.SmallXmlParserException::.ctor(System.String,System.Int32,System.Int32)
extern "C" void SmallXmlParserException__ctor_m1_2717 (SmallXmlParserException_t1_249 * __this, String_t* ___msg, int32_t ___line, int32_t ___column, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Xml.SmallXmlParserException::get_Line()
extern "C" int32_t SmallXmlParserException_get_Line_m1_2718 (SmallXmlParserException_t1_249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Xml.SmallXmlParserException::get_Column()
extern "C" int32_t SmallXmlParserException_get_Column_m1_2719 (SmallXmlParserException_t1_249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
