﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Xml.Schema.XsdBase64Binary
struct XsdBase64Binary_t4_37;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Xml.Schema.XsdBase64Binary::.ctor()
extern "C" void XsdBase64Binary__ctor_m4_52 (XsdBase64Binary_t4_37 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Xml.Schema.XsdBase64Binary::.cctor()
extern "C" void XsdBase64Binary__cctor_m4_53 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
