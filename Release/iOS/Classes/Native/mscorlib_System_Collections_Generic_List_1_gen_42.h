﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Boomlagoon.JSON.JSONValue[]
struct JSONValueU5BU5D_t8_370;

#include "mscorlib_System_Object.h"

// System.Collections.Generic.List`1<Boomlagoon.JSON.JSONValue>
struct  List_1_t1_1898  : public Object_t
{
	// T[] System.Collections.Generic.List`1::_items
	JSONValueU5BU5D_t8_370* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
};
struct List_1_t1_1898_StaticFields{
	// T[] System.Collections.Generic.List`1::EmptyArray
	JSONValueU5BU5D_t8_370* ___EmptyArray_4;
};
