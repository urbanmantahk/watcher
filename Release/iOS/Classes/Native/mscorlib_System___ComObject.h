﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_MarshalByRefObject.h"
#include "mscorlib_System_IntPtr.h"

// System.__ComObject
struct  __ComObject_t1_131  : public MarshalByRefObject_t1_69
{
	// System.IntPtr System.__ComObject::iunknown
	IntPtr_t ___iunknown_1;
	// System.IntPtr System.__ComObject::hash_table
	IntPtr_t ___hash_table_2;
};
