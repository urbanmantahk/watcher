﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.Emit.ILGenerator
struct ILGenerator_t1_480;
// System.Reflection.Module
struct Module_t1_495;
// System.Reflection.Emit.TokenGenerator
struct TokenGenerator_t1_523;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Type
struct Type_t;
// System.Reflection.Emit.LocalBuilder
struct LocalBuilder_t1_527;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t1_478;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// System.Reflection.Emit.Label[]
struct LabelU5BU5D_t1_1687;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Reflection.Emit.SignatureHelper
struct SignatureHelper_t1_551;
// System.String
struct String_t;
// System.Type[]
struct TypeU5BU5D_t1_31;
// System.Diagnostics.SymbolStore.ISymbolDocumentWriter
struct ISymbolDocumentWriter_t1_525;
// System.Diagnostics.SymbolStore.ISymbolWriter
struct ISymbolWriter_t1_536;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Guid.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_Reflection_Emit_OpCode.h"
#include "mscorlib_System_Reflection_Emit_Label.h"
#include "mscorlib_System_Runtime_InteropServices_CallingConvention.h"
#include "mscorlib_System_Reflection_CallingConventions.h"

// System.Void System.Reflection.Emit.ILGenerator::.ctor(System.Reflection.Module,System.Reflection.Emit.TokenGenerator,System.Int32)
extern "C" void ILGenerator__ctor_m1_5965 (ILGenerator_t1_480 * __this, Module_t1_495 * ___m, Object_t * ___token_gen, int32_t ___size, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ILGenerator::.cctor()
extern "C" void ILGenerator__cctor_m1_5966 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ILGenerator::System.Runtime.InteropServices._ILGenerator.GetIDsOfNames(System.Guid&,System.IntPtr,System.UInt32,System.UInt32,System.IntPtr)
extern "C" void ILGenerator_System_Runtime_InteropServices__ILGenerator_GetIDsOfNames_m1_5967 (ILGenerator_t1_480 * __this, Guid_t1_319 * ___riid, IntPtr_t ___rgszNames, uint32_t ___cNames, uint32_t ___lcid, IntPtr_t ___rgDispId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ILGenerator::System.Runtime.InteropServices._ILGenerator.GetTypeInfo(System.UInt32,System.UInt32,System.IntPtr)
extern "C" void ILGenerator_System_Runtime_InteropServices__ILGenerator_GetTypeInfo_m1_5968 (ILGenerator_t1_480 * __this, uint32_t ___iTInfo, uint32_t ___lcid, IntPtr_t ___ppTInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ILGenerator::System.Runtime.InteropServices._ILGenerator.GetTypeInfoCount(System.UInt32&)
extern "C" void ILGenerator_System_Runtime_InteropServices__ILGenerator_GetTypeInfoCount_m1_5969 (ILGenerator_t1_480 * __this, uint32_t* ___pcTInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ILGenerator::System.Runtime.InteropServices._ILGenerator.Invoke(System.UInt32,System.Guid&,System.UInt32,System.Int16,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr)
extern "C" void ILGenerator_System_Runtime_InteropServices__ILGenerator_Invoke_m1_5970 (ILGenerator_t1_480 * __this, uint32_t ___dispIdMember, Guid_t1_319 * ___riid, uint32_t ___lcid, int16_t ___wFlags, IntPtr_t ___pDispParams, IntPtr_t ___pVarResult, IntPtr_t ___pExcepInfo, IntPtr_t ___puArgErr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ILGenerator::add_token_fixup(System.Reflection.MemberInfo)
extern "C" void ILGenerator_add_token_fixup_m1_5971 (ILGenerator_t1_480 * __this, MemberInfo_t * ___mi, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ILGenerator::make_room(System.Int32)
extern "C" void ILGenerator_make_room_m1_5972 (ILGenerator_t1_480 * __this, int32_t ___nbytes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ILGenerator::emit_int(System.Int32)
extern "C" void ILGenerator_emit_int_m1_5973 (ILGenerator_t1_480 * __this, int32_t ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ILGenerator::ll_emit(System.Reflection.Emit.OpCode)
extern "C" void ILGenerator_ll_emit_m1_5974 (ILGenerator_t1_480 * __this, OpCode_t1_538  ___opcode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.ILGenerator::target_len(System.Reflection.Emit.OpCode)
extern "C" int32_t ILGenerator_target_len_m1_5975 (Object_t * __this /* static, unused */, OpCode_t1_538  ___opcode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ILGenerator::InternalEndClause()
extern "C" void ILGenerator_InternalEndClause_m1_5976 (ILGenerator_t1_480 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ILGenerator::BeginCatchBlock(System.Type)
extern "C" void ILGenerator_BeginCatchBlock_m1_5977 (ILGenerator_t1_480 * __this, Type_t * ___exceptionType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ILGenerator::BeginExceptFilterBlock()
extern "C" void ILGenerator_BeginExceptFilterBlock_m1_5978 (ILGenerator_t1_480 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.Label System.Reflection.Emit.ILGenerator::BeginExceptionBlock()
extern "C" Label_t1_513  ILGenerator_BeginExceptionBlock_m1_5979 (ILGenerator_t1_480 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ILGenerator::BeginFaultBlock()
extern "C" void ILGenerator_BeginFaultBlock_m1_5980 (ILGenerator_t1_480 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ILGenerator::BeginFinallyBlock()
extern "C" void ILGenerator_BeginFinallyBlock_m1_5981 (ILGenerator_t1_480 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ILGenerator::BeginScope()
extern "C" void ILGenerator_BeginScope_m1_5982 (ILGenerator_t1_480 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.LocalBuilder System.Reflection.Emit.ILGenerator::DeclareLocal(System.Type)
extern "C" LocalBuilder_t1_527 * ILGenerator_DeclareLocal_m1_5983 (ILGenerator_t1_480 * __this, Type_t * ___localType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.LocalBuilder System.Reflection.Emit.ILGenerator::DeclareLocal(System.Type,System.Boolean)
extern "C" LocalBuilder_t1_527 * ILGenerator_DeclareLocal_m1_5984 (ILGenerator_t1_480 * __this, Type_t * ___localType, bool ___pinned, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.Label System.Reflection.Emit.ILGenerator::DefineLabel()
extern "C" Label_t1_513  ILGenerator_DefineLabel_m1_5985 (ILGenerator_t1_480 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ILGenerator::Emit(System.Reflection.Emit.OpCode)
extern "C" void ILGenerator_Emit_m1_5986 (ILGenerator_t1_480 * __this, OpCode_t1_538  ___opcode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ILGenerator::Emit(System.Reflection.Emit.OpCode,System.Byte)
extern "C" void ILGenerator_Emit_m1_5987 (ILGenerator_t1_480 * __this, OpCode_t1_538  ___opcode, uint8_t ___arg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ILGenerator::Emit(System.Reflection.Emit.OpCode,System.Reflection.ConstructorInfo)
extern "C" void ILGenerator_Emit_m1_5988 (ILGenerator_t1_480 * __this, OpCode_t1_538  ___opcode, ConstructorInfo_t1_478 * ___con, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ILGenerator::Emit(System.Reflection.Emit.OpCode,System.Double)
extern "C" void ILGenerator_Emit_m1_5989 (ILGenerator_t1_480 * __this, OpCode_t1_538  ___opcode, double ___arg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ILGenerator::Emit(System.Reflection.Emit.OpCode,System.Reflection.FieldInfo)
extern "C" void ILGenerator_Emit_m1_5990 (ILGenerator_t1_480 * __this, OpCode_t1_538  ___opcode, FieldInfo_t * ___field, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ILGenerator::Emit(System.Reflection.Emit.OpCode,System.Int16)
extern "C" void ILGenerator_Emit_m1_5991 (ILGenerator_t1_480 * __this, OpCode_t1_538  ___opcode, int16_t ___arg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ILGenerator::Emit(System.Reflection.Emit.OpCode,System.Int32)
extern "C" void ILGenerator_Emit_m1_5992 (ILGenerator_t1_480 * __this, OpCode_t1_538  ___opcode, int32_t ___arg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ILGenerator::Emit(System.Reflection.Emit.OpCode,System.Int64)
extern "C" void ILGenerator_Emit_m1_5993 (ILGenerator_t1_480 * __this, OpCode_t1_538  ___opcode, int64_t ___arg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ILGenerator::Emit(System.Reflection.Emit.OpCode,System.Reflection.Emit.Label)
extern "C" void ILGenerator_Emit_m1_5994 (ILGenerator_t1_480 * __this, OpCode_t1_538  ___opcode, Label_t1_513  ___label, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ILGenerator::Emit(System.Reflection.Emit.OpCode,System.Reflection.Emit.Label[])
extern "C" void ILGenerator_Emit_m1_5995 (ILGenerator_t1_480 * __this, OpCode_t1_538  ___opcode, LabelU5BU5D_t1_1687* ___labels, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ILGenerator::Emit(System.Reflection.Emit.OpCode,System.Reflection.Emit.LocalBuilder)
extern "C" void ILGenerator_Emit_m1_5996 (ILGenerator_t1_480 * __this, OpCode_t1_538  ___opcode, LocalBuilder_t1_527 * ___local, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ILGenerator::Emit(System.Reflection.Emit.OpCode,System.Reflection.MethodInfo)
extern "C" void ILGenerator_Emit_m1_5997 (ILGenerator_t1_480 * __this, OpCode_t1_538  ___opcode, MethodInfo_t * ___meth, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ILGenerator::Emit(System.Reflection.Emit.OpCode,System.Reflection.MethodInfo,System.Int32)
extern "C" void ILGenerator_Emit_m1_5998 (ILGenerator_t1_480 * __this, OpCode_t1_538  ___opcode, MethodInfo_t * ___method, int32_t ___token, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ILGenerator::Emit(System.Reflection.Emit.OpCode,System.SByte)
extern "C" void ILGenerator_Emit_m1_5999 (ILGenerator_t1_480 * __this, OpCode_t1_538  ___opcode, int8_t ___arg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ILGenerator::Emit(System.Reflection.Emit.OpCode,System.Reflection.Emit.SignatureHelper)
extern "C" void ILGenerator_Emit_m1_6000 (ILGenerator_t1_480 * __this, OpCode_t1_538  ___opcode, SignatureHelper_t1_551 * ___signature, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ILGenerator::Emit(System.Reflection.Emit.OpCode,System.Single)
extern "C" void ILGenerator_Emit_m1_6001 (ILGenerator_t1_480 * __this, OpCode_t1_538  ___opcode, float ___arg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ILGenerator::Emit(System.Reflection.Emit.OpCode,System.String)
extern "C" void ILGenerator_Emit_m1_6002 (ILGenerator_t1_480 * __this, OpCode_t1_538  ___opcode, String_t* ___str, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ILGenerator::Emit(System.Reflection.Emit.OpCode,System.Type)
extern "C" void ILGenerator_Emit_m1_6003 (ILGenerator_t1_480 * __this, OpCode_t1_538  ___opcode, Type_t * ___cls, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ILGenerator::EmitCall(System.Reflection.Emit.OpCode,System.Reflection.MethodInfo,System.Type[])
extern "C" void ILGenerator_EmitCall_m1_6004 (ILGenerator_t1_480 * __this, OpCode_t1_538  ___opcode, MethodInfo_t * ___methodInfo, TypeU5BU5D_t1_31* ___optionalParameterTypes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ILGenerator::EmitCalli(System.Reflection.Emit.OpCode,System.Runtime.InteropServices.CallingConvention,System.Type,System.Type[])
extern "C" void ILGenerator_EmitCalli_m1_6005 (ILGenerator_t1_480 * __this, OpCode_t1_538  ___opcode, int32_t ___unmanagedCallConv, Type_t * ___returnType, TypeU5BU5D_t1_31* ___parameterTypes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ILGenerator::EmitCalli(System.Reflection.Emit.OpCode,System.Reflection.CallingConventions,System.Type,System.Type[],System.Type[])
extern "C" void ILGenerator_EmitCalli_m1_6006 (ILGenerator_t1_480 * __this, OpCode_t1_538  ___opcode, int32_t ___callingConvention, Type_t * ___returnType, TypeU5BU5D_t1_31* ___parameterTypes, TypeU5BU5D_t1_31* ___optionalParameterTypes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ILGenerator::EmitWriteLine(System.Reflection.FieldInfo)
extern "C" void ILGenerator_EmitWriteLine_m1_6007 (ILGenerator_t1_480 * __this, FieldInfo_t * ___fld, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ILGenerator::EmitWriteLine(System.Reflection.Emit.LocalBuilder)
extern "C" void ILGenerator_EmitWriteLine_m1_6008 (ILGenerator_t1_480 * __this, LocalBuilder_t1_527 * ___localBuilder, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ILGenerator::EmitWriteLine(System.String)
extern "C" void ILGenerator_EmitWriteLine_m1_6009 (ILGenerator_t1_480 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ILGenerator::EndExceptionBlock()
extern "C" void ILGenerator_EndExceptionBlock_m1_6010 (ILGenerator_t1_480 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ILGenerator::EndScope()
extern "C" void ILGenerator_EndScope_m1_6011 (ILGenerator_t1_480 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ILGenerator::MarkLabel(System.Reflection.Emit.Label)
extern "C" void ILGenerator_MarkLabel_m1_6012 (ILGenerator_t1_480 * __this, Label_t1_513  ___loc, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ILGenerator::MarkSequencePoint(System.Diagnostics.SymbolStore.ISymbolDocumentWriter,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" void ILGenerator_MarkSequencePoint_m1_6013 (ILGenerator_t1_480 * __this, Object_t * ___document, int32_t ___startLine, int32_t ___startColumn, int32_t ___endLine, int32_t ___endColumn, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ILGenerator::GenerateDebugInfo(System.Diagnostics.SymbolStore.ISymbolWriter)
extern "C" void ILGenerator_GenerateDebugInfo_m1_6014 (ILGenerator_t1_480 * __this, Object_t * ___symbolWriter, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.ILGenerator::get_HasDebugInfo()
extern "C" bool ILGenerator_get_HasDebugInfo_m1_6015 (ILGenerator_t1_480 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ILGenerator::ThrowException(System.Type)
extern "C" void ILGenerator_ThrowException_m1_6016 (ILGenerator_t1_480 * __this, Type_t * ___excType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ILGenerator::UsingNamespace(System.String)
extern "C" void ILGenerator_UsingNamespace_m1_6017 (ILGenerator_t1_480 * __this, String_t* ___usingNamespace, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ILGenerator::label_fixup()
extern "C" void ILGenerator_label_fixup_m1_6018 (ILGenerator_t1_480 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.ILGenerator::Mono_GetCurrentOffset(System.Reflection.Emit.ILGenerator)
extern "C" int32_t ILGenerator_Mono_GetCurrentOffset_m1_6019 (Object_t * __this /* static, unused */, ILGenerator_t1_480 * ___ig, const MethodInfo* method) IL2CPP_METHOD_ATTR;
