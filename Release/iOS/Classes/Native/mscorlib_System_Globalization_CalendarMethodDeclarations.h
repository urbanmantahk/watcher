﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Globalization.Calendar
struct Calendar_t1_338;
// System.String
struct String_t;
// System.Object
struct Object_t;
// System.String[]
struct StringU5BU5D_t1_238;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Globalization_CalendarAlgorithmType.h"
#include "mscorlib_System_DateTime.h"
#include "mscorlib_System_Globalization_CalendarWeekRule.h"
#include "mscorlib_System_DayOfWeek.h"

// System.Void System.Globalization.Calendar::.ctor()
extern "C" void Calendar__ctor_m1_3655 (Calendar_t1_338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.Calendar::get_M_DaysInWeek()
extern "C" int32_t Calendar_get_M_DaysInWeek_m1_3656 (Calendar_t1_338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.Calendar::M_ValidValues(System.Object,System.Object)
extern "C" String_t* Calendar_M_ValidValues_m1_3657 (Calendar_t1_338 * __this, Object_t * ___a, Object_t * ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.Calendar::M_ArgumentInRange(System.String,System.Int32,System.Int32,System.Int32)
extern "C" void Calendar_M_ArgumentInRange_m1_3658 (Calendar_t1_338 * __this, String_t* ___param, int32_t ___arg, int32_t ___a, int32_t ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.Calendar::M_CheckHMSM(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" void Calendar_M_CheckHMSM_m1_3659 (Calendar_t1_338 * __this, int32_t ___hour, int32_t ___minute, int32_t ___second, int32_t ___milliseconds, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.CalendarAlgorithmType System.Globalization.Calendar::get_AlgorithmType()
extern "C" int32_t Calendar_get_AlgorithmType_m1_3660 (Calendar_t1_338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.Calendar::get_MaxSupportedDateTime()
extern "C" DateTime_t1_150  Calendar_get_MaxSupportedDateTime_m1_3661 (Calendar_t1_338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.Calendar::get_MinSupportedDateTime()
extern "C" DateTime_t1_150  Calendar_get_MinSupportedDateTime_m1_3662 (Calendar_t1_338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Globalization.Calendar::Clone()
extern "C" Object_t * Calendar_Clone_m1_3663 (Calendar_t1_338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.Calendar::GetLeapMonth(System.Int32)
extern "C" int32_t Calendar_GetLeapMonth_m1_3664 (Calendar_t1_338 * __this, int32_t ___year, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.Calendar::GetLeapMonth(System.Int32,System.Int32)
extern "C" int32_t Calendar_GetLeapMonth_m1_3665 (Calendar_t1_338 * __this, int32_t ___year, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.Calendar::get_IsReadOnly()
extern "C" bool Calendar_get_IsReadOnly_m1_3666 (Calendar_t1_338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.Calendar System.Globalization.Calendar::ReadOnly(System.Globalization.Calendar)
extern "C" Calendar_t1_338 * Calendar_ReadOnly_m1_3667 (Object_t * __this /* static, unused */, Calendar_t1_338 * ___calendar, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.Calendar::CheckReadOnly()
extern "C" void Calendar_CheckReadOnly_m1_3668 (Calendar_t1_338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.Calendar::get_M_MaxYear()
extern "C" int32_t Calendar_get_M_MaxYear_m1_3669 (Calendar_t1_338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.Calendar::M_CheckYE(System.Int32,System.Int32&)
extern "C" void Calendar_M_CheckYE_m1_3670 (Calendar_t1_338 * __this, int32_t ___year, int32_t* ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.Calendar::get_TwoDigitYearMax()
extern "C" int32_t Calendar_get_TwoDigitYearMax_m1_3671 (Calendar_t1_338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.Calendar::set_TwoDigitYearMax(System.Int32)
extern "C" void Calendar_set_TwoDigitYearMax_m1_3672 (Calendar_t1_338 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.Calendar::AddDays(System.DateTime,System.Int32)
extern "C" DateTime_t1_150  Calendar_AddDays_m1_3673 (Calendar_t1_338 * __this, DateTime_t1_150  ___time, int32_t ___days, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.Calendar::AddHours(System.DateTime,System.Int32)
extern "C" DateTime_t1_150  Calendar_AddHours_m1_3674 (Calendar_t1_338 * __this, DateTime_t1_150  ___time, int32_t ___hours, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.Calendar::AddMilliseconds(System.DateTime,System.Double)
extern "C" DateTime_t1_150  Calendar_AddMilliseconds_m1_3675 (Calendar_t1_338 * __this, DateTime_t1_150  ___time, double ___milliseconds, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.Calendar::AddMinutes(System.DateTime,System.Int32)
extern "C" DateTime_t1_150  Calendar_AddMinutes_m1_3676 (Calendar_t1_338 * __this, DateTime_t1_150  ___time, int32_t ___minutes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.Calendar::AddSeconds(System.DateTime,System.Int32)
extern "C" DateTime_t1_150  Calendar_AddSeconds_m1_3677 (Calendar_t1_338 * __this, DateTime_t1_150  ___time, int32_t ___seconds, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.Calendar::AddWeeks(System.DateTime,System.Int32)
extern "C" DateTime_t1_150  Calendar_AddWeeks_m1_3678 (Calendar_t1_338 * __this, DateTime_t1_150  ___time, int32_t ___weeks, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.Calendar::GetDaysInMonth(System.Int32,System.Int32)
extern "C" int32_t Calendar_GetDaysInMonth_m1_3679 (Calendar_t1_338 * __this, int32_t ___year, int32_t ___month, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.Calendar::GetDaysInYear(System.Int32)
extern "C" int32_t Calendar_GetDaysInYear_m1_3680 (Calendar_t1_338 * __this, int32_t ___year, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.Calendar::GetHour(System.DateTime)
extern "C" int32_t Calendar_GetHour_m1_3681 (Calendar_t1_338 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Globalization.Calendar::GetMilliseconds(System.DateTime)
extern "C" double Calendar_GetMilliseconds_m1_3682 (Calendar_t1_338 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.Calendar::GetMinute(System.DateTime)
extern "C" int32_t Calendar_GetMinute_m1_3683 (Calendar_t1_338 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.Calendar::GetMonthsInYear(System.Int32)
extern "C" int32_t Calendar_GetMonthsInYear_m1_3684 (Calendar_t1_338 * __this, int32_t ___year, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.Calendar::GetSecond(System.DateTime)
extern "C" int32_t Calendar_GetSecond_m1_3685 (Calendar_t1_338 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.Calendar::M_DiffDays(System.DateTime,System.DateTime)
extern "C" int32_t Calendar_M_DiffDays_m1_3686 (Calendar_t1_338 * __this, DateTime_t1_150  ___timeA, DateTime_t1_150  ___timeB, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.Calendar::M_GetFirstDayOfSecondWeekOfYear(System.Int32,System.Globalization.CalendarWeekRule,System.DayOfWeek)
extern "C" DateTime_t1_150  Calendar_M_GetFirstDayOfSecondWeekOfYear_m1_3687 (Calendar_t1_338 * __this, int32_t ___year, int32_t ___rule, int32_t ___firstDayOfWeek, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.Calendar::GetWeekOfYear(System.DateTime,System.Globalization.CalendarWeekRule,System.DayOfWeek)
extern "C" int32_t Calendar_GetWeekOfYear_m1_3688 (Calendar_t1_338 * __this, DateTime_t1_150  ___time, int32_t ___rule, int32_t ___firstDayOfWeek, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.Calendar::IsLeapDay(System.Int32,System.Int32,System.Int32)
extern "C" bool Calendar_IsLeapDay_m1_3689 (Calendar_t1_338 * __this, int32_t ___year, int32_t ___month, int32_t ___day, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.Calendar::IsLeapMonth(System.Int32,System.Int32)
extern "C" bool Calendar_IsLeapMonth_m1_3690 (Calendar_t1_338 * __this, int32_t ___year, int32_t ___month, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.Calendar::IsLeapYear(System.Int32)
extern "C" bool Calendar_IsLeapYear_m1_3691 (Calendar_t1_338 * __this, int32_t ___year, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.Calendar::ToDateTime(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" DateTime_t1_150  Calendar_ToDateTime_m1_3692 (Calendar_t1_338 * __this, int32_t ___year, int32_t ___month, int32_t ___day, int32_t ___hour, int32_t ___minute, int32_t ___second, int32_t ___millisecond, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.Calendar::ToFourDigitYear(System.Int32)
extern "C" int32_t Calendar_ToFourDigitYear_m1_3693 (Calendar_t1_338 * __this, int32_t ___year, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.Globalization.Calendar::get_AbbreviatedEraNames()
extern "C" StringU5BU5D_t1_238* Calendar_get_AbbreviatedEraNames_m1_3694 (Calendar_t1_338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.Calendar::set_AbbreviatedEraNames(System.String[])
extern "C" void Calendar_set_AbbreviatedEraNames_m1_3695 (Calendar_t1_338 * __this, StringU5BU5D_t1_238* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.Globalization.Calendar::get_EraNames()
extern "C" StringU5BU5D_t1_238* Calendar_get_EraNames_m1_3696 (Calendar_t1_338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.Calendar::set_EraNames(System.String[])
extern "C" void Calendar_set_EraNames_m1_3697 (Calendar_t1_338 * __this, StringU5BU5D_t1_238* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
