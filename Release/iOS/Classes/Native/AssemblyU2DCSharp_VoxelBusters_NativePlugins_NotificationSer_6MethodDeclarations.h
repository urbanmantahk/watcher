﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.NotificationServiceSettings
struct NotificationServiceSettings_t8_270;
// VoxelBusters.NativePlugins.NotificationServiceSettings/iOSSettings
struct iOSSettings_t8_268;
// VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings
struct AndroidSettings_t8_269;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.NotificationServiceSettings::.ctor()
extern "C" void NotificationServiceSettings__ctor_m8_1563 (NotificationServiceSettings_t8_270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.NativePlugins.NotificationServiceSettings/iOSSettings VoxelBusters.NativePlugins.NotificationServiceSettings::get_iOS()
extern "C" iOSSettings_t8_268 * NotificationServiceSettings_get_iOS_m8_1564 (NotificationServiceSettings_t8_270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationServiceSettings::set_iOS(VoxelBusters.NativePlugins.NotificationServiceSettings/iOSSettings)
extern "C" void NotificationServiceSettings_set_iOS_m8_1565 (NotificationServiceSettings_t8_270 * __this, iOSSettings_t8_268 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings VoxelBusters.NativePlugins.NotificationServiceSettings::get_Android()
extern "C" AndroidSettings_t8_269 * NotificationServiceSettings_get_Android_m8_1566 (NotificationServiceSettings_t8_270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationServiceSettings::set_Android(VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings)
extern "C" void NotificationServiceSettings_set_Android_m8_1567 (NotificationServiceSettings_t8_270 * __this, AndroidSettings_t8_269 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
