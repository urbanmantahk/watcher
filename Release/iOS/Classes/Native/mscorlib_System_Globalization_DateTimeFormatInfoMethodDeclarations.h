﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Globalization.DateTimeFormatInfo
struct DateTimeFormatInfo_t1_362;
// System.IFormatProvider
struct IFormatProvider_t1_455;
// System.Object
struct Object_t;
// System.Type
struct Type_t;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1_238;
// System.Globalization.Calendar
struct Calendar_t1_338;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DayOfWeek.h"
#include "mscorlib_System_Globalization_CalendarWeekRule.h"

// System.Void System.Globalization.DateTimeFormatInfo::.ctor(System.Boolean)
extern "C" void DateTimeFormatInfo__ctor_m1_3972 (DateTimeFormatInfo_t1_362 * __this, bool ___read_only, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.DateTimeFormatInfo::.ctor()
extern "C" void DateTimeFormatInfo__ctor_m1_3973 (DateTimeFormatInfo_t1_362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.DateTimeFormatInfo::.cctor()
extern "C" void DateTimeFormatInfo__cctor_m1_3974 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.DateTimeFormatInfo System.Globalization.DateTimeFormatInfo::GetInstance(System.IFormatProvider)
extern "C" DateTimeFormatInfo_t1_362 * DateTimeFormatInfo_GetInstance_m1_3975 (Object_t * __this /* static, unused */, Object_t * ___provider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.DateTimeFormatInfo::get_IsReadOnly()
extern "C" bool DateTimeFormatInfo_get_IsReadOnly_m1_3976 (DateTimeFormatInfo_t1_362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.DateTimeFormatInfo System.Globalization.DateTimeFormatInfo::ReadOnly(System.Globalization.DateTimeFormatInfo)
extern "C" DateTimeFormatInfo_t1_362 * DateTimeFormatInfo_ReadOnly_m1_3977 (Object_t * __this /* static, unused */, DateTimeFormatInfo_t1_362 * ___dtfi, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Globalization.DateTimeFormatInfo::Clone()
extern "C" Object_t * DateTimeFormatInfo_Clone_m1_3978 (DateTimeFormatInfo_t1_362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Globalization.DateTimeFormatInfo::GetFormat(System.Type)
extern "C" Object_t * DateTimeFormatInfo_GetFormat_m1_3979 (DateTimeFormatInfo_t1_362 * __this, Type_t * ___formatType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.DateTimeFormatInfo::GetAbbreviatedEraName(System.Int32)
extern "C" String_t* DateTimeFormatInfo_GetAbbreviatedEraName_m1_3980 (DateTimeFormatInfo_t1_362 * __this, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.DateTimeFormatInfo::GetAbbreviatedMonthName(System.Int32)
extern "C" String_t* DateTimeFormatInfo_GetAbbreviatedMonthName_m1_3981 (DateTimeFormatInfo_t1_362 * __this, int32_t ___month, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.DateTimeFormatInfo::GetEra(System.String)
extern "C" int32_t DateTimeFormatInfo_GetEra_m1_3982 (DateTimeFormatInfo_t1_362 * __this, String_t* ___eraName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.DateTimeFormatInfo::GetEraName(System.Int32)
extern "C" String_t* DateTimeFormatInfo_GetEraName_m1_3983 (DateTimeFormatInfo_t1_362 * __this, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.DateTimeFormatInfo::GetMonthName(System.Int32)
extern "C" String_t* DateTimeFormatInfo_GetMonthName_m1_3984 (DateTimeFormatInfo_t1_362 * __this, int32_t ___month, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.Globalization.DateTimeFormatInfo::get_AbbreviatedDayNames()
extern "C" StringU5BU5D_t1_238* DateTimeFormatInfo_get_AbbreviatedDayNames_m1_3985 (DateTimeFormatInfo_t1_362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.DateTimeFormatInfo::set_AbbreviatedDayNames(System.String[])
extern "C" void DateTimeFormatInfo_set_AbbreviatedDayNames_m1_3986 (DateTimeFormatInfo_t1_362 * __this, StringU5BU5D_t1_238* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.Globalization.DateTimeFormatInfo::get_RawAbbreviatedDayNames()
extern "C" StringU5BU5D_t1_238* DateTimeFormatInfo_get_RawAbbreviatedDayNames_m1_3987 (DateTimeFormatInfo_t1_362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.DateTimeFormatInfo::set_RawAbbreviatedDayNames(System.String[])
extern "C" void DateTimeFormatInfo_set_RawAbbreviatedDayNames_m1_3988 (DateTimeFormatInfo_t1_362 * __this, StringU5BU5D_t1_238* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.Globalization.DateTimeFormatInfo::get_AbbreviatedMonthNames()
extern "C" StringU5BU5D_t1_238* DateTimeFormatInfo_get_AbbreviatedMonthNames_m1_3989 (DateTimeFormatInfo_t1_362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.DateTimeFormatInfo::set_AbbreviatedMonthNames(System.String[])
extern "C" void DateTimeFormatInfo_set_AbbreviatedMonthNames_m1_3990 (DateTimeFormatInfo_t1_362 * __this, StringU5BU5D_t1_238* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.Globalization.DateTimeFormatInfo::get_RawAbbreviatedMonthNames()
extern "C" StringU5BU5D_t1_238* DateTimeFormatInfo_get_RawAbbreviatedMonthNames_m1_3991 (DateTimeFormatInfo_t1_362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.DateTimeFormatInfo::set_RawAbbreviatedMonthNames(System.String[])
extern "C" void DateTimeFormatInfo_set_RawAbbreviatedMonthNames_m1_3992 (DateTimeFormatInfo_t1_362 * __this, StringU5BU5D_t1_238* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.Globalization.DateTimeFormatInfo::get_DayNames()
extern "C" StringU5BU5D_t1_238* DateTimeFormatInfo_get_DayNames_m1_3993 (DateTimeFormatInfo_t1_362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.DateTimeFormatInfo::set_DayNames(System.String[])
extern "C" void DateTimeFormatInfo_set_DayNames_m1_3994 (DateTimeFormatInfo_t1_362 * __this, StringU5BU5D_t1_238* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.Globalization.DateTimeFormatInfo::get_RawDayNames()
extern "C" StringU5BU5D_t1_238* DateTimeFormatInfo_get_RawDayNames_m1_3995 (DateTimeFormatInfo_t1_362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.DateTimeFormatInfo::set_RawDayNames(System.String[])
extern "C" void DateTimeFormatInfo_set_RawDayNames_m1_3996 (DateTimeFormatInfo_t1_362 * __this, StringU5BU5D_t1_238* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.Globalization.DateTimeFormatInfo::get_MonthNames()
extern "C" StringU5BU5D_t1_238* DateTimeFormatInfo_get_MonthNames_m1_3997 (DateTimeFormatInfo_t1_362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.DateTimeFormatInfo::set_MonthNames(System.String[])
extern "C" void DateTimeFormatInfo_set_MonthNames_m1_3998 (DateTimeFormatInfo_t1_362 * __this, StringU5BU5D_t1_238* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.Globalization.DateTimeFormatInfo::get_RawMonthNames()
extern "C" StringU5BU5D_t1_238* DateTimeFormatInfo_get_RawMonthNames_m1_3999 (DateTimeFormatInfo_t1_362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.DateTimeFormatInfo::set_RawMonthNames(System.String[])
extern "C" void DateTimeFormatInfo_set_RawMonthNames_m1_4000 (DateTimeFormatInfo_t1_362 * __this, StringU5BU5D_t1_238* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.DateTimeFormatInfo::get_AMDesignator()
extern "C" String_t* DateTimeFormatInfo_get_AMDesignator_m1_4001 (DateTimeFormatInfo_t1_362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.DateTimeFormatInfo::set_AMDesignator(System.String)
extern "C" void DateTimeFormatInfo_set_AMDesignator_m1_4002 (DateTimeFormatInfo_t1_362 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.DateTimeFormatInfo::get_PMDesignator()
extern "C" String_t* DateTimeFormatInfo_get_PMDesignator_m1_4003 (DateTimeFormatInfo_t1_362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.DateTimeFormatInfo::set_PMDesignator(System.String)
extern "C" void DateTimeFormatInfo_set_PMDesignator_m1_4004 (DateTimeFormatInfo_t1_362 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.DateTimeFormatInfo::get_DateSeparator()
extern "C" String_t* DateTimeFormatInfo_get_DateSeparator_m1_4005 (DateTimeFormatInfo_t1_362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.DateTimeFormatInfo::set_DateSeparator(System.String)
extern "C" void DateTimeFormatInfo_set_DateSeparator_m1_4006 (DateTimeFormatInfo_t1_362 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.DateTimeFormatInfo::get_TimeSeparator()
extern "C" String_t* DateTimeFormatInfo_get_TimeSeparator_m1_4007 (DateTimeFormatInfo_t1_362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.DateTimeFormatInfo::set_TimeSeparator(System.String)
extern "C" void DateTimeFormatInfo_set_TimeSeparator_m1_4008 (DateTimeFormatInfo_t1_362 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.DateTimeFormatInfo::get_LongDatePattern()
extern "C" String_t* DateTimeFormatInfo_get_LongDatePattern_m1_4009 (DateTimeFormatInfo_t1_362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.DateTimeFormatInfo::set_LongDatePattern(System.String)
extern "C" void DateTimeFormatInfo_set_LongDatePattern_m1_4010 (DateTimeFormatInfo_t1_362 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.DateTimeFormatInfo::get_ShortDatePattern()
extern "C" String_t* DateTimeFormatInfo_get_ShortDatePattern_m1_4011 (DateTimeFormatInfo_t1_362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.DateTimeFormatInfo::set_ShortDatePattern(System.String)
extern "C" void DateTimeFormatInfo_set_ShortDatePattern_m1_4012 (DateTimeFormatInfo_t1_362 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.DateTimeFormatInfo::get_ShortTimePattern()
extern "C" String_t* DateTimeFormatInfo_get_ShortTimePattern_m1_4013 (DateTimeFormatInfo_t1_362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.DateTimeFormatInfo::set_ShortTimePattern(System.String)
extern "C" void DateTimeFormatInfo_set_ShortTimePattern_m1_4014 (DateTimeFormatInfo_t1_362 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.DateTimeFormatInfo::get_LongTimePattern()
extern "C" String_t* DateTimeFormatInfo_get_LongTimePattern_m1_4015 (DateTimeFormatInfo_t1_362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.DateTimeFormatInfo::set_LongTimePattern(System.String)
extern "C" void DateTimeFormatInfo_set_LongTimePattern_m1_4016 (DateTimeFormatInfo_t1_362 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.DateTimeFormatInfo::get_MonthDayPattern()
extern "C" String_t* DateTimeFormatInfo_get_MonthDayPattern_m1_4017 (DateTimeFormatInfo_t1_362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.DateTimeFormatInfo::set_MonthDayPattern(System.String)
extern "C" void DateTimeFormatInfo_set_MonthDayPattern_m1_4018 (DateTimeFormatInfo_t1_362 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.DateTimeFormatInfo::get_YearMonthPattern()
extern "C" String_t* DateTimeFormatInfo_get_YearMonthPattern_m1_4019 (DateTimeFormatInfo_t1_362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.DateTimeFormatInfo::set_YearMonthPattern(System.String)
extern "C" void DateTimeFormatInfo_set_YearMonthPattern_m1_4020 (DateTimeFormatInfo_t1_362 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.DateTimeFormatInfo::get_FullDateTimePattern()
extern "C" String_t* DateTimeFormatInfo_get_FullDateTimePattern_m1_4021 (DateTimeFormatInfo_t1_362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.DateTimeFormatInfo::set_FullDateTimePattern(System.String)
extern "C" void DateTimeFormatInfo_set_FullDateTimePattern_m1_4022 (DateTimeFormatInfo_t1_362 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.DateTimeFormatInfo System.Globalization.DateTimeFormatInfo::get_CurrentInfo()
extern "C" DateTimeFormatInfo_t1_362 * DateTimeFormatInfo_get_CurrentInfo_m1_4023 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.DateTimeFormatInfo System.Globalization.DateTimeFormatInfo::get_InvariantInfo()
extern "C" DateTimeFormatInfo_t1_362 * DateTimeFormatInfo_get_InvariantInfo_m1_4024 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DayOfWeek System.Globalization.DateTimeFormatInfo::get_FirstDayOfWeek()
extern "C" int32_t DateTimeFormatInfo_get_FirstDayOfWeek_m1_4025 (DateTimeFormatInfo_t1_362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.DateTimeFormatInfo::set_FirstDayOfWeek(System.DayOfWeek)
extern "C" void DateTimeFormatInfo_set_FirstDayOfWeek_m1_4026 (DateTimeFormatInfo_t1_362 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.Calendar System.Globalization.DateTimeFormatInfo::get_Calendar()
extern "C" Calendar_t1_338 * DateTimeFormatInfo_get_Calendar_m1_4027 (DateTimeFormatInfo_t1_362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.DateTimeFormatInfo::set_Calendar(System.Globalization.Calendar)
extern "C" void DateTimeFormatInfo_set_Calendar_m1_4028 (DateTimeFormatInfo_t1_362 * __this, Calendar_t1_338 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.CalendarWeekRule System.Globalization.DateTimeFormatInfo::get_CalendarWeekRule()
extern "C" int32_t DateTimeFormatInfo_get_CalendarWeekRule_m1_4029 (DateTimeFormatInfo_t1_362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.DateTimeFormatInfo::set_CalendarWeekRule(System.Globalization.CalendarWeekRule)
extern "C" void DateTimeFormatInfo_set_CalendarWeekRule_m1_4030 (DateTimeFormatInfo_t1_362 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.DateTimeFormatInfo::get_RFC1123Pattern()
extern "C" String_t* DateTimeFormatInfo_get_RFC1123Pattern_m1_4031 (DateTimeFormatInfo_t1_362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.DateTimeFormatInfo::get_RoundtripPattern()
extern "C" String_t* DateTimeFormatInfo_get_RoundtripPattern_m1_4032 (DateTimeFormatInfo_t1_362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.DateTimeFormatInfo::get_SortableDateTimePattern()
extern "C" String_t* DateTimeFormatInfo_get_SortableDateTimePattern_m1_4033 (DateTimeFormatInfo_t1_362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.DateTimeFormatInfo::get_UniversalSortableDateTimePattern()
extern "C" String_t* DateTimeFormatInfo_get_UniversalSortableDateTimePattern_m1_4034 (DateTimeFormatInfo_t1_362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.Globalization.DateTimeFormatInfo::GetAllDateTimePatterns()
extern "C" StringU5BU5D_t1_238* DateTimeFormatInfo_GetAllDateTimePatterns_m1_4035 (DateTimeFormatInfo_t1_362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.Globalization.DateTimeFormatInfo::GetAllDateTimePatternsInternal()
extern "C" StringU5BU5D_t1_238* DateTimeFormatInfo_GetAllDateTimePatternsInternal_m1_4036 (DateTimeFormatInfo_t1_362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.DateTimeFormatInfo::FillAllDateTimePatterns()
extern "C" void DateTimeFormatInfo_FillAllDateTimePatterns_m1_4037 (DateTimeFormatInfo_t1_362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.Globalization.DateTimeFormatInfo::GetAllDateTimePatterns(System.Char)
extern "C" StringU5BU5D_t1_238* DateTimeFormatInfo_GetAllDateTimePatterns_m1_4038 (DateTimeFormatInfo_t1_362 * __this, uint16_t ___format, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.Globalization.DateTimeFormatInfo::GetAllRawDateTimePatterns(System.Char)
extern "C" StringU5BU5D_t1_238* DateTimeFormatInfo_GetAllRawDateTimePatterns_m1_4039 (DateTimeFormatInfo_t1_362 * __this, uint16_t ___format, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.DateTimeFormatInfo::GetDayName(System.DayOfWeek)
extern "C" String_t* DateTimeFormatInfo_GetDayName_m1_4040 (DateTimeFormatInfo_t1_362 * __this, int32_t ___dayofweek, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.DateTimeFormatInfo::GetAbbreviatedDayName(System.DayOfWeek)
extern "C" String_t* DateTimeFormatInfo_GetAbbreviatedDayName_m1_4041 (DateTimeFormatInfo_t1_362 * __this, int32_t ___dayofweek, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.DateTimeFormatInfo::FillInvariantPatterns()
extern "C" void DateTimeFormatInfo_FillInvariantPatterns_m1_4042 (DateTimeFormatInfo_t1_362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.Globalization.DateTimeFormatInfo::PopulateCombinedList(System.String[],System.String[])
extern "C" StringU5BU5D_t1_238* DateTimeFormatInfo_PopulateCombinedList_m1_4043 (DateTimeFormatInfo_t1_362 * __this, StringU5BU5D_t1_238* ___dates, StringU5BU5D_t1_238* ___times, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.Globalization.DateTimeFormatInfo::get_AbbreviatedMonthGenitiveNames()
extern "C" StringU5BU5D_t1_238* DateTimeFormatInfo_get_AbbreviatedMonthGenitiveNames_m1_4044 (DateTimeFormatInfo_t1_362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.DateTimeFormatInfo::set_AbbreviatedMonthGenitiveNames(System.String[])
extern "C" void DateTimeFormatInfo_set_AbbreviatedMonthGenitiveNames_m1_4045 (DateTimeFormatInfo_t1_362 * __this, StringU5BU5D_t1_238* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.Globalization.DateTimeFormatInfo::get_MonthGenitiveNames()
extern "C" StringU5BU5D_t1_238* DateTimeFormatInfo_get_MonthGenitiveNames_m1_4046 (DateTimeFormatInfo_t1_362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.DateTimeFormatInfo::set_MonthGenitiveNames(System.String[])
extern "C" void DateTimeFormatInfo_set_MonthGenitiveNames_m1_4047 (DateTimeFormatInfo_t1_362 * __this, StringU5BU5D_t1_238* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.DateTimeFormatInfo::get_NativeCalendarName()
extern "C" String_t* DateTimeFormatInfo_get_NativeCalendarName_m1_4048 (DateTimeFormatInfo_t1_362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.Globalization.DateTimeFormatInfo::get_ShortestDayNames()
extern "C" StringU5BU5D_t1_238* DateTimeFormatInfo_get_ShortestDayNames_m1_4049 (DateTimeFormatInfo_t1_362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.DateTimeFormatInfo::set_ShortestDayNames(System.String[])
extern "C" void DateTimeFormatInfo_set_ShortestDayNames_m1_4050 (DateTimeFormatInfo_t1_362 * __this, StringU5BU5D_t1_238* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.DateTimeFormatInfo::GetShortestDayName(System.DayOfWeek)
extern "C" String_t* DateTimeFormatInfo_GetShortestDayName_m1_4051 (DateTimeFormatInfo_t1_362 * __this, int32_t ___dayOfWeek, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.DateTimeFormatInfo::SetAllDateTimePatterns(System.String[],System.Char)
extern "C" void DateTimeFormatInfo_SetAllDateTimePatterns_m1_4052 (DateTimeFormatInfo_t1_362 * __this, StringU5BU5D_t1_238* ___patterns, uint16_t ___format, const MethodInfo* method) IL2CPP_METHOD_ATTR;
