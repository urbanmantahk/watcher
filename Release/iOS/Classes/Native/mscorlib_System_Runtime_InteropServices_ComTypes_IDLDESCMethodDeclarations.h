﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

extern "C" void IDLDESC_t1_727_marshal(const IDLDESC_t1_727& unmarshaled, IDLDESC_t1_727_marshaled& marshaled);
extern "C" void IDLDESC_t1_727_marshal_back(const IDLDESC_t1_727_marshaled& marshaled, IDLDESC_t1_727& unmarshaled);
extern "C" void IDLDESC_t1_727_marshal_cleanup(IDLDESC_t1_727_marshaled& marshaled);
