﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapDate
struct SoapDate_t1_967;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime.h"

// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapDate::.ctor()
extern "C" void SoapDate__ctor_m1_8682 (SoapDate_t1_967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapDate::.ctor(System.DateTime)
extern "C" void SoapDate__ctor_m1_8683 (SoapDate_t1_967 * __this, DateTime_t1_150  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapDate::.ctor(System.DateTime,System.Int32)
extern "C" void SoapDate__ctor_m1_8684 (SoapDate_t1_967 * __this, DateTime_t1_150  ___value, int32_t ___sign, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapDate::.cctor()
extern "C" void SoapDate__cctor_m1_8685 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.Remoting.Metadata.W3cXsd2001.SoapDate::get_Sign()
extern "C" int32_t SoapDate_get_Sign_m1_8686 (SoapDate_t1_967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapDate::set_Sign(System.Int32)
extern "C" void SoapDate_set_Sign_m1_8687 (SoapDate_t1_967 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Runtime.Remoting.Metadata.W3cXsd2001.SoapDate::get_Value()
extern "C" DateTime_t1_150  SoapDate_get_Value_m1_8688 (SoapDate_t1_967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapDate::set_Value(System.DateTime)
extern "C" void SoapDate_set_Value_m1_8689 (SoapDate_t1_967 * __this, DateTime_t1_150  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapDate::get_XsdType()
extern "C" String_t* SoapDate_get_XsdType_m1_8690 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapDate::GetXsdType()
extern "C" String_t* SoapDate_GetXsdType_m1_8691 (SoapDate_t1_967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapDate System.Runtime.Remoting.Metadata.W3cXsd2001.SoapDate::Parse(System.String)
extern "C" SoapDate_t1_967 * SoapDate_Parse_m1_8692 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapDate::ToString()
extern "C" String_t* SoapDate_ToString_m1_8693 (SoapDate_t1_967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
