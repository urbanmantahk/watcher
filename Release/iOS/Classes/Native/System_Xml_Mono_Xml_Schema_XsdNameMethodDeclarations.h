﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Xml.Schema.XsdName
struct XsdName_t4_13;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlTokenizedType.h"

// System.Void Mono.Xml.Schema.XsdName::.ctor()
extern "C" void XsdName__ctor_m4_19 (XsdName_t4_13 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdName::get_TokenizedType()
extern "C" int32_t XsdName_get_TokenizedType_m4_20 (XsdName_t4_13 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
