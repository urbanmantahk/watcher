﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.Internal.iOSNotificationPayload
struct iOSNotificationPayload_t8_267;
// System.Collections.IDictionary
struct IDictionary_t1_35;
// VoxelBusters.NativePlugins.CrossPlatformNotification
struct CrossPlatformNotification_t8_259;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eNotificationRe.h"
#include "UnityEngine_UnityEngine_iOS_CalendarUnit.h"

// System.Void VoxelBusters.NativePlugins.Internal.iOSNotificationPayload::.ctor(System.Collections.IDictionary)
extern "C" void iOSNotificationPayload__ctor_m8_1535 (iOSNotificationPayload_t8_267 * __this, Object_t * ____payloadDict, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IDictionary VoxelBusters.NativePlugins.Internal.iOSNotificationPayload::CreateNotificationPayload(VoxelBusters.NativePlugins.CrossPlatformNotification)
extern "C" Object_t * iOSNotificationPayload_CreateNotificationPayload_m8_1536 (Object_t * __this /* static, unused */, CrossPlatformNotification_t8_259 * ____notification, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.NativePlugins.eNotificationRepeatInterval VoxelBusters.NativePlugins.Internal.iOSNotificationPayload::ConvertToRepeatInterval(UnityEngine.iOS.CalendarUnit)
extern "C" int32_t iOSNotificationPayload_ConvertToRepeatInterval_m8_1537 (Object_t * __this /* static, unused */, int32_t ____unit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.iOS.CalendarUnit VoxelBusters.NativePlugins.Internal.iOSNotificationPayload::ConvertToCalendarUnit(VoxelBusters.NativePlugins.eNotificationRepeatInterval)
extern "C" int32_t iOSNotificationPayload_ConvertToCalendarUnit_m8_1538 (Object_t * __this /* static, unused */, int32_t ____repeatInterval, const MethodInfo* method) IL2CPP_METHOD_ATTR;
