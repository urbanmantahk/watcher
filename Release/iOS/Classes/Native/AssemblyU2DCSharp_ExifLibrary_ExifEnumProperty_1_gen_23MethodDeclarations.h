﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSLongitudeRef>
struct ExifEnumProperty_1_t8_362;
// System.Object
struct Object_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTag.h"
#include "AssemblyU2DCSharp_ExifLibrary_GPSLongitudeRef.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifInterOperability.h"

// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSLongitudeRef>::.ctor(ExifLibrary.ExifTag,T,System.Boolean)
extern "C" void ExifEnumProperty_1__ctor_m8_2288_gshared (ExifEnumProperty_1_t8_362 * __this, int32_t ___tag, uint8_t ___value, bool ___isbitfield, const MethodInfo* method);
#define ExifEnumProperty_1__ctor_m8_2288(__this, ___tag, ___value, ___isbitfield, method) (( void (*) (ExifEnumProperty_1_t8_362 *, int32_t, uint8_t, bool, const MethodInfo*))ExifEnumProperty_1__ctor_m8_2288_gshared)(__this, ___tag, ___value, ___isbitfield, method)
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSLongitudeRef>::.ctor(ExifLibrary.ExifTag,T)
extern "C" void ExifEnumProperty_1__ctor_m8_1999_gshared (ExifEnumProperty_1_t8_362 * __this, int32_t ___tag, uint8_t ___value, const MethodInfo* method);
#define ExifEnumProperty_1__ctor_m8_1999(__this, ___tag, ___value, method) (( void (*) (ExifEnumProperty_1_t8_362 *, int32_t, uint8_t, const MethodInfo*))ExifEnumProperty_1__ctor_m8_1999_gshared)(__this, ___tag, ___value, method)
// System.Object ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSLongitudeRef>::get__Value()
extern "C" Object_t * ExifEnumProperty_1_get__Value_m8_2289_gshared (ExifEnumProperty_1_t8_362 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get__Value_m8_2289(__this, method) (( Object_t * (*) (ExifEnumProperty_1_t8_362 *, const MethodInfo*))ExifEnumProperty_1_get__Value_m8_2289_gshared)(__this, method)
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSLongitudeRef>::set__Value(System.Object)
extern "C" void ExifEnumProperty_1_set__Value_m8_2290_gshared (ExifEnumProperty_1_t8_362 * __this, Object_t * ___value, const MethodInfo* method);
#define ExifEnumProperty_1_set__Value_m8_2290(__this, ___value, method) (( void (*) (ExifEnumProperty_1_t8_362 *, Object_t *, const MethodInfo*))ExifEnumProperty_1_set__Value_m8_2290_gshared)(__this, ___value, method)
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSLongitudeRef>::get_Value()
extern "C" uint8_t ExifEnumProperty_1_get_Value_m8_2291_gshared (ExifEnumProperty_1_t8_362 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get_Value_m8_2291(__this, method) (( uint8_t (*) (ExifEnumProperty_1_t8_362 *, const MethodInfo*))ExifEnumProperty_1_get_Value_m8_2291_gshared)(__this, method)
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSLongitudeRef>::set_Value(T)
extern "C" void ExifEnumProperty_1_set_Value_m8_2292_gshared (ExifEnumProperty_1_t8_362 * __this, uint8_t ___value, const MethodInfo* method);
#define ExifEnumProperty_1_set_Value_m8_2292(__this, ___value, method) (( void (*) (ExifEnumProperty_1_t8_362 *, uint8_t, const MethodInfo*))ExifEnumProperty_1_set_Value_m8_2292_gshared)(__this, ___value, method)
// System.Boolean ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSLongitudeRef>::get_IsBitField()
extern "C" bool ExifEnumProperty_1_get_IsBitField_m8_2293_gshared (ExifEnumProperty_1_t8_362 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get_IsBitField_m8_2293(__this, method) (( bool (*) (ExifEnumProperty_1_t8_362 *, const MethodInfo*))ExifEnumProperty_1_get_IsBitField_m8_2293_gshared)(__this, method)
// System.String ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSLongitudeRef>::ToString()
extern "C" String_t* ExifEnumProperty_1_ToString_m8_2294_gshared (ExifEnumProperty_1_t8_362 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_ToString_m8_2294(__this, method) (( String_t* (*) (ExifEnumProperty_1_t8_362 *, const MethodInfo*))ExifEnumProperty_1_ToString_m8_2294_gshared)(__this, method)
// ExifLibrary.ExifInterOperability ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSLongitudeRef>::get_Interoperability()
extern "C" ExifInterOperability_t8_113  ExifEnumProperty_1_get_Interoperability_m8_2295_gshared (ExifEnumProperty_1_t8_362 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get_Interoperability_m8_2295(__this, method) (( ExifInterOperability_t8_113  (*) (ExifEnumProperty_1_t8_362 *, const MethodInfo*))ExifEnumProperty_1_get_Interoperability_m8_2295_gshared)(__this, method)
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSLongitudeRef>::op_Implicit(ExifLibrary.ExifEnumProperty`1<T>)
extern "C" uint8_t ExifEnumProperty_1_op_Implicit_m8_2296_gshared (Object_t * __this /* static, unused */, ExifEnumProperty_1_t8_362 * ___obj, const MethodInfo* method);
#define ExifEnumProperty_1_op_Implicit_m8_2296(__this /* static, unused */, ___obj, method) (( uint8_t (*) (Object_t * /* static, unused */, ExifEnumProperty_1_t8_362 *, const MethodInfo*))ExifEnumProperty_1_op_Implicit_m8_2296_gshared)(__this /* static, unused */, ___obj, method)
