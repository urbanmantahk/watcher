﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Attribute.h"
#include "mscorlib_System_Runtime_InteropServices_CharSet.h"

// System.Runtime.InteropServices.DefaultCharSetAttribute
struct  DefaultCharSetAttribute_t1_54  : public Attribute_t1_2
{
	// System.Runtime.InteropServices.CharSet System.Runtime.InteropServices.DefaultCharSetAttribute::_set
	int32_t ____set_0;
};
