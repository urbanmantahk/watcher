﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.IntPtr[]
struct IntPtrU5BU5D_t1_34;
// System.RuntimeTypeHandle[]
struct RuntimeTypeHandleU5BU5D_t1_1672;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_ModuleHandle.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_Reflection_PortableExecutableKinds.h"
#include "mscorlib_System_Reflection_ImageFileMachine.h"
#include "mscorlib_System_RuntimeFieldHandle.h"
#include "mscorlib_System_RuntimeMethodHandle.h"
#include "mscorlib_System_RuntimeTypeHandle.h"

// System.Void System.ModuleHandle::.ctor(System.IntPtr)
extern "C" void ModuleHandle__ctor_m1_14287 (ModuleHandle_t1_1572 * __this, IntPtr_t ___v, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ModuleHandle::.cctor()
extern "C" void ModuleHandle__cctor_m1_14288 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.ModuleHandle::get_Value()
extern "C" IntPtr_t ModuleHandle_get_Value_m1_14289 (ModuleHandle_t1_1572 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.ModuleHandle::get_MDStreamVersion()
extern "C" int32_t ModuleHandle_get_MDStreamVersion_m1_14290 (ModuleHandle_t1_1572 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ModuleHandle::GetPEKind(System.Reflection.PortableExecutableKinds&,System.Reflection.ImageFileMachine&)
extern "C" void ModuleHandle_GetPEKind_m1_14291 (ModuleHandle_t1_1572 * __this, int32_t* ___peKind, int32_t* ___machine, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.RuntimeFieldHandle System.ModuleHandle::ResolveFieldHandle(System.Int32)
extern "C" RuntimeFieldHandle_t1_36  ModuleHandle_ResolveFieldHandle_m1_14292 (ModuleHandle_t1_1572 * __this, int32_t ___fieldToken, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.RuntimeMethodHandle System.ModuleHandle::ResolveMethodHandle(System.Int32)
extern "C" RuntimeMethodHandle_t1_479  ModuleHandle_ResolveMethodHandle_m1_14293 (ModuleHandle_t1_1572 * __this, int32_t ___methodToken, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.RuntimeTypeHandle System.ModuleHandle::ResolveTypeHandle(System.Int32)
extern "C" RuntimeTypeHandle_t1_30  ModuleHandle_ResolveTypeHandle_m1_14294 (ModuleHandle_t1_1572 * __this, int32_t ___typeToken, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr[] System.ModuleHandle::ptrs_from_handles(System.RuntimeTypeHandle[])
extern "C" IntPtrU5BU5D_t1_34* ModuleHandle_ptrs_from_handles_m1_14295 (ModuleHandle_t1_1572 * __this, RuntimeTypeHandleU5BU5D_t1_1672* ___handles, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.RuntimeTypeHandle System.ModuleHandle::ResolveTypeHandle(System.Int32,System.RuntimeTypeHandle[],System.RuntimeTypeHandle[])
extern "C" RuntimeTypeHandle_t1_30  ModuleHandle_ResolveTypeHandle_m1_14296 (ModuleHandle_t1_1572 * __this, int32_t ___typeToken, RuntimeTypeHandleU5BU5D_t1_1672* ___typeInstantiationContext, RuntimeTypeHandleU5BU5D_t1_1672* ___methodInstantiationContext, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.RuntimeMethodHandle System.ModuleHandle::ResolveMethodHandle(System.Int32,System.RuntimeTypeHandle[],System.RuntimeTypeHandle[])
extern "C" RuntimeMethodHandle_t1_479  ModuleHandle_ResolveMethodHandle_m1_14297 (ModuleHandle_t1_1572 * __this, int32_t ___methodToken, RuntimeTypeHandleU5BU5D_t1_1672* ___typeInstantiationContext, RuntimeTypeHandleU5BU5D_t1_1672* ___methodInstantiationContext, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.RuntimeFieldHandle System.ModuleHandle::ResolveFieldHandle(System.Int32,System.RuntimeTypeHandle[],System.RuntimeTypeHandle[])
extern "C" RuntimeFieldHandle_t1_36  ModuleHandle_ResolveFieldHandle_m1_14298 (ModuleHandle_t1_1572 * __this, int32_t ___fieldToken, RuntimeTypeHandleU5BU5D_t1_1672* ___typeInstantiationContext, RuntimeTypeHandleU5BU5D_t1_1672* ___methodInstantiationContext, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.RuntimeFieldHandle System.ModuleHandle::GetRuntimeFieldHandleFromMetadataToken(System.Int32)
extern "C" RuntimeFieldHandle_t1_36  ModuleHandle_GetRuntimeFieldHandleFromMetadataToken_m1_14299 (ModuleHandle_t1_1572 * __this, int32_t ___fieldToken, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.RuntimeMethodHandle System.ModuleHandle::GetRuntimeMethodHandleFromMetadataToken(System.Int32)
extern "C" RuntimeMethodHandle_t1_479  ModuleHandle_GetRuntimeMethodHandleFromMetadataToken_m1_14300 (ModuleHandle_t1_1572 * __this, int32_t ___methodToken, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.RuntimeTypeHandle System.ModuleHandle::GetRuntimeTypeHandleFromMetadataToken(System.Int32)
extern "C" RuntimeTypeHandle_t1_30  ModuleHandle_GetRuntimeTypeHandleFromMetadataToken_m1_14301 (ModuleHandle_t1_1572 * __this, int32_t ___typeToken, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ModuleHandle::Equals(System.Object)
extern "C" bool ModuleHandle_Equals_m1_14302 (ModuleHandle_t1_1572 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ModuleHandle::Equals(System.ModuleHandle)
extern "C" bool ModuleHandle_Equals_m1_14303 (ModuleHandle_t1_1572 * __this, ModuleHandle_t1_1572  ___handle, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.ModuleHandle::GetHashCode()
extern "C" int32_t ModuleHandle_GetHashCode_m1_14304 (ModuleHandle_t1_1572 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ModuleHandle::op_Equality(System.ModuleHandle,System.ModuleHandle)
extern "C" bool ModuleHandle_op_Equality_m1_14305 (Object_t * __this /* static, unused */, ModuleHandle_t1_1572  ___left, ModuleHandle_t1_1572  ___right, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ModuleHandle::op_Inequality(System.ModuleHandle,System.ModuleHandle)
extern "C" bool ModuleHandle_op_Inequality_m1_14306 (Object_t * __this /* static, unused */, ModuleHandle_t1_1572  ___left, ModuleHandle_t1_1572  ___right, const MethodInfo* method) IL2CPP_METHOD_ATTR;
