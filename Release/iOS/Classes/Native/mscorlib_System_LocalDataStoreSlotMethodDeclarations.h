﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.LocalDataStoreSlot
struct LocalDataStoreSlot_t1_1564;

#include "codegen/il2cpp-codegen.h"

// System.Void System.LocalDataStoreSlot::.ctor(System.Boolean)
extern "C" void LocalDataStoreSlot__ctor_m1_14184 (LocalDataStoreSlot_t1_1564 * __this, bool ___in_thread, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.LocalDataStoreSlot::.cctor()
extern "C" void LocalDataStoreSlot__cctor_m1_14185 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.LocalDataStoreSlot::Finalize()
extern "C" void LocalDataStoreSlot_Finalize_m1_14186 (LocalDataStoreSlot_t1_1564 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
