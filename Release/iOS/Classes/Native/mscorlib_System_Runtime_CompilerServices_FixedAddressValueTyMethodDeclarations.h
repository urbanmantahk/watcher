﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.CompilerServices.FixedAddressValueTypeAttribute
struct FixedAddressValueTypeAttribute_t1_686;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.CompilerServices.FixedAddressValueTypeAttribute::.ctor()
extern "C" void FixedAddressValueTypeAttribute__ctor_m1_7544 (FixedAddressValueTypeAttribute_t1_686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
