﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.DictionaryBase
struct DictionaryBase_t1_283;
// System.Object
struct Object_t;
// System.Collections.ICollection
struct ICollection_t1_280;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Collections.IDictionary
struct IDictionary_t1_35;
// System.Collections.Hashtable
struct Hashtable_t1_100;
// System.Array
struct Array_t;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1_863;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.DictionaryBase::.ctor()
extern "C" void DictionaryBase__ctor_m1_3198 (DictionaryBase_t1_283 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.DictionaryBase::System.Collections.IDictionary.get_IsFixedSize()
extern "C" bool DictionaryBase_System_Collections_IDictionary_get_IsFixedSize_m1_3199 (DictionaryBase_t1_283 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.DictionaryBase::System.Collections.IDictionary.get_IsReadOnly()
extern "C" bool DictionaryBase_System_Collections_IDictionary_get_IsReadOnly_m1_3200 (DictionaryBase_t1_283 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.DictionaryBase::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * DictionaryBase_System_Collections_IDictionary_get_Item_m1_3201 (DictionaryBase_t1_283 * __this, Object_t * ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.DictionaryBase::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void DictionaryBase_System_Collections_IDictionary_set_Item_m1_3202 (DictionaryBase_t1_283 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ICollection System.Collections.DictionaryBase::System.Collections.IDictionary.get_Keys()
extern "C" Object_t * DictionaryBase_System_Collections_IDictionary_get_Keys_m1_3203 (DictionaryBase_t1_283 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ICollection System.Collections.DictionaryBase::System.Collections.IDictionary.get_Values()
extern "C" Object_t * DictionaryBase_System_Collections_IDictionary_get_Values_m1_3204 (DictionaryBase_t1_283 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.DictionaryBase::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void DictionaryBase_System_Collections_IDictionary_Add_m1_3205 (DictionaryBase_t1_283 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.DictionaryBase::System.Collections.IDictionary.Remove(System.Object)
extern "C" void DictionaryBase_System_Collections_IDictionary_Remove_m1_3206 (DictionaryBase_t1_283 * __this, Object_t * ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.DictionaryBase::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool DictionaryBase_System_Collections_IDictionary_Contains_m1_3207 (DictionaryBase_t1_283 * __this, Object_t * ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.DictionaryBase::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool DictionaryBase_System_Collections_ICollection_get_IsSynchronized_m1_3208 (DictionaryBase_t1_283 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.DictionaryBase::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * DictionaryBase_System_Collections_ICollection_get_SyncRoot_m1_3209 (DictionaryBase_t1_283 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Collections.DictionaryBase::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * DictionaryBase_System_Collections_IEnumerable_GetEnumerator_m1_3210 (DictionaryBase_t1_283 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.DictionaryBase::Clear()
extern "C" void DictionaryBase_Clear_m1_3211 (DictionaryBase_t1_283 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.DictionaryBase::get_Count()
extern "C" int32_t DictionaryBase_get_Count_m1_3212 (DictionaryBase_t1_283 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IDictionary System.Collections.DictionaryBase::get_Dictionary()
extern "C" Object_t * DictionaryBase_get_Dictionary_m1_3213 (DictionaryBase_t1_283 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Hashtable System.Collections.DictionaryBase::get_InnerHashtable()
extern "C" Hashtable_t1_100 * DictionaryBase_get_InnerHashtable_m1_3214 (DictionaryBase_t1_283 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.DictionaryBase::CopyTo(System.Array,System.Int32)
extern "C" void DictionaryBase_CopyTo_m1_3215 (DictionaryBase_t1_283 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.DictionaryBase::DoCopy(System.Array,System.Int32)
extern "C" void DictionaryBase_DoCopy_m1_3216 (DictionaryBase_t1_283 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IDictionaryEnumerator System.Collections.DictionaryBase::GetEnumerator()
extern "C" Object_t * DictionaryBase_GetEnumerator_m1_3217 (DictionaryBase_t1_283 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.DictionaryBase::OnClear()
extern "C" void DictionaryBase_OnClear_m1_3218 (DictionaryBase_t1_283 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.DictionaryBase::OnClearComplete()
extern "C" void DictionaryBase_OnClearComplete_m1_3219 (DictionaryBase_t1_283 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.DictionaryBase::OnGet(System.Object,System.Object)
extern "C" Object_t * DictionaryBase_OnGet_m1_3220 (DictionaryBase_t1_283 * __this, Object_t * ___key, Object_t * ___currentValue, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.DictionaryBase::OnInsert(System.Object,System.Object)
extern "C" void DictionaryBase_OnInsert_m1_3221 (DictionaryBase_t1_283 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.DictionaryBase::OnInsertComplete(System.Object,System.Object)
extern "C" void DictionaryBase_OnInsertComplete_m1_3222 (DictionaryBase_t1_283 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.DictionaryBase::OnSet(System.Object,System.Object,System.Object)
extern "C" void DictionaryBase_OnSet_m1_3223 (DictionaryBase_t1_283 * __this, Object_t * ___key, Object_t * ___oldValue, Object_t * ___newValue, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.DictionaryBase::OnSetComplete(System.Object,System.Object,System.Object)
extern "C" void DictionaryBase_OnSetComplete_m1_3224 (DictionaryBase_t1_283 * __this, Object_t * ___key, Object_t * ___oldValue, Object_t * ___newValue, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.DictionaryBase::OnRemove(System.Object,System.Object)
extern "C" void DictionaryBase_OnRemove_m1_3225 (DictionaryBase_t1_283 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.DictionaryBase::OnRemoveComplete(System.Object,System.Object)
extern "C" void DictionaryBase_OnRemoveComplete_m1_3226 (DictionaryBase_t1_283 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.DictionaryBase::OnValidate(System.Object,System.Object)
extern "C" void DictionaryBase_OnValidate_m1_3227 (DictionaryBase_t1_283 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
