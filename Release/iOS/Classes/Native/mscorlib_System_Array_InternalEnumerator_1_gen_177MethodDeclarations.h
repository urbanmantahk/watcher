﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_177.h"
#include "AssemblyU2DCSharp_ExifLibrary_MathEx_Fraction32.h"

// System.Void System.Array/InternalEnumerator`1<ExifLibrary.MathEx/Fraction32>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_26311_gshared (InternalEnumerator_1_t1_2660 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1_26311(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1_2660 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m1_26311_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<ExifLibrary.MathEx/Fraction32>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_26312_gshared (InternalEnumerator_1_t1_2660 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_26312(__this, method) (( void (*) (InternalEnumerator_1_t1_2660 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_26312_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<ExifLibrary.MathEx/Fraction32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_26313_gshared (InternalEnumerator_1_t1_2660 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_26313(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1_2660 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_26313_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<ExifLibrary.MathEx/Fraction32>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_26314_gshared (InternalEnumerator_1_t1_2660 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1_26314(__this, method) (( void (*) (InternalEnumerator_1_t1_2660 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1_26314_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<ExifLibrary.MathEx/Fraction32>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_26315_gshared (InternalEnumerator_1_t1_2660 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1_26315(__this, method) (( bool (*) (InternalEnumerator_1_t1_2660 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1_26315_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<ExifLibrary.MathEx/Fraction32>::get_Current()
extern "C" Fraction32_t8_127  InternalEnumerator_1_get_Current_m1_26316_gshared (InternalEnumerator_1_t1_2660 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1_26316(__this, method) (( Fraction32_t8_127  (*) (InternalEnumerator_1_t1_2660 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1_26316_gshared)(__this, method)
