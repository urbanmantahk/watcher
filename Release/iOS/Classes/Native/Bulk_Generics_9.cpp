﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// ExifLibrary.ExifEnumProperty`1<ExifLibrary.WhiteBalance>
struct ExifEnumProperty_1_t8_354;
// System.Object
struct Object_t;
// System.String
struct String_t;
// ExifLibrary.ExifEnumProperty`1<ExifLibrary.SceneCaptureType>
struct ExifEnumProperty_1_t8_355;
// ExifLibrary.ExifEnumProperty`1<ExifLibrary.GainControl>
struct ExifEnumProperty_1_t8_356;
// ExifLibrary.ExifEnumProperty`1<ExifLibrary.Contrast>
struct ExifEnumProperty_1_t8_357;
// ExifLibrary.ExifEnumProperty`1<ExifLibrary.Saturation>
struct ExifEnumProperty_1_t8_358;
// ExifLibrary.ExifEnumProperty`1<ExifLibrary.Sharpness>
struct ExifEnumProperty_1_t8_359;
// ExifLibrary.ExifEnumProperty`1<ExifLibrary.SubjectDistanceRange>
struct ExifEnumProperty_1_t8_360;
// ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSLatitudeRef>
struct ExifEnumProperty_1_t8_361;
// ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSLongitudeRef>
struct ExifEnumProperty_1_t8_362;
// ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSAltitudeRef>
struct ExifEnumProperty_1_t8_363;
// ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSStatus>
struct ExifEnumProperty_1_t8_364;
// ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSMeasureMode>
struct ExifEnumProperty_1_t8_365;
// ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSSpeedRef>
struct ExifEnumProperty_1_t8_366;
// ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSDirectionRef>
struct ExifEnumProperty_1_t8_367;
// ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSDistanceRef>
struct ExifEnumProperty_1_t8_368;
// ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSDifferential>
struct ExifEnumProperty_1_t8_369;
// System.Collections.Generic.Dictionary`2<System.Object,System.Single>
struct Dictionary_2_t1_2704;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t1_1980;
// System.Collections.Generic.IDictionary`2<System.Object,System.Single>
struct IDictionary_2_t1_2890;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t1_2012;
// System.Collections.Generic.ICollection`1<System.Single>
struct ICollection_1_t1_2891;
// System.Collections.ICollection
struct ICollection_t1_280;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>[]
struct KeyValuePair_2U5BU5D_t1_2889;
// System.Array
struct Array_t;
// System.Collections.DictionaryEntry[]
struct DictionaryEntryU5BU5D_t1_869;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Collections.DictionaryEntry>
struct Transform_1_t1_2705;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>
struct Transform_1_t1_2715;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>
struct IEnumerator_1_t1_2892;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1_863;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Single>
struct KeyCollection_t1_2708;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>
struct ValueCollection_t1_2712;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1_2210;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Object>
struct Transform_1_t1_2711;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;
// System.Collections.Generic.IEnumerator`1<System.Single>
struct IEnumerator_1_t1_2893;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Single>
struct Transform_1_t1_2714;
// System.Single[]
struct SingleU5BU5D_t1_1695;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Single>
struct ShimEnumerator_t1_2716;
// System.Collections.Generic.EqualityComparer`1<System.Single>
struct EqualityComparer_1_t1_2717;
// System.Collections.Generic.GenericEqualityComparer`1<System.Single>
struct GenericEqualityComparer_1_t1_2718;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Single>
struct DefaultComparer_t1_2719;
// System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>
struct List_1_t1_1908;
// System.Collections.Generic.IEnumerable`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>
struct IEnumerable_1_t1_2894;
// VoxelBusters.DebugPRO.Internal.ConsoleLog[]
struct ConsoleLogU5BU5D_t8_382;
// System.Collections.Generic.IEnumerator`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>
struct IEnumerator_1_t1_2895;
// System.Collections.Generic.ICollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>
struct ICollection_1_t1_2896;
// System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>
struct ReadOnlyCollection_1_t1_2722;
// System.Collections.Generic.IComparer`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>
struct IComparer_1_t1_2897;
// System.Predicate`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>
struct Predicate_1_t1_2729;
// System.Action`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>
struct Action_1_t1_2730;
// System.Comparison`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>
struct Comparison_1_t1_2731;
// System.Collections.Generic.IList`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>
struct IList_1_t1_2723;
// System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>
struct Collection_1_t1_2724;
// System.Collections.Generic.Comparer`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>
struct Comparer_1_t1_2725;
// System.Collections.Generic.Comparer`1/DefaultComparer<VoxelBusters.DebugPRO.Internal.ConsoleLog>
struct DefaultComparer_t1_2726;
// System.Collections.Generic.EqualityComparer`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>
struct EqualityComparer_1_t1_2727;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<VoxelBusters.DebugPRO.Internal.ConsoleLog>
struct DefaultComparer_t1_2728;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_15.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_15MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTag.h"
#include "AssemblyU2DCSharp_ExifLibrary_WhiteBalance.h"
#include "mscorlib_System_Void.h"
#include "mscorlib_System_Boolean.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifPropertyMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifProperty.h"
#include "mscorlib_System_Object.h"
#include "mscorlib_System_String.h"
#include "mscorlib_System_ObjectMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifInterOperability.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTagFactoryMethodDeclarations.h"
#include "mscorlib_System_TypeMethodDeclarations.h"
#include "mscorlib_System_EnumMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifInterOperabilityMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_BitConverterExMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_UnknownEnumTypeExceptionMethodDeclarations.h"
#include "mscorlib_System_UInt16.h"
#include "mscorlib_System_Type.h"
#include "mscorlib_System_RuntimeTypeHandle.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Byte.h"
#include "mscorlib_System_UInt32.h"
#include "AssemblyU2DCSharp_ExifLibrary_BitConverterEx_ByteOrder.h"
#include "AssemblyU2DCSharp_ExifLibrary_UnknownEnumTypeException.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_16.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_16MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_SceneCaptureType.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_17.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_17MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_GainControl.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_18.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_18MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_Contrast.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_19.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_19MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_Saturation.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_20.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_20MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_Sharpness.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_21.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_21MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_SubjectDistanceRange.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_22.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_22MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_GPSLatitudeRef.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_23.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_23MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_GPSLongitudeRef.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_24.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_24MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_GPSAltitudeRef.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_25.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_25MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_GPSStatus.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_26.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_26MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_GPSMeasureMode.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_27.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_27MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_GPSSpeedRef.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_28.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_28MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_GPSDirectionRef.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_29.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_29MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_GPSDistanceRef.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_30.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_30MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_GPSDifferential.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_25.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_25MethodDeclarations.h"
#include "mscorlib_System_Int32.h"
#include "mscorlib_System_ArgumentNullExceptionMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_24.h"
#include "mscorlib_System_ArgumentNullException.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_24MethodDeclarations.h"
#include "mscorlib_System_Single.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Runtime_Serialization_SerializationInfo.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_28.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_31.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_41.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_41MethodDeclarations.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_42.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_42MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__22.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__22MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ShimEnumera_5.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ShimEnumera_5MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyNotFoundExceptionMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Link.h"
#include "mscorlib_System_Collections_Generic_KeyNotFoundException.h"
#include "mscorlib_System_ArgumentOutOfRangeExceptionMethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException.h"
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_genMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_gen.h"
#include "mscorlib_System_ArrayMethodDeclarations.h"
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
#include "mscorlib_System_ArgumentException.h"
#include "mscorlib_System_Collections_HashtableMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_gen_20MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_gen_20.h"
#include "mscorlib_System_Runtime_Serialization_SerializationInfoMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_28MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_31MethodDeclarations.h"
#include "mscorlib_System_StringMethodDeclarations.h"
#include "mscorlib_System_Collections_DictionaryEntryMethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_183.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_183MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException.h"
#include "mscorlib_System_SingleMethodDeclarations.h"
#include "mscorlib_System_NotSupportedExceptionMethodDeclarations.h"
#include "mscorlib_System_NotSupportedException.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_29.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_39.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_39MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_29MethodDeclarations.h"
#include "mscorlib_System_ObjectDisposedExceptionMethodDeclarations.h"
#include "mscorlib_System_ObjectDisposedException.h"
#include "mscorlib_System_AsyncCallback.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_32.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_40.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_40MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_32MethodDeclarations.h"
#include "mscorlib_System_ActivatorMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_Defau_20.h"
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_Defau_20MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_GenericEqualityComparer__7.h"
#include "mscorlib_System_Collections_Generic_GenericEqualityComparer__7MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_47.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_47MethodDeclarations.h"
#include "Assembly-CSharp_ArrayTypes.h"
#include "AssemblyU2DCSharp_VoxelBusters_DebugPRO_Internal_ConsoleLog.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_51.h"
#include "mscorlib_System_NullReferenceException.h"
#include "mscorlib_System_InvalidCastException.h"
#include "mscorlib_System_MathMethodDeclarations.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCollection_1_54.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCollection_1_54MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen_54.h"
#include "mscorlib_System_Predicate_1_gen_54MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen_58.h"
#include "mscorlib_System_Action_1_gen_58MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_51MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen_17MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen_17.h"
#include "mscorlib_System_Comparison_1_gen_54.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_184.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_184MethodDeclarations.h"
#include "mscorlib_System_Collections_ObjectModel_Collection_1_gen_12MethodDeclarations.h"
#include "mscorlib_System_Collections_ObjectModel_Collection_1_gen_12.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_DefaultCompar_17.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_DefaultCompar_17MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_gen_21.h"
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_gen_21MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_Defau_21.h"
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_Defau_21MethodDeclarations.h"
#include "mscorlib_System_Comparison_1_gen_54MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_187.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_187MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eShareOptions.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Single>::Do_CopyTo<System.Collections.DictionaryEntry,System.Collections.DictionaryEntry>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1_284_TisDictionaryEntry_t1_284_m1_29125_gshared (Dictionary_2_t1_2704 * __this, DictionaryEntryU5BU5D_t1_869* p0, int32_t p1, Transform_1_t1_2705 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1_284_TisDictionaryEntry_t1_284_m1_29125(__this, p0, p1, p2, method) (( void (*) (Dictionary_2_t1_2704 *, DictionaryEntryU5BU5D_t1_869*, int32_t, Transform_1_t1_2705 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1_284_TisDictionaryEntry_t1_284_m1_29125_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Single>::Do_ICollectionCopyTo<System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t1_2706_m1_29126_gshared (Dictionary_2_t1_2704 * __this, Array_t * p0, int32_t p1, Transform_1_t1_2715 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t1_2706_m1_29126(__this, p0, p1, p2, method) (( void (*) (Dictionary_2_t1_2704 *, Array_t *, int32_t, Transform_1_t1_2715 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t1_2706_m1_29126_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Single>::Do_CopyTo<System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>,System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t1_2706_TisKeyValuePair_2_t1_2706_m1_29128_gshared (Dictionary_2_t1_2704 * __this, KeyValuePair_2U5BU5D_t1_2889* p0, int32_t p1, Transform_1_t1_2715 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t1_2706_TisKeyValuePair_2_t1_2706_m1_29128(__this, p0, p1, p2, method) (( void (*) (Dictionary_2_t1_2704 *, KeyValuePair_2U5BU5D_t1_2889*, int32_t, Transform_1_t1_2715 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t1_2706_TisKeyValuePair_2_t1_2706_m1_29128_gshared)(__this, p0, p1, p2, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>(System.Int32)
extern "C" KeyValuePair_2_t1_2706  Array_InternalArray__get_Item_TisKeyValuePair_2_t1_2706_m1_29111_gshared (Array_t * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t1_2706_m1_29111(__this, p0, method) (( KeyValuePair_2_t1_2706  (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t1_2706_m1_29111_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Single>::Do_ICollectionCopyTo<System.Object>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m1_29120_gshared (Dictionary_2_t1_2704 * __this, Array_t * p0, int32_t p1, Transform_1_t1_2711 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m1_29120(__this, p0, p1, p2, method) (( void (*) (Dictionary_2_t1_2704 *, Array_t *, int32_t, Transform_1_t1_2711 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m1_29120_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Single>::Do_CopyTo<System.Object,System.Object>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C" void Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m1_29121_gshared (Dictionary_2_t1_2704 * __this, ObjectU5BU5D_t1_272* p0, int32_t p1, Transform_1_t1_2711 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m1_29121(__this, p0, p1, p2, method) (( void (*) (Dictionary_2_t1_2704 *, ObjectU5BU5D_t1_272*, int32_t, Transform_1_t1_2711 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m1_29121_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Single>::Do_ICollectionCopyTo<System.Single>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisSingle_t1_17_m1_29122_gshared (Dictionary_2_t1_2704 * __this, Array_t * p0, int32_t p1, Transform_1_t1_2714 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisSingle_t1_17_m1_29122(__this, p0, p1, p2, method) (( void (*) (Dictionary_2_t1_2704 *, Array_t *, int32_t, Transform_1_t1_2714 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisSingle_t1_17_m1_29122_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Single>::Do_CopyTo<System.Single,System.Single>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C" void Dictionary_2_Do_CopyTo_TisSingle_t1_17_TisSingle_t1_17_m1_29124_gshared (Dictionary_2_t1_2704 * __this, SingleU5BU5D_t1_1695* p0, int32_t p1, Transform_1_t1_2714 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisSingle_t1_17_TisSingle_t1_17_m1_29124(__this, p0, p1, p2, method) (( void (*) (Dictionary_2_t1_2704 *, SingleU5BU5D_t1_1695*, int32_t, Transform_1_t1_2714 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisSingle_t1_17_TisSingle_t1_17_m1_29124_gshared)(__this, p0, p1, p2, method)
// System.Int32 System.Array::BinarySearch<VoxelBusters.DebugPRO.Internal.ConsoleLog>(!!0[],System.Int32,System.Int32,!!0)
extern "C" int32_t Array_BinarySearch_TisConsoleLog_t8_170_m1_29140_gshared (Object_t * __this /* static, unused */, ConsoleLogU5BU5D_t8_382* p0, int32_t p1, int32_t p2, ConsoleLog_t8_170  p3, const MethodInfo* method);
#define Array_BinarySearch_TisConsoleLog_t8_170_m1_29140(__this /* static, unused */, p0, p1, p2, p3, method) (( int32_t (*) (Object_t * /* static, unused */, ConsoleLogU5BU5D_t8_382*, int32_t, int32_t, ConsoleLog_t8_170 , const MethodInfo*))Array_BinarySearch_TisConsoleLog_t8_170_m1_29140_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Int32 System.Array::BinarySearch<VoxelBusters.DebugPRO.Internal.ConsoleLog>(!!0[],System.Int32,System.Int32,!!0,System.Collections.Generic.IComparer`1<!!0>)
extern "C" int32_t Array_BinarySearch_TisConsoleLog_t8_170_m1_29141_gshared (Object_t * __this /* static, unused */, ConsoleLogU5BU5D_t8_382* p0, int32_t p1, int32_t p2, ConsoleLog_t8_170  p3, Object_t* p4, const MethodInfo* method);
#define Array_BinarySearch_TisConsoleLog_t8_170_m1_29141(__this /* static, unused */, p0, p1, p2, p3, p4, method) (( int32_t (*) (Object_t * /* static, unused */, ConsoleLogU5BU5D_t8_382*, int32_t, int32_t, ConsoleLog_t8_170 , Object_t*, const MethodInfo*))Array_BinarySearch_TisConsoleLog_t8_170_m1_29141_gshared)(__this /* static, unused */, p0, p1, p2, p3, p4, method)
// System.Int32 System.Array::IndexOf<VoxelBusters.DebugPRO.Internal.ConsoleLog>(!!0[],!!0,System.Int32,System.Int32)
extern "C" int32_t Array_IndexOf_TisConsoleLog_t8_170_m1_29142_gshared (Object_t * __this /* static, unused */, ConsoleLogU5BU5D_t8_382* p0, ConsoleLog_t8_170  p1, int32_t p2, int32_t p3, const MethodInfo* method);
#define Array_IndexOf_TisConsoleLog_t8_170_m1_29142(__this /* static, unused */, p0, p1, p2, p3, method) (( int32_t (*) (Object_t * /* static, unused */, ConsoleLogU5BU5D_t8_382*, ConsoleLog_t8_170 , int32_t, int32_t, const MethodInfo*))Array_IndexOf_TisConsoleLog_t8_170_m1_29142_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Int32 System.Array::LastIndexOf<VoxelBusters.DebugPRO.Internal.ConsoleLog>(!!0[],!!0,System.Int32,System.Int32)
extern "C" int32_t Array_LastIndexOf_TisConsoleLog_t8_170_m1_29143_gshared (Object_t * __this /* static, unused */, ConsoleLogU5BU5D_t8_382* p0, ConsoleLog_t8_170  p1, int32_t p2, int32_t p3, const MethodInfo* method);
#define Array_LastIndexOf_TisConsoleLog_t8_170_m1_29143(__this /* static, unused */, p0, p1, p2, p3, method) (( int32_t (*) (Object_t * /* static, unused */, ConsoleLogU5BU5D_t8_382*, ConsoleLog_t8_170 , int32_t, int32_t, const MethodInfo*))Array_LastIndexOf_TisConsoleLog_t8_170_m1_29143_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Void System.Array::Sort<VoxelBusters.DebugPRO.Internal.ConsoleLog>(!!0[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<!!0>)
extern "C" void Array_Sort_TisConsoleLog_t8_170_m1_29144_gshared (Object_t * __this /* static, unused */, ConsoleLogU5BU5D_t8_382* p0, int32_t p1, int32_t p2, Object_t* p3, const MethodInfo* method);
#define Array_Sort_TisConsoleLog_t8_170_m1_29144(__this /* static, unused */, p0, p1, p2, p3, method) (( void (*) (Object_t * /* static, unused */, ConsoleLogU5BU5D_t8_382*, int32_t, int32_t, Object_t*, const MethodInfo*))Array_Sort_TisConsoleLog_t8_170_m1_29144_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Void System.Array::Sort<VoxelBusters.DebugPRO.Internal.ConsoleLog>(!!0[],System.Int32,System.Comparison`1<!!0>)
extern "C" void Array_Sort_TisConsoleLog_t8_170_m1_29150_gshared (Object_t * __this /* static, unused */, ConsoleLogU5BU5D_t8_382* p0, int32_t p1, Comparison_1_t1_2731 * p2, const MethodInfo* method);
#define Array_Sort_TisConsoleLog_t8_170_m1_29150(__this /* static, unused */, p0, p1, p2, method) (( void (*) (Object_t * /* static, unused */, ConsoleLogU5BU5D_t8_382*, int32_t, Comparison_1_t1_2731 *, const MethodInfo*))Array_Sort_TisConsoleLog_t8_170_m1_29150_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Void System.Array::Resize<VoxelBusters.DebugPRO.Internal.ConsoleLog>(!!0[]&,System.Int32)
extern "C" void Array_Resize_TisConsoleLog_t8_170_m1_29138_gshared (Object_t * __this /* static, unused */, ConsoleLogU5BU5D_t8_382** p0, int32_t p1, const MethodInfo* method);
#define Array_Resize_TisConsoleLog_t8_170_m1_29138(__this /* static, unused */, p0, p1, method) (( void (*) (Object_t * /* static, unused */, ConsoleLogU5BU5D_t8_382**, int32_t, const MethodInfo*))Array_Resize_TisConsoleLog_t8_170_m1_29138_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 System.Array::InternalArray__get_Item<VoxelBusters.DebugPRO.Internal.ConsoleLog>(System.Int32)
extern "C" ConsoleLog_t8_170  Array_InternalArray__get_Item_TisConsoleLog_t8_170_m1_29129_gshared (Array_t * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisConsoleLog_t8_170_m1_29129(__this, p0, method) (( ConsoleLog_t8_170  (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisConsoleLog_t8_170_m1_29129_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<VoxelBusters.NativePlugins.eShareOptions>(System.Int32)
extern "C" int32_t Array_InternalArray__get_Item_TiseShareOptions_t8_287_m1_29153_gshared (Array_t * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TiseShareOptions_t8_287_m1_29153(__this, p0, method) (( int32_t (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TiseShareOptions_t8_287_m1_29153_gshared)(__this, p0, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.WhiteBalance>::.ctor(ExifLibrary.ExifTag,T,System.Boolean)
extern "C" void ExifEnumProperty_1__ctor_m8_1991_gshared (ExifEnumProperty_1_t8_354 * __this, int32_t ___tag, uint16_t ___value, bool ___isbitfield, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		NullCheck((ExifProperty_t8_99 *)__this);
		ExifProperty__ctor_m8_502((ExifProperty_t8_99 *)__this, (int32_t)L_0, /*hidden argument*/NULL);
		uint16_t L_1 = ___value;
		__this->___mValue_3 = L_1;
		bool L_2 = ___isbitfield;
		__this->___mIsBitField_4 = L_2;
		return;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.WhiteBalance>::.ctor(ExifLibrary.ExifTag,T)
extern "C" void ExifEnumProperty_1__ctor_m8_2216_gshared (ExifEnumProperty_1_t8_354 * __this, int32_t ___tag, uint16_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		uint16_t L_1 = ___value;
		NullCheck((ExifEnumProperty_1_t8_354 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_354 *, int32_t, uint16_t, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ExifEnumProperty_1_t8_354 *)__this, (int32_t)L_0, (uint16_t)L_1, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Object ExifLibrary.ExifEnumProperty`1<ExifLibrary.WhiteBalance>::get__Value()
extern "C" Object_t * ExifEnumProperty_1_get__Value_m8_2217_gshared (ExifEnumProperty_1_t8_354 * __this, const MethodInfo* method)
{
	{
		NullCheck((ExifEnumProperty_1_t8_354 *)__this);
		uint16_t L_0 = (( uint16_t (*) (ExifEnumProperty_1_t8_354 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ExifEnumProperty_1_t8_354 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		uint16_t L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.WhiteBalance>::set__Value(System.Object)
extern "C" void ExifEnumProperty_1_set__Value_m8_2218_gshared (ExifEnumProperty_1_t8_354 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		NullCheck((ExifEnumProperty_1_t8_354 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_354 *, uint16_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((ExifEnumProperty_1_t8_354 *)__this, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.WhiteBalance>::get_Value()
extern "C" uint16_t ExifEnumProperty_1_get_Value_m8_2219_gshared (ExifEnumProperty_1_t8_354 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = (uint16_t)(__this->___mValue_3);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.WhiteBalance>::set_Value(T)
extern "C" void ExifEnumProperty_1_set_Value_m8_2220_gshared (ExifEnumProperty_1_t8_354 * __this, uint16_t ___value, const MethodInfo* method)
{
	{
		uint16_t L_0 = ___value;
		__this->___mValue_3 = L_0;
		return;
	}
}
// System.Boolean ExifLibrary.ExifEnumProperty`1<ExifLibrary.WhiteBalance>::get_IsBitField()
extern "C" bool ExifEnumProperty_1_get_IsBitField_m8_2221_gshared (ExifEnumProperty_1_t8_354 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(__this->___mIsBitField_4);
		return L_0;
	}
}
// System.String ExifLibrary.ExifEnumProperty`1<ExifLibrary.WhiteBalance>::ToString()
extern "C" String_t* ExifEnumProperty_1_ToString_m8_2222_gshared (ExifEnumProperty_1_t8_354 * __this, const MethodInfo* method)
{
	{
		uint16_t* L_0 = (uint16_t*)&(__this->___mValue_3);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		return L_1;
	}
}
// ExifLibrary.ExifInterOperability ExifLibrary.ExifEnumProperty`1<ExifLibrary.WhiteBalance>::get_Interoperability()
extern const Il2CppType* FileSource_t8_75_0_0_0_var;
extern const Il2CppType* SceneType_t8_76_0_0_0_var;
extern const Il2CppType* GPSLatitudeRef_t8_86_0_0_0_var;
extern const Il2CppType* GPSLongitudeRef_t8_87_0_0_0_var;
extern const Il2CppType* GPSStatus_t8_89_0_0_0_var;
extern const Il2CppType* GPSMeasureMode_t8_90_0_0_0_var;
extern const Il2CppType* GPSSpeedRef_t8_91_0_0_0_var;
extern const Il2CppType* GPSDirectionRef_t8_92_0_0_0_var;
extern const Il2CppType* GPSDistanceRef_t8_93_0_0_0_var;
extern const Il2CppType* Byte_t1_11_0_0_0_var;
extern const Il2CppType* UInt16_t1_14_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Enum_t1_24_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern TypeInfo* UInt16_t1_14_il2cpp_TypeInfo_var;
extern TypeInfo* UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var;
extern "C" ExifInterOperability_t8_113  ExifEnumProperty_1_get_Interoperability_m8_2223_gshared (ExifEnumProperty_1_t8_354 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FileSource_t8_75_0_0_0_var = il2cpp_codegen_type_from_index(2003);
		SceneType_t8_76_0_0_0_var = il2cpp_codegen_type_from_index(2004);
		GPSLatitudeRef_t8_86_0_0_0_var = il2cpp_codegen_type_from_index(2014);
		GPSLongitudeRef_t8_87_0_0_0_var = il2cpp_codegen_type_from_index(2015);
		GPSStatus_t8_89_0_0_0_var = il2cpp_codegen_type_from_index(2017);
		GPSMeasureMode_t8_90_0_0_0_var = il2cpp_codegen_type_from_index(2018);
		GPSSpeedRef_t8_91_0_0_0_var = il2cpp_codegen_type_from_index(2019);
		GPSDirectionRef_t8_92_0_0_0_var = il2cpp_codegen_type_from_index(2020);
		GPSDistanceRef_t8_93_0_0_0_var = il2cpp_codegen_type_from_index(2021);
		Byte_t1_11_0_0_0_var = il2cpp_codegen_type_from_index(25);
		UInt16_t1_14_0_0_0_var = il2cpp_codegen_type_from_index(29);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Enum_t1_24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		UInt16_t1_14_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4656);
		s_Il2CppMethodIntialized = true;
	}
	uint16_t V_0 = 0;
	Type_t * V_1 = {0};
	Type_t * V_2 = {0};
	{
		int32_t L_0 = (int32_t)(((ExifProperty_t8_99 *)__this)->___mTag_0);
		uint16_t L_1 = ExifTagFactory_GetTagID_m8_630(NULL /*static, unused*/, (int32_t)L_0, /*hidden argument*/NULL);
		V_0 = (uint16_t)L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		V_1 = (Type_t *)L_2;
		Type_t * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t1_24_il2cpp_TypeInfo_var);
		Type_t * L_4 = Enum_GetUnderlyingType_m1_942(NULL /*static, unused*/, (Type_t *)L_3, /*hidden argument*/NULL);
		V_2 = (Type_t *)L_4;
		Type_t * L_5 = V_1;
		Type_t * L_6 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(FileSource_t8_75_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_5) == ((Object_t*)(Type_t *)L_6)))
		{
			goto IL_003e;
		}
	}
	{
		Type_t * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(SceneType_t8_76_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_7) == ((Object_t*)(Type_t *)L_8))))
		{
			goto IL_0060;
		}
	}

IL_003e:
	{
		uint16_t L_9 = V_0;
		ByteU5BU5D_t1_109* L_10 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint16_t L_11 = (uint16_t)(__this->___mValue_3);
		uint16_t L_12 = L_11;
		Object_t * L_13 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_12);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_10, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_13, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_14 = {0};
		ExifInterOperability__ctor_m8_496(&L_14, (uint16_t)L_9, (uint16_t)7, (uint32_t)1, (ByteU5BU5D_t1_109*)L_10, /*hidden argument*/NULL);
		return L_14;
	}

IL_0060:
	{
		Type_t * L_15 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_16 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLatitudeRef_t8_86_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_15) == ((Object_t*)(Type_t *)L_16)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_17 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_18 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLongitudeRef_t8_87_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_17) == ((Object_t*)(Type_t *)L_18)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_19 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_20 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSStatus_t8_89_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_19) == ((Object_t*)(Type_t *)L_20)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_21 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_22 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSMeasureMode_t8_90_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_21) == ((Object_t*)(Type_t *)L_22)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_23 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_24 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSSpeedRef_t8_91_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_23) == ((Object_t*)(Type_t *)L_24)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_25 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_26 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDirectionRef_t8_92_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_25) == ((Object_t*)(Type_t *)L_26)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_27 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_28 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDistanceRef_t8_93_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_27) == ((Object_t*)(Type_t *)L_28))))
		{
			goto IL_00f2;
		}
	}

IL_00d0:
	{
		uint16_t L_29 = V_0;
		ByteU5BU5D_t1_109* L_30 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 2));
		uint16_t L_31 = (uint16_t)(__this->___mValue_3);
		uint16_t L_32 = L_31;
		Object_t * L_33 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_32);
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_30, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_33, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_34 = {0};
		ExifInterOperability__ctor_m8_496(&L_34, (uint16_t)L_29, (uint16_t)2, (uint32_t)2, (ByteU5BU5D_t1_109*)L_30, /*hidden argument*/NULL);
		return L_34;
	}

IL_00f2:
	{
		Type_t * L_35 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(Byte_t1_11_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_35) == ((Object_t*)(Type_t *)L_36))))
		{
			goto IL_0124;
		}
	}
	{
		uint16_t L_37 = V_0;
		ByteU5BU5D_t1_109* L_38 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint16_t L_39 = (uint16_t)(__this->___mValue_3);
		uint16_t L_40 = L_39;
		Object_t * L_41 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_40);
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_38, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_41, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_42 = {0};
		ExifInterOperability__ctor_m8_496(&L_42, (uint16_t)L_37, (uint16_t)1, (uint32_t)1, (ByteU5BU5D_t1_109*)L_38, /*hidden argument*/NULL);
		return L_42;
	}

IL_0124:
	{
		Type_t * L_43 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_44 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(UInt16_t1_14_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_43) == ((Object_t*)(Type_t *)L_44))))
		{
			goto IL_0154;
		}
	}
	{
		uint16_t L_45 = V_0;
		uint16_t L_46 = (uint16_t)(__this->___mValue_3);
		uint16_t L_47 = L_46;
		Object_t * L_48 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_47);
		ByteU5BU5D_t1_109* L_49 = BitConverterEx_GetBytes_m8_354(NULL /*static, unused*/, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_48, UInt16_t1_14_il2cpp_TypeInfo_var)))), (int32_t)0, (int32_t)0, /*hidden argument*/NULL);
		ExifInterOperability_t8_113  L_50 = {0};
		ExifInterOperability__ctor_m8_496(&L_50, (uint16_t)L_45, (uint16_t)3, (uint32_t)1, (ByteU5BU5D_t1_109*)L_49, /*hidden argument*/NULL);
		return L_50;
	}

IL_0154:
	{
		UnknownEnumTypeException_t8_96 * L_51 = (UnknownEnumTypeException_t8_96 *)il2cpp_codegen_object_new (UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var);
		UnknownEnumTypeException__ctor_m8_407(L_51, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_51);
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.WhiteBalance>::op_Implicit(ExifLibrary.ExifEnumProperty`1<T>)
extern "C" uint16_t ExifEnumProperty_1_op_Implicit_m8_2224_gshared (Object_t * __this /* static, unused */, ExifEnumProperty_1_t8_354 * ___obj, const MethodInfo* method)
{
	{
		ExifEnumProperty_1_t8_354 * L_0 = ___obj;
		NullCheck(L_0);
		uint16_t L_1 = (uint16_t)(L_0->___mValue_3);
		return L_1;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.SceneCaptureType>::.ctor(ExifLibrary.ExifTag,T,System.Boolean)
extern "C" void ExifEnumProperty_1__ctor_m8_1992_gshared (ExifEnumProperty_1_t8_355 * __this, int32_t ___tag, uint16_t ___value, bool ___isbitfield, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		NullCheck((ExifProperty_t8_99 *)__this);
		ExifProperty__ctor_m8_502((ExifProperty_t8_99 *)__this, (int32_t)L_0, /*hidden argument*/NULL);
		uint16_t L_1 = ___value;
		__this->___mValue_3 = L_1;
		bool L_2 = ___isbitfield;
		__this->___mIsBitField_4 = L_2;
		return;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.SceneCaptureType>::.ctor(ExifLibrary.ExifTag,T)
extern "C" void ExifEnumProperty_1__ctor_m8_2225_gshared (ExifEnumProperty_1_t8_355 * __this, int32_t ___tag, uint16_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		uint16_t L_1 = ___value;
		NullCheck((ExifEnumProperty_1_t8_355 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_355 *, int32_t, uint16_t, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ExifEnumProperty_1_t8_355 *)__this, (int32_t)L_0, (uint16_t)L_1, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Object ExifLibrary.ExifEnumProperty`1<ExifLibrary.SceneCaptureType>::get__Value()
extern "C" Object_t * ExifEnumProperty_1_get__Value_m8_2226_gshared (ExifEnumProperty_1_t8_355 * __this, const MethodInfo* method)
{
	{
		NullCheck((ExifEnumProperty_1_t8_355 *)__this);
		uint16_t L_0 = (( uint16_t (*) (ExifEnumProperty_1_t8_355 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ExifEnumProperty_1_t8_355 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		uint16_t L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.SceneCaptureType>::set__Value(System.Object)
extern "C" void ExifEnumProperty_1_set__Value_m8_2227_gshared (ExifEnumProperty_1_t8_355 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		NullCheck((ExifEnumProperty_1_t8_355 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_355 *, uint16_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((ExifEnumProperty_1_t8_355 *)__this, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.SceneCaptureType>::get_Value()
extern "C" uint16_t ExifEnumProperty_1_get_Value_m8_2228_gshared (ExifEnumProperty_1_t8_355 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = (uint16_t)(__this->___mValue_3);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.SceneCaptureType>::set_Value(T)
extern "C" void ExifEnumProperty_1_set_Value_m8_2229_gshared (ExifEnumProperty_1_t8_355 * __this, uint16_t ___value, const MethodInfo* method)
{
	{
		uint16_t L_0 = ___value;
		__this->___mValue_3 = L_0;
		return;
	}
}
// System.Boolean ExifLibrary.ExifEnumProperty`1<ExifLibrary.SceneCaptureType>::get_IsBitField()
extern "C" bool ExifEnumProperty_1_get_IsBitField_m8_2230_gshared (ExifEnumProperty_1_t8_355 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(__this->___mIsBitField_4);
		return L_0;
	}
}
// System.String ExifLibrary.ExifEnumProperty`1<ExifLibrary.SceneCaptureType>::ToString()
extern "C" String_t* ExifEnumProperty_1_ToString_m8_2231_gshared (ExifEnumProperty_1_t8_355 * __this, const MethodInfo* method)
{
	{
		uint16_t* L_0 = (uint16_t*)&(__this->___mValue_3);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		return L_1;
	}
}
// ExifLibrary.ExifInterOperability ExifLibrary.ExifEnumProperty`1<ExifLibrary.SceneCaptureType>::get_Interoperability()
extern const Il2CppType* FileSource_t8_75_0_0_0_var;
extern const Il2CppType* SceneType_t8_76_0_0_0_var;
extern const Il2CppType* GPSLatitudeRef_t8_86_0_0_0_var;
extern const Il2CppType* GPSLongitudeRef_t8_87_0_0_0_var;
extern const Il2CppType* GPSStatus_t8_89_0_0_0_var;
extern const Il2CppType* GPSMeasureMode_t8_90_0_0_0_var;
extern const Il2CppType* GPSSpeedRef_t8_91_0_0_0_var;
extern const Il2CppType* GPSDirectionRef_t8_92_0_0_0_var;
extern const Il2CppType* GPSDistanceRef_t8_93_0_0_0_var;
extern const Il2CppType* Byte_t1_11_0_0_0_var;
extern const Il2CppType* UInt16_t1_14_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Enum_t1_24_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern TypeInfo* UInt16_t1_14_il2cpp_TypeInfo_var;
extern TypeInfo* UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var;
extern "C" ExifInterOperability_t8_113  ExifEnumProperty_1_get_Interoperability_m8_2232_gshared (ExifEnumProperty_1_t8_355 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FileSource_t8_75_0_0_0_var = il2cpp_codegen_type_from_index(2003);
		SceneType_t8_76_0_0_0_var = il2cpp_codegen_type_from_index(2004);
		GPSLatitudeRef_t8_86_0_0_0_var = il2cpp_codegen_type_from_index(2014);
		GPSLongitudeRef_t8_87_0_0_0_var = il2cpp_codegen_type_from_index(2015);
		GPSStatus_t8_89_0_0_0_var = il2cpp_codegen_type_from_index(2017);
		GPSMeasureMode_t8_90_0_0_0_var = il2cpp_codegen_type_from_index(2018);
		GPSSpeedRef_t8_91_0_0_0_var = il2cpp_codegen_type_from_index(2019);
		GPSDirectionRef_t8_92_0_0_0_var = il2cpp_codegen_type_from_index(2020);
		GPSDistanceRef_t8_93_0_0_0_var = il2cpp_codegen_type_from_index(2021);
		Byte_t1_11_0_0_0_var = il2cpp_codegen_type_from_index(25);
		UInt16_t1_14_0_0_0_var = il2cpp_codegen_type_from_index(29);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Enum_t1_24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		UInt16_t1_14_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4656);
		s_Il2CppMethodIntialized = true;
	}
	uint16_t V_0 = 0;
	Type_t * V_1 = {0};
	Type_t * V_2 = {0};
	{
		int32_t L_0 = (int32_t)(((ExifProperty_t8_99 *)__this)->___mTag_0);
		uint16_t L_1 = ExifTagFactory_GetTagID_m8_630(NULL /*static, unused*/, (int32_t)L_0, /*hidden argument*/NULL);
		V_0 = (uint16_t)L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		V_1 = (Type_t *)L_2;
		Type_t * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t1_24_il2cpp_TypeInfo_var);
		Type_t * L_4 = Enum_GetUnderlyingType_m1_942(NULL /*static, unused*/, (Type_t *)L_3, /*hidden argument*/NULL);
		V_2 = (Type_t *)L_4;
		Type_t * L_5 = V_1;
		Type_t * L_6 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(FileSource_t8_75_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_5) == ((Object_t*)(Type_t *)L_6)))
		{
			goto IL_003e;
		}
	}
	{
		Type_t * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(SceneType_t8_76_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_7) == ((Object_t*)(Type_t *)L_8))))
		{
			goto IL_0060;
		}
	}

IL_003e:
	{
		uint16_t L_9 = V_0;
		ByteU5BU5D_t1_109* L_10 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint16_t L_11 = (uint16_t)(__this->___mValue_3);
		uint16_t L_12 = L_11;
		Object_t * L_13 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_12);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_10, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_13, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_14 = {0};
		ExifInterOperability__ctor_m8_496(&L_14, (uint16_t)L_9, (uint16_t)7, (uint32_t)1, (ByteU5BU5D_t1_109*)L_10, /*hidden argument*/NULL);
		return L_14;
	}

IL_0060:
	{
		Type_t * L_15 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_16 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLatitudeRef_t8_86_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_15) == ((Object_t*)(Type_t *)L_16)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_17 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_18 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLongitudeRef_t8_87_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_17) == ((Object_t*)(Type_t *)L_18)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_19 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_20 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSStatus_t8_89_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_19) == ((Object_t*)(Type_t *)L_20)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_21 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_22 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSMeasureMode_t8_90_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_21) == ((Object_t*)(Type_t *)L_22)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_23 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_24 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSSpeedRef_t8_91_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_23) == ((Object_t*)(Type_t *)L_24)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_25 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_26 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDirectionRef_t8_92_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_25) == ((Object_t*)(Type_t *)L_26)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_27 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_28 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDistanceRef_t8_93_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_27) == ((Object_t*)(Type_t *)L_28))))
		{
			goto IL_00f2;
		}
	}

IL_00d0:
	{
		uint16_t L_29 = V_0;
		ByteU5BU5D_t1_109* L_30 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 2));
		uint16_t L_31 = (uint16_t)(__this->___mValue_3);
		uint16_t L_32 = L_31;
		Object_t * L_33 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_32);
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_30, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_33, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_34 = {0};
		ExifInterOperability__ctor_m8_496(&L_34, (uint16_t)L_29, (uint16_t)2, (uint32_t)2, (ByteU5BU5D_t1_109*)L_30, /*hidden argument*/NULL);
		return L_34;
	}

IL_00f2:
	{
		Type_t * L_35 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(Byte_t1_11_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_35) == ((Object_t*)(Type_t *)L_36))))
		{
			goto IL_0124;
		}
	}
	{
		uint16_t L_37 = V_0;
		ByteU5BU5D_t1_109* L_38 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint16_t L_39 = (uint16_t)(__this->___mValue_3);
		uint16_t L_40 = L_39;
		Object_t * L_41 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_40);
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_38, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_41, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_42 = {0};
		ExifInterOperability__ctor_m8_496(&L_42, (uint16_t)L_37, (uint16_t)1, (uint32_t)1, (ByteU5BU5D_t1_109*)L_38, /*hidden argument*/NULL);
		return L_42;
	}

IL_0124:
	{
		Type_t * L_43 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_44 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(UInt16_t1_14_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_43) == ((Object_t*)(Type_t *)L_44))))
		{
			goto IL_0154;
		}
	}
	{
		uint16_t L_45 = V_0;
		uint16_t L_46 = (uint16_t)(__this->___mValue_3);
		uint16_t L_47 = L_46;
		Object_t * L_48 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_47);
		ByteU5BU5D_t1_109* L_49 = BitConverterEx_GetBytes_m8_354(NULL /*static, unused*/, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_48, UInt16_t1_14_il2cpp_TypeInfo_var)))), (int32_t)0, (int32_t)0, /*hidden argument*/NULL);
		ExifInterOperability_t8_113  L_50 = {0};
		ExifInterOperability__ctor_m8_496(&L_50, (uint16_t)L_45, (uint16_t)3, (uint32_t)1, (ByteU5BU5D_t1_109*)L_49, /*hidden argument*/NULL);
		return L_50;
	}

IL_0154:
	{
		UnknownEnumTypeException_t8_96 * L_51 = (UnknownEnumTypeException_t8_96 *)il2cpp_codegen_object_new (UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var);
		UnknownEnumTypeException__ctor_m8_407(L_51, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_51);
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.SceneCaptureType>::op_Implicit(ExifLibrary.ExifEnumProperty`1<T>)
extern "C" uint16_t ExifEnumProperty_1_op_Implicit_m8_2233_gshared (Object_t * __this /* static, unused */, ExifEnumProperty_1_t8_355 * ___obj, const MethodInfo* method)
{
	{
		ExifEnumProperty_1_t8_355 * L_0 = ___obj;
		NullCheck(L_0);
		uint16_t L_1 = (uint16_t)(L_0->___mValue_3);
		return L_1;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GainControl>::.ctor(ExifLibrary.ExifTag,T,System.Boolean)
extern "C" void ExifEnumProperty_1__ctor_m8_1993_gshared (ExifEnumProperty_1_t8_356 * __this, int32_t ___tag, uint16_t ___value, bool ___isbitfield, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		NullCheck((ExifProperty_t8_99 *)__this);
		ExifProperty__ctor_m8_502((ExifProperty_t8_99 *)__this, (int32_t)L_0, /*hidden argument*/NULL);
		uint16_t L_1 = ___value;
		__this->___mValue_3 = L_1;
		bool L_2 = ___isbitfield;
		__this->___mIsBitField_4 = L_2;
		return;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GainControl>::.ctor(ExifLibrary.ExifTag,T)
extern "C" void ExifEnumProperty_1__ctor_m8_2234_gshared (ExifEnumProperty_1_t8_356 * __this, int32_t ___tag, uint16_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		uint16_t L_1 = ___value;
		NullCheck((ExifEnumProperty_1_t8_356 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_356 *, int32_t, uint16_t, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ExifEnumProperty_1_t8_356 *)__this, (int32_t)L_0, (uint16_t)L_1, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Object ExifLibrary.ExifEnumProperty`1<ExifLibrary.GainControl>::get__Value()
extern "C" Object_t * ExifEnumProperty_1_get__Value_m8_2235_gshared (ExifEnumProperty_1_t8_356 * __this, const MethodInfo* method)
{
	{
		NullCheck((ExifEnumProperty_1_t8_356 *)__this);
		uint16_t L_0 = (( uint16_t (*) (ExifEnumProperty_1_t8_356 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ExifEnumProperty_1_t8_356 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		uint16_t L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GainControl>::set__Value(System.Object)
extern "C" void ExifEnumProperty_1_set__Value_m8_2236_gshared (ExifEnumProperty_1_t8_356 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		NullCheck((ExifEnumProperty_1_t8_356 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_356 *, uint16_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((ExifEnumProperty_1_t8_356 *)__this, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.GainControl>::get_Value()
extern "C" uint16_t ExifEnumProperty_1_get_Value_m8_2237_gshared (ExifEnumProperty_1_t8_356 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = (uint16_t)(__this->___mValue_3);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GainControl>::set_Value(T)
extern "C" void ExifEnumProperty_1_set_Value_m8_2238_gshared (ExifEnumProperty_1_t8_356 * __this, uint16_t ___value, const MethodInfo* method)
{
	{
		uint16_t L_0 = ___value;
		__this->___mValue_3 = L_0;
		return;
	}
}
// System.Boolean ExifLibrary.ExifEnumProperty`1<ExifLibrary.GainControl>::get_IsBitField()
extern "C" bool ExifEnumProperty_1_get_IsBitField_m8_2239_gshared (ExifEnumProperty_1_t8_356 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(__this->___mIsBitField_4);
		return L_0;
	}
}
// System.String ExifLibrary.ExifEnumProperty`1<ExifLibrary.GainControl>::ToString()
extern "C" String_t* ExifEnumProperty_1_ToString_m8_2240_gshared (ExifEnumProperty_1_t8_356 * __this, const MethodInfo* method)
{
	{
		uint16_t* L_0 = (uint16_t*)&(__this->___mValue_3);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		return L_1;
	}
}
// ExifLibrary.ExifInterOperability ExifLibrary.ExifEnumProperty`1<ExifLibrary.GainControl>::get_Interoperability()
extern const Il2CppType* FileSource_t8_75_0_0_0_var;
extern const Il2CppType* SceneType_t8_76_0_0_0_var;
extern const Il2CppType* GPSLatitudeRef_t8_86_0_0_0_var;
extern const Il2CppType* GPSLongitudeRef_t8_87_0_0_0_var;
extern const Il2CppType* GPSStatus_t8_89_0_0_0_var;
extern const Il2CppType* GPSMeasureMode_t8_90_0_0_0_var;
extern const Il2CppType* GPSSpeedRef_t8_91_0_0_0_var;
extern const Il2CppType* GPSDirectionRef_t8_92_0_0_0_var;
extern const Il2CppType* GPSDistanceRef_t8_93_0_0_0_var;
extern const Il2CppType* Byte_t1_11_0_0_0_var;
extern const Il2CppType* UInt16_t1_14_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Enum_t1_24_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern TypeInfo* UInt16_t1_14_il2cpp_TypeInfo_var;
extern TypeInfo* UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var;
extern "C" ExifInterOperability_t8_113  ExifEnumProperty_1_get_Interoperability_m8_2241_gshared (ExifEnumProperty_1_t8_356 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FileSource_t8_75_0_0_0_var = il2cpp_codegen_type_from_index(2003);
		SceneType_t8_76_0_0_0_var = il2cpp_codegen_type_from_index(2004);
		GPSLatitudeRef_t8_86_0_0_0_var = il2cpp_codegen_type_from_index(2014);
		GPSLongitudeRef_t8_87_0_0_0_var = il2cpp_codegen_type_from_index(2015);
		GPSStatus_t8_89_0_0_0_var = il2cpp_codegen_type_from_index(2017);
		GPSMeasureMode_t8_90_0_0_0_var = il2cpp_codegen_type_from_index(2018);
		GPSSpeedRef_t8_91_0_0_0_var = il2cpp_codegen_type_from_index(2019);
		GPSDirectionRef_t8_92_0_0_0_var = il2cpp_codegen_type_from_index(2020);
		GPSDistanceRef_t8_93_0_0_0_var = il2cpp_codegen_type_from_index(2021);
		Byte_t1_11_0_0_0_var = il2cpp_codegen_type_from_index(25);
		UInt16_t1_14_0_0_0_var = il2cpp_codegen_type_from_index(29);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Enum_t1_24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		UInt16_t1_14_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4656);
		s_Il2CppMethodIntialized = true;
	}
	uint16_t V_0 = 0;
	Type_t * V_1 = {0};
	Type_t * V_2 = {0};
	{
		int32_t L_0 = (int32_t)(((ExifProperty_t8_99 *)__this)->___mTag_0);
		uint16_t L_1 = ExifTagFactory_GetTagID_m8_630(NULL /*static, unused*/, (int32_t)L_0, /*hidden argument*/NULL);
		V_0 = (uint16_t)L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		V_1 = (Type_t *)L_2;
		Type_t * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t1_24_il2cpp_TypeInfo_var);
		Type_t * L_4 = Enum_GetUnderlyingType_m1_942(NULL /*static, unused*/, (Type_t *)L_3, /*hidden argument*/NULL);
		V_2 = (Type_t *)L_4;
		Type_t * L_5 = V_1;
		Type_t * L_6 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(FileSource_t8_75_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_5) == ((Object_t*)(Type_t *)L_6)))
		{
			goto IL_003e;
		}
	}
	{
		Type_t * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(SceneType_t8_76_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_7) == ((Object_t*)(Type_t *)L_8))))
		{
			goto IL_0060;
		}
	}

IL_003e:
	{
		uint16_t L_9 = V_0;
		ByteU5BU5D_t1_109* L_10 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint16_t L_11 = (uint16_t)(__this->___mValue_3);
		uint16_t L_12 = L_11;
		Object_t * L_13 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_12);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_10, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_13, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_14 = {0};
		ExifInterOperability__ctor_m8_496(&L_14, (uint16_t)L_9, (uint16_t)7, (uint32_t)1, (ByteU5BU5D_t1_109*)L_10, /*hidden argument*/NULL);
		return L_14;
	}

IL_0060:
	{
		Type_t * L_15 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_16 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLatitudeRef_t8_86_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_15) == ((Object_t*)(Type_t *)L_16)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_17 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_18 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLongitudeRef_t8_87_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_17) == ((Object_t*)(Type_t *)L_18)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_19 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_20 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSStatus_t8_89_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_19) == ((Object_t*)(Type_t *)L_20)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_21 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_22 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSMeasureMode_t8_90_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_21) == ((Object_t*)(Type_t *)L_22)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_23 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_24 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSSpeedRef_t8_91_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_23) == ((Object_t*)(Type_t *)L_24)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_25 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_26 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDirectionRef_t8_92_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_25) == ((Object_t*)(Type_t *)L_26)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_27 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_28 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDistanceRef_t8_93_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_27) == ((Object_t*)(Type_t *)L_28))))
		{
			goto IL_00f2;
		}
	}

IL_00d0:
	{
		uint16_t L_29 = V_0;
		ByteU5BU5D_t1_109* L_30 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 2));
		uint16_t L_31 = (uint16_t)(__this->___mValue_3);
		uint16_t L_32 = L_31;
		Object_t * L_33 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_32);
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_30, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_33, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_34 = {0};
		ExifInterOperability__ctor_m8_496(&L_34, (uint16_t)L_29, (uint16_t)2, (uint32_t)2, (ByteU5BU5D_t1_109*)L_30, /*hidden argument*/NULL);
		return L_34;
	}

IL_00f2:
	{
		Type_t * L_35 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(Byte_t1_11_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_35) == ((Object_t*)(Type_t *)L_36))))
		{
			goto IL_0124;
		}
	}
	{
		uint16_t L_37 = V_0;
		ByteU5BU5D_t1_109* L_38 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint16_t L_39 = (uint16_t)(__this->___mValue_3);
		uint16_t L_40 = L_39;
		Object_t * L_41 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_40);
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_38, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_41, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_42 = {0};
		ExifInterOperability__ctor_m8_496(&L_42, (uint16_t)L_37, (uint16_t)1, (uint32_t)1, (ByteU5BU5D_t1_109*)L_38, /*hidden argument*/NULL);
		return L_42;
	}

IL_0124:
	{
		Type_t * L_43 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_44 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(UInt16_t1_14_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_43) == ((Object_t*)(Type_t *)L_44))))
		{
			goto IL_0154;
		}
	}
	{
		uint16_t L_45 = V_0;
		uint16_t L_46 = (uint16_t)(__this->___mValue_3);
		uint16_t L_47 = L_46;
		Object_t * L_48 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_47);
		ByteU5BU5D_t1_109* L_49 = BitConverterEx_GetBytes_m8_354(NULL /*static, unused*/, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_48, UInt16_t1_14_il2cpp_TypeInfo_var)))), (int32_t)0, (int32_t)0, /*hidden argument*/NULL);
		ExifInterOperability_t8_113  L_50 = {0};
		ExifInterOperability__ctor_m8_496(&L_50, (uint16_t)L_45, (uint16_t)3, (uint32_t)1, (ByteU5BU5D_t1_109*)L_49, /*hidden argument*/NULL);
		return L_50;
	}

IL_0154:
	{
		UnknownEnumTypeException_t8_96 * L_51 = (UnknownEnumTypeException_t8_96 *)il2cpp_codegen_object_new (UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var);
		UnknownEnumTypeException__ctor_m8_407(L_51, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_51);
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.GainControl>::op_Implicit(ExifLibrary.ExifEnumProperty`1<T>)
extern "C" uint16_t ExifEnumProperty_1_op_Implicit_m8_2242_gshared (Object_t * __this /* static, unused */, ExifEnumProperty_1_t8_356 * ___obj, const MethodInfo* method)
{
	{
		ExifEnumProperty_1_t8_356 * L_0 = ___obj;
		NullCheck(L_0);
		uint16_t L_1 = (uint16_t)(L_0->___mValue_3);
		return L_1;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.Contrast>::.ctor(ExifLibrary.ExifTag,T,System.Boolean)
extern "C" void ExifEnumProperty_1__ctor_m8_1994_gshared (ExifEnumProperty_1_t8_357 * __this, int32_t ___tag, uint16_t ___value, bool ___isbitfield, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		NullCheck((ExifProperty_t8_99 *)__this);
		ExifProperty__ctor_m8_502((ExifProperty_t8_99 *)__this, (int32_t)L_0, /*hidden argument*/NULL);
		uint16_t L_1 = ___value;
		__this->___mValue_3 = L_1;
		bool L_2 = ___isbitfield;
		__this->___mIsBitField_4 = L_2;
		return;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.Contrast>::.ctor(ExifLibrary.ExifTag,T)
extern "C" void ExifEnumProperty_1__ctor_m8_2243_gshared (ExifEnumProperty_1_t8_357 * __this, int32_t ___tag, uint16_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		uint16_t L_1 = ___value;
		NullCheck((ExifEnumProperty_1_t8_357 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_357 *, int32_t, uint16_t, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ExifEnumProperty_1_t8_357 *)__this, (int32_t)L_0, (uint16_t)L_1, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Object ExifLibrary.ExifEnumProperty`1<ExifLibrary.Contrast>::get__Value()
extern "C" Object_t * ExifEnumProperty_1_get__Value_m8_2244_gshared (ExifEnumProperty_1_t8_357 * __this, const MethodInfo* method)
{
	{
		NullCheck((ExifEnumProperty_1_t8_357 *)__this);
		uint16_t L_0 = (( uint16_t (*) (ExifEnumProperty_1_t8_357 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ExifEnumProperty_1_t8_357 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		uint16_t L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.Contrast>::set__Value(System.Object)
extern "C" void ExifEnumProperty_1_set__Value_m8_2245_gshared (ExifEnumProperty_1_t8_357 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		NullCheck((ExifEnumProperty_1_t8_357 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_357 *, uint16_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((ExifEnumProperty_1_t8_357 *)__this, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.Contrast>::get_Value()
extern "C" uint16_t ExifEnumProperty_1_get_Value_m8_2246_gshared (ExifEnumProperty_1_t8_357 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = (uint16_t)(__this->___mValue_3);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.Contrast>::set_Value(T)
extern "C" void ExifEnumProperty_1_set_Value_m8_2247_gshared (ExifEnumProperty_1_t8_357 * __this, uint16_t ___value, const MethodInfo* method)
{
	{
		uint16_t L_0 = ___value;
		__this->___mValue_3 = L_0;
		return;
	}
}
// System.Boolean ExifLibrary.ExifEnumProperty`1<ExifLibrary.Contrast>::get_IsBitField()
extern "C" bool ExifEnumProperty_1_get_IsBitField_m8_2248_gshared (ExifEnumProperty_1_t8_357 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(__this->___mIsBitField_4);
		return L_0;
	}
}
// System.String ExifLibrary.ExifEnumProperty`1<ExifLibrary.Contrast>::ToString()
extern "C" String_t* ExifEnumProperty_1_ToString_m8_2249_gshared (ExifEnumProperty_1_t8_357 * __this, const MethodInfo* method)
{
	{
		uint16_t* L_0 = (uint16_t*)&(__this->___mValue_3);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		return L_1;
	}
}
// ExifLibrary.ExifInterOperability ExifLibrary.ExifEnumProperty`1<ExifLibrary.Contrast>::get_Interoperability()
extern const Il2CppType* FileSource_t8_75_0_0_0_var;
extern const Il2CppType* SceneType_t8_76_0_0_0_var;
extern const Il2CppType* GPSLatitudeRef_t8_86_0_0_0_var;
extern const Il2CppType* GPSLongitudeRef_t8_87_0_0_0_var;
extern const Il2CppType* GPSStatus_t8_89_0_0_0_var;
extern const Il2CppType* GPSMeasureMode_t8_90_0_0_0_var;
extern const Il2CppType* GPSSpeedRef_t8_91_0_0_0_var;
extern const Il2CppType* GPSDirectionRef_t8_92_0_0_0_var;
extern const Il2CppType* GPSDistanceRef_t8_93_0_0_0_var;
extern const Il2CppType* Byte_t1_11_0_0_0_var;
extern const Il2CppType* UInt16_t1_14_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Enum_t1_24_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern TypeInfo* UInt16_t1_14_il2cpp_TypeInfo_var;
extern TypeInfo* UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var;
extern "C" ExifInterOperability_t8_113  ExifEnumProperty_1_get_Interoperability_m8_2250_gshared (ExifEnumProperty_1_t8_357 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FileSource_t8_75_0_0_0_var = il2cpp_codegen_type_from_index(2003);
		SceneType_t8_76_0_0_0_var = il2cpp_codegen_type_from_index(2004);
		GPSLatitudeRef_t8_86_0_0_0_var = il2cpp_codegen_type_from_index(2014);
		GPSLongitudeRef_t8_87_0_0_0_var = il2cpp_codegen_type_from_index(2015);
		GPSStatus_t8_89_0_0_0_var = il2cpp_codegen_type_from_index(2017);
		GPSMeasureMode_t8_90_0_0_0_var = il2cpp_codegen_type_from_index(2018);
		GPSSpeedRef_t8_91_0_0_0_var = il2cpp_codegen_type_from_index(2019);
		GPSDirectionRef_t8_92_0_0_0_var = il2cpp_codegen_type_from_index(2020);
		GPSDistanceRef_t8_93_0_0_0_var = il2cpp_codegen_type_from_index(2021);
		Byte_t1_11_0_0_0_var = il2cpp_codegen_type_from_index(25);
		UInt16_t1_14_0_0_0_var = il2cpp_codegen_type_from_index(29);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Enum_t1_24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		UInt16_t1_14_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4656);
		s_Il2CppMethodIntialized = true;
	}
	uint16_t V_0 = 0;
	Type_t * V_1 = {0};
	Type_t * V_2 = {0};
	{
		int32_t L_0 = (int32_t)(((ExifProperty_t8_99 *)__this)->___mTag_0);
		uint16_t L_1 = ExifTagFactory_GetTagID_m8_630(NULL /*static, unused*/, (int32_t)L_0, /*hidden argument*/NULL);
		V_0 = (uint16_t)L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		V_1 = (Type_t *)L_2;
		Type_t * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t1_24_il2cpp_TypeInfo_var);
		Type_t * L_4 = Enum_GetUnderlyingType_m1_942(NULL /*static, unused*/, (Type_t *)L_3, /*hidden argument*/NULL);
		V_2 = (Type_t *)L_4;
		Type_t * L_5 = V_1;
		Type_t * L_6 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(FileSource_t8_75_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_5) == ((Object_t*)(Type_t *)L_6)))
		{
			goto IL_003e;
		}
	}
	{
		Type_t * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(SceneType_t8_76_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_7) == ((Object_t*)(Type_t *)L_8))))
		{
			goto IL_0060;
		}
	}

IL_003e:
	{
		uint16_t L_9 = V_0;
		ByteU5BU5D_t1_109* L_10 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint16_t L_11 = (uint16_t)(__this->___mValue_3);
		uint16_t L_12 = L_11;
		Object_t * L_13 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_12);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_10, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_13, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_14 = {0};
		ExifInterOperability__ctor_m8_496(&L_14, (uint16_t)L_9, (uint16_t)7, (uint32_t)1, (ByteU5BU5D_t1_109*)L_10, /*hidden argument*/NULL);
		return L_14;
	}

IL_0060:
	{
		Type_t * L_15 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_16 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLatitudeRef_t8_86_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_15) == ((Object_t*)(Type_t *)L_16)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_17 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_18 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLongitudeRef_t8_87_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_17) == ((Object_t*)(Type_t *)L_18)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_19 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_20 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSStatus_t8_89_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_19) == ((Object_t*)(Type_t *)L_20)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_21 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_22 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSMeasureMode_t8_90_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_21) == ((Object_t*)(Type_t *)L_22)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_23 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_24 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSSpeedRef_t8_91_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_23) == ((Object_t*)(Type_t *)L_24)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_25 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_26 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDirectionRef_t8_92_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_25) == ((Object_t*)(Type_t *)L_26)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_27 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_28 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDistanceRef_t8_93_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_27) == ((Object_t*)(Type_t *)L_28))))
		{
			goto IL_00f2;
		}
	}

IL_00d0:
	{
		uint16_t L_29 = V_0;
		ByteU5BU5D_t1_109* L_30 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 2));
		uint16_t L_31 = (uint16_t)(__this->___mValue_3);
		uint16_t L_32 = L_31;
		Object_t * L_33 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_32);
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_30, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_33, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_34 = {0};
		ExifInterOperability__ctor_m8_496(&L_34, (uint16_t)L_29, (uint16_t)2, (uint32_t)2, (ByteU5BU5D_t1_109*)L_30, /*hidden argument*/NULL);
		return L_34;
	}

IL_00f2:
	{
		Type_t * L_35 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(Byte_t1_11_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_35) == ((Object_t*)(Type_t *)L_36))))
		{
			goto IL_0124;
		}
	}
	{
		uint16_t L_37 = V_0;
		ByteU5BU5D_t1_109* L_38 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint16_t L_39 = (uint16_t)(__this->___mValue_3);
		uint16_t L_40 = L_39;
		Object_t * L_41 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_40);
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_38, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_41, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_42 = {0};
		ExifInterOperability__ctor_m8_496(&L_42, (uint16_t)L_37, (uint16_t)1, (uint32_t)1, (ByteU5BU5D_t1_109*)L_38, /*hidden argument*/NULL);
		return L_42;
	}

IL_0124:
	{
		Type_t * L_43 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_44 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(UInt16_t1_14_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_43) == ((Object_t*)(Type_t *)L_44))))
		{
			goto IL_0154;
		}
	}
	{
		uint16_t L_45 = V_0;
		uint16_t L_46 = (uint16_t)(__this->___mValue_3);
		uint16_t L_47 = L_46;
		Object_t * L_48 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_47);
		ByteU5BU5D_t1_109* L_49 = BitConverterEx_GetBytes_m8_354(NULL /*static, unused*/, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_48, UInt16_t1_14_il2cpp_TypeInfo_var)))), (int32_t)0, (int32_t)0, /*hidden argument*/NULL);
		ExifInterOperability_t8_113  L_50 = {0};
		ExifInterOperability__ctor_m8_496(&L_50, (uint16_t)L_45, (uint16_t)3, (uint32_t)1, (ByteU5BU5D_t1_109*)L_49, /*hidden argument*/NULL);
		return L_50;
	}

IL_0154:
	{
		UnknownEnumTypeException_t8_96 * L_51 = (UnknownEnumTypeException_t8_96 *)il2cpp_codegen_object_new (UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var);
		UnknownEnumTypeException__ctor_m8_407(L_51, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_51);
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.Contrast>::op_Implicit(ExifLibrary.ExifEnumProperty`1<T>)
extern "C" uint16_t ExifEnumProperty_1_op_Implicit_m8_2251_gshared (Object_t * __this /* static, unused */, ExifEnumProperty_1_t8_357 * ___obj, const MethodInfo* method)
{
	{
		ExifEnumProperty_1_t8_357 * L_0 = ___obj;
		NullCheck(L_0);
		uint16_t L_1 = (uint16_t)(L_0->___mValue_3);
		return L_1;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.Saturation>::.ctor(ExifLibrary.ExifTag,T,System.Boolean)
extern "C" void ExifEnumProperty_1__ctor_m8_1995_gshared (ExifEnumProperty_1_t8_358 * __this, int32_t ___tag, uint16_t ___value, bool ___isbitfield, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		NullCheck((ExifProperty_t8_99 *)__this);
		ExifProperty__ctor_m8_502((ExifProperty_t8_99 *)__this, (int32_t)L_0, /*hidden argument*/NULL);
		uint16_t L_1 = ___value;
		__this->___mValue_3 = L_1;
		bool L_2 = ___isbitfield;
		__this->___mIsBitField_4 = L_2;
		return;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.Saturation>::.ctor(ExifLibrary.ExifTag,T)
extern "C" void ExifEnumProperty_1__ctor_m8_2252_gshared (ExifEnumProperty_1_t8_358 * __this, int32_t ___tag, uint16_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		uint16_t L_1 = ___value;
		NullCheck((ExifEnumProperty_1_t8_358 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_358 *, int32_t, uint16_t, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ExifEnumProperty_1_t8_358 *)__this, (int32_t)L_0, (uint16_t)L_1, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Object ExifLibrary.ExifEnumProperty`1<ExifLibrary.Saturation>::get__Value()
extern "C" Object_t * ExifEnumProperty_1_get__Value_m8_2253_gshared (ExifEnumProperty_1_t8_358 * __this, const MethodInfo* method)
{
	{
		NullCheck((ExifEnumProperty_1_t8_358 *)__this);
		uint16_t L_0 = (( uint16_t (*) (ExifEnumProperty_1_t8_358 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ExifEnumProperty_1_t8_358 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		uint16_t L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.Saturation>::set__Value(System.Object)
extern "C" void ExifEnumProperty_1_set__Value_m8_2254_gshared (ExifEnumProperty_1_t8_358 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		NullCheck((ExifEnumProperty_1_t8_358 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_358 *, uint16_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((ExifEnumProperty_1_t8_358 *)__this, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.Saturation>::get_Value()
extern "C" uint16_t ExifEnumProperty_1_get_Value_m8_2255_gshared (ExifEnumProperty_1_t8_358 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = (uint16_t)(__this->___mValue_3);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.Saturation>::set_Value(T)
extern "C" void ExifEnumProperty_1_set_Value_m8_2256_gshared (ExifEnumProperty_1_t8_358 * __this, uint16_t ___value, const MethodInfo* method)
{
	{
		uint16_t L_0 = ___value;
		__this->___mValue_3 = L_0;
		return;
	}
}
// System.Boolean ExifLibrary.ExifEnumProperty`1<ExifLibrary.Saturation>::get_IsBitField()
extern "C" bool ExifEnumProperty_1_get_IsBitField_m8_2257_gshared (ExifEnumProperty_1_t8_358 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(__this->___mIsBitField_4);
		return L_0;
	}
}
// System.String ExifLibrary.ExifEnumProperty`1<ExifLibrary.Saturation>::ToString()
extern "C" String_t* ExifEnumProperty_1_ToString_m8_2258_gshared (ExifEnumProperty_1_t8_358 * __this, const MethodInfo* method)
{
	{
		uint16_t* L_0 = (uint16_t*)&(__this->___mValue_3);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		return L_1;
	}
}
// ExifLibrary.ExifInterOperability ExifLibrary.ExifEnumProperty`1<ExifLibrary.Saturation>::get_Interoperability()
extern const Il2CppType* FileSource_t8_75_0_0_0_var;
extern const Il2CppType* SceneType_t8_76_0_0_0_var;
extern const Il2CppType* GPSLatitudeRef_t8_86_0_0_0_var;
extern const Il2CppType* GPSLongitudeRef_t8_87_0_0_0_var;
extern const Il2CppType* GPSStatus_t8_89_0_0_0_var;
extern const Il2CppType* GPSMeasureMode_t8_90_0_0_0_var;
extern const Il2CppType* GPSSpeedRef_t8_91_0_0_0_var;
extern const Il2CppType* GPSDirectionRef_t8_92_0_0_0_var;
extern const Il2CppType* GPSDistanceRef_t8_93_0_0_0_var;
extern const Il2CppType* Byte_t1_11_0_0_0_var;
extern const Il2CppType* UInt16_t1_14_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Enum_t1_24_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern TypeInfo* UInt16_t1_14_il2cpp_TypeInfo_var;
extern TypeInfo* UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var;
extern "C" ExifInterOperability_t8_113  ExifEnumProperty_1_get_Interoperability_m8_2259_gshared (ExifEnumProperty_1_t8_358 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FileSource_t8_75_0_0_0_var = il2cpp_codegen_type_from_index(2003);
		SceneType_t8_76_0_0_0_var = il2cpp_codegen_type_from_index(2004);
		GPSLatitudeRef_t8_86_0_0_0_var = il2cpp_codegen_type_from_index(2014);
		GPSLongitudeRef_t8_87_0_0_0_var = il2cpp_codegen_type_from_index(2015);
		GPSStatus_t8_89_0_0_0_var = il2cpp_codegen_type_from_index(2017);
		GPSMeasureMode_t8_90_0_0_0_var = il2cpp_codegen_type_from_index(2018);
		GPSSpeedRef_t8_91_0_0_0_var = il2cpp_codegen_type_from_index(2019);
		GPSDirectionRef_t8_92_0_0_0_var = il2cpp_codegen_type_from_index(2020);
		GPSDistanceRef_t8_93_0_0_0_var = il2cpp_codegen_type_from_index(2021);
		Byte_t1_11_0_0_0_var = il2cpp_codegen_type_from_index(25);
		UInt16_t1_14_0_0_0_var = il2cpp_codegen_type_from_index(29);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Enum_t1_24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		UInt16_t1_14_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4656);
		s_Il2CppMethodIntialized = true;
	}
	uint16_t V_0 = 0;
	Type_t * V_1 = {0};
	Type_t * V_2 = {0};
	{
		int32_t L_0 = (int32_t)(((ExifProperty_t8_99 *)__this)->___mTag_0);
		uint16_t L_1 = ExifTagFactory_GetTagID_m8_630(NULL /*static, unused*/, (int32_t)L_0, /*hidden argument*/NULL);
		V_0 = (uint16_t)L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		V_1 = (Type_t *)L_2;
		Type_t * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t1_24_il2cpp_TypeInfo_var);
		Type_t * L_4 = Enum_GetUnderlyingType_m1_942(NULL /*static, unused*/, (Type_t *)L_3, /*hidden argument*/NULL);
		V_2 = (Type_t *)L_4;
		Type_t * L_5 = V_1;
		Type_t * L_6 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(FileSource_t8_75_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_5) == ((Object_t*)(Type_t *)L_6)))
		{
			goto IL_003e;
		}
	}
	{
		Type_t * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(SceneType_t8_76_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_7) == ((Object_t*)(Type_t *)L_8))))
		{
			goto IL_0060;
		}
	}

IL_003e:
	{
		uint16_t L_9 = V_0;
		ByteU5BU5D_t1_109* L_10 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint16_t L_11 = (uint16_t)(__this->___mValue_3);
		uint16_t L_12 = L_11;
		Object_t * L_13 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_12);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_10, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_13, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_14 = {0};
		ExifInterOperability__ctor_m8_496(&L_14, (uint16_t)L_9, (uint16_t)7, (uint32_t)1, (ByteU5BU5D_t1_109*)L_10, /*hidden argument*/NULL);
		return L_14;
	}

IL_0060:
	{
		Type_t * L_15 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_16 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLatitudeRef_t8_86_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_15) == ((Object_t*)(Type_t *)L_16)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_17 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_18 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLongitudeRef_t8_87_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_17) == ((Object_t*)(Type_t *)L_18)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_19 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_20 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSStatus_t8_89_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_19) == ((Object_t*)(Type_t *)L_20)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_21 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_22 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSMeasureMode_t8_90_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_21) == ((Object_t*)(Type_t *)L_22)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_23 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_24 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSSpeedRef_t8_91_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_23) == ((Object_t*)(Type_t *)L_24)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_25 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_26 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDirectionRef_t8_92_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_25) == ((Object_t*)(Type_t *)L_26)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_27 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_28 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDistanceRef_t8_93_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_27) == ((Object_t*)(Type_t *)L_28))))
		{
			goto IL_00f2;
		}
	}

IL_00d0:
	{
		uint16_t L_29 = V_0;
		ByteU5BU5D_t1_109* L_30 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 2));
		uint16_t L_31 = (uint16_t)(__this->___mValue_3);
		uint16_t L_32 = L_31;
		Object_t * L_33 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_32);
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_30, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_33, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_34 = {0};
		ExifInterOperability__ctor_m8_496(&L_34, (uint16_t)L_29, (uint16_t)2, (uint32_t)2, (ByteU5BU5D_t1_109*)L_30, /*hidden argument*/NULL);
		return L_34;
	}

IL_00f2:
	{
		Type_t * L_35 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(Byte_t1_11_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_35) == ((Object_t*)(Type_t *)L_36))))
		{
			goto IL_0124;
		}
	}
	{
		uint16_t L_37 = V_0;
		ByteU5BU5D_t1_109* L_38 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint16_t L_39 = (uint16_t)(__this->___mValue_3);
		uint16_t L_40 = L_39;
		Object_t * L_41 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_40);
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_38, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_41, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_42 = {0};
		ExifInterOperability__ctor_m8_496(&L_42, (uint16_t)L_37, (uint16_t)1, (uint32_t)1, (ByteU5BU5D_t1_109*)L_38, /*hidden argument*/NULL);
		return L_42;
	}

IL_0124:
	{
		Type_t * L_43 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_44 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(UInt16_t1_14_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_43) == ((Object_t*)(Type_t *)L_44))))
		{
			goto IL_0154;
		}
	}
	{
		uint16_t L_45 = V_0;
		uint16_t L_46 = (uint16_t)(__this->___mValue_3);
		uint16_t L_47 = L_46;
		Object_t * L_48 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_47);
		ByteU5BU5D_t1_109* L_49 = BitConverterEx_GetBytes_m8_354(NULL /*static, unused*/, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_48, UInt16_t1_14_il2cpp_TypeInfo_var)))), (int32_t)0, (int32_t)0, /*hidden argument*/NULL);
		ExifInterOperability_t8_113  L_50 = {0};
		ExifInterOperability__ctor_m8_496(&L_50, (uint16_t)L_45, (uint16_t)3, (uint32_t)1, (ByteU5BU5D_t1_109*)L_49, /*hidden argument*/NULL);
		return L_50;
	}

IL_0154:
	{
		UnknownEnumTypeException_t8_96 * L_51 = (UnknownEnumTypeException_t8_96 *)il2cpp_codegen_object_new (UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var);
		UnknownEnumTypeException__ctor_m8_407(L_51, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_51);
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.Saturation>::op_Implicit(ExifLibrary.ExifEnumProperty`1<T>)
extern "C" uint16_t ExifEnumProperty_1_op_Implicit_m8_2260_gshared (Object_t * __this /* static, unused */, ExifEnumProperty_1_t8_358 * ___obj, const MethodInfo* method)
{
	{
		ExifEnumProperty_1_t8_358 * L_0 = ___obj;
		NullCheck(L_0);
		uint16_t L_1 = (uint16_t)(L_0->___mValue_3);
		return L_1;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.Sharpness>::.ctor(ExifLibrary.ExifTag,T,System.Boolean)
extern "C" void ExifEnumProperty_1__ctor_m8_1996_gshared (ExifEnumProperty_1_t8_359 * __this, int32_t ___tag, uint16_t ___value, bool ___isbitfield, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		NullCheck((ExifProperty_t8_99 *)__this);
		ExifProperty__ctor_m8_502((ExifProperty_t8_99 *)__this, (int32_t)L_0, /*hidden argument*/NULL);
		uint16_t L_1 = ___value;
		__this->___mValue_3 = L_1;
		bool L_2 = ___isbitfield;
		__this->___mIsBitField_4 = L_2;
		return;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.Sharpness>::.ctor(ExifLibrary.ExifTag,T)
extern "C" void ExifEnumProperty_1__ctor_m8_2261_gshared (ExifEnumProperty_1_t8_359 * __this, int32_t ___tag, uint16_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		uint16_t L_1 = ___value;
		NullCheck((ExifEnumProperty_1_t8_359 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_359 *, int32_t, uint16_t, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ExifEnumProperty_1_t8_359 *)__this, (int32_t)L_0, (uint16_t)L_1, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Object ExifLibrary.ExifEnumProperty`1<ExifLibrary.Sharpness>::get__Value()
extern "C" Object_t * ExifEnumProperty_1_get__Value_m8_2262_gshared (ExifEnumProperty_1_t8_359 * __this, const MethodInfo* method)
{
	{
		NullCheck((ExifEnumProperty_1_t8_359 *)__this);
		uint16_t L_0 = (( uint16_t (*) (ExifEnumProperty_1_t8_359 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ExifEnumProperty_1_t8_359 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		uint16_t L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.Sharpness>::set__Value(System.Object)
extern "C" void ExifEnumProperty_1_set__Value_m8_2263_gshared (ExifEnumProperty_1_t8_359 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		NullCheck((ExifEnumProperty_1_t8_359 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_359 *, uint16_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((ExifEnumProperty_1_t8_359 *)__this, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.Sharpness>::get_Value()
extern "C" uint16_t ExifEnumProperty_1_get_Value_m8_2264_gshared (ExifEnumProperty_1_t8_359 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = (uint16_t)(__this->___mValue_3);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.Sharpness>::set_Value(T)
extern "C" void ExifEnumProperty_1_set_Value_m8_2265_gshared (ExifEnumProperty_1_t8_359 * __this, uint16_t ___value, const MethodInfo* method)
{
	{
		uint16_t L_0 = ___value;
		__this->___mValue_3 = L_0;
		return;
	}
}
// System.Boolean ExifLibrary.ExifEnumProperty`1<ExifLibrary.Sharpness>::get_IsBitField()
extern "C" bool ExifEnumProperty_1_get_IsBitField_m8_2266_gshared (ExifEnumProperty_1_t8_359 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(__this->___mIsBitField_4);
		return L_0;
	}
}
// System.String ExifLibrary.ExifEnumProperty`1<ExifLibrary.Sharpness>::ToString()
extern "C" String_t* ExifEnumProperty_1_ToString_m8_2267_gshared (ExifEnumProperty_1_t8_359 * __this, const MethodInfo* method)
{
	{
		uint16_t* L_0 = (uint16_t*)&(__this->___mValue_3);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		return L_1;
	}
}
// ExifLibrary.ExifInterOperability ExifLibrary.ExifEnumProperty`1<ExifLibrary.Sharpness>::get_Interoperability()
extern const Il2CppType* FileSource_t8_75_0_0_0_var;
extern const Il2CppType* SceneType_t8_76_0_0_0_var;
extern const Il2CppType* GPSLatitudeRef_t8_86_0_0_0_var;
extern const Il2CppType* GPSLongitudeRef_t8_87_0_0_0_var;
extern const Il2CppType* GPSStatus_t8_89_0_0_0_var;
extern const Il2CppType* GPSMeasureMode_t8_90_0_0_0_var;
extern const Il2CppType* GPSSpeedRef_t8_91_0_0_0_var;
extern const Il2CppType* GPSDirectionRef_t8_92_0_0_0_var;
extern const Il2CppType* GPSDistanceRef_t8_93_0_0_0_var;
extern const Il2CppType* Byte_t1_11_0_0_0_var;
extern const Il2CppType* UInt16_t1_14_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Enum_t1_24_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern TypeInfo* UInt16_t1_14_il2cpp_TypeInfo_var;
extern TypeInfo* UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var;
extern "C" ExifInterOperability_t8_113  ExifEnumProperty_1_get_Interoperability_m8_2268_gshared (ExifEnumProperty_1_t8_359 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FileSource_t8_75_0_0_0_var = il2cpp_codegen_type_from_index(2003);
		SceneType_t8_76_0_0_0_var = il2cpp_codegen_type_from_index(2004);
		GPSLatitudeRef_t8_86_0_0_0_var = il2cpp_codegen_type_from_index(2014);
		GPSLongitudeRef_t8_87_0_0_0_var = il2cpp_codegen_type_from_index(2015);
		GPSStatus_t8_89_0_0_0_var = il2cpp_codegen_type_from_index(2017);
		GPSMeasureMode_t8_90_0_0_0_var = il2cpp_codegen_type_from_index(2018);
		GPSSpeedRef_t8_91_0_0_0_var = il2cpp_codegen_type_from_index(2019);
		GPSDirectionRef_t8_92_0_0_0_var = il2cpp_codegen_type_from_index(2020);
		GPSDistanceRef_t8_93_0_0_0_var = il2cpp_codegen_type_from_index(2021);
		Byte_t1_11_0_0_0_var = il2cpp_codegen_type_from_index(25);
		UInt16_t1_14_0_0_0_var = il2cpp_codegen_type_from_index(29);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Enum_t1_24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		UInt16_t1_14_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4656);
		s_Il2CppMethodIntialized = true;
	}
	uint16_t V_0 = 0;
	Type_t * V_1 = {0};
	Type_t * V_2 = {0};
	{
		int32_t L_0 = (int32_t)(((ExifProperty_t8_99 *)__this)->___mTag_0);
		uint16_t L_1 = ExifTagFactory_GetTagID_m8_630(NULL /*static, unused*/, (int32_t)L_0, /*hidden argument*/NULL);
		V_0 = (uint16_t)L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		V_1 = (Type_t *)L_2;
		Type_t * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t1_24_il2cpp_TypeInfo_var);
		Type_t * L_4 = Enum_GetUnderlyingType_m1_942(NULL /*static, unused*/, (Type_t *)L_3, /*hidden argument*/NULL);
		V_2 = (Type_t *)L_4;
		Type_t * L_5 = V_1;
		Type_t * L_6 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(FileSource_t8_75_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_5) == ((Object_t*)(Type_t *)L_6)))
		{
			goto IL_003e;
		}
	}
	{
		Type_t * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(SceneType_t8_76_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_7) == ((Object_t*)(Type_t *)L_8))))
		{
			goto IL_0060;
		}
	}

IL_003e:
	{
		uint16_t L_9 = V_0;
		ByteU5BU5D_t1_109* L_10 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint16_t L_11 = (uint16_t)(__this->___mValue_3);
		uint16_t L_12 = L_11;
		Object_t * L_13 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_12);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_10, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_13, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_14 = {0};
		ExifInterOperability__ctor_m8_496(&L_14, (uint16_t)L_9, (uint16_t)7, (uint32_t)1, (ByteU5BU5D_t1_109*)L_10, /*hidden argument*/NULL);
		return L_14;
	}

IL_0060:
	{
		Type_t * L_15 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_16 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLatitudeRef_t8_86_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_15) == ((Object_t*)(Type_t *)L_16)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_17 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_18 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLongitudeRef_t8_87_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_17) == ((Object_t*)(Type_t *)L_18)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_19 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_20 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSStatus_t8_89_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_19) == ((Object_t*)(Type_t *)L_20)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_21 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_22 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSMeasureMode_t8_90_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_21) == ((Object_t*)(Type_t *)L_22)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_23 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_24 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSSpeedRef_t8_91_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_23) == ((Object_t*)(Type_t *)L_24)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_25 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_26 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDirectionRef_t8_92_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_25) == ((Object_t*)(Type_t *)L_26)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_27 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_28 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDistanceRef_t8_93_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_27) == ((Object_t*)(Type_t *)L_28))))
		{
			goto IL_00f2;
		}
	}

IL_00d0:
	{
		uint16_t L_29 = V_0;
		ByteU5BU5D_t1_109* L_30 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 2));
		uint16_t L_31 = (uint16_t)(__this->___mValue_3);
		uint16_t L_32 = L_31;
		Object_t * L_33 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_32);
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_30, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_33, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_34 = {0};
		ExifInterOperability__ctor_m8_496(&L_34, (uint16_t)L_29, (uint16_t)2, (uint32_t)2, (ByteU5BU5D_t1_109*)L_30, /*hidden argument*/NULL);
		return L_34;
	}

IL_00f2:
	{
		Type_t * L_35 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(Byte_t1_11_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_35) == ((Object_t*)(Type_t *)L_36))))
		{
			goto IL_0124;
		}
	}
	{
		uint16_t L_37 = V_0;
		ByteU5BU5D_t1_109* L_38 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint16_t L_39 = (uint16_t)(__this->___mValue_3);
		uint16_t L_40 = L_39;
		Object_t * L_41 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_40);
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_38, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_41, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_42 = {0};
		ExifInterOperability__ctor_m8_496(&L_42, (uint16_t)L_37, (uint16_t)1, (uint32_t)1, (ByteU5BU5D_t1_109*)L_38, /*hidden argument*/NULL);
		return L_42;
	}

IL_0124:
	{
		Type_t * L_43 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_44 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(UInt16_t1_14_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_43) == ((Object_t*)(Type_t *)L_44))))
		{
			goto IL_0154;
		}
	}
	{
		uint16_t L_45 = V_0;
		uint16_t L_46 = (uint16_t)(__this->___mValue_3);
		uint16_t L_47 = L_46;
		Object_t * L_48 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_47);
		ByteU5BU5D_t1_109* L_49 = BitConverterEx_GetBytes_m8_354(NULL /*static, unused*/, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_48, UInt16_t1_14_il2cpp_TypeInfo_var)))), (int32_t)0, (int32_t)0, /*hidden argument*/NULL);
		ExifInterOperability_t8_113  L_50 = {0};
		ExifInterOperability__ctor_m8_496(&L_50, (uint16_t)L_45, (uint16_t)3, (uint32_t)1, (ByteU5BU5D_t1_109*)L_49, /*hidden argument*/NULL);
		return L_50;
	}

IL_0154:
	{
		UnknownEnumTypeException_t8_96 * L_51 = (UnknownEnumTypeException_t8_96 *)il2cpp_codegen_object_new (UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var);
		UnknownEnumTypeException__ctor_m8_407(L_51, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_51);
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.Sharpness>::op_Implicit(ExifLibrary.ExifEnumProperty`1<T>)
extern "C" uint16_t ExifEnumProperty_1_op_Implicit_m8_2269_gshared (Object_t * __this /* static, unused */, ExifEnumProperty_1_t8_359 * ___obj, const MethodInfo* method)
{
	{
		ExifEnumProperty_1_t8_359 * L_0 = ___obj;
		NullCheck(L_0);
		uint16_t L_1 = (uint16_t)(L_0->___mValue_3);
		return L_1;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.SubjectDistanceRange>::.ctor(ExifLibrary.ExifTag,T,System.Boolean)
extern "C" void ExifEnumProperty_1__ctor_m8_1997_gshared (ExifEnumProperty_1_t8_360 * __this, int32_t ___tag, uint16_t ___value, bool ___isbitfield, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		NullCheck((ExifProperty_t8_99 *)__this);
		ExifProperty__ctor_m8_502((ExifProperty_t8_99 *)__this, (int32_t)L_0, /*hidden argument*/NULL);
		uint16_t L_1 = ___value;
		__this->___mValue_3 = L_1;
		bool L_2 = ___isbitfield;
		__this->___mIsBitField_4 = L_2;
		return;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.SubjectDistanceRange>::.ctor(ExifLibrary.ExifTag,T)
extern "C" void ExifEnumProperty_1__ctor_m8_2270_gshared (ExifEnumProperty_1_t8_360 * __this, int32_t ___tag, uint16_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		uint16_t L_1 = ___value;
		NullCheck((ExifEnumProperty_1_t8_360 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_360 *, int32_t, uint16_t, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ExifEnumProperty_1_t8_360 *)__this, (int32_t)L_0, (uint16_t)L_1, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Object ExifLibrary.ExifEnumProperty`1<ExifLibrary.SubjectDistanceRange>::get__Value()
extern "C" Object_t * ExifEnumProperty_1_get__Value_m8_2271_gshared (ExifEnumProperty_1_t8_360 * __this, const MethodInfo* method)
{
	{
		NullCheck((ExifEnumProperty_1_t8_360 *)__this);
		uint16_t L_0 = (( uint16_t (*) (ExifEnumProperty_1_t8_360 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ExifEnumProperty_1_t8_360 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		uint16_t L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.SubjectDistanceRange>::set__Value(System.Object)
extern "C" void ExifEnumProperty_1_set__Value_m8_2272_gshared (ExifEnumProperty_1_t8_360 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		NullCheck((ExifEnumProperty_1_t8_360 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_360 *, uint16_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((ExifEnumProperty_1_t8_360 *)__this, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.SubjectDistanceRange>::get_Value()
extern "C" uint16_t ExifEnumProperty_1_get_Value_m8_2273_gshared (ExifEnumProperty_1_t8_360 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = (uint16_t)(__this->___mValue_3);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.SubjectDistanceRange>::set_Value(T)
extern "C" void ExifEnumProperty_1_set_Value_m8_2274_gshared (ExifEnumProperty_1_t8_360 * __this, uint16_t ___value, const MethodInfo* method)
{
	{
		uint16_t L_0 = ___value;
		__this->___mValue_3 = L_0;
		return;
	}
}
// System.Boolean ExifLibrary.ExifEnumProperty`1<ExifLibrary.SubjectDistanceRange>::get_IsBitField()
extern "C" bool ExifEnumProperty_1_get_IsBitField_m8_2275_gshared (ExifEnumProperty_1_t8_360 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(__this->___mIsBitField_4);
		return L_0;
	}
}
// System.String ExifLibrary.ExifEnumProperty`1<ExifLibrary.SubjectDistanceRange>::ToString()
extern "C" String_t* ExifEnumProperty_1_ToString_m8_2276_gshared (ExifEnumProperty_1_t8_360 * __this, const MethodInfo* method)
{
	{
		uint16_t* L_0 = (uint16_t*)&(__this->___mValue_3);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		return L_1;
	}
}
// ExifLibrary.ExifInterOperability ExifLibrary.ExifEnumProperty`1<ExifLibrary.SubjectDistanceRange>::get_Interoperability()
extern const Il2CppType* FileSource_t8_75_0_0_0_var;
extern const Il2CppType* SceneType_t8_76_0_0_0_var;
extern const Il2CppType* GPSLatitudeRef_t8_86_0_0_0_var;
extern const Il2CppType* GPSLongitudeRef_t8_87_0_0_0_var;
extern const Il2CppType* GPSStatus_t8_89_0_0_0_var;
extern const Il2CppType* GPSMeasureMode_t8_90_0_0_0_var;
extern const Il2CppType* GPSSpeedRef_t8_91_0_0_0_var;
extern const Il2CppType* GPSDirectionRef_t8_92_0_0_0_var;
extern const Il2CppType* GPSDistanceRef_t8_93_0_0_0_var;
extern const Il2CppType* Byte_t1_11_0_0_0_var;
extern const Il2CppType* UInt16_t1_14_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Enum_t1_24_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern TypeInfo* UInt16_t1_14_il2cpp_TypeInfo_var;
extern TypeInfo* UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var;
extern "C" ExifInterOperability_t8_113  ExifEnumProperty_1_get_Interoperability_m8_2277_gshared (ExifEnumProperty_1_t8_360 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FileSource_t8_75_0_0_0_var = il2cpp_codegen_type_from_index(2003);
		SceneType_t8_76_0_0_0_var = il2cpp_codegen_type_from_index(2004);
		GPSLatitudeRef_t8_86_0_0_0_var = il2cpp_codegen_type_from_index(2014);
		GPSLongitudeRef_t8_87_0_0_0_var = il2cpp_codegen_type_from_index(2015);
		GPSStatus_t8_89_0_0_0_var = il2cpp_codegen_type_from_index(2017);
		GPSMeasureMode_t8_90_0_0_0_var = il2cpp_codegen_type_from_index(2018);
		GPSSpeedRef_t8_91_0_0_0_var = il2cpp_codegen_type_from_index(2019);
		GPSDirectionRef_t8_92_0_0_0_var = il2cpp_codegen_type_from_index(2020);
		GPSDistanceRef_t8_93_0_0_0_var = il2cpp_codegen_type_from_index(2021);
		Byte_t1_11_0_0_0_var = il2cpp_codegen_type_from_index(25);
		UInt16_t1_14_0_0_0_var = il2cpp_codegen_type_from_index(29);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Enum_t1_24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		UInt16_t1_14_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4656);
		s_Il2CppMethodIntialized = true;
	}
	uint16_t V_0 = 0;
	Type_t * V_1 = {0};
	Type_t * V_2 = {0};
	{
		int32_t L_0 = (int32_t)(((ExifProperty_t8_99 *)__this)->___mTag_0);
		uint16_t L_1 = ExifTagFactory_GetTagID_m8_630(NULL /*static, unused*/, (int32_t)L_0, /*hidden argument*/NULL);
		V_0 = (uint16_t)L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		V_1 = (Type_t *)L_2;
		Type_t * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t1_24_il2cpp_TypeInfo_var);
		Type_t * L_4 = Enum_GetUnderlyingType_m1_942(NULL /*static, unused*/, (Type_t *)L_3, /*hidden argument*/NULL);
		V_2 = (Type_t *)L_4;
		Type_t * L_5 = V_1;
		Type_t * L_6 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(FileSource_t8_75_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_5) == ((Object_t*)(Type_t *)L_6)))
		{
			goto IL_003e;
		}
	}
	{
		Type_t * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(SceneType_t8_76_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_7) == ((Object_t*)(Type_t *)L_8))))
		{
			goto IL_0060;
		}
	}

IL_003e:
	{
		uint16_t L_9 = V_0;
		ByteU5BU5D_t1_109* L_10 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint16_t L_11 = (uint16_t)(__this->___mValue_3);
		uint16_t L_12 = L_11;
		Object_t * L_13 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_12);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_10, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_13, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_14 = {0};
		ExifInterOperability__ctor_m8_496(&L_14, (uint16_t)L_9, (uint16_t)7, (uint32_t)1, (ByteU5BU5D_t1_109*)L_10, /*hidden argument*/NULL);
		return L_14;
	}

IL_0060:
	{
		Type_t * L_15 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_16 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLatitudeRef_t8_86_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_15) == ((Object_t*)(Type_t *)L_16)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_17 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_18 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLongitudeRef_t8_87_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_17) == ((Object_t*)(Type_t *)L_18)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_19 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_20 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSStatus_t8_89_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_19) == ((Object_t*)(Type_t *)L_20)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_21 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_22 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSMeasureMode_t8_90_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_21) == ((Object_t*)(Type_t *)L_22)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_23 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_24 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSSpeedRef_t8_91_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_23) == ((Object_t*)(Type_t *)L_24)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_25 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_26 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDirectionRef_t8_92_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_25) == ((Object_t*)(Type_t *)L_26)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_27 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_28 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDistanceRef_t8_93_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_27) == ((Object_t*)(Type_t *)L_28))))
		{
			goto IL_00f2;
		}
	}

IL_00d0:
	{
		uint16_t L_29 = V_0;
		ByteU5BU5D_t1_109* L_30 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 2));
		uint16_t L_31 = (uint16_t)(__this->___mValue_3);
		uint16_t L_32 = L_31;
		Object_t * L_33 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_32);
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_30, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_33, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_34 = {0};
		ExifInterOperability__ctor_m8_496(&L_34, (uint16_t)L_29, (uint16_t)2, (uint32_t)2, (ByteU5BU5D_t1_109*)L_30, /*hidden argument*/NULL);
		return L_34;
	}

IL_00f2:
	{
		Type_t * L_35 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(Byte_t1_11_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_35) == ((Object_t*)(Type_t *)L_36))))
		{
			goto IL_0124;
		}
	}
	{
		uint16_t L_37 = V_0;
		ByteU5BU5D_t1_109* L_38 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint16_t L_39 = (uint16_t)(__this->___mValue_3);
		uint16_t L_40 = L_39;
		Object_t * L_41 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_40);
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_38, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_41, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_42 = {0};
		ExifInterOperability__ctor_m8_496(&L_42, (uint16_t)L_37, (uint16_t)1, (uint32_t)1, (ByteU5BU5D_t1_109*)L_38, /*hidden argument*/NULL);
		return L_42;
	}

IL_0124:
	{
		Type_t * L_43 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_44 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(UInt16_t1_14_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_43) == ((Object_t*)(Type_t *)L_44))))
		{
			goto IL_0154;
		}
	}
	{
		uint16_t L_45 = V_0;
		uint16_t L_46 = (uint16_t)(__this->___mValue_3);
		uint16_t L_47 = L_46;
		Object_t * L_48 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_47);
		ByteU5BU5D_t1_109* L_49 = BitConverterEx_GetBytes_m8_354(NULL /*static, unused*/, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_48, UInt16_t1_14_il2cpp_TypeInfo_var)))), (int32_t)0, (int32_t)0, /*hidden argument*/NULL);
		ExifInterOperability_t8_113  L_50 = {0};
		ExifInterOperability__ctor_m8_496(&L_50, (uint16_t)L_45, (uint16_t)3, (uint32_t)1, (ByteU5BU5D_t1_109*)L_49, /*hidden argument*/NULL);
		return L_50;
	}

IL_0154:
	{
		UnknownEnumTypeException_t8_96 * L_51 = (UnknownEnumTypeException_t8_96 *)il2cpp_codegen_object_new (UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var);
		UnknownEnumTypeException__ctor_m8_407(L_51, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_51);
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.SubjectDistanceRange>::op_Implicit(ExifLibrary.ExifEnumProperty`1<T>)
extern "C" uint16_t ExifEnumProperty_1_op_Implicit_m8_2278_gshared (Object_t * __this /* static, unused */, ExifEnumProperty_1_t8_360 * ___obj, const MethodInfo* method)
{
	{
		ExifEnumProperty_1_t8_360 * L_0 = ___obj;
		NullCheck(L_0);
		uint16_t L_1 = (uint16_t)(L_0->___mValue_3);
		return L_1;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSLatitudeRef>::.ctor(ExifLibrary.ExifTag,T,System.Boolean)
extern "C" void ExifEnumProperty_1__ctor_m8_2279_gshared (ExifEnumProperty_1_t8_361 * __this, int32_t ___tag, uint8_t ___value, bool ___isbitfield, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		NullCheck((ExifProperty_t8_99 *)__this);
		ExifProperty__ctor_m8_502((ExifProperty_t8_99 *)__this, (int32_t)L_0, /*hidden argument*/NULL);
		uint8_t L_1 = ___value;
		__this->___mValue_3 = L_1;
		bool L_2 = ___isbitfield;
		__this->___mIsBitField_4 = L_2;
		return;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSLatitudeRef>::.ctor(ExifLibrary.ExifTag,T)
extern "C" void ExifEnumProperty_1__ctor_m8_1998_gshared (ExifEnumProperty_1_t8_361 * __this, int32_t ___tag, uint8_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		uint8_t L_1 = ___value;
		NullCheck((ExifEnumProperty_1_t8_361 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_361 *, int32_t, uint8_t, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ExifEnumProperty_1_t8_361 *)__this, (int32_t)L_0, (uint8_t)L_1, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Object ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSLatitudeRef>::get__Value()
extern "C" Object_t * ExifEnumProperty_1_get__Value_m8_2280_gshared (ExifEnumProperty_1_t8_361 * __this, const MethodInfo* method)
{
	{
		NullCheck((ExifEnumProperty_1_t8_361 *)__this);
		uint8_t L_0 = (( uint8_t (*) (ExifEnumProperty_1_t8_361 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ExifEnumProperty_1_t8_361 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		uint8_t L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSLatitudeRef>::set__Value(System.Object)
extern "C" void ExifEnumProperty_1_set__Value_m8_2281_gshared (ExifEnumProperty_1_t8_361 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		NullCheck((ExifEnumProperty_1_t8_361 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_361 *, uint8_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((ExifEnumProperty_1_t8_361 *)__this, (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSLatitudeRef>::get_Value()
extern "C" uint8_t ExifEnumProperty_1_get_Value_m8_2282_gshared (ExifEnumProperty_1_t8_361 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = (uint8_t)(__this->___mValue_3);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSLatitudeRef>::set_Value(T)
extern "C" void ExifEnumProperty_1_set_Value_m8_2283_gshared (ExifEnumProperty_1_t8_361 * __this, uint8_t ___value, const MethodInfo* method)
{
	{
		uint8_t L_0 = ___value;
		__this->___mValue_3 = L_0;
		return;
	}
}
// System.Boolean ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSLatitudeRef>::get_IsBitField()
extern "C" bool ExifEnumProperty_1_get_IsBitField_m8_2284_gshared (ExifEnumProperty_1_t8_361 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(__this->___mIsBitField_4);
		return L_0;
	}
}
// System.String ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSLatitudeRef>::ToString()
extern "C" String_t* ExifEnumProperty_1_ToString_m8_2285_gshared (ExifEnumProperty_1_t8_361 * __this, const MethodInfo* method)
{
	{
		uint8_t* L_0 = (uint8_t*)&(__this->___mValue_3);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		return L_1;
	}
}
// ExifLibrary.ExifInterOperability ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSLatitudeRef>::get_Interoperability()
extern const Il2CppType* FileSource_t8_75_0_0_0_var;
extern const Il2CppType* SceneType_t8_76_0_0_0_var;
extern const Il2CppType* GPSLatitudeRef_t8_86_0_0_0_var;
extern const Il2CppType* GPSLongitudeRef_t8_87_0_0_0_var;
extern const Il2CppType* GPSStatus_t8_89_0_0_0_var;
extern const Il2CppType* GPSMeasureMode_t8_90_0_0_0_var;
extern const Il2CppType* GPSSpeedRef_t8_91_0_0_0_var;
extern const Il2CppType* GPSDirectionRef_t8_92_0_0_0_var;
extern const Il2CppType* GPSDistanceRef_t8_93_0_0_0_var;
extern const Il2CppType* Byte_t1_11_0_0_0_var;
extern const Il2CppType* UInt16_t1_14_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Enum_t1_24_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern TypeInfo* UInt16_t1_14_il2cpp_TypeInfo_var;
extern TypeInfo* UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var;
extern "C" ExifInterOperability_t8_113  ExifEnumProperty_1_get_Interoperability_m8_2286_gshared (ExifEnumProperty_1_t8_361 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FileSource_t8_75_0_0_0_var = il2cpp_codegen_type_from_index(2003);
		SceneType_t8_76_0_0_0_var = il2cpp_codegen_type_from_index(2004);
		GPSLatitudeRef_t8_86_0_0_0_var = il2cpp_codegen_type_from_index(2014);
		GPSLongitudeRef_t8_87_0_0_0_var = il2cpp_codegen_type_from_index(2015);
		GPSStatus_t8_89_0_0_0_var = il2cpp_codegen_type_from_index(2017);
		GPSMeasureMode_t8_90_0_0_0_var = il2cpp_codegen_type_from_index(2018);
		GPSSpeedRef_t8_91_0_0_0_var = il2cpp_codegen_type_from_index(2019);
		GPSDirectionRef_t8_92_0_0_0_var = il2cpp_codegen_type_from_index(2020);
		GPSDistanceRef_t8_93_0_0_0_var = il2cpp_codegen_type_from_index(2021);
		Byte_t1_11_0_0_0_var = il2cpp_codegen_type_from_index(25);
		UInt16_t1_14_0_0_0_var = il2cpp_codegen_type_from_index(29);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Enum_t1_24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		UInt16_t1_14_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4656);
		s_Il2CppMethodIntialized = true;
	}
	uint16_t V_0 = 0;
	Type_t * V_1 = {0};
	Type_t * V_2 = {0};
	{
		int32_t L_0 = (int32_t)(((ExifProperty_t8_99 *)__this)->___mTag_0);
		uint16_t L_1 = ExifTagFactory_GetTagID_m8_630(NULL /*static, unused*/, (int32_t)L_0, /*hidden argument*/NULL);
		V_0 = (uint16_t)L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		V_1 = (Type_t *)L_2;
		Type_t * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t1_24_il2cpp_TypeInfo_var);
		Type_t * L_4 = Enum_GetUnderlyingType_m1_942(NULL /*static, unused*/, (Type_t *)L_3, /*hidden argument*/NULL);
		V_2 = (Type_t *)L_4;
		Type_t * L_5 = V_1;
		Type_t * L_6 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(FileSource_t8_75_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_5) == ((Object_t*)(Type_t *)L_6)))
		{
			goto IL_003e;
		}
	}
	{
		Type_t * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(SceneType_t8_76_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_7) == ((Object_t*)(Type_t *)L_8))))
		{
			goto IL_0060;
		}
	}

IL_003e:
	{
		uint16_t L_9 = V_0;
		ByteU5BU5D_t1_109* L_10 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint8_t L_11 = (uint8_t)(__this->___mValue_3);
		uint8_t L_12 = L_11;
		Object_t * L_13 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_12);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_10, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_13, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_14 = {0};
		ExifInterOperability__ctor_m8_496(&L_14, (uint16_t)L_9, (uint16_t)7, (uint32_t)1, (ByteU5BU5D_t1_109*)L_10, /*hidden argument*/NULL);
		return L_14;
	}

IL_0060:
	{
		Type_t * L_15 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_16 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLatitudeRef_t8_86_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_15) == ((Object_t*)(Type_t *)L_16)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_17 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_18 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLongitudeRef_t8_87_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_17) == ((Object_t*)(Type_t *)L_18)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_19 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_20 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSStatus_t8_89_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_19) == ((Object_t*)(Type_t *)L_20)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_21 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_22 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSMeasureMode_t8_90_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_21) == ((Object_t*)(Type_t *)L_22)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_23 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_24 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSSpeedRef_t8_91_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_23) == ((Object_t*)(Type_t *)L_24)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_25 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_26 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDirectionRef_t8_92_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_25) == ((Object_t*)(Type_t *)L_26)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_27 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_28 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDistanceRef_t8_93_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_27) == ((Object_t*)(Type_t *)L_28))))
		{
			goto IL_00f2;
		}
	}

IL_00d0:
	{
		uint16_t L_29 = V_0;
		ByteU5BU5D_t1_109* L_30 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 2));
		uint8_t L_31 = (uint8_t)(__this->___mValue_3);
		uint8_t L_32 = L_31;
		Object_t * L_33 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_32);
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_30, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_33, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_34 = {0};
		ExifInterOperability__ctor_m8_496(&L_34, (uint16_t)L_29, (uint16_t)2, (uint32_t)2, (ByteU5BU5D_t1_109*)L_30, /*hidden argument*/NULL);
		return L_34;
	}

IL_00f2:
	{
		Type_t * L_35 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(Byte_t1_11_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_35) == ((Object_t*)(Type_t *)L_36))))
		{
			goto IL_0124;
		}
	}
	{
		uint16_t L_37 = V_0;
		ByteU5BU5D_t1_109* L_38 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint8_t L_39 = (uint8_t)(__this->___mValue_3);
		uint8_t L_40 = L_39;
		Object_t * L_41 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_40);
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_38, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_41, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_42 = {0};
		ExifInterOperability__ctor_m8_496(&L_42, (uint16_t)L_37, (uint16_t)1, (uint32_t)1, (ByteU5BU5D_t1_109*)L_38, /*hidden argument*/NULL);
		return L_42;
	}

IL_0124:
	{
		Type_t * L_43 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_44 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(UInt16_t1_14_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_43) == ((Object_t*)(Type_t *)L_44))))
		{
			goto IL_0154;
		}
	}
	{
		uint16_t L_45 = V_0;
		uint8_t L_46 = (uint8_t)(__this->___mValue_3);
		uint8_t L_47 = L_46;
		Object_t * L_48 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_47);
		ByteU5BU5D_t1_109* L_49 = BitConverterEx_GetBytes_m8_354(NULL /*static, unused*/, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_48, UInt16_t1_14_il2cpp_TypeInfo_var)))), (int32_t)0, (int32_t)0, /*hidden argument*/NULL);
		ExifInterOperability_t8_113  L_50 = {0};
		ExifInterOperability__ctor_m8_496(&L_50, (uint16_t)L_45, (uint16_t)3, (uint32_t)1, (ByteU5BU5D_t1_109*)L_49, /*hidden argument*/NULL);
		return L_50;
	}

IL_0154:
	{
		UnknownEnumTypeException_t8_96 * L_51 = (UnknownEnumTypeException_t8_96 *)il2cpp_codegen_object_new (UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var);
		UnknownEnumTypeException__ctor_m8_407(L_51, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_51);
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSLatitudeRef>::op_Implicit(ExifLibrary.ExifEnumProperty`1<T>)
extern "C" uint8_t ExifEnumProperty_1_op_Implicit_m8_2287_gshared (Object_t * __this /* static, unused */, ExifEnumProperty_1_t8_361 * ___obj, const MethodInfo* method)
{
	{
		ExifEnumProperty_1_t8_361 * L_0 = ___obj;
		NullCheck(L_0);
		uint8_t L_1 = (uint8_t)(L_0->___mValue_3);
		return L_1;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSLongitudeRef>::.ctor(ExifLibrary.ExifTag,T,System.Boolean)
extern "C" void ExifEnumProperty_1__ctor_m8_2288_gshared (ExifEnumProperty_1_t8_362 * __this, int32_t ___tag, uint8_t ___value, bool ___isbitfield, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		NullCheck((ExifProperty_t8_99 *)__this);
		ExifProperty__ctor_m8_502((ExifProperty_t8_99 *)__this, (int32_t)L_0, /*hidden argument*/NULL);
		uint8_t L_1 = ___value;
		__this->___mValue_3 = L_1;
		bool L_2 = ___isbitfield;
		__this->___mIsBitField_4 = L_2;
		return;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSLongitudeRef>::.ctor(ExifLibrary.ExifTag,T)
extern "C" void ExifEnumProperty_1__ctor_m8_1999_gshared (ExifEnumProperty_1_t8_362 * __this, int32_t ___tag, uint8_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		uint8_t L_1 = ___value;
		NullCheck((ExifEnumProperty_1_t8_362 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_362 *, int32_t, uint8_t, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ExifEnumProperty_1_t8_362 *)__this, (int32_t)L_0, (uint8_t)L_1, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Object ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSLongitudeRef>::get__Value()
extern "C" Object_t * ExifEnumProperty_1_get__Value_m8_2289_gshared (ExifEnumProperty_1_t8_362 * __this, const MethodInfo* method)
{
	{
		NullCheck((ExifEnumProperty_1_t8_362 *)__this);
		uint8_t L_0 = (( uint8_t (*) (ExifEnumProperty_1_t8_362 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ExifEnumProperty_1_t8_362 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		uint8_t L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSLongitudeRef>::set__Value(System.Object)
extern "C" void ExifEnumProperty_1_set__Value_m8_2290_gshared (ExifEnumProperty_1_t8_362 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		NullCheck((ExifEnumProperty_1_t8_362 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_362 *, uint8_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((ExifEnumProperty_1_t8_362 *)__this, (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSLongitudeRef>::get_Value()
extern "C" uint8_t ExifEnumProperty_1_get_Value_m8_2291_gshared (ExifEnumProperty_1_t8_362 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = (uint8_t)(__this->___mValue_3);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSLongitudeRef>::set_Value(T)
extern "C" void ExifEnumProperty_1_set_Value_m8_2292_gshared (ExifEnumProperty_1_t8_362 * __this, uint8_t ___value, const MethodInfo* method)
{
	{
		uint8_t L_0 = ___value;
		__this->___mValue_3 = L_0;
		return;
	}
}
// System.Boolean ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSLongitudeRef>::get_IsBitField()
extern "C" bool ExifEnumProperty_1_get_IsBitField_m8_2293_gshared (ExifEnumProperty_1_t8_362 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(__this->___mIsBitField_4);
		return L_0;
	}
}
// System.String ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSLongitudeRef>::ToString()
extern "C" String_t* ExifEnumProperty_1_ToString_m8_2294_gshared (ExifEnumProperty_1_t8_362 * __this, const MethodInfo* method)
{
	{
		uint8_t* L_0 = (uint8_t*)&(__this->___mValue_3);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		return L_1;
	}
}
// ExifLibrary.ExifInterOperability ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSLongitudeRef>::get_Interoperability()
extern const Il2CppType* FileSource_t8_75_0_0_0_var;
extern const Il2CppType* SceneType_t8_76_0_0_0_var;
extern const Il2CppType* GPSLatitudeRef_t8_86_0_0_0_var;
extern const Il2CppType* GPSLongitudeRef_t8_87_0_0_0_var;
extern const Il2CppType* GPSStatus_t8_89_0_0_0_var;
extern const Il2CppType* GPSMeasureMode_t8_90_0_0_0_var;
extern const Il2CppType* GPSSpeedRef_t8_91_0_0_0_var;
extern const Il2CppType* GPSDirectionRef_t8_92_0_0_0_var;
extern const Il2CppType* GPSDistanceRef_t8_93_0_0_0_var;
extern const Il2CppType* Byte_t1_11_0_0_0_var;
extern const Il2CppType* UInt16_t1_14_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Enum_t1_24_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern TypeInfo* UInt16_t1_14_il2cpp_TypeInfo_var;
extern TypeInfo* UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var;
extern "C" ExifInterOperability_t8_113  ExifEnumProperty_1_get_Interoperability_m8_2295_gshared (ExifEnumProperty_1_t8_362 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FileSource_t8_75_0_0_0_var = il2cpp_codegen_type_from_index(2003);
		SceneType_t8_76_0_0_0_var = il2cpp_codegen_type_from_index(2004);
		GPSLatitudeRef_t8_86_0_0_0_var = il2cpp_codegen_type_from_index(2014);
		GPSLongitudeRef_t8_87_0_0_0_var = il2cpp_codegen_type_from_index(2015);
		GPSStatus_t8_89_0_0_0_var = il2cpp_codegen_type_from_index(2017);
		GPSMeasureMode_t8_90_0_0_0_var = il2cpp_codegen_type_from_index(2018);
		GPSSpeedRef_t8_91_0_0_0_var = il2cpp_codegen_type_from_index(2019);
		GPSDirectionRef_t8_92_0_0_0_var = il2cpp_codegen_type_from_index(2020);
		GPSDistanceRef_t8_93_0_0_0_var = il2cpp_codegen_type_from_index(2021);
		Byte_t1_11_0_0_0_var = il2cpp_codegen_type_from_index(25);
		UInt16_t1_14_0_0_0_var = il2cpp_codegen_type_from_index(29);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Enum_t1_24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		UInt16_t1_14_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4656);
		s_Il2CppMethodIntialized = true;
	}
	uint16_t V_0 = 0;
	Type_t * V_1 = {0};
	Type_t * V_2 = {0};
	{
		int32_t L_0 = (int32_t)(((ExifProperty_t8_99 *)__this)->___mTag_0);
		uint16_t L_1 = ExifTagFactory_GetTagID_m8_630(NULL /*static, unused*/, (int32_t)L_0, /*hidden argument*/NULL);
		V_0 = (uint16_t)L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		V_1 = (Type_t *)L_2;
		Type_t * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t1_24_il2cpp_TypeInfo_var);
		Type_t * L_4 = Enum_GetUnderlyingType_m1_942(NULL /*static, unused*/, (Type_t *)L_3, /*hidden argument*/NULL);
		V_2 = (Type_t *)L_4;
		Type_t * L_5 = V_1;
		Type_t * L_6 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(FileSource_t8_75_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_5) == ((Object_t*)(Type_t *)L_6)))
		{
			goto IL_003e;
		}
	}
	{
		Type_t * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(SceneType_t8_76_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_7) == ((Object_t*)(Type_t *)L_8))))
		{
			goto IL_0060;
		}
	}

IL_003e:
	{
		uint16_t L_9 = V_0;
		ByteU5BU5D_t1_109* L_10 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint8_t L_11 = (uint8_t)(__this->___mValue_3);
		uint8_t L_12 = L_11;
		Object_t * L_13 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_12);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_10, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_13, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_14 = {0};
		ExifInterOperability__ctor_m8_496(&L_14, (uint16_t)L_9, (uint16_t)7, (uint32_t)1, (ByteU5BU5D_t1_109*)L_10, /*hidden argument*/NULL);
		return L_14;
	}

IL_0060:
	{
		Type_t * L_15 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_16 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLatitudeRef_t8_86_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_15) == ((Object_t*)(Type_t *)L_16)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_17 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_18 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLongitudeRef_t8_87_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_17) == ((Object_t*)(Type_t *)L_18)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_19 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_20 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSStatus_t8_89_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_19) == ((Object_t*)(Type_t *)L_20)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_21 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_22 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSMeasureMode_t8_90_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_21) == ((Object_t*)(Type_t *)L_22)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_23 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_24 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSSpeedRef_t8_91_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_23) == ((Object_t*)(Type_t *)L_24)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_25 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_26 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDirectionRef_t8_92_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_25) == ((Object_t*)(Type_t *)L_26)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_27 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_28 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDistanceRef_t8_93_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_27) == ((Object_t*)(Type_t *)L_28))))
		{
			goto IL_00f2;
		}
	}

IL_00d0:
	{
		uint16_t L_29 = V_0;
		ByteU5BU5D_t1_109* L_30 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 2));
		uint8_t L_31 = (uint8_t)(__this->___mValue_3);
		uint8_t L_32 = L_31;
		Object_t * L_33 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_32);
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_30, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_33, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_34 = {0};
		ExifInterOperability__ctor_m8_496(&L_34, (uint16_t)L_29, (uint16_t)2, (uint32_t)2, (ByteU5BU5D_t1_109*)L_30, /*hidden argument*/NULL);
		return L_34;
	}

IL_00f2:
	{
		Type_t * L_35 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(Byte_t1_11_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_35) == ((Object_t*)(Type_t *)L_36))))
		{
			goto IL_0124;
		}
	}
	{
		uint16_t L_37 = V_0;
		ByteU5BU5D_t1_109* L_38 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint8_t L_39 = (uint8_t)(__this->___mValue_3);
		uint8_t L_40 = L_39;
		Object_t * L_41 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_40);
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_38, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_41, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_42 = {0};
		ExifInterOperability__ctor_m8_496(&L_42, (uint16_t)L_37, (uint16_t)1, (uint32_t)1, (ByteU5BU5D_t1_109*)L_38, /*hidden argument*/NULL);
		return L_42;
	}

IL_0124:
	{
		Type_t * L_43 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_44 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(UInt16_t1_14_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_43) == ((Object_t*)(Type_t *)L_44))))
		{
			goto IL_0154;
		}
	}
	{
		uint16_t L_45 = V_0;
		uint8_t L_46 = (uint8_t)(__this->___mValue_3);
		uint8_t L_47 = L_46;
		Object_t * L_48 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_47);
		ByteU5BU5D_t1_109* L_49 = BitConverterEx_GetBytes_m8_354(NULL /*static, unused*/, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_48, UInt16_t1_14_il2cpp_TypeInfo_var)))), (int32_t)0, (int32_t)0, /*hidden argument*/NULL);
		ExifInterOperability_t8_113  L_50 = {0};
		ExifInterOperability__ctor_m8_496(&L_50, (uint16_t)L_45, (uint16_t)3, (uint32_t)1, (ByteU5BU5D_t1_109*)L_49, /*hidden argument*/NULL);
		return L_50;
	}

IL_0154:
	{
		UnknownEnumTypeException_t8_96 * L_51 = (UnknownEnumTypeException_t8_96 *)il2cpp_codegen_object_new (UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var);
		UnknownEnumTypeException__ctor_m8_407(L_51, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_51);
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSLongitudeRef>::op_Implicit(ExifLibrary.ExifEnumProperty`1<T>)
extern "C" uint8_t ExifEnumProperty_1_op_Implicit_m8_2296_gshared (Object_t * __this /* static, unused */, ExifEnumProperty_1_t8_362 * ___obj, const MethodInfo* method)
{
	{
		ExifEnumProperty_1_t8_362 * L_0 = ___obj;
		NullCheck(L_0);
		uint8_t L_1 = (uint8_t)(L_0->___mValue_3);
		return L_1;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSAltitudeRef>::.ctor(ExifLibrary.ExifTag,T,System.Boolean)
extern "C" void ExifEnumProperty_1__ctor_m8_2297_gshared (ExifEnumProperty_1_t8_363 * __this, int32_t ___tag, uint8_t ___value, bool ___isbitfield, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		NullCheck((ExifProperty_t8_99 *)__this);
		ExifProperty__ctor_m8_502((ExifProperty_t8_99 *)__this, (int32_t)L_0, /*hidden argument*/NULL);
		uint8_t L_1 = ___value;
		__this->___mValue_3 = L_1;
		bool L_2 = ___isbitfield;
		__this->___mIsBitField_4 = L_2;
		return;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSAltitudeRef>::.ctor(ExifLibrary.ExifTag,T)
extern "C" void ExifEnumProperty_1__ctor_m8_2000_gshared (ExifEnumProperty_1_t8_363 * __this, int32_t ___tag, uint8_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		uint8_t L_1 = ___value;
		NullCheck((ExifEnumProperty_1_t8_363 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_363 *, int32_t, uint8_t, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ExifEnumProperty_1_t8_363 *)__this, (int32_t)L_0, (uint8_t)L_1, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Object ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSAltitudeRef>::get__Value()
extern "C" Object_t * ExifEnumProperty_1_get__Value_m8_2298_gshared (ExifEnumProperty_1_t8_363 * __this, const MethodInfo* method)
{
	{
		NullCheck((ExifEnumProperty_1_t8_363 *)__this);
		uint8_t L_0 = (( uint8_t (*) (ExifEnumProperty_1_t8_363 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ExifEnumProperty_1_t8_363 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		uint8_t L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSAltitudeRef>::set__Value(System.Object)
extern "C" void ExifEnumProperty_1_set__Value_m8_2299_gshared (ExifEnumProperty_1_t8_363 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		NullCheck((ExifEnumProperty_1_t8_363 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_363 *, uint8_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((ExifEnumProperty_1_t8_363 *)__this, (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSAltitudeRef>::get_Value()
extern "C" uint8_t ExifEnumProperty_1_get_Value_m8_2300_gshared (ExifEnumProperty_1_t8_363 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = (uint8_t)(__this->___mValue_3);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSAltitudeRef>::set_Value(T)
extern "C" void ExifEnumProperty_1_set_Value_m8_2301_gshared (ExifEnumProperty_1_t8_363 * __this, uint8_t ___value, const MethodInfo* method)
{
	{
		uint8_t L_0 = ___value;
		__this->___mValue_3 = L_0;
		return;
	}
}
// System.Boolean ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSAltitudeRef>::get_IsBitField()
extern "C" bool ExifEnumProperty_1_get_IsBitField_m8_2302_gshared (ExifEnumProperty_1_t8_363 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(__this->___mIsBitField_4);
		return L_0;
	}
}
// System.String ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSAltitudeRef>::ToString()
extern "C" String_t* ExifEnumProperty_1_ToString_m8_2303_gshared (ExifEnumProperty_1_t8_363 * __this, const MethodInfo* method)
{
	{
		uint8_t* L_0 = (uint8_t*)&(__this->___mValue_3);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		return L_1;
	}
}
// ExifLibrary.ExifInterOperability ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSAltitudeRef>::get_Interoperability()
extern const Il2CppType* FileSource_t8_75_0_0_0_var;
extern const Il2CppType* SceneType_t8_76_0_0_0_var;
extern const Il2CppType* GPSLatitudeRef_t8_86_0_0_0_var;
extern const Il2CppType* GPSLongitudeRef_t8_87_0_0_0_var;
extern const Il2CppType* GPSStatus_t8_89_0_0_0_var;
extern const Il2CppType* GPSMeasureMode_t8_90_0_0_0_var;
extern const Il2CppType* GPSSpeedRef_t8_91_0_0_0_var;
extern const Il2CppType* GPSDirectionRef_t8_92_0_0_0_var;
extern const Il2CppType* GPSDistanceRef_t8_93_0_0_0_var;
extern const Il2CppType* Byte_t1_11_0_0_0_var;
extern const Il2CppType* UInt16_t1_14_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Enum_t1_24_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern TypeInfo* UInt16_t1_14_il2cpp_TypeInfo_var;
extern TypeInfo* UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var;
extern "C" ExifInterOperability_t8_113  ExifEnumProperty_1_get_Interoperability_m8_2304_gshared (ExifEnumProperty_1_t8_363 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FileSource_t8_75_0_0_0_var = il2cpp_codegen_type_from_index(2003);
		SceneType_t8_76_0_0_0_var = il2cpp_codegen_type_from_index(2004);
		GPSLatitudeRef_t8_86_0_0_0_var = il2cpp_codegen_type_from_index(2014);
		GPSLongitudeRef_t8_87_0_0_0_var = il2cpp_codegen_type_from_index(2015);
		GPSStatus_t8_89_0_0_0_var = il2cpp_codegen_type_from_index(2017);
		GPSMeasureMode_t8_90_0_0_0_var = il2cpp_codegen_type_from_index(2018);
		GPSSpeedRef_t8_91_0_0_0_var = il2cpp_codegen_type_from_index(2019);
		GPSDirectionRef_t8_92_0_0_0_var = il2cpp_codegen_type_from_index(2020);
		GPSDistanceRef_t8_93_0_0_0_var = il2cpp_codegen_type_from_index(2021);
		Byte_t1_11_0_0_0_var = il2cpp_codegen_type_from_index(25);
		UInt16_t1_14_0_0_0_var = il2cpp_codegen_type_from_index(29);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Enum_t1_24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		UInt16_t1_14_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4656);
		s_Il2CppMethodIntialized = true;
	}
	uint16_t V_0 = 0;
	Type_t * V_1 = {0};
	Type_t * V_2 = {0};
	{
		int32_t L_0 = (int32_t)(((ExifProperty_t8_99 *)__this)->___mTag_0);
		uint16_t L_1 = ExifTagFactory_GetTagID_m8_630(NULL /*static, unused*/, (int32_t)L_0, /*hidden argument*/NULL);
		V_0 = (uint16_t)L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		V_1 = (Type_t *)L_2;
		Type_t * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t1_24_il2cpp_TypeInfo_var);
		Type_t * L_4 = Enum_GetUnderlyingType_m1_942(NULL /*static, unused*/, (Type_t *)L_3, /*hidden argument*/NULL);
		V_2 = (Type_t *)L_4;
		Type_t * L_5 = V_1;
		Type_t * L_6 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(FileSource_t8_75_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_5) == ((Object_t*)(Type_t *)L_6)))
		{
			goto IL_003e;
		}
	}
	{
		Type_t * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(SceneType_t8_76_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_7) == ((Object_t*)(Type_t *)L_8))))
		{
			goto IL_0060;
		}
	}

IL_003e:
	{
		uint16_t L_9 = V_0;
		ByteU5BU5D_t1_109* L_10 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint8_t L_11 = (uint8_t)(__this->___mValue_3);
		uint8_t L_12 = L_11;
		Object_t * L_13 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_12);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_10, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_13, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_14 = {0};
		ExifInterOperability__ctor_m8_496(&L_14, (uint16_t)L_9, (uint16_t)7, (uint32_t)1, (ByteU5BU5D_t1_109*)L_10, /*hidden argument*/NULL);
		return L_14;
	}

IL_0060:
	{
		Type_t * L_15 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_16 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLatitudeRef_t8_86_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_15) == ((Object_t*)(Type_t *)L_16)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_17 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_18 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLongitudeRef_t8_87_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_17) == ((Object_t*)(Type_t *)L_18)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_19 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_20 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSStatus_t8_89_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_19) == ((Object_t*)(Type_t *)L_20)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_21 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_22 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSMeasureMode_t8_90_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_21) == ((Object_t*)(Type_t *)L_22)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_23 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_24 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSSpeedRef_t8_91_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_23) == ((Object_t*)(Type_t *)L_24)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_25 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_26 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDirectionRef_t8_92_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_25) == ((Object_t*)(Type_t *)L_26)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_27 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_28 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDistanceRef_t8_93_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_27) == ((Object_t*)(Type_t *)L_28))))
		{
			goto IL_00f2;
		}
	}

IL_00d0:
	{
		uint16_t L_29 = V_0;
		ByteU5BU5D_t1_109* L_30 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 2));
		uint8_t L_31 = (uint8_t)(__this->___mValue_3);
		uint8_t L_32 = L_31;
		Object_t * L_33 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_32);
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_30, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_33, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_34 = {0};
		ExifInterOperability__ctor_m8_496(&L_34, (uint16_t)L_29, (uint16_t)2, (uint32_t)2, (ByteU5BU5D_t1_109*)L_30, /*hidden argument*/NULL);
		return L_34;
	}

IL_00f2:
	{
		Type_t * L_35 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(Byte_t1_11_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_35) == ((Object_t*)(Type_t *)L_36))))
		{
			goto IL_0124;
		}
	}
	{
		uint16_t L_37 = V_0;
		ByteU5BU5D_t1_109* L_38 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint8_t L_39 = (uint8_t)(__this->___mValue_3);
		uint8_t L_40 = L_39;
		Object_t * L_41 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_40);
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_38, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_41, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_42 = {0};
		ExifInterOperability__ctor_m8_496(&L_42, (uint16_t)L_37, (uint16_t)1, (uint32_t)1, (ByteU5BU5D_t1_109*)L_38, /*hidden argument*/NULL);
		return L_42;
	}

IL_0124:
	{
		Type_t * L_43 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_44 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(UInt16_t1_14_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_43) == ((Object_t*)(Type_t *)L_44))))
		{
			goto IL_0154;
		}
	}
	{
		uint16_t L_45 = V_0;
		uint8_t L_46 = (uint8_t)(__this->___mValue_3);
		uint8_t L_47 = L_46;
		Object_t * L_48 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_47);
		ByteU5BU5D_t1_109* L_49 = BitConverterEx_GetBytes_m8_354(NULL /*static, unused*/, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_48, UInt16_t1_14_il2cpp_TypeInfo_var)))), (int32_t)0, (int32_t)0, /*hidden argument*/NULL);
		ExifInterOperability_t8_113  L_50 = {0};
		ExifInterOperability__ctor_m8_496(&L_50, (uint16_t)L_45, (uint16_t)3, (uint32_t)1, (ByteU5BU5D_t1_109*)L_49, /*hidden argument*/NULL);
		return L_50;
	}

IL_0154:
	{
		UnknownEnumTypeException_t8_96 * L_51 = (UnknownEnumTypeException_t8_96 *)il2cpp_codegen_object_new (UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var);
		UnknownEnumTypeException__ctor_m8_407(L_51, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_51);
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSAltitudeRef>::op_Implicit(ExifLibrary.ExifEnumProperty`1<T>)
extern "C" uint8_t ExifEnumProperty_1_op_Implicit_m8_2305_gshared (Object_t * __this /* static, unused */, ExifEnumProperty_1_t8_363 * ___obj, const MethodInfo* method)
{
	{
		ExifEnumProperty_1_t8_363 * L_0 = ___obj;
		NullCheck(L_0);
		uint8_t L_1 = (uint8_t)(L_0->___mValue_3);
		return L_1;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSStatus>::.ctor(ExifLibrary.ExifTag,T,System.Boolean)
extern "C" void ExifEnumProperty_1__ctor_m8_2306_gshared (ExifEnumProperty_1_t8_364 * __this, int32_t ___tag, uint8_t ___value, bool ___isbitfield, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		NullCheck((ExifProperty_t8_99 *)__this);
		ExifProperty__ctor_m8_502((ExifProperty_t8_99 *)__this, (int32_t)L_0, /*hidden argument*/NULL);
		uint8_t L_1 = ___value;
		__this->___mValue_3 = L_1;
		bool L_2 = ___isbitfield;
		__this->___mIsBitField_4 = L_2;
		return;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSStatus>::.ctor(ExifLibrary.ExifTag,T)
extern "C" void ExifEnumProperty_1__ctor_m8_2001_gshared (ExifEnumProperty_1_t8_364 * __this, int32_t ___tag, uint8_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		uint8_t L_1 = ___value;
		NullCheck((ExifEnumProperty_1_t8_364 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_364 *, int32_t, uint8_t, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ExifEnumProperty_1_t8_364 *)__this, (int32_t)L_0, (uint8_t)L_1, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Object ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSStatus>::get__Value()
extern "C" Object_t * ExifEnumProperty_1_get__Value_m8_2307_gshared (ExifEnumProperty_1_t8_364 * __this, const MethodInfo* method)
{
	{
		NullCheck((ExifEnumProperty_1_t8_364 *)__this);
		uint8_t L_0 = (( uint8_t (*) (ExifEnumProperty_1_t8_364 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ExifEnumProperty_1_t8_364 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		uint8_t L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSStatus>::set__Value(System.Object)
extern "C" void ExifEnumProperty_1_set__Value_m8_2308_gshared (ExifEnumProperty_1_t8_364 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		NullCheck((ExifEnumProperty_1_t8_364 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_364 *, uint8_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((ExifEnumProperty_1_t8_364 *)__this, (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSStatus>::get_Value()
extern "C" uint8_t ExifEnumProperty_1_get_Value_m8_2309_gshared (ExifEnumProperty_1_t8_364 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = (uint8_t)(__this->___mValue_3);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSStatus>::set_Value(T)
extern "C" void ExifEnumProperty_1_set_Value_m8_2310_gshared (ExifEnumProperty_1_t8_364 * __this, uint8_t ___value, const MethodInfo* method)
{
	{
		uint8_t L_0 = ___value;
		__this->___mValue_3 = L_0;
		return;
	}
}
// System.Boolean ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSStatus>::get_IsBitField()
extern "C" bool ExifEnumProperty_1_get_IsBitField_m8_2311_gshared (ExifEnumProperty_1_t8_364 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(__this->___mIsBitField_4);
		return L_0;
	}
}
// System.String ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSStatus>::ToString()
extern "C" String_t* ExifEnumProperty_1_ToString_m8_2312_gshared (ExifEnumProperty_1_t8_364 * __this, const MethodInfo* method)
{
	{
		uint8_t* L_0 = (uint8_t*)&(__this->___mValue_3);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		return L_1;
	}
}
// ExifLibrary.ExifInterOperability ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSStatus>::get_Interoperability()
extern const Il2CppType* FileSource_t8_75_0_0_0_var;
extern const Il2CppType* SceneType_t8_76_0_0_0_var;
extern const Il2CppType* GPSLatitudeRef_t8_86_0_0_0_var;
extern const Il2CppType* GPSLongitudeRef_t8_87_0_0_0_var;
extern const Il2CppType* GPSStatus_t8_89_0_0_0_var;
extern const Il2CppType* GPSMeasureMode_t8_90_0_0_0_var;
extern const Il2CppType* GPSSpeedRef_t8_91_0_0_0_var;
extern const Il2CppType* GPSDirectionRef_t8_92_0_0_0_var;
extern const Il2CppType* GPSDistanceRef_t8_93_0_0_0_var;
extern const Il2CppType* Byte_t1_11_0_0_0_var;
extern const Il2CppType* UInt16_t1_14_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Enum_t1_24_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern TypeInfo* UInt16_t1_14_il2cpp_TypeInfo_var;
extern TypeInfo* UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var;
extern "C" ExifInterOperability_t8_113  ExifEnumProperty_1_get_Interoperability_m8_2313_gshared (ExifEnumProperty_1_t8_364 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FileSource_t8_75_0_0_0_var = il2cpp_codegen_type_from_index(2003);
		SceneType_t8_76_0_0_0_var = il2cpp_codegen_type_from_index(2004);
		GPSLatitudeRef_t8_86_0_0_0_var = il2cpp_codegen_type_from_index(2014);
		GPSLongitudeRef_t8_87_0_0_0_var = il2cpp_codegen_type_from_index(2015);
		GPSStatus_t8_89_0_0_0_var = il2cpp_codegen_type_from_index(2017);
		GPSMeasureMode_t8_90_0_0_0_var = il2cpp_codegen_type_from_index(2018);
		GPSSpeedRef_t8_91_0_0_0_var = il2cpp_codegen_type_from_index(2019);
		GPSDirectionRef_t8_92_0_0_0_var = il2cpp_codegen_type_from_index(2020);
		GPSDistanceRef_t8_93_0_0_0_var = il2cpp_codegen_type_from_index(2021);
		Byte_t1_11_0_0_0_var = il2cpp_codegen_type_from_index(25);
		UInt16_t1_14_0_0_0_var = il2cpp_codegen_type_from_index(29);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Enum_t1_24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		UInt16_t1_14_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4656);
		s_Il2CppMethodIntialized = true;
	}
	uint16_t V_0 = 0;
	Type_t * V_1 = {0};
	Type_t * V_2 = {0};
	{
		int32_t L_0 = (int32_t)(((ExifProperty_t8_99 *)__this)->___mTag_0);
		uint16_t L_1 = ExifTagFactory_GetTagID_m8_630(NULL /*static, unused*/, (int32_t)L_0, /*hidden argument*/NULL);
		V_0 = (uint16_t)L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		V_1 = (Type_t *)L_2;
		Type_t * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t1_24_il2cpp_TypeInfo_var);
		Type_t * L_4 = Enum_GetUnderlyingType_m1_942(NULL /*static, unused*/, (Type_t *)L_3, /*hidden argument*/NULL);
		V_2 = (Type_t *)L_4;
		Type_t * L_5 = V_1;
		Type_t * L_6 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(FileSource_t8_75_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_5) == ((Object_t*)(Type_t *)L_6)))
		{
			goto IL_003e;
		}
	}
	{
		Type_t * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(SceneType_t8_76_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_7) == ((Object_t*)(Type_t *)L_8))))
		{
			goto IL_0060;
		}
	}

IL_003e:
	{
		uint16_t L_9 = V_0;
		ByteU5BU5D_t1_109* L_10 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint8_t L_11 = (uint8_t)(__this->___mValue_3);
		uint8_t L_12 = L_11;
		Object_t * L_13 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_12);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_10, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_13, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_14 = {0};
		ExifInterOperability__ctor_m8_496(&L_14, (uint16_t)L_9, (uint16_t)7, (uint32_t)1, (ByteU5BU5D_t1_109*)L_10, /*hidden argument*/NULL);
		return L_14;
	}

IL_0060:
	{
		Type_t * L_15 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_16 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLatitudeRef_t8_86_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_15) == ((Object_t*)(Type_t *)L_16)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_17 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_18 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLongitudeRef_t8_87_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_17) == ((Object_t*)(Type_t *)L_18)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_19 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_20 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSStatus_t8_89_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_19) == ((Object_t*)(Type_t *)L_20)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_21 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_22 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSMeasureMode_t8_90_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_21) == ((Object_t*)(Type_t *)L_22)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_23 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_24 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSSpeedRef_t8_91_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_23) == ((Object_t*)(Type_t *)L_24)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_25 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_26 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDirectionRef_t8_92_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_25) == ((Object_t*)(Type_t *)L_26)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_27 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_28 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDistanceRef_t8_93_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_27) == ((Object_t*)(Type_t *)L_28))))
		{
			goto IL_00f2;
		}
	}

IL_00d0:
	{
		uint16_t L_29 = V_0;
		ByteU5BU5D_t1_109* L_30 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 2));
		uint8_t L_31 = (uint8_t)(__this->___mValue_3);
		uint8_t L_32 = L_31;
		Object_t * L_33 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_32);
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_30, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_33, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_34 = {0};
		ExifInterOperability__ctor_m8_496(&L_34, (uint16_t)L_29, (uint16_t)2, (uint32_t)2, (ByteU5BU5D_t1_109*)L_30, /*hidden argument*/NULL);
		return L_34;
	}

IL_00f2:
	{
		Type_t * L_35 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(Byte_t1_11_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_35) == ((Object_t*)(Type_t *)L_36))))
		{
			goto IL_0124;
		}
	}
	{
		uint16_t L_37 = V_0;
		ByteU5BU5D_t1_109* L_38 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint8_t L_39 = (uint8_t)(__this->___mValue_3);
		uint8_t L_40 = L_39;
		Object_t * L_41 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_40);
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_38, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_41, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_42 = {0};
		ExifInterOperability__ctor_m8_496(&L_42, (uint16_t)L_37, (uint16_t)1, (uint32_t)1, (ByteU5BU5D_t1_109*)L_38, /*hidden argument*/NULL);
		return L_42;
	}

IL_0124:
	{
		Type_t * L_43 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_44 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(UInt16_t1_14_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_43) == ((Object_t*)(Type_t *)L_44))))
		{
			goto IL_0154;
		}
	}
	{
		uint16_t L_45 = V_0;
		uint8_t L_46 = (uint8_t)(__this->___mValue_3);
		uint8_t L_47 = L_46;
		Object_t * L_48 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_47);
		ByteU5BU5D_t1_109* L_49 = BitConverterEx_GetBytes_m8_354(NULL /*static, unused*/, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_48, UInt16_t1_14_il2cpp_TypeInfo_var)))), (int32_t)0, (int32_t)0, /*hidden argument*/NULL);
		ExifInterOperability_t8_113  L_50 = {0};
		ExifInterOperability__ctor_m8_496(&L_50, (uint16_t)L_45, (uint16_t)3, (uint32_t)1, (ByteU5BU5D_t1_109*)L_49, /*hidden argument*/NULL);
		return L_50;
	}

IL_0154:
	{
		UnknownEnumTypeException_t8_96 * L_51 = (UnknownEnumTypeException_t8_96 *)il2cpp_codegen_object_new (UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var);
		UnknownEnumTypeException__ctor_m8_407(L_51, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_51);
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSStatus>::op_Implicit(ExifLibrary.ExifEnumProperty`1<T>)
extern "C" uint8_t ExifEnumProperty_1_op_Implicit_m8_2314_gshared (Object_t * __this /* static, unused */, ExifEnumProperty_1_t8_364 * ___obj, const MethodInfo* method)
{
	{
		ExifEnumProperty_1_t8_364 * L_0 = ___obj;
		NullCheck(L_0);
		uint8_t L_1 = (uint8_t)(L_0->___mValue_3);
		return L_1;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSMeasureMode>::.ctor(ExifLibrary.ExifTag,T,System.Boolean)
extern "C" void ExifEnumProperty_1__ctor_m8_2315_gshared (ExifEnumProperty_1_t8_365 * __this, int32_t ___tag, uint8_t ___value, bool ___isbitfield, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		NullCheck((ExifProperty_t8_99 *)__this);
		ExifProperty__ctor_m8_502((ExifProperty_t8_99 *)__this, (int32_t)L_0, /*hidden argument*/NULL);
		uint8_t L_1 = ___value;
		__this->___mValue_3 = L_1;
		bool L_2 = ___isbitfield;
		__this->___mIsBitField_4 = L_2;
		return;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSMeasureMode>::.ctor(ExifLibrary.ExifTag,T)
extern "C" void ExifEnumProperty_1__ctor_m8_2002_gshared (ExifEnumProperty_1_t8_365 * __this, int32_t ___tag, uint8_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		uint8_t L_1 = ___value;
		NullCheck((ExifEnumProperty_1_t8_365 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_365 *, int32_t, uint8_t, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ExifEnumProperty_1_t8_365 *)__this, (int32_t)L_0, (uint8_t)L_1, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Object ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSMeasureMode>::get__Value()
extern "C" Object_t * ExifEnumProperty_1_get__Value_m8_2316_gshared (ExifEnumProperty_1_t8_365 * __this, const MethodInfo* method)
{
	{
		NullCheck((ExifEnumProperty_1_t8_365 *)__this);
		uint8_t L_0 = (( uint8_t (*) (ExifEnumProperty_1_t8_365 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ExifEnumProperty_1_t8_365 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		uint8_t L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSMeasureMode>::set__Value(System.Object)
extern "C" void ExifEnumProperty_1_set__Value_m8_2317_gshared (ExifEnumProperty_1_t8_365 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		NullCheck((ExifEnumProperty_1_t8_365 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_365 *, uint8_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((ExifEnumProperty_1_t8_365 *)__this, (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSMeasureMode>::get_Value()
extern "C" uint8_t ExifEnumProperty_1_get_Value_m8_2318_gshared (ExifEnumProperty_1_t8_365 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = (uint8_t)(__this->___mValue_3);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSMeasureMode>::set_Value(T)
extern "C" void ExifEnumProperty_1_set_Value_m8_2319_gshared (ExifEnumProperty_1_t8_365 * __this, uint8_t ___value, const MethodInfo* method)
{
	{
		uint8_t L_0 = ___value;
		__this->___mValue_3 = L_0;
		return;
	}
}
// System.Boolean ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSMeasureMode>::get_IsBitField()
extern "C" bool ExifEnumProperty_1_get_IsBitField_m8_2320_gshared (ExifEnumProperty_1_t8_365 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(__this->___mIsBitField_4);
		return L_0;
	}
}
// System.String ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSMeasureMode>::ToString()
extern "C" String_t* ExifEnumProperty_1_ToString_m8_2321_gshared (ExifEnumProperty_1_t8_365 * __this, const MethodInfo* method)
{
	{
		uint8_t* L_0 = (uint8_t*)&(__this->___mValue_3);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		return L_1;
	}
}
// ExifLibrary.ExifInterOperability ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSMeasureMode>::get_Interoperability()
extern const Il2CppType* FileSource_t8_75_0_0_0_var;
extern const Il2CppType* SceneType_t8_76_0_0_0_var;
extern const Il2CppType* GPSLatitudeRef_t8_86_0_0_0_var;
extern const Il2CppType* GPSLongitudeRef_t8_87_0_0_0_var;
extern const Il2CppType* GPSStatus_t8_89_0_0_0_var;
extern const Il2CppType* GPSMeasureMode_t8_90_0_0_0_var;
extern const Il2CppType* GPSSpeedRef_t8_91_0_0_0_var;
extern const Il2CppType* GPSDirectionRef_t8_92_0_0_0_var;
extern const Il2CppType* GPSDistanceRef_t8_93_0_0_0_var;
extern const Il2CppType* Byte_t1_11_0_0_0_var;
extern const Il2CppType* UInt16_t1_14_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Enum_t1_24_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern TypeInfo* UInt16_t1_14_il2cpp_TypeInfo_var;
extern TypeInfo* UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var;
extern "C" ExifInterOperability_t8_113  ExifEnumProperty_1_get_Interoperability_m8_2322_gshared (ExifEnumProperty_1_t8_365 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FileSource_t8_75_0_0_0_var = il2cpp_codegen_type_from_index(2003);
		SceneType_t8_76_0_0_0_var = il2cpp_codegen_type_from_index(2004);
		GPSLatitudeRef_t8_86_0_0_0_var = il2cpp_codegen_type_from_index(2014);
		GPSLongitudeRef_t8_87_0_0_0_var = il2cpp_codegen_type_from_index(2015);
		GPSStatus_t8_89_0_0_0_var = il2cpp_codegen_type_from_index(2017);
		GPSMeasureMode_t8_90_0_0_0_var = il2cpp_codegen_type_from_index(2018);
		GPSSpeedRef_t8_91_0_0_0_var = il2cpp_codegen_type_from_index(2019);
		GPSDirectionRef_t8_92_0_0_0_var = il2cpp_codegen_type_from_index(2020);
		GPSDistanceRef_t8_93_0_0_0_var = il2cpp_codegen_type_from_index(2021);
		Byte_t1_11_0_0_0_var = il2cpp_codegen_type_from_index(25);
		UInt16_t1_14_0_0_0_var = il2cpp_codegen_type_from_index(29);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Enum_t1_24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		UInt16_t1_14_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4656);
		s_Il2CppMethodIntialized = true;
	}
	uint16_t V_0 = 0;
	Type_t * V_1 = {0};
	Type_t * V_2 = {0};
	{
		int32_t L_0 = (int32_t)(((ExifProperty_t8_99 *)__this)->___mTag_0);
		uint16_t L_1 = ExifTagFactory_GetTagID_m8_630(NULL /*static, unused*/, (int32_t)L_0, /*hidden argument*/NULL);
		V_0 = (uint16_t)L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		V_1 = (Type_t *)L_2;
		Type_t * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t1_24_il2cpp_TypeInfo_var);
		Type_t * L_4 = Enum_GetUnderlyingType_m1_942(NULL /*static, unused*/, (Type_t *)L_3, /*hidden argument*/NULL);
		V_2 = (Type_t *)L_4;
		Type_t * L_5 = V_1;
		Type_t * L_6 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(FileSource_t8_75_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_5) == ((Object_t*)(Type_t *)L_6)))
		{
			goto IL_003e;
		}
	}
	{
		Type_t * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(SceneType_t8_76_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_7) == ((Object_t*)(Type_t *)L_8))))
		{
			goto IL_0060;
		}
	}

IL_003e:
	{
		uint16_t L_9 = V_0;
		ByteU5BU5D_t1_109* L_10 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint8_t L_11 = (uint8_t)(__this->___mValue_3);
		uint8_t L_12 = L_11;
		Object_t * L_13 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_12);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_10, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_13, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_14 = {0};
		ExifInterOperability__ctor_m8_496(&L_14, (uint16_t)L_9, (uint16_t)7, (uint32_t)1, (ByteU5BU5D_t1_109*)L_10, /*hidden argument*/NULL);
		return L_14;
	}

IL_0060:
	{
		Type_t * L_15 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_16 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLatitudeRef_t8_86_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_15) == ((Object_t*)(Type_t *)L_16)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_17 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_18 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLongitudeRef_t8_87_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_17) == ((Object_t*)(Type_t *)L_18)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_19 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_20 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSStatus_t8_89_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_19) == ((Object_t*)(Type_t *)L_20)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_21 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_22 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSMeasureMode_t8_90_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_21) == ((Object_t*)(Type_t *)L_22)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_23 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_24 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSSpeedRef_t8_91_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_23) == ((Object_t*)(Type_t *)L_24)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_25 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_26 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDirectionRef_t8_92_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_25) == ((Object_t*)(Type_t *)L_26)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_27 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_28 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDistanceRef_t8_93_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_27) == ((Object_t*)(Type_t *)L_28))))
		{
			goto IL_00f2;
		}
	}

IL_00d0:
	{
		uint16_t L_29 = V_0;
		ByteU5BU5D_t1_109* L_30 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 2));
		uint8_t L_31 = (uint8_t)(__this->___mValue_3);
		uint8_t L_32 = L_31;
		Object_t * L_33 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_32);
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_30, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_33, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_34 = {0};
		ExifInterOperability__ctor_m8_496(&L_34, (uint16_t)L_29, (uint16_t)2, (uint32_t)2, (ByteU5BU5D_t1_109*)L_30, /*hidden argument*/NULL);
		return L_34;
	}

IL_00f2:
	{
		Type_t * L_35 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(Byte_t1_11_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_35) == ((Object_t*)(Type_t *)L_36))))
		{
			goto IL_0124;
		}
	}
	{
		uint16_t L_37 = V_0;
		ByteU5BU5D_t1_109* L_38 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint8_t L_39 = (uint8_t)(__this->___mValue_3);
		uint8_t L_40 = L_39;
		Object_t * L_41 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_40);
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_38, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_41, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_42 = {0};
		ExifInterOperability__ctor_m8_496(&L_42, (uint16_t)L_37, (uint16_t)1, (uint32_t)1, (ByteU5BU5D_t1_109*)L_38, /*hidden argument*/NULL);
		return L_42;
	}

IL_0124:
	{
		Type_t * L_43 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_44 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(UInt16_t1_14_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_43) == ((Object_t*)(Type_t *)L_44))))
		{
			goto IL_0154;
		}
	}
	{
		uint16_t L_45 = V_0;
		uint8_t L_46 = (uint8_t)(__this->___mValue_3);
		uint8_t L_47 = L_46;
		Object_t * L_48 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_47);
		ByteU5BU5D_t1_109* L_49 = BitConverterEx_GetBytes_m8_354(NULL /*static, unused*/, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_48, UInt16_t1_14_il2cpp_TypeInfo_var)))), (int32_t)0, (int32_t)0, /*hidden argument*/NULL);
		ExifInterOperability_t8_113  L_50 = {0};
		ExifInterOperability__ctor_m8_496(&L_50, (uint16_t)L_45, (uint16_t)3, (uint32_t)1, (ByteU5BU5D_t1_109*)L_49, /*hidden argument*/NULL);
		return L_50;
	}

IL_0154:
	{
		UnknownEnumTypeException_t8_96 * L_51 = (UnknownEnumTypeException_t8_96 *)il2cpp_codegen_object_new (UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var);
		UnknownEnumTypeException__ctor_m8_407(L_51, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_51);
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSMeasureMode>::op_Implicit(ExifLibrary.ExifEnumProperty`1<T>)
extern "C" uint8_t ExifEnumProperty_1_op_Implicit_m8_2323_gshared (Object_t * __this /* static, unused */, ExifEnumProperty_1_t8_365 * ___obj, const MethodInfo* method)
{
	{
		ExifEnumProperty_1_t8_365 * L_0 = ___obj;
		NullCheck(L_0);
		uint8_t L_1 = (uint8_t)(L_0->___mValue_3);
		return L_1;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSSpeedRef>::.ctor(ExifLibrary.ExifTag,T,System.Boolean)
extern "C" void ExifEnumProperty_1__ctor_m8_2324_gshared (ExifEnumProperty_1_t8_366 * __this, int32_t ___tag, uint8_t ___value, bool ___isbitfield, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		NullCheck((ExifProperty_t8_99 *)__this);
		ExifProperty__ctor_m8_502((ExifProperty_t8_99 *)__this, (int32_t)L_0, /*hidden argument*/NULL);
		uint8_t L_1 = ___value;
		__this->___mValue_3 = L_1;
		bool L_2 = ___isbitfield;
		__this->___mIsBitField_4 = L_2;
		return;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSSpeedRef>::.ctor(ExifLibrary.ExifTag,T)
extern "C" void ExifEnumProperty_1__ctor_m8_2003_gshared (ExifEnumProperty_1_t8_366 * __this, int32_t ___tag, uint8_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		uint8_t L_1 = ___value;
		NullCheck((ExifEnumProperty_1_t8_366 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_366 *, int32_t, uint8_t, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ExifEnumProperty_1_t8_366 *)__this, (int32_t)L_0, (uint8_t)L_1, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Object ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSSpeedRef>::get__Value()
extern "C" Object_t * ExifEnumProperty_1_get__Value_m8_2325_gshared (ExifEnumProperty_1_t8_366 * __this, const MethodInfo* method)
{
	{
		NullCheck((ExifEnumProperty_1_t8_366 *)__this);
		uint8_t L_0 = (( uint8_t (*) (ExifEnumProperty_1_t8_366 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ExifEnumProperty_1_t8_366 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		uint8_t L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSSpeedRef>::set__Value(System.Object)
extern "C" void ExifEnumProperty_1_set__Value_m8_2326_gshared (ExifEnumProperty_1_t8_366 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		NullCheck((ExifEnumProperty_1_t8_366 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_366 *, uint8_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((ExifEnumProperty_1_t8_366 *)__this, (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSSpeedRef>::get_Value()
extern "C" uint8_t ExifEnumProperty_1_get_Value_m8_2327_gshared (ExifEnumProperty_1_t8_366 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = (uint8_t)(__this->___mValue_3);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSSpeedRef>::set_Value(T)
extern "C" void ExifEnumProperty_1_set_Value_m8_2328_gshared (ExifEnumProperty_1_t8_366 * __this, uint8_t ___value, const MethodInfo* method)
{
	{
		uint8_t L_0 = ___value;
		__this->___mValue_3 = L_0;
		return;
	}
}
// System.Boolean ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSSpeedRef>::get_IsBitField()
extern "C" bool ExifEnumProperty_1_get_IsBitField_m8_2329_gshared (ExifEnumProperty_1_t8_366 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(__this->___mIsBitField_4);
		return L_0;
	}
}
// System.String ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSSpeedRef>::ToString()
extern "C" String_t* ExifEnumProperty_1_ToString_m8_2330_gshared (ExifEnumProperty_1_t8_366 * __this, const MethodInfo* method)
{
	{
		uint8_t* L_0 = (uint8_t*)&(__this->___mValue_3);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		return L_1;
	}
}
// ExifLibrary.ExifInterOperability ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSSpeedRef>::get_Interoperability()
extern const Il2CppType* FileSource_t8_75_0_0_0_var;
extern const Il2CppType* SceneType_t8_76_0_0_0_var;
extern const Il2CppType* GPSLatitudeRef_t8_86_0_0_0_var;
extern const Il2CppType* GPSLongitudeRef_t8_87_0_0_0_var;
extern const Il2CppType* GPSStatus_t8_89_0_0_0_var;
extern const Il2CppType* GPSMeasureMode_t8_90_0_0_0_var;
extern const Il2CppType* GPSSpeedRef_t8_91_0_0_0_var;
extern const Il2CppType* GPSDirectionRef_t8_92_0_0_0_var;
extern const Il2CppType* GPSDistanceRef_t8_93_0_0_0_var;
extern const Il2CppType* Byte_t1_11_0_0_0_var;
extern const Il2CppType* UInt16_t1_14_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Enum_t1_24_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern TypeInfo* UInt16_t1_14_il2cpp_TypeInfo_var;
extern TypeInfo* UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var;
extern "C" ExifInterOperability_t8_113  ExifEnumProperty_1_get_Interoperability_m8_2331_gshared (ExifEnumProperty_1_t8_366 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FileSource_t8_75_0_0_0_var = il2cpp_codegen_type_from_index(2003);
		SceneType_t8_76_0_0_0_var = il2cpp_codegen_type_from_index(2004);
		GPSLatitudeRef_t8_86_0_0_0_var = il2cpp_codegen_type_from_index(2014);
		GPSLongitudeRef_t8_87_0_0_0_var = il2cpp_codegen_type_from_index(2015);
		GPSStatus_t8_89_0_0_0_var = il2cpp_codegen_type_from_index(2017);
		GPSMeasureMode_t8_90_0_0_0_var = il2cpp_codegen_type_from_index(2018);
		GPSSpeedRef_t8_91_0_0_0_var = il2cpp_codegen_type_from_index(2019);
		GPSDirectionRef_t8_92_0_0_0_var = il2cpp_codegen_type_from_index(2020);
		GPSDistanceRef_t8_93_0_0_0_var = il2cpp_codegen_type_from_index(2021);
		Byte_t1_11_0_0_0_var = il2cpp_codegen_type_from_index(25);
		UInt16_t1_14_0_0_0_var = il2cpp_codegen_type_from_index(29);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Enum_t1_24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		UInt16_t1_14_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4656);
		s_Il2CppMethodIntialized = true;
	}
	uint16_t V_0 = 0;
	Type_t * V_1 = {0};
	Type_t * V_2 = {0};
	{
		int32_t L_0 = (int32_t)(((ExifProperty_t8_99 *)__this)->___mTag_0);
		uint16_t L_1 = ExifTagFactory_GetTagID_m8_630(NULL /*static, unused*/, (int32_t)L_0, /*hidden argument*/NULL);
		V_0 = (uint16_t)L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		V_1 = (Type_t *)L_2;
		Type_t * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t1_24_il2cpp_TypeInfo_var);
		Type_t * L_4 = Enum_GetUnderlyingType_m1_942(NULL /*static, unused*/, (Type_t *)L_3, /*hidden argument*/NULL);
		V_2 = (Type_t *)L_4;
		Type_t * L_5 = V_1;
		Type_t * L_6 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(FileSource_t8_75_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_5) == ((Object_t*)(Type_t *)L_6)))
		{
			goto IL_003e;
		}
	}
	{
		Type_t * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(SceneType_t8_76_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_7) == ((Object_t*)(Type_t *)L_8))))
		{
			goto IL_0060;
		}
	}

IL_003e:
	{
		uint16_t L_9 = V_0;
		ByteU5BU5D_t1_109* L_10 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint8_t L_11 = (uint8_t)(__this->___mValue_3);
		uint8_t L_12 = L_11;
		Object_t * L_13 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_12);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_10, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_13, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_14 = {0};
		ExifInterOperability__ctor_m8_496(&L_14, (uint16_t)L_9, (uint16_t)7, (uint32_t)1, (ByteU5BU5D_t1_109*)L_10, /*hidden argument*/NULL);
		return L_14;
	}

IL_0060:
	{
		Type_t * L_15 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_16 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLatitudeRef_t8_86_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_15) == ((Object_t*)(Type_t *)L_16)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_17 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_18 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLongitudeRef_t8_87_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_17) == ((Object_t*)(Type_t *)L_18)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_19 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_20 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSStatus_t8_89_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_19) == ((Object_t*)(Type_t *)L_20)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_21 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_22 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSMeasureMode_t8_90_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_21) == ((Object_t*)(Type_t *)L_22)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_23 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_24 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSSpeedRef_t8_91_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_23) == ((Object_t*)(Type_t *)L_24)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_25 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_26 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDirectionRef_t8_92_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_25) == ((Object_t*)(Type_t *)L_26)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_27 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_28 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDistanceRef_t8_93_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_27) == ((Object_t*)(Type_t *)L_28))))
		{
			goto IL_00f2;
		}
	}

IL_00d0:
	{
		uint16_t L_29 = V_0;
		ByteU5BU5D_t1_109* L_30 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 2));
		uint8_t L_31 = (uint8_t)(__this->___mValue_3);
		uint8_t L_32 = L_31;
		Object_t * L_33 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_32);
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_30, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_33, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_34 = {0};
		ExifInterOperability__ctor_m8_496(&L_34, (uint16_t)L_29, (uint16_t)2, (uint32_t)2, (ByteU5BU5D_t1_109*)L_30, /*hidden argument*/NULL);
		return L_34;
	}

IL_00f2:
	{
		Type_t * L_35 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(Byte_t1_11_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_35) == ((Object_t*)(Type_t *)L_36))))
		{
			goto IL_0124;
		}
	}
	{
		uint16_t L_37 = V_0;
		ByteU5BU5D_t1_109* L_38 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint8_t L_39 = (uint8_t)(__this->___mValue_3);
		uint8_t L_40 = L_39;
		Object_t * L_41 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_40);
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_38, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_41, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_42 = {0};
		ExifInterOperability__ctor_m8_496(&L_42, (uint16_t)L_37, (uint16_t)1, (uint32_t)1, (ByteU5BU5D_t1_109*)L_38, /*hidden argument*/NULL);
		return L_42;
	}

IL_0124:
	{
		Type_t * L_43 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_44 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(UInt16_t1_14_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_43) == ((Object_t*)(Type_t *)L_44))))
		{
			goto IL_0154;
		}
	}
	{
		uint16_t L_45 = V_0;
		uint8_t L_46 = (uint8_t)(__this->___mValue_3);
		uint8_t L_47 = L_46;
		Object_t * L_48 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_47);
		ByteU5BU5D_t1_109* L_49 = BitConverterEx_GetBytes_m8_354(NULL /*static, unused*/, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_48, UInt16_t1_14_il2cpp_TypeInfo_var)))), (int32_t)0, (int32_t)0, /*hidden argument*/NULL);
		ExifInterOperability_t8_113  L_50 = {0};
		ExifInterOperability__ctor_m8_496(&L_50, (uint16_t)L_45, (uint16_t)3, (uint32_t)1, (ByteU5BU5D_t1_109*)L_49, /*hidden argument*/NULL);
		return L_50;
	}

IL_0154:
	{
		UnknownEnumTypeException_t8_96 * L_51 = (UnknownEnumTypeException_t8_96 *)il2cpp_codegen_object_new (UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var);
		UnknownEnumTypeException__ctor_m8_407(L_51, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_51);
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSSpeedRef>::op_Implicit(ExifLibrary.ExifEnumProperty`1<T>)
extern "C" uint8_t ExifEnumProperty_1_op_Implicit_m8_2332_gshared (Object_t * __this /* static, unused */, ExifEnumProperty_1_t8_366 * ___obj, const MethodInfo* method)
{
	{
		ExifEnumProperty_1_t8_366 * L_0 = ___obj;
		NullCheck(L_0);
		uint8_t L_1 = (uint8_t)(L_0->___mValue_3);
		return L_1;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSDirectionRef>::.ctor(ExifLibrary.ExifTag,T,System.Boolean)
extern "C" void ExifEnumProperty_1__ctor_m8_2333_gshared (ExifEnumProperty_1_t8_367 * __this, int32_t ___tag, uint8_t ___value, bool ___isbitfield, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		NullCheck((ExifProperty_t8_99 *)__this);
		ExifProperty__ctor_m8_502((ExifProperty_t8_99 *)__this, (int32_t)L_0, /*hidden argument*/NULL);
		uint8_t L_1 = ___value;
		__this->___mValue_3 = L_1;
		bool L_2 = ___isbitfield;
		__this->___mIsBitField_4 = L_2;
		return;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSDirectionRef>::.ctor(ExifLibrary.ExifTag,T)
extern "C" void ExifEnumProperty_1__ctor_m8_2004_gshared (ExifEnumProperty_1_t8_367 * __this, int32_t ___tag, uint8_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		uint8_t L_1 = ___value;
		NullCheck((ExifEnumProperty_1_t8_367 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_367 *, int32_t, uint8_t, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ExifEnumProperty_1_t8_367 *)__this, (int32_t)L_0, (uint8_t)L_1, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Object ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSDirectionRef>::get__Value()
extern "C" Object_t * ExifEnumProperty_1_get__Value_m8_2334_gshared (ExifEnumProperty_1_t8_367 * __this, const MethodInfo* method)
{
	{
		NullCheck((ExifEnumProperty_1_t8_367 *)__this);
		uint8_t L_0 = (( uint8_t (*) (ExifEnumProperty_1_t8_367 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ExifEnumProperty_1_t8_367 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		uint8_t L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSDirectionRef>::set__Value(System.Object)
extern "C" void ExifEnumProperty_1_set__Value_m8_2335_gshared (ExifEnumProperty_1_t8_367 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		NullCheck((ExifEnumProperty_1_t8_367 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_367 *, uint8_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((ExifEnumProperty_1_t8_367 *)__this, (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSDirectionRef>::get_Value()
extern "C" uint8_t ExifEnumProperty_1_get_Value_m8_2336_gshared (ExifEnumProperty_1_t8_367 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = (uint8_t)(__this->___mValue_3);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSDirectionRef>::set_Value(T)
extern "C" void ExifEnumProperty_1_set_Value_m8_2337_gshared (ExifEnumProperty_1_t8_367 * __this, uint8_t ___value, const MethodInfo* method)
{
	{
		uint8_t L_0 = ___value;
		__this->___mValue_3 = L_0;
		return;
	}
}
// System.Boolean ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSDirectionRef>::get_IsBitField()
extern "C" bool ExifEnumProperty_1_get_IsBitField_m8_2338_gshared (ExifEnumProperty_1_t8_367 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(__this->___mIsBitField_4);
		return L_0;
	}
}
// System.String ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSDirectionRef>::ToString()
extern "C" String_t* ExifEnumProperty_1_ToString_m8_2339_gshared (ExifEnumProperty_1_t8_367 * __this, const MethodInfo* method)
{
	{
		uint8_t* L_0 = (uint8_t*)&(__this->___mValue_3);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		return L_1;
	}
}
// ExifLibrary.ExifInterOperability ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSDirectionRef>::get_Interoperability()
extern const Il2CppType* FileSource_t8_75_0_0_0_var;
extern const Il2CppType* SceneType_t8_76_0_0_0_var;
extern const Il2CppType* GPSLatitudeRef_t8_86_0_0_0_var;
extern const Il2CppType* GPSLongitudeRef_t8_87_0_0_0_var;
extern const Il2CppType* GPSStatus_t8_89_0_0_0_var;
extern const Il2CppType* GPSMeasureMode_t8_90_0_0_0_var;
extern const Il2CppType* GPSSpeedRef_t8_91_0_0_0_var;
extern const Il2CppType* GPSDirectionRef_t8_92_0_0_0_var;
extern const Il2CppType* GPSDistanceRef_t8_93_0_0_0_var;
extern const Il2CppType* Byte_t1_11_0_0_0_var;
extern const Il2CppType* UInt16_t1_14_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Enum_t1_24_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern TypeInfo* UInt16_t1_14_il2cpp_TypeInfo_var;
extern TypeInfo* UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var;
extern "C" ExifInterOperability_t8_113  ExifEnumProperty_1_get_Interoperability_m8_2340_gshared (ExifEnumProperty_1_t8_367 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FileSource_t8_75_0_0_0_var = il2cpp_codegen_type_from_index(2003);
		SceneType_t8_76_0_0_0_var = il2cpp_codegen_type_from_index(2004);
		GPSLatitudeRef_t8_86_0_0_0_var = il2cpp_codegen_type_from_index(2014);
		GPSLongitudeRef_t8_87_0_0_0_var = il2cpp_codegen_type_from_index(2015);
		GPSStatus_t8_89_0_0_0_var = il2cpp_codegen_type_from_index(2017);
		GPSMeasureMode_t8_90_0_0_0_var = il2cpp_codegen_type_from_index(2018);
		GPSSpeedRef_t8_91_0_0_0_var = il2cpp_codegen_type_from_index(2019);
		GPSDirectionRef_t8_92_0_0_0_var = il2cpp_codegen_type_from_index(2020);
		GPSDistanceRef_t8_93_0_0_0_var = il2cpp_codegen_type_from_index(2021);
		Byte_t1_11_0_0_0_var = il2cpp_codegen_type_from_index(25);
		UInt16_t1_14_0_0_0_var = il2cpp_codegen_type_from_index(29);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Enum_t1_24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		UInt16_t1_14_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4656);
		s_Il2CppMethodIntialized = true;
	}
	uint16_t V_0 = 0;
	Type_t * V_1 = {0};
	Type_t * V_2 = {0};
	{
		int32_t L_0 = (int32_t)(((ExifProperty_t8_99 *)__this)->___mTag_0);
		uint16_t L_1 = ExifTagFactory_GetTagID_m8_630(NULL /*static, unused*/, (int32_t)L_0, /*hidden argument*/NULL);
		V_0 = (uint16_t)L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		V_1 = (Type_t *)L_2;
		Type_t * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t1_24_il2cpp_TypeInfo_var);
		Type_t * L_4 = Enum_GetUnderlyingType_m1_942(NULL /*static, unused*/, (Type_t *)L_3, /*hidden argument*/NULL);
		V_2 = (Type_t *)L_4;
		Type_t * L_5 = V_1;
		Type_t * L_6 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(FileSource_t8_75_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_5) == ((Object_t*)(Type_t *)L_6)))
		{
			goto IL_003e;
		}
	}
	{
		Type_t * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(SceneType_t8_76_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_7) == ((Object_t*)(Type_t *)L_8))))
		{
			goto IL_0060;
		}
	}

IL_003e:
	{
		uint16_t L_9 = V_0;
		ByteU5BU5D_t1_109* L_10 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint8_t L_11 = (uint8_t)(__this->___mValue_3);
		uint8_t L_12 = L_11;
		Object_t * L_13 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_12);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_10, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_13, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_14 = {0};
		ExifInterOperability__ctor_m8_496(&L_14, (uint16_t)L_9, (uint16_t)7, (uint32_t)1, (ByteU5BU5D_t1_109*)L_10, /*hidden argument*/NULL);
		return L_14;
	}

IL_0060:
	{
		Type_t * L_15 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_16 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLatitudeRef_t8_86_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_15) == ((Object_t*)(Type_t *)L_16)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_17 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_18 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLongitudeRef_t8_87_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_17) == ((Object_t*)(Type_t *)L_18)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_19 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_20 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSStatus_t8_89_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_19) == ((Object_t*)(Type_t *)L_20)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_21 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_22 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSMeasureMode_t8_90_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_21) == ((Object_t*)(Type_t *)L_22)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_23 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_24 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSSpeedRef_t8_91_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_23) == ((Object_t*)(Type_t *)L_24)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_25 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_26 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDirectionRef_t8_92_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_25) == ((Object_t*)(Type_t *)L_26)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_27 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_28 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDistanceRef_t8_93_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_27) == ((Object_t*)(Type_t *)L_28))))
		{
			goto IL_00f2;
		}
	}

IL_00d0:
	{
		uint16_t L_29 = V_0;
		ByteU5BU5D_t1_109* L_30 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 2));
		uint8_t L_31 = (uint8_t)(__this->___mValue_3);
		uint8_t L_32 = L_31;
		Object_t * L_33 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_32);
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_30, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_33, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_34 = {0};
		ExifInterOperability__ctor_m8_496(&L_34, (uint16_t)L_29, (uint16_t)2, (uint32_t)2, (ByteU5BU5D_t1_109*)L_30, /*hidden argument*/NULL);
		return L_34;
	}

IL_00f2:
	{
		Type_t * L_35 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(Byte_t1_11_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_35) == ((Object_t*)(Type_t *)L_36))))
		{
			goto IL_0124;
		}
	}
	{
		uint16_t L_37 = V_0;
		ByteU5BU5D_t1_109* L_38 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint8_t L_39 = (uint8_t)(__this->___mValue_3);
		uint8_t L_40 = L_39;
		Object_t * L_41 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_40);
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_38, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_41, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_42 = {0};
		ExifInterOperability__ctor_m8_496(&L_42, (uint16_t)L_37, (uint16_t)1, (uint32_t)1, (ByteU5BU5D_t1_109*)L_38, /*hidden argument*/NULL);
		return L_42;
	}

IL_0124:
	{
		Type_t * L_43 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_44 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(UInt16_t1_14_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_43) == ((Object_t*)(Type_t *)L_44))))
		{
			goto IL_0154;
		}
	}
	{
		uint16_t L_45 = V_0;
		uint8_t L_46 = (uint8_t)(__this->___mValue_3);
		uint8_t L_47 = L_46;
		Object_t * L_48 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_47);
		ByteU5BU5D_t1_109* L_49 = BitConverterEx_GetBytes_m8_354(NULL /*static, unused*/, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_48, UInt16_t1_14_il2cpp_TypeInfo_var)))), (int32_t)0, (int32_t)0, /*hidden argument*/NULL);
		ExifInterOperability_t8_113  L_50 = {0};
		ExifInterOperability__ctor_m8_496(&L_50, (uint16_t)L_45, (uint16_t)3, (uint32_t)1, (ByteU5BU5D_t1_109*)L_49, /*hidden argument*/NULL);
		return L_50;
	}

IL_0154:
	{
		UnknownEnumTypeException_t8_96 * L_51 = (UnknownEnumTypeException_t8_96 *)il2cpp_codegen_object_new (UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var);
		UnknownEnumTypeException__ctor_m8_407(L_51, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_51);
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSDirectionRef>::op_Implicit(ExifLibrary.ExifEnumProperty`1<T>)
extern "C" uint8_t ExifEnumProperty_1_op_Implicit_m8_2341_gshared (Object_t * __this /* static, unused */, ExifEnumProperty_1_t8_367 * ___obj, const MethodInfo* method)
{
	{
		ExifEnumProperty_1_t8_367 * L_0 = ___obj;
		NullCheck(L_0);
		uint8_t L_1 = (uint8_t)(L_0->___mValue_3);
		return L_1;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSDistanceRef>::.ctor(ExifLibrary.ExifTag,T,System.Boolean)
extern "C" void ExifEnumProperty_1__ctor_m8_2342_gshared (ExifEnumProperty_1_t8_368 * __this, int32_t ___tag, uint8_t ___value, bool ___isbitfield, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		NullCheck((ExifProperty_t8_99 *)__this);
		ExifProperty__ctor_m8_502((ExifProperty_t8_99 *)__this, (int32_t)L_0, /*hidden argument*/NULL);
		uint8_t L_1 = ___value;
		__this->___mValue_3 = L_1;
		bool L_2 = ___isbitfield;
		__this->___mIsBitField_4 = L_2;
		return;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSDistanceRef>::.ctor(ExifLibrary.ExifTag,T)
extern "C" void ExifEnumProperty_1__ctor_m8_2005_gshared (ExifEnumProperty_1_t8_368 * __this, int32_t ___tag, uint8_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		uint8_t L_1 = ___value;
		NullCheck((ExifEnumProperty_1_t8_368 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_368 *, int32_t, uint8_t, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ExifEnumProperty_1_t8_368 *)__this, (int32_t)L_0, (uint8_t)L_1, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Object ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSDistanceRef>::get__Value()
extern "C" Object_t * ExifEnumProperty_1_get__Value_m8_2343_gshared (ExifEnumProperty_1_t8_368 * __this, const MethodInfo* method)
{
	{
		NullCheck((ExifEnumProperty_1_t8_368 *)__this);
		uint8_t L_0 = (( uint8_t (*) (ExifEnumProperty_1_t8_368 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ExifEnumProperty_1_t8_368 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		uint8_t L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSDistanceRef>::set__Value(System.Object)
extern "C" void ExifEnumProperty_1_set__Value_m8_2344_gshared (ExifEnumProperty_1_t8_368 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		NullCheck((ExifEnumProperty_1_t8_368 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_368 *, uint8_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((ExifEnumProperty_1_t8_368 *)__this, (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSDistanceRef>::get_Value()
extern "C" uint8_t ExifEnumProperty_1_get_Value_m8_2345_gshared (ExifEnumProperty_1_t8_368 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = (uint8_t)(__this->___mValue_3);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSDistanceRef>::set_Value(T)
extern "C" void ExifEnumProperty_1_set_Value_m8_2346_gshared (ExifEnumProperty_1_t8_368 * __this, uint8_t ___value, const MethodInfo* method)
{
	{
		uint8_t L_0 = ___value;
		__this->___mValue_3 = L_0;
		return;
	}
}
// System.Boolean ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSDistanceRef>::get_IsBitField()
extern "C" bool ExifEnumProperty_1_get_IsBitField_m8_2347_gshared (ExifEnumProperty_1_t8_368 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(__this->___mIsBitField_4);
		return L_0;
	}
}
// System.String ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSDistanceRef>::ToString()
extern "C" String_t* ExifEnumProperty_1_ToString_m8_2348_gshared (ExifEnumProperty_1_t8_368 * __this, const MethodInfo* method)
{
	{
		uint8_t* L_0 = (uint8_t*)&(__this->___mValue_3);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		return L_1;
	}
}
// ExifLibrary.ExifInterOperability ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSDistanceRef>::get_Interoperability()
extern const Il2CppType* FileSource_t8_75_0_0_0_var;
extern const Il2CppType* SceneType_t8_76_0_0_0_var;
extern const Il2CppType* GPSLatitudeRef_t8_86_0_0_0_var;
extern const Il2CppType* GPSLongitudeRef_t8_87_0_0_0_var;
extern const Il2CppType* GPSStatus_t8_89_0_0_0_var;
extern const Il2CppType* GPSMeasureMode_t8_90_0_0_0_var;
extern const Il2CppType* GPSSpeedRef_t8_91_0_0_0_var;
extern const Il2CppType* GPSDirectionRef_t8_92_0_0_0_var;
extern const Il2CppType* GPSDistanceRef_t8_93_0_0_0_var;
extern const Il2CppType* Byte_t1_11_0_0_0_var;
extern const Il2CppType* UInt16_t1_14_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Enum_t1_24_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern TypeInfo* UInt16_t1_14_il2cpp_TypeInfo_var;
extern TypeInfo* UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var;
extern "C" ExifInterOperability_t8_113  ExifEnumProperty_1_get_Interoperability_m8_2349_gshared (ExifEnumProperty_1_t8_368 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FileSource_t8_75_0_0_0_var = il2cpp_codegen_type_from_index(2003);
		SceneType_t8_76_0_0_0_var = il2cpp_codegen_type_from_index(2004);
		GPSLatitudeRef_t8_86_0_0_0_var = il2cpp_codegen_type_from_index(2014);
		GPSLongitudeRef_t8_87_0_0_0_var = il2cpp_codegen_type_from_index(2015);
		GPSStatus_t8_89_0_0_0_var = il2cpp_codegen_type_from_index(2017);
		GPSMeasureMode_t8_90_0_0_0_var = il2cpp_codegen_type_from_index(2018);
		GPSSpeedRef_t8_91_0_0_0_var = il2cpp_codegen_type_from_index(2019);
		GPSDirectionRef_t8_92_0_0_0_var = il2cpp_codegen_type_from_index(2020);
		GPSDistanceRef_t8_93_0_0_0_var = il2cpp_codegen_type_from_index(2021);
		Byte_t1_11_0_0_0_var = il2cpp_codegen_type_from_index(25);
		UInt16_t1_14_0_0_0_var = il2cpp_codegen_type_from_index(29);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Enum_t1_24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		UInt16_t1_14_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4656);
		s_Il2CppMethodIntialized = true;
	}
	uint16_t V_0 = 0;
	Type_t * V_1 = {0};
	Type_t * V_2 = {0};
	{
		int32_t L_0 = (int32_t)(((ExifProperty_t8_99 *)__this)->___mTag_0);
		uint16_t L_1 = ExifTagFactory_GetTagID_m8_630(NULL /*static, unused*/, (int32_t)L_0, /*hidden argument*/NULL);
		V_0 = (uint16_t)L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		V_1 = (Type_t *)L_2;
		Type_t * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t1_24_il2cpp_TypeInfo_var);
		Type_t * L_4 = Enum_GetUnderlyingType_m1_942(NULL /*static, unused*/, (Type_t *)L_3, /*hidden argument*/NULL);
		V_2 = (Type_t *)L_4;
		Type_t * L_5 = V_1;
		Type_t * L_6 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(FileSource_t8_75_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_5) == ((Object_t*)(Type_t *)L_6)))
		{
			goto IL_003e;
		}
	}
	{
		Type_t * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(SceneType_t8_76_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_7) == ((Object_t*)(Type_t *)L_8))))
		{
			goto IL_0060;
		}
	}

IL_003e:
	{
		uint16_t L_9 = V_0;
		ByteU5BU5D_t1_109* L_10 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint8_t L_11 = (uint8_t)(__this->___mValue_3);
		uint8_t L_12 = L_11;
		Object_t * L_13 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_12);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_10, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_13, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_14 = {0};
		ExifInterOperability__ctor_m8_496(&L_14, (uint16_t)L_9, (uint16_t)7, (uint32_t)1, (ByteU5BU5D_t1_109*)L_10, /*hidden argument*/NULL);
		return L_14;
	}

IL_0060:
	{
		Type_t * L_15 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_16 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLatitudeRef_t8_86_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_15) == ((Object_t*)(Type_t *)L_16)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_17 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_18 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLongitudeRef_t8_87_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_17) == ((Object_t*)(Type_t *)L_18)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_19 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_20 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSStatus_t8_89_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_19) == ((Object_t*)(Type_t *)L_20)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_21 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_22 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSMeasureMode_t8_90_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_21) == ((Object_t*)(Type_t *)L_22)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_23 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_24 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSSpeedRef_t8_91_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_23) == ((Object_t*)(Type_t *)L_24)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_25 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_26 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDirectionRef_t8_92_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_25) == ((Object_t*)(Type_t *)L_26)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_27 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_28 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDistanceRef_t8_93_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_27) == ((Object_t*)(Type_t *)L_28))))
		{
			goto IL_00f2;
		}
	}

IL_00d0:
	{
		uint16_t L_29 = V_0;
		ByteU5BU5D_t1_109* L_30 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 2));
		uint8_t L_31 = (uint8_t)(__this->___mValue_3);
		uint8_t L_32 = L_31;
		Object_t * L_33 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_32);
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_30, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_33, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_34 = {0};
		ExifInterOperability__ctor_m8_496(&L_34, (uint16_t)L_29, (uint16_t)2, (uint32_t)2, (ByteU5BU5D_t1_109*)L_30, /*hidden argument*/NULL);
		return L_34;
	}

IL_00f2:
	{
		Type_t * L_35 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(Byte_t1_11_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_35) == ((Object_t*)(Type_t *)L_36))))
		{
			goto IL_0124;
		}
	}
	{
		uint16_t L_37 = V_0;
		ByteU5BU5D_t1_109* L_38 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint8_t L_39 = (uint8_t)(__this->___mValue_3);
		uint8_t L_40 = L_39;
		Object_t * L_41 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_40);
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_38, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_41, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_42 = {0};
		ExifInterOperability__ctor_m8_496(&L_42, (uint16_t)L_37, (uint16_t)1, (uint32_t)1, (ByteU5BU5D_t1_109*)L_38, /*hidden argument*/NULL);
		return L_42;
	}

IL_0124:
	{
		Type_t * L_43 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_44 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(UInt16_t1_14_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_43) == ((Object_t*)(Type_t *)L_44))))
		{
			goto IL_0154;
		}
	}
	{
		uint16_t L_45 = V_0;
		uint8_t L_46 = (uint8_t)(__this->___mValue_3);
		uint8_t L_47 = L_46;
		Object_t * L_48 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_47);
		ByteU5BU5D_t1_109* L_49 = BitConverterEx_GetBytes_m8_354(NULL /*static, unused*/, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_48, UInt16_t1_14_il2cpp_TypeInfo_var)))), (int32_t)0, (int32_t)0, /*hidden argument*/NULL);
		ExifInterOperability_t8_113  L_50 = {0};
		ExifInterOperability__ctor_m8_496(&L_50, (uint16_t)L_45, (uint16_t)3, (uint32_t)1, (ByteU5BU5D_t1_109*)L_49, /*hidden argument*/NULL);
		return L_50;
	}

IL_0154:
	{
		UnknownEnumTypeException_t8_96 * L_51 = (UnknownEnumTypeException_t8_96 *)il2cpp_codegen_object_new (UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var);
		UnknownEnumTypeException__ctor_m8_407(L_51, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_51);
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSDistanceRef>::op_Implicit(ExifLibrary.ExifEnumProperty`1<T>)
extern "C" uint8_t ExifEnumProperty_1_op_Implicit_m8_2350_gshared (Object_t * __this /* static, unused */, ExifEnumProperty_1_t8_368 * ___obj, const MethodInfo* method)
{
	{
		ExifEnumProperty_1_t8_368 * L_0 = ___obj;
		NullCheck(L_0);
		uint8_t L_1 = (uint8_t)(L_0->___mValue_3);
		return L_1;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSDifferential>::.ctor(ExifLibrary.ExifTag,T,System.Boolean)
extern "C" void ExifEnumProperty_1__ctor_m8_2351_gshared (ExifEnumProperty_1_t8_369 * __this, int32_t ___tag, uint16_t ___value, bool ___isbitfield, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		NullCheck((ExifProperty_t8_99 *)__this);
		ExifProperty__ctor_m8_502((ExifProperty_t8_99 *)__this, (int32_t)L_0, /*hidden argument*/NULL);
		uint16_t L_1 = ___value;
		__this->___mValue_3 = L_1;
		bool L_2 = ___isbitfield;
		__this->___mIsBitField_4 = L_2;
		return;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSDifferential>::.ctor(ExifLibrary.ExifTag,T)
extern "C" void ExifEnumProperty_1__ctor_m8_2006_gshared (ExifEnumProperty_1_t8_369 * __this, int32_t ___tag, uint16_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		uint16_t L_1 = ___value;
		NullCheck((ExifEnumProperty_1_t8_369 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_369 *, int32_t, uint16_t, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ExifEnumProperty_1_t8_369 *)__this, (int32_t)L_0, (uint16_t)L_1, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Object ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSDifferential>::get__Value()
extern "C" Object_t * ExifEnumProperty_1_get__Value_m8_2352_gshared (ExifEnumProperty_1_t8_369 * __this, const MethodInfo* method)
{
	{
		NullCheck((ExifEnumProperty_1_t8_369 *)__this);
		uint16_t L_0 = (( uint16_t (*) (ExifEnumProperty_1_t8_369 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ExifEnumProperty_1_t8_369 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		uint16_t L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSDifferential>::set__Value(System.Object)
extern "C" void ExifEnumProperty_1_set__Value_m8_2353_gshared (ExifEnumProperty_1_t8_369 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		NullCheck((ExifEnumProperty_1_t8_369 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_369 *, uint16_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((ExifEnumProperty_1_t8_369 *)__this, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSDifferential>::get_Value()
extern "C" uint16_t ExifEnumProperty_1_get_Value_m8_2354_gshared (ExifEnumProperty_1_t8_369 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = (uint16_t)(__this->___mValue_3);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSDifferential>::set_Value(T)
extern "C" void ExifEnumProperty_1_set_Value_m8_2355_gshared (ExifEnumProperty_1_t8_369 * __this, uint16_t ___value, const MethodInfo* method)
{
	{
		uint16_t L_0 = ___value;
		__this->___mValue_3 = L_0;
		return;
	}
}
// System.Boolean ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSDifferential>::get_IsBitField()
extern "C" bool ExifEnumProperty_1_get_IsBitField_m8_2356_gshared (ExifEnumProperty_1_t8_369 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(__this->___mIsBitField_4);
		return L_0;
	}
}
// System.String ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSDifferential>::ToString()
extern "C" String_t* ExifEnumProperty_1_ToString_m8_2357_gshared (ExifEnumProperty_1_t8_369 * __this, const MethodInfo* method)
{
	{
		uint16_t* L_0 = (uint16_t*)&(__this->___mValue_3);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		return L_1;
	}
}
// ExifLibrary.ExifInterOperability ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSDifferential>::get_Interoperability()
extern const Il2CppType* FileSource_t8_75_0_0_0_var;
extern const Il2CppType* SceneType_t8_76_0_0_0_var;
extern const Il2CppType* GPSLatitudeRef_t8_86_0_0_0_var;
extern const Il2CppType* GPSLongitudeRef_t8_87_0_0_0_var;
extern const Il2CppType* GPSStatus_t8_89_0_0_0_var;
extern const Il2CppType* GPSMeasureMode_t8_90_0_0_0_var;
extern const Il2CppType* GPSSpeedRef_t8_91_0_0_0_var;
extern const Il2CppType* GPSDirectionRef_t8_92_0_0_0_var;
extern const Il2CppType* GPSDistanceRef_t8_93_0_0_0_var;
extern const Il2CppType* Byte_t1_11_0_0_0_var;
extern const Il2CppType* UInt16_t1_14_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Enum_t1_24_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern TypeInfo* UInt16_t1_14_il2cpp_TypeInfo_var;
extern TypeInfo* UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var;
extern "C" ExifInterOperability_t8_113  ExifEnumProperty_1_get_Interoperability_m8_2358_gshared (ExifEnumProperty_1_t8_369 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FileSource_t8_75_0_0_0_var = il2cpp_codegen_type_from_index(2003);
		SceneType_t8_76_0_0_0_var = il2cpp_codegen_type_from_index(2004);
		GPSLatitudeRef_t8_86_0_0_0_var = il2cpp_codegen_type_from_index(2014);
		GPSLongitudeRef_t8_87_0_0_0_var = il2cpp_codegen_type_from_index(2015);
		GPSStatus_t8_89_0_0_0_var = il2cpp_codegen_type_from_index(2017);
		GPSMeasureMode_t8_90_0_0_0_var = il2cpp_codegen_type_from_index(2018);
		GPSSpeedRef_t8_91_0_0_0_var = il2cpp_codegen_type_from_index(2019);
		GPSDirectionRef_t8_92_0_0_0_var = il2cpp_codegen_type_from_index(2020);
		GPSDistanceRef_t8_93_0_0_0_var = il2cpp_codegen_type_from_index(2021);
		Byte_t1_11_0_0_0_var = il2cpp_codegen_type_from_index(25);
		UInt16_t1_14_0_0_0_var = il2cpp_codegen_type_from_index(29);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Enum_t1_24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		UInt16_t1_14_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4656);
		s_Il2CppMethodIntialized = true;
	}
	uint16_t V_0 = 0;
	Type_t * V_1 = {0};
	Type_t * V_2 = {0};
	{
		int32_t L_0 = (int32_t)(((ExifProperty_t8_99 *)__this)->___mTag_0);
		uint16_t L_1 = ExifTagFactory_GetTagID_m8_630(NULL /*static, unused*/, (int32_t)L_0, /*hidden argument*/NULL);
		V_0 = (uint16_t)L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		V_1 = (Type_t *)L_2;
		Type_t * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t1_24_il2cpp_TypeInfo_var);
		Type_t * L_4 = Enum_GetUnderlyingType_m1_942(NULL /*static, unused*/, (Type_t *)L_3, /*hidden argument*/NULL);
		V_2 = (Type_t *)L_4;
		Type_t * L_5 = V_1;
		Type_t * L_6 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(FileSource_t8_75_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_5) == ((Object_t*)(Type_t *)L_6)))
		{
			goto IL_003e;
		}
	}
	{
		Type_t * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(SceneType_t8_76_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_7) == ((Object_t*)(Type_t *)L_8))))
		{
			goto IL_0060;
		}
	}

IL_003e:
	{
		uint16_t L_9 = V_0;
		ByteU5BU5D_t1_109* L_10 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint16_t L_11 = (uint16_t)(__this->___mValue_3);
		uint16_t L_12 = L_11;
		Object_t * L_13 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_12);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_10, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_13, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_14 = {0};
		ExifInterOperability__ctor_m8_496(&L_14, (uint16_t)L_9, (uint16_t)7, (uint32_t)1, (ByteU5BU5D_t1_109*)L_10, /*hidden argument*/NULL);
		return L_14;
	}

IL_0060:
	{
		Type_t * L_15 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_16 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLatitudeRef_t8_86_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_15) == ((Object_t*)(Type_t *)L_16)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_17 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_18 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLongitudeRef_t8_87_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_17) == ((Object_t*)(Type_t *)L_18)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_19 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_20 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSStatus_t8_89_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_19) == ((Object_t*)(Type_t *)L_20)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_21 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_22 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSMeasureMode_t8_90_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_21) == ((Object_t*)(Type_t *)L_22)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_23 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_24 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSSpeedRef_t8_91_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_23) == ((Object_t*)(Type_t *)L_24)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_25 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_26 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDirectionRef_t8_92_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_25) == ((Object_t*)(Type_t *)L_26)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_27 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_28 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDistanceRef_t8_93_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_27) == ((Object_t*)(Type_t *)L_28))))
		{
			goto IL_00f2;
		}
	}

IL_00d0:
	{
		uint16_t L_29 = V_0;
		ByteU5BU5D_t1_109* L_30 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 2));
		uint16_t L_31 = (uint16_t)(__this->___mValue_3);
		uint16_t L_32 = L_31;
		Object_t * L_33 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_32);
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_30, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_33, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_34 = {0};
		ExifInterOperability__ctor_m8_496(&L_34, (uint16_t)L_29, (uint16_t)2, (uint32_t)2, (ByteU5BU5D_t1_109*)L_30, /*hidden argument*/NULL);
		return L_34;
	}

IL_00f2:
	{
		Type_t * L_35 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(Byte_t1_11_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_35) == ((Object_t*)(Type_t *)L_36))))
		{
			goto IL_0124;
		}
	}
	{
		uint16_t L_37 = V_0;
		ByteU5BU5D_t1_109* L_38 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint16_t L_39 = (uint16_t)(__this->___mValue_3);
		uint16_t L_40 = L_39;
		Object_t * L_41 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_40);
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_38, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_41, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_42 = {0};
		ExifInterOperability__ctor_m8_496(&L_42, (uint16_t)L_37, (uint16_t)1, (uint32_t)1, (ByteU5BU5D_t1_109*)L_38, /*hidden argument*/NULL);
		return L_42;
	}

IL_0124:
	{
		Type_t * L_43 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_44 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(UInt16_t1_14_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_43) == ((Object_t*)(Type_t *)L_44))))
		{
			goto IL_0154;
		}
	}
	{
		uint16_t L_45 = V_0;
		uint16_t L_46 = (uint16_t)(__this->___mValue_3);
		uint16_t L_47 = L_46;
		Object_t * L_48 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_47);
		ByteU5BU5D_t1_109* L_49 = BitConverterEx_GetBytes_m8_354(NULL /*static, unused*/, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_48, UInt16_t1_14_il2cpp_TypeInfo_var)))), (int32_t)0, (int32_t)0, /*hidden argument*/NULL);
		ExifInterOperability_t8_113  L_50 = {0};
		ExifInterOperability__ctor_m8_496(&L_50, (uint16_t)L_45, (uint16_t)3, (uint32_t)1, (ByteU5BU5D_t1_109*)L_49, /*hidden argument*/NULL);
		return L_50;
	}

IL_0154:
	{
		UnknownEnumTypeException_t8_96 * L_51 = (UnknownEnumTypeException_t8_96 *)il2cpp_codegen_object_new (UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var);
		UnknownEnumTypeException__ctor_m8_407(L_51, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_51);
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSDifferential>::op_Implicit(ExifLibrary.ExifEnumProperty`1<T>)
extern "C" uint16_t ExifEnumProperty_1_op_Implicit_m8_2359_gshared (Object_t * __this /* static, unused */, ExifEnumProperty_1_t8_369 * ___obj, const MethodInfo* method)
{
	{
		ExifEnumProperty_1_t8_369 * L_0 = ___obj;
		NullCheck(L_0);
		uint16_t L_1 = (uint16_t)(L_0->___mValue_3);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Single>::.ctor()
extern "C" void Dictionary_2__ctor_m1_26901_gshared (Dictionary_2_t1_2704 * __this, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1_0((Object_t *)__this, /*hidden argument*/NULL);
		NullCheck((Dictionary_2_t1_2704 *)__this);
		(( void (*) (Dictionary_2_t1_2704 *, int32_t, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Dictionary_2_t1_2704 *)__this, (int32_t)((int32_t)10), (Object_t*)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Single>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m1_26903_gshared (Dictionary_2_t1_2704 * __this, Object_t* ___comparer, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1_0((Object_t *)__this, /*hidden argument*/NULL);
		Object_t* L_0 = ___comparer;
		NullCheck((Dictionary_2_t1_2704 *)__this);
		(( void (*) (Dictionary_2_t1_2704 *, int32_t, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Dictionary_2_t1_2704 *)__this, (int32_t)((int32_t)10), (Object_t*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Single>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C" void Dictionary_2__ctor_m1_26905_gshared (Dictionary_2_t1_2704 * __this, Object_t* ___dictionary, const MethodInfo* method)
{
	{
		Object_t* L_0 = ___dictionary;
		NullCheck((Dictionary_2_t1_2704 *)__this);
		(( void (*) (Dictionary_2_t1_2704 *, Object_t*, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Dictionary_2_t1_2704 *)__this, (Object_t*)L_0, (Object_t*)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Single>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m1_26907_gshared (Dictionary_2_t1_2704 * __this, int32_t ___capacity, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1_0((Object_t *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___capacity;
		NullCheck((Dictionary_2_t1_2704 *)__this);
		(( void (*) (Dictionary_2_t1_2704 *, int32_t, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Dictionary_2_t1_2704 *)__this, (int32_t)L_0, (Object_t*)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Single>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t1_193_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral792;
extern "C" void Dictionary_2__ctor_m1_26909_gshared (Dictionary_2_t1_2704 * __this, Object_t* ___dictionary, Object_t* ___comparer, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		IEnumerator_t1_193_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(144);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		_stringLiteral792 = il2cpp_codegen_string_literal_from_index(792);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	KeyValuePair_2_t1_2706  V_1 = {0};
	Object_t* V_2 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1_0((Object_t *)__this, /*hidden argument*/NULL);
		Object_t* L_0 = ___dictionary;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, (String_t*)_stringLiteral792, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0017:
	{
		Object_t* L_2 = ___dictionary;
		NullCheck((Object_t*)L_2);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::get_Count() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Object_t*)L_2);
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		Object_t* L_5 = ___comparer;
		NullCheck((Dictionary_2_t1_2704 *)__this);
		(( void (*) (Dictionary_2_t1_2704 *, int32_t, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Dictionary_2_t1_2704 *)__this, (int32_t)L_4, (Object_t*)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Object_t* L_6 = ___dictionary;
		NullCheck((Object_t*)L_6);
		Object_t* L_7 = (Object_t*)InterfaceFuncInvoker0< Object_t* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Object_t*)L_6);
		V_2 = (Object_t*)L_7;
	}

IL_002d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_004d;
		}

IL_0032:
		{
			Object_t* L_8 = V_2;
			NullCheck((Object_t*)L_8);
			KeyValuePair_2_t1_2706  L_9 = (KeyValuePair_2_t1_2706 )InterfaceFuncInvoker0< KeyValuePair_2_t1_2706  >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Object_t*)L_8);
			V_1 = (KeyValuePair_2_t1_2706 )L_9;
			Object_t * L_10 = (( Object_t * (*) (KeyValuePair_2_t1_2706 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((KeyValuePair_2_t1_2706 *)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
			float L_11 = (( float (*) (KeyValuePair_2_t1_2706 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((KeyValuePair_2_t1_2706 *)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
			NullCheck((Dictionary_2_t1_2704 *)__this);
			VirtActionInvoker2< Object_t *, float >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Single>::Add(TKey,TValue) */, (Dictionary_2_t1_2704 *)__this, (Object_t *)L_10, (float)L_11);
		}

IL_004d:
		{
			Object_t* L_12 = V_2;
			NullCheck((Object_t *)L_12);
			bool L_13 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, (Object_t *)L_12);
			if (L_13)
			{
				goto IL_0032;
			}
		}

IL_0058:
		{
			IL2CPP_LEAVE(0x68, FINALLY_005d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_005d;
	}

FINALLY_005d:
	{ // begin finally (depth: 1)
		{
			Object_t* L_14 = V_2;
			if (L_14)
			{
				goto IL_0061;
			}
		}

IL_0060:
		{
			IL2CPP_END_FINALLY(93)
		}

IL_0061:
		{
			Object_t* L_15 = V_2;
			NullCheck((Object_t *)L_15);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, (Object_t *)L_15);
			IL2CPP_END_FINALLY(93)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(93)
	{
		IL2CPP_JUMP_TBL(0x68, IL_0068)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0068:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Single>::.ctor(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m1_26911_gshared (Dictionary_2_t1_2704 * __this, int32_t ___capacity, Object_t* ___comparer, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1_0((Object_t *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___capacity;
		Object_t* L_1 = ___comparer;
		NullCheck((Dictionary_2_t1_2704 *)__this);
		(( void (*) (Dictionary_2_t1_2704 *, int32_t, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Dictionary_2_t1_2704 *)__this, (int32_t)L_0, (Object_t*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Single>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m1_26913_gshared (Dictionary_2_t1_2704 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1_0((Object_t *)__this, /*hidden argument*/NULL);
		SerializationInfo_t1_293 * L_0 = ___info;
		__this->___serialization_info_13 = L_0;
		return;
	}
}
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Object,System.Single>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1_26915_gshared (Dictionary_2_t1_2704 * __this, const MethodInfo* method)
{
	{
		NullCheck((Dictionary_2_t1_2704 *)__this);
		KeyCollection_t1_2708 * L_0 = (( KeyCollection_t1_2708 * (*) (Dictionary_2_t1_2704 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((Dictionary_2_t1_2704 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return L_0;
	}
}
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Single>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1_26917_gshared (Dictionary_2_t1_2704 * __this, const MethodInfo* method)
{
	{
		NullCheck((Dictionary_2_t1_2704 *)__this);
		ValueCollection_t1_2712 * L_0 = (( ValueCollection_t1_2712 * (*) (Dictionary_2_t1_2704 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Dictionary_2_t1_2704 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		return L_0;
	}
}
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Object,System.Single>::System.Collections.IDictionary.get_Keys()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Keys_m1_26919_gshared (Dictionary_2_t1_2704 * __this, const MethodInfo* method)
{
	{
		NullCheck((Dictionary_2_t1_2704 *)__this);
		KeyCollection_t1_2708 * L_0 = (( KeyCollection_t1_2708 * (*) (Dictionary_2_t1_2704 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((Dictionary_2_t1_2704 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return L_0;
	}
}
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Object,System.Single>::System.Collections.IDictionary.get_Values()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Values_m1_26921_gshared (Dictionary_2_t1_2704 * __this, const MethodInfo* method)
{
	{
		NullCheck((Dictionary_2_t1_2704 *)__this);
		ValueCollection_t1_2712 * L_0 = (( ValueCollection_t1_2712 * (*) (Dictionary_2_t1_2704 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Dictionary_2_t1_2704 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		return L_0;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Single>::System.Collections.IDictionary.get_IsFixedSize()
extern "C" bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1_26923_gshared (Dictionary_2_t1_2704 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Single>::System.Collections.IDictionary.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1_26925_gshared (Dictionary_2_t1_2704 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Single>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m1_26927_gshared (Dictionary_2_t1_2704 * __this, Object_t * ___key, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___key;
		if (!((Object_t *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10))))
		{
			goto IL_002f;
		}
	}
	{
		Object_t * L_1 = ___key;
		NullCheck((Dictionary_2_t1_2704 *)__this);
		bool L_2 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Single>::ContainsKey(TKey) */, (Dictionary_2_t1_2704 *)__this, (Object_t *)((Object_t *)Castclass(L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10))));
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		Object_t * L_3 = ___key;
		NullCheck((Dictionary_2_t1_2704 *)__this);
		Object_t * L_4 = (( Object_t * (*) (Dictionary_2_t1_2704 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((Dictionary_2_t1_2704 *)__this, (Object_t *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		NullCheck((Dictionary_2_t1_2704 *)__this);
		float L_5 = (float)VirtFuncInvoker1< float, Object_t * >::Invoke(22 /* TValue System.Collections.Generic.Dictionary`2<System.Object,System.Single>::get_Item(TKey) */, (Dictionary_2_t1_2704 *)__this, (Object_t *)L_4);
		float L_6 = L_5;
		Object_t * L_7 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14), &L_6);
		return L_7;
	}

IL_002f:
	{
		return NULL;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Single>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m1_26929_gshared (Dictionary_2_t1_2704 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___key;
		NullCheck((Dictionary_2_t1_2704 *)__this);
		Object_t * L_1 = (( Object_t * (*) (Dictionary_2_t1_2704 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((Dictionary_2_t1_2704 *)__this, (Object_t *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		Object_t * L_2 = ___value;
		NullCheck((Dictionary_2_t1_2704 *)__this);
		float L_3 = (( float (*) (Dictionary_2_t1_2704 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)->method)((Dictionary_2_t1_2704 *)__this, (Object_t *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		NullCheck((Dictionary_2_t1_2704 *)__this);
		VirtActionInvoker2< Object_t *, float >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Single>::set_Item(TKey,TValue) */, (Dictionary_2_t1_2704 *)__this, (Object_t *)L_1, (float)L_3);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Single>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m1_26931_gshared (Dictionary_2_t1_2704 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___key;
		NullCheck((Dictionary_2_t1_2704 *)__this);
		Object_t * L_1 = (( Object_t * (*) (Dictionary_2_t1_2704 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((Dictionary_2_t1_2704 *)__this, (Object_t *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		Object_t * L_2 = ___value;
		NullCheck((Dictionary_2_t1_2704 *)__this);
		float L_3 = (( float (*) (Dictionary_2_t1_2704 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)->method)((Dictionary_2_t1_2704 *)__this, (Object_t *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		NullCheck((Dictionary_2_t1_2704 *)__this);
		VirtActionInvoker2< Object_t *, float >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Single>::Add(TKey,TValue) */, (Dictionary_2_t1_2704 *)__this, (Object_t *)L_1, (float)L_3);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Single>::System.Collections.IDictionary.Contains(System.Object)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral793;
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m1_26933_gshared (Dictionary_2_t1_2704 * __this, Object_t * ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		_stringLiteral793 = il2cpp_codegen_string_literal_from_index(793);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___key;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, (String_t*)_stringLiteral793, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		Object_t * L_2 = ___key;
		if (!((Object_t *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10))))
		{
			goto IL_0029;
		}
	}
	{
		Object_t * L_3 = ___key;
		NullCheck((Dictionary_2_t1_2704 *)__this);
		bool L_4 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Single>::ContainsKey(TKey) */, (Dictionary_2_t1_2704 *)__this, (Object_t *)((Object_t *)Castclass(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10))));
		return L_4;
	}

IL_0029:
	{
		return 0;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Single>::System.Collections.IDictionary.Remove(System.Object)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral793;
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m1_26935_gshared (Dictionary_2_t1_2704 * __this, Object_t * ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		_stringLiteral793 = il2cpp_codegen_string_literal_from_index(793);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___key;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, (String_t*)_stringLiteral793, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		Object_t * L_2 = ___key;
		if (!((Object_t *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10))))
		{
			goto IL_0029;
		}
	}
	{
		Object_t * L_3 = ___key;
		NullCheck((Dictionary_2_t1_2704 *)__this);
		VirtFuncInvoker1< bool, Object_t * >::Invoke(20 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Single>::Remove(TKey) */, (Dictionary_2_t1_2704 *)__this, (Object_t *)((Object_t *)Castclass(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10))));
	}

IL_0029:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Single>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1_26937_gshared (Dictionary_2_t1_2704 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Single>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1_26939_gshared (Dictionary_2_t1_2704 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Single>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1_26941_gshared (Dictionary_2_t1_2704 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Single>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1_26943_gshared (Dictionary_2_t1_2704 * __this, KeyValuePair_2_t1_2706  ___keyValuePair, const MethodInfo* method)
{
	{
		Object_t * L_0 = (( Object_t * (*) (KeyValuePair_2_t1_2706 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((KeyValuePair_2_t1_2706 *)(&___keyValuePair), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		float L_1 = (( float (*) (KeyValuePair_2_t1_2706 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((KeyValuePair_2_t1_2706 *)(&___keyValuePair), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		NullCheck((Dictionary_2_t1_2704 *)__this);
		VirtActionInvoker2< Object_t *, float >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Single>::Add(TKey,TValue) */, (Dictionary_2_t1_2704 *)__this, (Object_t *)L_0, (float)L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Single>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1_26945_gshared (Dictionary_2_t1_2704 * __this, KeyValuePair_2_t1_2706  ___keyValuePair, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1_2706  L_0 = ___keyValuePair;
		NullCheck((Dictionary_2_t1_2704 *)__this);
		bool L_1 = (( bool (*) (Dictionary_2_t1_2704 *, KeyValuePair_2_t1_2706 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->method)((Dictionary_2_t1_2704 *)__this, (KeyValuePair_2_t1_2706 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Single>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1_26947_gshared (Dictionary_2_t1_2704 * __this, KeyValuePair_2U5BU5D_t1_2889* ___array, int32_t ___index, const MethodInfo* method)
{
	{
		KeyValuePair_2U5BU5D_t1_2889* L_0 = ___array;
		int32_t L_1 = ___index;
		NullCheck((Dictionary_2_t1_2704 *)__this);
		(( void (*) (Dictionary_2_t1_2704 *, KeyValuePair_2U5BU5D_t1_2889*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19)->method)((Dictionary_2_t1_2704 *)__this, (KeyValuePair_2U5BU5D_t1_2889*)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19));
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Single>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1_26949_gshared (Dictionary_2_t1_2704 * __this, KeyValuePair_2_t1_2706  ___keyValuePair, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1_2706  L_0 = ___keyValuePair;
		NullCheck((Dictionary_2_t1_2704 *)__this);
		bool L_1 = (( bool (*) (Dictionary_2_t1_2704 *, KeyValuePair_2_t1_2706 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->method)((Dictionary_2_t1_2704 *)__this, (KeyValuePair_2_t1_2706 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18));
		if (L_1)
		{
			goto IL_000e;
		}
	}
	{
		return 0;
	}

IL_000e:
	{
		Object_t * L_2 = (( Object_t * (*) (KeyValuePair_2_t1_2706 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((KeyValuePair_2_t1_2706 *)(&___keyValuePair), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((Dictionary_2_t1_2704 *)__this);
		bool L_3 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(20 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Single>::Remove(TKey) */, (Dictionary_2_t1_2704 *)__this, (Object_t *)L_2);
		return L_3;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Single>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern TypeInfo* DictionaryEntryU5BU5D_t1_869_il2cpp_TypeInfo_var;
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m1_26951_gshared (Dictionary_2_t1_2704 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DictionaryEntryU5BU5D_t1_869_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(625);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2U5BU5D_t1_2889* V_0 = {0};
	DictionaryEntryU5BU5D_t1_869* V_1 = {0};
	int32_t G_B5_0 = 0;
	DictionaryEntryU5BU5D_t1_869* G_B5_1 = {0};
	Dictionary_2_t1_2704 * G_B5_2 = {0};
	int32_t G_B4_0 = 0;
	DictionaryEntryU5BU5D_t1_869* G_B4_1 = {0};
	Dictionary_2_t1_2704 * G_B4_2 = {0};
	{
		Array_t * L_0 = ___array;
		V_0 = (KeyValuePair_2U5BU5D_t1_2889*)((KeyValuePair_2U5BU5D_t1_2889*)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20)));
		KeyValuePair_2U5BU5D_t1_2889* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		KeyValuePair_2U5BU5D_t1_2889* L_2 = V_0;
		int32_t L_3 = ___index;
		NullCheck((Dictionary_2_t1_2704 *)__this);
		(( void (*) (Dictionary_2_t1_2704 *, KeyValuePair_2U5BU5D_t1_2889*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19)->method)((Dictionary_2_t1_2704 *)__this, (KeyValuePair_2U5BU5D_t1_2889*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19));
		return;
	}

IL_0016:
	{
		Array_t * L_4 = ___array;
		int32_t L_5 = ___index;
		NullCheck((Dictionary_2_t1_2704 *)__this);
		(( void (*) (Dictionary_2_t1_2704 *, Array_t *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 21)->method)((Dictionary_2_t1_2704 *)__this, (Array_t *)L_4, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 21));
		Array_t * L_6 = ___array;
		V_1 = (DictionaryEntryU5BU5D_t1_869*)((DictionaryEntryU5BU5D_t1_869*)IsInst(L_6, DictionaryEntryU5BU5D_t1_869_il2cpp_TypeInfo_var));
		DictionaryEntryU5BU5D_t1_869* L_7 = V_1;
		if (!L_7)
		{
			goto IL_0051;
		}
	}
	{
		DictionaryEntryU5BU5D_t1_869* L_8 = V_1;
		int32_t L_9 = ___index;
		Transform_1_t1_2705 * L_10 = ((Dictionary_2_t1_2704_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22)->static_fields)->___U3CU3Ef__amU24cacheB_15;
		G_B4_0 = L_9;
		G_B4_1 = L_8;
		G_B4_2 = ((Dictionary_2_t1_2704 *)(__this));
		if (L_10)
		{
			G_B5_0 = L_9;
			G_B5_1 = L_8;
			G_B5_2 = ((Dictionary_2_t1_2704 *)(__this));
			goto IL_0046;
		}
	}
	{
		IntPtr_t L_11 = { (void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23) };
		Transform_1_t1_2705 * L_12 = (Transform_1_t1_2705 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24));
		(( void (*) (Transform_1_t1_2705 *, Object_t *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25)->method)(L_12, (Object_t *)NULL, (IntPtr_t)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25));
		((Dictionary_2_t1_2704_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22)->static_fields)->___U3CU3Ef__amU24cacheB_15 = L_12;
		G_B5_0 = G_B4_0;
		G_B5_1 = G_B4_1;
		G_B5_2 = ((Dictionary_2_t1_2704 *)(G_B4_2));
	}

IL_0046:
	{
		Transform_1_t1_2705 * L_13 = ((Dictionary_2_t1_2704_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22)->static_fields)->___U3CU3Ef__amU24cacheB_15;
		NullCheck((Dictionary_2_t1_2704 *)G_B5_2);
		(( void (*) (Dictionary_2_t1_2704 *, DictionaryEntryU5BU5D_t1_869*, int32_t, Transform_1_t1_2705 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26)->method)((Dictionary_2_t1_2704 *)G_B5_2, (DictionaryEntryU5BU5D_t1_869*)G_B5_1, (int32_t)G_B5_0, (Transform_1_t1_2705 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26));
		return;
	}

IL_0051:
	{
		Array_t * L_14 = ___array;
		int32_t L_15 = ___index;
		IntPtr_t L_16 = { (void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27) };
		Transform_1_t1_2715 * L_17 = (Transform_1_t1_2715 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28));
		(( void (*) (Transform_1_t1_2715 *, Object_t *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29)->method)(L_17, (Object_t *)NULL, (IntPtr_t)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29));
		NullCheck((Dictionary_2_t1_2704 *)__this);
		(( void (*) (Dictionary_2_t1_2704 *, Array_t *, int32_t, Transform_1_t1_2715 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30)->method)((Dictionary_2_t1_2704 *)__this, (Array_t *)L_14, (int32_t)L_15, (Transform_1_t1_2715 *)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Single>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1_26953_gshared (Dictionary_2_t1_2704 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1_2710  L_0 = {0};
		(( void (*) (Enumerator_t1_2710 *, Dictionary_2_t1_2704 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 32)->method)(&L_0, (Dictionary_2_t1_2704 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 32));
		Enumerator_t1_2710  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 31), &L_1);
		return (Object_t *)L_2;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,System.Single>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1_26955_gshared (Dictionary_2_t1_2704 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1_2710  L_0 = {0};
		(( void (*) (Enumerator_t1_2710 *, Dictionary_2_t1_2704 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 32)->method)(&L_0, (Dictionary_2_t1_2704 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 32));
		Enumerator_t1_2710  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 31), &L_1);
		return (Object_t*)L_2;
	}
}
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Single>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1_26957_gshared (Dictionary_2_t1_2704 * __this, const MethodInfo* method)
{
	{
		ShimEnumerator_t1_2716 * L_0 = (ShimEnumerator_t1_2716 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 33));
		(( void (*) (ShimEnumerator_t1_2716 *, Dictionary_2_t1_2704 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 34)->method)(L_0, (Dictionary_2_t1_2704 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 34));
		return L_0;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.Single>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m1_26959_gshared (Dictionary_2_t1_2704 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)(__this->___count_10);
		return L_0;
	}
}
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Single>::get_Item(TKey)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* KeyNotFoundException_t1_257_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral793;
extern "C" float Dictionary_2_get_Item_m1_26961_gshared (Dictionary_2_t1_2704 * __this, Object_t * ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		KeyNotFoundException_t1_257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1969);
		_stringLiteral793 = il2cpp_codegen_string_literal_from_index(793);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Object_t * L_0 = ___key;
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, (String_t*)_stringLiteral793, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		Object_t* L_2 = (Object_t*)(__this->___hcp_12);
		Object_t * L_3 = ___key;
		NullCheck((Object_t*)L_2);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker1< int32_t, Object_t * >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<System.Object>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 35), (Object_t*)L_2, (Object_t *)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648)));
		Int32U5BU5D_t1_275* L_5 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		int32_t L_6 = V_0;
		Int32U5BU5D_t1_275* L_7 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		NullCheck(L_7);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)((int32_t)(((Array_t *)L_7)->max_length)))))));
		int32_t L_8 = ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)((int32_t)(((Array_t *)L_7)->max_length))))));
		V_1 = (int32_t)((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_5, L_8, sizeof(int32_t)))-(int32_t)1));
		goto IL_009b;
	}

IL_0048:
	{
		LinkU5BU5D_t1_1975* L_9 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		int32_t L_10 = V_1;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = (int32_t)(((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_9, L_10, sizeof(Link_t1_256 )))->___HashCode_0);
		int32_t L_12 = V_0;
		if ((!(((uint32_t)L_11) == ((uint32_t)L_12))))
		{
			goto IL_0089;
		}
	}
	{
		Object_t* L_13 = (Object_t*)(__this->___hcp_12);
		ObjectU5BU5D_t1_272* L_14 = (ObjectU5BU5D_t1_272*)(__this->___keySlots_6);
		int32_t L_15 = V_1;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		Object_t * L_17 = ___key;
		NullCheck((Object_t*)L_13);
		bool L_18 = (bool)InterfaceFuncInvoker2< bool, Object_t *, Object_t * >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Object>::Equals(T,T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 35), (Object_t*)L_13, (Object_t *)(*(Object_t **)(Object_t **)SZArrayLdElema(L_14, L_16, sizeof(Object_t *))), (Object_t *)L_17);
		if (!L_18)
		{
			goto IL_0089;
		}
	}
	{
		SingleU5BU5D_t1_1695* L_19 = (SingleU5BU5D_t1_1695*)(__this->___valueSlots_7);
		int32_t L_20 = V_1;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, L_20);
		int32_t L_21 = L_20;
		return (*(float*)(float*)SZArrayLdElema(L_19, L_21, sizeof(float)));
	}

IL_0089:
	{
		LinkU5BU5D_t1_1975* L_22 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		int32_t L_23 = V_1;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
		int32_t L_24 = (int32_t)(((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_22, L_23, sizeof(Link_t1_256 )))->___Next_1);
		V_1 = (int32_t)L_24;
	}

IL_009b:
	{
		int32_t L_25 = V_1;
		if ((!(((uint32_t)L_25) == ((uint32_t)(-1)))))
		{
			goto IL_0048;
		}
	}
	{
		KeyNotFoundException_t1_257 * L_26 = (KeyNotFoundException_t1_257 *)il2cpp_codegen_object_new (KeyNotFoundException_t1_257_il2cpp_TypeInfo_var);
		KeyNotFoundException__ctor_m1_2781(L_26, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_26);
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Single>::set_Item(TKey,TValue)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral793;
extern "C" void Dictionary_2_set_Item_m1_26963_gshared (Dictionary_2_t1_2704 * __this, Object_t * ___key, float ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		_stringLiteral793 = il2cpp_codegen_string_literal_from_index(793);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		Object_t * L_0 = ___key;
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, (String_t*)_stringLiteral793, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		Object_t* L_2 = (Object_t*)(__this->___hcp_12);
		Object_t * L_3 = ___key;
		NullCheck((Object_t*)L_2);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker1< int32_t, Object_t * >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<System.Object>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 35), (Object_t*)L_2, (Object_t *)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648)));
		int32_t L_5 = V_0;
		Int32U5BU5D_t1_275* L_6 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		NullCheck(L_6);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)((int32_t)(((Array_t *)L_6)->max_length))))));
		Int32U5BU5D_t1_275* L_7 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		int32_t L_8 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		V_2 = (int32_t)((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_7, L_9, sizeof(int32_t)))-(int32_t)1));
		V_3 = (int32_t)(-1);
		int32_t L_10 = V_2;
		if ((((int32_t)L_10) == ((int32_t)(-1))))
		{
			goto IL_00a2;
		}
	}

IL_004e:
	{
		LinkU5BU5D_t1_1975* L_11 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		int32_t L_12 = V_2;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		int32_t L_13 = (int32_t)(((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_11, L_12, sizeof(Link_t1_256 )))->___HashCode_0);
		int32_t L_14 = V_0;
		if ((!(((uint32_t)L_13) == ((uint32_t)L_14))))
		{
			goto IL_0087;
		}
	}
	{
		Object_t* L_15 = (Object_t*)(__this->___hcp_12);
		ObjectU5BU5D_t1_272* L_16 = (ObjectU5BU5D_t1_272*)(__this->___keySlots_6);
		int32_t L_17 = V_2;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		int32_t L_18 = L_17;
		Object_t * L_19 = ___key;
		NullCheck((Object_t*)L_15);
		bool L_20 = (bool)InterfaceFuncInvoker2< bool, Object_t *, Object_t * >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Object>::Equals(T,T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 35), (Object_t*)L_15, (Object_t *)(*(Object_t **)(Object_t **)SZArrayLdElema(L_16, L_18, sizeof(Object_t *))), (Object_t *)L_19);
		if (!L_20)
		{
			goto IL_0087;
		}
	}
	{
		goto IL_00a2;
	}

IL_0087:
	{
		int32_t L_21 = V_2;
		V_3 = (int32_t)L_21;
		LinkU5BU5D_t1_1975* L_22 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		int32_t L_23 = V_2;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
		int32_t L_24 = (int32_t)(((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_22, L_23, sizeof(Link_t1_256 )))->___Next_1);
		V_2 = (int32_t)L_24;
		int32_t L_25 = V_2;
		if ((!(((uint32_t)L_25) == ((uint32_t)(-1)))))
		{
			goto IL_004e;
		}
	}

IL_00a2:
	{
		int32_t L_26 = V_2;
		if ((!(((uint32_t)L_26) == ((uint32_t)(-1)))))
		{
			goto IL_0166;
		}
	}
	{
		int32_t L_27 = (int32_t)(__this->___count_10);
		int32_t L_28 = (int32_t)((int32_t)((int32_t)L_27+(int32_t)1));
		V_4 = (int32_t)L_28;
		__this->___count_10 = L_28;
		int32_t L_29 = V_4;
		int32_t L_30 = (int32_t)(__this->___threshold_11);
		if ((((int32_t)L_29) <= ((int32_t)L_30)))
		{
			goto IL_00de;
		}
	}
	{
		NullCheck((Dictionary_2_t1_2704 *)__this);
		(( void (*) (Dictionary_2_t1_2704 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 36)->method)((Dictionary_2_t1_2704 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 36));
		int32_t L_31 = V_0;
		Int32U5BU5D_t1_275* L_32 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		NullCheck(L_32);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_31&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)((int32_t)(((Array_t *)L_32)->max_length))))));
	}

IL_00de:
	{
		int32_t L_33 = (int32_t)(__this->___emptySlot_9);
		V_2 = (int32_t)L_33;
		int32_t L_34 = V_2;
		if ((!(((uint32_t)L_34) == ((uint32_t)(-1)))))
		{
			goto IL_0105;
		}
	}
	{
		int32_t L_35 = (int32_t)(__this->___touchedSlots_8);
		int32_t L_36 = (int32_t)L_35;
		V_4 = (int32_t)L_36;
		__this->___touchedSlots_8 = ((int32_t)((int32_t)L_36+(int32_t)1));
		int32_t L_37 = V_4;
		V_2 = (int32_t)L_37;
		goto IL_011c;
	}

IL_0105:
	{
		LinkU5BU5D_t1_1975* L_38 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		int32_t L_39 = V_2;
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, L_39);
		int32_t L_40 = (int32_t)(((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_38, L_39, sizeof(Link_t1_256 )))->___Next_1);
		__this->___emptySlot_9 = L_40;
	}

IL_011c:
	{
		LinkU5BU5D_t1_1975* L_41 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		int32_t L_42 = V_2;
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, L_42);
		Int32U5BU5D_t1_275* L_43 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		int32_t L_44 = V_1;
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, L_44);
		int32_t L_45 = L_44;
		((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_41, L_42, sizeof(Link_t1_256 )))->___Next_1 = ((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_43, L_45, sizeof(int32_t)))-(int32_t)1));
		Int32U5BU5D_t1_275* L_46 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		int32_t L_47 = V_1;
		int32_t L_48 = V_2;
		NullCheck(L_46);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_46, L_47);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_46, L_47, sizeof(int32_t))) = (int32_t)((int32_t)((int32_t)L_48+(int32_t)1));
		LinkU5BU5D_t1_1975* L_49 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		int32_t L_50 = V_2;
		NullCheck(L_49);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_49, L_50);
		int32_t L_51 = V_0;
		((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_49, L_50, sizeof(Link_t1_256 )))->___HashCode_0 = L_51;
		ObjectU5BU5D_t1_272* L_52 = (ObjectU5BU5D_t1_272*)(__this->___keySlots_6);
		int32_t L_53 = V_2;
		Object_t * L_54 = ___key;
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, L_53);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_52, L_53, sizeof(Object_t *))) = (Object_t *)L_54;
		goto IL_01b5;
	}

IL_0166:
	{
		int32_t L_55 = V_3;
		if ((((int32_t)L_55) == ((int32_t)(-1))))
		{
			goto IL_01b5;
		}
	}
	{
		LinkU5BU5D_t1_1975* L_56 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		int32_t L_57 = V_3;
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, L_57);
		LinkU5BU5D_t1_1975* L_58 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		int32_t L_59 = V_2;
		NullCheck(L_58);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_58, L_59);
		int32_t L_60 = (int32_t)(((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_58, L_59, sizeof(Link_t1_256 )))->___Next_1);
		((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_56, L_57, sizeof(Link_t1_256 )))->___Next_1 = L_60;
		LinkU5BU5D_t1_1975* L_61 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		int32_t L_62 = V_2;
		NullCheck(L_61);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_61, L_62);
		Int32U5BU5D_t1_275* L_63 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		int32_t L_64 = V_1;
		NullCheck(L_63);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_63, L_64);
		int32_t L_65 = L_64;
		((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_61, L_62, sizeof(Link_t1_256 )))->___Next_1 = ((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_63, L_65, sizeof(int32_t)))-(int32_t)1));
		Int32U5BU5D_t1_275* L_66 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		int32_t L_67 = V_1;
		int32_t L_68 = V_2;
		NullCheck(L_66);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_66, L_67);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_66, L_67, sizeof(int32_t))) = (int32_t)((int32_t)((int32_t)L_68+(int32_t)1));
	}

IL_01b5:
	{
		SingleU5BU5D_t1_1695* L_69 = (SingleU5BU5D_t1_1695*)(__this->___valueSlots_7);
		int32_t L_70 = V_2;
		float L_71 = ___value;
		NullCheck(L_69);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_69, L_70);
		*((float*)(float*)SZArrayLdElema(L_69, L_70, sizeof(float))) = (float)L_71;
		int32_t L_72 = (int32_t)(__this->___generation_14);
		__this->___generation_14 = ((int32_t)((int32_t)L_72+(int32_t)1));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Single>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern TypeInfo* ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral794;
extern "C" void Dictionary_2_Init_m1_26965_gshared (Dictionary_2_t1_2704 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		_stringLiteral794 = il2cpp_codegen_string_literal_from_index(794);
		s_Il2CppMethodIntialized = true;
	}
	Object_t* V_0 = {0};
	Dictionary_2_t1_2704 * G_B4_0 = {0};
	Dictionary_2_t1_2704 * G_B3_0 = {0};
	Object_t* G_B5_0 = {0};
	Dictionary_2_t1_2704 * G_B5_1 = {0};
	{
		int32_t L_0 = ___capacity;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0012;
		}
	}
	{
		ArgumentOutOfRangeException_t1_1501 * L_1 = (ArgumentOutOfRangeException_t1_1501 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1_13274(L_1, (String_t*)_stringLiteral794, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0012:
	{
		Object_t* L_2 = ___hcp;
		G_B3_0 = ((Dictionary_2_t1_2704 *)(__this));
		if (!L_2)
		{
			G_B4_0 = ((Dictionary_2_t1_2704 *)(__this));
			goto IL_0021;
		}
	}
	{
		Object_t* L_3 = ___hcp;
		V_0 = (Object_t*)L_3;
		Object_t* L_4 = V_0;
		G_B5_0 = L_4;
		G_B5_1 = ((Dictionary_2_t1_2704 *)(G_B3_0));
		goto IL_0026;
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 38));
		EqualityComparer_1_t1_1942 * L_5 = (( EqualityComparer_1_t1_1942 * (*) (Object_t * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 37)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 37));
		G_B5_0 = ((Object_t*)(L_5));
		G_B5_1 = ((Dictionary_2_t1_2704 *)(G_B4_0));
	}

IL_0026:
	{
		NullCheck(G_B5_1);
		G_B5_1->___hcp_12 = G_B5_0;
		int32_t L_6 = ___capacity;
		if (L_6)
		{
			goto IL_0035;
		}
	}
	{
		___capacity = (int32_t)((int32_t)10);
	}

IL_0035:
	{
		int32_t L_7 = ___capacity;
		___capacity = (int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)((float)((float)(((float)((float)L_7)))/(float)(0.9f))))))+(int32_t)1));
		int32_t L_8 = ___capacity;
		NullCheck((Dictionary_2_t1_2704 *)__this);
		(( void (*) (Dictionary_2_t1_2704 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39)->method)((Dictionary_2_t1_2704 *)__this, (int32_t)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39));
		__this->___generation_14 = 0;
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Single>::InitArrays(System.Int32)
extern TypeInfo* Int32U5BU5D_t1_275_il2cpp_TypeInfo_var;
extern TypeInfo* LinkU5BU5D_t1_1975_il2cpp_TypeInfo_var;
extern "C" void Dictionary_2_InitArrays_m1_26967_gshared (Dictionary_2_t1_2704 * __this, int32_t ___size, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32U5BU5D_t1_275_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(51);
		LinkU5BU5D_t1_1975_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4655);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___size;
		__this->___table_4 = ((Int32U5BU5D_t1_275*)SZArrayNew(Int32U5BU5D_t1_275_il2cpp_TypeInfo_var, L_0));
		int32_t L_1 = ___size;
		__this->___linkSlots_5 = ((LinkU5BU5D_t1_1975*)SZArrayNew(LinkU5BU5D_t1_1975_il2cpp_TypeInfo_var, L_1));
		__this->___emptySlot_9 = (-1);
		int32_t L_2 = ___size;
		__this->___keySlots_6 = ((ObjectU5BU5D_t1_272*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 40), L_2));
		int32_t L_3 = ___size;
		__this->___valueSlots_7 = ((SingleU5BU5D_t1_1695*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 41), L_3));
		__this->___touchedSlots_8 = 0;
		Int32U5BU5D_t1_275* L_4 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		NullCheck(L_4);
		__this->___threshold_11 = (((int32_t)((int32_t)((float)((float)(((float)((float)(((int32_t)((int32_t)(((Array_t *)L_4)->max_length)))))))*(float)(0.9f))))));
		int32_t L_5 = (int32_t)(__this->___threshold_11);
		if (L_5)
		{
			goto IL_0074;
		}
	}
	{
		Int32U5BU5D_t1_275* L_6 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		NullCheck(L_6);
		if ((((int32_t)(((int32_t)((int32_t)(((Array_t *)L_6)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_0074;
		}
	}
	{
		__this->___threshold_11 = 1;
	}

IL_0074:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Single>::CopyToCheck(System.Array,System.Int32)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral173;
extern Il2CppCodeGenString* _stringLiteral170;
extern Il2CppCodeGenString* _stringLiteral795;
extern Il2CppCodeGenString* _stringLiteral796;
extern "C" void Dictionary_2_CopyToCheck_m1_26969_gshared (Dictionary_2_t1_2704 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		_stringLiteral173 = il2cpp_codegen_string_literal_from_index(173);
		_stringLiteral170 = il2cpp_codegen_string_literal_from_index(170);
		_stringLiteral795 = il2cpp_codegen_string_literal_from_index(795);
		_stringLiteral796 = il2cpp_codegen_string_literal_from_index(796);
		s_Il2CppMethodIntialized = true;
	}
	{
		Array_t * L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, (String_t*)_stringLiteral173, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		int32_t L_2 = ___index;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0023;
		}
	}
	{
		ArgumentOutOfRangeException_t1_1501 * L_3 = (ArgumentOutOfRangeException_t1_1501 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1_13274(L_3, (String_t*)_stringLiteral170, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0023:
	{
		int32_t L_4 = ___index;
		Array_t * L_5 = ___array;
		NullCheck((Array_t *)L_5);
		int32_t L_6 = Array_get_Length_m1_992((Array_t *)L_5, /*hidden argument*/NULL);
		if ((((int32_t)L_4) <= ((int32_t)L_6)))
		{
			goto IL_003a;
		}
	}
	{
		ArgumentException_t1_1425 * L_7 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13260(L_7, (String_t*)_stringLiteral795, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_7);
	}

IL_003a:
	{
		Array_t * L_8 = ___array;
		NullCheck((Array_t *)L_8);
		int32_t L_9 = Array_get_Length_m1_992((Array_t *)L_8, /*hidden argument*/NULL);
		int32_t L_10 = ___index;
		NullCheck((Dictionary_2_t1_2704 *)__this);
		int32_t L_11 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.Single>::get_Count() */, (Dictionary_2_t1_2704 *)__this);
		if ((((int32_t)((int32_t)((int32_t)L_9-(int32_t)L_10))) >= ((int32_t)L_11)))
		{
			goto IL_0058;
		}
	}
	{
		ArgumentException_t1_1425 * L_12 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13260(L_12, (String_t*)_stringLiteral796, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_12);
	}

IL_0058:
	{
		return;
	}
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Single>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t1_2706  Dictionary_2_make_pair_m1_26971_gshared (Object_t * __this /* static, unused */, Object_t * ___key, float ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___key;
		float L_1 = ___value;
		KeyValuePair_2_t1_2706  L_2 = {0};
		(( void (*) (KeyValuePair_2_t1_2706 *, Object_t *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 44)->method)(&L_2, (Object_t *)L_0, (float)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 44));
		return L_2;
	}
}
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Single>::pick_key(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_key_m1_26973_gshared (Object_t * __this /* static, unused */, Object_t * ___key, float ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___key;
		return L_0;
	}
}
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Single>::pick_value(TKey,TValue)
extern "C" float Dictionary_2_pick_value_m1_26975_gshared (Object_t * __this /* static, unused */, Object_t * ___key, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		return L_0;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Single>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m1_26977_gshared (Dictionary_2_t1_2704 * __this, KeyValuePair_2U5BU5D_t1_2889* ___array, int32_t ___index, const MethodInfo* method)
{
	{
		KeyValuePair_2U5BU5D_t1_2889* L_0 = ___array;
		int32_t L_1 = ___index;
		NullCheck((Dictionary_2_t1_2704 *)__this);
		(( void (*) (Dictionary_2_t1_2704 *, Array_t *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 21)->method)((Dictionary_2_t1_2704 *)__this, (Array_t *)(Array_t *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 21));
		KeyValuePair_2U5BU5D_t1_2889* L_2 = ___array;
		int32_t L_3 = ___index;
		IntPtr_t L_4 = { (void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27) };
		Transform_1_t1_2715 * L_5 = (Transform_1_t1_2715 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28));
		(( void (*) (Transform_1_t1_2715 *, Object_t *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29)->method)(L_5, (Object_t *)NULL, (IntPtr_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29));
		NullCheck((Dictionary_2_t1_2704 *)__this);
		(( void (*) (Dictionary_2_t1_2704 *, KeyValuePair_2U5BU5D_t1_2889*, int32_t, Transform_1_t1_2715 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 45)->method)((Dictionary_2_t1_2704 *)__this, (KeyValuePair_2U5BU5D_t1_2889*)L_2, (int32_t)L_3, (Transform_1_t1_2715 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 45));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Single>::Resize()
extern TypeInfo* Hashtable_t1_100_il2cpp_TypeInfo_var;
extern TypeInfo* Int32U5BU5D_t1_275_il2cpp_TypeInfo_var;
extern TypeInfo* LinkU5BU5D_t1_1975_il2cpp_TypeInfo_var;
extern "C" void Dictionary_2_Resize_m1_26979_gshared (Dictionary_2_t1_2704 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Hashtable_t1_100_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(119);
		Int32U5BU5D_t1_275_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(51);
		LinkU5BU5D_t1_1975_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4655);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Int32U5BU5D_t1_275* V_1 = {0};
	LinkU5BU5D_t1_1975* V_2 = {0};
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	ObjectU5BU5D_t1_272* V_7 = {0};
	SingleU5BU5D_t1_1695* V_8 = {0};
	int32_t V_9 = 0;
	{
		Int32U5BU5D_t1_275* L_0 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		NullCheck(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Hashtable_t1_100_il2cpp_TypeInfo_var);
		int32_t L_1 = Hashtable_ToPrime_m1_3334(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Array_t *)L_0)->max_length))))<<(int32_t)1))|(int32_t)1)), /*hidden argument*/NULL);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		V_1 = (Int32U5BU5D_t1_275*)((Int32U5BU5D_t1_275*)SZArrayNew(Int32U5BU5D_t1_275_il2cpp_TypeInfo_var, L_2));
		int32_t L_3 = V_0;
		V_2 = (LinkU5BU5D_t1_1975*)((LinkU5BU5D_t1_1975*)SZArrayNew(LinkU5BU5D_t1_1975_il2cpp_TypeInfo_var, L_3));
		V_3 = (int32_t)0;
		goto IL_00b1;
	}

IL_0027:
	{
		Int32U5BU5D_t1_275* L_4 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		int32_t L_5 = V_3;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		V_4 = (int32_t)((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_4, L_6, sizeof(int32_t)))-(int32_t)1));
		goto IL_00a5;
	}

IL_0038:
	{
		LinkU5BU5D_t1_1975* L_7 = V_2;
		int32_t L_8 = V_4;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		Object_t* L_9 = (Object_t*)(__this->___hcp_12);
		ObjectU5BU5D_t1_272* L_10 = (ObjectU5BU5D_t1_272*)(__this->___keySlots_6);
		int32_t L_11 = V_4;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = L_11;
		NullCheck((Object_t*)L_9);
		int32_t L_13 = (int32_t)InterfaceFuncInvoker1< int32_t, Object_t * >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<System.Object>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 35), (Object_t*)L_9, (Object_t *)(*(Object_t **)(Object_t **)SZArrayLdElema(L_10, L_12, sizeof(Object_t *))));
		int32_t L_14 = (int32_t)((int32_t)((int32_t)L_13|(int32_t)((int32_t)-2147483648)));
		V_9 = (int32_t)L_14;
		((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_7, L_8, sizeof(Link_t1_256 )))->___HashCode_0 = L_14;
		int32_t L_15 = V_9;
		V_5 = (int32_t)L_15;
		int32_t L_16 = V_5;
		int32_t L_17 = V_0;
		V_6 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_16&(int32_t)((int32_t)2147483647)))%(int32_t)L_17));
		LinkU5BU5D_t1_1975* L_18 = V_2;
		int32_t L_19 = V_4;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_19);
		Int32U5BU5D_t1_275* L_20 = V_1;
		int32_t L_21 = V_6;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		int32_t L_22 = L_21;
		((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_18, L_19, sizeof(Link_t1_256 )))->___Next_1 = ((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_20, L_22, sizeof(int32_t)))-(int32_t)1));
		Int32U5BU5D_t1_275* L_23 = V_1;
		int32_t L_24 = V_6;
		int32_t L_25 = V_4;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_24);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_23, L_24, sizeof(int32_t))) = (int32_t)((int32_t)((int32_t)L_25+(int32_t)1));
		LinkU5BU5D_t1_1975* L_26 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		int32_t L_27 = V_4;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, L_27);
		int32_t L_28 = (int32_t)(((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_26, L_27, sizeof(Link_t1_256 )))->___Next_1);
		V_4 = (int32_t)L_28;
	}

IL_00a5:
	{
		int32_t L_29 = V_4;
		if ((!(((uint32_t)L_29) == ((uint32_t)(-1)))))
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_30 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_30+(int32_t)1));
	}

IL_00b1:
	{
		int32_t L_31 = V_3;
		Int32U5BU5D_t1_275* L_32 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		NullCheck(L_32);
		if ((((int32_t)L_31) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_32)->max_length)))))))
		{
			goto IL_0027;
		}
	}
	{
		Int32U5BU5D_t1_275* L_33 = V_1;
		__this->___table_4 = L_33;
		LinkU5BU5D_t1_1975* L_34 = V_2;
		__this->___linkSlots_5 = L_34;
		int32_t L_35 = V_0;
		V_7 = (ObjectU5BU5D_t1_272*)((ObjectU5BU5D_t1_272*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 40), L_35));
		int32_t L_36 = V_0;
		V_8 = (SingleU5BU5D_t1_1695*)((SingleU5BU5D_t1_1695*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 41), L_36));
		ObjectU5BU5D_t1_272* L_37 = (ObjectU5BU5D_t1_272*)(__this->___keySlots_6);
		ObjectU5BU5D_t1_272* L_38 = V_7;
		int32_t L_39 = (int32_t)(__this->___touchedSlots_8);
		Array_Copy_m1_1041(NULL /*static, unused*/, (Array_t *)(Array_t *)L_37, (int32_t)0, (Array_t *)(Array_t *)L_38, (int32_t)0, (int32_t)L_39, /*hidden argument*/NULL);
		SingleU5BU5D_t1_1695* L_40 = (SingleU5BU5D_t1_1695*)(__this->___valueSlots_7);
		SingleU5BU5D_t1_1695* L_41 = V_8;
		int32_t L_42 = (int32_t)(__this->___touchedSlots_8);
		Array_Copy_m1_1041(NULL /*static, unused*/, (Array_t *)(Array_t *)L_40, (int32_t)0, (Array_t *)(Array_t *)L_41, (int32_t)0, (int32_t)L_42, /*hidden argument*/NULL);
		ObjectU5BU5D_t1_272* L_43 = V_7;
		__this->___keySlots_6 = L_43;
		SingleU5BU5D_t1_1695* L_44 = V_8;
		__this->___valueSlots_7 = L_44;
		int32_t L_45 = V_0;
		__this->___threshold_11 = (((int32_t)((int32_t)((float)((float)(((float)((float)L_45)))*(float)(0.9f))))));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Single>::Add(TKey,TValue)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral793;
extern Il2CppCodeGenString* _stringLiteral798;
extern "C" void Dictionary_2_Add_m1_26981_gshared (Dictionary_2_t1_2704 * __this, Object_t * ___key, float ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		_stringLiteral793 = il2cpp_codegen_string_literal_from_index(793);
		_stringLiteral798 = il2cpp_codegen_string_literal_from_index(798);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		Object_t * L_0 = ___key;
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, (String_t*)_stringLiteral793, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		Object_t* L_2 = (Object_t*)(__this->___hcp_12);
		Object_t * L_3 = ___key;
		NullCheck((Object_t*)L_2);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker1< int32_t, Object_t * >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<System.Object>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 35), (Object_t*)L_2, (Object_t *)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648)));
		int32_t L_5 = V_0;
		Int32U5BU5D_t1_275* L_6 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		NullCheck(L_6);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)((int32_t)(((Array_t *)L_6)->max_length))))));
		Int32U5BU5D_t1_275* L_7 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		int32_t L_8 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		V_2 = (int32_t)((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_7, L_9, sizeof(int32_t)))-(int32_t)1));
		goto IL_009b;
	}

IL_004a:
	{
		LinkU5BU5D_t1_1975* L_10 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		int32_t L_11 = V_2;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = (int32_t)(((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_10, L_11, sizeof(Link_t1_256 )))->___HashCode_0);
		int32_t L_13 = V_0;
		if ((!(((uint32_t)L_12) == ((uint32_t)L_13))))
		{
			goto IL_0089;
		}
	}
	{
		Object_t* L_14 = (Object_t*)(__this->___hcp_12);
		ObjectU5BU5D_t1_272* L_15 = (ObjectU5BU5D_t1_272*)(__this->___keySlots_6);
		int32_t L_16 = V_2;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		int32_t L_17 = L_16;
		Object_t * L_18 = ___key;
		NullCheck((Object_t*)L_14);
		bool L_19 = (bool)InterfaceFuncInvoker2< bool, Object_t *, Object_t * >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Object>::Equals(T,T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 35), (Object_t*)L_14, (Object_t *)(*(Object_t **)(Object_t **)SZArrayLdElema(L_15, L_17, sizeof(Object_t *))), (Object_t *)L_18);
		if (!L_19)
		{
			goto IL_0089;
		}
	}
	{
		ArgumentException_t1_1425 * L_20 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13260(L_20, (String_t*)_stringLiteral798, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_20);
	}

IL_0089:
	{
		LinkU5BU5D_t1_1975* L_21 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		int32_t L_22 = V_2;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, L_22);
		int32_t L_23 = (int32_t)(((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_21, L_22, sizeof(Link_t1_256 )))->___Next_1);
		V_2 = (int32_t)L_23;
	}

IL_009b:
	{
		int32_t L_24 = V_2;
		if ((!(((uint32_t)L_24) == ((uint32_t)(-1)))))
		{
			goto IL_004a;
		}
	}
	{
		int32_t L_25 = (int32_t)(__this->___count_10);
		int32_t L_26 = (int32_t)((int32_t)((int32_t)L_25+(int32_t)1));
		V_3 = (int32_t)L_26;
		__this->___count_10 = L_26;
		int32_t L_27 = V_3;
		int32_t L_28 = (int32_t)(__this->___threshold_11);
		if ((((int32_t)L_27) <= ((int32_t)L_28)))
		{
			goto IL_00d5;
		}
	}
	{
		NullCheck((Dictionary_2_t1_2704 *)__this);
		(( void (*) (Dictionary_2_t1_2704 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 36)->method)((Dictionary_2_t1_2704 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 36));
		int32_t L_29 = V_0;
		Int32U5BU5D_t1_275* L_30 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		NullCheck(L_30);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_29&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)((int32_t)(((Array_t *)L_30)->max_length))))));
	}

IL_00d5:
	{
		int32_t L_31 = (int32_t)(__this->___emptySlot_9);
		V_2 = (int32_t)L_31;
		int32_t L_32 = V_2;
		if ((!(((uint32_t)L_32) == ((uint32_t)(-1)))))
		{
			goto IL_00fa;
		}
	}
	{
		int32_t L_33 = (int32_t)(__this->___touchedSlots_8);
		int32_t L_34 = (int32_t)L_33;
		V_3 = (int32_t)L_34;
		__this->___touchedSlots_8 = ((int32_t)((int32_t)L_34+(int32_t)1));
		int32_t L_35 = V_3;
		V_2 = (int32_t)L_35;
		goto IL_0111;
	}

IL_00fa:
	{
		LinkU5BU5D_t1_1975* L_36 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		int32_t L_37 = V_2;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, L_37);
		int32_t L_38 = (int32_t)(((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_36, L_37, sizeof(Link_t1_256 )))->___Next_1);
		__this->___emptySlot_9 = L_38;
	}

IL_0111:
	{
		LinkU5BU5D_t1_1975* L_39 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		int32_t L_40 = V_2;
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, L_40);
		int32_t L_41 = V_0;
		((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_39, L_40, sizeof(Link_t1_256 )))->___HashCode_0 = L_41;
		LinkU5BU5D_t1_1975* L_42 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		int32_t L_43 = V_2;
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, L_43);
		Int32U5BU5D_t1_275* L_44 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		int32_t L_45 = V_1;
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, L_45);
		int32_t L_46 = L_45;
		((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_42, L_43, sizeof(Link_t1_256 )))->___Next_1 = ((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_44, L_46, sizeof(int32_t)))-(int32_t)1));
		Int32U5BU5D_t1_275* L_47 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		int32_t L_48 = V_1;
		int32_t L_49 = V_2;
		NullCheck(L_47);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_47, L_48);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_47, L_48, sizeof(int32_t))) = (int32_t)((int32_t)((int32_t)L_49+(int32_t)1));
		ObjectU5BU5D_t1_272* L_50 = (ObjectU5BU5D_t1_272*)(__this->___keySlots_6);
		int32_t L_51 = V_2;
		Object_t * L_52 = ___key;
		NullCheck(L_50);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_50, L_51);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_50, L_51, sizeof(Object_t *))) = (Object_t *)L_52;
		SingleU5BU5D_t1_1695* L_53 = (SingleU5BU5D_t1_1695*)(__this->___valueSlots_7);
		int32_t L_54 = V_2;
		float L_55 = ___value;
		NullCheck(L_53);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_53, L_54);
		*((float*)(float*)SZArrayLdElema(L_53, L_54, sizeof(float))) = (float)L_55;
		int32_t L_56 = (int32_t)(__this->___generation_14);
		__this->___generation_14 = ((int32_t)((int32_t)L_56+(int32_t)1));
		return;
	}
}
// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2<System.Object,System.Single>::get_Comparer()
extern "C" Object_t* Dictionary_2_get_Comparer_m1_26983_gshared (Dictionary_2_t1_2704 * __this, const MethodInfo* method)
{
	{
		Object_t* L_0 = (Object_t*)(__this->___hcp_12);
		return L_0;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Single>::Clear()
extern "C" void Dictionary_2_Clear_m1_26985_gshared (Dictionary_2_t1_2704 * __this, const MethodInfo* method)
{
	{
		__this->___count_10 = 0;
		Int32U5BU5D_t1_275* L_0 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		Int32U5BU5D_t1_275* L_1 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		NullCheck(L_1);
		Array_Clear_m1_1037(NULL /*static, unused*/, (Array_t *)(Array_t *)L_0, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Array_t *)L_1)->max_length)))), /*hidden argument*/NULL);
		ObjectU5BU5D_t1_272* L_2 = (ObjectU5BU5D_t1_272*)(__this->___keySlots_6);
		ObjectU5BU5D_t1_272* L_3 = (ObjectU5BU5D_t1_272*)(__this->___keySlots_6);
		NullCheck(L_3);
		Array_Clear_m1_1037(NULL /*static, unused*/, (Array_t *)(Array_t *)L_2, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Array_t *)L_3)->max_length)))), /*hidden argument*/NULL);
		SingleU5BU5D_t1_1695* L_4 = (SingleU5BU5D_t1_1695*)(__this->___valueSlots_7);
		SingleU5BU5D_t1_1695* L_5 = (SingleU5BU5D_t1_1695*)(__this->___valueSlots_7);
		NullCheck(L_5);
		Array_Clear_m1_1037(NULL /*static, unused*/, (Array_t *)(Array_t *)L_4, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Array_t *)L_5)->max_length)))), /*hidden argument*/NULL);
		LinkU5BU5D_t1_1975* L_6 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		LinkU5BU5D_t1_1975* L_7 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		NullCheck(L_7);
		Array_Clear_m1_1037(NULL /*static, unused*/, (Array_t *)(Array_t *)L_6, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Array_t *)L_7)->max_length)))), /*hidden argument*/NULL);
		__this->___emptySlot_9 = (-1);
		__this->___touchedSlots_8 = 0;
		int32_t L_8 = (int32_t)(__this->___generation_14);
		__this->___generation_14 = ((int32_t)((int32_t)L_8+(int32_t)1));
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Single>::ContainsKey(TKey)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral793;
extern "C" bool Dictionary_2_ContainsKey_m1_26987_gshared (Dictionary_2_t1_2704 * __this, Object_t * ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		_stringLiteral793 = il2cpp_codegen_string_literal_from_index(793);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Object_t * L_0 = ___key;
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, (String_t*)_stringLiteral793, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		Object_t* L_2 = (Object_t*)(__this->___hcp_12);
		Object_t * L_3 = ___key;
		NullCheck((Object_t*)L_2);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker1< int32_t, Object_t * >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<System.Object>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 35), (Object_t*)L_2, (Object_t *)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648)));
		Int32U5BU5D_t1_275* L_5 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		int32_t L_6 = V_0;
		Int32U5BU5D_t1_275* L_7 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		NullCheck(L_7);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)((int32_t)(((Array_t *)L_7)->max_length)))))));
		int32_t L_8 = ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)((int32_t)(((Array_t *)L_7)->max_length))))));
		V_1 = (int32_t)((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_5, L_8, sizeof(int32_t)))-(int32_t)1));
		goto IL_0090;
	}

IL_0048:
	{
		LinkU5BU5D_t1_1975* L_9 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		int32_t L_10 = V_1;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = (int32_t)(((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_9, L_10, sizeof(Link_t1_256 )))->___HashCode_0);
		int32_t L_12 = V_0;
		if ((!(((uint32_t)L_11) == ((uint32_t)L_12))))
		{
			goto IL_007e;
		}
	}
	{
		Object_t* L_13 = (Object_t*)(__this->___hcp_12);
		ObjectU5BU5D_t1_272* L_14 = (ObjectU5BU5D_t1_272*)(__this->___keySlots_6);
		int32_t L_15 = V_1;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		Object_t * L_17 = ___key;
		NullCheck((Object_t*)L_13);
		bool L_18 = (bool)InterfaceFuncInvoker2< bool, Object_t *, Object_t * >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Object>::Equals(T,T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 35), (Object_t*)L_13, (Object_t *)(*(Object_t **)(Object_t **)SZArrayLdElema(L_14, L_16, sizeof(Object_t *))), (Object_t *)L_17);
		if (!L_18)
		{
			goto IL_007e;
		}
	}
	{
		return 1;
	}

IL_007e:
	{
		LinkU5BU5D_t1_1975* L_19 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		int32_t L_20 = V_1;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, L_20);
		int32_t L_21 = (int32_t)(((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_19, L_20, sizeof(Link_t1_256 )))->___Next_1);
		V_1 = (int32_t)L_21;
	}

IL_0090:
	{
		int32_t L_22 = V_1;
		if ((!(((uint32_t)L_22) == ((uint32_t)(-1)))))
		{
			goto IL_0048;
		}
	}
	{
		return 0;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Single>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m1_26989_gshared (Dictionary_2_t1_2704 * __this, float ___value, const MethodInfo* method)
{
	Object_t* V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 47));
		EqualityComparer_1_t1_2717 * L_0 = (( EqualityComparer_1_t1_2717 * (*) (Object_t * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 46)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 46));
		V_0 = (Object_t*)L_0;
		V_1 = (int32_t)0;
		goto IL_0054;
	}

IL_000d:
	{
		Int32U5BU5D_t1_275* L_1 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		int32_t L_2 = V_1;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		V_2 = (int32_t)((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_1, L_3, sizeof(int32_t)))-(int32_t)1));
		goto IL_0049;
	}

IL_001d:
	{
		Object_t* L_4 = V_0;
		SingleU5BU5D_t1_1695* L_5 = (SingleU5BU5D_t1_1695*)(__this->___valueSlots_7);
		int32_t L_6 = V_2;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		float L_8 = ___value;
		NullCheck((Object_t*)L_4);
		bool L_9 = (bool)InterfaceFuncInvoker2< bool, float, float >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Single>::Equals(T,T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 48), (Object_t*)L_4, (float)(*(float*)(float*)SZArrayLdElema(L_5, L_7, sizeof(float))), (float)L_8);
		if (!L_9)
		{
			goto IL_0037;
		}
	}
	{
		return 1;
	}

IL_0037:
	{
		LinkU5BU5D_t1_1975* L_10 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		int32_t L_11 = V_2;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = (int32_t)(((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_10, L_11, sizeof(Link_t1_256 )))->___Next_1);
		V_2 = (int32_t)L_12;
	}

IL_0049:
	{
		int32_t L_13 = V_2;
		if ((!(((uint32_t)L_13) == ((uint32_t)(-1)))))
		{
			goto IL_001d;
		}
	}
	{
		int32_t L_14 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_0054:
	{
		int32_t L_15 = V_1;
		Int32U5BU5D_t1_275* L_16 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		NullCheck(L_16);
		if ((((int32_t)L_15) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_16)->max_length)))))))
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Single>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral135;
extern Il2CppCodeGenString* _stringLiteral799;
extern Il2CppCodeGenString* _stringLiteral800;
extern Il2CppCodeGenString* _stringLiteral801;
extern Il2CppCodeGenString* _stringLiteral802;
extern "C" void Dictionary_2_GetObjectData_m1_26991_gshared (Dictionary_2_t1_2704 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		_stringLiteral135 = il2cpp_codegen_string_literal_from_index(135);
		_stringLiteral799 = il2cpp_codegen_string_literal_from_index(799);
		_stringLiteral800 = il2cpp_codegen_string_literal_from_index(800);
		_stringLiteral801 = il2cpp_codegen_string_literal_from_index(801);
		_stringLiteral802 = il2cpp_codegen_string_literal_from_index(802);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2U5BU5D_t1_2889* V_0 = {0};
	{
		SerializationInfo_t1_293 * L_0 = ___info;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, (String_t*)_stringLiteral135, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		SerializationInfo_t1_293 * L_2 = ___info;
		int32_t L_3 = (int32_t)(__this->___generation_14);
		NullCheck((SerializationInfo_t1_293 *)L_2);
		SerializationInfo_AddValue_m1_9630((SerializationInfo_t1_293 *)L_2, (String_t*)_stringLiteral799, (int32_t)L_3, /*hidden argument*/NULL);
		SerializationInfo_t1_293 * L_4 = ___info;
		Object_t* L_5 = (Object_t*)(__this->___hcp_12);
		NullCheck((SerializationInfo_t1_293 *)L_4);
		SerializationInfo_AddValue_m1_9642((SerializationInfo_t1_293 *)L_4, (String_t*)_stringLiteral800, (Object_t *)L_5, /*hidden argument*/NULL);
		V_0 = (KeyValuePair_2U5BU5D_t1_2889*)NULL;
		int32_t L_6 = (int32_t)(__this->___count_10);
		if ((((int32_t)L_6) <= ((int32_t)0)))
		{
			goto IL_0055;
		}
	}
	{
		int32_t L_7 = (int32_t)(__this->___count_10);
		V_0 = (KeyValuePair_2U5BU5D_t1_2889*)((KeyValuePair_2U5BU5D_t1_2889*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 49), L_7));
		KeyValuePair_2U5BU5D_t1_2889* L_8 = V_0;
		NullCheck((Dictionary_2_t1_2704 *)__this);
		(( void (*) (Dictionary_2_t1_2704 *, KeyValuePair_2U5BU5D_t1_2889*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19)->method)((Dictionary_2_t1_2704 *)__this, (KeyValuePair_2U5BU5D_t1_2889*)L_8, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19));
	}

IL_0055:
	{
		SerializationInfo_t1_293 * L_9 = ___info;
		Int32U5BU5D_t1_275* L_10 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		NullCheck(L_10);
		NullCheck((SerializationInfo_t1_293 *)L_9);
		SerializationInfo_AddValue_m1_9630((SerializationInfo_t1_293 *)L_9, (String_t*)_stringLiteral801, (int32_t)(((int32_t)((int32_t)(((Array_t *)L_10)->max_length)))), /*hidden argument*/NULL);
		SerializationInfo_t1_293 * L_11 = ___info;
		KeyValuePair_2U5BU5D_t1_2889* L_12 = V_0;
		NullCheck((SerializationInfo_t1_293 *)L_11);
		SerializationInfo_AddValue_m1_9642((SerializationInfo_t1_293 *)L_11, (String_t*)_stringLiteral802, (Object_t *)(Object_t *)L_12, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Single>::OnDeserialization(System.Object)
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral799;
extern Il2CppCodeGenString* _stringLiteral800;
extern Il2CppCodeGenString* _stringLiteral801;
extern Il2CppCodeGenString* _stringLiteral802;
extern "C" void Dictionary_2_OnDeserialization_m1_26993_gshared (Dictionary_2_t1_2704 * __this, Object_t * ___sender, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		_stringLiteral799 = il2cpp_codegen_string_literal_from_index(799);
		_stringLiteral800 = il2cpp_codegen_string_literal_from_index(800);
		_stringLiteral801 = il2cpp_codegen_string_literal_from_index(801);
		_stringLiteral802 = il2cpp_codegen_string_literal_from_index(802);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	KeyValuePair_2U5BU5D_t1_2889* V_1 = {0};
	int32_t V_2 = 0;
	{
		SerializationInfo_t1_293 * L_0 = (SerializationInfo_t1_293 *)(__this->___serialization_info_13);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		SerializationInfo_t1_293 * L_1 = (SerializationInfo_t1_293 *)(__this->___serialization_info_13);
		NullCheck((SerializationInfo_t1_293 *)L_1);
		int32_t L_2 = SerializationInfo_GetInt32_m1_9650((SerializationInfo_t1_293 *)L_1, (String_t*)_stringLiteral799, /*hidden argument*/NULL);
		__this->___generation_14 = L_2;
		SerializationInfo_t1_293 * L_3 = (SerializationInfo_t1_293 *)(__this->___serialization_info_13);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 50)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t1_293 *)L_3);
		Object_t * L_5 = SerializationInfo_GetValue_m1_9625((SerializationInfo_t1_293 *)L_3, (String_t*)_stringLiteral800, (Type_t *)L_4, /*hidden argument*/NULL);
		__this->___hcp_12 = ((Object_t*)Castclass(L_5, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 35)));
		SerializationInfo_t1_293 * L_6 = (SerializationInfo_t1_293 *)(__this->___serialization_info_13);
		NullCheck((SerializationInfo_t1_293 *)L_6);
		int32_t L_7 = SerializationInfo_GetInt32_m1_9650((SerializationInfo_t1_293 *)L_6, (String_t*)_stringLiteral801, /*hidden argument*/NULL);
		V_0 = (int32_t)L_7;
		SerializationInfo_t1_293 * L_8 = (SerializationInfo_t1_293 *)(__this->___serialization_info_13);
		Type_t * L_9 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 51)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t1_293 *)L_8);
		Object_t * L_10 = SerializationInfo_GetValue_m1_9625((SerializationInfo_t1_293 *)L_8, (String_t*)_stringLiteral802, (Type_t *)L_9, /*hidden argument*/NULL);
		V_1 = (KeyValuePair_2U5BU5D_t1_2889*)((KeyValuePair_2U5BU5D_t1_2889*)Castclass(L_10, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20)));
		int32_t L_11 = V_0;
		if ((((int32_t)L_11) >= ((int32_t)((int32_t)10))))
		{
			goto IL_0083;
		}
	}
	{
		V_0 = (int32_t)((int32_t)10);
	}

IL_0083:
	{
		int32_t L_12 = V_0;
		NullCheck((Dictionary_2_t1_2704 *)__this);
		(( void (*) (Dictionary_2_t1_2704 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39)->method)((Dictionary_2_t1_2704 *)__this, (int32_t)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39));
		__this->___count_10 = 0;
		KeyValuePair_2U5BU5D_t1_2889* L_13 = V_1;
		if (!L_13)
		{
			goto IL_00c9;
		}
	}
	{
		V_2 = (int32_t)0;
		goto IL_00c0;
	}

IL_009e:
	{
		KeyValuePair_2U5BU5D_t1_2889* L_14 = V_1;
		int32_t L_15 = V_2;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		Object_t * L_16 = (( Object_t * (*) (KeyValuePair_2_t1_2706 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((KeyValuePair_2_t1_2706 *)((KeyValuePair_2_t1_2706 *)(KeyValuePair_2_t1_2706 *)SZArrayLdElema(L_14, L_15, sizeof(KeyValuePair_2_t1_2706 ))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		KeyValuePair_2U5BU5D_t1_2889* L_17 = V_1;
		int32_t L_18 = V_2;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		float L_19 = (( float (*) (KeyValuePair_2_t1_2706 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((KeyValuePair_2_t1_2706 *)((KeyValuePair_2_t1_2706 *)(KeyValuePair_2_t1_2706 *)SZArrayLdElema(L_17, L_18, sizeof(KeyValuePair_2_t1_2706 ))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		NullCheck((Dictionary_2_t1_2704 *)__this);
		VirtActionInvoker2< Object_t *, float >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Single>::Add(TKey,TValue) */, (Dictionary_2_t1_2704 *)__this, (Object_t *)L_16, (float)L_19);
		int32_t L_20 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_00c0:
	{
		int32_t L_21 = V_2;
		KeyValuePair_2U5BU5D_t1_2889* L_22 = V_1;
		NullCheck(L_22);
		if ((((int32_t)L_21) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_22)->max_length)))))))
		{
			goto IL_009e;
		}
	}

IL_00c9:
	{
		int32_t L_23 = (int32_t)(__this->___generation_14);
		__this->___generation_14 = ((int32_t)((int32_t)L_23+(int32_t)1));
		__this->___serialization_info_13 = (SerializationInfo_t1_293 *)NULL;
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Single>::Remove(TKey)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t1_17_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral793;
extern "C" bool Dictionary_2_Remove_m1_26995_gshared (Dictionary_2_t1_2704 * __this, Object_t * ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		Single_t1_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		_stringLiteral793 = il2cpp_codegen_string_literal_from_index(793);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	Object_t * V_4 = {0};
	float V_5 = 0.0f;
	{
		Object_t * L_0 = ___key;
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, (String_t*)_stringLiteral793, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		Object_t* L_2 = (Object_t*)(__this->___hcp_12);
		Object_t * L_3 = ___key;
		NullCheck((Object_t*)L_2);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker1< int32_t, Object_t * >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<System.Object>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 35), (Object_t*)L_2, (Object_t *)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648)));
		int32_t L_5 = V_0;
		Int32U5BU5D_t1_275* L_6 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		NullCheck(L_6);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)((int32_t)(((Array_t *)L_6)->max_length))))));
		Int32U5BU5D_t1_275* L_7 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		int32_t L_8 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		V_2 = (int32_t)((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_7, L_9, sizeof(int32_t)))-(int32_t)1));
		int32_t L_10 = V_2;
		if ((!(((uint32_t)L_10) == ((uint32_t)(-1)))))
		{
			goto IL_004e;
		}
	}
	{
		return 0;
	}

IL_004e:
	{
		V_3 = (int32_t)(-1);
	}

IL_0050:
	{
		LinkU5BU5D_t1_1975* L_11 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		int32_t L_12 = V_2;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		int32_t L_13 = (int32_t)(((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_11, L_12, sizeof(Link_t1_256 )))->___HashCode_0);
		int32_t L_14 = V_0;
		if ((!(((uint32_t)L_13) == ((uint32_t)L_14))))
		{
			goto IL_0089;
		}
	}
	{
		Object_t* L_15 = (Object_t*)(__this->___hcp_12);
		ObjectU5BU5D_t1_272* L_16 = (ObjectU5BU5D_t1_272*)(__this->___keySlots_6);
		int32_t L_17 = V_2;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		int32_t L_18 = L_17;
		Object_t * L_19 = ___key;
		NullCheck((Object_t*)L_15);
		bool L_20 = (bool)InterfaceFuncInvoker2< bool, Object_t *, Object_t * >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Object>::Equals(T,T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 35), (Object_t*)L_15, (Object_t *)(*(Object_t **)(Object_t **)SZArrayLdElema(L_16, L_18, sizeof(Object_t *))), (Object_t *)L_19);
		if (!L_20)
		{
			goto IL_0089;
		}
	}
	{
		goto IL_00a4;
	}

IL_0089:
	{
		int32_t L_21 = V_2;
		V_3 = (int32_t)L_21;
		LinkU5BU5D_t1_1975* L_22 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		int32_t L_23 = V_2;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
		int32_t L_24 = (int32_t)(((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_22, L_23, sizeof(Link_t1_256 )))->___Next_1);
		V_2 = (int32_t)L_24;
		int32_t L_25 = V_2;
		if ((!(((uint32_t)L_25) == ((uint32_t)(-1)))))
		{
			goto IL_0050;
		}
	}

IL_00a4:
	{
		int32_t L_26 = V_2;
		if ((!(((uint32_t)L_26) == ((uint32_t)(-1)))))
		{
			goto IL_00ad;
		}
	}
	{
		return 0;
	}

IL_00ad:
	{
		int32_t L_27 = (int32_t)(__this->___count_10);
		__this->___count_10 = ((int32_t)((int32_t)L_27-(int32_t)1));
		int32_t L_28 = V_3;
		if ((!(((uint32_t)L_28) == ((uint32_t)(-1)))))
		{
			goto IL_00e2;
		}
	}
	{
		Int32U5BU5D_t1_275* L_29 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		int32_t L_30 = V_1;
		LinkU5BU5D_t1_1975* L_31 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		int32_t L_32 = V_2;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, L_32);
		int32_t L_33 = (int32_t)(((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_31, L_32, sizeof(Link_t1_256 )))->___Next_1);
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, L_30);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_29, L_30, sizeof(int32_t))) = (int32_t)((int32_t)((int32_t)L_33+(int32_t)1));
		goto IL_0104;
	}

IL_00e2:
	{
		LinkU5BU5D_t1_1975* L_34 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		int32_t L_35 = V_3;
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, L_35);
		LinkU5BU5D_t1_1975* L_36 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		int32_t L_37 = V_2;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, L_37);
		int32_t L_38 = (int32_t)(((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_36, L_37, sizeof(Link_t1_256 )))->___Next_1);
		((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_34, L_35, sizeof(Link_t1_256 )))->___Next_1 = L_38;
	}

IL_0104:
	{
		LinkU5BU5D_t1_1975* L_39 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		int32_t L_40 = V_2;
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, L_40);
		int32_t L_41 = (int32_t)(__this->___emptySlot_9);
		((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_39, L_40, sizeof(Link_t1_256 )))->___Next_1 = L_41;
		int32_t L_42 = V_2;
		__this->___emptySlot_9 = L_42;
		LinkU5BU5D_t1_1975* L_43 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		int32_t L_44 = V_2;
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, L_44);
		((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_43, L_44, sizeof(Link_t1_256 )))->___HashCode_0 = 0;
		ObjectU5BU5D_t1_272* L_45 = (ObjectU5BU5D_t1_272*)(__this->___keySlots_6);
		int32_t L_46 = V_2;
		Initobj (Object_t_il2cpp_TypeInfo_var, (&V_4));
		Object_t * L_47 = V_4;
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, L_46);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_45, L_46, sizeof(Object_t *))) = (Object_t *)L_47;
		SingleU5BU5D_t1_1695* L_48 = (SingleU5BU5D_t1_1695*)(__this->___valueSlots_7);
		int32_t L_49 = V_2;
		Initobj (Single_t1_17_il2cpp_TypeInfo_var, (&V_5));
		float L_50 = V_5;
		NullCheck(L_48);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_48, L_49);
		*((float*)(float*)SZArrayLdElema(L_48, L_49, sizeof(float))) = (float)L_50;
		int32_t L_51 = (int32_t)(__this->___generation_14);
		__this->___generation_14 = ((int32_t)((int32_t)L_51+(int32_t)1));
		return 1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Single>::TryGetValue(TKey,TValue&)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t1_17_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral793;
extern "C" bool Dictionary_2_TryGetValue_m1_26997_gshared (Dictionary_2_t1_2704 * __this, Object_t * ___key, float* ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		Single_t1_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		_stringLiteral793 = il2cpp_codegen_string_literal_from_index(793);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	{
		Object_t * L_0 = ___key;
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, (String_t*)_stringLiteral793, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		Object_t* L_2 = (Object_t*)(__this->___hcp_12);
		Object_t * L_3 = ___key;
		NullCheck((Object_t*)L_2);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker1< int32_t, Object_t * >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<System.Object>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 35), (Object_t*)L_2, (Object_t *)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648)));
		Int32U5BU5D_t1_275* L_5 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		int32_t L_6 = V_0;
		Int32U5BU5D_t1_275* L_7 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		NullCheck(L_7);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)((int32_t)(((Array_t *)L_7)->max_length)))))));
		int32_t L_8 = ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)((int32_t)(((Array_t *)L_7)->max_length))))));
		V_1 = (int32_t)((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_5, L_8, sizeof(int32_t)))-(int32_t)1));
		goto IL_00a2;
	}

IL_0048:
	{
		LinkU5BU5D_t1_1975* L_9 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		int32_t L_10 = V_1;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = (int32_t)(((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_9, L_10, sizeof(Link_t1_256 )))->___HashCode_0);
		int32_t L_12 = V_0;
		if ((!(((uint32_t)L_11) == ((uint32_t)L_12))))
		{
			goto IL_0090;
		}
	}
	{
		Object_t* L_13 = (Object_t*)(__this->___hcp_12);
		ObjectU5BU5D_t1_272* L_14 = (ObjectU5BU5D_t1_272*)(__this->___keySlots_6);
		int32_t L_15 = V_1;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		Object_t * L_17 = ___key;
		NullCheck((Object_t*)L_13);
		bool L_18 = (bool)InterfaceFuncInvoker2< bool, Object_t *, Object_t * >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Object>::Equals(T,T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 35), (Object_t*)L_13, (Object_t *)(*(Object_t **)(Object_t **)SZArrayLdElema(L_14, L_16, sizeof(Object_t *))), (Object_t *)L_17);
		if (!L_18)
		{
			goto IL_0090;
		}
	}
	{
		float* L_19 = ___value;
		SingleU5BU5D_t1_1695* L_20 = (SingleU5BU5D_t1_1695*)(__this->___valueSlots_7);
		int32_t L_21 = V_1;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		int32_t L_22 = L_21;
		(*(float*)L_19) = (*(float*)(float*)SZArrayLdElema(L_20, L_22, sizeof(float)));
		return 1;
	}

IL_0090:
	{
		LinkU5BU5D_t1_1975* L_23 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		int32_t L_24 = V_1;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_24);
		int32_t L_25 = (int32_t)(((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_23, L_24, sizeof(Link_t1_256 )))->___Next_1);
		V_1 = (int32_t)L_25;
	}

IL_00a2:
	{
		int32_t L_26 = V_1;
		if ((!(((uint32_t)L_26) == ((uint32_t)(-1)))))
		{
			goto IL_0048;
		}
	}
	{
		float* L_27 = ___value;
		Initobj (Single_t1_17_il2cpp_TypeInfo_var, (&V_2));
		float L_28 = V_2;
		(*(float*)L_27) = L_28;
		return 0;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Single>::get_Keys()
extern "C" KeyCollection_t1_2708 * Dictionary_2_get_Keys_m1_26999_gshared (Dictionary_2_t1_2704 * __this, const MethodInfo* method)
{
	{
		KeyCollection_t1_2708 * L_0 = (KeyCollection_t1_2708 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 52));
		(( void (*) (KeyCollection_t1_2708 *, Dictionary_2_t1_2704 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 53)->method)(L_0, (Dictionary_2_t1_2704 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 53));
		return L_0;
	}
}
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Single>::get_Values()
extern "C" ValueCollection_t1_2712 * Dictionary_2_get_Values_m1_27001_gshared (Dictionary_2_t1_2704 * __this, const MethodInfo* method)
{
	{
		ValueCollection_t1_2712 * L_0 = (ValueCollection_t1_2712 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 54));
		(( void (*) (ValueCollection_t1_2712 *, Dictionary_2_t1_2704 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 55)->method)(L_0, (Dictionary_2_t1_2704 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 55));
		return L_0;
	}
}
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Single>::ToTKey(System.Object)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral793;
extern Il2CppCodeGenString* _stringLiteral803;
extern "C" Object_t * Dictionary_2_ToTKey_m1_27003_gshared (Dictionary_2_t1_2704 * __this, Object_t * ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		_stringLiteral793 = il2cpp_codegen_string_literal_from_index(793);
		_stringLiteral803 = il2cpp_codegen_string_literal_from_index(803);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___key;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, (String_t*)_stringLiteral793, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		Object_t * L_2 = ___key;
		if (((Object_t *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10))))
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 56)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Type::ToString() */, (Type_t *)L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m1_559(NULL /*static, unused*/, (String_t*)_stringLiteral803, (String_t*)L_4, /*hidden argument*/NULL);
		ArgumentException_t1_1425 * L_6 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13262(L_6, (String_t*)L_5, (String_t*)_stringLiteral793, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_6);
	}

IL_0040:
	{
		Object_t * L_7 = ___key;
		return ((Object_t *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)));
	}
}
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Single>::ToTValue(System.Object)
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t1_17_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral803;
extern Il2CppCodeGenString* _stringLiteral61;
extern "C" float Dictionary_2_ToTValue_m1_27005_gshared (Dictionary_2_t1_2704 * __this, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Single_t1_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		_stringLiteral803 = il2cpp_codegen_string_literal_from_index(803);
		_stringLiteral61 = il2cpp_codegen_string_literal_from_index(61);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		Object_t * L_0 = ___value;
		if (L_0)
		{
			goto IL_0024;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 57)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_1);
		bool L_2 = (bool)VirtFuncInvoker0< bool >::Invoke(147 /* System.Boolean System.Type::get_IsValueType() */, (Type_t *)L_1);
		if (L_2)
		{
			goto IL_0024;
		}
	}
	{
		Initobj (Single_t1_17_il2cpp_TypeInfo_var, (&V_0));
		float L_3 = V_0;
		return L_3;
	}

IL_0024:
	{
		Object_t * L_4 = ___value;
		if (((Object_t *)IsInst(L_4, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14))))
		{
			goto IL_0053;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 57)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_5);
		String_t* L_6 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Type::ToString() */, (Type_t *)L_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m1_559(NULL /*static, unused*/, (String_t*)_stringLiteral803, (String_t*)L_6, /*hidden argument*/NULL);
		ArgumentException_t1_1425 * L_8 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13262(L_8, (String_t*)L_7, (String_t*)_stringLiteral61, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_8);
	}

IL_0053:
	{
		Object_t * L_9 = ___value;
		return ((*(float*)((float*)UnBox (L_9, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)))));
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Single>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m1_27007_gshared (Dictionary_2_t1_2704 * __this, KeyValuePair_2_t1_2706  ___pair, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		Object_t * L_0 = (( Object_t * (*) (KeyValuePair_2_t1_2706 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((KeyValuePair_2_t1_2706 *)(&___pair), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((Dictionary_2_t1_2704 *)__this);
		bool L_1 = (bool)VirtFuncInvoker2< bool, Object_t *, float* >::Invoke(21 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Single>::TryGetValue(TKey,TValue&) */, (Dictionary_2_t1_2704 *)__this, (Object_t *)L_0, (float*)(&V_0));
		if (L_1)
		{
			goto IL_0016;
		}
	}
	{
		return 0;
	}

IL_0016:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 47));
		EqualityComparer_1_t1_2717 * L_2 = (( EqualityComparer_1_t1_2717 * (*) (Object_t * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 46)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 46));
		float L_3 = (( float (*) (KeyValuePair_2_t1_2706 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((KeyValuePair_2_t1_2706 *)(&___pair), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		float L_4 = V_0;
		NullCheck((EqualityComparer_1_t1_2717 *)L_2);
		bool L_5 = (bool)VirtFuncInvoker2< bool, float, float >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Single>::Equals(T,T) */, (EqualityComparer_1_t1_2717 *)L_2, (float)L_3, (float)L_4);
		return L_5;
	}
}
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Single>::GetEnumerator()
extern "C" Enumerator_t1_2710  Dictionary_2_GetEnumerator_m1_27009_gshared (Dictionary_2_t1_2704 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1_2710  L_0 = {0};
		(( void (*) (Enumerator_t1_2710 *, Dictionary_2_t1_2704 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 32)->method)(&L_0, (Dictionary_2_t1_2704 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 32));
		return L_0;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,System.Single>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t1_284  Dictionary_2_U3CCopyToU3Em__0_m1_27011_gshared (Object_t * __this /* static, unused */, Object_t * ___key, float ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___key;
		float L_1 = ___value;
		float L_2 = L_1;
		Object_t * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14), &L_2);
		DictionaryEntry_t1_284  L_4 = {0};
		DictionaryEntry__ctor_m1_3228(&L_4, (Object_t *)L_0, (Object_t *)L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_27060_gshared (InternalEnumerator_1_t1_2707 * __this, Array_t * ___array, const MethodInfo* method)
{
	{
		Array_t * L_0 = ___array;
		__this->___array_2 = L_0;
		__this->___idx_3 = ((int32_t)-2);
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_27061_gshared (InternalEnumerator_1_t1_2707 * __this, const MethodInfo* method)
{
	{
		__this->___idx_3 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_27062_gshared (InternalEnumerator_1_t1_2707 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1_2706  L_0 = (( KeyValuePair_2_t1_2706  (*) (InternalEnumerator_1_t1_2707 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t1_2707 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1_2706  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_27063_gshared (InternalEnumerator_1_t1_2707 * __this, const MethodInfo* method)
{
	{
		__this->___idx_3 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_27064_gshared (InternalEnumerator_1_t1_2707 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)(__this->___idx_3);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Array_t * L_1 = (Array_t *)(__this->___array_2);
		NullCheck((Array_t *)L_1);
		int32_t L_2 = Array_get_Length_m1_992((Array_t *)L_1, /*hidden argument*/NULL);
		__this->___idx_3 = L_2;
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)(__this->___idx_3);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)(__this->___idx_3);
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->___idx_3 = L_5;
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::get_Current()
extern TypeInfo* InvalidOperationException_t1_1559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral217;
extern Il2CppCodeGenString* _stringLiteral218;
extern "C" KeyValuePair_2_t1_2706  InternalEnumerator_1_get_Current_m1_27065_gshared (InternalEnumerator_1_t1_2707 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvalidOperationException_t1_1559_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(78);
		_stringLiteral217 = il2cpp_codegen_string_literal_from_index(217);
		_stringLiteral218 = il2cpp_codegen_string_literal_from_index(218);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)(__this->___idx_3);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1_1559 * L_1 = (InvalidOperationException_t1_1559 *)il2cpp_codegen_object_new (InvalidOperationException_t1_1559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1_14171(L_1, (String_t*)_stringLiteral217, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)(__this->___idx_3);
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1_1559 * L_3 = (InvalidOperationException_t1_1559 *)il2cpp_codegen_object_new (InvalidOperationException_t1_1559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1_14171(L_3, (String_t*)_stringLiteral218, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_002f:
	{
		Array_t * L_4 = (Array_t *)(__this->___array_2);
		Array_t * L_5 = (Array_t *)(__this->___array_2);
		NullCheck((Array_t *)L_5);
		int32_t L_6 = Array_get_Length_m1_992((Array_t *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)(__this->___idx_3);
		NullCheck((Array_t *)L_4);
		KeyValuePair_2_t1_2706  L_8 = (( KeyValuePair_2_t1_2706  (*) (Array_t *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Array_t *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m1_26802_gshared (KeyValuePair_2_t1_2706 * __this, Object_t * ___key, float ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___key;
		(( void (*) (KeyValuePair_2_t1_2706 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((KeyValuePair_2_t1_2706 *)__this, (Object_t *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		float L_1 = ___value;
		(( void (*) (KeyValuePair_2_t1_2706 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((KeyValuePair_2_t1_2706 *)__this, (float)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::get_Key()
extern "C" Object_t * KeyValuePair_2_get_Key_m1_26804_gshared (KeyValuePair_2_t1_2706 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (Object_t *)(__this->___key_0);
		return L_0;
	}
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m1_26806_gshared (KeyValuePair_2_t1_2706 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		__this->___key_0 = L_0;
		return;
	}
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::get_Value()
extern "C" float KeyValuePair_2_get_Value_m1_26808_gshared (KeyValuePair_2_t1_2706 * __this, const MethodInfo* method)
{
	{
		float L_0 = (float)(__this->___value_1);
		return L_0;
	}
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m1_26810_gshared (KeyValuePair_2_t1_2706 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___value_1 = L_0;
		return;
	}
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::ToString()
extern TypeInfo* StringU5BU5D_t1_238_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral264;
extern Il2CppCodeGenString* _stringLiteral167;
extern Il2CppCodeGenString* _stringLiteral266;
extern "C" String_t* KeyValuePair_2_ToString_m1_26812_gshared (KeyValuePair_2_t1_2706 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringU5BU5D_t1_238_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(35);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral264 = il2cpp_codegen_string_literal_from_index(264);
		_stringLiteral167 = il2cpp_codegen_string_literal_from_index(167);
		_stringLiteral266 = il2cpp_codegen_string_literal_from_index(266);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	float V_1 = 0.0f;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1_238* G_B2_1 = {0};
	StringU5BU5D_t1_238* G_B2_2 = {0};
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1_238* G_B1_1 = {0};
	StringU5BU5D_t1_238* G_B1_2 = {0};
	String_t* G_B3_0 = {0};
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1_238* G_B3_2 = {0};
	StringU5BU5D_t1_238* G_B3_3 = {0};
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1_238* G_B5_1 = {0};
	StringU5BU5D_t1_238* G_B5_2 = {0};
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1_238* G_B4_1 = {0};
	StringU5BU5D_t1_238* G_B4_2 = {0};
	String_t* G_B6_0 = {0};
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1_238* G_B6_2 = {0};
	StringU5BU5D_t1_238* G_B6_3 = {0};
	{
		StringU5BU5D_t1_238* L_0 = (StringU5BU5D_t1_238*)((StringU5BU5D_t1_238*)SZArrayNew(StringU5BU5D_t1_238_il2cpp_TypeInfo_var, 5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral264);
		*((String_t**)(String_t**)SZArrayLdElema(L_0, 0, sizeof(String_t*))) = (String_t*)_stringLiteral264;
		StringU5BU5D_t1_238* L_1 = (StringU5BU5D_t1_238*)L_0;
		Object_t * L_2 = (( Object_t * (*) (KeyValuePair_2_t1_2706 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((KeyValuePair_2_t1_2706 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Object_t * L_3 = (( Object_t * (*) (KeyValuePair_2_t1_2706 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((KeyValuePair_2_t1_2706 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Object_t *)L_3;
		NullCheck((Object_t *)(*(&V_0)));
		String_t* L_4 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Object_t *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		*((String_t**)(String_t**)SZArrayLdElema(G_B3_2, G_B3_1, sizeof(String_t*))) = (String_t*)G_B3_0;
		StringU5BU5D_t1_238* L_6 = (StringU5BU5D_t1_238*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral167);
		*((String_t**)(String_t**)SZArrayLdElema(L_6, 2, sizeof(String_t*))) = (String_t*)_stringLiteral167;
		StringU5BU5D_t1_238* L_7 = (StringU5BU5D_t1_238*)L_6;
		float L_8 = (( float (*) (KeyValuePair_2_t1_2706 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((KeyValuePair_2_t1_2706 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		float L_9 = (( float (*) (KeyValuePair_2_t1_2706 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((KeyValuePair_2_t1_2706 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (float)L_9;
		NullCheck((float*)(&V_1));
		String_t* L_10 = Single_ToString_m1_633((float*)(&V_1), NULL);
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		*((String_t**)(String_t**)SZArrayLdElema(G_B6_2, G_B6_1, sizeof(String_t*))) = (String_t*)G_B6_0;
		StringU5BU5D_t1_238* L_12 = (StringU5BU5D_t1_238*)G_B6_3;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, _stringLiteral266);
		*((String_t**)(String_t**)SZArrayLdElema(L_12, 4, sizeof(String_t*))) = (String_t*)_stringLiteral266;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m1_563(NULL /*static, unused*/, (StringU5BU5D_t1_238*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Single>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral792;
extern "C" void KeyCollection__ctor_m1_26814_gshared (KeyCollection_t1_2708 * __this, Dictionary_2_t1_2704 * ___dictionary, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		_stringLiteral792 = il2cpp_codegen_string_literal_from_index(792);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1_0((Object_t *)__this, /*hidden argument*/NULL);
		Dictionary_2_t1_2704 * L_0 = ___dictionary;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, (String_t*)_stringLiteral792, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0017:
	{
		Dictionary_2_t1_2704 * L_2 = ___dictionary;
		__this->___dictionary_0 = L_2;
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Single>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral806;
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1_26816_gshared (KeyCollection_t1_2708 * __this, Object_t * ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		_stringLiteral806 = il2cpp_codegen_string_literal_from_index(806);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14417(L_0, (String_t*)_stringLiteral806, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Single>::System.Collections.Generic.ICollection<TKey>.Clear()
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral806;
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1_26818_gshared (KeyCollection_t1_2708 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		_stringLiteral806 = il2cpp_codegen_string_literal_from_index(806);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14417(L_0, (String_t*)_stringLiteral806, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Single>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C" bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1_26820_gshared (KeyCollection_t1_2708 * __this, Object_t * ___item, const MethodInfo* method)
{
	{
		Dictionary_2_t1_2704 * L_0 = (Dictionary_2_t1_2704 *)(__this->___dictionary_0);
		Object_t * L_1 = ___item;
		NullCheck((Dictionary_2_t1_2704 *)L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Single>::ContainsKey(TKey) */, (Dictionary_2_t1_2704 *)L_0, (Object_t *)L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Single>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral806;
extern "C" bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1_26822_gshared (KeyCollection_t1_2708 * __this, Object_t * ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		_stringLiteral806 = il2cpp_codegen_string_literal_from_index(806);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14417(L_0, (String_t*)_stringLiteral806, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Single>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C" Object_t* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1_26824_gshared (KeyCollection_t1_2708 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t1_2708 *)__this);
		Enumerator_t1_2709  L_0 = (( Enumerator_t1_2709  (*) (KeyCollection_t1_2708 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((KeyCollection_t1_2708 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Enumerator_t1_2709  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return (Object_t*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Single>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m1_26826_gshared (KeyCollection_t1_2708 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method)
{
	ObjectU5BU5D_t1_272* V_0 = {0};
	{
		Array_t * L_0 = ___array;
		V_0 = (ObjectU5BU5D_t1_272*)((ObjectU5BU5D_t1_272*)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
		ObjectU5BU5D_t1_272* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		ObjectU5BU5D_t1_272* L_2 = V_0;
		int32_t L_3 = ___index;
		NullCheck((KeyCollection_t1_2708 *)__this);
		(( void (*) (KeyCollection_t1_2708 *, ObjectU5BU5D_t1_272*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((KeyCollection_t1_2708 *)__this, (ObjectU5BU5D_t1_272*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t1_2704 * L_4 = (Dictionary_2_t1_2704 *)(__this->___dictionary_0);
		Array_t * L_5 = ___array;
		int32_t L_6 = ___index;
		NullCheck((Dictionary_2_t1_2704 *)L_4);
		(( void (*) (Dictionary_2_t1_2704 *, Array_t *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Dictionary_2_t1_2704 *)L_4, (Array_t *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		Dictionary_2_t1_2704 * L_7 = (Dictionary_2_t1_2704 *)(__this->___dictionary_0);
		Array_t * L_8 = ___array;
		int32_t L_9 = ___index;
		IntPtr_t L_10 = { (void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6) };
		Transform_1_t1_2711 * L_11 = (Transform_1_t1_2711 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		(( void (*) (Transform_1_t1_2711 *, Object_t *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(L_11, (Object_t *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		NullCheck((Dictionary_2_t1_2704 *)L_7);
		(( void (*) (Dictionary_2_t1_2704 *, Array_t *, int32_t, Transform_1_t1_2711 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Dictionary_2_t1_2704 *)L_7, (Array_t *)L_8, (int32_t)L_9, (Transform_1_t1_2711 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Single>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1_26828_gshared (KeyCollection_t1_2708 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t1_2708 *)__this);
		Enumerator_t1_2709  L_0 = (( Enumerator_t1_2709  (*) (KeyCollection_t1_2708 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((KeyCollection_t1_2708 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Enumerator_t1_2709  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return (Object_t *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Single>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C" bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1_26830_gshared (KeyCollection_t1_2708 * __this, const MethodInfo* method)
{
	{
		return 1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Single>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1_26832_gshared (KeyCollection_t1_2708 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Single>::System.Collections.ICollection.get_SyncRoot()
extern TypeInfo* ICollection_t1_280_il2cpp_TypeInfo_var;
extern "C" Object_t * KeyCollection_System_Collections_ICollection_get_SyncRoot_m1_26834_gshared (KeyCollection_t1_2708 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ICollection_t1_280_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(149);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t1_2704 * L_0 = (Dictionary_2_t1_2704 *)(__this->___dictionary_0);
		NullCheck((Object_t *)L_0);
		Object_t * L_1 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t1_280_il2cpp_TypeInfo_var, (Object_t *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Single>::CopyTo(TKey[],System.Int32)
extern "C" void KeyCollection_CopyTo_m1_26836_gshared (KeyCollection_t1_2708 * __this, ObjectU5BU5D_t1_272* ___array, int32_t ___index, const MethodInfo* method)
{
	{
		Dictionary_2_t1_2704 * L_0 = (Dictionary_2_t1_2704 *)(__this->___dictionary_0);
		ObjectU5BU5D_t1_272* L_1 = ___array;
		int32_t L_2 = ___index;
		NullCheck((Dictionary_2_t1_2704 *)L_0);
		(( void (*) (Dictionary_2_t1_2704 *, Array_t *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Dictionary_2_t1_2704 *)L_0, (Array_t *)(Array_t *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		Dictionary_2_t1_2704 * L_3 = (Dictionary_2_t1_2704 *)(__this->___dictionary_0);
		ObjectU5BU5D_t1_272* L_4 = ___array;
		int32_t L_5 = ___index;
		IntPtr_t L_6 = { (void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6) };
		Transform_1_t1_2711 * L_7 = (Transform_1_t1_2711 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		(( void (*) (Transform_1_t1_2711 *, Object_t *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(L_7, (Object_t *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		NullCheck((Dictionary_2_t1_2704 *)L_3);
		(( void (*) (Dictionary_2_t1_2704 *, ObjectU5BU5D_t1_272*, int32_t, Transform_1_t1_2711 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Dictionary_2_t1_2704 *)L_3, (ObjectU5BU5D_t1_272*)L_4, (int32_t)L_5, (Transform_1_t1_2711 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Single>::GetEnumerator()
extern "C" Enumerator_t1_2709  KeyCollection_GetEnumerator_m1_26838_gshared (KeyCollection_t1_2708 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1_2704 * L_0 = (Dictionary_2_t1_2704 *)(__this->___dictionary_0);
		Enumerator_t1_2709  L_1 = {0};
		(( void (*) (Enumerator_t1_2709 *, Dictionary_2_t1_2704 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(&L_1, (Dictionary_2_t1_2704 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Single>::get_Count()
extern "C" int32_t KeyCollection_get_Count_m1_26840_gshared (KeyCollection_t1_2708 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1_2704 * L_0 = (Dictionary_2_t1_2704 *)(__this->___dictionary_0);
		NullCheck((Dictionary_2_t1_2704 *)L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.Single>::get_Count() */, (Dictionary_2_t1_2704 *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Single>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m1_27066_gshared (Enumerator_t1_2709 * __this, Dictionary_2_t1_2704 * ___host, const MethodInfo* method)
{
	{
		Dictionary_2_t1_2704 * L_0 = ___host;
		NullCheck((Dictionary_2_t1_2704 *)L_0);
		Enumerator_t1_2710  L_1 = (( Enumerator_t1_2710  (*) (Dictionary_2_t1_2704 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Dictionary_2_t1_2704 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->___host_enumerator_0 = L_1;
		return;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Single>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m1_27067_gshared (Enumerator_t1_2709 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1_2710 * L_0 = (Enumerator_t1_2710 *)&(__this->___host_enumerator_0);
		Object_t * L_1 = (( Object_t * (*) (Enumerator_t1_2710 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Enumerator_t1_2710 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Single>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m1_27068_gshared (Enumerator_t1_2709 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1_2710 * L_0 = (Enumerator_t1_2710 *)&(__this->___host_enumerator_0);
		(( void (*) (Enumerator_t1_2710 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((Enumerator_t1_2710 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Single>::Dispose()
extern "C" void Enumerator_Dispose_m1_27069_gshared (Enumerator_t1_2709 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1_2710 * L_0 = (Enumerator_t1_2710 *)&(__this->___host_enumerator_0);
		(( void (*) (Enumerator_t1_2710 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Enumerator_t1_2710 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Single>::MoveNext()
extern "C" bool Enumerator_MoveNext_m1_27070_gshared (Enumerator_t1_2709 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1_2710 * L_0 = (Enumerator_t1_2710 *)&(__this->___host_enumerator_0);
		bool L_1 = (( bool (*) (Enumerator_t1_2710 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Enumerator_t1_2710 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Single>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m1_27071_gshared (Enumerator_t1_2709 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1_2710 * L_0 = (Enumerator_t1_2710 *)&(__this->___host_enumerator_0);
		KeyValuePair_2_t1_2706 * L_1 = (KeyValuePair_2_t1_2706 *)&(L_0->___current_3);
		Object_t * L_2 = (( Object_t * (*) (KeyValuePair_2_t1_2706 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((KeyValuePair_2_t1_2706 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Single>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m1_26870_gshared (Enumerator_t1_2710 * __this, Dictionary_2_t1_2704 * ___dictionary, const MethodInfo* method)
{
	{
		Dictionary_2_t1_2704 * L_0 = ___dictionary;
		__this->___dictionary_0 = L_0;
		Dictionary_2_t1_2704 * L_1 = ___dictionary;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)(L_1->___generation_14);
		__this->___stamp_2 = L_2;
		return;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Single>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m1_26872_gshared (Enumerator_t1_2710 * __this, const MethodInfo* method)
{
	{
		(( void (*) (Enumerator_t1_2710 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t1_2710 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1_2706  L_0 = (KeyValuePair_2_t1_2706 )(__this->___current_3);
		KeyValuePair_2_t1_2706  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Single>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m1_26874_gshared (Enumerator_t1_2710 * __this, const MethodInfo* method)
{
	{
		(( void (*) (Enumerator_t1_2710 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Enumerator_t1_2710 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Single>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t1_284  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_26876_gshared (Enumerator_t1_2710 * __this, const MethodInfo* method)
{
	{
		(( void (*) (Enumerator_t1_2710 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t1_2710 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1_2706 * L_0 = (KeyValuePair_2_t1_2706 *)&(__this->___current_3);
		Object_t * L_1 = (( Object_t * (*) (KeyValuePair_2_t1_2706 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((KeyValuePair_2_t1_2706 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		KeyValuePair_2_t1_2706 * L_2 = (KeyValuePair_2_t1_2706 *)&(__this->___current_3);
		float L_3 = (( float (*) (KeyValuePair_2_t1_2706 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((KeyValuePair_2_t1_2706 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		float L_4 = L_3;
		Object_t * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_4);
		DictionaryEntry_t1_284  L_6 = {0};
		DictionaryEntry__ctor_m1_3228(&L_6, (Object_t *)L_1, (Object_t *)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Single>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_26878_gshared (Enumerator_t1_2710 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (( Object_t * (*) (Enumerator_t1_2710 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((Enumerator_t1_2710 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return L_0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Single>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_26880_gshared (Enumerator_t1_2710 * __this, const MethodInfo* method)
{
	{
		float L_0 = (( float (*) (Enumerator_t1_2710 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((Enumerator_t1_2710 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		float L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Single>::MoveNext()
extern "C" bool Enumerator_MoveNext_m1_26882_gshared (Enumerator_t1_2710 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		(( void (*) (Enumerator_t1_2710 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Enumerator_t1_2710 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)(__this->___next_1);
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return 0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)(__this->___next_1);
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->___next_1 = ((int32_t)((int32_t)L_2+(int32_t)1));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t1_2704 * L_4 = (Dictionary_2_t1_2704 *)(__this->___dictionary_0);
		NullCheck(L_4);
		LinkU5BU5D_t1_1975* L_5 = (LinkU5BU5D_t1_1975*)(L_4->___linkSlots_5);
		int32_t L_6 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = (int32_t)(((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_5, L_6, sizeof(Link_t1_256 )))->___HashCode_0);
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t1_2704 * L_8 = (Dictionary_2_t1_2704 *)(__this->___dictionary_0);
		NullCheck(L_8);
		ObjectU5BU5D_t1_272* L_9 = (ObjectU5BU5D_t1_272*)(L_8->___keySlots_6);
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		Dictionary_2_t1_2704 * L_12 = (Dictionary_2_t1_2704 *)(__this->___dictionary_0);
		NullCheck(L_12);
		SingleU5BU5D_t1_1695* L_13 = (SingleU5BU5D_t1_1695*)(L_12->___valueSlots_7);
		int32_t L_14 = V_0;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		int32_t L_15 = L_14;
		KeyValuePair_2_t1_2706  L_16 = {0};
		(( void (*) (KeyValuePair_2_t1_2706 *, Object_t *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(&L_16, (Object_t *)(*(Object_t **)(Object_t **)SZArrayLdElema(L_9, L_11, sizeof(Object_t *))), (float)(*(float*)(float*)SZArrayLdElema(L_13, L_15, sizeof(float))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->___current_3 = L_16;
		return 1;
	}

IL_007b:
	{
		int32_t L_17 = (int32_t)(__this->___next_1);
		Dictionary_2_t1_2704 * L_18 = (Dictionary_2_t1_2704 *)(__this->___dictionary_0);
		NullCheck(L_18);
		int32_t L_19 = (int32_t)(L_18->___touchedSlots_8);
		if ((((int32_t)L_17) < ((int32_t)L_19)))
		{
			goto IL_0019;
		}
	}
	{
		__this->___next_1 = (-1);
		return 0;
	}
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Single>::get_Current()
extern "C" KeyValuePair_2_t1_2706  Enumerator_get_Current_m1_26884_gshared (Enumerator_t1_2710 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1_2706  L_0 = (KeyValuePair_2_t1_2706 )(__this->___current_3);
		return L_0;
	}
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Single>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m1_26886_gshared (Enumerator_t1_2710 * __this, const MethodInfo* method)
{
	{
		(( void (*) (Enumerator_t1_2710 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t1_2710 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1_2706 * L_0 = (KeyValuePair_2_t1_2706 *)&(__this->___current_3);
		Object_t * L_1 = (( Object_t * (*) (KeyValuePair_2_t1_2706 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((KeyValuePair_2_t1_2706 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Single>::get_CurrentValue()
extern "C" float Enumerator_get_CurrentValue_m1_26888_gshared (Enumerator_t1_2710 * __this, const MethodInfo* method)
{
	{
		(( void (*) (Enumerator_t1_2710 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t1_2710 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1_2706 * L_0 = (KeyValuePair_2_t1_2706 *)&(__this->___current_3);
		float L_1 = (( float (*) (KeyValuePair_2_t1_2706 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((KeyValuePair_2_t1_2706 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Single>::Reset()
extern "C" void Enumerator_Reset_m1_26890_gshared (Enumerator_t1_2710 * __this, const MethodInfo* method)
{
	{
		(( void (*) (Enumerator_t1_2710 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Enumerator_t1_2710 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->___next_1 = 0;
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Single>::VerifyState()
extern TypeInfo* ObjectDisposedException_t1_1588_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidOperationException_t1_1559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral804;
extern "C" void Enumerator_VerifyState_m1_26892_gshared (Enumerator_t1_2710 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectDisposedException_t1_1588_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(136);
		InvalidOperationException_t1_1559_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(78);
		_stringLiteral804 = il2cpp_codegen_string_literal_from_index(804);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t1_2704 * L_0 = (Dictionary_2_t1_2704 *)(__this->___dictionary_0);
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t1_1588 * L_1 = (ObjectDisposedException_t1_1588 *)il2cpp_codegen_object_new (ObjectDisposedException_t1_1588_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1_14523(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0012:
	{
		Dictionary_2_t1_2704 * L_2 = (Dictionary_2_t1_2704 *)(__this->___dictionary_0);
		NullCheck(L_2);
		int32_t L_3 = (int32_t)(L_2->___generation_14);
		int32_t L_4 = (int32_t)(__this->___stamp_2);
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t1_1559 * L_5 = (InvalidOperationException_t1_1559 *)il2cpp_codegen_object_new (InvalidOperationException_t1_1559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1_14171(L_5, (String_t*)_stringLiteral804, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_5);
	}

IL_0033:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Single>::VerifyCurrent()
extern TypeInfo* InvalidOperationException_t1_1559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral805;
extern "C" void Enumerator_VerifyCurrent_m1_26894_gshared (Enumerator_t1_2710 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvalidOperationException_t1_1559_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(78);
		_stringLiteral805 = il2cpp_codegen_string_literal_from_index(805);
		s_Il2CppMethodIntialized = true;
	}
	{
		(( void (*) (Enumerator_t1_2710 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Enumerator_t1_2710 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)(__this->___next_1);
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t1_1559 * L_1 = (InvalidOperationException_t1_1559 *)il2cpp_codegen_object_new (InvalidOperationException_t1_1559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1_14171(L_1, (String_t*)_stringLiteral805, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_001d:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Single>::Dispose()
extern "C" void Enumerator_Dispose_m1_26896_gshared (Enumerator_t1_2710 * __this, const MethodInfo* method)
{
	{
		__this->___dictionary_0 = (Dictionary_2_t1_2704 *)NULL;
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C" void Transform_1__ctor_m1_27072_gshared (Transform_1_t1_2711 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Object>::Invoke(TKey,TValue)
extern "C" Object_t * Transform_1_Invoke_m1_27073_gshared (Transform_1_t1_2711 * __this, Object_t * ___key, float ___value, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		Transform_1_Invoke_m1_27073((Transform_1_t1_2711 *)__this->___prev_9,___key, ___value, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef Object_t * (*FunctionPointerType) (Object_t *, Object_t * __this, Object_t * ___key, float ___value, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___key, ___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef Object_t * (*FunctionPointerType) (Object_t * __this, Object_t * ___key, float ___value, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___key, ___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef Object_t * (*FunctionPointerType) (Object_t * __this, float ___value, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(___key, ___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern TypeInfo* Single_t1_17_il2cpp_TypeInfo_var;
extern "C" Object_t * Transform_1_BeginInvoke_m1_27074_gshared (Transform_1_t1_2711 * __this, Object_t * ___key, float ___value, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Single_t1_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key;
	__d_args[1] = Box(Single_t1_17_il2cpp_TypeInfo_var, &___value);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Object>::EndInvoke(System.IAsyncResult)
extern "C" Object_t * Transform_1_EndInvoke_m1_27075_gshared (Transform_1_t1_2711 * __this, Object_t * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return (Object_t *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral792;
extern "C" void ValueCollection__ctor_m1_26842_gshared (ValueCollection_t1_2712 * __this, Dictionary_2_t1_2704 * ___dictionary, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		_stringLiteral792 = il2cpp_codegen_string_literal_from_index(792);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1_0((Object_t *)__this, /*hidden argument*/NULL);
		Dictionary_2_t1_2704 * L_0 = ___dictionary;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, (String_t*)_stringLiteral792, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0017:
	{
		Dictionary_2_t1_2704 * L_2 = ___dictionary;
		__this->___dictionary_0 = L_2;
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral806;
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1_26844_gshared (ValueCollection_t1_2712 * __this, float ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		_stringLiteral806 = il2cpp_codegen_string_literal_from_index(806);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14417(L_0, (String_t*)_stringLiteral806, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::System.Collections.Generic.ICollection<TValue>.Clear()
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral806;
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1_26846_gshared (ValueCollection_t1_2712 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		_stringLiteral806 = il2cpp_codegen_string_literal_from_index(806);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14417(L_0, (String_t*)_stringLiteral806, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1_26848_gshared (ValueCollection_t1_2712 * __this, float ___item, const MethodInfo* method)
{
	{
		Dictionary_2_t1_2704 * L_0 = (Dictionary_2_t1_2704 *)(__this->___dictionary_0);
		float L_1 = ___item;
		NullCheck((Dictionary_2_t1_2704 *)L_0);
		bool L_2 = (( bool (*) (Dictionary_2_t1_2704 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Dictionary_2_t1_2704 *)L_0, (float)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral806;
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1_26850_gshared (ValueCollection_t1_2712 * __this, float ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		_stringLiteral806 = il2cpp_codegen_string_literal_from_index(806);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14417(L_0, (String_t*)_stringLiteral806, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C" Object_t* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1_26852_gshared (ValueCollection_t1_2712 * __this, const MethodInfo* method)
{
	{
		NullCheck((ValueCollection_t1_2712 *)__this);
		Enumerator_t1_2713  L_0 = (( Enumerator_t1_2713  (*) (ValueCollection_t1_2712 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ValueCollection_t1_2712 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Enumerator_t1_2713  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return (Object_t*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m1_26854_gshared (ValueCollection_t1_2712 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method)
{
	SingleU5BU5D_t1_1695* V_0 = {0};
	{
		Array_t * L_0 = ___array;
		V_0 = (SingleU5BU5D_t1_1695*)((SingleU5BU5D_t1_1695*)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
		SingleU5BU5D_t1_1695* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		SingleU5BU5D_t1_1695* L_2 = V_0;
		int32_t L_3 = ___index;
		NullCheck((ValueCollection_t1_2712 *)__this);
		(( void (*) (ValueCollection_t1_2712 *, SingleU5BU5D_t1_1695*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((ValueCollection_t1_2712 *)__this, (SingleU5BU5D_t1_1695*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t1_2704 * L_4 = (Dictionary_2_t1_2704 *)(__this->___dictionary_0);
		Array_t * L_5 = ___array;
		int32_t L_6 = ___index;
		NullCheck((Dictionary_2_t1_2704 *)L_4);
		(( void (*) (Dictionary_2_t1_2704 *, Array_t *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Dictionary_2_t1_2704 *)L_4, (Array_t *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		Dictionary_2_t1_2704 * L_7 = (Dictionary_2_t1_2704 *)(__this->___dictionary_0);
		Array_t * L_8 = ___array;
		int32_t L_9 = ___index;
		IntPtr_t L_10 = { (void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6) };
		Transform_1_t1_2714 * L_11 = (Transform_1_t1_2714 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		(( void (*) (Transform_1_t1_2714 *, Object_t *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(L_11, (Object_t *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		NullCheck((Dictionary_2_t1_2704 *)L_7);
		(( void (*) (Dictionary_2_t1_2704 *, Array_t *, int32_t, Transform_1_t1_2714 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Dictionary_2_t1_2704 *)L_7, (Array_t *)L_8, (int32_t)L_9, (Transform_1_t1_2714 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1_26856_gshared (ValueCollection_t1_2712 * __this, const MethodInfo* method)
{
	{
		NullCheck((ValueCollection_t1_2712 *)__this);
		Enumerator_t1_2713  L_0 = (( Enumerator_t1_2713  (*) (ValueCollection_t1_2712 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ValueCollection_t1_2712 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Enumerator_t1_2713  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return (Object_t *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1_26858_gshared (ValueCollection_t1_2712 * __this, const MethodInfo* method)
{
	{
		return 1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1_26860_gshared (ValueCollection_t1_2712 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::System.Collections.ICollection.get_SyncRoot()
extern TypeInfo* ICollection_t1_280_il2cpp_TypeInfo_var;
extern "C" Object_t * ValueCollection_System_Collections_ICollection_get_SyncRoot_m1_26862_gshared (ValueCollection_t1_2712 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ICollection_t1_280_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(149);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t1_2704 * L_0 = (Dictionary_2_t1_2704 *)(__this->___dictionary_0);
		NullCheck((Object_t *)L_0);
		Object_t * L_1 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t1_280_il2cpp_TypeInfo_var, (Object_t *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::CopyTo(TValue[],System.Int32)
extern "C" void ValueCollection_CopyTo_m1_26864_gshared (ValueCollection_t1_2712 * __this, SingleU5BU5D_t1_1695* ___array, int32_t ___index, const MethodInfo* method)
{
	{
		Dictionary_2_t1_2704 * L_0 = (Dictionary_2_t1_2704 *)(__this->___dictionary_0);
		SingleU5BU5D_t1_1695* L_1 = ___array;
		int32_t L_2 = ___index;
		NullCheck((Dictionary_2_t1_2704 *)L_0);
		(( void (*) (Dictionary_2_t1_2704 *, Array_t *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Dictionary_2_t1_2704 *)L_0, (Array_t *)(Array_t *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		Dictionary_2_t1_2704 * L_3 = (Dictionary_2_t1_2704 *)(__this->___dictionary_0);
		SingleU5BU5D_t1_1695* L_4 = ___array;
		int32_t L_5 = ___index;
		IntPtr_t L_6 = { (void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6) };
		Transform_1_t1_2714 * L_7 = (Transform_1_t1_2714 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		(( void (*) (Transform_1_t1_2714 *, Object_t *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(L_7, (Object_t *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		NullCheck((Dictionary_2_t1_2704 *)L_3);
		(( void (*) (Dictionary_2_t1_2704 *, SingleU5BU5D_t1_1695*, int32_t, Transform_1_t1_2714 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Dictionary_2_t1_2704 *)L_3, (SingleU5BU5D_t1_1695*)L_4, (int32_t)L_5, (Transform_1_t1_2714 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::GetEnumerator()
extern "C" Enumerator_t1_2713  ValueCollection_GetEnumerator_m1_26866_gshared (ValueCollection_t1_2712 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1_2704 * L_0 = (Dictionary_2_t1_2704 *)(__this->___dictionary_0);
		Enumerator_t1_2713  L_1 = {0};
		(( void (*) (Enumerator_t1_2713 *, Dictionary_2_t1_2704 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(&L_1, (Dictionary_2_t1_2704 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::get_Count()
extern "C" int32_t ValueCollection_get_Count_m1_26868_gshared (ValueCollection_t1_2712 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1_2704 * L_0 = (Dictionary_2_t1_2704 *)(__this->___dictionary_0);
		NullCheck((Dictionary_2_t1_2704 *)L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.Single>::get_Count() */, (Dictionary_2_t1_2704 *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Single>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m1_27076_gshared (Enumerator_t1_2713 * __this, Dictionary_2_t1_2704 * ___host, const MethodInfo* method)
{
	{
		Dictionary_2_t1_2704 * L_0 = ___host;
		NullCheck((Dictionary_2_t1_2704 *)L_0);
		Enumerator_t1_2710  L_1 = (( Enumerator_t1_2710  (*) (Dictionary_2_t1_2704 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Dictionary_2_t1_2704 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->___host_enumerator_0 = L_1;
		return;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Single>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m1_27077_gshared (Enumerator_t1_2713 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1_2710 * L_0 = (Enumerator_t1_2710 *)&(__this->___host_enumerator_0);
		float L_1 = (( float (*) (Enumerator_t1_2710 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Enumerator_t1_2710 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		float L_2 = L_1;
		Object_t * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return L_3;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Single>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m1_27078_gshared (Enumerator_t1_2713 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1_2710 * L_0 = (Enumerator_t1_2710 *)&(__this->___host_enumerator_0);
		(( void (*) (Enumerator_t1_2710 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((Enumerator_t1_2710 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Single>::Dispose()
extern "C" void Enumerator_Dispose_m1_27079_gshared (Enumerator_t1_2713 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1_2710 * L_0 = (Enumerator_t1_2710 *)&(__this->___host_enumerator_0);
		(( void (*) (Enumerator_t1_2710 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Enumerator_t1_2710 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Single>::MoveNext()
extern "C" bool Enumerator_MoveNext_m1_27080_gshared (Enumerator_t1_2713 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1_2710 * L_0 = (Enumerator_t1_2710 *)&(__this->___host_enumerator_0);
		bool L_1 = (( bool (*) (Enumerator_t1_2710 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Enumerator_t1_2710 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Single>::get_Current()
extern "C" float Enumerator_get_Current_m1_27081_gshared (Enumerator_t1_2713 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1_2710 * L_0 = (Enumerator_t1_2710 *)&(__this->___host_enumerator_0);
		KeyValuePair_2_t1_2706 * L_1 = (KeyValuePair_2_t1_2706 *)&(L_0->___current_3);
		float L_2 = (( float (*) (KeyValuePair_2_t1_2706 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((KeyValuePair_2_t1_2706 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Single>::.ctor(System.Object,System.IntPtr)
extern "C" void Transform_1__ctor_m1_27082_gshared (Transform_1_t1_2714 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Single>::Invoke(TKey,TValue)
extern "C" float Transform_1_Invoke_m1_27083_gshared (Transform_1_t1_2714 * __this, Object_t * ___key, float ___value, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		Transform_1_Invoke_m1_27083((Transform_1_t1_2714 *)__this->___prev_9,___key, ___value, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef float (*FunctionPointerType) (Object_t *, Object_t * __this, Object_t * ___key, float ___value, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___key, ___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef float (*FunctionPointerType) (Object_t * __this, Object_t * ___key, float ___value, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___key, ___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef float (*FunctionPointerType) (Object_t * __this, float ___value, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(___key, ___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Single>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern TypeInfo* Single_t1_17_il2cpp_TypeInfo_var;
extern "C" Object_t * Transform_1_BeginInvoke_m1_27084_gshared (Transform_1_t1_2714 * __this, Object_t * ___key, float ___value, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Single_t1_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key;
	__d_args[1] = Box(Single_t1_17_il2cpp_TypeInfo_var, &___value);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Single>::EndInvoke(System.IAsyncResult)
extern "C" float Transform_1_EndInvoke_m1_27085_gshared (Transform_1_t1_2714 * __this, Object_t * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return *(float*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C" void Transform_1__ctor_m1_26794_gshared (Transform_1_t1_2705 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C" DictionaryEntry_t1_284  Transform_1_Invoke_m1_26796_gshared (Transform_1_t1_2705 * __this, Object_t * ___key, float ___value, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		Transform_1_Invoke_m1_26796((Transform_1_t1_2705 *)__this->___prev_9,___key, ___value, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t1_284  (*FunctionPointerType) (Object_t *, Object_t * __this, Object_t * ___key, float ___value, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___key, ___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef DictionaryEntry_t1_284  (*FunctionPointerType) (Object_t * __this, Object_t * ___key, float ___value, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___key, ___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef DictionaryEntry_t1_284  (*FunctionPointerType) (Object_t * __this, float ___value, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(___key, ___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern TypeInfo* Single_t1_17_il2cpp_TypeInfo_var;
extern "C" Object_t * Transform_1_BeginInvoke_m1_26798_gshared (Transform_1_t1_2705 * __this, Object_t * ___key, float ___value, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Single_t1_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key;
	__d_args[1] = Box(Single_t1_17_il2cpp_TypeInfo_var, &___value);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C" DictionaryEntry_t1_284  Transform_1_EndInvoke_m1_26800_gshared (Transform_1_t1_2705 * __this, Object_t * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return *(DictionaryEntry_t1_284 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::.ctor(System.Object,System.IntPtr)
extern "C" void Transform_1__ctor_m1_27086_gshared (Transform_1_t1_2715 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::Invoke(TKey,TValue)
extern "C" KeyValuePair_2_t1_2706  Transform_1_Invoke_m1_27087_gshared (Transform_1_t1_2715 * __this, Object_t * ___key, float ___value, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		Transform_1_Invoke_m1_27087((Transform_1_t1_2715 *)__this->___prev_9,___key, ___value, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t1_2706  (*FunctionPointerType) (Object_t *, Object_t * __this, Object_t * ___key, float ___value, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___key, ___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef KeyValuePair_2_t1_2706  (*FunctionPointerType) (Object_t * __this, Object_t * ___key, float ___value, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___key, ___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef KeyValuePair_2_t1_2706  (*FunctionPointerType) (Object_t * __this, float ___value, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(___key, ___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern TypeInfo* Single_t1_17_il2cpp_TypeInfo_var;
extern "C" Object_t * Transform_1_BeginInvoke_m1_27088_gshared (Transform_1_t1_2715 * __this, Object_t * ___key, float ___value, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Single_t1_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key;
	__d_args[1] = Box(Single_t1_17_il2cpp_TypeInfo_var, &___value);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Single,System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::EndInvoke(System.IAsyncResult)
extern "C" KeyValuePair_2_t1_2706  Transform_1_EndInvoke_m1_27089_gshared (Transform_1_t1_2715 * __this, Object_t * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return *(KeyValuePair_2_t1_2706 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Single>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m1_27090_gshared (ShimEnumerator_t1_2716 * __this, Dictionary_2_t1_2704 * ___host, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1_0((Object_t *)__this, /*hidden argument*/NULL);
		Dictionary_2_t1_2704 * L_0 = ___host;
		NullCheck((Dictionary_2_t1_2704 *)L_0);
		Enumerator_t1_2710  L_1 = (( Enumerator_t1_2710  (*) (Dictionary_2_t1_2704 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Dictionary_2_t1_2704 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->___host_enumerator_0 = L_1;
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Single>::Dispose()
extern "C" void ShimEnumerator_Dispose_m1_27091_gshared (ShimEnumerator_t1_2716 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1_2710 * L_0 = (Enumerator_t1_2710 *)&(__this->___host_enumerator_0);
		(( void (*) (Enumerator_t1_2710 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Enumerator_t1_2710 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Single>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m1_27092_gshared (ShimEnumerator_t1_2716 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1_2710 * L_0 = (Enumerator_t1_2710 *)&(__this->___host_enumerator_0);
		bool L_1 = (( bool (*) (Enumerator_t1_2710 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Enumerator_t1_2710 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Single>::get_Entry()
extern TypeInfo* IDictionaryEnumerator_t1_863_il2cpp_TypeInfo_var;
extern "C" DictionaryEntry_t1_284  ShimEnumerator_get_Entry_m1_27093_gshared (ShimEnumerator_t1_2716 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionaryEnumerator_t1_863_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(227);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t1_2710  L_0 = (Enumerator_t1_2710 )(__this->___host_enumerator_0);
		Enumerator_t1_2710  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), &L_1);
		NullCheck((Object_t *)L_2);
		DictionaryEntry_t1_284  L_3 = (DictionaryEntry_t1_284 )InterfaceFuncInvoker0< DictionaryEntry_t1_284  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t1_863_il2cpp_TypeInfo_var, (Object_t *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Single>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m1_27094_gshared (ShimEnumerator_t1_2716 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1_2706  V_0 = {0};
	{
		Enumerator_t1_2710 * L_0 = (Enumerator_t1_2710 *)&(__this->___host_enumerator_0);
		KeyValuePair_2_t1_2706  L_1 = (( KeyValuePair_2_t1_2706  (*) (Enumerator_t1_2710 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Enumerator_t1_2710 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_0 = (KeyValuePair_2_t1_2706 )L_1;
		Object_t * L_2 = (( Object_t * (*) (KeyValuePair_2_t1_2706 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((KeyValuePair_2_t1_2706 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Single>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m1_27095_gshared (ShimEnumerator_t1_2716 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1_2706  V_0 = {0};
	{
		Enumerator_t1_2710 * L_0 = (Enumerator_t1_2710 *)&(__this->___host_enumerator_0);
		KeyValuePair_2_t1_2706  L_1 = (( KeyValuePair_2_t1_2706  (*) (Enumerator_t1_2710 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Enumerator_t1_2710 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_0 = (KeyValuePair_2_t1_2706 )L_1;
		float L_2 = (( float (*) (KeyValuePair_2_t1_2706 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((KeyValuePair_2_t1_2706 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		float L_3 = L_2;
		Object_t * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Single>::get_Current()
extern TypeInfo* DictionaryEntry_t1_284_il2cpp_TypeInfo_var;
extern "C" Object_t * ShimEnumerator_get_Current_m1_27096_gshared (ShimEnumerator_t1_2716 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DictionaryEntry_t1_284_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(148);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((ShimEnumerator_t1_2716 *)__this);
		DictionaryEntry_t1_284  L_0 = (DictionaryEntry_t1_284 )VirtFuncInvoker0< DictionaryEntry_t1_284  >::Invoke(7 /* System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Single>::get_Entry() */, (ShimEnumerator_t1_2716 *)__this);
		DictionaryEntry_t1_284  L_1 = L_0;
		Object_t * L_2 = Box(DictionaryEntry_t1_284_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Single>::Reset()
extern "C" void ShimEnumerator_Reset_m1_27097_gshared (ShimEnumerator_t1_2716 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1_2710 * L_0 = (Enumerator_t1_2710 *)&(__this->___host_enumerator_0);
		(( void (*) (Enumerator_t1_2710 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Enumerator_t1_2710 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Single>::.ctor()
extern "C" void EqualityComparer_1__ctor_m1_27098_gshared (EqualityComparer_1_t1_2717 * __this, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1_0((Object_t *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Single>::.cctor()
extern const Il2CppType* GenericEqualityComparer_1_t1_3064_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* TypeU5BU5D_t1_31_il2cpp_TypeInfo_var;
extern "C" void EqualityComparer_1__cctor_m1_27099_gshared (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GenericEqualityComparer_1_t1_3064_0_0_0_var = il2cpp_codegen_type_from_index(4654);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		TypeU5BU5D_t1_31_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(63);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, Type_t * >::Invoke(169 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GenericEqualityComparer_1_t1_3064_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1_31* L_4 = (TypeU5BU5D_t1_31*)((TypeU5BU5D_t1_31*)SZArrayNew(TypeU5BU5D_t1_31_il2cpp_TypeInfo_var, 1));
		Type_t * L_5 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_4, 0, sizeof(Type_t *))) = (Type_t *)L_5;
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = (Type_t *)VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1_31* >::Invoke(210 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1_31*)L_4);
		Object_t * L_7 = Activator_CreateInstance_m1_13034(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t1_2717_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->____default_0 = ((EqualityComparer_1_t1_2717 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t1_2719 * L_8 = (DefaultComparer_t1_2719 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		(( void (*) (DefaultComparer_t1_2719 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t1_2717_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->____default_0 = L_8;
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Single>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C" int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1_27100_gshared (EqualityComparer_1_t1_2717 * __this, Object_t * ___obj, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___obj;
		NullCheck((EqualityComparer_1_t1_2717 *)__this);
		int32_t L_1 = (int32_t)VirtFuncInvoker1< int32_t, float >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Single>::GetHashCode(T) */, (EqualityComparer_1_t1_2717 *)__this, (float)((*(float*)((float*)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Single>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C" bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1_27101_gshared (EqualityComparer_1_t1_2717 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___x;
		Object_t * L_1 = ___y;
		NullCheck((EqualityComparer_1_t1_2717 *)__this);
		bool L_2 = (bool)VirtFuncInvoker2< bool, float, float >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Single>::Equals(T,T) */, (EqualityComparer_1_t1_2717 *)__this, (float)((*(float*)((float*)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6))))), (float)((*(float*)((float*)UnBox (L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Single>::get_Default()
extern "C" EqualityComparer_1_t1_2717 * EqualityComparer_1_get_Default_m1_27102_gshared (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t1_2717 * L_0 = ((EqualityComparer_1_t1_2717_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->____default_0;
		return L_0;
	}
}
// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.Single>::.ctor()
extern "C" void GenericEqualityComparer_1__ctor_m1_27103_gshared (GenericEqualityComparer_1_t1_2718 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t1_2717 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		(( void (*) (EqualityComparer_1_t1_2717 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((EqualityComparer_1_t1_2717 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Single>::GetHashCode(T)
extern "C" int32_t GenericEqualityComparer_1_GetHashCode_m1_27104_gshared (GenericEqualityComparer_1_t1_2718 * __this, float ___obj, const MethodInfo* method)
{
	{
		float L_0 = ___obj;
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		NullCheck((float*)(&___obj));
		int32_t L_1 = Single_GetHashCode_m1_622((float*)(&___obj), NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Single>::Equals(T,T)
extern "C" bool GenericEqualityComparer_1_Equals_m1_27105_gshared (GenericEqualityComparer_1_t1_2718 * __this, float ___x, float ___y, const MethodInfo* method)
{
	{
		float L_0 = ___x;
		goto IL_0015;
	}
	{
		float L_1 = ___y;
		float L_2 = L_1;
		Object_t * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return ((((Object_t*)(Object_t *)L_3) == ((Object_t*)(Object_t *)NULL))? 1 : 0);
	}

IL_0015:
	{
		float L_4 = ___y;
		NullCheck((float*)(&___x));
		bool L_5 = Single_Equals_m1_621((float*)(&___x), (float)L_4, NULL);
		return L_5;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Single>::.ctor()
extern "C" void DefaultComparer__ctor_m1_27106_gshared (DefaultComparer_t1_2719 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t1_2717 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		(( void (*) (EqualityComparer_1_t1_2717 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((EqualityComparer_1_t1_2717 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Single>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m1_27107_gshared (DefaultComparer_t1_2719 * __this, float ___obj, const MethodInfo* method)
{
	{
		float L_0 = ___obj;
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		NullCheck((float*)(&___obj));
		int32_t L_1 = Single_GetHashCode_m1_622((float*)(&___obj), NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Single>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m1_27108_gshared (DefaultComparer_t1_2719 * __this, float ___x, float ___y, const MethodInfo* method)
{
	{
		float L_0 = ___x;
		goto IL_0015;
	}
	{
		float L_1 = ___y;
		float L_2 = L_1;
		Object_t * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return ((((Object_t*)(Object_t *)L_3) == ((Object_t*)(Object_t *)NULL))? 1 : 0);
	}

IL_0015:
	{
		float L_4 = ___y;
		float L_5 = L_4;
		Object_t * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		NullCheck((float*)(&___x));
		bool L_7 = Single_Equals_m1_619((float*)(&___x), (Object_t *)L_6, NULL);
		return L_7;
	}
}
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::.ctor()
extern "C" void List_1__ctor_m1_15060_gshared (List_1_t1_1908 * __this, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1_0((Object_t *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ConsoleLogU5BU5D_t8_382* L_0 = ((List_1_t1_1908_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___EmptyArray_4;
		__this->____items_1 = L_0;
		return;
	}
}
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m1_27109_gshared (List_1_t1_1908 * __this, Object_t* ___collection, const MethodInfo* method)
{
	Object_t* V_0 = {0};
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1_0((Object_t *)__this, /*hidden argument*/NULL);
		Object_t* L_0 = ___collection;
		NullCheck((List_1_t1_1908 *)__this);
		(( void (*) (List_1_t1_1908 *, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((List_1_t1_1908 *)__this, (Object_t*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Object_t* L_1 = ___collection;
		V_0 = (Object_t*)((Object_t*)IsInst(L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
		Object_t* L_2 = V_0;
		if (L_2)
		{
			goto IL_0031;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ConsoleLogU5BU5D_t8_382* L_3 = ((List_1_t1_1908_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___EmptyArray_4;
		__this->____items_1 = L_3;
		Object_t* L_4 = ___collection;
		NullCheck((List_1_t1_1908 *)__this);
		(( void (*) (List_1_t1_1908 *, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((List_1_t1_1908 *)__this, (Object_t*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		goto IL_0049;
	}

IL_0031:
	{
		Object_t* L_5 = V_0;
		NullCheck((Object_t*)L_5);
		int32_t L_6 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::get_Count() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Object_t*)L_5);
		__this->____items_1 = ((ConsoleLogU5BU5D_t8_382*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), L_6));
		Object_t* L_7 = V_0;
		NullCheck((List_1_t1_1908 *)__this);
		(( void (*) (List_1_t1_1908 *, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((List_1_t1_1908 *)__this, (Object_t*)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
	}

IL_0049:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::.ctor(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral794;
extern "C" void List_1__ctor_m1_27110_gshared (List_1_t1_1908 * __this, int32_t ___capacity, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		_stringLiteral794 = il2cpp_codegen_string_literal_from_index(794);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1_0((Object_t *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___capacity;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		ArgumentOutOfRangeException_t1_1501 * L_1 = (ArgumentOutOfRangeException_t1_1501 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1_13274(L_1, (String_t*)_stringLiteral794, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0018:
	{
		int32_t L_2 = ___capacity;
		__this->____items_1 = ((ConsoleLogU5BU5D_t8_382*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), L_2));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::.ctor(T[],System.Int32)
extern "C" void List_1__ctor_m1_27111_gshared (List_1_t1_1908 * __this, ConsoleLogU5BU5D_t8_382* ___data, int32_t ___size, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1_0((Object_t *)__this, /*hidden argument*/NULL);
		ConsoleLogU5BU5D_t8_382* L_0 = ___data;
		__this->____items_1 = L_0;
		int32_t L_1 = ___size;
		__this->____size_2 = L_1;
		return;
	}
}
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::.cctor()
extern "C" void List_1__cctor_m1_27112_gshared (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		((List_1_t1_1908_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___EmptyArray_4 = ((ConsoleLogU5BU5D_t8_382*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), 0));
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_27113_gshared (List_1_t1_1908 * __this, const MethodInfo* method)
{
	{
		NullCheck((List_1_t1_1908 *)__this);
		Enumerator_t1_2721  L_0 = (( Enumerator_t1_2721  (*) (List_1_t1_1908 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((List_1_t1_1908 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		Enumerator_t1_2721  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), &L_1);
		return (Object_t*)L_2;
	}
}
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m1_27114_gshared (List_1_t1_1908 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method)
{
	{
		ConsoleLogU5BU5D_t8_382* L_0 = (ConsoleLogU5BU5D_t8_382*)(__this->____items_1);
		Array_t * L_1 = ___array;
		int32_t L_2 = ___arrayIndex;
		int32_t L_3 = (int32_t)(__this->____size_2);
		Array_Copy_m1_1041(NULL /*static, unused*/, (Array_t *)(Array_t *)L_0, (int32_t)0, (Array_t *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m1_27115_gshared (List_1_t1_1908 * __this, const MethodInfo* method)
{
	{
		NullCheck((List_1_t1_1908 *)__this);
		Enumerator_t1_2721  L_0 = (( Enumerator_t1_2721  (*) (List_1_t1_1908 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((List_1_t1_1908 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		Enumerator_t1_2721  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), &L_1);
		return (Object_t *)L_2;
	}
}
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IList.Add(System.Object)
extern TypeInfo* NullReferenceException_t1_1584_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidCastException_t1_1558_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral808;
extern "C" int32_t List_1_System_Collections_IList_Add_m1_27116_gshared (List_1_t1_1908 * __this, Object_t * ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NullReferenceException_t1_1584_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		InvalidCastException_t1_1558_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(26);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		_stringLiteral808 = il2cpp_codegen_string_literal_from_index(808);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Object_t * L_0 = ___item;
			NullCheck((List_1_t1_1908 *)__this);
			VirtActionInvoker1< ConsoleLog_t8_170  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Add(T) */, (List_1_t1_1908 *)__this, (ConsoleLog_t8_170 )((*(ConsoleLog_t8_170 *)((ConsoleLog_t8_170 *)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))));
			int32_t L_1 = (int32_t)(__this->____size_2);
			V_0 = (int32_t)((int32_t)((int32_t)L_1-(int32_t)1));
			goto IL_0036;
		}

IL_001a:
		{
			; // IL_001a: leave IL_002b
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1_33 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t1_1584_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001f;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t1_1558_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0025;
		throw e;
	}

CATCH_001f:
	{ // begin catch(System.NullReferenceException)
		goto IL_002b;
	} // end catch (depth: 1)

CATCH_0025:
	{ // begin catch(System.InvalidCastException)
		goto IL_002b;
	} // end catch (depth: 1)

IL_002b:
	{
		ArgumentException_t1_1425 * L_2 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13260(L_2, (String_t*)_stringLiteral808, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0036:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.Boolean System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IList.Contains(System.Object)
extern TypeInfo* NullReferenceException_t1_1584_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidCastException_t1_1558_il2cpp_TypeInfo_var;
extern "C" bool List_1_System_Collections_IList_Contains_m1_27117_gshared (List_1_t1_1908 * __this, Object_t * ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NullReferenceException_t1_1584_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		InvalidCastException_t1_1558_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(26);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Object_t * L_0 = ___item;
			NullCheck((List_1_t1_1908 *)__this);
			bool L_1 = (bool)VirtFuncInvoker1< bool, ConsoleLog_t8_170  >::Invoke(24 /* System.Boolean System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Contains(T) */, (List_1_t1_1908 *)__this, (ConsoleLog_t8_170 )((*(ConsoleLog_t8_170 *)((ConsoleLog_t8_170 *)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))));
			V_0 = (bool)L_1;
			goto IL_0025;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1_33 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t1_1584_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t1_1558_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return 0;
	}

IL_0025:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IList.IndexOf(System.Object)
extern TypeInfo* NullReferenceException_t1_1584_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidCastException_t1_1558_il2cpp_TypeInfo_var;
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m1_27118_gshared (List_1_t1_1908 * __this, Object_t * ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NullReferenceException_t1_1584_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		InvalidCastException_t1_1558_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(26);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Object_t * L_0 = ___item;
			NullCheck((List_1_t1_1908 *)__this);
			int32_t L_1 = (int32_t)VirtFuncInvoker1< int32_t, ConsoleLog_t8_170  >::Invoke(28 /* System.Int32 System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::IndexOf(T) */, (List_1_t1_1908 *)__this, (ConsoleLog_t8_170 )((*(ConsoleLog_t8_170 *)((ConsoleLog_t8_170 *)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))));
			V_0 = (int32_t)L_1;
			goto IL_0025;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1_33 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t1_1584_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t1_1558_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return (-1);
	}

IL_0025:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IList.Insert(System.Int32,System.Object)
extern TypeInfo* NullReferenceException_t1_1584_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidCastException_t1_1558_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral808;
extern "C" void List_1_System_Collections_IList_Insert_m1_27119_gshared (List_1_t1_1908 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NullReferenceException_t1_1584_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		InvalidCastException_t1_1558_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(26);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		_stringLiteral808 = il2cpp_codegen_string_literal_from_index(808);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = ___index;
		NullCheck((List_1_t1_1908 *)__this);
		(( void (*) (List_1_t1_1908 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((List_1_t1_1908 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_1 = ___index;
			Object_t * L_2 = ___item;
			NullCheck((List_1_t1_1908 *)__this);
			VirtActionInvoker2< int32_t, ConsoleLog_t8_170  >::Invoke(29 /* System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Insert(System.Int32,T) */, (List_1_t1_1908 *)__this, (int32_t)L_1, (ConsoleLog_t8_170 )((*(ConsoleLog_t8_170 *)((ConsoleLog_t8_170 *)UnBox (L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))));
			goto IL_0035;
		}

IL_0019:
		{
			; // IL_0019: leave IL_002a
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1_33 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t1_1584_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001e;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t1_1558_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0024;
		throw e;
	}

CATCH_001e:
	{ // begin catch(System.NullReferenceException)
		goto IL_002a;
	} // end catch (depth: 1)

CATCH_0024:
	{ // begin catch(System.InvalidCastException)
		goto IL_002a;
	} // end catch (depth: 1)

IL_002a:
	{
		ArgumentException_t1_1425 * L_3 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13260(L_3, (String_t*)_stringLiteral808, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0035:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IList.Remove(System.Object)
extern TypeInfo* NullReferenceException_t1_1584_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidCastException_t1_1558_il2cpp_TypeInfo_var;
extern "C" void List_1_System_Collections_IList_Remove_m1_27120_gshared (List_1_t1_1908 * __this, Object_t * ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NullReferenceException_t1_1584_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		InvalidCastException_t1_1558_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(26);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Object_t * L_0 = ___item;
			NullCheck((List_1_t1_1908 *)__this);
			VirtFuncInvoker1< bool, ConsoleLog_t8_170  >::Invoke(26 /* System.Boolean System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Remove(T) */, (List_1_t1_1908 *)__this, (ConsoleLog_t8_170 )((*(ConsoleLog_t8_170 *)((ConsoleLog_t8_170 *)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))));
			goto IL_0023;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1_33 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t1_1584_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t1_1558_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_27121_gshared (List_1_t1_1908 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Boolean System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m1_27122_gshared (List_1_t1_1908 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Object System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m1_27123_gshared (List_1_t1_1908 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Boolean System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m1_27124_gshared (List_1_t1_1908 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Boolean System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m1_27125_gshared (List_1_t1_1908 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Object System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m1_27126_gshared (List_1_t1_1908 * __this, int32_t ___index, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index;
		NullCheck((List_1_t1_1908 *)__this);
		ConsoleLog_t8_170  L_1 = (ConsoleLog_t8_170 )VirtFuncInvoker1< ConsoleLog_t8_170 , int32_t >::Invoke(31 /* T System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::get_Item(System.Int32) */, (List_1_t1_1908 *)__this, (int32_t)L_0);
		ConsoleLog_t8_170  L_2 = L_1;
		Object_t * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), &L_2);
		return L_3;
	}
}
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern TypeInfo* NullReferenceException_t1_1584_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidCastException_t1_1558_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral61;
extern "C" void List_1_System_Collections_IList_set_Item_m1_27127_gshared (List_1_t1_1908 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NullReferenceException_t1_1584_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		InvalidCastException_t1_1558_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(26);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		_stringLiteral61 = il2cpp_codegen_string_literal_from_index(61);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_0 = ___index;
			Object_t * L_1 = ___value;
			NullCheck((List_1_t1_1908 *)__this);
			VirtActionInvoker2< int32_t, ConsoleLog_t8_170  >::Invoke(32 /* System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::set_Item(System.Int32,T) */, (List_1_t1_1908 *)__this, (int32_t)L_0, (ConsoleLog_t8_170 )((*(ConsoleLog_t8_170 *)((ConsoleLog_t8_170 *)UnBox (L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))));
			goto IL_002e;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1_33 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t1_1584_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t1_1558_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		ArgumentException_t1_1425 * L_2 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13260(L_2, (String_t*)_stringLiteral61, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_002e:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Add(T)
extern "C" void List_1_Add_m1_27128_gshared (List_1_t1_1908 * __this, ConsoleLog_t8_170  ___item, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)(__this->____size_2);
		ConsoleLogU5BU5D_t8_382* L_1 = (ConsoleLogU5BU5D_t8_382*)(__this->____items_1);
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) == ((uint32_t)(((int32_t)((int32_t)(((Array_t *)L_1)->max_length))))))))
		{
			goto IL_001a;
		}
	}
	{
		NullCheck((List_1_t1_1908 *)__this);
		(( void (*) (List_1_t1_1908 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((List_1_t1_1908 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
	}

IL_001a:
	{
		ConsoleLogU5BU5D_t8_382* L_2 = (ConsoleLogU5BU5D_t8_382*)(__this->____items_1);
		int32_t L_3 = (int32_t)(__this->____size_2);
		int32_t L_4 = (int32_t)L_3;
		V_0 = (int32_t)L_4;
		__this->____size_2 = ((int32_t)((int32_t)L_4+(int32_t)1));
		int32_t L_5 = V_0;
		ConsoleLog_t8_170  L_6 = ___item;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_5);
		*((ConsoleLog_t8_170 *)(ConsoleLog_t8_170 *)SZArrayLdElema(L_2, L_5, sizeof(ConsoleLog_t8_170 ))) = (ConsoleLog_t8_170 )L_6;
		int32_t L_7 = (int32_t)(__this->____version_3);
		__this->____version_3 = ((int32_t)((int32_t)L_7+(int32_t)1));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m1_27129_gshared (List_1_t1_1908 * __this, int32_t ___newCount, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)(__this->____size_2);
		int32_t L_1 = ___newCount;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = V_0;
		ConsoleLogU5BU5D_t8_382* L_3 = (ConsoleLogU5BU5D_t8_382*)(__this->____items_1);
		NullCheck(L_3);
		if ((((int32_t)L_2) <= ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_3)->max_length)))))))
		{
			goto IL_0031;
		}
	}
	{
		NullCheck((List_1_t1_1908 *)__this);
		int32_t L_4 = (( int32_t (*) (List_1_t1_1908 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->method)((List_1_t1_1908 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18));
		int32_t L_5 = Math_Max_m1_14207(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)L_4*(int32_t)2)), (int32_t)4, /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		int32_t L_7 = Math_Max_m1_14207(NULL /*static, unused*/, (int32_t)L_5, (int32_t)L_6, /*hidden argument*/NULL);
		NullCheck((List_1_t1_1908 *)__this);
		(( void (*) (List_1_t1_1908 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19)->method)((List_1_t1_1908 *)__this, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19));
	}

IL_0031:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::CheckRange(System.Int32,System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral170;
extern Il2CppCodeGenString* _stringLiteral47;
extern Il2CppCodeGenString* _stringLiteral809;
extern "C" void List_1_CheckRange_m1_27130_gshared (List_1_t1_1908 * __this, int32_t ___idx, int32_t ___count, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		_stringLiteral170 = il2cpp_codegen_string_literal_from_index(170);
		_stringLiteral47 = il2cpp_codegen_string_literal_from_index(47);
		_stringLiteral809 = il2cpp_codegen_string_literal_from_index(809);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___idx;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0012;
		}
	}
	{
		ArgumentOutOfRangeException_t1_1501 * L_1 = (ArgumentOutOfRangeException_t1_1501 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1_13274(L_1, (String_t*)_stringLiteral170, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0012:
	{
		int32_t L_2 = ___count;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0024;
		}
	}
	{
		ArgumentOutOfRangeException_t1_1501 * L_3 = (ArgumentOutOfRangeException_t1_1501 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1_13274(L_3, (String_t*)_stringLiteral47, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0024:
	{
		int32_t L_4 = ___idx;
		int32_t L_5 = ___count;
		int32_t L_6 = (int32_t)(__this->____size_2);
		if ((!(((uint32_t)((int32_t)((int32_t)L_4+(int32_t)L_5))) > ((uint32_t)L_6))))
		{
			goto IL_003d;
		}
	}
	{
		ArgumentException_t1_1425 * L_7 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13260(L_7, (String_t*)_stringLiteral809, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_7);
	}

IL_003d:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m1_27131_gshared (List_1_t1_1908 * __this, Object_t* ___collection, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Object_t* L_0 = ___collection;
		NullCheck((Object_t*)L_0);
		int32_t L_1 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::get_Count() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Object_t*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if (L_2)
		{
			goto IL_000e;
		}
	}
	{
		return;
	}

IL_000e:
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t1_1908 *)__this);
		(( void (*) (List_1_t1_1908 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((List_1_t1_1908 *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		Object_t* L_4 = ___collection;
		ConsoleLogU5BU5D_t8_382* L_5 = (ConsoleLogU5BU5D_t8_382*)(__this->____items_1);
		int32_t L_6 = (int32_t)(__this->____size_2);
		NullCheck((Object_t*)L_4);
		InterfaceActionInvoker2< ConsoleLogU5BU5D_t8_382*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::CopyTo(T[],System.Int32) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Object_t*)L_4, (ConsoleLogU5BU5D_t8_382*)L_5, (int32_t)L_6);
		int32_t L_7 = (int32_t)(__this->____size_2);
		int32_t L_8 = V_0;
		__this->____size_2 = ((int32_t)((int32_t)L_7+(int32_t)L_8));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern TypeInfo* IEnumerator_t1_193_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern "C" void List_1_AddEnumerable_m1_27132_gshared (List_1_t1_1908 * __this, Object_t* ___enumerable, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IEnumerator_t1_193_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(144);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		s_Il2CppMethodIntialized = true;
	}
	ConsoleLog_t8_170  V_0 = {0};
	Object_t* V_1 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Object_t* L_0 = ___enumerable;
		NullCheck((Object_t*)L_0);
		Object_t* L_1 = (Object_t*)InterfaceFuncInvoker0< Object_t* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20), (Object_t*)L_0);
		V_1 = (Object_t*)L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_001a;
		}

IL_000c:
		{
			Object_t* L_2 = V_1;
			NullCheck((Object_t*)L_2);
			ConsoleLog_t8_170  L_3 = (ConsoleLog_t8_170 )InterfaceFuncInvoker0< ConsoleLog_t8_170  >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 21), (Object_t*)L_2);
			V_0 = (ConsoleLog_t8_170 )L_3;
			ConsoleLog_t8_170  L_4 = V_0;
			NullCheck((List_1_t1_1908 *)__this);
			VirtActionInvoker1< ConsoleLog_t8_170  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Add(T) */, (List_1_t1_1908 *)__this, (ConsoleLog_t8_170 )L_4);
		}

IL_001a:
		{
			Object_t* L_5 = V_1;
			NullCheck((Object_t *)L_5);
			bool L_6 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, (Object_t *)L_5);
			if (L_6)
			{
				goto IL_000c;
			}
		}

IL_0025:
		{
			IL2CPP_LEAVE(0x35, FINALLY_002a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_002a;
	}

FINALLY_002a:
	{ // begin finally (depth: 1)
		{
			Object_t* L_7 = V_1;
			if (L_7)
			{
				goto IL_002e;
			}
		}

IL_002d:
		{
			IL2CPP_END_FINALLY(42)
		}

IL_002e:
		{
			Object_t* L_8 = V_1;
			NullCheck((Object_t *)L_8);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, (Object_t *)L_8);
			IL2CPP_END_FINALLY(42)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(42)
	{
		IL2CPP_JUMP_TBL(0x35, IL_0035)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0035:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m1_27133_gshared (List_1_t1_1908 * __this, Object_t* ___collection, const MethodInfo* method)
{
	Object_t* V_0 = {0};
	{
		Object_t* L_0 = ___collection;
		NullCheck((List_1_t1_1908 *)__this);
		(( void (*) (List_1_t1_1908 *, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((List_1_t1_1908 *)__this, (Object_t*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Object_t* L_1 = ___collection;
		V_0 = (Object_t*)((Object_t*)IsInst(L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
		Object_t* L_2 = V_0;
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		Object_t* L_3 = V_0;
		NullCheck((List_1_t1_1908 *)__this);
		(( void (*) (List_1_t1_1908 *, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((List_1_t1_1908 *)__this, (Object_t*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		goto IL_0027;
	}

IL_0020:
	{
		Object_t* L_4 = ___collection;
		NullCheck((List_1_t1_1908 *)__this);
		(( void (*) (List_1_t1_1908 *, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((List_1_t1_1908 *)__this, (Object_t*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
	}

IL_0027:
	{
		int32_t L_5 = (int32_t)(__this->____version_3);
		__this->____version_3 = ((int32_t)((int32_t)L_5+(int32_t)1));
		return;
	}
}
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t1_2722 * List_1_AsReadOnly_m1_27134_gshared (List_1_t1_1908 * __this, const MethodInfo* method)
{
	{
		ReadOnlyCollection_1_t1_2722 * L_0 = (ReadOnlyCollection_1_t1_2722 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22));
		(( void (*) (ReadOnlyCollection_1_t1_2722 *, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23)->method)(L_0, (Object_t*)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23));
		return L_0;
	}
}
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::BinarySearch(T)
extern "C" int32_t List_1_BinarySearch_m1_27135_gshared (List_1_t1_1908 * __this, ConsoleLog_t8_170  ___item, const MethodInfo* method)
{
	{
		ConsoleLogU5BU5D_t8_382* L_0 = (ConsoleLogU5BU5D_t8_382*)(__this->____items_1);
		int32_t L_1 = (int32_t)(__this->____size_2);
		ConsoleLog_t8_170  L_2 = ___item;
		int32_t L_3 = (( int32_t (*) (Object_t * /* static, unused */, ConsoleLogU5BU5D_t8_382*, int32_t, int32_t, ConsoleLog_t8_170 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24)->method)(NULL /*static, unused*/, (ConsoleLogU5BU5D_t8_382*)L_0, (int32_t)0, (int32_t)L_1, (ConsoleLog_t8_170 )L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24));
		return L_3;
	}
}
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::BinarySearch(T,System.Collections.Generic.IComparer`1<T>)
extern "C" int32_t List_1_BinarySearch_m1_27136_gshared (List_1_t1_1908 * __this, ConsoleLog_t8_170  ___item, Object_t* ___comparer, const MethodInfo* method)
{
	{
		ConsoleLogU5BU5D_t8_382* L_0 = (ConsoleLogU5BU5D_t8_382*)(__this->____items_1);
		int32_t L_1 = (int32_t)(__this->____size_2);
		ConsoleLog_t8_170  L_2 = ___item;
		Object_t* L_3 = ___comparer;
		int32_t L_4 = (( int32_t (*) (Object_t * /* static, unused */, ConsoleLogU5BU5D_t8_382*, int32_t, int32_t, ConsoleLog_t8_170 , Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25)->method)(NULL /*static, unused*/, (ConsoleLogU5BU5D_t8_382*)L_0, (int32_t)0, (int32_t)L_1, (ConsoleLog_t8_170 )L_2, (Object_t*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25));
		return L_4;
	}
}
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::BinarySearch(System.Int32,System.Int32,T,System.Collections.Generic.IComparer`1<T>)
extern "C" int32_t List_1_BinarySearch_m1_27137_gshared (List_1_t1_1908 * __this, int32_t ___index, int32_t ___count, ConsoleLog_t8_170  ___item, Object_t* ___comparer, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index;
		int32_t L_1 = ___count;
		NullCheck((List_1_t1_1908 *)__this);
		(( void (*) (List_1_t1_1908 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26)->method)((List_1_t1_1908 *)__this, (int32_t)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26));
		ConsoleLogU5BU5D_t8_382* L_2 = (ConsoleLogU5BU5D_t8_382*)(__this->____items_1);
		int32_t L_3 = ___index;
		int32_t L_4 = ___count;
		ConsoleLog_t8_170  L_5 = ___item;
		Object_t* L_6 = ___comparer;
		int32_t L_7 = (( int32_t (*) (Object_t * /* static, unused */, ConsoleLogU5BU5D_t8_382*, int32_t, int32_t, ConsoleLog_t8_170 , Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25)->method)(NULL /*static, unused*/, (ConsoleLogU5BU5D_t8_382*)L_2, (int32_t)L_3, (int32_t)L_4, (ConsoleLog_t8_170 )L_5, (Object_t*)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25));
		return L_7;
	}
}
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Clear()
extern "C" void List_1_Clear_m1_27138_gshared (List_1_t1_1908 * __this, const MethodInfo* method)
{
	{
		ConsoleLogU5BU5D_t8_382* L_0 = (ConsoleLogU5BU5D_t8_382*)(__this->____items_1);
		ConsoleLogU5BU5D_t8_382* L_1 = (ConsoleLogU5BU5D_t8_382*)(__this->____items_1);
		NullCheck(L_1);
		Array_Clear_m1_1037(NULL /*static, unused*/, (Array_t *)(Array_t *)L_0, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Array_t *)L_1)->max_length)))), /*hidden argument*/NULL);
		__this->____size_2 = 0;
		int32_t L_2 = (int32_t)(__this->____version_3);
		__this->____version_3 = ((int32_t)((int32_t)L_2+(int32_t)1));
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Contains(T)
extern "C" bool List_1_Contains_m1_27139_gshared (List_1_t1_1908 * __this, ConsoleLog_t8_170  ___item, const MethodInfo* method)
{
	{
		ConsoleLogU5BU5D_t8_382* L_0 = (ConsoleLogU5BU5D_t8_382*)(__this->____items_1);
		ConsoleLog_t8_170  L_1 = ___item;
		int32_t L_2 = (int32_t)(__this->____size_2);
		int32_t L_3 = (( int32_t (*) (Object_t * /* static, unused */, ConsoleLogU5BU5D_t8_382*, ConsoleLog_t8_170 , int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27)->method)(NULL /*static, unused*/, (ConsoleLogU5BU5D_t8_382*)L_0, (ConsoleLog_t8_170 )L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27));
		return ((((int32_t)((((int32_t)L_3) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::CopyTo(T[])
extern "C" void List_1_CopyTo_m1_27140_gshared (List_1_t1_1908 * __this, ConsoleLogU5BU5D_t8_382* ___array, const MethodInfo* method)
{
	{
		ConsoleLogU5BU5D_t8_382* L_0 = (ConsoleLogU5BU5D_t8_382*)(__this->____items_1);
		ConsoleLogU5BU5D_t8_382* L_1 = ___array;
		int32_t L_2 = (int32_t)(__this->____size_2);
		Array_Copy_m1_1041(NULL /*static, unused*/, (Array_t *)(Array_t *)L_0, (int32_t)0, (Array_t *)(Array_t *)L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m1_27141_gshared (List_1_t1_1908 * __this, ConsoleLogU5BU5D_t8_382* ___array, int32_t ___arrayIndex, const MethodInfo* method)
{
	{
		ConsoleLogU5BU5D_t8_382* L_0 = (ConsoleLogU5BU5D_t8_382*)(__this->____items_1);
		ConsoleLogU5BU5D_t8_382* L_1 = ___array;
		int32_t L_2 = ___arrayIndex;
		int32_t L_3 = (int32_t)(__this->____size_2);
		Array_Copy_m1_1041(NULL /*static, unused*/, (Array_t *)(Array_t *)L_0, (int32_t)0, (Array_t *)(Array_t *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::CopyTo(System.Int32,T[],System.Int32,System.Int32)
extern "C" void List_1_CopyTo_m1_27142_gshared (List_1_t1_1908 * __this, int32_t ___index, ConsoleLogU5BU5D_t8_382* ___array, int32_t ___arrayIndex, int32_t ___count, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index;
		int32_t L_1 = ___count;
		NullCheck((List_1_t1_1908 *)__this);
		(( void (*) (List_1_t1_1908 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26)->method)((List_1_t1_1908 *)__this, (int32_t)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26));
		ConsoleLogU5BU5D_t8_382* L_2 = (ConsoleLogU5BU5D_t8_382*)(__this->____items_1);
		int32_t L_3 = ___index;
		ConsoleLogU5BU5D_t8_382* L_4 = ___array;
		int32_t L_5 = ___arrayIndex;
		int32_t L_6 = ___count;
		Array_Copy_m1_1041(NULL /*static, unused*/, (Array_t *)(Array_t *)L_2, (int32_t)L_3, (Array_t *)(Array_t *)L_4, (int32_t)L_5, (int32_t)L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Exists(System.Predicate`1<T>)
extern "C" bool List_1_Exists_m1_27143_gshared (List_1_t1_1908 * __this, Predicate_1_t1_2729 * ___match, const MethodInfo* method)
{
	{
		Predicate_1_t1_2729 * L_0 = ___match;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		(( void (*) (Object_t * /* static, unused */, Predicate_1_t1_2729 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28)->method)(NULL /*static, unused*/, (Predicate_1_t1_2729 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28));
		int32_t L_1 = (int32_t)(__this->____size_2);
		Predicate_1_t1_2729 * L_2 = ___match;
		NullCheck((List_1_t1_1908 *)__this);
		int32_t L_3 = (( int32_t (*) (List_1_t1_1908 *, int32_t, int32_t, Predicate_1_t1_2729 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29)->method)((List_1_t1_1908 *)__this, (int32_t)0, (int32_t)L_1, (Predicate_1_t1_2729 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29));
		return ((((int32_t)((((int32_t)L_3) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// T System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Find(System.Predicate`1<T>)
extern TypeInfo* ConsoleLog_t8_170_il2cpp_TypeInfo_var;
extern "C" ConsoleLog_t8_170  List_1_Find_m1_27144_gshared (List_1_t1_1908 * __this, Predicate_1_t1_2729 * ___match, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ConsoleLog_t8_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2096);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	ConsoleLog_t8_170  V_1 = {0};
	ConsoleLog_t8_170  G_B3_0 = {0};
	{
		Predicate_1_t1_2729 * L_0 = ___match;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		(( void (*) (Object_t * /* static, unused */, Predicate_1_t1_2729 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28)->method)(NULL /*static, unused*/, (Predicate_1_t1_2729 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28));
		int32_t L_1 = (int32_t)(__this->____size_2);
		Predicate_1_t1_2729 * L_2 = ___match;
		NullCheck((List_1_t1_1908 *)__this);
		int32_t L_3 = (( int32_t (*) (List_1_t1_1908 *, int32_t, int32_t, Predicate_1_t1_2729 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29)->method)((List_1_t1_1908 *)__this, (int32_t)0, (int32_t)L_1, (Predicate_1_t1_2729 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29));
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) == ((int32_t)(-1))))
		{
			goto IL_002d;
		}
	}
	{
		ConsoleLogU5BU5D_t8_382* L_5 = (ConsoleLogU5BU5D_t8_382*)(__this->____items_1);
		int32_t L_6 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		G_B3_0 = (*(ConsoleLog_t8_170 *)(ConsoleLog_t8_170 *)SZArrayLdElema(L_5, L_7, sizeof(ConsoleLog_t8_170 )));
		goto IL_0036;
	}

IL_002d:
	{
		Initobj (ConsoleLog_t8_170_il2cpp_TypeInfo_var, (&V_1));
		ConsoleLog_t8_170  L_8 = V_1;
		G_B3_0 = L_8;
	}

IL_0036:
	{
		return G_B3_0;
	}
}
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::CheckMatch(System.Predicate`1<T>)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral214;
extern "C" void List_1_CheckMatch_m1_27145_gshared (Object_t * __this /* static, unused */, Predicate_1_t1_2729 * ___match, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		_stringLiteral214 = il2cpp_codegen_string_literal_from_index(214);
		s_Il2CppMethodIntialized = true;
	}
	{
		Predicate_1_t1_2729 * L_0 = ___match;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, (String_t*)_stringLiteral214, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		return;
	}
}
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::FindAll(System.Predicate`1<T>)
extern "C" List_1_t1_1908 * List_1_FindAll_m1_27146_gshared (List_1_t1_1908 * __this, Predicate_1_t1_2729 * ___match, const MethodInfo* method)
{
	{
		Predicate_1_t1_2729 * L_0 = ___match;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		(( void (*) (Object_t * /* static, unused */, Predicate_1_t1_2729 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28)->method)(NULL /*static, unused*/, (Predicate_1_t1_2729 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28));
		int32_t L_1 = (int32_t)(__this->____size_2);
		if ((((int32_t)L_1) > ((int32_t)((int32_t)65536))))
		{
			goto IL_001e;
		}
	}
	{
		Predicate_1_t1_2729 * L_2 = ___match;
		NullCheck((List_1_t1_1908 *)__this);
		List_1_t1_1908 * L_3 = (( List_1_t1_1908 * (*) (List_1_t1_1908 *, Predicate_1_t1_2729 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30)->method)((List_1_t1_1908 *)__this, (Predicate_1_t1_2729 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30));
		return L_3;
	}

IL_001e:
	{
		Predicate_1_t1_2729 * L_4 = ___match;
		NullCheck((List_1_t1_1908 *)__this);
		List_1_t1_1908 * L_5 = (( List_1_t1_1908 * (*) (List_1_t1_1908 *, Predicate_1_t1_2729 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 31)->method)((List_1_t1_1908 *)__this, (Predicate_1_t1_2729 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 31));
		return L_5;
	}
}
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::FindAllStackBits(System.Predicate`1<T>)
extern "C" List_1_t1_1908 * List_1_FindAllStackBits_m1_27147_gshared (List_1_t1_1908 * __this, Predicate_1_t1_2729 * ___match, const MethodInfo* method)
{
	uint32_t* V_0 = {0};
	uint32_t* V_1 = {0};
	int32_t V_2 = 0;
	uint32_t V_3 = 0;
	int32_t V_4 = 0;
	ConsoleLogU5BU5D_t8_382* V_5 = {0};
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	{
		int32_t L_0 = (int32_t)(__this->____size_2);
		if ((uint64_t)(uint32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_0/(int32_t)((int32_t)32)))+(int32_t)1)) * (uint64_t)(uint32_t)4 > (uint64_t)(uint32_t)kIl2CppUInt32Max)
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_overflow_exception());
		int8_t* L_1 = (int8_t*) alloca(((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_0/(int32_t)((int32_t)32)))+(int32_t)1))*(int32_t)4)));
		memset(L_1,0,((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_0/(int32_t)((int32_t)32)))+(int32_t)1))*(int32_t)4)));
		V_0 = (uint32_t*)(L_1);
		uint32_t* L_2 = V_0;
		V_1 = (uint32_t*)L_2;
		V_2 = (int32_t)0;
		V_3 = (uint32_t)((int32_t)-2147483648);
		V_4 = (int32_t)0;
		goto IL_005f;
	}

IL_0022:
	{
		Predicate_1_t1_2729 * L_3 = ___match;
		ConsoleLogU5BU5D_t8_382* L_4 = (ConsoleLogU5BU5D_t8_382*)(__this->____items_1);
		int32_t L_5 = V_4;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		NullCheck((Predicate_1_t1_2729 *)L_3);
		bool L_7 = (( bool (*) (Predicate_1_t1_2729 *, ConsoleLog_t8_170 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 32)->method)((Predicate_1_t1_2729 *)L_3, (ConsoleLog_t8_170 )(*(ConsoleLog_t8_170 *)(ConsoleLog_t8_170 *)SZArrayLdElema(L_4, L_6, sizeof(ConsoleLog_t8_170 ))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 32));
		if (!L_7)
		{
			goto IL_0044;
		}
	}
	{
		uint32_t* L_8 = V_1;
		uint32_t* L_9 = V_1;
		uint32_t L_10 = V_3;
		*((int32_t*)(L_8)) = (int32_t)((int32_t)((int32_t)(*((uint32_t*)L_9))|(int32_t)L_10));
		int32_t L_11 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_0044:
	{
		uint32_t L_12 = V_3;
		V_3 = (uint32_t)((int32_t)((uint32_t)L_12>>1));
		uint32_t L_13 = V_3;
		if (L_13)
		{
			goto IL_0059;
		}
	}
	{
		uint32_t* L_14 = V_1;
		V_1 = (uint32_t*)((uint32_t*)((intptr_t)L_14+(intptr_t)(((intptr_t)4))));
		V_3 = (uint32_t)((int32_t)-2147483648);
	}

IL_0059:
	{
		int32_t L_15 = V_4;
		V_4 = (int32_t)((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_005f:
	{
		int32_t L_16 = V_4;
		int32_t L_17 = (int32_t)(__this->____size_2);
		if ((((int32_t)L_16) < ((int32_t)L_17)))
		{
			goto IL_0022;
		}
	}
	{
		int32_t L_18 = V_2;
		V_5 = (ConsoleLogU5BU5D_t8_382*)((ConsoleLogU5BU5D_t8_382*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), L_18));
		V_3 = (uint32_t)((int32_t)-2147483648);
		uint32_t* L_19 = V_0;
		V_1 = (uint32_t*)L_19;
		V_6 = (int32_t)0;
		V_7 = (int32_t)0;
		goto IL_00c7;
	}

IL_0087:
	{
		uint32_t* L_20 = V_1;
		uint32_t L_21 = V_3;
		uint32_t L_22 = V_3;
		if ((!(((uint32_t)((int32_t)((int32_t)(*((uint32_t*)L_20))&(int32_t)L_21))) == ((uint32_t)L_22))))
		{
			goto IL_00ac;
		}
	}
	{
		ConsoleLogU5BU5D_t8_382* L_23 = V_5;
		int32_t L_24 = V_6;
		int32_t L_25 = (int32_t)L_24;
		V_6 = (int32_t)((int32_t)((int32_t)L_25+(int32_t)1));
		ConsoleLogU5BU5D_t8_382* L_26 = (ConsoleLogU5BU5D_t8_382*)(__this->____items_1);
		int32_t L_27 = V_7;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, L_27);
		int32_t L_28 = L_27;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_25);
		*((ConsoleLog_t8_170 *)(ConsoleLog_t8_170 *)SZArrayLdElema(L_23, L_25, sizeof(ConsoleLog_t8_170 ))) = (ConsoleLog_t8_170 )(*(ConsoleLog_t8_170 *)(ConsoleLog_t8_170 *)SZArrayLdElema(L_26, L_28, sizeof(ConsoleLog_t8_170 )));
	}

IL_00ac:
	{
		uint32_t L_29 = V_3;
		V_3 = (uint32_t)((int32_t)((uint32_t)L_29>>1));
		uint32_t L_30 = V_3;
		if (L_30)
		{
			goto IL_00c1;
		}
	}
	{
		uint32_t* L_31 = V_1;
		V_1 = (uint32_t*)((uint32_t*)((intptr_t)L_31+(intptr_t)(((intptr_t)4))));
		V_3 = (uint32_t)((int32_t)-2147483648);
	}

IL_00c1:
	{
		int32_t L_32 = V_7;
		V_7 = (int32_t)((int32_t)((int32_t)L_32+(int32_t)1));
	}

IL_00c7:
	{
		int32_t L_33 = V_7;
		int32_t L_34 = (int32_t)(__this->____size_2);
		if ((((int32_t)L_33) >= ((int32_t)L_34)))
		{
			goto IL_00dc;
		}
	}
	{
		int32_t L_35 = V_6;
		int32_t L_36 = V_2;
		if ((((int32_t)L_35) < ((int32_t)L_36)))
		{
			goto IL_0087;
		}
	}

IL_00dc:
	{
		ConsoleLogU5BU5D_t8_382* L_37 = V_5;
		int32_t L_38 = V_2;
		List_1_t1_1908 * L_39 = (List_1_t1_1908 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 33));
		(( void (*) (List_1_t1_1908 *, ConsoleLogU5BU5D_t8_382*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 34)->method)(L_39, (ConsoleLogU5BU5D_t8_382*)L_37, (int32_t)L_38, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 34));
		return L_39;
	}
}
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::FindAllList(System.Predicate`1<T>)
extern "C" List_1_t1_1908 * List_1_FindAllList_m1_27148_gshared (List_1_t1_1908 * __this, Predicate_1_t1_2729 * ___match, const MethodInfo* method)
{
	List_1_t1_1908 * V_0 = {0};
	int32_t V_1 = 0;
	{
		List_1_t1_1908 * L_0 = (List_1_t1_1908 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 33));
		(( void (*) (List_1_t1_1908 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 35)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 35));
		V_0 = (List_1_t1_1908 *)L_0;
		V_1 = (int32_t)0;
		goto IL_003a;
	}

IL_000d:
	{
		Predicate_1_t1_2729 * L_1 = ___match;
		ConsoleLogU5BU5D_t8_382* L_2 = (ConsoleLogU5BU5D_t8_382*)(__this->____items_1);
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		NullCheck((Predicate_1_t1_2729 *)L_1);
		bool L_5 = (( bool (*) (Predicate_1_t1_2729 *, ConsoleLog_t8_170 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 32)->method)((Predicate_1_t1_2729 *)L_1, (ConsoleLog_t8_170 )(*(ConsoleLog_t8_170 *)(ConsoleLog_t8_170 *)SZArrayLdElema(L_2, L_4, sizeof(ConsoleLog_t8_170 ))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 32));
		if (!L_5)
		{
			goto IL_0036;
		}
	}
	{
		List_1_t1_1908 * L_6 = V_0;
		ConsoleLogU5BU5D_t8_382* L_7 = (ConsoleLogU5BU5D_t8_382*)(__this->____items_1);
		int32_t L_8 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		NullCheck((List_1_t1_1908 *)L_6);
		VirtActionInvoker1< ConsoleLog_t8_170  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Add(T) */, (List_1_t1_1908 *)L_6, (ConsoleLog_t8_170 )(*(ConsoleLog_t8_170 *)(ConsoleLog_t8_170 *)SZArrayLdElema(L_7, L_9, sizeof(ConsoleLog_t8_170 ))));
	}

IL_0036:
	{
		int32_t L_10 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_003a:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = (int32_t)(__this->____size_2);
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_000d;
		}
	}
	{
		List_1_t1_1908 * L_13 = V_0;
		return L_13;
	}
}
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::FindIndex(System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m1_27149_gshared (List_1_t1_1908 * __this, Predicate_1_t1_2729 * ___match, const MethodInfo* method)
{
	{
		Predicate_1_t1_2729 * L_0 = ___match;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		(( void (*) (Object_t * /* static, unused */, Predicate_1_t1_2729 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28)->method)(NULL /*static, unused*/, (Predicate_1_t1_2729 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28));
		int32_t L_1 = (int32_t)(__this->____size_2);
		Predicate_1_t1_2729 * L_2 = ___match;
		NullCheck((List_1_t1_1908 *)__this);
		int32_t L_3 = (( int32_t (*) (List_1_t1_1908 *, int32_t, int32_t, Predicate_1_t1_2729 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29)->method)((List_1_t1_1908 *)__this, (int32_t)0, (int32_t)L_1, (Predicate_1_t1_2729 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29));
		return L_3;
	}
}
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::FindIndex(System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m1_27150_gshared (List_1_t1_1908 * __this, int32_t ___startIndex, Predicate_1_t1_2729 * ___match, const MethodInfo* method)
{
	{
		Predicate_1_t1_2729 * L_0 = ___match;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		(( void (*) (Object_t * /* static, unused */, Predicate_1_t1_2729 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28)->method)(NULL /*static, unused*/, (Predicate_1_t1_2729 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28));
		int32_t L_1 = ___startIndex;
		NullCheck((List_1_t1_1908 *)__this);
		(( void (*) (List_1_t1_1908 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((List_1_t1_1908 *)__this, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		int32_t L_2 = ___startIndex;
		int32_t L_3 = (int32_t)(__this->____size_2);
		int32_t L_4 = ___startIndex;
		Predicate_1_t1_2729 * L_5 = ___match;
		NullCheck((List_1_t1_1908 *)__this);
		int32_t L_6 = (( int32_t (*) (List_1_t1_1908 *, int32_t, int32_t, Predicate_1_t1_2729 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29)->method)((List_1_t1_1908 *)__this, (int32_t)L_2, (int32_t)((int32_t)((int32_t)L_3-(int32_t)L_4)), (Predicate_1_t1_2729 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29));
		return L_6;
	}
}
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::FindIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m1_27151_gshared (List_1_t1_1908 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_2729 * ___match, const MethodInfo* method)
{
	{
		Predicate_1_t1_2729 * L_0 = ___match;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		(( void (*) (Object_t * /* static, unused */, Predicate_1_t1_2729 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28)->method)(NULL /*static, unused*/, (Predicate_1_t1_2729 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28));
		int32_t L_1 = ___startIndex;
		int32_t L_2 = ___count;
		NullCheck((List_1_t1_1908 *)__this);
		(( void (*) (List_1_t1_1908 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26)->method)((List_1_t1_1908 *)__this, (int32_t)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26));
		int32_t L_3 = ___startIndex;
		int32_t L_4 = ___count;
		Predicate_1_t1_2729 * L_5 = ___match;
		NullCheck((List_1_t1_1908 *)__this);
		int32_t L_6 = (( int32_t (*) (List_1_t1_1908 *, int32_t, int32_t, Predicate_1_t1_2729 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29)->method)((List_1_t1_1908 *)__this, (int32_t)L_3, (int32_t)L_4, (Predicate_1_t1_2729 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29));
		return L_6;
	}
}
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m1_27152_gshared (List_1_t1_1908 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_2729 * ___match, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___startIndex;
		int32_t L_1 = ___count;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = ___startIndex;
		V_1 = (int32_t)L_2;
		goto IL_0028;
	}

IL_000b:
	{
		Predicate_1_t1_2729 * L_3 = ___match;
		ConsoleLogU5BU5D_t8_382* L_4 = (ConsoleLogU5BU5D_t8_382*)(__this->____items_1);
		int32_t L_5 = V_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		NullCheck((Predicate_1_t1_2729 *)L_3);
		bool L_7 = (( bool (*) (Predicate_1_t1_2729 *, ConsoleLog_t8_170 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 32)->method)((Predicate_1_t1_2729 *)L_3, (ConsoleLog_t8_170 )(*(ConsoleLog_t8_170 *)(ConsoleLog_t8_170 *)SZArrayLdElema(L_4, L_6, sizeof(ConsoleLog_t8_170 ))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 32));
		if (!L_7)
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_8 = V_1;
		return L_8;
	}

IL_0024:
	{
		int32_t L_9 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0028:
	{
		int32_t L_10 = V_1;
		int32_t L_11 = V_0;
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_000b;
		}
	}
	{
		return (-1);
	}
}
// T System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::FindLast(System.Predicate`1<T>)
extern TypeInfo* ConsoleLog_t8_170_il2cpp_TypeInfo_var;
extern "C" ConsoleLog_t8_170  List_1_FindLast_m1_27153_gshared (List_1_t1_1908 * __this, Predicate_1_t1_2729 * ___match, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ConsoleLog_t8_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2096);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	ConsoleLog_t8_170  V_1 = {0};
	ConsoleLog_t8_170  G_B3_0 = {0};
	{
		Predicate_1_t1_2729 * L_0 = ___match;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		(( void (*) (Object_t * /* static, unused */, Predicate_1_t1_2729 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28)->method)(NULL /*static, unused*/, (Predicate_1_t1_2729 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28));
		int32_t L_1 = (int32_t)(__this->____size_2);
		Predicate_1_t1_2729 * L_2 = ___match;
		NullCheck((List_1_t1_1908 *)__this);
		int32_t L_3 = (( int32_t (*) (List_1_t1_1908 *, int32_t, int32_t, Predicate_1_t1_2729 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 36)->method)((List_1_t1_1908 *)__this, (int32_t)0, (int32_t)L_1, (Predicate_1_t1_2729 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 36));
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		if ((!(((uint32_t)L_4) == ((uint32_t)(-1)))))
		{
			goto IL_002a;
		}
	}
	{
		Initobj (ConsoleLog_t8_170_il2cpp_TypeInfo_var, (&V_1));
		ConsoleLog_t8_170  L_5 = V_1;
		G_B3_0 = L_5;
		goto IL_0031;
	}

IL_002a:
	{
		int32_t L_6 = V_0;
		NullCheck((List_1_t1_1908 *)__this);
		ConsoleLog_t8_170  L_7 = (ConsoleLog_t8_170 )VirtFuncInvoker1< ConsoleLog_t8_170 , int32_t >::Invoke(31 /* T System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::get_Item(System.Int32) */, (List_1_t1_1908 *)__this, (int32_t)L_6);
		G_B3_0 = L_7;
	}

IL_0031:
	{
		return G_B3_0;
	}
}
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::FindLastIndex(System.Predicate`1<T>)
extern "C" int32_t List_1_FindLastIndex_m1_27154_gshared (List_1_t1_1908 * __this, Predicate_1_t1_2729 * ___match, const MethodInfo* method)
{
	{
		Predicate_1_t1_2729 * L_0 = ___match;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		(( void (*) (Object_t * /* static, unused */, Predicate_1_t1_2729 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28)->method)(NULL /*static, unused*/, (Predicate_1_t1_2729 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28));
		int32_t L_1 = (int32_t)(__this->____size_2);
		Predicate_1_t1_2729 * L_2 = ___match;
		NullCheck((List_1_t1_1908 *)__this);
		int32_t L_3 = (( int32_t (*) (List_1_t1_1908 *, int32_t, int32_t, Predicate_1_t1_2729 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 36)->method)((List_1_t1_1908 *)__this, (int32_t)0, (int32_t)L_1, (Predicate_1_t1_2729 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 36));
		return L_3;
	}
}
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::FindLastIndex(System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindLastIndex_m1_27155_gshared (List_1_t1_1908 * __this, int32_t ___startIndex, Predicate_1_t1_2729 * ___match, const MethodInfo* method)
{
	{
		Predicate_1_t1_2729 * L_0 = ___match;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		(( void (*) (Object_t * /* static, unused */, Predicate_1_t1_2729 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28)->method)(NULL /*static, unused*/, (Predicate_1_t1_2729 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28));
		int32_t L_1 = ___startIndex;
		NullCheck((List_1_t1_1908 *)__this);
		(( void (*) (List_1_t1_1908 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((List_1_t1_1908 *)__this, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		int32_t L_2 = ___startIndex;
		Predicate_1_t1_2729 * L_3 = ___match;
		NullCheck((List_1_t1_1908 *)__this);
		int32_t L_4 = (( int32_t (*) (List_1_t1_1908 *, int32_t, int32_t, Predicate_1_t1_2729 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 36)->method)((List_1_t1_1908 *)__this, (int32_t)0, (int32_t)((int32_t)((int32_t)L_2+(int32_t)1)), (Predicate_1_t1_2729 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 36));
		return L_4;
	}
}
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::FindLastIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindLastIndex_m1_27156_gshared (List_1_t1_1908 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_2729 * ___match, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Predicate_1_t1_2729 * L_0 = ___match;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		(( void (*) (Object_t * /* static, unused */, Predicate_1_t1_2729 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28)->method)(NULL /*static, unused*/, (Predicate_1_t1_2729 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28));
		int32_t L_1 = ___startIndex;
		int32_t L_2 = ___count;
		V_0 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1-(int32_t)L_2))+(int32_t)1));
		int32_t L_3 = V_0;
		int32_t L_4 = ___count;
		NullCheck((List_1_t1_1908 *)__this);
		(( void (*) (List_1_t1_1908 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26)->method)((List_1_t1_1908 *)__this, (int32_t)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26));
		int32_t L_5 = V_0;
		int32_t L_6 = ___count;
		Predicate_1_t1_2729 * L_7 = ___match;
		NullCheck((List_1_t1_1908 *)__this);
		int32_t L_8 = (( int32_t (*) (List_1_t1_1908 *, int32_t, int32_t, Predicate_1_t1_2729 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 36)->method)((List_1_t1_1908 *)__this, (int32_t)L_5, (int32_t)L_6, (Predicate_1_t1_2729 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 36));
		return L_8;
	}
}
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::GetLastIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetLastIndex_m1_27157_gshared (List_1_t1_1908 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_2729 * ___match, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___startIndex;
		int32_t L_1 = ___count;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		goto IL_0026;
	}

IL_0009:
	{
		Predicate_1_t1_2729 * L_2 = ___match;
		ConsoleLogU5BU5D_t8_382* L_3 = (ConsoleLogU5BU5D_t8_382*)(__this->____items_1);
		int32_t L_4 = V_0;
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_5);
		int32_t L_6 = L_5;
		NullCheck((Predicate_1_t1_2729 *)L_2);
		bool L_7 = (( bool (*) (Predicate_1_t1_2729 *, ConsoleLog_t8_170 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 32)->method)((Predicate_1_t1_2729 *)L_2, (ConsoleLog_t8_170 )(*(ConsoleLog_t8_170 *)(ConsoleLog_t8_170 *)SZArrayLdElema(L_3, L_6, sizeof(ConsoleLog_t8_170 ))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 32));
		if (!L_7)
		{
			goto IL_0026;
		}
	}
	{
		int32_t L_8 = V_0;
		return L_8;
	}

IL_0026:
	{
		int32_t L_9 = V_0;
		int32_t L_10 = ___startIndex;
		if ((!(((uint32_t)L_9) == ((uint32_t)L_10))))
		{
			goto IL_0009;
		}
	}
	{
		return (-1);
	}
}
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::ForEach(System.Action`1<T>)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral215;
extern "C" void List_1_ForEach_m1_27158_gshared (List_1_t1_1908 * __this, Action_1_t1_2730 * ___action, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		_stringLiteral215 = il2cpp_codegen_string_literal_from_index(215);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		Action_1_t1_2730 * L_0 = ___action;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, (String_t*)_stringLiteral215, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		V_0 = (int32_t)0;
		goto IL_002e;
	}

IL_0018:
	{
		Action_1_t1_2730 * L_2 = ___action;
		ConsoleLogU5BU5D_t8_382* L_3 = (ConsoleLogU5BU5D_t8_382*)(__this->____items_1);
		int32_t L_4 = V_0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		NullCheck((Action_1_t1_2730 *)L_2);
		(( void (*) (Action_1_t1_2730 *, ConsoleLog_t8_170 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 37)->method)((Action_1_t1_2730 *)L_2, (ConsoleLog_t8_170 )(*(ConsoleLog_t8_170 *)(ConsoleLog_t8_170 *)SZArrayLdElema(L_3, L_5, sizeof(ConsoleLog_t8_170 ))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 37));
		int32_t L_6 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_002e:
	{
		int32_t L_7 = V_0;
		int32_t L_8 = (int32_t)(__this->____size_2);
		if ((((int32_t)L_7) < ((int32_t)L_8)))
		{
			goto IL_0018;
		}
	}
	{
		return;
	}
}
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::GetEnumerator()
extern "C" Enumerator_t1_2721  List_1_GetEnumerator_m1_27159_gshared (List_1_t1_1908 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1_2721  L_0 = {0};
		(( void (*) (Enumerator_t1_2721 *, List_1_t1_1908 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 38)->method)(&L_0, (List_1_t1_1908 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 38));
		return L_0;
	}
}
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::GetRange(System.Int32,System.Int32)
extern "C" List_1_t1_1908 * List_1_GetRange_m1_27160_gshared (List_1_t1_1908 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method)
{
	ConsoleLogU5BU5D_t8_382* V_0 = {0};
	{
		int32_t L_0 = ___index;
		int32_t L_1 = ___count;
		NullCheck((List_1_t1_1908 *)__this);
		(( void (*) (List_1_t1_1908 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26)->method)((List_1_t1_1908 *)__this, (int32_t)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26));
		int32_t L_2 = ___count;
		V_0 = (ConsoleLogU5BU5D_t8_382*)((ConsoleLogU5BU5D_t8_382*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), L_2));
		ConsoleLogU5BU5D_t8_382* L_3 = (ConsoleLogU5BU5D_t8_382*)(__this->____items_1);
		int32_t L_4 = ___index;
		ConsoleLogU5BU5D_t8_382* L_5 = V_0;
		int32_t L_6 = ___count;
		Array_Copy_m1_1041(NULL /*static, unused*/, (Array_t *)(Array_t *)L_3, (int32_t)L_4, (Array_t *)(Array_t *)L_5, (int32_t)0, (int32_t)L_6, /*hidden argument*/NULL);
		ConsoleLogU5BU5D_t8_382* L_7 = V_0;
		int32_t L_8 = ___count;
		List_1_t1_1908 * L_9 = (List_1_t1_1908 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 33));
		(( void (*) (List_1_t1_1908 *, ConsoleLogU5BU5D_t8_382*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 34)->method)(L_9, (ConsoleLogU5BU5D_t8_382*)L_7, (int32_t)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 34));
		return L_9;
	}
}
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m1_27161_gshared (List_1_t1_1908 * __this, ConsoleLog_t8_170  ___item, const MethodInfo* method)
{
	{
		ConsoleLogU5BU5D_t8_382* L_0 = (ConsoleLogU5BU5D_t8_382*)(__this->____items_1);
		ConsoleLog_t8_170  L_1 = ___item;
		int32_t L_2 = (int32_t)(__this->____size_2);
		int32_t L_3 = (( int32_t (*) (Object_t * /* static, unused */, ConsoleLogU5BU5D_t8_382*, ConsoleLog_t8_170 , int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27)->method)(NULL /*static, unused*/, (ConsoleLogU5BU5D_t8_382*)L_0, (ConsoleLog_t8_170 )L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27));
		return L_3;
	}
}
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::IndexOf(T,System.Int32)
extern "C" int32_t List_1_IndexOf_m1_27162_gshared (List_1_t1_1908 * __this, ConsoleLog_t8_170  ___item, int32_t ___index, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index;
		NullCheck((List_1_t1_1908 *)__this);
		(( void (*) (List_1_t1_1908 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((List_1_t1_1908 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		ConsoleLogU5BU5D_t8_382* L_1 = (ConsoleLogU5BU5D_t8_382*)(__this->____items_1);
		ConsoleLog_t8_170  L_2 = ___item;
		int32_t L_3 = ___index;
		int32_t L_4 = (int32_t)(__this->____size_2);
		int32_t L_5 = ___index;
		int32_t L_6 = (( int32_t (*) (Object_t * /* static, unused */, ConsoleLogU5BU5D_t8_382*, ConsoleLog_t8_170 , int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27)->method)(NULL /*static, unused*/, (ConsoleLogU5BU5D_t8_382*)L_1, (ConsoleLog_t8_170 )L_2, (int32_t)L_3, (int32_t)((int32_t)((int32_t)L_4-(int32_t)L_5)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27));
		return L_6;
	}
}
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::IndexOf(T,System.Int32,System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral170;
extern Il2CppCodeGenString* _stringLiteral47;
extern Il2CppCodeGenString* _stringLiteral809;
extern "C" int32_t List_1_IndexOf_m1_27163_gshared (List_1_t1_1908 * __this, ConsoleLog_t8_170  ___item, int32_t ___index, int32_t ___count, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		_stringLiteral170 = il2cpp_codegen_string_literal_from_index(170);
		_stringLiteral47 = il2cpp_codegen_string_literal_from_index(47);
		_stringLiteral809 = il2cpp_codegen_string_literal_from_index(809);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0012;
		}
	}
	{
		ArgumentOutOfRangeException_t1_1501 * L_1 = (ArgumentOutOfRangeException_t1_1501 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1_13274(L_1, (String_t*)_stringLiteral170, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0012:
	{
		int32_t L_2 = ___count;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0024;
		}
	}
	{
		ArgumentOutOfRangeException_t1_1501 * L_3 = (ArgumentOutOfRangeException_t1_1501 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1_13274(L_3, (String_t*)_stringLiteral47, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0024:
	{
		int32_t L_4 = ___index;
		int32_t L_5 = ___count;
		int32_t L_6 = (int32_t)(__this->____size_2);
		if ((!(((uint32_t)((int32_t)((int32_t)L_4+(int32_t)L_5))) > ((uint32_t)L_6))))
		{
			goto IL_003d;
		}
	}
	{
		ArgumentOutOfRangeException_t1_1501 * L_7 = (ArgumentOutOfRangeException_t1_1501 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1_13274(L_7, (String_t*)_stringLiteral809, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_7);
	}

IL_003d:
	{
		ConsoleLogU5BU5D_t8_382* L_8 = (ConsoleLogU5BU5D_t8_382*)(__this->____items_1);
		ConsoleLog_t8_170  L_9 = ___item;
		int32_t L_10 = ___index;
		int32_t L_11 = ___count;
		int32_t L_12 = (( int32_t (*) (Object_t * /* static, unused */, ConsoleLogU5BU5D_t8_382*, ConsoleLog_t8_170 , int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27)->method)(NULL /*static, unused*/, (ConsoleLogU5BU5D_t8_382*)L_8, (ConsoleLog_t8_170 )L_9, (int32_t)L_10, (int32_t)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27));
		return L_12;
	}
}
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m1_27164_gshared (List_1_t1_1908 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method)
{
	{
		int32_t L_0 = ___delta;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_000c;
		}
	}
	{
		int32_t L_1 = ___start;
		int32_t L_2 = ___delta;
		___start = (int32_t)((int32_t)((int32_t)L_1-(int32_t)L_2));
	}

IL_000c:
	{
		int32_t L_3 = ___start;
		int32_t L_4 = (int32_t)(__this->____size_2);
		if ((((int32_t)L_3) >= ((int32_t)L_4)))
		{
			goto IL_0035;
		}
	}
	{
		ConsoleLogU5BU5D_t8_382* L_5 = (ConsoleLogU5BU5D_t8_382*)(__this->____items_1);
		int32_t L_6 = ___start;
		ConsoleLogU5BU5D_t8_382* L_7 = (ConsoleLogU5BU5D_t8_382*)(__this->____items_1);
		int32_t L_8 = ___start;
		int32_t L_9 = ___delta;
		int32_t L_10 = (int32_t)(__this->____size_2);
		int32_t L_11 = ___start;
		Array_Copy_m1_1041(NULL /*static, unused*/, (Array_t *)(Array_t *)L_5, (int32_t)L_6, (Array_t *)(Array_t *)L_7, (int32_t)((int32_t)((int32_t)L_8+(int32_t)L_9)), (int32_t)((int32_t)((int32_t)L_10-(int32_t)L_11)), /*hidden argument*/NULL);
	}

IL_0035:
	{
		int32_t L_12 = (int32_t)(__this->____size_2);
		int32_t L_13 = ___delta;
		__this->____size_2 = ((int32_t)((int32_t)L_12+(int32_t)L_13));
		int32_t L_14 = ___delta;
		if ((((int32_t)L_14) >= ((int32_t)0)))
		{
			goto IL_005d;
		}
	}
	{
		ConsoleLogU5BU5D_t8_382* L_15 = (ConsoleLogU5BU5D_t8_382*)(__this->____items_1);
		int32_t L_16 = (int32_t)(__this->____size_2);
		int32_t L_17 = ___delta;
		Array_Clear_m1_1037(NULL /*static, unused*/, (Array_t *)(Array_t *)L_15, (int32_t)L_16, (int32_t)((-L_17)), /*hidden argument*/NULL);
	}

IL_005d:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::CheckIndex(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral170;
extern "C" void List_1_CheckIndex_m1_27165_gshared (List_1_t1_1908 * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		_stringLiteral170 = il2cpp_codegen_string_literal_from_index(170);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index;
		int32_t L_2 = (int32_t)(__this->____size_2);
		if ((!(((uint32_t)L_1) > ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}

IL_0013:
	{
		ArgumentOutOfRangeException_t1_1501 * L_3 = (ArgumentOutOfRangeException_t1_1501 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1_13274(L_3, (String_t*)_stringLiteral170, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_001e:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m1_27166_gshared (List_1_t1_1908 * __this, int32_t ___index, ConsoleLog_t8_170  ___item, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index;
		NullCheck((List_1_t1_1908 *)__this);
		(( void (*) (List_1_t1_1908 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((List_1_t1_1908 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		int32_t L_1 = (int32_t)(__this->____size_2);
		ConsoleLogU5BU5D_t8_382* L_2 = (ConsoleLogU5BU5D_t8_382*)(__this->____items_1);
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Array_t *)L_2)->max_length))))))))
		{
			goto IL_0021;
		}
	}
	{
		NullCheck((List_1_t1_1908 *)__this);
		(( void (*) (List_1_t1_1908 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((List_1_t1_1908 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
	}

IL_0021:
	{
		int32_t L_3 = ___index;
		NullCheck((List_1_t1_1908 *)__this);
		(( void (*) (List_1_t1_1908 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39)->method)((List_1_t1_1908 *)__this, (int32_t)L_3, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39));
		ConsoleLogU5BU5D_t8_382* L_4 = (ConsoleLogU5BU5D_t8_382*)(__this->____items_1);
		int32_t L_5 = ___index;
		ConsoleLog_t8_170  L_6 = ___item;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		*((ConsoleLog_t8_170 *)(ConsoleLog_t8_170 *)SZArrayLdElema(L_4, L_5, sizeof(ConsoleLog_t8_170 ))) = (ConsoleLog_t8_170 )L_6;
		int32_t L_7 = (int32_t)(__this->____version_3);
		__this->____version_3 = ((int32_t)((int32_t)L_7+(int32_t)1));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral629;
extern "C" void List_1_CheckCollection_m1_27167_gshared (List_1_t1_1908 * __this, Object_t* ___collection, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		_stringLiteral629 = il2cpp_codegen_string_literal_from_index(629);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t* L_0 = ___collection;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, (String_t*)_stringLiteral629, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::InsertRange(System.Int32,System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_InsertRange_m1_27168_gshared (List_1_t1_1908 * __this, int32_t ___index, Object_t* ___collection, const MethodInfo* method)
{
	ConsoleLogU5BU5D_t8_382* V_0 = {0};
	Object_t* V_1 = {0};
	{
		Object_t* L_0 = ___collection;
		NullCheck((List_1_t1_1908 *)__this);
		(( void (*) (List_1_t1_1908 *, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((List_1_t1_1908 *)__this, (Object_t*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		int32_t L_1 = ___index;
		NullCheck((List_1_t1_1908 *)__this);
		(( void (*) (List_1_t1_1908 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((List_1_t1_1908 *)__this, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		Object_t* L_2 = ___collection;
		if ((!(((Object_t*)(Object_t*)L_2) == ((Object_t*)(List_1_t1_1908 *)__this))))
		{
			goto IL_0055;
		}
	}
	{
		int32_t L_3 = (int32_t)(__this->____size_2);
		V_0 = (ConsoleLogU5BU5D_t8_382*)((ConsoleLogU5BU5D_t8_382*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), L_3));
		ConsoleLogU5BU5D_t8_382* L_4 = V_0;
		NullCheck((List_1_t1_1908 *)__this);
		VirtActionInvoker2< ConsoleLogU5BU5D_t8_382*, int32_t >::Invoke(25 /* System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::CopyTo(T[],System.Int32) */, (List_1_t1_1908 *)__this, (ConsoleLogU5BU5D_t8_382*)L_4, (int32_t)0);
		int32_t L_5 = (int32_t)(__this->____size_2);
		NullCheck((List_1_t1_1908 *)__this);
		(( void (*) (List_1_t1_1908 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((List_1_t1_1908 *)__this, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		int32_t L_6 = ___index;
		ConsoleLogU5BU5D_t8_382* L_7 = V_0;
		NullCheck(L_7);
		NullCheck((List_1_t1_1908 *)__this);
		(( void (*) (List_1_t1_1908 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39)->method)((List_1_t1_1908 *)__this, (int32_t)L_6, (int32_t)(((int32_t)((int32_t)(((Array_t *)L_7)->max_length)))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39));
		ConsoleLogU5BU5D_t8_382* L_8 = V_0;
		ConsoleLogU5BU5D_t8_382* L_9 = (ConsoleLogU5BU5D_t8_382*)(__this->____items_1);
		int32_t L_10 = ___index;
		ConsoleLogU5BU5D_t8_382* L_11 = V_0;
		NullCheck(L_11);
		Array_Copy_m1_1041(NULL /*static, unused*/, (Array_t *)(Array_t *)L_8, (int32_t)0, (Array_t *)(Array_t *)L_9, (int32_t)L_10, (int32_t)(((int32_t)((int32_t)(((Array_t *)L_11)->max_length)))), /*hidden argument*/NULL);
		goto IL_0077;
	}

IL_0055:
	{
		Object_t* L_12 = ___collection;
		V_1 = (Object_t*)((Object_t*)IsInst(L_12, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
		Object_t* L_13 = V_1;
		if (!L_13)
		{
			goto IL_006f;
		}
	}
	{
		int32_t L_14 = ___index;
		Object_t* L_15 = V_1;
		NullCheck((List_1_t1_1908 *)__this);
		(( void (*) (List_1_t1_1908 *, int32_t, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 41)->method)((List_1_t1_1908 *)__this, (int32_t)L_14, (Object_t*)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 41));
		goto IL_0077;
	}

IL_006f:
	{
		int32_t L_16 = ___index;
		Object_t* L_17 = ___collection;
		NullCheck((List_1_t1_1908 *)__this);
		(( void (*) (List_1_t1_1908 *, int32_t, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 42)->method)((List_1_t1_1908 *)__this, (int32_t)L_16, (Object_t*)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 42));
	}

IL_0077:
	{
		int32_t L_18 = (int32_t)(__this->____version_3);
		__this->____version_3 = ((int32_t)((int32_t)L_18+(int32_t)1));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::InsertCollection(System.Int32,System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_InsertCollection_m1_27169_gshared (List_1_t1_1908 * __this, int32_t ___index, Object_t* ___collection, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Object_t* L_0 = ___collection;
		NullCheck((Object_t*)L_0);
		int32_t L_1 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::get_Count() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Object_t*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		NullCheck((List_1_t1_1908 *)__this);
		(( void (*) (List_1_t1_1908 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((List_1_t1_1908 *)__this, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		int32_t L_3 = ___index;
		int32_t L_4 = V_0;
		NullCheck((List_1_t1_1908 *)__this);
		(( void (*) (List_1_t1_1908 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39)->method)((List_1_t1_1908 *)__this, (int32_t)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39));
		Object_t* L_5 = ___collection;
		ConsoleLogU5BU5D_t8_382* L_6 = (ConsoleLogU5BU5D_t8_382*)(__this->____items_1);
		int32_t L_7 = ___index;
		NullCheck((Object_t*)L_5);
		InterfaceActionInvoker2< ConsoleLogU5BU5D_t8_382*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::CopyTo(T[],System.Int32) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Object_t*)L_5, (ConsoleLogU5BU5D_t8_382*)L_6, (int32_t)L_7);
		return;
	}
}
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::InsertEnumeration(System.Int32,System.Collections.Generic.IEnumerable`1<T>)
extern TypeInfo* IEnumerator_t1_193_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern "C" void List_1_InsertEnumeration_m1_27170_gshared (List_1_t1_1908 * __this, int32_t ___index, Object_t* ___enumerable, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IEnumerator_t1_193_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(144);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		s_Il2CppMethodIntialized = true;
	}
	ConsoleLog_t8_170  V_0 = {0};
	Object_t* V_1 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Object_t* L_0 = ___enumerable;
		NullCheck((Object_t*)L_0);
		Object_t* L_1 = (Object_t*)InterfaceFuncInvoker0< Object_t* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20), (Object_t*)L_0);
		V_1 = (Object_t*)L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0020;
		}

IL_000c:
		{
			Object_t* L_2 = V_1;
			NullCheck((Object_t*)L_2);
			ConsoleLog_t8_170  L_3 = (ConsoleLog_t8_170 )InterfaceFuncInvoker0< ConsoleLog_t8_170  >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 21), (Object_t*)L_2);
			V_0 = (ConsoleLog_t8_170 )L_3;
			int32_t L_4 = ___index;
			int32_t L_5 = (int32_t)L_4;
			___index = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
			ConsoleLog_t8_170  L_6 = V_0;
			NullCheck((List_1_t1_1908 *)__this);
			VirtActionInvoker2< int32_t, ConsoleLog_t8_170  >::Invoke(29 /* System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Insert(System.Int32,T) */, (List_1_t1_1908 *)__this, (int32_t)L_5, (ConsoleLog_t8_170 )L_6);
		}

IL_0020:
		{
			Object_t* L_7 = V_1;
			NullCheck((Object_t *)L_7);
			bool L_8 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, (Object_t *)L_7);
			if (L_8)
			{
				goto IL_000c;
			}
		}

IL_002b:
		{
			IL2CPP_LEAVE(0x3B, FINALLY_0030);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0030;
	}

FINALLY_0030:
	{ // begin finally (depth: 1)
		{
			Object_t* L_9 = V_1;
			if (L_9)
			{
				goto IL_0034;
			}
		}

IL_0033:
		{
			IL2CPP_END_FINALLY(48)
		}

IL_0034:
		{
			Object_t* L_10 = V_1;
			NullCheck((Object_t *)L_10);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, (Object_t *)L_10);
			IL2CPP_END_FINALLY(48)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(48)
	{
		IL2CPP_JUMP_TBL(0x3B, IL_003b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_003b:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::LastIndexOf(T)
extern "C" int32_t List_1_LastIndexOf_m1_27171_gshared (List_1_t1_1908 * __this, ConsoleLog_t8_170  ___item, const MethodInfo* method)
{
	{
		ConsoleLogU5BU5D_t8_382* L_0 = (ConsoleLogU5BU5D_t8_382*)(__this->____items_1);
		ConsoleLog_t8_170  L_1 = ___item;
		int32_t L_2 = (int32_t)(__this->____size_2);
		int32_t L_3 = (int32_t)(__this->____size_2);
		int32_t L_4 = (( int32_t (*) (Object_t * /* static, unused */, ConsoleLogU5BU5D_t8_382*, ConsoleLog_t8_170 , int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 43)->method)(NULL /*static, unused*/, (ConsoleLogU5BU5D_t8_382*)L_0, (ConsoleLog_t8_170 )L_1, (int32_t)((int32_t)((int32_t)L_2-(int32_t)1)), (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 43));
		return L_4;
	}
}
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::LastIndexOf(T,System.Int32)
extern "C" int32_t List_1_LastIndexOf_m1_27172_gshared (List_1_t1_1908 * __this, ConsoleLog_t8_170  ___item, int32_t ___index, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index;
		NullCheck((List_1_t1_1908 *)__this);
		(( void (*) (List_1_t1_1908 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((List_1_t1_1908 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		ConsoleLogU5BU5D_t8_382* L_1 = (ConsoleLogU5BU5D_t8_382*)(__this->____items_1);
		ConsoleLog_t8_170  L_2 = ___item;
		int32_t L_3 = ___index;
		int32_t L_4 = ___index;
		int32_t L_5 = (( int32_t (*) (Object_t * /* static, unused */, ConsoleLogU5BU5D_t8_382*, ConsoleLog_t8_170 , int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 43)->method)(NULL /*static, unused*/, (ConsoleLogU5BU5D_t8_382*)L_1, (ConsoleLog_t8_170 )L_2, (int32_t)L_3, (int32_t)((int32_t)((int32_t)L_4+(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 43));
		return L_5;
	}
}
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::LastIndexOf(T,System.Int32,System.Int32)
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral170;
extern Il2CppCodeGenString* _stringLiteral810;
extern Il2CppCodeGenString* _stringLiteral47;
extern Il2CppCodeGenString* _stringLiteral811;
extern Il2CppCodeGenString* _stringLiteral812;
extern Il2CppCodeGenString* _stringLiteral813;
extern "C" int32_t List_1_LastIndexOf_m1_27173_gshared (List_1_t1_1908 * __this, ConsoleLog_t8_170  ___item, int32_t ___index, int32_t ___count, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		_stringLiteral170 = il2cpp_codegen_string_literal_from_index(170);
		_stringLiteral810 = il2cpp_codegen_string_literal_from_index(810);
		_stringLiteral47 = il2cpp_codegen_string_literal_from_index(47);
		_stringLiteral811 = il2cpp_codegen_string_literal_from_index(811);
		_stringLiteral812 = il2cpp_codegen_string_literal_from_index(812);
		_stringLiteral813 = il2cpp_codegen_string_literal_from_index(813);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		int32_t L_1 = ___index;
		int32_t L_2 = L_1;
		Object_t * L_3 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_2);
		ArgumentOutOfRangeException_t1_1501 * L_4 = (ArgumentOutOfRangeException_t1_1501 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1_13276(L_4, (String_t*)_stringLiteral170, (Object_t *)L_3, (String_t*)_stringLiteral810, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
	}

IL_001d:
	{
		int32_t L_5 = ___count;
		if ((((int32_t)L_5) >= ((int32_t)0)))
		{
			goto IL_003a;
		}
	}
	{
		int32_t L_6 = ___count;
		int32_t L_7 = L_6;
		Object_t * L_8 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_7);
		ArgumentOutOfRangeException_t1_1501 * L_9 = (ArgumentOutOfRangeException_t1_1501 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1_13276(L_9, (String_t*)_stringLiteral47, (Object_t *)L_8, (String_t*)_stringLiteral811, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_9);
	}

IL_003a:
	{
		int32_t L_10 = ___index;
		int32_t L_11 = ___count;
		if ((((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_10-(int32_t)L_11))+(int32_t)1))) >= ((int32_t)0)))
		{
			goto IL_005b;
		}
	}
	{
		int32_t L_12 = ___count;
		int32_t L_13 = L_12;
		Object_t * L_14 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_13);
		ArgumentOutOfRangeException_t1_1501 * L_15 = (ArgumentOutOfRangeException_t1_1501 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1_13276(L_15, (String_t*)_stringLiteral812, (Object_t *)L_14, (String_t*)_stringLiteral813, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_15);
	}

IL_005b:
	{
		ConsoleLogU5BU5D_t8_382* L_16 = (ConsoleLogU5BU5D_t8_382*)(__this->____items_1);
		ConsoleLog_t8_170  L_17 = ___item;
		int32_t L_18 = ___index;
		int32_t L_19 = ___count;
		int32_t L_20 = (( int32_t (*) (Object_t * /* static, unused */, ConsoleLogU5BU5D_t8_382*, ConsoleLog_t8_170 , int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 43)->method)(NULL /*static, unused*/, (ConsoleLogU5BU5D_t8_382*)L_16, (ConsoleLog_t8_170 )L_17, (int32_t)L_18, (int32_t)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 43));
		return L_20;
	}
}
// System.Boolean System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Remove(T)
extern "C" bool List_1_Remove_m1_27174_gshared (List_1_t1_1908 * __this, ConsoleLog_t8_170  ___item, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ConsoleLog_t8_170  L_0 = ___item;
		NullCheck((List_1_t1_1908 *)__this);
		int32_t L_1 = (int32_t)VirtFuncInvoker1< int32_t, ConsoleLog_t8_170  >::Invoke(28 /* System.Int32 System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::IndexOf(T) */, (List_1_t1_1908 *)__this, (ConsoleLog_t8_170 )L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)(-1))))
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t1_1908 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(30 /* System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::RemoveAt(System.Int32) */, (List_1_t1_1908 *)__this, (int32_t)L_3);
	}

IL_0016:
	{
		int32_t L_4 = V_0;
		return ((((int32_t)((((int32_t)L_4) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m1_27175_gshared (List_1_t1_1908 * __this, Predicate_1_t1_2729 * ___match, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Predicate_1_t1_2729 * L_0 = ___match;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		(( void (*) (Object_t * /* static, unused */, Predicate_1_t1_2729 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28)->method)(NULL /*static, unused*/, (Predicate_1_t1_2729 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28));
		V_0 = (int32_t)0;
		V_1 = (int32_t)0;
		V_0 = (int32_t)0;
		goto IL_0031;
	}

IL_0011:
	{
		Predicate_1_t1_2729 * L_1 = ___match;
		ConsoleLogU5BU5D_t8_382* L_2 = (ConsoleLogU5BU5D_t8_382*)(__this->____items_1);
		int32_t L_3 = V_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		NullCheck((Predicate_1_t1_2729 *)L_1);
		bool L_5 = (( bool (*) (Predicate_1_t1_2729 *, ConsoleLog_t8_170 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 32)->method)((Predicate_1_t1_2729 *)L_1, (ConsoleLog_t8_170 )(*(ConsoleLog_t8_170 *)(ConsoleLog_t8_170 *)SZArrayLdElema(L_2, L_4, sizeof(ConsoleLog_t8_170 ))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 32));
		if (!L_5)
		{
			goto IL_002d;
		}
	}
	{
		goto IL_003d;
	}

IL_002d:
	{
		int32_t L_6 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0031:
	{
		int32_t L_7 = V_0;
		int32_t L_8 = (int32_t)(__this->____size_2);
		if ((((int32_t)L_7) < ((int32_t)L_8)))
		{
			goto IL_0011;
		}
	}

IL_003d:
	{
		int32_t L_9 = V_0;
		int32_t L_10 = (int32_t)(__this->____size_2);
		if ((!(((uint32_t)L_9) == ((uint32_t)L_10))))
		{
			goto IL_004b;
		}
	}
	{
		return 0;
	}

IL_004b:
	{
		int32_t L_11 = (int32_t)(__this->____version_3);
		__this->____version_3 = ((int32_t)((int32_t)L_11+(int32_t)1));
		int32_t L_12 = V_0;
		V_1 = (int32_t)((int32_t)((int32_t)L_12+(int32_t)1));
		goto IL_0099;
	}

IL_0062:
	{
		Predicate_1_t1_2729 * L_13 = ___match;
		ConsoleLogU5BU5D_t8_382* L_14 = (ConsoleLogU5BU5D_t8_382*)(__this->____items_1);
		int32_t L_15 = V_1;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		NullCheck((Predicate_1_t1_2729 *)L_13);
		bool L_17 = (( bool (*) (Predicate_1_t1_2729 *, ConsoleLog_t8_170 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 32)->method)((Predicate_1_t1_2729 *)L_13, (ConsoleLog_t8_170 )(*(ConsoleLog_t8_170 *)(ConsoleLog_t8_170 *)SZArrayLdElema(L_14, L_16, sizeof(ConsoleLog_t8_170 ))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 32));
		if (L_17)
		{
			goto IL_0095;
		}
	}
	{
		ConsoleLogU5BU5D_t8_382* L_18 = (ConsoleLogU5BU5D_t8_382*)(__this->____items_1);
		int32_t L_19 = V_0;
		int32_t L_20 = (int32_t)L_19;
		V_0 = (int32_t)((int32_t)((int32_t)L_20+(int32_t)1));
		ConsoleLogU5BU5D_t8_382* L_21 = (ConsoleLogU5BU5D_t8_382*)(__this->____items_1);
		int32_t L_22 = V_1;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, L_22);
		int32_t L_23 = L_22;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_20);
		*((ConsoleLog_t8_170 *)(ConsoleLog_t8_170 *)SZArrayLdElema(L_18, L_20, sizeof(ConsoleLog_t8_170 ))) = (ConsoleLog_t8_170 )(*(ConsoleLog_t8_170 *)(ConsoleLog_t8_170 *)SZArrayLdElema(L_21, L_23, sizeof(ConsoleLog_t8_170 )));
	}

IL_0095:
	{
		int32_t L_24 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_0099:
	{
		int32_t L_25 = V_1;
		int32_t L_26 = (int32_t)(__this->____size_2);
		if ((((int32_t)L_25) < ((int32_t)L_26)))
		{
			goto IL_0062;
		}
	}
	{
		int32_t L_27 = V_1;
		int32_t L_28 = V_0;
		if ((((int32_t)((int32_t)((int32_t)L_27-(int32_t)L_28))) <= ((int32_t)0)))
		{
			goto IL_00bd;
		}
	}
	{
		ConsoleLogU5BU5D_t8_382* L_29 = (ConsoleLogU5BU5D_t8_382*)(__this->____items_1);
		int32_t L_30 = V_0;
		int32_t L_31 = V_1;
		int32_t L_32 = V_0;
		Array_Clear_m1_1037(NULL /*static, unused*/, (Array_t *)(Array_t *)L_29, (int32_t)L_30, (int32_t)((int32_t)((int32_t)L_31-(int32_t)L_32)), /*hidden argument*/NULL);
	}

IL_00bd:
	{
		int32_t L_33 = V_0;
		__this->____size_2 = L_33;
		int32_t L_34 = V_1;
		int32_t L_35 = V_0;
		return ((int32_t)((int32_t)L_34-(int32_t)L_35));
	}
}
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::RemoveAt(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral170;
extern "C" void List_1_RemoveAt_m1_27176_gshared (List_1_t1_1908 * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		_stringLiteral170 = il2cpp_codegen_string_literal_from_index(170);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index;
		int32_t L_2 = (int32_t)(__this->____size_2);
		if ((!(((uint32_t)L_1) >= ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}

IL_0013:
	{
		ArgumentOutOfRangeException_t1_1501 * L_3 = (ArgumentOutOfRangeException_t1_1501 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1_13274(L_3, (String_t*)_stringLiteral170, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_001e:
	{
		int32_t L_4 = ___index;
		NullCheck((List_1_t1_1908 *)__this);
		(( void (*) (List_1_t1_1908 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39)->method)((List_1_t1_1908 *)__this, (int32_t)L_4, (int32_t)(-1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39));
		ConsoleLogU5BU5D_t8_382* L_5 = (ConsoleLogU5BU5D_t8_382*)(__this->____items_1);
		int32_t L_6 = (int32_t)(__this->____size_2);
		Array_Clear_m1_1037(NULL /*static, unused*/, (Array_t *)(Array_t *)L_5, (int32_t)L_6, (int32_t)1, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)(__this->____version_3);
		__this->____version_3 = ((int32_t)((int32_t)L_7+(int32_t)1));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::RemoveRange(System.Int32,System.Int32)
extern "C" void List_1_RemoveRange_m1_27177_gshared (List_1_t1_1908 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index;
		int32_t L_1 = ___count;
		NullCheck((List_1_t1_1908 *)__this);
		(( void (*) (List_1_t1_1908 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26)->method)((List_1_t1_1908 *)__this, (int32_t)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26));
		int32_t L_2 = ___count;
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_3 = ___index;
		int32_t L_4 = ___count;
		NullCheck((List_1_t1_1908 *)__this);
		(( void (*) (List_1_t1_1908 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39)->method)((List_1_t1_1908 *)__this, (int32_t)L_3, (int32_t)((-L_4)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39));
		ConsoleLogU5BU5D_t8_382* L_5 = (ConsoleLogU5BU5D_t8_382*)(__this->____items_1);
		int32_t L_6 = (int32_t)(__this->____size_2);
		int32_t L_7 = ___count;
		Array_Clear_m1_1037(NULL /*static, unused*/, (Array_t *)(Array_t *)L_5, (int32_t)L_6, (int32_t)L_7, /*hidden argument*/NULL);
		int32_t L_8 = (int32_t)(__this->____version_3);
		__this->____version_3 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0038:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Reverse()
extern "C" void List_1_Reverse_m1_27178_gshared (List_1_t1_1908 * __this, const MethodInfo* method)
{
	{
		ConsoleLogU5BU5D_t8_382* L_0 = (ConsoleLogU5BU5D_t8_382*)(__this->____items_1);
		int32_t L_1 = (int32_t)(__this->____size_2);
		Array_Reverse_m1_1053(NULL /*static, unused*/, (Array_t *)(Array_t *)L_0, (int32_t)0, (int32_t)L_1, /*hidden argument*/NULL);
		int32_t L_2 = (int32_t)(__this->____version_3);
		__this->____version_3 = ((int32_t)((int32_t)L_2+(int32_t)1));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Reverse(System.Int32,System.Int32)
extern "C" void List_1_Reverse_m1_27179_gshared (List_1_t1_1908 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index;
		int32_t L_1 = ___count;
		NullCheck((List_1_t1_1908 *)__this);
		(( void (*) (List_1_t1_1908 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26)->method)((List_1_t1_1908 *)__this, (int32_t)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26));
		ConsoleLogU5BU5D_t8_382* L_2 = (ConsoleLogU5BU5D_t8_382*)(__this->____items_1);
		int32_t L_3 = ___index;
		int32_t L_4 = ___count;
		Array_Reverse_m1_1053(NULL /*static, unused*/, (Array_t *)(Array_t *)L_2, (int32_t)L_3, (int32_t)L_4, /*hidden argument*/NULL);
		int32_t L_5 = (int32_t)(__this->____version_3);
		__this->____version_3 = ((int32_t)((int32_t)L_5+(int32_t)1));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Sort()
extern "C" void List_1_Sort_m1_27180_gshared (List_1_t1_1908 * __this, const MethodInfo* method)
{
	{
		ConsoleLogU5BU5D_t8_382* L_0 = (ConsoleLogU5BU5D_t8_382*)(__this->____items_1);
		int32_t L_1 = (int32_t)(__this->____size_2);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 46));
		Comparer_1_t1_2725 * L_2 = (( Comparer_1_t1_2725 * (*) (Object_t * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 45)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 45));
		(( void (*) (Object_t * /* static, unused */, ConsoleLogU5BU5D_t8_382*, int32_t, int32_t, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 47)->method)(NULL /*static, unused*/, (ConsoleLogU5BU5D_t8_382*)L_0, (int32_t)0, (int32_t)L_1, (Object_t*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 47));
		int32_t L_3 = (int32_t)(__this->____version_3);
		__this->____version_3 = ((int32_t)((int32_t)L_3+(int32_t)1));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C" void List_1_Sort_m1_27181_gshared (List_1_t1_1908 * __this, Object_t* ___comparer, const MethodInfo* method)
{
	{
		ConsoleLogU5BU5D_t8_382* L_0 = (ConsoleLogU5BU5D_t8_382*)(__this->____items_1);
		int32_t L_1 = (int32_t)(__this->____size_2);
		Object_t* L_2 = ___comparer;
		(( void (*) (Object_t * /* static, unused */, ConsoleLogU5BU5D_t8_382*, int32_t, int32_t, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 47)->method)(NULL /*static, unused*/, (ConsoleLogU5BU5D_t8_382*)L_0, (int32_t)0, (int32_t)L_1, (Object_t*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 47));
		int32_t L_3 = (int32_t)(__this->____version_3);
		__this->____version_3 = ((int32_t)((int32_t)L_3+(int32_t)1));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m1_27182_gshared (List_1_t1_1908 * __this, Comparison_1_t1_2731 * ___comparison, const MethodInfo* method)
{
	{
		ConsoleLogU5BU5D_t8_382* L_0 = (ConsoleLogU5BU5D_t8_382*)(__this->____items_1);
		int32_t L_1 = (int32_t)(__this->____size_2);
		Comparison_1_t1_2731 * L_2 = ___comparison;
		(( void (*) (Object_t * /* static, unused */, ConsoleLogU5BU5D_t8_382*, int32_t, Comparison_1_t1_2731 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 48)->method)(NULL /*static, unused*/, (ConsoleLogU5BU5D_t8_382*)L_0, (int32_t)L_1, (Comparison_1_t1_2731 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 48));
		int32_t L_3 = (int32_t)(__this->____version_3);
		__this->____version_3 = ((int32_t)((int32_t)L_3+(int32_t)1));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Sort(System.Int32,System.Int32,System.Collections.Generic.IComparer`1<T>)
extern "C" void List_1_Sort_m1_27183_gshared (List_1_t1_1908 * __this, int32_t ___index, int32_t ___count, Object_t* ___comparer, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index;
		int32_t L_1 = ___count;
		NullCheck((List_1_t1_1908 *)__this);
		(( void (*) (List_1_t1_1908 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26)->method)((List_1_t1_1908 *)__this, (int32_t)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26));
		ConsoleLogU5BU5D_t8_382* L_2 = (ConsoleLogU5BU5D_t8_382*)(__this->____items_1);
		int32_t L_3 = ___index;
		int32_t L_4 = ___count;
		Object_t* L_5 = ___comparer;
		(( void (*) (Object_t * /* static, unused */, ConsoleLogU5BU5D_t8_382*, int32_t, int32_t, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 47)->method)(NULL /*static, unused*/, (ConsoleLogU5BU5D_t8_382*)L_2, (int32_t)L_3, (int32_t)L_4, (Object_t*)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 47));
		int32_t L_6 = (int32_t)(__this->____version_3);
		__this->____version_3 = ((int32_t)((int32_t)L_6+(int32_t)1));
		return;
	}
}
// T[] System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::ToArray()
extern "C" ConsoleLogU5BU5D_t8_382* List_1_ToArray_m1_27184_gshared (List_1_t1_1908 * __this, const MethodInfo* method)
{
	ConsoleLogU5BU5D_t8_382* V_0 = {0};
	{
		int32_t L_0 = (int32_t)(__this->____size_2);
		V_0 = (ConsoleLogU5BU5D_t8_382*)((ConsoleLogU5BU5D_t8_382*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), L_0));
		ConsoleLogU5BU5D_t8_382* L_1 = (ConsoleLogU5BU5D_t8_382*)(__this->____items_1);
		ConsoleLogU5BU5D_t8_382* L_2 = V_0;
		int32_t L_3 = (int32_t)(__this->____size_2);
		Array_Copy_m1_1040(NULL /*static, unused*/, (Array_t *)(Array_t *)L_1, (Array_t *)(Array_t *)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		ConsoleLogU5BU5D_t8_382* L_4 = V_0;
		return L_4;
	}
}
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::TrimExcess()
extern "C" void List_1_TrimExcess_m1_27185_gshared (List_1_t1_1908 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)(__this->____size_2);
		NullCheck((List_1_t1_1908 *)__this);
		(( void (*) (List_1_t1_1908 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19)->method)((List_1_t1_1908 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19));
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::TrueForAll(System.Predicate`1<T>)
extern "C" bool List_1_TrueForAll_m1_27186_gshared (List_1_t1_1908 * __this, Predicate_1_t1_2729 * ___match, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Predicate_1_t1_2729 * L_0 = ___match;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		(( void (*) (Object_t * /* static, unused */, Predicate_1_t1_2729 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28)->method)(NULL /*static, unused*/, (Predicate_1_t1_2729 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28));
		V_0 = (int32_t)0;
		goto IL_002a;
	}

IL_000d:
	{
		Predicate_1_t1_2729 * L_1 = ___match;
		ConsoleLogU5BU5D_t8_382* L_2 = (ConsoleLogU5BU5D_t8_382*)(__this->____items_1);
		int32_t L_3 = V_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		NullCheck((Predicate_1_t1_2729 *)L_1);
		bool L_5 = (( bool (*) (Predicate_1_t1_2729 *, ConsoleLog_t8_170 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 32)->method)((Predicate_1_t1_2729 *)L_1, (ConsoleLog_t8_170 )(*(ConsoleLog_t8_170 *)(ConsoleLog_t8_170 *)SZArrayLdElema(L_2, L_4, sizeof(ConsoleLog_t8_170 ))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 32));
		if (L_5)
		{
			goto IL_0026;
		}
	}
	{
		return 0;
	}

IL_0026:
	{
		int32_t L_6 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_002a:
	{
		int32_t L_7 = V_0;
		int32_t L_8 = (int32_t)(__this->____size_2);
		if ((((int32_t)L_7) < ((int32_t)L_8)))
		{
			goto IL_000d;
		}
	}
	{
		return 1;
	}
}
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m1_27187_gshared (List_1_t1_1908 * __this, const MethodInfo* method)
{
	{
		ConsoleLogU5BU5D_t8_382* L_0 = (ConsoleLogU5BU5D_t8_382*)(__this->____items_1);
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Array_t *)L_0)->max_length))));
	}
}
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::set_Capacity(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var;
extern "C" void List_1_set_Capacity_m1_27188_gshared (List_1_t1_1908 * __this, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___value;
		int32_t L_1 = (int32_t)(__this->____size_2);
		if ((!(((uint32_t)L_0) < ((uint32_t)L_1))))
		{
			goto IL_0012;
		}
	}
	{
		ArgumentOutOfRangeException_t1_1501 * L_2 = (ArgumentOutOfRangeException_t1_1501 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1_13273(L_2, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0012:
	{
		ConsoleLogU5BU5D_t8_382** L_3 = (ConsoleLogU5BU5D_t8_382**)&(__this->____items_1);
		int32_t L_4 = ___value;
		(( void (*) (Object_t * /* static, unused */, ConsoleLogU5BU5D_t8_382**, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 49)->method)(NULL /*static, unused*/, (ConsoleLogU5BU5D_t8_382**)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 49));
		return;
	}
}
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::get_Count()
extern "C" int32_t List_1_get_Count_m1_27189_gshared (List_1_t1_1908 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)(__this->____size_2);
		return L_0;
	}
}
// T System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::get_Item(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral170;
extern "C" ConsoleLog_t8_170  List_1_get_Item_m1_27190_gshared (List_1_t1_1908 * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		_stringLiteral170 = il2cpp_codegen_string_literal_from_index(170);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index;
		int32_t L_1 = (int32_t)(__this->____size_2);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1_1501 * L_2 = (ArgumentOutOfRangeException_t1_1501 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1_13274(L_2, (String_t*)_stringLiteral170, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		ConsoleLogU5BU5D_t8_382* L_3 = (ConsoleLogU5BU5D_t8_382*)(__this->____items_1);
		int32_t L_4 = ___index;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		return (*(ConsoleLog_t8_170 *)(ConsoleLog_t8_170 *)SZArrayLdElema(L_3, L_5, sizeof(ConsoleLog_t8_170 )));
	}
}
// System.Void System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::set_Item(System.Int32,T)
extern TypeInfo* ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral170;
extern "C" void List_1_set_Item_m1_27191_gshared (List_1_t1_1908 * __this, int32_t ___index, ConsoleLog_t8_170  ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		_stringLiteral170 = il2cpp_codegen_string_literal_from_index(170);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index;
		NullCheck((List_1_t1_1908 *)__this);
		(( void (*) (List_1_t1_1908 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((List_1_t1_1908 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		int32_t L_1 = ___index;
		int32_t L_2 = (int32_t)(__this->____size_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}
	{
		ArgumentOutOfRangeException_t1_1501 * L_3 = (ArgumentOutOfRangeException_t1_1501 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1_13274(L_3, (String_t*)_stringLiteral170, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_001e:
	{
		ConsoleLogU5BU5D_t8_382* L_4 = (ConsoleLogU5BU5D_t8_382*)(__this->____items_1);
		int32_t L_5 = ___index;
		ConsoleLog_t8_170  L_6 = ___value;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		*((ConsoleLog_t8_170 *)(ConsoleLog_t8_170 *)SZArrayLdElema(L_4, L_5, sizeof(ConsoleLog_t8_170 ))) = (ConsoleLog_t8_170 )L_6;
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_27192_gshared (InternalEnumerator_1_t1_2720 * __this, Array_t * ___array, const MethodInfo* method)
{
	{
		Array_t * L_0 = ___array;
		__this->___array_2 = L_0;
		__this->___idx_3 = ((int32_t)-2);
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_27193_gshared (InternalEnumerator_1_t1_2720 * __this, const MethodInfo* method)
{
	{
		__this->___idx_3 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_27194_gshared (InternalEnumerator_1_t1_2720 * __this, const MethodInfo* method)
{
	{
		ConsoleLog_t8_170  L_0 = (( ConsoleLog_t8_170  (*) (InternalEnumerator_1_t1_2720 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t1_2720 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ConsoleLog_t8_170  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_27195_gshared (InternalEnumerator_1_t1_2720 * __this, const MethodInfo* method)
{
	{
		__this->___idx_3 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_27196_gshared (InternalEnumerator_1_t1_2720 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)(__this->___idx_3);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Array_t * L_1 = (Array_t *)(__this->___array_2);
		NullCheck((Array_t *)L_1);
		int32_t L_2 = Array_get_Length_m1_992((Array_t *)L_1, /*hidden argument*/NULL);
		__this->___idx_3 = L_2;
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)(__this->___idx_3);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)(__this->___idx_3);
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->___idx_3 = L_5;
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::get_Current()
extern TypeInfo* InvalidOperationException_t1_1559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral217;
extern Il2CppCodeGenString* _stringLiteral218;
extern "C" ConsoleLog_t8_170  InternalEnumerator_1_get_Current_m1_27197_gshared (InternalEnumerator_1_t1_2720 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvalidOperationException_t1_1559_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(78);
		_stringLiteral217 = il2cpp_codegen_string_literal_from_index(217);
		_stringLiteral218 = il2cpp_codegen_string_literal_from_index(218);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)(__this->___idx_3);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1_1559 * L_1 = (InvalidOperationException_t1_1559 *)il2cpp_codegen_object_new (InvalidOperationException_t1_1559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1_14171(L_1, (String_t*)_stringLiteral217, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)(__this->___idx_3);
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1_1559 * L_3 = (InvalidOperationException_t1_1559 *)il2cpp_codegen_object_new (InvalidOperationException_t1_1559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1_14171(L_3, (String_t*)_stringLiteral218, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_002f:
	{
		Array_t * L_4 = (Array_t *)(__this->___array_2);
		Array_t * L_5 = (Array_t *)(__this->___array_2);
		NullCheck((Array_t *)L_5);
		int32_t L_6 = Array_get_Length_m1_992((Array_t *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)(__this->___idx_3);
		NullCheck((Array_t *)L_4);
		ConsoleLog_t8_170  L_8 = (( ConsoleLog_t8_170  (*) (Array_t *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Array_t *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Collections.Generic.List`1/Enumerator<VoxelBusters.DebugPRO.Internal.ConsoleLog>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m1_27198_gshared (Enumerator_t1_2721 * __this, List_1_t1_1908 * ___l, const MethodInfo* method)
{
	{
		List_1_t1_1908 * L_0 = ___l;
		__this->___l_0 = L_0;
		List_1_t1_1908 * L_1 = ___l;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)(L_1->____version_3);
		__this->___ver_2 = L_2;
		return;
	}
}
// System.Void System.Collections.Generic.List`1/Enumerator<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m1_27199_gshared (Enumerator_t1_2721 * __this, const MethodInfo* method)
{
	{
		(( void (*) (Enumerator_t1_2721 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t1_2721 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->___next_1 = 0;
		return;
	}
}
// System.Object System.Collections.Generic.List`1/Enumerator<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IEnumerator.get_Current()
extern TypeInfo* InvalidOperationException_t1_1559_il2cpp_TypeInfo_var;
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m1_27200_gshared (Enumerator_t1_2721 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvalidOperationException_t1_1559_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(78);
		s_Il2CppMethodIntialized = true;
	}
	{
		(( void (*) (Enumerator_t1_2721 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t1_2721 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)(__this->___next_1);
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1_1559 * L_1 = (InvalidOperationException_t1_1559 *)il2cpp_codegen_object_new (InvalidOperationException_t1_1559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1_14170(L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0018:
	{
		ConsoleLog_t8_170  L_2 = (ConsoleLog_t8_170 )(__this->___current_3);
		ConsoleLog_t8_170  L_3 = L_2;
		Object_t * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
// System.Void System.Collections.Generic.List`1/Enumerator<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Dispose()
extern "C" void Enumerator_Dispose_m1_27201_gshared (Enumerator_t1_2721 * __this, const MethodInfo* method)
{
	{
		__this->___l_0 = (List_1_t1_1908 *)NULL;
		return;
	}
}
// System.Void System.Collections.Generic.List`1/Enumerator<VoxelBusters.DebugPRO.Internal.ConsoleLog>::VerifyState()
extern TypeInfo* ObjectDisposedException_t1_1588_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidOperationException_t1_1559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral814;
extern "C" void Enumerator_VerifyState_m1_27202_gshared (Enumerator_t1_2721 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectDisposedException_t1_1588_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(136);
		InvalidOperationException_t1_1559_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(78);
		_stringLiteral814 = il2cpp_codegen_string_literal_from_index(814);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1_1908 * L_0 = (List_1_t1_1908 *)(__this->___l_0);
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t1_2721  L_1 = (*(Enumerator_t1_2721 *)__this);
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Object_t *)L_2);
		Type_t * L_3 = Object_GetType_m1_5((Object_t *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(159 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t1_1588 * L_5 = (ObjectDisposedException_t1_1588 *)il2cpp_codegen_object_new (ObjectDisposedException_t1_1588_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1_14523(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)(__this->___ver_2);
		List_1_t1_1908 * L_7 = (List_1_t1_1908 *)(__this->___l_0);
		NullCheck(L_7);
		int32_t L_8 = (int32_t)(L_7->____version_3);
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1_1559 * L_9 = (InvalidOperationException_t1_1559 *)il2cpp_codegen_object_new (InvalidOperationException_t1_1559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1_14171(L_9, (String_t*)_stringLiteral814, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_9);
	}

IL_0047:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<VoxelBusters.DebugPRO.Internal.ConsoleLog>::MoveNext()
extern "C" bool Enumerator_MoveNext_m1_27203_gshared (Enumerator_t1_2721 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		(( void (*) (Enumerator_t1_2721 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t1_2721 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)(__this->___next_1);
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return 0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)(__this->___next_1);
		List_1_t1_1908 * L_2 = (List_1_t1_1908 *)(__this->___l_0);
		NullCheck(L_2);
		int32_t L_3 = (int32_t)(L_2->____size_2);
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t1_1908 * L_4 = (List_1_t1_1908 *)(__this->___l_0);
		NullCheck(L_4);
		ConsoleLogU5BU5D_t8_382* L_5 = (ConsoleLogU5BU5D_t8_382*)(L_4->____items_1);
		int32_t L_6 = (int32_t)(__this->___next_1);
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->___next_1 = ((int32_t)((int32_t)L_7+(int32_t)1));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		__this->___current_3 = (*(ConsoleLog_t8_170 *)(ConsoleLog_t8_170 *)SZArrayLdElema(L_5, L_9, sizeof(ConsoleLog_t8_170 )));
		return 1;
	}

IL_0053:
	{
		__this->___next_1 = (-1);
		return 0;
	}
}
// T System.Collections.Generic.List`1/Enumerator<VoxelBusters.DebugPRO.Internal.ConsoleLog>::get_Current()
extern "C" ConsoleLog_t8_170  Enumerator_get_Current_m1_27204_gshared (Enumerator_t1_2721 * __this, const MethodInfo* method)
{
	{
		ConsoleLog_t8_170  L_0 = (ConsoleLog_t8_170 )(__this->___current_3);
		return L_0;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::.ctor(System.Collections.Generic.IList`1<T>)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral815;
extern "C" void ReadOnlyCollection_1__ctor_m1_27205_gshared (ReadOnlyCollection_1_t1_2722 * __this, Object_t* ___list, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		_stringLiteral815 = il2cpp_codegen_string_literal_from_index(815);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1_0((Object_t *)__this, /*hidden argument*/NULL);
		Object_t* L_0 = ___list;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, (String_t*)_stringLiteral815, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0017:
	{
		Object_t* L_2 = ___list;
		__this->___list_0 = L_2;
		return;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.Generic.ICollection<T>.Add(T)
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1_27206_gshared (ReadOnlyCollection_1_t1_2722 * __this, ConsoleLog_t8_170  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14416(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.Generic.ICollection<T>.Clear()
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1_27207_gshared (ReadOnlyCollection_1_t1_2722 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14416(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1_27208_gshared (ReadOnlyCollection_1_t1_2722 * __this, int32_t ___index, ConsoleLog_t8_170  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14416(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.Generic.ICollection<T>.Remove(T)
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1_27209_gshared (ReadOnlyCollection_1_t1_2722 * __this, ConsoleLog_t8_170  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14416(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1_27210_gshared (ReadOnlyCollection_1_t1_2722 * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14416(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// T System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C" ConsoleLog_t8_170  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1_27211_gshared (ReadOnlyCollection_1_t1_2722 * __this, int32_t ___index, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index;
		NullCheck((ReadOnlyCollection_1_t1_2722 *)__this);
		ConsoleLog_t8_170  L_1 = (ConsoleLog_t8_170 )VirtFuncInvoker1< ConsoleLog_t8_170 , int32_t >::Invoke(33 /* T System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::get_Item(System.Int32) */, (ReadOnlyCollection_1_t1_2722 *)__this, (int32_t)L_0);
		return L_1;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1_27212_gshared (ReadOnlyCollection_1_t1_2722 * __this, int32_t ___index, ConsoleLog_t8_170  ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14416(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_27213_gshared (ReadOnlyCollection_1_t1_2722 * __this, const MethodInfo* method)
{
	{
		return 1;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern TypeInfo* ICollection_t1_280_il2cpp_TypeInfo_var;
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1_27214_gshared (ReadOnlyCollection_1_t1_2722 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ICollection_t1_280_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(149);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		Array_t * L_1 = ___array;
		int32_t L_2 = ___index;
		NullCheck((Object_t *)((Object_t *)Castclass(L_0, ICollection_t1_280_il2cpp_TypeInfo_var)));
		InterfaceActionInvoker2< Array_t *, int32_t >::Invoke(3 /* System.Void System.Collections.ICollection::CopyTo(System.Array,System.Int32) */, ICollection_t1_280_il2cpp_TypeInfo_var, (Object_t *)((Object_t *)Castclass(L_0, ICollection_t1_280_il2cpp_TypeInfo_var)), (Array_t *)L_1, (int32_t)L_2);
		return;
	}
}
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IEnumerable.GetEnumerator()
extern TypeInfo* IEnumerable_t1_1677_il2cpp_TypeInfo_var;
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1_27215_gshared (ReadOnlyCollection_1_t1_2722 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IEnumerable_t1_1677_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(253);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		NullCheck((Object_t *)L_0);
		Object_t * L_1 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t1_1677_il2cpp_TypeInfo_var, (Object_t *)L_0);
		return L_1;
	}
}
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IList.Add(System.Object)
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m1_27216_gshared (ReadOnlyCollection_1_t1_2722 * __this, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14416(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IList.Clear()
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m1_27217_gshared (ReadOnlyCollection_1_t1_2722 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14416(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IList.Contains(System.Object)
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_Contains_m1_27218_gshared (ReadOnlyCollection_1_t1_2722 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		bool L_1 = (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, (Object_t *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Object_t* L_2 = (Object_t*)(__this->___list_0);
		Object_t * L_3 = ___value;
		NullCheck((Object_t*)L_2);
		bool L_4 = (bool)InterfaceFuncInvoker1< bool, ConsoleLog_t8_170  >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Contains(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Object_t*)L_2, (ConsoleLog_t8_170 )((*(ConsoleLog_t8_170 *)((ConsoleLog_t8_170 *)UnBox (L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))));
		return L_4;
	}

IL_001d:
	{
		return 0;
	}
}
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1_27219_gshared (ReadOnlyCollection_1_t1_2722 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		bool L_1 = (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, (Object_t *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Object_t* L_2 = (Object_t*)(__this->___list_0);
		Object_t * L_3 = ___value;
		NullCheck((Object_t*)L_2);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker1< int32_t, ConsoleLog_t8_170  >::Invoke(0 /* System.Int32 System.Collections.Generic.IList`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::IndexOf(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Object_t*)L_2, (ConsoleLog_t8_170 )((*(ConsoleLog_t8_170 *)((ConsoleLog_t8_170 *)UnBox (L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))));
		return L_4;
	}

IL_001d:
	{
		return (-1);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IList.Insert(System.Int32,System.Object)
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m1_27220_gshared (ReadOnlyCollection_1_t1_2722 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14416(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IList.Remove(System.Object)
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m1_27221_gshared (ReadOnlyCollection_1_t1_2722 * __this, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14416(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IList.RemoveAt(System.Int32)
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1_27222_gshared (ReadOnlyCollection_1_t1_2722 * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14416(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1_27223_gshared (ReadOnlyCollection_1_t1_2722 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1_27224_gshared (ReadOnlyCollection_1_t1_2722 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IList.get_IsFixedSize()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1_27225_gshared (ReadOnlyCollection_1_t1_2722 * __this, const MethodInfo* method)
{
	{
		return 1;
	}
}
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IList.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1_27226_gshared (ReadOnlyCollection_1_t1_2722 * __this, const MethodInfo* method)
{
	{
		return 1;
	}
}
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IList_get_Item_m1_27227_gshared (ReadOnlyCollection_1_t1_2722 * __this, int32_t ___index, const MethodInfo* method)
{
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		int32_t L_1 = ___index;
		NullCheck((Object_t*)L_0);
		ConsoleLog_t8_170  L_2 = (ConsoleLog_t8_170 )InterfaceFuncInvoker1< ConsoleLog_t8_170 , int32_t >::Invoke(3 /* T System.Collections.Generic.IList`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Object_t*)L_0, (int32_t)L_1);
		ConsoleLog_t8_170  L_3 = L_2;
		Object_t * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), &L_3);
		return L_4;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m1_27228_gshared (ReadOnlyCollection_1_t1_2722 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14416(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Contains(T)
extern "C" bool ReadOnlyCollection_1_Contains_m1_27229_gshared (ReadOnlyCollection_1_t1_2722 * __this, ConsoleLog_t8_170  ___value, const MethodInfo* method)
{
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		ConsoleLog_t8_170  L_1 = ___value;
		NullCheck((Object_t*)L_0);
		bool L_2 = (bool)InterfaceFuncInvoker1< bool, ConsoleLog_t8_170  >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Contains(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Object_t*)L_0, (ConsoleLog_t8_170 )L_1);
		return L_2;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::CopyTo(T[],System.Int32)
extern "C" void ReadOnlyCollection_1_CopyTo_m1_27230_gshared (ReadOnlyCollection_1_t1_2722 * __this, ConsoleLogU5BU5D_t8_382* ___array, int32_t ___index, const MethodInfo* method)
{
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		ConsoleLogU5BU5D_t8_382* L_1 = ___array;
		int32_t L_2 = ___index;
		NullCheck((Object_t*)L_0);
		InterfaceActionInvoker2< ConsoleLogU5BU5D_t8_382*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::CopyTo(T[],System.Int32) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Object_t*)L_0, (ConsoleLogU5BU5D_t8_382*)L_1, (int32_t)L_2);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::GetEnumerator()
extern "C" Object_t* ReadOnlyCollection_1_GetEnumerator_m1_27231_gshared (ReadOnlyCollection_1_t1_2722 * __this, const MethodInfo* method)
{
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		NullCheck((Object_t*)L_0);
		Object_t* L_1 = (Object_t*)InterfaceFuncInvoker0< Object_t* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Object_t*)L_0);
		return L_1;
	}
}
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::IndexOf(T)
extern "C" int32_t ReadOnlyCollection_1_IndexOf_m1_27232_gshared (ReadOnlyCollection_1_t1_2722 * __this, ConsoleLog_t8_170  ___value, const MethodInfo* method)
{
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		ConsoleLog_t8_170  L_1 = ___value;
		NullCheck((Object_t*)L_0);
		int32_t L_2 = (int32_t)InterfaceFuncInvoker1< int32_t, ConsoleLog_t8_170  >::Invoke(0 /* System.Int32 System.Collections.Generic.IList`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::IndexOf(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Object_t*)L_0, (ConsoleLog_t8_170 )L_1);
		return L_2;
	}
}
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::get_Count()
extern "C" int32_t ReadOnlyCollection_1_get_Count_m1_27233_gshared (ReadOnlyCollection_1_t1_2722 * __this, const MethodInfo* method)
{
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		NullCheck((Object_t*)L_0);
		int32_t L_1 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::get_Count() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Object_t*)L_0);
		return L_1;
	}
}
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::get_Items()
extern "C" Object_t* ReadOnlyCollection_1_get_Items_m1_27234_gshared (ReadOnlyCollection_1_t1_2722 * __this, const MethodInfo* method)
{
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		return L_0;
	}
}
// T System.Collections.ObjectModel.ReadOnlyCollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::get_Item(System.Int32)
extern "C" ConsoleLog_t8_170  ReadOnlyCollection_1_get_Item_m1_27235_gshared (ReadOnlyCollection_1_t1_2722 * __this, int32_t ___index, const MethodInfo* method)
{
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		int32_t L_1 = ___index;
		NullCheck((Object_t*)L_0);
		ConsoleLog_t8_170  L_2 = (ConsoleLog_t8_170 )InterfaceFuncInvoker1< ConsoleLog_t8_170 , int32_t >::Invoke(3 /* T System.Collections.Generic.IList`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Object_t*)L_0, (int32_t)L_1);
		return L_2;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::.ctor()
extern TypeInfo* ICollection_t1_280_il2cpp_TypeInfo_var;
extern "C" void Collection_1__ctor_m1_27236_gshared (Collection_1_t1_2724 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ICollection_t1_280_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(149);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t1_1908 * V_0 = {0};
	Object_t * V_1 = {0};
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1_0((Object_t *)__this, /*hidden argument*/NULL);
		List_1_t1_1908 * L_0 = (List_1_t1_1908 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		(( void (*) (List_1_t1_1908 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (List_1_t1_1908 *)L_0;
		List_1_t1_1908 * L_1 = V_0;
		V_1 = (Object_t *)L_1;
		Object_t * L_2 = V_1;
		NullCheck((Object_t *)L_2);
		Object_t * L_3 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t1_280_il2cpp_TypeInfo_var, (Object_t *)L_2);
		__this->___syncRoot_1 = L_3;
		List_1_t1_1908 * L_4 = V_0;
		__this->___list_0 = L_4;
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::.ctor(System.Collections.Generic.IList`1<T>)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* ICollection_t1_280_il2cpp_TypeInfo_var;
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral815;
extern "C" void Collection_1__ctor_m1_27237_gshared (Collection_1_t1_2724 * __this, Object_t* ___list, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		ICollection_t1_280_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(149);
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		_stringLiteral815 = il2cpp_codegen_string_literal_from_index(815);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	Collection_1_t1_2724 * G_B4_0 = {0};
	Collection_1_t1_2724 * G_B3_0 = {0};
	Object_t * G_B5_0 = {0};
	Collection_1_t1_2724 * G_B5_1 = {0};
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1_0((Object_t *)__this, /*hidden argument*/NULL);
		Object_t* L_0 = ___list;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, (String_t*)_stringLiteral815, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0017:
	{
		Object_t* L_2 = ___list;
		__this->___list_0 = L_2;
		Object_t* L_3 = ___list;
		V_0 = (Object_t *)((Object_t *)IsInst(L_3, ICollection_t1_280_il2cpp_TypeInfo_var));
		Object_t * L_4 = V_0;
		G_B3_0 = ((Collection_1_t1_2724 *)(__this));
		if (!L_4)
		{
			G_B4_0 = ((Collection_1_t1_2724 *)(__this));
			goto IL_0037;
		}
	}
	{
		Object_t * L_5 = V_0;
		NullCheck((Object_t *)L_5);
		Object_t * L_6 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t1_280_il2cpp_TypeInfo_var, (Object_t *)L_5);
		G_B5_0 = L_6;
		G_B5_1 = ((Collection_1_t1_2724 *)(G_B3_0));
		goto IL_003c;
	}

IL_0037:
	{
		Object_t * L_7 = (Object_t *)il2cpp_codegen_object_new (Object_t_il2cpp_TypeInfo_var);
		Object__ctor_m1_0(L_7, /*hidden argument*/NULL);
		G_B5_0 = L_7;
		G_B5_1 = ((Collection_1_t1_2724 *)(G_B4_0));
	}

IL_003c:
	{
		NullCheck(G_B5_1);
		G_B5_1->___syncRoot_1 = G_B5_0;
		return;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_27238_gshared (Collection_1_t1_2724 * __this, const MethodInfo* method)
{
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		NullCheck((Object_t*)L_0);
		bool L_1 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.Generic.ICollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::get_IsReadOnly() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Object_t*)L_0);
		return L_1;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern TypeInfo* ICollection_t1_280_il2cpp_TypeInfo_var;
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m1_27239_gshared (Collection_1_t1_2724 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ICollection_t1_280_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(149);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		Array_t * L_1 = ___array;
		int32_t L_2 = ___index;
		NullCheck((Object_t *)((Object_t *)Castclass(L_0, ICollection_t1_280_il2cpp_TypeInfo_var)));
		InterfaceActionInvoker2< Array_t *, int32_t >::Invoke(3 /* System.Void System.Collections.ICollection::CopyTo(System.Array,System.Int32) */, ICollection_t1_280_il2cpp_TypeInfo_var, (Object_t *)((Object_t *)Castclass(L_0, ICollection_t1_280_il2cpp_TypeInfo_var)), (Array_t *)L_1, (int32_t)L_2);
		return;
	}
}
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m1_27240_gshared (Collection_1_t1_2724 * __this, const MethodInfo* method)
{
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		NullCheck((Object_t*)L_0);
		Object_t* L_1 = (Object_t*)InterfaceFuncInvoker0< Object_t* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Object_t*)L_0);
		return L_1;
	}
}
// System.Int32 System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m1_27241_gshared (Collection_1_t1_2724 * __this, Object_t * ___value, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		NullCheck((Object_t*)L_0);
		int32_t L_1 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::get_Count() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Object_t*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		Object_t * L_3 = ___value;
		ConsoleLog_t8_170  L_4 = (( ConsoleLog_t8_170  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, (Object_t *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Collection_1_t1_2724 *)__this);
		VirtActionInvoker2< int32_t, ConsoleLog_t8_170  >::Invoke(34 /* System.Void System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::InsertItem(System.Int32,T) */, (Collection_1_t1_2724 *)__this, (int32_t)L_2, (ConsoleLog_t8_170 )L_4);
		int32_t L_5 = V_0;
		return L_5;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m1_27242_gshared (Collection_1_t1_2724 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		bool L_1 = (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(NULL /*static, unused*/, (Object_t *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Object_t* L_2 = (Object_t*)(__this->___list_0);
		Object_t * L_3 = ___value;
		NullCheck((Object_t*)L_2);
		bool L_4 = (bool)InterfaceFuncInvoker1< bool, ConsoleLog_t8_170  >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Contains(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Object_t*)L_2, (ConsoleLog_t8_170 )((*(ConsoleLog_t8_170 *)((ConsoleLog_t8_170 *)UnBox (L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))));
		return L_4;
	}

IL_001d:
	{
		return 0;
	}
}
// System.Int32 System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m1_27243_gshared (Collection_1_t1_2724 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		bool L_1 = (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(NULL /*static, unused*/, (Object_t *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Object_t* L_2 = (Object_t*)(__this->___list_0);
		Object_t * L_3 = ___value;
		NullCheck((Object_t*)L_2);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker1< int32_t, ConsoleLog_t8_170  >::Invoke(0 /* System.Int32 System.Collections.Generic.IList`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::IndexOf(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9), (Object_t*)L_2, (ConsoleLog_t8_170 )((*(ConsoleLog_t8_170 *)((ConsoleLog_t8_170 *)UnBox (L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))));
		return L_4;
	}

IL_001d:
	{
		return (-1);
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m1_27244_gshared (Collection_1_t1_2724 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index;
		Object_t * L_1 = ___value;
		ConsoleLog_t8_170  L_2 = (( ConsoleLog_t8_170  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, (Object_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Collection_1_t1_2724 *)__this);
		VirtActionInvoker2< int32_t, ConsoleLog_t8_170  >::Invoke(34 /* System.Void System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::InsertItem(System.Int32,T) */, (Collection_1_t1_2724 *)__this, (int32_t)L_0, (ConsoleLog_t8_170 )L_2);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m1_27245_gshared (Collection_1_t1_2724 * __this, Object_t * ___value, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		(( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(NULL /*static, unused*/, (Object_t*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		Object_t * L_1 = ___value;
		ConsoleLog_t8_170  L_2 = (( ConsoleLog_t8_170  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, (Object_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Collection_1_t1_2724 *)__this);
		int32_t L_3 = (int32_t)VirtFuncInvoker1< int32_t, ConsoleLog_t8_170  >::Invoke(28 /* System.Int32 System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::IndexOf(T) */, (Collection_1_t1_2724 *)__this, (ConsoleLog_t8_170 )L_2);
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		NullCheck((Collection_1_t1_2724 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(35 /* System.Void System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::RemoveItem(System.Int32) */, (Collection_1_t1_2724 *)__this, (int32_t)L_4);
		return;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m1_27246_gshared (Collection_1_t1_2724 * __this, const MethodInfo* method)
{
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		bool L_1 = (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->method)(NULL /*static, unused*/, (Object_t*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		return L_1;
	}
}
// System.Object System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m1_27247_gshared (Collection_1_t1_2724 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (Object_t *)(__this->___syncRoot_1);
		return L_0;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m1_27248_gshared (Collection_1_t1_2724 * __this, const MethodInfo* method)
{
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		bool L_1 = (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)(NULL /*static, unused*/, (Object_t*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		return L_1;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m1_27249_gshared (Collection_1_t1_2724 * __this, const MethodInfo* method)
{
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		NullCheck((Object_t*)L_0);
		bool L_1 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.Generic.ICollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::get_IsReadOnly() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Object_t*)L_0);
		return L_1;
	}
}
// System.Object System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m1_27250_gshared (Collection_1_t1_2724 * __this, int32_t ___index, const MethodInfo* method)
{
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		int32_t L_1 = ___index;
		NullCheck((Object_t*)L_0);
		ConsoleLog_t8_170  L_2 = (ConsoleLog_t8_170 )InterfaceFuncInvoker1< ConsoleLog_t8_170 , int32_t >::Invoke(3 /* T System.Collections.Generic.IList`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9), (Object_t*)L_0, (int32_t)L_1);
		ConsoleLog_t8_170  L_3 = L_2;
		Object_t * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), &L_3);
		return L_4;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m1_27251_gshared (Collection_1_t1_2724 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index;
		Object_t * L_1 = ___value;
		ConsoleLog_t8_170  L_2 = (( ConsoleLog_t8_170  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, (Object_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Collection_1_t1_2724 *)__this);
		VirtActionInvoker2< int32_t, ConsoleLog_t8_170  >::Invoke(36 /* System.Void System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::SetItem(System.Int32,T) */, (Collection_1_t1_2724 *)__this, (int32_t)L_0, (ConsoleLog_t8_170 )L_2);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Add(T)
extern "C" void Collection_1_Add_m1_27252_gshared (Collection_1_t1_2724 * __this, ConsoleLog_t8_170  ___item, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		NullCheck((Object_t*)L_0);
		int32_t L_1 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::get_Count() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Object_t*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		ConsoleLog_t8_170  L_3 = ___item;
		NullCheck((Collection_1_t1_2724 *)__this);
		VirtActionInvoker2< int32_t, ConsoleLog_t8_170  >::Invoke(34 /* System.Void System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::InsertItem(System.Int32,T) */, (Collection_1_t1_2724 *)__this, (int32_t)L_2, (ConsoleLog_t8_170 )L_3);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Clear()
extern "C" void Collection_1_Clear_m1_27253_gshared (Collection_1_t1_2724 * __this, const MethodInfo* method)
{
	{
		NullCheck((Collection_1_t1_2724 *)__this);
		VirtActionInvoker0::Invoke(33 /* System.Void System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::ClearItems() */, (Collection_1_t1_2724 *)__this);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::ClearItems()
extern "C" void Collection_1_ClearItems_m1_27254_gshared (Collection_1_t1_2724 * __this, const MethodInfo* method)
{
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		NullCheck((Object_t*)L_0);
		InterfaceActionInvoker0::Invoke(3 /* System.Void System.Collections.Generic.ICollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Clear() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Object_t*)L_0);
		return;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Contains(T)
extern "C" bool Collection_1_Contains_m1_27255_gshared (Collection_1_t1_2724 * __this, ConsoleLog_t8_170  ___item, const MethodInfo* method)
{
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		ConsoleLog_t8_170  L_1 = ___item;
		NullCheck((Object_t*)L_0);
		bool L_2 = (bool)InterfaceFuncInvoker1< bool, ConsoleLog_t8_170  >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Contains(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Object_t*)L_0, (ConsoleLog_t8_170 )L_1);
		return L_2;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m1_27256_gshared (Collection_1_t1_2724 * __this, ConsoleLogU5BU5D_t8_382* ___array, int32_t ___index, const MethodInfo* method)
{
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		ConsoleLogU5BU5D_t8_382* L_1 = ___array;
		int32_t L_2 = ___index;
		NullCheck((Object_t*)L_0);
		InterfaceActionInvoker2< ConsoleLogU5BU5D_t8_382*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::CopyTo(T[],System.Int32) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Object_t*)L_0, (ConsoleLogU5BU5D_t8_382*)L_1, (int32_t)L_2);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m1_27257_gshared (Collection_1_t1_2724 * __this, const MethodInfo* method)
{
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		NullCheck((Object_t*)L_0);
		Object_t* L_1 = (Object_t*)InterfaceFuncInvoker0< Object_t* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Object_t*)L_0);
		return L_1;
	}
}
// System.Int32 System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m1_27258_gshared (Collection_1_t1_2724 * __this, ConsoleLog_t8_170  ___item, const MethodInfo* method)
{
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		ConsoleLog_t8_170  L_1 = ___item;
		NullCheck((Object_t*)L_0);
		int32_t L_2 = (int32_t)InterfaceFuncInvoker1< int32_t, ConsoleLog_t8_170  >::Invoke(0 /* System.Int32 System.Collections.Generic.IList`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::IndexOf(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9), (Object_t*)L_0, (ConsoleLog_t8_170 )L_1);
		return L_2;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m1_27259_gshared (Collection_1_t1_2724 * __this, int32_t ___index, ConsoleLog_t8_170  ___item, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index;
		ConsoleLog_t8_170  L_1 = ___item;
		NullCheck((Collection_1_t1_2724 *)__this);
		VirtActionInvoker2< int32_t, ConsoleLog_t8_170  >::Invoke(34 /* System.Void System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::InsertItem(System.Int32,T) */, (Collection_1_t1_2724 *)__this, (int32_t)L_0, (ConsoleLog_t8_170 )L_1);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m1_27260_gshared (Collection_1_t1_2724 * __this, int32_t ___index, ConsoleLog_t8_170  ___item, const MethodInfo* method)
{
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		int32_t L_1 = ___index;
		ConsoleLog_t8_170  L_2 = ___item;
		NullCheck((Object_t*)L_0);
		InterfaceActionInvoker2< int32_t, ConsoleLog_t8_170  >::Invoke(1 /* System.Void System.Collections.Generic.IList`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Insert(System.Int32,T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9), (Object_t*)L_0, (int32_t)L_1, (ConsoleLog_t8_170 )L_2);
		return;
	}
}
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::get_Items()
extern "C" Object_t* Collection_1_get_Items_m1_27261_gshared (Collection_1_t1_2724 * __this, const MethodInfo* method)
{
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		return L_0;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Remove(T)
extern "C" bool Collection_1_Remove_m1_27262_gshared (Collection_1_t1_2724 * __this, ConsoleLog_t8_170  ___item, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ConsoleLog_t8_170  L_0 = ___item;
		NullCheck((Collection_1_t1_2724 *)__this);
		int32_t L_1 = (int32_t)VirtFuncInvoker1< int32_t, ConsoleLog_t8_170  >::Invoke(28 /* System.Int32 System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::IndexOf(T) */, (Collection_1_t1_2724 *)__this, (ConsoleLog_t8_170 )L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_0011;
		}
	}
	{
		return 0;
	}

IL_0011:
	{
		int32_t L_3 = V_0;
		NullCheck((Collection_1_t1_2724 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(35 /* System.Void System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::RemoveItem(System.Int32) */, (Collection_1_t1_2724 *)__this, (int32_t)L_3);
		return 1;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m1_27263_gshared (Collection_1_t1_2724 * __this, int32_t ___index, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index;
		NullCheck((Collection_1_t1_2724 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(35 /* System.Void System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::RemoveItem(System.Int32) */, (Collection_1_t1_2724 *)__this, (int32_t)L_0);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m1_27264_gshared (Collection_1_t1_2724 * __this, int32_t ___index, const MethodInfo* method)
{
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		int32_t L_1 = ___index;
		NullCheck((Object_t*)L_0);
		InterfaceActionInvoker1< int32_t >::Invoke(2 /* System.Void System.Collections.Generic.IList`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::RemoveAt(System.Int32) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9), (Object_t*)L_0, (int32_t)L_1);
		return;
	}
}
// System.Int32 System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::get_Count()
extern "C" int32_t Collection_1_get_Count_m1_27265_gshared (Collection_1_t1_2724 * __this, const MethodInfo* method)
{
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		NullCheck((Object_t*)L_0);
		int32_t L_1 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::get_Count() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Object_t*)L_0);
		return L_1;
	}
}
// T System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::get_Item(System.Int32)
extern "C" ConsoleLog_t8_170  Collection_1_get_Item_m1_27266_gshared (Collection_1_t1_2724 * __this, int32_t ___index, const MethodInfo* method)
{
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		int32_t L_1 = ___index;
		NullCheck((Object_t*)L_0);
		ConsoleLog_t8_170  L_2 = (ConsoleLog_t8_170 )InterfaceFuncInvoker1< ConsoleLog_t8_170 , int32_t >::Invoke(3 /* T System.Collections.Generic.IList`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9), (Object_t*)L_0, (int32_t)L_1);
		return L_2;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m1_27267_gshared (Collection_1_t1_2724 * __this, int32_t ___index, ConsoleLog_t8_170  ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index;
		ConsoleLog_t8_170  L_1 = ___value;
		NullCheck((Collection_1_t1_2724 *)__this);
		VirtActionInvoker2< int32_t, ConsoleLog_t8_170  >::Invoke(36 /* System.Void System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::SetItem(System.Int32,T) */, (Collection_1_t1_2724 *)__this, (int32_t)L_0, (ConsoleLog_t8_170 )L_1);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m1_27268_gshared (Collection_1_t1_2724 * __this, int32_t ___index, ConsoleLog_t8_170  ___item, const MethodInfo* method)
{
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		int32_t L_1 = ___index;
		ConsoleLog_t8_170  L_2 = ___item;
		NullCheck((Object_t*)L_0);
		InterfaceActionInvoker2< int32_t, ConsoleLog_t8_170  >::Invoke(4 /* System.Void System.Collections.Generic.IList`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::set_Item(System.Int32,T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9), (Object_t*)L_0, (int32_t)L_1, (ConsoleLog_t8_170 )L_2);
		return;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::IsValidItem(System.Object)
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern "C" bool Collection_1_IsValidItem_m1_27269_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	{
		Object_t * L_0 = ___item;
		if (((Object_t *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))
		{
			goto IL_0028;
		}
	}
	{
		Object_t * L_1 = ___item;
		if (L_1)
		{
			goto IL_0025;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_2);
		bool L_3 = (bool)VirtFuncInvoker0< bool >::Invoke(147 /* System.Boolean System.Type::get_IsValueType() */, (Type_t *)L_2);
		G_B4_0 = ((((int32_t)L_3) == ((int32_t)0))? 1 : 0);
		goto IL_0026;
	}

IL_0025:
	{
		G_B4_0 = 0;
	}

IL_0026:
	{
		G_B6_0 = G_B4_0;
		goto IL_0029;
	}

IL_0028:
	{
		G_B6_0 = 1;
	}

IL_0029:
	{
		return G_B6_0;
	}
}
// T System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::ConvertItem(System.Object)
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral808;
extern "C" ConsoleLog_t8_170  Collection_1_ConvertItem_m1_27270_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		_stringLiteral808 = il2cpp_codegen_string_literal_from_index(808);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___item;
		bool L_1 = (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(NULL /*static, unused*/, (Object_t *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		Object_t * L_2 = ___item;
		return ((*(ConsoleLog_t8_170 *)((ConsoleLog_t8_170 *)UnBox (L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)))));
	}

IL_0012:
	{
		ArgumentException_t1_1425 * L_3 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13260(L_3, (String_t*)_stringLiteral808, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern "C" void Collection_1_CheckWritable_m1_27271_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t* L_0 = ___list;
		NullCheck((Object_t*)L_0);
		bool L_1 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.Generic.ICollection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::get_IsReadOnly() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Object_t*)L_0);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		NotSupportedException_t1_1583 * L_2 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14416(L_2, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0011:
	{
		return;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern TypeInfo* ICollection_t1_280_il2cpp_TypeInfo_var;
extern "C" bool Collection_1_IsSynchronized_m1_27272_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ICollection_t1_280_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(149);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	int32_t G_B3_0 = 0;
	{
		Object_t* L_0 = ___list;
		V_0 = (Object_t *)((Object_t *)IsInst(L_0, ICollection_t1_280_il2cpp_TypeInfo_var));
		Object_t * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		Object_t * L_2 = V_0;
		NullCheck((Object_t *)L_2);
		bool L_3 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.ICollection::get_IsSynchronized() */, ICollection_t1_280_il2cpp_TypeInfo_var, (Object_t *)L_2);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_0019;
	}

IL_0018:
	{
		G_B3_0 = 0;
	}

IL_0019:
	{
		return G_B3_0;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern TypeInfo* IList_t1_262_il2cpp_TypeInfo_var;
extern "C" bool Collection_1_IsFixedSize_m1_27273_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IList_t1_262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(76);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	int32_t G_B3_0 = 0;
	{
		Object_t* L_0 = ___list;
		V_0 = (Object_t *)((Object_t *)IsInst(L_0, IList_t1_262_il2cpp_TypeInfo_var));
		Object_t * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		Object_t * L_2 = V_0;
		NullCheck((Object_t *)L_2);
		bool L_3 = (bool)InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IList::get_IsFixedSize() */, IList_t1_262_il2cpp_TypeInfo_var, (Object_t *)L_2);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_0019;
	}

IL_0018:
	{
		G_B3_0 = 0;
	}

IL_0019:
	{
		return G_B3_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::.ctor()
extern "C" void Comparer_1__ctor_m1_27274_gshared (Comparer_1_t1_2725 * __this, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1_0((Object_t *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::.cctor()
extern const Il2CppType* GenericComparer_1_t1_3063_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* TypeU5BU5D_t1_31_il2cpp_TypeInfo_var;
extern "C" void Comparer_1__cctor_m1_27275_gshared (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GenericComparer_1_t1_3063_0_0_0_var = il2cpp_codegen_type_from_index(4653);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		TypeU5BU5D_t1_31_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(63);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, Type_t * >::Invoke(169 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GenericComparer_1_t1_3063_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1_31* L_4 = (TypeU5BU5D_t1_31*)((TypeU5BU5D_t1_31*)SZArrayNew(TypeU5BU5D_t1_31_il2cpp_TypeInfo_var, 1));
		Type_t * L_5 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_4, 0, sizeof(Type_t *))) = (Type_t *)L_5;
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = (Type_t *)VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1_31* >::Invoke(210 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1_31*)L_4);
		Object_t * L_7 = Activator_CreateInstance_m1_13034(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t1_2725_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->____default_0 = ((Comparer_1_t1_2725 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t1_2726 * L_8 = (DefaultComparer_t1_2726 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		(( void (*) (DefaultComparer_t1_2726 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t1_2725_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->____default_0 = L_8;
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern "C" int32_t Comparer_1_System_Collections_IComparer_Compare_m1_27276_gshared (Comparer_1_t1_2725 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Object_t * L_0 = ___x;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Object_t * L_1 = ___y;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Object_t * L_2 = ___y;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Object_t * L_3 = ___x;
		if (!((Object_t *)IsInst(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Object_t * L_4 = ___y;
		if (!((Object_t *)IsInst(L_4, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Object_t * L_5 = ___x;
		Object_t * L_6 = ___y;
		NullCheck((Comparer_1_t1_2725 *)__this);
		int32_t L_7 = (int32_t)VirtFuncInvoker2< int32_t, ConsoleLog_t8_170 , ConsoleLog_t8_170  >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Compare(T,T) */, (Comparer_1_t1_2725 *)__this, (ConsoleLog_t8_170 )((*(ConsoleLog_t8_170 *)((ConsoleLog_t8_170 *)UnBox (L_5, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6))))), (ConsoleLog_t8_170 )((*(ConsoleLog_t8_170 *)((ConsoleLog_t8_170 *)UnBox (L_6, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t1_1425 * L_8 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13259(L_8, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::get_Default()
extern "C" Comparer_1_t1_2725 * Comparer_1_get_Default_m1_27277_gshared (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t1_2725 * L_0 = ((Comparer_1_t1_2725_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->____default_0;
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<VoxelBusters.DebugPRO.Internal.ConsoleLog>::.ctor()
extern "C" void DefaultComparer__ctor_m1_27278_gshared (DefaultComparer_t1_2726 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t1_2725 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		(( void (*) (Comparer_1_t1_2725 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Comparer_1_t1_2725 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Compare(T,T)
extern TypeInfo* IComparable_t1_1745_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral791;
extern "C" int32_t DefaultComparer_Compare_m1_27279_gshared (DefaultComparer_t1_2726 * __this, ConsoleLog_t8_170  ___x, ConsoleLog_t8_170  ___y, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IComparable_t1_1745_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		_stringLiteral791 = il2cpp_codegen_string_literal_from_index(791);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		ConsoleLog_t8_170  L_0 = ___x;
		goto IL_001e;
	}
	{
		ConsoleLog_t8_170  L_1 = ___y;
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		ConsoleLog_t8_170  L_2 = ___y;
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		ConsoleLog_t8_170  L_3 = ___x;
		ConsoleLog_t8_170  L_4 = L_3;
		Object_t * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_4);
		if (!((Object_t*)IsInst(L_5, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		ConsoleLog_t8_170  L_6 = ___x;
		ConsoleLog_t8_170  L_7 = L_6;
		Object_t * L_8 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_7);
		ConsoleLog_t8_170  L_9 = ___y;
		NullCheck((Object_t*)((Object_t*)Castclass(L_8, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))));
		int32_t L_10 = (int32_t)InterfaceFuncInvoker1< int32_t, ConsoleLog_t8_170  >::Invoke(0 /* System.Int32 System.IComparable`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::CompareTo(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Object_t*)((Object_t*)Castclass(L_8, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))), (ConsoleLog_t8_170 )L_9);
		return L_10;
	}

IL_004d:
	{
		ConsoleLog_t8_170  L_11 = ___x;
		ConsoleLog_t8_170  L_12 = L_11;
		Object_t * L_13 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_12);
		if (!((Object_t *)IsInst(L_13, IComparable_t1_1745_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		ConsoleLog_t8_170  L_14 = ___x;
		ConsoleLog_t8_170  L_15 = L_14;
		Object_t * L_16 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_15);
		ConsoleLog_t8_170  L_17 = ___y;
		ConsoleLog_t8_170  L_18 = L_17;
		Object_t * L_19 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_18);
		NullCheck((Object_t *)((Object_t *)Castclass(L_16, IComparable_t1_1745_il2cpp_TypeInfo_var)));
		int32_t L_20 = (int32_t)InterfaceFuncInvoker1< int32_t, Object_t * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t1_1745_il2cpp_TypeInfo_var, (Object_t *)((Object_t *)Castclass(L_16, IComparable_t1_1745_il2cpp_TypeInfo_var)), (Object_t *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t1_1425 * L_21 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13260(L_21, (String_t*)_stringLiteral791, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_21);
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::.ctor()
extern "C" void EqualityComparer_1__ctor_m1_27280_gshared (EqualityComparer_1_t1_2727 * __this, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1_0((Object_t *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::.cctor()
extern const Il2CppType* GenericEqualityComparer_1_t1_3064_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* TypeU5BU5D_t1_31_il2cpp_TypeInfo_var;
extern "C" void EqualityComparer_1__cctor_m1_27281_gshared (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GenericEqualityComparer_1_t1_3064_0_0_0_var = il2cpp_codegen_type_from_index(4654);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		TypeU5BU5D_t1_31_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(63);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, Type_t * >::Invoke(169 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GenericEqualityComparer_1_t1_3064_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1_31* L_4 = (TypeU5BU5D_t1_31*)((TypeU5BU5D_t1_31*)SZArrayNew(TypeU5BU5D_t1_31_il2cpp_TypeInfo_var, 1));
		Type_t * L_5 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_4, 0, sizeof(Type_t *))) = (Type_t *)L_5;
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = (Type_t *)VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1_31* >::Invoke(210 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1_31*)L_4);
		Object_t * L_7 = Activator_CreateInstance_m1_13034(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t1_2727_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->____default_0 = ((EqualityComparer_1_t1_2727 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t1_2728 * L_8 = (DefaultComparer_t1_2728 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		(( void (*) (DefaultComparer_t1_2728 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t1_2727_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->____default_0 = L_8;
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C" int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1_27282_gshared (EqualityComparer_1_t1_2727 * __this, Object_t * ___obj, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___obj;
		NullCheck((EqualityComparer_1_t1_2727 *)__this);
		int32_t L_1 = (int32_t)VirtFuncInvoker1< int32_t, ConsoleLog_t8_170  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::GetHashCode(T) */, (EqualityComparer_1_t1_2727 *)__this, (ConsoleLog_t8_170 )((*(ConsoleLog_t8_170 *)((ConsoleLog_t8_170 *)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C" bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1_27283_gshared (EqualityComparer_1_t1_2727 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___x;
		Object_t * L_1 = ___y;
		NullCheck((EqualityComparer_1_t1_2727 *)__this);
		bool L_2 = (bool)VirtFuncInvoker2< bool, ConsoleLog_t8_170 , ConsoleLog_t8_170  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Equals(T,T) */, (EqualityComparer_1_t1_2727 *)__this, (ConsoleLog_t8_170 )((*(ConsoleLog_t8_170 *)((ConsoleLog_t8_170 *)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6))))), (ConsoleLog_t8_170 )((*(ConsoleLog_t8_170 *)((ConsoleLog_t8_170 *)UnBox (L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::get_Default()
extern "C" EqualityComparer_1_t1_2727 * EqualityComparer_1_get_Default_m1_27284_gshared (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t1_2727 * L_0 = ((EqualityComparer_1_t1_2727_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->____default_0;
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<VoxelBusters.DebugPRO.Internal.ConsoleLog>::.ctor()
extern "C" void DefaultComparer__ctor_m1_27285_gshared (DefaultComparer_t1_2728 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t1_2727 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		(( void (*) (EqualityComparer_1_t1_2727 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((EqualityComparer_1_t1_2727 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<VoxelBusters.DebugPRO.Internal.ConsoleLog>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m1_27286_gshared (DefaultComparer_t1_2728 * __this, ConsoleLog_t8_170  ___obj, const MethodInfo* method)
{
	{
		ConsoleLog_t8_170  L_0 = ___obj;
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (&___obj)));
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (&___obj)));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m1_27287_gshared (DefaultComparer_t1_2728 * __this, ConsoleLog_t8_170  ___x, ConsoleLog_t8_170  ___y, const MethodInfo* method)
{
	{
		ConsoleLog_t8_170  L_0 = ___x;
		goto IL_0015;
	}
	{
		ConsoleLog_t8_170  L_1 = ___y;
		ConsoleLog_t8_170  L_2 = L_1;
		Object_t * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return ((((Object_t*)(Object_t *)L_3) == ((Object_t*)(Object_t *)NULL))? 1 : 0);
	}

IL_0015:
	{
		ConsoleLog_t8_170  L_4 = ___y;
		ConsoleLog_t8_170  L_5 = L_4;
		Object_t * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (&___x)));
		bool L_7 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (&___x)), (Object_t *)L_6);
		return L_7;
	}
}
// System.Void System.Predicate`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::.ctor(System.Object,System.IntPtr)
extern "C" void Predicate_1__ctor_m1_27288_gshared (Predicate_1_t1_2729 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Boolean System.Predicate`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Invoke(T)
extern "C" bool Predicate_1_Invoke_m1_27289_gshared (Predicate_1_t1_2729 * __this, ConsoleLog_t8_170  ___obj, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		Predicate_1_Invoke_m1_27289((Predicate_1_t1_2729 *)__this->___prev_9,___obj, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Object_t *, Object_t * __this, ConsoleLog_t8_170  ___obj, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___obj,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef bool (*FunctionPointerType) (Object_t * __this, ConsoleLog_t8_170  ___obj, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___obj,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult System.Predicate`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern TypeInfo* ConsoleLog_t8_170_il2cpp_TypeInfo_var;
extern "C" Object_t * Predicate_1_BeginInvoke_m1_27290_gshared (Predicate_1_t1_2729 * __this, ConsoleLog_t8_170  ___obj, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ConsoleLog_t8_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2096);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(ConsoleLog_t8_170_il2cpp_TypeInfo_var, &___obj);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Boolean System.Predicate`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::EndInvoke(System.IAsyncResult)
extern "C" bool Predicate_1_EndInvoke_m1_27291_gshared (Predicate_1_t1_2729 * __this, Object_t * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Action`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::.ctor(System.Object,System.IntPtr)
extern "C" void Action_1__ctor_m1_27292_gshared (Action_1_t1_2730 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void System.Action`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Invoke(T)
extern "C" void Action_1_Invoke_m1_27293_gshared (Action_1_t1_2730 * __this, ConsoleLog_t8_170  ___obj, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		Action_1_Invoke_m1_27293((Action_1_t1_2730 *)__this->___prev_9,___obj, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, ConsoleLog_t8_170  ___obj, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___obj,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, ConsoleLog_t8_170  ___obj, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___obj,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult System.Action`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern TypeInfo* ConsoleLog_t8_170_il2cpp_TypeInfo_var;
extern "C" Object_t * Action_1_BeginInvoke_m1_27294_gshared (Action_1_t1_2730 * __this, ConsoleLog_t8_170  ___obj, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ConsoleLog_t8_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2096);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(ConsoleLog_t8_170_il2cpp_TypeInfo_var, &___obj);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void System.Action`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::EndInvoke(System.IAsyncResult)
extern "C" void Action_1_EndInvoke_m1_27295_gshared (Action_1_t1_2730 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void System.Comparison`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::.ctor(System.Object,System.IntPtr)
extern "C" void Comparison_1__ctor_m1_27296_gshared (Comparison_1_t1_2731 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Int32 System.Comparison`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Invoke(T,T)
extern "C" int32_t Comparison_1_Invoke_m1_27297_gshared (Comparison_1_t1_2731 * __this, ConsoleLog_t8_170  ___x, ConsoleLog_t8_170  ___y, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		Comparison_1_Invoke_m1_27297((Comparison_1_t1_2731 *)__this->___prev_9,___x, ___y, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Object_t *, Object_t * __this, ConsoleLog_t8_170  ___x, ConsoleLog_t8_170  ___y, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___x, ___y,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (Object_t * __this, ConsoleLog_t8_170  ___x, ConsoleLog_t8_170  ___y, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___x, ___y,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult System.Comparison`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern TypeInfo* ConsoleLog_t8_170_il2cpp_TypeInfo_var;
extern "C" Object_t * Comparison_1_BeginInvoke_m1_27298_gshared (Comparison_1_t1_2731 * __this, ConsoleLog_t8_170  ___x, ConsoleLog_t8_170  ___y, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ConsoleLog_t8_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2096);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(ConsoleLog_t8_170_il2cpp_TypeInfo_var, &___x);
	__d_args[1] = Box(ConsoleLog_t8_170_il2cpp_TypeInfo_var, &___y);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Int32 System.Comparison`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::EndInvoke(System.IAsyncResult)
extern "C" int32_t Comparison_1_EndInvoke_m1_27299_gshared (Comparison_1_t1_2731 * __this, Object_t * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Array/InternalEnumerator`1<VoxelBusters.NativePlugins.eShareOptions>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_27443_gshared (InternalEnumerator_1_t1_2739 * __this, Array_t * ___array, const MethodInfo* method)
{
	{
		Array_t * L_0 = ___array;
		__this->___array_2 = L_0;
		__this->___idx_3 = ((int32_t)-2);
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<VoxelBusters.NativePlugins.eShareOptions>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_27444_gshared (InternalEnumerator_1_t1_2739 * __this, const MethodInfo* method)
{
	{
		__this->___idx_3 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<VoxelBusters.NativePlugins.eShareOptions>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_27445_gshared (InternalEnumerator_1_t1_2739 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (( int32_t (*) (InternalEnumerator_1_t1_2739 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t1_2739 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<VoxelBusters.NativePlugins.eShareOptions>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_27446_gshared (InternalEnumerator_1_t1_2739 * __this, const MethodInfo* method)
{
	{
		__this->___idx_3 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<VoxelBusters.NativePlugins.eShareOptions>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_27447_gshared (InternalEnumerator_1_t1_2739 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)(__this->___idx_3);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Array_t * L_1 = (Array_t *)(__this->___array_2);
		NullCheck((Array_t *)L_1);
		int32_t L_2 = Array_get_Length_m1_992((Array_t *)L_1, /*hidden argument*/NULL);
		__this->___idx_3 = L_2;
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)(__this->___idx_3);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)(__this->___idx_3);
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->___idx_3 = L_5;
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<VoxelBusters.NativePlugins.eShareOptions>::get_Current()
extern TypeInfo* InvalidOperationException_t1_1559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral217;
extern Il2CppCodeGenString* _stringLiteral218;
extern "C" int32_t InternalEnumerator_1_get_Current_m1_27448_gshared (InternalEnumerator_1_t1_2739 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvalidOperationException_t1_1559_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(78);
		_stringLiteral217 = il2cpp_codegen_string_literal_from_index(217);
		_stringLiteral218 = il2cpp_codegen_string_literal_from_index(218);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)(__this->___idx_3);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1_1559 * L_1 = (InvalidOperationException_t1_1559 *)il2cpp_codegen_object_new (InvalidOperationException_t1_1559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1_14171(L_1, (String_t*)_stringLiteral217, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)(__this->___idx_3);
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1_1559 * L_3 = (InvalidOperationException_t1_1559 *)il2cpp_codegen_object_new (InvalidOperationException_t1_1559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1_14171(L_3, (String_t*)_stringLiteral218, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_002f:
	{
		Array_t * L_4 = (Array_t *)(__this->___array_2);
		Array_t * L_5 = (Array_t *)(__this->___array_2);
		NullCheck((Array_t *)L_5);
		int32_t L_6 = Array_get_Length_m1_992((Array_t *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)(__this->___idx_3);
		NullCheck((Array_t *)L_4);
		int32_t L_8 = (( int32_t (*) (Array_t *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Array_t *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
