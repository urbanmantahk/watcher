﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>
struct ShimEnumerator_t1_2027;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t1_2018;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m1_16036_gshared (ShimEnumerator_t1_2027 * __this, Dictionary_2_t1_2018 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m1_16036(__this, ___host, method) (( void (*) (ShimEnumerator_t1_2027 *, Dictionary_2_t1_2018 *, const MethodInfo*))ShimEnumerator__ctor_m1_16036_gshared)(__this, ___host, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::Dispose()
extern "C" void ShimEnumerator_Dispose_m1_16037_gshared (ShimEnumerator_t1_2027 * __this, const MethodInfo* method);
#define ShimEnumerator_Dispose_m1_16037(__this, method) (( void (*) (ShimEnumerator_t1_2027 *, const MethodInfo*))ShimEnumerator_Dispose_m1_16037_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m1_16038_gshared (ShimEnumerator_t1_2027 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m1_16038(__this, method) (( bool (*) (ShimEnumerator_t1_2027 *, const MethodInfo*))ShimEnumerator_MoveNext_m1_16038_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Entry()
extern "C" DictionaryEntry_t1_284  ShimEnumerator_get_Entry_m1_16039_gshared (ShimEnumerator_t1_2027 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m1_16039(__this, method) (( DictionaryEntry_t1_284  (*) (ShimEnumerator_t1_2027 *, const MethodInfo*))ShimEnumerator_get_Entry_m1_16039_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m1_16040_gshared (ShimEnumerator_t1_2027 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m1_16040(__this, method) (( Object_t * (*) (ShimEnumerator_t1_2027 *, const MethodInfo*))ShimEnumerator_get_Key_m1_16040_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m1_16041_gshared (ShimEnumerator_t1_2027 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m1_16041(__this, method) (( Object_t * (*) (ShimEnumerator_t1_2027 *, const MethodInfo*))ShimEnumerator_get_Value_m1_16041_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m1_16042_gshared (ShimEnumerator_t1_2027 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m1_16042(__this, method) (( Object_t * (*) (ShimEnumerator_t1_2027 *, const MethodInfo*))ShimEnumerator_get_Current_m1_16042_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::Reset()
extern "C" void ShimEnumerator_Reset_m1_16043_gshared (ShimEnumerator_t1_2027 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m1_16043(__this, method) (( void (*) (ShimEnumerator_t1_2027 *, const MethodInfo*))ShimEnumerator_Reset_m1_16043_gshared)(__this, method)
