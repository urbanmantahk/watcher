﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.Cryptography.PKCS8/PrivateKeyInfo
struct PrivateKeyInfo_t1_169;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.String
struct String_t;
// System.Collections.ArrayList
struct ArrayList_t1_170;
// System.Security.Cryptography.RSA
struct RSA_t1_175;
// System.Security.Cryptography.DSA
struct DSA_t1_160;
// System.Security.Cryptography.AsymmetricAlgorithm
struct AsymmetricAlgorithm_t1_228;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Cryptography_DSAParameters.h"

// System.Void Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::.ctor()
extern "C" void PrivateKeyInfo__ctor_m1_2015 (PrivateKeyInfo_t1_169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::.ctor(System.Byte[])
extern "C" void PrivateKeyInfo__ctor_m1_2016 (PrivateKeyInfo_t1_169 * __this, ByteU5BU5D_t1_109* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::get_Algorithm()
extern "C" String_t* PrivateKeyInfo_get_Algorithm_m1_2017 (PrivateKeyInfo_t1_169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::set_Algorithm(System.String)
extern "C" void PrivateKeyInfo_set_Algorithm_m1_2018 (PrivateKeyInfo_t1_169 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::get_Attributes()
extern "C" ArrayList_t1_170 * PrivateKeyInfo_get_Attributes_m1_2019 (PrivateKeyInfo_t1_169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::get_PrivateKey()
extern "C" ByteU5BU5D_t1_109* PrivateKeyInfo_get_PrivateKey_m1_2020 (PrivateKeyInfo_t1_169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::set_PrivateKey(System.Byte[])
extern "C" void PrivateKeyInfo_set_PrivateKey_m1_2021 (PrivateKeyInfo_t1_169 * __this, ByteU5BU5D_t1_109* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::get_Version()
extern "C" int32_t PrivateKeyInfo_get_Version_m1_2022 (PrivateKeyInfo_t1_169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::set_Version(System.Int32)
extern "C" void PrivateKeyInfo_set_Version_m1_2023 (PrivateKeyInfo_t1_169 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::Decode(System.Byte[])
extern "C" void PrivateKeyInfo_Decode_m1_2024 (PrivateKeyInfo_t1_169 * __this, ByteU5BU5D_t1_109* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::GetBytes()
extern "C" ByteU5BU5D_t1_109* PrivateKeyInfo_GetBytes_m1_2025 (PrivateKeyInfo_t1_169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::RemoveLeadingZero(System.Byte[])
extern "C" ByteU5BU5D_t1_109* PrivateKeyInfo_RemoveLeadingZero_m1_2026 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___bigInt, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::Normalize(System.Byte[],System.Int32)
extern "C" ByteU5BU5D_t1_109* PrivateKeyInfo_Normalize_m1_2027 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___bigInt, int32_t ___length, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.RSA Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::DecodeRSA(System.Byte[])
extern "C" RSA_t1_175 * PrivateKeyInfo_DecodeRSA_m1_2028 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___keypair, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::Encode(System.Security.Cryptography.RSA)
extern "C" ByteU5BU5D_t1_109* PrivateKeyInfo_Encode_m1_2029 (Object_t * __this /* static, unused */, RSA_t1_175 * ___rsa, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.DSA Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::DecodeDSA(System.Byte[],System.Security.Cryptography.DSAParameters)
extern "C" DSA_t1_160 * PrivateKeyInfo_DecodeDSA_m1_2030 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___privateKey, DSAParameters_t1_1204  ___dsaParameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::Encode(System.Security.Cryptography.DSA)
extern "C" ByteU5BU5D_t1_109* PrivateKeyInfo_Encode_m1_2031 (Object_t * __this /* static, unused */, DSA_t1_160 * ___dsa, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::Encode(System.Security.Cryptography.AsymmetricAlgorithm)
extern "C" ByteU5BU5D_t1_109* PrivateKeyInfo_Encode_m1_2032 (Object_t * __this /* static, unused */, AsymmetricAlgorithm_t1_228 * ___aa, const MethodInfo* method) IL2CPP_METHOD_ATTR;
