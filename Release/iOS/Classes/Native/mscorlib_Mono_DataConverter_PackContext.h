﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.String
struct String_t;
// Mono.DataConverter
struct DataConverter_t1_252;

#include "mscorlib_System_Object.h"

// Mono.DataConverter/PackContext
struct  PackContext_t1_251  : public Object_t
{
	// System.Byte[] Mono.DataConverter/PackContext::buffer
	ByteU5BU5D_t1_109* ___buffer_0;
	// System.Int32 Mono.DataConverter/PackContext::next
	int32_t ___next_1;
	// System.String Mono.DataConverter/PackContext::description
	String_t* ___description_2;
	// System.Int32 Mono.DataConverter/PackContext::i
	int32_t ___i_3;
	// Mono.DataConverter Mono.DataConverter/PackContext::conv
	DataConverter_t1_252 * ___conv_4;
	// System.Int32 Mono.DataConverter/PackContext::repeat
	int32_t ___repeat_5;
	// System.Int32 Mono.DataConverter/PackContext::align
	int32_t ___align_6;
};
