﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Policy.SiteMembershipCondition
struct SiteMembershipCondition_t1_1362;
// System.String
struct String_t;
// System.Security.Policy.Evidence
struct Evidence_t1_398;
// System.Security.Policy.IMembershipCondition
struct IMembershipCondition_t1_1340;
// System.Object
struct Object_t;
// System.Security.SecurityElement
struct SecurityElement_t1_242;
// System.Security.Policy.PolicyLevel
struct PolicyLevel_t1_1357;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Policy.SiteMembershipCondition::.ctor()
extern "C" void SiteMembershipCondition__ctor_m1_11670 (SiteMembershipCondition_t1_1362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.SiteMembershipCondition::.ctor(System.String)
extern "C" void SiteMembershipCondition__ctor_m1_11671 (SiteMembershipCondition_t1_1362 * __this, String_t* ___site, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Policy.SiteMembershipCondition::get_Site()
extern "C" String_t* SiteMembershipCondition_get_Site_m1_11672 (SiteMembershipCondition_t1_1362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.SiteMembershipCondition::set_Site(System.String)
extern "C" void SiteMembershipCondition_set_Site_m1_11673 (SiteMembershipCondition_t1_1362 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Policy.SiteMembershipCondition::Check(System.Security.Policy.Evidence)
extern "C" bool SiteMembershipCondition_Check_m1_11674 (SiteMembershipCondition_t1_1362 * __this, Evidence_t1_398 * ___evidence, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.IMembershipCondition System.Security.Policy.SiteMembershipCondition::Copy()
extern "C" Object_t * SiteMembershipCondition_Copy_m1_11675 (SiteMembershipCondition_t1_1362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Policy.SiteMembershipCondition::Equals(System.Object)
extern "C" bool SiteMembershipCondition_Equals_m1_11676 (SiteMembershipCondition_t1_1362 * __this, Object_t * ___o, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.SiteMembershipCondition::FromXml(System.Security.SecurityElement)
extern "C" void SiteMembershipCondition_FromXml_m1_11677 (SiteMembershipCondition_t1_1362 * __this, SecurityElement_t1_242 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.SiteMembershipCondition::FromXml(System.Security.SecurityElement,System.Security.Policy.PolicyLevel)
extern "C" void SiteMembershipCondition_FromXml_m1_11678 (SiteMembershipCondition_t1_1362 * __this, SecurityElement_t1_242 * ___e, PolicyLevel_t1_1357 * ___level, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Policy.SiteMembershipCondition::GetHashCode()
extern "C" int32_t SiteMembershipCondition_GetHashCode_m1_11679 (SiteMembershipCondition_t1_1362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Policy.SiteMembershipCondition::ToString()
extern "C" String_t* SiteMembershipCondition_ToString_m1_11680 (SiteMembershipCondition_t1_1362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityElement System.Security.Policy.SiteMembershipCondition::ToXml()
extern "C" SecurityElement_t1_242 * SiteMembershipCondition_ToXml_m1_11681 (SiteMembershipCondition_t1_1362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityElement System.Security.Policy.SiteMembershipCondition::ToXml(System.Security.Policy.PolicyLevel)
extern "C" SecurityElement_t1_242 * SiteMembershipCondition_ToXml_m1_11682 (SiteMembershipCondition_t1_1362 * __this, PolicyLevel_t1_1357 * ___level, const MethodInfo* method) IL2CPP_METHOD_ATTR;
