﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.AccessControl.AccessRule
struct AccessRule_t1_1111;
// System.Security.Principal.IdentityReference
struct IdentityReference_t1_1120;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_AccessControl_InheritanceFlags.h"
#include "mscorlib_System_Security_AccessControl_PropagationFlags.h"
#include "mscorlib_System_Security_AccessControl_AccessControlType.h"

// System.Void System.Security.AccessControl.AccessRule::.ctor(System.Security.Principal.IdentityReference,System.Int32,System.Boolean,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.AccessControlType)
extern "C" void AccessRule__ctor_m1_9706 (AccessRule_t1_1111 * __this, IdentityReference_t1_1120 * ___identity, int32_t ___accessMask, bool ___isInherited, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.AccessControl.AccessControlType System.Security.AccessControl.AccessRule::get_AccessControlType()
extern "C" int32_t AccessRule_get_AccessControlType_m1_9707 (AccessRule_t1_1111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
