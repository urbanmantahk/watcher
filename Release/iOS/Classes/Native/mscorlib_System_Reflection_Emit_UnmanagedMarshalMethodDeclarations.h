﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.Emit.UnmanagedMarshal
struct UnmanagedMarshal_t1_505;
// System.Type
struct Type_t;
// System.String
struct String_t;
// System.Runtime.InteropServices.MarshalAsAttribute
struct MarshalAsAttribute_t1_42;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_InteropServices_UnmanagedType.h"
#include "mscorlib_System_Guid.h"

// System.Void System.Reflection.Emit.UnmanagedMarshal::.ctor(System.Runtime.InteropServices.UnmanagedType,System.Int32)
extern "C" void UnmanagedMarshal__ctor_m1_6544 (UnmanagedMarshal_t1_505 * __this, int32_t ___maint, int32_t ___cnt, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.UnmanagedMarshal::.ctor(System.Runtime.InteropServices.UnmanagedType,System.Runtime.InteropServices.UnmanagedType)
extern "C" void UnmanagedMarshal__ctor_m1_6545 (UnmanagedMarshal_t1_505 * __this, int32_t ___maint, int32_t ___elemt, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.InteropServices.UnmanagedType System.Reflection.Emit.UnmanagedMarshal::get_BaseType()
extern "C" int32_t UnmanagedMarshal_get_BaseType_m1_6546 (UnmanagedMarshal_t1_505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.UnmanagedMarshal::get_ElementCount()
extern "C" int32_t UnmanagedMarshal_get_ElementCount_m1_6547 (UnmanagedMarshal_t1_505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.InteropServices.UnmanagedType System.Reflection.Emit.UnmanagedMarshal::get_GetUnmanagedType()
extern "C" int32_t UnmanagedMarshal_get_GetUnmanagedType_m1_6548 (UnmanagedMarshal_t1_505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Guid System.Reflection.Emit.UnmanagedMarshal::get_IIDGuid()
extern "C" Guid_t1_319  UnmanagedMarshal_get_IIDGuid_m1_6549 (UnmanagedMarshal_t1_505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.UnmanagedMarshal System.Reflection.Emit.UnmanagedMarshal::DefineByValArray(System.Int32)
extern "C" UnmanagedMarshal_t1_505 * UnmanagedMarshal_DefineByValArray_m1_6550 (Object_t * __this /* static, unused */, int32_t ___elemCount, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.UnmanagedMarshal System.Reflection.Emit.UnmanagedMarshal::DefineByValTStr(System.Int32)
extern "C" UnmanagedMarshal_t1_505 * UnmanagedMarshal_DefineByValTStr_m1_6551 (Object_t * __this /* static, unused */, int32_t ___elemCount, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.UnmanagedMarshal System.Reflection.Emit.UnmanagedMarshal::DefineLPArray(System.Runtime.InteropServices.UnmanagedType)
extern "C" UnmanagedMarshal_t1_505 * UnmanagedMarshal_DefineLPArray_m1_6552 (Object_t * __this /* static, unused */, int32_t ___elemType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.UnmanagedMarshal System.Reflection.Emit.UnmanagedMarshal::DefineSafeArray(System.Runtime.InteropServices.UnmanagedType)
extern "C" UnmanagedMarshal_t1_505 * UnmanagedMarshal_DefineSafeArray_m1_6553 (Object_t * __this /* static, unused */, int32_t ___elemType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.UnmanagedMarshal System.Reflection.Emit.UnmanagedMarshal::DefineUnmanagedMarshal(System.Runtime.InteropServices.UnmanagedType)
extern "C" UnmanagedMarshal_t1_505 * UnmanagedMarshal_DefineUnmanagedMarshal_m1_6554 (Object_t * __this /* static, unused */, int32_t ___unmanagedType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.UnmanagedMarshal System.Reflection.Emit.UnmanagedMarshal::DefineCustom(System.Type,System.String,System.String,System.Guid)
extern "C" UnmanagedMarshal_t1_505 * UnmanagedMarshal_DefineCustom_m1_6555 (Object_t * __this /* static, unused */, Type_t * ___typeref, String_t* ___cookie, String_t* ___mtype, Guid_t1_319  ___id, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.UnmanagedMarshal System.Reflection.Emit.UnmanagedMarshal::DefineLPArrayInternal(System.Runtime.InteropServices.UnmanagedType,System.Int32,System.Int32)
extern "C" UnmanagedMarshal_t1_505 * UnmanagedMarshal_DefineLPArrayInternal_m1_6556 (Object_t * __this /* static, unused */, int32_t ___elemType, int32_t ___sizeConst, int32_t ___sizeParamIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.InteropServices.MarshalAsAttribute System.Reflection.Emit.UnmanagedMarshal::ToMarshalAsAttribute()
extern "C" MarshalAsAttribute_t1_42 * UnmanagedMarshal_ToMarshalAsAttribute_m1_6557 (UnmanagedMarshal_t1_505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
