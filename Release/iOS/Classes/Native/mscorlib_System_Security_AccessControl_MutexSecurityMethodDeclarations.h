﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.AccessControl.MutexSecurity
struct MutexSecurity_t1_1161;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Security.AccessControl.AccessRule
struct AccessRule_t1_1111;
// System.Security.Principal.IdentityReference
struct IdentityReference_t1_1120;
// System.Security.AccessControl.MutexAccessRule
struct MutexAccessRule_t1_1158;
// System.Security.AccessControl.AuditRule
struct AuditRule_t1_1119;
// System.Security.AccessControl.MutexAuditRule
struct MutexAuditRule_t1_1159;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_AccessControl_AccessControlSections.h"
#include "mscorlib_System_Security_AccessControl_InheritanceFlags.h"
#include "mscorlib_System_Security_AccessControl_PropagationFlags.h"
#include "mscorlib_System_Security_AccessControl_AccessControlType.h"
#include "mscorlib_System_Security_AccessControl_AuditFlags.h"

// System.Void System.Security.AccessControl.MutexSecurity::.ctor()
extern "C" void MutexSecurity__ctor_m1_9937 (MutexSecurity_t1_1161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.MutexSecurity::.ctor(System.String,System.Security.AccessControl.AccessControlSections)
extern "C" void MutexSecurity__ctor_m1_9938 (MutexSecurity_t1_1161 * __this, String_t* ___name, int32_t ___includeSections, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Security.AccessControl.MutexSecurity::get_AccessRightType()
extern "C" Type_t * MutexSecurity_get_AccessRightType_m1_9939 (MutexSecurity_t1_1161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Security.AccessControl.MutexSecurity::get_AccessRuleType()
extern "C" Type_t * MutexSecurity_get_AccessRuleType_m1_9940 (MutexSecurity_t1_1161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Security.AccessControl.MutexSecurity::get_AuditRuleType()
extern "C" Type_t * MutexSecurity_get_AuditRuleType_m1_9941 (MutexSecurity_t1_1161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.AccessControl.AccessRule System.Security.AccessControl.MutexSecurity::AccessRuleFactory(System.Security.Principal.IdentityReference,System.Int32,System.Boolean,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.AccessControlType)
extern "C" AccessRule_t1_1111 * MutexSecurity_AccessRuleFactory_m1_9942 (MutexSecurity_t1_1161 * __this, IdentityReference_t1_1120 * ___identityReference, int32_t ___accessMask, bool ___isInherited, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.MutexSecurity::AddAccessRule(System.Security.AccessControl.MutexAccessRule)
extern "C" void MutexSecurity_AddAccessRule_m1_9943 (MutexSecurity_t1_1161 * __this, MutexAccessRule_t1_1158 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.AccessControl.MutexSecurity::RemoveAccessRule(System.Security.AccessControl.MutexAccessRule)
extern "C" bool MutexSecurity_RemoveAccessRule_m1_9944 (MutexSecurity_t1_1161 * __this, MutexAccessRule_t1_1158 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.MutexSecurity::RemoveAccessRuleAll(System.Security.AccessControl.MutexAccessRule)
extern "C" void MutexSecurity_RemoveAccessRuleAll_m1_9945 (MutexSecurity_t1_1161 * __this, MutexAccessRule_t1_1158 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.MutexSecurity::RemoveAccessRuleSpecific(System.Security.AccessControl.MutexAccessRule)
extern "C" void MutexSecurity_RemoveAccessRuleSpecific_m1_9946 (MutexSecurity_t1_1161 * __this, MutexAccessRule_t1_1158 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.MutexSecurity::ResetAccessRule(System.Security.AccessControl.MutexAccessRule)
extern "C" void MutexSecurity_ResetAccessRule_m1_9947 (MutexSecurity_t1_1161 * __this, MutexAccessRule_t1_1158 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.MutexSecurity::SetAccessRule(System.Security.AccessControl.MutexAccessRule)
extern "C" void MutexSecurity_SetAccessRule_m1_9948 (MutexSecurity_t1_1161 * __this, MutexAccessRule_t1_1158 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.AccessControl.AuditRule System.Security.AccessControl.MutexSecurity::AuditRuleFactory(System.Security.Principal.IdentityReference,System.Int32,System.Boolean,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.AuditFlags)
extern "C" AuditRule_t1_1119 * MutexSecurity_AuditRuleFactory_m1_9949 (MutexSecurity_t1_1161 * __this, IdentityReference_t1_1120 * ___identityReference, int32_t ___accessMask, bool ___isInherited, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___flags, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.MutexSecurity::AddAuditRule(System.Security.AccessControl.MutexAuditRule)
extern "C" void MutexSecurity_AddAuditRule_m1_9950 (MutexSecurity_t1_1161 * __this, MutexAuditRule_t1_1159 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.AccessControl.MutexSecurity::RemoveAuditRule(System.Security.AccessControl.MutexAuditRule)
extern "C" bool MutexSecurity_RemoveAuditRule_m1_9951 (MutexSecurity_t1_1161 * __this, MutexAuditRule_t1_1159 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.MutexSecurity::RemoveAuditRuleAll(System.Security.AccessControl.MutexAuditRule)
extern "C" void MutexSecurity_RemoveAuditRuleAll_m1_9952 (MutexSecurity_t1_1161 * __this, MutexAuditRule_t1_1159 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.MutexSecurity::RemoveAuditRuleSpecific(System.Security.AccessControl.MutexAuditRule)
extern "C" void MutexSecurity_RemoveAuditRuleSpecific_m1_9953 (MutexSecurity_t1_1161 * __this, MutexAuditRule_t1_1159 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.MutexSecurity::SetAuditRule(System.Security.AccessControl.MutexAuditRule)
extern "C" void MutexSecurity_SetAuditRule_m1_9954 (MutexSecurity_t1_1161 * __this, MutexAuditRule_t1_1159 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
