﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.MonoArrayMethod
struct MonoArrayMethod_t1_537;
// System.Type
struct Type_t;
// System.String
struct String_t;
// System.Type[]
struct TypeU5BU5D_t1_31;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Reflection.ICustomAttributeProvider
struct ICustomAttributeProvider_t1_1686;
// System.Reflection.ParameterInfo[]
struct ParameterInfoU5BU5D_t1_1685;
// System.Object
struct Object_t;
// System.Reflection.Binder
struct Binder_t1_585;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Globalization.CultureInfo
struct CultureInfo_t1_277;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_CallingConventions.h"
#include "mscorlib_System_Reflection_MethodImplAttributes.h"
#include "mscorlib_System_Reflection_BindingFlags.h"
#include "mscorlib_System_RuntimeMethodHandle.h"
#include "mscorlib_System_Reflection_MethodAttributes.h"

// System.Void System.Reflection.MonoArrayMethod::.ctor(System.Type,System.String,System.Reflection.CallingConventions,System.Type,System.Type[])
extern "C" void MonoArrayMethod__ctor_m1_6238 (MonoArrayMethod_t1_537 * __this, Type_t * ___arrayClass, String_t* ___methodName, int32_t ___callingConvention, Type_t * ___returnType, TypeU5BU5D_t1_31* ___parameterTypes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Reflection.MonoArrayMethod::GetBaseDefinition()
extern "C" MethodInfo_t * MonoArrayMethod_GetBaseDefinition_m1_6239 (MonoArrayMethod_t1_537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.MonoArrayMethod::get_ReturnType()
extern "C" Type_t * MonoArrayMethod_get_ReturnType_m1_6240 (MonoArrayMethod_t1_537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ICustomAttributeProvider System.Reflection.MonoArrayMethod::get_ReturnTypeCustomAttributes()
extern "C" Object_t * MonoArrayMethod_get_ReturnTypeCustomAttributes_m1_6241 (MonoArrayMethod_t1_537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodImplAttributes System.Reflection.MonoArrayMethod::GetMethodImplementationFlags()
extern "C" int32_t MonoArrayMethod_GetMethodImplementationFlags_m1_6242 (MonoArrayMethod_t1_537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ParameterInfo[] System.Reflection.MonoArrayMethod::GetParameters()
extern "C" ParameterInfoU5BU5D_t1_1685* MonoArrayMethod_GetParameters_m1_6243 (MonoArrayMethod_t1_537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Reflection.MonoArrayMethod::Invoke(System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo)
extern "C" Object_t * MonoArrayMethod_Invoke_m1_6244 (MonoArrayMethod_t1_537 * __this, Object_t * ___obj, int32_t ___invokeAttr, Binder_t1_585 * ___binder, ObjectU5BU5D_t1_272* ___parameters, CultureInfo_t1_277 * ___culture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.RuntimeMethodHandle System.Reflection.MonoArrayMethod::get_MethodHandle()
extern "C" RuntimeMethodHandle_t1_479  MonoArrayMethod_get_MethodHandle_m1_6245 (MonoArrayMethod_t1_537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodAttributes System.Reflection.MonoArrayMethod::get_Attributes()
extern "C" int32_t MonoArrayMethod_get_Attributes_m1_6246 (MonoArrayMethod_t1_537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.MonoArrayMethod::get_ReflectedType()
extern "C" Type_t * MonoArrayMethod_get_ReflectedType_m1_6247 (MonoArrayMethod_t1_537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.MonoArrayMethod::get_DeclaringType()
extern "C" Type_t * MonoArrayMethod_get_DeclaringType_m1_6248 (MonoArrayMethod_t1_537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.MonoArrayMethod::get_Name()
extern "C" String_t* MonoArrayMethod_get_Name_m1_6249 (MonoArrayMethod_t1_537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.MonoArrayMethod::IsDefined(System.Type,System.Boolean)
extern "C" bool MonoArrayMethod_IsDefined_m1_6250 (MonoArrayMethod_t1_537 * __this, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Reflection.MonoArrayMethod::GetCustomAttributes(System.Boolean)
extern "C" ObjectU5BU5D_t1_272* MonoArrayMethod_GetCustomAttributes_m1_6251 (MonoArrayMethod_t1_537 * __this, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Reflection.MonoArrayMethod::GetCustomAttributes(System.Type,System.Boolean)
extern "C" ObjectU5BU5D_t1_272* MonoArrayMethod_GetCustomAttributes_m1_6252 (MonoArrayMethod_t1_537 * __this, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.MonoArrayMethod::ToString()
extern "C" String_t* MonoArrayMethod_ToString_m1_6253 (MonoArrayMethod_t1_537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
