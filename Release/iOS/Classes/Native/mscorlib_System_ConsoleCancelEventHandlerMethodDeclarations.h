﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.ConsoleCancelEventHandler
struct ConsoleCancelEventHandler_t1_1630;
// System.Object
struct Object_t;
// System.ConsoleCancelEventArgs
struct ConsoleCancelEventArgs_t1_1513;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"

// System.Void System.ConsoleCancelEventHandler::.ctor(System.Object,System.IntPtr)
extern "C" void ConsoleCancelEventHandler__ctor_m1_14855 (ConsoleCancelEventHandler_t1_1630 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ConsoleCancelEventHandler::Invoke(System.Object,System.ConsoleCancelEventArgs)
extern "C" void ConsoleCancelEventHandler_Invoke_m1_14856 (ConsoleCancelEventHandler_t1_1630 * __this, Object_t * ___sender, ConsoleCancelEventArgs_t1_1513 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_ConsoleCancelEventHandler_t1_1630(Il2CppObject* delegate, Object_t * ___sender, ConsoleCancelEventArgs_t1_1513 * ___e);
// System.IAsyncResult System.ConsoleCancelEventHandler::BeginInvoke(System.Object,System.ConsoleCancelEventArgs,System.AsyncCallback,System.Object)
extern "C" Object_t * ConsoleCancelEventHandler_BeginInvoke_m1_14857 (ConsoleCancelEventHandler_t1_1630 * __this, Object_t * ___sender, ConsoleCancelEventArgs_t1_1513 * ___e, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ConsoleCancelEventHandler::EndInvoke(System.IAsyncResult)
extern "C" void ConsoleCancelEventHandler_EndInvoke_m1_14858 (ConsoleCancelEventHandler_t1_1630 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
