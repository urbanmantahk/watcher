﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Threading.CompressedStack
struct CompressedStack_t1_1395;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;
// System.Threading.ContextCallback
struct ContextCallback_t1_1624;
// System.Object
struct Object_t;
// System.Collections.IList
struct IList_t1_262;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.Threading.CompressedStack::.ctor(System.Int32)
extern "C" void CompressedStack__ctor_m1_12624 (CompressedStack_t1_1395 * __this, int32_t ___length, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.CompressedStack::.ctor(System.Threading.CompressedStack)
extern "C" void CompressedStack__ctor_m1_12625 (CompressedStack_t1_1395 * __this, CompressedStack_t1_1395 * ___cs, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.CompressedStack System.Threading.CompressedStack::CreateCopy()
extern "C" CompressedStack_t1_1395 * CompressedStack_CreateCopy_m1_12626 (CompressedStack_t1_1395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.CompressedStack System.Threading.CompressedStack::Capture()
extern "C" CompressedStack_t1_1395 * CompressedStack_Capture_m1_12627 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.CompressedStack System.Threading.CompressedStack::GetCompressedStack()
extern "C" CompressedStack_t1_1395 * CompressedStack_GetCompressedStack_m1_12628 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.CompressedStack::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void CompressedStack_GetObjectData_m1_12629 (CompressedStack_t1_1395 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.CompressedStack::Run(System.Threading.CompressedStack,System.Threading.ContextCallback,System.Object)
extern "C" void CompressedStack_Run_m1_12630 (Object_t * __this /* static, unused */, CompressedStack_t1_1395 * ___compressedStack, ContextCallback_t1_1624 * ___callback, Object_t * ___state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.CompressedStack::Equals(System.Threading.CompressedStack)
extern "C" bool CompressedStack_Equals_m1_12631 (CompressedStack_t1_1395 * __this, CompressedStack_t1_1395 * ___cs, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.CompressedStack::IsEmpty()
extern "C" bool CompressedStack_IsEmpty_m1_12632 (CompressedStack_t1_1395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IList System.Threading.CompressedStack::get_List()
extern "C" Object_t * CompressedStack_get_List_m1_12633 (CompressedStack_t1_1395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
