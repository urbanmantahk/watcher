﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object.h"
#include "mscorlib_System_Decimal.h"

// System.Runtime.InteropServices.CurrencyWrapper
struct  CurrencyWrapper_t1_777  : public Object_t
{
	// System.Decimal System.Runtime.InteropServices.CurrencyWrapper::currency
	Decimal_t1_19  ___currency_0;
};
