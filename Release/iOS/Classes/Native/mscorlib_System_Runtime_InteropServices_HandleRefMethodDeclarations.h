﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"
#include "mscorlib_System_IntPtr.h"

// System.Void System.Runtime.InteropServices.HandleRef::.ctor(System.Object,System.IntPtr)
extern "C" void HandleRef__ctor_m1_7672 (HandleRef_t1_797 * __this, Object_t * ___wrapper, IntPtr_t ___handle, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.HandleRef::get_Handle()
extern "C" IntPtr_t HandleRef_get_Handle_m1_7673 (HandleRef_t1_797 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.InteropServices.HandleRef::get_Wrapper()
extern "C" Object_t * HandleRef_get_Wrapper_m1_7674 (HandleRef_t1_797 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.HandleRef::ToIntPtr(System.Runtime.InteropServices.HandleRef)
extern "C" IntPtr_t HandleRef_ToIntPtr_m1_7675 (Object_t * __this /* static, unused */, HandleRef_t1_797  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.HandleRef::op_Explicit(System.Runtime.InteropServices.HandleRef)
extern "C" IntPtr_t HandleRef_op_Explicit_m1_7676 (Object_t * __this /* static, unused */, HandleRef_t1_797  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
