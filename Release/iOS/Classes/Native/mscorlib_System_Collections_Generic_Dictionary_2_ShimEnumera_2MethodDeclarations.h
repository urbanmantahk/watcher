﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>
struct ShimEnumerator_t1_2395;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t1_1830;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m1_21006_gshared (ShimEnumerator_t1_2395 * __this, Dictionary_2_t1_1830 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m1_21006(__this, ___host, method) (( void (*) (ShimEnumerator_t1_2395 *, Dictionary_2_t1_1830 *, const MethodInfo*))ShimEnumerator__ctor_m1_21006_gshared)(__this, ___host, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::Dispose()
extern "C" void ShimEnumerator_Dispose_m1_21007_gshared (ShimEnumerator_t1_2395 * __this, const MethodInfo* method);
#define ShimEnumerator_Dispose_m1_21007(__this, method) (( void (*) (ShimEnumerator_t1_2395 *, const MethodInfo*))ShimEnumerator_Dispose_m1_21007_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m1_21008_gshared (ShimEnumerator_t1_2395 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m1_21008(__this, method) (( bool (*) (ShimEnumerator_t1_2395 *, const MethodInfo*))ShimEnumerator_MoveNext_m1_21008_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Entry()
extern "C" DictionaryEntry_t1_284  ShimEnumerator_get_Entry_m1_21009_gshared (ShimEnumerator_t1_2395 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m1_21009(__this, method) (( DictionaryEntry_t1_284  (*) (ShimEnumerator_t1_2395 *, const MethodInfo*))ShimEnumerator_get_Entry_m1_21009_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m1_21010_gshared (ShimEnumerator_t1_2395 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m1_21010(__this, method) (( Object_t * (*) (ShimEnumerator_t1_2395 *, const MethodInfo*))ShimEnumerator_get_Key_m1_21010_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m1_21011_gshared (ShimEnumerator_t1_2395 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m1_21011(__this, method) (( Object_t * (*) (ShimEnumerator_t1_2395 *, const MethodInfo*))ShimEnumerator_get_Value_m1_21011_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m1_21012_gshared (ShimEnumerator_t1_2395 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m1_21012(__this, method) (( Object_t * (*) (ShimEnumerator_t1_2395 *, const MethodInfo*))ShimEnumerator_get_Current_m1_21012_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::Reset()
extern "C" void ShimEnumerator_Reset_m1_21013_gshared (ShimEnumerator_t1_2395 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m1_21013(__this, method) (( void (*) (ShimEnumerator_t1_2395 *, const MethodInfo*))ShimEnumerator_Reset_m1_21013_gshared)(__this, method)
