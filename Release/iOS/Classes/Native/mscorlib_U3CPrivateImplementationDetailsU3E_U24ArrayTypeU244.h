﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType.h"

// <PrivateImplementationDetails>/$ArrayType$40
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU2440_t1_1635 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU2440_t1_1635__padding[40];
	};
};
#pragma pack(pop, tp)
