﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Security_Policy_DefaultPolicies_Key.h"

// System.Security.Policy.DefaultPolicies/Key
struct  Key_t1_1342 
{
	// System.Int32 System.Security.Policy.DefaultPolicies/Key::value__
	int32_t ___value___1;
};
