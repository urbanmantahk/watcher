﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Globalization_CCHijriCalendar_Month.h"

// System.Globalization.CCHijriCalendar/Month
struct  Month_t1_349 
{
	// System.Int32 System.Globalization.CCHijriCalendar/Month::value__
	int32_t ___value___1;
};
