﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.Emit.DynamicMethodTokenGenerator
struct DynamicMethodTokenGenerator_t1_497;
// System.Reflection.Emit.DynamicMethod
struct DynamicMethod_t1_496;
// System.String
struct String_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Type[]
struct TypeU5BU5D_t1_31;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Reflection.Emit.SignatureHelper
struct SignatureHelper_t1_551;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Reflection.Emit.DynamicMethodTokenGenerator::.ctor(System.Reflection.Emit.DynamicMethod)
extern "C" void DynamicMethodTokenGenerator__ctor_m1_5731 (DynamicMethodTokenGenerator_t1_497 * __this, DynamicMethod_t1_496 * ___m, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.DynamicMethodTokenGenerator::GetToken(System.String)
extern "C" int32_t DynamicMethodTokenGenerator_GetToken_m1_5732 (DynamicMethodTokenGenerator_t1_497 * __this, String_t* ___str, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.DynamicMethodTokenGenerator::GetToken(System.Reflection.MethodInfo,System.Type[])
extern "C" int32_t DynamicMethodTokenGenerator_GetToken_m1_5733 (DynamicMethodTokenGenerator_t1_497 * __this, MethodInfo_t * ___method, TypeU5BU5D_t1_31* ___opt_param_types, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.DynamicMethodTokenGenerator::GetToken(System.Reflection.MemberInfo)
extern "C" int32_t DynamicMethodTokenGenerator_GetToken_m1_5734 (DynamicMethodTokenGenerator_t1_497 * __this, MemberInfo_t * ___member, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.DynamicMethodTokenGenerator::GetToken(System.Reflection.Emit.SignatureHelper)
extern "C" int32_t DynamicMethodTokenGenerator_GetToken_m1_5735 (DynamicMethodTokenGenerator_t1_497 * __this, SignatureHelper_t1_551 * ___helper, const MethodInfo* method) IL2CPP_METHOD_ATTR;
