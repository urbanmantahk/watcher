﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Security.Policy.IApplicationTrustManager
struct IApplicationTrustManager_t1_1332;
// System.Security.Policy.ApplicationTrustCollection
struct ApplicationTrustCollection_t1_1331;

#include "mscorlib_System_Object.h"

// System.Security.Policy.ApplicationSecurityManager
struct  ApplicationSecurityManager_t1_1330  : public Object_t
{
};
struct ApplicationSecurityManager_t1_1330_StaticFields{
	// System.Security.Policy.IApplicationTrustManager System.Security.Policy.ApplicationSecurityManager::_appTrustManager
	Object_t * ____appTrustManager_1;
	// System.Security.Policy.ApplicationTrustCollection System.Security.Policy.ApplicationSecurityManager::_userAppTrusts
	ApplicationTrustCollection_t1_1331 * ____userAppTrusts_2;
};
