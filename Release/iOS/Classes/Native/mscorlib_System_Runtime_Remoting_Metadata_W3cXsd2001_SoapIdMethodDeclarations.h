﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapId
struct SoapId_t1_975;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapId::.ctor()
extern "C" void SoapId__ctor_m1_8742 (SoapId_t1_975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapId::.ctor(System.String)
extern "C" void SoapId__ctor_m1_8743 (SoapId_t1_975 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapId::get_Value()
extern "C" String_t* SoapId_get_Value_m1_8744 (SoapId_t1_975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapId::set_Value(System.String)
extern "C" void SoapId_set_Value_m1_8745 (SoapId_t1_975 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapId::get_XsdType()
extern "C" String_t* SoapId_get_XsdType_m1_8746 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapId::GetXsdType()
extern "C" String_t* SoapId_GetXsdType_m1_8747 (SoapId_t1_975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapId System.Runtime.Remoting.Metadata.W3cXsd2001.SoapId::Parse(System.String)
extern "C" SoapId_t1_975 * SoapId_Parse_m1_8748 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapId::ToString()
extern "C" String_t* SoapId_ToString_m1_8749 (SoapId_t1_975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
