﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NPBinding
struct NPBinding_t8_333;
// VoxelBusters.NativePlugins.NotificationService
struct NotificationService_t8_261;
// VoxelBusters.NativePlugins.UI
struct UI_t8_303;
// VoxelBusters.NativePlugins.Utility
struct Utility_t8_306;

#include "codegen/il2cpp-codegen.h"

// System.Void NPBinding::.ctor()
extern "C" void NPBinding__ctor_m8_1928 (NPBinding_t8_333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.NativePlugins.NotificationService NPBinding::get_NotificationService()
extern "C" NotificationService_t8_261 * NPBinding_get_NotificationService_m8_1929 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.NativePlugins.UI NPBinding::get_UI()
extern "C" UI_t8_303 * NPBinding_get_UI_m8_1930 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.NativePlugins.Utility NPBinding::get_Utility()
extern "C" Utility_t8_306 * NPBinding_get_Utility_m8_1931 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NPBinding::Init()
extern "C" void NPBinding_Init_m8_1932 (NPBinding_t8_333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
