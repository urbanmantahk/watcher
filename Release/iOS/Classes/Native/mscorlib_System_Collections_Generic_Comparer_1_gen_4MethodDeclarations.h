﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1<System.Guid>
struct Comparer_1_t1_2169;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Generic.Comparer`1<System.Guid>::.ctor()
extern "C" void Comparer_1__ctor_m1_17612_gshared (Comparer_1_t1_2169 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m1_17612(__this, method) (( void (*) (Comparer_1_t1_2169 *, const MethodInfo*))Comparer_1__ctor_m1_17612_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<System.Guid>::.cctor()
extern "C" void Comparer_1__cctor_m1_17613_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m1_17613(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m1_17613_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<System.Guid>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C" int32_t Comparer_1_System_Collections_IComparer_Compare_m1_17614_gshared (Comparer_1_t1_2169 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m1_17614(__this, ___x, ___y, method) (( int32_t (*) (Comparer_1_t1_2169 *, Object_t *, Object_t *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m1_17614_gshared)(__this, ___x, ___y, method)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.Guid>::get_Default()
extern "C" Comparer_1_t1_2169 * Comparer_1_get_Default_m1_17615_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m1_17615(__this /* static, unused */, method) (( Comparer_1_t1_2169 * (*) (Object_t * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m1_17615_gshared)(__this /* static, unused */, method)
