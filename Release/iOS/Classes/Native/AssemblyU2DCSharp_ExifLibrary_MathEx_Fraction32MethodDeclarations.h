﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.Object
struct Object_t;
// System.IFormatProvider
struct IFormatProvider_t1_455;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ExifLibrary_MathEx_Fraction32.h"

// System.Void ExifLibrary.MathEx/Fraction32::.ctor(System.Int32,System.Int32)
extern "C" void Fraction32__ctor_m8_659 (Fraction32_t8_127 * __this, int32_t ___numerator, int32_t ___denominator, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.MathEx/Fraction32::.ctor(System.Int32)
extern "C" void Fraction32__ctor_m8_660 (Fraction32_t8_127 * __this, int32_t ___numerator, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.MathEx/Fraction32::.ctor(ExifLibrary.MathEx/Fraction32)
extern "C" void Fraction32__ctor_m8_661 (Fraction32_t8_127 * __this, Fraction32_t8_127  ___f, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.MathEx/Fraction32::.ctor(System.Single)
extern "C" void Fraction32__ctor_m8_662 (Fraction32_t8_127 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.MathEx/Fraction32::.ctor(System.Double)
extern "C" void Fraction32__ctor_m8_663 (Fraction32_t8_127 * __this, double ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.MathEx/Fraction32::.ctor(System.String)
extern "C" void Fraction32__ctor_m8_664 (Fraction32_t8_127 * __this, String_t* ___s, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.MathEx/Fraction32::.cctor()
extern "C" void Fraction32__cctor_m8_665 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExifLibrary.MathEx/Fraction32::get_Numerator()
extern "C" int32_t Fraction32_get_Numerator_m8_666 (Fraction32_t8_127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.MathEx/Fraction32::set_Numerator(System.Int32)
extern "C" void Fraction32_set_Numerator_m8_667 (Fraction32_t8_127 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExifLibrary.MathEx/Fraction32::get_Denominator()
extern "C" int32_t Fraction32_get_Denominator_m8_668 (Fraction32_t8_127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.MathEx/Fraction32::set_Denominator(System.Int32)
extern "C" void Fraction32_set_Denominator_m8_669 (Fraction32_t8_127 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExifLibrary.MathEx/Fraction32::get_IsNegative()
extern "C" bool Fraction32_get_IsNegative_m8_670 (Fraction32_t8_127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.MathEx/Fraction32::set_IsNegative(System.Boolean)
extern "C" void Fraction32_set_IsNegative_m8_671 (Fraction32_t8_127 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExifLibrary.MathEx/Fraction32::IsNan(ExifLibrary.MathEx/Fraction32)
extern "C" bool Fraction32_IsNan_m8_672 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExifLibrary.MathEx/Fraction32::IsNegativeInfinity(ExifLibrary.MathEx/Fraction32)
extern "C" bool Fraction32_IsNegativeInfinity_m8_673 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExifLibrary.MathEx/Fraction32::IsPositiveInfinity(ExifLibrary.MathEx/Fraction32)
extern "C" bool Fraction32_IsPositiveInfinity_m8_674 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExifLibrary.MathEx/Fraction32::IsInfinity(ExifLibrary.MathEx/Fraction32)
extern "C" bool Fraction32_IsInfinity_m8_675 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::Inverse(ExifLibrary.MathEx/Fraction32)
extern "C" Fraction32_t8_127  Fraction32_Inverse_m8_676 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::Parse(System.String)
extern "C" Fraction32_t8_127  Fraction32_Parse_m8_677 (Object_t * __this /* static, unused */, String_t* ___s, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExifLibrary.MathEx/Fraction32::TryParse(System.String,ExifLibrary.MathEx/Fraction32&)
extern "C" bool Fraction32_TryParse_m8_678 (Object_t * __this /* static, unused */, String_t* ___s, Fraction32_t8_127 * ___f, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.MathEx/Fraction32::Set(System.Int32,System.Int32)
extern "C" void Fraction32_Set_m8_679 (Fraction32_t8_127 * __this, int32_t ___numerator, int32_t ___denominator, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExifLibrary.MathEx/Fraction32::Equals(System.Object)
extern "C" bool Fraction32_Equals_m8_680 (Fraction32_t8_127 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExifLibrary.MathEx/Fraction32::Equals(ExifLibrary.MathEx/Fraction32)
extern "C" bool Fraction32_Equals_m8_681 (Fraction32_t8_127 * __this, Fraction32_t8_127  ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExifLibrary.MathEx/Fraction32::GetHashCode()
extern "C" int32_t Fraction32_GetHashCode_m8_682 (Fraction32_t8_127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExifLibrary.MathEx/Fraction32::ToString(System.String,System.IFormatProvider)
extern "C" String_t* Fraction32_ToString_m8_683 (Fraction32_t8_127 * __this, String_t* ___format, Object_t * ___formatProvider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExifLibrary.MathEx/Fraction32::ToString(System.String)
extern "C" String_t* Fraction32_ToString_m8_684 (Fraction32_t8_127 * __this, String_t* ___format, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExifLibrary.MathEx/Fraction32::ToString(System.IFormatProvider)
extern "C" String_t* Fraction32_ToString_m8_685 (Fraction32_t8_127 * __this, Object_t * ___formatProvider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExifLibrary.MathEx/Fraction32::ToString()
extern "C" String_t* Fraction32_ToString_m8_686 (Fraction32_t8_127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExifLibrary.MathEx/Fraction32::CompareTo(System.Object)
extern "C" int32_t Fraction32_CompareTo_m8_687 (Fraction32_t8_127 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExifLibrary.MathEx/Fraction32::CompareTo(ExifLibrary.MathEx/Fraction32)
extern "C" int32_t Fraction32_CompareTo_m8_688 (Fraction32_t8_127 * __this, Fraction32_t8_127  ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::FromDouble(System.Double,System.Int32)
extern "C" Fraction32_t8_127  Fraction32_FromDouble_m8_689 (Object_t * __this /* static, unused */, double ___value, int32_t ___precision, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::FromString(System.String)
extern "C" Fraction32_t8_127  Fraction32_FromString_m8_690 (Object_t * __this /* static, unused */, String_t* ___s, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.MathEx/Fraction32::Reduce(System.Int32&,System.Int32&)
extern "C" void Fraction32_Reduce_m8_691 (Object_t * __this /* static, unused */, int32_t* ___numerator, int32_t* ___denominator, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::op_Multiply(ExifLibrary.MathEx/Fraction32,System.Int32)
extern "C" Fraction32_t8_127  Fraction32_op_Multiply_m8_692 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f, int32_t ___n, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::op_Multiply(System.Int32,ExifLibrary.MathEx/Fraction32)
extern "C" Fraction32_t8_127  Fraction32_op_Multiply_m8_693 (Object_t * __this /* static, unused */, int32_t ___n, Fraction32_t8_127  ___f, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::op_Multiply(ExifLibrary.MathEx/Fraction32,System.Single)
extern "C" Fraction32_t8_127  Fraction32_op_Multiply_m8_694 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f, float ___n, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::op_Multiply(System.Single,ExifLibrary.MathEx/Fraction32)
extern "C" Fraction32_t8_127  Fraction32_op_Multiply_m8_695 (Object_t * __this /* static, unused */, float ___n, Fraction32_t8_127  ___f, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::op_Multiply(ExifLibrary.MathEx/Fraction32,System.Double)
extern "C" Fraction32_t8_127  Fraction32_op_Multiply_m8_696 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f, double ___n, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::op_Multiply(System.Double,ExifLibrary.MathEx/Fraction32)
extern "C" Fraction32_t8_127  Fraction32_op_Multiply_m8_697 (Object_t * __this /* static, unused */, double ___n, Fraction32_t8_127  ___f, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::op_Multiply(ExifLibrary.MathEx/Fraction32,ExifLibrary.MathEx/Fraction32)
extern "C" Fraction32_t8_127  Fraction32_op_Multiply_m8_698 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f1, Fraction32_t8_127  ___f2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::op_Division(ExifLibrary.MathEx/Fraction32,System.Int32)
extern "C" Fraction32_t8_127  Fraction32_op_Division_m8_699 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f, int32_t ___n, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::op_Division(ExifLibrary.MathEx/Fraction32,System.Single)
extern "C" Fraction32_t8_127  Fraction32_op_Division_m8_700 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f, float ___n, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::op_Division(ExifLibrary.MathEx/Fraction32,System.Double)
extern "C" Fraction32_t8_127  Fraction32_op_Division_m8_701 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f, double ___n, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::op_Division(ExifLibrary.MathEx/Fraction32,ExifLibrary.MathEx/Fraction32)
extern "C" Fraction32_t8_127  Fraction32_op_Division_m8_702 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f1, Fraction32_t8_127  ___f2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::op_Addition(ExifLibrary.MathEx/Fraction32,System.Int32)
extern "C" Fraction32_t8_127  Fraction32_op_Addition_m8_703 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f, int32_t ___n, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::op_Addition(System.Int32,ExifLibrary.MathEx/Fraction32)
extern "C" Fraction32_t8_127  Fraction32_op_Addition_m8_704 (Object_t * __this /* static, unused */, int32_t ___n, Fraction32_t8_127  ___f, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::op_Addition(ExifLibrary.MathEx/Fraction32,System.Single)
extern "C" Fraction32_t8_127  Fraction32_op_Addition_m8_705 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f, float ___n, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::op_Addition(System.Single,ExifLibrary.MathEx/Fraction32)
extern "C" Fraction32_t8_127  Fraction32_op_Addition_m8_706 (Object_t * __this /* static, unused */, float ___n, Fraction32_t8_127  ___f, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::op_Addition(ExifLibrary.MathEx/Fraction32,System.Double)
extern "C" Fraction32_t8_127  Fraction32_op_Addition_m8_707 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f, double ___n, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::op_Addition(System.Double,ExifLibrary.MathEx/Fraction32)
extern "C" Fraction32_t8_127  Fraction32_op_Addition_m8_708 (Object_t * __this /* static, unused */, double ___n, Fraction32_t8_127  ___f, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::op_Addition(ExifLibrary.MathEx/Fraction32,ExifLibrary.MathEx/Fraction32)
extern "C" Fraction32_t8_127  Fraction32_op_Addition_m8_709 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f1, Fraction32_t8_127  ___f2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::op_Subtraction(ExifLibrary.MathEx/Fraction32,System.Int32)
extern "C" Fraction32_t8_127  Fraction32_op_Subtraction_m8_710 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f, int32_t ___n, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::op_Subtraction(System.Int32,ExifLibrary.MathEx/Fraction32)
extern "C" Fraction32_t8_127  Fraction32_op_Subtraction_m8_711 (Object_t * __this /* static, unused */, int32_t ___n, Fraction32_t8_127  ___f, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::op_Subtraction(ExifLibrary.MathEx/Fraction32,System.Single)
extern "C" Fraction32_t8_127  Fraction32_op_Subtraction_m8_712 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f, float ___n, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::op_Subtraction(System.Single,ExifLibrary.MathEx/Fraction32)
extern "C" Fraction32_t8_127  Fraction32_op_Subtraction_m8_713 (Object_t * __this /* static, unused */, float ___n, Fraction32_t8_127  ___f, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::op_Subtraction(ExifLibrary.MathEx/Fraction32,System.Double)
extern "C" Fraction32_t8_127  Fraction32_op_Subtraction_m8_714 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f, double ___n, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::op_Subtraction(System.Double,ExifLibrary.MathEx/Fraction32)
extern "C" Fraction32_t8_127  Fraction32_op_Subtraction_m8_715 (Object_t * __this /* static, unused */, double ___n, Fraction32_t8_127  ___f, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::op_Subtraction(ExifLibrary.MathEx/Fraction32,ExifLibrary.MathEx/Fraction32)
extern "C" Fraction32_t8_127  Fraction32_op_Subtraction_m8_716 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f1, Fraction32_t8_127  ___f2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::op_Increment(ExifLibrary.MathEx/Fraction32)
extern "C" Fraction32_t8_127  Fraction32_op_Increment_m8_717 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/Fraction32 ExifLibrary.MathEx/Fraction32::op_Decrement(ExifLibrary.MathEx/Fraction32)
extern "C" Fraction32_t8_127  Fraction32_op_Decrement_m8_718 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExifLibrary.MathEx/Fraction32::op_Explicit(ExifLibrary.MathEx/Fraction32)
extern "C" int32_t Fraction32_op_Explicit_m8_719 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ExifLibrary.MathEx/Fraction32::op_Explicit(ExifLibrary.MathEx/Fraction32)
extern "C" float Fraction32_op_Explicit_m8_720 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double ExifLibrary.MathEx/Fraction32::op_Explicit(ExifLibrary.MathEx/Fraction32)
extern "C" double Fraction32_op_Explicit_m8_721 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExifLibrary.MathEx/Fraction32::op_Equality(ExifLibrary.MathEx/Fraction32,ExifLibrary.MathEx/Fraction32)
extern "C" bool Fraction32_op_Equality_m8_722 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f1, Fraction32_t8_127  ___f2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExifLibrary.MathEx/Fraction32::op_Inequality(ExifLibrary.MathEx/Fraction32,ExifLibrary.MathEx/Fraction32)
extern "C" bool Fraction32_op_Inequality_m8_723 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f1, Fraction32_t8_127  ___f2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExifLibrary.MathEx/Fraction32::op_LessThan(ExifLibrary.MathEx/Fraction32,ExifLibrary.MathEx/Fraction32)
extern "C" bool Fraction32_op_LessThan_m8_724 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f1, Fraction32_t8_127  ___f2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExifLibrary.MathEx/Fraction32::op_GreaterThan(ExifLibrary.MathEx/Fraction32,ExifLibrary.MathEx/Fraction32)
extern "C" bool Fraction32_op_GreaterThan_m8_725 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___f1, Fraction32_t8_127  ___f2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void Fraction32_t8_127_marshal(const Fraction32_t8_127& unmarshaled, Fraction32_t8_127_marshaled& marshaled);
extern "C" void Fraction32_t8_127_marshal_back(const Fraction32_t8_127_marshaled& marshaled, Fraction32_t8_127& unmarshaled);
extern "C" void Fraction32_t8_127_marshal_cleanup(Fraction32_t8_127_marshaled& marshaled);
