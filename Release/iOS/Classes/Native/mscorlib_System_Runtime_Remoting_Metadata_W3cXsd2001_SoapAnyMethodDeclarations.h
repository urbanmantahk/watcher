﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapAnyUri
struct SoapAnyUri_t1_965;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapAnyUri::.ctor()
extern "C" void SoapAnyUri__ctor_m1_8666 (SoapAnyUri_t1_965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapAnyUri::.ctor(System.String)
extern "C" void SoapAnyUri__ctor_m1_8667 (SoapAnyUri_t1_965 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapAnyUri::get_Value()
extern "C" String_t* SoapAnyUri_get_Value_m1_8668 (SoapAnyUri_t1_965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapAnyUri::set_Value(System.String)
extern "C" void SoapAnyUri_set_Value_m1_8669 (SoapAnyUri_t1_965 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapAnyUri::get_XsdType()
extern "C" String_t* SoapAnyUri_get_XsdType_m1_8670 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapAnyUri::GetXsdType()
extern "C" String_t* SoapAnyUri_GetXsdType_m1_8671 (SoapAnyUri_t1_965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapAnyUri System.Runtime.Remoting.Metadata.W3cXsd2001.SoapAnyUri::Parse(System.String)
extern "C" SoapAnyUri_t1_965 * SoapAnyUri_Parse_m1_8672 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapAnyUri::ToString()
extern "C" String_t* SoapAnyUri_ToString_m1_8673 (SoapAnyUri_t1_965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
