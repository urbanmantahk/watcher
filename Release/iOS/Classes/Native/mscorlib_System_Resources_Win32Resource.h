﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Resources.NameOrId
struct NameOrId_t1_663;

#include "mscorlib_System_Object.h"

// System.Resources.Win32Resource
struct  Win32Resource_t1_664  : public Object_t
{
	// System.Resources.NameOrId System.Resources.Win32Resource::type
	NameOrId_t1_663 * ___type_0;
	// System.Resources.NameOrId System.Resources.Win32Resource::name
	NameOrId_t1_663 * ___name_1;
	// System.Int32 System.Resources.Win32Resource::language
	int32_t ___language_2;
};
