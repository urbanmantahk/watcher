﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.Schema.XmlSchemaSimpleTypeList
struct XmlSchemaSimpleTypeList_t4_70;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t4_71;
// System.Xml.Schema.XmlSchemaSimpleType
struct XmlSchemaSimpleType_t4_63;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Xml.Schema.XmlSchemaSimpleTypeList::.ctor()
extern "C" void XmlSchemaSimpleTypeList__ctor_m4_102 (XmlSchemaSimpleTypeList_t4_70 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlSchemaSimpleTypeList::set_ItemTypeName(System.Xml.XmlQualifiedName)
extern "C" void XmlSchemaSimpleTypeList_set_ItemTypeName_m4_103 (XmlSchemaSimpleTypeList_t4_70 * __this, XmlQualifiedName_t4_71 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Schema.XmlSchemaSimpleTypeList::set_ItemType(System.Xml.Schema.XmlSchemaSimpleType)
extern "C" void XmlSchemaSimpleTypeList_set_ItemType_m4_104 (XmlSchemaSimpleTypeList_t4_70 * __this, XmlSchemaSimpleType_t4_63 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
