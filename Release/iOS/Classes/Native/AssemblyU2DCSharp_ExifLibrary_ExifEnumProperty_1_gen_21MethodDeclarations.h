﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExifLibrary.ExifEnumProperty`1<ExifLibrary.SubjectDistanceRange>
struct ExifEnumProperty_1_t8_360;
// System.Object
struct Object_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTag.h"
#include "AssemblyU2DCSharp_ExifLibrary_SubjectDistanceRange.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifInterOperability.h"

// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.SubjectDistanceRange>::.ctor(ExifLibrary.ExifTag,T,System.Boolean)
extern "C" void ExifEnumProperty_1__ctor_m8_1997_gshared (ExifEnumProperty_1_t8_360 * __this, int32_t ___tag, uint16_t ___value, bool ___isbitfield, const MethodInfo* method);
#define ExifEnumProperty_1__ctor_m8_1997(__this, ___tag, ___value, ___isbitfield, method) (( void (*) (ExifEnumProperty_1_t8_360 *, int32_t, uint16_t, bool, const MethodInfo*))ExifEnumProperty_1__ctor_m8_1997_gshared)(__this, ___tag, ___value, ___isbitfield, method)
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.SubjectDistanceRange>::.ctor(ExifLibrary.ExifTag,T)
extern "C" void ExifEnumProperty_1__ctor_m8_2270_gshared (ExifEnumProperty_1_t8_360 * __this, int32_t ___tag, uint16_t ___value, const MethodInfo* method);
#define ExifEnumProperty_1__ctor_m8_2270(__this, ___tag, ___value, method) (( void (*) (ExifEnumProperty_1_t8_360 *, int32_t, uint16_t, const MethodInfo*))ExifEnumProperty_1__ctor_m8_2270_gshared)(__this, ___tag, ___value, method)
// System.Object ExifLibrary.ExifEnumProperty`1<ExifLibrary.SubjectDistanceRange>::get__Value()
extern "C" Object_t * ExifEnumProperty_1_get__Value_m8_2271_gshared (ExifEnumProperty_1_t8_360 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get__Value_m8_2271(__this, method) (( Object_t * (*) (ExifEnumProperty_1_t8_360 *, const MethodInfo*))ExifEnumProperty_1_get__Value_m8_2271_gshared)(__this, method)
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.SubjectDistanceRange>::set__Value(System.Object)
extern "C" void ExifEnumProperty_1_set__Value_m8_2272_gshared (ExifEnumProperty_1_t8_360 * __this, Object_t * ___value, const MethodInfo* method);
#define ExifEnumProperty_1_set__Value_m8_2272(__this, ___value, method) (( void (*) (ExifEnumProperty_1_t8_360 *, Object_t *, const MethodInfo*))ExifEnumProperty_1_set__Value_m8_2272_gshared)(__this, ___value, method)
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.SubjectDistanceRange>::get_Value()
extern "C" uint16_t ExifEnumProperty_1_get_Value_m8_2273_gshared (ExifEnumProperty_1_t8_360 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get_Value_m8_2273(__this, method) (( uint16_t (*) (ExifEnumProperty_1_t8_360 *, const MethodInfo*))ExifEnumProperty_1_get_Value_m8_2273_gshared)(__this, method)
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.SubjectDistanceRange>::set_Value(T)
extern "C" void ExifEnumProperty_1_set_Value_m8_2274_gshared (ExifEnumProperty_1_t8_360 * __this, uint16_t ___value, const MethodInfo* method);
#define ExifEnumProperty_1_set_Value_m8_2274(__this, ___value, method) (( void (*) (ExifEnumProperty_1_t8_360 *, uint16_t, const MethodInfo*))ExifEnumProperty_1_set_Value_m8_2274_gshared)(__this, ___value, method)
// System.Boolean ExifLibrary.ExifEnumProperty`1<ExifLibrary.SubjectDistanceRange>::get_IsBitField()
extern "C" bool ExifEnumProperty_1_get_IsBitField_m8_2275_gshared (ExifEnumProperty_1_t8_360 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get_IsBitField_m8_2275(__this, method) (( bool (*) (ExifEnumProperty_1_t8_360 *, const MethodInfo*))ExifEnumProperty_1_get_IsBitField_m8_2275_gshared)(__this, method)
// System.String ExifLibrary.ExifEnumProperty`1<ExifLibrary.SubjectDistanceRange>::ToString()
extern "C" String_t* ExifEnumProperty_1_ToString_m8_2276_gshared (ExifEnumProperty_1_t8_360 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_ToString_m8_2276(__this, method) (( String_t* (*) (ExifEnumProperty_1_t8_360 *, const MethodInfo*))ExifEnumProperty_1_ToString_m8_2276_gshared)(__this, method)
// ExifLibrary.ExifInterOperability ExifLibrary.ExifEnumProperty`1<ExifLibrary.SubjectDistanceRange>::get_Interoperability()
extern "C" ExifInterOperability_t8_113  ExifEnumProperty_1_get_Interoperability_m8_2277_gshared (ExifEnumProperty_1_t8_360 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get_Interoperability_m8_2277(__this, method) (( ExifInterOperability_t8_113  (*) (ExifEnumProperty_1_t8_360 *, const MethodInfo*))ExifEnumProperty_1_get_Interoperability_m8_2277_gshared)(__this, method)
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.SubjectDistanceRange>::op_Implicit(ExifLibrary.ExifEnumProperty`1<T>)
extern "C" uint16_t ExifEnumProperty_1_op_Implicit_m8_2278_gshared (Object_t * __this /* static, unused */, ExifEnumProperty_1_t8_360 * ___obj, const MethodInfo* method);
#define ExifEnumProperty_1_op_Implicit_m8_2278(__this /* static, unused */, ___obj, method) (( uint16_t (*) (Object_t * /* static, unused */, ExifEnumProperty_1_t8_360 *, const MethodInfo*))ExifEnumProperty_1_op_Implicit_m8_2278_gshared)(__this /* static, unused */, ___obj, method)
