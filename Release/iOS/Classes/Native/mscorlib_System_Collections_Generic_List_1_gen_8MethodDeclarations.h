﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_t1_1835;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Vector4>
struct IEnumerable_1_t1_2798;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t6_278;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector4>
struct IEnumerator_1_t1_2799;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<UnityEngine.Vector4>
struct ICollection_1_t1_2800;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector4>
struct ReadOnlyCollection_1_t1_2280;
// System.Collections.Generic.IComparer`1<UnityEngine.Vector4>
struct IComparer_1_t1_2801;
// System.Predicate`1<UnityEngine.Vector4>
struct Predicate_1_t1_2287;
// System.Action`1<UnityEngine.Vector4>
struct Action_1_t1_2288;
// System.Comparison`1<UnityEngine.Vector4>
struct Comparison_1_t1_2289;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector4.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_16.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::.ctor()
extern "C" void List_1__ctor_m1_18949_gshared (List_1_t1_1835 * __this, const MethodInfo* method);
#define List_1__ctor_m1_18949(__this, method) (( void (*) (List_1_t1_1835 *, const MethodInfo*))List_1__ctor_m1_18949_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m1_18950_gshared (List_1_t1_1835 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m1_18950(__this, ___collection, method) (( void (*) (List_1_t1_1835 *, Object_t*, const MethodInfo*))List_1__ctor_m1_18950_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::.ctor(System.Int32)
extern "C" void List_1__ctor_m1_18951_gshared (List_1_t1_1835 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m1_18951(__this, ___capacity, method) (( void (*) (List_1_t1_1835 *, int32_t, const MethodInfo*))List_1__ctor_m1_18951_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::.ctor(T[],System.Int32)
extern "C" void List_1__ctor_m1_18952_gshared (List_1_t1_1835 * __this, Vector4U5BU5D_t6_278* ___data, int32_t ___size, const MethodInfo* method);
#define List_1__ctor_m1_18952(__this, ___data, ___size, method) (( void (*) (List_1_t1_1835 *, Vector4U5BU5D_t6_278*, int32_t, const MethodInfo*))List_1__ctor_m1_18952_gshared)(__this, ___data, ___size, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::.cctor()
extern "C" void List_1__cctor_m1_18953_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m1_18953(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m1_18953_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_18954_gshared (List_1_t1_1835 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_18954(__this, method) (( Object_t* (*) (List_1_t1_1835 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_18954_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m1_18955_gshared (List_1_t1_1835 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m1_18955(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_1835 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m1_18955_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m1_18956_gshared (List_1_t1_1835 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1_18956(__this, method) (( Object_t * (*) (List_1_t1_1835 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1_18956_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m1_18957_gshared (List_1_t1_1835 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m1_18957(__this, ___item, method) (( int32_t (*) (List_1_t1_1835 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m1_18957_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m1_18958_gshared (List_1_t1_1835 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m1_18958(__this, ___item, method) (( bool (*) (List_1_t1_1835 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1_18958_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m1_18959_gshared (List_1_t1_1835 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m1_18959(__this, ___item, method) (( int32_t (*) (List_1_t1_1835 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m1_18959_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m1_18960_gshared (List_1_t1_1835 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m1_18960(__this, ___index, ___item, method) (( void (*) (List_1_t1_1835 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m1_18960_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m1_18961_gshared (List_1_t1_1835 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m1_18961(__this, ___item, method) (( void (*) (List_1_t1_1835 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1_18961_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_18962_gshared (List_1_t1_1835 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_18962(__this, method) (( bool (*) (List_1_t1_1835 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_18962_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m1_18963_gshared (List_1_t1_1835 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m1_18963(__this, method) (( bool (*) (List_1_t1_1835 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m1_18963_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m1_18964_gshared (List_1_t1_1835 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m1_18964(__this, method) (( Object_t * (*) (List_1_t1_1835 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1_18964_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m1_18965_gshared (List_1_t1_1835 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m1_18965(__this, method) (( bool (*) (List_1_t1_1835 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m1_18965_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m1_18966_gshared (List_1_t1_1835 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m1_18966(__this, method) (( bool (*) (List_1_t1_1835 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m1_18966_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m1_18967_gshared (List_1_t1_1835 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m1_18967(__this, ___index, method) (( Object_t * (*) (List_1_t1_1835 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m1_18967_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m1_18968_gshared (List_1_t1_1835 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m1_18968(__this, ___index, ___value, method) (( void (*) (List_1_t1_1835 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m1_18968_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::Add(T)
extern "C" void List_1_Add_m1_18969_gshared (List_1_t1_1835 * __this, Vector4_t6_54  ___item, const MethodInfo* method);
#define List_1_Add_m1_18969(__this, ___item, method) (( void (*) (List_1_t1_1835 *, Vector4_t6_54 , const MethodInfo*))List_1_Add_m1_18969_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m1_18970_gshared (List_1_t1_1835 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m1_18970(__this, ___newCount, method) (( void (*) (List_1_t1_1835 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m1_18970_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::CheckRange(System.Int32,System.Int32)
extern "C" void List_1_CheckRange_m1_18971_gshared (List_1_t1_1835 * __this, int32_t ___idx, int32_t ___count, const MethodInfo* method);
#define List_1_CheckRange_m1_18971(__this, ___idx, ___count, method) (( void (*) (List_1_t1_1835 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m1_18971_gshared)(__this, ___idx, ___count, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m1_18972_gshared (List_1_t1_1835 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m1_18972(__this, ___collection, method) (( void (*) (List_1_t1_1835 *, Object_t*, const MethodInfo*))List_1_AddCollection_m1_18972_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m1_18973_gshared (List_1_t1_1835 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m1_18973(__this, ___enumerable, method) (( void (*) (List_1_t1_1835 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m1_18973_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m1_15022_gshared (List_1_t1_1835 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m1_15022(__this, ___collection, method) (( void (*) (List_1_t1_1835 *, Object_t*, const MethodInfo*))List_1_AddRange_m1_15022_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.Vector4>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t1_2280 * List_1_AsReadOnly_m1_18974_gshared (List_1_t1_1835 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m1_18974(__this, method) (( ReadOnlyCollection_1_t1_2280 * (*) (List_1_t1_1835 *, const MethodInfo*))List_1_AsReadOnly_m1_18974_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector4>::BinarySearch(T)
extern "C" int32_t List_1_BinarySearch_m1_18975_gshared (List_1_t1_1835 * __this, Vector4_t6_54  ___item, const MethodInfo* method);
#define List_1_BinarySearch_m1_18975(__this, ___item, method) (( int32_t (*) (List_1_t1_1835 *, Vector4_t6_54 , const MethodInfo*))List_1_BinarySearch_m1_18975_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector4>::BinarySearch(T,System.Collections.Generic.IComparer`1<T>)
extern "C" int32_t List_1_BinarySearch_m1_18976_gshared (List_1_t1_1835 * __this, Vector4_t6_54  ___item, Object_t* ___comparer, const MethodInfo* method);
#define List_1_BinarySearch_m1_18976(__this, ___item, ___comparer, method) (( int32_t (*) (List_1_t1_1835 *, Vector4_t6_54 , Object_t*, const MethodInfo*))List_1_BinarySearch_m1_18976_gshared)(__this, ___item, ___comparer, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector4>::BinarySearch(System.Int32,System.Int32,T,System.Collections.Generic.IComparer`1<T>)
extern "C" int32_t List_1_BinarySearch_m1_18977_gshared (List_1_t1_1835 * __this, int32_t ___index, int32_t ___count, Vector4_t6_54  ___item, Object_t* ___comparer, const MethodInfo* method);
#define List_1_BinarySearch_m1_18977(__this, ___index, ___count, ___item, ___comparer, method) (( int32_t (*) (List_1_t1_1835 *, int32_t, int32_t, Vector4_t6_54 , Object_t*, const MethodInfo*))List_1_BinarySearch_m1_18977_gshared)(__this, ___index, ___count, ___item, ___comparer, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::Clear()
extern "C" void List_1_Clear_m1_18978_gshared (List_1_t1_1835 * __this, const MethodInfo* method);
#define List_1_Clear_m1_18978(__this, method) (( void (*) (List_1_t1_1835 *, const MethodInfo*))List_1_Clear_m1_18978_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector4>::Contains(T)
extern "C" bool List_1_Contains_m1_18979_gshared (List_1_t1_1835 * __this, Vector4_t6_54  ___item, const MethodInfo* method);
#define List_1_Contains_m1_18979(__this, ___item, method) (( bool (*) (List_1_t1_1835 *, Vector4_t6_54 , const MethodInfo*))List_1_Contains_m1_18979_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::CopyTo(T[])
extern "C" void List_1_CopyTo_m1_18980_gshared (List_1_t1_1835 * __this, Vector4U5BU5D_t6_278* ___array, const MethodInfo* method);
#define List_1_CopyTo_m1_18980(__this, ___array, method) (( void (*) (List_1_t1_1835 *, Vector4U5BU5D_t6_278*, const MethodInfo*))List_1_CopyTo_m1_18980_gshared)(__this, ___array, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m1_18981_gshared (List_1_t1_1835 * __this, Vector4U5BU5D_t6_278* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m1_18981(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_1835 *, Vector4U5BU5D_t6_278*, int32_t, const MethodInfo*))List_1_CopyTo_m1_18981_gshared)(__this, ___array, ___arrayIndex, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::CopyTo(System.Int32,T[],System.Int32,System.Int32)
extern "C" void List_1_CopyTo_m1_18982_gshared (List_1_t1_1835 * __this, int32_t ___index, Vector4U5BU5D_t6_278* ___array, int32_t ___arrayIndex, int32_t ___count, const MethodInfo* method);
#define List_1_CopyTo_m1_18982(__this, ___index, ___array, ___arrayIndex, ___count, method) (( void (*) (List_1_t1_1835 *, int32_t, Vector4U5BU5D_t6_278*, int32_t, int32_t, const MethodInfo*))List_1_CopyTo_m1_18982_gshared)(__this, ___index, ___array, ___arrayIndex, ___count, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector4>::Exists(System.Predicate`1<T>)
extern "C" bool List_1_Exists_m1_18983_gshared (List_1_t1_1835 * __this, Predicate_1_t1_2287 * ___match, const MethodInfo* method);
#define List_1_Exists_m1_18983(__this, ___match, method) (( bool (*) (List_1_t1_1835 *, Predicate_1_t1_2287 *, const MethodInfo*))List_1_Exists_m1_18983_gshared)(__this, ___match, method)
// T System.Collections.Generic.List`1<UnityEngine.Vector4>::Find(System.Predicate`1<T>)
extern "C" Vector4_t6_54  List_1_Find_m1_18984_gshared (List_1_t1_1835 * __this, Predicate_1_t1_2287 * ___match, const MethodInfo* method);
#define List_1_Find_m1_18984(__this, ___match, method) (( Vector4_t6_54  (*) (List_1_t1_1835 *, Predicate_1_t1_2287 *, const MethodInfo*))List_1_Find_m1_18984_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m1_18985_gshared (Object_t * __this /* static, unused */, Predicate_1_t1_2287 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m1_18985(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t1_2287 *, const MethodInfo*))List_1_CheckMatch_m1_18985_gshared)(__this /* static, unused */, ___match, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<UnityEngine.Vector4>::FindAll(System.Predicate`1<T>)
extern "C" List_1_t1_1835 * List_1_FindAll_m1_18986_gshared (List_1_t1_1835 * __this, Predicate_1_t1_2287 * ___match, const MethodInfo* method);
#define List_1_FindAll_m1_18986(__this, ___match, method) (( List_1_t1_1835 * (*) (List_1_t1_1835 *, Predicate_1_t1_2287 *, const MethodInfo*))List_1_FindAll_m1_18986_gshared)(__this, ___match, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<UnityEngine.Vector4>::FindAllStackBits(System.Predicate`1<T>)
extern "C" List_1_t1_1835 * List_1_FindAllStackBits_m1_18987_gshared (List_1_t1_1835 * __this, Predicate_1_t1_2287 * ___match, const MethodInfo* method);
#define List_1_FindAllStackBits_m1_18987(__this, ___match, method) (( List_1_t1_1835 * (*) (List_1_t1_1835 *, Predicate_1_t1_2287 *, const MethodInfo*))List_1_FindAllStackBits_m1_18987_gshared)(__this, ___match, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<UnityEngine.Vector4>::FindAllList(System.Predicate`1<T>)
extern "C" List_1_t1_1835 * List_1_FindAllList_m1_18988_gshared (List_1_t1_1835 * __this, Predicate_1_t1_2287 * ___match, const MethodInfo* method);
#define List_1_FindAllList_m1_18988(__this, ___match, method) (( List_1_t1_1835 * (*) (List_1_t1_1835 *, Predicate_1_t1_2287 *, const MethodInfo*))List_1_FindAllList_m1_18988_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector4>::FindIndex(System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m1_18989_gshared (List_1_t1_1835 * __this, Predicate_1_t1_2287 * ___match, const MethodInfo* method);
#define List_1_FindIndex_m1_18989(__this, ___match, method) (( int32_t (*) (List_1_t1_1835 *, Predicate_1_t1_2287 *, const MethodInfo*))List_1_FindIndex_m1_18989_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector4>::FindIndex(System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m1_18990_gshared (List_1_t1_1835 * __this, int32_t ___startIndex, Predicate_1_t1_2287 * ___match, const MethodInfo* method);
#define List_1_FindIndex_m1_18990(__this, ___startIndex, ___match, method) (( int32_t (*) (List_1_t1_1835 *, int32_t, Predicate_1_t1_2287 *, const MethodInfo*))List_1_FindIndex_m1_18990_gshared)(__this, ___startIndex, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector4>::FindIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m1_18991_gshared (List_1_t1_1835 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_2287 * ___match, const MethodInfo* method);
#define List_1_FindIndex_m1_18991(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_1835 *, int32_t, int32_t, Predicate_1_t1_2287 *, const MethodInfo*))List_1_FindIndex_m1_18991_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector4>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m1_18992_gshared (List_1_t1_1835 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_2287 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m1_18992(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_1835 *, int32_t, int32_t, Predicate_1_t1_2287 *, const MethodInfo*))List_1_GetIndex_m1_18992_gshared)(__this, ___startIndex, ___count, ___match, method)
// T System.Collections.Generic.List`1<UnityEngine.Vector4>::FindLast(System.Predicate`1<T>)
extern "C" Vector4_t6_54  List_1_FindLast_m1_18993_gshared (List_1_t1_1835 * __this, Predicate_1_t1_2287 * ___match, const MethodInfo* method);
#define List_1_FindLast_m1_18993(__this, ___match, method) (( Vector4_t6_54  (*) (List_1_t1_1835 *, Predicate_1_t1_2287 *, const MethodInfo*))List_1_FindLast_m1_18993_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector4>::FindLastIndex(System.Predicate`1<T>)
extern "C" int32_t List_1_FindLastIndex_m1_18994_gshared (List_1_t1_1835 * __this, Predicate_1_t1_2287 * ___match, const MethodInfo* method);
#define List_1_FindLastIndex_m1_18994(__this, ___match, method) (( int32_t (*) (List_1_t1_1835 *, Predicate_1_t1_2287 *, const MethodInfo*))List_1_FindLastIndex_m1_18994_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector4>::FindLastIndex(System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindLastIndex_m1_18995_gshared (List_1_t1_1835 * __this, int32_t ___startIndex, Predicate_1_t1_2287 * ___match, const MethodInfo* method);
#define List_1_FindLastIndex_m1_18995(__this, ___startIndex, ___match, method) (( int32_t (*) (List_1_t1_1835 *, int32_t, Predicate_1_t1_2287 *, const MethodInfo*))List_1_FindLastIndex_m1_18995_gshared)(__this, ___startIndex, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector4>::FindLastIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindLastIndex_m1_18996_gshared (List_1_t1_1835 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_2287 * ___match, const MethodInfo* method);
#define List_1_FindLastIndex_m1_18996(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_1835 *, int32_t, int32_t, Predicate_1_t1_2287 *, const MethodInfo*))List_1_FindLastIndex_m1_18996_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector4>::GetLastIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetLastIndex_m1_18997_gshared (List_1_t1_1835 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_2287 * ___match, const MethodInfo* method);
#define List_1_GetLastIndex_m1_18997(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_1835 *, int32_t, int32_t, Predicate_1_t1_2287 *, const MethodInfo*))List_1_GetLastIndex_m1_18997_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::ForEach(System.Action`1<T>)
extern "C" void List_1_ForEach_m1_18998_gshared (List_1_t1_1835 * __this, Action_1_t1_2288 * ___action, const MethodInfo* method);
#define List_1_ForEach_m1_18998(__this, ___action, method) (( void (*) (List_1_t1_1835 *, Action_1_t1_2288 *, const MethodInfo*))List_1_ForEach_m1_18998_gshared)(__this, ___action, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Vector4>::GetEnumerator()
extern "C" Enumerator_t1_2279  List_1_GetEnumerator_m1_18999_gshared (List_1_t1_1835 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m1_18999(__this, method) (( Enumerator_t1_2279  (*) (List_1_t1_1835 *, const MethodInfo*))List_1_GetEnumerator_m1_18999_gshared)(__this, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<UnityEngine.Vector4>::GetRange(System.Int32,System.Int32)
extern "C" List_1_t1_1835 * List_1_GetRange_m1_19000_gshared (List_1_t1_1835 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_GetRange_m1_19000(__this, ___index, ___count, method) (( List_1_t1_1835 * (*) (List_1_t1_1835 *, int32_t, int32_t, const MethodInfo*))List_1_GetRange_m1_19000_gshared)(__this, ___index, ___count, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector4>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m1_19001_gshared (List_1_t1_1835 * __this, Vector4_t6_54  ___item, const MethodInfo* method);
#define List_1_IndexOf_m1_19001(__this, ___item, method) (( int32_t (*) (List_1_t1_1835 *, Vector4_t6_54 , const MethodInfo*))List_1_IndexOf_m1_19001_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector4>::IndexOf(T,System.Int32)
extern "C" int32_t List_1_IndexOf_m1_19002_gshared (List_1_t1_1835 * __this, Vector4_t6_54  ___item, int32_t ___index, const MethodInfo* method);
#define List_1_IndexOf_m1_19002(__this, ___item, ___index, method) (( int32_t (*) (List_1_t1_1835 *, Vector4_t6_54 , int32_t, const MethodInfo*))List_1_IndexOf_m1_19002_gshared)(__this, ___item, ___index, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector4>::IndexOf(T,System.Int32,System.Int32)
extern "C" int32_t List_1_IndexOf_m1_19003_gshared (List_1_t1_1835 * __this, Vector4_t6_54  ___item, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_IndexOf_m1_19003(__this, ___item, ___index, ___count, method) (( int32_t (*) (List_1_t1_1835 *, Vector4_t6_54 , int32_t, int32_t, const MethodInfo*))List_1_IndexOf_m1_19003_gshared)(__this, ___item, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m1_19004_gshared (List_1_t1_1835 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m1_19004(__this, ___start, ___delta, method) (( void (*) (List_1_t1_1835 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m1_19004_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m1_19005_gshared (List_1_t1_1835 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m1_19005(__this, ___index, method) (( void (*) (List_1_t1_1835 *, int32_t, const MethodInfo*))List_1_CheckIndex_m1_19005_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m1_19006_gshared (List_1_t1_1835 * __this, int32_t ___index, Vector4_t6_54  ___item, const MethodInfo* method);
#define List_1_Insert_m1_19006(__this, ___index, ___item, method) (( void (*) (List_1_t1_1835 *, int32_t, Vector4_t6_54 , const MethodInfo*))List_1_Insert_m1_19006_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m1_19007_gshared (List_1_t1_1835 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m1_19007(__this, ___collection, method) (( void (*) (List_1_t1_1835 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m1_19007_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::InsertRange(System.Int32,System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_InsertRange_m1_19008_gshared (List_1_t1_1835 * __this, int32_t ___index, Object_t* ___collection, const MethodInfo* method);
#define List_1_InsertRange_m1_19008(__this, ___index, ___collection, method) (( void (*) (List_1_t1_1835 *, int32_t, Object_t*, const MethodInfo*))List_1_InsertRange_m1_19008_gshared)(__this, ___index, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::InsertCollection(System.Int32,System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_InsertCollection_m1_19009_gshared (List_1_t1_1835 * __this, int32_t ___index, Object_t* ___collection, const MethodInfo* method);
#define List_1_InsertCollection_m1_19009(__this, ___index, ___collection, method) (( void (*) (List_1_t1_1835 *, int32_t, Object_t*, const MethodInfo*))List_1_InsertCollection_m1_19009_gshared)(__this, ___index, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::InsertEnumeration(System.Int32,System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_InsertEnumeration_m1_19010_gshared (List_1_t1_1835 * __this, int32_t ___index, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_InsertEnumeration_m1_19010(__this, ___index, ___enumerable, method) (( void (*) (List_1_t1_1835 *, int32_t, Object_t*, const MethodInfo*))List_1_InsertEnumeration_m1_19010_gshared)(__this, ___index, ___enumerable, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector4>::LastIndexOf(T)
extern "C" int32_t List_1_LastIndexOf_m1_19011_gshared (List_1_t1_1835 * __this, Vector4_t6_54  ___item, const MethodInfo* method);
#define List_1_LastIndexOf_m1_19011(__this, ___item, method) (( int32_t (*) (List_1_t1_1835 *, Vector4_t6_54 , const MethodInfo*))List_1_LastIndexOf_m1_19011_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector4>::LastIndexOf(T,System.Int32)
extern "C" int32_t List_1_LastIndexOf_m1_19012_gshared (List_1_t1_1835 * __this, Vector4_t6_54  ___item, int32_t ___index, const MethodInfo* method);
#define List_1_LastIndexOf_m1_19012(__this, ___item, ___index, method) (( int32_t (*) (List_1_t1_1835 *, Vector4_t6_54 , int32_t, const MethodInfo*))List_1_LastIndexOf_m1_19012_gshared)(__this, ___item, ___index, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector4>::LastIndexOf(T,System.Int32,System.Int32)
extern "C" int32_t List_1_LastIndexOf_m1_19013_gshared (List_1_t1_1835 * __this, Vector4_t6_54  ___item, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_LastIndexOf_m1_19013(__this, ___item, ___index, ___count, method) (( int32_t (*) (List_1_t1_1835 *, Vector4_t6_54 , int32_t, int32_t, const MethodInfo*))List_1_LastIndexOf_m1_19013_gshared)(__this, ___item, ___index, ___count, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector4>::Remove(T)
extern "C" bool List_1_Remove_m1_19014_gshared (List_1_t1_1835 * __this, Vector4_t6_54  ___item, const MethodInfo* method);
#define List_1_Remove_m1_19014(__this, ___item, method) (( bool (*) (List_1_t1_1835 *, Vector4_t6_54 , const MethodInfo*))List_1_Remove_m1_19014_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector4>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m1_19015_gshared (List_1_t1_1835 * __this, Predicate_1_t1_2287 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m1_19015(__this, ___match, method) (( int32_t (*) (List_1_t1_1835 *, Predicate_1_t1_2287 *, const MethodInfo*))List_1_RemoveAll_m1_19015_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m1_19016_gshared (List_1_t1_1835 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m1_19016(__this, ___index, method) (( void (*) (List_1_t1_1835 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1_19016_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::RemoveRange(System.Int32,System.Int32)
extern "C" void List_1_RemoveRange_m1_19017_gshared (List_1_t1_1835 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_RemoveRange_m1_19017(__this, ___index, ___count, method) (( void (*) (List_1_t1_1835 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m1_19017_gshared)(__this, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::Reverse()
extern "C" void List_1_Reverse_m1_19018_gshared (List_1_t1_1835 * __this, const MethodInfo* method);
#define List_1_Reverse_m1_19018(__this, method) (( void (*) (List_1_t1_1835 *, const MethodInfo*))List_1_Reverse_m1_19018_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::Reverse(System.Int32,System.Int32)
extern "C" void List_1_Reverse_m1_19019_gshared (List_1_t1_1835 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_Reverse_m1_19019(__this, ___index, ___count, method) (( void (*) (List_1_t1_1835 *, int32_t, int32_t, const MethodInfo*))List_1_Reverse_m1_19019_gshared)(__this, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::Sort()
extern "C" void List_1_Sort_m1_19020_gshared (List_1_t1_1835 * __this, const MethodInfo* method);
#define List_1_Sort_m1_19020(__this, method) (( void (*) (List_1_t1_1835 *, const MethodInfo*))List_1_Sort_m1_19020_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C" void List_1_Sort_m1_19021_gshared (List_1_t1_1835 * __this, Object_t* ___comparer, const MethodInfo* method);
#define List_1_Sort_m1_19021(__this, ___comparer, method) (( void (*) (List_1_t1_1835 *, Object_t*, const MethodInfo*))List_1_Sort_m1_19021_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m1_19022_gshared (List_1_t1_1835 * __this, Comparison_1_t1_2289 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m1_19022(__this, ___comparison, method) (( void (*) (List_1_t1_1835 *, Comparison_1_t1_2289 *, const MethodInfo*))List_1_Sort_m1_19022_gshared)(__this, ___comparison, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::Sort(System.Int32,System.Int32,System.Collections.Generic.IComparer`1<T>)
extern "C" void List_1_Sort_m1_19023_gshared (List_1_t1_1835 * __this, int32_t ___index, int32_t ___count, Object_t* ___comparer, const MethodInfo* method);
#define List_1_Sort_m1_19023(__this, ___index, ___count, ___comparer, method) (( void (*) (List_1_t1_1835 *, int32_t, int32_t, Object_t*, const MethodInfo*))List_1_Sort_m1_19023_gshared)(__this, ___index, ___count, ___comparer, method)
// T[] System.Collections.Generic.List`1<UnityEngine.Vector4>::ToArray()
extern "C" Vector4U5BU5D_t6_278* List_1_ToArray_m1_19024_gshared (List_1_t1_1835 * __this, const MethodInfo* method);
#define List_1_ToArray_m1_19024(__this, method) (( Vector4U5BU5D_t6_278* (*) (List_1_t1_1835 *, const MethodInfo*))List_1_ToArray_m1_19024_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::TrimExcess()
extern "C" void List_1_TrimExcess_m1_19025_gshared (List_1_t1_1835 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m1_19025(__this, method) (( void (*) (List_1_t1_1835 *, const MethodInfo*))List_1_TrimExcess_m1_19025_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector4>::TrueForAll(System.Predicate`1<T>)
extern "C" bool List_1_TrueForAll_m1_19026_gshared (List_1_t1_1835 * __this, Predicate_1_t1_2287 * ___match, const MethodInfo* method);
#define List_1_TrueForAll_m1_19026(__this, ___match, method) (( bool (*) (List_1_t1_1835 *, Predicate_1_t1_2287 *, const MethodInfo*))List_1_TrueForAll_m1_19026_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector4>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m1_19027_gshared (List_1_t1_1835 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m1_19027(__this, method) (( int32_t (*) (List_1_t1_1835 *, const MethodInfo*))List_1_get_Capacity_m1_19027_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m1_19028_gshared (List_1_t1_1835 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m1_19028(__this, ___value, method) (( void (*) (List_1_t1_1835 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1_19028_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector4>::get_Count()
extern "C" int32_t List_1_get_Count_m1_19029_gshared (List_1_t1_1835 * __this, const MethodInfo* method);
#define List_1_get_Count_m1_19029(__this, method) (( int32_t (*) (List_1_t1_1835 *, const MethodInfo*))List_1_get_Count_m1_19029_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.Vector4>::get_Item(System.Int32)
extern "C" Vector4_t6_54  List_1_get_Item_m1_19030_gshared (List_1_t1_1835 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m1_19030(__this, ___index, method) (( Vector4_t6_54  (*) (List_1_t1_1835 *, int32_t, const MethodInfo*))List_1_get_Item_m1_19030_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m1_19031_gshared (List_1_t1_1835 * __this, int32_t ___index, Vector4_t6_54  ___value, const MethodInfo* method);
#define List_1_set_Item_m1_19031(__this, ___index, ___value, method) (( void (*) (List_1_t1_1835 *, int32_t, Vector4_t6_54 , const MethodInfo*))List_1_set_Item_m1_19031_gshared)(__this, ___index, ___value, method)
