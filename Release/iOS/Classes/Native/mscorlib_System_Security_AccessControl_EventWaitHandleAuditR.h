﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Security_AccessControl_AuditRule.h"
#include "mscorlib_System_Security_AccessControl_EventWaitHandleRights.h"

// System.Security.AccessControl.EventWaitHandleAuditRule
struct  EventWaitHandleAuditRule_t1_1150  : public AuditRule_t1_1119
{
	// System.Security.AccessControl.EventWaitHandleRights System.Security.AccessControl.EventWaitHandleAuditRule::rights
	int32_t ___rights_6;
};
