﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,VoxelBusters.NativePlugins.Internal.NPObject>
struct Dictionary_2_t1_1913;

#include "mscorlib_System_Object.h"

// VoxelBusters.NativePlugins.Internal.NPObjectManager
struct  NPObjectManager_t8_326  : public Object_t
{
};
struct NPObjectManager_t8_326_StaticFields{
	// System.Collections.Generic.Dictionary`2<System.String,VoxelBusters.NativePlugins.Internal.NPObject> VoxelBusters.NativePlugins.Internal.NPObjectManager::gameServicesObjectCollection
	Dictionary_2_t1_1913 * ___gameServicesObjectCollection_0;
};
