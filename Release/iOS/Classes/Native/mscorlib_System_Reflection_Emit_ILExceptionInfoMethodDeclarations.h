﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_Emit_ILExceptionInfo.h"

// System.Int32 System.Reflection.Emit.ILExceptionInfo::NumHandlers()
extern "C" int32_t ILExceptionInfo_NumHandlers_m1_5954 (ILExceptionInfo_t1_511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ILExceptionInfo::AddCatch(System.Type,System.Int32)
extern "C" void ILExceptionInfo_AddCatch_m1_5955 (ILExceptionInfo_t1_511 * __this, Type_t * ___extype, int32_t ___offset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ILExceptionInfo::AddFinally(System.Int32)
extern "C" void ILExceptionInfo_AddFinally_m1_5956 (ILExceptionInfo_t1_511 * __this, int32_t ___offset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ILExceptionInfo::AddFault(System.Int32)
extern "C" void ILExceptionInfo_AddFault_m1_5957 (ILExceptionInfo_t1_511 * __this, int32_t ___offset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ILExceptionInfo::AddFilter(System.Int32)
extern "C" void ILExceptionInfo_AddFilter_m1_5958 (ILExceptionInfo_t1_511 * __this, int32_t ___offset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ILExceptionInfo::End(System.Int32)
extern "C" void ILExceptionInfo_End_m1_5959 (ILExceptionInfo_t1_511 * __this, int32_t ___offset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.ILExceptionInfo::LastClauseType()
extern "C" int32_t ILExceptionInfo_LastClauseType_m1_5960 (ILExceptionInfo_t1_511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ILExceptionInfo::PatchFilterClause(System.Int32)
extern "C" void ILExceptionInfo_PatchFilterClause_m1_5961 (ILExceptionInfo_t1_511 * __this, int32_t ___start, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ILExceptionInfo::Debug(System.Int32)
extern "C" void ILExceptionInfo_Debug_m1_5962 (ILExceptionInfo_t1_511 * __this, int32_t ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ILExceptionInfo::add_block(System.Int32)
extern "C" void ILExceptionInfo_add_block_m1_5963 (ILExceptionInfo_t1_511 * __this, int32_t ___offset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
