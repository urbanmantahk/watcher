﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.Cryptography.PKCS8/PrivateKeyInfo
struct PrivateKeyInfo_t2_23;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Security.Cryptography.RSA
struct RSA_t1_175;
// System.Security.Cryptography.DSA
struct DSA_t1_160;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Cryptography_DSAParameters.h"

// System.Void Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::.ctor()
extern "C" void PrivateKeyInfo__ctor_m2_156 (PrivateKeyInfo_t2_23 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::.ctor(System.Byte[])
extern "C" void PrivateKeyInfo__ctor_m2_157 (PrivateKeyInfo_t2_23 * __this, ByteU5BU5D_t1_109* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::get_PrivateKey()
extern "C" ByteU5BU5D_t1_109* PrivateKeyInfo_get_PrivateKey_m2_158 (PrivateKeyInfo_t2_23 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::Decode(System.Byte[])
extern "C" void PrivateKeyInfo_Decode_m2_159 (PrivateKeyInfo_t2_23 * __this, ByteU5BU5D_t1_109* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::RemoveLeadingZero(System.Byte[])
extern "C" ByteU5BU5D_t1_109* PrivateKeyInfo_RemoveLeadingZero_m2_160 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___bigInt, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::Normalize(System.Byte[],System.Int32)
extern "C" ByteU5BU5D_t1_109* PrivateKeyInfo_Normalize_m2_161 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___bigInt, int32_t ___length, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.RSA Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::DecodeRSA(System.Byte[])
extern "C" RSA_t1_175 * PrivateKeyInfo_DecodeRSA_m2_162 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___keypair, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.DSA Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::DecodeDSA(System.Byte[],System.Security.Cryptography.DSAParameters)
extern "C" DSA_t1_160 * PrivateKeyInfo_DecodeDSA_m2_163 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___privateKey, DSAParameters_t1_1204  ___dsaParameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
