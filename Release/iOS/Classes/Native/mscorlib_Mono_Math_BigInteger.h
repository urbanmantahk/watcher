﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.UInt32[]
struct UInt32U5BU5D_t1_142;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t1_143;

#include "mscorlib_System_Object.h"

// Mono.Math.BigInteger
struct  BigInteger_t1_139  : public Object_t
{
	// System.UInt32 Mono.Math.BigInteger::length
	uint32_t ___length_2;
	// System.UInt32[] Mono.Math.BigInteger::data
	UInt32U5BU5D_t1_142* ___data_3;
};
struct BigInteger_t1_139_StaticFields{
	// System.UInt32[] Mono.Math.BigInteger::smallPrimes
	UInt32U5BU5D_t1_142* ___smallPrimes_4;
	// System.Security.Cryptography.RandomNumberGenerator Mono.Math.BigInteger::rng
	RandomNumberGenerator_t1_143 * ___rng_5;
};
