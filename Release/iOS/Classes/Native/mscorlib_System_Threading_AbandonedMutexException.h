﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Threading.Mutex
struct Mutex_t1_400;

#include "mscorlib_System_SystemException.h"

// System.Threading.AbandonedMutexException
struct  AbandonedMutexException_t1_1456  : public SystemException_t1_250
{
	// System.Threading.Mutex System.Threading.AbandonedMutexException::mutex
	Mutex_t1_400 * ___mutex_12;
	// System.Int32 System.Threading.AbandonedMutexException::mutex_index
	int32_t ___mutex_index_13;
};
