﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t1_92;

#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_12.h"

// VoxelBusters.Utility.Plist
struct  Plist_t8_51  : public Dictionary_2_t1_1903
{
};
struct Plist_t8_51_StaticFields{
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> VoxelBusters.Utility.Plist::<>f__switch$map0
	Dictionary_2_t1_92 * ___U3CU3Ef__switchU24map0_16;
};
