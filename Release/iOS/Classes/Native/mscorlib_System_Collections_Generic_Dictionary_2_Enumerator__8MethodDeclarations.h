﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__7MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1_21052(__this, ___dictionary, method) (( void (*) (Enumerator_t1_2399 *, Dictionary_2_t1_1827 *, const MethodInfo*))Enumerator__ctor_m1_20956_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_21053(__this, method) (( Object_t * (*) (Enumerator_t1_2399 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_20957_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1_21054(__this, method) (( void (*) (Enumerator_t1_2399 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_20958_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_21055(__this, method) (( DictionaryEntry_t1_284  (*) (Enumerator_t1_2399 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_20959_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_21056(__this, method) (( Object_t * (*) (Enumerator_t1_2399 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_20960_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_21057(__this, method) (( Object_t * (*) (Enumerator_t1_2399 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_20961_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::MoveNext()
#define Enumerator_MoveNext_m1_21058(__this, method) (( bool (*) (Enumerator_t1_2399 *, const MethodInfo*))Enumerator_MoveNext_m1_20962_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::get_Current()
#define Enumerator_get_Current_m1_21059(__this, method) (( KeyValuePair_2_t1_2396  (*) (Enumerator_t1_2399 *, const MethodInfo*))Enumerator_get_Current_m1_20963_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1_21060(__this, method) (( int32_t (*) (Enumerator_t1_2399 *, const MethodInfo*))Enumerator_get_CurrentKey_m1_20964_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1_21061(__this, method) (( LayoutCache_t6_173 * (*) (Enumerator_t1_2399 *, const MethodInfo*))Enumerator_get_CurrentValue_m1_20965_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::Reset()
#define Enumerator_Reset_m1_21062(__this, method) (( void (*) (Enumerator_t1_2399 *, const MethodInfo*))Enumerator_Reset_m1_20966_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::VerifyState()
#define Enumerator_VerifyState_m1_21063(__this, method) (( void (*) (Enumerator_t1_2399 *, const MethodInfo*))Enumerator_VerifyState_m1_20967_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1_21064(__this, method) (( void (*) (Enumerator_t1_2399 *, const MethodInfo*))Enumerator_VerifyCurrent_m1_20968_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::Dispose()
#define Enumerator_Dispose_m1_21065(__this, method) (( void (*) (Enumerator_t1_2399 *, const MethodInfo*))Enumerator_Dispose_m1_20969_gshared)(__this, method)
