﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_URL.h"

// System.Void VoxelBusters.Utility.URL::.ctor(System.String)
extern "C" void URL__ctor_m8_957 (URL_t8_156 * __this, String_t* ____URLString, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.Utility.URL::get_URLString()
extern "C" String_t* URL_get_URLString_m8_958 (URL_t8_156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.URL::set_URLString(System.String)
extern "C" void URL_set_URLString_m8_959 (URL_t8_156 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.Utility.URL VoxelBusters.Utility.URL::FileURLWithPath(System.String,System.String)
extern "C" URL_t8_156  URL_FileURLWithPath_m8_960 (Object_t * __this /* static, unused */, String_t* ____rootPath, String_t* ____relativePath, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.Utility.URL VoxelBusters.Utility.URL::FileURLWithPath(System.String)
extern "C" URL_t8_156  URL_FileURLWithPath_m8_961 (Object_t * __this /* static, unused */, String_t* ____path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.Utility.URL VoxelBusters.Utility.URL::URLWithString(System.String,System.String)
extern "C" URL_t8_156  URL_URLWithString_m8_962 (Object_t * __this /* static, unused */, String_t* ____rootURLString, String_t* ____relativePath, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.Utility.URL VoxelBusters.Utility.URL::URLWithString(System.String)
extern "C" URL_t8_156  URL_URLWithString_m8_963 (Object_t * __this /* static, unused */, String_t* ____URLString, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.Utility.URL::isFileReferenceURL()
extern "C" bool URL_isFileReferenceURL_m8_964 (URL_t8_156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.Utility.URL::ToString()
extern "C" String_t* URL_ToString_m8_965 (URL_t8_156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void URL_t8_156_marshal(const URL_t8_156& unmarshaled, URL_t8_156_marshaled& marshaled);
extern "C" void URL_t8_156_marshal_back(const URL_t8_156_marshaled& marshaled, URL_t8_156& unmarshaled);
extern "C" void URL_t8_156_marshal_cleanup(URL_t8_156_marshaled& marshaled);
