﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Type
struct Type_t;

#include "mscorlib_System_Attribute.h"

// System.Runtime.InteropServices.ComEventInterfaceAttribute
struct  ComEventInterfaceAttribute_t1_771  : public Attribute_t1_2
{
	// System.Type System.Runtime.InteropServices.ComEventInterfaceAttribute::si
	Type_t * ___si_0;
	// System.Type System.Runtime.InteropServices.ComEventInterfaceAttribute::ep
	Type_t * ___ep_1;
};
