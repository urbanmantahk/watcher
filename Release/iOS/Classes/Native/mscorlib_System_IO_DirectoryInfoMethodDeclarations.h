﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.IO.DirectoryInfo
struct DirectoryInfo_t1_399;
// System.String
struct String_t;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;
// System.IO.FileInfo[]
struct FileInfoU5BU5D_t1_1680;
// System.IO.DirectoryInfo[]
struct DirectoryInfoU5BU5D_t1_1681;
// System.IO.FileSystemInfo[]
struct FileSystemInfoU5BU5D_t1_1679;
// System.Collections.ArrayList
struct ArrayList_t1_170;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_IO_SearchOption.h"

// System.Void System.IO.DirectoryInfo::.ctor(System.String)
extern "C" void DirectoryInfo__ctor_m1_4791 (DirectoryInfo_t1_399 * __this, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.DirectoryInfo::.ctor(System.String,System.Boolean)
extern "C" void DirectoryInfo__ctor_m1_4792 (DirectoryInfo_t1_399 * __this, String_t* ___path, bool ___simpleOriginalPath, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.DirectoryInfo::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void DirectoryInfo__ctor_m1_4793 (DirectoryInfo_t1_399 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.DirectoryInfo::Initialize()
extern "C" void DirectoryInfo_Initialize_m1_4794 (DirectoryInfo_t1_399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IO.DirectoryInfo::get_Exists()
extern "C" bool DirectoryInfo_get_Exists_m1_4795 (DirectoryInfo_t1_399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.IO.DirectoryInfo::get_Name()
extern "C" String_t* DirectoryInfo_get_Name_m1_4796 (DirectoryInfo_t1_399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.DirectoryInfo System.IO.DirectoryInfo::get_Parent()
extern "C" DirectoryInfo_t1_399 * DirectoryInfo_get_Parent_m1_4797 (DirectoryInfo_t1_399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.DirectoryInfo System.IO.DirectoryInfo::get_Root()
extern "C" DirectoryInfo_t1_399 * DirectoryInfo_get_Root_m1_4798 (DirectoryInfo_t1_399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.DirectoryInfo::Create()
extern "C" void DirectoryInfo_Create_m1_4799 (DirectoryInfo_t1_399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.DirectoryInfo System.IO.DirectoryInfo::CreateSubdirectory(System.String)
extern "C" DirectoryInfo_t1_399 * DirectoryInfo_CreateSubdirectory_m1_4800 (DirectoryInfo_t1_399 * __this, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.FileInfo[] System.IO.DirectoryInfo::GetFiles()
extern "C" FileInfoU5BU5D_t1_1680* DirectoryInfo_GetFiles_m1_4801 (DirectoryInfo_t1_399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.FileInfo[] System.IO.DirectoryInfo::GetFiles(System.String)
extern "C" FileInfoU5BU5D_t1_1680* DirectoryInfo_GetFiles_m1_4802 (DirectoryInfo_t1_399 * __this, String_t* ___searchPattern, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.DirectoryInfo[] System.IO.DirectoryInfo::GetDirectories()
extern "C" DirectoryInfoU5BU5D_t1_1681* DirectoryInfo_GetDirectories_m1_4803 (DirectoryInfo_t1_399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.DirectoryInfo[] System.IO.DirectoryInfo::GetDirectories(System.String)
extern "C" DirectoryInfoU5BU5D_t1_1681* DirectoryInfo_GetDirectories_m1_4804 (DirectoryInfo_t1_399 * __this, String_t* ___searchPattern, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.FileSystemInfo[] System.IO.DirectoryInfo::GetFileSystemInfos()
extern "C" FileSystemInfoU5BU5D_t1_1679* DirectoryInfo_GetFileSystemInfos_m1_4805 (DirectoryInfo_t1_399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.FileSystemInfo[] System.IO.DirectoryInfo::GetFileSystemInfos(System.String)
extern "C" FileSystemInfoU5BU5D_t1_1679* DirectoryInfo_GetFileSystemInfos_m1_4806 (DirectoryInfo_t1_399 * __this, String_t* ___searchPattern, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.DirectoryInfo::Delete()
extern "C" void DirectoryInfo_Delete_m1_4807 (DirectoryInfo_t1_399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.DirectoryInfo::Delete(System.Boolean)
extern "C" void DirectoryInfo_Delete_m1_4808 (DirectoryInfo_t1_399 * __this, bool ___recursive, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.DirectoryInfo::MoveTo(System.String)
extern "C" void DirectoryInfo_MoveTo_m1_4809 (DirectoryInfo_t1_399 * __this, String_t* ___destDirName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.IO.DirectoryInfo::ToString()
extern "C" String_t* DirectoryInfo_ToString_m1_4810 (DirectoryInfo_t1_399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.DirectoryInfo[] System.IO.DirectoryInfo::GetDirectories(System.String,System.IO.SearchOption)
extern "C" DirectoryInfoU5BU5D_t1_1681* DirectoryInfo_GetDirectories_m1_4811 (DirectoryInfo_t1_399 * __this, String_t* ___searchPattern, int32_t ___searchOption, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.IO.DirectoryInfo::GetFilesSubdirs(System.Collections.ArrayList,System.String)
extern "C" int32_t DirectoryInfo_GetFilesSubdirs_m1_4812 (DirectoryInfo_t1_399 * __this, ArrayList_t1_170 * ___l, String_t* ___pattern, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.FileInfo[] System.IO.DirectoryInfo::GetFiles(System.String,System.IO.SearchOption)
extern "C" FileInfoU5BU5D_t1_1680* DirectoryInfo_GetFiles_m1_4813 (DirectoryInfo_t1_399 * __this, String_t* ___searchPattern, int32_t ___searchOption, const MethodInfo* method) IL2CPP_METHOD_ATTR;
