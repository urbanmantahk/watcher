﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Globalization.JapaneseLunisolarCalendar
struct JapaneseLunisolarCalendar_t1_379;
// System.Int32[]
struct Int32U5BU5D_t1_275;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime.h"

// System.Void System.Globalization.JapaneseLunisolarCalendar::.ctor()
extern "C" void JapaneseLunisolarCalendar__ctor_m1_4245 (JapaneseLunisolarCalendar_t1_379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.JapaneseLunisolarCalendar::.cctor()
extern "C" void JapaneseLunisolarCalendar__cctor_m1_4246 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.JapaneseLunisolarCalendar::get_ActualCurrentEra()
extern "C" int32_t JapaneseLunisolarCalendar_get_ActualCurrentEra_m1_4247 (JapaneseLunisolarCalendar_t1_379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] System.Globalization.JapaneseLunisolarCalendar::get_Eras()
extern "C" Int32U5BU5D_t1_275* JapaneseLunisolarCalendar_get_Eras_m1_4248 (JapaneseLunisolarCalendar_t1_379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.JapaneseLunisolarCalendar::GetEra(System.DateTime)
extern "C" int32_t JapaneseLunisolarCalendar_GetEra_m1_4249 (JapaneseLunisolarCalendar_t1_379 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.JapaneseLunisolarCalendar::get_MinSupportedDateTime()
extern "C" DateTime_t1_150  JapaneseLunisolarCalendar_get_MinSupportedDateTime_m1_4250 (JapaneseLunisolarCalendar_t1_379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.JapaneseLunisolarCalendar::get_MaxSupportedDateTime()
extern "C" DateTime_t1_150  JapaneseLunisolarCalendar_get_MaxSupportedDateTime_m1_4251 (JapaneseLunisolarCalendar_t1_379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
