﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_15MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1_23083(__this, ___host, method) (( void (*) (Enumerator_t1_1884 *, Dictionary_2_t1_1861 *, const MethodInfo*))Enumerator__ctor_m1_20988_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_23084(__this, method) (( Object_t * (*) (Enumerator_t1_1884 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_20989_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1_23085(__this, method) (( void (*) (Enumerator_t1_1884 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_20990_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::Dispose()
#define Enumerator_Dispose_m1_23086(__this, method) (( void (*) (Enumerator_t1_1884 *, const MethodInfo*))Enumerator_Dispose_m1_20991_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::MoveNext()
#define Enumerator_MoveNext_m1_14989(__this, method) (( bool (*) (Enumerator_t1_1884 *, const MethodInfo*))Enumerator_MoveNext_m1_20992_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::get_Current()
#define Enumerator_get_Current_m1_14988(__this, method) (( PointerEventData_t7_36 * (*) (Enumerator_t1_1884 *, const MethodInfo*))Enumerator_get_Current_m1_20993_gshared)(__this, method)
