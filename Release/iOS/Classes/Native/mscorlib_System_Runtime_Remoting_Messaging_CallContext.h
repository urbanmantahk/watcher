﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Runtime.Remoting.Messaging.Header[]
struct HeaderU5BU5D_t1_927;
// System.Collections.Hashtable
struct Hashtable_t1_100;

#include "mscorlib_System_Object.h"

// System.Runtime.Remoting.Messaging.CallContext
struct  CallContext_t1_926  : public Object_t
{
};
struct CallContext_t1_926_ThreadStaticFields{
	// System.Runtime.Remoting.Messaging.Header[] System.Runtime.Remoting.Messaging.CallContext::Headers
	HeaderU5BU5D_t1_927* ___Headers_0;
	// System.Collections.Hashtable System.Runtime.Remoting.Messaging.CallContext::datastore
	Hashtable_t1_100 * ___datastore_1;
};
