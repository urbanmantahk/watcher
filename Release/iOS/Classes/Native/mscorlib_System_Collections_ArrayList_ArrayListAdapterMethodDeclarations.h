﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ArrayList/ArrayListAdapter
struct ArrayListAdapter_t1_261;
// System.Collections.IList
struct IList_t1_262;
// System.Object
struct Object_t;
// System.Collections.ICollection
struct ICollection_t1_280;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Collections.IComparer
struct IComparer_t1_295;
// System.Collections.ArrayList
struct ArrayList_t1_170;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.ArrayList/ArrayListAdapter::.ctor(System.Collections.IList)
extern "C" void ArrayListAdapter__ctor_m1_2802 (ArrayListAdapter_t1_261 * __this, Object_t * ___adaptee, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.ArrayList/ArrayListAdapter::get_Item(System.Int32)
extern "C" Object_t * ArrayListAdapter_get_Item_m1_2803 (ArrayListAdapter_t1_261 * __this, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/ArrayListAdapter::set_Item(System.Int32,System.Object)
extern "C" void ArrayListAdapter_set_Item_m1_2804 (ArrayListAdapter_t1_261 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.ArrayList/ArrayListAdapter::get_Count()
extern "C" int32_t ArrayListAdapter_get_Count_m1_2805 (ArrayListAdapter_t1_261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.ArrayList/ArrayListAdapter::get_Capacity()
extern "C" int32_t ArrayListAdapter_get_Capacity_m1_2806 (ArrayListAdapter_t1_261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/ArrayListAdapter::set_Capacity(System.Int32)
extern "C" void ArrayListAdapter_set_Capacity_m1_2807 (ArrayListAdapter_t1_261 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.ArrayList/ArrayListAdapter::get_IsFixedSize()
extern "C" bool ArrayListAdapter_get_IsFixedSize_m1_2808 (ArrayListAdapter_t1_261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.ArrayList/ArrayListAdapter::get_IsReadOnly()
extern "C" bool ArrayListAdapter_get_IsReadOnly_m1_2809 (ArrayListAdapter_t1_261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.ArrayList/ArrayListAdapter::get_SyncRoot()
extern "C" Object_t * ArrayListAdapter_get_SyncRoot_m1_2810 (ArrayListAdapter_t1_261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.ArrayList/ArrayListAdapter::Add(System.Object)
extern "C" int32_t ArrayListAdapter_Add_m1_2811 (ArrayListAdapter_t1_261 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/ArrayListAdapter::Clear()
extern "C" void ArrayListAdapter_Clear_m1_2812 (ArrayListAdapter_t1_261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.ArrayList/ArrayListAdapter::Contains(System.Object)
extern "C" bool ArrayListAdapter_Contains_m1_2813 (ArrayListAdapter_t1_261 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.ArrayList/ArrayListAdapter::IndexOf(System.Object)
extern "C" int32_t ArrayListAdapter_IndexOf_m1_2814 (ArrayListAdapter_t1_261 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.ArrayList/ArrayListAdapter::IndexOf(System.Object,System.Int32)
extern "C" int32_t ArrayListAdapter_IndexOf_m1_2815 (ArrayListAdapter_t1_261 * __this, Object_t * ___value, int32_t ___startIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.ArrayList/ArrayListAdapter::IndexOf(System.Object,System.Int32,System.Int32)
extern "C" int32_t ArrayListAdapter_IndexOf_m1_2816 (ArrayListAdapter_t1_261 * __this, Object_t * ___value, int32_t ___startIndex, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.ArrayList/ArrayListAdapter::LastIndexOf(System.Object)
extern "C" int32_t ArrayListAdapter_LastIndexOf_m1_2817 (ArrayListAdapter_t1_261 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.ArrayList/ArrayListAdapter::LastIndexOf(System.Object,System.Int32)
extern "C" int32_t ArrayListAdapter_LastIndexOf_m1_2818 (ArrayListAdapter_t1_261 * __this, Object_t * ___value, int32_t ___startIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.ArrayList/ArrayListAdapter::LastIndexOf(System.Object,System.Int32,System.Int32)
extern "C" int32_t ArrayListAdapter_LastIndexOf_m1_2819 (ArrayListAdapter_t1_261 * __this, Object_t * ___value, int32_t ___startIndex, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/ArrayListAdapter::Insert(System.Int32,System.Object)
extern "C" void ArrayListAdapter_Insert_m1_2820 (ArrayListAdapter_t1_261 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/ArrayListAdapter::InsertRange(System.Int32,System.Collections.ICollection)
extern "C" void ArrayListAdapter_InsertRange_m1_2821 (ArrayListAdapter_t1_261 * __this, int32_t ___index, Object_t * ___c, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/ArrayListAdapter::Remove(System.Object)
extern "C" void ArrayListAdapter_Remove_m1_2822 (ArrayListAdapter_t1_261 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/ArrayListAdapter::RemoveAt(System.Int32)
extern "C" void ArrayListAdapter_RemoveAt_m1_2823 (ArrayListAdapter_t1_261 * __this, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/ArrayListAdapter::RemoveRange(System.Int32,System.Int32)
extern "C" void ArrayListAdapter_RemoveRange_m1_2824 (ArrayListAdapter_t1_261 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/ArrayListAdapter::Reverse()
extern "C" void ArrayListAdapter_Reverse_m1_2825 (ArrayListAdapter_t1_261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/ArrayListAdapter::Reverse(System.Int32,System.Int32)
extern "C" void ArrayListAdapter_Reverse_m1_2826 (ArrayListAdapter_t1_261 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/ArrayListAdapter::SetRange(System.Int32,System.Collections.ICollection)
extern "C" void ArrayListAdapter_SetRange_m1_2827 (ArrayListAdapter_t1_261 * __this, int32_t ___index, Object_t * ___c, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/ArrayListAdapter::CopyTo(System.Array)
extern "C" void ArrayListAdapter_CopyTo_m1_2828 (ArrayListAdapter_t1_261 * __this, Array_t * ___array, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/ArrayListAdapter::CopyTo(System.Array,System.Int32)
extern "C" void ArrayListAdapter_CopyTo_m1_2829 (ArrayListAdapter_t1_261 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/ArrayListAdapter::CopyTo(System.Int32,System.Array,System.Int32,System.Int32)
extern "C" void ArrayListAdapter_CopyTo_m1_2830 (ArrayListAdapter_t1_261 * __this, int32_t ___index, Array_t * ___array, int32_t ___arrayIndex, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.ArrayList/ArrayListAdapter::get_IsSynchronized()
extern "C" bool ArrayListAdapter_get_IsSynchronized_m1_2831 (ArrayListAdapter_t1_261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Collections.ArrayList/ArrayListAdapter::GetEnumerator()
extern "C" Object_t * ArrayListAdapter_GetEnumerator_m1_2832 (ArrayListAdapter_t1_261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Collections.ArrayList/ArrayListAdapter::GetEnumerator(System.Int32,System.Int32)
extern "C" Object_t * ArrayListAdapter_GetEnumerator_m1_2833 (ArrayListAdapter_t1_261 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/ArrayListAdapter::AddRange(System.Collections.ICollection)
extern "C" void ArrayListAdapter_AddRange_m1_2834 (ArrayListAdapter_t1_261 * __this, Object_t * ___c, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.ArrayList/ArrayListAdapter::BinarySearch(System.Object)
extern "C" int32_t ArrayListAdapter_BinarySearch_m1_2835 (ArrayListAdapter_t1_261 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.ArrayList/ArrayListAdapter::BinarySearch(System.Object,System.Collections.IComparer)
extern "C" int32_t ArrayListAdapter_BinarySearch_m1_2836 (ArrayListAdapter_t1_261 * __this, Object_t * ___value, Object_t * ___comparer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.ArrayList/ArrayListAdapter::BinarySearch(System.Int32,System.Int32,System.Object,System.Collections.IComparer)
extern "C" int32_t ArrayListAdapter_BinarySearch_m1_2837 (ArrayListAdapter_t1_261 * __this, int32_t ___index, int32_t ___count, Object_t * ___value, Object_t * ___comparer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.ArrayList/ArrayListAdapter::Clone()
extern "C" Object_t * ArrayListAdapter_Clone_m1_2838 (ArrayListAdapter_t1_261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList System.Collections.ArrayList/ArrayListAdapter::GetRange(System.Int32,System.Int32)
extern "C" ArrayList_t1_170 * ArrayListAdapter_GetRange_m1_2839 (ArrayListAdapter_t1_261 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/ArrayListAdapter::TrimToSize()
extern "C" void ArrayListAdapter_TrimToSize_m1_2840 (ArrayListAdapter_t1_261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/ArrayListAdapter::Sort()
extern "C" void ArrayListAdapter_Sort_m1_2841 (ArrayListAdapter_t1_261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/ArrayListAdapter::Sort(System.Collections.IComparer)
extern "C" void ArrayListAdapter_Sort_m1_2842 (ArrayListAdapter_t1_261 * __this, Object_t * ___comparer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/ArrayListAdapter::Sort(System.Int32,System.Int32,System.Collections.IComparer)
extern "C" void ArrayListAdapter_Sort_m1_2843 (ArrayListAdapter_t1_261 * __this, int32_t ___index, int32_t ___count, Object_t * ___comparer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/ArrayListAdapter::Swap(System.Collections.IList,System.Int32,System.Int32)
extern "C" void ArrayListAdapter_Swap_m1_2844 (Object_t * __this /* static, unused */, Object_t * ___list, int32_t ___x, int32_t ___y, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/ArrayListAdapter::QuickSort(System.Collections.IList,System.Int32,System.Int32,System.Collections.IComparer)
extern "C" void ArrayListAdapter_QuickSort_m1_2845 (Object_t * __this /* static, unused */, Object_t * ___list, int32_t ___left, int32_t ___right, Object_t * ___comparer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Collections.ArrayList/ArrayListAdapter::ToArray()
extern "C" ObjectU5BU5D_t1_272* ArrayListAdapter_ToArray_m1_2846 (ArrayListAdapter_t1_261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Array System.Collections.ArrayList/ArrayListAdapter::ToArray(System.Type)
extern "C" Array_t * ArrayListAdapter_ToArray_m1_2847 (ArrayListAdapter_t1_261 * __this, Type_t * ___elementType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
