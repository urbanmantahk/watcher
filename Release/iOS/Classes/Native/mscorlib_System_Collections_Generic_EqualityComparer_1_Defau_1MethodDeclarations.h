﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Reflection.CustomAttributeTypedArgument>
struct DefaultComparer_t1_2076;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArgument.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Reflection.CustomAttributeTypedArgument>::.ctor()
extern "C" void DefaultComparer__ctor_m1_16481_gshared (DefaultComparer_t1_2076 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1_16481(__this, method) (( void (*) (DefaultComparer_t1_2076 *, const MethodInfo*))DefaultComparer__ctor_m1_16481_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Reflection.CustomAttributeTypedArgument>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m1_16482_gshared (DefaultComparer_t1_2076 * __this, CustomAttributeTypedArgument_t1_594  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m1_16482(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t1_2076 *, CustomAttributeTypedArgument_t1_594 , const MethodInfo*))DefaultComparer_GetHashCode_m1_16482_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Reflection.CustomAttributeTypedArgument>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m1_16483_gshared (DefaultComparer_t1_2076 * __this, CustomAttributeTypedArgument_t1_594  ___x, CustomAttributeTypedArgument_t1_594  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m1_16483(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t1_2076 *, CustomAttributeTypedArgument_t1_594 , CustomAttributeTypedArgument_t1_594 , const MethodInfo*))DefaultComparer_Equals_m1_16483_gshared)(__this, ___x, ___y, method)
