﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__4MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Action,System.Collections.Generic.Dictionary`2<System.String,System.Single>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1_27046(__this, ___dictionary, method) (( void (*) (Enumerator_t1_2703 *, Dictionary_2_t1_1907 *, const MethodInfo*))Enumerator__ctor_m1_15990_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Action,System.Collections.Generic.Dictionary`2<System.String,System.Single>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_27047(__this, method) (( Object_t * (*) (Enumerator_t1_2703 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_15991_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Action,System.Collections.Generic.Dictionary`2<System.String,System.Single>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1_27048(__this, method) (( void (*) (Enumerator_t1_2703 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_15992_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Action,System.Collections.Generic.Dictionary`2<System.String,System.Single>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_27049(__this, method) (( DictionaryEntry_t1_284  (*) (Enumerator_t1_2703 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_15993_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Action,System.Collections.Generic.Dictionary`2<System.String,System.Single>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_27050(__this, method) (( Object_t * (*) (Enumerator_t1_2703 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_15994_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Action,System.Collections.Generic.Dictionary`2<System.String,System.Single>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_27051(__this, method) (( Object_t * (*) (Enumerator_t1_2703 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_15995_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Action,System.Collections.Generic.Dictionary`2<System.String,System.Single>>::MoveNext()
#define Enumerator_MoveNext_m1_27052(__this, method) (( bool (*) (Enumerator_t1_2703 *, const MethodInfo*))Enumerator_MoveNext_m1_15996_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Action,System.Collections.Generic.Dictionary`2<System.String,System.Single>>::get_Current()
#define Enumerator_get_Current_m1_27053(__this, method) (( KeyValuePair_2_t1_2700  (*) (Enumerator_t1_2703 *, const MethodInfo*))Enumerator_get_Current_m1_15997_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Action,System.Collections.Generic.Dictionary`2<System.String,System.Single>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1_27054(__this, method) (( Action_t5_11 * (*) (Enumerator_t1_2703 *, const MethodInfo*))Enumerator_get_CurrentKey_m1_15998_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Action,System.Collections.Generic.Dictionary`2<System.String,System.Single>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1_27055(__this, method) (( Dictionary_2_t1_1929 * (*) (Enumerator_t1_2703 *, const MethodInfo*))Enumerator_get_CurrentValue_m1_15999_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Action,System.Collections.Generic.Dictionary`2<System.String,System.Single>>::Reset()
#define Enumerator_Reset_m1_27056(__this, method) (( void (*) (Enumerator_t1_2703 *, const MethodInfo*))Enumerator_Reset_m1_16000_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Action,System.Collections.Generic.Dictionary`2<System.String,System.Single>>::VerifyState()
#define Enumerator_VerifyState_m1_27057(__this, method) (( void (*) (Enumerator_t1_2703 *, const MethodInfo*))Enumerator_VerifyState_m1_16001_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Action,System.Collections.Generic.Dictionary`2<System.String,System.Single>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1_27058(__this, method) (( void (*) (Enumerator_t1_2703 *, const MethodInfo*))Enumerator_VerifyCurrent_m1_16002_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Action,System.Collections.Generic.Dictionary`2<System.String,System.Single>>::Dispose()
#define Enumerator_Dispose_m1_27059(__this, method) (( void (*) (Enumerator_t1_2703 *, const MethodInfo*))Enumerator_Dispose_m1_16003_gshared)(__this, method)
