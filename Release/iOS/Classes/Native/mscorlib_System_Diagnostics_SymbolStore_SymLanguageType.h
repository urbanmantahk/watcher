﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object.h"
#include "mscorlib_System_Guid.h"

// System.Diagnostics.SymbolStore.SymLanguageType
struct  SymLanguageType_t1_320  : public Object_t
{
};
struct SymLanguageType_t1_320_StaticFields{
	// System.Guid System.Diagnostics.SymbolStore.SymLanguageType::Basic
	Guid_t1_319  ___Basic_0;
	// System.Guid System.Diagnostics.SymbolStore.SymLanguageType::C
	Guid_t1_319  ___C_1;
	// System.Guid System.Diagnostics.SymbolStore.SymLanguageType::Cobol
	Guid_t1_319  ___Cobol_2;
	// System.Guid System.Diagnostics.SymbolStore.SymLanguageType::CPlusPlus
	Guid_t1_319  ___CPlusPlus_3;
	// System.Guid System.Diagnostics.SymbolStore.SymLanguageType::CSharp
	Guid_t1_319  ___CSharp_4;
	// System.Guid System.Diagnostics.SymbolStore.SymLanguageType::ILAssembly
	Guid_t1_319  ___ILAssembly_5;
	// System.Guid System.Diagnostics.SymbolStore.SymLanguageType::Java
	Guid_t1_319  ___Java_6;
	// System.Guid System.Diagnostics.SymbolStore.SymLanguageType::JScript
	Guid_t1_319  ___JScript_7;
	// System.Guid System.Diagnostics.SymbolStore.SymLanguageType::MCPlusPlus
	Guid_t1_319  ___MCPlusPlus_8;
	// System.Guid System.Diagnostics.SymbolStore.SymLanguageType::Pascal
	Guid_t1_319  ___Pascal_9;
	// System.Guid System.Diagnostics.SymbolStore.SymLanguageType::SMC
	Guid_t1_319  ___SMC_10;
};
