﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object.h"

// Microsoft.Win32.ExpandString
struct  ExpandString_t1_98  : public Object_t
{
	// System.String Microsoft.Win32.ExpandString::value
	String_t* ___value_0;
};
