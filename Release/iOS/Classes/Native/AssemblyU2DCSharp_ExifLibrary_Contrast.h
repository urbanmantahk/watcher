﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "AssemblyU2DCSharp_ExifLibrary_Contrast.h"

// ExifLibrary.Contrast
struct  Contrast_t8_82 
{
	// System.UInt16 ExifLibrary.Contrast::value__
	uint16_t ___value___1;
};
