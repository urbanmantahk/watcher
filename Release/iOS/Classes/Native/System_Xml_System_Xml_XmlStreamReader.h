﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Xml.XmlInputStream
struct XmlInputStream_t4_140;
// System.Xml.XmlException
struct XmlException_t4_137;

#include "System_Xml_System_Xml_NonBlockingStreamReader.h"

// System.Xml.XmlStreamReader
struct  XmlStreamReader_t4_138  : public NonBlockingStreamReader_t4_139
{
	// System.Xml.XmlInputStream System.Xml.XmlStreamReader::input
	XmlInputStream_t4_140 * ___input_12;
};
struct XmlStreamReader_t4_138_StaticFields{
	// System.Xml.XmlException System.Xml.XmlStreamReader::invalidDataException
	XmlException_t4_137 * ___invalidDataException_13;
};
