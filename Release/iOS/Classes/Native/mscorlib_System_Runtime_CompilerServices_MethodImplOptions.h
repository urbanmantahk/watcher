﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Runtime_CompilerServices_MethodImplOptions.h"

// System.Runtime.CompilerServices.MethodImplOptions
struct  MethodImplOptions_t1_704 
{
	// System.Int32 System.Runtime.CompilerServices.MethodImplOptions::value__
	int32_t ___value___1;
};
