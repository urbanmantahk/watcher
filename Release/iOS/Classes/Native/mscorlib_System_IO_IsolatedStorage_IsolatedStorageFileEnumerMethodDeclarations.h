﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.IO.IsolatedStorage.IsolatedStorageFileEnumerator
struct IsolatedStorageFileEnumerator_t1_401;
// System.String
struct String_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_IsolatedStorage_IsolatedStorageScope.h"

// System.Void System.IO.IsolatedStorage.IsolatedStorageFileEnumerator::.ctor(System.IO.IsolatedStorage.IsolatedStorageScope,System.String)
extern "C" void IsolatedStorageFileEnumerator__ctor_m1_4644 (IsolatedStorageFileEnumerator_t1_401 * __this, int32_t ___scope, String_t* ___root, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.IO.IsolatedStorage.IsolatedStorageFileEnumerator::get_Current()
extern "C" Object_t * IsolatedStorageFileEnumerator_get_Current_m1_4645 (IsolatedStorageFileEnumerator_t1_401 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IO.IsolatedStorage.IsolatedStorageFileEnumerator::MoveNext()
extern "C" bool IsolatedStorageFileEnumerator_MoveNext_m1_4646 (IsolatedStorageFileEnumerator_t1_401 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.IsolatedStorage.IsolatedStorageFileEnumerator::Reset()
extern "C" void IsolatedStorageFileEnumerator_Reset_m1_4647 (IsolatedStorageFileEnumerator_t1_401 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
