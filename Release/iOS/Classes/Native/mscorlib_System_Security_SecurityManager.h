﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Object_t;
// System.Collections.ArrayList
struct ArrayList_t1_170;
// System.Security.IPermission
struct IPermission_t1_1400;
// System.Collections.Hashtable
struct Hashtable_t1_100;
// System.Security.Policy.PolicyLevel
struct PolicyLevel_t1_1357;
// System.Security.Permissions.SecurityPermission
struct SecurityPermission_t1_1308;

#include "mscorlib_System_Object.h"

// System.Security.SecurityManager
struct  SecurityManager_t1_1406  : public Object_t
{
};
struct SecurityManager_t1_1406_StaticFields{
	// System.Object System.Security.SecurityManager::_lockObject
	Object_t * ____lockObject_0;
	// System.Collections.ArrayList System.Security.SecurityManager::_hierarchy
	ArrayList_t1_170 * ____hierarchy_1;
	// System.Security.IPermission System.Security.SecurityManager::_unmanagedCode
	Object_t * ____unmanagedCode_2;
	// System.Collections.Hashtable System.Security.SecurityManager::_declsecCache
	Hashtable_t1_100 * ____declsecCache_3;
	// System.Security.Policy.PolicyLevel System.Security.SecurityManager::_level
	PolicyLevel_t1_1357 * ____level_4;
	// System.Security.Permissions.SecurityPermission System.Security.SecurityManager::_execution
	SecurityPermission_t1_1308 * ____execution_5;
};
