﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.MonoListItem
struct MonoListItem_t1_1576;

#include "codegen/il2cpp-codegen.h"

// System.Void System.MonoListItem::.ctor()
extern "C" void MonoListItem__ctor_m1_14327 (MonoListItem_t1_1576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
