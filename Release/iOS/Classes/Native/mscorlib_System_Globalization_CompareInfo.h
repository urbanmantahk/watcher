﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Mono.Globalization.Unicode.SimpleCollator
struct SimpleCollator_t1_123;
// System.Collections.Hashtable
struct Hashtable_t1_100;
// System.Object
struct Object_t;

#include "mscorlib_System_Object.h"
#include "mscorlib_System_Globalization_CompareOptions.h"

// System.Globalization.CompareInfo
struct  CompareInfo_t1_282  : public Object_t
{
	// System.Int32 System.Globalization.CompareInfo::culture
	int32_t ___culture_3;
	// System.String System.Globalization.CompareInfo::icu_name
	String_t* ___icu_name_4;
	// System.Int32 System.Globalization.CompareInfo::win32LCID
	int32_t ___win32LCID_5;
	// System.String System.Globalization.CompareInfo::m_name
	String_t* ___m_name_6;
	// Mono.Globalization.Unicode.SimpleCollator System.Globalization.CompareInfo::collator
	SimpleCollator_t1_123 * ___collator_7;
};
struct CompareInfo_t1_282_StaticFields{
	// System.Boolean System.Globalization.CompareInfo::useManagedCollation
	bool ___useManagedCollation_2;
	// System.Collections.Hashtable System.Globalization.CompareInfo::collators
	Hashtable_t1_100 * ___collators_8;
	// System.Object System.Globalization.CompareInfo::monitor
	Object_t * ___monitor_9;
};
