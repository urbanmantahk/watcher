﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "AssemblyU2DCSharp_ExifLibrary_GPSStatus.h"

// ExifLibrary.GPSStatus
struct  GPSStatus_t8_89 
{
	// System.Byte ExifLibrary.GPSStatus::value__
	uint8_t ___value___1;
};
