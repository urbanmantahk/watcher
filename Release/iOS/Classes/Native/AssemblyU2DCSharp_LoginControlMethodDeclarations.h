﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LoginControl
struct LoginControl_t8_11;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void LoginControl::.ctor()
extern "C" void LoginControl__ctor_m8_92 (LoginControl_t8_11 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginControl::Start()
extern "C" void LoginControl_Start_m8_93 (LoginControl_t8_11 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginControl::Login()
extern "C" void LoginControl_Login_m8_94 (LoginControl_t8_11 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator LoginControl::LoginRoute(System.String,System.String)
extern "C" Object_t * LoginControl_LoginRoute_m8_95 (LoginControl_t8_11 * __this, String_t* ____phone, String_t* ____password, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginControl::DisplayError(System.String)
extern "C" void LoginControl_DisplayError_m8_96 (LoginControl_t8_11 * __this, String_t* ____msg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
