﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.RSAPKCS1KeyExchangeFormatter
struct RSAPKCS1KeyExchangeFormatter_t1_1239;
// System.Security.Cryptography.AsymmetricAlgorithm
struct AsymmetricAlgorithm_t1_228;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t1_143;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Cryptography.RSAPKCS1KeyExchangeFormatter::.ctor()
extern "C" void RSAPKCS1KeyExchangeFormatter__ctor_m1_10576 (RSAPKCS1KeyExchangeFormatter_t1_1239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RSAPKCS1KeyExchangeFormatter::.ctor(System.Security.Cryptography.AsymmetricAlgorithm)
extern "C" void RSAPKCS1KeyExchangeFormatter__ctor_m1_10577 (RSAPKCS1KeyExchangeFormatter_t1_1239 * __this, AsymmetricAlgorithm_t1_228 * ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.RandomNumberGenerator System.Security.Cryptography.RSAPKCS1KeyExchangeFormatter::get_Rng()
extern "C" RandomNumberGenerator_t1_143 * RSAPKCS1KeyExchangeFormatter_get_Rng_m1_10578 (RSAPKCS1KeyExchangeFormatter_t1_1239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RSAPKCS1KeyExchangeFormatter::set_Rng(System.Security.Cryptography.RandomNumberGenerator)
extern "C" void RSAPKCS1KeyExchangeFormatter_set_Rng_m1_10579 (RSAPKCS1KeyExchangeFormatter_t1_1239 * __this, RandomNumberGenerator_t1_143 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Cryptography.RSAPKCS1KeyExchangeFormatter::get_Parameters()
extern "C" String_t* RSAPKCS1KeyExchangeFormatter_get_Parameters_m1_10580 (RSAPKCS1KeyExchangeFormatter_t1_1239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.RSAPKCS1KeyExchangeFormatter::CreateKeyExchange(System.Byte[])
extern "C" ByteU5BU5D_t1_109* RSAPKCS1KeyExchangeFormatter_CreateKeyExchange_m1_10581 (RSAPKCS1KeyExchangeFormatter_t1_1239 * __this, ByteU5BU5D_t1_109* ___rgbData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.RSAPKCS1KeyExchangeFormatter::CreateKeyExchange(System.Byte[],System.Type)
extern "C" ByteU5BU5D_t1_109* RSAPKCS1KeyExchangeFormatter_CreateKeyExchange_m1_10582 (RSAPKCS1KeyExchangeFormatter_t1_1239 * __this, ByteU5BU5D_t1_109* ___rgbData, Type_t * ___symAlgType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RSAPKCS1KeyExchangeFormatter::SetRSAKey(System.Security.Cryptography.AsymmetricAlgorithm)
extern "C" void RSAPKCS1KeyExchangeFormatter_SetRSAKey_m1_10583 (RSAPKCS1KeyExchangeFormatter_t1_1239 * __this, AsymmetricAlgorithm_t1_228 * ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RSAPKCS1KeyExchangeFormatter::SetKey(System.Security.Cryptography.AsymmetricAlgorithm)
extern "C" void RSAPKCS1KeyExchangeFormatter_SetKey_m1_10584 (RSAPKCS1KeyExchangeFormatter_t1_1239 * __this, AsymmetricAlgorithm_t1_228 * ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
