﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_SystemException.h"

// System.Runtime.InteropServices.SafeArrayTypeMismatchException
struct  SafeArrayTypeMismatchException_t1_825  : public SystemException_t1_250
{
};
