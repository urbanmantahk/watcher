﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VoxelBusters.NativePlugins.MediaLibrarySettings/AndroidSettings
struct AndroidSettings_t8_252;

#include "mscorlib_System_Object.h"

// VoxelBusters.NativePlugins.MediaLibrarySettings
struct  MediaLibrarySettings_t8_253  : public Object_t
{
	// VoxelBusters.NativePlugins.MediaLibrarySettings/AndroidSettings VoxelBusters.NativePlugins.MediaLibrarySettings::m_android
	AndroidSettings_t8_252 * ___m_android_0;
};
