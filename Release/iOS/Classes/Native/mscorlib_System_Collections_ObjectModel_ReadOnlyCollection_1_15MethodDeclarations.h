﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector4>
struct ReadOnlyCollection_1_t1_2280;
// System.Collections.Generic.IList`1<UnityEngine.Vector4>
struct IList_1_t1_2281;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Object
struct Object_t;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t6_278;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector4>
struct IEnumerator_1_t1_2799;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector4.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector4>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C" void ReadOnlyCollection_1__ctor_m1_19039_gshared (ReadOnlyCollection_1_t1_2280 * __this, Object_t* ___list, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m1_19039(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t1_2280 *, Object_t*, const MethodInfo*))ReadOnlyCollection_1__ctor_m1_19039_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector4>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1_19040_gshared (ReadOnlyCollection_1_t1_2280 * __this, Vector4_t6_54  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1_19040(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t1_2280 *, Vector4_t6_54 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1_19040_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector4>::System.Collections.Generic.ICollection<T>.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1_19041_gshared (ReadOnlyCollection_1_t1_2280 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1_19041(__this, method) (( void (*) (ReadOnlyCollection_1_t1_2280 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1_19041_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector4>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1_19042_gshared (ReadOnlyCollection_1_t1_2280 * __this, int32_t ___index, Vector4_t6_54  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1_19042(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t1_2280 *, int32_t, Vector4_t6_54 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1_19042_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector4>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1_19043_gshared (ReadOnlyCollection_1_t1_2280 * __this, Vector4_t6_54  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1_19043(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t1_2280 *, Vector4_t6_54 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1_19043_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector4>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1_19044_gshared (ReadOnlyCollection_1_t1_2280 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1_19044(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t1_2280 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1_19044_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector4>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C" Vector4_t6_54  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1_19045_gshared (ReadOnlyCollection_1_t1_2280 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1_19045(__this, ___index, method) (( Vector4_t6_54  (*) (ReadOnlyCollection_1_t1_2280 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1_19045_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector4>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1_19046_gshared (ReadOnlyCollection_1_t1_2280 * __this, int32_t ___index, Vector4_t6_54  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1_19046(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t1_2280 *, int32_t, Vector4_t6_54 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1_19046_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector4>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_19047_gshared (ReadOnlyCollection_1_t1_2280 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_19047(__this, method) (( bool (*) (ReadOnlyCollection_1_t1_2280 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_19047_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector4>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1_19048_gshared (ReadOnlyCollection_1_t1_2280 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1_19048(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t1_2280 *, Array_t *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1_19048_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector4>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1_19049_gshared (ReadOnlyCollection_1_t1_2280 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1_19049(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t1_2280 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1_19049_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector4>::System.Collections.IList.Add(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m1_19050_gshared (ReadOnlyCollection_1_t1_2280 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m1_19050(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t1_2280 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m1_19050_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector4>::System.Collections.IList.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m1_19051_gshared (ReadOnlyCollection_1_t1_2280 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m1_19051(__this, method) (( void (*) (ReadOnlyCollection_1_t1_2280 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m1_19051_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector4>::System.Collections.IList.Contains(System.Object)
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_Contains_m1_19052_gshared (ReadOnlyCollection_1_t1_2280 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m1_19052(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t1_2280 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m1_19052_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector4>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1_19053_gshared (ReadOnlyCollection_1_t1_2280 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1_19053(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t1_2280 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1_19053_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector4>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m1_19054_gshared (ReadOnlyCollection_1_t1_2280 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m1_19054(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t1_2280 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m1_19054_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector4>::System.Collections.IList.Remove(System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m1_19055_gshared (ReadOnlyCollection_1_t1_2280 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m1_19055(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t1_2280 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m1_19055_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector4>::System.Collections.IList.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1_19056_gshared (ReadOnlyCollection_1_t1_2280 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1_19056(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t1_2280 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1_19056_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector4>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1_19057_gshared (ReadOnlyCollection_1_t1_2280 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1_19057(__this, method) (( bool (*) (ReadOnlyCollection_1_t1_2280 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1_19057_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector4>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1_19058_gshared (ReadOnlyCollection_1_t1_2280 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1_19058(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t1_2280 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1_19058_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector4>::System.Collections.IList.get_IsFixedSize()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1_19059_gshared (ReadOnlyCollection_1_t1_2280 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1_19059(__this, method) (( bool (*) (ReadOnlyCollection_1_t1_2280 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1_19059_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector4>::System.Collections.IList.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1_19060_gshared (ReadOnlyCollection_1_t1_2280 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1_19060(__this, method) (( bool (*) (ReadOnlyCollection_1_t1_2280 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1_19060_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector4>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IList_get_Item_m1_19061_gshared (ReadOnlyCollection_1_t1_2280 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m1_19061(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t1_2280 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m1_19061_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector4>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m1_19062_gshared (ReadOnlyCollection_1_t1_2280 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m1_19062(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t1_2280 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m1_19062_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector4>::Contains(T)
extern "C" bool ReadOnlyCollection_1_Contains_m1_19063_gshared (ReadOnlyCollection_1_t1_2280 * __this, Vector4_t6_54  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m1_19063(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t1_2280 *, Vector4_t6_54 , const MethodInfo*))ReadOnlyCollection_1_Contains_m1_19063_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector4>::CopyTo(T[],System.Int32)
extern "C" void ReadOnlyCollection_1_CopyTo_m1_19064_gshared (ReadOnlyCollection_1_t1_2280 * __this, Vector4U5BU5D_t6_278* ___array, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m1_19064(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t1_2280 *, Vector4U5BU5D_t6_278*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m1_19064_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector4>::GetEnumerator()
extern "C" Object_t* ReadOnlyCollection_1_GetEnumerator_m1_19065_gshared (ReadOnlyCollection_1_t1_2280 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m1_19065(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t1_2280 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m1_19065_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector4>::IndexOf(T)
extern "C" int32_t ReadOnlyCollection_1_IndexOf_m1_19066_gshared (ReadOnlyCollection_1_t1_2280 * __this, Vector4_t6_54  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m1_19066(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t1_2280 *, Vector4_t6_54 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m1_19066_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector4>::get_Count()
extern "C" int32_t ReadOnlyCollection_1_get_Count_m1_19067_gshared (ReadOnlyCollection_1_t1_2280 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m1_19067(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t1_2280 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m1_19067_gshared)(__this, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector4>::get_Items()
extern "C" Object_t* ReadOnlyCollection_1_get_Items_m1_19068_gshared (ReadOnlyCollection_1_t1_2280 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Items_m1_19068(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t1_2280 *, const MethodInfo*))ReadOnlyCollection_1_get_Items_m1_19068_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector4>::get_Item(System.Int32)
extern "C" Vector4_t6_54  ReadOnlyCollection_1_get_Item_m1_19069_gshared (ReadOnlyCollection_1_t1_2280 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m1_19069(__this, ___index, method) (( Vector4_t6_54  (*) (ReadOnlyCollection_1_t1_2280 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m1_19069_gshared)(__this, ___index, method)
