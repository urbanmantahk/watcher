﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExifLibrary.ExifSRational
struct ExifSRational_t8_126;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Int32[]
struct Int32U5BU5D_t1_275;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTag.h"
#include "AssemblyU2DCSharp_ExifLibrary_MathEx_Fraction32.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifInterOperability.h"

// System.Void ExifLibrary.ExifSRational::.ctor(ExifLibrary.ExifTag,System.Int32,System.Int32)
extern "C" void ExifSRational__ctor_m8_608 (ExifSRational_t8_126 * __this, int32_t ___tag, int32_t ___numerator, int32_t ___denominator, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.ExifSRational::.ctor(ExifLibrary.ExifTag,ExifLibrary.MathEx/Fraction32)
extern "C" void ExifSRational__ctor_m8_609 (ExifSRational_t8_126 * __this, int32_t ___tag, Fraction32_t8_127  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ExifLibrary.ExifSRational::get__Value()
extern "C" Object_t * ExifSRational_get__Value_m8_610 (ExifSRational_t8_126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.ExifSRational::set__Value(System.Object)
extern "C" void ExifSRational_set__Value_m8_611 (ExifSRational_t8_126 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/Fraction32 ExifLibrary.ExifSRational::get_Value()
extern "C" Fraction32_t8_127  ExifSRational_get_Value_m8_612 (ExifSRational_t8_126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.ExifSRational::set_Value(ExifLibrary.MathEx/Fraction32)
extern "C" void ExifSRational_set_Value_m8_613 (ExifSRational_t8_126 * __this, Fraction32_t8_127  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExifLibrary.ExifSRational::ToString()
extern "C" String_t* ExifSRational_ToString_m8_614 (ExifSRational_t8_126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ExifLibrary.ExifSRational::ToFloat()
extern "C" float ExifSRational_ToFloat_m8_615 (ExifSRational_t8_126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] ExifLibrary.ExifSRational::ToArray()
extern "C" Int32U5BU5D_t1_275* ExifSRational_ToArray_m8_616 (ExifSRational_t8_126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.ExifInterOperability ExifLibrary.ExifSRational::get_Interoperability()
extern "C" ExifInterOperability_t8_113  ExifSRational_get_Interoperability_m8_617 (ExifSRational_t8_126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ExifLibrary.ExifSRational::op_Explicit(ExifLibrary.ExifSRational)
extern "C" float ExifSRational_op_Explicit_m8_618 (Object_t * __this /* static, unused */, ExifSRational_t8_126 * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
