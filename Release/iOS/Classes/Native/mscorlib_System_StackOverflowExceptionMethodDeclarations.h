﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.StackOverflowException
struct StackOverflowException_t1_1597;
// System.String
struct String_t;
// System.Exception
struct Exception_t1_33;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.StackOverflowException::.ctor()
extern "C" void StackOverflowException__ctor_m1_14580 (StackOverflowException_t1_1597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.StackOverflowException::.ctor(System.String)
extern "C" void StackOverflowException__ctor_m1_14581 (StackOverflowException_t1_1597 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.StackOverflowException::.ctor(System.String,System.Exception)
extern "C" void StackOverflowException__ctor_m1_14582 (StackOverflowException_t1_1597 * __this, String_t* ___message, Exception_t1_33 * ___innerException, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.StackOverflowException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void StackOverflowException__ctor_m1_14583 (StackOverflowException_t1_1597 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
