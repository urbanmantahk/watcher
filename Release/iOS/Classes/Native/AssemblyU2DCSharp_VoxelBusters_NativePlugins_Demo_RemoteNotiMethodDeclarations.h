﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.Demo.RemoteNotificationTest
struct RemoteNotificationTest_t8_184;
// VoxelBusters.NativePlugins.CrossPlatformNotification
struct CrossPlatformNotification_t8_259;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.Demo.RemoteNotificationTest::.ctor()
extern "C" void RemoteNotificationTest__ctor_m8_1084 (RemoteNotificationTest_t8_184 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Demo.RemoteNotificationTest::Start()
extern "C" void RemoteNotificationTest_Start_m8_1085 (RemoteNotificationTest_t8_184 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Demo.RemoteNotificationTest::OnEnable()
extern "C" void RemoteNotificationTest_OnEnable_m8_1086 (RemoteNotificationTest_t8_184 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Demo.RemoteNotificationTest::OnDisable()
extern "C" void RemoteNotificationTest_OnDisable_m8_1087 (RemoteNotificationTest_t8_184 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Demo.RemoteNotificationTest::OnGUI()
extern "C" void RemoteNotificationTest_OnGUI_m8_1088 (RemoteNotificationTest_t8_184 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Demo.RemoteNotificationTest::DidReceiveLocalNotificationEvent(VoxelBusters.NativePlugins.CrossPlatformNotification)
extern "C" void RemoteNotificationTest_DidReceiveLocalNotificationEvent_m8_1089 (RemoteNotificationTest_t8_184 * __this, CrossPlatformNotification_t8_259 * ____notification, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Demo.RemoteNotificationTest::DidReceiveRemoteNotificationEvent(VoxelBusters.NativePlugins.CrossPlatformNotification)
extern "C" void RemoteNotificationTest_DidReceiveRemoteNotificationEvent_m8_1090 (RemoteNotificationTest_t8_184 * __this, CrossPlatformNotification_t8_259 * ____notification, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Demo.RemoteNotificationTest::DidFinishRegisterForRemoteNotificationEvent(System.String,System.String)
extern "C" void RemoteNotificationTest_DidFinishRegisterForRemoteNotificationEvent_m8_1091 (RemoteNotificationTest_t8_184 * __this, String_t* ____deviceToken, String_t* ____error, const MethodInfo* method) IL2CPP_METHOD_ATTR;
