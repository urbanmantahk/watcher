﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.Emit.SignatureHelper
struct SignatureHelper_t1_551;
// System.Reflection.Emit.ModuleBuilder
struct ModuleBuilder_t1_475;
// System.Reflection.Module
struct Module_t1_495;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t1_31;
// System.Type[][]
struct TypeU5BU5DU5BU5D_t1_483;
// System.String
struct String_t;
// System.Exception
struct Exception_t1_33;
// System.Object
struct Object_t;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_Emit_SignatureHelper_SignatureHel.h"
#include "mscorlib_System_Guid.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_Reflection_CallingConventions.h"
#include "mscorlib_System_Runtime_InteropServices_CallingConvention.h"

// System.Void System.Reflection.Emit.SignatureHelper::.ctor(System.Reflection.Emit.ModuleBuilder,System.Reflection.Emit.SignatureHelper/SignatureHelperType)
extern "C" void SignatureHelper__ctor_m1_6356 (SignatureHelper_t1_551 * __this, ModuleBuilder_t1_475 * ___module, int32_t ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.SignatureHelper::System.Runtime.InteropServices._SignatureHelper.GetIDsOfNames(System.Guid&,System.IntPtr,System.UInt32,System.UInt32,System.IntPtr)
extern "C" void SignatureHelper_System_Runtime_InteropServices__SignatureHelper_GetIDsOfNames_m1_6357 (SignatureHelper_t1_551 * __this, Guid_t1_319 * ___riid, IntPtr_t ___rgszNames, uint32_t ___cNames, uint32_t ___lcid, IntPtr_t ___rgDispId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.SignatureHelper::System.Runtime.InteropServices._SignatureHelper.GetTypeInfo(System.UInt32,System.UInt32,System.IntPtr)
extern "C" void SignatureHelper_System_Runtime_InteropServices__SignatureHelper_GetTypeInfo_m1_6358 (SignatureHelper_t1_551 * __this, uint32_t ___iTInfo, uint32_t ___lcid, IntPtr_t ___ppTInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.SignatureHelper::System.Runtime.InteropServices._SignatureHelper.GetTypeInfoCount(System.UInt32&)
extern "C" void SignatureHelper_System_Runtime_InteropServices__SignatureHelper_GetTypeInfoCount_m1_6359 (SignatureHelper_t1_551 * __this, uint32_t* ___pcTInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.SignatureHelper::System.Runtime.InteropServices._SignatureHelper.Invoke(System.UInt32,System.Guid&,System.UInt32,System.Int16,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr)
extern "C" void SignatureHelper_System_Runtime_InteropServices__SignatureHelper_Invoke_m1_6360 (SignatureHelper_t1_551 * __this, uint32_t ___dispIdMember, Guid_t1_319 * ___riid, uint32_t ___lcid, int16_t ___wFlags, IntPtr_t ___pDispParams, IntPtr_t ___pVarResult, IntPtr_t ___pExcepInfo, IntPtr_t ___puArgErr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.SignatureHelper System.Reflection.Emit.SignatureHelper::GetFieldSigHelper(System.Reflection.Module)
extern "C" SignatureHelper_t1_551 * SignatureHelper_GetFieldSigHelper_m1_6361 (Object_t * __this /* static, unused */, Module_t1_495 * ___mod, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.SignatureHelper System.Reflection.Emit.SignatureHelper::GetLocalVarSigHelper(System.Reflection.Module)
extern "C" SignatureHelper_t1_551 * SignatureHelper_GetLocalVarSigHelper_m1_6362 (Object_t * __this /* static, unused */, Module_t1_495 * ___mod, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.SignatureHelper System.Reflection.Emit.SignatureHelper::GetLocalVarSigHelper()
extern "C" SignatureHelper_t1_551 * SignatureHelper_GetLocalVarSigHelper_m1_6363 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.SignatureHelper System.Reflection.Emit.SignatureHelper::GetMethodSigHelper(System.Reflection.CallingConventions,System.Type)
extern "C" SignatureHelper_t1_551 * SignatureHelper_GetMethodSigHelper_m1_6364 (Object_t * __this /* static, unused */, int32_t ___callingConvention, Type_t * ___returnType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.SignatureHelper System.Reflection.Emit.SignatureHelper::GetMethodSigHelper(System.Runtime.InteropServices.CallingConvention,System.Type)
extern "C" SignatureHelper_t1_551 * SignatureHelper_GetMethodSigHelper_m1_6365 (Object_t * __this /* static, unused */, int32_t ___unmanagedCallingConvention, Type_t * ___returnType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.SignatureHelper System.Reflection.Emit.SignatureHelper::GetMethodSigHelper(System.Reflection.Module,System.Reflection.CallingConventions,System.Type)
extern "C" SignatureHelper_t1_551 * SignatureHelper_GetMethodSigHelper_m1_6366 (Object_t * __this /* static, unused */, Module_t1_495 * ___mod, int32_t ___callingConvention, Type_t * ___returnType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.SignatureHelper System.Reflection.Emit.SignatureHelper::GetMethodSigHelper(System.Reflection.Module,System.Runtime.InteropServices.CallingConvention,System.Type)
extern "C" SignatureHelper_t1_551 * SignatureHelper_GetMethodSigHelper_m1_6367 (Object_t * __this /* static, unused */, Module_t1_495 * ___mod, int32_t ___unmanagedCallConv, Type_t * ___returnType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.SignatureHelper System.Reflection.Emit.SignatureHelper::GetMethodSigHelper(System.Reflection.Module,System.Type,System.Type[])
extern "C" SignatureHelper_t1_551 * SignatureHelper_GetMethodSigHelper_m1_6368 (Object_t * __this /* static, unused */, Module_t1_495 * ___mod, Type_t * ___returnType, TypeU5BU5D_t1_31* ___parameterTypes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.SignatureHelper System.Reflection.Emit.SignatureHelper::GetPropertySigHelper(System.Reflection.Module,System.Type,System.Type[])
extern "C" SignatureHelper_t1_551 * SignatureHelper_GetPropertySigHelper_m1_6369 (Object_t * __this /* static, unused */, Module_t1_495 * ___mod, Type_t * ___returnType, TypeU5BU5D_t1_31* ___parameterTypes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.SignatureHelper::AppendArray(System.Type[]&,System.Type)
extern "C" int32_t SignatureHelper_AppendArray_m1_6370 (Object_t * __this /* static, unused */, TypeU5BU5D_t1_31** ___array, Type_t * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.SignatureHelper::AppendArrayAt(System.Type[][]&,System.Type[],System.Int32)
extern "C" void SignatureHelper_AppendArrayAt_m1_6371 (Object_t * __this /* static, unused */, TypeU5BU5DU5BU5D_t1_483** ___array, TypeU5BU5D_t1_31* ___t, int32_t ___pos, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.SignatureHelper::ValidateParameterModifiers(System.String,System.Type[])
extern "C" void SignatureHelper_ValidateParameterModifiers_m1_6372 (Object_t * __this /* static, unused */, String_t* ___name, TypeU5BU5D_t1_31* ___parameter_modifiers, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.SignatureHelper::ValidateCustomModifier(System.Int32,System.Type[][],System.String)
extern "C" void SignatureHelper_ValidateCustomModifier_m1_6373 (Object_t * __this /* static, unused */, int32_t ___n, TypeU5BU5DU5BU5D_t1_483* ___custom_modifiers, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception System.Reflection.Emit.SignatureHelper::MissingFeature()
extern "C" Exception_t1_33 * SignatureHelper_MissingFeature_m1_6374 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.SignatureHelper::AddArguments(System.Type[],System.Type[][],System.Type[][])
extern "C" void SignatureHelper_AddArguments_m1_6375 (SignatureHelper_t1_551 * __this, TypeU5BU5D_t1_31* ___arguments, TypeU5BU5DU5BU5D_t1_483* ___requiredCustomModifiers, TypeU5BU5DU5BU5D_t1_483* ___optionalCustomModifiers, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.SignatureHelper::AddArgument(System.Type,System.Boolean)
extern "C" void SignatureHelper_AddArgument_m1_6376 (SignatureHelper_t1_551 * __this, Type_t * ___argument, bool ___pinned, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.SignatureHelper::AddArgument(System.Type,System.Type[],System.Type[])
extern "C" void SignatureHelper_AddArgument_m1_6377 (SignatureHelper_t1_551 * __this, Type_t * ___argument, TypeU5BU5D_t1_31* ___requiredCustomModifiers, TypeU5BU5D_t1_31* ___optionalCustomModifiers, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.SignatureHelper System.Reflection.Emit.SignatureHelper::GetPropertySigHelper(System.Reflection.Module,System.Type,System.Type[],System.Type[],System.Type[],System.Type[][],System.Type[][])
extern "C" SignatureHelper_t1_551 * SignatureHelper_GetPropertySigHelper_m1_6378 (Object_t * __this /* static, unused */, Module_t1_495 * ___mod, Type_t * ___returnType, TypeU5BU5D_t1_31* ___requiredReturnTypeCustomModifiers, TypeU5BU5D_t1_31* ___optionalReturnTypeCustomModifiers, TypeU5BU5D_t1_31* ___parameterTypes, TypeU5BU5DU5BU5D_t1_483* ___requiredParameterTypeCustomModifiers, TypeU5BU5DU5BU5D_t1_483* ___optionalParameterTypeCustomModifiers, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.SignatureHelper::AddArgument(System.Type)
extern "C" void SignatureHelper_AddArgument_m1_6379 (SignatureHelper_t1_551 * __this, Type_t * ___clsArgument, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.SignatureHelper::AddSentinel()
extern "C" void SignatureHelper_AddSentinel_m1_6380 (SignatureHelper_t1_551 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.SignatureHelper::CompareOK(System.Type[][],System.Type[][])
extern "C" bool SignatureHelper_CompareOK_m1_6381 (Object_t * __this /* static, unused */, TypeU5BU5DU5BU5D_t1_483* ___one, TypeU5BU5DU5BU5D_t1_483* ___two, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.SignatureHelper::Equals(System.Object)
extern "C" bool SignatureHelper_Equals_m1_6382 (SignatureHelper_t1_551 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.SignatureHelper::GetHashCode()
extern "C" int32_t SignatureHelper_GetHashCode_m1_6383 (SignatureHelper_t1_551 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Reflection.Emit.SignatureHelper::get_signature_local()
extern "C" ByteU5BU5D_t1_109* SignatureHelper_get_signature_local_m1_6384 (SignatureHelper_t1_551 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Reflection.Emit.SignatureHelper::get_signature_field()
extern "C" ByteU5BU5D_t1_109* SignatureHelper_get_signature_field_m1_6385 (SignatureHelper_t1_551 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Reflection.Emit.SignatureHelper::GetSignature()
extern "C" ByteU5BU5D_t1_109* SignatureHelper_GetSignature_m1_6386 (SignatureHelper_t1_551 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Emit.SignatureHelper::ToString()
extern "C" String_t* SignatureHelper_ToString_m1_6387 (SignatureHelper_t1_551 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.SignatureHelper System.Reflection.Emit.SignatureHelper::GetMethodSigHelper(System.Reflection.Module,System.Reflection.CallingConventions,System.Runtime.InteropServices.CallingConvention,System.Type,System.Type[])
extern "C" SignatureHelper_t1_551 * SignatureHelper_GetMethodSigHelper_m1_6388 (Object_t * __this /* static, unused */, Module_t1_495 * ___mod, int32_t ___callingConvention, int32_t ___unmanagedCallingConvention, Type_t * ___returnType, TypeU5BU5D_t1_31* ___parameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
