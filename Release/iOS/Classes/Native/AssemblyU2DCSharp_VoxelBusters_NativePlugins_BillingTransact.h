﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object.h"
#include "mscorlib_System_DateTime.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eBillingTransac.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eBillingTransac_0.h"

// VoxelBusters.NativePlugins.BillingTransaction
struct  BillingTransaction_t8_211  : public Object_t
{
	// System.String VoxelBusters.NativePlugins.BillingTransaction::<ProductIdentifier>k__BackingField
	String_t* ___U3CProductIdentifierU3Ek__BackingField_0;
	// System.DateTime VoxelBusters.NativePlugins.BillingTransaction::<TransactionDateUTC>k__BackingField
	DateTime_t1_150  ___U3CTransactionDateUTCU3Ek__BackingField_1;
	// System.DateTime VoxelBusters.NativePlugins.BillingTransaction::<TransactionDateLocal>k__BackingField
	DateTime_t1_150  ___U3CTransactionDateLocalU3Ek__BackingField_2;
	// System.String VoxelBusters.NativePlugins.BillingTransaction::<TransactionIdentifier>k__BackingField
	String_t* ___U3CTransactionIdentifierU3Ek__BackingField_3;
	// System.String VoxelBusters.NativePlugins.BillingTransaction::<TransactionReceipt>k__BackingField
	String_t* ___U3CTransactionReceiptU3Ek__BackingField_4;
	// VoxelBusters.NativePlugins.eBillingTransactionState VoxelBusters.NativePlugins.BillingTransaction::<TransactionState>k__BackingField
	int32_t ___U3CTransactionStateU3Ek__BackingField_5;
	// VoxelBusters.NativePlugins.eBillingTransactionVerificationState VoxelBusters.NativePlugins.BillingTransaction::<VerificationState>k__BackingField
	int32_t ___U3CVerificationStateU3Ek__BackingField_6;
	// System.String VoxelBusters.NativePlugins.BillingTransaction::<Error>k__BackingField
	String_t* ___U3CErrorU3Ek__BackingField_7;
	// System.String VoxelBusters.NativePlugins.BillingTransaction::<RawPurchaseData>k__BackingField
	String_t* ___U3CRawPurchaseDataU3Ek__BackingField_8;
};
