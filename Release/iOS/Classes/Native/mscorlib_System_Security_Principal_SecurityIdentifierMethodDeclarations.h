﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Principal.SecurityIdentifier
struct SecurityIdentifier_t1_1132;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Object
struct Object_t;
// System.Type
struct Type_t;
// System.Security.Principal.IdentityReference
struct IdentityReference_t1_1120;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_Security_Principal_WellKnownSidType.h"

// System.Void System.Security.Principal.SecurityIdentifier::.ctor(System.String)
extern "C" void SecurityIdentifier__ctor_m1_11825 (SecurityIdentifier_t1_1132 * __this, String_t* ___sddlForm, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Principal.SecurityIdentifier::.ctor(System.Byte[],System.Int32)
extern "C" void SecurityIdentifier__ctor_m1_11826 (SecurityIdentifier_t1_1132 * __this, ByteU5BU5D_t1_109* ___binaryForm, int32_t ___offset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Principal.SecurityIdentifier::.ctor(System.IntPtr)
extern "C" void SecurityIdentifier__ctor_m1_11827 (SecurityIdentifier_t1_1132 * __this, IntPtr_t ___binaryForm, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Principal.SecurityIdentifier::.ctor(System.Security.Principal.WellKnownSidType,System.Security.Principal.SecurityIdentifier)
extern "C" void SecurityIdentifier__ctor_m1_11828 (SecurityIdentifier_t1_1132 * __this, int32_t ___sidType, SecurityIdentifier_t1_1132 * ___domainSid, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Principal.SecurityIdentifier::.cctor()
extern "C" void SecurityIdentifier__cctor_m1_11829 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Principal.SecurityIdentifier System.Security.Principal.SecurityIdentifier::get_AccountDomainSid()
extern "C" SecurityIdentifier_t1_1132 * SecurityIdentifier_get_AccountDomainSid_m1_11830 (SecurityIdentifier_t1_1132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Principal.SecurityIdentifier::get_BinaryLength()
extern "C" int32_t SecurityIdentifier_get_BinaryLength_m1_11831 (SecurityIdentifier_t1_1132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Principal.SecurityIdentifier::get_Value()
extern "C" String_t* SecurityIdentifier_get_Value_m1_11832 (SecurityIdentifier_t1_1132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Principal.SecurityIdentifier::CompareTo(System.Security.Principal.SecurityIdentifier)
extern "C" int32_t SecurityIdentifier_CompareTo_m1_11833 (SecurityIdentifier_t1_1132 * __this, SecurityIdentifier_t1_1132 * ___sid, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Principal.SecurityIdentifier::Equals(System.Object)
extern "C" bool SecurityIdentifier_Equals_m1_11834 (SecurityIdentifier_t1_1132 * __this, Object_t * ___o, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Principal.SecurityIdentifier::Equals(System.Security.Principal.SecurityIdentifier)
extern "C" bool SecurityIdentifier_Equals_m1_11835 (SecurityIdentifier_t1_1132 * __this, SecurityIdentifier_t1_1132 * ___sid, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Principal.SecurityIdentifier::GetBinaryForm(System.Byte[],System.Int32)
extern "C" void SecurityIdentifier_GetBinaryForm_m1_11836 (SecurityIdentifier_t1_1132 * __this, ByteU5BU5D_t1_109* ___binaryForm, int32_t ___offset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Principal.SecurityIdentifier::GetHashCode()
extern "C" int32_t SecurityIdentifier_GetHashCode_m1_11837 (SecurityIdentifier_t1_1132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Principal.SecurityIdentifier::IsAccountSid()
extern "C" bool SecurityIdentifier_IsAccountSid_m1_11838 (SecurityIdentifier_t1_1132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Principal.SecurityIdentifier::IsEqualDomainSid(System.Security.Principal.SecurityIdentifier)
extern "C" bool SecurityIdentifier_IsEqualDomainSid_m1_11839 (SecurityIdentifier_t1_1132 * __this, SecurityIdentifier_t1_1132 * ___sid, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Principal.SecurityIdentifier::IsValidTargetType(System.Type)
extern "C" bool SecurityIdentifier_IsValidTargetType_m1_11840 (SecurityIdentifier_t1_1132 * __this, Type_t * ___targetType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Principal.SecurityIdentifier::IsWellKnown(System.Security.Principal.WellKnownSidType)
extern "C" bool SecurityIdentifier_IsWellKnown_m1_11841 (SecurityIdentifier_t1_1132 * __this, int32_t ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Principal.SecurityIdentifier::ToString()
extern "C" String_t* SecurityIdentifier_ToString_m1_11842 (SecurityIdentifier_t1_1132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Principal.IdentityReference System.Security.Principal.SecurityIdentifier::Translate(System.Type)
extern "C" IdentityReference_t1_1120 * SecurityIdentifier_Translate_m1_11843 (SecurityIdentifier_t1_1132 * __this, Type_t * ___targetType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Principal.SecurityIdentifier::op_Equality(System.Security.Principal.SecurityIdentifier,System.Security.Principal.SecurityIdentifier)
extern "C" bool SecurityIdentifier_op_Equality_m1_11844 (Object_t * __this /* static, unused */, SecurityIdentifier_t1_1132 * ___left, SecurityIdentifier_t1_1132 * ___right, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Principal.SecurityIdentifier::op_Inequality(System.Security.Principal.SecurityIdentifier,System.Security.Principal.SecurityIdentifier)
extern "C" bool SecurityIdentifier_op_Inequality_m1_11845 (Object_t * __this /* static, unused */, SecurityIdentifier_t1_1132 * ___left, SecurityIdentifier_t1_1132 * ___right, const MethodInfo* method) IL2CPP_METHOD_ATTR;
