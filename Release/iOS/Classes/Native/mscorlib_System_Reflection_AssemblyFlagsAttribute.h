﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Attribute.h"

// System.Reflection.AssemblyFlagsAttribute
struct  AssemblyFlagsAttribute_t1_572  : public Attribute_t1_2
{
	// System.UInt32 System.Reflection.AssemblyFlagsAttribute::flags
	uint32_t ___flags_0;
};
