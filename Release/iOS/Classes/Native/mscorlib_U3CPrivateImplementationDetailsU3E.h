﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU245.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU244.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU243.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_0.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU243_0.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242_0.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU243_1.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU244_0.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU246.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_1.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_2.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU248.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU248_0.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU247.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_3.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU249.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242_1.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242_2.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_4.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU246_0.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_5.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU243_2.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_6.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU245_0.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_7.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242_3.h"
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_8.h"

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t1_1661  : public Object_t
{
};
struct U3CPrivateImplementationDetailsU3E_t1_1661_StaticFields{
	// <PrivateImplementationDetails>/$ArrayType$56 <PrivateImplementationDetails>::$$field-0
	U24ArrayTypeU2456_t1_1632  ___U24U24fieldU2D0_0;
	// <PrivateImplementationDetails>/$ArrayType$24 <PrivateImplementationDetails>::$$field-1
	U24ArrayTypeU2424_t1_1633  ___U24U24fieldU2D1_1;
	// <PrivateImplementationDetails>/$ArrayType$24 <PrivateImplementationDetails>::$$field-2
	U24ArrayTypeU2424_t1_1633  ___U24U24fieldU2D2_2;
	// <PrivateImplementationDetails>/$ArrayType$24 <PrivateImplementationDetails>::$$field-3
	U24ArrayTypeU2424_t1_1633  ___U24U24fieldU2D3_3;
	// <PrivateImplementationDetails>/$ArrayType$24 <PrivateImplementationDetails>::$$field-4
	U24ArrayTypeU2424_t1_1633  ___U24U24fieldU2D4_4;
	// <PrivateImplementationDetails>/$ArrayType$16 <PrivateImplementationDetails>::$$field-5
	U24ArrayTypeU2416_t1_1634  ___U24U24fieldU2D5_5;
	// <PrivateImplementationDetails>/$ArrayType$16 <PrivateImplementationDetails>::$$field-6
	U24ArrayTypeU2416_t1_1634  ___U24U24fieldU2D6_6;
	// <PrivateImplementationDetails>/$ArrayType$40 <PrivateImplementationDetails>::$$field-7
	U24ArrayTypeU2440_t1_1635  ___U24U24fieldU2D7_7;
	// <PrivateImplementationDetails>/$ArrayType$40 <PrivateImplementationDetails>::$$field-8
	U24ArrayTypeU2440_t1_1635  ___U24U24fieldU2D8_8;
	// <PrivateImplementationDetails>/$ArrayType$36 <PrivateImplementationDetails>::$$field-9
	U24ArrayTypeU2436_t1_1636  ___U24U24fieldU2D9_9;
	// <PrivateImplementationDetails>/$ArrayType$36 <PrivateImplementationDetails>::$$field-10
	U24ArrayTypeU2436_t1_1636  ___U24U24fieldU2D10_10;
	// <PrivateImplementationDetails>/$ArrayType$120 <PrivateImplementationDetails>::$$field-11
	U24ArrayTypeU24120_t1_1637  ___U24U24fieldU2D11_11;
	// <PrivateImplementationDetails>/$ArrayType$120 <PrivateImplementationDetails>::$$field-12
	U24ArrayTypeU24120_t1_1637  ___U24U24fieldU2D12_12;
	// <PrivateImplementationDetails>/$ArrayType$36 <PrivateImplementationDetails>::$$field-13
	U24ArrayTypeU2436_t1_1636  ___U24U24fieldU2D13_13;
	// <PrivateImplementationDetails>/$ArrayType$36 <PrivateImplementationDetails>::$$field-14
	U24ArrayTypeU2436_t1_1636  ___U24U24fieldU2D14_14;
	// <PrivateImplementationDetails>/$ArrayType$3132 <PrivateImplementationDetails>::$$field-15
	U24ArrayTypeU243132_t1_1638  ___U24U24fieldU2D15_15;
	// <PrivateImplementationDetails>/$ArrayType$20 <PrivateImplementationDetails>::$$field-16
	U24ArrayTypeU2420_t1_1639  ___U24U24fieldU2D16_16;
	// <PrivateImplementationDetails>/$ArrayType$32 <PrivateImplementationDetails>::$$field-17
	U24ArrayTypeU2432_t1_1640  ___U24U24fieldU2D17_17;
	// <PrivateImplementationDetails>/$ArrayType$48 <PrivateImplementationDetails>::$$field-18
	U24ArrayTypeU2448_t1_1641  ___U24U24fieldU2D18_18;
	// <PrivateImplementationDetails>/$ArrayType$64 <PrivateImplementationDetails>::$$field-19
	U24ArrayTypeU2464_t1_1642  ___U24U24fieldU2D19_19;
	// <PrivateImplementationDetails>/$ArrayType$64 <PrivateImplementationDetails>::$$field-20
	U24ArrayTypeU2464_t1_1642  ___U24U24fieldU2D20_20;
	// <PrivateImplementationDetails>/$ArrayType$64 <PrivateImplementationDetails>::$$field-21
	U24ArrayTypeU2464_t1_1642  ___U24U24fieldU2D21_21;
	// <PrivateImplementationDetails>/$ArrayType$64 <PrivateImplementationDetails>::$$field-22
	U24ArrayTypeU2464_t1_1642  ___U24U24fieldU2D22_22;
	// <PrivateImplementationDetails>/$ArrayType$12 <PrivateImplementationDetails>::$$field-23
	U24ArrayTypeU2412_t1_1643  ___U24U24fieldU2D23_23;
	// <PrivateImplementationDetails>/$ArrayType$12 <PrivateImplementationDetails>::$$field-24
	U24ArrayTypeU2412_t1_1643  ___U24U24fieldU2D24_24;
	// <PrivateImplementationDetails>/$ArrayType$12 <PrivateImplementationDetails>::$$field-25
	U24ArrayTypeU2412_t1_1643  ___U24U24fieldU2D25_25;
	// <PrivateImplementationDetails>/$ArrayType$16 <PrivateImplementationDetails>::$$field-26
	U24ArrayTypeU2416_t1_1634  ___U24U24fieldU2D26_26;
	// <PrivateImplementationDetails>/$ArrayType$136 <PrivateImplementationDetails>::$$field-27
	U24ArrayTypeU24136_t1_1644  ___U24U24fieldU2D27_27;
	// <PrivateImplementationDetails>/$ArrayType$8 <PrivateImplementationDetails>::$$field-28
	U24ArrayTypeU248_t1_1645  ___U24U24fieldU2D28_28;
	// <PrivateImplementationDetails>/$ArrayType$84 <PrivateImplementationDetails>::$$field-29
	U24ArrayTypeU2484_t1_1646  ___U24U24fieldU2D29_29;
	// <PrivateImplementationDetails>/$ArrayType$72 <PrivateImplementationDetails>::$$field-30
	U24ArrayTypeU2472_t1_1647  ___U24U24fieldU2D30_30;
	// <PrivateImplementationDetails>/$ArrayType$8 <PrivateImplementationDetails>::$$field-31
	U24ArrayTypeU248_t1_1645  ___U24U24fieldU2D31_31;
	// <PrivateImplementationDetails>/$ArrayType$20 <PrivateImplementationDetails>::$$field-32
	U24ArrayTypeU2420_t1_1639  ___U24U24fieldU2D32_32;
	// <PrivateImplementationDetails>/$ArrayType$64 <PrivateImplementationDetails>::$$field-33
	U24ArrayTypeU2464_t1_1642  ___U24U24fieldU2D33_33;
	// <PrivateImplementationDetails>/$ArrayType$124 <PrivateImplementationDetails>::$$field-34
	U24ArrayTypeU24124_t1_1648  ___U24U24fieldU2D34_34;
	// <PrivateImplementationDetails>/$ArrayType$32 <PrivateImplementationDetails>::$$field-35
	U24ArrayTypeU2432_t1_1640  ___U24U24fieldU2D35_35;
	// <PrivateImplementationDetails>/$ArrayType$96 <PrivateImplementationDetails>::$$field-36
	U24ArrayTypeU2496_t1_1649  ___U24U24fieldU2D36_36;
	// <PrivateImplementationDetails>/$ArrayType$2048 <PrivateImplementationDetails>::$$field-37
	U24ArrayTypeU242048_t1_1650  ___U24U24fieldU2D37_37;
	// <PrivateImplementationDetails>/$ArrayType$56 <PrivateImplementationDetails>::$$field-38
	U24ArrayTypeU2456_t1_1632  ___U24U24fieldU2D38_38;
	// <PrivateImplementationDetails>/$ArrayType$16 <PrivateImplementationDetails>::$$field-39
	U24ArrayTypeU2416_t1_1634  ___U24U24fieldU2D39_39;
	// <PrivateImplementationDetails>/$ArrayType$48 <PrivateImplementationDetails>::$$field-40
	U24ArrayTypeU2448_t1_1641  ___U24U24fieldU2D40_40;
	// <PrivateImplementationDetails>/$ArrayType$2048 <PrivateImplementationDetails>::$$field-41
	U24ArrayTypeU242048_t1_1650  ___U24U24fieldU2D41_41;
	// <PrivateImplementationDetails>/$ArrayType$2048 <PrivateImplementationDetails>::$$field-42
	U24ArrayTypeU242048_t1_1650  ___U24U24fieldU2D42_42;
	// <PrivateImplementationDetails>/$ArrayType$256 <PrivateImplementationDetails>::$$field-43
	U24ArrayTypeU24256_t1_1651  ___U24U24fieldU2D43_43;
	// <PrivateImplementationDetails>/$ArrayType$256 <PrivateImplementationDetails>::$$field-44
	U24ArrayTypeU24256_t1_1651  ___U24U24fieldU2D44_44;
	// <PrivateImplementationDetails>/$ArrayType$120 <PrivateImplementationDetails>::$$field-45
	U24ArrayTypeU24120_t1_1637  ___U24U24fieldU2D45_45;
	// <PrivateImplementationDetails>/$ArrayType$256 <PrivateImplementationDetails>::$$field-46
	U24ArrayTypeU24256_t1_1651  ___U24U24fieldU2D46_46;
	// <PrivateImplementationDetails>/$ArrayType$256 <PrivateImplementationDetails>::$$field-47
	U24ArrayTypeU24256_t1_1651  ___U24U24fieldU2D47_47;
	// <PrivateImplementationDetails>/$ArrayType$1024 <PrivateImplementationDetails>::$$field-48
	U24ArrayTypeU241024_t1_1652  ___U24U24fieldU2D48_48;
	// <PrivateImplementationDetails>/$ArrayType$1024 <PrivateImplementationDetails>::$$field-49
	U24ArrayTypeU241024_t1_1652  ___U24U24fieldU2D49_49;
	// <PrivateImplementationDetails>/$ArrayType$1024 <PrivateImplementationDetails>::$$field-50
	U24ArrayTypeU241024_t1_1652  ___U24U24fieldU2D50_50;
	// <PrivateImplementationDetails>/$ArrayType$1024 <PrivateImplementationDetails>::$$field-51
	U24ArrayTypeU241024_t1_1652  ___U24U24fieldU2D51_51;
	// <PrivateImplementationDetails>/$ArrayType$1024 <PrivateImplementationDetails>::$$field-52
	U24ArrayTypeU241024_t1_1652  ___U24U24fieldU2D52_52;
	// <PrivateImplementationDetails>/$ArrayType$1024 <PrivateImplementationDetails>::$$field-53
	U24ArrayTypeU241024_t1_1652  ___U24U24fieldU2D53_53;
	// <PrivateImplementationDetails>/$ArrayType$1024 <PrivateImplementationDetails>::$$field-54
	U24ArrayTypeU241024_t1_1652  ___U24U24fieldU2D54_54;
	// <PrivateImplementationDetails>/$ArrayType$1024 <PrivateImplementationDetails>::$$field-55
	U24ArrayTypeU241024_t1_1652  ___U24U24fieldU2D55_55;
	// <PrivateImplementationDetails>/$ArrayType$256 <PrivateImplementationDetails>::$$field-56
	U24ArrayTypeU24256_t1_1651  ___U24U24fieldU2D56_56;
	// <PrivateImplementationDetails>/$ArrayType$640 <PrivateImplementationDetails>::$$field-57
	U24ArrayTypeU24640_t1_1653  ___U24U24fieldU2D57_57;
	// <PrivateImplementationDetails>/$ArrayType$96 <PrivateImplementationDetails>::$$field-58
	U24ArrayTypeU2496_t1_1649  ___U24U24fieldU2D58_58;
	// <PrivateImplementationDetails>/$ArrayType$160 <PrivateImplementationDetails>::$$field-59
	U24ArrayTypeU24160_t1_1654  ___U24U24fieldU2D59_59;
	// <PrivateImplementationDetails>/$ArrayType$12 <PrivateImplementationDetails>::$$field-60
	U24ArrayTypeU2412_t1_1643  ___U24U24fieldU2D60_60;
	// <PrivateImplementationDetails>/$ArrayType$380 <PrivateImplementationDetails>::$$field-61
	U24ArrayTypeU24380_t1_1655  ___U24U24fieldU2D61_61;
	// <PrivateImplementationDetails>/$ArrayType$128 <PrivateImplementationDetails>::$$field-62
	U24ArrayTypeU24128_t1_1656  ___U24U24fieldU2D62_62;
	// <PrivateImplementationDetails>/$ArrayType$256 <PrivateImplementationDetails>::$$field-63
	U24ArrayTypeU24256_t1_1651  ___U24U24fieldU2D63_63;
	// <PrivateImplementationDetails>/$ArrayType$52 <PrivateImplementationDetails>::$$field-64
	U24ArrayTypeU2452_t1_1657  ___U24U24fieldU2D64_64;
	// <PrivateImplementationDetails>/$ArrayType$52 <PrivateImplementationDetails>::$$field-65
	U24ArrayTypeU2452_t1_1657  ___U24U24fieldU2D65_65;
	// <PrivateImplementationDetails>/$ArrayType$1668 <PrivateImplementationDetails>::$$field-66
	U24ArrayTypeU241668_t1_1658  ___U24U24fieldU2D66_66;
	// <PrivateImplementationDetails>/$ArrayType$2100 <PrivateImplementationDetails>::$$field-67
	U24ArrayTypeU242100_t1_1659  ___U24U24fieldU2D67_67;
	// <PrivateImplementationDetails>/$ArrayType$1452 <PrivateImplementationDetails>::$$field-68
	U24ArrayTypeU241452_t1_1660  ___U24U24fieldU2D68_68;
};
