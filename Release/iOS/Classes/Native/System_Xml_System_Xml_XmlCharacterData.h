﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "System_Xml_System_Xml_XmlLinkedNode.h"

// System.Xml.XmlCharacterData
struct  XmlCharacterData_t4_125  : public XmlLinkedNode_t4_118
{
	// System.String System.Xml.XmlCharacterData::data
	String_t* ___data_6;
};
