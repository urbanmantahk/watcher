﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Security_Permissions_CodeAccessSecurityAttri.h"

// System.Security.Permissions.FileDialogPermissionAttribute
struct  FileDialogPermissionAttribute_t1_1273  : public CodeAccessSecurityAttribute_t1_1266
{
	// System.Boolean System.Security.Permissions.FileDialogPermissionAttribute::canOpen
	bool ___canOpen_2;
	// System.Boolean System.Security.Permissions.FileDialogPermissionAttribute::canSave
	bool ___canSave_3;
};
