﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Diagnostics.CodeAnalysis.SuppressMessageAttribute
struct SuppressMessageAttribute_t1_316;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Diagnostics.CodeAnalysis.SuppressMessageAttribute::.ctor(System.String,System.String)
extern "C" void SuppressMessageAttribute__ctor_m1_3552 (SuppressMessageAttribute_t1_316 * __this, String_t* ___category, String_t* ___checkId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Diagnostics.CodeAnalysis.SuppressMessageAttribute::get_Category()
extern "C" String_t* SuppressMessageAttribute_get_Category_m1_3553 (SuppressMessageAttribute_t1_316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Diagnostics.CodeAnalysis.SuppressMessageAttribute::get_CheckId()
extern "C" String_t* SuppressMessageAttribute_get_CheckId_m1_3554 (SuppressMessageAttribute_t1_316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Diagnostics.CodeAnalysis.SuppressMessageAttribute::get_Justification()
extern "C" String_t* SuppressMessageAttribute_get_Justification_m1_3555 (SuppressMessageAttribute_t1_316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.CodeAnalysis.SuppressMessageAttribute::set_Justification(System.String)
extern "C" void SuppressMessageAttribute_set_Justification_m1_3556 (SuppressMessageAttribute_t1_316 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Diagnostics.CodeAnalysis.SuppressMessageAttribute::get_MessageId()
extern "C" String_t* SuppressMessageAttribute_get_MessageId_m1_3557 (SuppressMessageAttribute_t1_316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.CodeAnalysis.SuppressMessageAttribute::set_MessageId(System.String)
extern "C" void SuppressMessageAttribute_set_MessageId_m1_3558 (SuppressMessageAttribute_t1_316 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Diagnostics.CodeAnalysis.SuppressMessageAttribute::get_Scope()
extern "C" String_t* SuppressMessageAttribute_get_Scope_m1_3559 (SuppressMessageAttribute_t1_316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.CodeAnalysis.SuppressMessageAttribute::set_Scope(System.String)
extern "C" void SuppressMessageAttribute_set_Scope_m1_3560 (SuppressMessageAttribute_t1_316 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Diagnostics.CodeAnalysis.SuppressMessageAttribute::get_Target()
extern "C" String_t* SuppressMessageAttribute_get_Target_m1_3561 (SuppressMessageAttribute_t1_316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.CodeAnalysis.SuppressMessageAttribute::set_Target(System.String)
extern "C" void SuppressMessageAttribute_set_Target_m1_3562 (SuppressMessageAttribute_t1_316 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
