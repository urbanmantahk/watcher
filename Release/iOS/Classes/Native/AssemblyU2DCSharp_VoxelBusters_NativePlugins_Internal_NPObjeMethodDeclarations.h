﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.Internal.NPObject
struct NPObject_t8_222;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_NPObje_0.h"

// System.Void VoxelBusters.NativePlugins.Internal.NPObject::.ctor()
extern "C" void NPObject__ctor_m8_1902 (NPObject_t8_222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Internal.NPObject::.ctor(VoxelBusters.NativePlugins.Internal.NPObjectManager/eCollectionType)
extern "C" void NPObject__ctor_m8_1903 (NPObject_t8_222 * __this, int32_t ____collectionType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.Internal.NPObject::GetInstanceID()
extern "C" String_t* NPObject_GetInstanceID_m8_1904 (NPObject_t8_222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
