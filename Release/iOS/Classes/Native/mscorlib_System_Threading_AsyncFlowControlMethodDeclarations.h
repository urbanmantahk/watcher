﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Threading.Thread
struct Thread_t1_901;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Threading_AsyncFlowControl.h"
#include "mscorlib_System_Threading_AsyncFlowControlType.h"

// System.Void System.Threading.AsyncFlowControl::.ctor(System.Threading.Thread,System.Threading.AsyncFlowControlType)
extern "C" void AsyncFlowControl__ctor_m1_12615 (AsyncFlowControl_t1_1459 * __this, Thread_t1_901 * ___t, int32_t ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.AsyncFlowControl::System.IDisposable.Dispose()
extern "C" void AsyncFlowControl_System_IDisposable_Dispose_m1_12616 (AsyncFlowControl_t1_1459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.AsyncFlowControl::Undo()
extern "C" void AsyncFlowControl_Undo_m1_12617 (AsyncFlowControl_t1_1459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Threading.AsyncFlowControl::GetHashCode()
extern "C" int32_t AsyncFlowControl_GetHashCode_m1_12618 (AsyncFlowControl_t1_1459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.AsyncFlowControl::Equals(System.Object)
extern "C" bool AsyncFlowControl_Equals_m1_12619 (AsyncFlowControl_t1_1459 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.AsyncFlowControl::Equals(System.Threading.AsyncFlowControl)
extern "C" bool AsyncFlowControl_Equals_m1_12620 (AsyncFlowControl_t1_1459 * __this, AsyncFlowControl_t1_1459  ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.AsyncFlowControl::op_Equality(System.Threading.AsyncFlowControl,System.Threading.AsyncFlowControl)
extern "C" bool AsyncFlowControl_op_Equality_m1_12621 (Object_t * __this /* static, unused */, AsyncFlowControl_t1_1459  ___a, AsyncFlowControl_t1_1459  ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.AsyncFlowControl::op_Inequality(System.Threading.AsyncFlowControl,System.Threading.AsyncFlowControl)
extern "C" bool AsyncFlowControl_op_Inequality_m1_12622 (Object_t * __this /* static, unused */, AsyncFlowControl_t1_1459  ___a, AsyncFlowControl_t1_1459  ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
