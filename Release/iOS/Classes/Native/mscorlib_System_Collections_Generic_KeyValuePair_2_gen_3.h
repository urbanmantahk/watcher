﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ExifLibrary.ExifProperty
struct ExifProperty_t8_99;

#include "mscorlib_System_ValueType.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTag.h"

// System.Collections.Generic.KeyValuePair`2<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>
struct  KeyValuePair_2_t1_1924 
{
	// TKey System.Collections.Generic.KeyValuePair`2::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	ExifProperty_t8_99 * ___value_1;
};
