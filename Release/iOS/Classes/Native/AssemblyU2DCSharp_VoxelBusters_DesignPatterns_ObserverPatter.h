﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<VoxelBusters.DesignPatterns.IObserver>
struct List_1_t1_2632;

#include "AssemblyU2DCSharp_VoxelBusters_DesignPatterns_SingletonPatte_0.h"

// VoxelBusters.DesignPatterns.ObserverPattern`1<System.Object>
struct  ObserverPattern_1_t8_371  : public SingletonPattern_1_t8_372
{
	// System.Collections.Generic.List`1<VoxelBusters.DesignPatterns.IObserver> VoxelBusters.DesignPatterns.ObserverPattern`1::m_observers
	List_1_t1_2632 * ___m_observers_9;
};
