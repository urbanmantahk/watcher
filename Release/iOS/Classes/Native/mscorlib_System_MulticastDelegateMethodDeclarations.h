﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.MulticastDelegate
struct MulticastDelegate_t1_21;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Delegate[]
struct DelegateU5BU5D_t1_1664;
// System.Delegate
struct Delegate_t1_22;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.MulticastDelegate::.ctor(System.Object,System.String)
extern "C" void MulticastDelegate__ctor_m1_868 (MulticastDelegate_t1_21 * __this, Object_t * ___target, String_t* ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.MulticastDelegate::.ctor(System.Type,System.String)
extern "C" void MulticastDelegate__ctor_m1_869 (MulticastDelegate_t1_21 * __this, Type_t * ___target, String_t* ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.MulticastDelegate::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void MulticastDelegate_GetObjectData_m1_870 (MulticastDelegate_t1_21 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.MulticastDelegate::DynamicInvokeImpl(System.Object[])
extern "C" Object_t * MulticastDelegate_DynamicInvokeImpl_m1_871 (MulticastDelegate_t1_21 * __this, ObjectU5BU5D_t1_272* ___args, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.MulticastDelegate::Equals(System.Object)
extern "C" bool MulticastDelegate_Equals_m1_872 (MulticastDelegate_t1_21 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.MulticastDelegate::GetHashCode()
extern "C" int32_t MulticastDelegate_GetHashCode_m1_873 (MulticastDelegate_t1_21 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Delegate[] System.MulticastDelegate::GetInvocationList()
extern "C" DelegateU5BU5D_t1_1664* MulticastDelegate_GetInvocationList_m1_874 (MulticastDelegate_t1_21 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.MulticastDelegate::CombineImpl(System.Delegate)
extern "C" Delegate_t1_22 * MulticastDelegate_CombineImpl_m1_875 (MulticastDelegate_t1_21 * __this, Delegate_t1_22 * ___follow, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.MulticastDelegate::BaseEquals(System.MulticastDelegate)
extern "C" bool MulticastDelegate_BaseEquals_m1_876 (MulticastDelegate_t1_21 * __this, MulticastDelegate_t1_21 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.MulticastDelegate System.MulticastDelegate::KPM(System.MulticastDelegate,System.MulticastDelegate,System.MulticastDelegate&)
extern "C" MulticastDelegate_t1_21 * MulticastDelegate_KPM_m1_877 (Object_t * __this /* static, unused */, MulticastDelegate_t1_21 * ___needle, MulticastDelegate_t1_21 * ___haystack, MulticastDelegate_t1_21 ** ___tail, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.MulticastDelegate::RemoveImpl(System.Delegate)
extern "C" Delegate_t1_22 * MulticastDelegate_RemoveImpl_m1_878 (MulticastDelegate_t1_21 * __this, Delegate_t1_22 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.MulticastDelegate::op_Equality(System.MulticastDelegate,System.MulticastDelegate)
extern "C" bool MulticastDelegate_op_Equality_m1_879 (Object_t * __this /* static, unused */, MulticastDelegate_t1_21 * ___d1, MulticastDelegate_t1_21 * ___d2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.MulticastDelegate::op_Inequality(System.MulticastDelegate,System.MulticastDelegate)
extern "C" bool MulticastDelegate_op_Inequality_m1_880 (Object_t * __this /* static, unused */, MulticastDelegate_t1_21 * ___d1, MulticastDelegate_t1_21 * ___d2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
