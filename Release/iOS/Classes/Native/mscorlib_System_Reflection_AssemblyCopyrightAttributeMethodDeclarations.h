﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.AssemblyCopyrightAttribute
struct AssemblyCopyrightAttribute_t1_567;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Reflection.AssemblyCopyrightAttribute::.ctor(System.String)
extern "C" void AssemblyCopyrightAttribute__ctor_m1_6674 (AssemblyCopyrightAttribute_t1_567 * __this, String_t* ___copyright, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.AssemblyCopyrightAttribute::get_Copyright()
extern "C" String_t* AssemblyCopyrightAttribute_get_Copyright_m1_6675 (AssemblyCopyrightAttribute_t1_567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
