﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Reflection_Emit_PEFileKinds.h"

// System.Reflection.Emit.PEFileKinds
struct  PEFileKinds_t1_543 
{
	// System.Int32 System.Reflection.Emit.PEFileKinds::value__
	int32_t ___value___1;
};
