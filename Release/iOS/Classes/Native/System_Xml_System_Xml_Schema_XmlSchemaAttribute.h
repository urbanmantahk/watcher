﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "System_Xml_System_Xml_Schema_XmlSchemaAnnotated.h"

// System.Xml.Schema.XmlSchemaAttribute
struct  XmlSchemaAttribute_t4_55  : public XmlSchemaAnnotated_t4_53
{
};
