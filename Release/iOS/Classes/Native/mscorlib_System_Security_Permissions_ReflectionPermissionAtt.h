﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Security_Permissions_CodeAccessSecurityAttri.h"
#include "mscorlib_System_Security_Permissions_ReflectionPermissionFla.h"

// System.Security.Permissions.ReflectionPermissionAttribute
struct  ReflectionPermissionAttribute_t1_1302  : public CodeAccessSecurityAttribute_t1_1266
{
	// System.Security.Permissions.ReflectionPermissionFlag System.Security.Permissions.ReflectionPermissionAttribute::flags
	int32_t ___flags_2;
	// System.Boolean System.Security.Permissions.ReflectionPermissionAttribute::memberAccess
	bool ___memberAccess_3;
	// System.Boolean System.Security.Permissions.ReflectionPermissionAttribute::reflectionEmit
	bool ___reflectionEmit_4;
	// System.Boolean System.Security.Permissions.ReflectionPermissionAttribute::typeInfo
	bool ___typeInfo_5;
};
