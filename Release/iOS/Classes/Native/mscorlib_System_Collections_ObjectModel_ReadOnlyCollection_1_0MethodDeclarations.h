﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>
struct ReadOnlyCollection_1_t1_1751;
// System.Collections.Generic.IList`1<System.Reflection.CustomAttributeNamedArgument>
struct IList_1_t1_590;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Object
struct Object_t;
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t1_1749;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeNamedArgument>
struct IEnumerator_1_t1_2781;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgument.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C" void ReadOnlyCollection_1__ctor_m1_16518_gshared (ReadOnlyCollection_1_t1_1751 * __this, Object_t* ___list, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m1_16518(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t1_1751 *, Object_t*, const MethodInfo*))ReadOnlyCollection_1__ctor_m1_16518_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1_16519_gshared (ReadOnlyCollection_1_t1_1751 * __this, CustomAttributeNamedArgument_t1_593  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1_16519(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t1_1751 *, CustomAttributeNamedArgument_t1_593 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1_16519_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.ICollection<T>.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1_16520_gshared (ReadOnlyCollection_1_t1_1751 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1_16520(__this, method) (( void (*) (ReadOnlyCollection_1_t1_1751 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1_16520_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1_16521_gshared (ReadOnlyCollection_1_t1_1751 * __this, int32_t ___index, CustomAttributeNamedArgument_t1_593  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1_16521(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t1_1751 *, int32_t, CustomAttributeNamedArgument_t1_593 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1_16521_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1_16522_gshared (ReadOnlyCollection_1_t1_1751 * __this, CustomAttributeNamedArgument_t1_593  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1_16522(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t1_1751 *, CustomAttributeNamedArgument_t1_593 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1_16522_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1_16523_gshared (ReadOnlyCollection_1_t1_1751 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1_16523(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t1_1751 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1_16523_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C" CustomAttributeNamedArgument_t1_593  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1_16524_gshared (ReadOnlyCollection_1_t1_1751 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1_16524(__this, ___index, method) (( CustomAttributeNamedArgument_t1_593  (*) (ReadOnlyCollection_1_t1_1751 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1_16524_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1_16525_gshared (ReadOnlyCollection_1_t1_1751 * __this, int32_t ___index, CustomAttributeNamedArgument_t1_593  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1_16525(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t1_1751 *, int32_t, CustomAttributeNamedArgument_t1_593 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1_16525_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_16526_gshared (ReadOnlyCollection_1_t1_1751 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_16526(__this, method) (( bool (*) (ReadOnlyCollection_1_t1_1751 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_16526_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1_16527_gshared (ReadOnlyCollection_1_t1_1751 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1_16527(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t1_1751 *, Array_t *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1_16527_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1_16528_gshared (ReadOnlyCollection_1_t1_1751 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1_16528(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t1_1751 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1_16528_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Add(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m1_16529_gshared (ReadOnlyCollection_1_t1_1751 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m1_16529(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t1_1751 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m1_16529_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m1_16530_gshared (ReadOnlyCollection_1_t1_1751 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m1_16530(__this, method) (( void (*) (ReadOnlyCollection_1_t1_1751 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m1_16530_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Contains(System.Object)
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_Contains_m1_16531_gshared (ReadOnlyCollection_1_t1_1751 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m1_16531(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t1_1751 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m1_16531_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1_16532_gshared (ReadOnlyCollection_1_t1_1751 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1_16532(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t1_1751 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1_16532_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m1_16533_gshared (ReadOnlyCollection_1_t1_1751 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m1_16533(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t1_1751 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m1_16533_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Remove(System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m1_16534_gshared (ReadOnlyCollection_1_t1_1751 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m1_16534(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t1_1751 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m1_16534_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1_16535_gshared (ReadOnlyCollection_1_t1_1751 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1_16535(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t1_1751 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1_16535_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1_16536_gshared (ReadOnlyCollection_1_t1_1751 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1_16536(__this, method) (( bool (*) (ReadOnlyCollection_1_t1_1751 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1_16536_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1_16537_gshared (ReadOnlyCollection_1_t1_1751 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1_16537(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t1_1751 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1_16537_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.get_IsFixedSize()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1_16538_gshared (ReadOnlyCollection_1_t1_1751 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1_16538(__this, method) (( bool (*) (ReadOnlyCollection_1_t1_1751 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1_16538_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1_16539_gshared (ReadOnlyCollection_1_t1_1751 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1_16539(__this, method) (( bool (*) (ReadOnlyCollection_1_t1_1751 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1_16539_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IList_get_Item_m1_16540_gshared (ReadOnlyCollection_1_t1_1751 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m1_16540(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t1_1751 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m1_16540_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m1_16541_gshared (ReadOnlyCollection_1_t1_1751 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m1_16541(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t1_1751 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m1_16541_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::Contains(T)
extern "C" bool ReadOnlyCollection_1_Contains_m1_16542_gshared (ReadOnlyCollection_1_t1_1751 * __this, CustomAttributeNamedArgument_t1_593  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m1_16542(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t1_1751 *, CustomAttributeNamedArgument_t1_593 , const MethodInfo*))ReadOnlyCollection_1_Contains_m1_16542_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::CopyTo(T[],System.Int32)
extern "C" void ReadOnlyCollection_1_CopyTo_m1_16543_gshared (ReadOnlyCollection_1_t1_1751 * __this, CustomAttributeNamedArgumentU5BU5D_t1_1749* ___array, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m1_16543(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t1_1751 *, CustomAttributeNamedArgumentU5BU5D_t1_1749*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m1_16543_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::GetEnumerator()
extern "C" Object_t* ReadOnlyCollection_1_GetEnumerator_m1_16544_gshared (ReadOnlyCollection_1_t1_1751 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m1_16544(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t1_1751 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m1_16544_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T)
extern "C" int32_t ReadOnlyCollection_1_IndexOf_m1_16545_gshared (ReadOnlyCollection_1_t1_1751 * __this, CustomAttributeNamedArgument_t1_593  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m1_16545(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t1_1751 *, CustomAttributeNamedArgument_t1_593 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m1_16545_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::get_Count()
extern "C" int32_t ReadOnlyCollection_1_get_Count_m1_16546_gshared (ReadOnlyCollection_1_t1_1751 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m1_16546(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t1_1751 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m1_16546_gshared)(__this, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::get_Items()
extern "C" Object_t* ReadOnlyCollection_1_get_Items_m1_16547_gshared (ReadOnlyCollection_1_t1_1751 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Items_m1_16547(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t1_1751 *, const MethodInfo*))ReadOnlyCollection_1_get_Items_m1_16547_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::get_Item(System.Int32)
extern "C" CustomAttributeNamedArgument_t1_593  ReadOnlyCollection_1_get_Item_m1_16548_gshared (ReadOnlyCollection_1_t1_1751 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m1_16548(__this, ___index, method) (( CustomAttributeNamedArgument_t1_593  (*) (ReadOnlyCollection_1_t1_1751 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m1_16548_gshared)(__this, ___index, method)
