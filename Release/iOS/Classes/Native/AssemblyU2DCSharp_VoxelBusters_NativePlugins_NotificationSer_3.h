﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_NotificationSer_2.h"

// VoxelBusters.NativePlugins.NotificationServiceIOS
struct  NotificationServiceIOS_t8_262  : public NotificationService_t8_261
{
};
