﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_ActivationContext_ContextForm.h"

// System.ActivationContext/ContextForm
struct  ContextForm_t1_1491 
{
	// System.Int32 System.ActivationContext/ContextForm::value__
	int32_t ___value___1;
};
