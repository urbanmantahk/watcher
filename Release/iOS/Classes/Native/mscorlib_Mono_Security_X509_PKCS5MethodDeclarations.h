﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.X509.PKCS5
struct PKCS5_t1_182;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Security.X509.PKCS5::.ctor()
extern "C" void PKCS5__ctor_m1_2121 (PKCS5_t1_182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
