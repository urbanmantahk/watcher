﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.StrongName/StrongNameSignature
struct StrongNameSignature_t1_230;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Security.StrongName/StrongNameSignature::.ctor()
extern "C" void StrongNameSignature__ctor_m1_2560 (StrongNameSignature_t1_230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.StrongName/StrongNameSignature::get_Hash()
extern "C" ByteU5BU5D_t1_109* StrongNameSignature_get_Hash_m1_2561 (StrongNameSignature_t1_230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.StrongName/StrongNameSignature::set_Hash(System.Byte[])
extern "C" void StrongNameSignature_set_Hash_m1_2562 (StrongNameSignature_t1_230 * __this, ByteU5BU5D_t1_109* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.StrongName/StrongNameSignature::get_Signature()
extern "C" ByteU5BU5D_t1_109* StrongNameSignature_get_Signature_m1_2563 (StrongNameSignature_t1_230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.StrongName/StrongNameSignature::set_Signature(System.Byte[])
extern "C" void StrongNameSignature_set_Signature_m1_2564 (StrongNameSignature_t1_230 * __this, ByteU5BU5D_t1_109* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Mono.Security.StrongName/StrongNameSignature::get_MetadataPosition()
extern "C" uint32_t StrongNameSignature_get_MetadataPosition_m1_2565 (StrongNameSignature_t1_230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.StrongName/StrongNameSignature::set_MetadataPosition(System.UInt32)
extern "C" void StrongNameSignature_set_MetadataPosition_m1_2566 (StrongNameSignature_t1_230 * __this, uint32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Mono.Security.StrongName/StrongNameSignature::get_MetadataLength()
extern "C" uint32_t StrongNameSignature_get_MetadataLength_m1_2567 (StrongNameSignature_t1_230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.StrongName/StrongNameSignature::set_MetadataLength(System.UInt32)
extern "C" void StrongNameSignature_set_MetadataLength_m1_2568 (StrongNameSignature_t1_230 * __this, uint32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Mono.Security.StrongName/StrongNameSignature::get_SignaturePosition()
extern "C" uint32_t StrongNameSignature_get_SignaturePosition_m1_2569 (StrongNameSignature_t1_230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.StrongName/StrongNameSignature::set_SignaturePosition(System.UInt32)
extern "C" void StrongNameSignature_set_SignaturePosition_m1_2570 (StrongNameSignature_t1_230 * __this, uint32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Mono.Security.StrongName/StrongNameSignature::get_SignatureLength()
extern "C" uint32_t StrongNameSignature_get_SignatureLength_m1_2571 (StrongNameSignature_t1_230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.StrongName/StrongNameSignature::set_SignatureLength(System.UInt32)
extern "C" void StrongNameSignature_set_SignatureLength_m1_2572 (StrongNameSignature_t1_230 * __this, uint32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte Mono.Security.StrongName/StrongNameSignature::get_CliFlag()
extern "C" uint8_t StrongNameSignature_get_CliFlag_m1_2573 (StrongNameSignature_t1_230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.StrongName/StrongNameSignature::set_CliFlag(System.Byte)
extern "C" void StrongNameSignature_set_CliFlag_m1_2574 (StrongNameSignature_t1_230 * __this, uint8_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Mono.Security.StrongName/StrongNameSignature::get_CliFlagPosition()
extern "C" uint32_t StrongNameSignature_get_CliFlagPosition_m1_2575 (StrongNameSignature_t1_230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.StrongName/StrongNameSignature::set_CliFlagPosition(System.UInt32)
extern "C" void StrongNameSignature_set_CliFlagPosition_m1_2576 (StrongNameSignature_t1_230 * __this, uint32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
