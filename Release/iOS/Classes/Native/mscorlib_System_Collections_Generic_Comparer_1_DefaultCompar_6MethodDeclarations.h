﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct DefaultComparer_t1_2192;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor()
extern "C" void DefaultComparer__ctor_m1_17959_gshared (DefaultComparer_t1_2192 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1_17959(__this, method) (( void (*) (DefaultComparer_t1_2192 *, const MethodInfo*))DefaultComparer__ctor_m1_17959_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m1_17960_gshared (DefaultComparer_t1_2192 * __this, KeyValuePair_2_t1_2015  ___x, KeyValuePair_2_t1_2015  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m1_17960(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t1_2192 *, KeyValuePair_2_t1_2015 , KeyValuePair_2_t1_2015 , const MethodInfo*))DefaultComparer_Compare_m1_17960_gshared)(__this, ___x, ___y, method)
