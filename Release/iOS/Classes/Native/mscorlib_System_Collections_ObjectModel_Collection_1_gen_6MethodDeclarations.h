﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>
struct Collection_1_t1_2304;
// System.Collections.Generic.IList`1<UnityEngine.Color32>
struct IList_1_t1_2303;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Object
struct Object_t;
// UnityEngine.Color32[]
struct Color32U5BU5D_t6_280;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Color32>
struct IEnumerator_1_t1_2807;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color32.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::.ctor()
extern "C" void Collection_1__ctor_m1_19440_gshared (Collection_1_t1_2304 * __this, const MethodInfo* method);
#define Collection_1__ctor_m1_19440(__this, method) (( void (*) (Collection_1_t1_2304 *, const MethodInfo*))Collection_1__ctor_m1_19440_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1__ctor_m1_19441_gshared (Collection_1_t1_2304 * __this, Object_t* ___list, const MethodInfo* method);
#define Collection_1__ctor_m1_19441(__this, ___list, method) (( void (*) (Collection_1_t1_2304 *, Object_t*, const MethodInfo*))Collection_1__ctor_m1_19441_gshared)(__this, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_19442_gshared (Collection_1_t1_2304 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_19442(__this, method) (( bool (*) (Collection_1_t1_2304 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_19442_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m1_19443_gshared (Collection_1_t1_2304 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m1_19443(__this, ___array, ___index, method) (( void (*) (Collection_1_t1_2304 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m1_19443_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m1_19444_gshared (Collection_1_t1_2304 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m1_19444(__this, method) (( Object_t * (*) (Collection_1_t1_2304 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m1_19444_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m1_19445_gshared (Collection_1_t1_2304 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m1_19445(__this, ___value, method) (( int32_t (*) (Collection_1_t1_2304 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m1_19445_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m1_19446_gshared (Collection_1_t1_2304 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m1_19446(__this, ___value, method) (( bool (*) (Collection_1_t1_2304 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m1_19446_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m1_19447_gshared (Collection_1_t1_2304 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m1_19447(__this, ___value, method) (( int32_t (*) (Collection_1_t1_2304 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m1_19447_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m1_19448_gshared (Collection_1_t1_2304 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m1_19448(__this, ___index, ___value, method) (( void (*) (Collection_1_t1_2304 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m1_19448_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m1_19449_gshared (Collection_1_t1_2304 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m1_19449(__this, ___value, method) (( void (*) (Collection_1_t1_2304 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m1_19449_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m1_19450_gshared (Collection_1_t1_2304 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m1_19450(__this, method) (( bool (*) (Collection_1_t1_2304 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m1_19450_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m1_19451_gshared (Collection_1_t1_2304 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m1_19451(__this, method) (( Object_t * (*) (Collection_1_t1_2304 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m1_19451_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m1_19452_gshared (Collection_1_t1_2304 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m1_19452(__this, method) (( bool (*) (Collection_1_t1_2304 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m1_19452_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m1_19453_gshared (Collection_1_t1_2304 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m1_19453(__this, method) (( bool (*) (Collection_1_t1_2304 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m1_19453_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m1_19454_gshared (Collection_1_t1_2304 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m1_19454(__this, ___index, method) (( Object_t * (*) (Collection_1_t1_2304 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m1_19454_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m1_19455_gshared (Collection_1_t1_2304 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m1_19455(__this, ___index, ___value, method) (( void (*) (Collection_1_t1_2304 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m1_19455_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::Add(T)
extern "C" void Collection_1_Add_m1_19456_gshared (Collection_1_t1_2304 * __this, Color32_t6_49  ___item, const MethodInfo* method);
#define Collection_1_Add_m1_19456(__this, ___item, method) (( void (*) (Collection_1_t1_2304 *, Color32_t6_49 , const MethodInfo*))Collection_1_Add_m1_19456_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::Clear()
extern "C" void Collection_1_Clear_m1_19457_gshared (Collection_1_t1_2304 * __this, const MethodInfo* method);
#define Collection_1_Clear_m1_19457(__this, method) (( void (*) (Collection_1_t1_2304 *, const MethodInfo*))Collection_1_Clear_m1_19457_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::ClearItems()
extern "C" void Collection_1_ClearItems_m1_19458_gshared (Collection_1_t1_2304 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m1_19458(__this, method) (( void (*) (Collection_1_t1_2304 *, const MethodInfo*))Collection_1_ClearItems_m1_19458_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::Contains(T)
extern "C" bool Collection_1_Contains_m1_19459_gshared (Collection_1_t1_2304 * __this, Color32_t6_49  ___item, const MethodInfo* method);
#define Collection_1_Contains_m1_19459(__this, ___item, method) (( bool (*) (Collection_1_t1_2304 *, Color32_t6_49 , const MethodInfo*))Collection_1_Contains_m1_19459_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m1_19460_gshared (Collection_1_t1_2304 * __this, Color32U5BU5D_t6_280* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m1_19460(__this, ___array, ___index, method) (( void (*) (Collection_1_t1_2304 *, Color32U5BU5D_t6_280*, int32_t, const MethodInfo*))Collection_1_CopyTo_m1_19460_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m1_19461_gshared (Collection_1_t1_2304 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m1_19461(__this, method) (( Object_t* (*) (Collection_1_t1_2304 *, const MethodInfo*))Collection_1_GetEnumerator_m1_19461_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m1_19462_gshared (Collection_1_t1_2304 * __this, Color32_t6_49  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m1_19462(__this, ___item, method) (( int32_t (*) (Collection_1_t1_2304 *, Color32_t6_49 , const MethodInfo*))Collection_1_IndexOf_m1_19462_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m1_19463_gshared (Collection_1_t1_2304 * __this, int32_t ___index, Color32_t6_49  ___item, const MethodInfo* method);
#define Collection_1_Insert_m1_19463(__this, ___index, ___item, method) (( void (*) (Collection_1_t1_2304 *, int32_t, Color32_t6_49 , const MethodInfo*))Collection_1_Insert_m1_19463_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m1_19464_gshared (Collection_1_t1_2304 * __this, int32_t ___index, Color32_t6_49  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m1_19464(__this, ___index, ___item, method) (( void (*) (Collection_1_t1_2304 *, int32_t, Color32_t6_49 , const MethodInfo*))Collection_1_InsertItem_m1_19464_gshared)(__this, ___index, ___item, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::get_Items()
extern "C" Object_t* Collection_1_get_Items_m1_19465_gshared (Collection_1_t1_2304 * __this, const MethodInfo* method);
#define Collection_1_get_Items_m1_19465(__this, method) (( Object_t* (*) (Collection_1_t1_2304 *, const MethodInfo*))Collection_1_get_Items_m1_19465_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::Remove(T)
extern "C" bool Collection_1_Remove_m1_19466_gshared (Collection_1_t1_2304 * __this, Color32_t6_49  ___item, const MethodInfo* method);
#define Collection_1_Remove_m1_19466(__this, ___item, method) (( bool (*) (Collection_1_t1_2304 *, Color32_t6_49 , const MethodInfo*))Collection_1_Remove_m1_19466_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m1_19467_gshared (Collection_1_t1_2304 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m1_19467(__this, ___index, method) (( void (*) (Collection_1_t1_2304 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m1_19467_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m1_19468_gshared (Collection_1_t1_2304 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m1_19468(__this, ___index, method) (( void (*) (Collection_1_t1_2304 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m1_19468_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::get_Count()
extern "C" int32_t Collection_1_get_Count_m1_19469_gshared (Collection_1_t1_2304 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m1_19469(__this, method) (( int32_t (*) (Collection_1_t1_2304 *, const MethodInfo*))Collection_1_get_Count_m1_19469_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::get_Item(System.Int32)
extern "C" Color32_t6_49  Collection_1_get_Item_m1_19470_gshared (Collection_1_t1_2304 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m1_19470(__this, ___index, method) (( Color32_t6_49  (*) (Collection_1_t1_2304 *, int32_t, const MethodInfo*))Collection_1_get_Item_m1_19470_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m1_19471_gshared (Collection_1_t1_2304 * __this, int32_t ___index, Color32_t6_49  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m1_19471(__this, ___index, ___value, method) (( void (*) (Collection_1_t1_2304 *, int32_t, Color32_t6_49 , const MethodInfo*))Collection_1_set_Item_m1_19471_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m1_19472_gshared (Collection_1_t1_2304 * __this, int32_t ___index, Color32_t6_49  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m1_19472(__this, ___index, ___item, method) (( void (*) (Collection_1_t1_2304 *, int32_t, Color32_t6_49 , const MethodInfo*))Collection_1_SetItem_m1_19472_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m1_19473_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m1_19473(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m1_19473_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::ConvertItem(System.Object)
extern "C" Color32_t6_49  Collection_1_ConvertItem_m1_19474_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m1_19474(__this /* static, unused */, ___item, method) (( Color32_t6_49  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m1_19474_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m1_19475_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m1_19475(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m1_19475_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m1_19476_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m1_19476(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m1_19476_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m1_19477_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m1_19477(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m1_19477_gshared)(__this /* static, unused */, ___list, method)
