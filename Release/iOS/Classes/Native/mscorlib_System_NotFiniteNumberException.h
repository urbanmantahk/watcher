﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ArithmeticException.h"

// System.NotFiniteNumberException
struct  NotFiniteNumberException_t1_1581  : public ArithmeticException_t1_1502
{
	// System.Double System.NotFiniteNumberException::offending_number
	double ___offending_number_14;
};
