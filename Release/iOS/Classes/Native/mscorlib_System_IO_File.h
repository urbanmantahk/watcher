﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object.h"
#include "mscorlib_System_Nullable_1_gen.h"

// System.IO.File
struct  File_t1_419  : public Object_t
{
};
struct File_t1_419_StaticFields{
	// System.Nullable`1<System.DateTime> System.IO.File::defaultLocalFileTime
	Nullable_1_t1_371  ___defaultLocalFileTime_0;
};
