﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Queue/SyncQueue
struct SyncQueue_t1_297;
// System.Collections.Queue
struct Queue_t1_298;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Object[]
struct ObjectU5BU5D_t1_272;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Queue/SyncQueue::.ctor(System.Collections.Queue)
extern "C" void SyncQueue__ctor_m1_3335 (SyncQueue_t1_297 * __this, Queue_t1_298 * ___queue, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Queue/SyncQueue::get_Count()
extern "C" int32_t SyncQueue_get_Count_m1_3336 (SyncQueue_t1_297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Queue/SyncQueue::get_IsSynchronized()
extern "C" bool SyncQueue_get_IsSynchronized_m1_3337 (SyncQueue_t1_297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Queue/SyncQueue::get_SyncRoot()
extern "C" Object_t * SyncQueue_get_SyncRoot_m1_3338 (SyncQueue_t1_297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Queue/SyncQueue::CopyTo(System.Array,System.Int32)
extern "C" void SyncQueue_CopyTo_m1_3339 (SyncQueue_t1_297 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Collections.Queue/SyncQueue::GetEnumerator()
extern "C" Object_t * SyncQueue_GetEnumerator_m1_3340 (SyncQueue_t1_297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Queue/SyncQueue::Clone()
extern "C" Object_t * SyncQueue_Clone_m1_3341 (SyncQueue_t1_297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Queue/SyncQueue::Clear()
extern "C" void SyncQueue_Clear_m1_3342 (SyncQueue_t1_297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Queue/SyncQueue::TrimToSize()
extern "C" void SyncQueue_TrimToSize_m1_3343 (SyncQueue_t1_297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Queue/SyncQueue::Contains(System.Object)
extern "C" bool SyncQueue_Contains_m1_3344 (SyncQueue_t1_297 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Queue/SyncQueue::Dequeue()
extern "C" Object_t * SyncQueue_Dequeue_m1_3345 (SyncQueue_t1_297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Queue/SyncQueue::Enqueue(System.Object)
extern "C" void SyncQueue_Enqueue_m1_3346 (SyncQueue_t1_297 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Queue/SyncQueue::Peek()
extern "C" Object_t * SyncQueue_Peek_m1_3347 (SyncQueue_t1_297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Collections.Queue/SyncQueue::ToArray()
extern "C" ObjectU5BU5D_t1_272* SyncQueue_ToArray_m1_3348 (SyncQueue_t1_297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
