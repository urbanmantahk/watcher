﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Security_Policy_CodeGroup.h"
#include "mscorlib_System_Security_Permissions_FileIOPermissionAccess.h"

// System.Security.Policy.FileCodeGroup
struct  FileCodeGroup_t1_1346  : public CodeGroup_t1_1339
{
	// System.Security.Permissions.FileIOPermissionAccess System.Security.Policy.FileCodeGroup::m_access
	int32_t ___m_access_6;
};
