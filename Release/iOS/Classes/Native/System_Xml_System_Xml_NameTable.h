﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Xml.NameTable/Entry[]
struct EntryU5BU5D_t4_109;

#include "System_Xml_System_Xml_XmlNameTable.h"

// System.Xml.NameTable
struct  NameTable_t4_108  : public XmlNameTable_t4_67
{
	// System.Int32 System.Xml.NameTable::count
	int32_t ___count_0;
	// System.Xml.NameTable/Entry[] System.Xml.NameTable::buckets
	EntryU5BU5D_t4_109* ___buckets_1;
	// System.Int32 System.Xml.NameTable::size
	int32_t ___size_2;
};
