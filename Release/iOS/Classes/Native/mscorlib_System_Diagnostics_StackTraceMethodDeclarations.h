﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Diagnostics.StackTrace
struct StackTrace_t1_336;
// System.Exception
struct Exception_t1_33;
// System.Diagnostics.StackFrame
struct StackFrame_t1_334;
// System.Threading.Thread
struct Thread_t1_901;
// System.Diagnostics.StackFrame[]
struct StackFrameU5BU5D_t1_337;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Diagnostics.StackTrace::.ctor()
extern "C" void StackTrace__ctor_m1_3638 (StackTrace_t1_336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.StackTrace::.ctor(System.Boolean)
extern "C" void StackTrace__ctor_m1_3639 (StackTrace_t1_336 * __this, bool ___fNeedFileInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.StackTrace::.ctor(System.Int32)
extern "C" void StackTrace__ctor_m1_3640 (StackTrace_t1_336 * __this, int32_t ___skipFrames, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.StackTrace::.ctor(System.Int32,System.Boolean)
extern "C" void StackTrace__ctor_m1_3641 (StackTrace_t1_336 * __this, int32_t ___skipFrames, bool ___fNeedFileInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.StackTrace::.ctor(System.Exception)
extern "C" void StackTrace__ctor_m1_3642 (StackTrace_t1_336 * __this, Exception_t1_33 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.StackTrace::.ctor(System.Exception,System.Boolean)
extern "C" void StackTrace__ctor_m1_3643 (StackTrace_t1_336 * __this, Exception_t1_33 * ___e, bool ___fNeedFileInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.StackTrace::.ctor(System.Exception,System.Int32)
extern "C" void StackTrace__ctor_m1_3644 (StackTrace_t1_336 * __this, Exception_t1_33 * ___e, int32_t ___skipFrames, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.StackTrace::.ctor(System.Exception,System.Int32,System.Boolean)
extern "C" void StackTrace__ctor_m1_3645 (StackTrace_t1_336 * __this, Exception_t1_33 * ___e, int32_t ___skipFrames, bool ___fNeedFileInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.StackTrace::.ctor(System.Exception,System.Int32,System.Boolean,System.Boolean)
extern "C" void StackTrace__ctor_m1_3646 (StackTrace_t1_336 * __this, Exception_t1_33 * ___e, int32_t ___skipFrames, bool ___fNeedFileInfo, bool ___returnNativeFrames, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.StackTrace::.ctor(System.Diagnostics.StackFrame)
extern "C" void StackTrace__ctor_m1_3647 (StackTrace_t1_336 * __this, StackFrame_t1_334 * ___frame, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.StackTrace::.ctor(System.Threading.Thread,System.Boolean)
extern "C" void StackTrace__ctor_m1_3648 (StackTrace_t1_336 * __this, Thread_t1_901 * ___targetThread, bool ___needFileInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.StackTrace::init_frames(System.Int32,System.Boolean)
extern "C" void StackTrace_init_frames_m1_3649 (StackTrace_t1_336 * __this, int32_t ___skipFrames, bool ___fNeedFileInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Diagnostics.StackFrame[] System.Diagnostics.StackTrace::get_trace(System.Exception,System.Int32,System.Boolean)
extern "C" StackFrameU5BU5D_t1_337* StackTrace_get_trace_m1_3650 (Object_t * __this /* static, unused */, Exception_t1_33 * ___e, int32_t ___skipFrames, bool ___fNeedFileInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Diagnostics.StackTrace::get_FrameCount()
extern "C" int32_t StackTrace_get_FrameCount_m1_3651 (StackTrace_t1_336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Diagnostics.StackFrame System.Diagnostics.StackTrace::GetFrame(System.Int32)
extern "C" StackFrame_t1_334 * StackTrace_GetFrame_m1_3652 (StackTrace_t1_336 * __this, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Diagnostics.StackFrame[] System.Diagnostics.StackTrace::GetFrames()
extern "C" StackFrameU5BU5D_t1_337* StackTrace_GetFrames_m1_3653 (StackTrace_t1_336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Diagnostics.StackTrace::ToString()
extern "C" String_t* StackTrace_ToString_m1_3654 (StackTrace_t1_336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
