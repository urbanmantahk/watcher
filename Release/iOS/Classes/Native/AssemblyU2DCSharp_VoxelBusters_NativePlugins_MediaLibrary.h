﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VoxelBusters.NativePlugins.MediaLibrary/PickImageCompletion
struct PickImageCompletion_t8_240;
// VoxelBusters.NativePlugins.MediaLibrary/SaveImageToGalleryCompletion
struct SaveImageToGalleryCompletion_t8_241;
// VoxelBusters.NativePlugins.MediaLibrary/PickVideoCompletion
struct PickVideoCompletion_t8_242;
// VoxelBusters.NativePlugins.MediaLibrary/PlayVideoCompletion
struct PlayVideoCompletion_t8_243;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// VoxelBusters.NativePlugins.MediaLibrary
struct  MediaLibrary_t8_245  : public MonoBehaviour_t6_91
{
	// VoxelBusters.NativePlugins.MediaLibrary/PickImageCompletion VoxelBusters.NativePlugins.MediaLibrary::OnPickImageFinished
	PickImageCompletion_t8_240 * ___OnPickImageFinished_2;
	// VoxelBusters.NativePlugins.MediaLibrary/SaveImageToGalleryCompletion VoxelBusters.NativePlugins.MediaLibrary::OnSaveImageToGalleryFinished
	SaveImageToGalleryCompletion_t8_241 * ___OnSaveImageToGalleryFinished_3;
	// VoxelBusters.NativePlugins.MediaLibrary/PickVideoCompletion VoxelBusters.NativePlugins.MediaLibrary::OnPickVideoFinished
	PickVideoCompletion_t8_242 * ___OnPickVideoFinished_4;
	// VoxelBusters.NativePlugins.MediaLibrary/PlayVideoCompletion VoxelBusters.NativePlugins.MediaLibrary::OnPlayVideoFinished
	PlayVideoCompletion_t8_243 * ___OnPlayVideoFinished_5;
	// System.Single VoxelBusters.NativePlugins.MediaLibrary::m_scaleFactor
	float ___m_scaleFactor_6;
};
