﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Principal.GenericPrincipal
struct GenericPrincipal_t1_1373;
// System.Security.Principal.IIdentity
struct IIdentity_t1_1374;
// System.String[]
struct StringU5BU5D_t1_238;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Principal.GenericPrincipal::.ctor(System.Security.Principal.IIdentity,System.String[])
extern "C" void GenericPrincipal__ctor_m1_11789 (GenericPrincipal_t1_1373 * __this, Object_t * ___identity, StringU5BU5D_t1_238* ___roles, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Principal.IIdentity System.Security.Principal.GenericPrincipal::get_Identity()
extern "C" Object_t * GenericPrincipal_get_Identity_m1_11790 (GenericPrincipal_t1_1373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Principal.GenericPrincipal::IsInRole(System.String)
extern "C" bool GenericPrincipal_IsInRole_m1_11791 (GenericPrincipal_t1_1373 * __this, String_t* ___role, const MethodInfo* method) IL2CPP_METHOD_ATTR;
