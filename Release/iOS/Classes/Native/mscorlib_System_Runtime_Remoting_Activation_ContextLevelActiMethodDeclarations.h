﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Activation.ContextLevelActivator
struct ContextLevelActivator_t1_855;
// System.Runtime.Remoting.Activation.IActivator
struct IActivator_t1_851;
// System.Runtime.Remoting.Activation.IConstructionReturnMessage
struct IConstructionReturnMessage_t1_1703;
// System.Runtime.Remoting.Activation.IConstructionCallMessage
struct IConstructionCallMessage_t1_1702;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Remoting_Activation_ActivatorLevel.h"

// System.Void System.Runtime.Remoting.Activation.ContextLevelActivator::.ctor(System.Runtime.Remoting.Activation.IActivator)
extern "C" void ContextLevelActivator__ctor_m1_7962 (ContextLevelActivator_t1_855 * __this, Object_t * ___next, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Activation.ActivatorLevel System.Runtime.Remoting.Activation.ContextLevelActivator::get_Level()
extern "C" int32_t ContextLevelActivator_get_Level_m1_7963 (ContextLevelActivator_t1_855 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Activation.IActivator System.Runtime.Remoting.Activation.ContextLevelActivator::get_NextActivator()
extern "C" Object_t * ContextLevelActivator_get_NextActivator_m1_7964 (ContextLevelActivator_t1_855 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Activation.ContextLevelActivator::set_NextActivator(System.Runtime.Remoting.Activation.IActivator)
extern "C" void ContextLevelActivator_set_NextActivator_m1_7965 (ContextLevelActivator_t1_855 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Activation.IConstructionReturnMessage System.Runtime.Remoting.Activation.ContextLevelActivator::Activate(System.Runtime.Remoting.Activation.IConstructionCallMessage)
extern "C" Object_t * ContextLevelActivator_Activate_m1_7966 (ContextLevelActivator_t1_855 * __this, Object_t * ___ctorCall, const MethodInfo* method) IL2CPP_METHOD_ATTR;
