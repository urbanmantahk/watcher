﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__4MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Boomlagoon.JSON.JSONValue>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1_25764(__this, ___dictionary, method) (( void (*) (Enumerator_t1_2630 *, Dictionary_2_t1_1919 *, const MethodInfo*))Enumerator__ctor_m1_15990_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,Boomlagoon.JSON.JSONValue>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_25765(__this, method) (( Object_t * (*) (Enumerator_t1_2630 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_15991_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Boomlagoon.JSON.JSONValue>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1_25766(__this, method) (( void (*) (Enumerator_t1_2630 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_15992_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,Boomlagoon.JSON.JSONValue>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_25767(__this, method) (( DictionaryEntry_t1_284  (*) (Enumerator_t1_2630 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_15993_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,Boomlagoon.JSON.JSONValue>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_25768(__this, method) (( Object_t * (*) (Enumerator_t1_2630 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_15994_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,Boomlagoon.JSON.JSONValue>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_25769(__this, method) (( Object_t * (*) (Enumerator_t1_2630 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_15995_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,Boomlagoon.JSON.JSONValue>::MoveNext()
#define Enumerator_MoveNext_m1_25770(__this, method) (( bool (*) (Enumerator_t1_2630 *, const MethodInfo*))Enumerator_MoveNext_m1_15996_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,Boomlagoon.JSON.JSONValue>::get_Current()
#define Enumerator_get_Current_m1_25771(__this, method) (( KeyValuePair_2_t1_1915  (*) (Enumerator_t1_2630 *, const MethodInfo*))Enumerator_get_Current_m1_15997_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,Boomlagoon.JSON.JSONValue>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1_25772(__this, method) (( String_t* (*) (Enumerator_t1_2630 *, const MethodInfo*))Enumerator_get_CurrentKey_m1_15998_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,Boomlagoon.JSON.JSONValue>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1_25773(__this, method) (( JSONValue_t8_4 * (*) (Enumerator_t1_2630 *, const MethodInfo*))Enumerator_get_CurrentValue_m1_15999_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Boomlagoon.JSON.JSONValue>::Reset()
#define Enumerator_Reset_m1_25774(__this, method) (( void (*) (Enumerator_t1_2630 *, const MethodInfo*))Enumerator_Reset_m1_16000_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Boomlagoon.JSON.JSONValue>::VerifyState()
#define Enumerator_VerifyState_m1_25775(__this, method) (( void (*) (Enumerator_t1_2630 *, const MethodInfo*))Enumerator_VerifyState_m1_16001_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Boomlagoon.JSON.JSONValue>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1_25776(__this, method) (( void (*) (Enumerator_t1_2630 *, const MethodInfo*))Enumerator_VerifyCurrent_m1_16002_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Boomlagoon.JSON.JSONValue>::Dispose()
#define Enumerator_Dispose_m1_25777(__this, method) (( void (*) (Enumerator_t1_2630 *, const MethodInfo*))Enumerator_Dispose_m1_16003_gshared)(__this, method)
