﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Permissions.KeyContainerPermissionAccessEntry
struct KeyContainerPermissionAccessEntry_t1_1290;
// System.Security.Cryptography.CspParameters
struct CspParameters_t1_164;
// System.String
struct String_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Permissions_KeyContainerPermissionF.h"

// System.Void System.Security.Permissions.KeyContainerPermissionAccessEntry::.ctor(System.Security.Cryptography.CspParameters,System.Security.Permissions.KeyContainerPermissionFlags)
extern "C" void KeyContainerPermissionAccessEntry__ctor_m1_10967 (KeyContainerPermissionAccessEntry_t1_1290 * __this, CspParameters_t1_164 * ___parameters, int32_t ___flags, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.KeyContainerPermissionAccessEntry::.ctor(System.String,System.Security.Permissions.KeyContainerPermissionFlags)
extern "C" void KeyContainerPermissionAccessEntry__ctor_m1_10968 (KeyContainerPermissionAccessEntry_t1_1290 * __this, String_t* ___keyContainerName, int32_t ___flags, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.KeyContainerPermissionAccessEntry::.ctor(System.String,System.String,System.Int32,System.String,System.Int32,System.Security.Permissions.KeyContainerPermissionFlags)
extern "C" void KeyContainerPermissionAccessEntry__ctor_m1_10969 (KeyContainerPermissionAccessEntry_t1_1290 * __this, String_t* ___keyStore, String_t* ___providerName, int32_t ___providerType, String_t* ___keyContainerName, int32_t ___keySpec, int32_t ___flags, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Permissions.KeyContainerPermissionFlags System.Security.Permissions.KeyContainerPermissionAccessEntry::get_Flags()
extern "C" int32_t KeyContainerPermissionAccessEntry_get_Flags_m1_10970 (KeyContainerPermissionAccessEntry_t1_1290 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.KeyContainerPermissionAccessEntry::set_Flags(System.Security.Permissions.KeyContainerPermissionFlags)
extern "C" void KeyContainerPermissionAccessEntry_set_Flags_m1_10971 (KeyContainerPermissionAccessEntry_t1_1290 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Permissions.KeyContainerPermissionAccessEntry::get_KeyContainerName()
extern "C" String_t* KeyContainerPermissionAccessEntry_get_KeyContainerName_m1_10972 (KeyContainerPermissionAccessEntry_t1_1290 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.KeyContainerPermissionAccessEntry::set_KeyContainerName(System.String)
extern "C" void KeyContainerPermissionAccessEntry_set_KeyContainerName_m1_10973 (KeyContainerPermissionAccessEntry_t1_1290 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Permissions.KeyContainerPermissionAccessEntry::get_KeySpec()
extern "C" int32_t KeyContainerPermissionAccessEntry_get_KeySpec_m1_10974 (KeyContainerPermissionAccessEntry_t1_1290 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.KeyContainerPermissionAccessEntry::set_KeySpec(System.Int32)
extern "C" void KeyContainerPermissionAccessEntry_set_KeySpec_m1_10975 (KeyContainerPermissionAccessEntry_t1_1290 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Permissions.KeyContainerPermissionAccessEntry::get_KeyStore()
extern "C" String_t* KeyContainerPermissionAccessEntry_get_KeyStore_m1_10976 (KeyContainerPermissionAccessEntry_t1_1290 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.KeyContainerPermissionAccessEntry::set_KeyStore(System.String)
extern "C" void KeyContainerPermissionAccessEntry_set_KeyStore_m1_10977 (KeyContainerPermissionAccessEntry_t1_1290 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Permissions.KeyContainerPermissionAccessEntry::get_ProviderName()
extern "C" String_t* KeyContainerPermissionAccessEntry_get_ProviderName_m1_10978 (KeyContainerPermissionAccessEntry_t1_1290 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.KeyContainerPermissionAccessEntry::set_ProviderName(System.String)
extern "C" void KeyContainerPermissionAccessEntry_set_ProviderName_m1_10979 (KeyContainerPermissionAccessEntry_t1_1290 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Permissions.KeyContainerPermissionAccessEntry::get_ProviderType()
extern "C" int32_t KeyContainerPermissionAccessEntry_get_ProviderType_m1_10980 (KeyContainerPermissionAccessEntry_t1_1290 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.KeyContainerPermissionAccessEntry::set_ProviderType(System.Int32)
extern "C" void KeyContainerPermissionAccessEntry_set_ProviderType_m1_10981 (KeyContainerPermissionAccessEntry_t1_1290 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.KeyContainerPermissionAccessEntry::Equals(System.Object)
extern "C" bool KeyContainerPermissionAccessEntry_Equals_m1_10982 (KeyContainerPermissionAccessEntry_t1_1290 * __this, Object_t * ___o, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Permissions.KeyContainerPermissionAccessEntry::GetHashCode()
extern "C" int32_t KeyContainerPermissionAccessEntry_GetHashCode_m1_10983 (KeyContainerPermissionAccessEntry_t1_1290 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
