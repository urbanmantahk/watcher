﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.EqualityComparer`1<System.Byte>
struct EqualityComparer_1_t1_2656;

#include "mscorlib_System_Object.h"

// System.Collections.Generic.EqualityComparer`1<System.Byte>
struct  EqualityComparer_1_t1_2656  : public Object_t
{
};
struct EqualityComparer_1_t1_2656_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t1_2656 * ____default_0;
};
