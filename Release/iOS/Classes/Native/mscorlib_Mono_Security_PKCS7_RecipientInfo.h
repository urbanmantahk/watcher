﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "mscorlib_System_Object.h"

// Mono.Security.PKCS7/RecipientInfo
struct  RecipientInfo_t1_225  : public Object_t
{
	// System.Int32 Mono.Security.PKCS7/RecipientInfo::_version
	int32_t ____version_0;
	// System.String Mono.Security.PKCS7/RecipientInfo::_oid
	String_t* ____oid_1;
	// System.Byte[] Mono.Security.PKCS7/RecipientInfo::_key
	ByteU5BU5D_t1_109* ____key_2;
	// System.Byte[] Mono.Security.PKCS7/RecipientInfo::_ski
	ByteU5BU5D_t1_109* ____ski_3;
	// System.String Mono.Security.PKCS7/RecipientInfo::_issuer
	String_t* ____issuer_4;
	// System.Byte[] Mono.Security.PKCS7/RecipientInfo::_serial
	ByteU5BU5D_t1_109* ____serial_5;
};
