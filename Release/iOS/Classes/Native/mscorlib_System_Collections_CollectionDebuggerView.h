﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.ICollection
struct ICollection_t1_280;

#include "mscorlib_System_Object.h"

// System.Collections.CollectionDebuggerView
struct  CollectionDebuggerView_t1_279  : public Object_t
{
	// System.Collections.ICollection System.Collections.CollectionDebuggerView::c
	Object_t * ___c_0;
};
