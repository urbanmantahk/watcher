﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_SystemException.h"

// System.BadImageFormatException
struct  BadImageFormatException_t1_1506  : public SystemException_t1_250
{
	// System.String System.BadImageFormatException::fileName
	String_t* ___fileName_13;
	// System.String System.BadImageFormatException::fusionLog
	String_t* ___fusionLog_14;
};
