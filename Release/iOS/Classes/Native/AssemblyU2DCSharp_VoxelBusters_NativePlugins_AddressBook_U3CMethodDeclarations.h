﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.AddressBook/<ReadContacts>c__AnonStorey9
struct U3CReadContactsU3Ec__AnonStorey9_t8_195;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eABAuthorizatio.h"

// System.Void VoxelBusters.NativePlugins.AddressBook/<ReadContacts>c__AnonStorey9::.ctor()
extern "C" void U3CReadContactsU3Ec__AnonStorey9__ctor_m8_1126 (U3CReadContactsU3Ec__AnonStorey9_t8_195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.AddressBook/<ReadContacts>c__AnonStorey9::<>m__8(VoxelBusters.NativePlugins.eABAuthorizationStatus,System.String)
extern "C" void U3CReadContactsU3Ec__AnonStorey9_U3CU3Em__8_m8_1127 (U3CReadContactsU3Ec__AnonStorey9_t8_195 * __this, int32_t ____newAuthStatus, String_t* ____error, const MethodInfo* method) IL2CPP_METHOD_ATTR;
