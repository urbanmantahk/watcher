﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Net.ICredentials
struct ICredentials_t3_18;

#include "System_Xml_System_Xml_XmlResolver.h"

// System.Xml.XmlUrlResolver
struct  XmlUrlResolver_t4_185  : public XmlResolver_t4_68
{
	// System.Net.ICredentials System.Xml.XmlUrlResolver::credential
	Object_t * ___credential_0;
};
