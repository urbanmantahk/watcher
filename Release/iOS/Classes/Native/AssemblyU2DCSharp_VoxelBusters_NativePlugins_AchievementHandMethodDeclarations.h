﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.AchievementHandler
struct AchievementHandler_t8_327;
// VoxelBusters.NativePlugins.AchievementDescription[]
struct AchievementDescriptionU5BU5D_t8_224;
// VoxelBusters.NativePlugins.AchievementDescription
struct AchievementDescription_t8_225;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.AchievementHandler::.ctor()
extern "C" void AchievementHandler__ctor_m8_1908 (AchievementHandler_t8_327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.AchievementHandler::.cctor()
extern "C" void AchievementHandler__cctor_m8_1909 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.AchievementHandler::SetAchievementDescriptionList(VoxelBusters.NativePlugins.AchievementDescription[])
extern "C" void AchievementHandler_SetAchievementDescriptionList_m8_1910 (Object_t * __this /* static, unused */, AchievementDescriptionU5BU5D_t8_224* ____descriptionList, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.NativePlugins.AchievementDescription VoxelBusters.NativePlugins.AchievementHandler::GetAchievementDescription(System.String)
extern "C" AchievementDescription_t8_225 * AchievementHandler_GetAchievementDescription_m8_1911 (Object_t * __this /* static, unused */, String_t* ____achievementID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
