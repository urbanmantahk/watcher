﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Globalization.PersianCalendar
struct PersianCalendar_t1_384;
// System.Int32[]
struct Int32U5BU5D_t1_275;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime.h"
#include "mscorlib_System_DayOfWeek.h"
#include "mscorlib_System_Globalization_CalendarAlgorithmType.h"

// System.Void System.Globalization.PersianCalendar::.ctor()
extern "C" void PersianCalendar__ctor_m1_4385 (PersianCalendar_t1_384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.PersianCalendar::.cctor()
extern "C" void PersianCalendar__cctor_m1_4386 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] System.Globalization.PersianCalendar::get_Eras()
extern "C" Int32U5BU5D_t1_275* PersianCalendar_get_Eras_m1_4387 (PersianCalendar_t1_384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.PersianCalendar::get_TwoDigitYearMax()
extern "C" int32_t PersianCalendar_get_TwoDigitYearMax_m1_4388 (PersianCalendar_t1_384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.PersianCalendar::set_TwoDigitYearMax(System.Int32)
extern "C" void PersianCalendar_set_TwoDigitYearMax_m1_4389 (PersianCalendar_t1_384 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.PersianCalendar::M_CheckDateTime(System.DateTime)
extern "C" void PersianCalendar_M_CheckDateTime_m1_4390 (PersianCalendar_t1_384 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.PersianCalendar::M_CheckEra(System.Int32&)
extern "C" void PersianCalendar_M_CheckEra_m1_4391 (PersianCalendar_t1_384 * __this, int32_t* ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.PersianCalendar::M_CheckYE(System.Int32,System.Int32&)
extern "C" void PersianCalendar_M_CheckYE_m1_4392 (PersianCalendar_t1_384 * __this, int32_t ___year, int32_t* ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.PersianCalendar::M_CheckYME(System.Int32,System.Int32,System.Int32&)
extern "C" void PersianCalendar_M_CheckYME_m1_4393 (PersianCalendar_t1_384 * __this, int32_t ___year, int32_t ___month, int32_t* ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.PersianCalendar::M_CheckYMDE(System.Int32,System.Int32,System.Int32,System.Int32&)
extern "C" void PersianCalendar_M_CheckYMDE_m1_4394 (PersianCalendar_t1_384 * __this, int32_t ___year, int32_t ___month, int32_t ___day, int32_t* ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.PersianCalendar::fixed_from_dmy(System.Int32,System.Int32,System.Int32)
extern "C" int32_t PersianCalendar_fixed_from_dmy_m1_4395 (PersianCalendar_t1_384 * __this, int32_t ___day, int32_t ___month, int32_t ___year, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.PersianCalendar::year_from_fixed(System.Int32)
extern "C" int32_t PersianCalendar_year_from_fixed_m1_4396 (PersianCalendar_t1_384 * __this, int32_t ___date, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.PersianCalendar::my_from_fixed(System.Int32&,System.Int32&,System.Int32)
extern "C" void PersianCalendar_my_from_fixed_m1_4397 (PersianCalendar_t1_384 * __this, int32_t* ___month, int32_t* ___year, int32_t ___date, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.PersianCalendar::dmy_from_fixed(System.Int32&,System.Int32&,System.Int32&,System.Int32)
extern "C" void PersianCalendar_dmy_from_fixed_m1_4398 (PersianCalendar_t1_384 * __this, int32_t* ___day, int32_t* ___month, int32_t* ___year, int32_t ___date, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.PersianCalendar::is_leap_year(System.Int32)
extern "C" bool PersianCalendar_is_leap_year_m1_4399 (PersianCalendar_t1_384 * __this, int32_t ___year, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.PersianCalendar::AddMonths(System.DateTime,System.Int32)
extern "C" DateTime_t1_150  PersianCalendar_AddMonths_m1_4400 (PersianCalendar_t1_384 * __this, DateTime_t1_150  ___time, int32_t ___months, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.PersianCalendar::AddYears(System.DateTime,System.Int32)
extern "C" DateTime_t1_150  PersianCalendar_AddYears_m1_4401 (PersianCalendar_t1_384 * __this, DateTime_t1_150  ___time, int32_t ___years, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.PersianCalendar::GetDayOfMonth(System.DateTime)
extern "C" int32_t PersianCalendar_GetDayOfMonth_m1_4402 (PersianCalendar_t1_384 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DayOfWeek System.Globalization.PersianCalendar::GetDayOfWeek(System.DateTime)
extern "C" int32_t PersianCalendar_GetDayOfWeek_m1_4403 (PersianCalendar_t1_384 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.PersianCalendar::GetDayOfYear(System.DateTime)
extern "C" int32_t PersianCalendar_GetDayOfYear_m1_4404 (PersianCalendar_t1_384 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.PersianCalendar::GetDaysInMonth(System.Int32,System.Int32,System.Int32)
extern "C" int32_t PersianCalendar_GetDaysInMonth_m1_4405 (PersianCalendar_t1_384 * __this, int32_t ___year, int32_t ___month, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.PersianCalendar::GetDaysInYear(System.Int32,System.Int32)
extern "C" int32_t PersianCalendar_GetDaysInYear_m1_4406 (PersianCalendar_t1_384 * __this, int32_t ___year, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.PersianCalendar::GetEra(System.DateTime)
extern "C" int32_t PersianCalendar_GetEra_m1_4407 (PersianCalendar_t1_384 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.PersianCalendar::GetLeapMonth(System.Int32,System.Int32)
extern "C" int32_t PersianCalendar_GetLeapMonth_m1_4408 (PersianCalendar_t1_384 * __this, int32_t ___year, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.PersianCalendar::GetMonth(System.DateTime)
extern "C" int32_t PersianCalendar_GetMonth_m1_4409 (PersianCalendar_t1_384 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.PersianCalendar::GetMonthsInYear(System.Int32,System.Int32)
extern "C" int32_t PersianCalendar_GetMonthsInYear_m1_4410 (PersianCalendar_t1_384 * __this, int32_t ___year, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.PersianCalendar::GetYear(System.DateTime)
extern "C" int32_t PersianCalendar_GetYear_m1_4411 (PersianCalendar_t1_384 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.PersianCalendar::IsLeapDay(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" bool PersianCalendar_IsLeapDay_m1_4412 (PersianCalendar_t1_384 * __this, int32_t ___year, int32_t ___month, int32_t ___day, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.PersianCalendar::IsLeapMonth(System.Int32,System.Int32,System.Int32)
extern "C" bool PersianCalendar_IsLeapMonth_m1_4413 (PersianCalendar_t1_384 * __this, int32_t ___year, int32_t ___month, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.PersianCalendar::IsLeapYear(System.Int32,System.Int32)
extern "C" bool PersianCalendar_IsLeapYear_m1_4414 (PersianCalendar_t1_384 * __this, int32_t ___year, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.PersianCalendar::ToDateTime(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" DateTime_t1_150  PersianCalendar_ToDateTime_m1_4415 (PersianCalendar_t1_384 * __this, int32_t ___year, int32_t ___month, int32_t ___day, int32_t ___hour, int32_t ___minute, int32_t ___second, int32_t ___millisecond, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.PersianCalendar::ToFourDigitYear(System.Int32)
extern "C" int32_t PersianCalendar_ToFourDigitYear_m1_4416 (PersianCalendar_t1_384 * __this, int32_t ___year, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.CalendarAlgorithmType System.Globalization.PersianCalendar::get_AlgorithmType()
extern "C" int32_t PersianCalendar_get_AlgorithmType_m1_4417 (PersianCalendar_t1_384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.PersianCalendar::get_MinSupportedDateTime()
extern "C" DateTime_t1_150  PersianCalendar_get_MinSupportedDateTime_m1_4418 (PersianCalendar_t1_384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.PersianCalendar::get_MaxSupportedDateTime()
extern "C" DateTime_t1_150  PersianCalendar_get_MaxSupportedDateTime_m1_4419 (PersianCalendar_t1_384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
