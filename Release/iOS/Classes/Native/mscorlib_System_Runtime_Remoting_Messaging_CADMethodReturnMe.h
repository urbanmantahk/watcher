﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Object_t;
// System.Runtime.Remoting.Messaging.CADArgHolder
struct CADArgHolder_t1_921;

#include "mscorlib_System_Runtime_Remoting_Messaging_CADMessageBase.h"

// System.Runtime.Remoting.Messaging.CADMethodReturnMessage
struct  CADMethodReturnMessage_t1_881  : public CADMessageBase_t1_924
{
	// System.Object System.Runtime.Remoting.Messaging.CADMethodReturnMessage::_returnValue
	Object_t * ____returnValue_4;
	// System.Runtime.Remoting.Messaging.CADArgHolder System.Runtime.Remoting.Messaging.CADMethodReturnMessage::_exception
	CADArgHolder_t1_921 * ____exception_5;
};
