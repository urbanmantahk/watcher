﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_MonoTODOAttribute.h"

// System.MonoNotSupportedAttribute
struct  MonoNotSupportedAttribute_t1_81  : public MonoTODOAttribute_t1_76
{
};
