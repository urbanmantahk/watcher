﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.X509.X520/OrganizationalUnitName
struct OrganizationalUnitName_t1_207;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Security.X509.X520/OrganizationalUnitName::.ctor()
extern "C" void OrganizationalUnitName__ctor_m1_2407 (OrganizationalUnitName_t1_207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
