﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.Object
struct Object_t6_5;
struct Object_t6_5_marshaled;

#include "mscorlib_System_ValueType.h"
#include "AssemblyU2DCSharp_VoxelBusters_DebugPRO_Internal_eConsoleLog.h"

// VoxelBusters.DebugPRO.Internal.ConsoleLog
struct  ConsoleLog_t8_170 
{
	// System.Int32 VoxelBusters.DebugPRO.Internal.ConsoleLog::m_ID
	int32_t ___m_ID_0;
	// System.Int32 VoxelBusters.DebugPRO.Internal.ConsoleLog::m_tagID
	int32_t ___m_tagID_1;
	// System.String VoxelBusters.DebugPRO.Internal.ConsoleLog::m_message
	String_t* ___m_message_2;
	// VoxelBusters.DebugPRO.Internal.eConsoleLogType VoxelBusters.DebugPRO.Internal.ConsoleLog::m_type
	int32_t ___m_type_3;
	// System.String VoxelBusters.DebugPRO.Internal.ConsoleLog::m_stackTrace
	String_t* ___m_stackTrace_4;
	// System.String VoxelBusters.DebugPRO.Internal.ConsoleLog::m_description
	String_t* ___m_description_5;
	// System.String VoxelBusters.DebugPRO.Internal.ConsoleLog::m_callerFileName
	String_t* ___m_callerFileName_6;
	// System.Int32 VoxelBusters.DebugPRO.Internal.ConsoleLog::m_callerFileLineNumber
	int32_t ___m_callerFileLineNumber_7;
	// UnityEngine.Object VoxelBusters.DebugPRO.Internal.ConsoleLog::<Context>k__BackingField
	Object_t6_5 * ___U3CContextU3Ek__BackingField_8;
};
// Native definition for marshalling of: VoxelBusters.DebugPRO.Internal.ConsoleLog
struct Object_t6_5_marshaled;
struct ConsoleLog_t8_170_marshaled
{
	int32_t ___m_ID_0;
	int32_t ___m_tagID_1;
	char* ___m_message_2;
	int32_t ___m_type_3;
	char* ___m_stackTrace_4;
	char* ___m_description_5;
	char* ___m_callerFileName_6;
	int32_t ___m_callerFileLineNumber_7;
	Object_t6_5_marshaled* ___U3CContextU3Ek__BackingField_8;
};
