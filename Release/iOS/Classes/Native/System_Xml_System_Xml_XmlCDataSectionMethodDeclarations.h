﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XmlCDataSection
struct XmlCDataSection_t4_124;
// System.String
struct String_t;
// System.Xml.XmlDocument
struct XmlDocument_t4_123;
// System.Xml.XmlNode
struct XmlNode_t4_116;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlNodeType.h"

// System.Void System.Xml.XmlCDataSection::.ctor(System.String,System.Xml.XmlDocument)
extern "C" void XmlCDataSection__ctor_m4_350 (XmlCDataSection_t4_124 * __this, String_t* ___data, XmlDocument_t4_123 * ___doc, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XmlCDataSection::get_LocalName()
extern "C" String_t* XmlCDataSection_get_LocalName_m4_351 (XmlCDataSection_t4_124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XmlCDataSection::get_Name()
extern "C" String_t* XmlCDataSection_get_Name_m4_352 (XmlCDataSection_t4_124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNodeType System.Xml.XmlCDataSection::get_NodeType()
extern "C" int32_t XmlCDataSection_get_NodeType_m4_353 (XmlCDataSection_t4_124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNode System.Xml.XmlCDataSection::get_ParentNode()
extern "C" XmlNode_t4_116 * XmlCDataSection_get_ParentNode_m4_354 (XmlCDataSection_t4_124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNode System.Xml.XmlCDataSection::CloneNode(System.Boolean)
extern "C" XmlNode_t4_116 * XmlCDataSection_CloneNode_m4_355 (XmlCDataSection_t4_124 * __this, bool ___deep, const MethodInfo* method) IL2CPP_METHOD_ATTR;
