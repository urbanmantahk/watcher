﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Principal.WindowsPrincipal
struct WindowsPrincipal_t1_1386;
// System.Security.Principal.WindowsIdentity
struct WindowsIdentity_t1_1384;
// System.Security.Principal.IIdentity
struct IIdentity_t1_1374;
// System.String
struct String_t;
// System.Security.Principal.SecurityIdentifier
struct SecurityIdentifier_t1_1132;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Principal_WindowsBuiltInRole.h"
#include "mscorlib_System_IntPtr.h"

// System.Void System.Security.Principal.WindowsPrincipal::.ctor(System.Security.Principal.WindowsIdentity)
extern "C" void WindowsPrincipal__ctor_m1_11889 (WindowsPrincipal_t1_1386 * __this, WindowsIdentity_t1_1384 * ___ntIdentity, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Principal.IIdentity System.Security.Principal.WindowsPrincipal::get_Identity()
extern "C" Object_t * WindowsPrincipal_get_Identity_m1_11890 (WindowsPrincipal_t1_1386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Principal.WindowsPrincipal::IsInRole(System.Int32)
extern "C" bool WindowsPrincipal_IsInRole_m1_11891 (WindowsPrincipal_t1_1386 * __this, int32_t ___rid, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Principal.WindowsPrincipal::IsInRole(System.String)
extern "C" bool WindowsPrincipal_IsInRole_m1_11892 (WindowsPrincipal_t1_1386 * __this, String_t* ___role, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Principal.WindowsPrincipal::IsInRole(System.Security.Principal.WindowsBuiltInRole)
extern "C" bool WindowsPrincipal_IsInRole_m1_11893 (WindowsPrincipal_t1_1386 * __this, int32_t ___role, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Principal.WindowsPrincipal::IsInRole(System.Security.Principal.SecurityIdentifier)
extern "C" bool WindowsPrincipal_IsInRole_m1_11894 (WindowsPrincipal_t1_1386 * __this, SecurityIdentifier_t1_1132 * ___sid, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Principal.WindowsPrincipal::get_IsPosix()
extern "C" bool WindowsPrincipal_get_IsPosix_m1_11895 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Security.Principal.WindowsPrincipal::get_Token()
extern "C" IntPtr_t WindowsPrincipal_get_Token_m1_11896 (WindowsPrincipal_t1_1386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Principal.WindowsPrincipal::IsMemberOfGroupId(System.IntPtr,System.IntPtr)
extern "C" bool WindowsPrincipal_IsMemberOfGroupId_m1_11897 (Object_t * __this /* static, unused */, IntPtr_t ___user, IntPtr_t ___group, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Principal.WindowsPrincipal::IsMemberOfGroupName(System.IntPtr,System.String)
extern "C" bool WindowsPrincipal_IsMemberOfGroupName_m1_11898 (Object_t * __this /* static, unused */, IntPtr_t ___user, String_t* ___group, const MethodInfo* method) IL2CPP_METHOD_ATTR;
