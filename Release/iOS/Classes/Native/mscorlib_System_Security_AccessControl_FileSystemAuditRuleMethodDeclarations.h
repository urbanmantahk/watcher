﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.AccessControl.FileSystemAuditRule
struct FileSystemAuditRule_t1_1155;
// System.Security.Principal.IdentityReference
struct IdentityReference_t1_1120;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_AccessControl_FileSystemRights.h"
#include "mscorlib_System_Security_AccessControl_AuditFlags.h"
#include "mscorlib_System_Security_AccessControl_InheritanceFlags.h"
#include "mscorlib_System_Security_AccessControl_PropagationFlags.h"

// System.Void System.Security.AccessControl.FileSystemAuditRule::.ctor(System.Security.Principal.IdentityReference,System.Security.AccessControl.FileSystemRights,System.Security.AccessControl.AuditFlags)
extern "C" void FileSystemAuditRule__ctor_m1_9875 (FileSystemAuditRule_t1_1155 * __this, IdentityReference_t1_1120 * ___identity, int32_t ___fileSystemRights, int32_t ___flags, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.FileSystemAuditRule::.ctor(System.String,System.Security.AccessControl.FileSystemRights,System.Security.AccessControl.AuditFlags)
extern "C" void FileSystemAuditRule__ctor_m1_9876 (FileSystemAuditRule_t1_1155 * __this, String_t* ___identity, int32_t ___fileSystemRights, int32_t ___flags, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.FileSystemAuditRule::.ctor(System.Security.Principal.IdentityReference,System.Security.AccessControl.FileSystemRights,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.AuditFlags)
extern "C" void FileSystemAuditRule__ctor_m1_9877 (FileSystemAuditRule_t1_1155 * __this, IdentityReference_t1_1120 * ___identity, int32_t ___fileSystemRights, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___flags, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.FileSystemAuditRule::.ctor(System.String,System.Security.AccessControl.FileSystemRights,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.AuditFlags)
extern "C" void FileSystemAuditRule__ctor_m1_9878 (FileSystemAuditRule_t1_1155 * __this, String_t* ___identity, int32_t ___fileSystemRights, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___flags, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.AccessControl.FileSystemRights System.Security.AccessControl.FileSystemAuditRule::get_FileSystemRights()
extern "C" int32_t FileSystemAuditRule_get_FileSystemRights_m1_9879 (FileSystemAuditRule_t1_1155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
