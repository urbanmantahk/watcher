﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_184.h"
#include "AssemblyU2DCSharp_VoxelBusters_DebugPRO_Internal_ConsoleLog.h"

// System.Void System.Array/InternalEnumerator`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_27192_gshared (InternalEnumerator_1_t1_2720 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1_27192(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1_2720 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m1_27192_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_27193_gshared (InternalEnumerator_1_t1_2720 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_27193(__this, method) (( void (*) (InternalEnumerator_1_t1_2720 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_27193_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_27194_gshared (InternalEnumerator_1_t1_2720 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_27194(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1_2720 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_27194_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_27195_gshared (InternalEnumerator_1_t1_2720 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1_27195(__this, method) (( void (*) (InternalEnumerator_1_t1_2720 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1_27195_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_27196_gshared (InternalEnumerator_1_t1_2720 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1_27196(__this, method) (( bool (*) (InternalEnumerator_1_t1_2720 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1_27196_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::get_Current()
extern "C" ConsoleLog_t8_170  InternalEnumerator_1_get_Current_m1_27197_gshared (InternalEnumerator_1_t1_2720 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1_27197(__this, method) (( ConsoleLog_t8_170  (*) (InternalEnumerator_1_t1_2720 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1_27197_gshared)(__this, method)
