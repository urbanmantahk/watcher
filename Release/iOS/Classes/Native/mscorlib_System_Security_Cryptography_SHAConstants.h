﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.UInt32[]
struct UInt32U5BU5D_t1_142;
// System.UInt64[]
struct UInt64U5BU5D_t1_1256;

#include "mscorlib_System_Object.h"

// System.Security.Cryptography.SHAConstants
struct  SHAConstants_t1_1259  : public Object_t
{
};
struct SHAConstants_t1_1259_StaticFields{
	// System.UInt32[] System.Security.Cryptography.SHAConstants::K1
	UInt32U5BU5D_t1_142* ___K1_0;
	// System.UInt64[] System.Security.Cryptography.SHAConstants::K2
	UInt64U5BU5D_t1_1256* ___K2_1;
};
