﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Object
struct Object_t;
// System.Security.IPermission
struct IPermission_t1_1400;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Security.Policy.Evidence
struct Evidence_t1_398;
// System.Reflection.AssemblyName
struct AssemblyName_t1_576;

#include "mscorlib_System_SystemException.h"
#include "mscorlib_System_Security_Permissions_SecurityAction.h"
#include "mscorlib_System_Security_SecurityZone.h"

// System.Security.SecurityException
struct  SecurityException_t1_1399  : public SystemException_t1_250
{
	// System.String System.Security.SecurityException::permissionState
	String_t* ___permissionState_12;
	// System.Type System.Security.SecurityException::permissionType
	Type_t * ___permissionType_13;
	// System.String System.Security.SecurityException::_granted
	String_t* ____granted_14;
	// System.String System.Security.SecurityException::_refused
	String_t* ____refused_15;
	// System.Object System.Security.SecurityException::_demanded
	Object_t * ____demanded_16;
	// System.Security.IPermission System.Security.SecurityException::_firstperm
	Object_t * ____firstperm_17;
	// System.Reflection.MethodInfo System.Security.SecurityException::_method
	MethodInfo_t * ____method_18;
	// System.Security.Policy.Evidence System.Security.SecurityException::_evidence
	Evidence_t1_398 * ____evidence_19;
	// System.Security.Permissions.SecurityAction System.Security.SecurityException::_action
	int32_t ____action_20;
	// System.Object System.Security.SecurityException::_denyset
	Object_t * ____denyset_21;
	// System.Object System.Security.SecurityException::_permitset
	Object_t * ____permitset_22;
	// System.Reflection.AssemblyName System.Security.SecurityException::_assembly
	AssemblyName_t1_576 * ____assembly_23;
	// System.String System.Security.SecurityException::_url
	String_t* ____url_24;
	// System.Security.SecurityZone System.Security.SecurityException::_zone
	int32_t ____zone_25;
};
