﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object.h"

// System.Xml.XmlNameEntry
struct  XmlNameEntry_t4_117  : public Object_t
{
	// System.String System.Xml.XmlNameEntry::Prefix
	String_t* ___Prefix_0;
	// System.String System.Xml.XmlNameEntry::LocalName
	String_t* ___LocalName_1;
	// System.String System.Xml.XmlNameEntry::NS
	String_t* ___NS_2;
	// System.Int32 System.Xml.XmlNameEntry::Hash
	int32_t ___Hash_3;
	// System.String System.Xml.XmlNameEntry::prefixed_name_cache
	String_t* ___prefixed_name_cache_4;
};
