﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct ICollection_1_t1_2014;

#include "mscorlib_System_Object.h"

// System.Collections.Generic.CollectionDebuggerView`2<System.Object,System.Object>
struct  CollectionDebuggerView_2_t1_2013  : public Object_t
{
	// System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<T,U>> System.Collections.Generic.CollectionDebuggerView`2::c
	Object_t* ___c_0;
};
