﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object.h"

// VoxelBusters.NativePlugins.TwitterSession
struct  TwitterSession_t8_295  : public Object_t
{
	// System.String VoxelBusters.NativePlugins.TwitterSession::<AuthToken>k__BackingField
	String_t* ___U3CAuthTokenU3Ek__BackingField_0;
	// System.String VoxelBusters.NativePlugins.TwitterSession::<AuthTokenSecret>k__BackingField
	String_t* ___U3CAuthTokenSecretU3Ek__BackingField_1;
	// System.String VoxelBusters.NativePlugins.TwitterSession::<UserName>k__BackingField
	String_t* ___U3CUserNameU3Ek__BackingField_2;
	// System.String VoxelBusters.NativePlugins.TwitterSession::<UserID>k__BackingField
	String_t* ___U3CUserIDU3Ek__BackingField_3;
};
