﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.AccessControl.ObjectAce
struct ObjectAce_t1_1164;
// System.Security.Principal.SecurityIdentifier
struct SecurityIdentifier_t1_1132;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_AccessControl_AceFlags.h"
#include "mscorlib_System_Security_AccessControl_AceQualifier.h"
#include "mscorlib_System_Security_AccessControl_ObjectAceFlags.h"
#include "mscorlib_System_Guid.h"

// System.Void System.Security.AccessControl.ObjectAce::.ctor(System.Security.AccessControl.AceFlags,System.Security.AccessControl.AceQualifier,System.Int32,System.Security.Principal.SecurityIdentifier,System.Security.AccessControl.ObjectAceFlags,System.Guid,System.Guid,System.Boolean,System.Byte[])
extern "C" void ObjectAce__ctor_m1_9974 (ObjectAce_t1_1164 * __this, uint8_t ___aceFlags, int32_t ___qualifier, int32_t ___accessMask, SecurityIdentifier_t1_1132 * ___sid, int32_t ___flags, Guid_t1_319  ___type, Guid_t1_319  ___inheritedType, bool ___isCallback, ByteU5BU5D_t1_109* ___opaque, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.AccessControl.ObjectAce::get_BinaryLength()
extern "C" int32_t ObjectAce_get_BinaryLength_m1_9975 (ObjectAce_t1_1164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Guid System.Security.AccessControl.ObjectAce::get_InheritedObjectAceType()
extern "C" Guid_t1_319  ObjectAce_get_InheritedObjectAceType_m1_9976 (ObjectAce_t1_1164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.ObjectAce::set_InheritedObjectAceType(System.Guid)
extern "C" void ObjectAce_set_InheritedObjectAceType_m1_9977 (ObjectAce_t1_1164 * __this, Guid_t1_319  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.AccessControl.ObjectAceFlags System.Security.AccessControl.ObjectAce::get_ObjectAceFlags()
extern "C" int32_t ObjectAce_get_ObjectAceFlags_m1_9978 (ObjectAce_t1_1164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.ObjectAce::set_ObjectAceFlags(System.Security.AccessControl.ObjectAceFlags)
extern "C" void ObjectAce_set_ObjectAceFlags_m1_9979 (ObjectAce_t1_1164 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Guid System.Security.AccessControl.ObjectAce::get_ObjectAceType()
extern "C" Guid_t1_319  ObjectAce_get_ObjectAceType_m1_9980 (ObjectAce_t1_1164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.ObjectAce::set_ObjectAceType(System.Guid)
extern "C" void ObjectAce_set_ObjectAceType_m1_9981 (ObjectAce_t1_1164 * __this, Guid_t1_319  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.ObjectAce::GetBinaryForm(System.Byte[],System.Int32)
extern "C" void ObjectAce_GetBinaryForm_m1_9982 (ObjectAce_t1_1164 * __this, ByteU5BU5D_t1_109* ___binaryForm, int32_t ___offset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.AccessControl.ObjectAce::MaxOpaqueLength(System.Boolean)
extern "C" int32_t ObjectAce_MaxOpaqueLength_m1_9983 (Object_t * __this /* static, unused */, bool ___isCallback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
