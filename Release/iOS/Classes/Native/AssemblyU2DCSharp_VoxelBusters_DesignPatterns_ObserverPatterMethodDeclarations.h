﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.DesignPatterns.ObserverPattern`1<System.Object>
struct ObserverPattern_1_t8_371;
// VoxelBusters.DesignPatterns.IObserver
struct IObserver_t8_374;
// System.String
struct String_t;
// System.Collections.ArrayList
struct ArrayList_t1_170;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.DesignPatterns.ObserverPattern`1<System.Object>::.ctor()
extern "C" void ObserverPattern_1__ctor_m8_2028_gshared (ObserverPattern_1_t8_371 * __this, const MethodInfo* method);
#define ObserverPattern_1__ctor_m8_2028(__this, method) (( void (*) (ObserverPattern_1_t8_371 *, const MethodInfo*))ObserverPattern_1__ctor_m8_2028_gshared)(__this, method)
// System.Void VoxelBusters.DesignPatterns.ObserverPattern`1<System.Object>::AddObserver(VoxelBusters.DesignPatterns.IObserver)
extern "C" void ObserverPattern_1_AddObserver_m8_2029_gshared (ObserverPattern_1_t8_371 * __this, Object_t * ____observer, const MethodInfo* method);
#define ObserverPattern_1_AddObserver_m8_2029(__this, ____observer, method) (( void (*) (ObserverPattern_1_t8_371 *, Object_t *, const MethodInfo*))ObserverPattern_1_AddObserver_m8_2029_gshared)(__this, ____observer, method)
// System.Void VoxelBusters.DesignPatterns.ObserverPattern`1<System.Object>::RemoveObserver(VoxelBusters.DesignPatterns.IObserver)
extern "C" void ObserverPattern_1_RemoveObserver_m8_2030_gshared (ObserverPattern_1_t8_371 * __this, Object_t * ____observer, const MethodInfo* method);
#define ObserverPattern_1_RemoveObserver_m8_2030(__this, ____observer, method) (( void (*) (ObserverPattern_1_t8_371 *, Object_t *, const MethodInfo*))ObserverPattern_1_RemoveObserver_m8_2030_gshared)(__this, ____observer, method)
// System.Void VoxelBusters.DesignPatterns.ObserverPattern`1<System.Object>::NotifyObservers(System.String,System.Collections.ArrayList)
extern "C" void ObserverPattern_1_NotifyObservers_m8_2031_gshared (ObserverPattern_1_t8_371 * __this, String_t* ____key, ArrayList_t1_170 * ____data, const MethodInfo* method);
#define ObserverPattern_1_NotifyObservers_m8_2031(__this, ____key, ____data, method) (( void (*) (ObserverPattern_1_t8_371 *, String_t*, ArrayList_t1_170 *, const MethodInfo*))ObserverPattern_1_NotifyObservers_m8_2031_gshared)(__this, ____key, ____data, method)
