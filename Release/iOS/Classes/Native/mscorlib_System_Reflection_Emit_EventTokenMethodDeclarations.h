﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_Emit_EventToken.h"

// System.Void System.Reflection.Emit.EventToken::.ctor(System.Int32)
extern "C" void EventToken__ctor_m1_5823 (EventToken_t1_504 * __this, int32_t ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.EventToken::.cctor()
extern "C" void EventToken__cctor_m1_5824 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.EventToken::Equals(System.Object)
extern "C" bool EventToken_Equals_m1_5825 (EventToken_t1_504 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.EventToken::Equals(System.Reflection.Emit.EventToken)
extern "C" bool EventToken_Equals_m1_5826 (EventToken_t1_504 * __this, EventToken_t1_504  ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.EventToken::GetHashCode()
extern "C" int32_t EventToken_GetHashCode_m1_5827 (EventToken_t1_504 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.EventToken::get_Token()
extern "C" int32_t EventToken_get_Token_m1_5828 (EventToken_t1_504 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.EventToken::op_Equality(System.Reflection.Emit.EventToken,System.Reflection.Emit.EventToken)
extern "C" bool EventToken_op_Equality_m1_5829 (Object_t * __this /* static, unused */, EventToken_t1_504  ___a, EventToken_t1_504  ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.EventToken::op_Inequality(System.Reflection.Emit.EventToken,System.Reflection.Emit.EventToken)
extern "C" bool EventToken_op_Inequality_m1_5830 (Object_t * __this /* static, unused */, EventToken_t1_504  ___a, EventToken_t1_504  ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
