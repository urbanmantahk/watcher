﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.AccessControl.NativeObjectSecurity/ExceptionFromErrorCode
struct ExceptionFromErrorCode_t1_1162;
// System.Object
struct Object_t;
// System.Exception
struct Exception_t1_33;
// System.String
struct String_t;
// System.Runtime.InteropServices.SafeHandle
struct SafeHandle_t1_88;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_String.h"
#include "mscorlib_System_Runtime_InteropServices_SafeHandleMethodDeclarations.h"

// System.Void System.Security.AccessControl.NativeObjectSecurity/ExceptionFromErrorCode::.ctor(System.Object,System.IntPtr)
extern "C" void ExceptionFromErrorCode__ctor_m1_9955 (ExceptionFromErrorCode_t1_1162 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception System.Security.AccessControl.NativeObjectSecurity/ExceptionFromErrorCode::Invoke(System.Int32,System.String,System.Runtime.InteropServices.SafeHandle,System.Object)
extern "C" Exception_t1_33 * ExceptionFromErrorCode_Invoke_m1_9956 (ExceptionFromErrorCode_t1_1162 * __this, int32_t ___errorCode, String_t* ___name, SafeHandle_t1_88 * ___handle, Object_t * ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" Exception_t1_33 * pinvoke_delegate_wrapper_ExceptionFromErrorCode_t1_1162(Il2CppObject* delegate, int32_t ___errorCode, String_t* ___name, SafeHandle_t1_88 * ___handle, Object_t * ___context);
// System.IAsyncResult System.Security.AccessControl.NativeObjectSecurity/ExceptionFromErrorCode::BeginInvoke(System.Int32,System.String,System.Runtime.InteropServices.SafeHandle,System.Object,System.AsyncCallback,System.Object)
extern "C" Object_t * ExceptionFromErrorCode_BeginInvoke_m1_9957 (ExceptionFromErrorCode_t1_1162 * __this, int32_t ___errorCode, String_t* ___name, SafeHandle_t1_88 * ___handle, Object_t * ___context, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception System.Security.AccessControl.NativeObjectSecurity/ExceptionFromErrorCode::EndInvoke(System.IAsyncResult)
extern "C" Exception_t1_33 * ExceptionFromErrorCode_EndInvoke_m1_9958 (ExceptionFromErrorCode_t1_1162 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
