﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object.h"

// System.Security.Policy.SiteMembershipCondition
struct  SiteMembershipCondition_t1_1362  : public Object_t
{
	// System.Int32 System.Security.Policy.SiteMembershipCondition::version
	int32_t ___version_0;
	// System.String System.Security.Policy.SiteMembershipCondition::_site
	String_t* ____site_1;
};
