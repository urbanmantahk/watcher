﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_IO_SearchOption.h"

// System.IO.SearchOption
struct  SearchOption_t1_439 
{
	// System.Int32 System.IO.SearchOption::value__
	int32_t ___value___1;
};
