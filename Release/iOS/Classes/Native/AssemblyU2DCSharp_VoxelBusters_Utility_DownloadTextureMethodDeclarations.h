﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.Utility.DownloadTexture
struct DownloadTexture_t8_162;
// VoxelBusters.Utility.DownloadTexture/Completion
struct Completion_t8_161;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_URL.h"

// System.Void VoxelBusters.Utility.DownloadTexture::.ctor(VoxelBusters.Utility.URL,System.Boolean,System.Boolean)
extern "C" void DownloadTexture__ctor_m8_929 (DownloadTexture_t8_162 * __this, URL_t8_156  ____URL, bool ____isAsynchronous, bool ____autoFixOrientation, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.Utility.DownloadTexture::get_AutoFixOrientation()
extern "C" bool DownloadTexture_get_AutoFixOrientation_m8_930 (DownloadTexture_t8_162 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.DownloadTexture::set_AutoFixOrientation(System.Boolean)
extern "C" void DownloadTexture_set_AutoFixOrientation_m8_931 (DownloadTexture_t8_162 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single VoxelBusters.Utility.DownloadTexture::get_ScaleFactor()
extern "C" float DownloadTexture_get_ScaleFactor_m8_932 (DownloadTexture_t8_162 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.DownloadTexture::set_ScaleFactor(System.Single)
extern "C" void DownloadTexture_set_ScaleFactor_m8_933 (DownloadTexture_t8_162 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.Utility.DownloadTexture/Completion VoxelBusters.Utility.DownloadTexture::get_OnCompletion()
extern "C" Completion_t8_161 * DownloadTexture_get_OnCompletion_m8_934 (DownloadTexture_t8_162 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.DownloadTexture::set_OnCompletion(VoxelBusters.Utility.DownloadTexture/Completion)
extern "C" void DownloadTexture_set_OnCompletion_m8_935 (DownloadTexture_t8_162 * __this, Completion_t8_161 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.DownloadTexture::DidFailStartRequestWithError(System.String)
extern "C" void DownloadTexture_DidFailStartRequestWithError_m8_936 (DownloadTexture_t8_162 * __this, String_t* ____error, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.DownloadTexture::OnFetchingResponse()
extern "C" void DownloadTexture_OnFetchingResponse_m8_937 (DownloadTexture_t8_162 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
