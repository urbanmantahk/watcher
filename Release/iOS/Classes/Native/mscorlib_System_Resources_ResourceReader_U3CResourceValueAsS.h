﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object.h"
#include "mscorlib_System_IntPtr.h"

// System.Resources.ResourceReader/<ResourceValueAsStream>c__AnonStorey2
struct  U3CResourceValueAsStreamU3Ec__AnonStorey2_t1_651  : public Object_t
{
	// System.IntPtr System.Resources.ResourceReader/<ResourceValueAsStream>c__AnonStorey2::ptr
	IntPtr_t ___ptr_0;
};
