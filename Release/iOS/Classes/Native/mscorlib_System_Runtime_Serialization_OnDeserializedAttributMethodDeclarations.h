﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Serialization.OnDeserializedAttribute
struct OnDeserializedAttribute_t1_1086;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Serialization.OnDeserializedAttribute::.ctor()
extern "C" void OnDeserializedAttribute__ctor_m1_9583 (OnDeserializedAttribute_t1_1086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
