﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Attribute.h"

// System.Runtime.InteropServices.PrimaryInteropAssemblyAttribute
struct  PrimaryInteropAssemblyAttribute_t1_815  : public Attribute_t1_2
{
	// System.Int32 System.Runtime.InteropServices.PrimaryInteropAssemblyAttribute::major
	int32_t ___major_0;
	// System.Int32 System.Runtime.InteropServices.PrimaryInteropAssemblyAttribute::minor
	int32_t ___minor_1;
};
