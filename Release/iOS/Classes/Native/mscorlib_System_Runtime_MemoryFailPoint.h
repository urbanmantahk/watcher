﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Runtime_ConstrainedExecution_CriticalFinaliz.h"

// System.Runtime.MemoryFailPoint
struct  MemoryFailPoint_t1_1106  : public CriticalFinalizerObject_t1_713
{
};
