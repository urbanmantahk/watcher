﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.Security.AccessControl.GenericAce>
struct List_1_t1_1125;

#include "mscorlib_System_Security_AccessControl_GenericAcl.h"

// System.Security.AccessControl.CommonAcl
struct  CommonAcl_t1_1124  : public GenericAcl_t1_1114
{
	// System.Boolean System.Security.AccessControl.CommonAcl::is_container
	bool ___is_container_4;
	// System.Boolean System.Security.AccessControl.CommonAcl::is_ds
	bool ___is_ds_5;
	// System.Byte System.Security.AccessControl.CommonAcl::revision
	uint8_t ___revision_6;
	// System.Collections.Generic.List`1<System.Security.AccessControl.GenericAce> System.Security.AccessControl.CommonAcl::list
	List_1_t1_1125 * ___list_7;
};
