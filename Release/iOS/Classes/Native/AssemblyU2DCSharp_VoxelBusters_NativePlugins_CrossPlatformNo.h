﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object.h"

// VoxelBusters.NativePlugins.CrossPlatformNotification/AndroidSpecificProperties
struct  AndroidSpecificProperties_t8_265  : public Object_t
{
	// System.String VoxelBusters.NativePlugins.CrossPlatformNotification/AndroidSpecificProperties::<ContentTitle>k__BackingField
	String_t* ___U3CContentTitleU3Ek__BackingField_4;
	// System.String VoxelBusters.NativePlugins.CrossPlatformNotification/AndroidSpecificProperties::<TickerText>k__BackingField
	String_t* ___U3CTickerTextU3Ek__BackingField_5;
	// System.String VoxelBusters.NativePlugins.CrossPlatformNotification/AndroidSpecificProperties::<Tag>k__BackingField
	String_t* ___U3CTagU3Ek__BackingField_6;
	// System.String VoxelBusters.NativePlugins.CrossPlatformNotification/AndroidSpecificProperties::<CustomSound>k__BackingField
	String_t* ___U3CCustomSoundU3Ek__BackingField_7;
	// System.String VoxelBusters.NativePlugins.CrossPlatformNotification/AndroidSpecificProperties::<LargeIcon>k__BackingField
	String_t* ___U3CLargeIconU3Ek__BackingField_8;
};
