﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"

extern const Il2CppType Object_t_0_0_0;
static const Il2CppType* GenInst_Object_t_0_0_0_Types[] = { &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0 = { 1, GenInst_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0 = { 2, GenInst_Object_t_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &Object_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0 = { 3, GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &Object_t_0_0_0, &Object_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0 = { 4, GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types };
extern const Il2CppType String_t_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Types[] = { &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0 = { 1, GenInst_String_t_0_0_0_Types };
extern const Il2CppType Int32_t1_3_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Int32_t1_3_0_0_0_Types[] = { &String_t_0_0_0, &Int32_t1_3_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t1_3_0_0_0 = { 2, GenInst_String_t_0_0_0_Int32_t1_3_0_0_0_Types };
extern const Il2CppType DateTime_t1_150_0_0_0;
static const Il2CppType* GenInst_DateTime_t1_150_0_0_0_Types[] = { &DateTime_t1_150_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTime_t1_150_0_0_0 = { 1, GenInst_DateTime_t1_150_0_0_0_Types };
extern const Il2CppType Type_t_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_Types[] = { &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0 = { 1, GenInst_Type_t_0_0_0_Types };
extern const Il2CppType CustomAttributeTypedArgument_t1_594_0_0_0;
static const Il2CppType* GenInst_CustomAttributeTypedArgument_t1_594_0_0_0_Types[] = { &CustomAttributeTypedArgument_t1_594_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeTypedArgument_t1_594_0_0_0 = { 1, GenInst_CustomAttributeTypedArgument_t1_594_0_0_0_Types };
extern const Il2CppType CustomAttributeNamedArgument_t1_593_0_0_0;
static const Il2CppType* GenInst_CustomAttributeNamedArgument_t1_593_0_0_0_Types[] = { &CustomAttributeNamedArgument_t1_593_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeNamedArgument_t1_593_0_0_0 = { 1, GenInst_CustomAttributeNamedArgument_t1_593_0_0_0_Types };
extern const Il2CppType ExceptionHandlingClause_t1_597_0_0_0;
static const Il2CppType* GenInst_ExceptionHandlingClause_t1_597_0_0_0_Types[] = { &ExceptionHandlingClause_t1_597_0_0_0 };
extern const Il2CppGenericInst GenInst_ExceptionHandlingClause_t1_597_0_0_0 = { 1, GenInst_ExceptionHandlingClause_t1_597_0_0_0_Types };
extern const Il2CppType LocalVariableInfo_t1_528_0_0_0;
static const Il2CppType* GenInst_LocalVariableInfo_t1_528_0_0_0_Types[] = { &LocalVariableInfo_t1_528_0_0_0 };
extern const Il2CppGenericInst GenInst_LocalVariableInfo_t1_528_0_0_0 = { 1, GenInst_LocalVariableInfo_t1_528_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1_3_0_0_0_Int32_t1_3_0_0_0_Types[] = { &Int32_t1_3_0_0_0, &Int32_t1_3_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1_3_0_0_0_Int32_t1_3_0_0_0 = { 2, GenInst_Int32_t1_3_0_0_0_Int32_t1_3_0_0_0_Types };
extern const Il2CppType GenericAce_t1_1145_0_0_0;
static const Il2CppType* GenInst_GenericAce_t1_1145_0_0_0_Types[] = { &GenericAce_t1_1145_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericAce_t1_1145_0_0_0 = { 1, GenInst_GenericAce_t1_1145_0_0_0_Types };
extern const Il2CppType AccessRule_t1_1111_0_0_0;
static const Il2CppType* GenInst_AccessRule_t1_1111_0_0_0_Types[] = { &AccessRule_t1_1111_0_0_0 };
extern const Il2CppGenericInst GenInst_AccessRule_t1_1111_0_0_0 = { 1, GenInst_AccessRule_t1_1111_0_0_0_Types };
extern const Il2CppType AuditRule_t1_1119_0_0_0;
static const Il2CppType* GenInst_AuditRule_t1_1119_0_0_0_Types[] = { &AuditRule_t1_1119_0_0_0 };
extern const Il2CppGenericInst GenInst_AuditRule_t1_1119_0_0_0 = { 1, GenInst_AuditRule_t1_1119_0_0_0_Types };
extern const Il2CppType StrongName_t1_1363_0_0_0;
static const Il2CppType* GenInst_StrongName_t1_1363_0_0_0_Types[] = { &StrongName_t1_1363_0_0_0 };
extern const Il2CppGenericInst GenInst_StrongName_t1_1363_0_0_0 = { 1, GenInst_StrongName_t1_1363_0_0_0_Types };
extern const Il2CppType CustomAttributeBuilder_t1_487_0_0_0;
static const Il2CppType* GenInst_CustomAttributeBuilder_t1_487_0_0_0_Types[] = { &CustomAttributeBuilder_t1_487_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeBuilder_t1_487_0_0_0 = { 1, GenInst_CustomAttributeBuilder_t1_487_0_0_0_Types };
extern const Il2CppType DateTimeOffset_t1_1527_0_0_0;
static const Il2CppType* GenInst_DateTimeOffset_t1_1527_0_0_0_Types[] = { &DateTimeOffset_t1_1527_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTimeOffset_t1_1527_0_0_0 = { 1, GenInst_DateTimeOffset_t1_1527_0_0_0_Types };
extern const Il2CppType TimeSpan_t1_368_0_0_0;
static const Il2CppType* GenInst_TimeSpan_t1_368_0_0_0_Types[] = { &TimeSpan_t1_368_0_0_0 };
extern const Il2CppGenericInst GenInst_TimeSpan_t1_368_0_0_0 = { 1, GenInst_TimeSpan_t1_368_0_0_0_Types };
extern const Il2CppType Guid_t1_319_0_0_0;
static const Il2CppType* GenInst_Guid_t1_319_0_0_0_Types[] = { &Guid_t1_319_0_0_0 };
extern const Il2CppGenericInst GenInst_Guid_t1_319_0_0_0 = { 1, GenInst_Guid_t1_319_0_0_0_Types };
extern const Il2CppType CustomAttributeData_t1_588_0_0_0;
static const Il2CppType* GenInst_CustomAttributeData_t1_588_0_0_0_Types[] = { &CustomAttributeData_t1_588_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeData_t1_588_0_0_0 = { 1, GenInst_CustomAttributeData_t1_588_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_1805_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_1805_0_0_0_Types[] = { &KeyValuePair_2_t1_1805_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_1805_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_1805_0_0_0_Types };
extern const Il2CppType DTDNode_t4_89_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_DTDNode_t4_89_0_0_0_Types[] = { &String_t_0_0_0, &DTDNode_t4_89_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_DTDNode_t4_89_0_0_0 = { 2, GenInst_String_t_0_0_0_DTDNode_t4_89_0_0_0_Types };
static const Il2CppType* GenInst_DTDNode_t4_89_0_0_0_Types[] = { &DTDNode_t4_89_0_0_0 };
extern const Il2CppGenericInst GenInst_DTDNode_t4_89_0_0_0 = { 1, GenInst_DTDNode_t4_89_0_0_0_Types };
extern const Il2CppType Char_t1_15_0_0_0;
static const Il2CppType* GenInst_Char_t1_15_0_0_0_Types[] = { &Char_t1_15_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t1_15_0_0_0 = { 1, GenInst_Char_t1_15_0_0_0_Types };
extern const Il2CppType Cookie_t3_81_0_0_0;
static const Il2CppType* GenInst_Cookie_t3_81_0_0_0_Types[] = { &Cookie_t3_81_0_0_0 };
extern const Il2CppGenericInst GenInst_Cookie_t3_81_0_0_0 = { 1, GenInst_Cookie_t3_81_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1_3_0_0_0_Types[] = { &Int32_t1_3_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1_3_0_0_0 = { 1, GenInst_Int32_t1_3_0_0_0_Types };
extern const Il2CppType Boolean_t1_20_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Boolean_t1_20_0_0_0_Types[] = { &String_t_0_0_0, &Boolean_t1_20_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t1_20_0_0_0 = { 2, GenInst_String_t_0_0_0_Boolean_t1_20_0_0_0_Types };
extern const Il2CppType GcLeaderboard_t6_21_0_0_0;
static const Il2CppType* GenInst_GcLeaderboard_t6_21_0_0_0_Types[] = { &GcLeaderboard_t6_21_0_0_0 };
extern const Il2CppGenericInst GenInst_GcLeaderboard_t6_21_0_0_0 = { 1, GenInst_GcLeaderboard_t6_21_0_0_0_Types };
extern const Il2CppType IAchievementDescriptionU5BU5D_t6_291_0_0_0;
static const Il2CppType* GenInst_IAchievementDescriptionU5BU5D_t6_291_0_0_0_Types[] = { &IAchievementDescriptionU5BU5D_t6_291_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementDescriptionU5BU5D_t6_291_0_0_0 = { 1, GenInst_IAchievementDescriptionU5BU5D_t6_291_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t1_20_0_0_0_Types[] = { &Boolean_t1_20_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t1_20_0_0_0 = { 1, GenInst_Boolean_t1_20_0_0_0_Types };
extern const Il2CppType IAchievementU5BU5D_t6_293_0_0_0;
static const Il2CppType* GenInst_IAchievementU5BU5D_t6_293_0_0_0_Types[] = { &IAchievementU5BU5D_t6_293_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementU5BU5D_t6_293_0_0_0 = { 1, GenInst_IAchievementU5BU5D_t6_293_0_0_0_Types };
extern const Il2CppType IScoreU5BU5D_t6_226_0_0_0;
static const Il2CppType* GenInst_IScoreU5BU5D_t6_226_0_0_0_Types[] = { &IScoreU5BU5D_t6_226_0_0_0 };
extern const Il2CppGenericInst GenInst_IScoreU5BU5D_t6_226_0_0_0 = { 1, GenInst_IScoreU5BU5D_t6_226_0_0_0_Types };
extern const Il2CppType IUserProfileU5BU5D_t6_222_0_0_0;
static const Il2CppType* GenInst_IUserProfileU5BU5D_t6_222_0_0_0_Types[] = { &IUserProfileU5BU5D_t6_222_0_0_0 };
extern const Il2CppGenericInst GenInst_IUserProfileU5BU5D_t6_222_0_0_0 = { 1, GenInst_IUserProfileU5BU5D_t6_222_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0 = { 2, GenInst_String_t_0_0_0_String_t_0_0_0_Types };
extern const Il2CppType ByteU5BU5D_t1_109_0_0_0;
static const Il2CppType* GenInst_ByteU5BU5D_t1_109_0_0_0_Types[] = { &ByteU5BU5D_t1_109_0_0_0 };
extern const Il2CppGenericInst GenInst_ByteU5BU5D_t1_109_0_0_0 = { 1, GenInst_ByteU5BU5D_t1_109_0_0_0_Types };
extern const Il2CppType Rigidbody2D_t6_120_0_0_0;
static const Il2CppType* GenInst_Rigidbody2D_t6_120_0_0_0_Types[] = { &Rigidbody2D_t6_120_0_0_0 };
extern const Il2CppGenericInst GenInst_Rigidbody2D_t6_120_0_0_0 = { 1, GenInst_Rigidbody2D_t6_120_0_0_0_Types };
extern const Il2CppType Font_t6_149_0_0_0;
static const Il2CppType* GenInst_Font_t6_149_0_0_0_Types[] = { &Font_t6_149_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t6_149_0_0_0 = { 1, GenInst_Font_t6_149_0_0_0_Types };
extern const Il2CppType UIVertex_t6_158_0_0_0;
static const Il2CppType* GenInst_UIVertex_t6_158_0_0_0_Types[] = { &UIVertex_t6_158_0_0_0 };
extern const Il2CppGenericInst GenInst_UIVertex_t6_158_0_0_0 = { 1, GenInst_UIVertex_t6_158_0_0_0_Types };
extern const Il2CppType UICharInfo_t6_150_0_0_0;
static const Il2CppType* GenInst_UICharInfo_t6_150_0_0_0_Types[] = { &UICharInfo_t6_150_0_0_0 };
extern const Il2CppGenericInst GenInst_UICharInfo_t6_150_0_0_0 = { 1, GenInst_UICharInfo_t6_150_0_0_0_Types };
extern const Il2CppType UILineInfo_t6_151_0_0_0;
static const Il2CppType* GenInst_UILineInfo_t6_151_0_0_0_Types[] = { &UILineInfo_t6_151_0_0_0 };
extern const Il2CppGenericInst GenInst_UILineInfo_t6_151_0_0_0 = { 1, GenInst_UILineInfo_t6_151_0_0_0_Types };
extern const Il2CppType LayoutCache_t6_173_0_0_0;
static const Il2CppType* GenInst_Int32_t1_3_0_0_0_LayoutCache_t6_173_0_0_0_Types[] = { &Int32_t1_3_0_0_0, &LayoutCache_t6_173_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1_3_0_0_0_LayoutCache_t6_173_0_0_0 = { 2, GenInst_Int32_t1_3_0_0_0_LayoutCache_t6_173_0_0_0_Types };
extern const Il2CppType GUILayoutEntry_t6_177_0_0_0;
static const Il2CppType* GenInst_GUILayoutEntry_t6_177_0_0_0_Types[] = { &GUILayoutEntry_t6_177_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayoutEntry_t6_177_0_0_0 = { 1, GenInst_GUILayoutEntry_t6_177_0_0_0_Types };
extern const Il2CppType GUIStyle_t6_176_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_GUIStyle_t6_176_0_0_0_Types[] = { &String_t_0_0_0, &GUIStyle_t6_176_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t6_176_0_0_0 = { 2, GenInst_String_t_0_0_0_GUIStyle_t6_176_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1_3_0_0_0_Object_t_0_0_0_Types[] = { &Int32_t1_3_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1_3_0_0_0_Object_t_0_0_0 = { 2, GenInst_Int32_t1_3_0_0_0_Object_t_0_0_0_Types };
extern const Il2CppType GUILayer_t6_31_0_0_0;
static const Il2CppType* GenInst_GUILayer_t6_31_0_0_0_Types[] = { &GUILayer_t6_31_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayer_t6_31_0_0_0 = { 1, GenInst_GUILayer_t6_31_0_0_0_Types };
extern const Il2CppType Event_t6_162_0_0_0;
extern const Il2CppType TextEditOp_t6_253_0_0_0;
static const Il2CppType* GenInst_Event_t6_162_0_0_0_TextEditOp_t6_253_0_0_0_Types[] = { &Event_t6_162_0_0_0, &TextEditOp_t6_253_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t6_162_0_0_0_TextEditOp_t6_253_0_0_0 = { 2, GenInst_Event_t6_162_0_0_0_TextEditOp_t6_253_0_0_0_Types };
extern const Il2CppType Single_t1_17_0_0_0;
static const Il2CppType* GenInst_Single_t1_17_0_0_0_Types[] = { &Single_t1_17_0_0_0 };
extern const Il2CppGenericInst GenInst_Single_t1_17_0_0_0 = { 1, GenInst_Single_t1_17_0_0_0_Types };
extern const Il2CppType PersistentCall_t6_261_0_0_0;
static const Il2CppType* GenInst_PersistentCall_t6_261_0_0_0_Types[] = { &PersistentCall_t6_261_0_0_0 };
extern const Il2CppGenericInst GenInst_PersistentCall_t6_261_0_0_0 = { 1, GenInst_PersistentCall_t6_261_0_0_0_Types };
extern const Il2CppType BaseInvokableCall_t6_257_0_0_0;
static const Il2CppType* GenInst_BaseInvokableCall_t6_257_0_0_0_Types[] = { &BaseInvokableCall_t6_257_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseInvokableCall_t6_257_0_0_0 = { 1, GenInst_BaseInvokableCall_t6_257_0_0_0_Types };
extern const Il2CppType BaseInputModule_t7_4_0_0_0;
static const Il2CppType* GenInst_BaseInputModule_t7_4_0_0_0_Types[] = { &BaseInputModule_t7_4_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseInputModule_t7_4_0_0_0 = { 1, GenInst_BaseInputModule_t7_4_0_0_0_Types };
extern const Il2CppType RaycastResult_t7_31_0_0_0;
static const Il2CppType* GenInst_RaycastResult_t7_31_0_0_0_Types[] = { &RaycastResult_t7_31_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastResult_t7_31_0_0_0 = { 1, GenInst_RaycastResult_t7_31_0_0_0_Types };
extern const Il2CppType IDeselectHandler_t7_172_0_0_0;
static const Il2CppType* GenInst_IDeselectHandler_t7_172_0_0_0_Types[] = { &IDeselectHandler_t7_172_0_0_0 };
extern const Il2CppGenericInst GenInst_IDeselectHandler_t7_172_0_0_0 = { 1, GenInst_IDeselectHandler_t7_172_0_0_0_Types };
extern const Il2CppType ISelectHandler_t7_171_0_0_0;
static const Il2CppType* GenInst_ISelectHandler_t7_171_0_0_0_Types[] = { &ISelectHandler_t7_171_0_0_0 };
extern const Il2CppGenericInst GenInst_ISelectHandler_t7_171_0_0_0 = { 1, GenInst_ISelectHandler_t7_171_0_0_0_Types };
extern const Il2CppType BaseEventData_t7_5_0_0_0;
static const Il2CppType* GenInst_BaseEventData_t7_5_0_0_0_Types[] = { &BaseEventData_t7_5_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseEventData_t7_5_0_0_0 = { 1, GenInst_BaseEventData_t7_5_0_0_0_Types };
extern const Il2CppType Entry_t7_7_0_0_0;
static const Il2CppType* GenInst_Entry_t7_7_0_0_0_Types[] = { &Entry_t7_7_0_0_0 };
extern const Il2CppGenericInst GenInst_Entry_t7_7_0_0_0 = { 1, GenInst_Entry_t7_7_0_0_0_Types };
extern const Il2CppType IPointerEnterHandler_t7_159_0_0_0;
static const Il2CppType* GenInst_IPointerEnterHandler_t7_159_0_0_0_Types[] = { &IPointerEnterHandler_t7_159_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerEnterHandler_t7_159_0_0_0 = { 1, GenInst_IPointerEnterHandler_t7_159_0_0_0_Types };
extern const Il2CppType IPointerExitHandler_t7_160_0_0_0;
static const Il2CppType* GenInst_IPointerExitHandler_t7_160_0_0_0_Types[] = { &IPointerExitHandler_t7_160_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerExitHandler_t7_160_0_0_0 = { 1, GenInst_IPointerExitHandler_t7_160_0_0_0_Types };
extern const Il2CppType IPointerDownHandler_t7_161_0_0_0;
static const Il2CppType* GenInst_IPointerDownHandler_t7_161_0_0_0_Types[] = { &IPointerDownHandler_t7_161_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerDownHandler_t7_161_0_0_0 = { 1, GenInst_IPointerDownHandler_t7_161_0_0_0_Types };
extern const Il2CppType IPointerUpHandler_t7_162_0_0_0;
static const Il2CppType* GenInst_IPointerUpHandler_t7_162_0_0_0_Types[] = { &IPointerUpHandler_t7_162_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerUpHandler_t7_162_0_0_0 = { 1, GenInst_IPointerUpHandler_t7_162_0_0_0_Types };
extern const Il2CppType IPointerClickHandler_t7_163_0_0_0;
static const Il2CppType* GenInst_IPointerClickHandler_t7_163_0_0_0_Types[] = { &IPointerClickHandler_t7_163_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerClickHandler_t7_163_0_0_0 = { 1, GenInst_IPointerClickHandler_t7_163_0_0_0_Types };
extern const Il2CppType IInitializePotentialDragHandler_t7_164_0_0_0;
static const Il2CppType* GenInst_IInitializePotentialDragHandler_t7_164_0_0_0_Types[] = { &IInitializePotentialDragHandler_t7_164_0_0_0 };
extern const Il2CppGenericInst GenInst_IInitializePotentialDragHandler_t7_164_0_0_0 = { 1, GenInst_IInitializePotentialDragHandler_t7_164_0_0_0_Types };
extern const Il2CppType IBeginDragHandler_t7_165_0_0_0;
static const Il2CppType* GenInst_IBeginDragHandler_t7_165_0_0_0_Types[] = { &IBeginDragHandler_t7_165_0_0_0 };
extern const Il2CppGenericInst GenInst_IBeginDragHandler_t7_165_0_0_0 = { 1, GenInst_IBeginDragHandler_t7_165_0_0_0_Types };
extern const Il2CppType IDragHandler_t7_166_0_0_0;
static const Il2CppType* GenInst_IDragHandler_t7_166_0_0_0_Types[] = { &IDragHandler_t7_166_0_0_0 };
extern const Il2CppGenericInst GenInst_IDragHandler_t7_166_0_0_0 = { 1, GenInst_IDragHandler_t7_166_0_0_0_Types };
extern const Il2CppType IEndDragHandler_t7_167_0_0_0;
static const Il2CppType* GenInst_IEndDragHandler_t7_167_0_0_0_Types[] = { &IEndDragHandler_t7_167_0_0_0 };
extern const Il2CppGenericInst GenInst_IEndDragHandler_t7_167_0_0_0 = { 1, GenInst_IEndDragHandler_t7_167_0_0_0_Types };
extern const Il2CppType IDropHandler_t7_168_0_0_0;
static const Il2CppType* GenInst_IDropHandler_t7_168_0_0_0_Types[] = { &IDropHandler_t7_168_0_0_0 };
extern const Il2CppGenericInst GenInst_IDropHandler_t7_168_0_0_0 = { 1, GenInst_IDropHandler_t7_168_0_0_0_Types };
extern const Il2CppType IScrollHandler_t7_169_0_0_0;
static const Il2CppType* GenInst_IScrollHandler_t7_169_0_0_0_Types[] = { &IScrollHandler_t7_169_0_0_0 };
extern const Il2CppGenericInst GenInst_IScrollHandler_t7_169_0_0_0 = { 1, GenInst_IScrollHandler_t7_169_0_0_0_Types };
extern const Il2CppType IUpdateSelectedHandler_t7_170_0_0_0;
static const Il2CppType* GenInst_IUpdateSelectedHandler_t7_170_0_0_0_Types[] = { &IUpdateSelectedHandler_t7_170_0_0_0 };
extern const Il2CppGenericInst GenInst_IUpdateSelectedHandler_t7_170_0_0_0 = { 1, GenInst_IUpdateSelectedHandler_t7_170_0_0_0_Types };
extern const Il2CppType IMoveHandler_t7_173_0_0_0;
static const Il2CppType* GenInst_IMoveHandler_t7_173_0_0_0_Types[] = { &IMoveHandler_t7_173_0_0_0 };
extern const Il2CppGenericInst GenInst_IMoveHandler_t7_173_0_0_0 = { 1, GenInst_IMoveHandler_t7_173_0_0_0_Types };
extern const Il2CppType ISubmitHandler_t7_174_0_0_0;
static const Il2CppType* GenInst_ISubmitHandler_t7_174_0_0_0_Types[] = { &ISubmitHandler_t7_174_0_0_0 };
extern const Il2CppGenericInst GenInst_ISubmitHandler_t7_174_0_0_0 = { 1, GenInst_ISubmitHandler_t7_174_0_0_0_Types };
extern const Il2CppType ICancelHandler_t7_175_0_0_0;
static const Il2CppType* GenInst_ICancelHandler_t7_175_0_0_0_Types[] = { &ICancelHandler_t7_175_0_0_0 };
extern const Il2CppGenericInst GenInst_ICancelHandler_t7_175_0_0_0 = { 1, GenInst_ICancelHandler_t7_175_0_0_0_Types };
extern const Il2CppType List_1_t1_1879_0_0_0;
static const Il2CppType* GenInst_List_1_t1_1879_0_0_0_Types[] = { &List_1_t1_1879_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1_1879_0_0_0 = { 1, GenInst_List_1_t1_1879_0_0_0_Types };
extern const Il2CppType IEventSystemHandler_t7_200_0_0_0;
static const Il2CppType* GenInst_IEventSystemHandler_t7_200_0_0_0_Types[] = { &IEventSystemHandler_t7_200_0_0_0 };
extern const Il2CppGenericInst GenInst_IEventSystemHandler_t7_200_0_0_0 = { 1, GenInst_IEventSystemHandler_t7_200_0_0_0_Types };
extern const Il2CppType Transform_t6_65_0_0_0;
static const Il2CppType* GenInst_Transform_t6_65_0_0_0_Types[] = { &Transform_t6_65_0_0_0 };
extern const Il2CppGenericInst GenInst_Transform_t6_65_0_0_0 = { 1, GenInst_Transform_t6_65_0_0_0_Types };
extern const Il2CppType PointerEventData_t7_36_0_0_0;
static const Il2CppType* GenInst_PointerEventData_t7_36_0_0_0_Types[] = { &PointerEventData_t7_36_0_0_0 };
extern const Il2CppGenericInst GenInst_PointerEventData_t7_36_0_0_0 = { 1, GenInst_PointerEventData_t7_36_0_0_0_Types };
extern const Il2CppType AxisEventData_t7_33_0_0_0;
static const Il2CppType* GenInst_AxisEventData_t7_33_0_0_0_Types[] = { &AxisEventData_t7_33_0_0_0 };
extern const Il2CppGenericInst GenInst_AxisEventData_t7_33_0_0_0 = { 1, GenInst_AxisEventData_t7_33_0_0_0_Types };
extern const Il2CppType BaseRaycaster_t7_32_0_0_0;
static const Il2CppType* GenInst_BaseRaycaster_t7_32_0_0_0_Types[] = { &BaseRaycaster_t7_32_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseRaycaster_t7_32_0_0_0 = { 1, GenInst_BaseRaycaster_t7_32_0_0_0_Types };
extern const Il2CppType Camera_t6_86_0_0_0;
static const Il2CppType* GenInst_Camera_t6_86_0_0_0_Types[] = { &Camera_t6_86_0_0_0 };
extern const Il2CppGenericInst GenInst_Camera_t6_86_0_0_0 = { 1, GenInst_Camera_t6_86_0_0_0_Types };
extern const Il2CppType GameObject_t6_97_0_0_0;
static const Il2CppType* GenInst_GameObject_t6_97_0_0_0_Types[] = { &GameObject_t6_97_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_t6_97_0_0_0 = { 1, GenInst_GameObject_t6_97_0_0_0_Types };
extern const Il2CppType EventSystem_t7_2_0_0_0;
static const Il2CppType* GenInst_EventSystem_t7_2_0_0_0_Types[] = { &EventSystem_t7_2_0_0_0 };
extern const Il2CppGenericInst GenInst_EventSystem_t7_2_0_0_0 = { 1, GenInst_EventSystem_t7_2_0_0_0_Types };
extern const Il2CppType ButtonState_t7_37_0_0_0;
static const Il2CppType* GenInst_ButtonState_t7_37_0_0_0_Types[] = { &ButtonState_t7_37_0_0_0 };
extern const Il2CppGenericInst GenInst_ButtonState_t7_37_0_0_0 = { 1, GenInst_ButtonState_t7_37_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1_3_0_0_0_PointerEventData_t7_36_0_0_0_Types[] = { &Int32_t1_3_0_0_0, &PointerEventData_t7_36_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1_3_0_0_0_PointerEventData_t7_36_0_0_0 = { 2, GenInst_Int32_t1_3_0_0_0_PointerEventData_t7_36_0_0_0_Types };
extern const Il2CppType SpriteRenderer_t6_76_0_0_0;
static const Il2CppType* GenInst_SpriteRenderer_t6_76_0_0_0_Types[] = { &SpriteRenderer_t6_76_0_0_0 };
extern const Il2CppGenericInst GenInst_SpriteRenderer_t6_76_0_0_0 = { 1, GenInst_SpriteRenderer_t6_76_0_0_0_Types };
extern const Il2CppType RaycastHit_t6_116_0_0_0;
static const Il2CppType* GenInst_RaycastHit_t6_116_0_0_0_Types[] = { &RaycastHit_t6_116_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastHit_t6_116_0_0_0 = { 1, GenInst_RaycastHit_t6_116_0_0_0_Types };
extern const Il2CppType Color_t6_40_0_0_0;
static const Il2CppType* GenInst_Color_t6_40_0_0_0_Types[] = { &Color_t6_40_0_0_0 };
extern const Il2CppGenericInst GenInst_Color_t6_40_0_0_0 = { 1, GenInst_Color_t6_40_0_0_0_Types };
extern const Il2CppType ICanvasElement_t7_176_0_0_0;
static const Il2CppType* GenInst_ICanvasElement_t7_176_0_0_0_Types[] = { &ICanvasElement_t7_176_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t7_176_0_0_0 = { 1, GenInst_ICanvasElement_t7_176_0_0_0_Types };
extern const Il2CppType RectTransform_t6_64_0_0_0;
static const Il2CppType* GenInst_RectTransform_t6_64_0_0_0_Types[] = { &RectTransform_t6_64_0_0_0 };
extern const Il2CppGenericInst GenInst_RectTransform_t6_64_0_0_0 = { 1, GenInst_RectTransform_t6_64_0_0_0_Types };
extern const Il2CppType Image_t7_64_0_0_0;
static const Il2CppType* GenInst_Image_t7_64_0_0_0_Types[] = { &Image_t7_64_0_0_0 };
extern const Il2CppGenericInst GenInst_Image_t7_64_0_0_0 = { 1, GenInst_Image_t7_64_0_0_0_Types };
extern const Il2CppType Button_t7_54_0_0_0;
static const Il2CppType* GenInst_Button_t7_54_0_0_0_Types[] = { &Button_t7_54_0_0_0 };
extern const Il2CppGenericInst GenInst_Button_t7_54_0_0_0 = { 1, GenInst_Button_t7_54_0_0_0_Types };
extern const Il2CppType Text_t7_63_0_0_0;
static const Il2CppType* GenInst_Text_t7_63_0_0_0_Types[] = { &Text_t7_63_0_0_0 };
extern const Il2CppGenericInst GenInst_Text_t7_63_0_0_0 = { 1, GenInst_Text_t7_63_0_0_0_Types };
extern const Il2CppType RawImage_t7_107_0_0_0;
static const Il2CppType* GenInst_RawImage_t7_107_0_0_0_Types[] = { &RawImage_t7_107_0_0_0 };
extern const Il2CppGenericInst GenInst_RawImage_t7_107_0_0_0 = { 1, GenInst_RawImage_t7_107_0_0_0_Types };
extern const Il2CppType Slider_t7_125_0_0_0;
static const Il2CppType* GenInst_Slider_t7_125_0_0_0_Types[] = { &Slider_t7_125_0_0_0 };
extern const Il2CppGenericInst GenInst_Slider_t7_125_0_0_0 = { 1, GenInst_Slider_t7_125_0_0_0_Types };
extern const Il2CppType Scrollbar_t7_113_0_0_0;
static const Il2CppType* GenInst_Scrollbar_t7_113_0_0_0_Types[] = { &Scrollbar_t7_113_0_0_0 };
extern const Il2CppGenericInst GenInst_Scrollbar_t7_113_0_0_0 = { 1, GenInst_Scrollbar_t7_113_0_0_0_Types };
extern const Il2CppType Toggle_t7_65_0_0_0;
static const Il2CppType* GenInst_Toggle_t7_65_0_0_0_Types[] = { &Toggle_t7_65_0_0_0 };
extern const Il2CppGenericInst GenInst_Toggle_t7_65_0_0_0 = { 1, GenInst_Toggle_t7_65_0_0_0_Types };
extern const Il2CppType InputField_t7_98_0_0_0;
static const Il2CppType* GenInst_InputField_t7_98_0_0_0_Types[] = { &InputField_t7_98_0_0_0 };
extern const Il2CppGenericInst GenInst_InputField_t7_98_0_0_0 = { 1, GenInst_InputField_t7_98_0_0_0_Types };
extern const Il2CppType ScrollRect_t7_117_0_0_0;
static const Il2CppType* GenInst_ScrollRect_t7_117_0_0_0_Types[] = { &ScrollRect_t7_117_0_0_0 };
extern const Il2CppGenericInst GenInst_ScrollRect_t7_117_0_0_0 = { 1, GenInst_ScrollRect_t7_117_0_0_0_Types };
extern const Il2CppType Mask_t7_100_0_0_0;
static const Il2CppType* GenInst_Mask_t7_100_0_0_0_Types[] = { &Mask_t7_100_0_0_0 };
extern const Il2CppGenericInst GenInst_Mask_t7_100_0_0_0 = { 1, GenInst_Mask_t7_100_0_0_0_Types };
extern const Il2CppType Dropdown_t7_70_0_0_0;
static const Il2CppType* GenInst_Dropdown_t7_70_0_0_0_Types[] = { &Dropdown_t7_70_0_0_0 };
extern const Il2CppGenericInst GenInst_Dropdown_t7_70_0_0_0 = { 1, GenInst_Dropdown_t7_70_0_0_0_Types };
extern const Il2CppType OptionData_t7_66_0_0_0;
static const Il2CppType* GenInst_OptionData_t7_66_0_0_0_Types[] = { &OptionData_t7_66_0_0_0 };
extern const Il2CppGenericInst GenInst_OptionData_t7_66_0_0_0 = { 1, GenInst_OptionData_t7_66_0_0_0_Types };
extern const Il2CppType DropdownItem_t7_62_0_0_0;
static const Il2CppType* GenInst_DropdownItem_t7_62_0_0_0_Types[] = { &DropdownItem_t7_62_0_0_0 };
extern const Il2CppGenericInst GenInst_DropdownItem_t7_62_0_0_0 = { 1, GenInst_DropdownItem_t7_62_0_0_0_Types };
extern const Il2CppType FloatTween_t7_50_0_0_0;
static const Il2CppType* GenInst_FloatTween_t7_50_0_0_0_Types[] = { &FloatTween_t7_50_0_0_0 };
extern const Il2CppGenericInst GenInst_FloatTween_t7_50_0_0_0 = { 1, GenInst_FloatTween_t7_50_0_0_0_Types };
extern const Il2CppType Canvas_t6_156_0_0_0;
static const Il2CppType* GenInst_Canvas_t6_156_0_0_0_Types[] = { &Canvas_t6_156_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t6_156_0_0_0 = { 1, GenInst_Canvas_t6_156_0_0_0_Types };
extern const Il2CppType GraphicRaycaster_t7_79_0_0_0;
static const Il2CppType* GenInst_GraphicRaycaster_t7_79_0_0_0_Types[] = { &GraphicRaycaster_t7_79_0_0_0 };
extern const Il2CppGenericInst GenInst_GraphicRaycaster_t7_79_0_0_0 = { 1, GenInst_GraphicRaycaster_t7_79_0_0_0_Types };
extern const Il2CppType CanvasGroup_t6_157_0_0_0;
static const Il2CppType* GenInst_CanvasGroup_t6_157_0_0_0_Types[] = { &CanvasGroup_t6_157_0_0_0 };
extern const Il2CppGenericInst GenInst_CanvasGroup_t6_157_0_0_0 = { 1, GenInst_CanvasGroup_t6_157_0_0_0_Types };
extern const Il2CppType List_1_t1_1889_0_0_0;
static const Il2CppType* GenInst_Font_t6_149_0_0_0_List_1_t1_1889_0_0_0_Types[] = { &Font_t6_149_0_0_0, &List_1_t1_1889_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t6_149_0_0_0_List_1_t1_1889_0_0_0 = { 2, GenInst_Font_t6_149_0_0_0_List_1_t1_1889_0_0_0_Types };
extern const Il2CppType ColorTween_t7_48_0_0_0;
static const Il2CppType* GenInst_ColorTween_t7_48_0_0_0_Types[] = { &ColorTween_t7_48_0_0_0 };
extern const Il2CppGenericInst GenInst_ColorTween_t7_48_0_0_0 = { 1, GenInst_ColorTween_t7_48_0_0_0_Types };
extern const Il2CppType CanvasRenderer_t6_159_0_0_0;
static const Il2CppType* GenInst_CanvasRenderer_t6_159_0_0_0_Types[] = { &CanvasRenderer_t6_159_0_0_0 };
extern const Il2CppGenericInst GenInst_CanvasRenderer_t6_159_0_0_0 = { 1, GenInst_CanvasRenderer_t6_159_0_0_0_Types };
extern const Il2CppType Component_t6_26_0_0_0;
static const Il2CppType* GenInst_Component_t6_26_0_0_0_Types[] = { &Component_t6_26_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_t6_26_0_0_0 = { 1, GenInst_Component_t6_26_0_0_0_Types };
extern const Il2CppType Graphic_t7_75_0_0_0;
static const Il2CppType* GenInst_Graphic_t7_75_0_0_0_Types[] = { &Graphic_t7_75_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t7_75_0_0_0 = { 1, GenInst_Graphic_t7_75_0_0_0_Types };
extern const Il2CppType IndexedSet_1_t7_185_0_0_0;
static const Il2CppType* GenInst_Canvas_t6_156_0_0_0_IndexedSet_1_t7_185_0_0_0_Types[] = { &Canvas_t6_156_0_0_0, &IndexedSet_1_t7_185_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t6_156_0_0_0_IndexedSet_1_t7_185_0_0_0 = { 2, GenInst_Canvas_t6_156_0_0_0_IndexedSet_1_t7_185_0_0_0_Types };
extern const Il2CppType Sprite_t6_75_0_0_0;
static const Il2CppType* GenInst_Sprite_t6_75_0_0_0_Types[] = { &Sprite_t6_75_0_0_0 };
extern const Il2CppGenericInst GenInst_Sprite_t6_75_0_0_0 = { 1, GenInst_Sprite_t6_75_0_0_0_Types };
extern const Il2CppType Type_t7_81_0_0_0;
static const Il2CppType* GenInst_Type_t7_81_0_0_0_Types[] = { &Type_t7_81_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t7_81_0_0_0 = { 1, GenInst_Type_t7_81_0_0_0_Types };
extern const Il2CppType FillMethod_t7_82_0_0_0;
static const Il2CppType* GenInst_FillMethod_t7_82_0_0_0_Types[] = { &FillMethod_t7_82_0_0_0 };
extern const Il2CppGenericInst GenInst_FillMethod_t7_82_0_0_0 = { 1, GenInst_FillMethod_t7_82_0_0_0_Types };
extern const Il2CppType SubmitEvent_t7_93_0_0_0;
static const Il2CppType* GenInst_SubmitEvent_t7_93_0_0_0_Types[] = { &SubmitEvent_t7_93_0_0_0 };
extern const Il2CppGenericInst GenInst_SubmitEvent_t7_93_0_0_0 = { 1, GenInst_SubmitEvent_t7_93_0_0_0_Types };
extern const Il2CppType OnChangeEvent_t7_94_0_0_0;
static const Il2CppType* GenInst_OnChangeEvent_t7_94_0_0_0_Types[] = { &OnChangeEvent_t7_94_0_0_0 };
extern const Il2CppGenericInst GenInst_OnChangeEvent_t7_94_0_0_0 = { 1, GenInst_OnChangeEvent_t7_94_0_0_0_Types };
extern const Il2CppType OnValidateInput_t7_96_0_0_0;
static const Il2CppType* GenInst_OnValidateInput_t7_96_0_0_0_Types[] = { &OnValidateInput_t7_96_0_0_0 };
extern const Il2CppGenericInst GenInst_OnValidateInput_t7_96_0_0_0 = { 1, GenInst_OnValidateInput_t7_96_0_0_0_Types };
extern const Il2CppType ContentType_t7_89_0_0_0;
static const Il2CppType* GenInst_ContentType_t7_89_0_0_0_Types[] = { &ContentType_t7_89_0_0_0 };
extern const Il2CppGenericInst GenInst_ContentType_t7_89_0_0_0 = { 1, GenInst_ContentType_t7_89_0_0_0_Types };
extern const Il2CppType LineType_t7_92_0_0_0;
static const Il2CppType* GenInst_LineType_t7_92_0_0_0_Types[] = { &LineType_t7_92_0_0_0 };
extern const Il2CppGenericInst GenInst_LineType_t7_92_0_0_0 = { 1, GenInst_LineType_t7_92_0_0_0_Types };
extern const Il2CppType InputType_t7_90_0_0_0;
static const Il2CppType* GenInst_InputType_t7_90_0_0_0_Types[] = { &InputType_t7_90_0_0_0 };
extern const Il2CppGenericInst GenInst_InputType_t7_90_0_0_0 = { 1, GenInst_InputType_t7_90_0_0_0_Types };
extern const Il2CppType TouchScreenKeyboardType_t6_44_0_0_0;
static const Il2CppType* GenInst_TouchScreenKeyboardType_t6_44_0_0_0_Types[] = { &TouchScreenKeyboardType_t6_44_0_0_0 };
extern const Il2CppGenericInst GenInst_TouchScreenKeyboardType_t6_44_0_0_0 = { 1, GenInst_TouchScreenKeyboardType_t6_44_0_0_0_Types };
extern const Il2CppType CharacterValidation_t7_91_0_0_0;
static const Il2CppType* GenInst_CharacterValidation_t7_91_0_0_0_Types[] = { &CharacterValidation_t7_91_0_0_0 };
extern const Il2CppGenericInst GenInst_CharacterValidation_t7_91_0_0_0 = { 1, GenInst_CharacterValidation_t7_91_0_0_0_Types };
extern const Il2CppType LayoutElement_t7_149_0_0_0;
static const Il2CppType* GenInst_LayoutElement_t7_149_0_0_0_Types[] = { &LayoutElement_t7_149_0_0_0 };
extern const Il2CppGenericInst GenInst_LayoutElement_t7_149_0_0_0 = { 1, GenInst_LayoutElement_t7_149_0_0_0_Types };
extern const Il2CppType IClippable_t7_178_0_0_0;
static const Il2CppType* GenInst_IClippable_t7_178_0_0_0_Types[] = { &IClippable_t7_178_0_0_0 };
extern const Il2CppGenericInst GenInst_IClippable_t7_178_0_0_0 = { 1, GenInst_IClippable_t7_178_0_0_0_Types };
extern const Il2CppType RectMask2D_t7_102_0_0_0;
static const Il2CppType* GenInst_RectMask2D_t7_102_0_0_0_Types[] = { &RectMask2D_t7_102_0_0_0 };
extern const Il2CppGenericInst GenInst_RectMask2D_t7_102_0_0_0 = { 1, GenInst_RectMask2D_t7_102_0_0_0_Types };
extern const Il2CppType Direction_t7_109_0_0_0;
static const Il2CppType* GenInst_Direction_t7_109_0_0_0_Types[] = { &Direction_t7_109_0_0_0 };
extern const Il2CppGenericInst GenInst_Direction_t7_109_0_0_0 = { 1, GenInst_Direction_t7_109_0_0_0_Types };
extern const Il2CppType Vector2_t6_47_0_0_0;
static const Il2CppType* GenInst_Vector2_t6_47_0_0_0_Types[] = { &Vector2_t6_47_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector2_t6_47_0_0_0 = { 1, GenInst_Vector2_t6_47_0_0_0_Types };
extern const Il2CppType Selectable_t7_55_0_0_0;
static const Il2CppType* GenInst_Selectable_t7_55_0_0_0_Types[] = { &Selectable_t7_55_0_0_0 };
extern const Il2CppGenericInst GenInst_Selectable_t7_55_0_0_0 = { 1, GenInst_Selectable_t7_55_0_0_0_Types };
extern const Il2CppType Navigation_t7_106_0_0_0;
static const Il2CppType* GenInst_Navigation_t7_106_0_0_0_Types[] = { &Navigation_t7_106_0_0_0 };
extern const Il2CppGenericInst GenInst_Navigation_t7_106_0_0_0 = { 1, GenInst_Navigation_t7_106_0_0_0_Types };
extern const Il2CppType Transition_t7_118_0_0_0;
static const Il2CppType* GenInst_Transition_t7_118_0_0_0_Types[] = { &Transition_t7_118_0_0_0 };
extern const Il2CppGenericInst GenInst_Transition_t7_118_0_0_0 = { 1, GenInst_Transition_t7_118_0_0_0_Types };
extern const Il2CppType ColorBlock_t7_59_0_0_0;
static const Il2CppType* GenInst_ColorBlock_t7_59_0_0_0_Types[] = { &ColorBlock_t7_59_0_0_0 };
extern const Il2CppGenericInst GenInst_ColorBlock_t7_59_0_0_0 = { 1, GenInst_ColorBlock_t7_59_0_0_0_Types };
extern const Il2CppType SpriteState_t7_120_0_0_0;
static const Il2CppType* GenInst_SpriteState_t7_120_0_0_0_Types[] = { &SpriteState_t7_120_0_0_0 };
extern const Il2CppGenericInst GenInst_SpriteState_t7_120_0_0_0 = { 1, GenInst_SpriteState_t7_120_0_0_0_Types };
extern const Il2CppType AnimationTriggers_t7_51_0_0_0;
static const Il2CppType* GenInst_AnimationTriggers_t7_51_0_0_0_Types[] = { &AnimationTriggers_t7_51_0_0_0 };
extern const Il2CppGenericInst GenInst_AnimationTriggers_t7_51_0_0_0 = { 1, GenInst_AnimationTriggers_t7_51_0_0_0_Types };
extern const Il2CppType Animator_t6_139_0_0_0;
static const Il2CppType* GenInst_Animator_t6_139_0_0_0_Types[] = { &Animator_t6_139_0_0_0 };
extern const Il2CppGenericInst GenInst_Animator_t6_139_0_0_0 = { 1, GenInst_Animator_t6_139_0_0_0_Types };
extern const Il2CppType Direction_t7_122_0_0_0;
static const Il2CppType* GenInst_Direction_t7_122_0_0_0_Types[] = { &Direction_t7_122_0_0_0 };
extern const Il2CppGenericInst GenInst_Direction_t7_122_0_0_0 = { 1, GenInst_Direction_t7_122_0_0_0_Types };
extern const Il2CppType MatEntry_t7_126_0_0_0;
static const Il2CppType* GenInst_MatEntry_t7_126_0_0_0_Types[] = { &MatEntry_t7_126_0_0_0 };
extern const Il2CppGenericInst GenInst_MatEntry_t7_126_0_0_0 = { 1, GenInst_MatEntry_t7_126_0_0_0_Types };
static const Il2CppType* GenInst_Toggle_t7_65_0_0_0_Boolean_t1_20_0_0_0_Types[] = { &Toggle_t7_65_0_0_0, &Boolean_t1_20_0_0_0 };
extern const Il2CppGenericInst GenInst_Toggle_t7_65_0_0_0_Boolean_t1_20_0_0_0 = { 2, GenInst_Toggle_t7_65_0_0_0_Boolean_t1_20_0_0_0_Types };
extern const Il2CppType IClipper_t7_179_0_0_0;
static const Il2CppType* GenInst_IClipper_t7_179_0_0_0_Types[] = { &IClipper_t7_179_0_0_0 };
extern const Il2CppGenericInst GenInst_IClipper_t7_179_0_0_0 = { 1, GenInst_IClipper_t7_179_0_0_0_Types };
extern const Il2CppType AspectMode_t7_134_0_0_0;
static const Il2CppType* GenInst_AspectMode_t7_134_0_0_0_Types[] = { &AspectMode_t7_134_0_0_0 };
extern const Il2CppGenericInst GenInst_AspectMode_t7_134_0_0_0 = { 1, GenInst_AspectMode_t7_134_0_0_0_Types };
extern const Il2CppType FitMode_t7_140_0_0_0;
static const Il2CppType* GenInst_FitMode_t7_140_0_0_0_Types[] = { &FitMode_t7_140_0_0_0 };
extern const Il2CppGenericInst GenInst_FitMode_t7_140_0_0_0 = { 1, GenInst_FitMode_t7_140_0_0_0_Types };
extern const Il2CppType Corner_t7_142_0_0_0;
static const Il2CppType* GenInst_Corner_t7_142_0_0_0_Types[] = { &Corner_t7_142_0_0_0 };
extern const Il2CppGenericInst GenInst_Corner_t7_142_0_0_0 = { 1, GenInst_Corner_t7_142_0_0_0_Types };
extern const Il2CppType Axis_t7_143_0_0_0;
static const Il2CppType* GenInst_Axis_t7_143_0_0_0_Types[] = { &Axis_t7_143_0_0_0 };
extern const Il2CppGenericInst GenInst_Axis_t7_143_0_0_0 = { 1, GenInst_Axis_t7_143_0_0_0_Types };
extern const Il2CppType Constraint_t7_144_0_0_0;
static const Il2CppType* GenInst_Constraint_t7_144_0_0_0_Types[] = { &Constraint_t7_144_0_0_0 };
extern const Il2CppGenericInst GenInst_Constraint_t7_144_0_0_0 = { 1, GenInst_Constraint_t7_144_0_0_0_Types };
extern const Il2CppType RectOffset_t6_178_0_0_0;
static const Il2CppType* GenInst_RectOffset_t6_178_0_0_0_Types[] = { &RectOffset_t6_178_0_0_0 };
extern const Il2CppGenericInst GenInst_RectOffset_t6_178_0_0_0 = { 1, GenInst_RectOffset_t6_178_0_0_0_Types };
extern const Il2CppType TextAnchor_t6_144_0_0_0;
static const Il2CppType* GenInst_TextAnchor_t6_144_0_0_0_Types[] = { &TextAnchor_t6_144_0_0_0 };
extern const Il2CppGenericInst GenInst_TextAnchor_t6_144_0_0_0 = { 1, GenInst_TextAnchor_t6_144_0_0_0_Types };
extern const Il2CppType LayoutRebuilder_t7_150_0_0_0;
static const Il2CppType* GenInst_LayoutRebuilder_t7_150_0_0_0_Types[] = { &LayoutRebuilder_t7_150_0_0_0 };
extern const Il2CppGenericInst GenInst_LayoutRebuilder_t7_150_0_0_0 = { 1, GenInst_LayoutRebuilder_t7_150_0_0_0_Types };
extern const Il2CppType ILayoutElement_t7_180_0_0_0;
static const Il2CppType* GenInst_ILayoutElement_t7_180_0_0_0_Single_t1_17_0_0_0_Types[] = { &ILayoutElement_t7_180_0_0_0, &Single_t1_17_0_0_0 };
extern const Il2CppGenericInst GenInst_ILayoutElement_t7_180_0_0_0_Single_t1_17_0_0_0 = { 2, GenInst_ILayoutElement_t7_180_0_0_0_Single_t1_17_0_0_0_Types };
extern const Il2CppType Vector3_t6_48_0_0_0;
static const Il2CppType* GenInst_Vector3_t6_48_0_0_0_Types[] = { &Vector3_t6_48_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3_t6_48_0_0_0 = { 1, GenInst_Vector3_t6_48_0_0_0_Types };
extern const Il2CppType Color32_t6_49_0_0_0;
static const Il2CppType* GenInst_Color32_t6_49_0_0_0_Types[] = { &Color32_t6_49_0_0_0 };
extern const Il2CppGenericInst GenInst_Color32_t6_49_0_0_0 = { 1, GenInst_Color32_t6_49_0_0_0_Types };
extern const Il2CppType Vector4_t6_54_0_0_0;
static const Il2CppType* GenInst_Vector4_t6_54_0_0_0_Types[] = { &Vector4_t6_54_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector4_t6_54_0_0_0 = { 1, GenInst_Vector4_t6_54_0_0_0_Types };
extern const Il2CppType JSONValue_t8_4_0_0_0;
static const Il2CppType* GenInst_JSONValue_t8_4_0_0_0_Types[] = { &JSONValue_t8_4_0_0_0 };
extern const Il2CppGenericInst GenInst_JSONValue_t8_4_0_0_0 = { 1, GenInst_JSONValue_t8_4_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_JSONValue_t8_4_0_0_0_Types[] = { &String_t_0_0_0, &JSONValue_t8_4_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_JSONValue_t8_4_0_0_0 = { 2, GenInst_String_t_0_0_0_JSONValue_t8_4_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_1915_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_1915_0_0_0_Types[] = { &KeyValuePair_2_t1_1915_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_1915_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_1915_0_0_0_Types };
extern const Il2CppType DemoSubMenu_t8_18_0_0_0;
static const Il2CppType* GenInst_DemoSubMenu_t8_18_0_0_0_Types[] = { &DemoSubMenu_t8_18_0_0_0 };
extern const Il2CppGenericInst GenInst_DemoSubMenu_t8_18_0_0_0 = { 1, GenInst_DemoSubMenu_t8_18_0_0_0_Types };
extern const Il2CppType GUIScrollView_t8_19_0_0_0;
static const Il2CppType* GenInst_GUIScrollView_t8_19_0_0_0_Types[] = { &GUIScrollView_t8_19_0_0_0 };
extern const Il2CppGenericInst GenInst_GUIScrollView_t8_19_0_0_0 = { 1, GenInst_GUIScrollView_t8_19_0_0_0_Types };
extern const Il2CppType ShaderInfo_t8_26_0_0_0;
static const Il2CppType* GenInst_ShaderInfo_t8_26_0_0_0_Types[] = { &ShaderInfo_t8_26_0_0_0 };
extern const Il2CppGenericInst GenInst_ShaderInfo_t8_26_0_0_0 = { 1, GenInst_ShaderInfo_t8_26_0_0_0_Types };
extern const Il2CppType ShaderUtility_t8_29_0_0_0;
static const Il2CppType* GenInst_ShaderUtility_t8_29_0_0_0_Types[] = { &ShaderUtility_t8_29_0_0_0 };
extern const Il2CppGenericInst GenInst_ShaderUtility_t8_29_0_0_0 = { 1, GenInst_ShaderUtility_t8_29_0_0_0_Types };
extern const Il2CppType Texture2D_t6_33_0_0_0;
static const Il2CppType* GenInst_Texture2D_t6_33_0_0_0_Types[] = { &Texture2D_t6_33_0_0_0 };
extern const Il2CppGenericInst GenInst_Texture2D_t6_33_0_0_0 = { 1, GenInst_Texture2D_t6_33_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Object_t_0_0_0_Types[] = { &String_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Object_t_0_0_0 = { 2, GenInst_String_t_0_0_0_Object_t_0_0_0_Types };
extern const Il2CppType Byte_t1_11_0_0_0;
static const Il2CppType* GenInst_Byte_t1_11_0_0_0_Types[] = { &Byte_t1_11_0_0_0 };
extern const Il2CppGenericInst GenInst_Byte_t1_11_0_0_0 = { 1, GenInst_Byte_t1_11_0_0_0_Types };
extern const Il2CppType JPEGSection_t8_112_0_0_0;
static const Il2CppType* GenInst_JPEGSection_t8_112_0_0_0_Types[] = { &JPEGSection_t8_112_0_0_0 };
extern const Il2CppGenericInst GenInst_JPEGSection_t8_112_0_0_0 = { 1, GenInst_JPEGSection_t8_112_0_0_0_Types };
extern const Il2CppType IFD_t8_131_0_0_0;
static const Il2CppType* GenInst_Int32_t1_3_0_0_0_IFD_t8_131_0_0_0_Types[] = { &Int32_t1_3_0_0_0, &IFD_t8_131_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1_3_0_0_0_IFD_t8_131_0_0_0 = { 2, GenInst_Int32_t1_3_0_0_0_IFD_t8_131_0_0_0_Types };
static const Il2CppType* GenInst_IFD_t8_131_0_0_0_Types[] = { &IFD_t8_131_0_0_0 };
extern const Il2CppGenericInst GenInst_IFD_t8_131_0_0_0 = { 1, GenInst_IFD_t8_131_0_0_0_Types };
extern const Il2CppType ExifTag_t8_132_0_0_0;
extern const Il2CppType ExifProperty_t8_99_0_0_0;
static const Il2CppType* GenInst_ExifTag_t8_132_0_0_0_ExifProperty_t8_99_0_0_0_Types[] = { &ExifTag_t8_132_0_0_0, &ExifProperty_t8_99_0_0_0 };
extern const Il2CppGenericInst GenInst_ExifTag_t8_132_0_0_0_ExifProperty_t8_99_0_0_0 = { 2, GenInst_ExifTag_t8_132_0_0_0_ExifProperty_t8_99_0_0_0_Types };
static const Il2CppType* GenInst_ExifProperty_t8_99_0_0_0_Types[] = { &ExifProperty_t8_99_0_0_0 };
extern const Il2CppGenericInst GenInst_ExifProperty_t8_99_0_0_0 = { 1, GenInst_ExifProperty_t8_99_0_0_0_Types };
extern const Il2CppType Compression_t8_63_0_0_0;
static const Il2CppType* GenInst_Compression_t8_63_0_0_0_Types[] = { &Compression_t8_63_0_0_0 };
extern const Il2CppGenericInst GenInst_Compression_t8_63_0_0_0 = { 1, GenInst_Compression_t8_63_0_0_0_Types };
extern const Il2CppType PhotometricInterpretation_t8_64_0_0_0;
static const Il2CppType* GenInst_PhotometricInterpretation_t8_64_0_0_0_Types[] = { &PhotometricInterpretation_t8_64_0_0_0 };
extern const Il2CppGenericInst GenInst_PhotometricInterpretation_t8_64_0_0_0 = { 1, GenInst_PhotometricInterpretation_t8_64_0_0_0_Types };
extern const Il2CppType Orientation_t8_65_0_0_0;
static const Il2CppType* GenInst_Orientation_t8_65_0_0_0_Types[] = { &Orientation_t8_65_0_0_0 };
extern const Il2CppGenericInst GenInst_Orientation_t8_65_0_0_0 = { 1, GenInst_Orientation_t8_65_0_0_0_Types };
extern const Il2CppType PlanarConfiguration_t8_66_0_0_0;
static const Il2CppType* GenInst_PlanarConfiguration_t8_66_0_0_0_Types[] = { &PlanarConfiguration_t8_66_0_0_0 };
extern const Il2CppGenericInst GenInst_PlanarConfiguration_t8_66_0_0_0 = { 1, GenInst_PlanarConfiguration_t8_66_0_0_0_Types };
extern const Il2CppType YCbCrPositioning_t8_67_0_0_0;
static const Il2CppType* GenInst_YCbCrPositioning_t8_67_0_0_0_Types[] = { &YCbCrPositioning_t8_67_0_0_0 };
extern const Il2CppGenericInst GenInst_YCbCrPositioning_t8_67_0_0_0 = { 1, GenInst_YCbCrPositioning_t8_67_0_0_0_Types };
extern const Il2CppType ResolutionUnit_t8_68_0_0_0;
static const Il2CppType* GenInst_ResolutionUnit_t8_68_0_0_0_Types[] = { &ResolutionUnit_t8_68_0_0_0 };
extern const Il2CppGenericInst GenInst_ResolutionUnit_t8_68_0_0_0 = { 1, GenInst_ResolutionUnit_t8_68_0_0_0_Types };
extern const Il2CppType ColorSpace_t8_69_0_0_0;
static const Il2CppType* GenInst_ColorSpace_t8_69_0_0_0_Types[] = { &ColorSpace_t8_69_0_0_0 };
extern const Il2CppGenericInst GenInst_ColorSpace_t8_69_0_0_0 = { 1, GenInst_ColorSpace_t8_69_0_0_0_Types };
extern const Il2CppType ExposureProgram_t8_70_0_0_0;
static const Il2CppType* GenInst_ExposureProgram_t8_70_0_0_0_Types[] = { &ExposureProgram_t8_70_0_0_0 };
extern const Il2CppGenericInst GenInst_ExposureProgram_t8_70_0_0_0 = { 1, GenInst_ExposureProgram_t8_70_0_0_0_Types };
extern const Il2CppType MeteringMode_t8_71_0_0_0;
static const Il2CppType* GenInst_MeteringMode_t8_71_0_0_0_Types[] = { &MeteringMode_t8_71_0_0_0 };
extern const Il2CppGenericInst GenInst_MeteringMode_t8_71_0_0_0 = { 1, GenInst_MeteringMode_t8_71_0_0_0_Types };
extern const Il2CppType LightSource_t8_72_0_0_0;
static const Il2CppType* GenInst_LightSource_t8_72_0_0_0_Types[] = { &LightSource_t8_72_0_0_0 };
extern const Il2CppGenericInst GenInst_LightSource_t8_72_0_0_0 = { 1, GenInst_LightSource_t8_72_0_0_0_Types };
extern const Il2CppType Flash_t8_73_0_0_0;
static const Il2CppType* GenInst_Flash_t8_73_0_0_0_Types[] = { &Flash_t8_73_0_0_0 };
extern const Il2CppGenericInst GenInst_Flash_t8_73_0_0_0 = { 1, GenInst_Flash_t8_73_0_0_0_Types };
extern const Il2CppType SensingMethod_t8_74_0_0_0;
static const Il2CppType* GenInst_SensingMethod_t8_74_0_0_0_Types[] = { &SensingMethod_t8_74_0_0_0 };
extern const Il2CppGenericInst GenInst_SensingMethod_t8_74_0_0_0 = { 1, GenInst_SensingMethod_t8_74_0_0_0_Types };
extern const Il2CppType FileSource_t8_75_0_0_0;
static const Il2CppType* GenInst_FileSource_t8_75_0_0_0_Types[] = { &FileSource_t8_75_0_0_0 };
extern const Il2CppGenericInst GenInst_FileSource_t8_75_0_0_0 = { 1, GenInst_FileSource_t8_75_0_0_0_Types };
extern const Il2CppType SceneType_t8_76_0_0_0;
static const Il2CppType* GenInst_SceneType_t8_76_0_0_0_Types[] = { &SceneType_t8_76_0_0_0 };
extern const Il2CppGenericInst GenInst_SceneType_t8_76_0_0_0 = { 1, GenInst_SceneType_t8_76_0_0_0_Types };
extern const Il2CppType CustomRendered_t8_77_0_0_0;
static const Il2CppType* GenInst_CustomRendered_t8_77_0_0_0_Types[] = { &CustomRendered_t8_77_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomRendered_t8_77_0_0_0 = { 1, GenInst_CustomRendered_t8_77_0_0_0_Types };
extern const Il2CppType ExposureMode_t8_78_0_0_0;
static const Il2CppType* GenInst_ExposureMode_t8_78_0_0_0_Types[] = { &ExposureMode_t8_78_0_0_0 };
extern const Il2CppGenericInst GenInst_ExposureMode_t8_78_0_0_0 = { 1, GenInst_ExposureMode_t8_78_0_0_0_Types };
extern const Il2CppType WhiteBalance_t8_79_0_0_0;
static const Il2CppType* GenInst_WhiteBalance_t8_79_0_0_0_Types[] = { &WhiteBalance_t8_79_0_0_0 };
extern const Il2CppGenericInst GenInst_WhiteBalance_t8_79_0_0_0 = { 1, GenInst_WhiteBalance_t8_79_0_0_0_Types };
extern const Il2CppType SceneCaptureType_t8_80_0_0_0;
static const Il2CppType* GenInst_SceneCaptureType_t8_80_0_0_0_Types[] = { &SceneCaptureType_t8_80_0_0_0 };
extern const Il2CppGenericInst GenInst_SceneCaptureType_t8_80_0_0_0 = { 1, GenInst_SceneCaptureType_t8_80_0_0_0_Types };
extern const Il2CppType GainControl_t8_81_0_0_0;
static const Il2CppType* GenInst_GainControl_t8_81_0_0_0_Types[] = { &GainControl_t8_81_0_0_0 };
extern const Il2CppGenericInst GenInst_GainControl_t8_81_0_0_0 = { 1, GenInst_GainControl_t8_81_0_0_0_Types };
extern const Il2CppType Contrast_t8_82_0_0_0;
static const Il2CppType* GenInst_Contrast_t8_82_0_0_0_Types[] = { &Contrast_t8_82_0_0_0 };
extern const Il2CppGenericInst GenInst_Contrast_t8_82_0_0_0 = { 1, GenInst_Contrast_t8_82_0_0_0_Types };
extern const Il2CppType Saturation_t8_83_0_0_0;
static const Il2CppType* GenInst_Saturation_t8_83_0_0_0_Types[] = { &Saturation_t8_83_0_0_0 };
extern const Il2CppGenericInst GenInst_Saturation_t8_83_0_0_0 = { 1, GenInst_Saturation_t8_83_0_0_0_Types };
extern const Il2CppType Sharpness_t8_84_0_0_0;
static const Il2CppType* GenInst_Sharpness_t8_84_0_0_0_Types[] = { &Sharpness_t8_84_0_0_0 };
extern const Il2CppGenericInst GenInst_Sharpness_t8_84_0_0_0 = { 1, GenInst_Sharpness_t8_84_0_0_0_Types };
extern const Il2CppType SubjectDistanceRange_t8_85_0_0_0;
static const Il2CppType* GenInst_SubjectDistanceRange_t8_85_0_0_0_Types[] = { &SubjectDistanceRange_t8_85_0_0_0 };
extern const Il2CppGenericInst GenInst_SubjectDistanceRange_t8_85_0_0_0 = { 1, GenInst_SubjectDistanceRange_t8_85_0_0_0_Types };
extern const Il2CppType GPSLatitudeRef_t8_86_0_0_0;
static const Il2CppType* GenInst_GPSLatitudeRef_t8_86_0_0_0_Types[] = { &GPSLatitudeRef_t8_86_0_0_0 };
extern const Il2CppGenericInst GenInst_GPSLatitudeRef_t8_86_0_0_0 = { 1, GenInst_GPSLatitudeRef_t8_86_0_0_0_Types };
extern const Il2CppType GPSLongitudeRef_t8_87_0_0_0;
static const Il2CppType* GenInst_GPSLongitudeRef_t8_87_0_0_0_Types[] = { &GPSLongitudeRef_t8_87_0_0_0 };
extern const Il2CppGenericInst GenInst_GPSLongitudeRef_t8_87_0_0_0 = { 1, GenInst_GPSLongitudeRef_t8_87_0_0_0_Types };
extern const Il2CppType GPSAltitudeRef_t8_88_0_0_0;
static const Il2CppType* GenInst_GPSAltitudeRef_t8_88_0_0_0_Types[] = { &GPSAltitudeRef_t8_88_0_0_0 };
extern const Il2CppGenericInst GenInst_GPSAltitudeRef_t8_88_0_0_0 = { 1, GenInst_GPSAltitudeRef_t8_88_0_0_0_Types };
extern const Il2CppType GPSStatus_t8_89_0_0_0;
static const Il2CppType* GenInst_GPSStatus_t8_89_0_0_0_Types[] = { &GPSStatus_t8_89_0_0_0 };
extern const Il2CppGenericInst GenInst_GPSStatus_t8_89_0_0_0 = { 1, GenInst_GPSStatus_t8_89_0_0_0_Types };
extern const Il2CppType GPSMeasureMode_t8_90_0_0_0;
static const Il2CppType* GenInst_GPSMeasureMode_t8_90_0_0_0_Types[] = { &GPSMeasureMode_t8_90_0_0_0 };
extern const Il2CppGenericInst GenInst_GPSMeasureMode_t8_90_0_0_0 = { 1, GenInst_GPSMeasureMode_t8_90_0_0_0_Types };
extern const Il2CppType GPSSpeedRef_t8_91_0_0_0;
static const Il2CppType* GenInst_GPSSpeedRef_t8_91_0_0_0_Types[] = { &GPSSpeedRef_t8_91_0_0_0 };
extern const Il2CppGenericInst GenInst_GPSSpeedRef_t8_91_0_0_0 = { 1, GenInst_GPSSpeedRef_t8_91_0_0_0_Types };
extern const Il2CppType GPSDirectionRef_t8_92_0_0_0;
static const Il2CppType* GenInst_GPSDirectionRef_t8_92_0_0_0_Types[] = { &GPSDirectionRef_t8_92_0_0_0 };
extern const Il2CppGenericInst GenInst_GPSDirectionRef_t8_92_0_0_0 = { 1, GenInst_GPSDirectionRef_t8_92_0_0_0_Types };
extern const Il2CppType GPSDistanceRef_t8_93_0_0_0;
static const Il2CppType* GenInst_GPSDistanceRef_t8_93_0_0_0_Types[] = { &GPSDistanceRef_t8_93_0_0_0 };
extern const Il2CppGenericInst GenInst_GPSDistanceRef_t8_93_0_0_0 = { 1, GenInst_GPSDistanceRef_t8_93_0_0_0_Types };
extern const Il2CppType GPSDifferential_t8_94_0_0_0;
static const Il2CppType* GenInst_GPSDifferential_t8_94_0_0_0_Types[] = { &GPSDifferential_t8_94_0_0_0 };
extern const Il2CppGenericInst GenInst_GPSDifferential_t8_94_0_0_0 = { 1, GenInst_GPSDifferential_t8_94_0_0_0_Types };
extern const Il2CppType GUISkin_t6_169_0_0_0;
static const Il2CppType* GenInst_GUISkin_t6_169_0_0_0_Types[] = { &GUISkin_t6_169_0_0_0 };
extern const Il2CppGenericInst GenInst_GUISkin_t6_169_0_0_0 = { 1, GenInst_GUISkin_t6_169_0_0_0_Types };
extern const Il2CppType GUISubMenu_t8_143_0_0_0;
static const Il2CppType* GenInst_GUISubMenu_t8_143_0_0_0_Types[] = { &GUISubMenu_t8_143_0_0_0 };
extern const Il2CppGenericInst GenInst_GUISubMenu_t8_143_0_0_0 = { 1, GenInst_GUISubMenu_t8_143_0_0_0_Types };
extern const Il2CppType Action_t5_11_0_0_0;
extern const Il2CppType Dictionary_2_t1_1929_0_0_0;
static const Il2CppType* GenInst_Action_t5_11_0_0_0_Dictionary_2_t1_1929_0_0_0_Types[] = { &Action_t5_11_0_0_0, &Dictionary_2_t1_1929_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_t5_11_0_0_0_Dictionary_2_t1_1929_0_0_0 = { 2, GenInst_Action_t5_11_0_0_0_Dictionary_2_t1_1929_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Single_t1_17_0_0_0_Types[] = { &String_t_0_0_0, &Single_t1_17_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Single_t1_17_0_0_0 = { 2, GenInst_String_t_0_0_0_Single_t1_17_0_0_0_Types };
extern const Il2CppType MonoBehaviour_t6_91_0_0_0;
static const Il2CppType* GenInst_MonoBehaviour_t6_91_0_0_0_Types[] = { &MonoBehaviour_t6_91_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoBehaviour_t6_91_0_0_0 = { 1, GenInst_MonoBehaviour_t6_91_0_0_0_Types };
extern const Il2CppType List_1_t1_1742_0_0_0;
static const Il2CppType* GenInst_List_1_t1_1742_0_0_0_Types[] = { &List_1_t1_1742_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1_1742_0_0_0 = { 1, GenInst_List_1_t1_1742_0_0_0_Types };
extern const Il2CppType ConsoleLog_t8_170_0_0_0;
static const Il2CppType* GenInst_ConsoleLog_t8_170_0_0_0_Types[] = { &ConsoleLog_t8_170_0_0_0 };
extern const Il2CppGenericInst GenInst_ConsoleLog_t8_170_0_0_0 = { 1, GenInst_ConsoleLog_t8_170_0_0_0_Types };
extern const Il2CppType ConsoleTag_t8_171_0_0_0;
static const Il2CppType* GenInst_ConsoleTag_t8_171_0_0_0_Types[] = { &ConsoleTag_t8_171_0_0_0 };
extern const Il2CppGenericInst GenInst_ConsoleTag_t8_171_0_0_0 = { 1, GenInst_ConsoleTag_t8_171_0_0_0_Types };
extern const Il2CppType Console_t8_169_0_0_0;
static const Il2CppType* GenInst_Console_t8_169_0_0_0_Types[] = { &Console_t8_169_0_0_0 };
extern const Il2CppGenericInst GenInst_Console_t8_169_0_0_0 = { 1, GenInst_Console_t8_169_0_0_0_Types };
static const Il2CppType* GenInst_ConsoleTag_t8_171_0_0_0_Boolean_t1_20_0_0_0_Types[] = { &ConsoleTag_t8_171_0_0_0, &Boolean_t1_20_0_0_0 };
extern const Il2CppGenericInst GenInst_ConsoleTag_t8_171_0_0_0_Boolean_t1_20_0_0_0 = { 2, GenInst_ConsoleTag_t8_171_0_0_0_Boolean_t1_20_0_0_0_Types };
extern const Il2CppType UnityDebugUtility_t8_173_0_0_0;
static const Il2CppType* GenInst_UnityDebugUtility_t8_173_0_0_0_Types[] = { &UnityDebugUtility_t8_173_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityDebugUtility_t8_173_0_0_0 = { 1, GenInst_UnityDebugUtility_t8_173_0_0_0_Types };
extern const Il2CppType IList_t1_262_0_0_0;
static const Il2CppType* GenInst_IList_t1_262_0_0_0_Types[] = { &IList_t1_262_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_t1_262_0_0_0 = { 1, GenInst_IList_t1_262_0_0_0_Types };
extern const Il2CppType Int64_t1_7_0_0_0;
static const Il2CppType* GenInst_Int64_t1_7_0_0_0_Types[] = { &Int64_t1_7_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t1_7_0_0_0 = { 1, GenInst_Int64_t1_7_0_0_0_Types };
extern const Il2CppType BillingProduct_t8_207_0_0_0;
static const Il2CppType* GenInst_BillingProduct_t8_207_0_0_0_Types[] = { &BillingProduct_t8_207_0_0_0 };
extern const Il2CppGenericInst GenInst_BillingProduct_t8_207_0_0_0 = { 1, GenInst_BillingProduct_t8_207_0_0_0_Types };
extern const Il2CppType eNotificationRepeatInterval_t8_264_0_0_0;
static const Il2CppType* GenInst_eNotificationRepeatInterval_t8_264_0_0_0_Types[] = { &eNotificationRepeatInterval_t8_264_0_0_0 };
extern const Il2CppGenericInst GenInst_eNotificationRepeatInterval_t8_264_0_0_0 = { 1, GenInst_eNotificationRepeatInterval_t8_264_0_0_0_Types };
extern const Il2CppType IDictionary_t1_35_0_0_0;
static const Il2CppType* GenInst_IDictionary_t1_35_0_0_0_Types[] = { &IDictionary_t1_35_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_t1_35_0_0_0 = { 1, GenInst_IDictionary_t1_35_0_0_0_Types };
extern const Il2CppType CalendarUnit_t6_103_0_0_0;
static const Il2CppType* GenInst_CalendarUnit_t6_103_0_0_0_Types[] = { &CalendarUnit_t6_103_0_0_0 };
extern const Il2CppGenericInst GenInst_CalendarUnit_t6_103_0_0_0 = { 1, GenInst_CalendarUnit_t6_103_0_0_0_Types };
extern const Il2CppType NPBinding_t8_333_0_0_0;
static const Il2CppType* GenInst_NPBinding_t8_333_0_0_0_Types[] = { &NPBinding_t8_333_0_0_0 };
extern const Il2CppGenericInst GenInst_NPBinding_t8_333_0_0_0 = { 1, GenInst_NPBinding_t8_333_0_0_0_Types };
extern const Il2CppType AlertDialogCompletion_t8_300_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_AlertDialogCompletion_t8_300_0_0_0_Types[] = { &String_t_0_0_0, &AlertDialogCompletion_t8_300_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_AlertDialogCompletion_t8_300_0_0_0 = { 2, GenInst_String_t_0_0_0_AlertDialogCompletion_t8_300_0_0_0_Types };
extern const Il2CppType RateMyApp_t8_307_0_0_0;
static const Il2CppType* GenInst_RateMyApp_t8_307_0_0_0_Types[] = { &RateMyApp_t8_307_0_0_0 };
extern const Il2CppGenericInst GenInst_RateMyApp_t8_307_0_0_0 = { 1, GenInst_RateMyApp_t8_307_0_0_0_Types };
extern const Il2CppType WebView_t8_191_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_WebView_t8_191_0_0_0_Types[] = { &String_t_0_0_0, &WebView_t8_191_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_WebView_t8_191_0_0_0 = { 2, GenInst_String_t_0_0_0_WebView_t8_191_0_0_0_Types };
extern const Il2CppType NPObject_t8_222_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_NPObject_t8_222_0_0_0_Types[] = { &String_t_0_0_0, &NPObject_t8_222_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_NPObject_t8_222_0_0_0 = { 2, GenInst_String_t_0_0_0_NPObject_t8_222_0_0_0_Types };
extern const Il2CppType NotificationService_t8_261_0_0_0;
static const Il2CppType* GenInst_NotificationService_t8_261_0_0_0_Types[] = { &NotificationService_t8_261_0_0_0 };
extern const Il2CppGenericInst GenInst_NotificationService_t8_261_0_0_0 = { 1, GenInst_NotificationService_t8_261_0_0_0_Types };
extern const Il2CppType UI_t8_303_0_0_0;
static const Il2CppType* GenInst_UI_t8_303_0_0_0_Types[] = { &UI_t8_303_0_0_0 };
extern const Il2CppGenericInst GenInst_UI_t8_303_0_0_0 = { 1, GenInst_UI_t8_303_0_0_0_Types };
extern const Il2CppType Utility_t8_306_0_0_0;
static const Il2CppType* GenInst_Utility_t8_306_0_0_0_Types[] = { &Utility_t8_306_0_0_0 };
extern const Il2CppGenericInst GenInst_Utility_t8_306_0_0_0 = { 1, GenInst_Utility_t8_306_0_0_0_Types };
extern const Il2CppType NPSettings_t8_335_0_0_0;
static const Il2CppType* GenInst_NPSettings_t8_335_0_0_0_Types[] = { &NPSettings_t8_335_0_0_0 };
extern const Il2CppGenericInst GenInst_NPSettings_t8_335_0_0_0 = { 1, GenInst_NPSettings_t8_335_0_0_0_Types };
extern const Il2CppType Attribute_t1_2_0_0_0;
static const Il2CppType* GenInst_Attribute_t1_2_0_0_0_Types[] = { &Attribute_t1_2_0_0_0 };
extern const Il2CppGenericInst GenInst_Attribute_t1_2_0_0_0 = { 1, GenInst_Attribute_t1_2_0_0_0_Types };
extern const Il2CppType _Attribute_t1_3065_0_0_0;
static const Il2CppType* GenInst__Attribute_t1_3065_0_0_0_Types[] = { &_Attribute_t1_3065_0_0_0 };
extern const Il2CppGenericInst GenInst__Attribute_t1_3065_0_0_0 = { 1, GenInst__Attribute_t1_3065_0_0_0_Types };
extern const Il2CppType IConvertible_t1_1746_0_0_0;
static const Il2CppType* GenInst_IConvertible_t1_1746_0_0_0_Types[] = { &IConvertible_t1_1746_0_0_0 };
extern const Il2CppGenericInst GenInst_IConvertible_t1_1746_0_0_0 = { 1, GenInst_IConvertible_t1_1746_0_0_0_Types };
extern const Il2CppType IComparable_t1_1745_0_0_0;
static const Il2CppType* GenInst_IComparable_t1_1745_0_0_0_Types[] = { &IComparable_t1_1745_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_t1_1745_0_0_0 = { 1, GenInst_IComparable_t1_1745_0_0_0_Types };
extern const Il2CppType IComparable_1_t1_3343_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1_3343_0_0_0_Types[] = { &IComparable_1_t1_3343_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1_3343_0_0_0 = { 1, GenInst_IComparable_1_t1_3343_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1_3348_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1_3348_0_0_0_Types[] = { &IEquatable_1_t1_3348_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1_3348_0_0_0 = { 1, GenInst_IEquatable_1_t1_3348_0_0_0_Types };
extern const Il2CppType ValueType_t1_1_0_0_0;
static const Il2CppType* GenInst_ValueType_t1_1_0_0_0_Types[] = { &ValueType_t1_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueType_t1_1_0_0_0 = { 1, GenInst_ValueType_t1_1_0_0_0_Types };
extern const Il2CppType UInt32_t1_8_0_0_0;
static const Il2CppType* GenInst_UInt32_t1_8_0_0_0_Types[] = { &UInt32_t1_8_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt32_t1_8_0_0_0 = { 1, GenInst_UInt32_t1_8_0_0_0_Types };
extern const Il2CppType UInt64_t1_10_0_0_0;
static const Il2CppType* GenInst_UInt64_t1_10_0_0_0_Types[] = { &UInt64_t1_10_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt64_t1_10_0_0_0 = { 1, GenInst_UInt64_t1_10_0_0_0_Types };
extern const Il2CppType SByte_t1_12_0_0_0;
static const Il2CppType* GenInst_SByte_t1_12_0_0_0_Types[] = { &SByte_t1_12_0_0_0 };
extern const Il2CppGenericInst GenInst_SByte_t1_12_0_0_0 = { 1, GenInst_SByte_t1_12_0_0_0_Types };
extern const Il2CppType Int16_t1_13_0_0_0;
static const Il2CppType* GenInst_Int16_t1_13_0_0_0_Types[] = { &Int16_t1_13_0_0_0 };
extern const Il2CppGenericInst GenInst_Int16_t1_13_0_0_0 = { 1, GenInst_Int16_t1_13_0_0_0_Types };
extern const Il2CppType UInt16_t1_14_0_0_0;
static const Il2CppType* GenInst_UInt16_t1_14_0_0_0_Types[] = { &UInt16_t1_14_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt16_t1_14_0_0_0 = { 1, GenInst_UInt16_t1_14_0_0_0_Types };
extern const Il2CppType IEnumerable_t1_1677_0_0_0;
static const Il2CppType* GenInst_IEnumerable_t1_1677_0_0_0_Types[] = { &IEnumerable_t1_1677_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_t1_1677_0_0_0 = { 1, GenInst_IEnumerable_t1_1677_0_0_0_Types };
extern const Il2CppType ICloneable_t1_1762_0_0_0;
static const Il2CppType* GenInst_ICloneable_t1_1762_0_0_0_Types[] = { &ICloneable_t1_1762_0_0_0 };
extern const Il2CppGenericInst GenInst_ICloneable_t1_1762_0_0_0 = { 1, GenInst_ICloneable_t1_1762_0_0_0_Types };
extern const Il2CppType IComparable_1_t1_3372_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1_3372_0_0_0_Types[] = { &IComparable_1_t1_3372_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1_3372_0_0_0 = { 1, GenInst_IComparable_1_t1_3372_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1_3373_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1_3373_0_0_0_Types[] = { &IEquatable_1_t1_3373_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1_3373_0_0_0 = { 1, GenInst_IEquatable_1_t1_3373_0_0_0_Types };
extern const Il2CppType IReflect_t1_3077_0_0_0;
static const Il2CppType* GenInst_IReflect_t1_3077_0_0_0_Types[] = { &IReflect_t1_3077_0_0_0 };
extern const Il2CppGenericInst GenInst_IReflect_t1_3077_0_0_0 = { 1, GenInst_IReflect_t1_3077_0_0_0_Types };
extern const Il2CppType _Type_t1_3075_0_0_0;
static const Il2CppType* GenInst__Type_t1_3075_0_0_0_Types[] = { &_Type_t1_3075_0_0_0 };
extern const Il2CppGenericInst GenInst__Type_t1_3075_0_0_0 = { 1, GenInst__Type_t1_3075_0_0_0_Types };
extern const Il2CppType MemberInfo_t_0_0_0;
static const Il2CppType* GenInst_MemberInfo_t_0_0_0_Types[] = { &MemberInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MemberInfo_t_0_0_0 = { 1, GenInst_MemberInfo_t_0_0_0_Types };
extern const Il2CppType ICustomAttributeProvider_t1_1686_0_0_0;
static const Il2CppType* GenInst_ICustomAttributeProvider_t1_1686_0_0_0_Types[] = { &ICustomAttributeProvider_t1_1686_0_0_0 };
extern const Il2CppGenericInst GenInst_ICustomAttributeProvider_t1_1686_0_0_0 = { 1, GenInst_ICustomAttributeProvider_t1_1686_0_0_0_Types };
extern const Il2CppType _MemberInfo_t1_3076_0_0_0;
static const Il2CppType* GenInst__MemberInfo_t1_3076_0_0_0_Types[] = { &_MemberInfo_t1_3076_0_0_0 };
extern const Il2CppGenericInst GenInst__MemberInfo_t1_3076_0_0_0 = { 1, GenInst__MemberInfo_t1_3076_0_0_0_Types };
extern const Il2CppType IFormattable_t1_1744_0_0_0;
static const Il2CppType* GenInst_IFormattable_t1_1744_0_0_0_Types[] = { &IFormattable_t1_1744_0_0_0 };
extern const Il2CppGenericInst GenInst_IFormattable_t1_1744_0_0_0 = { 1, GenInst_IFormattable_t1_1744_0_0_0_Types };
extern const Il2CppType IComparable_1_t1_3330_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1_3330_0_0_0_Types[] = { &IComparable_1_t1_3330_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1_3330_0_0_0 = { 1, GenInst_IComparable_1_t1_3330_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1_3331_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1_3331_0_0_0_Types[] = { &IEquatable_1_t1_3331_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1_3331_0_0_0 = { 1, GenInst_IEquatable_1_t1_3331_0_0_0_Types };
extern const Il2CppType Double_t1_18_0_0_0;
static const Il2CppType* GenInst_Double_t1_18_0_0_0_Types[] = { &Double_t1_18_0_0_0 };
extern const Il2CppGenericInst GenInst_Double_t1_18_0_0_0 = { 1, GenInst_Double_t1_18_0_0_0_Types };
extern const Il2CppType IComparable_1_t1_3432_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1_3432_0_0_0_Types[] = { &IComparable_1_t1_3432_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1_3432_0_0_0 = { 1, GenInst_IComparable_1_t1_3432_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1_3437_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1_3437_0_0_0_Types[] = { &IEquatable_1_t1_3437_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1_3437_0_0_0 = { 1, GenInst_IEquatable_1_t1_3437_0_0_0_Types };
extern const Il2CppType IComparable_1_t1_3364_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1_3364_0_0_0_Types[] = { &IComparable_1_t1_3364_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1_3364_0_0_0 = { 1, GenInst_IComparable_1_t1_3364_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1_3365_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1_3365_0_0_0_Types[] = { &IEquatable_1_t1_3365_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1_3365_0_0_0 = { 1, GenInst_IEquatable_1_t1_3365_0_0_0_Types };
extern const Il2CppType IComparable_1_t1_3456_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1_3456_0_0_0_Types[] = { &IComparable_1_t1_3456_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1_3456_0_0_0 = { 1, GenInst_IComparable_1_t1_3456_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1_3461_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1_3461_0_0_0_Types[] = { &IEquatable_1_t1_3461_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1_3461_0_0_0 = { 1, GenInst_IEquatable_1_t1_3461_0_0_0_Types };
extern const Il2CppType Decimal_t1_19_0_0_0;
static const Il2CppType* GenInst_Decimal_t1_19_0_0_0_Types[] = { &Decimal_t1_19_0_0_0 };
extern const Il2CppGenericInst GenInst_Decimal_t1_19_0_0_0 = { 1, GenInst_Decimal_t1_19_0_0_0_Types };
extern const Il2CppType Delegate_t1_22_0_0_0;
static const Il2CppType* GenInst_Delegate_t1_22_0_0_0_Types[] = { &Delegate_t1_22_0_0_0 };
extern const Il2CppGenericInst GenInst_Delegate_t1_22_0_0_0 = { 1, GenInst_Delegate_t1_22_0_0_0_Types };
extern const Il2CppType ISerializable_t1_1778_0_0_0;
static const Il2CppType* GenInst_ISerializable_t1_1778_0_0_0_Types[] = { &ISerializable_t1_1778_0_0_0 };
extern const Il2CppGenericInst GenInst_ISerializable_t1_1778_0_0_0 = { 1, GenInst_ISerializable_t1_1778_0_0_0_Types };
extern const Il2CppType ParameterInfo_t1_627_0_0_0;
static const Il2CppType* GenInst_ParameterInfo_t1_627_0_0_0_Types[] = { &ParameterInfo_t1_627_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterInfo_t1_627_0_0_0 = { 1, GenInst_ParameterInfo_t1_627_0_0_0_Types };
extern const Il2CppType _ParameterInfo_t1_3135_0_0_0;
static const Il2CppType* GenInst__ParameterInfo_t1_3135_0_0_0_Types[] = { &_ParameterInfo_t1_3135_0_0_0 };
extern const Il2CppGenericInst GenInst__ParameterInfo_t1_3135_0_0_0 = { 1, GenInst__ParameterInfo_t1_3135_0_0_0_Types };
extern const Il2CppType ParameterModifier_t1_628_0_0_0;
static const Il2CppType* GenInst_ParameterModifier_t1_628_0_0_0_Types[] = { &ParameterModifier_t1_628_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterModifier_t1_628_0_0_0 = { 1, GenInst_ParameterModifier_t1_628_0_0_0_Types };
extern const Il2CppType IComparable_1_t1_3370_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1_3370_0_0_0_Types[] = { &IComparable_1_t1_3370_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1_3370_0_0_0 = { 1, GenInst_IComparable_1_t1_3370_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1_3371_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1_3371_0_0_0_Types[] = { &IEquatable_1_t1_3371_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1_3371_0_0_0 = { 1, GenInst_IEquatable_1_t1_3371_0_0_0_Types };
extern const Il2CppType IComparable_1_t1_3360_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1_3360_0_0_0_Types[] = { &IComparable_1_t1_3360_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1_3360_0_0_0 = { 1, GenInst_IComparable_1_t1_3360_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1_3361_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1_3361_0_0_0_Types[] = { &IEquatable_1_t1_3361_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1_3361_0_0_0 = { 1, GenInst_IEquatable_1_t1_3361_0_0_0_Types };
extern const Il2CppType IComparable_1_t1_3362_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1_3362_0_0_0_Types[] = { &IComparable_1_t1_3362_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1_3362_0_0_0 = { 1, GenInst_IComparable_1_t1_3362_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1_3363_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1_3363_0_0_0_Types[] = { &IEquatable_1_t1_3363_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1_3363_0_0_0 = { 1, GenInst_IEquatable_1_t1_3363_0_0_0_Types };
extern const Il2CppType IComparable_1_t1_3368_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1_3368_0_0_0_Types[] = { &IComparable_1_t1_3368_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1_3368_0_0_0 = { 1, GenInst_IComparable_1_t1_3368_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1_3369_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1_3369_0_0_0_Types[] = { &IEquatable_1_t1_3369_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1_3369_0_0_0 = { 1, GenInst_IEquatable_1_t1_3369_0_0_0_Types };
extern const Il2CppType IComparable_1_t1_3366_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1_3366_0_0_0_Types[] = { &IComparable_1_t1_3366_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1_3366_0_0_0 = { 1, GenInst_IComparable_1_t1_3366_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1_3367_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1_3367_0_0_0_Types[] = { &IEquatable_1_t1_3367_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1_3367_0_0_0 = { 1, GenInst_IEquatable_1_t1_3367_0_0_0_Types };
extern const Il2CppType IComparable_1_t1_3358_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1_3358_0_0_0_Types[] = { &IComparable_1_t1_3358_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1_3358_0_0_0 = { 1, GenInst_IComparable_1_t1_3358_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1_3359_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1_3359_0_0_0_Types[] = { &IEquatable_1_t1_3359_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1_3359_0_0_0 = { 1, GenInst_IEquatable_1_t1_3359_0_0_0_Types };
extern const Il2CppType MethodInfo_t_0_0_0;
static const Il2CppType* GenInst_MethodInfo_t_0_0_0_Types[] = { &MethodInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodInfo_t_0_0_0 = { 1, GenInst_MethodInfo_t_0_0_0_Types };
extern const Il2CppType _MethodInfo_t1_3133_0_0_0;
static const Il2CppType* GenInst__MethodInfo_t1_3133_0_0_0_Types[] = { &_MethodInfo_t1_3133_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodInfo_t1_3133_0_0_0 = { 1, GenInst__MethodInfo_t1_3133_0_0_0_Types };
extern const Il2CppType MethodBase_t1_335_0_0_0;
static const Il2CppType* GenInst_MethodBase_t1_335_0_0_0_Types[] = { &MethodBase_t1_335_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodBase_t1_335_0_0_0 = { 1, GenInst_MethodBase_t1_335_0_0_0_Types };
extern const Il2CppType _MethodBase_t1_3132_0_0_0;
static const Il2CppType* GenInst__MethodBase_t1_3132_0_0_0_Types[] = { &_MethodBase_t1_3132_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodBase_t1_3132_0_0_0 = { 1, GenInst__MethodBase_t1_3132_0_0_0_Types };
extern const Il2CppType EventInfo_t_0_0_0;
static const Il2CppType* GenInst_EventInfo_t_0_0_0_Types[] = { &EventInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_EventInfo_t_0_0_0 = { 1, GenInst_EventInfo_t_0_0_0_Types };
extern const Il2CppType _EventInfo_t1_3130_0_0_0;
static const Il2CppType* GenInst__EventInfo_t1_3130_0_0_0_Types[] = { &_EventInfo_t1_3130_0_0_0 };
extern const Il2CppGenericInst GenInst__EventInfo_t1_3130_0_0_0 = { 1, GenInst__EventInfo_t1_3130_0_0_0_Types };
extern const Il2CppType FieldInfo_t_0_0_0;
static const Il2CppType* GenInst_FieldInfo_t_0_0_0_Types[] = { &FieldInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldInfo_t_0_0_0 = { 1, GenInst_FieldInfo_t_0_0_0_Types };
extern const Il2CppType _FieldInfo_t1_3131_0_0_0;
static const Il2CppType* GenInst__FieldInfo_t1_3131_0_0_0_Types[] = { &_FieldInfo_t1_3131_0_0_0 };
extern const Il2CppGenericInst GenInst__FieldInfo_t1_3131_0_0_0 = { 1, GenInst__FieldInfo_t1_3131_0_0_0_Types };
extern const Il2CppType PropertyInfo_t_0_0_0;
static const Il2CppType* GenInst_PropertyInfo_t_0_0_0_Types[] = { &PropertyInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyInfo_t_0_0_0 = { 1, GenInst_PropertyInfo_t_0_0_0_Types };
extern const Il2CppType _PropertyInfo_t1_3136_0_0_0;
static const Il2CppType* GenInst__PropertyInfo_t1_3136_0_0_0_Types[] = { &_PropertyInfo_t1_3136_0_0_0 };
extern const Il2CppGenericInst GenInst__PropertyInfo_t1_3136_0_0_0 = { 1, GenInst__PropertyInfo_t1_3136_0_0_0_Types };
extern const Il2CppType ConstructorInfo_t1_478_0_0_0;
static const Il2CppType* GenInst_ConstructorInfo_t1_478_0_0_0_Types[] = { &ConstructorInfo_t1_478_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorInfo_t1_478_0_0_0 = { 1, GenInst_ConstructorInfo_t1_478_0_0_0_Types };
extern const Il2CppType _ConstructorInfo_t1_3129_0_0_0;
static const Il2CppType* GenInst__ConstructorInfo_t1_3129_0_0_0_Types[] = { &_ConstructorInfo_t1_3129_0_0_0 };
extern const Il2CppGenericInst GenInst__ConstructorInfo_t1_3129_0_0_0 = { 1, GenInst__ConstructorInfo_t1_3129_0_0_0_Types };
extern const Il2CppType IntPtr_t_0_0_0;
static const Il2CppType* GenInst_IntPtr_t_0_0_0_Types[] = { &IntPtr_t_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0 = { 1, GenInst_IntPtr_t_0_0_0_Types };
extern const Il2CppType RuntimeTypeHandle_t1_30_0_0_0;
static const Il2CppType* GenInst_RuntimeTypeHandle_t1_30_0_0_0_Types[] = { &RuntimeTypeHandle_t1_30_0_0_0 };
extern const Il2CppGenericInst GenInst_RuntimeTypeHandle_t1_30_0_0_0 = { 1, GenInst_RuntimeTypeHandle_t1_30_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int32_t1_3_0_0_0_Types[] = { &Object_t_0_0_0, &Int32_t1_3_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t1_3_0_0_0 = { 2, GenInst_Object_t_0_0_0_Int32_t1_3_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_1981_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_1981_0_0_0_Types[] = { &KeyValuePair_2_t1_1981_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_1981_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_1981_0_0_0_Types };
extern const Il2CppType Link_t1_256_0_0_0;
static const Il2CppType* GenInst_Link_t1_256_0_0_0_Types[] = { &Link_t1_256_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t1_256_0_0_0 = { 1, GenInst_Link_t1_256_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int32_t1_3_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &Int32_t1_3_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t1_3_0_0_0_Object_t_0_0_0 = { 3, GenInst_Object_t_0_0_0_Int32_t1_3_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int32_t1_3_0_0_0_Int32_t1_3_0_0_0_Types[] = { &Object_t_0_0_0, &Int32_t1_3_0_0_0, &Int32_t1_3_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t1_3_0_0_0_Int32_t1_3_0_0_0 = { 3, GenInst_Object_t_0_0_0_Int32_t1_3_0_0_0_Int32_t1_3_0_0_0_Types };
extern const Il2CppType DictionaryEntry_t1_284_0_0_0;
static const Il2CppType* GenInst_Object_t_0_0_0_Int32_t1_3_0_0_0_DictionaryEntry_t1_284_0_0_0_Types[] = { &Object_t_0_0_0, &Int32_t1_3_0_0_0, &DictionaryEntry_t1_284_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t1_3_0_0_0_DictionaryEntry_t1_284_0_0_0 = { 3, GenInst_Object_t_0_0_0_Int32_t1_3_0_0_0_DictionaryEntry_t1_284_0_0_0_Types };
static const Il2CppType* GenInst_DictionaryEntry_t1_284_0_0_0_Types[] = { &DictionaryEntry_t1_284_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t1_284_0_0_0 = { 1, GenInst_DictionaryEntry_t1_284_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int32_t1_3_0_0_0_KeyValuePair_2_t1_1981_0_0_0_Types[] = { &Object_t_0_0_0, &Int32_t1_3_0_0_0, &KeyValuePair_2_t1_1981_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t1_3_0_0_0_KeyValuePair_2_t1_1981_0_0_0 = { 3, GenInst_Object_t_0_0_0_Int32_t1_3_0_0_0_KeyValuePair_2_t1_1981_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Int32_t1_3_0_0_0_DictionaryEntry_t1_284_0_0_0_Types[] = { &String_t_0_0_0, &Int32_t1_3_0_0_0, &DictionaryEntry_t1_284_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t1_3_0_0_0_DictionaryEntry_t1_284_0_0_0 = { 3, GenInst_String_t_0_0_0_Int32_t1_3_0_0_0_DictionaryEntry_t1_284_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_1997_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_1997_0_0_0_Types[] = { &KeyValuePair_2_t1_1997_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_1997_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_1997_0_0_0_Types };
extern const Il2CppType DirectoryInfo_t1_399_0_0_0;
static const Il2CppType* GenInst_DirectoryInfo_t1_399_0_0_0_Types[] = { &DirectoryInfo_t1_399_0_0_0 };
extern const Il2CppGenericInst GenInst_DirectoryInfo_t1_399_0_0_0 = { 1, GenInst_DirectoryInfo_t1_399_0_0_0_Types };
extern const Il2CppType FileSystemInfo_t1_411_0_0_0;
static const Il2CppType* GenInst_FileSystemInfo_t1_411_0_0_0_Types[] = { &FileSystemInfo_t1_411_0_0_0 };
extern const Il2CppGenericInst GenInst_FileSystemInfo_t1_411_0_0_0 = { 1, GenInst_FileSystemInfo_t1_411_0_0_0_Types };
extern const Il2CppType MarshalByRefObject_t1_69_0_0_0;
static const Il2CppType* GenInst_MarshalByRefObject_t1_69_0_0_0_Types[] = { &MarshalByRefObject_t1_69_0_0_0 };
extern const Il2CppGenericInst GenInst_MarshalByRefObject_t1_69_0_0_0 = { 1, GenInst_MarshalByRefObject_t1_69_0_0_0_Types };
extern const Il2CppType TableRange_t1_104_0_0_0;
static const Il2CppType* GenInst_TableRange_t1_104_0_0_0_Types[] = { &TableRange_t1_104_0_0_0 };
extern const Il2CppGenericInst GenInst_TableRange_t1_104_0_0_0 = { 1, GenInst_TableRange_t1_104_0_0_0_Types };
extern const Il2CppType TailoringInfo_t1_107_0_0_0;
static const Il2CppType* GenInst_TailoringInfo_t1_107_0_0_0_Types[] = { &TailoringInfo_t1_107_0_0_0 };
extern const Il2CppGenericInst GenInst_TailoringInfo_t1_107_0_0_0 = { 1, GenInst_TailoringInfo_t1_107_0_0_0_Types };
extern const Il2CppType Contraction_t1_108_0_0_0;
static const Il2CppType* GenInst_Contraction_t1_108_0_0_0_Types[] = { &Contraction_t1_108_0_0_0 };
extern const Il2CppGenericInst GenInst_Contraction_t1_108_0_0_0 = { 1, GenInst_Contraction_t1_108_0_0_0_Types };
extern const Il2CppType Level2Map_t1_111_0_0_0;
static const Il2CppType* GenInst_Level2Map_t1_111_0_0_0_Types[] = { &Level2Map_t1_111_0_0_0 };
extern const Il2CppGenericInst GenInst_Level2Map_t1_111_0_0_0 = { 1, GenInst_Level2Map_t1_111_0_0_0_Types };
extern const Il2CppType BigInteger_t1_139_0_0_0;
static const Il2CppType* GenInst_BigInteger_t1_139_0_0_0_Types[] = { &BigInteger_t1_139_0_0_0 };
extern const Il2CppGenericInst GenInst_BigInteger_t1_139_0_0_0 = { 1, GenInst_BigInteger_t1_139_0_0_0_Types };
extern const Il2CppType KeySizes_t1_1219_0_0_0;
static const Il2CppType* GenInst_KeySizes_t1_1219_0_0_0_Types[] = { &KeySizes_t1_1219_0_0_0 };
extern const Il2CppGenericInst GenInst_KeySizes_t1_1219_0_0_0 = { 1, GenInst_KeySizes_t1_1219_0_0_0_Types };
extern const Il2CppType X509Certificate_t1_151_0_0_0;
static const Il2CppType* GenInst_X509Certificate_t1_151_0_0_0_Types[] = { &X509Certificate_t1_151_0_0_0 };
extern const Il2CppGenericInst GenInst_X509Certificate_t1_151_0_0_0 = { 1, GenInst_X509Certificate_t1_151_0_0_0_Types };
extern const Il2CppType X509Extension_t1_178_0_0_0;
static const Il2CppType* GenInst_X509Extension_t1_178_0_0_0_Types[] = { &X509Extension_t1_178_0_0_0 };
extern const Il2CppGenericInst GenInst_X509Extension_t1_178_0_0_0 = { 1, GenInst_X509Extension_t1_178_0_0_0_Types };
extern const Il2CppType UriScheme_t1_236_0_0_0;
static const Il2CppType* GenInst_UriScheme_t1_236_0_0_0_Types[] = { &UriScheme_t1_236_0_0_0 };
extern const Il2CppGenericInst GenInst_UriScheme_t1_236_0_0_0 = { 1, GenInst_UriScheme_t1_236_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_2015_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_2015_0_0_0_Types[] = { &KeyValuePair_2_t1_2015_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_2015_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_2015_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t1_284_0_0_0_Types[] = { &Object_t_0_0_0, &Object_t_0_0_0, &DictionaryEntry_t1_284_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t1_284_0_0_0 = { 3, GenInst_Object_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t1_284_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1_2015_0_0_0_Types[] = { &Object_t_0_0_0, &Object_t_0_0_0, &KeyValuePair_2_t1_2015_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1_2015_0_0_0 = { 3, GenInst_Object_t_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1_2015_0_0_0_Types };
extern const Il2CppType IComparable_1_t1_3467_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1_3467_0_0_0_Types[] = { &IComparable_1_t1_3467_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1_3467_0_0_0 = { 1, GenInst_IComparable_1_t1_3467_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1_3468_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1_3468_0_0_0_Types[] = { &IEquatable_1_t1_3468_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1_3468_0_0_0 = { 1, GenInst_IEquatable_1_t1_3468_0_0_0_Types };
extern const Il2CppType Slot_t1_285_0_0_0;
static const Il2CppType* GenInst_Slot_t1_285_0_0_0_Types[] = { &Slot_t1_285_0_0_0 };
extern const Il2CppGenericInst GenInst_Slot_t1_285_0_0_0 = { 1, GenInst_Slot_t1_285_0_0_0_Types };
extern const Il2CppType Slot_t1_301_0_0_0;
static const Il2CppType* GenInst_Slot_t1_301_0_0_0_Types[] = { &Slot_t1_301_0_0_0 };
extern const Il2CppGenericInst GenInst_Slot_t1_301_0_0_0 = { 1, GenInst_Slot_t1_301_0_0_0_Types };
extern const Il2CppType ISymbolVariable_t1_3110_0_0_0;
static const Il2CppType* GenInst_ISymbolVariable_t1_3110_0_0_0_Types[] = { &ISymbolVariable_t1_3110_0_0_0 };
extern const Il2CppGenericInst GenInst_ISymbolVariable_t1_3110_0_0_0 = { 1, GenInst_ISymbolVariable_t1_3110_0_0_0_Types };
extern const Il2CppType ISymbolDocument_t1_3105_0_0_0;
static const Il2CppType* GenInst_ISymbolDocument_t1_3105_0_0_0_Types[] = { &ISymbolDocument_t1_3105_0_0_0 };
extern const Il2CppGenericInst GenInst_ISymbolDocument_t1_3105_0_0_0 = { 1, GenInst_ISymbolDocument_t1_3105_0_0_0_Types };
extern const Il2CppType ISymbolNamespace_t1_3107_0_0_0;
static const Il2CppType* GenInst_ISymbolNamespace_t1_3107_0_0_0_Types[] = { &ISymbolNamespace_t1_3107_0_0_0 };
extern const Il2CppGenericInst GenInst_ISymbolNamespace_t1_3107_0_0_0 = { 1, GenInst_ISymbolNamespace_t1_3107_0_0_0_Types };
extern const Il2CppType ISymbolScope_t1_3109_0_0_0;
static const Il2CppType* GenInst_ISymbolScope_t1_3109_0_0_0_Types[] = { &ISymbolScope_t1_3109_0_0_0 };
extern const Il2CppGenericInst GenInst_ISymbolScope_t1_3109_0_0_0 = { 1, GenInst_ISymbolScope_t1_3109_0_0_0_Types };
extern const Il2CppType StackFrame_t1_334_0_0_0;
static const Il2CppType* GenInst_StackFrame_t1_334_0_0_0_Types[] = { &StackFrame_t1_334_0_0_0 };
extern const Il2CppGenericInst GenInst_StackFrame_t1_334_0_0_0 = { 1, GenInst_StackFrame_t1_334_0_0_0_Types };
extern const Il2CppType Calendar_t1_338_0_0_0;
static const Il2CppType* GenInst_Calendar_t1_338_0_0_0_Types[] = { &Calendar_t1_338_0_0_0 };
extern const Il2CppGenericInst GenInst_Calendar_t1_338_0_0_0 = { 1, GenInst_Calendar_t1_338_0_0_0_Types };
extern const Il2CppType CultureInfo_t1_277_0_0_0;
static const Il2CppType* GenInst_CultureInfo_t1_277_0_0_0_Types[] = { &CultureInfo_t1_277_0_0_0 };
extern const Il2CppGenericInst GenInst_CultureInfo_t1_277_0_0_0 = { 1, GenInst_CultureInfo_t1_277_0_0_0_Types };
extern const Il2CppType IFormatProvider_t1_455_0_0_0;
static const Il2CppType* GenInst_IFormatProvider_t1_455_0_0_0_Types[] = { &IFormatProvider_t1_455_0_0_0 };
extern const Il2CppGenericInst GenInst_IFormatProvider_t1_455_0_0_0 = { 1, GenInst_IFormatProvider_t1_455_0_0_0_Types };
extern const Il2CppType FileInfo_t1_422_0_0_0;
static const Il2CppType* GenInst_FileInfo_t1_422_0_0_0_Types[] = { &FileInfo_t1_422_0_0_0 };
extern const Il2CppGenericInst GenInst_FileInfo_t1_422_0_0_0 = { 1, GenInst_FileInfo_t1_422_0_0_0_Types };
extern const Il2CppType DriveInfo_t1_415_0_0_0;
static const Il2CppType* GenInst_DriveInfo_t1_415_0_0_0_Types[] = { &DriveInfo_t1_415_0_0_0 };
extern const Il2CppGenericInst GenInst_DriveInfo_t1_415_0_0_0 = { 1, GenInst_DriveInfo_t1_415_0_0_0_Types };
extern const Il2CppType ModuleBuilder_t1_475_0_0_0;
static const Il2CppType* GenInst_ModuleBuilder_t1_475_0_0_0_Types[] = { &ModuleBuilder_t1_475_0_0_0 };
extern const Il2CppGenericInst GenInst_ModuleBuilder_t1_475_0_0_0 = { 1, GenInst_ModuleBuilder_t1_475_0_0_0_Types };
extern const Il2CppType _ModuleBuilder_t1_3122_0_0_0;
static const Il2CppType* GenInst__ModuleBuilder_t1_3122_0_0_0_Types[] = { &_ModuleBuilder_t1_3122_0_0_0 };
extern const Il2CppGenericInst GenInst__ModuleBuilder_t1_3122_0_0_0 = { 1, GenInst__ModuleBuilder_t1_3122_0_0_0_Types };
extern const Il2CppType Module_t1_495_0_0_0;
static const Il2CppType* GenInst_Module_t1_495_0_0_0_Types[] = { &Module_t1_495_0_0_0 };
extern const Il2CppGenericInst GenInst_Module_t1_495_0_0_0 = { 1, GenInst_Module_t1_495_0_0_0_Types };
extern const Il2CppType _Module_t1_3134_0_0_0;
static const Il2CppType* GenInst__Module_t1_3134_0_0_0_Types[] = { &_Module_t1_3134_0_0_0 };
extern const Il2CppGenericInst GenInst__Module_t1_3134_0_0_0 = { 1, GenInst__Module_t1_3134_0_0_0_Types };
extern const Il2CppType _CustomAttributeBuilder_t1_3114_0_0_0;
static const Il2CppType* GenInst__CustomAttributeBuilder_t1_3114_0_0_0_Types[] = { &_CustomAttributeBuilder_t1_3114_0_0_0 };
extern const Il2CppGenericInst GenInst__CustomAttributeBuilder_t1_3114_0_0_0 = { 1, GenInst__CustomAttributeBuilder_t1_3114_0_0_0_Types };
extern const Il2CppType MonoResource_t1_464_0_0_0;
static const Il2CppType* GenInst_MonoResource_t1_464_0_0_0_Types[] = { &MonoResource_t1_464_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoResource_t1_464_0_0_0 = { 1, GenInst_MonoResource_t1_464_0_0_0_Types };
extern const Il2CppType MonoWin32Resource_t1_465_0_0_0;
static const Il2CppType* GenInst_MonoWin32Resource_t1_465_0_0_0_Types[] = { &MonoWin32Resource_t1_465_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoWin32Resource_t1_465_0_0_0 = { 1, GenInst_MonoWin32Resource_t1_465_0_0_0_Types };
extern const Il2CppType RefEmitPermissionSet_t1_463_0_0_0;
static const Il2CppType* GenInst_RefEmitPermissionSet_t1_463_0_0_0_Types[] = { &RefEmitPermissionSet_t1_463_0_0_0 };
extern const Il2CppGenericInst GenInst_RefEmitPermissionSet_t1_463_0_0_0 = { 1, GenInst_RefEmitPermissionSet_t1_463_0_0_0_Types };
extern const Il2CppType ICONDIRENTRY_t1_667_0_0_0;
static const Il2CppType* GenInst_ICONDIRENTRY_t1_667_0_0_0_Types[] = { &ICONDIRENTRY_t1_667_0_0_0 };
extern const Il2CppGenericInst GenInst_ICONDIRENTRY_t1_667_0_0_0 = { 1, GenInst_ICONDIRENTRY_t1_667_0_0_0_Types };
extern const Il2CppType Win32IconResource_t1_666_0_0_0;
static const Il2CppType* GenInst_Win32IconResource_t1_666_0_0_0_Types[] = { &Win32IconResource_t1_666_0_0_0 };
extern const Il2CppGenericInst GenInst_Win32IconResource_t1_666_0_0_0 = { 1, GenInst_Win32IconResource_t1_666_0_0_0_Types };
extern const Il2CppType Win32Resource_t1_664_0_0_0;
static const Il2CppType* GenInst_Win32Resource_t1_664_0_0_0_Types[] = { &Win32Resource_t1_664_0_0_0 };
extern const Il2CppGenericInst GenInst_Win32Resource_t1_664_0_0_0 = { 1, GenInst_Win32Resource_t1_664_0_0_0_Types };
extern const Il2CppType FileStream_t1_146_0_0_0;
static const Il2CppType* GenInst_FileStream_t1_146_0_0_0_Types[] = { &FileStream_t1_146_0_0_0 };
extern const Il2CppGenericInst GenInst_FileStream_t1_146_0_0_0 = { 1, GenInst_FileStream_t1_146_0_0_0_Types };
extern const Il2CppType Stream_t1_405_0_0_0;
static const Il2CppType* GenInst_Stream_t1_405_0_0_0_Types[] = { &Stream_t1_405_0_0_0 };
extern const Il2CppGenericInst GenInst_Stream_t1_405_0_0_0 = { 1, GenInst_Stream_t1_405_0_0_0_Types };
extern const Il2CppType IDisposable_t1_1035_0_0_0;
static const Il2CppType* GenInst_IDisposable_t1_1035_0_0_0_Types[] = { &IDisposable_t1_1035_0_0_0 };
extern const Il2CppGenericInst GenInst_IDisposable_t1_1035_0_0_0 = { 1, GenInst_IDisposable_t1_1035_0_0_0_Types };
extern const Il2CppType ParameterBuilder_t1_545_0_0_0;
static const Il2CppType* GenInst_ParameterBuilder_t1_545_0_0_0_Types[] = { &ParameterBuilder_t1_545_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterBuilder_t1_545_0_0_0 = { 1, GenInst_ParameterBuilder_t1_545_0_0_0_Types };
extern const Il2CppType _ParameterBuilder_t1_3123_0_0_0;
static const Il2CppType* GenInst__ParameterBuilder_t1_3123_0_0_0_Types[] = { &_ParameterBuilder_t1_3123_0_0_0 };
extern const Il2CppGenericInst GenInst__ParameterBuilder_t1_3123_0_0_0 = { 1, GenInst__ParameterBuilder_t1_3123_0_0_0_Types };
extern const Il2CppType TypeU5BU5D_t1_31_0_0_0;
static const Il2CppType* GenInst_TypeU5BU5D_t1_31_0_0_0_Types[] = { &TypeU5BU5D_t1_31_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeU5BU5D_t1_31_0_0_0 = { 1, GenInst_TypeU5BU5D_t1_31_0_0_0_Types };
extern const Il2CppType Array_t_0_0_0;
static const Il2CppType* GenInst_Array_t_0_0_0_Types[] = { &Array_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_t_0_0_0 = { 1, GenInst_Array_t_0_0_0_Types };
extern const Il2CppType ICollection_t1_280_0_0_0;
static const Il2CppType* GenInst_ICollection_t1_280_0_0_0_Types[] = { &ICollection_t1_280_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_t1_280_0_0_0 = { 1, GenInst_ICollection_t1_280_0_0_0_Types };
extern const Il2CppType MethodBuilder_t1_501_0_0_0;
static const Il2CppType* GenInst_MethodBuilder_t1_501_0_0_0_Types[] = { &MethodBuilder_t1_501_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodBuilder_t1_501_0_0_0 = { 1, GenInst_MethodBuilder_t1_501_0_0_0_Types };
extern const Il2CppType _MethodBuilder_t1_3120_0_0_0;
static const Il2CppType* GenInst__MethodBuilder_t1_3120_0_0_0_Types[] = { &_MethodBuilder_t1_3120_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodBuilder_t1_3120_0_0_0 = { 1, GenInst__MethodBuilder_t1_3120_0_0_0_Types };
extern const Il2CppType ILExceptionBlock_t1_510_0_0_0;
static const Il2CppType* GenInst_ILExceptionBlock_t1_510_0_0_0_Types[] = { &ILExceptionBlock_t1_510_0_0_0 };
extern const Il2CppGenericInst GenInst_ILExceptionBlock_t1_510_0_0_0 = { 1, GenInst_ILExceptionBlock_t1_510_0_0_0_Types };
extern const Il2CppType LocalBuilder_t1_527_0_0_0;
static const Il2CppType* GenInst_LocalBuilder_t1_527_0_0_0_Types[] = { &LocalBuilder_t1_527_0_0_0 };
extern const Il2CppGenericInst GenInst_LocalBuilder_t1_527_0_0_0 = { 1, GenInst_LocalBuilder_t1_527_0_0_0_Types };
extern const Il2CppType _LocalBuilder_t1_3119_0_0_0;
static const Il2CppType* GenInst__LocalBuilder_t1_3119_0_0_0_Types[] = { &_LocalBuilder_t1_3119_0_0_0 };
extern const Il2CppGenericInst GenInst__LocalBuilder_t1_3119_0_0_0 = { 1, GenInst__LocalBuilder_t1_3119_0_0_0_Types };
extern const Il2CppType ILExceptionInfo_t1_511_0_0_0;
static const Il2CppType* GenInst_ILExceptionInfo_t1_511_0_0_0_Types[] = { &ILExceptionInfo_t1_511_0_0_0 };
extern const Il2CppGenericInst GenInst_ILExceptionInfo_t1_511_0_0_0 = { 1, GenInst_ILExceptionInfo_t1_511_0_0_0_Types };
extern const Il2CppType ILTokenInfo_t1_514_0_0_0;
static const Il2CppType* GenInst_ILTokenInfo_t1_514_0_0_0_Types[] = { &ILTokenInfo_t1_514_0_0_0 };
extern const Il2CppGenericInst GenInst_ILTokenInfo_t1_514_0_0_0 = { 1, GenInst_ILTokenInfo_t1_514_0_0_0_Types };
extern const Il2CppType LabelData_t1_516_0_0_0;
static const Il2CppType* GenInst_LabelData_t1_516_0_0_0_Types[] = { &LabelData_t1_516_0_0_0 };
extern const Il2CppGenericInst GenInst_LabelData_t1_516_0_0_0 = { 1, GenInst_LabelData_t1_516_0_0_0_Types };
extern const Il2CppType LabelFixup_t1_515_0_0_0;
static const Il2CppType* GenInst_LabelFixup_t1_515_0_0_0_Types[] = { &LabelFixup_t1_515_0_0_0 };
extern const Il2CppGenericInst GenInst_LabelFixup_t1_515_0_0_0 = { 1, GenInst_LabelFixup_t1_515_0_0_0_Types };
extern const Il2CppType Label_t1_513_0_0_0;
static const Il2CppType* GenInst_Label_t1_513_0_0_0_Types[] = { &Label_t1_513_0_0_0 };
extern const Il2CppGenericInst GenInst_Label_t1_513_0_0_0 = { 1, GenInst_Label_t1_513_0_0_0_Types };
extern const Il2CppType SequencePoint_t1_526_0_0_0;
static const Il2CppType* GenInst_SequencePoint_t1_526_0_0_0_Types[] = { &SequencePoint_t1_526_0_0_0 };
extern const Il2CppGenericInst GenInst_SequencePoint_t1_526_0_0_0 = { 1, GenInst_SequencePoint_t1_526_0_0_0_Types };
extern const Il2CppType GenericTypeParameterBuilder_t1_509_0_0_0;
static const Il2CppType* GenInst_GenericTypeParameterBuilder_t1_509_0_0_0_Types[] = { &GenericTypeParameterBuilder_t1_509_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericTypeParameterBuilder_t1_509_0_0_0 = { 1, GenInst_GenericTypeParameterBuilder_t1_509_0_0_0_Types };
extern const Il2CppType TypeBuilder_t1_481_0_0_0;
static const Il2CppType* GenInst_TypeBuilder_t1_481_0_0_0_Types[] = { &TypeBuilder_t1_481_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeBuilder_t1_481_0_0_0 = { 1, GenInst_TypeBuilder_t1_481_0_0_0_Types };
extern const Il2CppType _TypeBuilder_t1_3126_0_0_0;
static const Il2CppType* GenInst__TypeBuilder_t1_3126_0_0_0_Types[] = { &_TypeBuilder_t1_3126_0_0_0 };
extern const Il2CppGenericInst GenInst__TypeBuilder_t1_3126_0_0_0 = { 1, GenInst__TypeBuilder_t1_3126_0_0_0_Types };
extern const Il2CppType FieldBuilder_t1_499_0_0_0;
static const Il2CppType* GenInst_FieldBuilder_t1_499_0_0_0_Types[] = { &FieldBuilder_t1_499_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldBuilder_t1_499_0_0_0 = { 1, GenInst_FieldBuilder_t1_499_0_0_0_Types };
extern const Il2CppType _FieldBuilder_t1_3117_0_0_0;
static const Il2CppType* GenInst__FieldBuilder_t1_3117_0_0_0_Types[] = { &_FieldBuilder_t1_3117_0_0_0 };
extern const Il2CppGenericInst GenInst__FieldBuilder_t1_3117_0_0_0 = { 1, GenInst__FieldBuilder_t1_3117_0_0_0_Types };
extern const Il2CppType ConstructorBuilder_t1_477_0_0_0;
static const Il2CppType* GenInst_ConstructorBuilder_t1_477_0_0_0_Types[] = { &ConstructorBuilder_t1_477_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorBuilder_t1_477_0_0_0 = { 1, GenInst_ConstructorBuilder_t1_477_0_0_0_Types };
extern const Il2CppType _ConstructorBuilder_t1_3113_0_0_0;
static const Il2CppType* GenInst__ConstructorBuilder_t1_3113_0_0_0_Types[] = { &_ConstructorBuilder_t1_3113_0_0_0 };
extern const Il2CppGenericInst GenInst__ConstructorBuilder_t1_3113_0_0_0 = { 1, GenInst__ConstructorBuilder_t1_3113_0_0_0_Types };
extern const Il2CppType PropertyBuilder_t1_547_0_0_0;
static const Il2CppType* GenInst_PropertyBuilder_t1_547_0_0_0_Types[] = { &PropertyBuilder_t1_547_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyBuilder_t1_547_0_0_0 = { 1, GenInst_PropertyBuilder_t1_547_0_0_0_Types };
extern const Il2CppType _PropertyBuilder_t1_3124_0_0_0;
static const Il2CppType* GenInst__PropertyBuilder_t1_3124_0_0_0_Types[] = { &_PropertyBuilder_t1_3124_0_0_0 };
extern const Il2CppGenericInst GenInst__PropertyBuilder_t1_3124_0_0_0 = { 1, GenInst__PropertyBuilder_t1_3124_0_0_0_Types };
extern const Il2CppType EventBuilder_t1_500_0_0_0;
static const Il2CppType* GenInst_EventBuilder_t1_500_0_0_0_Types[] = { &EventBuilder_t1_500_0_0_0 };
extern const Il2CppGenericInst GenInst_EventBuilder_t1_500_0_0_0 = { 1, GenInst_EventBuilder_t1_500_0_0_0_Types };
extern const Il2CppType _EventBuilder_t1_3116_0_0_0;
static const Il2CppType* GenInst__EventBuilder_t1_3116_0_0_0_Types[] = { &_EventBuilder_t1_3116_0_0_0 };
extern const Il2CppGenericInst GenInst__EventBuilder_t1_3116_0_0_0 = { 1, GenInst__EventBuilder_t1_3116_0_0_0_Types };
extern const Il2CppType AssemblyName_t1_576_0_0_0;
static const Il2CppType* GenInst_AssemblyName_t1_576_0_0_0_Types[] = { &AssemblyName_t1_576_0_0_0 };
extern const Il2CppGenericInst GenInst_AssemblyName_t1_576_0_0_0 = { 1, GenInst_AssemblyName_t1_576_0_0_0_Types };
extern const Il2CppType _AssemblyName_t1_3128_0_0_0;
static const Il2CppType* GenInst__AssemblyName_t1_3128_0_0_0_Types[] = { &_AssemblyName_t1_3128_0_0_0 };
extern const Il2CppGenericInst GenInst__AssemblyName_t1_3128_0_0_0 = { 1, GenInst__AssemblyName_t1_3128_0_0_0_Types };
extern const Il2CppType IDeserializationCallback_t1_1781_0_0_0;
static const Il2CppType* GenInst_IDeserializationCallback_t1_1781_0_0_0_Types[] = { &IDeserializationCallback_t1_1781_0_0_0 };
extern const Il2CppGenericInst GenInst_IDeserializationCallback_t1_1781_0_0_0 = { 1, GenInst_IDeserializationCallback_t1_1781_0_0_0_Types };
extern const Il2CppType DecimalConstantAttribute_t1_63_0_0_0;
static const Il2CppType* GenInst_DecimalConstantAttribute_t1_63_0_0_0_Types[] = { &DecimalConstantAttribute_t1_63_0_0_0 };
extern const Il2CppGenericInst GenInst_DecimalConstantAttribute_t1_63_0_0_0 = { 1, GenInst_DecimalConstantAttribute_t1_63_0_0_0_Types };
extern const Il2CppType DateTimeConstantAttribute_t1_682_0_0_0;
static const Il2CppType* GenInst_DateTimeConstantAttribute_t1_682_0_0_0_Types[] = { &DateTimeConstantAttribute_t1_682_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTimeConstantAttribute_t1_682_0_0_0 = { 1, GenInst_DateTimeConstantAttribute_t1_682_0_0_0_Types };
extern const Il2CppType CustomConstantAttribute_t1_681_0_0_0;
static const Il2CppType* GenInst_CustomConstantAttribute_t1_681_0_0_0_Types[] = { &CustomConstantAttribute_t1_681_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomConstantAttribute_t1_681_0_0_0 = { 1, GenInst_CustomConstantAttribute_t1_681_0_0_0_Types };
extern const Il2CppType Exception_t1_33_0_0_0;
static const Il2CppType* GenInst_Exception_t1_33_0_0_0_Types[] = { &Exception_t1_33_0_0_0 };
extern const Il2CppGenericInst GenInst_Exception_t1_33_0_0_0 = { 1, GenInst_Exception_t1_33_0_0_0_Types };
extern const Il2CppType _Exception_t1_3078_0_0_0;
static const Il2CppType* GenInst__Exception_t1_3078_0_0_0_Types[] = { &_Exception_t1_3078_0_0_0 };
extern const Il2CppGenericInst GenInst__Exception_t1_3078_0_0_0 = { 1, GenInst__Exception_t1_3078_0_0_0_Types };
extern const Il2CppType ResourceInfo_t1_647_0_0_0;
static const Il2CppType* GenInst_ResourceInfo_t1_647_0_0_0_Types[] = { &ResourceInfo_t1_647_0_0_0 };
extern const Il2CppGenericInst GenInst_ResourceInfo_t1_647_0_0_0 = { 1, GenInst_ResourceInfo_t1_647_0_0_0_Types };
extern const Il2CppType ResourceCacheItem_t1_648_0_0_0;
static const Il2CppType* GenInst_ResourceCacheItem_t1_648_0_0_0_Types[] = { &ResourceCacheItem_t1_648_0_0_0 };
extern const Il2CppGenericInst GenInst_ResourceCacheItem_t1_648_0_0_0 = { 1, GenInst_ResourceCacheItem_t1_648_0_0_0_Types };
extern const Il2CppType IConnectionPoint_t1_3138_0_0_0;
static const Il2CppType* GenInst_IConnectionPoint_t1_3138_0_0_0_Types[] = { &IConnectionPoint_t1_3138_0_0_0 };
extern const Il2CppGenericInst GenInst_IConnectionPoint_t1_3138_0_0_0 = { 1, GenInst_IConnectionPoint_t1_3138_0_0_0_Types };
extern const Il2CppType CONNECTDATA_t1_723_0_0_0;
static const Il2CppType* GenInst_CONNECTDATA_t1_723_0_0_0_Types[] = { &CONNECTDATA_t1_723_0_0_0 };
extern const Il2CppGenericInst GenInst_CONNECTDATA_t1_723_0_0_0 = { 1, GenInst_CONNECTDATA_t1_723_0_0_0_Types };
extern const Il2CppType IMoniker_t1_3145_0_0_0;
static const Il2CppType* GenInst_IMoniker_t1_3145_0_0_0_Types[] = { &IMoniker_t1_3145_0_0_0 };
extern const Il2CppGenericInst GenInst_IMoniker_t1_3145_0_0_0 = { 1, GenInst_IMoniker_t1_3145_0_0_0_Types };
extern const Il2CppType ITypeInfo_t1_1697_0_0_0;
static const Il2CppType* GenInst_ITypeInfo_t1_1697_0_0_0_Types[] = { &ITypeInfo_t1_1697_0_0_0 };
extern const Il2CppGenericInst GenInst_ITypeInfo_t1_1697_0_0_0 = { 1, GenInst_ITypeInfo_t1_1697_0_0_0_Types };
extern const Il2CppType UCOMIConnectionPoint_t1_3160_0_0_0;
static const Il2CppType* GenInst_UCOMIConnectionPoint_t1_3160_0_0_0_Types[] = { &UCOMIConnectionPoint_t1_3160_0_0_0 };
extern const Il2CppGenericInst GenInst_UCOMIConnectionPoint_t1_3160_0_0_0 = { 1, GenInst_UCOMIConnectionPoint_t1_3160_0_0_0_Types };
extern const Il2CppType CONNECTDATA_t1_762_0_0_0;
static const Il2CppType* GenInst_CONNECTDATA_t1_762_0_0_0_Types[] = { &CONNECTDATA_t1_762_0_0_0 };
extern const Il2CppGenericInst GenInst_CONNECTDATA_t1_762_0_0_0 = { 1, GenInst_CONNECTDATA_t1_762_0_0_0_Types };
extern const Il2CppType UCOMIMoniker_t1_3167_0_0_0;
static const Il2CppType* GenInst_UCOMIMoniker_t1_3167_0_0_0_Types[] = { &UCOMIMoniker_t1_3167_0_0_0 };
extern const Il2CppGenericInst GenInst_UCOMIMoniker_t1_3167_0_0_0 = { 1, GenInst_UCOMIMoniker_t1_3167_0_0_0_Types };
extern const Il2CppType UCOMITypeInfo_t1_1696_0_0_0;
static const Il2CppType* GenInst_UCOMITypeInfo_t1_1696_0_0_0_Types[] = { &UCOMITypeInfo_t1_1696_0_0_0 };
extern const Il2CppGenericInst GenInst_UCOMITypeInfo_t1_1696_0_0_0 = { 1, GenInst_UCOMITypeInfo_t1_1696_0_0_0_Types };
extern const Il2CppType IChannel_t1_1710_0_0_0;
static const Il2CppType* GenInst_IChannel_t1_1710_0_0_0_Types[] = { &IChannel_t1_1710_0_0_0 };
extern const Il2CppGenericInst GenInst_IChannel_t1_1710_0_0_0 = { 1, GenInst_IChannel_t1_1710_0_0_0_Types };
extern const Il2CppType IContextProperty_t1_1716_0_0_0;
static const Il2CppType* GenInst_IContextProperty_t1_1716_0_0_0_Types[] = { &IContextProperty_t1_1716_0_0_0 };
extern const Il2CppGenericInst GenInst_IContextProperty_t1_1716_0_0_0 = { 1, GenInst_IContextProperty_t1_1716_0_0_0_Types };
extern const Il2CppType IComparable_1_t1_3465_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1_3465_0_0_0_Types[] = { &IComparable_1_t1_3465_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1_3465_0_0_0 = { 1, GenInst_IComparable_1_t1_3465_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1_3466_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1_3466_0_0_0_Types[] = { &IEquatable_1_t1_3466_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1_3466_0_0_0 = { 1, GenInst_IEquatable_1_t1_3466_0_0_0_Types };
extern const Il2CppType Header_t1_939_0_0_0;
static const Il2CppType* GenInst_Header_t1_939_0_0_0_Types[] = { &Header_t1_939_0_0_0 };
extern const Il2CppGenericInst GenInst_Header_t1_939_0_0_0 = { 1, GenInst_Header_t1_939_0_0_0_Types };
extern const Il2CppType ITrackingHandler_t1_1721_0_0_0;
static const Il2CppType* GenInst_ITrackingHandler_t1_1721_0_0_0_Types[] = { &ITrackingHandler_t1_1721_0_0_0 };
extern const Il2CppGenericInst GenInst_ITrackingHandler_t1_1721_0_0_0 = { 1, GenInst_ITrackingHandler_t1_1721_0_0_0_Types };
extern const Il2CppType IContextAttribute_t1_1764_0_0_0;
static const Il2CppType* GenInst_IContextAttribute_t1_1764_0_0_0_Types[] = { &IContextAttribute_t1_1764_0_0_0 };
extern const Il2CppGenericInst GenInst_IContextAttribute_t1_1764_0_0_0 = { 1, GenInst_IContextAttribute_t1_1764_0_0_0_Types };
extern const Il2CppType ActivatedClientTypeEntry_t1_1008_0_0_0;
static const Il2CppType* GenInst_ActivatedClientTypeEntry_t1_1008_0_0_0_Types[] = { &ActivatedClientTypeEntry_t1_1008_0_0_0 };
extern const Il2CppGenericInst GenInst_ActivatedClientTypeEntry_t1_1008_0_0_0 = { 1, GenInst_ActivatedClientTypeEntry_t1_1008_0_0_0_Types };
extern const Il2CppType TypeEntry_t1_1009_0_0_0;
static const Il2CppType* GenInst_TypeEntry_t1_1009_0_0_0_Types[] = { &TypeEntry_t1_1009_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeEntry_t1_1009_0_0_0 = { 1, GenInst_TypeEntry_t1_1009_0_0_0_Types };
extern const Il2CppType ActivatedServiceTypeEntry_t1_1010_0_0_0;
static const Il2CppType* GenInst_ActivatedServiceTypeEntry_t1_1010_0_0_0_Types[] = { &ActivatedServiceTypeEntry_t1_1010_0_0_0 };
extern const Il2CppGenericInst GenInst_ActivatedServiceTypeEntry_t1_1010_0_0_0 = { 1, GenInst_ActivatedServiceTypeEntry_t1_1010_0_0_0_Types };
extern const Il2CppType WellKnownClientTypeEntry_t1_1039_0_0_0;
static const Il2CppType* GenInst_WellKnownClientTypeEntry_t1_1039_0_0_0_Types[] = { &WellKnownClientTypeEntry_t1_1039_0_0_0 };
extern const Il2CppGenericInst GenInst_WellKnownClientTypeEntry_t1_1039_0_0_0 = { 1, GenInst_WellKnownClientTypeEntry_t1_1039_0_0_0_Types };
extern const Il2CppType WellKnownServiceTypeEntry_t1_1041_0_0_0;
static const Il2CppType* GenInst_WellKnownServiceTypeEntry_t1_1041_0_0_0_Types[] = { &WellKnownServiceTypeEntry_t1_1041_0_0_0 };
extern const Il2CppGenericInst GenInst_WellKnownServiceTypeEntry_t1_1041_0_0_0 = { 1, GenInst_WellKnownServiceTypeEntry_t1_1041_0_0_0_Types };
extern const Il2CppType IComparable_1_t1_4020_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1_4020_0_0_0_Types[] = { &IComparable_1_t1_4020_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1_4020_0_0_0 = { 1, GenInst_IComparable_1_t1_4020_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1_4025_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1_4025_0_0_0_Types[] = { &IEquatable_1_t1_4025_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1_4025_0_0_0 = { 1, GenInst_IEquatable_1_t1_4025_0_0_0_Types };
extern const Il2CppType IComparable_1_t1_4033_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1_4033_0_0_0_Types[] = { &IComparable_1_t1_4033_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1_4033_0_0_0 = { 1, GenInst_IComparable_1_t1_4033_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1_4038_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1_4038_0_0_0_Types[] = { &IEquatable_1_t1_4038_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1_4038_0_0_0 = { 1, GenInst_IEquatable_1_t1_4038_0_0_0_Types };
extern const Il2CppType TypeTag_t1_1044_0_0_0;
static const Il2CppType* GenInst_TypeTag_t1_1044_0_0_0_Types[] = { &TypeTag_t1_1044_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeTag_t1_1044_0_0_0 = { 1, GenInst_TypeTag_t1_1044_0_0_0_Types };
extern const Il2CppType Enum_t1_24_0_0_0;
static const Il2CppType* GenInst_Enum_t1_24_0_0_0_Types[] = { &Enum_t1_24_0_0_0 };
extern const Il2CppGenericInst GenInst_Enum_t1_24_0_0_0 = { 1, GenInst_Enum_t1_24_0_0_0_Types };
extern const Il2CppType MonoType_t_0_0_0;
static const Il2CppType* GenInst_MonoType_t_0_0_0_Types[] = { &MonoType_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoType_t_0_0_0 = { 1, GenInst_MonoType_t_0_0_0_Types };
extern const Il2CppType SerializationEntry_t1_1093_0_0_0;
static const Il2CppType* GenInst_SerializationEntry_t1_1093_0_0_0_Types[] = { &SerializationEntry_t1_1093_0_0_0 };
extern const Il2CppGenericInst GenInst_SerializationEntry_t1_1093_0_0_0 = { 1, GenInst_SerializationEntry_t1_1093_0_0_0_Types };
extern const Il2CppType AuthorizationRule_t1_1112_0_0_0;
static const Il2CppType* GenInst_AuthorizationRule_t1_1112_0_0_0_Types[] = { &AuthorizationRule_t1_1112_0_0_0 };
extern const Il2CppGenericInst GenInst_AuthorizationRule_t1_1112_0_0_0 = { 1, GenInst_AuthorizationRule_t1_1112_0_0_0_Types };
extern const Il2CppType KeyContainerPermissionAccessEntry_t1_1290_0_0_0;
static const Il2CppType* GenInst_KeyContainerPermissionAccessEntry_t1_1290_0_0_0_Types[] = { &KeyContainerPermissionAccessEntry_t1_1290_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyContainerPermissionAccessEntry_t1_1290_0_0_0 = { 1, GenInst_KeyContainerPermissionAccessEntry_t1_1290_0_0_0_Types };
extern const Il2CppType ApplicationTrust_t1_1333_0_0_0;
static const Il2CppType* GenInst_ApplicationTrust_t1_1333_0_0_0_Types[] = { &ApplicationTrust_t1_1333_0_0_0 };
extern const Il2CppGenericInst GenInst_ApplicationTrust_t1_1333_0_0_0 = { 1, GenInst_ApplicationTrust_t1_1333_0_0_0_Types };
extern const Il2CppType ISecurityEncodable_t1_1788_0_0_0;
static const Il2CppType* GenInst_ISecurityEncodable_t1_1788_0_0_0_Types[] = { &ISecurityEncodable_t1_1788_0_0_0 };
extern const Il2CppGenericInst GenInst_ISecurityEncodable_t1_1788_0_0_0 = { 1, GenInst_ISecurityEncodable_t1_1788_0_0_0_Types };
extern const Il2CppType CodeConnectAccess_t1_1338_0_0_0;
static const Il2CppType* GenInst_CodeConnectAccess_t1_1338_0_0_0_Types[] = { &CodeConnectAccess_t1_1338_0_0_0 };
extern const Il2CppGenericInst GenInst_CodeConnectAccess_t1_1338_0_0_0 = { 1, GenInst_CodeConnectAccess_t1_1338_0_0_0_Types };
extern const Il2CppType IdentityReference_t1_1120_0_0_0;
static const Il2CppType* GenInst_IdentityReference_t1_1120_0_0_0_Types[] = { &IdentityReference_t1_1120_0_0_0 };
extern const Il2CppGenericInst GenInst_IdentityReference_t1_1120_0_0_0 = { 1, GenInst_IdentityReference_t1_1120_0_0_0_Types };
extern const Il2CppType SecurityIdentifier_t1_1132_0_0_0;
static const Il2CppType* GenInst_SecurityIdentifier_t1_1132_0_0_0_Types[] = { &SecurityIdentifier_t1_1132_0_0_0 };
extern const Il2CppGenericInst GenInst_SecurityIdentifier_t1_1132_0_0_0 = { 1, GenInst_SecurityIdentifier_t1_1132_0_0_0_Types };
extern const Il2CppType Evidence_t1_398_0_0_0;
static const Il2CppType* GenInst_Evidence_t1_398_0_0_0_Types[] = { &Evidence_t1_398_0_0_0 };
extern const Il2CppGenericInst GenInst_Evidence_t1_398_0_0_0 = { 1, GenInst_Evidence_t1_398_0_0_0_Types };
extern const Il2CppType EncodingInfo_t1_1438_0_0_0;
static const Il2CppType* GenInst_EncodingInfo_t1_1438_0_0_0_Types[] = { &EncodingInfo_t1_1438_0_0_0 };
extern const Il2CppGenericInst GenInst_EncodingInfo_t1_1438_0_0_0 = { 1, GenInst_EncodingInfo_t1_1438_0_0_0_Types };
extern const Il2CppType WaitHandle_t1_917_0_0_0;
static const Il2CppType* GenInst_WaitHandle_t1_917_0_0_0_Types[] = { &WaitHandle_t1_917_0_0_0 };
extern const Il2CppGenericInst GenInst_WaitHandle_t1_917_0_0_0 = { 1, GenInst_WaitHandle_t1_917_0_0_0_Types };
extern const Il2CppType Assembly_t1_467_0_0_0;
static const Il2CppType* GenInst_Assembly_t1_467_0_0_0_Types[] = { &Assembly_t1_467_0_0_0 };
extern const Il2CppGenericInst GenInst_Assembly_t1_467_0_0_0 = { 1, GenInst_Assembly_t1_467_0_0_0_Types };
extern const Il2CppType _Assembly_t1_3127_0_0_0;
static const Il2CppType* GenInst__Assembly_t1_3127_0_0_0_Types[] = { &_Assembly_t1_3127_0_0_0 };
extern const Il2CppGenericInst GenInst__Assembly_t1_3127_0_0_0 = { 1, GenInst__Assembly_t1_3127_0_0_0_Types };
extern const Il2CppType Version_t1_578_0_0_0;
static const Il2CppType* GenInst_Version_t1_578_0_0_0_Types[] = { &Version_t1_578_0_0_0 };
extern const Il2CppGenericInst GenInst_Version_t1_578_0_0_0 = { 1, GenInst_Version_t1_578_0_0_0_Types };
extern const Il2CppType BigInteger_t2_4_0_0_0;
static const Il2CppType* GenInst_BigInteger_t2_4_0_0_0_Types[] = { &BigInteger_t2_4_0_0_0 };
extern const Il2CppGenericInst GenInst_BigInteger_t2_4_0_0_0 = { 1, GenInst_BigInteger_t2_4_0_0_0_Types };
extern const Il2CppType X509Certificate_t1_1179_0_0_0;
static const Il2CppType* GenInst_X509Certificate_t1_1179_0_0_0_Types[] = { &X509Certificate_t1_1179_0_0_0 };
extern const Il2CppGenericInst GenInst_X509Certificate_t1_1179_0_0_0 = { 1, GenInst_X509Certificate_t1_1179_0_0_0_Types };
extern const Il2CppType ClientCertificateType_t2_98_0_0_0;
static const Il2CppType* GenInst_ClientCertificateType_t2_98_0_0_0_Types[] = { &ClientCertificateType_t2_98_0_0_0 };
extern const Il2CppGenericInst GenInst_ClientCertificateType_t2_98_0_0_0 = { 1, GenInst_ClientCertificateType_t2_98_0_0_0_Types };
extern const Il2CppType Entry_t4_107_0_0_0;
static const Il2CppType* GenInst_Entry_t4_107_0_0_0_Types[] = { &Entry_t4_107_0_0_0 };
extern const Il2CppGenericInst GenInst_Entry_t4_107_0_0_0 = { 1, GenInst_Entry_t4_107_0_0_0_Types };
extern const Il2CppType XmlNode_t4_116_0_0_0;
static const Il2CppType* GenInst_XmlNode_t4_116_0_0_0_Types[] = { &XmlNode_t4_116_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlNode_t4_116_0_0_0 = { 1, GenInst_XmlNode_t4_116_0_0_0_Types };
extern const Il2CppType IXPathNavigable_t4_197_0_0_0;
static const Il2CppType* GenInst_IXPathNavigable_t4_197_0_0_0_Types[] = { &IXPathNavigable_t4_197_0_0_0 };
extern const Il2CppGenericInst GenInst_IXPathNavigable_t4_197_0_0_0 = { 1, GenInst_IXPathNavigable_t4_197_0_0_0_Types };
extern const Il2CppType NsDecl_t4_141_0_0_0;
static const Il2CppType* GenInst_NsDecl_t4_141_0_0_0_Types[] = { &NsDecl_t4_141_0_0_0 };
extern const Il2CppGenericInst GenInst_NsDecl_t4_141_0_0_0 = { 1, GenInst_NsDecl_t4_141_0_0_0_Types };
extern const Il2CppType NsScope_t4_142_0_0_0;
static const Il2CppType* GenInst_NsScope_t4_142_0_0_0_Types[] = { &NsScope_t4_142_0_0_0 };
extern const Il2CppGenericInst GenInst_NsScope_t4_142_0_0_0 = { 1, GenInst_NsScope_t4_142_0_0_0_Types };
extern const Il2CppType XmlAttributeTokenInfo_t4_170_0_0_0;
static const Il2CppType* GenInst_XmlAttributeTokenInfo_t4_170_0_0_0_Types[] = { &XmlAttributeTokenInfo_t4_170_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlAttributeTokenInfo_t4_170_0_0_0 = { 1, GenInst_XmlAttributeTokenInfo_t4_170_0_0_0_Types };
extern const Il2CppType XmlTokenInfo_t4_168_0_0_0;
static const Il2CppType* GenInst_XmlTokenInfo_t4_168_0_0_0_Types[] = { &XmlTokenInfo_t4_168_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlTokenInfo_t4_168_0_0_0 = { 1, GenInst_XmlTokenInfo_t4_168_0_0_0_Types };
extern const Il2CppType TagName_t4_171_0_0_0;
static const Il2CppType* GenInst_TagName_t4_171_0_0_0_Types[] = { &TagName_t4_171_0_0_0 };
extern const Il2CppGenericInst GenInst_TagName_t4_171_0_0_0 = { 1, GenInst_TagName_t4_171_0_0_0_Types };
extern const Il2CppType XmlNodeInfo_t4_178_0_0_0;
static const Il2CppType* GenInst_XmlNodeInfo_t4_178_0_0_0_Types[] = { &XmlNodeInfo_t4_178_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlNodeInfo_t4_178_0_0_0 = { 1, GenInst_XmlNodeInfo_t4_178_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Boolean_t1_20_0_0_0_Types[] = { &Object_t_0_0_0, &Boolean_t1_20_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Boolean_t1_20_0_0_0 = { 2, GenInst_Object_t_0_0_0_Boolean_t1_20_0_0_0_Types };
extern const Il2CppType X509ChainStatus_t3_157_0_0_0;
static const Il2CppType* GenInst_X509ChainStatus_t3_157_0_0_0_Types[] = { &X509ChainStatus_t3_157_0_0_0 };
extern const Il2CppGenericInst GenInst_X509ChainStatus_t3_157_0_0_0 = { 1, GenInst_X509ChainStatus_t3_157_0_0_0_Types };
extern const Il2CppType IPAddress_t3_121_0_0_0;
static const Il2CppType* GenInst_IPAddress_t3_121_0_0_0_Types[] = { &IPAddress_t3_121_0_0_0 };
extern const Il2CppGenericInst GenInst_IPAddress_t3_121_0_0_0 = { 1, GenInst_IPAddress_t3_121_0_0_0_Types };
extern const Il2CppType ArraySegment_1_t1_2214_0_0_0;
static const Il2CppType* GenInst_ArraySegment_1_t1_2214_0_0_0_Types[] = { &ArraySegment_1_t1_2214_0_0_0 };
extern const Il2CppGenericInst GenInst_ArraySegment_1_t1_2214_0_0_0 = { 1, GenInst_ArraySegment_1_t1_2214_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_2223_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_2223_0_0_0_Types[] = { &KeyValuePair_2_t1_2223_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_2223_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_2223_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Boolean_t1_20_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &Boolean_t1_20_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Boolean_t1_20_0_0_0_Object_t_0_0_0 = { 3, GenInst_Object_t_0_0_0_Boolean_t1_20_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Boolean_t1_20_0_0_0_Boolean_t1_20_0_0_0_Types[] = { &Object_t_0_0_0, &Boolean_t1_20_0_0_0, &Boolean_t1_20_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Boolean_t1_20_0_0_0_Boolean_t1_20_0_0_0 = { 3, GenInst_Object_t_0_0_0_Boolean_t1_20_0_0_0_Boolean_t1_20_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Boolean_t1_20_0_0_0_DictionaryEntry_t1_284_0_0_0_Types[] = { &Object_t_0_0_0, &Boolean_t1_20_0_0_0, &DictionaryEntry_t1_284_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Boolean_t1_20_0_0_0_DictionaryEntry_t1_284_0_0_0 = { 3, GenInst_Object_t_0_0_0_Boolean_t1_20_0_0_0_DictionaryEntry_t1_284_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Boolean_t1_20_0_0_0_KeyValuePair_2_t1_2223_0_0_0_Types[] = { &Object_t_0_0_0, &Boolean_t1_20_0_0_0, &KeyValuePair_2_t1_2223_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Boolean_t1_20_0_0_0_KeyValuePair_2_t1_2223_0_0_0 = { 3, GenInst_Object_t_0_0_0_Boolean_t1_20_0_0_0_KeyValuePair_2_t1_2223_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Boolean_t1_20_0_0_0_DictionaryEntry_t1_284_0_0_0_Types[] = { &String_t_0_0_0, &Boolean_t1_20_0_0_0, &DictionaryEntry_t1_284_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t1_20_0_0_0_DictionaryEntry_t1_284_0_0_0 = { 3, GenInst_String_t_0_0_0_Boolean_t1_20_0_0_0_DictionaryEntry_t1_284_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_2237_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_2237_0_0_0_Types[] = { &KeyValuePair_2_t1_2237_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_2237_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_2237_0_0_0_Types };
extern const Il2CppType Capture_t3_176_0_0_0;
static const Il2CppType* GenInst_Capture_t3_176_0_0_0_Types[] = { &Capture_t3_176_0_0_0 };
extern const Il2CppGenericInst GenInst_Capture_t3_176_0_0_0 = { 1, GenInst_Capture_t3_176_0_0_0_Types };
extern const Il2CppType Group_t3_15_0_0_0;
static const Il2CppType* GenInst_Group_t3_15_0_0_0_Types[] = { &Group_t3_15_0_0_0 };
extern const Il2CppGenericInst GenInst_Group_t3_15_0_0_0 = { 1, GenInst_Group_t3_15_0_0_0_Types };
extern const Il2CppType Mark_t3_199_0_0_0;
static const Il2CppType* GenInst_Mark_t3_199_0_0_0_Types[] = { &Mark_t3_199_0_0_0 };
extern const Il2CppGenericInst GenInst_Mark_t3_199_0_0_0 = { 1, GenInst_Mark_t3_199_0_0_0_Types };
extern const Il2CppType UriScheme_t3_234_0_0_0;
static const Il2CppType* GenInst_UriScheme_t3_234_0_0_0_Types[] = { &UriScheme_t3_234_0_0_0 };
extern const Il2CppGenericInst GenInst_UriScheme_t3_234_0_0_0 = { 1, GenInst_UriScheme_t3_234_0_0_0_Types };
extern const Il2CppType Object_t6_5_0_0_0;
static const Il2CppType* GenInst_Object_t6_5_0_0_0_Types[] = { &Object_t6_5_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t6_5_0_0_0 = { 1, GenInst_Object_t6_5_0_0_0_Types };
extern const Il2CppType IAchievementDescription_t6_372_0_0_0;
static const Il2CppType* GenInst_IAchievementDescription_t6_372_0_0_0_Types[] = { &IAchievementDescription_t6_372_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementDescription_t6_372_0_0_0 = { 1, GenInst_IAchievementDescription_t6_372_0_0_0_Types };
extern const Il2CppType IAchievement_t6_277_0_0_0;
static const Il2CppType* GenInst_IAchievement_t6_277_0_0_0_Types[] = { &IAchievement_t6_277_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievement_t6_277_0_0_0 = { 1, GenInst_IAchievement_t6_277_0_0_0_Types };
extern const Il2CppType IScore_t6_228_0_0_0;
static const Il2CppType* GenInst_IScore_t6_228_0_0_0_Types[] = { &IScore_t6_228_0_0_0 };
extern const Il2CppGenericInst GenInst_IScore_t6_228_0_0_0 = { 1, GenInst_IScore_t6_228_0_0_0_Types };
extern const Il2CppType IUserProfile_t6_371_0_0_0;
static const Il2CppType* GenInst_IUserProfile_t6_371_0_0_0_Types[] = { &IUserProfile_t6_371_0_0_0 };
extern const Il2CppGenericInst GenInst_IUserProfile_t6_371_0_0_0 = { 1, GenInst_IUserProfile_t6_371_0_0_0_Types };
extern const Il2CppType AchievementDescription_t6_224_0_0_0;
static const Il2CppType* GenInst_AchievementDescription_t6_224_0_0_0_Types[] = { &AchievementDescription_t6_224_0_0_0 };
extern const Il2CppGenericInst GenInst_AchievementDescription_t6_224_0_0_0 = { 1, GenInst_AchievementDescription_t6_224_0_0_0_Types };
extern const Il2CppType UserProfile_t6_221_0_0_0;
static const Il2CppType* GenInst_UserProfile_t6_221_0_0_0_Types[] = { &UserProfile_t6_221_0_0_0 };
extern const Il2CppGenericInst GenInst_UserProfile_t6_221_0_0_0 = { 1, GenInst_UserProfile_t6_221_0_0_0_Types };
extern const Il2CppType GcAchievementData_t6_210_0_0_0;
static const Il2CppType* GenInst_GcAchievementData_t6_210_0_0_0_Types[] = { &GcAchievementData_t6_210_0_0_0 };
extern const Il2CppGenericInst GenInst_GcAchievementData_t6_210_0_0_0 = { 1, GenInst_GcAchievementData_t6_210_0_0_0_Types };
extern const Il2CppType Achievement_t6_223_0_0_0;
static const Il2CppType* GenInst_Achievement_t6_223_0_0_0_Types[] = { &Achievement_t6_223_0_0_0 };
extern const Il2CppGenericInst GenInst_Achievement_t6_223_0_0_0 = { 1, GenInst_Achievement_t6_223_0_0_0_Types };
extern const Il2CppType GcScoreData_t6_211_0_0_0;
static const Il2CppType* GenInst_GcScoreData_t6_211_0_0_0_Types[] = { &GcScoreData_t6_211_0_0_0 };
extern const Il2CppGenericInst GenInst_GcScoreData_t6_211_0_0_0 = { 1, GenInst_GcScoreData_t6_211_0_0_0_Types };
extern const Il2CppType Score_t6_225_0_0_0;
static const Il2CppType* GenInst_Score_t6_225_0_0_0_Types[] = { &Score_t6_225_0_0_0 };
extern const Il2CppGenericInst GenInst_Score_t6_225_0_0_0 = { 1, GenInst_Score_t6_225_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t1_284_0_0_0_Types[] = { &String_t_0_0_0, &String_t_0_0_0, &DictionaryEntry_t1_284_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t1_284_0_0_0 = { 3, GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t1_284_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_1845_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_1845_0_0_0_Types[] = { &KeyValuePair_2_t1_1845_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_1845_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_1845_0_0_0_Types };
extern const Il2CppType Behaviour_t6_30_0_0_0;
static const Il2CppType* GenInst_Behaviour_t6_30_0_0_0_Types[] = { &Behaviour_t6_30_0_0_0 };
extern const Il2CppGenericInst GenInst_Behaviour_t6_30_0_0_0 = { 1, GenInst_Behaviour_t6_30_0_0_0_Types };
extern const Il2CppType Display_t6_89_0_0_0;
static const Il2CppType* GenInst_Display_t6_89_0_0_0_Types[] = { &Display_t6_89_0_0_0 };
extern const Il2CppGenericInst GenInst_Display_t6_89_0_0_0 = { 1, GenInst_Display_t6_89_0_0_0_Types };
extern const Il2CppType Touch_t6_94_0_0_0;
static const Il2CppType* GenInst_Touch_t6_94_0_0_0_Types[] = { &Touch_t6_94_0_0_0 };
extern const Il2CppGenericInst GenInst_Touch_t6_94_0_0_0 = { 1, GenInst_Touch_t6_94_0_0_0_Types };
extern const Il2CppType ContactPoint_t6_113_0_0_0;
static const Il2CppType* GenInst_ContactPoint_t6_113_0_0_0_Types[] = { &ContactPoint_t6_113_0_0_0 };
extern const Il2CppGenericInst GenInst_ContactPoint_t6_113_0_0_0 = { 1, GenInst_ContactPoint_t6_113_0_0_0_Types };
extern const Il2CppType RaycastHit2D_t6_118_0_0_0;
static const Il2CppType* GenInst_RaycastHit2D_t6_118_0_0_0_Types[] = { &RaycastHit2D_t6_118_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastHit2D_t6_118_0_0_0 = { 1, GenInst_RaycastHit2D_t6_118_0_0_0_Types };
extern const Il2CppType ContactPoint2D_t6_121_0_0_0;
static const Il2CppType* GenInst_ContactPoint2D_t6_121_0_0_0_Types[] = { &ContactPoint2D_t6_121_0_0_0 };
extern const Il2CppGenericInst GenInst_ContactPoint2D_t6_121_0_0_0 = { 1, GenInst_ContactPoint2D_t6_121_0_0_0_Types };
extern const Il2CppType Keyframe_t6_135_0_0_0;
static const Il2CppType* GenInst_Keyframe_t6_135_0_0_0_Types[] = { &Keyframe_t6_135_0_0_0 };
extern const Il2CppGenericInst GenInst_Keyframe_t6_135_0_0_0 = { 1, GenInst_Keyframe_t6_135_0_0_0_Types };
extern const Il2CppType CharacterInfo_t6_147_0_0_0;
static const Il2CppType* GenInst_CharacterInfo_t6_147_0_0_0_Types[] = { &CharacterInfo_t6_147_0_0_0 };
extern const Il2CppGenericInst GenInst_CharacterInfo_t6_147_0_0_0 = { 1, GenInst_CharacterInfo_t6_147_0_0_0_Types };
extern const Il2CppType GUILayoutOption_t6_182_0_0_0;
static const Il2CppType* GenInst_GUILayoutOption_t6_182_0_0_0_Types[] = { &GUILayoutOption_t6_182_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayoutOption_t6_182_0_0_0 = { 1, GenInst_GUILayoutOption_t6_182_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_2385_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_2385_0_0_0_Types[] = { &KeyValuePair_2_t1_2385_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_2385_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_2385_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1_3_0_0_0_Object_t_0_0_0_Int32_t1_3_0_0_0_Types[] = { &Int32_t1_3_0_0_0, &Object_t_0_0_0, &Int32_t1_3_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1_3_0_0_0_Object_t_0_0_0_Int32_t1_3_0_0_0 = { 3, GenInst_Int32_t1_3_0_0_0_Object_t_0_0_0_Int32_t1_3_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1_3_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types[] = { &Int32_t1_3_0_0_0, &Object_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1_3_0_0_0_Object_t_0_0_0_Object_t_0_0_0 = { 3, GenInst_Int32_t1_3_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1_3_0_0_0_Object_t_0_0_0_DictionaryEntry_t1_284_0_0_0_Types[] = { &Int32_t1_3_0_0_0, &Object_t_0_0_0, &DictionaryEntry_t1_284_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1_3_0_0_0_Object_t_0_0_0_DictionaryEntry_t1_284_0_0_0 = { 3, GenInst_Int32_t1_3_0_0_0_Object_t_0_0_0_DictionaryEntry_t1_284_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1_3_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1_2385_0_0_0_Types[] = { &Int32_t1_3_0_0_0, &Object_t_0_0_0, &KeyValuePair_2_t1_2385_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1_3_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1_2385_0_0_0 = { 3, GenInst_Int32_t1_3_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1_2385_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1_3_0_0_0_LayoutCache_t6_173_0_0_0_DictionaryEntry_t1_284_0_0_0_Types[] = { &Int32_t1_3_0_0_0, &LayoutCache_t6_173_0_0_0, &DictionaryEntry_t1_284_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1_3_0_0_0_LayoutCache_t6_173_0_0_0_DictionaryEntry_t1_284_0_0_0 = { 3, GenInst_Int32_t1_3_0_0_0_LayoutCache_t6_173_0_0_0_DictionaryEntry_t1_284_0_0_0_Types };
static const Il2CppType* GenInst_LayoutCache_t6_173_0_0_0_Types[] = { &LayoutCache_t6_173_0_0_0 };
extern const Il2CppGenericInst GenInst_LayoutCache_t6_173_0_0_0 = { 1, GenInst_LayoutCache_t6_173_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_2396_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_2396_0_0_0_Types[] = { &KeyValuePair_2_t1_2396_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_2396_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_2396_0_0_0_Types };
static const Il2CppType* GenInst_GUIStyle_t6_176_0_0_0_Types[] = { &GUIStyle_t6_176_0_0_0 };
extern const Il2CppGenericInst GenInst_GUIStyle_t6_176_0_0_0 = { 1, GenInst_GUIStyle_t6_176_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GUIStyle_t6_176_0_0_0_DictionaryEntry_t1_284_0_0_0_Types[] = { &String_t_0_0_0, &GUIStyle_t6_176_0_0_0, &DictionaryEntry_t1_284_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t6_176_0_0_0_DictionaryEntry_t1_284_0_0_0 = { 3, GenInst_String_t_0_0_0_GUIStyle_t6_176_0_0_0_DictionaryEntry_t1_284_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_2407_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_2407_0_0_0_Types[] = { &KeyValuePair_2_t1_2407_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_2407_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_2407_0_0_0_Types };
extern const Il2CppType DisallowMultipleComponent_t6_201_0_0_0;
static const Il2CppType* GenInst_DisallowMultipleComponent_t6_201_0_0_0_Types[] = { &DisallowMultipleComponent_t6_201_0_0_0 };
extern const Il2CppGenericInst GenInst_DisallowMultipleComponent_t6_201_0_0_0 = { 1, GenInst_DisallowMultipleComponent_t6_201_0_0_0_Types };
extern const Il2CppType ExecuteInEditMode_t6_204_0_0_0;
static const Il2CppType* GenInst_ExecuteInEditMode_t6_204_0_0_0_Types[] = { &ExecuteInEditMode_t6_204_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteInEditMode_t6_204_0_0_0 = { 1, GenInst_ExecuteInEditMode_t6_204_0_0_0_Types };
extern const Il2CppType RequireComponent_t6_202_0_0_0;
static const Il2CppType* GenInst_RequireComponent_t6_202_0_0_0_Types[] = { &RequireComponent_t6_202_0_0_0 };
extern const Il2CppGenericInst GenInst_RequireComponent_t6_202_0_0_0 = { 1, GenInst_RequireComponent_t6_202_0_0_0_Types };
extern const Il2CppType HitInfo_t6_229_0_0_0;
static const Il2CppType* GenInst_HitInfo_t6_229_0_0_0_Types[] = { &HitInfo_t6_229_0_0_0 };
extern const Il2CppGenericInst GenInst_HitInfo_t6_229_0_0_0 = { 1, GenInst_HitInfo_t6_229_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_TextEditOp_t6_253_0_0_0_Types[] = { &Object_t_0_0_0, &TextEditOp_t6_253_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_TextEditOp_t6_253_0_0_0 = { 2, GenInst_Object_t_0_0_0_TextEditOp_t6_253_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_2424_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_2424_0_0_0_Types[] = { &KeyValuePair_2_t1_2424_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_2424_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_2424_0_0_0_Types };
static const Il2CppType* GenInst_TextEditOp_t6_253_0_0_0_Types[] = { &TextEditOp_t6_253_0_0_0 };
extern const Il2CppGenericInst GenInst_TextEditOp_t6_253_0_0_0 = { 1, GenInst_TextEditOp_t6_253_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_TextEditOp_t6_253_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &TextEditOp_t6_253_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_TextEditOp_t6_253_0_0_0_Object_t_0_0_0 = { 3, GenInst_Object_t_0_0_0_TextEditOp_t6_253_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_TextEditOp_t6_253_0_0_0_TextEditOp_t6_253_0_0_0_Types[] = { &Object_t_0_0_0, &TextEditOp_t6_253_0_0_0, &TextEditOp_t6_253_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_TextEditOp_t6_253_0_0_0_TextEditOp_t6_253_0_0_0 = { 3, GenInst_Object_t_0_0_0_TextEditOp_t6_253_0_0_0_TextEditOp_t6_253_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_TextEditOp_t6_253_0_0_0_DictionaryEntry_t1_284_0_0_0_Types[] = { &Object_t_0_0_0, &TextEditOp_t6_253_0_0_0, &DictionaryEntry_t1_284_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_TextEditOp_t6_253_0_0_0_DictionaryEntry_t1_284_0_0_0 = { 3, GenInst_Object_t_0_0_0_TextEditOp_t6_253_0_0_0_DictionaryEntry_t1_284_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_TextEditOp_t6_253_0_0_0_KeyValuePair_2_t1_2424_0_0_0_Types[] = { &Object_t_0_0_0, &TextEditOp_t6_253_0_0_0, &KeyValuePair_2_t1_2424_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_TextEditOp_t6_253_0_0_0_KeyValuePair_2_t1_2424_0_0_0 = { 3, GenInst_Object_t_0_0_0_TextEditOp_t6_253_0_0_0_KeyValuePair_2_t1_2424_0_0_0_Types };
static const Il2CppType* GenInst_Event_t6_162_0_0_0_Types[] = { &Event_t6_162_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t6_162_0_0_0 = { 1, GenInst_Event_t6_162_0_0_0_Types };
static const Il2CppType* GenInst_Event_t6_162_0_0_0_TextEditOp_t6_253_0_0_0_DictionaryEntry_t1_284_0_0_0_Types[] = { &Event_t6_162_0_0_0, &TextEditOp_t6_253_0_0_0, &DictionaryEntry_t1_284_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t6_162_0_0_0_TextEditOp_t6_253_0_0_0_DictionaryEntry_t1_284_0_0_0 = { 3, GenInst_Event_t6_162_0_0_0_TextEditOp_t6_253_0_0_0_DictionaryEntry_t1_284_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_2438_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_2438_0_0_0_Types[] = { &KeyValuePair_2_t1_2438_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_2438_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_2438_0_0_0_Types };
extern const Il2CppType List_1_t1_1894_0_0_0;
static const Il2CppType* GenInst_List_1_t1_1894_0_0_0_Types[] = { &List_1_t1_1894_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1_1894_0_0_0 = { 1, GenInst_List_1_t1_1894_0_0_0_Types };
extern const Il2CppType List_1_t1_1840_0_0_0;
static const Il2CppType* GenInst_List_1_t1_1840_0_0_0_Types[] = { &List_1_t1_1840_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1_1840_0_0_0 = { 1, GenInst_List_1_t1_1840_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1_3_0_0_0_PointerEventData_t7_36_0_0_0_DictionaryEntry_t1_284_0_0_0_Types[] = { &Int32_t1_3_0_0_0, &PointerEventData_t7_36_0_0_0, &DictionaryEntry_t1_284_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1_3_0_0_0_PointerEventData_t7_36_0_0_0_DictionaryEntry_t1_284_0_0_0 = { 3, GenInst_Int32_t1_3_0_0_0_PointerEventData_t7_36_0_0_0_DictionaryEntry_t1_284_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_1886_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_1886_0_0_0_Types[] = { &KeyValuePair_2_t1_1886_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_1886_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_1886_0_0_0_Types };
static const Il2CppType* GenInst_ICanvasElement_t7_176_0_0_0_Int32_t1_3_0_0_0_Types[] = { &ICanvasElement_t7_176_0_0_0, &Int32_t1_3_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t7_176_0_0_0_Int32_t1_3_0_0_0 = { 2, GenInst_ICanvasElement_t7_176_0_0_0_Int32_t1_3_0_0_0_Types };
static const Il2CppType* GenInst_ICanvasElement_t7_176_0_0_0_Int32_t1_3_0_0_0_DictionaryEntry_t1_284_0_0_0_Types[] = { &ICanvasElement_t7_176_0_0_0, &Int32_t1_3_0_0_0, &DictionaryEntry_t1_284_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t7_176_0_0_0_Int32_t1_3_0_0_0_DictionaryEntry_t1_284_0_0_0 = { 3, GenInst_ICanvasElement_t7_176_0_0_0_Int32_t1_3_0_0_0_DictionaryEntry_t1_284_0_0_0_Types };
extern const Il2CppType List_1_t1_1888_0_0_0;
static const Il2CppType* GenInst_List_1_t1_1888_0_0_0_Types[] = { &List_1_t1_1888_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1_1888_0_0_0 = { 1, GenInst_List_1_t1_1888_0_0_0_Types };
static const Il2CppType* GenInst_Font_t6_149_0_0_0_List_1_t1_1889_0_0_0_DictionaryEntry_t1_284_0_0_0_Types[] = { &Font_t6_149_0_0_0, &List_1_t1_1889_0_0_0, &DictionaryEntry_t1_284_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t6_149_0_0_0_List_1_t1_1889_0_0_0_DictionaryEntry_t1_284_0_0_0 = { 3, GenInst_Font_t6_149_0_0_0_List_1_t1_1889_0_0_0_DictionaryEntry_t1_284_0_0_0_Types };
static const Il2CppType* GenInst_List_1_t1_1889_0_0_0_Types[] = { &List_1_t1_1889_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1_1889_0_0_0 = { 1, GenInst_List_1_t1_1889_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_2541_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_2541_0_0_0_Types[] = { &KeyValuePair_2_t1_2541_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_2541_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_2541_0_0_0_Types };
static const Il2CppType* GenInst_Graphic_t7_75_0_0_0_Int32_t1_3_0_0_0_Types[] = { &Graphic_t7_75_0_0_0, &Int32_t1_3_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t7_75_0_0_0_Int32_t1_3_0_0_0 = { 2, GenInst_Graphic_t7_75_0_0_0_Int32_t1_3_0_0_0_Types };
static const Il2CppType* GenInst_Graphic_t7_75_0_0_0_Int32_t1_3_0_0_0_DictionaryEntry_t1_284_0_0_0_Types[] = { &Graphic_t7_75_0_0_0, &Int32_t1_3_0_0_0, &DictionaryEntry_t1_284_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t7_75_0_0_0_Int32_t1_3_0_0_0_DictionaryEntry_t1_284_0_0_0 = { 3, GenInst_Graphic_t7_75_0_0_0_Int32_t1_3_0_0_0_DictionaryEntry_t1_284_0_0_0_Types };
static const Il2CppType* GenInst_Canvas_t6_156_0_0_0_IndexedSet_1_t7_185_0_0_0_DictionaryEntry_t1_284_0_0_0_Types[] = { &Canvas_t6_156_0_0_0, &IndexedSet_1_t7_185_0_0_0, &DictionaryEntry_t1_284_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t6_156_0_0_0_IndexedSet_1_t7_185_0_0_0_DictionaryEntry_t1_284_0_0_0 = { 3, GenInst_Canvas_t6_156_0_0_0_IndexedSet_1_t7_185_0_0_0_DictionaryEntry_t1_284_0_0_0_Types };
static const Il2CppType* GenInst_IndexedSet_1_t7_185_0_0_0_Types[] = { &IndexedSet_1_t7_185_0_0_0 };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t7_185_0_0_0 = { 1, GenInst_IndexedSet_1_t7_185_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_2553_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_2553_0_0_0_Types[] = { &KeyValuePair_2_t1_2553_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_2553_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_2553_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_2557_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_2557_0_0_0_Types[] = { &KeyValuePair_2_t1_2557_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_2557_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_2557_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_2561_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_2561_0_0_0_Types[] = { &KeyValuePair_2_t1_2561_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_2561_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_2561_0_0_0_Types };
static const Il2CppType* GenInst_IClipper_t7_179_0_0_0_Int32_t1_3_0_0_0_Types[] = { &IClipper_t7_179_0_0_0, &Int32_t1_3_0_0_0 };
extern const Il2CppGenericInst GenInst_IClipper_t7_179_0_0_0_Int32_t1_3_0_0_0 = { 2, GenInst_IClipper_t7_179_0_0_0_Int32_t1_3_0_0_0_Types };
static const Il2CppType* GenInst_IClipper_t7_179_0_0_0_Int32_t1_3_0_0_0_DictionaryEntry_t1_284_0_0_0_Types[] = { &IClipper_t7_179_0_0_0, &Int32_t1_3_0_0_0, &DictionaryEntry_t1_284_0_0_0 };
extern const Il2CppGenericInst GenInst_IClipper_t7_179_0_0_0_Int32_t1_3_0_0_0_DictionaryEntry_t1_284_0_0_0 = { 3, GenInst_IClipper_t7_179_0_0_0_Int32_t1_3_0_0_0_DictionaryEntry_t1_284_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_2606_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_2606_0_0_0_Types[] = { &KeyValuePair_2_t1_2606_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_2606_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_2606_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Single_t1_17_0_0_0_Types[] = { &Object_t_0_0_0, &Single_t1_17_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Single_t1_17_0_0_0 = { 2, GenInst_Object_t_0_0_0_Single_t1_17_0_0_0_Types };
extern const Il2CppType List_1_t1_1834_0_0_0;
static const Il2CppType* GenInst_List_1_t1_1834_0_0_0_Types[] = { &List_1_t1_1834_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1_1834_0_0_0 = { 1, GenInst_List_1_t1_1834_0_0_0_Types };
extern const Il2CppType List_1_t1_1837_0_0_0;
static const Il2CppType* GenInst_List_1_t1_1837_0_0_0_Types[] = { &List_1_t1_1837_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1_1837_0_0_0 = { 1, GenInst_List_1_t1_1837_0_0_0_Types };
extern const Il2CppType List_1_t1_1836_0_0_0;
static const Il2CppType* GenInst_List_1_t1_1836_0_0_0_Types[] = { &List_1_t1_1836_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1_1836_0_0_0 = { 1, GenInst_List_1_t1_1836_0_0_0_Types };
extern const Il2CppType List_1_t1_1835_0_0_0;
static const Il2CppType* GenInst_List_1_t1_1835_0_0_0_Types[] = { &List_1_t1_1835_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1_1835_0_0_0 = { 1, GenInst_List_1_t1_1835_0_0_0_Types };
extern const Il2CppType List_1_t1_1838_0_0_0;
static const Il2CppType* GenInst_List_1_t1_1838_0_0_0_Types[] = { &List_1_t1_1838_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1_1838_0_0_0 = { 1, GenInst_List_1_t1_1838_0_0_0_Types };
extern const Il2CppType List_1_t1_1824_0_0_0;
static const Il2CppType* GenInst_List_1_t1_1824_0_0_0_Types[] = { &List_1_t1_1824_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1_1824_0_0_0 = { 1, GenInst_List_1_t1_1824_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_JSONValue_t8_4_0_0_0_DictionaryEntry_t1_284_0_0_0_Types[] = { &String_t_0_0_0, &JSONValue_t8_4_0_0_0, &DictionaryEntry_t1_284_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_JSONValue_t8_4_0_0_0_DictionaryEntry_t1_284_0_0_0 = { 3, GenInst_String_t_0_0_0_JSONValue_t8_4_0_0_0_DictionaryEntry_t1_284_0_0_0_Types };
extern const Il2CppType DemoGUIWindow_t8_14_0_0_0;
static const Il2CppType* GenInst_DemoGUIWindow_t8_14_0_0_0_Types[] = { &DemoGUIWindow_t8_14_0_0_0 };
extern const Il2CppGenericInst GenInst_DemoGUIWindow_t8_14_0_0_0 = { 1, GenInst_DemoGUIWindow_t8_14_0_0_0_Types };
extern const Il2CppType GUIModalWindow_t8_15_0_0_0;
static const Il2CppType* GenInst_GUIModalWindow_t8_15_0_0_0_Types[] = { &GUIModalWindow_t8_15_0_0_0 };
extern const Il2CppGenericInst GenInst_GUIModalWindow_t8_15_0_0_0 = { 1, GenInst_GUIModalWindow_t8_15_0_0_0_Types };
extern const Il2CppType IObserver_t8_374_0_0_0;
static const Il2CppType* GenInst_IObserver_t8_374_0_0_0_Types[] = { &IObserver_t8_374_0_0_0 };
extern const Il2CppGenericInst GenInst_IObserver_t8_374_0_0_0 = { 1, GenInst_IObserver_t8_374_0_0_0_Types };
extern const Il2CppType ShaderProperty_t8_27_0_0_0;
static const Il2CppType* GenInst_ShaderProperty_t8_27_0_0_0_Types[] = { &ShaderProperty_t8_27_0_0_0 };
extern const Il2CppGenericInst GenInst_ShaderProperty_t8_27_0_0_0 = { 1, GenInst_ShaderProperty_t8_27_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t1_284_0_0_0_Types[] = { &String_t_0_0_0, &Object_t_0_0_0, &DictionaryEntry_t1_284_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t1_284_0_0_0 = { 3, GenInst_String_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t1_284_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_2652_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_2652_0_0_0_Types[] = { &KeyValuePair_2_t1_2652_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_2652_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_2652_0_0_0_Types };
extern const Il2CppType UFraction32_t8_121_0_0_0;
static const Il2CppType* GenInst_UFraction32_t8_121_0_0_0_Types[] = { &UFraction32_t8_121_0_0_0 };
extern const Il2CppGenericInst GenInst_UFraction32_t8_121_0_0_0 = { 1, GenInst_UFraction32_t8_121_0_0_0_Types };
extern const Il2CppType IComparable_1_t1_4524_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1_4524_0_0_0_Types[] = { &IComparable_1_t1_4524_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1_4524_0_0_0 = { 1, GenInst_IComparable_1_t1_4524_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1_4529_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1_4529_0_0_0_Types[] = { &IEquatable_1_t1_4529_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1_4529_0_0_0 = { 1, GenInst_IEquatable_1_t1_4529_0_0_0_Types };
extern const Il2CppType Fraction32_t8_127_0_0_0;
static const Il2CppType* GenInst_Fraction32_t8_127_0_0_0_Types[] = { &Fraction32_t8_127_0_0_0 };
extern const Il2CppGenericInst GenInst_Fraction32_t8_127_0_0_0 = { 1, GenInst_Fraction32_t8_127_0_0_0_Types };
extern const Il2CppType IComparable_1_t1_4537_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1_4537_0_0_0_Types[] = { &IComparable_1_t1_4537_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1_4537_0_0_0 = { 1, GenInst_IComparable_1_t1_4537_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1_4542_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1_4542_0_0_0_Types[] = { &IEquatable_1_t1_4542_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1_4542_0_0_0 = { 1, GenInst_IEquatable_1_t1_4542_0_0_0_Types };
static const Il2CppType* GenInst_ExifTag_t8_132_0_0_0_Object_t_0_0_0_Types[] = { &ExifTag_t8_132_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_ExifTag_t8_132_0_0_0_Object_t_0_0_0 = { 2, GenInst_ExifTag_t8_132_0_0_0_Object_t_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_2665_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_2665_0_0_0_Types[] = { &KeyValuePair_2_t1_2665_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_2665_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_2665_0_0_0_Types };
static const Il2CppType* GenInst_ExifTag_t8_132_0_0_0_Types[] = { &ExifTag_t8_132_0_0_0 };
extern const Il2CppGenericInst GenInst_ExifTag_t8_132_0_0_0 = { 1, GenInst_ExifTag_t8_132_0_0_0_Types };
static const Il2CppType* GenInst_ExifTag_t8_132_0_0_0_Object_t_0_0_0_ExifTag_t8_132_0_0_0_Types[] = { &ExifTag_t8_132_0_0_0, &Object_t_0_0_0, &ExifTag_t8_132_0_0_0 };
extern const Il2CppGenericInst GenInst_ExifTag_t8_132_0_0_0_Object_t_0_0_0_ExifTag_t8_132_0_0_0 = { 3, GenInst_ExifTag_t8_132_0_0_0_Object_t_0_0_0_ExifTag_t8_132_0_0_0_Types };
static const Il2CppType* GenInst_ExifTag_t8_132_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types[] = { &ExifTag_t8_132_0_0_0, &Object_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_ExifTag_t8_132_0_0_0_Object_t_0_0_0_Object_t_0_0_0 = { 3, GenInst_ExifTag_t8_132_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_ExifTag_t8_132_0_0_0_Object_t_0_0_0_DictionaryEntry_t1_284_0_0_0_Types[] = { &ExifTag_t8_132_0_0_0, &Object_t_0_0_0, &DictionaryEntry_t1_284_0_0_0 };
extern const Il2CppGenericInst GenInst_ExifTag_t8_132_0_0_0_Object_t_0_0_0_DictionaryEntry_t1_284_0_0_0 = { 3, GenInst_ExifTag_t8_132_0_0_0_Object_t_0_0_0_DictionaryEntry_t1_284_0_0_0_Types };
static const Il2CppType* GenInst_ExifTag_t8_132_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1_2665_0_0_0_Types[] = { &ExifTag_t8_132_0_0_0, &Object_t_0_0_0, &KeyValuePair_2_t1_2665_0_0_0 };
extern const Il2CppGenericInst GenInst_ExifTag_t8_132_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1_2665_0_0_0 = { 3, GenInst_ExifTag_t8_132_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1_2665_0_0_0_Types };
static const Il2CppType* GenInst_ExifTag_t8_132_0_0_0_ExifProperty_t8_99_0_0_0_DictionaryEntry_t1_284_0_0_0_Types[] = { &ExifTag_t8_132_0_0_0, &ExifProperty_t8_99_0_0_0, &DictionaryEntry_t1_284_0_0_0 };
extern const Il2CppGenericInst GenInst_ExifTag_t8_132_0_0_0_ExifProperty_t8_99_0_0_0_DictionaryEntry_t1_284_0_0_0 = { 3, GenInst_ExifTag_t8_132_0_0_0_ExifProperty_t8_99_0_0_0_DictionaryEntry_t1_284_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_1924_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_1924_0_0_0_Types[] = { &KeyValuePair_2_t1_1924_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_1924_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_1924_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_2681_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_2681_0_0_0_Types[] = { &KeyValuePair_2_t1_2681_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_2681_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_2681_0_0_0_Types };
extern const Il2CppType GUIMenuBase_t8_141_0_0_0;
static const Il2CppType* GenInst_GUIMenuBase_t8_141_0_0_0_Types[] = { &GUIMenuBase_t8_141_0_0_0 };
extern const Il2CppGenericInst GenInst_GUIMenuBase_t8_141_0_0_0 = { 1, GenInst_GUIMenuBase_t8_141_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Single_t1_17_0_0_0_DictionaryEntry_t1_284_0_0_0_Types[] = { &String_t_0_0_0, &Single_t1_17_0_0_0, &DictionaryEntry_t1_284_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Single_t1_17_0_0_0_DictionaryEntry_t1_284_0_0_0 = { 3, GenInst_String_t_0_0_0_Single_t1_17_0_0_0_DictionaryEntry_t1_284_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_2696_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_2696_0_0_0_Types[] = { &KeyValuePair_2_t1_2696_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_2696_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_2696_0_0_0_Types };
static const Il2CppType* GenInst_Action_t5_11_0_0_0_Types[] = { &Action_t5_11_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_t5_11_0_0_0 = { 1, GenInst_Action_t5_11_0_0_0_Types };
static const Il2CppType* GenInst_Action_t5_11_0_0_0_Dictionary_2_t1_1929_0_0_0_DictionaryEntry_t1_284_0_0_0_Types[] = { &Action_t5_11_0_0_0, &Dictionary_2_t1_1929_0_0_0, &DictionaryEntry_t1_284_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_t5_11_0_0_0_Dictionary_2_t1_1929_0_0_0_DictionaryEntry_t1_284_0_0_0 = { 3, GenInst_Action_t5_11_0_0_0_Dictionary_2_t1_1929_0_0_0_DictionaryEntry_t1_284_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t1_1929_0_0_0_Types[] = { &Dictionary_2_t1_1929_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t1_1929_0_0_0 = { 1, GenInst_Dictionary_2_t1_1929_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_2700_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_2700_0_0_0_Types[] = { &KeyValuePair_2_t1_2700_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_2700_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_2700_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_2706_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_2706_0_0_0_Types[] = { &KeyValuePair_2_t1_2706_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_2706_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_2706_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Single_t1_17_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &Single_t1_17_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Single_t1_17_0_0_0_Object_t_0_0_0 = { 3, GenInst_Object_t_0_0_0_Single_t1_17_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Single_t1_17_0_0_0_Single_t1_17_0_0_0_Types[] = { &Object_t_0_0_0, &Single_t1_17_0_0_0, &Single_t1_17_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Single_t1_17_0_0_0_Single_t1_17_0_0_0 = { 3, GenInst_Object_t_0_0_0_Single_t1_17_0_0_0_Single_t1_17_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Single_t1_17_0_0_0_DictionaryEntry_t1_284_0_0_0_Types[] = { &Object_t_0_0_0, &Single_t1_17_0_0_0, &DictionaryEntry_t1_284_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Single_t1_17_0_0_0_DictionaryEntry_t1_284_0_0_0 = { 3, GenInst_Object_t_0_0_0_Single_t1_17_0_0_0_DictionaryEntry_t1_284_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Single_t1_17_0_0_0_KeyValuePair_2_t1_2706_0_0_0_Types[] = { &Object_t_0_0_0, &Single_t1_17_0_0_0, &KeyValuePair_2_t1_2706_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Single_t1_17_0_0_0_KeyValuePair_2_t1_2706_0_0_0 = { 3, GenInst_Object_t_0_0_0_Single_t1_17_0_0_0_KeyValuePair_2_t1_2706_0_0_0_Types };
extern const Il2CppType AddressBookContact_t8_198_0_0_0;
static const Il2CppType* GenInst_AddressBookContact_t8_198_0_0_0_Types[] = { &AddressBookContact_t8_198_0_0_0 };
extern const Il2CppGenericInst GenInst_AddressBookContact_t8_198_0_0_0 = { 1, GenInst_AddressBookContact_t8_198_0_0_0_Types };
extern const Il2CppType Texture_t6_32_0_0_0;
static const Il2CppType* GenInst_Texture_t6_32_0_0_0_Types[] = { &Texture_t6_32_0_0_0 };
extern const Il2CppGenericInst GenInst_Texture_t6_32_0_0_0 = { 1, GenInst_Texture_t6_32_0_0_0_Types };
extern const Il2CppType eShareOptions_t8_287_0_0_0;
static const Il2CppType* GenInst_eShareOptions_t8_287_0_0_0_Types[] = { &eShareOptions_t8_287_0_0_0 };
extern const Il2CppGenericInst GenInst_eShareOptions_t8_287_0_0_0 = { 1, GenInst_eShareOptions_t8_287_0_0_0_Types };
extern const Il2CppType PlatformID_t8_331_0_0_0;
static const Il2CppType* GenInst_PlatformID_t8_331_0_0_0_Types[] = { &PlatformID_t8_331_0_0_0 };
extern const Il2CppGenericInst GenInst_PlatformID_t8_331_0_0_0 = { 1, GenInst_PlatformID_t8_331_0_0_0_Types };
extern const Il2CppType Achievement_t8_221_0_0_0;
static const Il2CppType* GenInst_Achievement_t8_221_0_0_0_Types[] = { &Achievement_t8_221_0_0_0 };
extern const Il2CppGenericInst GenInst_Achievement_t8_221_0_0_0 = { 1, GenInst_Achievement_t8_221_0_0_0_Types };
static const Il2CppType* GenInst_NPObject_t8_222_0_0_0_Types[] = { &NPObject_t8_222_0_0_0 };
extern const Il2CppGenericInst GenInst_NPObject_t8_222_0_0_0 = { 1, GenInst_NPObject_t8_222_0_0_0_Types };
extern const Il2CppType AchievementDescription_t8_225_0_0_0;
static const Il2CppType* GenInst_AchievementDescription_t8_225_0_0_0_Types[] = { &AchievementDescription_t8_225_0_0_0 };
extern const Il2CppGenericInst GenInst_AchievementDescription_t8_225_0_0_0 = { 1, GenInst_AchievementDescription_t8_225_0_0_0_Types };
extern const Il2CppType Score_t8_231_0_0_0;
static const Il2CppType* GenInst_Score_t8_231_0_0_0_Types[] = { &Score_t8_231_0_0_0 };
extern const Il2CppGenericInst GenInst_Score_t8_231_0_0_0 = { 1, GenInst_Score_t8_231_0_0_0_Types };
extern const Il2CppType User_t8_235_0_0_0;
static const Il2CppType* GenInst_User_t8_235_0_0_0_Types[] = { &User_t8_235_0_0_0 };
extern const Il2CppGenericInst GenInst_User_t8_235_0_0_0 = { 1, GenInst_User_t8_235_0_0_0_Types };
extern const Il2CppType IDContainer_t8_328_0_0_0;
static const Il2CppType* GenInst_IDContainer_t8_328_0_0_0_Types[] = { &IDContainer_t8_328_0_0_0 };
extern const Il2CppGenericInst GenInst_IDContainer_t8_328_0_0_0 = { 1, GenInst_IDContainer_t8_328_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_AlertDialogCompletion_t8_300_0_0_0_DictionaryEntry_t1_284_0_0_0_Types[] = { &String_t_0_0_0, &AlertDialogCompletion_t8_300_0_0_0, &DictionaryEntry_t1_284_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_AlertDialogCompletion_t8_300_0_0_0_DictionaryEntry_t1_284_0_0_0 = { 3, GenInst_String_t_0_0_0_AlertDialogCompletion_t8_300_0_0_0_DictionaryEntry_t1_284_0_0_0_Types };
static const Il2CppType* GenInst_AlertDialogCompletion_t8_300_0_0_0_Types[] = { &AlertDialogCompletion_t8_300_0_0_0 };
extern const Il2CppGenericInst GenInst_AlertDialogCompletion_t8_300_0_0_0 = { 1, GenInst_AlertDialogCompletion_t8_300_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_2754_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_2754_0_0_0_Types[] = { &KeyValuePair_2_t1_2754_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_2754_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_2754_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_WebView_t8_191_0_0_0_DictionaryEntry_t1_284_0_0_0_Types[] = { &String_t_0_0_0, &WebView_t8_191_0_0_0, &DictionaryEntry_t1_284_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_WebView_t8_191_0_0_0_DictionaryEntry_t1_284_0_0_0 = { 3, GenInst_String_t_0_0_0_WebView_t8_191_0_0_0_DictionaryEntry_t1_284_0_0_0_Types };
static const Il2CppType* GenInst_WebView_t8_191_0_0_0_Types[] = { &WebView_t8_191_0_0_0 };
extern const Il2CppGenericInst GenInst_WebView_t8_191_0_0_0 = { 1, GenInst_WebView_t8_191_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_2759_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_2759_0_0_0_Types[] = { &KeyValuePair_2_t1_2759_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_2759_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_2759_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_NPObject_t8_222_0_0_0_DictionaryEntry_t1_284_0_0_0_Types[] = { &String_t_0_0_0, &NPObject_t8_222_0_0_0, &DictionaryEntry_t1_284_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_NPObject_t8_222_0_0_0_DictionaryEntry_t1_284_0_0_0 = { 3, GenInst_String_t_0_0_0_NPObject_t8_222_0_0_0_DictionaryEntry_t1_284_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_2764_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_2764_0_0_0_Types[] = { &KeyValuePair_2_t1_2764_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_2764_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_2764_0_0_0_Types };
static const Il2CppType* GenInst_DictionaryEntry_t1_284_0_0_0_DictionaryEntry_t1_284_0_0_0_Types[] = { &DictionaryEntry_t1_284_0_0_0, &DictionaryEntry_t1_284_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t1_284_0_0_0_DictionaryEntry_t1_284_0_0_0 = { 2, GenInst_DictionaryEntry_t1_284_0_0_0_DictionaryEntry_t1_284_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1_1981_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t1_1981_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_1981_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t1_1981_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1_1981_0_0_0_KeyValuePair_2_t1_1981_0_0_0_Types[] = { &KeyValuePair_2_t1_1981_0_0_0, &KeyValuePair_2_t1_1981_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_1981_0_0_0_KeyValuePair_2_t1_1981_0_0_0 = { 2, GenInst_KeyValuePair_2_t1_1981_0_0_0_KeyValuePair_2_t1_1981_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1_2015_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t1_2015_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_2015_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t1_2015_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1_2015_0_0_0_KeyValuePair_2_t1_2015_0_0_0_Types[] = { &KeyValuePair_2_t1_2015_0_0_0, &KeyValuePair_2_t1_2015_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_2015_0_0_0_KeyValuePair_2_t1_2015_0_0_0 = { 2, GenInst_KeyValuePair_2_t1_2015_0_0_0_KeyValuePair_2_t1_2015_0_0_0_Types };
static const Il2CppType* GenInst_CustomAttributeTypedArgument_t1_594_0_0_0_CustomAttributeTypedArgument_t1_594_0_0_0_Types[] = { &CustomAttributeTypedArgument_t1_594_0_0_0, &CustomAttributeTypedArgument_t1_594_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeTypedArgument_t1_594_0_0_0_CustomAttributeTypedArgument_t1_594_0_0_0 = { 2, GenInst_CustomAttributeTypedArgument_t1_594_0_0_0_CustomAttributeTypedArgument_t1_594_0_0_0_Types };
static const Il2CppType* GenInst_CustomAttributeNamedArgument_t1_593_0_0_0_CustomAttributeNamedArgument_t1_593_0_0_0_Types[] = { &CustomAttributeNamedArgument_t1_593_0_0_0, &CustomAttributeNamedArgument_t1_593_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeNamedArgument_t1_593_0_0_0_CustomAttributeNamedArgument_t1_593_0_0_0 = { 2, GenInst_CustomAttributeNamedArgument_t1_593_0_0_0_CustomAttributeNamedArgument_t1_593_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t1_20_0_0_0_Object_t_0_0_0_Types[] = { &Boolean_t1_20_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t1_20_0_0_0_Object_t_0_0_0 = { 2, GenInst_Boolean_t1_20_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t1_20_0_0_0_Boolean_t1_20_0_0_0_Types[] = { &Boolean_t1_20_0_0_0, &Boolean_t1_20_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t1_20_0_0_0_Boolean_t1_20_0_0_0 = { 2, GenInst_Boolean_t1_20_0_0_0_Boolean_t1_20_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1_2223_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t1_2223_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_2223_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t1_2223_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1_2223_0_0_0_KeyValuePair_2_t1_2223_0_0_0_Types[] = { &KeyValuePair_2_t1_2223_0_0_0, &KeyValuePair_2_t1_2223_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_2223_0_0_0_KeyValuePair_2_t1_2223_0_0_0 = { 2, GenInst_KeyValuePair_2_t1_2223_0_0_0_KeyValuePair_2_t1_2223_0_0_0_Types };
static const Il2CppType* GenInst_Vector3_t6_48_0_0_0_Vector3_t6_48_0_0_0_Types[] = { &Vector3_t6_48_0_0_0, &Vector3_t6_48_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3_t6_48_0_0_0_Vector3_t6_48_0_0_0 = { 2, GenInst_Vector3_t6_48_0_0_0_Vector3_t6_48_0_0_0_Types };
static const Il2CppType* GenInst_Vector4_t6_54_0_0_0_Vector4_t6_54_0_0_0_Types[] = { &Vector4_t6_54_0_0_0, &Vector4_t6_54_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector4_t6_54_0_0_0_Vector4_t6_54_0_0_0 = { 2, GenInst_Vector4_t6_54_0_0_0_Vector4_t6_54_0_0_0_Types };
static const Il2CppType* GenInst_Vector2_t6_47_0_0_0_Vector2_t6_47_0_0_0_Types[] = { &Vector2_t6_47_0_0_0, &Vector2_t6_47_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector2_t6_47_0_0_0_Vector2_t6_47_0_0_0 = { 2, GenInst_Vector2_t6_47_0_0_0_Vector2_t6_47_0_0_0_Types };
static const Il2CppType* GenInst_Color32_t6_49_0_0_0_Color32_t6_49_0_0_0_Types[] = { &Color32_t6_49_0_0_0, &Color32_t6_49_0_0_0 };
extern const Il2CppGenericInst GenInst_Color32_t6_49_0_0_0_Color32_t6_49_0_0_0 = { 2, GenInst_Color32_t6_49_0_0_0_Color32_t6_49_0_0_0_Types };
static const Il2CppType* GenInst_UIVertex_t6_158_0_0_0_UIVertex_t6_158_0_0_0_Types[] = { &UIVertex_t6_158_0_0_0, &UIVertex_t6_158_0_0_0 };
extern const Il2CppGenericInst GenInst_UIVertex_t6_158_0_0_0_UIVertex_t6_158_0_0_0 = { 2, GenInst_UIVertex_t6_158_0_0_0_UIVertex_t6_158_0_0_0_Types };
static const Il2CppType* GenInst_UICharInfo_t6_150_0_0_0_UICharInfo_t6_150_0_0_0_Types[] = { &UICharInfo_t6_150_0_0_0, &UICharInfo_t6_150_0_0_0 };
extern const Il2CppGenericInst GenInst_UICharInfo_t6_150_0_0_0_UICharInfo_t6_150_0_0_0 = { 2, GenInst_UICharInfo_t6_150_0_0_0_UICharInfo_t6_150_0_0_0_Types };
static const Il2CppType* GenInst_UILineInfo_t6_151_0_0_0_UILineInfo_t6_151_0_0_0_Types[] = { &UILineInfo_t6_151_0_0_0, &UILineInfo_t6_151_0_0_0 };
extern const Il2CppGenericInst GenInst_UILineInfo_t6_151_0_0_0_UILineInfo_t6_151_0_0_0 = { 2, GenInst_UILineInfo_t6_151_0_0_0_UILineInfo_t6_151_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1_2385_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t1_2385_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_2385_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t1_2385_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1_2385_0_0_0_KeyValuePair_2_t1_2385_0_0_0_Types[] = { &KeyValuePair_2_t1_2385_0_0_0, &KeyValuePair_2_t1_2385_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_2385_0_0_0_KeyValuePair_2_t1_2385_0_0_0 = { 2, GenInst_KeyValuePair_2_t1_2385_0_0_0_KeyValuePair_2_t1_2385_0_0_0_Types };
static const Il2CppType* GenInst_TextEditOp_t6_253_0_0_0_Object_t_0_0_0_Types[] = { &TextEditOp_t6_253_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_TextEditOp_t6_253_0_0_0_Object_t_0_0_0 = { 2, GenInst_TextEditOp_t6_253_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_TextEditOp_t6_253_0_0_0_TextEditOp_t6_253_0_0_0_Types[] = { &TextEditOp_t6_253_0_0_0, &TextEditOp_t6_253_0_0_0 };
extern const Il2CppGenericInst GenInst_TextEditOp_t6_253_0_0_0_TextEditOp_t6_253_0_0_0 = { 2, GenInst_TextEditOp_t6_253_0_0_0_TextEditOp_t6_253_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1_2424_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t1_2424_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_2424_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t1_2424_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1_2424_0_0_0_KeyValuePair_2_t1_2424_0_0_0_Types[] = { &KeyValuePair_2_t1_2424_0_0_0, &KeyValuePair_2_t1_2424_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_2424_0_0_0_KeyValuePair_2_t1_2424_0_0_0 = { 2, GenInst_KeyValuePair_2_t1_2424_0_0_0_KeyValuePair_2_t1_2424_0_0_0_Types };
static const Il2CppType* GenInst_RaycastResult_t7_31_0_0_0_RaycastResult_t7_31_0_0_0_Types[] = { &RaycastResult_t7_31_0_0_0, &RaycastResult_t7_31_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastResult_t7_31_0_0_0_RaycastResult_t7_31_0_0_0 = { 2, GenInst_RaycastResult_t7_31_0_0_0_RaycastResult_t7_31_0_0_0_Types };
static const Il2CppType* GenInst_ExifTag_t8_132_0_0_0_ExifTag_t8_132_0_0_0_Types[] = { &ExifTag_t8_132_0_0_0, &ExifTag_t8_132_0_0_0 };
extern const Il2CppGenericInst GenInst_ExifTag_t8_132_0_0_0_ExifTag_t8_132_0_0_0 = { 2, GenInst_ExifTag_t8_132_0_0_0_ExifTag_t8_132_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1_2665_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t1_2665_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_2665_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t1_2665_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1_2665_0_0_0_KeyValuePair_2_t1_2665_0_0_0_Types[] = { &KeyValuePair_2_t1_2665_0_0_0, &KeyValuePair_2_t1_2665_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_2665_0_0_0_KeyValuePair_2_t1_2665_0_0_0 = { 2, GenInst_KeyValuePair_2_t1_2665_0_0_0_KeyValuePair_2_t1_2665_0_0_0_Types };
static const Il2CppType* GenInst_Single_t1_17_0_0_0_Object_t_0_0_0_Types[] = { &Single_t1_17_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Single_t1_17_0_0_0_Object_t_0_0_0 = { 2, GenInst_Single_t1_17_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Single_t1_17_0_0_0_Single_t1_17_0_0_0_Types[] = { &Single_t1_17_0_0_0, &Single_t1_17_0_0_0 };
extern const Il2CppGenericInst GenInst_Single_t1_17_0_0_0_Single_t1_17_0_0_0 = { 2, GenInst_Single_t1_17_0_0_0_Single_t1_17_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1_2706_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t1_2706_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_2706_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t1_2706_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1_2706_0_0_0_KeyValuePair_2_t1_2706_0_0_0_Types[] = { &KeyValuePair_2_t1_2706_0_0_0, &KeyValuePair_2_t1_2706_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_2706_0_0_0_KeyValuePair_2_t1_2706_0_0_0 = { 2, GenInst_KeyValuePair_2_t1_2706_0_0_0_KeyValuePair_2_t1_2706_0_0_0_Types };
static const Il2CppType* GenInst_ConsoleLog_t8_170_0_0_0_ConsoleLog_t8_170_0_0_0_Types[] = { &ConsoleLog_t8_170_0_0_0, &ConsoleLog_t8_170_0_0_0 };
extern const Il2CppGenericInst GenInst_ConsoleLog_t8_170_0_0_0_ConsoleLog_t8_170_0_0_0 = { 2, GenInst_ConsoleLog_t8_170_0_0_0_ConsoleLog_t8_170_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t1_3069_gp_0_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t1_3069_gp_0_0_0_0_Types[] = { &IEnumerable_1_t1_3069_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t1_3069_gp_0_0_0_0 = { 1, GenInst_IEnumerable_1_t1_3069_gp_0_0_0_0_Types };
extern const Il2CppType Array_InternalArray__IEnumerable_GetEnumerator_m1_29229_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m1_29229_gp_0_0_0_0_Types[] = { &Array_InternalArray__IEnumerable_GetEnumerator_m1_29229_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m1_29229_gp_0_0_0_0 = { 1, GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m1_29229_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1_29241_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1_29241_gp_0_0_0_0_Array_Sort_m1_29241_gp_0_0_0_0_Types[] = { &Array_Sort_m1_29241_gp_0_0_0_0, &Array_Sort_m1_29241_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1_29241_gp_0_0_0_0_Array_Sort_m1_29241_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m1_29241_gp_0_0_0_0_Array_Sort_m1_29241_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1_29242_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m1_29242_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1_29242_gp_0_0_0_0_Array_Sort_m1_29242_gp_1_0_0_0_Types[] = { &Array_Sort_m1_29242_gp_0_0_0_0, &Array_Sort_m1_29242_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1_29242_gp_0_0_0_0_Array_Sort_m1_29242_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m1_29242_gp_0_0_0_0_Array_Sort_m1_29242_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m1_29243_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1_29243_gp_0_0_0_0_Types[] = { &Array_Sort_m1_29243_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1_29243_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m1_29243_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m1_29243_gp_0_0_0_0_Array_Sort_m1_29243_gp_0_0_0_0_Types[] = { &Array_Sort_m1_29243_gp_0_0_0_0, &Array_Sort_m1_29243_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1_29243_gp_0_0_0_0_Array_Sort_m1_29243_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m1_29243_gp_0_0_0_0_Array_Sort_m1_29243_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1_29244_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1_29244_gp_0_0_0_0_Types[] = { &Array_Sort_m1_29244_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1_29244_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m1_29244_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1_29244_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1_29244_gp_0_0_0_0_Array_Sort_m1_29244_gp_1_0_0_0_Types[] = { &Array_Sort_m1_29244_gp_0_0_0_0, &Array_Sort_m1_29244_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1_29244_gp_0_0_0_0_Array_Sort_m1_29244_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m1_29244_gp_0_0_0_0_Array_Sort_m1_29244_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m1_29245_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1_29245_gp_0_0_0_0_Array_Sort_m1_29245_gp_0_0_0_0_Types[] = { &Array_Sort_m1_29245_gp_0_0_0_0, &Array_Sort_m1_29245_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1_29245_gp_0_0_0_0_Array_Sort_m1_29245_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m1_29245_gp_0_0_0_0_Array_Sort_m1_29245_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1_29246_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m1_29246_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1_29246_gp_0_0_0_0_Array_Sort_m1_29246_gp_1_0_0_0_Types[] = { &Array_Sort_m1_29246_gp_0_0_0_0, &Array_Sort_m1_29246_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1_29246_gp_0_0_0_0_Array_Sort_m1_29246_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m1_29246_gp_0_0_0_0_Array_Sort_m1_29246_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m1_29247_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1_29247_gp_0_0_0_0_Types[] = { &Array_Sort_m1_29247_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1_29247_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m1_29247_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m1_29247_gp_0_0_0_0_Array_Sort_m1_29247_gp_0_0_0_0_Types[] = { &Array_Sort_m1_29247_gp_0_0_0_0, &Array_Sort_m1_29247_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1_29247_gp_0_0_0_0_Array_Sort_m1_29247_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m1_29247_gp_0_0_0_0_Array_Sort_m1_29247_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1_29248_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1_29248_gp_0_0_0_0_Types[] = { &Array_Sort_m1_29248_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1_29248_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m1_29248_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1_29248_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1_29248_gp_1_0_0_0_Types[] = { &Array_Sort_m1_29248_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1_29248_gp_1_0_0_0 = { 1, GenInst_Array_Sort_m1_29248_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m1_29248_gp_0_0_0_0_Array_Sort_m1_29248_gp_1_0_0_0_Types[] = { &Array_Sort_m1_29248_gp_0_0_0_0, &Array_Sort_m1_29248_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1_29248_gp_0_0_0_0_Array_Sort_m1_29248_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m1_29248_gp_0_0_0_0_Array_Sort_m1_29248_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m1_29249_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1_29249_gp_0_0_0_0_Types[] = { &Array_Sort_m1_29249_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1_29249_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m1_29249_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1_29250_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1_29250_gp_0_0_0_0_Types[] = { &Array_Sort_m1_29250_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1_29250_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m1_29250_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m1_29251_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m1_29251_gp_0_0_0_0_Types[] = { &Array_qsort_m1_29251_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m1_29251_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m1_29251_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m1_29251_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m1_29251_gp_0_0_0_0_Array_qsort_m1_29251_gp_1_0_0_0_Types[] = { &Array_qsort_m1_29251_gp_0_0_0_0, &Array_qsort_m1_29251_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m1_29251_gp_0_0_0_0_Array_qsort_m1_29251_gp_1_0_0_0 = { 2, GenInst_Array_qsort_m1_29251_gp_0_0_0_0_Array_qsort_m1_29251_gp_1_0_0_0_Types };
extern const Il2CppType Array_compare_m1_29252_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_compare_m1_29252_gp_0_0_0_0_Types[] = { &Array_compare_m1_29252_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_compare_m1_29252_gp_0_0_0_0 = { 1, GenInst_Array_compare_m1_29252_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m1_29253_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m1_29253_gp_0_0_0_0_Types[] = { &Array_qsort_m1_29253_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m1_29253_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m1_29253_gp_0_0_0_0_Types };
extern const Il2CppType Array_Resize_m1_29256_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Resize_m1_29256_gp_0_0_0_0_Types[] = { &Array_Resize_m1_29256_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Resize_m1_29256_gp_0_0_0_0 = { 1, GenInst_Array_Resize_m1_29256_gp_0_0_0_0_Types };
extern const Il2CppType Array_TrueForAll_m1_29258_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_TrueForAll_m1_29258_gp_0_0_0_0_Types[] = { &Array_TrueForAll_m1_29258_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_TrueForAll_m1_29258_gp_0_0_0_0 = { 1, GenInst_Array_TrueForAll_m1_29258_gp_0_0_0_0_Types };
extern const Il2CppType Array_ForEach_m1_29259_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_ForEach_m1_29259_gp_0_0_0_0_Types[] = { &Array_ForEach_m1_29259_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_ForEach_m1_29259_gp_0_0_0_0 = { 1, GenInst_Array_ForEach_m1_29259_gp_0_0_0_0_Types };
extern const Il2CppType Array_ConvertAll_m1_29260_gp_0_0_0_0;
extern const Il2CppType Array_ConvertAll_m1_29260_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_ConvertAll_m1_29260_gp_0_0_0_0_Array_ConvertAll_m1_29260_gp_1_0_0_0_Types[] = { &Array_ConvertAll_m1_29260_gp_0_0_0_0, &Array_ConvertAll_m1_29260_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_ConvertAll_m1_29260_gp_0_0_0_0_Array_ConvertAll_m1_29260_gp_1_0_0_0 = { 2, GenInst_Array_ConvertAll_m1_29260_gp_0_0_0_0_Array_ConvertAll_m1_29260_gp_1_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m1_29261_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m1_29261_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m1_29261_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m1_29261_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m1_29261_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m1_29262_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m1_29262_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m1_29262_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m1_29262_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m1_29262_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m1_29263_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m1_29263_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m1_29263_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m1_29263_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m1_29263_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m1_29264_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m1_29264_gp_0_0_0_0_Types[] = { &Array_FindIndex_m1_29264_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m1_29264_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m1_29264_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m1_29265_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m1_29265_gp_0_0_0_0_Types[] = { &Array_FindIndex_m1_29265_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m1_29265_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m1_29265_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m1_29266_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m1_29266_gp_0_0_0_0_Types[] = { &Array_FindIndex_m1_29266_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m1_29266_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m1_29266_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m1_29267_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m1_29267_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m1_29267_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m1_29267_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m1_29267_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m1_29268_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m1_29268_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m1_29268_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m1_29268_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m1_29268_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m1_29269_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m1_29269_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m1_29269_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m1_29269_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m1_29269_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m1_29270_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m1_29270_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m1_29270_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m1_29270_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m1_29270_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m1_29271_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m1_29271_gp_0_0_0_0_Types[] = { &Array_IndexOf_m1_29271_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m1_29271_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m1_29271_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m1_29272_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m1_29272_gp_0_0_0_0_Types[] = { &Array_IndexOf_m1_29272_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m1_29272_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m1_29272_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m1_29273_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m1_29273_gp_0_0_0_0_Types[] = { &Array_IndexOf_m1_29273_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m1_29273_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m1_29273_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m1_29274_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m1_29274_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m1_29274_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m1_29274_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m1_29274_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m1_29275_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m1_29275_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m1_29275_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m1_29275_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m1_29275_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m1_29276_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m1_29276_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m1_29276_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m1_29276_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m1_29276_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindAll_m1_29277_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindAll_m1_29277_gp_0_0_0_0_Types[] = { &Array_FindAll_m1_29277_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindAll_m1_29277_gp_0_0_0_0 = { 1, GenInst_Array_FindAll_m1_29277_gp_0_0_0_0_Types };
extern const Il2CppType Array_Exists_m1_29278_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Exists_m1_29278_gp_0_0_0_0_Types[] = { &Array_Exists_m1_29278_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Exists_m1_29278_gp_0_0_0_0 = { 1, GenInst_Array_Exists_m1_29278_gp_0_0_0_0_Types };
extern const Il2CppType Array_AsReadOnly_m1_29279_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_AsReadOnly_m1_29279_gp_0_0_0_0_Types[] = { &Array_AsReadOnly_m1_29279_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_AsReadOnly_m1_29279_gp_0_0_0_0 = { 1, GenInst_Array_AsReadOnly_m1_29279_gp_0_0_0_0_Types };
extern const Il2CppType Array_Find_m1_29280_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Find_m1_29280_gp_0_0_0_0_Types[] = { &Array_Find_m1_29280_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Find_m1_29280_gp_0_0_0_0 = { 1, GenInst_Array_Find_m1_29280_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLast_m1_29281_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLast_m1_29281_gp_0_0_0_0_Types[] = { &Array_FindLast_m1_29281_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLast_m1_29281_gp_0_0_0_0 = { 1, GenInst_Array_FindLast_m1_29281_gp_0_0_0_0_Types };
extern const Il2CppType InternalEnumerator_1_t1_3070_gp_0_0_0_0;
static const Il2CppType* GenInst_InternalEnumerator_1_t1_3070_gp_0_0_0_0_Types[] = { &InternalEnumerator_1_t1_3070_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InternalEnumerator_1_t1_3070_gp_0_0_0_0 = { 1, GenInst_InternalEnumerator_1_t1_3070_gp_0_0_0_0_Types };
extern const Il2CppType ArrayReadOnlyList_1_t1_3071_gp_0_0_0_0;
static const Il2CppType* GenInst_ArrayReadOnlyList_1_t1_3071_gp_0_0_0_0_Types[] = { &ArrayReadOnlyList_1_t1_3071_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ArrayReadOnlyList_1_t1_3071_gp_0_0_0_0 = { 1, GenInst_ArrayReadOnlyList_1_t1_3071_gp_0_0_0_0_Types };
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator0_t1_3072_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1_3072_gp_0_0_0_0_Types[] = { &U3CGetEnumeratorU3Ec__Iterator0_t1_3072_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1_3072_gp_0_0_0_0 = { 1, GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1_3072_gp_0_0_0_0_Types };
extern const Il2CppType IList_1_t1_3073_gp_0_0_0_0;
static const Il2CppType* GenInst_IList_1_t1_3073_gp_0_0_0_0_Types[] = { &IList_1_t1_3073_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t1_3073_gp_0_0_0_0 = { 1, GenInst_IList_1_t1_3073_gp_0_0_0_0_Types };
extern const Il2CppType ICollection_1_t1_3074_gp_0_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t1_3074_gp_0_0_0_0_Types[] = { &ICollection_1_t1_3074_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t1_3074_gp_0_0_0_0 = { 1, GenInst_ICollection_1_t1_3074_gp_0_0_0_0_Types };
extern const Il2CppType Nullable_1_t1_1747_gp_0_0_0_0;
static const Il2CppType* GenInst_Nullable_1_t1_1747_gp_0_0_0_0_Types[] = { &Nullable_1_t1_1747_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Nullable_1_t1_1747_gp_0_0_0_0 = { 1, GenInst_Nullable_1_t1_1747_gp_0_0_0_0_Types };
extern const Il2CppType CollectionDebuggerView_1_t1_3080_gp_0_0_0_0;
static const Il2CppType* GenInst_CollectionDebuggerView_1_t1_3080_gp_0_0_0_0_Types[] = { &CollectionDebuggerView_1_t1_3080_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CollectionDebuggerView_1_t1_3080_gp_0_0_0_0 = { 1, GenInst_CollectionDebuggerView_1_t1_3080_gp_0_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_4690_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_4690_0_0_0_Types[] = { &KeyValuePair_2_t1_4690_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_4690_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_4690_0_0_0_Types };
extern const Il2CppType CollectionDebuggerView_2_t1_3081_gp_0_0_0_0;
extern const Il2CppType CollectionDebuggerView_2_t1_3081_gp_1_0_0_0;
static const Il2CppType* GenInst_CollectionDebuggerView_2_t1_3081_gp_0_0_0_0_CollectionDebuggerView_2_t1_3081_gp_1_0_0_0_Types[] = { &CollectionDebuggerView_2_t1_3081_gp_0_0_0_0, &CollectionDebuggerView_2_t1_3081_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_CollectionDebuggerView_2_t1_3081_gp_0_0_0_0_CollectionDebuggerView_2_t1_3081_gp_1_0_0_0 = { 2, GenInst_CollectionDebuggerView_2_t1_3081_gp_0_0_0_0_CollectionDebuggerView_2_t1_3081_gp_1_0_0_0_Types };
extern const Il2CppType Comparer_1_t1_3082_gp_0_0_0_0;
static const Il2CppType* GenInst_Comparer_1_t1_3082_gp_0_0_0_0_Types[] = { &Comparer_1_t1_3082_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Comparer_1_t1_3082_gp_0_0_0_0 = { 1, GenInst_Comparer_1_t1_3082_gp_0_0_0_0_Types };
extern const Il2CppType DefaultComparer_t1_3083_gp_0_0_0_0;
static const Il2CppType* GenInst_DefaultComparer_t1_3083_gp_0_0_0_0_Types[] = { &DefaultComparer_t1_3083_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultComparer_t1_3083_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t1_3083_gp_0_0_0_0_Types };
extern const Il2CppType GenericComparer_1_t1_3063_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericComparer_1_t1_3063_gp_0_0_0_0_Types[] = { &GenericComparer_1_t1_3063_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericComparer_1_t1_3063_gp_0_0_0_0 = { 1, GenInst_GenericComparer_1_t1_3063_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_t1_3084_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t1_3084_gp_0_0_0_0_Types[] = { &Dictionary_2_t1_3084_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t1_3084_gp_0_0_0_0 = { 1, GenInst_Dictionary_2_t1_3084_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_t1_3084_gp_1_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t1_3084_gp_0_0_0_0_Dictionary_2_t1_3084_gp_1_0_0_0_Types[] = { &Dictionary_2_t1_3084_gp_0_0_0_0, &Dictionary_2_t1_3084_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t1_3084_gp_0_0_0_0_Dictionary_2_t1_3084_gp_1_0_0_0 = { 2, GenInst_Dictionary_2_t1_3084_gp_0_0_0_0_Dictionary_2_t1_3084_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t1_3084_gp_1_0_0_0_Types[] = { &Dictionary_2_t1_3084_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t1_3084_gp_1_0_0_0 = { 1, GenInst_Dictionary_2_t1_3084_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_4705_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_4705_0_0_0_Types[] = { &KeyValuePair_2_t1_4705_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_4705_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_4705_0_0_0_Types };
extern const Il2CppType Dictionary_2_Do_CopyTo_m1_29645_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t1_3084_gp_0_0_0_0_Dictionary_2_t1_3084_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m1_29645_gp_0_0_0_0_Types[] = { &Dictionary_2_t1_3084_gp_0_0_0_0, &Dictionary_2_t1_3084_gp_1_0_0_0, &Dictionary_2_Do_CopyTo_m1_29645_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t1_3084_gp_0_0_0_0_Dictionary_2_t1_3084_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m1_29645_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t1_3084_gp_0_0_0_0_Dictionary_2_t1_3084_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m1_29645_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_Do_ICollectionCopyTo_m1_29650_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t1_3084_gp_0_0_0_0_Dictionary_2_t1_3084_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m1_29650_gp_0_0_0_0_Types[] = { &Dictionary_2_t1_3084_gp_0_0_0_0, &Dictionary_2_t1_3084_gp_1_0_0_0, &Dictionary_2_Do_ICollectionCopyTo_m1_29650_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t1_3084_gp_0_0_0_0_Dictionary_2_t1_3084_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m1_29650_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t1_3084_gp_0_0_0_0_Dictionary_2_t1_3084_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m1_29650_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_Do_ICollectionCopyTo_m1_29650_gp_0_0_0_0_Object_t_0_0_0_Types[] = { &Dictionary_2_Do_ICollectionCopyTo_m1_29650_gp_0_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_Do_ICollectionCopyTo_m1_29650_gp_0_0_0_0_Object_t_0_0_0 = { 2, GenInst_Dictionary_2_Do_ICollectionCopyTo_m1_29650_gp_0_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t1_3084_gp_0_0_0_0_Dictionary_2_t1_3084_gp_1_0_0_0_DictionaryEntry_t1_284_0_0_0_Types[] = { &Dictionary_2_t1_3084_gp_0_0_0_0, &Dictionary_2_t1_3084_gp_1_0_0_0, &DictionaryEntry_t1_284_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t1_3084_gp_0_0_0_0_Dictionary_2_t1_3084_gp_1_0_0_0_DictionaryEntry_t1_284_0_0_0 = { 3, GenInst_Dictionary_2_t1_3084_gp_0_0_0_0_Dictionary_2_t1_3084_gp_1_0_0_0_DictionaryEntry_t1_284_0_0_0_Types };
extern const Il2CppType ShimEnumerator_t1_3085_gp_0_0_0_0;
extern const Il2CppType ShimEnumerator_t1_3085_gp_1_0_0_0;
static const Il2CppType* GenInst_ShimEnumerator_t1_3085_gp_0_0_0_0_ShimEnumerator_t1_3085_gp_1_0_0_0_Types[] = { &ShimEnumerator_t1_3085_gp_0_0_0_0, &ShimEnumerator_t1_3085_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ShimEnumerator_t1_3085_gp_0_0_0_0_ShimEnumerator_t1_3085_gp_1_0_0_0 = { 2, GenInst_ShimEnumerator_t1_3085_gp_0_0_0_0_ShimEnumerator_t1_3085_gp_1_0_0_0_Types };
extern const Il2CppType Enumerator_t1_3086_gp_0_0_0_0;
extern const Il2CppType Enumerator_t1_3086_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t1_3086_gp_0_0_0_0_Enumerator_t1_3086_gp_1_0_0_0_Types[] = { &Enumerator_t1_3086_gp_0_0_0_0, &Enumerator_t1_3086_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t1_3086_gp_0_0_0_0_Enumerator_t1_3086_gp_1_0_0_0 = { 2, GenInst_Enumerator_t1_3086_gp_0_0_0_0_Enumerator_t1_3086_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_4718_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_4718_0_0_0_Types[] = { &KeyValuePair_2_t1_4718_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_4718_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_4718_0_0_0_Types };
extern const Il2CppType KeyCollection_t1_3087_gp_0_0_0_0;
extern const Il2CppType KeyCollection_t1_3087_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyCollection_t1_3087_gp_0_0_0_0_KeyCollection_t1_3087_gp_1_0_0_0_Types[] = { &KeyCollection_t1_3087_gp_0_0_0_0, &KeyCollection_t1_3087_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t1_3087_gp_0_0_0_0_KeyCollection_t1_3087_gp_1_0_0_0 = { 2, GenInst_KeyCollection_t1_3087_gp_0_0_0_0_KeyCollection_t1_3087_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t1_3087_gp_0_0_0_0_Types[] = { &KeyCollection_t1_3087_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t1_3087_gp_0_0_0_0 = { 1, GenInst_KeyCollection_t1_3087_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t1_3088_gp_0_0_0_0;
extern const Il2CppType Enumerator_t1_3088_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t1_3088_gp_0_0_0_0_Enumerator_t1_3088_gp_1_0_0_0_Types[] = { &Enumerator_t1_3088_gp_0_0_0_0, &Enumerator_t1_3088_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t1_3088_gp_0_0_0_0_Enumerator_t1_3088_gp_1_0_0_0 = { 2, GenInst_Enumerator_t1_3088_gp_0_0_0_0_Enumerator_t1_3088_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerator_t1_3088_gp_0_0_0_0_Types[] = { &Enumerator_t1_3088_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t1_3088_gp_0_0_0_0 = { 1, GenInst_Enumerator_t1_3088_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t1_3087_gp_0_0_0_0_KeyCollection_t1_3087_gp_1_0_0_0_KeyCollection_t1_3087_gp_0_0_0_0_Types[] = { &KeyCollection_t1_3087_gp_0_0_0_0, &KeyCollection_t1_3087_gp_1_0_0_0, &KeyCollection_t1_3087_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t1_3087_gp_0_0_0_0_KeyCollection_t1_3087_gp_1_0_0_0_KeyCollection_t1_3087_gp_0_0_0_0 = { 3, GenInst_KeyCollection_t1_3087_gp_0_0_0_0_KeyCollection_t1_3087_gp_1_0_0_0_KeyCollection_t1_3087_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t1_3087_gp_0_0_0_0_KeyCollection_t1_3087_gp_0_0_0_0_Types[] = { &KeyCollection_t1_3087_gp_0_0_0_0, &KeyCollection_t1_3087_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t1_3087_gp_0_0_0_0_KeyCollection_t1_3087_gp_0_0_0_0 = { 2, GenInst_KeyCollection_t1_3087_gp_0_0_0_0_KeyCollection_t1_3087_gp_0_0_0_0_Types };
extern const Il2CppType ValueCollection_t1_3089_gp_0_0_0_0;
extern const Il2CppType ValueCollection_t1_3089_gp_1_0_0_0;
static const Il2CppType* GenInst_ValueCollection_t1_3089_gp_0_0_0_0_ValueCollection_t1_3089_gp_1_0_0_0_Types[] = { &ValueCollection_t1_3089_gp_0_0_0_0, &ValueCollection_t1_3089_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t1_3089_gp_0_0_0_0_ValueCollection_t1_3089_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t1_3089_gp_0_0_0_0_ValueCollection_t1_3089_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t1_3089_gp_1_0_0_0_Types[] = { &ValueCollection_t1_3089_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t1_3089_gp_1_0_0_0 = { 1, GenInst_ValueCollection_t1_3089_gp_1_0_0_0_Types };
extern const Il2CppType Enumerator_t1_3090_gp_0_0_0_0;
extern const Il2CppType Enumerator_t1_3090_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t1_3090_gp_0_0_0_0_Enumerator_t1_3090_gp_1_0_0_0_Types[] = { &Enumerator_t1_3090_gp_0_0_0_0, &Enumerator_t1_3090_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t1_3090_gp_0_0_0_0_Enumerator_t1_3090_gp_1_0_0_0 = { 2, GenInst_Enumerator_t1_3090_gp_0_0_0_0_Enumerator_t1_3090_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerator_t1_3090_gp_1_0_0_0_Types[] = { &Enumerator_t1_3090_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t1_3090_gp_1_0_0_0 = { 1, GenInst_Enumerator_t1_3090_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t1_3089_gp_0_0_0_0_ValueCollection_t1_3089_gp_1_0_0_0_ValueCollection_t1_3089_gp_1_0_0_0_Types[] = { &ValueCollection_t1_3089_gp_0_0_0_0, &ValueCollection_t1_3089_gp_1_0_0_0, &ValueCollection_t1_3089_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t1_3089_gp_0_0_0_0_ValueCollection_t1_3089_gp_1_0_0_0_ValueCollection_t1_3089_gp_1_0_0_0 = { 3, GenInst_ValueCollection_t1_3089_gp_0_0_0_0_ValueCollection_t1_3089_gp_1_0_0_0_ValueCollection_t1_3089_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t1_3089_gp_1_0_0_0_ValueCollection_t1_3089_gp_1_0_0_0_Types[] = { &ValueCollection_t1_3089_gp_1_0_0_0, &ValueCollection_t1_3089_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t1_3089_gp_1_0_0_0_ValueCollection_t1_3089_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t1_3089_gp_1_0_0_0_ValueCollection_t1_3089_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t1_3084_gp_0_0_0_0_Dictionary_2_t1_3084_gp_1_0_0_0_KeyValuePair_2_t1_4705_0_0_0_Types[] = { &Dictionary_2_t1_3084_gp_0_0_0_0, &Dictionary_2_t1_3084_gp_1_0_0_0, &KeyValuePair_2_t1_4705_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t1_3084_gp_0_0_0_0_Dictionary_2_t1_3084_gp_1_0_0_0_KeyValuePair_2_t1_4705_0_0_0 = { 3, GenInst_Dictionary_2_t1_3084_gp_0_0_0_0_Dictionary_2_t1_3084_gp_1_0_0_0_KeyValuePair_2_t1_4705_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1_4705_0_0_0_KeyValuePair_2_t1_4705_0_0_0_Types[] = { &KeyValuePair_2_t1_4705_0_0_0, &KeyValuePair_2_t1_4705_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_4705_0_0_0_KeyValuePair_2_t1_4705_0_0_0 = { 2, GenInst_KeyValuePair_2_t1_4705_0_0_0_KeyValuePair_2_t1_4705_0_0_0_Types };
extern const Il2CppType EqualityComparer_1_t1_3092_gp_0_0_0_0;
static const Il2CppType* GenInst_EqualityComparer_1_t1_3092_gp_0_0_0_0_Types[] = { &EqualityComparer_1_t1_3092_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_EqualityComparer_1_t1_3092_gp_0_0_0_0 = { 1, GenInst_EqualityComparer_1_t1_3092_gp_0_0_0_0_Types };
extern const Il2CppType DefaultComparer_t1_3093_gp_0_0_0_0;
static const Il2CppType* GenInst_DefaultComparer_t1_3093_gp_0_0_0_0_Types[] = { &DefaultComparer_t1_3093_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultComparer_t1_3093_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t1_3093_gp_0_0_0_0_Types };
extern const Il2CppType GenericEqualityComparer_1_t1_3064_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericEqualityComparer_1_t1_3064_gp_0_0_0_0_Types[] = { &GenericEqualityComparer_1_t1_3064_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericEqualityComparer_1_t1_3064_gp_0_0_0_0 = { 1, GenInst_GenericEqualityComparer_1_t1_3064_gp_0_0_0_0_Types };
extern const Il2CppType IDictionary_2_t1_3095_gp_0_0_0_0;
static const Il2CppType* GenInst_IDictionary_2_t1_3095_gp_0_0_0_0_Types[] = { &IDictionary_2_t1_3095_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t1_3095_gp_0_0_0_0 = { 1, GenInst_IDictionary_2_t1_3095_gp_0_0_0_0_Types };
extern const Il2CppType IDictionary_2_t1_3095_gp_1_0_0_0;
static const Il2CppType* GenInst_IDictionary_2_t1_3095_gp_1_0_0_0_Types[] = { &IDictionary_2_t1_3095_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t1_3095_gp_1_0_0_0 = { 1, GenInst_IDictionary_2_t1_3095_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_4756_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_4756_0_0_0_Types[] = { &KeyValuePair_2_t1_4756_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_4756_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_4756_0_0_0_Types };
static const Il2CppType* GenInst_IDictionary_2_t1_3095_gp_0_0_0_0_IDictionary_2_t1_3095_gp_1_0_0_0_Types[] = { &IDictionary_2_t1_3095_gp_0_0_0_0, &IDictionary_2_t1_3095_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t1_3095_gp_0_0_0_0_IDictionary_2_t1_3095_gp_1_0_0_0 = { 2, GenInst_IDictionary_2_t1_3095_gp_0_0_0_0_IDictionary_2_t1_3095_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_3097_gp_0_0_0_0;
extern const Il2CppType KeyValuePair_2_t1_3097_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_3097_gp_0_0_0_0_KeyValuePair_2_t1_3097_gp_1_0_0_0_Types[] = { &KeyValuePair_2_t1_3097_gp_0_0_0_0, &KeyValuePair_2_t1_3097_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_3097_gp_0_0_0_0_KeyValuePair_2_t1_3097_gp_1_0_0_0 = { 2, GenInst_KeyValuePair_2_t1_3097_gp_0_0_0_0_KeyValuePair_2_t1_3097_gp_1_0_0_0_Types };
extern const Il2CppType List_1_t1_3098_gp_0_0_0_0;
static const Il2CppType* GenInst_List_1_t1_3098_gp_0_0_0_0_Types[] = { &List_1_t1_3098_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1_3098_gp_0_0_0_0 = { 1, GenInst_List_1_t1_3098_gp_0_0_0_0_Types };
extern const Il2CppType List_1_ConvertAll_m1_29796_gp_0_0_0_0;
static const Il2CppType* GenInst_List_1_t1_3098_gp_0_0_0_0_List_1_ConvertAll_m1_29796_gp_0_0_0_0_Types[] = { &List_1_t1_3098_gp_0_0_0_0, &List_1_ConvertAll_m1_29796_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1_3098_gp_0_0_0_0_List_1_ConvertAll_m1_29796_gp_0_0_0_0 = { 2, GenInst_List_1_t1_3098_gp_0_0_0_0_List_1_ConvertAll_m1_29796_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_List_1_ConvertAll_m1_29796_gp_0_0_0_0_Types[] = { &List_1_ConvertAll_m1_29796_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_ConvertAll_m1_29796_gp_0_0_0_0 = { 1, GenInst_List_1_ConvertAll_m1_29796_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t1_3099_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t1_3099_gp_0_0_0_0_Types[] = { &Enumerator_t1_3099_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t1_3099_gp_0_0_0_0 = { 1, GenInst_Enumerator_t1_3099_gp_0_0_0_0_Types };
extern const Il2CppType Collection_1_t1_3100_gp_0_0_0_0;
static const Il2CppType* GenInst_Collection_1_t1_3100_gp_0_0_0_0_Types[] = { &Collection_1_t1_3100_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Collection_1_t1_3100_gp_0_0_0_0 = { 1, GenInst_Collection_1_t1_3100_gp_0_0_0_0_Types };
extern const Il2CppType KeyedCollection_2_t1_3101_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyedCollection_2_t1_3101_gp_1_0_0_0_Types[] = { &KeyedCollection_2_t1_3101_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyedCollection_2_t1_3101_gp_1_0_0_0 = { 1, GenInst_KeyedCollection_2_t1_3101_gp_1_0_0_0_Types };
extern const Il2CppType KeyedCollection_2_t1_3101_gp_0_0_0_0;
static const Il2CppType* GenInst_KeyedCollection_2_t1_3101_gp_0_0_0_0_Types[] = { &KeyedCollection_2_t1_3101_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyedCollection_2_t1_3101_gp_0_0_0_0 = { 1, GenInst_KeyedCollection_2_t1_3101_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_KeyedCollection_2_t1_3101_gp_0_0_0_0_KeyedCollection_2_t1_3101_gp_1_0_0_0_Types[] = { &KeyedCollection_2_t1_3101_gp_0_0_0_0, &KeyedCollection_2_t1_3101_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyedCollection_2_t1_3101_gp_0_0_0_0_KeyedCollection_2_t1_3101_gp_1_0_0_0 = { 2, GenInst_KeyedCollection_2_t1_3101_gp_0_0_0_0_KeyedCollection_2_t1_3101_gp_1_0_0_0_Types };
extern const Il2CppType ReadOnlyCollection_1_t1_3102_gp_0_0_0_0;
static const Il2CppType* GenInst_ReadOnlyCollection_1_t1_3102_gp_0_0_0_0_Types[] = { &ReadOnlyCollection_1_t1_3102_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ReadOnlyCollection_1_t1_3102_gp_0_0_0_0 = { 1, GenInst_ReadOnlyCollection_1_t1_3102_gp_0_0_0_0_Types };
extern const Il2CppType EventInfo_AddEventFrame_m1_30082_gp_0_0_0_0;
extern const Il2CppType EventInfo_AddEventFrame_m1_30082_gp_1_0_0_0;
static const Il2CppType* GenInst_EventInfo_AddEventFrame_m1_30082_gp_0_0_0_0_EventInfo_AddEventFrame_m1_30082_gp_1_0_0_0_Types[] = { &EventInfo_AddEventFrame_m1_30082_gp_0_0_0_0, &EventInfo_AddEventFrame_m1_30082_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_EventInfo_AddEventFrame_m1_30082_gp_0_0_0_0_EventInfo_AddEventFrame_m1_30082_gp_1_0_0_0 = { 2, GenInst_EventInfo_AddEventFrame_m1_30082_gp_0_0_0_0_EventInfo_AddEventFrame_m1_30082_gp_1_0_0_0_Types };
extern const Il2CppType EventInfo_StaticAddEventAdapterFrame_m1_30083_gp_0_0_0_0;
static const Il2CppType* GenInst_EventInfo_StaticAddEventAdapterFrame_m1_30083_gp_0_0_0_0_Types[] = { &EventInfo_StaticAddEventAdapterFrame_m1_30083_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_EventInfo_StaticAddEventAdapterFrame_m1_30083_gp_0_0_0_0 = { 1, GenInst_EventInfo_StaticAddEventAdapterFrame_m1_30083_gp_0_0_0_0_Types };
extern const Il2CppType MonoProperty_GetterAdapterFrame_m1_30104_gp_0_0_0_0;
extern const Il2CppType MonoProperty_GetterAdapterFrame_m1_30104_gp_1_0_0_0;
static const Il2CppType* GenInst_MonoProperty_GetterAdapterFrame_m1_30104_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m1_30104_gp_1_0_0_0_Types[] = { &MonoProperty_GetterAdapterFrame_m1_30104_gp_0_0_0_0, &MonoProperty_GetterAdapterFrame_m1_30104_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoProperty_GetterAdapterFrame_m1_30104_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m1_30104_gp_1_0_0_0 = { 2, GenInst_MonoProperty_GetterAdapterFrame_m1_30104_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m1_30104_gp_1_0_0_0_Types };
extern const Il2CppType MonoProperty_StaticGetterAdapterFrame_m1_30105_gp_0_0_0_0;
static const Il2CppType* GenInst_MonoProperty_StaticGetterAdapterFrame_m1_30105_gp_0_0_0_0_Types[] = { &MonoProperty_StaticGetterAdapterFrame_m1_30105_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoProperty_StaticGetterAdapterFrame_m1_30105_gp_0_0_0_0 = { 1, GenInst_MonoProperty_StaticGetterAdapterFrame_m1_30105_gp_0_0_0_0_Types };
extern const Il2CppType ArraySegment_1_t1_3192_gp_0_0_0_0;
static const Il2CppType* GenInst_ArraySegment_1_t1_3192_gp_0_0_0_0_Types[] = { &ArraySegment_1_t1_3192_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ArraySegment_1_t1_3192_gp_0_0_0_0 = { 1, GenInst_ArraySegment_1_t1_3192_gp_0_0_0_0_Types };
extern const Il2CppType Nullable_Compare_m1_31199_gp_0_0_0_0;
static const Il2CppType* GenInst_Nullable_Compare_m1_31199_gp_0_0_0_0_Types[] = { &Nullable_Compare_m1_31199_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Nullable_Compare_m1_31199_gp_0_0_0_0 = { 1, GenInst_Nullable_Compare_m1_31199_gp_0_0_0_0_Types };
extern const Il2CppType Nullable_Equals_m1_31200_gp_0_0_0_0;
static const Il2CppType* GenInst_Nullable_Equals_m1_31200_gp_0_0_0_0_Types[] = { &Nullable_Equals_m1_31200_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Nullable_Equals_m1_31200_gp_0_0_0_0 = { 1, GenInst_Nullable_Equals_m1_31200_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_First_m5_93_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_First_m5_93_gp_0_0_0_0_Types[] = { &Enumerable_First_m5_93_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_First_m5_93_gp_0_0_0_0 = { 1, GenInst_Enumerable_First_m5_93_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_First_m5_93_gp_0_0_0_0_Boolean_t1_20_0_0_0_Types[] = { &Enumerable_First_m5_93_gp_0_0_0_0, &Boolean_t1_20_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_First_m5_93_gp_0_0_0_0_Boolean_t1_20_0_0_0 = { 2, GenInst_Enumerable_First_m5_93_gp_0_0_0_0_Boolean_t1_20_0_0_0_Types };
extern const Il2CppType Enumerable_FirstOrDefault_m5_94_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_FirstOrDefault_m5_94_gp_0_0_0_0_Types[] = { &Enumerable_FirstOrDefault_m5_94_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_FirstOrDefault_m5_94_gp_0_0_0_0 = { 1, GenInst_Enumerable_FirstOrDefault_m5_94_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_FirstOrDefault_m5_94_gp_0_0_0_0_Boolean_t1_20_0_0_0_Types[] = { &Enumerable_FirstOrDefault_m5_94_gp_0_0_0_0, &Boolean_t1_20_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_FirstOrDefault_m5_94_gp_0_0_0_0_Boolean_t1_20_0_0_0 = { 2, GenInst_Enumerable_FirstOrDefault_m5_94_gp_0_0_0_0_Boolean_t1_20_0_0_0_Types };
extern const Il2CppType Enumerable_Where_m5_95_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Where_m5_95_gp_0_0_0_0_Types[] = { &Enumerable_Where_m5_95_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Where_m5_95_gp_0_0_0_0 = { 1, GenInst_Enumerable_Where_m5_95_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Where_m5_95_gp_0_0_0_0_Boolean_t1_20_0_0_0_Types[] = { &Enumerable_Where_m5_95_gp_0_0_0_0, &Boolean_t1_20_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Where_m5_95_gp_0_0_0_0_Boolean_t1_20_0_0_0 = { 2, GenInst_Enumerable_Where_m5_95_gp_0_0_0_0_Boolean_t1_20_0_0_0_Types };
extern const Il2CppType Enumerable_CreateWhereIterator_m5_96_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateWhereIterator_m5_96_gp_0_0_0_0_Types[] = { &Enumerable_CreateWhereIterator_m5_96_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m5_96_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateWhereIterator_m5_96_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_CreateWhereIterator_m5_96_gp_0_0_0_0_Boolean_t1_20_0_0_0_Types[] = { &Enumerable_CreateWhereIterator_m5_96_gp_0_0_0_0, &Boolean_t1_20_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m5_96_gp_0_0_0_0_Boolean_t1_20_0_0_0 = { 2, GenInst_Enumerable_CreateWhereIterator_m5_96_gp_0_0_0_0_Boolean_t1_20_0_0_0_Types };
extern const Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t5_24_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t5_24_gp_0_0_0_0_Types[] = { &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t5_24_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t5_24_gp_0_0_0_0 = { 1, GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t5_24_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t5_24_gp_0_0_0_0_Boolean_t1_20_0_0_0_Types[] = { &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t5_24_gp_0_0_0_0, &Boolean_t1_20_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t5_24_gp_0_0_0_0_Boolean_t1_20_0_0_0 = { 2, GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t5_24_gp_0_0_0_0_Boolean_t1_20_0_0_0_Types };
extern const Il2CppType Queue_1_t3_299_gp_0_0_0_0;
static const Il2CppType* GenInst_Queue_1_t3_299_gp_0_0_0_0_Types[] = { &Queue_1_t3_299_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Queue_1_t3_299_gp_0_0_0_0 = { 1, GenInst_Queue_1_t3_299_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t3_300_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t3_300_gp_0_0_0_0_Types[] = { &Enumerator_t3_300_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t3_300_gp_0_0_0_0 = { 1, GenInst_Enumerator_t3_300_gp_0_0_0_0_Types };
extern const Il2CppType SortedList_2_t3_301_gp_0_0_0_0;
static const Il2CppType* GenInst_SortedList_2_t3_301_gp_0_0_0_0_Types[] = { &SortedList_2_t3_301_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SortedList_2_t3_301_gp_0_0_0_0 = { 1, GenInst_SortedList_2_t3_301_gp_0_0_0_0_Types };
extern const Il2CppType SortedList_2_t3_301_gp_1_0_0_0;
static const Il2CppType* GenInst_SortedList_2_t3_301_gp_1_0_0_0_Types[] = { &SortedList_2_t3_301_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_SortedList_2_t3_301_gp_1_0_0_0 = { 1, GenInst_SortedList_2_t3_301_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_SortedList_2_t3_301_gp_0_0_0_0_SortedList_2_t3_301_gp_1_0_0_0_Types[] = { &SortedList_2_t3_301_gp_0_0_0_0, &SortedList_2_t3_301_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_SortedList_2_t3_301_gp_0_0_0_0_SortedList_2_t3_301_gp_1_0_0_0 = { 2, GenInst_SortedList_2_t3_301_gp_0_0_0_0_SortedList_2_t3_301_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_4816_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_4816_0_0_0_Types[] = { &KeyValuePair_2_t1_4816_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_4816_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_4816_0_0_0_Types };
extern const Il2CppType EnumeratorMode_t3_302_gp_0_0_0_0;
extern const Il2CppType EnumeratorMode_t3_302_gp_1_0_0_0;
static const Il2CppType* GenInst_EnumeratorMode_t3_302_gp_0_0_0_0_EnumeratorMode_t3_302_gp_1_0_0_0_Types[] = { &EnumeratorMode_t3_302_gp_0_0_0_0, &EnumeratorMode_t3_302_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_EnumeratorMode_t3_302_gp_0_0_0_0_EnumeratorMode_t3_302_gp_1_0_0_0 = { 2, GenInst_EnumeratorMode_t3_302_gp_0_0_0_0_EnumeratorMode_t3_302_gp_1_0_0_0_Types };
extern const Il2CppType Enumerator_t3_303_gp_0_0_0_0;
extern const Il2CppType Enumerator_t3_303_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t3_303_gp_0_0_0_0_Enumerator_t3_303_gp_1_0_0_0_Types[] = { &Enumerator_t3_303_gp_0_0_0_0, &Enumerator_t3_303_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t3_303_gp_0_0_0_0_Enumerator_t3_303_gp_1_0_0_0 = { 2, GenInst_Enumerator_t3_303_gp_0_0_0_0_Enumerator_t3_303_gp_1_0_0_0_Types };
extern const Il2CppType KeyEnumerator_t3_304_gp_0_0_0_0;
extern const Il2CppType KeyEnumerator_t3_304_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyEnumerator_t3_304_gp_0_0_0_0_KeyEnumerator_t3_304_gp_1_0_0_0_Types[] = { &KeyEnumerator_t3_304_gp_0_0_0_0, &KeyEnumerator_t3_304_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyEnumerator_t3_304_gp_0_0_0_0_KeyEnumerator_t3_304_gp_1_0_0_0 = { 2, GenInst_KeyEnumerator_t3_304_gp_0_0_0_0_KeyEnumerator_t3_304_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_KeyEnumerator_t3_304_gp_0_0_0_0_Types[] = { &KeyEnumerator_t3_304_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyEnumerator_t3_304_gp_0_0_0_0 = { 1, GenInst_KeyEnumerator_t3_304_gp_0_0_0_0_Types };
extern const Il2CppType ValueEnumerator_t3_305_gp_0_0_0_0;
extern const Il2CppType ValueEnumerator_t3_305_gp_1_0_0_0;
static const Il2CppType* GenInst_ValueEnumerator_t3_305_gp_0_0_0_0_ValueEnumerator_t3_305_gp_1_0_0_0_Types[] = { &ValueEnumerator_t3_305_gp_0_0_0_0, &ValueEnumerator_t3_305_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueEnumerator_t3_305_gp_0_0_0_0_ValueEnumerator_t3_305_gp_1_0_0_0 = { 2, GenInst_ValueEnumerator_t3_305_gp_0_0_0_0_ValueEnumerator_t3_305_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueEnumerator_t3_305_gp_1_0_0_0_Types[] = { &ValueEnumerator_t3_305_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueEnumerator_t3_305_gp_1_0_0_0 = { 1, GenInst_ValueEnumerator_t3_305_gp_1_0_0_0_Types };
extern const Il2CppType ListKeys_t3_306_gp_0_0_0_0;
extern const Il2CppType ListKeys_t3_306_gp_1_0_0_0;
static const Il2CppType* GenInst_ListKeys_t3_306_gp_0_0_0_0_ListKeys_t3_306_gp_1_0_0_0_Types[] = { &ListKeys_t3_306_gp_0_0_0_0, &ListKeys_t3_306_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ListKeys_t3_306_gp_0_0_0_0_ListKeys_t3_306_gp_1_0_0_0 = { 2, GenInst_ListKeys_t3_306_gp_0_0_0_0_ListKeys_t3_306_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ListKeys_t3_306_gp_0_0_0_0_Types[] = { &ListKeys_t3_306_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ListKeys_t3_306_gp_0_0_0_0 = { 1, GenInst_ListKeys_t3_306_gp_0_0_0_0_Types };
extern const Il2CppType GetEnumeratorU3Ec__Iterator2_t3_307_gp_0_0_0_0;
extern const Il2CppType GetEnumeratorU3Ec__Iterator2_t3_307_gp_1_0_0_0;
static const Il2CppType* GenInst_GetEnumeratorU3Ec__Iterator2_t3_307_gp_0_0_0_0_GetEnumeratorU3Ec__Iterator2_t3_307_gp_1_0_0_0_Types[] = { &GetEnumeratorU3Ec__Iterator2_t3_307_gp_0_0_0_0, &GetEnumeratorU3Ec__Iterator2_t3_307_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_GetEnumeratorU3Ec__Iterator2_t3_307_gp_0_0_0_0_GetEnumeratorU3Ec__Iterator2_t3_307_gp_1_0_0_0 = { 2, GenInst_GetEnumeratorU3Ec__Iterator2_t3_307_gp_0_0_0_0_GetEnumeratorU3Ec__Iterator2_t3_307_gp_1_0_0_0_Types };
extern const Il2CppType ListValues_t3_308_gp_0_0_0_0;
extern const Il2CppType ListValues_t3_308_gp_1_0_0_0;
static const Il2CppType* GenInst_ListValues_t3_308_gp_0_0_0_0_ListValues_t3_308_gp_1_0_0_0_Types[] = { &ListValues_t3_308_gp_0_0_0_0, &ListValues_t3_308_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ListValues_t3_308_gp_0_0_0_0_ListValues_t3_308_gp_1_0_0_0 = { 2, GenInst_ListValues_t3_308_gp_0_0_0_0_ListValues_t3_308_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ListValues_t3_308_gp_1_0_0_0_Types[] = { &ListValues_t3_308_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ListValues_t3_308_gp_1_0_0_0 = { 1, GenInst_ListValues_t3_308_gp_1_0_0_0_Types };
extern const Il2CppType GetEnumeratorU3Ec__Iterator3_t3_309_gp_0_0_0_0;
extern const Il2CppType GetEnumeratorU3Ec__Iterator3_t3_309_gp_1_0_0_0;
static const Il2CppType* GenInst_GetEnumeratorU3Ec__Iterator3_t3_309_gp_0_0_0_0_GetEnumeratorU3Ec__Iterator3_t3_309_gp_1_0_0_0_Types[] = { &GetEnumeratorU3Ec__Iterator3_t3_309_gp_0_0_0_0, &GetEnumeratorU3Ec__Iterator3_t3_309_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_GetEnumeratorU3Ec__Iterator3_t3_309_gp_0_0_0_0_GetEnumeratorU3Ec__Iterator3_t3_309_gp_1_0_0_0 = { 2, GenInst_GetEnumeratorU3Ec__Iterator3_t3_309_gp_0_0_0_0_GetEnumeratorU3Ec__Iterator3_t3_309_gp_1_0_0_0_Types };
extern const Il2CppType GetEnumeratorU3Ec__Iterator0_t3_310_gp_0_0_0_0;
extern const Il2CppType GetEnumeratorU3Ec__Iterator0_t3_310_gp_1_0_0_0;
static const Il2CppType* GenInst_GetEnumeratorU3Ec__Iterator0_t3_310_gp_0_0_0_0_GetEnumeratorU3Ec__Iterator0_t3_310_gp_1_0_0_0_Types[] = { &GetEnumeratorU3Ec__Iterator0_t3_310_gp_0_0_0_0, &GetEnumeratorU3Ec__Iterator0_t3_310_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_GetEnumeratorU3Ec__Iterator0_t3_310_gp_0_0_0_0_GetEnumeratorU3Ec__Iterator0_t3_310_gp_1_0_0_0 = { 2, GenInst_GetEnumeratorU3Ec__Iterator0_t3_310_gp_0_0_0_0_GetEnumeratorU3Ec__Iterator0_t3_310_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_4833_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_4833_0_0_0_Types[] = { &KeyValuePair_2_t1_4833_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_4833_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_4833_0_0_0_Types };
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator1_t3_311_gp_0_0_0_0;
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator1_t3_311_gp_1_0_0_0;
static const Il2CppType* GenInst_U3CGetEnumeratorU3Ec__Iterator1_t3_311_gp_0_0_0_0_U3CGetEnumeratorU3Ec__Iterator1_t3_311_gp_1_0_0_0_Types[] = { &U3CGetEnumeratorU3Ec__Iterator1_t3_311_gp_0_0_0_0, &U3CGetEnumeratorU3Ec__Iterator1_t3_311_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CGetEnumeratorU3Ec__Iterator1_t3_311_gp_0_0_0_0_U3CGetEnumeratorU3Ec__Iterator1_t3_311_gp_1_0_0_0 = { 2, GenInst_U3CGetEnumeratorU3Ec__Iterator1_t3_311_gp_0_0_0_0_U3CGetEnumeratorU3Ec__Iterator1_t3_311_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1_4835_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1_4835_0_0_0_Types[] = { &KeyValuePair_2_t1_4835_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1_4835_0_0_0 = { 1, GenInst_KeyValuePair_2_t1_4835_0_0_0_Types };
extern const Il2CppType Stack_1_t3_312_gp_0_0_0_0;
static const Il2CppType* GenInst_Stack_1_t3_312_gp_0_0_0_0_Types[] = { &Stack_1_t3_312_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Stack_1_t3_312_gp_0_0_0_0 = { 1, GenInst_Stack_1_t3_312_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t3_313_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t3_313_gp_0_0_0_0_Types[] = { &Enumerator_t3_313_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t3_313_gp_0_0_0_0 = { 1, GenInst_Enumerator_t3_313_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m6_2136_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m6_2136_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m6_2136_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m6_2136_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m6_2136_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m6_2137_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m6_2137_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m6_2137_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m6_2137_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m6_2137_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m6_2138_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m6_2138_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m6_2138_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m6_2138_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m6_2138_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m6_2139_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m6_2139_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m6_2139_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m6_2139_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m6_2139_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponents_m6_2141_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponents_m6_2141_gp_0_0_0_0_Types[] = { &Component_GetComponents_m6_2141_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponents_m6_2141_gp_0_0_0_0 = { 1, GenInst_Component_GetComponents_m6_2141_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponents_m6_2144_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponents_m6_2144_gp_0_0_0_0_Types[] = { &GameObject_GetComponents_m6_2144_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponents_m6_2144_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponents_m6_2144_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentsInChildren_m6_2146_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentsInChildren_m6_2146_gp_0_0_0_0_Types[] = { &GameObject_GetComponentsInChildren_m6_2146_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInChildren_m6_2146_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInChildren_m6_2146_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentsInParent_m6_2147_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentsInParent_m6_2147_gp_0_0_0_0_Types[] = { &GameObject_GetComponentsInParent_m6_2147_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInParent_m6_2147_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInParent_m6_2147_gp_0_0_0_0_Types };
extern const Il2CppType InvokableCall_1_t6_373_gp_0_0_0_0;
static const Il2CppType* GenInst_InvokableCall_1_t6_373_gp_0_0_0_0_Types[] = { &InvokableCall_1_t6_373_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_1_t6_373_gp_0_0_0_0 = { 1, GenInst_InvokableCall_1_t6_373_gp_0_0_0_0_Types };
extern const Il2CppType InvokableCall_2_t6_374_gp_0_0_0_0;
extern const Il2CppType InvokableCall_2_t6_374_gp_1_0_0_0;
static const Il2CppType* GenInst_InvokableCall_2_t6_374_gp_0_0_0_0_InvokableCall_2_t6_374_gp_1_0_0_0_Types[] = { &InvokableCall_2_t6_374_gp_0_0_0_0, &InvokableCall_2_t6_374_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t6_374_gp_0_0_0_0_InvokableCall_2_t6_374_gp_1_0_0_0 = { 2, GenInst_InvokableCall_2_t6_374_gp_0_0_0_0_InvokableCall_2_t6_374_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_2_t6_374_gp_0_0_0_0_Types[] = { &InvokableCall_2_t6_374_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t6_374_gp_0_0_0_0 = { 1, GenInst_InvokableCall_2_t6_374_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_2_t6_374_gp_1_0_0_0_Types[] = { &InvokableCall_2_t6_374_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t6_374_gp_1_0_0_0 = { 1, GenInst_InvokableCall_2_t6_374_gp_1_0_0_0_Types };
extern const Il2CppType InvokableCall_3_t6_375_gp_0_0_0_0;
extern const Il2CppType InvokableCall_3_t6_375_gp_1_0_0_0;
extern const Il2CppType InvokableCall_3_t6_375_gp_2_0_0_0;
static const Il2CppType* GenInst_InvokableCall_3_t6_375_gp_0_0_0_0_InvokableCall_3_t6_375_gp_1_0_0_0_InvokableCall_3_t6_375_gp_2_0_0_0_Types[] = { &InvokableCall_3_t6_375_gp_0_0_0_0, &InvokableCall_3_t6_375_gp_1_0_0_0, &InvokableCall_3_t6_375_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t6_375_gp_0_0_0_0_InvokableCall_3_t6_375_gp_1_0_0_0_InvokableCall_3_t6_375_gp_2_0_0_0 = { 3, GenInst_InvokableCall_3_t6_375_gp_0_0_0_0_InvokableCall_3_t6_375_gp_1_0_0_0_InvokableCall_3_t6_375_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t6_375_gp_0_0_0_0_Types[] = { &InvokableCall_3_t6_375_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t6_375_gp_0_0_0_0 = { 1, GenInst_InvokableCall_3_t6_375_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t6_375_gp_1_0_0_0_Types[] = { &InvokableCall_3_t6_375_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t6_375_gp_1_0_0_0 = { 1, GenInst_InvokableCall_3_t6_375_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t6_375_gp_2_0_0_0_Types[] = { &InvokableCall_3_t6_375_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t6_375_gp_2_0_0_0 = { 1, GenInst_InvokableCall_3_t6_375_gp_2_0_0_0_Types };
extern const Il2CppType InvokableCall_4_t6_376_gp_0_0_0_0;
extern const Il2CppType InvokableCall_4_t6_376_gp_1_0_0_0;
extern const Il2CppType InvokableCall_4_t6_376_gp_2_0_0_0;
extern const Il2CppType InvokableCall_4_t6_376_gp_3_0_0_0;
static const Il2CppType* GenInst_InvokableCall_4_t6_376_gp_0_0_0_0_InvokableCall_4_t6_376_gp_1_0_0_0_InvokableCall_4_t6_376_gp_2_0_0_0_InvokableCall_4_t6_376_gp_3_0_0_0_Types[] = { &InvokableCall_4_t6_376_gp_0_0_0_0, &InvokableCall_4_t6_376_gp_1_0_0_0, &InvokableCall_4_t6_376_gp_2_0_0_0, &InvokableCall_4_t6_376_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t6_376_gp_0_0_0_0_InvokableCall_4_t6_376_gp_1_0_0_0_InvokableCall_4_t6_376_gp_2_0_0_0_InvokableCall_4_t6_376_gp_3_0_0_0 = { 4, GenInst_InvokableCall_4_t6_376_gp_0_0_0_0_InvokableCall_4_t6_376_gp_1_0_0_0_InvokableCall_4_t6_376_gp_2_0_0_0_InvokableCall_4_t6_376_gp_3_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t6_376_gp_0_0_0_0_Types[] = { &InvokableCall_4_t6_376_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t6_376_gp_0_0_0_0 = { 1, GenInst_InvokableCall_4_t6_376_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t6_376_gp_1_0_0_0_Types[] = { &InvokableCall_4_t6_376_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t6_376_gp_1_0_0_0 = { 1, GenInst_InvokableCall_4_t6_376_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t6_376_gp_2_0_0_0_Types[] = { &InvokableCall_4_t6_376_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t6_376_gp_2_0_0_0 = { 1, GenInst_InvokableCall_4_t6_376_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t6_376_gp_3_0_0_0_Types[] = { &InvokableCall_4_t6_376_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t6_376_gp_3_0_0_0 = { 1, GenInst_InvokableCall_4_t6_376_gp_3_0_0_0_Types };
extern const Il2CppType CachedInvokableCall_1_t6_299_gp_0_0_0_0;
static const Il2CppType* GenInst_CachedInvokableCall_1_t6_299_gp_0_0_0_0_Types[] = { &CachedInvokableCall_1_t6_299_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CachedInvokableCall_1_t6_299_gp_0_0_0_0 = { 1, GenInst_CachedInvokableCall_1_t6_299_gp_0_0_0_0_Types };
extern const Il2CppType UnityEvent_1_t6_377_gp_0_0_0_0;
static const Il2CppType* GenInst_UnityEvent_1_t6_377_gp_0_0_0_0_Types[] = { &UnityEvent_1_t6_377_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_1_t6_377_gp_0_0_0_0 = { 1, GenInst_UnityEvent_1_t6_377_gp_0_0_0_0_Types };
extern const Il2CppType UnityEvent_2_t6_378_gp_0_0_0_0;
extern const Il2CppType UnityEvent_2_t6_378_gp_1_0_0_0;
static const Il2CppType* GenInst_UnityEvent_2_t6_378_gp_0_0_0_0_UnityEvent_2_t6_378_gp_1_0_0_0_Types[] = { &UnityEvent_2_t6_378_gp_0_0_0_0, &UnityEvent_2_t6_378_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_2_t6_378_gp_0_0_0_0_UnityEvent_2_t6_378_gp_1_0_0_0 = { 2, GenInst_UnityEvent_2_t6_378_gp_0_0_0_0_UnityEvent_2_t6_378_gp_1_0_0_0_Types };
extern const Il2CppType UnityEvent_3_t6_379_gp_0_0_0_0;
extern const Il2CppType UnityEvent_3_t6_379_gp_1_0_0_0;
extern const Il2CppType UnityEvent_3_t6_379_gp_2_0_0_0;
static const Il2CppType* GenInst_UnityEvent_3_t6_379_gp_0_0_0_0_UnityEvent_3_t6_379_gp_1_0_0_0_UnityEvent_3_t6_379_gp_2_0_0_0_Types[] = { &UnityEvent_3_t6_379_gp_0_0_0_0, &UnityEvent_3_t6_379_gp_1_0_0_0, &UnityEvent_3_t6_379_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_3_t6_379_gp_0_0_0_0_UnityEvent_3_t6_379_gp_1_0_0_0_UnityEvent_3_t6_379_gp_2_0_0_0 = { 3, GenInst_UnityEvent_3_t6_379_gp_0_0_0_0_UnityEvent_3_t6_379_gp_1_0_0_0_UnityEvent_3_t6_379_gp_2_0_0_0_Types };
extern const Il2CppType UnityEvent_4_t6_380_gp_0_0_0_0;
extern const Il2CppType UnityEvent_4_t6_380_gp_1_0_0_0;
extern const Il2CppType UnityEvent_4_t6_380_gp_2_0_0_0;
extern const Il2CppType UnityEvent_4_t6_380_gp_3_0_0_0;
static const Il2CppType* GenInst_UnityEvent_4_t6_380_gp_0_0_0_0_UnityEvent_4_t6_380_gp_1_0_0_0_UnityEvent_4_t6_380_gp_2_0_0_0_UnityEvent_4_t6_380_gp_3_0_0_0_Types[] = { &UnityEvent_4_t6_380_gp_0_0_0_0, &UnityEvent_4_t6_380_gp_1_0_0_0, &UnityEvent_4_t6_380_gp_2_0_0_0, &UnityEvent_4_t6_380_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_4_t6_380_gp_0_0_0_0_UnityEvent_4_t6_380_gp_1_0_0_0_UnityEvent_4_t6_380_gp_2_0_0_0_UnityEvent_4_t6_380_gp_3_0_0_0 = { 4, GenInst_UnityEvent_4_t6_380_gp_0_0_0_0_UnityEvent_4_t6_380_gp_1_0_0_0_UnityEvent_4_t6_380_gp_2_0_0_0_UnityEvent_4_t6_380_gp_3_0_0_0_Types };
extern const Il2CppType ExecuteEvents_Execute_m7_1774_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_Execute_m7_1774_gp_0_0_0_0_Types[] = { &ExecuteEvents_Execute_m7_1774_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_Execute_m7_1774_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_Execute_m7_1774_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_ExecuteHierarchy_m7_1775_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_ExecuteHierarchy_m7_1775_gp_0_0_0_0_Types[] = { &ExecuteEvents_ExecuteHierarchy_m7_1775_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_ExecuteHierarchy_m7_1775_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_ExecuteHierarchy_m7_1775_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_GetEventList_m7_1777_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_GetEventList_m7_1777_gp_0_0_0_0_Types[] = { &ExecuteEvents_GetEventList_m7_1777_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventList_m7_1777_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_GetEventList_m7_1777_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_CanHandleEvent_m7_1778_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_CanHandleEvent_m7_1778_gp_0_0_0_0_Types[] = { &ExecuteEvents_CanHandleEvent_m7_1778_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_CanHandleEvent_m7_1778_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_CanHandleEvent_m7_1778_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_GetEventHandler_m7_1779_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_GetEventHandler_m7_1779_gp_0_0_0_0_Types[] = { &ExecuteEvents_GetEventHandler_m7_1779_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventHandler_m7_1779_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_GetEventHandler_m7_1779_gp_0_0_0_0_Types };
extern const Il2CppType TweenRunner_1_t7_235_gp_0_0_0_0;
static const Il2CppType* GenInst_TweenRunner_1_t7_235_gp_0_0_0_0_Types[] = { &TweenRunner_1_t7_235_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenRunner_1_t7_235_gp_0_0_0_0 = { 1, GenInst_TweenRunner_1_t7_235_gp_0_0_0_0_Types };
extern const Il2CppType Dropdown_GetOrAddComponent_m7_1806_gp_0_0_0_0;
static const Il2CppType* GenInst_Dropdown_GetOrAddComponent_m7_1806_gp_0_0_0_0_Types[] = { &Dropdown_GetOrAddComponent_m7_1806_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dropdown_GetOrAddComponent_m7_1806_gp_0_0_0_0 = { 1, GenInst_Dropdown_GetOrAddComponent_m7_1806_gp_0_0_0_0_Types };
extern const Il2CppType IndexedSet_1_t7_239_gp_0_0_0_0;
static const Il2CppType* GenInst_IndexedSet_1_t7_239_gp_0_0_0_0_Types[] = { &IndexedSet_1_t7_239_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t7_239_gp_0_0_0_0 = { 1, GenInst_IndexedSet_1_t7_239_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_IndexedSet_1_t7_239_gp_0_0_0_0_Int32_t1_3_0_0_0_Types[] = { &IndexedSet_1_t7_239_gp_0_0_0_0, &Int32_t1_3_0_0_0 };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t7_239_gp_0_0_0_0_Int32_t1_3_0_0_0 = { 2, GenInst_IndexedSet_1_t7_239_gp_0_0_0_0_Int32_t1_3_0_0_0_Types };
extern const Il2CppType ListPool_1_t7_240_gp_0_0_0_0;
static const Il2CppType* GenInst_ListPool_1_t7_240_gp_0_0_0_0_Types[] = { &ListPool_1_t7_240_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ListPool_1_t7_240_gp_0_0_0_0 = { 1, GenInst_ListPool_1_t7_240_gp_0_0_0_0_Types };
extern const Il2CppType List_1_t1_4856_0_0_0;
static const Il2CppType* GenInst_List_1_t1_4856_0_0_0_Types[] = { &List_1_t1_4856_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1_4856_0_0_0 = { 1, GenInst_List_1_t1_4856_0_0_0_Types };
extern const Il2CppType ObjectPool_1_t7_241_gp_0_0_0_0;
static const Il2CppType* GenInst_ObjectPool_1_t7_241_gp_0_0_0_0_Types[] = { &ObjectPool_1_t7_241_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectPool_1_t7_241_gp_0_0_0_0 = { 1, GenInst_ObjectPool_1_t7_241_gp_0_0_0_0_Types };
extern const Il2CppType Extensions_Pop_m8_2384_gp_0_0_0_0;
static const Il2CppType* GenInst_Extensions_Pop_m8_2384_gp_0_0_0_0_Types[] = { &Extensions_Pop_m8_2384_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Extensions_Pop_m8_2384_gp_0_0_0_0 = { 1, GenInst_Extensions_Pop_m8_2384_gp_0_0_0_0_Types };
extern const Il2CppType ObserverPattern_1_t8_392_gp_0_0_0_0;
static const Il2CppType* GenInst_ObserverPattern_1_t8_392_gp_0_0_0_0_Types[] = { &ObserverPattern_1_t8_392_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ObserverPattern_1_t8_392_gp_0_0_0_0 = { 1, GenInst_ObserverPattern_1_t8_392_gp_0_0_0_0_Types };
extern const Il2CppType SingletonPattern_1_t8_394_gp_0_0_0_0;
static const Il2CppType* GenInst_SingletonPattern_1_t8_394_gp_0_0_0_0_Types[] = { &SingletonPattern_1_t8_394_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SingletonPattern_1_t8_394_gp_0_0_0_0 = { 1, GenInst_SingletonPattern_1_t8_394_gp_0_0_0_0_Types };
extern const Il2CppType ComponentExtensions_AddComponentIfDoesntExist_m8_2407_gp_0_0_0_0;
static const Il2CppType* GenInst_ComponentExtensions_AddComponentIfDoesntExist_m8_2407_gp_0_0_0_0_Types[] = { &ComponentExtensions_AddComponentIfDoesntExist_m8_2407_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ComponentExtensions_AddComponentIfDoesntExist_m8_2407_gp_0_0_0_0 = { 1, GenInst_ComponentExtensions_AddComponentIfDoesntExist_m8_2407_gp_0_0_0_0_Types };
extern const Il2CppType ComponentExtensions_GetComponentsInChildren_m8_2408_gp_0_0_0_0;
static const Il2CppType* GenInst_ComponentExtensions_GetComponentsInChildren_m8_2408_gp_0_0_0_0_Types[] = { &ComponentExtensions_GetComponentsInChildren_m8_2408_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ComponentExtensions_GetComponentsInChildren_m8_2408_gp_0_0_0_0 = { 1, GenInst_ComponentExtensions_GetComponentsInChildren_m8_2408_gp_0_0_0_0_Types };
extern const Il2CppType GameObjectExtensions_AddInvisibleComponent_m8_2409_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObjectExtensions_AddInvisibleComponent_m8_2409_gp_0_0_0_0_Types[] = { &GameObjectExtensions_AddInvisibleComponent_m8_2409_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObjectExtensions_AddInvisibleComponent_m8_2409_gp_0_0_0_0 = { 1, GenInst_GameObjectExtensions_AddInvisibleComponent_m8_2409_gp_0_0_0_0_Types };
extern const Il2CppType IDictionaryExtensions_GetIfAvailable_m8_2411_gp_0_0_0_0;
static const Il2CppType* GenInst_IDictionaryExtensions_GetIfAvailable_m8_2411_gp_0_0_0_0_Types[] = { &IDictionaryExtensions_GetIfAvailable_m8_2411_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionaryExtensions_GetIfAvailable_m8_2411_gp_0_0_0_0 = { 1, GenInst_IDictionaryExtensions_GetIfAvailable_m8_2411_gp_0_0_0_0_Types };
extern const Il2CppType AdvancedScriptableObject_1_t8_395_gp_0_0_0_0;
static const Il2CppType* GenInst_AdvancedScriptableObject_1_t8_395_gp_0_0_0_0_Types[] = { &AdvancedScriptableObject_1_t8_395_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AdvancedScriptableObject_1_t8_395_gp_0_0_0_0 = { 1, GenInst_AdvancedScriptableObject_1_t8_395_gp_0_0_0_0_Types };
extern const Il2CppType ExifEnumProperty_1_t8_396_gp_0_0_0_0;
static const Il2CppType* GenInst_ExifEnumProperty_1_t8_396_gp_0_0_0_0_Types[] = { &ExifEnumProperty_1_t8_396_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExifEnumProperty_1_t8_396_gp_0_0_0_0 = { 1, GenInst_ExifEnumProperty_1_t8_396_gp_0_0_0_0_Types };
extern const Il2CppType NPBinding_AddComponentBasedOnPlatform_m8_2489_gp_0_0_0_0;
static const Il2CppType* GenInst_NPBinding_AddComponentBasedOnPlatform_m8_2489_gp_0_0_0_0_Types[] = { &NPBinding_AddComponentBasedOnPlatform_m8_2489_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_NPBinding_AddComponentBasedOnPlatform_m8_2489_gp_0_0_0_0 = { 1, GenInst_NPBinding_AddComponentBasedOnPlatform_m8_2489_gp_0_0_0_0_Types };
extern const Il2CppGenericInst* const g_Il2CppGenericInstTable[793] = 
{
	&GenInst_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0,
	&GenInst_String_t_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t1_3_0_0_0,
	&GenInst_DateTime_t1_150_0_0_0,
	&GenInst_Type_t_0_0_0,
	&GenInst_CustomAttributeTypedArgument_t1_594_0_0_0,
	&GenInst_CustomAttributeNamedArgument_t1_593_0_0_0,
	&GenInst_ExceptionHandlingClause_t1_597_0_0_0,
	&GenInst_LocalVariableInfo_t1_528_0_0_0,
	&GenInst_Int32_t1_3_0_0_0_Int32_t1_3_0_0_0,
	&GenInst_GenericAce_t1_1145_0_0_0,
	&GenInst_AccessRule_t1_1111_0_0_0,
	&GenInst_AuditRule_t1_1119_0_0_0,
	&GenInst_StrongName_t1_1363_0_0_0,
	&GenInst_CustomAttributeBuilder_t1_487_0_0_0,
	&GenInst_DateTimeOffset_t1_1527_0_0_0,
	&GenInst_TimeSpan_t1_368_0_0_0,
	&GenInst_Guid_t1_319_0_0_0,
	&GenInst_CustomAttributeData_t1_588_0_0_0,
	&GenInst_KeyValuePair_2_t1_1805_0_0_0,
	&GenInst_String_t_0_0_0_DTDNode_t4_89_0_0_0,
	&GenInst_DTDNode_t4_89_0_0_0,
	&GenInst_Char_t1_15_0_0_0,
	&GenInst_Cookie_t3_81_0_0_0,
	&GenInst_Int32_t1_3_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t1_20_0_0_0,
	&GenInst_GcLeaderboard_t6_21_0_0_0,
	&GenInst_IAchievementDescriptionU5BU5D_t6_291_0_0_0,
	&GenInst_Boolean_t1_20_0_0_0,
	&GenInst_IAchievementU5BU5D_t6_293_0_0_0,
	&GenInst_IScoreU5BU5D_t6_226_0_0_0,
	&GenInst_IUserProfileU5BU5D_t6_222_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0,
	&GenInst_ByteU5BU5D_t1_109_0_0_0,
	&GenInst_Rigidbody2D_t6_120_0_0_0,
	&GenInst_Font_t6_149_0_0_0,
	&GenInst_UIVertex_t6_158_0_0_0,
	&GenInst_UICharInfo_t6_150_0_0_0,
	&GenInst_UILineInfo_t6_151_0_0_0,
	&GenInst_Int32_t1_3_0_0_0_LayoutCache_t6_173_0_0_0,
	&GenInst_GUILayoutEntry_t6_177_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t6_176_0_0_0,
	&GenInst_Int32_t1_3_0_0_0_Object_t_0_0_0,
	&GenInst_GUILayer_t6_31_0_0_0,
	&GenInst_Event_t6_162_0_0_0_TextEditOp_t6_253_0_0_0,
	&GenInst_Single_t1_17_0_0_0,
	&GenInst_PersistentCall_t6_261_0_0_0,
	&GenInst_BaseInvokableCall_t6_257_0_0_0,
	&GenInst_BaseInputModule_t7_4_0_0_0,
	&GenInst_RaycastResult_t7_31_0_0_0,
	&GenInst_IDeselectHandler_t7_172_0_0_0,
	&GenInst_ISelectHandler_t7_171_0_0_0,
	&GenInst_BaseEventData_t7_5_0_0_0,
	&GenInst_Entry_t7_7_0_0_0,
	&GenInst_IPointerEnterHandler_t7_159_0_0_0,
	&GenInst_IPointerExitHandler_t7_160_0_0_0,
	&GenInst_IPointerDownHandler_t7_161_0_0_0,
	&GenInst_IPointerUpHandler_t7_162_0_0_0,
	&GenInst_IPointerClickHandler_t7_163_0_0_0,
	&GenInst_IInitializePotentialDragHandler_t7_164_0_0_0,
	&GenInst_IBeginDragHandler_t7_165_0_0_0,
	&GenInst_IDragHandler_t7_166_0_0_0,
	&GenInst_IEndDragHandler_t7_167_0_0_0,
	&GenInst_IDropHandler_t7_168_0_0_0,
	&GenInst_IScrollHandler_t7_169_0_0_0,
	&GenInst_IUpdateSelectedHandler_t7_170_0_0_0,
	&GenInst_IMoveHandler_t7_173_0_0_0,
	&GenInst_ISubmitHandler_t7_174_0_0_0,
	&GenInst_ICancelHandler_t7_175_0_0_0,
	&GenInst_List_1_t1_1879_0_0_0,
	&GenInst_IEventSystemHandler_t7_200_0_0_0,
	&GenInst_Transform_t6_65_0_0_0,
	&GenInst_PointerEventData_t7_36_0_0_0,
	&GenInst_AxisEventData_t7_33_0_0_0,
	&GenInst_BaseRaycaster_t7_32_0_0_0,
	&GenInst_Camera_t6_86_0_0_0,
	&GenInst_GameObject_t6_97_0_0_0,
	&GenInst_EventSystem_t7_2_0_0_0,
	&GenInst_ButtonState_t7_37_0_0_0,
	&GenInst_Int32_t1_3_0_0_0_PointerEventData_t7_36_0_0_0,
	&GenInst_SpriteRenderer_t6_76_0_0_0,
	&GenInst_RaycastHit_t6_116_0_0_0,
	&GenInst_Color_t6_40_0_0_0,
	&GenInst_ICanvasElement_t7_176_0_0_0,
	&GenInst_RectTransform_t6_64_0_0_0,
	&GenInst_Image_t7_64_0_0_0,
	&GenInst_Button_t7_54_0_0_0,
	&GenInst_Text_t7_63_0_0_0,
	&GenInst_RawImage_t7_107_0_0_0,
	&GenInst_Slider_t7_125_0_0_0,
	&GenInst_Scrollbar_t7_113_0_0_0,
	&GenInst_Toggle_t7_65_0_0_0,
	&GenInst_InputField_t7_98_0_0_0,
	&GenInst_ScrollRect_t7_117_0_0_0,
	&GenInst_Mask_t7_100_0_0_0,
	&GenInst_Dropdown_t7_70_0_0_0,
	&GenInst_OptionData_t7_66_0_0_0,
	&GenInst_DropdownItem_t7_62_0_0_0,
	&GenInst_FloatTween_t7_50_0_0_0,
	&GenInst_Canvas_t6_156_0_0_0,
	&GenInst_GraphicRaycaster_t7_79_0_0_0,
	&GenInst_CanvasGroup_t6_157_0_0_0,
	&GenInst_Font_t6_149_0_0_0_List_1_t1_1889_0_0_0,
	&GenInst_ColorTween_t7_48_0_0_0,
	&GenInst_CanvasRenderer_t6_159_0_0_0,
	&GenInst_Component_t6_26_0_0_0,
	&GenInst_Graphic_t7_75_0_0_0,
	&GenInst_Canvas_t6_156_0_0_0_IndexedSet_1_t7_185_0_0_0,
	&GenInst_Sprite_t6_75_0_0_0,
	&GenInst_Type_t7_81_0_0_0,
	&GenInst_FillMethod_t7_82_0_0_0,
	&GenInst_SubmitEvent_t7_93_0_0_0,
	&GenInst_OnChangeEvent_t7_94_0_0_0,
	&GenInst_OnValidateInput_t7_96_0_0_0,
	&GenInst_ContentType_t7_89_0_0_0,
	&GenInst_LineType_t7_92_0_0_0,
	&GenInst_InputType_t7_90_0_0_0,
	&GenInst_TouchScreenKeyboardType_t6_44_0_0_0,
	&GenInst_CharacterValidation_t7_91_0_0_0,
	&GenInst_LayoutElement_t7_149_0_0_0,
	&GenInst_IClippable_t7_178_0_0_0,
	&GenInst_RectMask2D_t7_102_0_0_0,
	&GenInst_Direction_t7_109_0_0_0,
	&GenInst_Vector2_t6_47_0_0_0,
	&GenInst_Selectable_t7_55_0_0_0,
	&GenInst_Navigation_t7_106_0_0_0,
	&GenInst_Transition_t7_118_0_0_0,
	&GenInst_ColorBlock_t7_59_0_0_0,
	&GenInst_SpriteState_t7_120_0_0_0,
	&GenInst_AnimationTriggers_t7_51_0_0_0,
	&GenInst_Animator_t6_139_0_0_0,
	&GenInst_Direction_t7_122_0_0_0,
	&GenInst_MatEntry_t7_126_0_0_0,
	&GenInst_Toggle_t7_65_0_0_0_Boolean_t1_20_0_0_0,
	&GenInst_IClipper_t7_179_0_0_0,
	&GenInst_AspectMode_t7_134_0_0_0,
	&GenInst_FitMode_t7_140_0_0_0,
	&GenInst_Corner_t7_142_0_0_0,
	&GenInst_Axis_t7_143_0_0_0,
	&GenInst_Constraint_t7_144_0_0_0,
	&GenInst_RectOffset_t6_178_0_0_0,
	&GenInst_TextAnchor_t6_144_0_0_0,
	&GenInst_LayoutRebuilder_t7_150_0_0_0,
	&GenInst_ILayoutElement_t7_180_0_0_0_Single_t1_17_0_0_0,
	&GenInst_Vector3_t6_48_0_0_0,
	&GenInst_Color32_t6_49_0_0_0,
	&GenInst_Vector4_t6_54_0_0_0,
	&GenInst_JSONValue_t8_4_0_0_0,
	&GenInst_String_t_0_0_0_JSONValue_t8_4_0_0_0,
	&GenInst_KeyValuePair_2_t1_1915_0_0_0,
	&GenInst_DemoSubMenu_t8_18_0_0_0,
	&GenInst_GUIScrollView_t8_19_0_0_0,
	&GenInst_ShaderInfo_t8_26_0_0_0,
	&GenInst_ShaderUtility_t8_29_0_0_0,
	&GenInst_Texture2D_t6_33_0_0_0,
	&GenInst_String_t_0_0_0_Object_t_0_0_0,
	&GenInst_Byte_t1_11_0_0_0,
	&GenInst_JPEGSection_t8_112_0_0_0,
	&GenInst_Int32_t1_3_0_0_0_IFD_t8_131_0_0_0,
	&GenInst_IFD_t8_131_0_0_0,
	&GenInst_ExifTag_t8_132_0_0_0_ExifProperty_t8_99_0_0_0,
	&GenInst_ExifProperty_t8_99_0_0_0,
	&GenInst_Compression_t8_63_0_0_0,
	&GenInst_PhotometricInterpretation_t8_64_0_0_0,
	&GenInst_Orientation_t8_65_0_0_0,
	&GenInst_PlanarConfiguration_t8_66_0_0_0,
	&GenInst_YCbCrPositioning_t8_67_0_0_0,
	&GenInst_ResolutionUnit_t8_68_0_0_0,
	&GenInst_ColorSpace_t8_69_0_0_0,
	&GenInst_ExposureProgram_t8_70_0_0_0,
	&GenInst_MeteringMode_t8_71_0_0_0,
	&GenInst_LightSource_t8_72_0_0_0,
	&GenInst_Flash_t8_73_0_0_0,
	&GenInst_SensingMethod_t8_74_0_0_0,
	&GenInst_FileSource_t8_75_0_0_0,
	&GenInst_SceneType_t8_76_0_0_0,
	&GenInst_CustomRendered_t8_77_0_0_0,
	&GenInst_ExposureMode_t8_78_0_0_0,
	&GenInst_WhiteBalance_t8_79_0_0_0,
	&GenInst_SceneCaptureType_t8_80_0_0_0,
	&GenInst_GainControl_t8_81_0_0_0,
	&GenInst_Contrast_t8_82_0_0_0,
	&GenInst_Saturation_t8_83_0_0_0,
	&GenInst_Sharpness_t8_84_0_0_0,
	&GenInst_SubjectDistanceRange_t8_85_0_0_0,
	&GenInst_GPSLatitudeRef_t8_86_0_0_0,
	&GenInst_GPSLongitudeRef_t8_87_0_0_0,
	&GenInst_GPSAltitudeRef_t8_88_0_0_0,
	&GenInst_GPSStatus_t8_89_0_0_0,
	&GenInst_GPSMeasureMode_t8_90_0_0_0,
	&GenInst_GPSSpeedRef_t8_91_0_0_0,
	&GenInst_GPSDirectionRef_t8_92_0_0_0,
	&GenInst_GPSDistanceRef_t8_93_0_0_0,
	&GenInst_GPSDifferential_t8_94_0_0_0,
	&GenInst_GUISkin_t6_169_0_0_0,
	&GenInst_GUISubMenu_t8_143_0_0_0,
	&GenInst_Action_t5_11_0_0_0_Dictionary_2_t1_1929_0_0_0,
	&GenInst_String_t_0_0_0_Single_t1_17_0_0_0,
	&GenInst_MonoBehaviour_t6_91_0_0_0,
	&GenInst_List_1_t1_1742_0_0_0,
	&GenInst_ConsoleLog_t8_170_0_0_0,
	&GenInst_ConsoleTag_t8_171_0_0_0,
	&GenInst_Console_t8_169_0_0_0,
	&GenInst_ConsoleTag_t8_171_0_0_0_Boolean_t1_20_0_0_0,
	&GenInst_UnityDebugUtility_t8_173_0_0_0,
	&GenInst_IList_t1_262_0_0_0,
	&GenInst_Int64_t1_7_0_0_0,
	&GenInst_BillingProduct_t8_207_0_0_0,
	&GenInst_eNotificationRepeatInterval_t8_264_0_0_0,
	&GenInst_IDictionary_t1_35_0_0_0,
	&GenInst_CalendarUnit_t6_103_0_0_0,
	&GenInst_NPBinding_t8_333_0_0_0,
	&GenInst_String_t_0_0_0_AlertDialogCompletion_t8_300_0_0_0,
	&GenInst_RateMyApp_t8_307_0_0_0,
	&GenInst_String_t_0_0_0_WebView_t8_191_0_0_0,
	&GenInst_String_t_0_0_0_NPObject_t8_222_0_0_0,
	&GenInst_NotificationService_t8_261_0_0_0,
	&GenInst_UI_t8_303_0_0_0,
	&GenInst_Utility_t8_306_0_0_0,
	&GenInst_NPSettings_t8_335_0_0_0,
	&GenInst_Attribute_t1_2_0_0_0,
	&GenInst__Attribute_t1_3065_0_0_0,
	&GenInst_IConvertible_t1_1746_0_0_0,
	&GenInst_IComparable_t1_1745_0_0_0,
	&GenInst_IComparable_1_t1_3343_0_0_0,
	&GenInst_IEquatable_1_t1_3348_0_0_0,
	&GenInst_ValueType_t1_1_0_0_0,
	&GenInst_UInt32_t1_8_0_0_0,
	&GenInst_UInt64_t1_10_0_0_0,
	&GenInst_SByte_t1_12_0_0_0,
	&GenInst_Int16_t1_13_0_0_0,
	&GenInst_UInt16_t1_14_0_0_0,
	&GenInst_IEnumerable_t1_1677_0_0_0,
	&GenInst_ICloneable_t1_1762_0_0_0,
	&GenInst_IComparable_1_t1_3372_0_0_0,
	&GenInst_IEquatable_1_t1_3373_0_0_0,
	&GenInst_IReflect_t1_3077_0_0_0,
	&GenInst__Type_t1_3075_0_0_0,
	&GenInst_MemberInfo_t_0_0_0,
	&GenInst_ICustomAttributeProvider_t1_1686_0_0_0,
	&GenInst__MemberInfo_t1_3076_0_0_0,
	&GenInst_IFormattable_t1_1744_0_0_0,
	&GenInst_IComparable_1_t1_3330_0_0_0,
	&GenInst_IEquatable_1_t1_3331_0_0_0,
	&GenInst_Double_t1_18_0_0_0,
	&GenInst_IComparable_1_t1_3432_0_0_0,
	&GenInst_IEquatable_1_t1_3437_0_0_0,
	&GenInst_IComparable_1_t1_3364_0_0_0,
	&GenInst_IEquatable_1_t1_3365_0_0_0,
	&GenInst_IComparable_1_t1_3456_0_0_0,
	&GenInst_IEquatable_1_t1_3461_0_0_0,
	&GenInst_Decimal_t1_19_0_0_0,
	&GenInst_Delegate_t1_22_0_0_0,
	&GenInst_ISerializable_t1_1778_0_0_0,
	&GenInst_ParameterInfo_t1_627_0_0_0,
	&GenInst__ParameterInfo_t1_3135_0_0_0,
	&GenInst_ParameterModifier_t1_628_0_0_0,
	&GenInst_IComparable_1_t1_3370_0_0_0,
	&GenInst_IEquatable_1_t1_3371_0_0_0,
	&GenInst_IComparable_1_t1_3360_0_0_0,
	&GenInst_IEquatable_1_t1_3361_0_0_0,
	&GenInst_IComparable_1_t1_3362_0_0_0,
	&GenInst_IEquatable_1_t1_3363_0_0_0,
	&GenInst_IComparable_1_t1_3368_0_0_0,
	&GenInst_IEquatable_1_t1_3369_0_0_0,
	&GenInst_IComparable_1_t1_3366_0_0_0,
	&GenInst_IEquatable_1_t1_3367_0_0_0,
	&GenInst_IComparable_1_t1_3358_0_0_0,
	&GenInst_IEquatable_1_t1_3359_0_0_0,
	&GenInst_MethodInfo_t_0_0_0,
	&GenInst__MethodInfo_t1_3133_0_0_0,
	&GenInst_MethodBase_t1_335_0_0_0,
	&GenInst__MethodBase_t1_3132_0_0_0,
	&GenInst_EventInfo_t_0_0_0,
	&GenInst__EventInfo_t1_3130_0_0_0,
	&GenInst_FieldInfo_t_0_0_0,
	&GenInst__FieldInfo_t1_3131_0_0_0,
	&GenInst_PropertyInfo_t_0_0_0,
	&GenInst__PropertyInfo_t1_3136_0_0_0,
	&GenInst_ConstructorInfo_t1_478_0_0_0,
	&GenInst__ConstructorInfo_t1_3129_0_0_0,
	&GenInst_IntPtr_t_0_0_0,
	&GenInst_RuntimeTypeHandle_t1_30_0_0_0,
	&GenInst_Object_t_0_0_0_Int32_t1_3_0_0_0,
	&GenInst_KeyValuePair_2_t1_1981_0_0_0,
	&GenInst_Link_t1_256_0_0_0,
	&GenInst_Object_t_0_0_0_Int32_t1_3_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_Int32_t1_3_0_0_0_Int32_t1_3_0_0_0,
	&GenInst_Object_t_0_0_0_Int32_t1_3_0_0_0_DictionaryEntry_t1_284_0_0_0,
	&GenInst_DictionaryEntry_t1_284_0_0_0,
	&GenInst_Object_t_0_0_0_Int32_t1_3_0_0_0_KeyValuePair_2_t1_1981_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t1_3_0_0_0_DictionaryEntry_t1_284_0_0_0,
	&GenInst_KeyValuePair_2_t1_1997_0_0_0,
	&GenInst_DirectoryInfo_t1_399_0_0_0,
	&GenInst_FileSystemInfo_t1_411_0_0_0,
	&GenInst_MarshalByRefObject_t1_69_0_0_0,
	&GenInst_TableRange_t1_104_0_0_0,
	&GenInst_TailoringInfo_t1_107_0_0_0,
	&GenInst_Contraction_t1_108_0_0_0,
	&GenInst_Level2Map_t1_111_0_0_0,
	&GenInst_BigInteger_t1_139_0_0_0,
	&GenInst_KeySizes_t1_1219_0_0_0,
	&GenInst_X509Certificate_t1_151_0_0_0,
	&GenInst_X509Extension_t1_178_0_0_0,
	&GenInst_UriScheme_t1_236_0_0_0,
	&GenInst_KeyValuePair_2_t1_2015_0_0_0,
	&GenInst_Object_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t1_284_0_0_0,
	&GenInst_Object_t_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1_2015_0_0_0,
	&GenInst_IComparable_1_t1_3467_0_0_0,
	&GenInst_IEquatable_1_t1_3468_0_0_0,
	&GenInst_Slot_t1_285_0_0_0,
	&GenInst_Slot_t1_301_0_0_0,
	&GenInst_ISymbolVariable_t1_3110_0_0_0,
	&GenInst_ISymbolDocument_t1_3105_0_0_0,
	&GenInst_ISymbolNamespace_t1_3107_0_0_0,
	&GenInst_ISymbolScope_t1_3109_0_0_0,
	&GenInst_StackFrame_t1_334_0_0_0,
	&GenInst_Calendar_t1_338_0_0_0,
	&GenInst_CultureInfo_t1_277_0_0_0,
	&GenInst_IFormatProvider_t1_455_0_0_0,
	&GenInst_FileInfo_t1_422_0_0_0,
	&GenInst_DriveInfo_t1_415_0_0_0,
	&GenInst_ModuleBuilder_t1_475_0_0_0,
	&GenInst__ModuleBuilder_t1_3122_0_0_0,
	&GenInst_Module_t1_495_0_0_0,
	&GenInst__Module_t1_3134_0_0_0,
	&GenInst__CustomAttributeBuilder_t1_3114_0_0_0,
	&GenInst_MonoResource_t1_464_0_0_0,
	&GenInst_MonoWin32Resource_t1_465_0_0_0,
	&GenInst_RefEmitPermissionSet_t1_463_0_0_0,
	&GenInst_ICONDIRENTRY_t1_667_0_0_0,
	&GenInst_Win32IconResource_t1_666_0_0_0,
	&GenInst_Win32Resource_t1_664_0_0_0,
	&GenInst_FileStream_t1_146_0_0_0,
	&GenInst_Stream_t1_405_0_0_0,
	&GenInst_IDisposable_t1_1035_0_0_0,
	&GenInst_ParameterBuilder_t1_545_0_0_0,
	&GenInst__ParameterBuilder_t1_3123_0_0_0,
	&GenInst_TypeU5BU5D_t1_31_0_0_0,
	&GenInst_Array_t_0_0_0,
	&GenInst_ICollection_t1_280_0_0_0,
	&GenInst_MethodBuilder_t1_501_0_0_0,
	&GenInst__MethodBuilder_t1_3120_0_0_0,
	&GenInst_ILExceptionBlock_t1_510_0_0_0,
	&GenInst_LocalBuilder_t1_527_0_0_0,
	&GenInst__LocalBuilder_t1_3119_0_0_0,
	&GenInst_ILExceptionInfo_t1_511_0_0_0,
	&GenInst_ILTokenInfo_t1_514_0_0_0,
	&GenInst_LabelData_t1_516_0_0_0,
	&GenInst_LabelFixup_t1_515_0_0_0,
	&GenInst_Label_t1_513_0_0_0,
	&GenInst_SequencePoint_t1_526_0_0_0,
	&GenInst_GenericTypeParameterBuilder_t1_509_0_0_0,
	&GenInst_TypeBuilder_t1_481_0_0_0,
	&GenInst__TypeBuilder_t1_3126_0_0_0,
	&GenInst_FieldBuilder_t1_499_0_0_0,
	&GenInst__FieldBuilder_t1_3117_0_0_0,
	&GenInst_ConstructorBuilder_t1_477_0_0_0,
	&GenInst__ConstructorBuilder_t1_3113_0_0_0,
	&GenInst_PropertyBuilder_t1_547_0_0_0,
	&GenInst__PropertyBuilder_t1_3124_0_0_0,
	&GenInst_EventBuilder_t1_500_0_0_0,
	&GenInst__EventBuilder_t1_3116_0_0_0,
	&GenInst_AssemblyName_t1_576_0_0_0,
	&GenInst__AssemblyName_t1_3128_0_0_0,
	&GenInst_IDeserializationCallback_t1_1781_0_0_0,
	&GenInst_DecimalConstantAttribute_t1_63_0_0_0,
	&GenInst_DateTimeConstantAttribute_t1_682_0_0_0,
	&GenInst_CustomConstantAttribute_t1_681_0_0_0,
	&GenInst_Exception_t1_33_0_0_0,
	&GenInst__Exception_t1_3078_0_0_0,
	&GenInst_ResourceInfo_t1_647_0_0_0,
	&GenInst_ResourceCacheItem_t1_648_0_0_0,
	&GenInst_IConnectionPoint_t1_3138_0_0_0,
	&GenInst_CONNECTDATA_t1_723_0_0_0,
	&GenInst_IMoniker_t1_3145_0_0_0,
	&GenInst_ITypeInfo_t1_1697_0_0_0,
	&GenInst_UCOMIConnectionPoint_t1_3160_0_0_0,
	&GenInst_CONNECTDATA_t1_762_0_0_0,
	&GenInst_UCOMIMoniker_t1_3167_0_0_0,
	&GenInst_UCOMITypeInfo_t1_1696_0_0_0,
	&GenInst_IChannel_t1_1710_0_0_0,
	&GenInst_IContextProperty_t1_1716_0_0_0,
	&GenInst_IComparable_1_t1_3465_0_0_0,
	&GenInst_IEquatable_1_t1_3466_0_0_0,
	&GenInst_Header_t1_939_0_0_0,
	&GenInst_ITrackingHandler_t1_1721_0_0_0,
	&GenInst_IContextAttribute_t1_1764_0_0_0,
	&GenInst_ActivatedClientTypeEntry_t1_1008_0_0_0,
	&GenInst_TypeEntry_t1_1009_0_0_0,
	&GenInst_ActivatedServiceTypeEntry_t1_1010_0_0_0,
	&GenInst_WellKnownClientTypeEntry_t1_1039_0_0_0,
	&GenInst_WellKnownServiceTypeEntry_t1_1041_0_0_0,
	&GenInst_IComparable_1_t1_4020_0_0_0,
	&GenInst_IEquatable_1_t1_4025_0_0_0,
	&GenInst_IComparable_1_t1_4033_0_0_0,
	&GenInst_IEquatable_1_t1_4038_0_0_0,
	&GenInst_TypeTag_t1_1044_0_0_0,
	&GenInst_Enum_t1_24_0_0_0,
	&GenInst_MonoType_t_0_0_0,
	&GenInst_SerializationEntry_t1_1093_0_0_0,
	&GenInst_AuthorizationRule_t1_1112_0_0_0,
	&GenInst_KeyContainerPermissionAccessEntry_t1_1290_0_0_0,
	&GenInst_ApplicationTrust_t1_1333_0_0_0,
	&GenInst_ISecurityEncodable_t1_1788_0_0_0,
	&GenInst_CodeConnectAccess_t1_1338_0_0_0,
	&GenInst_IdentityReference_t1_1120_0_0_0,
	&GenInst_SecurityIdentifier_t1_1132_0_0_0,
	&GenInst_Evidence_t1_398_0_0_0,
	&GenInst_EncodingInfo_t1_1438_0_0_0,
	&GenInst_WaitHandle_t1_917_0_0_0,
	&GenInst_Assembly_t1_467_0_0_0,
	&GenInst__Assembly_t1_3127_0_0_0,
	&GenInst_Version_t1_578_0_0_0,
	&GenInst_BigInteger_t2_4_0_0_0,
	&GenInst_X509Certificate_t1_1179_0_0_0,
	&GenInst_ClientCertificateType_t2_98_0_0_0,
	&GenInst_Entry_t4_107_0_0_0,
	&GenInst_XmlNode_t4_116_0_0_0,
	&GenInst_IXPathNavigable_t4_197_0_0_0,
	&GenInst_NsDecl_t4_141_0_0_0,
	&GenInst_NsScope_t4_142_0_0_0,
	&GenInst_XmlAttributeTokenInfo_t4_170_0_0_0,
	&GenInst_XmlTokenInfo_t4_168_0_0_0,
	&GenInst_TagName_t4_171_0_0_0,
	&GenInst_XmlNodeInfo_t4_178_0_0_0,
	&GenInst_Object_t_0_0_0_Boolean_t1_20_0_0_0,
	&GenInst_X509ChainStatus_t3_157_0_0_0,
	&GenInst_IPAddress_t3_121_0_0_0,
	&GenInst_ArraySegment_1_t1_2214_0_0_0,
	&GenInst_KeyValuePair_2_t1_2223_0_0_0,
	&GenInst_Object_t_0_0_0_Boolean_t1_20_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_Boolean_t1_20_0_0_0_Boolean_t1_20_0_0_0,
	&GenInst_Object_t_0_0_0_Boolean_t1_20_0_0_0_DictionaryEntry_t1_284_0_0_0,
	&GenInst_Object_t_0_0_0_Boolean_t1_20_0_0_0_KeyValuePair_2_t1_2223_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t1_20_0_0_0_DictionaryEntry_t1_284_0_0_0,
	&GenInst_KeyValuePair_2_t1_2237_0_0_0,
	&GenInst_Capture_t3_176_0_0_0,
	&GenInst_Group_t3_15_0_0_0,
	&GenInst_Mark_t3_199_0_0_0,
	&GenInst_UriScheme_t3_234_0_0_0,
	&GenInst_Object_t6_5_0_0_0,
	&GenInst_IAchievementDescription_t6_372_0_0_0,
	&GenInst_IAchievement_t6_277_0_0_0,
	&GenInst_IScore_t6_228_0_0_0,
	&GenInst_IUserProfile_t6_371_0_0_0,
	&GenInst_AchievementDescription_t6_224_0_0_0,
	&GenInst_UserProfile_t6_221_0_0_0,
	&GenInst_GcAchievementData_t6_210_0_0_0,
	&GenInst_Achievement_t6_223_0_0_0,
	&GenInst_GcScoreData_t6_211_0_0_0,
	&GenInst_Score_t6_225_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t1_284_0_0_0,
	&GenInst_KeyValuePair_2_t1_1845_0_0_0,
	&GenInst_Behaviour_t6_30_0_0_0,
	&GenInst_Display_t6_89_0_0_0,
	&GenInst_Touch_t6_94_0_0_0,
	&GenInst_ContactPoint_t6_113_0_0_0,
	&GenInst_RaycastHit2D_t6_118_0_0_0,
	&GenInst_ContactPoint2D_t6_121_0_0_0,
	&GenInst_Keyframe_t6_135_0_0_0,
	&GenInst_CharacterInfo_t6_147_0_0_0,
	&GenInst_GUILayoutOption_t6_182_0_0_0,
	&GenInst_KeyValuePair_2_t1_2385_0_0_0,
	&GenInst_Int32_t1_3_0_0_0_Object_t_0_0_0_Int32_t1_3_0_0_0,
	&GenInst_Int32_t1_3_0_0_0_Object_t_0_0_0_Object_t_0_0_0,
	&GenInst_Int32_t1_3_0_0_0_Object_t_0_0_0_DictionaryEntry_t1_284_0_0_0,
	&GenInst_Int32_t1_3_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1_2385_0_0_0,
	&GenInst_Int32_t1_3_0_0_0_LayoutCache_t6_173_0_0_0_DictionaryEntry_t1_284_0_0_0,
	&GenInst_LayoutCache_t6_173_0_0_0,
	&GenInst_KeyValuePair_2_t1_2396_0_0_0,
	&GenInst_GUIStyle_t6_176_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t6_176_0_0_0_DictionaryEntry_t1_284_0_0_0,
	&GenInst_KeyValuePair_2_t1_2407_0_0_0,
	&GenInst_DisallowMultipleComponent_t6_201_0_0_0,
	&GenInst_ExecuteInEditMode_t6_204_0_0_0,
	&GenInst_RequireComponent_t6_202_0_0_0,
	&GenInst_HitInfo_t6_229_0_0_0,
	&GenInst_Object_t_0_0_0_TextEditOp_t6_253_0_0_0,
	&GenInst_KeyValuePair_2_t1_2424_0_0_0,
	&GenInst_TextEditOp_t6_253_0_0_0,
	&GenInst_Object_t_0_0_0_TextEditOp_t6_253_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_TextEditOp_t6_253_0_0_0_TextEditOp_t6_253_0_0_0,
	&GenInst_Object_t_0_0_0_TextEditOp_t6_253_0_0_0_DictionaryEntry_t1_284_0_0_0,
	&GenInst_Object_t_0_0_0_TextEditOp_t6_253_0_0_0_KeyValuePair_2_t1_2424_0_0_0,
	&GenInst_Event_t6_162_0_0_0,
	&GenInst_Event_t6_162_0_0_0_TextEditOp_t6_253_0_0_0_DictionaryEntry_t1_284_0_0_0,
	&GenInst_KeyValuePair_2_t1_2438_0_0_0,
	&GenInst_List_1_t1_1894_0_0_0,
	&GenInst_List_1_t1_1840_0_0_0,
	&GenInst_Int32_t1_3_0_0_0_PointerEventData_t7_36_0_0_0_DictionaryEntry_t1_284_0_0_0,
	&GenInst_KeyValuePair_2_t1_1886_0_0_0,
	&GenInst_ICanvasElement_t7_176_0_0_0_Int32_t1_3_0_0_0,
	&GenInst_ICanvasElement_t7_176_0_0_0_Int32_t1_3_0_0_0_DictionaryEntry_t1_284_0_0_0,
	&GenInst_List_1_t1_1888_0_0_0,
	&GenInst_Font_t6_149_0_0_0_List_1_t1_1889_0_0_0_DictionaryEntry_t1_284_0_0_0,
	&GenInst_List_1_t1_1889_0_0_0,
	&GenInst_KeyValuePair_2_t1_2541_0_0_0,
	&GenInst_Graphic_t7_75_0_0_0_Int32_t1_3_0_0_0,
	&GenInst_Graphic_t7_75_0_0_0_Int32_t1_3_0_0_0_DictionaryEntry_t1_284_0_0_0,
	&GenInst_Canvas_t6_156_0_0_0_IndexedSet_1_t7_185_0_0_0_DictionaryEntry_t1_284_0_0_0,
	&GenInst_IndexedSet_1_t7_185_0_0_0,
	&GenInst_KeyValuePair_2_t1_2553_0_0_0,
	&GenInst_KeyValuePair_2_t1_2557_0_0_0,
	&GenInst_KeyValuePair_2_t1_2561_0_0_0,
	&GenInst_IClipper_t7_179_0_0_0_Int32_t1_3_0_0_0,
	&GenInst_IClipper_t7_179_0_0_0_Int32_t1_3_0_0_0_DictionaryEntry_t1_284_0_0_0,
	&GenInst_KeyValuePair_2_t1_2606_0_0_0,
	&GenInst_Object_t_0_0_0_Single_t1_17_0_0_0,
	&GenInst_List_1_t1_1834_0_0_0,
	&GenInst_List_1_t1_1837_0_0_0,
	&GenInst_List_1_t1_1836_0_0_0,
	&GenInst_List_1_t1_1835_0_0_0,
	&GenInst_List_1_t1_1838_0_0_0,
	&GenInst_List_1_t1_1824_0_0_0,
	&GenInst_String_t_0_0_0_JSONValue_t8_4_0_0_0_DictionaryEntry_t1_284_0_0_0,
	&GenInst_DemoGUIWindow_t8_14_0_0_0,
	&GenInst_GUIModalWindow_t8_15_0_0_0,
	&GenInst_IObserver_t8_374_0_0_0,
	&GenInst_ShaderProperty_t8_27_0_0_0,
	&GenInst_String_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t1_284_0_0_0,
	&GenInst_KeyValuePair_2_t1_2652_0_0_0,
	&GenInst_UFraction32_t8_121_0_0_0,
	&GenInst_IComparable_1_t1_4524_0_0_0,
	&GenInst_IEquatable_1_t1_4529_0_0_0,
	&GenInst_Fraction32_t8_127_0_0_0,
	&GenInst_IComparable_1_t1_4537_0_0_0,
	&GenInst_IEquatable_1_t1_4542_0_0_0,
	&GenInst_ExifTag_t8_132_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t1_2665_0_0_0,
	&GenInst_ExifTag_t8_132_0_0_0,
	&GenInst_ExifTag_t8_132_0_0_0_Object_t_0_0_0_ExifTag_t8_132_0_0_0,
	&GenInst_ExifTag_t8_132_0_0_0_Object_t_0_0_0_Object_t_0_0_0,
	&GenInst_ExifTag_t8_132_0_0_0_Object_t_0_0_0_DictionaryEntry_t1_284_0_0_0,
	&GenInst_ExifTag_t8_132_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1_2665_0_0_0,
	&GenInst_ExifTag_t8_132_0_0_0_ExifProperty_t8_99_0_0_0_DictionaryEntry_t1_284_0_0_0,
	&GenInst_KeyValuePair_2_t1_1924_0_0_0,
	&GenInst_KeyValuePair_2_t1_2681_0_0_0,
	&GenInst_GUIMenuBase_t8_141_0_0_0,
	&GenInst_String_t_0_0_0_Single_t1_17_0_0_0_DictionaryEntry_t1_284_0_0_0,
	&GenInst_KeyValuePair_2_t1_2696_0_0_0,
	&GenInst_Action_t5_11_0_0_0,
	&GenInst_Action_t5_11_0_0_0_Dictionary_2_t1_1929_0_0_0_DictionaryEntry_t1_284_0_0_0,
	&GenInst_Dictionary_2_t1_1929_0_0_0,
	&GenInst_KeyValuePair_2_t1_2700_0_0_0,
	&GenInst_KeyValuePair_2_t1_2706_0_0_0,
	&GenInst_Object_t_0_0_0_Single_t1_17_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_Single_t1_17_0_0_0_Single_t1_17_0_0_0,
	&GenInst_Object_t_0_0_0_Single_t1_17_0_0_0_DictionaryEntry_t1_284_0_0_0,
	&GenInst_Object_t_0_0_0_Single_t1_17_0_0_0_KeyValuePair_2_t1_2706_0_0_0,
	&GenInst_AddressBookContact_t8_198_0_0_0,
	&GenInst_Texture_t6_32_0_0_0,
	&GenInst_eShareOptions_t8_287_0_0_0,
	&GenInst_PlatformID_t8_331_0_0_0,
	&GenInst_Achievement_t8_221_0_0_0,
	&GenInst_NPObject_t8_222_0_0_0,
	&GenInst_AchievementDescription_t8_225_0_0_0,
	&GenInst_Score_t8_231_0_0_0,
	&GenInst_User_t8_235_0_0_0,
	&GenInst_IDContainer_t8_328_0_0_0,
	&GenInst_String_t_0_0_0_AlertDialogCompletion_t8_300_0_0_0_DictionaryEntry_t1_284_0_0_0,
	&GenInst_AlertDialogCompletion_t8_300_0_0_0,
	&GenInst_KeyValuePair_2_t1_2754_0_0_0,
	&GenInst_String_t_0_0_0_WebView_t8_191_0_0_0_DictionaryEntry_t1_284_0_0_0,
	&GenInst_WebView_t8_191_0_0_0,
	&GenInst_KeyValuePair_2_t1_2759_0_0_0,
	&GenInst_String_t_0_0_0_NPObject_t8_222_0_0_0_DictionaryEntry_t1_284_0_0_0,
	&GenInst_KeyValuePair_2_t1_2764_0_0_0,
	&GenInst_DictionaryEntry_t1_284_0_0_0_DictionaryEntry_t1_284_0_0_0,
	&GenInst_KeyValuePair_2_t1_1981_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t1_1981_0_0_0_KeyValuePair_2_t1_1981_0_0_0,
	&GenInst_KeyValuePair_2_t1_2015_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t1_2015_0_0_0_KeyValuePair_2_t1_2015_0_0_0,
	&GenInst_CustomAttributeTypedArgument_t1_594_0_0_0_CustomAttributeTypedArgument_t1_594_0_0_0,
	&GenInst_CustomAttributeNamedArgument_t1_593_0_0_0_CustomAttributeNamedArgument_t1_593_0_0_0,
	&GenInst_Boolean_t1_20_0_0_0_Object_t_0_0_0,
	&GenInst_Boolean_t1_20_0_0_0_Boolean_t1_20_0_0_0,
	&GenInst_KeyValuePair_2_t1_2223_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t1_2223_0_0_0_KeyValuePair_2_t1_2223_0_0_0,
	&GenInst_Vector3_t6_48_0_0_0_Vector3_t6_48_0_0_0,
	&GenInst_Vector4_t6_54_0_0_0_Vector4_t6_54_0_0_0,
	&GenInst_Vector2_t6_47_0_0_0_Vector2_t6_47_0_0_0,
	&GenInst_Color32_t6_49_0_0_0_Color32_t6_49_0_0_0,
	&GenInst_UIVertex_t6_158_0_0_0_UIVertex_t6_158_0_0_0,
	&GenInst_UICharInfo_t6_150_0_0_0_UICharInfo_t6_150_0_0_0,
	&GenInst_UILineInfo_t6_151_0_0_0_UILineInfo_t6_151_0_0_0,
	&GenInst_KeyValuePair_2_t1_2385_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t1_2385_0_0_0_KeyValuePair_2_t1_2385_0_0_0,
	&GenInst_TextEditOp_t6_253_0_0_0_Object_t_0_0_0,
	&GenInst_TextEditOp_t6_253_0_0_0_TextEditOp_t6_253_0_0_0,
	&GenInst_KeyValuePair_2_t1_2424_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t1_2424_0_0_0_KeyValuePair_2_t1_2424_0_0_0,
	&GenInst_RaycastResult_t7_31_0_0_0_RaycastResult_t7_31_0_0_0,
	&GenInst_ExifTag_t8_132_0_0_0_ExifTag_t8_132_0_0_0,
	&GenInst_KeyValuePair_2_t1_2665_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t1_2665_0_0_0_KeyValuePair_2_t1_2665_0_0_0,
	&GenInst_Single_t1_17_0_0_0_Object_t_0_0_0,
	&GenInst_Single_t1_17_0_0_0_Single_t1_17_0_0_0,
	&GenInst_KeyValuePair_2_t1_2706_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t1_2706_0_0_0_KeyValuePair_2_t1_2706_0_0_0,
	&GenInst_ConsoleLog_t8_170_0_0_0_ConsoleLog_t8_170_0_0_0,
	&GenInst_IEnumerable_1_t1_3069_gp_0_0_0_0,
	&GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m1_29229_gp_0_0_0_0,
	&GenInst_Array_Sort_m1_29241_gp_0_0_0_0_Array_Sort_m1_29241_gp_0_0_0_0,
	&GenInst_Array_Sort_m1_29242_gp_0_0_0_0_Array_Sort_m1_29242_gp_1_0_0_0,
	&GenInst_Array_Sort_m1_29243_gp_0_0_0_0,
	&GenInst_Array_Sort_m1_29243_gp_0_0_0_0_Array_Sort_m1_29243_gp_0_0_0_0,
	&GenInst_Array_Sort_m1_29244_gp_0_0_0_0,
	&GenInst_Array_Sort_m1_29244_gp_0_0_0_0_Array_Sort_m1_29244_gp_1_0_0_0,
	&GenInst_Array_Sort_m1_29245_gp_0_0_0_0_Array_Sort_m1_29245_gp_0_0_0_0,
	&GenInst_Array_Sort_m1_29246_gp_0_0_0_0_Array_Sort_m1_29246_gp_1_0_0_0,
	&GenInst_Array_Sort_m1_29247_gp_0_0_0_0,
	&GenInst_Array_Sort_m1_29247_gp_0_0_0_0_Array_Sort_m1_29247_gp_0_0_0_0,
	&GenInst_Array_Sort_m1_29248_gp_0_0_0_0,
	&GenInst_Array_Sort_m1_29248_gp_1_0_0_0,
	&GenInst_Array_Sort_m1_29248_gp_0_0_0_0_Array_Sort_m1_29248_gp_1_0_0_0,
	&GenInst_Array_Sort_m1_29249_gp_0_0_0_0,
	&GenInst_Array_Sort_m1_29250_gp_0_0_0_0,
	&GenInst_Array_qsort_m1_29251_gp_0_0_0_0,
	&GenInst_Array_qsort_m1_29251_gp_0_0_0_0_Array_qsort_m1_29251_gp_1_0_0_0,
	&GenInst_Array_compare_m1_29252_gp_0_0_0_0,
	&GenInst_Array_qsort_m1_29253_gp_0_0_0_0,
	&GenInst_Array_Resize_m1_29256_gp_0_0_0_0,
	&GenInst_Array_TrueForAll_m1_29258_gp_0_0_0_0,
	&GenInst_Array_ForEach_m1_29259_gp_0_0_0_0,
	&GenInst_Array_ConvertAll_m1_29260_gp_0_0_0_0_Array_ConvertAll_m1_29260_gp_1_0_0_0,
	&GenInst_Array_FindLastIndex_m1_29261_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m1_29262_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m1_29263_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m1_29264_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m1_29265_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m1_29266_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m1_29267_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m1_29268_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m1_29269_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m1_29270_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m1_29271_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m1_29272_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m1_29273_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m1_29274_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m1_29275_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m1_29276_gp_0_0_0_0,
	&GenInst_Array_FindAll_m1_29277_gp_0_0_0_0,
	&GenInst_Array_Exists_m1_29278_gp_0_0_0_0,
	&GenInst_Array_AsReadOnly_m1_29279_gp_0_0_0_0,
	&GenInst_Array_Find_m1_29280_gp_0_0_0_0,
	&GenInst_Array_FindLast_m1_29281_gp_0_0_0_0,
	&GenInst_InternalEnumerator_1_t1_3070_gp_0_0_0_0,
	&GenInst_ArrayReadOnlyList_1_t1_3071_gp_0_0_0_0,
	&GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1_3072_gp_0_0_0_0,
	&GenInst_IList_1_t1_3073_gp_0_0_0_0,
	&GenInst_ICollection_1_t1_3074_gp_0_0_0_0,
	&GenInst_Nullable_1_t1_1747_gp_0_0_0_0,
	&GenInst_CollectionDebuggerView_1_t1_3080_gp_0_0_0_0,
	&GenInst_KeyValuePair_2_t1_4690_0_0_0,
	&GenInst_CollectionDebuggerView_2_t1_3081_gp_0_0_0_0_CollectionDebuggerView_2_t1_3081_gp_1_0_0_0,
	&GenInst_Comparer_1_t1_3082_gp_0_0_0_0,
	&GenInst_DefaultComparer_t1_3083_gp_0_0_0_0,
	&GenInst_GenericComparer_1_t1_3063_gp_0_0_0_0,
	&GenInst_Dictionary_2_t1_3084_gp_0_0_0_0,
	&GenInst_Dictionary_2_t1_3084_gp_0_0_0_0_Dictionary_2_t1_3084_gp_1_0_0_0,
	&GenInst_Dictionary_2_t1_3084_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t1_4705_0_0_0,
	&GenInst_Dictionary_2_t1_3084_gp_0_0_0_0_Dictionary_2_t1_3084_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m1_29645_gp_0_0_0_0,
	&GenInst_Dictionary_2_t1_3084_gp_0_0_0_0_Dictionary_2_t1_3084_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m1_29650_gp_0_0_0_0,
	&GenInst_Dictionary_2_Do_ICollectionCopyTo_m1_29650_gp_0_0_0_0_Object_t_0_0_0,
	&GenInst_Dictionary_2_t1_3084_gp_0_0_0_0_Dictionary_2_t1_3084_gp_1_0_0_0_DictionaryEntry_t1_284_0_0_0,
	&GenInst_ShimEnumerator_t1_3085_gp_0_0_0_0_ShimEnumerator_t1_3085_gp_1_0_0_0,
	&GenInst_Enumerator_t1_3086_gp_0_0_0_0_Enumerator_t1_3086_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t1_4718_0_0_0,
	&GenInst_KeyCollection_t1_3087_gp_0_0_0_0_KeyCollection_t1_3087_gp_1_0_0_0,
	&GenInst_KeyCollection_t1_3087_gp_0_0_0_0,
	&GenInst_Enumerator_t1_3088_gp_0_0_0_0_Enumerator_t1_3088_gp_1_0_0_0,
	&GenInst_Enumerator_t1_3088_gp_0_0_0_0,
	&GenInst_KeyCollection_t1_3087_gp_0_0_0_0_KeyCollection_t1_3087_gp_1_0_0_0_KeyCollection_t1_3087_gp_0_0_0_0,
	&GenInst_KeyCollection_t1_3087_gp_0_0_0_0_KeyCollection_t1_3087_gp_0_0_0_0,
	&GenInst_ValueCollection_t1_3089_gp_0_0_0_0_ValueCollection_t1_3089_gp_1_0_0_0,
	&GenInst_ValueCollection_t1_3089_gp_1_0_0_0,
	&GenInst_Enumerator_t1_3090_gp_0_0_0_0_Enumerator_t1_3090_gp_1_0_0_0,
	&GenInst_Enumerator_t1_3090_gp_1_0_0_0,
	&GenInst_ValueCollection_t1_3089_gp_0_0_0_0_ValueCollection_t1_3089_gp_1_0_0_0_ValueCollection_t1_3089_gp_1_0_0_0,
	&GenInst_ValueCollection_t1_3089_gp_1_0_0_0_ValueCollection_t1_3089_gp_1_0_0_0,
	&GenInst_Dictionary_2_t1_3084_gp_0_0_0_0_Dictionary_2_t1_3084_gp_1_0_0_0_KeyValuePair_2_t1_4705_0_0_0,
	&GenInst_KeyValuePair_2_t1_4705_0_0_0_KeyValuePair_2_t1_4705_0_0_0,
	&GenInst_EqualityComparer_1_t1_3092_gp_0_0_0_0,
	&GenInst_DefaultComparer_t1_3093_gp_0_0_0_0,
	&GenInst_GenericEqualityComparer_1_t1_3064_gp_0_0_0_0,
	&GenInst_IDictionary_2_t1_3095_gp_0_0_0_0,
	&GenInst_IDictionary_2_t1_3095_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t1_4756_0_0_0,
	&GenInst_IDictionary_2_t1_3095_gp_0_0_0_0_IDictionary_2_t1_3095_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t1_3097_gp_0_0_0_0_KeyValuePair_2_t1_3097_gp_1_0_0_0,
	&GenInst_List_1_t1_3098_gp_0_0_0_0,
	&GenInst_List_1_t1_3098_gp_0_0_0_0_List_1_ConvertAll_m1_29796_gp_0_0_0_0,
	&GenInst_List_1_ConvertAll_m1_29796_gp_0_0_0_0,
	&GenInst_Enumerator_t1_3099_gp_0_0_0_0,
	&GenInst_Collection_1_t1_3100_gp_0_0_0_0,
	&GenInst_KeyedCollection_2_t1_3101_gp_1_0_0_0,
	&GenInst_KeyedCollection_2_t1_3101_gp_0_0_0_0,
	&GenInst_KeyedCollection_2_t1_3101_gp_0_0_0_0_KeyedCollection_2_t1_3101_gp_1_0_0_0,
	&GenInst_ReadOnlyCollection_1_t1_3102_gp_0_0_0_0,
	&GenInst_EventInfo_AddEventFrame_m1_30082_gp_0_0_0_0_EventInfo_AddEventFrame_m1_30082_gp_1_0_0_0,
	&GenInst_EventInfo_StaticAddEventAdapterFrame_m1_30083_gp_0_0_0_0,
	&GenInst_MonoProperty_GetterAdapterFrame_m1_30104_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m1_30104_gp_1_0_0_0,
	&GenInst_MonoProperty_StaticGetterAdapterFrame_m1_30105_gp_0_0_0_0,
	&GenInst_ArraySegment_1_t1_3192_gp_0_0_0_0,
	&GenInst_Nullable_Compare_m1_31199_gp_0_0_0_0,
	&GenInst_Nullable_Equals_m1_31200_gp_0_0_0_0,
	&GenInst_Enumerable_First_m5_93_gp_0_0_0_0,
	&GenInst_Enumerable_First_m5_93_gp_0_0_0_0_Boolean_t1_20_0_0_0,
	&GenInst_Enumerable_FirstOrDefault_m5_94_gp_0_0_0_0,
	&GenInst_Enumerable_FirstOrDefault_m5_94_gp_0_0_0_0_Boolean_t1_20_0_0_0,
	&GenInst_Enumerable_Where_m5_95_gp_0_0_0_0,
	&GenInst_Enumerable_Where_m5_95_gp_0_0_0_0_Boolean_t1_20_0_0_0,
	&GenInst_Enumerable_CreateWhereIterator_m5_96_gp_0_0_0_0,
	&GenInst_Enumerable_CreateWhereIterator_m5_96_gp_0_0_0_0_Boolean_t1_20_0_0_0,
	&GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t5_24_gp_0_0_0_0,
	&GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t5_24_gp_0_0_0_0_Boolean_t1_20_0_0_0,
	&GenInst_Queue_1_t3_299_gp_0_0_0_0,
	&GenInst_Enumerator_t3_300_gp_0_0_0_0,
	&GenInst_SortedList_2_t3_301_gp_0_0_0_0,
	&GenInst_SortedList_2_t3_301_gp_1_0_0_0,
	&GenInst_SortedList_2_t3_301_gp_0_0_0_0_SortedList_2_t3_301_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t1_4816_0_0_0,
	&GenInst_EnumeratorMode_t3_302_gp_0_0_0_0_EnumeratorMode_t3_302_gp_1_0_0_0,
	&GenInst_Enumerator_t3_303_gp_0_0_0_0_Enumerator_t3_303_gp_1_0_0_0,
	&GenInst_KeyEnumerator_t3_304_gp_0_0_0_0_KeyEnumerator_t3_304_gp_1_0_0_0,
	&GenInst_KeyEnumerator_t3_304_gp_0_0_0_0,
	&GenInst_ValueEnumerator_t3_305_gp_0_0_0_0_ValueEnumerator_t3_305_gp_1_0_0_0,
	&GenInst_ValueEnumerator_t3_305_gp_1_0_0_0,
	&GenInst_ListKeys_t3_306_gp_0_0_0_0_ListKeys_t3_306_gp_1_0_0_0,
	&GenInst_ListKeys_t3_306_gp_0_0_0_0,
	&GenInst_GetEnumeratorU3Ec__Iterator2_t3_307_gp_0_0_0_0_GetEnumeratorU3Ec__Iterator2_t3_307_gp_1_0_0_0,
	&GenInst_ListValues_t3_308_gp_0_0_0_0_ListValues_t3_308_gp_1_0_0_0,
	&GenInst_ListValues_t3_308_gp_1_0_0_0,
	&GenInst_GetEnumeratorU3Ec__Iterator3_t3_309_gp_0_0_0_0_GetEnumeratorU3Ec__Iterator3_t3_309_gp_1_0_0_0,
	&GenInst_GetEnumeratorU3Ec__Iterator0_t3_310_gp_0_0_0_0_GetEnumeratorU3Ec__Iterator0_t3_310_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t1_4833_0_0_0,
	&GenInst_U3CGetEnumeratorU3Ec__Iterator1_t3_311_gp_0_0_0_0_U3CGetEnumeratorU3Ec__Iterator1_t3_311_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t1_4835_0_0_0,
	&GenInst_Stack_1_t3_312_gp_0_0_0_0,
	&GenInst_Enumerator_t3_313_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m6_2136_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m6_2137_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m6_2138_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m6_2139_gp_0_0_0_0,
	&GenInst_Component_GetComponents_m6_2141_gp_0_0_0_0,
	&GenInst_GameObject_GetComponents_m6_2144_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInChildren_m6_2146_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInParent_m6_2147_gp_0_0_0_0,
	&GenInst_InvokableCall_1_t6_373_gp_0_0_0_0,
	&GenInst_InvokableCall_2_t6_374_gp_0_0_0_0_InvokableCall_2_t6_374_gp_1_0_0_0,
	&GenInst_InvokableCall_2_t6_374_gp_0_0_0_0,
	&GenInst_InvokableCall_2_t6_374_gp_1_0_0_0,
	&GenInst_InvokableCall_3_t6_375_gp_0_0_0_0_InvokableCall_3_t6_375_gp_1_0_0_0_InvokableCall_3_t6_375_gp_2_0_0_0,
	&GenInst_InvokableCall_3_t6_375_gp_0_0_0_0,
	&GenInst_InvokableCall_3_t6_375_gp_1_0_0_0,
	&GenInst_InvokableCall_3_t6_375_gp_2_0_0_0,
	&GenInst_InvokableCall_4_t6_376_gp_0_0_0_0_InvokableCall_4_t6_376_gp_1_0_0_0_InvokableCall_4_t6_376_gp_2_0_0_0_InvokableCall_4_t6_376_gp_3_0_0_0,
	&GenInst_InvokableCall_4_t6_376_gp_0_0_0_0,
	&GenInst_InvokableCall_4_t6_376_gp_1_0_0_0,
	&GenInst_InvokableCall_4_t6_376_gp_2_0_0_0,
	&GenInst_InvokableCall_4_t6_376_gp_3_0_0_0,
	&GenInst_CachedInvokableCall_1_t6_299_gp_0_0_0_0,
	&GenInst_UnityEvent_1_t6_377_gp_0_0_0_0,
	&GenInst_UnityEvent_2_t6_378_gp_0_0_0_0_UnityEvent_2_t6_378_gp_1_0_0_0,
	&GenInst_UnityEvent_3_t6_379_gp_0_0_0_0_UnityEvent_3_t6_379_gp_1_0_0_0_UnityEvent_3_t6_379_gp_2_0_0_0,
	&GenInst_UnityEvent_4_t6_380_gp_0_0_0_0_UnityEvent_4_t6_380_gp_1_0_0_0_UnityEvent_4_t6_380_gp_2_0_0_0_UnityEvent_4_t6_380_gp_3_0_0_0,
	&GenInst_ExecuteEvents_Execute_m7_1774_gp_0_0_0_0,
	&GenInst_ExecuteEvents_ExecuteHierarchy_m7_1775_gp_0_0_0_0,
	&GenInst_ExecuteEvents_GetEventList_m7_1777_gp_0_0_0_0,
	&GenInst_ExecuteEvents_CanHandleEvent_m7_1778_gp_0_0_0_0,
	&GenInst_ExecuteEvents_GetEventHandler_m7_1779_gp_0_0_0_0,
	&GenInst_TweenRunner_1_t7_235_gp_0_0_0_0,
	&GenInst_Dropdown_GetOrAddComponent_m7_1806_gp_0_0_0_0,
	&GenInst_IndexedSet_1_t7_239_gp_0_0_0_0,
	&GenInst_IndexedSet_1_t7_239_gp_0_0_0_0_Int32_t1_3_0_0_0,
	&GenInst_ListPool_1_t7_240_gp_0_0_0_0,
	&GenInst_List_1_t1_4856_0_0_0,
	&GenInst_ObjectPool_1_t7_241_gp_0_0_0_0,
	&GenInst_Extensions_Pop_m8_2384_gp_0_0_0_0,
	&GenInst_ObserverPattern_1_t8_392_gp_0_0_0_0,
	&GenInst_SingletonPattern_1_t8_394_gp_0_0_0_0,
	&GenInst_ComponentExtensions_AddComponentIfDoesntExist_m8_2407_gp_0_0_0_0,
	&GenInst_ComponentExtensions_GetComponentsInChildren_m8_2408_gp_0_0_0_0,
	&GenInst_GameObjectExtensions_AddInvisibleComponent_m8_2409_gp_0_0_0_0,
	&GenInst_IDictionaryExtensions_GetIfAvailable_m8_2411_gp_0_0_0_0,
	&GenInst_AdvancedScriptableObject_1_t8_395_gp_0_0_0_0,
	&GenInst_ExifEnumProperty_1_t8_396_gp_0_0_0_0,
	&GenInst_NPBinding_AddComponentBasedOnPlatform_m8_2489_gp_0_0_0_0,
};
