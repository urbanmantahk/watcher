﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType.h"
#include "mscorlib_System_Guid.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_Runtime_InteropServices_TYPEKIND.h"
#include "mscorlib_System_Runtime_InteropServices_TYPEFLAGS.h"
#include "mscorlib_System_Runtime_InteropServices_TYPEDESC.h"
#include "mscorlib_System_Runtime_InteropServices_IDLDESC.h"

// System.Runtime.InteropServices.TYPEATTR
struct  TYPEATTR_t1_827 
{
	// System.Guid System.Runtime.InteropServices.TYPEATTR::guid
	Guid_t1_319  ___guid_1;
	// System.Int32 System.Runtime.InteropServices.TYPEATTR::lcid
	int32_t ___lcid_2;
	// System.Int32 System.Runtime.InteropServices.TYPEATTR::dwReserved
	int32_t ___dwReserved_3;
	// System.Int32 System.Runtime.InteropServices.TYPEATTR::memidConstructor
	int32_t ___memidConstructor_4;
	// System.Int32 System.Runtime.InteropServices.TYPEATTR::memidDestructor
	int32_t ___memidDestructor_5;
	// System.IntPtr System.Runtime.InteropServices.TYPEATTR::lpstrSchema
	IntPtr_t ___lpstrSchema_6;
	// System.Int32 System.Runtime.InteropServices.TYPEATTR::cbSizeInstance
	int32_t ___cbSizeInstance_7;
	// System.Runtime.InteropServices.TYPEKIND System.Runtime.InteropServices.TYPEATTR::typekind
	int32_t ___typekind_8;
	// System.Int16 System.Runtime.InteropServices.TYPEATTR::cFuncs
	int16_t ___cFuncs_9;
	// System.Int16 System.Runtime.InteropServices.TYPEATTR::cVars
	int16_t ___cVars_10;
	// System.Int16 System.Runtime.InteropServices.TYPEATTR::cImplTypes
	int16_t ___cImplTypes_11;
	// System.Int16 System.Runtime.InteropServices.TYPEATTR::cbSizeVft
	int16_t ___cbSizeVft_12;
	// System.Int16 System.Runtime.InteropServices.TYPEATTR::cbAlignment
	int16_t ___cbAlignment_13;
	// System.Runtime.InteropServices.TYPEFLAGS System.Runtime.InteropServices.TYPEATTR::wTypeFlags
	int32_t ___wTypeFlags_14;
	// System.Int16 System.Runtime.InteropServices.TYPEATTR::wMajorVerNum
	int16_t ___wMajorVerNum_15;
	// System.Int16 System.Runtime.InteropServices.TYPEATTR::wMinorVerNum
	int16_t ___wMinorVerNum_16;
	// System.Runtime.InteropServices.TYPEDESC System.Runtime.InteropServices.TYPEATTR::tdescAlias
	TYPEDESC_t1_786  ___tdescAlias_17;
	// System.Runtime.InteropServices.IDLDESC System.Runtime.InteropServices.TYPEATTR::idldescType
	IDLDESC_t1_783  ___idldescType_18;
};
// Native definition for marshalling of: System.Runtime.InteropServices.TYPEATTR
struct TYPEATTR_t1_827_marshaled
{
	Guid_t1_319  ___guid_1;
	int32_t ___lcid_2;
	int32_t ___dwReserved_3;
	int32_t ___memidConstructor_4;
	int32_t ___memidDestructor_5;
	intptr_t ___lpstrSchema_6;
	int32_t ___cbSizeInstance_7;
	int32_t ___typekind_8;
	int16_t ___cFuncs_9;
	int16_t ___cVars_10;
	int16_t ___cImplTypes_11;
	int16_t ___cbSizeVft_12;
	int16_t ___cbAlignment_13;
	int32_t ___wTypeFlags_14;
	int16_t ___wMajorVerNum_15;
	int16_t ___wMinorVerNum_16;
	TYPEDESC_t1_786  ___tdescAlias_17;
	IDLDESC_t1_783_marshaled ___idldescType_18;
};
