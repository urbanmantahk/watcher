﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.X509.X509ExtensionCollection
struct X509ExtensionCollection_t1_190;
// Mono.Security.ASN1
struct ASN1_t1_149;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// Mono.Security.X509.X509Extension
struct X509Extension_t1_178;
// Mono.Security.X509.X509Extension[]
struct X509ExtensionU5BU5D_t1_1675;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Security.X509.X509ExtensionCollection::.ctor()
extern "C" void X509ExtensionCollection__ctor_m1_2338 (X509ExtensionCollection_t1_190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509ExtensionCollection::.ctor(Mono.Security.ASN1)
extern "C" void X509ExtensionCollection__ctor_m1_2339 (X509ExtensionCollection_t1_190 * __this, ASN1_t1_149 * ___asn1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Mono.Security.X509.X509ExtensionCollection::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * X509ExtensionCollection_System_Collections_IEnumerable_GetEnumerator_m1_2340 (X509ExtensionCollection_t1_190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Security.X509.X509ExtensionCollection::Add(Mono.Security.X509.X509Extension)
extern "C" int32_t X509ExtensionCollection_Add_m1_2341 (X509ExtensionCollection_t1_190 * __this, X509Extension_t1_178 * ___extension, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509ExtensionCollection::AddRange(Mono.Security.X509.X509Extension[])
extern "C" void X509ExtensionCollection_AddRange_m1_2342 (X509ExtensionCollection_t1_190 * __this, X509ExtensionU5BU5D_t1_1675* ___extension, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509ExtensionCollection::AddRange(Mono.Security.X509.X509ExtensionCollection)
extern "C" void X509ExtensionCollection_AddRange_m1_2343 (X509ExtensionCollection_t1_190 * __this, X509ExtensionCollection_t1_190 * ___collection, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509ExtensionCollection::Contains(Mono.Security.X509.X509Extension)
extern "C" bool X509ExtensionCollection_Contains_m1_2344 (X509ExtensionCollection_t1_190 * __this, X509Extension_t1_178 * ___extension, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509ExtensionCollection::Contains(System.String)
extern "C" bool X509ExtensionCollection_Contains_m1_2345 (X509ExtensionCollection_t1_190 * __this, String_t* ___oid, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509ExtensionCollection::CopyTo(Mono.Security.X509.X509Extension[],System.Int32)
extern "C" void X509ExtensionCollection_CopyTo_m1_2346 (X509ExtensionCollection_t1_190 * __this, X509ExtensionU5BU5D_t1_1675* ___extensions, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Security.X509.X509ExtensionCollection::IndexOf(Mono.Security.X509.X509Extension)
extern "C" int32_t X509ExtensionCollection_IndexOf_m1_2347 (X509ExtensionCollection_t1_190 * __this, X509Extension_t1_178 * ___extension, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Security.X509.X509ExtensionCollection::IndexOf(System.String)
extern "C" int32_t X509ExtensionCollection_IndexOf_m1_2348 (X509ExtensionCollection_t1_190 * __this, String_t* ___oid, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509ExtensionCollection::Insert(System.Int32,Mono.Security.X509.X509Extension)
extern "C" void X509ExtensionCollection_Insert_m1_2349 (X509ExtensionCollection_t1_190 * __this, int32_t ___index, X509Extension_t1_178 * ___extension, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509ExtensionCollection::Remove(Mono.Security.X509.X509Extension)
extern "C" void X509ExtensionCollection_Remove_m1_2350 (X509ExtensionCollection_t1_190 * __this, X509Extension_t1_178 * ___extension, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509ExtensionCollection::Remove(System.String)
extern "C" void X509ExtensionCollection_Remove_m1_2351 (X509ExtensionCollection_t1_190 * __this, String_t* ___oid, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Extension Mono.Security.X509.X509ExtensionCollection::get_Item(System.Int32)
extern "C" X509Extension_t1_178 * X509ExtensionCollection_get_Item_m1_2352 (X509ExtensionCollection_t1_190 * __this, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Extension Mono.Security.X509.X509ExtensionCollection::get_Item(System.String)
extern "C" X509Extension_t1_178 * X509ExtensionCollection_get_Item_m1_2353 (X509ExtensionCollection_t1_190 * __this, String_t* ___oid, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.X509.X509ExtensionCollection::GetBytes()
extern "C" ByteU5BU5D_t1_109* X509ExtensionCollection_GetBytes_m1_2354 (X509ExtensionCollection_t1_190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
