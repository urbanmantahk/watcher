﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Security_AccessControl_CryptoKeyRights.h"

// System.Security.AccessControl.CryptoKeyRights
struct  CryptoKeyRights_t1_1141 
{
	// System.Int32 System.Security.AccessControl.CryptoKeyRights::value__
	int32_t ___value___1;
};
