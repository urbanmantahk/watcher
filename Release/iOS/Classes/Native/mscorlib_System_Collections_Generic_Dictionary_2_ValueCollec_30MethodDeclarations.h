﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_8MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Action,System.Collections.Generic.Dictionary`2<System.String,System.Single>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m1_27032(__this, ___dictionary, method) (( void (*) (ValueCollection_t1_2702 *, Dictionary_2_t1_1907 *, const MethodInfo*))ValueCollection__ctor_m1_16008_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Action,System.Collections.Generic.Dictionary`2<System.String,System.Single>>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1_27033(__this, ___item, method) (( void (*) (ValueCollection_t1_2702 *, Dictionary_2_t1_1929 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1_16009_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Action,System.Collections.Generic.Dictionary`2<System.String,System.Single>>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1_27034(__this, method) (( void (*) (ValueCollection_t1_2702 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1_16010_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Action,System.Collections.Generic.Dictionary`2<System.String,System.Single>>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1_27035(__this, ___item, method) (( bool (*) (ValueCollection_t1_2702 *, Dictionary_2_t1_1929 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1_16011_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Action,System.Collections.Generic.Dictionary`2<System.String,System.Single>>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1_27036(__this, ___item, method) (( bool (*) (ValueCollection_t1_2702 *, Dictionary_2_t1_1929 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1_16012_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Action,System.Collections.Generic.Dictionary`2<System.String,System.Single>>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1_27037(__this, method) (( Object_t* (*) (ValueCollection_t1_2702 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1_16013_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Action,System.Collections.Generic.Dictionary`2<System.String,System.Single>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m1_27038(__this, ___array, ___index, method) (( void (*) (ValueCollection_t1_2702 *, Array_t *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m1_16014_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Action,System.Collections.Generic.Dictionary`2<System.String,System.Single>>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1_27039(__this, method) (( Object_t * (*) (ValueCollection_t1_2702 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1_16015_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Action,System.Collections.Generic.Dictionary`2<System.String,System.Single>>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1_27040(__this, method) (( bool (*) (ValueCollection_t1_2702 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1_16016_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Action,System.Collections.Generic.Dictionary`2<System.String,System.Single>>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1_27041(__this, method) (( bool (*) (ValueCollection_t1_2702 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1_16017_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Action,System.Collections.Generic.Dictionary`2<System.String,System.Single>>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m1_27042(__this, method) (( Object_t * (*) (ValueCollection_t1_2702 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1_16018_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Action,System.Collections.Generic.Dictionary`2<System.String,System.Single>>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m1_27043(__this, ___array, ___index, method) (( void (*) (ValueCollection_t1_2702 *, Dictionary_2U5BU5D_t1_2692*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1_16019_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Action,System.Collections.Generic.Dictionary`2<System.String,System.Single>>::GetEnumerator()
#define ValueCollection_GetEnumerator_m1_27044(__this, method) (( Enumerator_t1_2888  (*) (ValueCollection_t1_2702 *, const MethodInfo*))ValueCollection_GetEnumerator_m1_16020_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Action,System.Collections.Generic.Dictionary`2<System.String,System.Single>>::get_Count()
#define ValueCollection_get_Count_m1_27045(__this, method) (( int32_t (*) (ValueCollection_t1_2702 *, const MethodInfo*))ValueCollection_get_Count_m1_16021_gshared)(__this, method)
