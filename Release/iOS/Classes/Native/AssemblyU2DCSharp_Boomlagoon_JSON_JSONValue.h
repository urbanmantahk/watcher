﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Boomlagoon.JSON.JSONObject
struct JSONObject_t8_5;
// Boomlagoon.JSON.JSONArray
struct JSONArray_t8_6;
// Boomlagoon.JSON.JSONValue
struct JSONValue_t8_4;

#include "mscorlib_System_Object.h"
#include "AssemblyU2DCSharp_Boomlagoon_JSON_JSONValueType.h"

// Boomlagoon.JSON.JSONValue
struct  JSONValue_t8_4  : public Object_t
{
	// Boomlagoon.JSON.JSONValueType Boomlagoon.JSON.JSONValue::<Type>k__BackingField
	int32_t ___U3CTypeU3Ek__BackingField_0;
	// System.String Boomlagoon.JSON.JSONValue::<Str>k__BackingField
	String_t* ___U3CStrU3Ek__BackingField_1;
	// System.Double Boomlagoon.JSON.JSONValue::<Number>k__BackingField
	double ___U3CNumberU3Ek__BackingField_2;
	// Boomlagoon.JSON.JSONObject Boomlagoon.JSON.JSONValue::<Obj>k__BackingField
	JSONObject_t8_5 * ___U3CObjU3Ek__BackingField_3;
	// Boomlagoon.JSON.JSONArray Boomlagoon.JSON.JSONValue::<Array>k__BackingField
	JSONArray_t8_6 * ___U3CArrayU3Ek__BackingField_4;
	// System.Boolean Boomlagoon.JSON.JSONValue::<Boolean>k__BackingField
	bool ___U3CBooleanU3Ek__BackingField_5;
	// Boomlagoon.JSON.JSONValue Boomlagoon.JSON.JSONValue::<Parent>k__BackingField
	JSONValue_t8_4 * ___U3CParentU3Ek__BackingField_6;
};
