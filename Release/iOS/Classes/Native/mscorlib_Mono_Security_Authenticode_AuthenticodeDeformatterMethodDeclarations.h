﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.Authenticode.AuthenticodeDeformatter
struct AuthenticodeDeformatter_t1_147;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// Mono.Security.X509.X509CertificateCollection
struct X509CertificateCollection_t1_148;
// Mono.Security.X509.X509Certificate
struct X509Certificate_t1_151;
// Mono.Security.PKCS7/SignedData
struct SignedData_t1_226;
// System.Security.Cryptography.HashAlgorithm
struct HashAlgorithm_t1_162;
// Mono.Security.PKCS7/SignerInfo
struct SignerInfo_t1_227;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime.h"

// System.Void Mono.Security.Authenticode.AuthenticodeDeformatter::.ctor()
extern "C" void AuthenticodeDeformatter__ctor_m1_1870 (AuthenticodeDeformatter_t1_147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Authenticode.AuthenticodeDeformatter::.ctor(System.String)
extern "C" void AuthenticodeDeformatter__ctor_m1_1871 (AuthenticodeDeformatter_t1_147 * __this, String_t* ___fileName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.Authenticode.AuthenticodeDeformatter::get_FileName()
extern "C" String_t* AuthenticodeDeformatter_get_FileName_m1_1872 (AuthenticodeDeformatter_t1_147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Authenticode.AuthenticodeDeformatter::set_FileName(System.String)
extern "C" void AuthenticodeDeformatter_set_FileName_m1_1873 (AuthenticodeDeformatter_t1_147 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Authenticode.AuthenticodeDeformatter::get_Hash()
extern "C" ByteU5BU5D_t1_109* AuthenticodeDeformatter_get_Hash_m1_1874 (AuthenticodeDeformatter_t1_147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Security.Authenticode.AuthenticodeDeformatter::get_Reason()
extern "C" int32_t AuthenticodeDeformatter_get_Reason_m1_1875 (AuthenticodeDeformatter_t1_147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.Authenticode.AuthenticodeDeformatter::IsTrusted()
extern "C" bool AuthenticodeDeformatter_IsTrusted_m1_1876 (AuthenticodeDeformatter_t1_147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Authenticode.AuthenticodeDeformatter::get_Signature()
extern "C" ByteU5BU5D_t1_109* AuthenticodeDeformatter_get_Signature_m1_1877 (AuthenticodeDeformatter_t1_147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Mono.Security.Authenticode.AuthenticodeDeformatter::get_Timestamp()
extern "C" DateTime_t1_150  AuthenticodeDeformatter_get_Timestamp_m1_1878 (AuthenticodeDeformatter_t1_147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509CertificateCollection Mono.Security.Authenticode.AuthenticodeDeformatter::get_Certificates()
extern "C" X509CertificateCollection_t1_148 * AuthenticodeDeformatter_get_Certificates_m1_1879 (AuthenticodeDeformatter_t1_147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Certificate Mono.Security.Authenticode.AuthenticodeDeformatter::get_SigningCertificate()
extern "C" X509Certificate_t1_151 * AuthenticodeDeformatter_get_SigningCertificate_m1_1880 (AuthenticodeDeformatter_t1_147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.Authenticode.AuthenticodeDeformatter::CheckSignature(System.String)
extern "C" bool AuthenticodeDeformatter_CheckSignature_m1_1881 (AuthenticodeDeformatter_t1_147 * __this, String_t* ___fileName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.Authenticode.AuthenticodeDeformatter::CompareIssuerSerial(System.String,System.Byte[],Mono.Security.X509.X509Certificate)
extern "C" bool AuthenticodeDeformatter_CompareIssuerSerial_m1_1882 (AuthenticodeDeformatter_t1_147 * __this, String_t* ___issuer, ByteU5BU5D_t1_109* ___serial, X509Certificate_t1_151 * ___x509, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.Authenticode.AuthenticodeDeformatter::VerifySignature(Mono.Security.PKCS7/SignedData,System.Byte[],System.Security.Cryptography.HashAlgorithm)
extern "C" bool AuthenticodeDeformatter_VerifySignature_m1_1883 (AuthenticodeDeformatter_t1_147 * __this, SignedData_t1_226 * ___sd, ByteU5BU5D_t1_109* ___calculatedMessageDigest, HashAlgorithm_t1_162 * ___ha, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.Authenticode.AuthenticodeDeformatter::VerifyCounterSignature(Mono.Security.PKCS7/SignerInfo,System.Byte[])
extern "C" bool AuthenticodeDeformatter_VerifyCounterSignature_m1_1884 (AuthenticodeDeformatter_t1_147 * __this, SignerInfo_t1_227 * ___cs, ByteU5BU5D_t1_109* ___signature, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Authenticode.AuthenticodeDeformatter::Reset()
extern "C" void AuthenticodeDeformatter_Reset_m1_1885 (AuthenticodeDeformatter_t1_147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
