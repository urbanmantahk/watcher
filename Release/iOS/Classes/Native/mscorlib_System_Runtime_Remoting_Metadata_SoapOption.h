﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Runtime_Remoting_Metadata_SoapOption.h"

// System.Runtime.Remoting.Metadata.SoapOption
struct  SoapOption_t1_1000 
{
	// System.Int32 System.Runtime.Remoting.Metadata.SoapOption::value__
	int32_t ___value___1;
};
