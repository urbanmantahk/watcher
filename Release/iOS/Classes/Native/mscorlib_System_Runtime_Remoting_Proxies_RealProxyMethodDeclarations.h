﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Proxies.RealProxy
struct RealProxy_t1_130;
// System.Type
struct Type_t;
// System.Runtime.Remoting.ClientIdentity
struct ClientIdentity_t1_1013;
// System.Object
struct Object_t;
// System.Runtime.Remoting.ObjRef
struct ObjRef_t1_923;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;
// System.Runtime.Remoting.Identity
struct Identity_t1_943;
// System.Runtime.Remoting.Messaging.IMessage
struct IMessage_t1_875;
// System.Exception
struct Exception_t1_33;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.String
struct String_t;
// System.Runtime.Remoting.Activation.IConstructionReturnMessage
struct IConstructionReturnMessage_t1_1703;
// System.Runtime.Remoting.Activation.IConstructionCallMessage
struct IConstructionCallMessage_t1_1702;
// System.MarshalByRefObject
struct MarshalByRefObject_t1_69;
// System.Runtime.Remoting.Messaging.IMethodReturnMessage
struct IMethodReturnMessage_t1_1718;
// System.Runtime.Remoting.Messaging.MonoMethodMessage
struct MonoMethodMessage_t1_919;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Guid.h"

// System.Void System.Runtime.Remoting.Proxies.RealProxy::.ctor()
extern "C" void RealProxy__ctor_m1_8989 (RealProxy_t1_130 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Proxies.RealProxy::.ctor(System.Type)
extern "C" void RealProxy__ctor_m1_8990 (RealProxy_t1_130 * __this, Type_t * ___classToProxy, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Proxies.RealProxy::.ctor(System.Type,System.Runtime.Remoting.ClientIdentity)
extern "C" void RealProxy__ctor_m1_8991 (RealProxy_t1_130 * __this, Type_t * ___classToProxy, ClientIdentity_t1_1013 * ___identity, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Proxies.RealProxy::.ctor(System.Type,System.IntPtr,System.Object)
extern "C" void RealProxy__ctor_m1_8992 (RealProxy_t1_130 * __this, Type_t * ___classToProxy, IntPtr_t ___stub, Object_t * ___stubData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Runtime.Remoting.Proxies.RealProxy::InternalGetProxyType(System.Object)
extern "C" Type_t * RealProxy_InternalGetProxyType_m1_8993 (Object_t * __this /* static, unused */, Object_t * ___transparentProxy, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Runtime.Remoting.Proxies.RealProxy::GetProxiedType()
extern "C" Type_t * RealProxy_GetProxiedType_m1_8994 (RealProxy_t1_130 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.ObjRef System.Runtime.Remoting.Proxies.RealProxy::CreateObjRef(System.Type)
extern "C" ObjRef_t1_923 * RealProxy_CreateObjRef_m1_8995 (RealProxy_t1_130 * __this, Type_t * ___requestedType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Proxies.RealProxy::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void RealProxy_GetObjectData_m1_8996 (RealProxy_t1_130 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Identity System.Runtime.Remoting.Proxies.RealProxy::get_ObjectIdentity()
extern "C" Identity_t1_943 * RealProxy_get_ObjectIdentity_m1_8997 (RealProxy_t1_130 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Proxies.RealProxy::set_ObjectIdentity(System.Runtime.Remoting.Identity)
extern "C" void RealProxy_set_ObjectIdentity_m1_8998 (RealProxy_t1_130 * __this, Identity_t1_943 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.Remoting.Proxies.RealProxy::GetCOMIUnknown(System.Boolean)
extern "C" IntPtr_t RealProxy_GetCOMIUnknown_m1_8999 (RealProxy_t1_130 * __this, bool ___fIsMarshalled, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Proxies.RealProxy::SetCOMIUnknown(System.IntPtr)
extern "C" void RealProxy_SetCOMIUnknown_m1_9000 (RealProxy_t1_130 * __this, IntPtr_t ___i, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.Remoting.Proxies.RealProxy::SupportsInterface(System.Guid&)
extern "C" IntPtr_t RealProxy_SupportsInterface_m1_9001 (RealProxy_t1_130 * __this, Guid_t1_319 * ___iid, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Proxies.RealProxy::GetStubData(System.Runtime.Remoting.Proxies.RealProxy)
extern "C" Object_t * RealProxy_GetStubData_m1_9002 (Object_t * __this /* static, unused */, RealProxy_t1_130 * ___rp, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Proxies.RealProxy::SetStubData(System.Runtime.Remoting.Proxies.RealProxy,System.Object)
extern "C" void RealProxy_SetStubData_m1_9003 (Object_t * __this /* static, unused */, RealProxy_t1_130 * ___rp, Object_t * ___stubData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Proxies.RealProxy::PrivateInvoke(System.Runtime.Remoting.Proxies.RealProxy,System.Runtime.Remoting.Messaging.IMessage,System.Exception&,System.Object[]&)
extern "C" Object_t * RealProxy_PrivateInvoke_m1_9004 (Object_t * __this /* static, unused */, RealProxy_t1_130 * ___rp, Object_t * ___msg, Exception_t1_33 ** ___exc, ObjectU5BU5D_t1_272** ___out_args, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Proxies.RealProxy::InternalGetTransparentProxy(System.String)
extern "C" Object_t * RealProxy_InternalGetTransparentProxy_m1_9005 (RealProxy_t1_130 * __this, String_t* ___className, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Proxies.RealProxy::GetTransparentProxy()
extern "C" Object_t * RealProxy_GetTransparentProxy_m1_9006 (RealProxy_t1_130 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Activation.IConstructionReturnMessage System.Runtime.Remoting.Proxies.RealProxy::InitializeServerObject(System.Runtime.Remoting.Activation.IConstructionCallMessage)
extern "C" Object_t * RealProxy_InitializeServerObject_m1_9007 (RealProxy_t1_130 * __this, Object_t * ___ctorMsg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Proxies.RealProxy::AttachServer(System.MarshalByRefObject)
extern "C" void RealProxy_AttachServer_m1_9008 (RealProxy_t1_130 * __this, MarshalByRefObject_t1_69 * ___s, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.MarshalByRefObject System.Runtime.Remoting.Proxies.RealProxy::DetachServer()
extern "C" MarshalByRefObject_t1_69 * RealProxy_DetachServer_m1_9009 (RealProxy_t1_130 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.MarshalByRefObject System.Runtime.Remoting.Proxies.RealProxy::GetUnwrappedServer()
extern "C" MarshalByRefObject_t1_69 * RealProxy_GetUnwrappedServer_m1_9010 (RealProxy_t1_130 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Proxies.RealProxy::SetTargetDomain(System.Int32)
extern "C" void RealProxy_SetTargetDomain_m1_9011 (RealProxy_t1_130 * __this, int32_t ___domainId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Proxies.RealProxy::GetAppDomainTarget()
extern "C" Object_t * RealProxy_GetAppDomainTarget_m1_9012 (RealProxy_t1_130 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Runtime.Remoting.Proxies.RealProxy::ProcessResponse(System.Runtime.Remoting.Messaging.IMethodReturnMessage,System.Runtime.Remoting.Messaging.MonoMethodMessage)
extern "C" ObjectU5BU5D_t1_272* RealProxy_ProcessResponse_m1_9013 (Object_t * __this /* static, unused */, Object_t * ___mrm, MonoMethodMessage_t1_919 * ___call, const MethodInfo* method) IL2CPP_METHOD_ATTR;
