﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.Internal.Constants
struct Constants_t8_323;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.Internal.Constants::.ctor()
extern "C" void Constants__ctor_m8_1899 (Constants_t8_323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
