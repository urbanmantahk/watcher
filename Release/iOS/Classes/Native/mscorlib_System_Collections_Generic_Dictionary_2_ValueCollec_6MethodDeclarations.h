﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t1_1978;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_6.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m1_15741_gshared (Enumerator_t1_1989 * __this, Dictionary_2_t1_1978 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m1_15741(__this, ___host, method) (( void (*) (Enumerator_t1_1989 *, Dictionary_2_t1_1978 *, const MethodInfo*))Enumerator__ctor_m1_15741_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m1_15742_gshared (Enumerator_t1_1989 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_15742(__this, method) (( Object_t * (*) (Enumerator_t1_1989 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_15742_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m1_15743_gshared (Enumerator_t1_1989 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1_15743(__this, method) (( void (*) (Enumerator_t1_1989 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_15743_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int32>::Dispose()
extern "C" void Enumerator_Dispose_m1_15744_gshared (Enumerator_t1_1989 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1_15744(__this, method) (( void (*) (Enumerator_t1_1989 *, const MethodInfo*))Enumerator_Dispose_m1_15744_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int32>::MoveNext()
extern "C" bool Enumerator_MoveNext_m1_15745_gshared (Enumerator_t1_1989 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1_15745(__this, method) (( bool (*) (Enumerator_t1_1989 *, const MethodInfo*))Enumerator_MoveNext_m1_15745_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int32>::get_Current()
extern "C" int32_t Enumerator_get_Current_m1_15746_gshared (Enumerator_t1_1989 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1_15746(__this, method) (( int32_t (*) (Enumerator_t1_1989 *, const MethodInfo*))Enumerator_get_Current_m1_15746_gshared)(__this, method)
