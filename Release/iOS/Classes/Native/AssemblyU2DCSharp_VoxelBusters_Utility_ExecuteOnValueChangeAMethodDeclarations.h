﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.Utility.ExecuteOnValueChangeAttribute
struct ExecuteOnValueChangeAttribute_t8_148;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.Utility.ExecuteOnValueChangeAttribute::.ctor()
extern "C" void ExecuteOnValueChangeAttribute__ctor_m8_865 (ExecuteOnValueChangeAttribute_t8_148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.ExecuteOnValueChangeAttribute::.ctor(System.String)
extern "C" void ExecuteOnValueChangeAttribute__ctor_m8_866 (ExecuteOnValueChangeAttribute_t8_148 * __this, String_t* ____method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.Utility.ExecuteOnValueChangeAttribute::get_InvokeMethod()
extern "C" String_t* ExecuteOnValueChangeAttribute_get_InvokeMethod_m8_867 (ExecuteOnValueChangeAttribute_t8_148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.ExecuteOnValueChangeAttribute::set_InvokeMethod(System.String)
extern "C" void ExecuteOnValueChangeAttribute_set_InvokeMethod_m8_868 (ExecuteOnValueChangeAttribute_t8_148 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
