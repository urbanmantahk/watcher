﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Permissions.PrincipalPermission/PrincipalInfo
struct PrincipalInfo_t1_1296;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Permissions.PrincipalPermission/PrincipalInfo::.ctor(System.String,System.String,System.Boolean)
extern "C" void PrincipalInfo__ctor_m1_11032 (PrincipalInfo_t1_1296 * __this, String_t* ___name, String_t* ___role, bool ___isAuthenticated, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Permissions.PrincipalPermission/PrincipalInfo::get_Name()
extern "C" String_t* PrincipalInfo_get_Name_m1_11033 (PrincipalInfo_t1_1296 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Permissions.PrincipalPermission/PrincipalInfo::get_Role()
extern "C" String_t* PrincipalInfo_get_Role_m1_11034 (PrincipalInfo_t1_1296 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.PrincipalPermission/PrincipalInfo::get_IsAuthenticated()
extern "C" bool PrincipalInfo_get_IsAuthenticated_m1_11035 (PrincipalInfo_t1_1296 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
