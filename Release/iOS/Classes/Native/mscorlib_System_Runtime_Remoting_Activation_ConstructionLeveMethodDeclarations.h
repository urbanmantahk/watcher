﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Activation.ConstructionLevelActivator
struct ConstructionLevelActivator_t1_854;
// System.Runtime.Remoting.Activation.IActivator
struct IActivator_t1_851;
// System.Runtime.Remoting.Activation.IConstructionReturnMessage
struct IConstructionReturnMessage_t1_1703;
// System.Runtime.Remoting.Activation.IConstructionCallMessage
struct IConstructionCallMessage_t1_1702;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Remoting_Activation_ActivatorLevel.h"

// System.Void System.Runtime.Remoting.Activation.ConstructionLevelActivator::.ctor()
extern "C" void ConstructionLevelActivator__ctor_m1_7957 (ConstructionLevelActivator_t1_854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Activation.ActivatorLevel System.Runtime.Remoting.Activation.ConstructionLevelActivator::get_Level()
extern "C" int32_t ConstructionLevelActivator_get_Level_m1_7958 (ConstructionLevelActivator_t1_854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Activation.IActivator System.Runtime.Remoting.Activation.ConstructionLevelActivator::get_NextActivator()
extern "C" Object_t * ConstructionLevelActivator_get_NextActivator_m1_7959 (ConstructionLevelActivator_t1_854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Activation.ConstructionLevelActivator::set_NextActivator(System.Runtime.Remoting.Activation.IActivator)
extern "C" void ConstructionLevelActivator_set_NextActivator_m1_7960 (ConstructionLevelActivator_t1_854 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Activation.IConstructionReturnMessage System.Runtime.Remoting.Activation.ConstructionLevelActivator::Activate(System.Runtime.Remoting.Activation.IConstructionCallMessage)
extern "C" Object_t * ConstructionLevelActivator_Activate_m1_7961 (ConstructionLevelActivator_t1_854 * __this, Object_t * ___msg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
