﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>
struct U3CGetEnumeratorU3Ec__Iterator0_t1_1967;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::.ctor()
extern "C" void U3CGetEnumeratorU3Ec__Iterator0__ctor_m1_15512_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1_1967 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0__ctor_m1_15512(__this, method) (( void (*) (U3CGetEnumeratorU3Ec__Iterator0_t1_1967 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0__ctor_m1_15512_gshared)(__this, method)
// T System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C" Object_t * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1_15513_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1_1967 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1_15513(__this, method) (( Object_t * (*) (U3CGetEnumeratorU3Ec__Iterator0_t1_1967 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1_15513_gshared)(__this, method)
// System.Object System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1_15514_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1_1967 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1_15514(__this, method) (( Object_t * (*) (U3CGetEnumeratorU3Ec__Iterator0_t1_1967 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1_15514_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::MoveNext()
extern "C" bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m1_15515_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1_1967 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m1_15515(__this, method) (( bool (*) (U3CGetEnumeratorU3Ec__Iterator0_t1_1967 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m1_15515_gshared)(__this, method)
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::Dispose()
extern "C" void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m1_15516_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1_1967 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_Dispose_m1_15516(__this, method) (( void (*) (U3CGetEnumeratorU3Ec__Iterator0_t1_1967 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_Dispose_m1_15516_gshared)(__this, method)
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::Reset()
extern "C" void U3CGetEnumeratorU3Ec__Iterator0_Reset_m1_15517_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1_1967 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_Reset_m1_15517(__this, method) (( void (*) (U3CGetEnumeratorU3Ec__Iterator0_t1_1967 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_Reset_m1_15517_gshared)(__this, method)
