﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_OutOfMemoryException.h"

// System.InsufficientMemoryException
struct  InsufficientMemoryException_t1_1556  : public OutOfMemoryException_t1_1557
{
};
