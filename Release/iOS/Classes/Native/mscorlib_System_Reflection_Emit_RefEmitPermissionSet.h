﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_ValueType.h"
#include "mscorlib_System_Security_Permissions_SecurityAction.h"

// System.Reflection.Emit.RefEmitPermissionSet
struct  RefEmitPermissionSet_t1_463 
{
	// System.Security.Permissions.SecurityAction System.Reflection.Emit.RefEmitPermissionSet::action
	int32_t ___action_0;
	// System.String System.Reflection.Emit.RefEmitPermissionSet::pset
	String_t* ___pset_1;
};
// Native definition for marshalling of: System.Reflection.Emit.RefEmitPermissionSet
struct RefEmitPermissionSet_t1_463_marshaled
{
	int32_t ___action_0;
	char* ___pset_1;
};
