﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.Demo.MediaLibraryDemo
struct MediaLibraryDemo_t8_181;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.Demo.MediaLibraryDemo::.ctor()
extern "C" void MediaLibraryDemo__ctor_m8_1059 (MediaLibraryDemo_t8_181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
