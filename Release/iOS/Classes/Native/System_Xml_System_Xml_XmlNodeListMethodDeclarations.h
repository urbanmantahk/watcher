﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XmlNodeList
struct XmlNodeList_t4_147;
// System.Xml.XmlNode
struct XmlNode_t4_116;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Xml.XmlNodeList::.ctor()
extern "C" void XmlNodeList__ctor_m4_663 (XmlNodeList_t4_147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNode System.Xml.XmlNodeList::get_ItemOf(System.Int32)
extern "C" XmlNode_t4_116 * XmlNodeList_get_ItemOf_m4_664 (XmlNodeList_t4_147 * __this, int32_t ___i, const MethodInfo* method) IL2CPP_METHOD_ATTR;
