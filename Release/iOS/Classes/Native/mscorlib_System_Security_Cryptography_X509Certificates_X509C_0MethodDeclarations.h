﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.X509Certificates.X509Certificate
struct X509Certificate_t1_1179;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.String
struct String_t;
// System.Security.SecureString
struct SecureString_t1_1197;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_Security_Cryptography_X509Certificates_X509K.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Security_Cryptography_X509Certificates_X509C_1.h"

// System.Void System.Security.Cryptography.X509Certificates.X509Certificate::.ctor(System.Byte[],System.Boolean)
extern "C" void X509Certificate__ctor_m1_10100 (X509Certificate_t1_1179 * __this, ByteU5BU5D_t1_109* ___data, bool ___dates, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate::.ctor(System.Byte[])
extern "C" void X509Certificate__ctor_m1_10101 (X509Certificate_t1_1179 * __this, ByteU5BU5D_t1_109* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate::.ctor(System.IntPtr)
extern "C" void X509Certificate__ctor_m1_10102 (X509Certificate_t1_1179 * __this, IntPtr_t ___handle, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate::.ctor(System.Security.Cryptography.X509Certificates.X509Certificate)
extern "C" void X509Certificate__ctor_m1_10103 (X509Certificate_t1_1179 * __this, X509Certificate_t1_1179 * ___cert, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate::.ctor()
extern "C" void X509Certificate__ctor_m1_10104 (X509Certificate_t1_1179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate::.ctor(System.Byte[],System.String)
extern "C" void X509Certificate__ctor_m1_10105 (X509Certificate_t1_1179 * __this, ByteU5BU5D_t1_109* ___rawData, String_t* ___password, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate::.ctor(System.Byte[],System.Security.SecureString)
extern "C" void X509Certificate__ctor_m1_10106 (X509Certificate_t1_1179 * __this, ByteU5BU5D_t1_109* ___rawData, SecureString_t1_1197 * ___password, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate::.ctor(System.Byte[],System.String,System.Security.Cryptography.X509Certificates.X509KeyStorageFlags)
extern "C" void X509Certificate__ctor_m1_10107 (X509Certificate_t1_1179 * __this, ByteU5BU5D_t1_109* ___rawData, String_t* ___password, int32_t ___keyStorageFlags, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate::.ctor(System.Byte[],System.Security.SecureString,System.Security.Cryptography.X509Certificates.X509KeyStorageFlags)
extern "C" void X509Certificate__ctor_m1_10108 (X509Certificate_t1_1179 * __this, ByteU5BU5D_t1_109* ___rawData, SecureString_t1_1197 * ___password, int32_t ___keyStorageFlags, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate::.ctor(System.String)
extern "C" void X509Certificate__ctor_m1_10109 (X509Certificate_t1_1179 * __this, String_t* ___fileName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate::.ctor(System.String,System.String)
extern "C" void X509Certificate__ctor_m1_10110 (X509Certificate_t1_1179 * __this, String_t* ___fileName, String_t* ___password, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate::.ctor(System.String,System.Security.SecureString)
extern "C" void X509Certificate__ctor_m1_10111 (X509Certificate_t1_1179 * __this, String_t* ___fileName, SecureString_t1_1197 * ___password, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate::.ctor(System.String,System.String,System.Security.Cryptography.X509Certificates.X509KeyStorageFlags)
extern "C" void X509Certificate__ctor_m1_10112 (X509Certificate_t1_1179 * __this, String_t* ___fileName, String_t* ___password, int32_t ___keyStorageFlags, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate::.ctor(System.String,System.Security.SecureString,System.Security.Cryptography.X509Certificates.X509KeyStorageFlags)
extern "C" void X509Certificate__ctor_m1_10113 (X509Certificate_t1_1179 * __this, String_t* ___fileName, SecureString_t1_1197 * ___password, int32_t ___keyStorageFlags, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void X509Certificate__ctor_m1_10114 (X509Certificate_t1_1179 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate::System.Runtime.Serialization.IDeserializationCallback.OnDeserialization(System.Object)
extern "C" void X509Certificate_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m1_10115 (X509Certificate_t1_1179 * __this, Object_t * ___sender, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void X509Certificate_System_Runtime_Serialization_ISerializable_GetObjectData_m1_10116 (X509Certificate_t1_1179 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Cryptography.X509Certificates.X509Certificate::tostr(System.Byte[])
extern "C" String_t* X509Certificate_tostr_m1_10117 (X509Certificate_t1_1179 * __this, ByteU5BU5D_t1_109* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.X509Certificates.X509Certificate System.Security.Cryptography.X509Certificates.X509Certificate::CreateFromCertFile(System.String)
extern "C" X509Certificate_t1_1179 * X509Certificate_CreateFromCertFile_m1_10118 (Object_t * __this /* static, unused */, String_t* ___filename, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.X509Certificates.X509Certificate System.Security.Cryptography.X509Certificates.X509Certificate::CreateFromSignedFile(System.String)
extern "C" X509Certificate_t1_1179 * X509Certificate_CreateFromSignedFile_m1_10119 (Object_t * __this /* static, unused */, String_t* ___filename, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate::InitFromHandle(System.IntPtr)
extern "C" void X509Certificate_InitFromHandle_m1_10120 (X509Certificate_t1_1179 * __this, IntPtr_t ___handle, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.X509Certificates.X509Certificate::Equals(System.Security.Cryptography.X509Certificates.X509Certificate)
extern "C" bool X509Certificate_Equals_m1_10121 (X509Certificate_t1_1179 * __this, X509Certificate_t1_1179 * ___other, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate::GetCertHash()
extern "C" ByteU5BU5D_t1_109* X509Certificate_GetCertHash_m1_10122 (X509Certificate_t1_1179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Cryptography.X509Certificates.X509Certificate::GetCertHashString()
extern "C" String_t* X509Certificate_GetCertHashString_m1_10123 (X509Certificate_t1_1179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Cryptography.X509Certificates.X509Certificate::GetEffectiveDateString()
extern "C" String_t* X509Certificate_GetEffectiveDateString_m1_10124 (X509Certificate_t1_1179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Cryptography.X509Certificates.X509Certificate::GetExpirationDateString()
extern "C" String_t* X509Certificate_GetExpirationDateString_m1_10125 (X509Certificate_t1_1179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Cryptography.X509Certificates.X509Certificate::GetFormat()
extern "C" String_t* X509Certificate_GetFormat_m1_10126 (X509Certificate_t1_1179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Cryptography.X509Certificates.X509Certificate::GetHashCode()
extern "C" int32_t X509Certificate_GetHashCode_m1_10127 (X509Certificate_t1_1179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Cryptography.X509Certificates.X509Certificate::GetIssuerName()
extern "C" String_t* X509Certificate_GetIssuerName_m1_10128 (X509Certificate_t1_1179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Cryptography.X509Certificates.X509Certificate::GetKeyAlgorithm()
extern "C" String_t* X509Certificate_GetKeyAlgorithm_m1_10129 (X509Certificate_t1_1179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate::GetKeyAlgorithmParameters()
extern "C" ByteU5BU5D_t1_109* X509Certificate_GetKeyAlgorithmParameters_m1_10130 (X509Certificate_t1_1179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Cryptography.X509Certificates.X509Certificate::GetKeyAlgorithmParametersString()
extern "C" String_t* X509Certificate_GetKeyAlgorithmParametersString_m1_10131 (X509Certificate_t1_1179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Cryptography.X509Certificates.X509Certificate::GetName()
extern "C" String_t* X509Certificate_GetName_m1_10132 (X509Certificate_t1_1179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate::GetPublicKey()
extern "C" ByteU5BU5D_t1_109* X509Certificate_GetPublicKey_m1_10133 (X509Certificate_t1_1179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Cryptography.X509Certificates.X509Certificate::GetPublicKeyString()
extern "C" String_t* X509Certificate_GetPublicKeyString_m1_10134 (X509Certificate_t1_1179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate::GetRawCertData()
extern "C" ByteU5BU5D_t1_109* X509Certificate_GetRawCertData_m1_10135 (X509Certificate_t1_1179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Cryptography.X509Certificates.X509Certificate::GetRawCertDataString()
extern "C" String_t* X509Certificate_GetRawCertDataString_m1_10136 (X509Certificate_t1_1179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate::GetSerialNumber()
extern "C" ByteU5BU5D_t1_109* X509Certificate_GetSerialNumber_m1_10137 (X509Certificate_t1_1179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Cryptography.X509Certificates.X509Certificate::GetSerialNumberString()
extern "C" String_t* X509Certificate_GetSerialNumberString_m1_10138 (X509Certificate_t1_1179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Cryptography.X509Certificates.X509Certificate::ToString()
extern "C" String_t* X509Certificate_ToString_m1_10139 (X509Certificate_t1_1179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Cryptography.X509Certificates.X509Certificate::ToString(System.Boolean)
extern "C" String_t* X509Certificate_ToString_m1_10140 (X509Certificate_t1_1179 * __this, bool ___fVerbose, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate::Load(System.String)
extern "C" ByteU5BU5D_t1_109* X509Certificate_Load_m1_10141 (Object_t * __this /* static, unused */, String_t* ___fileName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Cryptography.X509Certificates.X509Certificate::get_Issuer()
extern "C" String_t* X509Certificate_get_Issuer_m1_10142 (X509Certificate_t1_1179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Cryptography.X509Certificates.X509Certificate::get_Subject()
extern "C" String_t* X509Certificate_get_Subject_m1_10143 (X509Certificate_t1_1179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Security.Cryptography.X509Certificates.X509Certificate::get_Handle()
extern "C" IntPtr_t X509Certificate_get_Handle_m1_10144 (X509Certificate_t1_1179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.X509Certificates.X509Certificate::Equals(System.Object)
extern "C" bool X509Certificate_Equals_m1_10145 (X509Certificate_t1_1179 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate::Export(System.Security.Cryptography.X509Certificates.X509ContentType)
extern "C" ByteU5BU5D_t1_109* X509Certificate_Export_m1_10146 (X509Certificate_t1_1179 * __this, int32_t ___contentType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate::Export(System.Security.Cryptography.X509Certificates.X509ContentType,System.String)
extern "C" ByteU5BU5D_t1_109* X509Certificate_Export_m1_10147 (X509Certificate_t1_1179 * __this, int32_t ___contentType, String_t* ___password, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate::Export(System.Security.Cryptography.X509Certificates.X509ContentType,System.Security.SecureString)
extern "C" ByteU5BU5D_t1_109* X509Certificate_Export_m1_10148 (X509Certificate_t1_1179 * __this, int32_t ___contentType, SecureString_t1_1197 * ___password, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate::Export(System.Security.Cryptography.X509Certificates.X509ContentType,System.Byte[])
extern "C" ByteU5BU5D_t1_109* X509Certificate_Export_m1_10149 (X509Certificate_t1_1179 * __this, int32_t ___contentType, ByteU5BU5D_t1_109* ___password, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate::Import(System.Byte[])
extern "C" void X509Certificate_Import_m1_10150 (X509Certificate_t1_1179 * __this, ByteU5BU5D_t1_109* ___rawData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate::Import(System.Byte[],System.String,System.Security.Cryptography.X509Certificates.X509KeyStorageFlags)
extern "C" void X509Certificate_Import_m1_10151 (X509Certificate_t1_1179 * __this, ByteU5BU5D_t1_109* ___rawData, String_t* ___password, int32_t ___keyStorageFlags, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate::Import(System.Byte[],System.Security.SecureString,System.Security.Cryptography.X509Certificates.X509KeyStorageFlags)
extern "C" void X509Certificate_Import_m1_10152 (X509Certificate_t1_1179 * __this, ByteU5BU5D_t1_109* ___rawData, SecureString_t1_1197 * ___password, int32_t ___keyStorageFlags, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate::Import(System.String)
extern "C" void X509Certificate_Import_m1_10153 (X509Certificate_t1_1179 * __this, String_t* ___fileName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate::Import(System.String,System.String,System.Security.Cryptography.X509Certificates.X509KeyStorageFlags)
extern "C" void X509Certificate_Import_m1_10154 (X509Certificate_t1_1179 * __this, String_t* ___fileName, String_t* ___password, int32_t ___keyStorageFlags, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate::Import(System.String,System.Security.SecureString,System.Security.Cryptography.X509Certificates.X509KeyStorageFlags)
extern "C" void X509Certificate_Import_m1_10155 (X509Certificate_t1_1179 * __this, String_t* ___fileName, SecureString_t1_1197 * ___password, int32_t ___keyStorageFlags, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.X509Certificates.X509Certificate::Reset()
extern "C" void X509Certificate_Reset_m1_10156 (X509Certificate_t1_1179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
