﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Security_Cryptography_FromBase64TransformMod.h"

// System.Security.Cryptography.FromBase64TransformMode
struct  FromBase64TransformMode_t1_1208 
{
	// System.Int32 System.Security.Cryptography.FromBase64TransformMode::value__
	int32_t ___value___1;
};
