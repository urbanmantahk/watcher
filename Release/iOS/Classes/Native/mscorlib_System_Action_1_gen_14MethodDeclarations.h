﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Action`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Action_1_t1_2196;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6.h"

// System.Void System.Action`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C" void Action_1__ctor_m1_17973_gshared (Action_1_t1_2196 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Action_1__ctor_m1_17973(__this, ___object, ___method, method) (( void (*) (Action_1_t1_2196 *, Object_t *, IntPtr_t, const MethodInfo*))Action_1__ctor_m1_17973_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Invoke(T)
extern "C" void Action_1_Invoke_m1_17974_gshared (Action_1_t1_2196 * __this, KeyValuePair_2_t1_2015  ___obj, const MethodInfo* method);
#define Action_1_Invoke_m1_17974(__this, ___obj, method) (( void (*) (Action_1_t1_2196 *, KeyValuePair_2_t1_2015 , const MethodInfo*))Action_1_Invoke_m1_17974_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Action`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * Action_1_BeginInvoke_m1_17975_gshared (Action_1_t1_2196 * __this, KeyValuePair_2_t1_2015  ___obj, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Action_1_BeginInvoke_m1_17975(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Action_1_t1_2196 *, KeyValuePair_2_t1_2015 , AsyncCallback_t1_28 *, Object_t *, const MethodInfo*))Action_1_BeginInvoke_m1_17975_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Void System.Action`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C" void Action_1_EndInvoke_m1_17976_gshared (Action_1_t1_2196 * __this, Object_t * ___result, const MethodInfo* method);
#define Action_1_EndInvoke_m1_17976(__this, ___result, method) (( void (*) (Action_1_t1_2196 *, Object_t *, const MethodInfo*))Action_1_EndInvoke_m1_17976_gshared)(__this, ___result, method)
