﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "mscorlib_System_ValueType.h"

// ExifLibrary.ExifInterOperability
struct  ExifInterOperability_t8_113 
{
	// System.UInt16 ExifLibrary.ExifInterOperability::mTagID
	uint16_t ___mTagID_0;
	// System.UInt16 ExifLibrary.ExifInterOperability::mTypeID
	uint16_t ___mTypeID_1;
	// System.UInt32 ExifLibrary.ExifInterOperability::mCount
	uint32_t ___mCount_2;
	// System.Byte[] ExifLibrary.ExifInterOperability::mData
	ByteU5BU5D_t1_109* ___mData_3;
};
// Native definition for marshalling of: ExifLibrary.ExifInterOperability
struct ExifInterOperability_t8_113_marshaled
{
	uint16_t ___mTagID_0;
	uint16_t ___mTypeID_1;
	uint32_t ___mCount_2;
	uint8_t* ___mData_3;
};
