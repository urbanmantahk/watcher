﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.Utility.ShaderUtility
struct ShaderUtility_t8_29;
// System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>
struct List_1_t1_1901;
// VoxelBusters.Utility.ShaderUtility/ShaderInfo
struct ShaderInfo_t8_26;
// UnityEngine.Material
struct Material_t6_72;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.Utility.ShaderUtility::.ctor()
extern "C" void ShaderUtility__ctor_m8_176 (ShaderUtility_t8_29 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo> VoxelBusters.Utility.ShaderUtility::get_ShaderInfoList()
extern "C" List_1_t1_1901 * ShaderUtility_get_ShaderInfoList_m8_177 (ShaderUtility_t8_29 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.ShaderUtility::set_ShaderInfoList(System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>)
extern "C" void ShaderUtility_set_ShaderInfoList_m8_178 (ShaderUtility_t8_29 * __this, List_1_t1_1901 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.ShaderUtility::OnEnable()
extern "C" void ShaderUtility_OnEnable_m8_179 (ShaderUtility_t8_29 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.Utility.ShaderUtility/ShaderInfo VoxelBusters.Utility.ShaderUtility::GetShaderInfo(UnityEngine.Material)
extern "C" ShaderInfo_t8_26 * ShaderUtility_GetShaderInfo_m8_180 (ShaderUtility_t8_29 * __this, Material_t6_72 * ____material, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.Utility.ShaderUtility/ShaderInfo VoxelBusters.Utility.ShaderUtility::GetShaderInfo(System.String)
extern "C" ShaderInfo_t8_26 * ShaderUtility_GetShaderInfo_m8_181 (ShaderUtility_t8_29 * __this, String_t* ____shaderName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.ShaderUtility::ReloadShaderUtility()
extern "C" void ShaderUtility_ReloadShaderUtility_m8_182 (ShaderUtility_t8_29 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
