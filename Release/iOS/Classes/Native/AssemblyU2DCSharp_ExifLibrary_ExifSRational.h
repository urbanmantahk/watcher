﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_ExifLibrary_ExifProperty.h"
#include "AssemblyU2DCSharp_ExifLibrary_MathEx_Fraction32.h"

// ExifLibrary.ExifSRational
struct  ExifSRational_t8_126  : public ExifProperty_t8_99
{
	// ExifLibrary.MathEx/Fraction32 ExifLibrary.ExifSRational::mValue
	Fraction32_t8_127  ___mValue_3;
};
