﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Security_Permissions_CodeAccessSecurityAttri.h"

// System.Security.Permissions.UrlIdentityPermissionAttribute
struct  UrlIdentityPermissionAttribute_t1_1322  : public CodeAccessSecurityAttribute_t1_1266
{
	// System.String System.Security.Permissions.UrlIdentityPermissionAttribute::url
	String_t* ___url_2;
};
