﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.Utility.WebRequest/JSONResponse
struct JSONResponse_t8_157;
// System.Object
struct Object_t;
// System.Collections.IDictionary
struct IDictionary_t1_35;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"

// System.Void VoxelBusters.Utility.WebRequest/JSONResponse::.ctor(System.Object,System.IntPtr)
extern "C" void JSONResponse__ctor_m8_903 (JSONResponse_t8_157 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.WebRequest/JSONResponse::Invoke(System.Collections.IDictionary)
extern "C" void JSONResponse_Invoke_m8_904 (JSONResponse_t8_157 * __this, Object_t * ____response, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_JSONResponse_t8_157(Il2CppObject* delegate, Object_t * ____response);
// System.IAsyncResult VoxelBusters.Utility.WebRequest/JSONResponse::BeginInvoke(System.Collections.IDictionary,System.AsyncCallback,System.Object)
extern "C" Object_t * JSONResponse_BeginInvoke_m8_905 (JSONResponse_t8_157 * __this, Object_t * ____response, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.WebRequest/JSONResponse::EndInvoke(System.IAsyncResult)
extern "C" void JSONResponse_EndInvoke_m8_906 (JSONResponse_t8_157 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
