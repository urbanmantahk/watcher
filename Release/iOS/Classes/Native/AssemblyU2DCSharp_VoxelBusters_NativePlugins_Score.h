﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VoxelBusters.NativePlugins.Score/ReportScoreCompletion
struct ReportScoreCompletion_t8_232;
// System.String
struct String_t;

#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_NPObje.h"

// VoxelBusters.NativePlugins.Score
struct  Score_t8_231  : public NPObject_t8_222
{
	// VoxelBusters.NativePlugins.Score/ReportScoreCompletion VoxelBusters.NativePlugins.Score::ReportScoreFinishedEvent
	ReportScoreCompletion_t8_232 * ___ReportScoreFinishedEvent_1;
	// System.String VoxelBusters.NativePlugins.Score::<LeaderboardGlobalID>k__BackingField
	String_t* ___U3CLeaderboardGlobalIDU3Ek__BackingField_2;
};
