﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.UI.InputField
struct InputField_t7_98;
// UnityEngine.GameObject
struct GameObject_t6_97;
// UnityEngine.UI.Text
struct Text_t7_63;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// LoginControl
struct  LoginControl_t8_11  : public MonoBehaviour_t6_91
{
	// System.String LoginControl::url_dev
	String_t* ___url_dev_2;
	// UnityEngine.UI.InputField LoginControl::phone
	InputField_t7_98 * ___phone_3;
	// UnityEngine.UI.InputField LoginControl::password
	InputField_t7_98 * ___password_4;
	// UnityEngine.GameObject LoginControl::dialog_error
	GameObject_t6_97 * ___dialog_error_5;
	// UnityEngine.UI.Text LoginControl::error_msg
	Text_t7_63 * ___error_msg_6;
};
