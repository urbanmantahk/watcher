﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[]
struct Int32U5BU5D_t1_275;

#include "AssemblyU2DCSharp_ExifLibrary_ExifProperty.h"

// ExifLibrary.ExifSIntArray
struct  ExifSIntArray_t8_125  : public ExifProperty_t8_99
{
	// System.Int32[] ExifLibrary.ExifSIntArray::mValue
	Int32U5BU5D_t1_275* ___mValue_3;
};
