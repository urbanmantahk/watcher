﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__4MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,VoxelBusters.NativePlugins.Internal.NPObject>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1_27930(__this, ___dictionary, method) (( void (*) (Enumerator_t1_2767 *, Dictionary_2_t1_1913 *, const MethodInfo*))Enumerator__ctor_m1_15990_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,VoxelBusters.NativePlugins.Internal.NPObject>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_27931(__this, method) (( Object_t * (*) (Enumerator_t1_2767 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_15991_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,VoxelBusters.NativePlugins.Internal.NPObject>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1_27932(__this, method) (( void (*) (Enumerator_t1_2767 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_15992_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,VoxelBusters.NativePlugins.Internal.NPObject>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_27933(__this, method) (( DictionaryEntry_t1_284  (*) (Enumerator_t1_2767 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_15993_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,VoxelBusters.NativePlugins.Internal.NPObject>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_27934(__this, method) (( Object_t * (*) (Enumerator_t1_2767 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_15994_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,VoxelBusters.NativePlugins.Internal.NPObject>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_27935(__this, method) (( Object_t * (*) (Enumerator_t1_2767 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_15995_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,VoxelBusters.NativePlugins.Internal.NPObject>::MoveNext()
#define Enumerator_MoveNext_m1_27936(__this, method) (( bool (*) (Enumerator_t1_2767 *, const MethodInfo*))Enumerator_MoveNext_m1_15996_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,VoxelBusters.NativePlugins.Internal.NPObject>::get_Current()
#define Enumerator_get_Current_m1_27937(__this, method) (( KeyValuePair_2_t1_2764  (*) (Enumerator_t1_2767 *, const MethodInfo*))Enumerator_get_Current_m1_15997_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,VoxelBusters.NativePlugins.Internal.NPObject>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1_27938(__this, method) (( String_t* (*) (Enumerator_t1_2767 *, const MethodInfo*))Enumerator_get_CurrentKey_m1_15998_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,VoxelBusters.NativePlugins.Internal.NPObject>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1_27939(__this, method) (( NPObject_t8_222 * (*) (Enumerator_t1_2767 *, const MethodInfo*))Enumerator_get_CurrentValue_m1_15999_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,VoxelBusters.NativePlugins.Internal.NPObject>::Reset()
#define Enumerator_Reset_m1_27940(__this, method) (( void (*) (Enumerator_t1_2767 *, const MethodInfo*))Enumerator_Reset_m1_16000_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,VoxelBusters.NativePlugins.Internal.NPObject>::VerifyState()
#define Enumerator_VerifyState_m1_27941(__this, method) (( void (*) (Enumerator_t1_2767 *, const MethodInfo*))Enumerator_VerifyState_m1_16001_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,VoxelBusters.NativePlugins.Internal.NPObject>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1_27942(__this, method) (( void (*) (Enumerator_t1_2767 *, const MethodInfo*))Enumerator_VerifyCurrent_m1_16002_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,VoxelBusters.NativePlugins.Internal.NPObject>::Dispose()
#define Enumerator_Dispose_m1_27943(__this, method) (( void (*) (Enumerator_t1_2767 *, const MethodInfo*))Enumerator_Dispose_m1_16003_gshared)(__this, method)
