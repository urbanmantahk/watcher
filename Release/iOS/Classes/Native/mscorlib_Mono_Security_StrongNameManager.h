﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Hashtable
struct Hashtable_t1_100;

#include "mscorlib_System_Object.h"

// Mono.Security.StrongNameManager
struct  StrongNameManager_t1_234  : public Object_t
{
};
struct StrongNameManager_t1_234_StaticFields{
	// System.Collections.Hashtable Mono.Security.StrongNameManager::mappings
	Hashtable_t1_100 * ___mappings_0;
	// System.Collections.Hashtable Mono.Security.StrongNameManager::tokens
	Hashtable_t1_100 * ___tokens_1;
};
