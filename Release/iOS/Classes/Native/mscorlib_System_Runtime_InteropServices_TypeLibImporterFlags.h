﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Runtime_InteropServices_TypeLibImporterFlags.h"

// System.Runtime.InteropServices.TypeLibImporterFlags
struct  TypeLibImporterFlags_t1_836 
{
	// System.Int32 System.Runtime.InteropServices.TypeLibImporterFlags::value__
	int32_t ___value___1;
};
