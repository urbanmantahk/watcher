﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_DtdInputState.h"

// Mono.Xml2.XmlTextReader/DtdInputState
struct  DtdInputState_t4_172 
{
	// System.Int32 Mono.Xml2.XmlTextReader/DtdInputState::value__
	int32_t ___value___1;
};
