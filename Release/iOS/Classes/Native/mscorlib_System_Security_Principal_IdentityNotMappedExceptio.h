﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Security.Principal.IdentityReferenceCollection
struct IdentityReferenceCollection_t1_1376;

#include "mscorlib_System_SystemException.h"

// System.Security.Principal.IdentityNotMappedException
struct  IdentityNotMappedException_t1_1375  : public SystemException_t1_250
{
	// System.Security.Principal.IdentityReferenceCollection System.Security.Principal.IdentityNotMappedException::_coll
	IdentityReferenceCollection_t1_1376 * ____coll_12;
};
