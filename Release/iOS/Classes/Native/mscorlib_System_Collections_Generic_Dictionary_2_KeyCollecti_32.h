﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,VoxelBusters.NativePlugins.Internal.NPObject>
struct Dictionary_2_t1_1913;

#include "mscorlib_System_Object.h"

// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,VoxelBusters.NativePlugins.Internal.NPObject>
struct  KeyCollection_t1_2765  : public Object_t
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection::dictionary
	Dictionary_2_t1_1913 * ___dictionary_0;
};
