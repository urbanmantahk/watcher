﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Char[]
struct CharU5BU5D_t1_16;
// System.Collections.ArrayList
struct ArrayList_t1_170;

#include "mscorlib_System_Security_CodeAccessPermission.h"
#include "mscorlib_System_Security_Permissions_FileIOPermissionAccess.h"

// System.Security.Permissions.FileIOPermission
struct  FileIOPermission_t1_1274  : public CodeAccessPermission_t1_1268
{
	// System.Boolean System.Security.Permissions.FileIOPermission::m_Unrestricted
	bool ___m_Unrestricted_3;
	// System.Security.Permissions.FileIOPermissionAccess System.Security.Permissions.FileIOPermission::m_AllFilesAccess
	int32_t ___m_AllFilesAccess_4;
	// System.Security.Permissions.FileIOPermissionAccess System.Security.Permissions.FileIOPermission::m_AllLocalFilesAccess
	int32_t ___m_AllLocalFilesAccess_5;
	// System.Collections.ArrayList System.Security.Permissions.FileIOPermission::readList
	ArrayList_t1_170 * ___readList_6;
	// System.Collections.ArrayList System.Security.Permissions.FileIOPermission::writeList
	ArrayList_t1_170 * ___writeList_7;
	// System.Collections.ArrayList System.Security.Permissions.FileIOPermission::appendList
	ArrayList_t1_170 * ___appendList_8;
	// System.Collections.ArrayList System.Security.Permissions.FileIOPermission::pathList
	ArrayList_t1_170 * ___pathList_9;
};
struct FileIOPermission_t1_1274_StaticFields{
	// System.Char[] System.Security.Permissions.FileIOPermission::BadPathNameCharacters
	CharU5BU5D_t1_16* ___BadPathNameCharacters_1;
	// System.Char[] System.Security.Permissions.FileIOPermission::BadFileNameCharacters
	CharU5BU5D_t1_16* ___BadFileNameCharacters_2;
};
