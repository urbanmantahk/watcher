﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Mono.Security.PKCS7/ContentInfo
struct ContentInfo_t1_222;
// System.Collections.ArrayList
struct ArrayList_t1_170;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "mscorlib_System_Object.h"

// Mono.Security.PKCS7/EnvelopedData
struct  EnvelopedData_t1_224  : public Object_t
{
	// System.Byte Mono.Security.PKCS7/EnvelopedData::_version
	uint8_t ____version_0;
	// Mono.Security.PKCS7/ContentInfo Mono.Security.PKCS7/EnvelopedData::_content
	ContentInfo_t1_222 * ____content_1;
	// Mono.Security.PKCS7/ContentInfo Mono.Security.PKCS7/EnvelopedData::_encryptionAlgorithm
	ContentInfo_t1_222 * ____encryptionAlgorithm_2;
	// System.Collections.ArrayList Mono.Security.PKCS7/EnvelopedData::_recipientInfos
	ArrayList_t1_170 * ____recipientInfos_3;
	// System.Byte[] Mono.Security.PKCS7/EnvelopedData::_encrypted
	ByteU5BU5D_t1_109* ____encrypted_4;
};
