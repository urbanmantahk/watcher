﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Exception.h"

// ExifLibrary.NotValidJPEGFileException
struct  NotValidJPEGFileException_t8_134  : public Exception_t1_33
{
};
