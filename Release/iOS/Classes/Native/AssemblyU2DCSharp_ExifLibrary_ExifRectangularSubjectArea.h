﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_ExifLibrary_ExifPointSubjectArea.h"

// ExifLibrary.ExifRectangularSubjectArea
struct  ExifRectangularSubjectArea_t8_105  : public ExifPointSubjectArea_t8_102
{
};
