﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object.h"

// VoxelBusters.NativePlugins.ApplicationSettings/iOSSettings
struct  iOSSettings_t8_319  : public Object_t
{
	// System.String VoxelBusters.NativePlugins.ApplicationSettings/iOSSettings::m_storeIdentifier
	String_t* ___m_storeIdentifier_0;
};
