﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_38.h"

// System.Void System.Array/InternalEnumerator`1<System.Boolean>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_16061_gshared (InternalEnumerator_1_t1_2030 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1_16061(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1_2030 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m1_16061_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Boolean>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_16062_gshared (InternalEnumerator_1_t1_2030 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_16062(__this, method) (( void (*) (InternalEnumerator_1_t1_2030 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_16062_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Boolean>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_16063_gshared (InternalEnumerator_1_t1_2030 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_16063(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1_2030 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_16063_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Boolean>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_16064_gshared (InternalEnumerator_1_t1_2030 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1_16064(__this, method) (( void (*) (InternalEnumerator_1_t1_2030 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1_16064_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Boolean>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_16065_gshared (InternalEnumerator_1_t1_2030 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1_16065(__this, method) (( bool (*) (InternalEnumerator_1_t1_2030 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1_16065_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Boolean>::get_Current()
extern "C" bool InternalEnumerator_1_get_Current_m1_16066_gshared (InternalEnumerator_1_t1_2030 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1_16066(__this, method) (( bool (*) (InternalEnumerator_1_t1_2030 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1_16066_gshared)(__this, method)
