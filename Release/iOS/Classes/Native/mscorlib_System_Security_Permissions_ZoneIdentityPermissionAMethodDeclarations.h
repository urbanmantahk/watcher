﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Permissions.ZoneIdentityPermissionAttribute
struct ZoneIdentityPermissionAttribute_t1_1324;
// System.Security.IPermission
struct IPermission_t1_1400;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Permissions_SecurityAction.h"
#include "mscorlib_System_Security_SecurityZone.h"

// System.Void System.Security.Permissions.ZoneIdentityPermissionAttribute::.ctor(System.Security.Permissions.SecurityAction)
extern "C" void ZoneIdentityPermissionAttribute__ctor_m1_11310 (ZoneIdentityPermissionAttribute_t1_1324 * __this, int32_t ___action, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityZone System.Security.Permissions.ZoneIdentityPermissionAttribute::get_Zone()
extern "C" int32_t ZoneIdentityPermissionAttribute_get_Zone_m1_11311 (ZoneIdentityPermissionAttribute_t1_1324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.ZoneIdentityPermissionAttribute::set_Zone(System.Security.SecurityZone)
extern "C" void ZoneIdentityPermissionAttribute_set_Zone_m1_11312 (ZoneIdentityPermissionAttribute_t1_1324 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.ZoneIdentityPermissionAttribute::CreatePermission()
extern "C" Object_t * ZoneIdentityPermissionAttribute_CreatePermission_m1_11313 (ZoneIdentityPermissionAttribute_t1_1324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
