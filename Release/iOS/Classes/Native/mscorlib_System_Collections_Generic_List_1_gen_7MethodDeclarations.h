﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1_1834;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Vector3>
struct IEnumerable_1_t1_2794;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t6_161;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector3>
struct IEnumerator_1_t1_2795;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<UnityEngine.Vector3>
struct ICollection_1_t1_2796;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>
struct ReadOnlyCollection_1_t1_2269;
// System.Collections.Generic.IComparer`1<UnityEngine.Vector3>
struct IComparer_1_t1_2797;
// System.Predicate`1<UnityEngine.Vector3>
struct Predicate_1_t1_2276;
// System.Action`1<UnityEngine.Vector3>
struct Action_1_t1_2277;
// System.Comparison`1<UnityEngine.Vector3>
struct Comparison_1_t1_2278;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector3.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_15.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor()
extern "C" void List_1__ctor_m1_18764_gshared (List_1_t1_1834 * __this, const MethodInfo* method);
#define List_1__ctor_m1_18764(__this, method) (( void (*) (List_1_t1_1834 *, const MethodInfo*))List_1__ctor_m1_18764_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m1_18765_gshared (List_1_t1_1834 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m1_18765(__this, ___collection, method) (( void (*) (List_1_t1_1834 *, Object_t*, const MethodInfo*))List_1__ctor_m1_18765_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor(System.Int32)
extern "C" void List_1__ctor_m1_18766_gshared (List_1_t1_1834 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m1_18766(__this, ___capacity, method) (( void (*) (List_1_t1_1834 *, int32_t, const MethodInfo*))List_1__ctor_m1_18766_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor(T[],System.Int32)
extern "C" void List_1__ctor_m1_18767_gshared (List_1_t1_1834 * __this, Vector3U5BU5D_t6_161* ___data, int32_t ___size, const MethodInfo* method);
#define List_1__ctor_m1_18767(__this, ___data, ___size, method) (( void (*) (List_1_t1_1834 *, Vector3U5BU5D_t6_161*, int32_t, const MethodInfo*))List_1__ctor_m1_18767_gshared)(__this, ___data, ___size, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.cctor()
extern "C" void List_1__cctor_m1_18768_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m1_18768(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m1_18768_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_18769_gshared (List_1_t1_1834 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_18769(__this, method) (( Object_t* (*) (List_1_t1_1834 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_18769_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m1_18770_gshared (List_1_t1_1834 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m1_18770(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_1834 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m1_18770_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m1_18771_gshared (List_1_t1_1834 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1_18771(__this, method) (( Object_t * (*) (List_1_t1_1834 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1_18771_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m1_18772_gshared (List_1_t1_1834 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m1_18772(__this, ___item, method) (( int32_t (*) (List_1_t1_1834 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m1_18772_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m1_18773_gshared (List_1_t1_1834 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m1_18773(__this, ___item, method) (( bool (*) (List_1_t1_1834 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1_18773_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m1_18774_gshared (List_1_t1_1834 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m1_18774(__this, ___item, method) (( int32_t (*) (List_1_t1_1834 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m1_18774_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m1_18775_gshared (List_1_t1_1834 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m1_18775(__this, ___index, ___item, method) (( void (*) (List_1_t1_1834 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m1_18775_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m1_18776_gshared (List_1_t1_1834 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m1_18776(__this, ___item, method) (( void (*) (List_1_t1_1834 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1_18776_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_18777_gshared (List_1_t1_1834 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_18777(__this, method) (( bool (*) (List_1_t1_1834 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_18777_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m1_18778_gshared (List_1_t1_1834 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m1_18778(__this, method) (( bool (*) (List_1_t1_1834 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m1_18778_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m1_18779_gshared (List_1_t1_1834 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m1_18779(__this, method) (( Object_t * (*) (List_1_t1_1834 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1_18779_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m1_18780_gshared (List_1_t1_1834 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m1_18780(__this, method) (( bool (*) (List_1_t1_1834 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m1_18780_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m1_18781_gshared (List_1_t1_1834 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m1_18781(__this, method) (( bool (*) (List_1_t1_1834 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m1_18781_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m1_18782_gshared (List_1_t1_1834 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m1_18782(__this, ___index, method) (( Object_t * (*) (List_1_t1_1834 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m1_18782_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m1_18783_gshared (List_1_t1_1834 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m1_18783(__this, ___index, ___value, method) (( void (*) (List_1_t1_1834 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m1_18783_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(T)
extern "C" void List_1_Add_m1_18784_gshared (List_1_t1_1834 * __this, Vector3_t6_48  ___item, const MethodInfo* method);
#define List_1_Add_m1_18784(__this, ___item, method) (( void (*) (List_1_t1_1834 *, Vector3_t6_48 , const MethodInfo*))List_1_Add_m1_18784_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m1_18785_gshared (List_1_t1_1834 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m1_18785(__this, ___newCount, method) (( void (*) (List_1_t1_1834 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m1_18785_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::CheckRange(System.Int32,System.Int32)
extern "C" void List_1_CheckRange_m1_18786_gshared (List_1_t1_1834 * __this, int32_t ___idx, int32_t ___count, const MethodInfo* method);
#define List_1_CheckRange_m1_18786(__this, ___idx, ___count, method) (( void (*) (List_1_t1_1834 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m1_18786_gshared)(__this, ___idx, ___count, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m1_18787_gshared (List_1_t1_1834 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m1_18787(__this, ___collection, method) (( void (*) (List_1_t1_1834 *, Object_t*, const MethodInfo*))List_1_AddCollection_m1_18787_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m1_18788_gshared (List_1_t1_1834 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m1_18788(__this, ___enumerable, method) (( void (*) (List_1_t1_1834 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m1_18788_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m1_15019_gshared (List_1_t1_1834 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m1_15019(__this, ___collection, method) (( void (*) (List_1_t1_1834 *, Object_t*, const MethodInfo*))List_1_AddRange_m1_15019_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.Vector3>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t1_2269 * List_1_AsReadOnly_m1_18789_gshared (List_1_t1_1834 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m1_18789(__this, method) (( ReadOnlyCollection_1_t1_2269 * (*) (List_1_t1_1834 *, const MethodInfo*))List_1_AsReadOnly_m1_18789_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::BinarySearch(T)
extern "C" int32_t List_1_BinarySearch_m1_18790_gshared (List_1_t1_1834 * __this, Vector3_t6_48  ___item, const MethodInfo* method);
#define List_1_BinarySearch_m1_18790(__this, ___item, method) (( int32_t (*) (List_1_t1_1834 *, Vector3_t6_48 , const MethodInfo*))List_1_BinarySearch_m1_18790_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::BinarySearch(T,System.Collections.Generic.IComparer`1<T>)
extern "C" int32_t List_1_BinarySearch_m1_18791_gshared (List_1_t1_1834 * __this, Vector3_t6_48  ___item, Object_t* ___comparer, const MethodInfo* method);
#define List_1_BinarySearch_m1_18791(__this, ___item, ___comparer, method) (( int32_t (*) (List_1_t1_1834 *, Vector3_t6_48 , Object_t*, const MethodInfo*))List_1_BinarySearch_m1_18791_gshared)(__this, ___item, ___comparer, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::BinarySearch(System.Int32,System.Int32,T,System.Collections.Generic.IComparer`1<T>)
extern "C" int32_t List_1_BinarySearch_m1_18792_gshared (List_1_t1_1834 * __this, int32_t ___index, int32_t ___count, Vector3_t6_48  ___item, Object_t* ___comparer, const MethodInfo* method);
#define List_1_BinarySearch_m1_18792(__this, ___index, ___count, ___item, ___comparer, method) (( int32_t (*) (List_1_t1_1834 *, int32_t, int32_t, Vector3_t6_48 , Object_t*, const MethodInfo*))List_1_BinarySearch_m1_18792_gshared)(__this, ___index, ___count, ___item, ___comparer, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Clear()
extern "C" void List_1_Clear_m1_18793_gshared (List_1_t1_1834 * __this, const MethodInfo* method);
#define List_1_Clear_m1_18793(__this, method) (( void (*) (List_1_t1_1834 *, const MethodInfo*))List_1_Clear_m1_18793_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector3>::Contains(T)
extern "C" bool List_1_Contains_m1_18794_gshared (List_1_t1_1834 * __this, Vector3_t6_48  ___item, const MethodInfo* method);
#define List_1_Contains_m1_18794(__this, ___item, method) (( bool (*) (List_1_t1_1834 *, Vector3_t6_48 , const MethodInfo*))List_1_Contains_m1_18794_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::CopyTo(T[])
extern "C" void List_1_CopyTo_m1_18795_gshared (List_1_t1_1834 * __this, Vector3U5BU5D_t6_161* ___array, const MethodInfo* method);
#define List_1_CopyTo_m1_18795(__this, ___array, method) (( void (*) (List_1_t1_1834 *, Vector3U5BU5D_t6_161*, const MethodInfo*))List_1_CopyTo_m1_18795_gshared)(__this, ___array, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m1_18796_gshared (List_1_t1_1834 * __this, Vector3U5BU5D_t6_161* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m1_18796(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_1834 *, Vector3U5BU5D_t6_161*, int32_t, const MethodInfo*))List_1_CopyTo_m1_18796_gshared)(__this, ___array, ___arrayIndex, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::CopyTo(System.Int32,T[],System.Int32,System.Int32)
extern "C" void List_1_CopyTo_m1_18797_gshared (List_1_t1_1834 * __this, int32_t ___index, Vector3U5BU5D_t6_161* ___array, int32_t ___arrayIndex, int32_t ___count, const MethodInfo* method);
#define List_1_CopyTo_m1_18797(__this, ___index, ___array, ___arrayIndex, ___count, method) (( void (*) (List_1_t1_1834 *, int32_t, Vector3U5BU5D_t6_161*, int32_t, int32_t, const MethodInfo*))List_1_CopyTo_m1_18797_gshared)(__this, ___index, ___array, ___arrayIndex, ___count, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector3>::Exists(System.Predicate`1<T>)
extern "C" bool List_1_Exists_m1_18798_gshared (List_1_t1_1834 * __this, Predicate_1_t1_2276 * ___match, const MethodInfo* method);
#define List_1_Exists_m1_18798(__this, ___match, method) (( bool (*) (List_1_t1_1834 *, Predicate_1_t1_2276 *, const MethodInfo*))List_1_Exists_m1_18798_gshared)(__this, ___match, method)
// T System.Collections.Generic.List`1<UnityEngine.Vector3>::Find(System.Predicate`1<T>)
extern "C" Vector3_t6_48  List_1_Find_m1_18799_gshared (List_1_t1_1834 * __this, Predicate_1_t1_2276 * ___match, const MethodInfo* method);
#define List_1_Find_m1_18799(__this, ___match, method) (( Vector3_t6_48  (*) (List_1_t1_1834 *, Predicate_1_t1_2276 *, const MethodInfo*))List_1_Find_m1_18799_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m1_18800_gshared (Object_t * __this /* static, unused */, Predicate_1_t1_2276 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m1_18800(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t1_2276 *, const MethodInfo*))List_1_CheckMatch_m1_18800_gshared)(__this /* static, unused */, ___match, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<UnityEngine.Vector3>::FindAll(System.Predicate`1<T>)
extern "C" List_1_t1_1834 * List_1_FindAll_m1_18801_gshared (List_1_t1_1834 * __this, Predicate_1_t1_2276 * ___match, const MethodInfo* method);
#define List_1_FindAll_m1_18801(__this, ___match, method) (( List_1_t1_1834 * (*) (List_1_t1_1834 *, Predicate_1_t1_2276 *, const MethodInfo*))List_1_FindAll_m1_18801_gshared)(__this, ___match, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<UnityEngine.Vector3>::FindAllStackBits(System.Predicate`1<T>)
extern "C" List_1_t1_1834 * List_1_FindAllStackBits_m1_18802_gshared (List_1_t1_1834 * __this, Predicate_1_t1_2276 * ___match, const MethodInfo* method);
#define List_1_FindAllStackBits_m1_18802(__this, ___match, method) (( List_1_t1_1834 * (*) (List_1_t1_1834 *, Predicate_1_t1_2276 *, const MethodInfo*))List_1_FindAllStackBits_m1_18802_gshared)(__this, ___match, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<UnityEngine.Vector3>::FindAllList(System.Predicate`1<T>)
extern "C" List_1_t1_1834 * List_1_FindAllList_m1_18803_gshared (List_1_t1_1834 * __this, Predicate_1_t1_2276 * ___match, const MethodInfo* method);
#define List_1_FindAllList_m1_18803(__this, ___match, method) (( List_1_t1_1834 * (*) (List_1_t1_1834 *, Predicate_1_t1_2276 *, const MethodInfo*))List_1_FindAllList_m1_18803_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::FindIndex(System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m1_18804_gshared (List_1_t1_1834 * __this, Predicate_1_t1_2276 * ___match, const MethodInfo* method);
#define List_1_FindIndex_m1_18804(__this, ___match, method) (( int32_t (*) (List_1_t1_1834 *, Predicate_1_t1_2276 *, const MethodInfo*))List_1_FindIndex_m1_18804_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::FindIndex(System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m1_18805_gshared (List_1_t1_1834 * __this, int32_t ___startIndex, Predicate_1_t1_2276 * ___match, const MethodInfo* method);
#define List_1_FindIndex_m1_18805(__this, ___startIndex, ___match, method) (( int32_t (*) (List_1_t1_1834 *, int32_t, Predicate_1_t1_2276 *, const MethodInfo*))List_1_FindIndex_m1_18805_gshared)(__this, ___startIndex, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::FindIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m1_18806_gshared (List_1_t1_1834 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_2276 * ___match, const MethodInfo* method);
#define List_1_FindIndex_m1_18806(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_1834 *, int32_t, int32_t, Predicate_1_t1_2276 *, const MethodInfo*))List_1_FindIndex_m1_18806_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m1_18807_gshared (List_1_t1_1834 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_2276 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m1_18807(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_1834 *, int32_t, int32_t, Predicate_1_t1_2276 *, const MethodInfo*))List_1_GetIndex_m1_18807_gshared)(__this, ___startIndex, ___count, ___match, method)
// T System.Collections.Generic.List`1<UnityEngine.Vector3>::FindLast(System.Predicate`1<T>)
extern "C" Vector3_t6_48  List_1_FindLast_m1_18808_gshared (List_1_t1_1834 * __this, Predicate_1_t1_2276 * ___match, const MethodInfo* method);
#define List_1_FindLast_m1_18808(__this, ___match, method) (( Vector3_t6_48  (*) (List_1_t1_1834 *, Predicate_1_t1_2276 *, const MethodInfo*))List_1_FindLast_m1_18808_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::FindLastIndex(System.Predicate`1<T>)
extern "C" int32_t List_1_FindLastIndex_m1_18809_gshared (List_1_t1_1834 * __this, Predicate_1_t1_2276 * ___match, const MethodInfo* method);
#define List_1_FindLastIndex_m1_18809(__this, ___match, method) (( int32_t (*) (List_1_t1_1834 *, Predicate_1_t1_2276 *, const MethodInfo*))List_1_FindLastIndex_m1_18809_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::FindLastIndex(System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindLastIndex_m1_18810_gshared (List_1_t1_1834 * __this, int32_t ___startIndex, Predicate_1_t1_2276 * ___match, const MethodInfo* method);
#define List_1_FindLastIndex_m1_18810(__this, ___startIndex, ___match, method) (( int32_t (*) (List_1_t1_1834 *, int32_t, Predicate_1_t1_2276 *, const MethodInfo*))List_1_FindLastIndex_m1_18810_gshared)(__this, ___startIndex, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::FindLastIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindLastIndex_m1_18811_gshared (List_1_t1_1834 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_2276 * ___match, const MethodInfo* method);
#define List_1_FindLastIndex_m1_18811(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_1834 *, int32_t, int32_t, Predicate_1_t1_2276 *, const MethodInfo*))List_1_FindLastIndex_m1_18811_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::GetLastIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetLastIndex_m1_18812_gshared (List_1_t1_1834 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_2276 * ___match, const MethodInfo* method);
#define List_1_GetLastIndex_m1_18812(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_1834 *, int32_t, int32_t, Predicate_1_t1_2276 *, const MethodInfo*))List_1_GetLastIndex_m1_18812_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::ForEach(System.Action`1<T>)
extern "C" void List_1_ForEach_m1_18813_gshared (List_1_t1_1834 * __this, Action_1_t1_2277 * ___action, const MethodInfo* method);
#define List_1_ForEach_m1_18813(__this, ___action, method) (( void (*) (List_1_t1_1834 *, Action_1_t1_2277 *, const MethodInfo*))List_1_ForEach_m1_18813_gshared)(__this, ___action, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Vector3>::GetEnumerator()
extern "C" Enumerator_t1_2268  List_1_GetEnumerator_m1_18814_gshared (List_1_t1_1834 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m1_18814(__this, method) (( Enumerator_t1_2268  (*) (List_1_t1_1834 *, const MethodInfo*))List_1_GetEnumerator_m1_18814_gshared)(__this, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<UnityEngine.Vector3>::GetRange(System.Int32,System.Int32)
extern "C" List_1_t1_1834 * List_1_GetRange_m1_18815_gshared (List_1_t1_1834 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_GetRange_m1_18815(__this, ___index, ___count, method) (( List_1_t1_1834 * (*) (List_1_t1_1834 *, int32_t, int32_t, const MethodInfo*))List_1_GetRange_m1_18815_gshared)(__this, ___index, ___count, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m1_18816_gshared (List_1_t1_1834 * __this, Vector3_t6_48  ___item, const MethodInfo* method);
#define List_1_IndexOf_m1_18816(__this, ___item, method) (( int32_t (*) (List_1_t1_1834 *, Vector3_t6_48 , const MethodInfo*))List_1_IndexOf_m1_18816_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::IndexOf(T,System.Int32)
extern "C" int32_t List_1_IndexOf_m1_18817_gshared (List_1_t1_1834 * __this, Vector3_t6_48  ___item, int32_t ___index, const MethodInfo* method);
#define List_1_IndexOf_m1_18817(__this, ___item, ___index, method) (( int32_t (*) (List_1_t1_1834 *, Vector3_t6_48 , int32_t, const MethodInfo*))List_1_IndexOf_m1_18817_gshared)(__this, ___item, ___index, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::IndexOf(T,System.Int32,System.Int32)
extern "C" int32_t List_1_IndexOf_m1_18818_gshared (List_1_t1_1834 * __this, Vector3_t6_48  ___item, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_IndexOf_m1_18818(__this, ___item, ___index, ___count, method) (( int32_t (*) (List_1_t1_1834 *, Vector3_t6_48 , int32_t, int32_t, const MethodInfo*))List_1_IndexOf_m1_18818_gshared)(__this, ___item, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m1_18819_gshared (List_1_t1_1834 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m1_18819(__this, ___start, ___delta, method) (( void (*) (List_1_t1_1834 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m1_18819_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m1_18820_gshared (List_1_t1_1834 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m1_18820(__this, ___index, method) (( void (*) (List_1_t1_1834 *, int32_t, const MethodInfo*))List_1_CheckIndex_m1_18820_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m1_18821_gshared (List_1_t1_1834 * __this, int32_t ___index, Vector3_t6_48  ___item, const MethodInfo* method);
#define List_1_Insert_m1_18821(__this, ___index, ___item, method) (( void (*) (List_1_t1_1834 *, int32_t, Vector3_t6_48 , const MethodInfo*))List_1_Insert_m1_18821_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m1_18822_gshared (List_1_t1_1834 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m1_18822(__this, ___collection, method) (( void (*) (List_1_t1_1834 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m1_18822_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::InsertRange(System.Int32,System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_InsertRange_m1_18823_gshared (List_1_t1_1834 * __this, int32_t ___index, Object_t* ___collection, const MethodInfo* method);
#define List_1_InsertRange_m1_18823(__this, ___index, ___collection, method) (( void (*) (List_1_t1_1834 *, int32_t, Object_t*, const MethodInfo*))List_1_InsertRange_m1_18823_gshared)(__this, ___index, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::InsertCollection(System.Int32,System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_InsertCollection_m1_18824_gshared (List_1_t1_1834 * __this, int32_t ___index, Object_t* ___collection, const MethodInfo* method);
#define List_1_InsertCollection_m1_18824(__this, ___index, ___collection, method) (( void (*) (List_1_t1_1834 *, int32_t, Object_t*, const MethodInfo*))List_1_InsertCollection_m1_18824_gshared)(__this, ___index, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::InsertEnumeration(System.Int32,System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_InsertEnumeration_m1_18825_gshared (List_1_t1_1834 * __this, int32_t ___index, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_InsertEnumeration_m1_18825(__this, ___index, ___enumerable, method) (( void (*) (List_1_t1_1834 *, int32_t, Object_t*, const MethodInfo*))List_1_InsertEnumeration_m1_18825_gshared)(__this, ___index, ___enumerable, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::LastIndexOf(T)
extern "C" int32_t List_1_LastIndexOf_m1_18826_gshared (List_1_t1_1834 * __this, Vector3_t6_48  ___item, const MethodInfo* method);
#define List_1_LastIndexOf_m1_18826(__this, ___item, method) (( int32_t (*) (List_1_t1_1834 *, Vector3_t6_48 , const MethodInfo*))List_1_LastIndexOf_m1_18826_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::LastIndexOf(T,System.Int32)
extern "C" int32_t List_1_LastIndexOf_m1_18827_gshared (List_1_t1_1834 * __this, Vector3_t6_48  ___item, int32_t ___index, const MethodInfo* method);
#define List_1_LastIndexOf_m1_18827(__this, ___item, ___index, method) (( int32_t (*) (List_1_t1_1834 *, Vector3_t6_48 , int32_t, const MethodInfo*))List_1_LastIndexOf_m1_18827_gshared)(__this, ___item, ___index, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::LastIndexOf(T,System.Int32,System.Int32)
extern "C" int32_t List_1_LastIndexOf_m1_18828_gshared (List_1_t1_1834 * __this, Vector3_t6_48  ___item, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_LastIndexOf_m1_18828(__this, ___item, ___index, ___count, method) (( int32_t (*) (List_1_t1_1834 *, Vector3_t6_48 , int32_t, int32_t, const MethodInfo*))List_1_LastIndexOf_m1_18828_gshared)(__this, ___item, ___index, ___count, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector3>::Remove(T)
extern "C" bool List_1_Remove_m1_18829_gshared (List_1_t1_1834 * __this, Vector3_t6_48  ___item, const MethodInfo* method);
#define List_1_Remove_m1_18829(__this, ___item, method) (( bool (*) (List_1_t1_1834 *, Vector3_t6_48 , const MethodInfo*))List_1_Remove_m1_18829_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m1_18830_gshared (List_1_t1_1834 * __this, Predicate_1_t1_2276 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m1_18830(__this, ___match, method) (( int32_t (*) (List_1_t1_1834 *, Predicate_1_t1_2276 *, const MethodInfo*))List_1_RemoveAll_m1_18830_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m1_18831_gshared (List_1_t1_1834 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m1_18831(__this, ___index, method) (( void (*) (List_1_t1_1834 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1_18831_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::RemoveRange(System.Int32,System.Int32)
extern "C" void List_1_RemoveRange_m1_18832_gshared (List_1_t1_1834 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_RemoveRange_m1_18832(__this, ___index, ___count, method) (( void (*) (List_1_t1_1834 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m1_18832_gshared)(__this, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Reverse()
extern "C" void List_1_Reverse_m1_18833_gshared (List_1_t1_1834 * __this, const MethodInfo* method);
#define List_1_Reverse_m1_18833(__this, method) (( void (*) (List_1_t1_1834 *, const MethodInfo*))List_1_Reverse_m1_18833_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Reverse(System.Int32,System.Int32)
extern "C" void List_1_Reverse_m1_18834_gshared (List_1_t1_1834 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_Reverse_m1_18834(__this, ___index, ___count, method) (( void (*) (List_1_t1_1834 *, int32_t, int32_t, const MethodInfo*))List_1_Reverse_m1_18834_gshared)(__this, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Sort()
extern "C" void List_1_Sort_m1_18835_gshared (List_1_t1_1834 * __this, const MethodInfo* method);
#define List_1_Sort_m1_18835(__this, method) (( void (*) (List_1_t1_1834 *, const MethodInfo*))List_1_Sort_m1_18835_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C" void List_1_Sort_m1_18836_gshared (List_1_t1_1834 * __this, Object_t* ___comparer, const MethodInfo* method);
#define List_1_Sort_m1_18836(__this, ___comparer, method) (( void (*) (List_1_t1_1834 *, Object_t*, const MethodInfo*))List_1_Sort_m1_18836_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m1_18837_gshared (List_1_t1_1834 * __this, Comparison_1_t1_2278 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m1_18837(__this, ___comparison, method) (( void (*) (List_1_t1_1834 *, Comparison_1_t1_2278 *, const MethodInfo*))List_1_Sort_m1_18837_gshared)(__this, ___comparison, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Sort(System.Int32,System.Int32,System.Collections.Generic.IComparer`1<T>)
extern "C" void List_1_Sort_m1_18838_gshared (List_1_t1_1834 * __this, int32_t ___index, int32_t ___count, Object_t* ___comparer, const MethodInfo* method);
#define List_1_Sort_m1_18838(__this, ___index, ___count, ___comparer, method) (( void (*) (List_1_t1_1834 *, int32_t, int32_t, Object_t*, const MethodInfo*))List_1_Sort_m1_18838_gshared)(__this, ___index, ___count, ___comparer, method)
// T[] System.Collections.Generic.List`1<UnityEngine.Vector3>::ToArray()
extern "C" Vector3U5BU5D_t6_161* List_1_ToArray_m1_18839_gshared (List_1_t1_1834 * __this, const MethodInfo* method);
#define List_1_ToArray_m1_18839(__this, method) (( Vector3U5BU5D_t6_161* (*) (List_1_t1_1834 *, const MethodInfo*))List_1_ToArray_m1_18839_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::TrimExcess()
extern "C" void List_1_TrimExcess_m1_18840_gshared (List_1_t1_1834 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m1_18840(__this, method) (( void (*) (List_1_t1_1834 *, const MethodInfo*))List_1_TrimExcess_m1_18840_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector3>::TrueForAll(System.Predicate`1<T>)
extern "C" bool List_1_TrueForAll_m1_18841_gshared (List_1_t1_1834 * __this, Predicate_1_t1_2276 * ___match, const MethodInfo* method);
#define List_1_TrueForAll_m1_18841(__this, ___match, method) (( bool (*) (List_1_t1_1834 *, Predicate_1_t1_2276 *, const MethodInfo*))List_1_TrueForAll_m1_18841_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m1_18842_gshared (List_1_t1_1834 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m1_18842(__this, method) (( int32_t (*) (List_1_t1_1834 *, const MethodInfo*))List_1_get_Capacity_m1_18842_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m1_18843_gshared (List_1_t1_1834 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m1_18843(__this, ___value, method) (( void (*) (List_1_t1_1834 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1_18843_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Count()
extern "C" int32_t List_1_get_Count_m1_18844_gshared (List_1_t1_1834 * __this, const MethodInfo* method);
#define List_1_get_Count_m1_18844(__this, method) (( int32_t (*) (List_1_t1_1834 *, const MethodInfo*))List_1_get_Count_m1_18844_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Item(System.Int32)
extern "C" Vector3_t6_48  List_1_get_Item_m1_18845_gshared (List_1_t1_1834 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m1_18845(__this, ___index, method) (( Vector3_t6_48  (*) (List_1_t1_1834 *, int32_t, const MethodInfo*))List_1_get_Item_m1_18845_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m1_18846_gshared (List_1_t1_1834 * __this, int32_t ___index, Vector3_t6_48  ___value, const MethodInfo* method);
#define List_1_set_Item_m1_18846(__this, ___index, ___value, method) (( void (*) (List_1_t1_1834 *, int32_t, Vector3_t6_48 , const MethodInfo*))List_1_set_Item_m1_18846_gshared)(__this, ___index, ___value, method)
