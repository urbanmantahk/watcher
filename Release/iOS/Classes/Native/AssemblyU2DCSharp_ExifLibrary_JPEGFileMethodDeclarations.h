﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExifLibrary.JPEGFile
struct JPEGFile_t8_111;
// System.IO.Stream
struct Stream_t1_405;
// System.String
struct String_t;
// System.Collections.Generic.List`1<ExifLibrary.JPEGSection>
struct List_1_t1_1906;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "codegen/il2cpp-codegen.h"

// System.Void ExifLibrary.JPEGFile::.ctor()
extern "C" void JPEGFile__ctor_m8_639 (JPEGFile_t8_111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.JPEGFile::.ctor(System.IO.Stream)
extern "C" void JPEGFile__ctor_m8_640 (JPEGFile_t8_111 * __this, Stream_t1_405 * ___stream, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.JPEGFile::.ctor(System.String)
extern "C" void JPEGFile__ctor_m8_641 (JPEGFile_t8_111 * __this, String_t* ___filename, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<ExifLibrary.JPEGSection> ExifLibrary.JPEGFile::get_Sections()
extern "C" List_1_t1_1906 * JPEGFile_get_Sections_m8_642 (JPEGFile_t8_111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.JPEGFile::set_Sections(System.Collections.Generic.List`1<ExifLibrary.JPEGSection>)
extern "C" void JPEGFile_set_Sections_m8_643 (JPEGFile_t8_111 * __this, List_1_t1_1906 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ExifLibrary.JPEGFile::get_TrailingData()
extern "C" ByteU5BU5D_t1_109* JPEGFile_get_TrailingData_m8_644 (JPEGFile_t8_111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.JPEGFile::set_TrailingData(System.Byte[])
extern "C" void JPEGFile_set_TrailingData_m8_645 (JPEGFile_t8_111 * __this, ByteU5BU5D_t1_109* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.JPEGFile::Save(System.IO.Stream)
extern "C" void JPEGFile_Save_m8_646 (JPEGFile_t8_111 * __this, Stream_t1_405 * ___stream, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.JPEGFile::Save(System.String)
extern "C" void JPEGFile_Save_m8_647 (JPEGFile_t8_111 * __this, String_t* ___filename, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExifLibrary.JPEGFile::Read(System.IO.Stream)
extern "C" bool JPEGFile_Read_m8_648 (JPEGFile_t8_111 * __this, Stream_t1_405 * ___stream, const MethodInfo* method) IL2CPP_METHOD_ATTR;
