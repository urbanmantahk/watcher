﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_ePickVideoFinis.h"

// VoxelBusters.NativePlugins.ePickVideoFinishReason
struct  ePickVideoFinishReason_t8_250 
{
	// System.Int32 VoxelBusters.NativePlugins.ePickVideoFinishReason::value__
	int32_t ___value___1;
};
