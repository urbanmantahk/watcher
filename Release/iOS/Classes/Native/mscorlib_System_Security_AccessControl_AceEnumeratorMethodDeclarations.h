﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.AccessControl.AceEnumerator
struct AceEnumerator_t1_1113;
// System.Security.AccessControl.GenericAcl
struct GenericAcl_t1_1114;
// System.Object
struct Object_t;
// System.Security.AccessControl.GenericAce
struct GenericAce_t1_1145;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.AccessControl.AceEnumerator::.ctor(System.Security.AccessControl.GenericAcl)
extern "C" void AceEnumerator__ctor_m1_9708 (AceEnumerator_t1_1113 * __this, GenericAcl_t1_1114 * ___owner, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Security.AccessControl.AceEnumerator::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * AceEnumerator_System_Collections_IEnumerator_get_Current_m1_9709 (AceEnumerator_t1_1113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.AccessControl.GenericAce System.Security.AccessControl.AceEnumerator::get_Current()
extern "C" GenericAce_t1_1145 * AceEnumerator_get_Current_m1_9710 (AceEnumerator_t1_1113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.AccessControl.AceEnumerator::MoveNext()
extern "C" bool AceEnumerator_MoveNext_m1_9711 (AceEnumerator_t1_1113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.AceEnumerator::Reset()
extern "C" void AceEnumerator_Reset_m1_9712 (AceEnumerator_t1_1113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
