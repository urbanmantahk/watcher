﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.IEnumerator
struct IEnumerator_t1_193;

#include "mscorlib_System_Object.h"

// System.Collections.ArrayList/ArrayListAdapter/EnumeratorWithRange
struct  EnumeratorWithRange_t1_260  : public Object_t
{
	// System.Int32 System.Collections.ArrayList/ArrayListAdapter/EnumeratorWithRange::m_StartIndex
	int32_t ___m_StartIndex_0;
	// System.Int32 System.Collections.ArrayList/ArrayListAdapter/EnumeratorWithRange::m_Count
	int32_t ___m_Count_1;
	// System.Int32 System.Collections.ArrayList/ArrayListAdapter/EnumeratorWithRange::m_MaxCount
	int32_t ___m_MaxCount_2;
	// System.Collections.IEnumerator System.Collections.ArrayList/ArrayListAdapter/EnumeratorWithRange::m_Enumerator
	Object_t * ___m_Enumerator_3;
};
