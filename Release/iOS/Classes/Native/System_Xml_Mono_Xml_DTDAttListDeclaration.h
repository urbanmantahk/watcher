﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Hashtable
struct Hashtable_t1_100;
// System.Collections.ArrayList
struct ArrayList_t1_170;

#include "System_Xml_Mono_Xml_DTDNode.h"

// Mono.Xml.DTDAttListDeclaration
struct  DTDAttListDeclaration_t4_96  : public DTDNode_t4_89
{
	// System.String Mono.Xml.DTDAttListDeclaration::name
	String_t* ___name_5;
	// System.Collections.Hashtable Mono.Xml.DTDAttListDeclaration::attributeOrders
	Hashtable_t1_100 * ___attributeOrders_6;
	// System.Collections.ArrayList Mono.Xml.DTDAttListDeclaration::attributes
	ArrayList_t1_170 * ___attributes_7;
};
