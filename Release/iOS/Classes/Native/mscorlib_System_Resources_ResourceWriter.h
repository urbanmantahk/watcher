﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.SortedList
struct SortedList_t1_304;
// System.IO.Stream
struct Stream_t1_405;

#include "mscorlib_System_Object.h"

// System.Resources.ResourceWriter
struct  ResourceWriter_t1_658  : public Object_t
{
	// System.Collections.SortedList System.Resources.ResourceWriter::resources
	SortedList_t1_304 * ___resources_0;
	// System.IO.Stream System.Resources.ResourceWriter::stream
	Stream_t1_405 * ___stream_1;
};
