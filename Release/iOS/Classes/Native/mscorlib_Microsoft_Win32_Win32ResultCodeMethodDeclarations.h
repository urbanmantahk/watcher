﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Microsoft.Win32.Win32ResultCode
struct Win32ResultCode_t1_103;

#include "codegen/il2cpp-codegen.h"

// System.Void Microsoft.Win32.Win32ResultCode::.ctor()
extern "C" void Win32ResultCode__ctor_m1_1563 (Win32ResultCode_t1_103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
