﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExifLibrary.ExifUShort
struct ExifUShort_t8_117;
// System.Object
struct Object_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTag.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifInterOperability.h"

// System.Void ExifLibrary.ExifUShort::.ctor(ExifLibrary.ExifTag,System.UInt16)
extern "C" void ExifUShort__ctor_m8_533 (ExifUShort_t8_117 * __this, int32_t ___tag, uint16_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ExifLibrary.ExifUShort::get__Value()
extern "C" Object_t * ExifUShort_get__Value_m8_534 (ExifUShort_t8_117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.ExifUShort::set__Value(System.Object)
extern "C" void ExifUShort_set__Value_m8_535 (ExifUShort_t8_117 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 ExifLibrary.ExifUShort::get_Value()
extern "C" uint16_t ExifUShort_get_Value_m8_536 (ExifUShort_t8_117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.ExifUShort::set_Value(System.UInt16)
extern "C" void ExifUShort_set_Value_m8_537 (ExifUShort_t8_117 * __this, uint16_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExifLibrary.ExifUShort::ToString()
extern "C" String_t* ExifUShort_ToString_m8_538 (ExifUShort_t8_117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.ExifInterOperability ExifLibrary.ExifUShort::get_Interoperability()
extern "C" ExifInterOperability_t8_113  ExifUShort_get_Interoperability_m8_539 (ExifUShort_t8_117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 ExifLibrary.ExifUShort::op_Implicit(ExifLibrary.ExifUShort)
extern "C" uint16_t ExifUShort_op_Implicit_m8_540 (Object_t * __this /* static, unused */, ExifUShort_t8_117 * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
