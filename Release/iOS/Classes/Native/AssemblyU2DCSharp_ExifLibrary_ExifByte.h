﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_ExifLibrary_ExifProperty.h"

// ExifLibrary.ExifByte
struct  ExifByte_t8_114  : public ExifProperty_t8_99
{
	// System.Byte ExifLibrary.ExifByte::mValue
	uint8_t ___mValue_3;
};
