﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderProperty>
struct List_1_t1_1900;

#include "mscorlib_System_Object.h"

// VoxelBusters.Utility.ShaderUtility/ShaderInfo
struct  ShaderInfo_t8_26  : public Object_t
{
	// System.String VoxelBusters.Utility.ShaderUtility/ShaderInfo::m_name
	String_t* ___m_name_0;
	// System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderProperty> VoxelBusters.Utility.ShaderUtility/ShaderInfo::m_propertyList
	List_1_t1_1900 * ___m_propertyList_1;
};
