﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Type
struct Type_t;
// System.Object
struct Object_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArgument.h"

// System.Void System.Reflection.CustomAttributeTypedArgument::.ctor(System.Type,System.Object)
extern "C" void CustomAttributeTypedArgument__ctor_m1_6802 (CustomAttributeTypedArgument_t1_594 * __this, Type_t * ___argumentType, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.CustomAttributeTypedArgument::get_ArgumentType()
extern "C" Type_t * CustomAttributeTypedArgument_get_ArgumentType_m1_6803 (CustomAttributeTypedArgument_t1_594 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Reflection.CustomAttributeTypedArgument::get_Value()
extern "C" Object_t * CustomAttributeTypedArgument_get_Value_m1_6804 (CustomAttributeTypedArgument_t1_594 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.CustomAttributeTypedArgument::ToString()
extern "C" String_t* CustomAttributeTypedArgument_ToString_m1_6805 (CustomAttributeTypedArgument_t1_594 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.CustomAttributeTypedArgument::Equals(System.Object)
extern "C" bool CustomAttributeTypedArgument_Equals_m1_6806 (CustomAttributeTypedArgument_t1_594 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.CustomAttributeTypedArgument::GetHashCode()
extern "C" int32_t CustomAttributeTypedArgument_GetHashCode_m1_6807 (CustomAttributeTypedArgument_t1_594 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.CustomAttributeTypedArgument::op_Equality(System.Reflection.CustomAttributeTypedArgument,System.Reflection.CustomAttributeTypedArgument)
extern "C" bool CustomAttributeTypedArgument_op_Equality_m1_6808 (Object_t * __this /* static, unused */, CustomAttributeTypedArgument_t1_594  ___left, CustomAttributeTypedArgument_t1_594  ___right, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.CustomAttributeTypedArgument::op_Inequality(System.Reflection.CustomAttributeTypedArgument,System.Reflection.CustomAttributeTypedArgument)
extern "C" bool CustomAttributeTypedArgument_op_Inequality_m1_6809 (Object_t * __this /* static, unused */, CustomAttributeTypedArgument_t1_594  ___left, CustomAttributeTypedArgument_t1_594  ___right, const MethodInfo* method) IL2CPP_METHOD_ATTR;
