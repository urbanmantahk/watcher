﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>
struct Comparer_1_t1_2725;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Generic.Comparer`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::.ctor()
extern "C" void Comparer_1__ctor_m1_27274_gshared (Comparer_1_t1_2725 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m1_27274(__this, method) (( void (*) (Comparer_1_t1_2725 *, const MethodInfo*))Comparer_1__ctor_m1_27274_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::.cctor()
extern "C" void Comparer_1__cctor_m1_27275_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m1_27275(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m1_27275_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C" int32_t Comparer_1_System_Collections_IComparer_Compare_m1_27276_gshared (Comparer_1_t1_2725 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m1_27276(__this, ___x, ___y, method) (( int32_t (*) (Comparer_1_t1_2725 *, Object_t *, Object_t *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m1_27276_gshared)(__this, ___x, ___y, method)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::get_Default()
extern "C" Comparer_1_t1_2725 * Comparer_1_get_Default_m1_27277_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m1_27277(__this /* static, unused */, method) (( Comparer_1_t1_2725 * (*) (Object_t * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m1_27277_gshared)(__this /* static, unused */, method)
