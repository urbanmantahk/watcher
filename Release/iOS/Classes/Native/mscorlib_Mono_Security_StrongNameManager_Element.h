﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Hashtable
struct Hashtable_t1_100;

#include "mscorlib_System_Object.h"

// Mono.Security.StrongNameManager/Element
struct  Element_t1_233  : public Object_t
{
	// System.Collections.Hashtable Mono.Security.StrongNameManager/Element::assemblies
	Hashtable_t1_100 * ___assemblies_0;
};
