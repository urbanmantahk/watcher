﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.AccessControl.FileSystemAccessRule
struct FileSystemAccessRule_t1_1154;
// System.Security.Principal.IdentityReference
struct IdentityReference_t1_1120;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_AccessControl_FileSystemRights.h"
#include "mscorlib_System_Security_AccessControl_AccessControlType.h"
#include "mscorlib_System_Security_AccessControl_InheritanceFlags.h"
#include "mscorlib_System_Security_AccessControl_PropagationFlags.h"

// System.Void System.Security.AccessControl.FileSystemAccessRule::.ctor(System.Security.Principal.IdentityReference,System.Security.AccessControl.FileSystemRights,System.Security.AccessControl.AccessControlType)
extern "C" void FileSystemAccessRule__ctor_m1_9870 (FileSystemAccessRule_t1_1154 * __this, IdentityReference_t1_1120 * ___identity, int32_t ___fileSystemRights, int32_t ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.FileSystemAccessRule::.ctor(System.String,System.Security.AccessControl.FileSystemRights,System.Security.AccessControl.AccessControlType)
extern "C" void FileSystemAccessRule__ctor_m1_9871 (FileSystemAccessRule_t1_1154 * __this, String_t* ___identity, int32_t ___fileSystemRights, int32_t ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.FileSystemAccessRule::.ctor(System.Security.Principal.IdentityReference,System.Security.AccessControl.FileSystemRights,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.AccessControlType)
extern "C" void FileSystemAccessRule__ctor_m1_9872 (FileSystemAccessRule_t1_1154 * __this, IdentityReference_t1_1120 * ___identity, int32_t ___fileSystemRights, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.FileSystemAccessRule::.ctor(System.String,System.Security.AccessControl.FileSystemRights,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.AccessControlType)
extern "C" void FileSystemAccessRule__ctor_m1_9873 (FileSystemAccessRule_t1_1154 * __this, String_t* ___identity, int32_t ___fileSystemRights, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.AccessControl.FileSystemRights System.Security.AccessControl.FileSystemAccessRule::get_FileSystemRights()
extern "C" int32_t FileSystemAccessRule_get_FileSystemRights_m1_9874 (FileSystemAccessRule_t1_1154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
