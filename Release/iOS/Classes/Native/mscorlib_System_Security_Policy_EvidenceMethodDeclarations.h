﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Policy.Evidence
struct Evidence_t1_398;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Object
struct Object_t;
// System.Collections.ArrayList
struct ArrayList_t1_170;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Type
struct Type_t;
// System.Reflection.Assembly
struct Assembly_t1_467;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Policy.Evidence::.ctor()
extern "C" void Evidence__ctor_m1_11454 (Evidence_t1_398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.Evidence::.ctor(System.Security.Policy.Evidence)
extern "C" void Evidence__ctor_m1_11455 (Evidence_t1_398 * __this, Evidence_t1_398 * ___evidence, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.Evidence::.ctor(System.Object[],System.Object[])
extern "C" void Evidence__ctor_m1_11456 (Evidence_t1_398 * __this, ObjectU5BU5D_t1_272* ___hostEvidence, ObjectU5BU5D_t1_272* ___assemblyEvidence, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Policy.Evidence::get_Count()
extern "C" int32_t Evidence_get_Count_m1_11457 (Evidence_t1_398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Policy.Evidence::get_IsReadOnly()
extern "C" bool Evidence_get_IsReadOnly_m1_11458 (Evidence_t1_398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Policy.Evidence::get_IsSynchronized()
extern "C" bool Evidence_get_IsSynchronized_m1_11459 (Evidence_t1_398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Policy.Evidence::get_Locked()
extern "C" bool Evidence_get_Locked_m1_11460 (Evidence_t1_398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.Evidence::set_Locked(System.Boolean)
extern "C" void Evidence_set_Locked_m1_11461 (Evidence_t1_398 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Security.Policy.Evidence::get_SyncRoot()
extern "C" Object_t * Evidence_get_SyncRoot_m1_11462 (Evidence_t1_398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList System.Security.Policy.Evidence::get_HostEvidenceList()
extern "C" ArrayList_t1_170 * Evidence_get_HostEvidenceList_m1_11463 (Evidence_t1_398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList System.Security.Policy.Evidence::get_AssemblyEvidenceList()
extern "C" ArrayList_t1_170 * Evidence_get_AssemblyEvidenceList_m1_11464 (Evidence_t1_398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.Evidence::AddAssembly(System.Object)
extern "C" void Evidence_AddAssembly_m1_11465 (Evidence_t1_398 * __this, Object_t * ___id, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.Evidence::AddHost(System.Object)
extern "C" void Evidence_AddHost_m1_11466 (Evidence_t1_398 * __this, Object_t * ___id, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.Evidence::Clear()
extern "C" void Evidence_Clear_m1_11467 (Evidence_t1_398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.Evidence::CopyTo(System.Array,System.Int32)
extern "C" void Evidence_CopyTo_m1_11468 (Evidence_t1_398 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Policy.Evidence::Equals(System.Object)
extern "C" bool Evidence_Equals_m1_11469 (Evidence_t1_398 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Security.Policy.Evidence::GetEnumerator()
extern "C" Object_t * Evidence_GetEnumerator_m1_11470 (Evidence_t1_398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Security.Policy.Evidence::GetAssemblyEnumerator()
extern "C" Object_t * Evidence_GetAssemblyEnumerator_m1_11471 (Evidence_t1_398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Policy.Evidence::GetHashCode()
extern "C" int32_t Evidence_GetHashCode_m1_11472 (Evidence_t1_398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Security.Policy.Evidence::GetHostEnumerator()
extern "C" Object_t * Evidence_GetHostEnumerator_m1_11473 (Evidence_t1_398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.Evidence::Merge(System.Security.Policy.Evidence)
extern "C" void Evidence_Merge_m1_11474 (Evidence_t1_398 * __this, Evidence_t1_398 * ___evidence, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.Evidence::RemoveType(System.Type)
extern "C" void Evidence_RemoveType_m1_11475 (Evidence_t1_398 * __this, Type_t * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Policy.Evidence::IsAuthenticodePresent(System.Reflection.Assembly)
extern "C" bool Evidence_IsAuthenticodePresent_m1_11476 (Object_t * __this /* static, unused */, Assembly_t1_467 * ___a, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.Evidence System.Security.Policy.Evidence::GetDefaultHostEvidence(System.Reflection.Assembly)
extern "C" Evidence_t1_398 * Evidence_GetDefaultHostEvidence_m1_11477 (Object_t * __this /* static, unused */, Assembly_t1_467 * ___a, const MethodInfo* method) IL2CPP_METHOD_ATTR;
