﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Threading_AsyncFlowControlType.h"

// System.Threading.AsyncFlowControlType
struct  AsyncFlowControlType_t1_1458 
{
	// System.Int32 System.Threading.AsyncFlowControlType::value__
	int32_t ___value___1;
};
