﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.IDictionary[]
struct IDictionaryU5BU5D_t1_861;
// System.Collections.ArrayList
struct ArrayList_t1_170;

#include "mscorlib_System_Object.h"

// System.Runtime.Remoting.Channels.AggregateDictionary
struct  AggregateDictionary_t1_860  : public Object_t
{
	// System.Collections.IDictionary[] System.Runtime.Remoting.Channels.AggregateDictionary::dictionaries
	IDictionaryU5BU5D_t1_861* ___dictionaries_0;
	// System.Collections.ArrayList System.Runtime.Remoting.Channels.AggregateDictionary::_values
	ArrayList_t1_170 * ____values_1;
	// System.Collections.ArrayList System.Runtime.Remoting.Channels.AggregateDictionary::_keys
	ArrayList_t1_170 * ____keys_2;
};
