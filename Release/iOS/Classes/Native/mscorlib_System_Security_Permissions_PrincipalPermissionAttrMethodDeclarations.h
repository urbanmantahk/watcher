﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Permissions.PrincipalPermissionAttribute
struct PrincipalPermissionAttribute_t1_1298;
// System.String
struct String_t;
// System.Security.IPermission
struct IPermission_t1_1400;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Permissions_SecurityAction.h"

// System.Void System.Security.Permissions.PrincipalPermissionAttribute::.ctor(System.Security.Permissions.SecurityAction)
extern "C" void PrincipalPermissionAttribute__ctor_m1_11055 (PrincipalPermissionAttribute_t1_1298 * __this, int32_t ___action, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.PrincipalPermissionAttribute::get_Authenticated()
extern "C" bool PrincipalPermissionAttribute_get_Authenticated_m1_11056 (PrincipalPermissionAttribute_t1_1298 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.PrincipalPermissionAttribute::set_Authenticated(System.Boolean)
extern "C" void PrincipalPermissionAttribute_set_Authenticated_m1_11057 (PrincipalPermissionAttribute_t1_1298 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Permissions.PrincipalPermissionAttribute::get_Name()
extern "C" String_t* PrincipalPermissionAttribute_get_Name_m1_11058 (PrincipalPermissionAttribute_t1_1298 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.PrincipalPermissionAttribute::set_Name(System.String)
extern "C" void PrincipalPermissionAttribute_set_Name_m1_11059 (PrincipalPermissionAttribute_t1_1298 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Permissions.PrincipalPermissionAttribute::get_Role()
extern "C" String_t* PrincipalPermissionAttribute_get_Role_m1_11060 (PrincipalPermissionAttribute_t1_1298 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.PrincipalPermissionAttribute::set_Role(System.String)
extern "C" void PrincipalPermissionAttribute_set_Role_m1_11061 (PrincipalPermissionAttribute_t1_1298 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.PrincipalPermissionAttribute::CreatePermission()
extern "C" Object_t * PrincipalPermissionAttribute_CreatePermission_m1_11062 (PrincipalPermissionAttribute_t1_1298 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
