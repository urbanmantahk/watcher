﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Xml.Schema.XsdNormalizedString
struct XsdNormalizedString_t4_8;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlTokenizedType.h"

// System.Void Mono.Xml.Schema.XsdNormalizedString::.ctor()
extern "C" void XsdNormalizedString__ctor_m4_9 (XsdNormalizedString_t4_8 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdNormalizedString::get_TokenizedType()
extern "C" int32_t XsdNormalizedString_get_TokenizedType_m4_10 (XsdNormalizedString_t4_8 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
