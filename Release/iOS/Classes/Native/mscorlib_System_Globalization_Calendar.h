﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String[]
struct StringU5BU5D_t1_238;

#include "mscorlib_System_Object.h"

// System.Globalization.Calendar
struct  Calendar_t1_338  : public Object_t
{
	// System.Boolean System.Globalization.Calendar::m_isReadOnly
	bool ___m_isReadOnly_1;
	// System.Int32 System.Globalization.Calendar::twoDigitYearMax
	int32_t ___twoDigitYearMax_2;
	// System.Int32 System.Globalization.Calendar::M_MaxYearValue
	int32_t ___M_MaxYearValue_3;
	// System.String[] System.Globalization.Calendar::M_AbbrEraNames
	StringU5BU5D_t1_238* ___M_AbbrEraNames_4;
	// System.String[] System.Globalization.Calendar::M_EraNames
	StringU5BU5D_t1_238* ___M_EraNames_5;
	// System.Int32 System.Globalization.Calendar::m_currentEraValue
	int32_t ___m_currentEraValue_6;
};
