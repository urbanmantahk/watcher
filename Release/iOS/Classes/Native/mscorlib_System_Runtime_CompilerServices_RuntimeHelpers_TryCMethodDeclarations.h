﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.CompilerServices.RuntimeHelpers/TryCode
struct TryCode_t1_71;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"

// System.Void System.Runtime.CompilerServices.RuntimeHelpers/TryCode::.ctor(System.Object,System.IntPtr)
extern "C" void TryCode__ctor_m1_1378 (TryCode_t1_71 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.CompilerServices.RuntimeHelpers/TryCode::Invoke(System.Object)
extern "C" void TryCode_Invoke_m1_1379 (TryCode_t1_71 * __this, Object_t * ___userData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_TryCode_t1_71(Il2CppObject* delegate, Object_t * ___userData);
// System.IAsyncResult System.Runtime.CompilerServices.RuntimeHelpers/TryCode::BeginInvoke(System.Object,System.AsyncCallback,System.Object)
extern "C" Object_t * TryCode_BeginInvoke_m1_1380 (TryCode_t1_71 * __this, Object_t * ___userData, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.CompilerServices.RuntimeHelpers/TryCode::EndInvoke(System.IAsyncResult)
extern "C" void TryCode_EndInvoke_m1_1381 (TryCode_t1_71 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
