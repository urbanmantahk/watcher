﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>
struct Collection_1_t1_2282;
// System.Collections.Generic.IList`1<UnityEngine.Vector4>
struct IList_1_t1_2281;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Object
struct Object_t;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t6_278;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector4>
struct IEnumerator_1_t1_2799;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector4.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::.ctor()
extern "C" void Collection_1__ctor_m1_19070_gshared (Collection_1_t1_2282 * __this, const MethodInfo* method);
#define Collection_1__ctor_m1_19070(__this, method) (( void (*) (Collection_1_t1_2282 *, const MethodInfo*))Collection_1__ctor_m1_19070_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1__ctor_m1_19071_gshared (Collection_1_t1_2282 * __this, Object_t* ___list, const MethodInfo* method);
#define Collection_1__ctor_m1_19071(__this, ___list, method) (( void (*) (Collection_1_t1_2282 *, Object_t*, const MethodInfo*))Collection_1__ctor_m1_19071_gshared)(__this, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_19072_gshared (Collection_1_t1_2282 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_19072(__this, method) (( bool (*) (Collection_1_t1_2282 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_19072_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m1_19073_gshared (Collection_1_t1_2282 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m1_19073(__this, ___array, ___index, method) (( void (*) (Collection_1_t1_2282 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m1_19073_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m1_19074_gshared (Collection_1_t1_2282 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m1_19074(__this, method) (( Object_t * (*) (Collection_1_t1_2282 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m1_19074_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m1_19075_gshared (Collection_1_t1_2282 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m1_19075(__this, ___value, method) (( int32_t (*) (Collection_1_t1_2282 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m1_19075_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m1_19076_gshared (Collection_1_t1_2282 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m1_19076(__this, ___value, method) (( bool (*) (Collection_1_t1_2282 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m1_19076_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m1_19077_gshared (Collection_1_t1_2282 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m1_19077(__this, ___value, method) (( int32_t (*) (Collection_1_t1_2282 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m1_19077_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m1_19078_gshared (Collection_1_t1_2282 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m1_19078(__this, ___index, ___value, method) (( void (*) (Collection_1_t1_2282 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m1_19078_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m1_19079_gshared (Collection_1_t1_2282 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m1_19079(__this, ___value, method) (( void (*) (Collection_1_t1_2282 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m1_19079_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m1_19080_gshared (Collection_1_t1_2282 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m1_19080(__this, method) (( bool (*) (Collection_1_t1_2282 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m1_19080_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m1_19081_gshared (Collection_1_t1_2282 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m1_19081(__this, method) (( Object_t * (*) (Collection_1_t1_2282 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m1_19081_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m1_19082_gshared (Collection_1_t1_2282 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m1_19082(__this, method) (( bool (*) (Collection_1_t1_2282 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m1_19082_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m1_19083_gshared (Collection_1_t1_2282 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m1_19083(__this, method) (( bool (*) (Collection_1_t1_2282 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m1_19083_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m1_19084_gshared (Collection_1_t1_2282 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m1_19084(__this, ___index, method) (( Object_t * (*) (Collection_1_t1_2282 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m1_19084_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m1_19085_gshared (Collection_1_t1_2282 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m1_19085(__this, ___index, ___value, method) (( void (*) (Collection_1_t1_2282 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m1_19085_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::Add(T)
extern "C" void Collection_1_Add_m1_19086_gshared (Collection_1_t1_2282 * __this, Vector4_t6_54  ___item, const MethodInfo* method);
#define Collection_1_Add_m1_19086(__this, ___item, method) (( void (*) (Collection_1_t1_2282 *, Vector4_t6_54 , const MethodInfo*))Collection_1_Add_m1_19086_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::Clear()
extern "C" void Collection_1_Clear_m1_19087_gshared (Collection_1_t1_2282 * __this, const MethodInfo* method);
#define Collection_1_Clear_m1_19087(__this, method) (( void (*) (Collection_1_t1_2282 *, const MethodInfo*))Collection_1_Clear_m1_19087_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::ClearItems()
extern "C" void Collection_1_ClearItems_m1_19088_gshared (Collection_1_t1_2282 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m1_19088(__this, method) (( void (*) (Collection_1_t1_2282 *, const MethodInfo*))Collection_1_ClearItems_m1_19088_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::Contains(T)
extern "C" bool Collection_1_Contains_m1_19089_gshared (Collection_1_t1_2282 * __this, Vector4_t6_54  ___item, const MethodInfo* method);
#define Collection_1_Contains_m1_19089(__this, ___item, method) (( bool (*) (Collection_1_t1_2282 *, Vector4_t6_54 , const MethodInfo*))Collection_1_Contains_m1_19089_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m1_19090_gshared (Collection_1_t1_2282 * __this, Vector4U5BU5D_t6_278* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m1_19090(__this, ___array, ___index, method) (( void (*) (Collection_1_t1_2282 *, Vector4U5BU5D_t6_278*, int32_t, const MethodInfo*))Collection_1_CopyTo_m1_19090_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m1_19091_gshared (Collection_1_t1_2282 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m1_19091(__this, method) (( Object_t* (*) (Collection_1_t1_2282 *, const MethodInfo*))Collection_1_GetEnumerator_m1_19091_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m1_19092_gshared (Collection_1_t1_2282 * __this, Vector4_t6_54  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m1_19092(__this, ___item, method) (( int32_t (*) (Collection_1_t1_2282 *, Vector4_t6_54 , const MethodInfo*))Collection_1_IndexOf_m1_19092_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m1_19093_gshared (Collection_1_t1_2282 * __this, int32_t ___index, Vector4_t6_54  ___item, const MethodInfo* method);
#define Collection_1_Insert_m1_19093(__this, ___index, ___item, method) (( void (*) (Collection_1_t1_2282 *, int32_t, Vector4_t6_54 , const MethodInfo*))Collection_1_Insert_m1_19093_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m1_19094_gshared (Collection_1_t1_2282 * __this, int32_t ___index, Vector4_t6_54  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m1_19094(__this, ___index, ___item, method) (( void (*) (Collection_1_t1_2282 *, int32_t, Vector4_t6_54 , const MethodInfo*))Collection_1_InsertItem_m1_19094_gshared)(__this, ___index, ___item, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::get_Items()
extern "C" Object_t* Collection_1_get_Items_m1_19095_gshared (Collection_1_t1_2282 * __this, const MethodInfo* method);
#define Collection_1_get_Items_m1_19095(__this, method) (( Object_t* (*) (Collection_1_t1_2282 *, const MethodInfo*))Collection_1_get_Items_m1_19095_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::Remove(T)
extern "C" bool Collection_1_Remove_m1_19096_gshared (Collection_1_t1_2282 * __this, Vector4_t6_54  ___item, const MethodInfo* method);
#define Collection_1_Remove_m1_19096(__this, ___item, method) (( bool (*) (Collection_1_t1_2282 *, Vector4_t6_54 , const MethodInfo*))Collection_1_Remove_m1_19096_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m1_19097_gshared (Collection_1_t1_2282 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m1_19097(__this, ___index, method) (( void (*) (Collection_1_t1_2282 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m1_19097_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m1_19098_gshared (Collection_1_t1_2282 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m1_19098(__this, ___index, method) (( void (*) (Collection_1_t1_2282 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m1_19098_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::get_Count()
extern "C" int32_t Collection_1_get_Count_m1_19099_gshared (Collection_1_t1_2282 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m1_19099(__this, method) (( int32_t (*) (Collection_1_t1_2282 *, const MethodInfo*))Collection_1_get_Count_m1_19099_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::get_Item(System.Int32)
extern "C" Vector4_t6_54  Collection_1_get_Item_m1_19100_gshared (Collection_1_t1_2282 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m1_19100(__this, ___index, method) (( Vector4_t6_54  (*) (Collection_1_t1_2282 *, int32_t, const MethodInfo*))Collection_1_get_Item_m1_19100_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m1_19101_gshared (Collection_1_t1_2282 * __this, int32_t ___index, Vector4_t6_54  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m1_19101(__this, ___index, ___value, method) (( void (*) (Collection_1_t1_2282 *, int32_t, Vector4_t6_54 , const MethodInfo*))Collection_1_set_Item_m1_19101_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m1_19102_gshared (Collection_1_t1_2282 * __this, int32_t ___index, Vector4_t6_54  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m1_19102(__this, ___index, ___item, method) (( void (*) (Collection_1_t1_2282 *, int32_t, Vector4_t6_54 , const MethodInfo*))Collection_1_SetItem_m1_19102_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m1_19103_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m1_19103(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m1_19103_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::ConvertItem(System.Object)
extern "C" Vector4_t6_54  Collection_1_ConvertItem_m1_19104_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m1_19104(__this /* static, unused */, ___item, method) (( Vector4_t6_54  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m1_19104_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m1_19105_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m1_19105(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m1_19105_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m1_19106_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m1_19106(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m1_19106_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m1_19107_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m1_19107(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m1_19107_gshared)(__this /* static, unused */, ___list, method)
