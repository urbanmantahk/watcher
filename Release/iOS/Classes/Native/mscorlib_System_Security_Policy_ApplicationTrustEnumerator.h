﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.IEnumerator
struct IEnumerator_t1_193;

#include "mscorlib_System_Object.h"

// System.Security.Policy.ApplicationTrustEnumerator
struct  ApplicationTrustEnumerator_t1_1336  : public Object_t
{
	// System.Collections.IEnumerator System.Security.Policy.ApplicationTrustEnumerator::e
	Object_t * ___e_0;
};
