﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.X509.Extensions.SubjectKeyIdentifierExtension
struct SubjectKeyIdentifierExtension_t1_181;
// Mono.Security.ASN1
struct ASN1_t1_149;
// Mono.Security.X509.X509Extension
struct X509Extension_t1_178;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Security.X509.Extensions.SubjectKeyIdentifierExtension::.ctor()
extern "C" void SubjectKeyIdentifierExtension__ctor_m1_2114 (SubjectKeyIdentifierExtension_t1_181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.Extensions.SubjectKeyIdentifierExtension::.ctor(Mono.Security.ASN1)
extern "C" void SubjectKeyIdentifierExtension__ctor_m1_2115 (SubjectKeyIdentifierExtension_t1_181 * __this, ASN1_t1_149 * ___asn1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.Extensions.SubjectKeyIdentifierExtension::.ctor(Mono.Security.X509.X509Extension)
extern "C" void SubjectKeyIdentifierExtension__ctor_m1_2116 (SubjectKeyIdentifierExtension_t1_181 * __this, X509Extension_t1_178 * ___extension, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.Extensions.SubjectKeyIdentifierExtension::Decode()
extern "C" void SubjectKeyIdentifierExtension_Decode_m1_2117 (SubjectKeyIdentifierExtension_t1_181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.X509.Extensions.SubjectKeyIdentifierExtension::get_Name()
extern "C" String_t* SubjectKeyIdentifierExtension_get_Name_m1_2118 (SubjectKeyIdentifierExtension_t1_181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.X509.Extensions.SubjectKeyIdentifierExtension::get_Identifier()
extern "C" ByteU5BU5D_t1_109* SubjectKeyIdentifierExtension_get_Identifier_m1_2119 (SubjectKeyIdentifierExtension_t1_181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.X509.Extensions.SubjectKeyIdentifierExtension::ToString()
extern "C" String_t* SubjectKeyIdentifierExtension_ToString_m1_2120 (SubjectKeyIdentifierExtension_t1_181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
