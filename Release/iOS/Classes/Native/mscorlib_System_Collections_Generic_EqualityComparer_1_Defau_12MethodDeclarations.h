﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Color32>
struct DefaultComparer_t1_2308;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color32.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Color32>::.ctor()
extern "C" void DefaultComparer__ctor_m1_19489_gshared (DefaultComparer_t1_2308 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1_19489(__this, method) (( void (*) (DefaultComparer_t1_2308 *, const MethodInfo*))DefaultComparer__ctor_m1_19489_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Color32>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m1_19490_gshared (DefaultComparer_t1_2308 * __this, Color32_t6_49  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m1_19490(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t1_2308 *, Color32_t6_49 , const MethodInfo*))DefaultComparer_GetHashCode_m1_19490_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Color32>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m1_19491_gshared (DefaultComparer_t1_2308 * __this, Color32_t6_49  ___x, Color32_t6_49  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m1_19491(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t1_2308 *, Color32_t6_49 , Color32_t6_49 , const MethodInfo*))DefaultComparer_Equals_m1_19491_gshared)(__this, ___x, ___y, method)
