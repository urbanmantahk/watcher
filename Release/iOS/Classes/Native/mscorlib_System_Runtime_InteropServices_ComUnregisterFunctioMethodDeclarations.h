﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.InteropServices.ComUnregisterFunctionAttribute
struct ComUnregisterFunctionAttribute_t1_776;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.InteropServices.ComUnregisterFunctionAttribute::.ctor()
extern "C" void ComUnregisterFunctionAttribute__ctor_m1_7619 (ComUnregisterFunctionAttribute_t1_776 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
