﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Text.EncodingInfo
struct EncodingInfo_t1_1438;
// System.String
struct String_t;
// System.Object
struct Object_t;
// System.Text.Encoding
struct Encoding_t1_406;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Text.EncodingInfo::.ctor(System.Int32)
extern "C" void EncodingInfo__ctor_m1_12373 (EncodingInfo_t1_1438 * __this, int32_t ___cp, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.EncodingInfo::get_CodePage()
extern "C" int32_t EncodingInfo_get_CodePage_m1_12374 (EncodingInfo_t1_1438 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Text.EncodingInfo::get_DisplayName()
extern "C" String_t* EncodingInfo_get_DisplayName_m1_12375 (EncodingInfo_t1_1438 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Text.EncodingInfo::get_Name()
extern "C" String_t* EncodingInfo_get_Name_m1_12376 (EncodingInfo_t1_1438 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Text.EncodingInfo::Equals(System.Object)
extern "C" bool EncodingInfo_Equals_m1_12377 (EncodingInfo_t1_1438 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.EncodingInfo::GetHashCode()
extern "C" int32_t EncodingInfo_GetHashCode_m1_12378 (EncodingInfo_t1_1438 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.Encoding System.Text.EncodingInfo::GetEncoding()
extern "C" Encoding_t1_406 * EncodingInfo_GetEncoding_m1_12379 (EncodingInfo_t1_1438 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
