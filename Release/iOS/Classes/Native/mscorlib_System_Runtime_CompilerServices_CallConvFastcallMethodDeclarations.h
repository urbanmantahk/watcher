﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.CompilerServices.CallConvFastcall
struct CallConvFastcall_t1_674;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.CompilerServices.CallConvFastcall::.ctor()
extern "C" void CallConvFastcall__ctor_m1_7527 (CallConvFastcall_t1_674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
