﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Reflection_GenericParameterAttributes.h"

// System.Reflection.GenericParameterAttributes
struct  GenericParameterAttributes_t1_600 
{
	// System.Int32 System.Reflection.GenericParameterAttributes::value__
	int32_t ___value___1;
};
