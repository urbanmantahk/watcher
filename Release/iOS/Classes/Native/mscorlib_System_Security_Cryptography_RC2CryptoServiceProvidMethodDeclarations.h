﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.RC2CryptoServiceProvider
struct RC2CryptoServiceProvider_t1_1229;
// System.Security.Cryptography.ICryptoTransform
struct ICryptoTransform_t1_156;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Cryptography.RC2CryptoServiceProvider::.ctor()
extern "C" void RC2CryptoServiceProvider__ctor_m1_10463 (RC2CryptoServiceProvider_t1_1229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Cryptography.RC2CryptoServiceProvider::get_EffectiveKeySize()
extern "C" int32_t RC2CryptoServiceProvider_get_EffectiveKeySize_m1_10464 (RC2CryptoServiceProvider_t1_1229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RC2CryptoServiceProvider::set_EffectiveKeySize(System.Int32)
extern "C" void RC2CryptoServiceProvider_set_EffectiveKeySize_m1_10465 (RC2CryptoServiceProvider_t1_1229 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.RC2CryptoServiceProvider::CreateDecryptor(System.Byte[],System.Byte[])
extern "C" Object_t * RC2CryptoServiceProvider_CreateDecryptor_m1_10466 (RC2CryptoServiceProvider_t1_1229 * __this, ByteU5BU5D_t1_109* ___rgbKey, ByteU5BU5D_t1_109* ___rgbIV, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.RC2CryptoServiceProvider::CreateEncryptor(System.Byte[],System.Byte[])
extern "C" Object_t * RC2CryptoServiceProvider_CreateEncryptor_m1_10467 (RC2CryptoServiceProvider_t1_1229 * __this, ByteU5BU5D_t1_109* ___rgbKey, ByteU5BU5D_t1_109* ___rgbIV, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RC2CryptoServiceProvider::GenerateIV()
extern "C" void RC2CryptoServiceProvider_GenerateIV_m1_10468 (RC2CryptoServiceProvider_t1_1229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RC2CryptoServiceProvider::GenerateKey()
extern "C" void RC2CryptoServiceProvider_GenerateKey_m1_10469 (RC2CryptoServiceProvider_t1_1229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.RC2CryptoServiceProvider::get_UseSalt()
extern "C" bool RC2CryptoServiceProvider_get_UseSalt_m1_10470 (RC2CryptoServiceProvider_t1_1229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RC2CryptoServiceProvider::set_UseSalt(System.Boolean)
extern "C" void RC2CryptoServiceProvider_set_UseSalt_m1_10471 (RC2CryptoServiceProvider_t1_1229 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
