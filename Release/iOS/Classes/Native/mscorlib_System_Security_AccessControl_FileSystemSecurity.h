﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Security_AccessControl_NativeObjectSecurity.h"

// System.Security.AccessControl.FileSystemSecurity
struct  FileSystemSecurity_t1_1148  : public NativeObjectSecurity_t1_1143
{
};
