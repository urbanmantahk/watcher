﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.InteropServices.AllowReversePInvokeCallsAttribute
struct AllowReversePInvokeCallsAttribute_t1_751;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.InteropServices.AllowReversePInvokeCallsAttribute::.ctor()
extern "C" void AllowReversePInvokeCallsAttribute__ctor_m1_7575 (AllowReversePInvokeCallsAttribute_t1_751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
