﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.SortedList/ListKeys
struct ListKeys_t1_305;
// System.Collections.SortedList
struct SortedList_t1_304;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.SortedList/ListKeys::.ctor(System.Collections.SortedList)
extern "C" void ListKeys__ctor_m1_3391 (ListKeys_t1_305 * __this, SortedList_t1_304 * ___host, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.SortedList/ListKeys::get_Count()
extern "C" int32_t ListKeys_get_Count_m1_3392 (ListKeys_t1_305 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.SortedList/ListKeys::get_IsSynchronized()
extern "C" bool ListKeys_get_IsSynchronized_m1_3393 (ListKeys_t1_305 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.SortedList/ListKeys::get_SyncRoot()
extern "C" Object_t * ListKeys_get_SyncRoot_m1_3394 (ListKeys_t1_305 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.SortedList/ListKeys::CopyTo(System.Array,System.Int32)
extern "C" void ListKeys_CopyTo_m1_3395 (ListKeys_t1_305 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.SortedList/ListKeys::get_IsFixedSize()
extern "C" bool ListKeys_get_IsFixedSize_m1_3396 (ListKeys_t1_305 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.SortedList/ListKeys::get_IsReadOnly()
extern "C" bool ListKeys_get_IsReadOnly_m1_3397 (ListKeys_t1_305 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.SortedList/ListKeys::get_Item(System.Int32)
extern "C" Object_t * ListKeys_get_Item_m1_3398 (ListKeys_t1_305 * __this, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.SortedList/ListKeys::set_Item(System.Int32,System.Object)
extern "C" void ListKeys_set_Item_m1_3399 (ListKeys_t1_305 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.SortedList/ListKeys::Add(System.Object)
extern "C" int32_t ListKeys_Add_m1_3400 (ListKeys_t1_305 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.SortedList/ListKeys::Clear()
extern "C" void ListKeys_Clear_m1_3401 (ListKeys_t1_305 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.SortedList/ListKeys::Contains(System.Object)
extern "C" bool ListKeys_Contains_m1_3402 (ListKeys_t1_305 * __this, Object_t * ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.SortedList/ListKeys::IndexOf(System.Object)
extern "C" int32_t ListKeys_IndexOf_m1_3403 (ListKeys_t1_305 * __this, Object_t * ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.SortedList/ListKeys::Insert(System.Int32,System.Object)
extern "C" void ListKeys_Insert_m1_3404 (ListKeys_t1_305 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.SortedList/ListKeys::Remove(System.Object)
extern "C" void ListKeys_Remove_m1_3405 (ListKeys_t1_305 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.SortedList/ListKeys::RemoveAt(System.Int32)
extern "C" void ListKeys_RemoveAt_m1_3406 (ListKeys_t1_305 * __this, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Collections.SortedList/ListKeys::GetEnumerator()
extern "C" Object_t * ListKeys_GetEnumerator_m1_3407 (ListKeys_t1_305 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
