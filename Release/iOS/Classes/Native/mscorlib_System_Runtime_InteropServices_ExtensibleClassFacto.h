﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Hashtable
struct Hashtable_t1_100;

#include "mscorlib_System_Object.h"

// System.Runtime.InteropServices.ExtensibleClassFactory
struct  ExtensibleClassFactory_t1_790  : public Object_t
{
};
struct ExtensibleClassFactory_t1_790_StaticFields{
	// System.Collections.Hashtable System.Runtime.InteropServices.ExtensibleClassFactory::hashtable
	Hashtable_t1_100 * ___hashtable_0;
};
