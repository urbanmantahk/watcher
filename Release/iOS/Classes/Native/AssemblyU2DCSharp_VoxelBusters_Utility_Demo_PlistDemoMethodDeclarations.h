﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.Utility.Demo.PlistDemo
struct PlistDemo_t8_50;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.Utility.Demo.PlistDemo::.ctor()
extern "C" void PlistDemo__ctor_m8_261 (PlistDemo_t8_50 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.Demo.PlistDemo::OnGUI()
extern "C" void PlistDemo_OnGUI_m8_262 (PlistDemo_t8_50 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.Demo.PlistDemo::OnGUIButtonPressed(System.String)
extern "C" void PlistDemo_OnGUIButtonPressed_m8_263 (PlistDemo_t8_50 * __this, String_t* ____buttonName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.Demo.PlistDemo::LoadPlistAtPath()
extern "C" void PlistDemo_LoadPlistAtPath_m8_264 (PlistDemo_t8_50 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.Demo.PlistDemo::LoadPlistText()
extern "C" void PlistDemo_LoadPlistText_m8_265 (PlistDemo_t8_50 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.Demo.PlistDemo::GetKeyPathValue()
extern "C" void PlistDemo_GetKeyPathValue_m8_266 (PlistDemo_t8_50 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.Demo.PlistDemo::AddValue()
extern "C" void PlistDemo_AddValue_m8_267 (PlistDemo_t8_50 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.Demo.PlistDemo::Save()
extern "C" void PlistDemo_Save_m8_268 (PlistDemo_t8_50 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
