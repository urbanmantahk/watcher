﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExifLibrary.ExifEnumProperty`1<ExifLibrary.PhotometricInterpretation>
struct ExifEnumProperty_1_t8_339;
// System.Object
struct Object_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTag.h"
#include "AssemblyU2DCSharp_ExifLibrary_PhotometricInterpretation.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifInterOperability.h"

// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.PhotometricInterpretation>::.ctor(ExifLibrary.ExifTag,T,System.Boolean)
extern "C" void ExifEnumProperty_1__ctor_m8_2082_gshared (ExifEnumProperty_1_t8_339 * __this, int32_t ___tag, uint16_t ___value, bool ___isbitfield, const MethodInfo* method);
#define ExifEnumProperty_1__ctor_m8_2082(__this, ___tag, ___value, ___isbitfield, method) (( void (*) (ExifEnumProperty_1_t8_339 *, int32_t, uint16_t, bool, const MethodInfo*))ExifEnumProperty_1__ctor_m8_2082_gshared)(__this, ___tag, ___value, ___isbitfield, method)
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.PhotometricInterpretation>::.ctor(ExifLibrary.ExifTag,T)
extern "C" void ExifEnumProperty_1__ctor_m8_1975_gshared (ExifEnumProperty_1_t8_339 * __this, int32_t ___tag, uint16_t ___value, const MethodInfo* method);
#define ExifEnumProperty_1__ctor_m8_1975(__this, ___tag, ___value, method) (( void (*) (ExifEnumProperty_1_t8_339 *, int32_t, uint16_t, const MethodInfo*))ExifEnumProperty_1__ctor_m8_1975_gshared)(__this, ___tag, ___value, method)
// System.Object ExifLibrary.ExifEnumProperty`1<ExifLibrary.PhotometricInterpretation>::get__Value()
extern "C" Object_t * ExifEnumProperty_1_get__Value_m8_2083_gshared (ExifEnumProperty_1_t8_339 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get__Value_m8_2083(__this, method) (( Object_t * (*) (ExifEnumProperty_1_t8_339 *, const MethodInfo*))ExifEnumProperty_1_get__Value_m8_2083_gshared)(__this, method)
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.PhotometricInterpretation>::set__Value(System.Object)
extern "C" void ExifEnumProperty_1_set__Value_m8_2084_gshared (ExifEnumProperty_1_t8_339 * __this, Object_t * ___value, const MethodInfo* method);
#define ExifEnumProperty_1_set__Value_m8_2084(__this, ___value, method) (( void (*) (ExifEnumProperty_1_t8_339 *, Object_t *, const MethodInfo*))ExifEnumProperty_1_set__Value_m8_2084_gshared)(__this, ___value, method)
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.PhotometricInterpretation>::get_Value()
extern "C" uint16_t ExifEnumProperty_1_get_Value_m8_2085_gshared (ExifEnumProperty_1_t8_339 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get_Value_m8_2085(__this, method) (( uint16_t (*) (ExifEnumProperty_1_t8_339 *, const MethodInfo*))ExifEnumProperty_1_get_Value_m8_2085_gshared)(__this, method)
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.PhotometricInterpretation>::set_Value(T)
extern "C" void ExifEnumProperty_1_set_Value_m8_2086_gshared (ExifEnumProperty_1_t8_339 * __this, uint16_t ___value, const MethodInfo* method);
#define ExifEnumProperty_1_set_Value_m8_2086(__this, ___value, method) (( void (*) (ExifEnumProperty_1_t8_339 *, uint16_t, const MethodInfo*))ExifEnumProperty_1_set_Value_m8_2086_gshared)(__this, ___value, method)
// System.Boolean ExifLibrary.ExifEnumProperty`1<ExifLibrary.PhotometricInterpretation>::get_IsBitField()
extern "C" bool ExifEnumProperty_1_get_IsBitField_m8_2087_gshared (ExifEnumProperty_1_t8_339 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get_IsBitField_m8_2087(__this, method) (( bool (*) (ExifEnumProperty_1_t8_339 *, const MethodInfo*))ExifEnumProperty_1_get_IsBitField_m8_2087_gshared)(__this, method)
// System.String ExifLibrary.ExifEnumProperty`1<ExifLibrary.PhotometricInterpretation>::ToString()
extern "C" String_t* ExifEnumProperty_1_ToString_m8_2088_gshared (ExifEnumProperty_1_t8_339 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_ToString_m8_2088(__this, method) (( String_t* (*) (ExifEnumProperty_1_t8_339 *, const MethodInfo*))ExifEnumProperty_1_ToString_m8_2088_gshared)(__this, method)
// ExifLibrary.ExifInterOperability ExifLibrary.ExifEnumProperty`1<ExifLibrary.PhotometricInterpretation>::get_Interoperability()
extern "C" ExifInterOperability_t8_113  ExifEnumProperty_1_get_Interoperability_m8_2089_gshared (ExifEnumProperty_1_t8_339 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get_Interoperability_m8_2089(__this, method) (( ExifInterOperability_t8_113  (*) (ExifEnumProperty_1_t8_339 *, const MethodInfo*))ExifEnumProperty_1_get_Interoperability_m8_2089_gshared)(__this, method)
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.PhotometricInterpretation>::op_Implicit(ExifLibrary.ExifEnumProperty`1<T>)
extern "C" uint16_t ExifEnumProperty_1_op_Implicit_m8_2090_gshared (Object_t * __this /* static, unused */, ExifEnumProperty_1_t8_339 * ___obj, const MethodInfo* method);
#define ExifEnumProperty_1_op_Implicit_m8_2090(__this /* static, unused */, ___obj, method) (( uint16_t (*) (Object_t * /* static, unused */, ExifEnumProperty_1_t8_339 *, const MethodInfo*))ExifEnumProperty_1_op_Implicit_m8_2090_gshared)(__this /* static, unused */, ___obj, method)
