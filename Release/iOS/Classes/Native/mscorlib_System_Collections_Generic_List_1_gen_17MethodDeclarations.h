﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.UILineInfo>
struct List_1_t1_1826;
// System.Collections.Generic.IEnumerable`1<UnityEngine.UILineInfo>
struct IEnumerable_1_t1_2821;
// UnityEngine.UILineInfo[]
struct UILineInfoU5BU5D_t6_289;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UILineInfo>
struct IEnumerator_1_t1_2822;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<UnityEngine.UILineInfo>
struct ICollection_1_t1_1893;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>
struct ReadOnlyCollection_1_t1_2372;
// System.Collections.Generic.IComparer`1<UnityEngine.UILineInfo>
struct IComparer_1_t1_2823;
// System.Predicate`1<UnityEngine.UILineInfo>
struct Predicate_1_t1_2378;
// System.Action`1<UnityEngine.UILineInfo>
struct Action_1_t1_2379;
// System.Comparison`1<UnityEngine.UILineInfo>
struct Comparison_1_t1_2380;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_UILineInfo.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::.ctor()
extern "C" void List_1__ctor_m1_20623_gshared (List_1_t1_1826 * __this, const MethodInfo* method);
#define List_1__ctor_m1_20623(__this, method) (( void (*) (List_1_t1_1826 *, const MethodInfo*))List_1__ctor_m1_20623_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m1_20624_gshared (List_1_t1_1826 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m1_20624(__this, ___collection, method) (( void (*) (List_1_t1_1826 *, Object_t*, const MethodInfo*))List_1__ctor_m1_20624_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::.ctor(System.Int32)
extern "C" void List_1__ctor_m1_14955_gshared (List_1_t1_1826 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m1_14955(__this, ___capacity, method) (( void (*) (List_1_t1_1826 *, int32_t, const MethodInfo*))List_1__ctor_m1_14955_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::.ctor(T[],System.Int32)
extern "C" void List_1__ctor_m1_20625_gshared (List_1_t1_1826 * __this, UILineInfoU5BU5D_t6_289* ___data, int32_t ___size, const MethodInfo* method);
#define List_1__ctor_m1_20625(__this, ___data, ___size, method) (( void (*) (List_1_t1_1826 *, UILineInfoU5BU5D_t6_289*, int32_t, const MethodInfo*))List_1__ctor_m1_20625_gshared)(__this, ___data, ___size, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::.cctor()
extern "C" void List_1__cctor_m1_20626_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m1_20626(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m1_20626_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_20627_gshared (List_1_t1_1826 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_20627(__this, method) (( Object_t* (*) (List_1_t1_1826 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_20627_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m1_20628_gshared (List_1_t1_1826 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m1_20628(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_1826 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m1_20628_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m1_20629_gshared (List_1_t1_1826 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1_20629(__this, method) (( Object_t * (*) (List_1_t1_1826 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1_20629_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m1_20630_gshared (List_1_t1_1826 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m1_20630(__this, ___item, method) (( int32_t (*) (List_1_t1_1826 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m1_20630_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m1_20631_gshared (List_1_t1_1826 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m1_20631(__this, ___item, method) (( bool (*) (List_1_t1_1826 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1_20631_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m1_20632_gshared (List_1_t1_1826 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m1_20632(__this, ___item, method) (( int32_t (*) (List_1_t1_1826 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m1_20632_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m1_20633_gshared (List_1_t1_1826 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m1_20633(__this, ___index, ___item, method) (( void (*) (List_1_t1_1826 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m1_20633_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m1_20634_gshared (List_1_t1_1826 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m1_20634(__this, ___item, method) (( void (*) (List_1_t1_1826 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1_20634_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_20635_gshared (List_1_t1_1826 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_20635(__this, method) (( bool (*) (List_1_t1_1826 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_20635_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m1_20636_gshared (List_1_t1_1826 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m1_20636(__this, method) (( bool (*) (List_1_t1_1826 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m1_20636_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m1_20637_gshared (List_1_t1_1826 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m1_20637(__this, method) (( Object_t * (*) (List_1_t1_1826 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1_20637_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m1_20638_gshared (List_1_t1_1826 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m1_20638(__this, method) (( bool (*) (List_1_t1_1826 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m1_20638_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m1_20639_gshared (List_1_t1_1826 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m1_20639(__this, method) (( bool (*) (List_1_t1_1826 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m1_20639_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m1_20640_gshared (List_1_t1_1826 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m1_20640(__this, ___index, method) (( Object_t * (*) (List_1_t1_1826 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m1_20640_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m1_20641_gshared (List_1_t1_1826 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m1_20641(__this, ___index, ___value, method) (( void (*) (List_1_t1_1826 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m1_20641_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Add(T)
extern "C" void List_1_Add_m1_20642_gshared (List_1_t1_1826 * __this, UILineInfo_t6_151  ___item, const MethodInfo* method);
#define List_1_Add_m1_20642(__this, ___item, method) (( void (*) (List_1_t1_1826 *, UILineInfo_t6_151 , const MethodInfo*))List_1_Add_m1_20642_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m1_20643_gshared (List_1_t1_1826 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m1_20643(__this, ___newCount, method) (( void (*) (List_1_t1_1826 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m1_20643_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::CheckRange(System.Int32,System.Int32)
extern "C" void List_1_CheckRange_m1_20644_gshared (List_1_t1_1826 * __this, int32_t ___idx, int32_t ___count, const MethodInfo* method);
#define List_1_CheckRange_m1_20644(__this, ___idx, ___count, method) (( void (*) (List_1_t1_1826 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m1_20644_gshared)(__this, ___idx, ___count, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m1_20645_gshared (List_1_t1_1826 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m1_20645(__this, ___collection, method) (( void (*) (List_1_t1_1826 *, Object_t*, const MethodInfo*))List_1_AddCollection_m1_20645_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m1_20646_gshared (List_1_t1_1826 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m1_20646(__this, ___enumerable, method) (( void (*) (List_1_t1_1826 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m1_20646_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m1_20647_gshared (List_1_t1_1826 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m1_20647(__this, ___collection, method) (( void (*) (List_1_t1_1826 *, Object_t*, const MethodInfo*))List_1_AddRange_m1_20647_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.UILineInfo>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t1_2372 * List_1_AsReadOnly_m1_20648_gshared (List_1_t1_1826 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m1_20648(__this, method) (( ReadOnlyCollection_1_t1_2372 * (*) (List_1_t1_1826 *, const MethodInfo*))List_1_AsReadOnly_m1_20648_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::BinarySearch(T)
extern "C" int32_t List_1_BinarySearch_m1_20649_gshared (List_1_t1_1826 * __this, UILineInfo_t6_151  ___item, const MethodInfo* method);
#define List_1_BinarySearch_m1_20649(__this, ___item, method) (( int32_t (*) (List_1_t1_1826 *, UILineInfo_t6_151 , const MethodInfo*))List_1_BinarySearch_m1_20649_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::BinarySearch(T,System.Collections.Generic.IComparer`1<T>)
extern "C" int32_t List_1_BinarySearch_m1_20650_gshared (List_1_t1_1826 * __this, UILineInfo_t6_151  ___item, Object_t* ___comparer, const MethodInfo* method);
#define List_1_BinarySearch_m1_20650(__this, ___item, ___comparer, method) (( int32_t (*) (List_1_t1_1826 *, UILineInfo_t6_151 , Object_t*, const MethodInfo*))List_1_BinarySearch_m1_20650_gshared)(__this, ___item, ___comparer, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::BinarySearch(System.Int32,System.Int32,T,System.Collections.Generic.IComparer`1<T>)
extern "C" int32_t List_1_BinarySearch_m1_20651_gshared (List_1_t1_1826 * __this, int32_t ___index, int32_t ___count, UILineInfo_t6_151  ___item, Object_t* ___comparer, const MethodInfo* method);
#define List_1_BinarySearch_m1_20651(__this, ___index, ___count, ___item, ___comparer, method) (( int32_t (*) (List_1_t1_1826 *, int32_t, int32_t, UILineInfo_t6_151 , Object_t*, const MethodInfo*))List_1_BinarySearch_m1_20651_gshared)(__this, ___index, ___count, ___item, ___comparer, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Clear()
extern "C" void List_1_Clear_m1_20652_gshared (List_1_t1_1826 * __this, const MethodInfo* method);
#define List_1_Clear_m1_20652(__this, method) (( void (*) (List_1_t1_1826 *, const MethodInfo*))List_1_Clear_m1_20652_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Contains(T)
extern "C" bool List_1_Contains_m1_20653_gshared (List_1_t1_1826 * __this, UILineInfo_t6_151  ___item, const MethodInfo* method);
#define List_1_Contains_m1_20653(__this, ___item, method) (( bool (*) (List_1_t1_1826 *, UILineInfo_t6_151 , const MethodInfo*))List_1_Contains_m1_20653_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::CopyTo(T[])
extern "C" void List_1_CopyTo_m1_20654_gshared (List_1_t1_1826 * __this, UILineInfoU5BU5D_t6_289* ___array, const MethodInfo* method);
#define List_1_CopyTo_m1_20654(__this, ___array, method) (( void (*) (List_1_t1_1826 *, UILineInfoU5BU5D_t6_289*, const MethodInfo*))List_1_CopyTo_m1_20654_gshared)(__this, ___array, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m1_20655_gshared (List_1_t1_1826 * __this, UILineInfoU5BU5D_t6_289* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m1_20655(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_1826 *, UILineInfoU5BU5D_t6_289*, int32_t, const MethodInfo*))List_1_CopyTo_m1_20655_gshared)(__this, ___array, ___arrayIndex, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::CopyTo(System.Int32,T[],System.Int32,System.Int32)
extern "C" void List_1_CopyTo_m1_20656_gshared (List_1_t1_1826 * __this, int32_t ___index, UILineInfoU5BU5D_t6_289* ___array, int32_t ___arrayIndex, int32_t ___count, const MethodInfo* method);
#define List_1_CopyTo_m1_20656(__this, ___index, ___array, ___arrayIndex, ___count, method) (( void (*) (List_1_t1_1826 *, int32_t, UILineInfoU5BU5D_t6_289*, int32_t, int32_t, const MethodInfo*))List_1_CopyTo_m1_20656_gshared)(__this, ___index, ___array, ___arrayIndex, ___count, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Exists(System.Predicate`1<T>)
extern "C" bool List_1_Exists_m1_20657_gshared (List_1_t1_1826 * __this, Predicate_1_t1_2378 * ___match, const MethodInfo* method);
#define List_1_Exists_m1_20657(__this, ___match, method) (( bool (*) (List_1_t1_1826 *, Predicate_1_t1_2378 *, const MethodInfo*))List_1_Exists_m1_20657_gshared)(__this, ___match, method)
// T System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Find(System.Predicate`1<T>)
extern "C" UILineInfo_t6_151  List_1_Find_m1_20658_gshared (List_1_t1_1826 * __this, Predicate_1_t1_2378 * ___match, const MethodInfo* method);
#define List_1_Find_m1_20658(__this, ___match, method) (( UILineInfo_t6_151  (*) (List_1_t1_1826 *, Predicate_1_t1_2378 *, const MethodInfo*))List_1_Find_m1_20658_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m1_20659_gshared (Object_t * __this /* static, unused */, Predicate_1_t1_2378 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m1_20659(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t1_2378 *, const MethodInfo*))List_1_CheckMatch_m1_20659_gshared)(__this /* static, unused */, ___match, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<UnityEngine.UILineInfo>::FindAll(System.Predicate`1<T>)
extern "C" List_1_t1_1826 * List_1_FindAll_m1_20660_gshared (List_1_t1_1826 * __this, Predicate_1_t1_2378 * ___match, const MethodInfo* method);
#define List_1_FindAll_m1_20660(__this, ___match, method) (( List_1_t1_1826 * (*) (List_1_t1_1826 *, Predicate_1_t1_2378 *, const MethodInfo*))List_1_FindAll_m1_20660_gshared)(__this, ___match, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<UnityEngine.UILineInfo>::FindAllStackBits(System.Predicate`1<T>)
extern "C" List_1_t1_1826 * List_1_FindAllStackBits_m1_20661_gshared (List_1_t1_1826 * __this, Predicate_1_t1_2378 * ___match, const MethodInfo* method);
#define List_1_FindAllStackBits_m1_20661(__this, ___match, method) (( List_1_t1_1826 * (*) (List_1_t1_1826 *, Predicate_1_t1_2378 *, const MethodInfo*))List_1_FindAllStackBits_m1_20661_gshared)(__this, ___match, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<UnityEngine.UILineInfo>::FindAllList(System.Predicate`1<T>)
extern "C" List_1_t1_1826 * List_1_FindAllList_m1_20662_gshared (List_1_t1_1826 * __this, Predicate_1_t1_2378 * ___match, const MethodInfo* method);
#define List_1_FindAllList_m1_20662(__this, ___match, method) (( List_1_t1_1826 * (*) (List_1_t1_1826 *, Predicate_1_t1_2378 *, const MethodInfo*))List_1_FindAllList_m1_20662_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::FindIndex(System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m1_20663_gshared (List_1_t1_1826 * __this, Predicate_1_t1_2378 * ___match, const MethodInfo* method);
#define List_1_FindIndex_m1_20663(__this, ___match, method) (( int32_t (*) (List_1_t1_1826 *, Predicate_1_t1_2378 *, const MethodInfo*))List_1_FindIndex_m1_20663_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::FindIndex(System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m1_20664_gshared (List_1_t1_1826 * __this, int32_t ___startIndex, Predicate_1_t1_2378 * ___match, const MethodInfo* method);
#define List_1_FindIndex_m1_20664(__this, ___startIndex, ___match, method) (( int32_t (*) (List_1_t1_1826 *, int32_t, Predicate_1_t1_2378 *, const MethodInfo*))List_1_FindIndex_m1_20664_gshared)(__this, ___startIndex, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::FindIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m1_20665_gshared (List_1_t1_1826 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_2378 * ___match, const MethodInfo* method);
#define List_1_FindIndex_m1_20665(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_1826 *, int32_t, int32_t, Predicate_1_t1_2378 *, const MethodInfo*))List_1_FindIndex_m1_20665_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m1_20666_gshared (List_1_t1_1826 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_2378 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m1_20666(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_1826 *, int32_t, int32_t, Predicate_1_t1_2378 *, const MethodInfo*))List_1_GetIndex_m1_20666_gshared)(__this, ___startIndex, ___count, ___match, method)
// T System.Collections.Generic.List`1<UnityEngine.UILineInfo>::FindLast(System.Predicate`1<T>)
extern "C" UILineInfo_t6_151  List_1_FindLast_m1_20667_gshared (List_1_t1_1826 * __this, Predicate_1_t1_2378 * ___match, const MethodInfo* method);
#define List_1_FindLast_m1_20667(__this, ___match, method) (( UILineInfo_t6_151  (*) (List_1_t1_1826 *, Predicate_1_t1_2378 *, const MethodInfo*))List_1_FindLast_m1_20667_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::FindLastIndex(System.Predicate`1<T>)
extern "C" int32_t List_1_FindLastIndex_m1_20668_gshared (List_1_t1_1826 * __this, Predicate_1_t1_2378 * ___match, const MethodInfo* method);
#define List_1_FindLastIndex_m1_20668(__this, ___match, method) (( int32_t (*) (List_1_t1_1826 *, Predicate_1_t1_2378 *, const MethodInfo*))List_1_FindLastIndex_m1_20668_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::FindLastIndex(System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindLastIndex_m1_20669_gshared (List_1_t1_1826 * __this, int32_t ___startIndex, Predicate_1_t1_2378 * ___match, const MethodInfo* method);
#define List_1_FindLastIndex_m1_20669(__this, ___startIndex, ___match, method) (( int32_t (*) (List_1_t1_1826 *, int32_t, Predicate_1_t1_2378 *, const MethodInfo*))List_1_FindLastIndex_m1_20669_gshared)(__this, ___startIndex, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::FindLastIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindLastIndex_m1_20670_gshared (List_1_t1_1826 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_2378 * ___match, const MethodInfo* method);
#define List_1_FindLastIndex_m1_20670(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_1826 *, int32_t, int32_t, Predicate_1_t1_2378 *, const MethodInfo*))List_1_FindLastIndex_m1_20670_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::GetLastIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetLastIndex_m1_20671_gshared (List_1_t1_1826 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_2378 * ___match, const MethodInfo* method);
#define List_1_GetLastIndex_m1_20671(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_1826 *, int32_t, int32_t, Predicate_1_t1_2378 *, const MethodInfo*))List_1_GetLastIndex_m1_20671_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::ForEach(System.Action`1<T>)
extern "C" void List_1_ForEach_m1_20672_gshared (List_1_t1_1826 * __this, Action_1_t1_2379 * ___action, const MethodInfo* method);
#define List_1_ForEach_m1_20672(__this, ___action, method) (( void (*) (List_1_t1_1826 *, Action_1_t1_2379 *, const MethodInfo*))List_1_ForEach_m1_20672_gshared)(__this, ___action, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.UILineInfo>::GetEnumerator()
extern "C" Enumerator_t1_2371  List_1_GetEnumerator_m1_20673_gshared (List_1_t1_1826 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m1_20673(__this, method) (( Enumerator_t1_2371  (*) (List_1_t1_1826 *, const MethodInfo*))List_1_GetEnumerator_m1_20673_gshared)(__this, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<UnityEngine.UILineInfo>::GetRange(System.Int32,System.Int32)
extern "C" List_1_t1_1826 * List_1_GetRange_m1_20674_gshared (List_1_t1_1826 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_GetRange_m1_20674(__this, ___index, ___count, method) (( List_1_t1_1826 * (*) (List_1_t1_1826 *, int32_t, int32_t, const MethodInfo*))List_1_GetRange_m1_20674_gshared)(__this, ___index, ___count, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m1_20675_gshared (List_1_t1_1826 * __this, UILineInfo_t6_151  ___item, const MethodInfo* method);
#define List_1_IndexOf_m1_20675(__this, ___item, method) (( int32_t (*) (List_1_t1_1826 *, UILineInfo_t6_151 , const MethodInfo*))List_1_IndexOf_m1_20675_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::IndexOf(T,System.Int32)
extern "C" int32_t List_1_IndexOf_m1_20676_gshared (List_1_t1_1826 * __this, UILineInfo_t6_151  ___item, int32_t ___index, const MethodInfo* method);
#define List_1_IndexOf_m1_20676(__this, ___item, ___index, method) (( int32_t (*) (List_1_t1_1826 *, UILineInfo_t6_151 , int32_t, const MethodInfo*))List_1_IndexOf_m1_20676_gshared)(__this, ___item, ___index, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::IndexOf(T,System.Int32,System.Int32)
extern "C" int32_t List_1_IndexOf_m1_20677_gshared (List_1_t1_1826 * __this, UILineInfo_t6_151  ___item, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_IndexOf_m1_20677(__this, ___item, ___index, ___count, method) (( int32_t (*) (List_1_t1_1826 *, UILineInfo_t6_151 , int32_t, int32_t, const MethodInfo*))List_1_IndexOf_m1_20677_gshared)(__this, ___item, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m1_20678_gshared (List_1_t1_1826 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m1_20678(__this, ___start, ___delta, method) (( void (*) (List_1_t1_1826 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m1_20678_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m1_20679_gshared (List_1_t1_1826 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m1_20679(__this, ___index, method) (( void (*) (List_1_t1_1826 *, int32_t, const MethodInfo*))List_1_CheckIndex_m1_20679_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m1_20680_gshared (List_1_t1_1826 * __this, int32_t ___index, UILineInfo_t6_151  ___item, const MethodInfo* method);
#define List_1_Insert_m1_20680(__this, ___index, ___item, method) (( void (*) (List_1_t1_1826 *, int32_t, UILineInfo_t6_151 , const MethodInfo*))List_1_Insert_m1_20680_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m1_20681_gshared (List_1_t1_1826 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m1_20681(__this, ___collection, method) (( void (*) (List_1_t1_1826 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m1_20681_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::InsertRange(System.Int32,System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_InsertRange_m1_20682_gshared (List_1_t1_1826 * __this, int32_t ___index, Object_t* ___collection, const MethodInfo* method);
#define List_1_InsertRange_m1_20682(__this, ___index, ___collection, method) (( void (*) (List_1_t1_1826 *, int32_t, Object_t*, const MethodInfo*))List_1_InsertRange_m1_20682_gshared)(__this, ___index, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::InsertCollection(System.Int32,System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_InsertCollection_m1_20683_gshared (List_1_t1_1826 * __this, int32_t ___index, Object_t* ___collection, const MethodInfo* method);
#define List_1_InsertCollection_m1_20683(__this, ___index, ___collection, method) (( void (*) (List_1_t1_1826 *, int32_t, Object_t*, const MethodInfo*))List_1_InsertCollection_m1_20683_gshared)(__this, ___index, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::InsertEnumeration(System.Int32,System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_InsertEnumeration_m1_20684_gshared (List_1_t1_1826 * __this, int32_t ___index, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_InsertEnumeration_m1_20684(__this, ___index, ___enumerable, method) (( void (*) (List_1_t1_1826 *, int32_t, Object_t*, const MethodInfo*))List_1_InsertEnumeration_m1_20684_gshared)(__this, ___index, ___enumerable, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::LastIndexOf(T)
extern "C" int32_t List_1_LastIndexOf_m1_20685_gshared (List_1_t1_1826 * __this, UILineInfo_t6_151  ___item, const MethodInfo* method);
#define List_1_LastIndexOf_m1_20685(__this, ___item, method) (( int32_t (*) (List_1_t1_1826 *, UILineInfo_t6_151 , const MethodInfo*))List_1_LastIndexOf_m1_20685_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::LastIndexOf(T,System.Int32)
extern "C" int32_t List_1_LastIndexOf_m1_20686_gshared (List_1_t1_1826 * __this, UILineInfo_t6_151  ___item, int32_t ___index, const MethodInfo* method);
#define List_1_LastIndexOf_m1_20686(__this, ___item, ___index, method) (( int32_t (*) (List_1_t1_1826 *, UILineInfo_t6_151 , int32_t, const MethodInfo*))List_1_LastIndexOf_m1_20686_gshared)(__this, ___item, ___index, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::LastIndexOf(T,System.Int32,System.Int32)
extern "C" int32_t List_1_LastIndexOf_m1_20687_gshared (List_1_t1_1826 * __this, UILineInfo_t6_151  ___item, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_LastIndexOf_m1_20687(__this, ___item, ___index, ___count, method) (( int32_t (*) (List_1_t1_1826 *, UILineInfo_t6_151 , int32_t, int32_t, const MethodInfo*))List_1_LastIndexOf_m1_20687_gshared)(__this, ___item, ___index, ___count, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Remove(T)
extern "C" bool List_1_Remove_m1_20688_gshared (List_1_t1_1826 * __this, UILineInfo_t6_151  ___item, const MethodInfo* method);
#define List_1_Remove_m1_20688(__this, ___item, method) (( bool (*) (List_1_t1_1826 *, UILineInfo_t6_151 , const MethodInfo*))List_1_Remove_m1_20688_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m1_20689_gshared (List_1_t1_1826 * __this, Predicate_1_t1_2378 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m1_20689(__this, ___match, method) (( int32_t (*) (List_1_t1_1826 *, Predicate_1_t1_2378 *, const MethodInfo*))List_1_RemoveAll_m1_20689_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m1_20690_gshared (List_1_t1_1826 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m1_20690(__this, ___index, method) (( void (*) (List_1_t1_1826 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1_20690_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::RemoveRange(System.Int32,System.Int32)
extern "C" void List_1_RemoveRange_m1_20691_gshared (List_1_t1_1826 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_RemoveRange_m1_20691(__this, ___index, ___count, method) (( void (*) (List_1_t1_1826 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m1_20691_gshared)(__this, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Reverse()
extern "C" void List_1_Reverse_m1_20692_gshared (List_1_t1_1826 * __this, const MethodInfo* method);
#define List_1_Reverse_m1_20692(__this, method) (( void (*) (List_1_t1_1826 *, const MethodInfo*))List_1_Reverse_m1_20692_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Reverse(System.Int32,System.Int32)
extern "C" void List_1_Reverse_m1_20693_gshared (List_1_t1_1826 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_Reverse_m1_20693(__this, ___index, ___count, method) (( void (*) (List_1_t1_1826 *, int32_t, int32_t, const MethodInfo*))List_1_Reverse_m1_20693_gshared)(__this, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Sort()
extern "C" void List_1_Sort_m1_20694_gshared (List_1_t1_1826 * __this, const MethodInfo* method);
#define List_1_Sort_m1_20694(__this, method) (( void (*) (List_1_t1_1826 *, const MethodInfo*))List_1_Sort_m1_20694_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C" void List_1_Sort_m1_20695_gshared (List_1_t1_1826 * __this, Object_t* ___comparer, const MethodInfo* method);
#define List_1_Sort_m1_20695(__this, ___comparer, method) (( void (*) (List_1_t1_1826 *, Object_t*, const MethodInfo*))List_1_Sort_m1_20695_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m1_20696_gshared (List_1_t1_1826 * __this, Comparison_1_t1_2380 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m1_20696(__this, ___comparison, method) (( void (*) (List_1_t1_1826 *, Comparison_1_t1_2380 *, const MethodInfo*))List_1_Sort_m1_20696_gshared)(__this, ___comparison, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Sort(System.Int32,System.Int32,System.Collections.Generic.IComparer`1<T>)
extern "C" void List_1_Sort_m1_20697_gshared (List_1_t1_1826 * __this, int32_t ___index, int32_t ___count, Object_t* ___comparer, const MethodInfo* method);
#define List_1_Sort_m1_20697(__this, ___index, ___count, ___comparer, method) (( void (*) (List_1_t1_1826 *, int32_t, int32_t, Object_t*, const MethodInfo*))List_1_Sort_m1_20697_gshared)(__this, ___index, ___count, ___comparer, method)
// T[] System.Collections.Generic.List`1<UnityEngine.UILineInfo>::ToArray()
extern "C" UILineInfoU5BU5D_t6_289* List_1_ToArray_m1_20698_gshared (List_1_t1_1826 * __this, const MethodInfo* method);
#define List_1_ToArray_m1_20698(__this, method) (( UILineInfoU5BU5D_t6_289* (*) (List_1_t1_1826 *, const MethodInfo*))List_1_ToArray_m1_20698_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::TrimExcess()
extern "C" void List_1_TrimExcess_m1_20699_gshared (List_1_t1_1826 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m1_20699(__this, method) (( void (*) (List_1_t1_1826 *, const MethodInfo*))List_1_TrimExcess_m1_20699_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::TrueForAll(System.Predicate`1<T>)
extern "C" bool List_1_TrueForAll_m1_20700_gshared (List_1_t1_1826 * __this, Predicate_1_t1_2378 * ___match, const MethodInfo* method);
#define List_1_TrueForAll_m1_20700(__this, ___match, method) (( bool (*) (List_1_t1_1826 *, Predicate_1_t1_2378 *, const MethodInfo*))List_1_TrueForAll_m1_20700_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m1_20701_gshared (List_1_t1_1826 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m1_20701(__this, method) (( int32_t (*) (List_1_t1_1826 *, const MethodInfo*))List_1_get_Capacity_m1_20701_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m1_20702_gshared (List_1_t1_1826 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m1_20702(__this, ___value, method) (( void (*) (List_1_t1_1826 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1_20702_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::get_Count()
extern "C" int32_t List_1_get_Count_m1_20703_gshared (List_1_t1_1826 * __this, const MethodInfo* method);
#define List_1_get_Count_m1_20703(__this, method) (( int32_t (*) (List_1_t1_1826 *, const MethodInfo*))List_1_get_Count_m1_20703_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.UILineInfo>::get_Item(System.Int32)
extern "C" UILineInfo_t6_151  List_1_get_Item_m1_20704_gshared (List_1_t1_1826 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m1_20704(__this, ___index, method) (( UILineInfo_t6_151  (*) (List_1_t1_1826 *, int32_t, const MethodInfo*))List_1_get_Item_m1_20704_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m1_20705_gshared (List_1_t1_1826 * __this, int32_t ___index, UILineInfo_t6_151  ___value, const MethodInfo* method);
#define List_1_set_Item_m1_20705(__this, ___index, ___value, method) (( void (*) (List_1_t1_1826 *, int32_t, UILineInfo_t6_151 , const MethodInfo*))List_1_set_Item_m1_20705_gshared)(__this, ___index, ___value, method)
