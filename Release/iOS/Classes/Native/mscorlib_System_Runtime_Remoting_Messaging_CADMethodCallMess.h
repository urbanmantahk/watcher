﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Runtime_Remoting_Messaging_CADMessageBase.h"
#include "mscorlib_System_RuntimeMethodHandle.h"

// System.Runtime.Remoting.Messaging.CADMethodCallMessage
struct  CADMethodCallMessage_t1_925  : public CADMessageBase_t1_924
{
	// System.String System.Runtime.Remoting.Messaging.CADMethodCallMessage::_uri
	String_t* ____uri_4;
	// System.RuntimeMethodHandle System.Runtime.Remoting.Messaging.CADMethodCallMessage::MethodHandle
	RuntimeMethodHandle_t1_479  ___MethodHandle_5;
	// System.String System.Runtime.Remoting.Messaging.CADMethodCallMessage::FullTypeName
	String_t* ___FullTypeName_6;
};
