﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.UI/LoginPromptCompletion
struct LoginPromptCompletion_t8_302;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_String.h"

// System.Void VoxelBusters.NativePlugins.UI/LoginPromptCompletion::.ctor(System.Object,System.IntPtr)
extern "C" void LoginPromptCompletion__ctor_m8_1756 (LoginPromptCompletion_t8_302 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.UI/LoginPromptCompletion::Invoke(System.String,System.String,System.String)
extern "C" void LoginPromptCompletion_Invoke_m8_1757 (LoginPromptCompletion_t8_302 * __this, String_t* ____buttonPressed, String_t* ____usernameText, String_t* ____passwordText, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_LoginPromptCompletion_t8_302(Il2CppObject* delegate, String_t* ____buttonPressed, String_t* ____usernameText, String_t* ____passwordText);
// System.IAsyncResult VoxelBusters.NativePlugins.UI/LoginPromptCompletion::BeginInvoke(System.String,System.String,System.String,System.AsyncCallback,System.Object)
extern "C" Object_t * LoginPromptCompletion_BeginInvoke_m8_1758 (LoginPromptCompletion_t8_302 * __this, String_t* ____buttonPressed, String_t* ____usernameText, String_t* ____passwordText, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.UI/LoginPromptCompletion::EndInvoke(System.IAsyncResult)
extern "C" void LoginPromptCompletion_EndInvoke_m8_1759 (LoginPromptCompletion_t8_302 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
