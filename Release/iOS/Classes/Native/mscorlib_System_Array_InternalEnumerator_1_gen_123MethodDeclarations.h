﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_123.h"
#include "System_Xml_System_Xml_XmlNamespaceManager_NsDecl.h"

// System.Void System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsDecl>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_18044_gshared (InternalEnumerator_1_t1_2205 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1_18044(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1_2205 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m1_18044_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsDecl>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_18045_gshared (InternalEnumerator_1_t1_2205 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_18045(__this, method) (( void (*) (InternalEnumerator_1_t1_2205 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_18045_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsDecl>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_18046_gshared (InternalEnumerator_1_t1_2205 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_18046(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1_2205 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_18046_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsDecl>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_18047_gshared (InternalEnumerator_1_t1_2205 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1_18047(__this, method) (( void (*) (InternalEnumerator_1_t1_2205 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1_18047_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsDecl>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_18048_gshared (InternalEnumerator_1_t1_2205 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1_18048(__this, method) (( bool (*) (InternalEnumerator_1_t1_2205 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1_18048_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsDecl>::get_Current()
extern "C" NsDecl_t4_141  InternalEnumerator_1_get_Current_m1_18049_gshared (InternalEnumerator_1_t1_2205 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1_18049(__this, method) (( NsDecl_t4_141  (*) (InternalEnumerator_1_t1_2205 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1_18049_gshared)(__this, method)
