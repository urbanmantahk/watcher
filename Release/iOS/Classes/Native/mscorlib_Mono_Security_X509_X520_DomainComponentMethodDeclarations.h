﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.X509.X520/DomainComponent
struct DomainComponent_t1_209;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Security.X509.X520/DomainComponent::.ctor()
extern "C" void DomainComponent__ctor_m1_2409 (DomainComponent_t1_209 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
