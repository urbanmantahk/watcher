﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Xml.DTDNotationDeclaration
struct DTDNotationDeclaration_t4_99;
// Mono.Xml.DTDObjectModel
struct DTDObjectModel_t4_82;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Xml.DTDNotationDeclaration::.ctor(Mono.Xml.DTDObjectModel)
extern "C" void DTDNotationDeclaration__ctor_m4_239 (DTDNotationDeclaration_t4_99 * __this, DTDObjectModel_t4_82 * ___root, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Xml.DTDNotationDeclaration::get_Name()
extern "C" String_t* DTDNotationDeclaration_get_Name_m4_240 (DTDNotationDeclaration_t4_99 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Xml.DTDNotationDeclaration::set_Name(System.String)
extern "C" void DTDNotationDeclaration_set_Name_m4_241 (DTDNotationDeclaration_t4_99 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Xml.DTDNotationDeclaration::get_PublicId()
extern "C" String_t* DTDNotationDeclaration_get_PublicId_m4_242 (DTDNotationDeclaration_t4_99 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Xml.DTDNotationDeclaration::set_PublicId(System.String)
extern "C" void DTDNotationDeclaration_set_PublicId_m4_243 (DTDNotationDeclaration_t4_99 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Xml.DTDNotationDeclaration::get_SystemId()
extern "C" String_t* DTDNotationDeclaration_get_SystemId_m4_244 (DTDNotationDeclaration_t4_99 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Xml.DTDNotationDeclaration::set_SystemId(System.String)
extern "C" void DTDNotationDeclaration_set_SystemId_m4_245 (DTDNotationDeclaration_t4_99 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Xml.DTDNotationDeclaration::get_LocalName()
extern "C" String_t* DTDNotationDeclaration_get_LocalName_m4_246 (DTDNotationDeclaration_t4_99 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Xml.DTDNotationDeclaration::set_LocalName(System.String)
extern "C" void DTDNotationDeclaration_set_LocalName_m4_247 (DTDNotationDeclaration_t4_99 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Xml.DTDNotationDeclaration::get_Prefix()
extern "C" String_t* DTDNotationDeclaration_get_Prefix_m4_248 (DTDNotationDeclaration_t4_99 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Xml.DTDNotationDeclaration::set_Prefix(System.String)
extern "C" void DTDNotationDeclaration_set_Prefix_m4_249 (DTDNotationDeclaration_t4_99 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
