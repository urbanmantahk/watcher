﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Globalization.KoreanLunisolarCalendar
struct KoreanLunisolarCalendar_t1_382;
// System.Int32[]
struct Int32U5BU5D_t1_275;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime.h"

// System.Void System.Globalization.KoreanLunisolarCalendar::.ctor()
extern "C" void KoreanLunisolarCalendar__ctor_m1_4311 (KoreanLunisolarCalendar_t1_382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.KoreanLunisolarCalendar::.cctor()
extern "C" void KoreanLunisolarCalendar__cctor_m1_4312 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] System.Globalization.KoreanLunisolarCalendar::get_Eras()
extern "C" Int32U5BU5D_t1_275* KoreanLunisolarCalendar_get_Eras_m1_4313 (KoreanLunisolarCalendar_t1_382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.KoreanLunisolarCalendar::GetEra(System.DateTime)
extern "C" int32_t KoreanLunisolarCalendar_GetEra_m1_4314 (KoreanLunisolarCalendar_t1_382 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.KoreanLunisolarCalendar::get_MinSupportedDateTime()
extern "C" DateTime_t1_150  KoreanLunisolarCalendar_get_MinSupportedDateTime_m1_4315 (KoreanLunisolarCalendar_t1_382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.KoreanLunisolarCalendar::get_MaxSupportedDateTime()
extern "C" DateTime_t1_150  KoreanLunisolarCalendar_get_MaxSupportedDateTime_m1_4316 (KoreanLunisolarCalendar_t1_382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
