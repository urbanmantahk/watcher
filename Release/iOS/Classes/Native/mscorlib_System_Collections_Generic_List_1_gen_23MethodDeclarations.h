﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t1_1859;
// System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.RaycastResult>
struct IEnumerable_1_t1_2840;
// UnityEngine.EventSystems.RaycastResult[]
struct RaycastResultU5BU5D_t7_205;
// System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.RaycastResult>
struct IEnumerator_1_t1_2841;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.RaycastResult>
struct ICollection_1_t1_2842;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>
struct ReadOnlyCollection_1_t1_2469;
// System.Collections.Generic.IComparer`1<UnityEngine.EventSystems.RaycastResult>
struct IComparer_1_t1_2843;
// System.Predicate`1<UnityEngine.EventSystems.RaycastResult>
struct Predicate_1_t1_2476;
// System.Action`1<UnityEngine.EventSystems.RaycastResult>
struct Action_1_t1_2477;
// System.Comparison`1<UnityEngine.EventSystems.RaycastResult>
struct Comparison_1_t1_1854;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_30.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::.ctor()
extern "C" void List_1__ctor_m1_14983_gshared (List_1_t1_1859 * __this, const MethodInfo* method);
#define List_1__ctor_m1_14983(__this, method) (( void (*) (List_1_t1_1859 *, const MethodInfo*))List_1__ctor_m1_14983_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m1_22266_gshared (List_1_t1_1859 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m1_22266(__this, ___collection, method) (( void (*) (List_1_t1_1859 *, Object_t*, const MethodInfo*))List_1__ctor_m1_22266_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Int32)
extern "C" void List_1__ctor_m1_22267_gshared (List_1_t1_1859 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m1_22267(__this, ___capacity, method) (( void (*) (List_1_t1_1859 *, int32_t, const MethodInfo*))List_1__ctor_m1_22267_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::.ctor(T[],System.Int32)
extern "C" void List_1__ctor_m1_22268_gshared (List_1_t1_1859 * __this, RaycastResultU5BU5D_t7_205* ___data, int32_t ___size, const MethodInfo* method);
#define List_1__ctor_m1_22268(__this, ___data, ___size, method) (( void (*) (List_1_t1_1859 *, RaycastResultU5BU5D_t7_205*, int32_t, const MethodInfo*))List_1__ctor_m1_22268_gshared)(__this, ___data, ___size, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::.cctor()
extern "C" void List_1__cctor_m1_22269_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m1_22269(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m1_22269_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_22270_gshared (List_1_t1_1859 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_22270(__this, method) (( Object_t* (*) (List_1_t1_1859 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_22270_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m1_22271_gshared (List_1_t1_1859 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m1_22271(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_1859 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m1_22271_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m1_22272_gshared (List_1_t1_1859 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1_22272(__this, method) (( Object_t * (*) (List_1_t1_1859 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1_22272_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m1_22273_gshared (List_1_t1_1859 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m1_22273(__this, ___item, method) (( int32_t (*) (List_1_t1_1859 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m1_22273_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m1_22274_gshared (List_1_t1_1859 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m1_22274(__this, ___item, method) (( bool (*) (List_1_t1_1859 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1_22274_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m1_22275_gshared (List_1_t1_1859 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m1_22275(__this, ___item, method) (( int32_t (*) (List_1_t1_1859 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m1_22275_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m1_22276_gshared (List_1_t1_1859 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m1_22276(__this, ___index, ___item, method) (( void (*) (List_1_t1_1859 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m1_22276_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m1_22277_gshared (List_1_t1_1859 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m1_22277(__this, ___item, method) (( void (*) (List_1_t1_1859 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1_22277_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_22278_gshared (List_1_t1_1859 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_22278(__this, method) (( bool (*) (List_1_t1_1859 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_22278_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m1_22279_gshared (List_1_t1_1859 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m1_22279(__this, method) (( bool (*) (List_1_t1_1859 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m1_22279_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m1_22280_gshared (List_1_t1_1859 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m1_22280(__this, method) (( Object_t * (*) (List_1_t1_1859 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1_22280_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m1_22281_gshared (List_1_t1_1859 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m1_22281(__this, method) (( bool (*) (List_1_t1_1859 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m1_22281_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m1_22282_gshared (List_1_t1_1859 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m1_22282(__this, method) (( bool (*) (List_1_t1_1859 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m1_22282_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m1_22283_gshared (List_1_t1_1859 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m1_22283(__this, ___index, method) (( Object_t * (*) (List_1_t1_1859 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m1_22283_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m1_22284_gshared (List_1_t1_1859 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m1_22284(__this, ___index, ___value, method) (( void (*) (List_1_t1_1859 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m1_22284_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Add(T)
extern "C" void List_1_Add_m1_22285_gshared (List_1_t1_1859 * __this, RaycastResult_t7_31  ___item, const MethodInfo* method);
#define List_1_Add_m1_22285(__this, ___item, method) (( void (*) (List_1_t1_1859 *, RaycastResult_t7_31 , const MethodInfo*))List_1_Add_m1_22285_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m1_22286_gshared (List_1_t1_1859 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m1_22286(__this, ___newCount, method) (( void (*) (List_1_t1_1859 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m1_22286_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::CheckRange(System.Int32,System.Int32)
extern "C" void List_1_CheckRange_m1_22287_gshared (List_1_t1_1859 * __this, int32_t ___idx, int32_t ___count, const MethodInfo* method);
#define List_1_CheckRange_m1_22287(__this, ___idx, ___count, method) (( void (*) (List_1_t1_1859 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m1_22287_gshared)(__this, ___idx, ___count, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m1_22288_gshared (List_1_t1_1859 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m1_22288(__this, ___collection, method) (( void (*) (List_1_t1_1859 *, Object_t*, const MethodInfo*))List_1_AddCollection_m1_22288_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m1_22289_gshared (List_1_t1_1859 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m1_22289(__this, ___enumerable, method) (( void (*) (List_1_t1_1859 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m1_22289_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m1_22290_gshared (List_1_t1_1859 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m1_22290(__this, ___collection, method) (( void (*) (List_1_t1_1859 *, Object_t*, const MethodInfo*))List_1_AddRange_m1_22290_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t1_2469 * List_1_AsReadOnly_m1_22291_gshared (List_1_t1_1859 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m1_22291(__this, method) (( ReadOnlyCollection_1_t1_2469 * (*) (List_1_t1_1859 *, const MethodInfo*))List_1_AsReadOnly_m1_22291_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::BinarySearch(T)
extern "C" int32_t List_1_BinarySearch_m1_22292_gshared (List_1_t1_1859 * __this, RaycastResult_t7_31  ___item, const MethodInfo* method);
#define List_1_BinarySearch_m1_22292(__this, ___item, method) (( int32_t (*) (List_1_t1_1859 *, RaycastResult_t7_31 , const MethodInfo*))List_1_BinarySearch_m1_22292_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::BinarySearch(T,System.Collections.Generic.IComparer`1<T>)
extern "C" int32_t List_1_BinarySearch_m1_22293_gshared (List_1_t1_1859 * __this, RaycastResult_t7_31  ___item, Object_t* ___comparer, const MethodInfo* method);
#define List_1_BinarySearch_m1_22293(__this, ___item, ___comparer, method) (( int32_t (*) (List_1_t1_1859 *, RaycastResult_t7_31 , Object_t*, const MethodInfo*))List_1_BinarySearch_m1_22293_gshared)(__this, ___item, ___comparer, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::BinarySearch(System.Int32,System.Int32,T,System.Collections.Generic.IComparer`1<T>)
extern "C" int32_t List_1_BinarySearch_m1_22294_gshared (List_1_t1_1859 * __this, int32_t ___index, int32_t ___count, RaycastResult_t7_31  ___item, Object_t* ___comparer, const MethodInfo* method);
#define List_1_BinarySearch_m1_22294(__this, ___index, ___count, ___item, ___comparer, method) (( int32_t (*) (List_1_t1_1859 *, int32_t, int32_t, RaycastResult_t7_31 , Object_t*, const MethodInfo*))List_1_BinarySearch_m1_22294_gshared)(__this, ___index, ___count, ___item, ___comparer, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Clear()
extern "C" void List_1_Clear_m1_22295_gshared (List_1_t1_1859 * __this, const MethodInfo* method);
#define List_1_Clear_m1_22295(__this, method) (( void (*) (List_1_t1_1859 *, const MethodInfo*))List_1_Clear_m1_22295_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Contains(T)
extern "C" bool List_1_Contains_m1_22296_gshared (List_1_t1_1859 * __this, RaycastResult_t7_31  ___item, const MethodInfo* method);
#define List_1_Contains_m1_22296(__this, ___item, method) (( bool (*) (List_1_t1_1859 *, RaycastResult_t7_31 , const MethodInfo*))List_1_Contains_m1_22296_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::CopyTo(T[])
extern "C" void List_1_CopyTo_m1_22297_gshared (List_1_t1_1859 * __this, RaycastResultU5BU5D_t7_205* ___array, const MethodInfo* method);
#define List_1_CopyTo_m1_22297(__this, ___array, method) (( void (*) (List_1_t1_1859 *, RaycastResultU5BU5D_t7_205*, const MethodInfo*))List_1_CopyTo_m1_22297_gshared)(__this, ___array, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m1_22298_gshared (List_1_t1_1859 * __this, RaycastResultU5BU5D_t7_205* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m1_22298(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_1859 *, RaycastResultU5BU5D_t7_205*, int32_t, const MethodInfo*))List_1_CopyTo_m1_22298_gshared)(__this, ___array, ___arrayIndex, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::CopyTo(System.Int32,T[],System.Int32,System.Int32)
extern "C" void List_1_CopyTo_m1_22299_gshared (List_1_t1_1859 * __this, int32_t ___index, RaycastResultU5BU5D_t7_205* ___array, int32_t ___arrayIndex, int32_t ___count, const MethodInfo* method);
#define List_1_CopyTo_m1_22299(__this, ___index, ___array, ___arrayIndex, ___count, method) (( void (*) (List_1_t1_1859 *, int32_t, RaycastResultU5BU5D_t7_205*, int32_t, int32_t, const MethodInfo*))List_1_CopyTo_m1_22299_gshared)(__this, ___index, ___array, ___arrayIndex, ___count, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Exists(System.Predicate`1<T>)
extern "C" bool List_1_Exists_m1_22300_gshared (List_1_t1_1859 * __this, Predicate_1_t1_2476 * ___match, const MethodInfo* method);
#define List_1_Exists_m1_22300(__this, ___match, method) (( bool (*) (List_1_t1_1859 *, Predicate_1_t1_2476 *, const MethodInfo*))List_1_Exists_m1_22300_gshared)(__this, ___match, method)
// T System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Find(System.Predicate`1<T>)
extern "C" RaycastResult_t7_31  List_1_Find_m1_22301_gshared (List_1_t1_1859 * __this, Predicate_1_t1_2476 * ___match, const MethodInfo* method);
#define List_1_Find_m1_22301(__this, ___match, method) (( RaycastResult_t7_31  (*) (List_1_t1_1859 *, Predicate_1_t1_2476 *, const MethodInfo*))List_1_Find_m1_22301_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m1_22302_gshared (Object_t * __this /* static, unused */, Predicate_1_t1_2476 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m1_22302(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t1_2476 *, const MethodInfo*))List_1_CheckMatch_m1_22302_gshared)(__this /* static, unused */, ___match, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::FindAll(System.Predicate`1<T>)
extern "C" List_1_t1_1859 * List_1_FindAll_m1_22303_gshared (List_1_t1_1859 * __this, Predicate_1_t1_2476 * ___match, const MethodInfo* method);
#define List_1_FindAll_m1_22303(__this, ___match, method) (( List_1_t1_1859 * (*) (List_1_t1_1859 *, Predicate_1_t1_2476 *, const MethodInfo*))List_1_FindAll_m1_22303_gshared)(__this, ___match, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::FindAllStackBits(System.Predicate`1<T>)
extern "C" List_1_t1_1859 * List_1_FindAllStackBits_m1_22304_gshared (List_1_t1_1859 * __this, Predicate_1_t1_2476 * ___match, const MethodInfo* method);
#define List_1_FindAllStackBits_m1_22304(__this, ___match, method) (( List_1_t1_1859 * (*) (List_1_t1_1859 *, Predicate_1_t1_2476 *, const MethodInfo*))List_1_FindAllStackBits_m1_22304_gshared)(__this, ___match, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::FindAllList(System.Predicate`1<T>)
extern "C" List_1_t1_1859 * List_1_FindAllList_m1_22305_gshared (List_1_t1_1859 * __this, Predicate_1_t1_2476 * ___match, const MethodInfo* method);
#define List_1_FindAllList_m1_22305(__this, ___match, method) (( List_1_t1_1859 * (*) (List_1_t1_1859 *, Predicate_1_t1_2476 *, const MethodInfo*))List_1_FindAllList_m1_22305_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::FindIndex(System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m1_22306_gshared (List_1_t1_1859 * __this, Predicate_1_t1_2476 * ___match, const MethodInfo* method);
#define List_1_FindIndex_m1_22306(__this, ___match, method) (( int32_t (*) (List_1_t1_1859 *, Predicate_1_t1_2476 *, const MethodInfo*))List_1_FindIndex_m1_22306_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::FindIndex(System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m1_22307_gshared (List_1_t1_1859 * __this, int32_t ___startIndex, Predicate_1_t1_2476 * ___match, const MethodInfo* method);
#define List_1_FindIndex_m1_22307(__this, ___startIndex, ___match, method) (( int32_t (*) (List_1_t1_1859 *, int32_t, Predicate_1_t1_2476 *, const MethodInfo*))List_1_FindIndex_m1_22307_gshared)(__this, ___startIndex, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::FindIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m1_22308_gshared (List_1_t1_1859 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_2476 * ___match, const MethodInfo* method);
#define List_1_FindIndex_m1_22308(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_1859 *, int32_t, int32_t, Predicate_1_t1_2476 *, const MethodInfo*))List_1_FindIndex_m1_22308_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m1_22309_gshared (List_1_t1_1859 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_2476 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m1_22309(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_1859 *, int32_t, int32_t, Predicate_1_t1_2476 *, const MethodInfo*))List_1_GetIndex_m1_22309_gshared)(__this, ___startIndex, ___count, ___match, method)
// T System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::FindLast(System.Predicate`1<T>)
extern "C" RaycastResult_t7_31  List_1_FindLast_m1_22310_gshared (List_1_t1_1859 * __this, Predicate_1_t1_2476 * ___match, const MethodInfo* method);
#define List_1_FindLast_m1_22310(__this, ___match, method) (( RaycastResult_t7_31  (*) (List_1_t1_1859 *, Predicate_1_t1_2476 *, const MethodInfo*))List_1_FindLast_m1_22310_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::FindLastIndex(System.Predicate`1<T>)
extern "C" int32_t List_1_FindLastIndex_m1_22311_gshared (List_1_t1_1859 * __this, Predicate_1_t1_2476 * ___match, const MethodInfo* method);
#define List_1_FindLastIndex_m1_22311(__this, ___match, method) (( int32_t (*) (List_1_t1_1859 *, Predicate_1_t1_2476 *, const MethodInfo*))List_1_FindLastIndex_m1_22311_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::FindLastIndex(System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindLastIndex_m1_22312_gshared (List_1_t1_1859 * __this, int32_t ___startIndex, Predicate_1_t1_2476 * ___match, const MethodInfo* method);
#define List_1_FindLastIndex_m1_22312(__this, ___startIndex, ___match, method) (( int32_t (*) (List_1_t1_1859 *, int32_t, Predicate_1_t1_2476 *, const MethodInfo*))List_1_FindLastIndex_m1_22312_gshared)(__this, ___startIndex, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::FindLastIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindLastIndex_m1_22313_gshared (List_1_t1_1859 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_2476 * ___match, const MethodInfo* method);
#define List_1_FindLastIndex_m1_22313(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_1859 *, int32_t, int32_t, Predicate_1_t1_2476 *, const MethodInfo*))List_1_FindLastIndex_m1_22313_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::GetLastIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetLastIndex_m1_22314_gshared (List_1_t1_1859 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_2476 * ___match, const MethodInfo* method);
#define List_1_GetLastIndex_m1_22314(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_1859 *, int32_t, int32_t, Predicate_1_t1_2476 *, const MethodInfo*))List_1_GetLastIndex_m1_22314_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::ForEach(System.Action`1<T>)
extern "C" void List_1_ForEach_m1_22315_gshared (List_1_t1_1859 * __this, Action_1_t1_2477 * ___action, const MethodInfo* method);
#define List_1_ForEach_m1_22315(__this, ___action, method) (( void (*) (List_1_t1_1859 *, Action_1_t1_2477 *, const MethodInfo*))List_1_ForEach_m1_22315_gshared)(__this, ___action, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::GetEnumerator()
extern "C" Enumerator_t1_2468  List_1_GetEnumerator_m1_22316_gshared (List_1_t1_1859 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m1_22316(__this, method) (( Enumerator_t1_2468  (*) (List_1_t1_1859 *, const MethodInfo*))List_1_GetEnumerator_m1_22316_gshared)(__this, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::GetRange(System.Int32,System.Int32)
extern "C" List_1_t1_1859 * List_1_GetRange_m1_22317_gshared (List_1_t1_1859 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_GetRange_m1_22317(__this, ___index, ___count, method) (( List_1_t1_1859 * (*) (List_1_t1_1859 *, int32_t, int32_t, const MethodInfo*))List_1_GetRange_m1_22317_gshared)(__this, ___index, ___count, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m1_22318_gshared (List_1_t1_1859 * __this, RaycastResult_t7_31  ___item, const MethodInfo* method);
#define List_1_IndexOf_m1_22318(__this, ___item, method) (( int32_t (*) (List_1_t1_1859 *, RaycastResult_t7_31 , const MethodInfo*))List_1_IndexOf_m1_22318_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::IndexOf(T,System.Int32)
extern "C" int32_t List_1_IndexOf_m1_22319_gshared (List_1_t1_1859 * __this, RaycastResult_t7_31  ___item, int32_t ___index, const MethodInfo* method);
#define List_1_IndexOf_m1_22319(__this, ___item, ___index, method) (( int32_t (*) (List_1_t1_1859 *, RaycastResult_t7_31 , int32_t, const MethodInfo*))List_1_IndexOf_m1_22319_gshared)(__this, ___item, ___index, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::IndexOf(T,System.Int32,System.Int32)
extern "C" int32_t List_1_IndexOf_m1_22320_gshared (List_1_t1_1859 * __this, RaycastResult_t7_31  ___item, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_IndexOf_m1_22320(__this, ___item, ___index, ___count, method) (( int32_t (*) (List_1_t1_1859 *, RaycastResult_t7_31 , int32_t, int32_t, const MethodInfo*))List_1_IndexOf_m1_22320_gshared)(__this, ___item, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m1_22321_gshared (List_1_t1_1859 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m1_22321(__this, ___start, ___delta, method) (( void (*) (List_1_t1_1859 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m1_22321_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m1_22322_gshared (List_1_t1_1859 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m1_22322(__this, ___index, method) (( void (*) (List_1_t1_1859 *, int32_t, const MethodInfo*))List_1_CheckIndex_m1_22322_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m1_22323_gshared (List_1_t1_1859 * __this, int32_t ___index, RaycastResult_t7_31  ___item, const MethodInfo* method);
#define List_1_Insert_m1_22323(__this, ___index, ___item, method) (( void (*) (List_1_t1_1859 *, int32_t, RaycastResult_t7_31 , const MethodInfo*))List_1_Insert_m1_22323_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m1_22324_gshared (List_1_t1_1859 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m1_22324(__this, ___collection, method) (( void (*) (List_1_t1_1859 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m1_22324_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::InsertRange(System.Int32,System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_InsertRange_m1_22325_gshared (List_1_t1_1859 * __this, int32_t ___index, Object_t* ___collection, const MethodInfo* method);
#define List_1_InsertRange_m1_22325(__this, ___index, ___collection, method) (( void (*) (List_1_t1_1859 *, int32_t, Object_t*, const MethodInfo*))List_1_InsertRange_m1_22325_gshared)(__this, ___index, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::InsertCollection(System.Int32,System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_InsertCollection_m1_22326_gshared (List_1_t1_1859 * __this, int32_t ___index, Object_t* ___collection, const MethodInfo* method);
#define List_1_InsertCollection_m1_22326(__this, ___index, ___collection, method) (( void (*) (List_1_t1_1859 *, int32_t, Object_t*, const MethodInfo*))List_1_InsertCollection_m1_22326_gshared)(__this, ___index, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::InsertEnumeration(System.Int32,System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_InsertEnumeration_m1_22327_gshared (List_1_t1_1859 * __this, int32_t ___index, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_InsertEnumeration_m1_22327(__this, ___index, ___enumerable, method) (( void (*) (List_1_t1_1859 *, int32_t, Object_t*, const MethodInfo*))List_1_InsertEnumeration_m1_22327_gshared)(__this, ___index, ___enumerable, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::LastIndexOf(T)
extern "C" int32_t List_1_LastIndexOf_m1_22328_gshared (List_1_t1_1859 * __this, RaycastResult_t7_31  ___item, const MethodInfo* method);
#define List_1_LastIndexOf_m1_22328(__this, ___item, method) (( int32_t (*) (List_1_t1_1859 *, RaycastResult_t7_31 , const MethodInfo*))List_1_LastIndexOf_m1_22328_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::LastIndexOf(T,System.Int32)
extern "C" int32_t List_1_LastIndexOf_m1_22329_gshared (List_1_t1_1859 * __this, RaycastResult_t7_31  ___item, int32_t ___index, const MethodInfo* method);
#define List_1_LastIndexOf_m1_22329(__this, ___item, ___index, method) (( int32_t (*) (List_1_t1_1859 *, RaycastResult_t7_31 , int32_t, const MethodInfo*))List_1_LastIndexOf_m1_22329_gshared)(__this, ___item, ___index, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::LastIndexOf(T,System.Int32,System.Int32)
extern "C" int32_t List_1_LastIndexOf_m1_22330_gshared (List_1_t1_1859 * __this, RaycastResult_t7_31  ___item, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_LastIndexOf_m1_22330(__this, ___item, ___index, ___count, method) (( int32_t (*) (List_1_t1_1859 *, RaycastResult_t7_31 , int32_t, int32_t, const MethodInfo*))List_1_LastIndexOf_m1_22330_gshared)(__this, ___item, ___index, ___count, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Remove(T)
extern "C" bool List_1_Remove_m1_22331_gshared (List_1_t1_1859 * __this, RaycastResult_t7_31  ___item, const MethodInfo* method);
#define List_1_Remove_m1_22331(__this, ___item, method) (( bool (*) (List_1_t1_1859 *, RaycastResult_t7_31 , const MethodInfo*))List_1_Remove_m1_22331_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m1_22332_gshared (List_1_t1_1859 * __this, Predicate_1_t1_2476 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m1_22332(__this, ___match, method) (( int32_t (*) (List_1_t1_1859 *, Predicate_1_t1_2476 *, const MethodInfo*))List_1_RemoveAll_m1_22332_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m1_22333_gshared (List_1_t1_1859 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m1_22333(__this, ___index, method) (( void (*) (List_1_t1_1859 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1_22333_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::RemoveRange(System.Int32,System.Int32)
extern "C" void List_1_RemoveRange_m1_22334_gshared (List_1_t1_1859 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_RemoveRange_m1_22334(__this, ___index, ___count, method) (( void (*) (List_1_t1_1859 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m1_22334_gshared)(__this, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Reverse()
extern "C" void List_1_Reverse_m1_22335_gshared (List_1_t1_1859 * __this, const MethodInfo* method);
#define List_1_Reverse_m1_22335(__this, method) (( void (*) (List_1_t1_1859 *, const MethodInfo*))List_1_Reverse_m1_22335_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Reverse(System.Int32,System.Int32)
extern "C" void List_1_Reverse_m1_22336_gshared (List_1_t1_1859 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_Reverse_m1_22336(__this, ___index, ___count, method) (( void (*) (List_1_t1_1859 *, int32_t, int32_t, const MethodInfo*))List_1_Reverse_m1_22336_gshared)(__this, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Sort()
extern "C" void List_1_Sort_m1_22337_gshared (List_1_t1_1859 * __this, const MethodInfo* method);
#define List_1_Sort_m1_22337(__this, method) (( void (*) (List_1_t1_1859 *, const MethodInfo*))List_1_Sort_m1_22337_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C" void List_1_Sort_m1_22338_gshared (List_1_t1_1859 * __this, Object_t* ___comparer, const MethodInfo* method);
#define List_1_Sort_m1_22338(__this, ___comparer, method) (( void (*) (List_1_t1_1859 *, Object_t*, const MethodInfo*))List_1_Sort_m1_22338_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m1_14978_gshared (List_1_t1_1859 * __this, Comparison_1_t1_1854 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m1_14978(__this, ___comparison, method) (( void (*) (List_1_t1_1859 *, Comparison_1_t1_1854 *, const MethodInfo*))List_1_Sort_m1_14978_gshared)(__this, ___comparison, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Sort(System.Int32,System.Int32,System.Collections.Generic.IComparer`1<T>)
extern "C" void List_1_Sort_m1_22339_gshared (List_1_t1_1859 * __this, int32_t ___index, int32_t ___count, Object_t* ___comparer, const MethodInfo* method);
#define List_1_Sort_m1_22339(__this, ___index, ___count, ___comparer, method) (( void (*) (List_1_t1_1859 *, int32_t, int32_t, Object_t*, const MethodInfo*))List_1_Sort_m1_22339_gshared)(__this, ___index, ___count, ___comparer, method)
// T[] System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::ToArray()
extern "C" RaycastResultU5BU5D_t7_205* List_1_ToArray_m1_22340_gshared (List_1_t1_1859 * __this, const MethodInfo* method);
#define List_1_ToArray_m1_22340(__this, method) (( RaycastResultU5BU5D_t7_205* (*) (List_1_t1_1859 *, const MethodInfo*))List_1_ToArray_m1_22340_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::TrimExcess()
extern "C" void List_1_TrimExcess_m1_22341_gshared (List_1_t1_1859 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m1_22341(__this, method) (( void (*) (List_1_t1_1859 *, const MethodInfo*))List_1_TrimExcess_m1_22341_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::TrueForAll(System.Predicate`1<T>)
extern "C" bool List_1_TrueForAll_m1_22342_gshared (List_1_t1_1859 * __this, Predicate_1_t1_2476 * ___match, const MethodInfo* method);
#define List_1_TrueForAll_m1_22342(__this, ___match, method) (( bool (*) (List_1_t1_1859 *, Predicate_1_t1_2476 *, const MethodInfo*))List_1_TrueForAll_m1_22342_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m1_22343_gshared (List_1_t1_1859 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m1_22343(__this, method) (( int32_t (*) (List_1_t1_1859 *, const MethodInfo*))List_1_get_Capacity_m1_22343_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m1_22344_gshared (List_1_t1_1859 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m1_22344(__this, ___value, method) (( void (*) (List_1_t1_1859 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1_22344_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::get_Count()
extern "C" int32_t List_1_get_Count_m1_22345_gshared (List_1_t1_1859 * __this, const MethodInfo* method);
#define List_1_get_Count_m1_22345(__this, method) (( int32_t (*) (List_1_t1_1859 *, const MethodInfo*))List_1_get_Count_m1_22345_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::get_Item(System.Int32)
extern "C" RaycastResult_t7_31  List_1_get_Item_m1_22346_gshared (List_1_t1_1859 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m1_22346(__this, ___index, method) (( RaycastResult_t7_31  (*) (List_1_t1_1859 *, int32_t, const MethodInfo*))List_1_get_Item_m1_22346_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m1_22347_gshared (List_1_t1_1859 * __this, int32_t ___index, RaycastResult_t7_31  ___value, const MethodInfo* method);
#define List_1_set_Item_m1_22347(__this, ___index, ___value, method) (( void (*) (List_1_t1_1859 *, int32_t, RaycastResult_t7_31 , const MethodInfo*))List_1_set_Item_m1_22347_gshared)(__this, ___index, ___value, method)
