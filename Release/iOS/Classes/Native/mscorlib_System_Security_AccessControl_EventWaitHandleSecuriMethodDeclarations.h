﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.AccessControl.EventWaitHandleSecurity
struct EventWaitHandleSecurity_t1_1152;
// System.Type
struct Type_t;
// System.Security.AccessControl.AccessRule
struct AccessRule_t1_1111;
// System.Security.Principal.IdentityReference
struct IdentityReference_t1_1120;
// System.Security.AccessControl.EventWaitHandleAccessRule
struct EventWaitHandleAccessRule_t1_1149;
// System.Security.AccessControl.AuditRule
struct AuditRule_t1_1119;
// System.Security.AccessControl.EventWaitHandleAuditRule
struct EventWaitHandleAuditRule_t1_1150;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_AccessControl_InheritanceFlags.h"
#include "mscorlib_System_Security_AccessControl_PropagationFlags.h"
#include "mscorlib_System_Security_AccessControl_AccessControlType.h"
#include "mscorlib_System_Security_AccessControl_AuditFlags.h"

// System.Void System.Security.AccessControl.EventWaitHandleSecurity::.ctor()
extern "C" void EventWaitHandleSecurity__ctor_m1_9851 (EventWaitHandleSecurity_t1_1152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Security.AccessControl.EventWaitHandleSecurity::get_AccessRightType()
extern "C" Type_t * EventWaitHandleSecurity_get_AccessRightType_m1_9852 (EventWaitHandleSecurity_t1_1152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Security.AccessControl.EventWaitHandleSecurity::get_AccessRuleType()
extern "C" Type_t * EventWaitHandleSecurity_get_AccessRuleType_m1_9853 (EventWaitHandleSecurity_t1_1152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Security.AccessControl.EventWaitHandleSecurity::get_AuditRuleType()
extern "C" Type_t * EventWaitHandleSecurity_get_AuditRuleType_m1_9854 (EventWaitHandleSecurity_t1_1152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.AccessControl.AccessRule System.Security.AccessControl.EventWaitHandleSecurity::AccessRuleFactory(System.Security.Principal.IdentityReference,System.Int32,System.Boolean,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.AccessControlType)
extern "C" AccessRule_t1_1111 * EventWaitHandleSecurity_AccessRuleFactory_m1_9855 (EventWaitHandleSecurity_t1_1152 * __this, IdentityReference_t1_1120 * ___identityReference, int32_t ___accessMask, bool ___isInherited, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.EventWaitHandleSecurity::AddAccessRule(System.Security.AccessControl.EventWaitHandleAccessRule)
extern "C" void EventWaitHandleSecurity_AddAccessRule_m1_9856 (EventWaitHandleSecurity_t1_1152 * __this, EventWaitHandleAccessRule_t1_1149 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.AccessControl.EventWaitHandleSecurity::RemoveAccessRule(System.Security.AccessControl.EventWaitHandleAccessRule)
extern "C" bool EventWaitHandleSecurity_RemoveAccessRule_m1_9857 (EventWaitHandleSecurity_t1_1152 * __this, EventWaitHandleAccessRule_t1_1149 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.EventWaitHandleSecurity::RemoveAccessRuleAll(System.Security.AccessControl.EventWaitHandleAccessRule)
extern "C" void EventWaitHandleSecurity_RemoveAccessRuleAll_m1_9858 (EventWaitHandleSecurity_t1_1152 * __this, EventWaitHandleAccessRule_t1_1149 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.EventWaitHandleSecurity::RemoveAccessRuleSpecific(System.Security.AccessControl.EventWaitHandleAccessRule)
extern "C" void EventWaitHandleSecurity_RemoveAccessRuleSpecific_m1_9859 (EventWaitHandleSecurity_t1_1152 * __this, EventWaitHandleAccessRule_t1_1149 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.EventWaitHandleSecurity::ResetAccessRule(System.Security.AccessControl.EventWaitHandleAccessRule)
extern "C" void EventWaitHandleSecurity_ResetAccessRule_m1_9860 (EventWaitHandleSecurity_t1_1152 * __this, EventWaitHandleAccessRule_t1_1149 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.EventWaitHandleSecurity::SetAccessRule(System.Security.AccessControl.EventWaitHandleAccessRule)
extern "C" void EventWaitHandleSecurity_SetAccessRule_m1_9861 (EventWaitHandleSecurity_t1_1152 * __this, EventWaitHandleAccessRule_t1_1149 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.AccessControl.AuditRule System.Security.AccessControl.EventWaitHandleSecurity::AuditRuleFactory(System.Security.Principal.IdentityReference,System.Int32,System.Boolean,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.AuditFlags)
extern "C" AuditRule_t1_1119 * EventWaitHandleSecurity_AuditRuleFactory_m1_9862 (EventWaitHandleSecurity_t1_1152 * __this, IdentityReference_t1_1120 * ___identityReference, int32_t ___accessMask, bool ___isInherited, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___flags, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.EventWaitHandleSecurity::AddAuditRule(System.Security.AccessControl.EventWaitHandleAuditRule)
extern "C" void EventWaitHandleSecurity_AddAuditRule_m1_9863 (EventWaitHandleSecurity_t1_1152 * __this, EventWaitHandleAuditRule_t1_1150 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.AccessControl.EventWaitHandleSecurity::RemoveAuditRule(System.Security.AccessControl.EventWaitHandleAuditRule)
extern "C" bool EventWaitHandleSecurity_RemoveAuditRule_m1_9864 (EventWaitHandleSecurity_t1_1152 * __this, EventWaitHandleAuditRule_t1_1150 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.EventWaitHandleSecurity::RemoveAuditRuleAll(System.Security.AccessControl.EventWaitHandleAuditRule)
extern "C" void EventWaitHandleSecurity_RemoveAuditRuleAll_m1_9865 (EventWaitHandleSecurity_t1_1152 * __this, EventWaitHandleAuditRule_t1_1150 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.EventWaitHandleSecurity::RemoveAuditRuleSpecific(System.Security.AccessControl.EventWaitHandleAuditRule)
extern "C" void EventWaitHandleSecurity_RemoveAuditRuleSpecific_m1_9866 (EventWaitHandleSecurity_t1_1152 * __this, EventWaitHandleAuditRule_t1_1150 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.EventWaitHandleSecurity::SetAuditRule(System.Security.AccessControl.EventWaitHandleAuditRule)
extern "C" void EventWaitHandleSecurity_SetAuditRule_m1_9867 (EventWaitHandleSecurity_t1_1152 * __this, EventWaitHandleAuditRule_t1_1150 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
