﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Attribute.h"
#include "mscorlib_System_Runtime_CompilerServices_MethodImplOptions.h"
#include "mscorlib_System_Runtime_CompilerServices_MethodCodeType.h"

// System.Runtime.CompilerServices.MethodImplAttribute
struct  MethodImplAttribute_t1_41  : public Attribute_t1_2
{
	// System.Runtime.CompilerServices.MethodImplOptions System.Runtime.CompilerServices.MethodImplAttribute::_val
	int32_t ____val_0;
	// System.Runtime.CompilerServices.MethodCodeType System.Runtime.CompilerServices.MethodImplAttribute::MethodCodeType
	int32_t ___MethodCodeType_1;
};
