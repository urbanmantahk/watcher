﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_8MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Boomlagoon.JSON.JSONValue>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1_25664(__this, ___l, method) (( void (*) (Enumerator_t1_1918 *, List_1_t1_1898 *, const MethodInfo*))Enumerator__ctor_m1_15258_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Boomlagoon.JSON.JSONValue>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1_25665(__this, method) (( void (*) (Enumerator_t1_1918 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_15259_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Boomlagoon.JSON.JSONValue>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_25666(__this, method) (( Object_t * (*) (Enumerator_t1_1918 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_15260_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Boomlagoon.JSON.JSONValue>::Dispose()
#define Enumerator_Dispose_m1_25667(__this, method) (( void (*) (Enumerator_t1_1918 *, const MethodInfo*))Enumerator_Dispose_m1_15261_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Boomlagoon.JSON.JSONValue>::VerifyState()
#define Enumerator_VerifyState_m1_25668(__this, method) (( void (*) (Enumerator_t1_1918 *, const MethodInfo*))Enumerator_VerifyState_m1_15262_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Boomlagoon.JSON.JSONValue>::MoveNext()
#define Enumerator_MoveNext_m1_15029(__this, method) (( bool (*) (Enumerator_t1_1918 *, const MethodInfo*))Enumerator_MoveNext_m1_15263_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Boomlagoon.JSON.JSONValue>::get_Current()
#define Enumerator_get_Current_m1_15028(__this, method) (( JSONValue_t8_4 * (*) (Enumerator_t1_1918 *, const MethodInfo*))Enumerator_get_Current_m1_15264_gshared)(__this, method)
