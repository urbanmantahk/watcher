﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapToken
struct SoapToken_t1_994;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapToken::.ctor()
extern "C" void SoapToken__ctor_m1_8903 (SoapToken_t1_994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapToken::.ctor(System.String)
extern "C" void SoapToken__ctor_m1_8904 (SoapToken_t1_994 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapToken::get_Value()
extern "C" String_t* SoapToken_get_Value_m1_8905 (SoapToken_t1_994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapToken::set_Value(System.String)
extern "C" void SoapToken_set_Value_m1_8906 (SoapToken_t1_994 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapToken::get_XsdType()
extern "C" String_t* SoapToken_get_XsdType_m1_8907 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapToken::GetXsdType()
extern "C" String_t* SoapToken_GetXsdType_m1_8908 (SoapToken_t1_994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapToken System.Runtime.Remoting.Metadata.W3cXsd2001.SoapToken::Parse(System.String)
extern "C" SoapToken_t1_994 * SoapToken_Parse_m1_8909 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapToken::ToString()
extern "C" String_t* SoapToken_ToString_m1_8910 (SoapToken_t1_994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
