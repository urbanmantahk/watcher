﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.Utility.GUIModalWindow
struct GUIModalWindow_t8_15;
// VoxelBusters.Utility.GUIScrollView
struct GUIScrollView_t8_19;
// UnityEngine.GUISkin
struct GUISkin_t6_169;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.Utility.GUIModalWindow::.ctor()
extern "C" void GUIModalWindow__ctor_m8_790 (GUIModalWindow_t8_15 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.Utility.GUIScrollView VoxelBusters.Utility.GUIModalWindow::get_RootScrollView()
extern "C" GUIScrollView_t8_19 * GUIModalWindow_get_RootScrollView_m8_791 (GUIModalWindow_t8_15 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUISkin VoxelBusters.Utility.GUIModalWindow::get_UISkin()
extern "C" GUISkin_t6_169 * GUIModalWindow_get_UISkin_m8_792 (GUIModalWindow_t8_15 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.GUIModalWindow::set_UISkin(UnityEngine.GUISkin)
extern "C" void GUIModalWindow_set_UISkin_m8_793 (GUIModalWindow_t8_15 * __this, GUISkin_t6_169 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.GUIModalWindow::Awake()
extern "C" void GUIModalWindow_Awake_m8_794 (GUIModalWindow_t8_15 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.GUIModalWindow::Start()
extern "C" void GUIModalWindow_Start_m8_795 (GUIModalWindow_t8_15 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.GUIModalWindow::OnEnable()
extern "C" void GUIModalWindow_OnEnable_m8_796 (GUIModalWindow_t8_15 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.GUIModalWindow::OnDisable()
extern "C" void GUIModalWindow_OnDisable_m8_797 (GUIModalWindow_t8_15 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.GUIModalWindow::OnGUI()
extern "C" void GUIModalWindow_OnGUI_m8_798 (GUIModalWindow_t8_15 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.GUIModalWindow::GUIWindowBase(System.Int32)
extern "C" void GUIModalWindow_GUIWindowBase_m8_799 (GUIModalWindow_t8_15 * __this, int32_t ____windowID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.GUIModalWindow::OnGUIWindow()
extern "C" void GUIModalWindow_OnGUIWindow_m8_800 (GUIModalWindow_t8_15 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.GUIModalWindow::AdjustFontBasedOnScreen()
extern "C" void GUIModalWindow_AdjustFontBasedOnScreen_m8_801 (GUIModalWindow_t8_15 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.GUIModalWindow::AdjustWindowBasedOnScreen()
extern "C" void GUIModalWindow_AdjustWindowBasedOnScreen_m8_802 (GUIModalWindow_t8_15 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.GUIModalWindow::SetSkin()
extern "C" void GUIModalWindow_SetSkin_m8_803 (GUIModalWindow_t8_15 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.GUIModalWindow::RestoreSkin()
extern "C" void GUIModalWindow_RestoreSkin_m8_804 (GUIModalWindow_t8_15 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single VoxelBusters.Utility.GUIModalWindow::GetWindowWidth()
extern "C" float GUIModalWindow_GetWindowWidth_m8_805 (GUIModalWindow_t8_15 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single VoxelBusters.Utility.GUIModalWindow::GetWindowHeight()
extern "C" float GUIModalWindow_GetWindowHeight_m8_806 (GUIModalWindow_t8_15 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
