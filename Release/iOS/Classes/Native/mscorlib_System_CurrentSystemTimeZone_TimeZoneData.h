﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_CurrentSystemTimeZone_TimeZoneData.h"

// System.CurrentSystemTimeZone/TimeZoneData
struct  TimeZoneData_t1_1607 
{
	// System.Int32 System.CurrentSystemTimeZone/TimeZoneData::value__
	int32_t ___value___1;
};
