﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Globalization.CCGregorianEraHandler
struct CCGregorianEraHandler_t1_353;

#include "mscorlib_System_Globalization_Calendar.h"
#include "mscorlib_System_DateTime.h"

// System.Globalization.ThaiBuddhistCalendar
struct  ThaiBuddhistCalendar_t1_391  : public Calendar_t1_338
{
};
struct ThaiBuddhistCalendar_t1_391_StaticFields{
	// System.Globalization.CCGregorianEraHandler System.Globalization.ThaiBuddhistCalendar::M_EraHandler
	CCGregorianEraHandler_t1_353 * ___M_EraHandler_8;
	// System.DateTime System.Globalization.ThaiBuddhistCalendar::ThaiMin
	DateTime_t1_150  ___ThaiMin_9;
	// System.DateTime System.Globalization.ThaiBuddhistCalendar::ThaiMax
	DateTime_t1_150  ___ThaiMax_10;
};
