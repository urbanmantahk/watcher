﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Runtime_InteropServices_PARAMFLAG.h"

// System.Runtime.InteropServices.PARAMFLAG
struct  PARAMFLAG_t1_813 
{
	// System.Int32 System.Runtime.InteropServices.PARAMFLAG::value__
	int32_t ___value___1;
};
