﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Security_Permissions_CodeAccessSecurityAttri.h"

// System.Security.Permissions.EnvironmentPermissionAttribute
struct  EnvironmentPermissionAttribute_t1_1270  : public CodeAccessSecurityAttribute_t1_1266
{
	// System.String System.Security.Permissions.EnvironmentPermissionAttribute::read
	String_t* ___read_2;
	// System.String System.Security.Permissions.EnvironmentPermissionAttribute::write
	String_t* ___write_3;
};
