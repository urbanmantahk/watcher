﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.Collections.IDictionary
struct IDictionary_t1_35;
// System.Collections.IList
struct IList_t1_262;

#include "codegen/il2cpp-codegen.h"

// System.String VoxelBusters.Utility.JSONParserExtensions::ToJSON(System.Collections.IDictionary)
extern "C" String_t* JSONParserExtensions_ToJSON_m8_269 (Object_t * __this /* static, unused */, Object_t * ____dictionary, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.Utility.JSONParserExtensions::ToJSON(System.Collections.IList)
extern "C" String_t* JSONParserExtensions_ToJSON_m8_270 (Object_t * __this /* static, unused */, Object_t * ____list, const MethodInfo* method) IL2CPP_METHOD_ATTR;
