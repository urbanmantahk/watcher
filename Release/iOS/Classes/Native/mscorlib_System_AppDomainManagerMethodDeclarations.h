﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.AppDomainManager
struct AppDomainManager_t1_1493;
// System.Runtime.Hosting.ApplicationActivator
struct ApplicationActivator_t1_719;
// System.Reflection.Assembly
struct Assembly_t1_467;
// System.Threading.HostExecutionContextManager
struct HostExecutionContextManager_t1_1464;
// System.Security.HostSecurityManager
struct HostSecurityManager_t1_1389;
// System.AppDomain
struct AppDomain_t1_1403;
// System.String
struct String_t;
// System.Security.Policy.Evidence
struct Evidence_t1_398;
// System.AppDomainSetup
struct AppDomainSetup_t1_1497;
// System.Security.SecurityState
struct SecurityState_t1_1408;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_AppDomainManagerInitializationOptions.h"

// System.Void System.AppDomainManager::.ctor()
extern "C" void AppDomainManager__ctor_m1_13180 (AppDomainManager_t1_1493 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Hosting.ApplicationActivator System.AppDomainManager::get_ApplicationActivator()
extern "C" ApplicationActivator_t1_719 * AppDomainManager_get_ApplicationActivator_m1_13181 (AppDomainManager_t1_1493 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.AppDomainManager::get_EntryAssembly()
extern "C" Assembly_t1_467 * AppDomainManager_get_EntryAssembly_m1_13182 (AppDomainManager_t1_1493 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.HostExecutionContextManager System.AppDomainManager::get_HostExecutionContextManager()
extern "C" HostExecutionContextManager_t1_1464 * AppDomainManager_get_HostExecutionContextManager_m1_13183 (AppDomainManager_t1_1493 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.HostSecurityManager System.AppDomainManager::get_HostSecurityManager()
extern "C" HostSecurityManager_t1_1389 * AppDomainManager_get_HostSecurityManager_m1_13184 (AppDomainManager_t1_1493 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.AppDomainManagerInitializationOptions System.AppDomainManager::get_InitializationFlags()
extern "C" int32_t AppDomainManager_get_InitializationFlags_m1_13185 (AppDomainManager_t1_1493 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomainManager::set_InitializationFlags(System.AppDomainManagerInitializationOptions)
extern "C" void AppDomainManager_set_InitializationFlags_m1_13186 (AppDomainManager_t1_1493 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.AppDomain System.AppDomainManager::CreateDomain(System.String,System.Security.Policy.Evidence,System.AppDomainSetup)
extern "C" AppDomain_t1_1403 * AppDomainManager_CreateDomain_m1_13187 (AppDomainManager_t1_1493 * __this, String_t* ___friendlyName, Evidence_t1_398 * ___securityInfo, AppDomainSetup_t1_1497 * ___appDomainInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomainManager::InitializeNewDomain(System.AppDomainSetup)
extern "C" void AppDomainManager_InitializeNewDomain_m1_13188 (AppDomainManager_t1_1493 * __this, AppDomainSetup_t1_1497 * ___appDomainInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.AppDomainManager::CheckSecuritySettings(System.Security.SecurityState)
extern "C" bool AppDomainManager_CheckSecuritySettings_m1_13189 (AppDomainManager_t1_1493 * __this, SecurityState_t1_1408 * ___state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.AppDomain System.AppDomainManager::CreateDomainHelper(System.String,System.Security.Policy.Evidence,System.AppDomainSetup)
extern "C" AppDomain_t1_1403 * AppDomainManager_CreateDomainHelper_m1_13190 (Object_t * __this /* static, unused */, String_t* ___friendlyName, Evidence_t1_398 * ___securityInfo, AppDomainSetup_t1_1497 * ___appDomainInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
