﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.IO.SearchPattern/Op
struct Op_t1_440;
// System.Char[]
struct CharU5BU5D_t1_16;

#include "mscorlib_System_Object.h"

// System.IO.SearchPattern
struct  SearchPattern_t1_442  : public Object_t
{
	// System.IO.SearchPattern/Op System.IO.SearchPattern::ops
	Op_t1_440 * ___ops_0;
	// System.Boolean System.IO.SearchPattern::ignore
	bool ___ignore_1;
};
struct SearchPattern_t1_442_StaticFields{
	// System.Char[] System.IO.SearchPattern::WildcardChars
	CharU5BU5D_t1_16* ___WildcardChars_2;
	// System.Char[] System.IO.SearchPattern::InvalidChars
	CharU5BU5D_t1_16* ___InvalidChars_3;
};
