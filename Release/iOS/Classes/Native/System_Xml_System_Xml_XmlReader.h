﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Xml.XmlReaderBinarySupport
struct XmlReaderBinarySupport_t4_161;
// System.Xml.XmlReaderSettings
struct XmlReaderSettings_t4_162;

#include "mscorlib_System_Object.h"

// System.Xml.XmlReader
struct  XmlReader_t4_160  : public Object_t
{
	// System.Xml.XmlReaderBinarySupport System.Xml.XmlReader::binary
	XmlReaderBinarySupport_t4_161 * ___binary_0;
	// System.Xml.XmlReaderSettings System.Xml.XmlReader::settings
	XmlReaderSettings_t4_162 * ___settings_1;
};
