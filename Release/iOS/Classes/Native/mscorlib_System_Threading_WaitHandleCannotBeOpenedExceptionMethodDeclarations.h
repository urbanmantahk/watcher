﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Threading.WaitHandleCannotBeOpenedException
struct WaitHandleCannotBeOpenedException_t1_1489;
// System.String
struct String_t;
// System.Exception
struct Exception_t1_33;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.Threading.WaitHandleCannotBeOpenedException::.ctor()
extern "C" void WaitHandleCannotBeOpenedException__ctor_m1_12998 (WaitHandleCannotBeOpenedException_t1_1489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.WaitHandleCannotBeOpenedException::.ctor(System.String)
extern "C" void WaitHandleCannotBeOpenedException__ctor_m1_12999 (WaitHandleCannotBeOpenedException_t1_1489 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.WaitHandleCannotBeOpenedException::.ctor(System.String,System.Exception)
extern "C" void WaitHandleCannotBeOpenedException__ctor_m1_13000 (WaitHandleCannotBeOpenedException_t1_1489 * __this, String_t* ___message, Exception_t1_33 * ___innerException, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.WaitHandleCannotBeOpenedException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void WaitHandleCannotBeOpenedException__ctor_m1_13001 (WaitHandleCannotBeOpenedException_t1_1489 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
