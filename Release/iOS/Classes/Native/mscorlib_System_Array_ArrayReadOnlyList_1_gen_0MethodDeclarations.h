﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>
struct ArrayReadOnlyList_1_t1_2080;
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t1_1748;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeTypedArgument>
struct IEnumerator_1_t1_2778;
// System.Exception
struct Exception_t1_33;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArgument.h"

// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(T[])
extern "C" void ArrayReadOnlyList_1__ctor_m1_16496_gshared (ArrayReadOnlyList_1_t1_2080 * __this, CustomAttributeTypedArgumentU5BU5D_t1_1748* ___array, const MethodInfo* method);
#define ArrayReadOnlyList_1__ctor_m1_16496(__this, ___array, method) (( void (*) (ArrayReadOnlyList_1_t1_2080 *, CustomAttributeTypedArgumentU5BU5D_t1_1748*, const MethodInfo*))ArrayReadOnlyList_1__ctor_m1_16496_gshared)(__this, ___array, method)
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m1_16497_gshared (ArrayReadOnlyList_1_t1_2080 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m1_16497(__this, method) (( Object_t * (*) (ArrayReadOnlyList_1_t1_2080 *, const MethodInfo*))ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m1_16497_gshared)(__this, method)
// T System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::get_Item(System.Int32)
extern "C" CustomAttributeTypedArgument_t1_594  ArrayReadOnlyList_1_get_Item_m1_16498_gshared (ArrayReadOnlyList_1_t1_2080 * __this, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_Item_m1_16498(__this, ___index, method) (( CustomAttributeTypedArgument_t1_594  (*) (ArrayReadOnlyList_1_t1_2080 *, int32_t, const MethodInfo*))ArrayReadOnlyList_1_get_Item_m1_16498_gshared)(__this, ___index, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::set_Item(System.Int32,T)
extern "C" void ArrayReadOnlyList_1_set_Item_m1_16499_gshared (ArrayReadOnlyList_1_t1_2080 * __this, int32_t ___index, CustomAttributeTypedArgument_t1_594  ___value, const MethodInfo* method);
#define ArrayReadOnlyList_1_set_Item_m1_16499(__this, ___index, ___value, method) (( void (*) (ArrayReadOnlyList_1_t1_2080 *, int32_t, CustomAttributeTypedArgument_t1_594 , const MethodInfo*))ArrayReadOnlyList_1_set_Item_m1_16499_gshared)(__this, ___index, ___value, method)
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::get_Count()
extern "C" int32_t ArrayReadOnlyList_1_get_Count_m1_16500_gshared (ArrayReadOnlyList_1_t1_2080 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_Count_m1_16500(__this, method) (( int32_t (*) (ArrayReadOnlyList_1_t1_2080 *, const MethodInfo*))ArrayReadOnlyList_1_get_Count_m1_16500_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::get_IsReadOnly()
extern "C" bool ArrayReadOnlyList_1_get_IsReadOnly_m1_16501_gshared (ArrayReadOnlyList_1_t1_2080 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_IsReadOnly_m1_16501(__this, method) (( bool (*) (ArrayReadOnlyList_1_t1_2080 *, const MethodInfo*))ArrayReadOnlyList_1_get_IsReadOnly_m1_16501_gshared)(__this, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Add(T)
extern "C" void ArrayReadOnlyList_1_Add_m1_16502_gshared (ArrayReadOnlyList_1_t1_2080 * __this, CustomAttributeTypedArgument_t1_594  ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Add_m1_16502(__this, ___item, method) (( void (*) (ArrayReadOnlyList_1_t1_2080 *, CustomAttributeTypedArgument_t1_594 , const MethodInfo*))ArrayReadOnlyList_1_Add_m1_16502_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Clear()
extern "C" void ArrayReadOnlyList_1_Clear_m1_16503_gshared (ArrayReadOnlyList_1_t1_2080 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_Clear_m1_16503(__this, method) (( void (*) (ArrayReadOnlyList_1_t1_2080 *, const MethodInfo*))ArrayReadOnlyList_1_Clear_m1_16503_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Contains(T)
extern "C" bool ArrayReadOnlyList_1_Contains_m1_16504_gshared (ArrayReadOnlyList_1_t1_2080 * __this, CustomAttributeTypedArgument_t1_594  ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Contains_m1_16504(__this, ___item, method) (( bool (*) (ArrayReadOnlyList_1_t1_2080 *, CustomAttributeTypedArgument_t1_594 , const MethodInfo*))ArrayReadOnlyList_1_Contains_m1_16504_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::CopyTo(T[],System.Int32)
extern "C" void ArrayReadOnlyList_1_CopyTo_m1_16505_gshared (ArrayReadOnlyList_1_t1_2080 * __this, CustomAttributeTypedArgumentU5BU5D_t1_1748* ___array, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_CopyTo_m1_16505(__this, ___array, ___index, method) (( void (*) (ArrayReadOnlyList_1_t1_2080 *, CustomAttributeTypedArgumentU5BU5D_t1_1748*, int32_t, const MethodInfo*))ArrayReadOnlyList_1_CopyTo_m1_16505_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::GetEnumerator()
extern "C" Object_t* ArrayReadOnlyList_1_GetEnumerator_m1_16506_gshared (ArrayReadOnlyList_1_t1_2080 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_GetEnumerator_m1_16506(__this, method) (( Object_t* (*) (ArrayReadOnlyList_1_t1_2080 *, const MethodInfo*))ArrayReadOnlyList_1_GetEnumerator_m1_16506_gshared)(__this, method)
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T)
extern "C" int32_t ArrayReadOnlyList_1_IndexOf_m1_16507_gshared (ArrayReadOnlyList_1_t1_2080 * __this, CustomAttributeTypedArgument_t1_594  ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_IndexOf_m1_16507(__this, ___item, method) (( int32_t (*) (ArrayReadOnlyList_1_t1_2080 *, CustomAttributeTypedArgument_t1_594 , const MethodInfo*))ArrayReadOnlyList_1_IndexOf_m1_16507_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Insert(System.Int32,T)
extern "C" void ArrayReadOnlyList_1_Insert_m1_16508_gshared (ArrayReadOnlyList_1_t1_2080 * __this, int32_t ___index, CustomAttributeTypedArgument_t1_594  ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Insert_m1_16508(__this, ___index, ___item, method) (( void (*) (ArrayReadOnlyList_1_t1_2080 *, int32_t, CustomAttributeTypedArgument_t1_594 , const MethodInfo*))ArrayReadOnlyList_1_Insert_m1_16508_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Remove(T)
extern "C" bool ArrayReadOnlyList_1_Remove_m1_16509_gshared (ArrayReadOnlyList_1_t1_2080 * __this, CustomAttributeTypedArgument_t1_594  ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Remove_m1_16509(__this, ___item, method) (( bool (*) (ArrayReadOnlyList_1_t1_2080 *, CustomAttributeTypedArgument_t1_594 , const MethodInfo*))ArrayReadOnlyList_1_Remove_m1_16509_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::RemoveAt(System.Int32)
extern "C" void ArrayReadOnlyList_1_RemoveAt_m1_16510_gshared (ArrayReadOnlyList_1_t1_2080 * __this, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_RemoveAt_m1_16510(__this, ___index, method) (( void (*) (ArrayReadOnlyList_1_t1_2080 *, int32_t, const MethodInfo*))ArrayReadOnlyList_1_RemoveAt_m1_16510_gshared)(__this, ___index, method)
// System.Exception System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::ReadOnlyError()
extern "C" Exception_t1_33 * ArrayReadOnlyList_1_ReadOnlyError_m1_16511_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define ArrayReadOnlyList_1_ReadOnlyError_m1_16511(__this /* static, unused */, method) (( Exception_t1_33 * (*) (Object_t * /* static, unused */, const MethodInfo*))ArrayReadOnlyList_1_ReadOnlyError_m1_16511_gshared)(__this /* static, unused */, method)
