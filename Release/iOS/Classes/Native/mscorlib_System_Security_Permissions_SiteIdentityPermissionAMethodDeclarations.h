﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Permissions.SiteIdentityPermissionAttribute
struct SiteIdentityPermissionAttribute_t1_1312;
// System.String
struct String_t;
// System.Security.IPermission
struct IPermission_t1_1400;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Permissions_SecurityAction.h"

// System.Void System.Security.Permissions.SiteIdentityPermissionAttribute::.ctor(System.Security.Permissions.SecurityAction)
extern "C" void SiteIdentityPermissionAttribute__ctor_m1_11208 (SiteIdentityPermissionAttribute_t1_1312 * __this, int32_t ___action, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Permissions.SiteIdentityPermissionAttribute::get_Site()
extern "C" String_t* SiteIdentityPermissionAttribute_get_Site_m1_11209 (SiteIdentityPermissionAttribute_t1_1312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.SiteIdentityPermissionAttribute::set_Site(System.String)
extern "C" void SiteIdentityPermissionAttribute_set_Site_m1_11210 (SiteIdentityPermissionAttribute_t1_1312 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.SiteIdentityPermissionAttribute::CreatePermission()
extern "C" Object_t * SiteIdentityPermissionAttribute_CreatePermission_m1_11211 (SiteIdentityPermissionAttribute_t1_1312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
