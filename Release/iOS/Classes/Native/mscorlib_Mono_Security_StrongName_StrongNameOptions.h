﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_Mono_Security_StrongName_StrongNameOptions.h"

// Mono.Security.StrongName/StrongNameOptions
struct  StrongNameOptions_t1_231 
{
	// System.Int32 Mono.Security.StrongName/StrongNameOptions::value__
	int32_t ___value___1;
};
