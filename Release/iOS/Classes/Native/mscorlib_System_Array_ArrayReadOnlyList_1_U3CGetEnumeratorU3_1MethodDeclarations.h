﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>
struct U3CGetEnumeratorU3Ec__Iterator0_t1_2093;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgument.h"

// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::.ctor()
extern "C" void U3CGetEnumeratorU3Ec__Iterator0__ctor_m1_16720_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1_2093 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0__ctor_m1_16720(__this, method) (( void (*) (U3CGetEnumeratorU3Ec__Iterator0_t1_2093 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0__ctor_m1_16720_gshared)(__this, method)
// T System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C" CustomAttributeNamedArgument_t1_593  U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1_16721_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1_2093 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1_16721(__this, method) (( CustomAttributeNamedArgument_t1_593  (*) (U3CGetEnumeratorU3Ec__Iterator0_t1_2093 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1_16721_gshared)(__this, method)
// System.Object System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1_16722_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1_2093 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1_16722(__this, method) (( Object_t * (*) (U3CGetEnumeratorU3Ec__Iterator0_t1_2093 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1_16722_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::MoveNext()
extern "C" bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m1_16723_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1_2093 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m1_16723(__this, method) (( bool (*) (U3CGetEnumeratorU3Ec__Iterator0_t1_2093 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m1_16723_gshared)(__this, method)
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::Dispose()
extern "C" void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m1_16724_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1_2093 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_Dispose_m1_16724(__this, method) (( void (*) (U3CGetEnumeratorU3Ec__Iterator0_t1_2093 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_Dispose_m1_16724_gshared)(__this, method)
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::Reset()
extern "C" void U3CGetEnumeratorU3Ec__Iterator0_Reset_m1_16725_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1_2093 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_Reset_m1_16725(__this, method) (( void (*) (U3CGetEnumeratorU3Ec__Iterator0_t1_2093 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_Reset_m1_16725_gshared)(__this, method)
