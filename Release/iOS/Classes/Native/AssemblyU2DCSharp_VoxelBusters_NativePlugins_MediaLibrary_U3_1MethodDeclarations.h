﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.MediaLibrary/<SaveImageToGallery>c__AnonStoreyD
struct U3CSaveImageToGalleryU3Ec__AnonStoreyD_t8_247;
// UnityEngine.Texture2D
struct Texture2D_t6_33;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.MediaLibrary/<SaveImageToGallery>c__AnonStoreyD::.ctor()
extern "C" void U3CSaveImageToGalleryU3Ec__AnonStoreyD__ctor_m8_1367 (U3CSaveImageToGalleryU3Ec__AnonStoreyD_t8_247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.MediaLibrary/<SaveImageToGallery>c__AnonStoreyD::<>m__E(UnityEngine.Texture2D,System.String)
extern "C" void U3CSaveImageToGalleryU3Ec__AnonStoreyD_U3CU3Em__E_m8_1368 (U3CSaveImageToGalleryU3Ec__AnonStoreyD_t8_247 * __this, Texture2D_t6_33 * ____texture, String_t* ____error, const MethodInfo* method) IL2CPP_METHOD_ATTR;
