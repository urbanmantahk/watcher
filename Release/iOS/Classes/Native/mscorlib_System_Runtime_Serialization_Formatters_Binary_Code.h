﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Object_t;
// System.Reflection.Emit.ModuleBuilder
struct ModuleBuilder_t1_475;

#include "mscorlib_System_Object.h"

// System.Runtime.Serialization.Formatters.Binary.CodeGenerator
struct  CodeGenerator_t1_1051  : public Object_t
{
};
struct CodeGenerator_t1_1051_StaticFields{
	// System.Object System.Runtime.Serialization.Formatters.Binary.CodeGenerator::monitor
	Object_t * ___monitor_0;
	// System.Reflection.Emit.ModuleBuilder System.Runtime.Serialization.Formatters.Binary.CodeGenerator::_module
	ModuleBuilder_t1_475 * ____module_1;
};
