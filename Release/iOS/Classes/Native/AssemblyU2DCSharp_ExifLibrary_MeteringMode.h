﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "AssemblyU2DCSharp_ExifLibrary_MeteringMode.h"

// ExifLibrary.MeteringMode
struct  MeteringMode_t8_71 
{
	// System.UInt16 ExifLibrary.MeteringMode::value__
	uint16_t ___value___1;
};
