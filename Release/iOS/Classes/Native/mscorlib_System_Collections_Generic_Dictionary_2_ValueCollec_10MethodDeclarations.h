﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Boolean>
struct ValueCollection_t1_2229;
// System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>
struct Dictionary_2_t1_2221;
// System.Collections.Generic.IEnumerator`1<System.Boolean>
struct IEnumerator_1_t1_2791;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Object
struct Object_t;
// System.Boolean[]
struct BooleanU5BU5D_t1_458;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_11.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ValueCollection__ctor_m1_18394_gshared (ValueCollection_t1_2229 * __this, Dictionary_2_t1_2221 * ___dictionary, const MethodInfo* method);
#define ValueCollection__ctor_m1_18394(__this, ___dictionary, method) (( void (*) (ValueCollection_t1_2229 *, Dictionary_2_t1_2221 *, const MethodInfo*))ValueCollection__ctor_m1_18394_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Boolean>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1_18395_gshared (ValueCollection_t1_2229 * __this, bool ___item, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1_18395(__this, ___item, method) (( void (*) (ValueCollection_t1_2229 *, bool, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1_18395_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Boolean>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1_18396_gshared (ValueCollection_t1_2229 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1_18396(__this, method) (( void (*) (ValueCollection_t1_2229 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1_18396_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Boolean>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1_18397_gshared (ValueCollection_t1_2229 * __this, bool ___item, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1_18397(__this, ___item, method) (( bool (*) (ValueCollection_t1_2229 *, bool, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1_18397_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Boolean>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1_18398_gshared (ValueCollection_t1_2229 * __this, bool ___item, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1_18398(__this, ___item, method) (( bool (*) (ValueCollection_t1_2229 *, bool, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1_18398_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Boolean>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C" Object_t* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1_18399_gshared (ValueCollection_t1_2229 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1_18399(__this, method) (( Object_t* (*) (ValueCollection_t1_2229 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1_18399_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Boolean>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m1_18400_gshared (ValueCollection_t1_2229 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m1_18400(__this, ___array, ___index, method) (( void (*) (ValueCollection_t1_2229 *, Array_t *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m1_18400_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Boolean>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1_18401_gshared (ValueCollection_t1_2229 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1_18401(__this, method) (( Object_t * (*) (ValueCollection_t1_2229 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1_18401_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Boolean>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1_18402_gshared (ValueCollection_t1_2229 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1_18402(__this, method) (( bool (*) (ValueCollection_t1_2229 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1_18402_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Boolean>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1_18403_gshared (ValueCollection_t1_2229 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1_18403(__this, method) (( bool (*) (ValueCollection_t1_2229 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1_18403_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Boolean>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * ValueCollection_System_Collections_ICollection_get_SyncRoot_m1_18404_gshared (ValueCollection_t1_2229 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m1_18404(__this, method) (( Object_t * (*) (ValueCollection_t1_2229 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1_18404_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Boolean>::CopyTo(TValue[],System.Int32)
extern "C" void ValueCollection_CopyTo_m1_18405_gshared (ValueCollection_t1_2229 * __this, BooleanU5BU5D_t1_458* ___array, int32_t ___index, const MethodInfo* method);
#define ValueCollection_CopyTo_m1_18405(__this, ___array, ___index, method) (( void (*) (ValueCollection_t1_2229 *, BooleanU5BU5D_t1_458*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1_18405_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Boolean>::GetEnumerator()
extern "C" Enumerator_t1_2230  ValueCollection_GetEnumerator_m1_18406_gshared (ValueCollection_t1_2229 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m1_18406(__this, method) (( Enumerator_t1_2230  (*) (ValueCollection_t1_2229 *, const MethodInfo*))ValueCollection_GetEnumerator_m1_18406_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Boolean>::get_Count()
extern "C" int32_t ValueCollection_get_Count_m1_18407_gshared (ValueCollection_t1_2229 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m1_18407(__this, method) (( int32_t (*) (ValueCollection_t1_2229 *, const MethodInfo*))ValueCollection_get_Count_m1_18407_gshared)(__this, method)
