﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.Emit.DynamicMethod/AnonHostModuleHolder
struct AnonHostModuleHolder_t1_494;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Reflection.Emit.DynamicMethod/AnonHostModuleHolder::.ctor()
extern "C" void AnonHostModuleHolder__ctor_m1_5688 (AnonHostModuleHolder_t1_494 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.DynamicMethod/AnonHostModuleHolder::.cctor()
extern "C" void AnonHostModuleHolder__cctor_m1_5689 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
