﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.ConstrainedExecution.ReliabilityContractAttribute
struct ReliabilityContractAttribute_t1_715;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_ConstrainedExecution_Consistency.h"
#include "mscorlib_System_Runtime_ConstrainedExecution_Cer.h"

// System.Void System.Runtime.ConstrainedExecution.ReliabilityContractAttribute::.ctor(System.Runtime.ConstrainedExecution.Consistency,System.Runtime.ConstrainedExecution.Cer)
extern "C" void ReliabilityContractAttribute__ctor_m1_7561 (ReliabilityContractAttribute_t1_715 * __this, int32_t ___consistencyGuarantee, int32_t ___cer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.ConstrainedExecution.Cer System.Runtime.ConstrainedExecution.ReliabilityContractAttribute::get_Cer()
extern "C" int32_t ReliabilityContractAttribute_get_Cer_m1_7562 (ReliabilityContractAttribute_t1_715 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.ConstrainedExecution.Consistency System.Runtime.ConstrainedExecution.ReliabilityContractAttribute::get_ConsistencyGuarantee()
extern "C" int32_t ReliabilityContractAttribute_get_ConsistencyGuarantee_m1_7563 (ReliabilityContractAttribute_t1_715 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
