﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Xml.Schema.XsdGYear
struct XsdGYear_t4_50;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Xml.Schema.XsdGYear::.ctor()
extern "C" void XsdGYear__ctor_m4_75 (XsdGYear_t4_50 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
