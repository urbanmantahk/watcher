﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.ObjectHandle
struct ObjectHandle_t1_1019;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.ObjectHandle::.ctor(System.Object)
extern "C" void ObjectHandle__ctor_m1_9103 (ObjectHandle_t1_1019 * __this, Object_t * ___o, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.ObjectHandle::InitializeLifetimeService()
extern "C" Object_t * ObjectHandle_InitializeLifetimeService_m1_9104 (ObjectHandle_t1_1019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.ObjectHandle::Unwrap()
extern "C" Object_t * ObjectHandle_Unwrap_m1_9105 (ObjectHandle_t1_1019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
