﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.GameServicesSettings/iOSSettings
struct iOSSettings_t8_237;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.GameServicesSettings/iOSSettings::.ctor()
extern "C" void iOSSettings__ctor_m8_1335 (iOSSettings_t8_237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.NativePlugins.GameServicesSettings/iOSSettings::get_ShowDefaultAchievementCompletionBanner()
extern "C" bool iOSSettings_get_ShowDefaultAchievementCompletionBanner_m8_1336 (iOSSettings_t8_237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.GameServicesSettings/iOSSettings::set_ShowDefaultAchievementCompletionBanner(System.Boolean)
extern "C" void iOSSettings_set_ShowDefaultAchievementCompletionBanner_m8_1337 (iOSSettings_t8_237 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
