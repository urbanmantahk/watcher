﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1_238;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_ShareI.h"

// VoxelBusters.NativePlugins.MailShareComposer
struct  MailShareComposer_t8_279  : public ShareImageUtility_t8_280
{
	// System.String VoxelBusters.NativePlugins.MailShareComposer::<Subject>k__BackingField
	String_t* ___U3CSubjectU3Ek__BackingField_3;
	// System.String VoxelBusters.NativePlugins.MailShareComposer::<Body>k__BackingField
	String_t* ___U3CBodyU3Ek__BackingField_4;
	// System.Boolean VoxelBusters.NativePlugins.MailShareComposer::<IsHTMLBody>k__BackingField
	bool ___U3CIsHTMLBodyU3Ek__BackingField_5;
	// System.String[] VoxelBusters.NativePlugins.MailShareComposer::<ToRecipients>k__BackingField
	StringU5BU5D_t1_238* ___U3CToRecipientsU3Ek__BackingField_6;
	// System.String[] VoxelBusters.NativePlugins.MailShareComposer::<CCRecipients>k__BackingField
	StringU5BU5D_t1_238* ___U3CCCRecipientsU3Ek__BackingField_7;
	// System.String[] VoxelBusters.NativePlugins.MailShareComposer::<BCCRecipients>k__BackingField
	StringU5BU5D_t1_238* ___U3CBCCRecipientsU3Ek__BackingField_8;
	// System.Byte[] VoxelBusters.NativePlugins.MailShareComposer::<AttachmentData>k__BackingField
	ByteU5BU5D_t1_109* ___U3CAttachmentDataU3Ek__BackingField_9;
	// System.String VoxelBusters.NativePlugins.MailShareComposer::<AttachmentFileName>k__BackingField
	String_t* ___U3CAttachmentFileNameU3Ek__BackingField_10;
	// System.String VoxelBusters.NativePlugins.MailShareComposer::<MimeType>k__BackingField
	String_t* ___U3CMimeTypeU3Ek__BackingField_11;
};
