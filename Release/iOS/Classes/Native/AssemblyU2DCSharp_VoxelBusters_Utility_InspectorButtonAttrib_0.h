﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_PropertyAttribute.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_InspectorButtonAttrib.h"

// VoxelBusters.Utility.InspectorButtonAttribute
struct  InspectorButtonAttribute_t8_147  : public PropertyAttribute_t6_236
{
	// System.String VoxelBusters.Utility.InspectorButtonAttribute::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.String VoxelBusters.Utility.InspectorButtonAttribute::<InvokeMethod>k__BackingField
	String_t* ___U3CInvokeMethodU3Ek__BackingField_1;
	// VoxelBusters.Utility.InspectorButtonAttribute/ePosition VoxelBusters.Utility.InspectorButtonAttribute::<Position>k__BackingField
	int32_t ___U3CPositionU3Ek__BackingField_2;
};
