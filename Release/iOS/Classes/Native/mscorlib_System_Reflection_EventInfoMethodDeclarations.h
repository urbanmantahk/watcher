﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.EventInfo
struct EventInfo_t;
// System.Type
struct Type_t;
// System.Object
struct Object_t;
// System.Delegate
struct Delegate_t1_22;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Reflection.MethodInfo[]
struct MethodInfoU5BU5D_t1_603;
// System.Reflection.EventInfo/AddEventAdapter
struct AddEventAdapter_t1_596;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Guid.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_Reflection_MemberTypes.h"

// System.Void System.Reflection.EventInfo::.ctor()
extern "C" void EventInfo__ctor_m1_6814 (EventInfo_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.EventInfo::System.Runtime.InteropServices._EventInfo.GetIDsOfNames(System.Guid&,System.IntPtr,System.UInt32,System.UInt32,System.IntPtr)
extern "C" void EventInfo_System_Runtime_InteropServices__EventInfo_GetIDsOfNames_m1_6815 (EventInfo_t * __this, Guid_t1_319 * ___riid, IntPtr_t ___rgszNames, uint32_t ___cNames, uint32_t ___lcid, IntPtr_t ___rgDispId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.EventInfo::System.Runtime.InteropServices._EventInfo.GetTypeInfo(System.UInt32,System.UInt32,System.IntPtr)
extern "C" void EventInfo_System_Runtime_InteropServices__EventInfo_GetTypeInfo_m1_6816 (EventInfo_t * __this, uint32_t ___iTInfo, uint32_t ___lcid, IntPtr_t ___ppTInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.EventInfo::System.Runtime.InteropServices._EventInfo.GetTypeInfoCount(System.UInt32&)
extern "C" void EventInfo_System_Runtime_InteropServices__EventInfo_GetTypeInfoCount_m1_6817 (EventInfo_t * __this, uint32_t* ___pcTInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.EventInfo::System.Runtime.InteropServices._EventInfo.Invoke(System.UInt32,System.Guid&,System.UInt32,System.Int16,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr)
extern "C" void EventInfo_System_Runtime_InteropServices__EventInfo_Invoke_m1_6818 (EventInfo_t * __this, uint32_t ___dispIdMember, Guid_t1_319 * ___riid, uint32_t ___lcid, int16_t ___wFlags, IntPtr_t ___pDispParams, IntPtr_t ___pVarResult, IntPtr_t ___pExcepInfo, IntPtr_t ___puArgErr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.EventInfo::get_EventHandlerType()
extern "C" Type_t * EventInfo_get_EventHandlerType_m1_6819 (EventInfo_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.EventInfo::get_IsMulticast()
extern "C" bool EventInfo_get_IsMulticast_m1_6820 (EventInfo_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.EventInfo::get_IsSpecialName()
extern "C" bool EventInfo_get_IsSpecialName_m1_6821 (EventInfo_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MemberTypes System.Reflection.EventInfo::get_MemberType()
extern "C" int32_t EventInfo_get_MemberType_m1_6822 (EventInfo_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.EventInfo::AddEventHandler(System.Object,System.Delegate)
extern "C" void EventInfo_AddEventHandler_m1_6823 (EventInfo_t * __this, Object_t * ___target, Delegate_t1_22 * ___handler, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Reflection.EventInfo::GetAddMethod()
extern "C" MethodInfo_t * EventInfo_GetAddMethod_m1_6824 (EventInfo_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Reflection.EventInfo::GetRaiseMethod()
extern "C" MethodInfo_t * EventInfo_GetRaiseMethod_m1_6825 (EventInfo_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Reflection.EventInfo::GetRemoveMethod()
extern "C" MethodInfo_t * EventInfo_GetRemoveMethod_m1_6826 (EventInfo_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo[] System.Reflection.EventInfo::GetOtherMethods(System.Boolean)
extern "C" MethodInfoU5BU5D_t1_603* EventInfo_GetOtherMethods_m1_6827 (EventInfo_t * __this, bool ___nonPublic, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo[] System.Reflection.EventInfo::GetOtherMethods()
extern "C" MethodInfoU5BU5D_t1_603* EventInfo_GetOtherMethods_m1_6828 (EventInfo_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.EventInfo::RemoveEventHandler(System.Object,System.Delegate)
extern "C" void EventInfo_RemoveEventHandler_m1_6829 (EventInfo_t * __this, Object_t * ___target, Delegate_t1_22 * ___handler, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.EventInfo/AddEventAdapter System.Reflection.EventInfo::CreateAddEventDelegate(System.Reflection.MethodInfo)
extern "C" AddEventAdapter_t1_596 * EventInfo_CreateAddEventDelegate_m1_6830 (Object_t * __this /* static, unused */, MethodInfo_t * ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.EventInfo::System.Runtime.InteropServices._EventInfo.GetType()
extern "C" Type_t * EventInfo_System_Runtime_InteropServices__EventInfo_GetType_m1_6831 (EventInfo_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
