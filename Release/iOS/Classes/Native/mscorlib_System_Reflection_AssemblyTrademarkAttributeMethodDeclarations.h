﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.AssemblyTrademarkAttribute
struct AssemblyTrademarkAttribute_t1_583;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Reflection.AssemblyTrademarkAttribute::.ctor(System.String)
extern "C" void AssemblyTrademarkAttribute__ctor_m1_6742 (AssemblyTrademarkAttribute_t1_583 * __this, String_t* ___trademark, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.AssemblyTrademarkAttribute::get_Trademark()
extern "C" String_t* AssemblyTrademarkAttribute_get_Trademark_m1_6743 (AssemblyTrademarkAttribute_t1_583 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
