﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"

extern const methodPointerType* native_delegate_wrapper_DeflateStream_UnmanagedRead_m3_178_indirect;
extern const methodPointerType* native_delegate_wrapper_DeflateStream_UnmanagedWrite_m3_180_indirect;
extern const methodPointerType * g_PInvokeWrapperPointers[2] = 
{
	(methodPointerType*)&native_delegate_wrapper_DeflateStream_UnmanagedRead_m3_178_indirect,
	(methodPointerType*)&native_delegate_wrapper_DeflateStream_UnmanagedWrite_m3_180_indirect,
};
