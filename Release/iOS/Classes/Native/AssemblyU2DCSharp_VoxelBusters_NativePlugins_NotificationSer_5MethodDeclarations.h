﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings
struct AndroidSettings_t8_269;
// System.String[]
struct StringU5BU5D_t1_238;
// UnityEngine.Texture2D
struct Texture2D_t6_33;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings::.ctor()
extern "C" void AndroidSettings__ctor_m8_1542 (AndroidSettings_t8_269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings::get_SenderIDList()
extern "C" StringU5BU5D_t1_238* AndroidSettings_get_SenderIDList_m8_1543 (AndroidSettings_t8_269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings::set_SenderIDList(System.String[])
extern "C" void AndroidSettings_set_SenderIDList_m8_1544 (AndroidSettings_t8_269 * __this, StringU5BU5D_t1_238* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings::get_NeedsBigStyle()
extern "C" bool AndroidSettings_get_NeedsBigStyle_m8_1545 (AndroidSettings_t8_269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings::set_NeedsBigStyle(System.Boolean)
extern "C" void AndroidSettings_set_NeedsBigStyle_m8_1546 (AndroidSettings_t8_269 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings::get_AllowVibration()
extern "C" bool AndroidSettings_get_AllowVibration_m8_1547 (AndroidSettings_t8_269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings::set_AllowVibration(System.Boolean)
extern "C" void AndroidSettings_set_AllowVibration_m8_1548 (AndroidSettings_t8_269 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings::get_WhiteSmallIcon()
extern "C" Texture2D_t6_33 * AndroidSettings_get_WhiteSmallIcon_m8_1549 (AndroidSettings_t8_269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings::set_WhiteSmallIcon(UnityEngine.Texture2D)
extern "C" void AndroidSettings_set_WhiteSmallIcon_m8_1550 (AndroidSettings_t8_269 * __this, Texture2D_t6_33 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings::get_ColouredSmallIcon()
extern "C" Texture2D_t6_33 * AndroidSettings_get_ColouredSmallIcon_m8_1551 (AndroidSettings_t8_269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings::set_ColouredSmallIcon(UnityEngine.Texture2D)
extern "C" void AndroidSettings_set_ColouredSmallIcon_m8_1552 (AndroidSettings_t8_269 * __this, Texture2D_t6_33 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings::get_TickerTextKey()
extern "C" String_t* AndroidSettings_get_TickerTextKey_m8_1553 (AndroidSettings_t8_269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings::set_TickerTextKey(System.String)
extern "C" void AndroidSettings_set_TickerTextKey_m8_1554 (AndroidSettings_t8_269 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings::get_ContentTitleKey()
extern "C" String_t* AndroidSettings_get_ContentTitleKey_m8_1555 (AndroidSettings_t8_269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings::set_ContentTitleKey(System.String)
extern "C" void AndroidSettings_set_ContentTitleKey_m8_1556 (AndroidSettings_t8_269 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings::get_ContentTextKey()
extern "C" String_t* AndroidSettings_get_ContentTextKey_m8_1557 (AndroidSettings_t8_269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings::set_ContentTextKey(System.String)
extern "C" void AndroidSettings_set_ContentTextKey_m8_1558 (AndroidSettings_t8_269 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings::get_UserInfoKey()
extern "C" String_t* AndroidSettings_get_UserInfoKey_m8_1559 (AndroidSettings_t8_269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings::set_UserInfoKey(System.String)
extern "C" void AndroidSettings_set_UserInfoKey_m8_1560 (AndroidSettings_t8_269 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings::get_TagKey()
extern "C" String_t* AndroidSettings_get_TagKey_m8_1561 (AndroidSettings_t8_269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationServiceSettings/AndroidSettings::set_TagKey(System.String)
extern "C" void AndroidSettings_set_TagKey_m8_1562 (AndroidSettings_t8_269 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
