﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExifLibrary.GPSLatitudeLongitude
struct GPSLatitudeLongitude_t8_106;
// ExifLibrary.MathEx/UFraction32[]
struct UFraction32U5BU5D_t8_122;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTag.h"
#include "AssemblyU2DCSharp_ExifLibrary_MathEx_UFraction32.h"

// System.Void ExifLibrary.GPSLatitudeLongitude::.ctor(ExifLibrary.ExifTag,ExifLibrary.MathEx/UFraction32[])
extern "C" void GPSLatitudeLongitude__ctor_m8_454 (GPSLatitudeLongitude_t8_106 * __this, int32_t ___tag, UFraction32U5BU5D_t8_122* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/UFraction32[] ExifLibrary.GPSLatitudeLongitude::get_Value()
extern "C" UFraction32U5BU5D_t8_122* GPSLatitudeLongitude_get_Value_m8_455 (GPSLatitudeLongitude_t8_106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.GPSLatitudeLongitude::set_Value(ExifLibrary.MathEx/UFraction32[])
extern "C" void GPSLatitudeLongitude_set_Value_m8_456 (GPSLatitudeLongitude_t8_106 * __this, UFraction32U5BU5D_t8_122* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/UFraction32 ExifLibrary.GPSLatitudeLongitude::get_Degrees()
extern "C" UFraction32_t8_121  GPSLatitudeLongitude_get_Degrees_m8_457 (GPSLatitudeLongitude_t8_106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.GPSLatitudeLongitude::set_Degrees(ExifLibrary.MathEx/UFraction32)
extern "C" void GPSLatitudeLongitude_set_Degrees_m8_458 (GPSLatitudeLongitude_t8_106 * __this, UFraction32_t8_121  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/UFraction32 ExifLibrary.GPSLatitudeLongitude::get_Minutes()
extern "C" UFraction32_t8_121  GPSLatitudeLongitude_get_Minutes_m8_459 (GPSLatitudeLongitude_t8_106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.GPSLatitudeLongitude::set_Minutes(ExifLibrary.MathEx/UFraction32)
extern "C" void GPSLatitudeLongitude_set_Minutes_m8_460 (GPSLatitudeLongitude_t8_106 * __this, UFraction32_t8_121  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/UFraction32 ExifLibrary.GPSLatitudeLongitude::get_Seconds()
extern "C" UFraction32_t8_121  GPSLatitudeLongitude_get_Seconds_m8_461 (GPSLatitudeLongitude_t8_106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.GPSLatitudeLongitude::set_Seconds(ExifLibrary.MathEx/UFraction32)
extern "C" void GPSLatitudeLongitude_set_Seconds_m8_462 (GPSLatitudeLongitude_t8_106 * __this, UFraction32_t8_121  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ExifLibrary.GPSLatitudeLongitude::ToFloat()
extern "C" float GPSLatitudeLongitude_ToFloat_m8_463 (GPSLatitudeLongitude_t8_106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExifLibrary.GPSLatitudeLongitude::ToString()
extern "C" String_t* GPSLatitudeLongitude_ToString_m8_464 (GPSLatitudeLongitude_t8_106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ExifLibrary.GPSLatitudeLongitude::op_Explicit(ExifLibrary.GPSLatitudeLongitude)
extern "C" float GPSLatitudeLongitude_op_Explicit_m8_465 (Object_t * __this /* static, unused */, GPSLatitudeLongitude_t8_106 * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
