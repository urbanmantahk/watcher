﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__2MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1_24394(__this, ___dictionary, method) (( void (*) (Enumerator_t1_2564 *, Dictionary_2_t1_1892 *, const MethodInfo*))Enumerator__ctor_m1_15709_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_24395(__this, method) (( Object_t * (*) (Enumerator_t1_2564 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_15710_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1_24396(__this, method) (( void (*) (Enumerator_t1_2564 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_15711_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_24397(__this, method) (( DictionaryEntry_t1_284  (*) (Enumerator_t1_2564 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_15712_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_24398(__this, method) (( Object_t * (*) (Enumerator_t1_2564 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_15713_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_24399(__this, method) (( Object_t * (*) (Enumerator_t1_2564 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_15714_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::MoveNext()
#define Enumerator_MoveNext_m1_24400(__this, method) (( bool (*) (Enumerator_t1_2564 *, const MethodInfo*))Enumerator_MoveNext_m1_15715_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::get_Current()
#define Enumerator_get_Current_m1_24401(__this, method) (( KeyValuePair_2_t1_2561  (*) (Enumerator_t1_2564 *, const MethodInfo*))Enumerator_get_Current_m1_15716_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1_24402(__this, method) (( Object_t * (*) (Enumerator_t1_2564 *, const MethodInfo*))Enumerator_get_CurrentKey_m1_15717_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1_24403(__this, method) (( int32_t (*) (Enumerator_t1_2564 *, const MethodInfo*))Enumerator_get_CurrentValue_m1_15718_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::Reset()
#define Enumerator_Reset_m1_24404(__this, method) (( void (*) (Enumerator_t1_2564 *, const MethodInfo*))Enumerator_Reset_m1_15719_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::VerifyState()
#define Enumerator_VerifyState_m1_24405(__this, method) (( void (*) (Enumerator_t1_2564 *, const MethodInfo*))Enumerator_VerifyState_m1_15720_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1_24406(__this, method) (( void (*) (Enumerator_t1_2564 *, const MethodInfo*))Enumerator_VerifyCurrent_m1_15721_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::Dispose()
#define Enumerator_Dispose_m1_24407(__this, method) (( void (*) (Enumerator_t1_2564 *, const MethodInfo*))Enumerator_Dispose_m1_15722_gshared)(__this, method)
