﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Serialization.Formatters.SoapMessage
struct SoapMessage_t1_1072;
// System.Runtime.Remoting.Messaging.Header[]
struct HeaderU5BU5D_t1_927;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1_238;
// System.Type[]
struct TypeU5BU5D_t1_31;
// System.Object[]
struct ObjectU5BU5D_t1_272;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Serialization.Formatters.SoapMessage::.ctor()
extern "C" void SoapMessage__ctor_m1_9489 (SoapMessage_t1_1072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Messaging.Header[] System.Runtime.Serialization.Formatters.SoapMessage::get_Headers()
extern "C" HeaderU5BU5D_t1_927* SoapMessage_get_Headers_m1_9490 (SoapMessage_t1_1072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.Formatters.SoapMessage::set_Headers(System.Runtime.Remoting.Messaging.Header[])
extern "C" void SoapMessage_set_Headers_m1_9491 (SoapMessage_t1_1072 * __this, HeaderU5BU5D_t1_927* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Serialization.Formatters.SoapMessage::get_MethodName()
extern "C" String_t* SoapMessage_get_MethodName_m1_9492 (SoapMessage_t1_1072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.Formatters.SoapMessage::set_MethodName(System.String)
extern "C" void SoapMessage_set_MethodName_m1_9493 (SoapMessage_t1_1072 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.Runtime.Serialization.Formatters.SoapMessage::get_ParamNames()
extern "C" StringU5BU5D_t1_238* SoapMessage_get_ParamNames_m1_9494 (SoapMessage_t1_1072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.Formatters.SoapMessage::set_ParamNames(System.String[])
extern "C" void SoapMessage_set_ParamNames_m1_9495 (SoapMessage_t1_1072 * __this, StringU5BU5D_t1_238* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type[] System.Runtime.Serialization.Formatters.SoapMessage::get_ParamTypes()
extern "C" TypeU5BU5D_t1_31* SoapMessage_get_ParamTypes_m1_9496 (SoapMessage_t1_1072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.Formatters.SoapMessage::set_ParamTypes(System.Type[])
extern "C" void SoapMessage_set_ParamTypes_m1_9497 (SoapMessage_t1_1072 * __this, TypeU5BU5D_t1_31* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Runtime.Serialization.Formatters.SoapMessage::get_ParamValues()
extern "C" ObjectU5BU5D_t1_272* SoapMessage_get_ParamValues_m1_9498 (SoapMessage_t1_1072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.Formatters.SoapMessage::set_ParamValues(System.Object[])
extern "C" void SoapMessage_set_ParamValues_m1_9499 (SoapMessage_t1_1072 * __this, ObjectU5BU5D_t1_272* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Serialization.Formatters.SoapMessage::get_XmlNameSpace()
extern "C" String_t* SoapMessage_get_XmlNameSpace_m1_9500 (SoapMessage_t1_1072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.Formatters.SoapMessage::set_XmlNameSpace(System.String)
extern "C" void SoapMessage_set_XmlNameSpace_m1_9501 (SoapMessage_t1_1072 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
