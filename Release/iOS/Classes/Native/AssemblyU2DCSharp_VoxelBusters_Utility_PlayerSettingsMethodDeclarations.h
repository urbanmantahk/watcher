﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.Utility.PlayerSettings
struct PlayerSettings_t8_39;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.Utility.PlayerSettings::.ctor()
extern "C" void PlayerSettings__ctor_m8_212 (PlayerSettings_t8_39 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.Utility.PlayerSettings::GetBundleVersion()
extern "C" String_t* PlayerSettings_GetBundleVersion_m8_213 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.Utility.PlayerSettings::GetBundleIdentifier()
extern "C" String_t* PlayerSettings_GetBundleIdentifier_m8_214 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
