﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AlertControl
struct AlertControl_t8_9;
// System.String
struct String_t;
// VoxelBusters.NativePlugins.CrossPlatformNotification
struct CrossPlatformNotification_t8_259;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Action
struct Action_t5_11;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eNotificationRe.h"

// System.Void AlertControl::.ctor()
extern "C" void AlertControl__ctor_m8_73 (AlertControl_t8_9 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AlertControl::Update()
extern "C" void AlertControl_Update_m8_74 (AlertControl_t8_9 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AlertControl::CheckIn()
extern "C" void AlertControl_CheckIn_m8_75 (AlertControl_t8_9 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AlertControl::UpdateTimeDisplay()
extern "C" void AlertControl_UpdateTimeDisplay_m8_76 (AlertControl_t8_9 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AlertControl::ShowButton()
extern "C" void AlertControl_ShowButton_m8_77 (AlertControl_t8_9 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AlertControl::RenewAMTimer()
extern "C" void AlertControl_RenewAMTimer_m8_78 (AlertControl_t8_9 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AlertControl::RenewPMTimer()
extern "C" void AlertControl_RenewPMTimer_m8_79 (AlertControl_t8_9 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AlertControl::CancelLocalNotification(System.String)
extern "C" void AlertControl_CancelLocalNotification_m8_80 (AlertControl_t8_9 * __this, String_t* ____notificationID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AlertControl::ScheduleLocalNotification(VoxelBusters.NativePlugins.CrossPlatformNotification)
extern "C" String_t* AlertControl_ScheduleLocalNotification_m8_81 (AlertControl_t8_9 * __this, CrossPlatformNotification_t8_259 * ____notification, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.NativePlugins.CrossPlatformNotification AlertControl::CreateNotification(System.DateTime,VoxelBusters.NativePlugins.eNotificationRepeatInterval)
extern "C" CrossPlatformNotification_t8_259 * AlertControl_CreateNotification_m8_82 (AlertControl_t8_9 * __this, DateTime_t1_150  ____time, int32_t ____repeatInterval, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AlertControl::GoToSetting()
extern "C" void AlertControl_GoToSetting_m8_83 (AlertControl_t8_9 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AlertControl::CheckInServer(System.Action)
extern "C" Object_t * AlertControl_CheckInServer_m8_84 (AlertControl_t8_9 * __this, Action_t5_11 * ____action, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AlertControl::<CheckIn>m__0()
extern "C" void AlertControl_U3CCheckInU3Em__0_m8_85 (AlertControl_t8_9 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
