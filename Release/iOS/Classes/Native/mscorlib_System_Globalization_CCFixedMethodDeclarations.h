﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Globalization.CCFixed
struct CCFixed_t1_342;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime.h"
#include "mscorlib_System_DayOfWeek.h"

// System.Void System.Globalization.CCFixed::.ctor()
extern "C" void CCFixed__ctor_m1_3708 (CCFixed_t1_342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.CCFixed::ToDateTime(System.Int32)
extern "C" DateTime_t1_150  CCFixed_ToDateTime_m1_3709 (Object_t * __this /* static, unused */, int32_t ___date, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.CCFixed::ToDateTime(System.Int32,System.Int32,System.Int32,System.Int32,System.Double)
extern "C" DateTime_t1_150  CCFixed_ToDateTime_m1_3710 (Object_t * __this /* static, unused */, int32_t ___date, int32_t ___hour, int32_t ___minute, int32_t ___second, double ___milliseconds, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCFixed::FromDateTime(System.DateTime)
extern "C" int32_t CCFixed_FromDateTime_m1_3711 (Object_t * __this /* static, unused */, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DayOfWeek System.Globalization.CCFixed::day_of_week(System.Int32)
extern "C" int32_t CCFixed_day_of_week_m1_3712 (Object_t * __this /* static, unused */, int32_t ___date, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCFixed::kday_on_or_before(System.Int32,System.Int32)
extern "C" int32_t CCFixed_kday_on_or_before_m1_3713 (Object_t * __this /* static, unused */, int32_t ___date, int32_t ___k, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCFixed::kday_on_or_after(System.Int32,System.Int32)
extern "C" int32_t CCFixed_kday_on_or_after_m1_3714 (Object_t * __this /* static, unused */, int32_t ___date, int32_t ___k, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCFixed::kd_nearest(System.Int32,System.Int32)
extern "C" int32_t CCFixed_kd_nearest_m1_3715 (Object_t * __this /* static, unused */, int32_t ___date, int32_t ___k, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCFixed::kday_after(System.Int32,System.Int32)
extern "C" int32_t CCFixed_kday_after_m1_3716 (Object_t * __this /* static, unused */, int32_t ___date, int32_t ___k, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCFixed::kday_before(System.Int32,System.Int32)
extern "C" int32_t CCFixed_kday_before_m1_3717 (Object_t * __this /* static, unused */, int32_t ___date, int32_t ___k, const MethodInfo* method) IL2CPP_METHOD_ATTR;
