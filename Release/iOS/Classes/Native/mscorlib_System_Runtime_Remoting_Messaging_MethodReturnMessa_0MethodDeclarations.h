﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Messaging.MethodReturnMessageWrapper
struct MethodReturnMessageWrapper_t1_952;
// System.Runtime.Remoting.Messaging.IMethodReturnMessage
struct IMethodReturnMessage_t1_1718;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Exception
struct Exception_t1_33;
// System.Runtime.Remoting.Messaging.LogicalCallContext
struct LogicalCallContext_t1_941;
// System.Reflection.MethodBase
struct MethodBase_t1_335;
// System.String
struct String_t;
// System.Object
struct Object_t;
// System.Collections.IDictionary
struct IDictionary_t1_35;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Messaging.MethodReturnMessageWrapper::.ctor(System.Runtime.Remoting.Messaging.IMethodReturnMessage)
extern "C" void MethodReturnMessageWrapper__ctor_m1_8550 (MethodReturnMessageWrapper_t1_952 * __this, Object_t * ___msg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.Remoting.Messaging.MethodReturnMessageWrapper::get_ArgCount()
extern "C" int32_t MethodReturnMessageWrapper_get_ArgCount_m1_8551 (MethodReturnMessageWrapper_t1_952 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Runtime.Remoting.Messaging.MethodReturnMessageWrapper::get_Args()
extern "C" ObjectU5BU5D_t1_272* MethodReturnMessageWrapper_get_Args_m1_8552 (MethodReturnMessageWrapper_t1_952 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Messaging.MethodReturnMessageWrapper::set_Args(System.Object[])
extern "C" void MethodReturnMessageWrapper_set_Args_m1_8553 (MethodReturnMessageWrapper_t1_952 * __this, ObjectU5BU5D_t1_272* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception System.Runtime.Remoting.Messaging.MethodReturnMessageWrapper::get_Exception()
extern "C" Exception_t1_33 * MethodReturnMessageWrapper_get_Exception_m1_8554 (MethodReturnMessageWrapper_t1_952 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Messaging.MethodReturnMessageWrapper::set_Exception(System.Exception)
extern "C" void MethodReturnMessageWrapper_set_Exception_m1_8555 (MethodReturnMessageWrapper_t1_952 * __this, Exception_t1_33 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Remoting.Messaging.MethodReturnMessageWrapper::get_HasVarArgs()
extern "C" bool MethodReturnMessageWrapper_get_HasVarArgs_m1_8556 (MethodReturnMessageWrapper_t1_952 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Messaging.LogicalCallContext System.Runtime.Remoting.Messaging.MethodReturnMessageWrapper::get_LogicalCallContext()
extern "C" LogicalCallContext_t1_941 * MethodReturnMessageWrapper_get_LogicalCallContext_m1_8557 (MethodReturnMessageWrapper_t1_952 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodBase System.Runtime.Remoting.Messaging.MethodReturnMessageWrapper::get_MethodBase()
extern "C" MethodBase_t1_335 * MethodReturnMessageWrapper_get_MethodBase_m1_8558 (MethodReturnMessageWrapper_t1_952 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Messaging.MethodReturnMessageWrapper::get_MethodName()
extern "C" String_t* MethodReturnMessageWrapper_get_MethodName_m1_8559 (MethodReturnMessageWrapper_t1_952 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Messaging.MethodReturnMessageWrapper::get_MethodSignature()
extern "C" Object_t * MethodReturnMessageWrapper_get_MethodSignature_m1_8560 (MethodReturnMessageWrapper_t1_952 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.Remoting.Messaging.MethodReturnMessageWrapper::get_OutArgCount()
extern "C" int32_t MethodReturnMessageWrapper_get_OutArgCount_m1_8561 (MethodReturnMessageWrapper_t1_952 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Runtime.Remoting.Messaging.MethodReturnMessageWrapper::get_OutArgs()
extern "C" ObjectU5BU5D_t1_272* MethodReturnMessageWrapper_get_OutArgs_m1_8562 (MethodReturnMessageWrapper_t1_952 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IDictionary System.Runtime.Remoting.Messaging.MethodReturnMessageWrapper::get_Properties()
extern "C" Object_t * MethodReturnMessageWrapper_get_Properties_m1_8563 (MethodReturnMessageWrapper_t1_952 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Messaging.MethodReturnMessageWrapper::get_ReturnValue()
extern "C" Object_t * MethodReturnMessageWrapper_get_ReturnValue_m1_8564 (MethodReturnMessageWrapper_t1_952 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Messaging.MethodReturnMessageWrapper::set_ReturnValue(System.Object)
extern "C" void MethodReturnMessageWrapper_set_ReturnValue_m1_8565 (MethodReturnMessageWrapper_t1_952 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Messaging.MethodReturnMessageWrapper::get_TypeName()
extern "C" String_t* MethodReturnMessageWrapper_get_TypeName_m1_8566 (MethodReturnMessageWrapper_t1_952 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Messaging.MethodReturnMessageWrapper::get_Uri()
extern "C" String_t* MethodReturnMessageWrapper_get_Uri_m1_8567 (MethodReturnMessageWrapper_t1_952 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Messaging.MethodReturnMessageWrapper::set_Uri(System.String)
extern "C" void MethodReturnMessageWrapper_set_Uri_m1_8568 (MethodReturnMessageWrapper_t1_952 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Messaging.MethodReturnMessageWrapper::GetArg(System.Int32)
extern "C" Object_t * MethodReturnMessageWrapper_GetArg_m1_8569 (MethodReturnMessageWrapper_t1_952 * __this, int32_t ___argNum, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Messaging.MethodReturnMessageWrapper::GetArgName(System.Int32)
extern "C" String_t* MethodReturnMessageWrapper_GetArgName_m1_8570 (MethodReturnMessageWrapper_t1_952 * __this, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Messaging.MethodReturnMessageWrapper::GetOutArg(System.Int32)
extern "C" Object_t * MethodReturnMessageWrapper_GetOutArg_m1_8571 (MethodReturnMessageWrapper_t1_952 * __this, int32_t ___argNum, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Messaging.MethodReturnMessageWrapper::GetOutArgName(System.Int32)
extern "C" String_t* MethodReturnMessageWrapper_GetOutArgName_m1_8572 (MethodReturnMessageWrapper_t1_952 * __this, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
