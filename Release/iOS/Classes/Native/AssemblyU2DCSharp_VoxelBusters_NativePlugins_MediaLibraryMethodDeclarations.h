﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.MediaLibrary
struct MediaLibrary_t8_245;
// System.String
struct String_t;
// System.Collections.IDictionary
struct IDictionary_t1_35;
// VoxelBusters.NativePlugins.MediaLibrary/PickImageCompletion
struct PickImageCompletion_t8_240;
// VoxelBusters.NativePlugins.MediaLibrary/SaveImageToGalleryCompletion
struct SaveImageToGalleryCompletion_t8_241;
// UnityEngine.Texture2D
struct Texture2D_t6_33;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// VoxelBusters.NativePlugins.MediaLibrary/PlayVideoCompletion
struct PlayVideoCompletion_t8_243;
// VoxelBusters.NativePlugins.MediaLibrary/PickVideoCompletion
struct PickVideoCompletion_t8_242;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_ePickImageFinis.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_ePickVideoFinis.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_ePlayVideoFinis.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eImageSource.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_URL.h"

// System.Void VoxelBusters.NativePlugins.MediaLibrary::.ctor()
extern "C" void MediaLibrary__ctor_m8_1369 (MediaLibrary_t8_245 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.MediaLibrary::PickImageFinished(System.String)
extern "C" void MediaLibrary_PickImageFinished_m8_1370 (MediaLibrary_t8_245 * __this, String_t* ____responseJson, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.MediaLibrary::PickImageFinished(System.String,VoxelBusters.NativePlugins.ePickImageFinishReason)
extern "C" void MediaLibrary_PickImageFinished_m8_1371 (MediaLibrary_t8_245 * __this, String_t* ____imagePath, int32_t ____finishReason, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.MediaLibrary::SaveImageToGalleryFinished(System.String)
extern "C" void MediaLibrary_SaveImageToGalleryFinished_m8_1372 (MediaLibrary_t8_245 * __this, String_t* ____savedStatus, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.MediaLibrary::SaveImageToGalleryFinished(System.Boolean)
extern "C" void MediaLibrary_SaveImageToGalleryFinished_m8_1373 (MediaLibrary_t8_245 * __this, bool ____savedSuccessfully, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.MediaLibrary::PickVideoFinished(System.String)
extern "C" void MediaLibrary_PickVideoFinished_m8_1374 (MediaLibrary_t8_245 * __this, String_t* ____reasonStr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.MediaLibrary::PickVideoFinished(VoxelBusters.NativePlugins.ePickVideoFinishReason)
extern "C" void MediaLibrary_PickVideoFinished_m8_1375 (MediaLibrary_t8_245 * __this, int32_t ____finishReason, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.MediaLibrary::PlayVideoFinished(System.String)
extern "C" void MediaLibrary_PlayVideoFinished_m8_1376 (MediaLibrary_t8_245 * __this, String_t* ____reasonStr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.MediaLibrary::PlayVideoFinished(VoxelBusters.NativePlugins.ePlayVideoFinishReason)
extern "C" void MediaLibrary_PlayVideoFinished_m8_1377 (MediaLibrary_t8_245 * __this, int32_t ____finishReason, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.MediaLibrary::ParsePickImageFinishedData(System.Collections.IDictionary,System.String&,VoxelBusters.NativePlugins.ePickImageFinishReason&)
extern "C" void MediaLibrary_ParsePickImageFinishedData_m8_1378 (MediaLibrary_t8_245 * __this, Object_t * ____infoDict, String_t** ____selectedImagePath, int32_t* ____finishReason, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.MediaLibrary::ParsePickVideoFinishedData(System.String,VoxelBusters.NativePlugins.ePickVideoFinishReason&)
extern "C" void MediaLibrary_ParsePickVideoFinishedData_m8_1379 (MediaLibrary_t8_245 * __this, String_t* ____reasonString, int32_t* ____finishReason, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.MediaLibrary::ParsePlayVideoFinishedData(System.String,VoxelBusters.NativePlugins.ePlayVideoFinishReason&)
extern "C" void MediaLibrary_ParsePlayVideoFinishedData_m8_1380 (MediaLibrary_t8_245 * __this, String_t* ____reasonString, int32_t* ____finishReason, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.MediaLibrary::Awake()
extern "C" void MediaLibrary_Awake_m8_1381 (MediaLibrary_t8_245 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.NativePlugins.MediaLibrary::IsCameraSupported()
extern "C" bool MediaLibrary_IsCameraSupported_m8_1382 (MediaLibrary_t8_245 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.MediaLibrary::PickImage(VoxelBusters.NativePlugins.eImageSource,System.Single,VoxelBusters.NativePlugins.MediaLibrary/PickImageCompletion)
extern "C" void MediaLibrary_PickImage_m8_1383 (MediaLibrary_t8_245 * __this, int32_t ____source, float ____scaleFactor, PickImageCompletion_t8_240 * ____onCompletion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.MediaLibrary::SaveScreenshotToGallery(VoxelBusters.NativePlugins.MediaLibrary/SaveImageToGalleryCompletion)
extern "C" void MediaLibrary_SaveScreenshotToGallery_m8_1384 (MediaLibrary_t8_245 * __this, SaveImageToGalleryCompletion_t8_241 * ____onCompletion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.MediaLibrary::SaveImageToGallery(VoxelBusters.Utility.URL,VoxelBusters.NativePlugins.MediaLibrary/SaveImageToGalleryCompletion)
extern "C" void MediaLibrary_SaveImageToGallery_m8_1385 (MediaLibrary_t8_245 * __this, URL_t8_156  ____URL, SaveImageToGalleryCompletion_t8_241 * ____onCompletion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.MediaLibrary::SaveImageToGallery(UnityEngine.Texture2D,VoxelBusters.NativePlugins.MediaLibrary/SaveImageToGalleryCompletion)
extern "C" void MediaLibrary_SaveImageToGallery_m8_1386 (MediaLibrary_t8_245 * __this, Texture2D_t6_33 * ____texture, SaveImageToGalleryCompletion_t8_241 * ____onCompletion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.MediaLibrary::SaveImageToGallery(System.Byte[],VoxelBusters.NativePlugins.MediaLibrary/SaveImageToGalleryCompletion)
extern "C" void MediaLibrary_SaveImageToGallery_m8_1387 (MediaLibrary_t8_245 * __this, ByteU5BU5D_t1_109* ____imageByteArray, SaveImageToGalleryCompletion_t8_241 * ____onCompletion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.MediaLibrary::PlayYoutubeVideo(System.String,VoxelBusters.NativePlugins.MediaLibrary/PlayVideoCompletion)
extern "C" void MediaLibrary_PlayYoutubeVideo_m8_1388 (MediaLibrary_t8_245 * __this, String_t* ____videoID, PlayVideoCompletion_t8_243 * ____onCompletion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.MediaLibrary::PlayEmbeddedVideo(System.String,VoxelBusters.NativePlugins.MediaLibrary/PlayVideoCompletion)
extern "C" void MediaLibrary_PlayEmbeddedVideo_m8_1389 (MediaLibrary_t8_245 * __this, String_t* ____embedHTMLString, PlayVideoCompletion_t8_243 * ____onCompletion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.MediaLibrary::PlayVideoFromURL(VoxelBusters.Utility.URL,VoxelBusters.NativePlugins.MediaLibrary/PlayVideoCompletion)
extern "C" void MediaLibrary_PlayVideoFromURL_m8_1390 (MediaLibrary_t8_245 * __this, URL_t8_156  ____URL, PlayVideoCompletion_t8_243 * ____onCompletion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.MediaLibrary::PlayVideoFromGallery(VoxelBusters.NativePlugins.MediaLibrary/PickVideoCompletion,VoxelBusters.NativePlugins.MediaLibrary/PlayVideoCompletion)
extern "C" void MediaLibrary_PlayVideoFromGallery_m8_1391 (MediaLibrary_t8_245 * __this, PickVideoCompletion_t8_242 * ____onPickVideoCompletion, PlayVideoCompletion_t8_243 * ____onPlayVideoCompletion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.MediaLibrary::ExtractYoutubeVideoID(System.String)
extern "C" String_t* MediaLibrary_ExtractYoutubeVideoID_m8_1392 (MediaLibrary_t8_245 * __this, String_t* ____url, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.MediaLibrary::GetYoutubeEmbedHTMLString(System.String)
extern "C" String_t* MediaLibrary_GetYoutubeEmbedHTMLString_m8_1393 (MediaLibrary_t8_245 * __this, String_t* ____videoID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
