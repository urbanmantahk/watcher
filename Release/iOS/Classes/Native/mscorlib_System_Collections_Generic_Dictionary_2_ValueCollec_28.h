﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__19.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<ExifLibrary.ExifTag,System.Object>
struct  Enumerator_t1_2673 
{
	// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator::host_enumerator
	Enumerator_t1_2670  ___host_enumerator_0;
};
