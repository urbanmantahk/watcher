﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

extern "C" void NsScope_t4_142_marshal(const NsScope_t4_142& unmarshaled, NsScope_t4_142_marshaled& marshaled);
extern "C" void NsScope_t4_142_marshal_back(const NsScope_t4_142_marshaled& marshaled, NsScope_t4_142& unmarshaled);
extern "C" void NsScope_t4_142_marshal_cleanup(NsScope_t4_142_marshaled& marshaled);
