﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_173.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_22348_gshared (InternalEnumerator_1_t1_2467 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1_22348(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1_2467 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m1_22348_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_22349_gshared (InternalEnumerator_1_t1_2467 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_22349(__this, method) (( void (*) (InternalEnumerator_1_t1_2467 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_22349_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_22350_gshared (InternalEnumerator_1_t1_2467 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_22350(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1_2467 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_22350_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_22351_gshared (InternalEnumerator_1_t1_2467 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1_22351(__this, method) (( void (*) (InternalEnumerator_1_t1_2467 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1_22351_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_22352_gshared (InternalEnumerator_1_t1_2467 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1_22352(__this, method) (( bool (*) (InternalEnumerator_1_t1_2467 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1_22352_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::get_Current()
extern "C" RaycastResult_t7_31  InternalEnumerator_1_get_Current_m1_22353_gshared (InternalEnumerator_1_t1_2467 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1_22353(__this, method) (( RaycastResult_t7_31  (*) (InternalEnumerator_1_t1_2467 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1_22353_gshared)(__this, method)
