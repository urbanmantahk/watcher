﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.GameServicesSettings
struct GameServicesSettings_t8_238;
// VoxelBusters.NativePlugins.GameServicesSettings/AndroidSettings
struct AndroidSettings_t8_236;
// VoxelBusters.NativePlugins.GameServicesSettings/iOSSettings
struct iOSSettings_t8_237;
// VoxelBusters.NativePlugins.IDContainer[]
struct IDContainerU5BU5D_t8_239;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.GameServicesSettings::.ctor()
extern "C" void GameServicesSettings__ctor_m8_1338 (GameServicesSettings_t8_238 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.NativePlugins.GameServicesSettings/AndroidSettings VoxelBusters.NativePlugins.GameServicesSettings::get_Android()
extern "C" AndroidSettings_t8_236 * GameServicesSettings_get_Android_m8_1339 (GameServicesSettings_t8_238 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.GameServicesSettings::set_Android(VoxelBusters.NativePlugins.GameServicesSettings/AndroidSettings)
extern "C" void GameServicesSettings_set_Android_m8_1340 (GameServicesSettings_t8_238 * __this, AndroidSettings_t8_236 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.NativePlugins.GameServicesSettings/iOSSettings VoxelBusters.NativePlugins.GameServicesSettings::get_IOS()
extern "C" iOSSettings_t8_237 * GameServicesSettings_get_IOS_m8_1341 (GameServicesSettings_t8_238 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.GameServicesSettings::set_IOS(VoxelBusters.NativePlugins.GameServicesSettings/iOSSettings)
extern "C" void GameServicesSettings_set_IOS_m8_1342 (GameServicesSettings_t8_238 * __this, iOSSettings_t8_237 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.NativePlugins.IDContainer[] VoxelBusters.NativePlugins.GameServicesSettings::get_AchievementIDCollection()
extern "C" IDContainerU5BU5D_t8_239* GameServicesSettings_get_AchievementIDCollection_m8_1343 (GameServicesSettings_t8_238 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.GameServicesSettings::set_AchievementIDCollection(VoxelBusters.NativePlugins.IDContainer[])
extern "C" void GameServicesSettings_set_AchievementIDCollection_m8_1344 (GameServicesSettings_t8_238 * __this, IDContainerU5BU5D_t8_239* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.NativePlugins.IDContainer[] VoxelBusters.NativePlugins.GameServicesSettings::get_LeaderboardIDCollection()
extern "C" IDContainerU5BU5D_t8_239* GameServicesSettings_get_LeaderboardIDCollection_m8_1345 (GameServicesSettings_t8_238 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.GameServicesSettings::set_LeaderboardIDCollection(VoxelBusters.NativePlugins.IDContainer[])
extern "C" void GameServicesSettings_set_LeaderboardIDCollection_m8_1346 (GameServicesSettings_t8_238 * __this, IDContainerU5BU5D_t8_239* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
