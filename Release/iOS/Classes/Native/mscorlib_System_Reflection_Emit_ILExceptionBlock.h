﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Type
struct Type_t;

#include "mscorlib_System_ValueType.h"

// System.Reflection.Emit.ILExceptionBlock
struct  ILExceptionBlock_t1_510 
{
	// System.Type System.Reflection.Emit.ILExceptionBlock::extype
	Type_t * ___extype_5;
	// System.Int32 System.Reflection.Emit.ILExceptionBlock::type
	int32_t ___type_6;
	// System.Int32 System.Reflection.Emit.ILExceptionBlock::start
	int32_t ___start_7;
	// System.Int32 System.Reflection.Emit.ILExceptionBlock::len
	int32_t ___len_8;
	// System.Int32 System.Reflection.Emit.ILExceptionBlock::filter_offset
	int32_t ___filter_offset_9;
};
