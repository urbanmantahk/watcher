﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Runtime_CompilerServices_CustomConstantAttri.h"

// System.Runtime.CompilerServices.DateTimeConstantAttribute
struct  DateTimeConstantAttribute_t1_682  : public CustomConstantAttribute_t1_681
{
	// System.Int64 System.Runtime.CompilerServices.DateTimeConstantAttribute::ticks
	int64_t ___ticks_0;
};
