﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Stack`1<System.Object>
struct Stack_1_t3_250;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1_2210;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Stack_1_Enumerator_gen.h"

// System.Void System.Collections.Generic.Stack`1<System.Object>::.ctor()
extern "C" void Stack_1__ctor_m3_1816_gshared (Stack_1_t3_250 * __this, const MethodInfo* method);
#define Stack_1__ctor_m3_1816(__this, method) (( void (*) (Stack_1_t3_250 *, const MethodInfo*))Stack_1__ctor_m3_1816_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Stack_1_System_Collections_ICollection_get_IsSynchronized_m3_1818_gshared (Stack_1_t3_250 * __this, const MethodInfo* method);
#define Stack_1_System_Collections_ICollection_get_IsSynchronized_m3_1818(__this, method) (( bool (*) (Stack_1_t3_250 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_IsSynchronized_m3_1818_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Stack_1_System_Collections_ICollection_get_SyncRoot_m3_1820_gshared (Stack_1_t3_250 * __this, const MethodInfo* method);
#define Stack_1_System_Collections_ICollection_get_SyncRoot_m3_1820(__this, method) (( Object_t * (*) (Stack_1_t3_250 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_SyncRoot_m3_1820_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Stack_1_System_Collections_ICollection_CopyTo_m3_1822_gshared (Stack_1_t3_250 * __this, Array_t * ___dest, int32_t ___idx, const MethodInfo* method);
#define Stack_1_System_Collections_ICollection_CopyTo_m3_1822(__this, ___dest, ___idx, method) (( void (*) (Stack_1_t3_250 *, Array_t *, int32_t, const MethodInfo*))Stack_1_System_Collections_ICollection_CopyTo_m3_1822_gshared)(__this, ___dest, ___idx, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3_1824_gshared (Stack_1_t3_250 * __this, const MethodInfo* method);
#define Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3_1824(__this, method) (( Object_t* (*) (Stack_1_t3_250 *, const MethodInfo*))Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3_1824_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Stack_1_System_Collections_IEnumerable_GetEnumerator_m3_1826_gshared (Stack_1_t3_250 * __this, const MethodInfo* method);
#define Stack_1_System_Collections_IEnumerable_GetEnumerator_m3_1826(__this, method) (( Object_t * (*) (Stack_1_t3_250 *, const MethodInfo*))Stack_1_System_Collections_IEnumerable_GetEnumerator_m3_1826_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<System.Object>::Contains(T)
extern "C" bool Stack_1_Contains_m3_1827_gshared (Stack_1_t3_250 * __this, Object_t * ___t, const MethodInfo* method);
#define Stack_1_Contains_m3_1827(__this, ___t, method) (( bool (*) (Stack_1_t3_250 *, Object_t *, const MethodInfo*))Stack_1_Contains_m3_1827_gshared)(__this, ___t, method)
// T System.Collections.Generic.Stack`1<System.Object>::Peek()
extern "C" Object_t * Stack_1_Peek_m3_1829_gshared (Stack_1_t3_250 * __this, const MethodInfo* method);
#define Stack_1_Peek_m3_1829(__this, method) (( Object_t * (*) (Stack_1_t3_250 *, const MethodInfo*))Stack_1_Peek_m3_1829_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Object>::Pop()
extern "C" Object_t * Stack_1_Pop_m3_1830_gshared (Stack_1_t3_250 * __this, const MethodInfo* method);
#define Stack_1_Pop_m3_1830(__this, method) (( Object_t * (*) (Stack_1_t3_250 *, const MethodInfo*))Stack_1_Pop_m3_1830_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Object>::Push(T)
extern "C" void Stack_1_Push_m3_1831_gshared (Stack_1_t3_250 * __this, Object_t * ___t, const MethodInfo* method);
#define Stack_1_Push_m3_1831(__this, ___t, method) (( void (*) (Stack_1_t3_250 *, Object_t *, const MethodInfo*))Stack_1_Push_m3_1831_gshared)(__this, ___t, method)
// System.Int32 System.Collections.Generic.Stack`1<System.Object>::get_Count()
extern "C" int32_t Stack_1_get_Count_m3_1833_gshared (Stack_1_t3_250 * __this, const MethodInfo* method);
#define Stack_1_get_Count_m3_1833(__this, method) (( int32_t (*) (Stack_1_t3_250 *, const MethodInfo*))Stack_1_get_Count_m3_1833_gshared)(__this, method)
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<System.Object>::GetEnumerator()
extern "C" Enumerator_t3_251  Stack_1_GetEnumerator_m3_1835_gshared (Stack_1_t3_250 * __this, const MethodInfo* method);
#define Stack_1_GetEnumerator_m3_1835(__this, method) (( Enumerator_t3_251  (*) (Stack_1_t3_250 *, const MethodInfo*))Stack_1_GetEnumerator_m3_1835_gshared)(__this, method)
