﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.Internal.iOSWebViewMessage
struct iOSWebViewMessage_t8_315;
// System.Collections.IDictionary
struct IDictionary_t1_35;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.Internal.iOSWebViewMessage::.ctor(System.Collections.IDictionary)
extern "C" void iOSWebViewMessage__ctor_m8_1860 (iOSWebViewMessage_t8_315 * __this, Object_t * ____schemeDataJsonDict, const MethodInfo* method) IL2CPP_METHOD_ATTR;
