﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/Transform`1<ExifLibrary.ExifTag,System.Object,ExifLibrary.ExifTag>
struct Transform_1_t1_2671;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTag.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<ExifLibrary.ExifTag,System.Object,ExifLibrary.ExifTag>::.ctor(System.Object,System.IntPtr)
extern "C" void Transform_1__ctor_m1_26478_gshared (Transform_1_t1_2671 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Transform_1__ctor_m1_26478(__this, ___object, ___method, method) (( void (*) (Transform_1_t1_2671 *, Object_t *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m1_26478_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<ExifLibrary.ExifTag,System.Object,ExifLibrary.ExifTag>::Invoke(TKey,TValue)
extern "C" int32_t Transform_1_Invoke_m1_26479_gshared (Transform_1_t1_2671 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Transform_1_Invoke_m1_26479(__this, ___key, ___value, method) (( int32_t (*) (Transform_1_t1_2671 *, int32_t, Object_t *, const MethodInfo*))Transform_1_Invoke_m1_26479_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<ExifLibrary.ExifTag,System.Object,ExifLibrary.ExifTag>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C" Object_t * Transform_1_BeginInvoke_m1_26480_gshared (Transform_1_t1_2671 * __this, int32_t ___key, Object_t * ___value, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Transform_1_BeginInvoke_m1_26480(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t1_2671 *, int32_t, Object_t *, AsyncCallback_t1_28 *, Object_t *, const MethodInfo*))Transform_1_BeginInvoke_m1_26480_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<ExifLibrary.ExifTag,System.Object,ExifLibrary.ExifTag>::EndInvoke(System.IAsyncResult)
extern "C" int32_t Transform_1_EndInvoke_m1_26481_gshared (Transform_1_t1_2671 * __this, Object_t * ___result, const MethodInfo* method);
#define Transform_1_EndInvoke_m1_26481(__this, ___result, method) (( int32_t (*) (Transform_1_t1_2671 *, Object_t *, const MethodInfo*))Transform_1_EndInvoke_m1_26481_gshared)(__this, ___result, method)
