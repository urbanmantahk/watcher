﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.AssemblyKeyNameAttribute
struct AssemblyKeyNameAttribute_t1_575;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Reflection.AssemblyKeyNameAttribute::.ctor(System.String)
extern "C" void AssemblyKeyNameAttribute__ctor_m1_6693 (AssemblyKeyNameAttribute_t1_575 * __this, String_t* ___keyName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.AssemblyKeyNameAttribute::get_KeyName()
extern "C" String_t* AssemblyKeyNameAttribute_get_KeyName_m1_6694 (AssemblyKeyNameAttribute_t1_575 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
