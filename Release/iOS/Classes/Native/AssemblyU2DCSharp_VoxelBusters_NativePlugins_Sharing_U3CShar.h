﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VoxelBusters.NativePlugins.Sharing/SharingCompletion
struct SharingCompletion_t8_271;
// VoxelBusters.NativePlugins.Sharing
struct Sharing_t8_273;

#include "mscorlib_System_Object.h"

// VoxelBusters.NativePlugins.Sharing/<ShareScreenshotOnWhatsApp>c__AnonStoreyF
struct  U3CShareScreenshotOnWhatsAppU3Ec__AnonStoreyF_t8_276  : public Object_t
{
	// VoxelBusters.NativePlugins.Sharing/SharingCompletion VoxelBusters.NativePlugins.Sharing/<ShareScreenshotOnWhatsApp>c__AnonStoreyF::_onCompletion
	SharingCompletion_t8_271 * ____onCompletion_0;
	// VoxelBusters.NativePlugins.Sharing VoxelBusters.NativePlugins.Sharing/<ShareScreenshotOnWhatsApp>c__AnonStoreyF::<>f__this
	Sharing_t8_273 * ___U3CU3Ef__this_1;
};
