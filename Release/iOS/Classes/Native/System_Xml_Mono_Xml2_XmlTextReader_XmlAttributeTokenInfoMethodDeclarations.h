﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Xml2.XmlTextReader/XmlAttributeTokenInfo
struct XmlAttributeTokenInfo_t4_170;
// Mono.Xml2.XmlTextReader
struct XmlTextReader_t4_169;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Xml2.XmlTextReader/XmlAttributeTokenInfo::.ctor(Mono.Xml2.XmlTextReader)
extern "C" void XmlAttributeTokenInfo__ctor_m4_784 (XmlAttributeTokenInfo_t4_170 * __this, XmlTextReader_t4_169 * ___reader, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Xml2.XmlTextReader/XmlAttributeTokenInfo::get_Value()
extern "C" String_t* XmlAttributeTokenInfo_get_Value_m4_785 (XmlAttributeTokenInfo_t4_170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Xml2.XmlTextReader/XmlAttributeTokenInfo::set_Value(System.String)
extern "C" void XmlAttributeTokenInfo_set_Value_m4_786 (XmlAttributeTokenInfo_t4_170 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Xml2.XmlTextReader/XmlAttributeTokenInfo::Clear()
extern "C" void XmlAttributeTokenInfo_Clear_m4_787 (XmlAttributeTokenInfo_t4_170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Xml2.XmlTextReader/XmlAttributeTokenInfo::FillXmlns()
extern "C" void XmlAttributeTokenInfo_FillXmlns_m4_788 (XmlAttributeTokenInfo_t4_170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Xml2.XmlTextReader/XmlAttributeTokenInfo::FillNamespace()
extern "C" void XmlAttributeTokenInfo_FillNamespace_m4_789 (XmlAttributeTokenInfo_t4_170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
