﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_45MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::.ctor()
#define List_1__ctor_m1_15008(__this, method) (( void (*) (List_1_t1_1870 *, const MethodInfo*))List_1__ctor_m1_15034_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m1_24547(__this, ___collection, method) (( void (*) (List_1_t1_1870 *, Object_t*, const MethodInfo*))List_1__ctor_m1_15095_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::.ctor(System.Int32)
#define List_1__ctor_m1_24548(__this, ___capacity, method) (( void (*) (List_1_t1_1870 *, int32_t, const MethodInfo*))List_1__ctor_m1_15097_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::.ctor(T[],System.Int32)
#define List_1__ctor_m1_24549(__this, ___data, ___size, method) (( void (*) (List_1_t1_1870 *, IClippableU5BU5D_t7_221*, int32_t, const MethodInfo*))List_1__ctor_m1_15099_gshared)(__this, ___data, ___size, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::.cctor()
#define List_1__cctor_m1_24550(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m1_15101_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_24551(__this, method) (( Object_t* (*) (List_1_t1_1870 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_15103_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m1_24552(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_1870 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m1_15105_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1_24553(__this, method) (( Object_t * (*) (List_1_t1_1870 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1_15107_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m1_24554(__this, ___item, method) (( int32_t (*) (List_1_t1_1870 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m1_15109_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m1_24555(__this, ___item, method) (( bool (*) (List_1_t1_1870 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1_15111_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m1_24556(__this, ___item, method) (( int32_t (*) (List_1_t1_1870 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m1_15113_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m1_24557(__this, ___index, ___item, method) (( void (*) (List_1_t1_1870 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m1_15115_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m1_24558(__this, ___item, method) (( void (*) (List_1_t1_1870 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1_15117_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_24559(__this, method) (( bool (*) (List_1_t1_1870 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_15119_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m1_24560(__this, method) (( bool (*) (List_1_t1_1870 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m1_15121_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m1_24561(__this, method) (( Object_t * (*) (List_1_t1_1870 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1_15123_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m1_24562(__this, method) (( bool (*) (List_1_t1_1870 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m1_15125_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m1_24563(__this, method) (( bool (*) (List_1_t1_1870 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m1_15127_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m1_24564(__this, ___index, method) (( Object_t * (*) (List_1_t1_1870 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m1_15129_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m1_24565(__this, ___index, ___value, method) (( void (*) (List_1_t1_1870 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m1_15131_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::Add(T)
#define List_1_Add_m1_24566(__this, ___item, method) (( void (*) (List_1_t1_1870 *, Object_t *, const MethodInfo*))List_1_Add_m1_15133_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m1_24567(__this, ___newCount, method) (( void (*) (List_1_t1_1870 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m1_15135_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::CheckRange(System.Int32,System.Int32)
#define List_1_CheckRange_m1_24568(__this, ___idx, ___count, method) (( void (*) (List_1_t1_1870 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m1_15137_gshared)(__this, ___idx, ___count, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m1_24569(__this, ___collection, method) (( void (*) (List_1_t1_1870 *, Object_t*, const MethodInfo*))List_1_AddCollection_m1_15139_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m1_24570(__this, ___enumerable, method) (( void (*) (List_1_t1_1870 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m1_15141_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m1_24571(__this, ___collection, method) (( void (*) (List_1_t1_1870 *, Object_t*, const MethodInfo*))List_1_AddRange_m1_15143_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::AsReadOnly()
#define List_1_AsReadOnly_m1_24572(__this, method) (( ReadOnlyCollection_1_t1_2572 * (*) (List_1_t1_1870 *, const MethodInfo*))List_1_AsReadOnly_m1_15145_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::BinarySearch(T)
#define List_1_BinarySearch_m1_24573(__this, ___item, method) (( int32_t (*) (List_1_t1_1870 *, Object_t *, const MethodInfo*))List_1_BinarySearch_m1_15147_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::BinarySearch(T,System.Collections.Generic.IComparer`1<T>)
#define List_1_BinarySearch_m1_24574(__this, ___item, ___comparer, method) (( int32_t (*) (List_1_t1_1870 *, Object_t *, Object_t*, const MethodInfo*))List_1_BinarySearch_m1_15149_gshared)(__this, ___item, ___comparer, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::BinarySearch(System.Int32,System.Int32,T,System.Collections.Generic.IComparer`1<T>)
#define List_1_BinarySearch_m1_24575(__this, ___index, ___count, ___item, ___comparer, method) (( int32_t (*) (List_1_t1_1870 *, int32_t, int32_t, Object_t *, Object_t*, const MethodInfo*))List_1_BinarySearch_m1_15151_gshared)(__this, ___index, ___count, ___item, ___comparer, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::Clear()
#define List_1_Clear_m1_24576(__this, method) (( void (*) (List_1_t1_1870 *, const MethodInfo*))List_1_Clear_m1_15153_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::Contains(T)
#define List_1_Contains_m1_24577(__this, ___item, method) (( bool (*) (List_1_t1_1870 *, Object_t *, const MethodInfo*))List_1_Contains_m1_15155_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::CopyTo(T[])
#define List_1_CopyTo_m1_24578(__this, ___array, method) (( void (*) (List_1_t1_1870 *, IClippableU5BU5D_t7_221*, const MethodInfo*))List_1_CopyTo_m1_15157_gshared)(__this, ___array, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m1_24579(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_1870 *, IClippableU5BU5D_t7_221*, int32_t, const MethodInfo*))List_1_CopyTo_m1_15159_gshared)(__this, ___array, ___arrayIndex, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::CopyTo(System.Int32,T[],System.Int32,System.Int32)
#define List_1_CopyTo_m1_24580(__this, ___index, ___array, ___arrayIndex, ___count, method) (( void (*) (List_1_t1_1870 *, int32_t, IClippableU5BU5D_t7_221*, int32_t, int32_t, const MethodInfo*))List_1_CopyTo_m1_15161_gshared)(__this, ___index, ___array, ___arrayIndex, ___count, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::Exists(System.Predicate`1<T>)
#define List_1_Exists_m1_24581(__this, ___match, method) (( bool (*) (List_1_t1_1870 *, Predicate_1_t1_2574 *, const MethodInfo*))List_1_Exists_m1_15163_gshared)(__this, ___match, method)
// T System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::Find(System.Predicate`1<T>)
#define List_1_Find_m1_24582(__this, ___match, method) (( Object_t * (*) (List_1_t1_1870 *, Predicate_1_t1_2574 *, const MethodInfo*))List_1_Find_m1_15165_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m1_24583(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t1_2574 *, const MethodInfo*))List_1_CheckMatch_m1_15167_gshared)(__this /* static, unused */, ___match, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::FindAll(System.Predicate`1<T>)
#define List_1_FindAll_m1_24584(__this, ___match, method) (( List_1_t1_1870 * (*) (List_1_t1_1870 *, Predicate_1_t1_2574 *, const MethodInfo*))List_1_FindAll_m1_15169_gshared)(__this, ___match, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::FindAllStackBits(System.Predicate`1<T>)
#define List_1_FindAllStackBits_m1_24585(__this, ___match, method) (( List_1_t1_1870 * (*) (List_1_t1_1870 *, Predicate_1_t1_2574 *, const MethodInfo*))List_1_FindAllStackBits_m1_15171_gshared)(__this, ___match, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::FindAllList(System.Predicate`1<T>)
#define List_1_FindAllList_m1_24586(__this, ___match, method) (( List_1_t1_1870 * (*) (List_1_t1_1870 *, Predicate_1_t1_2574 *, const MethodInfo*))List_1_FindAllList_m1_15173_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::FindIndex(System.Predicate`1<T>)
#define List_1_FindIndex_m1_24587(__this, ___match, method) (( int32_t (*) (List_1_t1_1870 *, Predicate_1_t1_2574 *, const MethodInfo*))List_1_FindIndex_m1_15175_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::FindIndex(System.Int32,System.Predicate`1<T>)
#define List_1_FindIndex_m1_24588(__this, ___startIndex, ___match, method) (( int32_t (*) (List_1_t1_1870 *, int32_t, Predicate_1_t1_2574 *, const MethodInfo*))List_1_FindIndex_m1_15177_gshared)(__this, ___startIndex, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::FindIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_FindIndex_m1_24589(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_1870 *, int32_t, int32_t, Predicate_1_t1_2574 *, const MethodInfo*))List_1_FindIndex_m1_15179_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m1_24590(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_1870 *, int32_t, int32_t, Predicate_1_t1_2574 *, const MethodInfo*))List_1_GetIndex_m1_15181_gshared)(__this, ___startIndex, ___count, ___match, method)
// T System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::FindLast(System.Predicate`1<T>)
#define List_1_FindLast_m1_24591(__this, ___match, method) (( Object_t * (*) (List_1_t1_1870 *, Predicate_1_t1_2574 *, const MethodInfo*))List_1_FindLast_m1_15183_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::FindLastIndex(System.Predicate`1<T>)
#define List_1_FindLastIndex_m1_24592(__this, ___match, method) (( int32_t (*) (List_1_t1_1870 *, Predicate_1_t1_2574 *, const MethodInfo*))List_1_FindLastIndex_m1_15185_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::FindLastIndex(System.Int32,System.Predicate`1<T>)
#define List_1_FindLastIndex_m1_24593(__this, ___startIndex, ___match, method) (( int32_t (*) (List_1_t1_1870 *, int32_t, Predicate_1_t1_2574 *, const MethodInfo*))List_1_FindLastIndex_m1_15187_gshared)(__this, ___startIndex, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::FindLastIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_FindLastIndex_m1_24594(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_1870 *, int32_t, int32_t, Predicate_1_t1_2574 *, const MethodInfo*))List_1_FindLastIndex_m1_15189_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::GetLastIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetLastIndex_m1_24595(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_1870 *, int32_t, int32_t, Predicate_1_t1_2574 *, const MethodInfo*))List_1_GetLastIndex_m1_15191_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::ForEach(System.Action`1<T>)
#define List_1_ForEach_m1_24596(__this, ___action, method) (( void (*) (List_1_t1_1870 *, Action_1_t1_2575 *, const MethodInfo*))List_1_ForEach_m1_15193_gshared)(__this, ___action, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::GetEnumerator()
#define List_1_GetEnumerator_m1_24597(__this, method) (( Enumerator_t1_2576  (*) (List_1_t1_1870 *, const MethodInfo*))List_1_GetEnumerator_m1_15195_gshared)(__this, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::GetRange(System.Int32,System.Int32)
#define List_1_GetRange_m1_24598(__this, ___index, ___count, method) (( List_1_t1_1870 * (*) (List_1_t1_1870 *, int32_t, int32_t, const MethodInfo*))List_1_GetRange_m1_15197_gshared)(__this, ___index, ___count, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::IndexOf(T)
#define List_1_IndexOf_m1_24599(__this, ___item, method) (( int32_t (*) (List_1_t1_1870 *, Object_t *, const MethodInfo*))List_1_IndexOf_m1_15199_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::IndexOf(T,System.Int32)
#define List_1_IndexOf_m1_24600(__this, ___item, ___index, method) (( int32_t (*) (List_1_t1_1870 *, Object_t *, int32_t, const MethodInfo*))List_1_IndexOf_m1_15201_gshared)(__this, ___item, ___index, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::IndexOf(T,System.Int32,System.Int32)
#define List_1_IndexOf_m1_24601(__this, ___item, ___index, ___count, method) (( int32_t (*) (List_1_t1_1870 *, Object_t *, int32_t, int32_t, const MethodInfo*))List_1_IndexOf_m1_15203_gshared)(__this, ___item, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m1_24602(__this, ___start, ___delta, method) (( void (*) (List_1_t1_1870 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m1_15205_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m1_24603(__this, ___index, method) (( void (*) (List_1_t1_1870 *, int32_t, const MethodInfo*))List_1_CheckIndex_m1_15207_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::Insert(System.Int32,T)
#define List_1_Insert_m1_24604(__this, ___index, ___item, method) (( void (*) (List_1_t1_1870 *, int32_t, Object_t *, const MethodInfo*))List_1_Insert_m1_15209_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m1_24605(__this, ___collection, method) (( void (*) (List_1_t1_1870 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m1_15211_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::InsertRange(System.Int32,System.Collections.Generic.IEnumerable`1<T>)
#define List_1_InsertRange_m1_24606(__this, ___index, ___collection, method) (( void (*) (List_1_t1_1870 *, int32_t, Object_t*, const MethodInfo*))List_1_InsertRange_m1_15213_gshared)(__this, ___index, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::InsertCollection(System.Int32,System.Collections.Generic.ICollection`1<T>)
#define List_1_InsertCollection_m1_24607(__this, ___index, ___collection, method) (( void (*) (List_1_t1_1870 *, int32_t, Object_t*, const MethodInfo*))List_1_InsertCollection_m1_15215_gshared)(__this, ___index, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::InsertEnumeration(System.Int32,System.Collections.Generic.IEnumerable`1<T>)
#define List_1_InsertEnumeration_m1_24608(__this, ___index, ___enumerable, method) (( void (*) (List_1_t1_1870 *, int32_t, Object_t*, const MethodInfo*))List_1_InsertEnumeration_m1_15217_gshared)(__this, ___index, ___enumerable, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::LastIndexOf(T)
#define List_1_LastIndexOf_m1_24609(__this, ___item, method) (( int32_t (*) (List_1_t1_1870 *, Object_t *, const MethodInfo*))List_1_LastIndexOf_m1_15219_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::LastIndexOf(T,System.Int32)
#define List_1_LastIndexOf_m1_24610(__this, ___item, ___index, method) (( int32_t (*) (List_1_t1_1870 *, Object_t *, int32_t, const MethodInfo*))List_1_LastIndexOf_m1_15221_gshared)(__this, ___item, ___index, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::LastIndexOf(T,System.Int32,System.Int32)
#define List_1_LastIndexOf_m1_24611(__this, ___item, ___index, ___count, method) (( int32_t (*) (List_1_t1_1870 *, Object_t *, int32_t, int32_t, const MethodInfo*))List_1_LastIndexOf_m1_15223_gshared)(__this, ___item, ___index, ___count, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::Remove(T)
#define List_1_Remove_m1_24612(__this, ___item, method) (( bool (*) (List_1_t1_1870 *, Object_t *, const MethodInfo*))List_1_Remove_m1_15225_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m1_24613(__this, ___match, method) (( int32_t (*) (List_1_t1_1870 *, Predicate_1_t1_2574 *, const MethodInfo*))List_1_RemoveAll_m1_15227_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m1_24614(__this, ___index, method) (( void (*) (List_1_t1_1870 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1_15229_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::RemoveRange(System.Int32,System.Int32)
#define List_1_RemoveRange_m1_24615(__this, ___index, ___count, method) (( void (*) (List_1_t1_1870 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m1_15231_gshared)(__this, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::Reverse()
#define List_1_Reverse_m1_24616(__this, method) (( void (*) (List_1_t1_1870 *, const MethodInfo*))List_1_Reverse_m1_15233_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::Reverse(System.Int32,System.Int32)
#define List_1_Reverse_m1_24617(__this, ___index, ___count, method) (( void (*) (List_1_t1_1870 *, int32_t, int32_t, const MethodInfo*))List_1_Reverse_m1_15235_gshared)(__this, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::Sort()
#define List_1_Sort_m1_24618(__this, method) (( void (*) (List_1_t1_1870 *, const MethodInfo*))List_1_Sort_m1_15237_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::Sort(System.Collections.Generic.IComparer`1<T>)
#define List_1_Sort_m1_24619(__this, ___comparer, method) (( void (*) (List_1_t1_1870 *, Object_t*, const MethodInfo*))List_1_Sort_m1_15239_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m1_24620(__this, ___comparison, method) (( void (*) (List_1_t1_1870 *, Comparison_1_t1_2577 *, const MethodInfo*))List_1_Sort_m1_15241_gshared)(__this, ___comparison, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::Sort(System.Int32,System.Int32,System.Collections.Generic.IComparer`1<T>)
#define List_1_Sort_m1_24621(__this, ___index, ___count, ___comparer, method) (( void (*) (List_1_t1_1870 *, int32_t, int32_t, Object_t*, const MethodInfo*))List_1_Sort_m1_15243_gshared)(__this, ___index, ___count, ___comparer, method)
// T[] System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::ToArray()
#define List_1_ToArray_m1_24622(__this, method) (( IClippableU5BU5D_t7_221* (*) (List_1_t1_1870 *, const MethodInfo*))List_1_ToArray_m1_15035_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::TrimExcess()
#define List_1_TrimExcess_m1_24623(__this, method) (( void (*) (List_1_t1_1870 *, const MethodInfo*))List_1_TrimExcess_m1_15245_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::TrueForAll(System.Predicate`1<T>)
#define List_1_TrueForAll_m1_24624(__this, ___match, method) (( bool (*) (List_1_t1_1870 *, Predicate_1_t1_2574 *, const MethodInfo*))List_1_TrueForAll_m1_15247_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::get_Capacity()
#define List_1_get_Capacity_m1_24625(__this, method) (( int32_t (*) (List_1_t1_1870 *, const MethodInfo*))List_1_get_Capacity_m1_15249_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m1_24626(__this, ___value, method) (( void (*) (List_1_t1_1870 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1_15251_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::get_Count()
#define List_1_get_Count_m1_24627(__this, method) (( int32_t (*) (List_1_t1_1870 *, const MethodInfo*))List_1_get_Count_m1_15253_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::get_Item(System.Int32)
#define List_1_get_Item_m1_24628(__this, ___index, method) (( Object_t * (*) (List_1_t1_1870 *, int32_t, const MethodInfo*))List_1_get_Item_m1_15255_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::set_Item(System.Int32,T)
#define List_1_set_Item_m1_24629(__this, ___index, ___value, method) (( void (*) (List_1_t1_1870 *, int32_t, Object_t *, const MethodInfo*))List_1_set_Item_m1_15257_gshared)(__this, ___index, ___value, method)
