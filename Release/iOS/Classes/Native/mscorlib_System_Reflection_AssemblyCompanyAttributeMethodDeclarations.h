﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.AssemblyCompanyAttribute
struct AssemblyCompanyAttribute_t1_565;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Reflection.AssemblyCompanyAttribute::.ctor(System.String)
extern "C" void AssemblyCompanyAttribute__ctor_m1_6670 (AssemblyCompanyAttribute_t1_565 * __this, String_t* ___company, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.AssemblyCompanyAttribute::get_Company()
extern "C" String_t* AssemblyCompanyAttribute_get_Company_m1_6671 (AssemblyCompanyAttribute_t1_565 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
