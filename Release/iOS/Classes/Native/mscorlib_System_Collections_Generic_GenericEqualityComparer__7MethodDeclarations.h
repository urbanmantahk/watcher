﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.GenericEqualityComparer`1<System.Single>
struct GenericEqualityComparer_1_t1_2718;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.Single>::.ctor()
extern "C" void GenericEqualityComparer_1__ctor_m1_27103_gshared (GenericEqualityComparer_1_t1_2718 * __this, const MethodInfo* method);
#define GenericEqualityComparer_1__ctor_m1_27103(__this, method) (( void (*) (GenericEqualityComparer_1_t1_2718 *, const MethodInfo*))GenericEqualityComparer_1__ctor_m1_27103_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Single>::GetHashCode(T)
extern "C" int32_t GenericEqualityComparer_1_GetHashCode_m1_27104_gshared (GenericEqualityComparer_1_t1_2718 * __this, float ___obj, const MethodInfo* method);
#define GenericEqualityComparer_1_GetHashCode_m1_27104(__this, ___obj, method) (( int32_t (*) (GenericEqualityComparer_1_t1_2718 *, float, const MethodInfo*))GenericEqualityComparer_1_GetHashCode_m1_27104_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Single>::Equals(T,T)
extern "C" bool GenericEqualityComparer_1_Equals_m1_27105_gshared (GenericEqualityComparer_1_t1_2718 * __this, float ___x, float ___y, const MethodInfo* method);
#define GenericEqualityComparer_1_Equals_m1_27105(__this, ___x, ___y, method) (( bool (*) (GenericEqualityComparer_1_t1_2718 *, float, float, const MethodInfo*))GenericEqualityComparer_1_Equals_m1_27105_gshared)(__this, ___x, ___y, method)
