﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object.h"

// VoxelBusters.NativePlugins.RateMyApp/Settings
struct  Settings_t8_310  : public Object_t
{
	// System.Boolean VoxelBusters.NativePlugins.RateMyApp/Settings::m_isEnabled
	bool ___m_isEnabled_0;
	// System.String VoxelBusters.NativePlugins.RateMyApp/Settings::m_title
	String_t* ___m_title_1;
	// System.String VoxelBusters.NativePlugins.RateMyApp/Settings::m_message
	String_t* ___m_message_2;
	// System.Int32 VoxelBusters.NativePlugins.RateMyApp/Settings::m_showFirstPromptAfterHours
	int32_t ___m_showFirstPromptAfterHours_3;
	// System.Int32 VoxelBusters.NativePlugins.RateMyApp/Settings::m_successivePromptAfterHours
	int32_t ___m_successivePromptAfterHours_4;
	// System.Int32 VoxelBusters.NativePlugins.RateMyApp/Settings::m_successivePromptAfterLaunches
	int32_t ___m_successivePromptAfterLaunches_5;
	// System.String VoxelBusters.NativePlugins.RateMyApp/Settings::m_remindMeLaterButtonText
	String_t* ___m_remindMeLaterButtonText_6;
	// System.String VoxelBusters.NativePlugins.RateMyApp/Settings::m_rateItButtonText
	String_t* ___m_rateItButtonText_7;
	// System.String VoxelBusters.NativePlugins.RateMyApp/Settings::m_dontAskButtonText
	String_t* ___m_dontAskButtonText_8;
};
