﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_Emit_SignatureToken.h"

// System.Void System.Reflection.Emit.SignatureToken::.ctor(System.Int32)
extern "C" void SignatureToken__ctor_m1_6389 (SignatureToken_t1_552 * __this, int32_t ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.SignatureToken::.cctor()
extern "C" void SignatureToken__cctor_m1_6390 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.SignatureToken::Equals(System.Object)
extern "C" bool SignatureToken_Equals_m1_6391 (SignatureToken_t1_552 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.SignatureToken::Equals(System.Reflection.Emit.SignatureToken)
extern "C" bool SignatureToken_Equals_m1_6392 (SignatureToken_t1_552 * __this, SignatureToken_t1_552  ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.SignatureToken::GetHashCode()
extern "C" int32_t SignatureToken_GetHashCode_m1_6393 (SignatureToken_t1_552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.SignatureToken::get_Token()
extern "C" int32_t SignatureToken_get_Token_m1_6394 (SignatureToken_t1_552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.SignatureToken::op_Equality(System.Reflection.Emit.SignatureToken,System.Reflection.Emit.SignatureToken)
extern "C" bool SignatureToken_op_Equality_m1_6395 (Object_t * __this /* static, unused */, SignatureToken_t1_552  ___a, SignatureToken_t1_552  ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.SignatureToken::op_Inequality(System.Reflection.Emit.SignatureToken,System.Reflection.Emit.SignatureToken)
extern "C" bool SignatureToken_op_Inequality_m1_6396 (Object_t * __this /* static, unused */, SignatureToken_t1_552  ___a, SignatureToken_t1_552  ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
