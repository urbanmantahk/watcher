﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Action,System.Collections.Generic.Dictionary`2<System.String,System.Single>>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1_27012(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t1_2700 *, Action_t5_11 *, Dictionary_2_t1_1929 *, const MethodInfo*))KeyValuePair_2__ctor_m1_15906_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Action,System.Collections.Generic.Dictionary`2<System.String,System.Single>>::get_Key()
#define KeyValuePair_2_get_Key_m1_27013(__this, method) (( Action_t5_11 * (*) (KeyValuePair_2_t1_2700 *, const MethodInfo*))KeyValuePair_2_get_Key_m1_15907_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Action,System.Collections.Generic.Dictionary`2<System.String,System.Single>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1_27014(__this, ___value, method) (( void (*) (KeyValuePair_2_t1_2700 *, Action_t5_11 *, const MethodInfo*))KeyValuePair_2_set_Key_m1_15908_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Action,System.Collections.Generic.Dictionary`2<System.String,System.Single>>::get_Value()
#define KeyValuePair_2_get_Value_m1_27015(__this, method) (( Dictionary_2_t1_1929 * (*) (KeyValuePair_2_t1_2700 *, const MethodInfo*))KeyValuePair_2_get_Value_m1_15909_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Action,System.Collections.Generic.Dictionary`2<System.String,System.Single>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1_27016(__this, ___value, method) (( void (*) (KeyValuePair_2_t1_2700 *, Dictionary_2_t1_1929 *, const MethodInfo*))KeyValuePair_2_set_Value_m1_15910_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Action,System.Collections.Generic.Dictionary`2<System.String,System.Single>>::ToString()
#define KeyValuePair_2_ToString_m1_27017(__this, method) (( String_t* (*) (KeyValuePair_2_t1_2700 *, const MethodInfo*))KeyValuePair_2_ToString_m1_15911_gshared)(__this, method)
