﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// VoxelBusters.Utility.ShaderUtility
struct ShaderUtility_t8_29;

#include "UnityEngine_UnityEngine_ScriptableObject.h"

// VoxelBusters.Utility.AdvancedScriptableObject`1<VoxelBusters.Utility.ShaderUtility>
struct  AdvancedScriptableObject_1_t8_30  : public ScriptableObject_t6_15
{
};
struct AdvancedScriptableObject_1_t8_30_StaticFields{
	// T VoxelBusters.Utility.AdvancedScriptableObject`1::instance
	ShaderUtility_t8_29 * ___instance_6;
	// System.String VoxelBusters.Utility.AdvancedScriptableObject`1::assetGroupFolderName
	String_t* ___assetGroupFolderName_7;
};
