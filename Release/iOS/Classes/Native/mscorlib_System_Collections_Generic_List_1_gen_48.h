﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VoxelBusters.DebugPRO.Internal.ConsoleTag[]
struct ConsoleTagU5BU5D_t8_383;

#include "mscorlib_System_Object.h"

// System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleTag>
struct  List_1_t1_1909  : public Object_t
{
	// T[] System.Collections.Generic.List`1::_items
	ConsoleTagU5BU5D_t8_383* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
};
struct List_1_t1_1909_StaticFields{
	// T[] System.Collections.Generic.List`1::EmptyArray
	ConsoleTagU5BU5D_t8_383* ___EmptyArray_4;
};
