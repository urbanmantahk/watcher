﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Resources.ResourceReader/<ResourceValueAsStream>c__AnonStorey2
struct U3CResourceValueAsStreamU3Ec__AnonStorey2_t1_651;
// System.Object
struct Object_t;
// System.EventArgs
struct EventArgs_t1_158;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Resources.ResourceReader/<ResourceValueAsStream>c__AnonStorey2::.ctor()
extern "C" void U3CResourceValueAsStreamU3Ec__AnonStorey2__ctor_m1_7403 (U3CResourceValueAsStreamU3Ec__AnonStorey2_t1_651 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.ResourceReader/<ResourceValueAsStream>c__AnonStorey2::<>m__1(System.Object,System.EventArgs)
extern "C" void U3CResourceValueAsStreamU3Ec__AnonStorey2_U3CU3Em__1_m1_7404 (U3CResourceValueAsStreamU3Ec__AnonStorey2_t1_651 * __this, Object_t * ___o, EventArgs_t1_158 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
