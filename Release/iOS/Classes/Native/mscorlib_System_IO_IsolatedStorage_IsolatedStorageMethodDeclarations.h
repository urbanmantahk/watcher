﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.IO.IsolatedStorage.IsolatedStorage
struct IsolatedStorage_t1_394;
// System.Object
struct Object_t;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_IsolatedStorage_IsolatedStorageScope.h"

// System.Void System.IO.IsolatedStorage.IsolatedStorage::.ctor()
extern "C" void IsolatedStorage__ctor_m1_4588 (IsolatedStorage_t1_394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.IO.IsolatedStorage.IsolatedStorage::get_ApplicationIdentity()
extern "C" Object_t * IsolatedStorage_get_ApplicationIdentity_m1_4589 (IsolatedStorage_t1_394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.IO.IsolatedStorage.IsolatedStorage::get_AssemblyIdentity()
extern "C" Object_t * IsolatedStorage_get_AssemblyIdentity_m1_4590 (IsolatedStorage_t1_394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 System.IO.IsolatedStorage.IsolatedStorage::get_CurrentSize()
extern "C" uint64_t IsolatedStorage_get_CurrentSize_m1_4591 (IsolatedStorage_t1_394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.IO.IsolatedStorage.IsolatedStorage::get_DomainIdentity()
extern "C" Object_t * IsolatedStorage_get_DomainIdentity_m1_4592 (IsolatedStorage_t1_394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 System.IO.IsolatedStorage.IsolatedStorage::get_MaximumSize()
extern "C" uint64_t IsolatedStorage_get_MaximumSize_m1_4593 (IsolatedStorage_t1_394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.IsolatedStorage.IsolatedStorageScope System.IO.IsolatedStorage.IsolatedStorage::get_Scope()
extern "C" int32_t IsolatedStorage_get_Scope_m1_4594 (IsolatedStorage_t1_394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char System.IO.IsolatedStorage.IsolatedStorage::get_SeparatorExternal()
extern "C" uint16_t IsolatedStorage_get_SeparatorExternal_m1_4595 (IsolatedStorage_t1_394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char System.IO.IsolatedStorage.IsolatedStorage::get_SeparatorInternal()
extern "C" uint16_t IsolatedStorage_get_SeparatorInternal_m1_4596 (IsolatedStorage_t1_394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.IsolatedStorage.IsolatedStorage::InitStore(System.IO.IsolatedStorage.IsolatedStorageScope,System.Type,System.Type)
extern "C" void IsolatedStorage_InitStore_m1_4597 (IsolatedStorage_t1_394 * __this, int32_t ___scope, Type_t * ___domainEvidenceType, Type_t * ___assemblyEvidenceType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.IsolatedStorage.IsolatedStorage::InitStore(System.IO.IsolatedStorage.IsolatedStorageScope,System.Type)
extern "C" void IsolatedStorage_InitStore_m1_4598 (IsolatedStorage_t1_394 * __this, int32_t ___scope, Type_t * ___appEvidenceType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
