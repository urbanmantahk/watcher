﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.RateMyApp/Settings
struct Settings_t8_310;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.RateMyApp/Settings::.ctor()
extern "C" void Settings__ctor_m8_1940 (Settings_t8_310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.NativePlugins.RateMyApp/Settings::get_IsEnabled()
extern "C" bool Settings_get_IsEnabled_m8_1941 (Settings_t8_310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.RateMyApp/Settings::get_Title()
extern "C" String_t* Settings_get_Title_m8_1942 (Settings_t8_310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.RateMyApp/Settings::get_Message()
extern "C" String_t* Settings_get_Message_m8_1943 (Settings_t8_310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 VoxelBusters.NativePlugins.RateMyApp/Settings::get_ShowFirstPromptAfterHours()
extern "C" int32_t Settings_get_ShowFirstPromptAfterHours_m8_1944 (Settings_t8_310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 VoxelBusters.NativePlugins.RateMyApp/Settings::get_SuccessivePromptAfterHours()
extern "C" int32_t Settings_get_SuccessivePromptAfterHours_m8_1945 (Settings_t8_310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 VoxelBusters.NativePlugins.RateMyApp/Settings::get_SuccessivePromptAfterLaunches()
extern "C" int32_t Settings_get_SuccessivePromptAfterLaunches_m8_1946 (Settings_t8_310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.RateMyApp/Settings::get_RemindMeLaterButtonText()
extern "C" String_t* Settings_get_RemindMeLaterButtonText_m8_1947 (Settings_t8_310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.RateMyApp/Settings::get_RateItButtonText()
extern "C" String_t* Settings_get_RateItButtonText_m8_1948 (Settings_t8_310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.RateMyApp/Settings::get_DontAskButtonText()
extern "C" String_t* Settings_get_DontAskButtonText_m8_1949 (Settings_t8_310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
