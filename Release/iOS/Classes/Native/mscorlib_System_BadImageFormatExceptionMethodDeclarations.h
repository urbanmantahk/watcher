﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.BadImageFormatException
struct BadImageFormatException_t1_1506;
// System.String
struct String_t;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;
// System.Exception
struct Exception_t1_33;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.BadImageFormatException::.ctor()
extern "C" void BadImageFormatException__ctor_m1_13292 (BadImageFormatException_t1_1506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.BadImageFormatException::.ctor(System.String)
extern "C" void BadImageFormatException__ctor_m1_13293 (BadImageFormatException_t1_1506 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.BadImageFormatException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void BadImageFormatException__ctor_m1_13294 (BadImageFormatException_t1_1506 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.BadImageFormatException::.ctor(System.String,System.Exception)
extern "C" void BadImageFormatException__ctor_m1_13295 (BadImageFormatException_t1_1506 * __this, String_t* ___message, Exception_t1_33 * ___inner, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.BadImageFormatException::.ctor(System.String,System.String)
extern "C" void BadImageFormatException__ctor_m1_13296 (BadImageFormatException_t1_1506 * __this, String_t* ___message, String_t* ___fileName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.BadImageFormatException::.ctor(System.String,System.String,System.Exception)
extern "C" void BadImageFormatException__ctor_m1_13297 (BadImageFormatException_t1_1506 * __this, String_t* ___message, String_t* ___fileName, Exception_t1_33 * ___inner, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.BadImageFormatException::get_Message()
extern "C" String_t* BadImageFormatException_get_Message_m1_13298 (BadImageFormatException_t1_1506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.BadImageFormatException::get_FileName()
extern "C" String_t* BadImageFormatException_get_FileName_m1_13299 (BadImageFormatException_t1_1506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.BadImageFormatException::get_FusionLog()
extern "C" String_t* BadImageFormatException_get_FusionLog_m1_13300 (BadImageFormatException_t1_1506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.BadImageFormatException::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void BadImageFormatException_GetObjectData_m1_13301 (BadImageFormatException_t1_1506 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.BadImageFormatException::ToString()
extern "C" String_t* BadImageFormatException_ToString_m1_13302 (BadImageFormatException_t1_1506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
