﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Globalization_Calendar.h"
#include "mscorlib_System_Globalization_GregorianCalendarTypes.h"
#include "mscorlib_System_Nullable_1_gen.h"

// System.Globalization.GregorianCalendar
struct  GregorianCalendar_t1_370  : public Calendar_t1_338
{
	// System.Globalization.GregorianCalendarTypes System.Globalization.GregorianCalendar::m_type
	int32_t ___m_type_8;
};
struct GregorianCalendar_t1_370_StaticFields{
	// System.Nullable`1<System.DateTime> System.Globalization.GregorianCalendar::Min
	Nullable_1_t1_371  ___Min_9;
	// System.Nullable`1<System.DateTime> System.Globalization.GregorianCalendar::Max
	Nullable_1_t1_371  ___Max_10;
};
