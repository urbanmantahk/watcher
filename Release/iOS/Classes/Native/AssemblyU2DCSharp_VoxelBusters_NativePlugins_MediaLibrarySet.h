﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object.h"

// VoxelBusters.NativePlugins.MediaLibrarySettings/AndroidSettings
struct  AndroidSettings_t8_252  : public Object_t
{
	// System.String VoxelBusters.NativePlugins.MediaLibrarySettings/AndroidSettings::m_youtubeAPIKey
	String_t* ___m_youtubeAPIKey_0;
};
