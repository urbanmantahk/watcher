﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType.h"

// <PrivateImplementationDetails>/$ArrayType$380
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU24380_t1_1655 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU24380_t1_1655__padding[380];
	};
};
#pragma pack(pop, tp)
