﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.SingletonIdentity
struct SingletonIdentity_t1_1032;
// System.String
struct String_t;
// System.Runtime.Remoting.Contexts.Context
struct Context_t1_891;
// System.Type
struct Type_t;
// System.MarshalByRefObject
struct MarshalByRefObject_t1_69;
// System.Runtime.Remoting.Messaging.IMessage
struct IMessage_t1_875;
// System.Runtime.Remoting.Messaging.IMessageCtrl
struct IMessageCtrl_t1_920;
// System.Runtime.Remoting.Messaging.IMessageSink
struct IMessageSink_t1_874;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.SingletonIdentity::.ctor(System.String,System.Runtime.Remoting.Contexts.Context,System.Type)
extern "C" void SingletonIdentity__ctor_m1_9263 (SingletonIdentity_t1_1032 * __this, String_t* ___objectUri, Context_t1_891 * ___context, Type_t * ___objectType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.MarshalByRefObject System.Runtime.Remoting.SingletonIdentity::GetServerObject()
extern "C" MarshalByRefObject_t1_69 * SingletonIdentity_GetServerObject_m1_9264 (SingletonIdentity_t1_1032 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Messaging.IMessage System.Runtime.Remoting.SingletonIdentity::SyncObjectProcessMessage(System.Runtime.Remoting.Messaging.IMessage)
extern "C" Object_t * SingletonIdentity_SyncObjectProcessMessage_m1_9265 (SingletonIdentity_t1_1032 * __this, Object_t * ___msg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Messaging.IMessageCtrl System.Runtime.Remoting.SingletonIdentity::AsyncObjectProcessMessage(System.Runtime.Remoting.Messaging.IMessage,System.Runtime.Remoting.Messaging.IMessageSink)
extern "C" Object_t * SingletonIdentity_AsyncObjectProcessMessage_m1_9266 (SingletonIdentity_t1_1032 * __this, Object_t * ___msg, Object_t * ___replySink, const MethodInfo* method) IL2CPP_METHOD_ATTR;
