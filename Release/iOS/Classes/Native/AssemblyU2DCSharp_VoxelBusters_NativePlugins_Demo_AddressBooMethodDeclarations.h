﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.Demo.AddressBookDemo
struct AddressBookDemo_t8_176;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.Demo.AddressBookDemo::.ctor()
extern "C" void AddressBookDemo__ctor_m8_1056 (AddressBookDemo_t8_176 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
