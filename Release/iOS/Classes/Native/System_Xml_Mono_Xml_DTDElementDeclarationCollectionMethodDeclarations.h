﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Xml.DTDElementDeclarationCollection
struct DTDElementDeclarationCollection_t4_83;
// Mono.Xml.DTDObjectModel
struct DTDObjectModel_t4_82;
// System.String
struct String_t;
// Mono.Xml.DTDElementDeclaration
struct DTDElementDeclaration_t4_94;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Xml.DTDElementDeclarationCollection::.ctor(Mono.Xml.DTDObjectModel)
extern "C" void DTDElementDeclarationCollection__ctor_m4_165 (DTDElementDeclarationCollection_t4_83 * __this, DTDObjectModel_t4_82 * ___root, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Xml.DTDElementDeclarationCollection::Add(System.String,Mono.Xml.DTDElementDeclaration)
extern "C" void DTDElementDeclarationCollection_Add_m4_166 (DTDElementDeclarationCollection_t4_83 * __this, String_t* ___name, DTDElementDeclaration_t4_94 * ___decl, const MethodInfo* method) IL2CPP_METHOD_ATTR;
