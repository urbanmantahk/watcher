﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.NetworkConnectivitySettings/EditorSettings
struct EditorSettings_t8_254;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.NetworkConnectivitySettings/EditorSettings::.ctor()
extern "C" void EditorSettings__ctor_m8_1400 (EditorSettings_t8_254 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 VoxelBusters.NativePlugins.NetworkConnectivitySettings/EditorSettings::get_TimeOutPeriod()
extern "C" int32_t EditorSettings_get_TimeOutPeriod_m8_1401 (EditorSettings_t8_254 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NetworkConnectivitySettings/EditorSettings::set_TimeOutPeriod(System.Int32)
extern "C" void EditorSettings_set_TimeOutPeriod_m8_1402 (EditorSettings_t8_254 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 VoxelBusters.NativePlugins.NetworkConnectivitySettings/EditorSettings::get_MaxRetryCount()
extern "C" int32_t EditorSettings_get_MaxRetryCount_m8_1403 (EditorSettings_t8_254 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NetworkConnectivitySettings/EditorSettings::set_MaxRetryCount(System.Int32)
extern "C" void EditorSettings_set_MaxRetryCount_m8_1404 (EditorSettings_t8_254 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single VoxelBusters.NativePlugins.NetworkConnectivitySettings/EditorSettings::get_TimeGapBetweenPolling()
extern "C" float EditorSettings_get_TimeGapBetweenPolling_m8_1405 (EditorSettings_t8_254 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NetworkConnectivitySettings/EditorSettings::set_TimeGapBetweenPolling(System.Single)
extern "C" void EditorSettings_set_TimeGapBetweenPolling_m8_1406 (EditorSettings_t8_254 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
