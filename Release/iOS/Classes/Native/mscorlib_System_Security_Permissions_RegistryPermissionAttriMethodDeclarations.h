﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Permissions.RegistryPermissionAttribute
struct RegistryPermissionAttribute_t1_1306;
// System.String
struct String_t;
// System.Security.IPermission
struct IPermission_t1_1400;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Permissions_SecurityAction.h"

// System.Void System.Security.Permissions.RegistryPermissionAttribute::.ctor(System.Security.Permissions.SecurityAction)
extern "C" void RegistryPermissionAttribute__ctor_m1_11130 (RegistryPermissionAttribute_t1_1306 * __this, int32_t ___action, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Permissions.RegistryPermissionAttribute::get_All()
extern "C" String_t* RegistryPermissionAttribute_get_All_m1_11131 (RegistryPermissionAttribute_t1_1306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.RegistryPermissionAttribute::set_All(System.String)
extern "C" void RegistryPermissionAttribute_set_All_m1_11132 (RegistryPermissionAttribute_t1_1306 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Permissions.RegistryPermissionAttribute::get_Create()
extern "C" String_t* RegistryPermissionAttribute_get_Create_m1_11133 (RegistryPermissionAttribute_t1_1306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.RegistryPermissionAttribute::set_Create(System.String)
extern "C" void RegistryPermissionAttribute_set_Create_m1_11134 (RegistryPermissionAttribute_t1_1306 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Permissions.RegistryPermissionAttribute::get_Read()
extern "C" String_t* RegistryPermissionAttribute_get_Read_m1_11135 (RegistryPermissionAttribute_t1_1306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.RegistryPermissionAttribute::set_Read(System.String)
extern "C" void RegistryPermissionAttribute_set_Read_m1_11136 (RegistryPermissionAttribute_t1_1306 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Permissions.RegistryPermissionAttribute::get_Write()
extern "C" String_t* RegistryPermissionAttribute_get_Write_m1_11137 (RegistryPermissionAttribute_t1_1306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.RegistryPermissionAttribute::set_Write(System.String)
extern "C" void RegistryPermissionAttribute_set_Write_m1_11138 (RegistryPermissionAttribute_t1_1306 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Permissions.RegistryPermissionAttribute::get_ChangeAccessControl()
extern "C" String_t* RegistryPermissionAttribute_get_ChangeAccessControl_m1_11139 (RegistryPermissionAttribute_t1_1306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.RegistryPermissionAttribute::set_ChangeAccessControl(System.String)
extern "C" void RegistryPermissionAttribute_set_ChangeAccessControl_m1_11140 (RegistryPermissionAttribute_t1_1306 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Permissions.RegistryPermissionAttribute::get_ViewAccessControl()
extern "C" String_t* RegistryPermissionAttribute_get_ViewAccessControl_m1_11141 (RegistryPermissionAttribute_t1_1306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.RegistryPermissionAttribute::set_ViewAccessControl(System.String)
extern "C" void RegistryPermissionAttribute_set_ViewAccessControl_m1_11142 (RegistryPermissionAttribute_t1_1306 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Permissions.RegistryPermissionAttribute::get_ViewAndModify()
extern "C" String_t* RegistryPermissionAttribute_get_ViewAndModify_m1_11143 (RegistryPermissionAttribute_t1_1306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.RegistryPermissionAttribute::set_ViewAndModify(System.String)
extern "C" void RegistryPermissionAttribute_set_ViewAndModify_m1_11144 (RegistryPermissionAttribute_t1_1306 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.RegistryPermissionAttribute::CreatePermission()
extern "C" Object_t * RegistryPermissionAttribute_CreatePermission_m1_11145 (RegistryPermissionAttribute_t1_1306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
