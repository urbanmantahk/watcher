﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eBillingTransac_0.h"

// VoxelBusters.NativePlugins.eBillingTransactionVerificationState
struct  eBillingTransactionVerificationState_t8_204 
{
	// System.Int32 VoxelBusters.NativePlugins.eBillingTransactionVerificationState::value__
	int32_t ___value___1;
};
