﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__10MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1_21722(__this, ___dictionary, method) (( void (*) (Enumerator_t1_2441 *, Dictionary_2_t1_1831 *, const MethodInfo*))Enumerator__ctor_m1_21618_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_21723(__this, method) (( Object_t * (*) (Enumerator_t1_2441 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_21619_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1_21724(__this, method) (( void (*) (Enumerator_t1_2441 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_21620_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_21725(__this, method) (( DictionaryEntry_t1_284  (*) (Enumerator_t1_2441 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_21621_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_21726(__this, method) (( Object_t * (*) (Enumerator_t1_2441 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_21622_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_21727(__this, method) (( Object_t * (*) (Enumerator_t1_2441 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_21623_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::MoveNext()
#define Enumerator_MoveNext_m1_21728(__this, method) (( bool (*) (Enumerator_t1_2441 *, const MethodInfo*))Enumerator_MoveNext_m1_21624_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Current()
#define Enumerator_get_Current_m1_21729(__this, method) (( KeyValuePair_2_t1_2438  (*) (Enumerator_t1_2441 *, const MethodInfo*))Enumerator_get_Current_m1_21625_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1_21730(__this, method) (( Event_t6_162 * (*) (Enumerator_t1_2441 *, const MethodInfo*))Enumerator_get_CurrentKey_m1_21626_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1_21731(__this, method) (( int32_t (*) (Enumerator_t1_2441 *, const MethodInfo*))Enumerator_get_CurrentValue_m1_21627_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::Reset()
#define Enumerator_Reset_m1_21732(__this, method) (( void (*) (Enumerator_t1_2441 *, const MethodInfo*))Enumerator_Reset_m1_21628_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::VerifyState()
#define Enumerator_VerifyState_m1_21733(__this, method) (( void (*) (Enumerator_t1_2441 *, const MethodInfo*))Enumerator_VerifyState_m1_21629_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1_21734(__this, method) (( void (*) (Enumerator_t1_2441 *, const MethodInfo*))Enumerator_VerifyCurrent_m1_21630_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::Dispose()
#define Enumerator_Dispose_m1_21735(__this, method) (( void (*) (Enumerator_t1_2441 *, const MethodInfo*))Enumerator_Dispose_m1_21631_gshared)(__this, method)
