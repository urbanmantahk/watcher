﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.String
struct String_t;
// System.Version
struct Version_t1_578;

#include "mscorlib_System_Object.h"

// System.ApplicationId
struct  ApplicationId_t1_1329  : public Object_t
{
	// System.Byte[] System.ApplicationId::_token
	ByteU5BU5D_t1_109* ____token_0;
	// System.String System.ApplicationId::_name
	String_t* ____name_1;
	// System.Version System.ApplicationId::_version
	Version_t1_578 * ____version_2;
	// System.String System.ApplicationId::_proc
	String_t* ____proc_3;
	// System.String System.ApplicationId::_culture
	String_t* ____culture_4;
};
