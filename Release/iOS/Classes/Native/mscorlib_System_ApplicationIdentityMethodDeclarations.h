﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.ApplicationIdentity
struct ApplicationIdentity_t1_718;
// System.String
struct String_t;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.ApplicationIdentity::.ctor(System.String)
extern "C" void ApplicationIdentity__ctor_m1_13254 (ApplicationIdentity_t1_718 * __this, String_t* ___applicationIdentityFullName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ApplicationIdentity::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void ApplicationIdentity_System_Runtime_Serialization_ISerializable_GetObjectData_m1_13255 (ApplicationIdentity_t1_718 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.ApplicationIdentity::get_CodeBase()
extern "C" String_t* ApplicationIdentity_get_CodeBase_m1_13256 (ApplicationIdentity_t1_718 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.ApplicationIdentity::get_FullName()
extern "C" String_t* ApplicationIdentity_get_FullName_m1_13257 (ApplicationIdentity_t1_718 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.ApplicationIdentity::ToString()
extern "C" String_t* ApplicationIdentity_ToString_m1_13258 (ApplicationIdentity_t1_718 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
