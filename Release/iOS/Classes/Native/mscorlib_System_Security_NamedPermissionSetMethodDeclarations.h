﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.NamedPermissionSet
struct NamedPermissionSet_t1_1344;
// System.String
struct String_t;
// System.Security.PermissionSet
struct PermissionSet_t1_563;
// System.Security.SecurityElement
struct SecurityElement_t1_242;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Permissions_PermissionState.h"

// System.Void System.Security.NamedPermissionSet::.ctor()
extern "C" void NamedPermissionSet__ctor_m1_11941 (NamedPermissionSet_t1_1344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.NamedPermissionSet::.ctor(System.String,System.Security.PermissionSet)
extern "C" void NamedPermissionSet__ctor_m1_11942 (NamedPermissionSet_t1_1344 * __this, String_t* ___name, PermissionSet_t1_563 * ___permSet, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.NamedPermissionSet::.ctor(System.String,System.Security.Permissions.PermissionState)
extern "C" void NamedPermissionSet__ctor_m1_11943 (NamedPermissionSet_t1_1344 * __this, String_t* ___name, int32_t ___state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.NamedPermissionSet::.ctor(System.Security.NamedPermissionSet)
extern "C" void NamedPermissionSet__ctor_m1_11944 (NamedPermissionSet_t1_1344 * __this, NamedPermissionSet_t1_1344 * ___permSet, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.NamedPermissionSet::.ctor(System.String)
extern "C" void NamedPermissionSet__ctor_m1_11945 (NamedPermissionSet_t1_1344 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.NamedPermissionSet::get_Description()
extern "C" String_t* NamedPermissionSet_get_Description_m1_11946 (NamedPermissionSet_t1_1344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.NamedPermissionSet::set_Description(System.String)
extern "C" void NamedPermissionSet_set_Description_m1_11947 (NamedPermissionSet_t1_1344 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.NamedPermissionSet::get_Name()
extern "C" String_t* NamedPermissionSet_get_Name_m1_11948 (NamedPermissionSet_t1_1344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.NamedPermissionSet::set_Name(System.String)
extern "C" void NamedPermissionSet_set_Name_m1_11949 (NamedPermissionSet_t1_1344 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.PermissionSet System.Security.NamedPermissionSet::Copy()
extern "C" PermissionSet_t1_563 * NamedPermissionSet_Copy_m1_11950 (NamedPermissionSet_t1_1344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.NamedPermissionSet System.Security.NamedPermissionSet::Copy(System.String)
extern "C" NamedPermissionSet_t1_1344 * NamedPermissionSet_Copy_m1_11951 (NamedPermissionSet_t1_1344 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.NamedPermissionSet::FromXml(System.Security.SecurityElement)
extern "C" void NamedPermissionSet_FromXml_m1_11952 (NamedPermissionSet_t1_1344 * __this, SecurityElement_t1_242 * ___et, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityElement System.Security.NamedPermissionSet::ToXml()
extern "C" SecurityElement_t1_242 * NamedPermissionSet_ToXml_m1_11953 (NamedPermissionSet_t1_1344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.NamedPermissionSet::Equals(System.Object)
extern "C" bool NamedPermissionSet_Equals_m1_11954 (NamedPermissionSet_t1_1344 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.NamedPermissionSet::GetHashCode()
extern "C" int32_t NamedPermissionSet_GetHashCode_m1_11955 (NamedPermissionSet_t1_1344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
