﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1_1838;
// System.Collections.Generic.IEnumerable`1<System.Int32>
struct IEnumerable_1_t1_2810;
// System.Int32[]
struct Int32U5BU5D_t1_275;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t1_2773;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<System.Int32>
struct ICollection_1_t1_2771;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32>
struct ReadOnlyCollection_1_t1_2313;
// System.Collections.Generic.IComparer`1<System.Int32>
struct IComparer_1_t1_2682;
// System.Predicate`1<System.Int32>
struct Predicate_1_t1_2315;
// System.Action`1<System.Int32>
struct Action_1_t1_2316;
// System.Comparison`1<System.Int32>
struct Comparison_1_t1_2317;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_19.h"

// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor()
extern "C" void List_1__ctor_m1_19504_gshared (List_1_t1_1838 * __this, const MethodInfo* method);
#define List_1__ctor_m1_19504(__this, method) (( void (*) (List_1_t1_1838 *, const MethodInfo*))List_1__ctor_m1_19504_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m1_19505_gshared (List_1_t1_1838 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m1_19505(__this, ___collection, method) (( void (*) (List_1_t1_1838 *, Object_t*, const MethodInfo*))List_1__ctor_m1_19505_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor(System.Int32)
extern "C" void List_1__ctor_m1_19506_gshared (List_1_t1_1838 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m1_19506(__this, ___capacity, method) (( void (*) (List_1_t1_1838 *, int32_t, const MethodInfo*))List_1__ctor_m1_19506_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor(T[],System.Int32)
extern "C" void List_1__ctor_m1_19507_gshared (List_1_t1_1838 * __this, Int32U5BU5D_t1_275* ___data, int32_t ___size, const MethodInfo* method);
#define List_1__ctor_m1_19507(__this, ___data, ___size, method) (( void (*) (List_1_t1_1838 *, Int32U5BU5D_t1_275*, int32_t, const MethodInfo*))List_1__ctor_m1_19507_gshared)(__this, ___data, ___size, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::.cctor()
extern "C" void List_1__cctor_m1_19508_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m1_19508(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m1_19508_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Int32>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_19509_gshared (List_1_t1_1838 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_19509(__this, method) (( Object_t* (*) (List_1_t1_1838 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_19509_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m1_19510_gshared (List_1_t1_1838 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m1_19510(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_1838 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m1_19510_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m1_19511_gshared (List_1_t1_1838 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1_19511(__this, method) (( Object_t * (*) (List_1_t1_1838 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1_19511_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m1_19512_gshared (List_1_t1_1838 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m1_19512(__this, ___item, method) (( int32_t (*) (List_1_t1_1838 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m1_19512_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m1_19513_gshared (List_1_t1_1838 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m1_19513(__this, ___item, method) (( bool (*) (List_1_t1_1838 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1_19513_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m1_19514_gshared (List_1_t1_1838 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m1_19514(__this, ___item, method) (( int32_t (*) (List_1_t1_1838 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m1_19514_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m1_19515_gshared (List_1_t1_1838 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m1_19515(__this, ___index, ___item, method) (( void (*) (List_1_t1_1838 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m1_19515_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m1_19516_gshared (List_1_t1_1838 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m1_19516(__this, ___item, method) (( void (*) (List_1_t1_1838 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1_19516_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Int32>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_19517_gshared (List_1_t1_1838 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_19517(__this, method) (( bool (*) (List_1_t1_1838 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_19517_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m1_19518_gshared (List_1_t1_1838 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m1_19518(__this, method) (( bool (*) (List_1_t1_1838 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m1_19518_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m1_19519_gshared (List_1_t1_1838 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m1_19519(__this, method) (( Object_t * (*) (List_1_t1_1838 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1_19519_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m1_19520_gshared (List_1_t1_1838 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m1_19520(__this, method) (( bool (*) (List_1_t1_1838 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m1_19520_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m1_19521_gshared (List_1_t1_1838 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m1_19521(__this, method) (( bool (*) (List_1_t1_1838 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m1_19521_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m1_19522_gshared (List_1_t1_1838 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m1_19522(__this, ___index, method) (( Object_t * (*) (List_1_t1_1838 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m1_19522_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m1_19523_gshared (List_1_t1_1838 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m1_19523(__this, ___index, ___value, method) (( void (*) (List_1_t1_1838 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m1_19523_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Add(T)
extern "C" void List_1_Add_m1_19524_gshared (List_1_t1_1838 * __this, int32_t ___item, const MethodInfo* method);
#define List_1_Add_m1_19524(__this, ___item, method) (( void (*) (List_1_t1_1838 *, int32_t, const MethodInfo*))List_1_Add_m1_19524_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m1_19525_gshared (List_1_t1_1838 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m1_19525(__this, ___newCount, method) (( void (*) (List_1_t1_1838 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m1_19525_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::CheckRange(System.Int32,System.Int32)
extern "C" void List_1_CheckRange_m1_19526_gshared (List_1_t1_1838 * __this, int32_t ___idx, int32_t ___count, const MethodInfo* method);
#define List_1_CheckRange_m1_19526(__this, ___idx, ___count, method) (( void (*) (List_1_t1_1838 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m1_19526_gshared)(__this, ___idx, ___count, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m1_19527_gshared (List_1_t1_1838 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m1_19527(__this, ___collection, method) (( void (*) (List_1_t1_1838 *, Object_t*, const MethodInfo*))List_1_AddCollection_m1_19527_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m1_19528_gshared (List_1_t1_1838 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m1_19528(__this, ___enumerable, method) (( void (*) (List_1_t1_1838 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m1_19528_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m1_15023_gshared (List_1_t1_1838 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m1_15023(__this, ___collection, method) (( void (*) (List_1_t1_1838 *, Object_t*, const MethodInfo*))List_1_AddRange_m1_15023_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Int32>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t1_2313 * List_1_AsReadOnly_m1_19529_gshared (List_1_t1_1838 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m1_19529(__this, method) (( ReadOnlyCollection_1_t1_2313 * (*) (List_1_t1_1838 *, const MethodInfo*))List_1_AsReadOnly_m1_19529_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::BinarySearch(T)
extern "C" int32_t List_1_BinarySearch_m1_19530_gshared (List_1_t1_1838 * __this, int32_t ___item, const MethodInfo* method);
#define List_1_BinarySearch_m1_19530(__this, ___item, method) (( int32_t (*) (List_1_t1_1838 *, int32_t, const MethodInfo*))List_1_BinarySearch_m1_19530_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::BinarySearch(T,System.Collections.Generic.IComparer`1<T>)
extern "C" int32_t List_1_BinarySearch_m1_19531_gshared (List_1_t1_1838 * __this, int32_t ___item, Object_t* ___comparer, const MethodInfo* method);
#define List_1_BinarySearch_m1_19531(__this, ___item, ___comparer, method) (( int32_t (*) (List_1_t1_1838 *, int32_t, Object_t*, const MethodInfo*))List_1_BinarySearch_m1_19531_gshared)(__this, ___item, ___comparer, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::BinarySearch(System.Int32,System.Int32,T,System.Collections.Generic.IComparer`1<T>)
extern "C" int32_t List_1_BinarySearch_m1_19532_gshared (List_1_t1_1838 * __this, int32_t ___index, int32_t ___count, int32_t ___item, Object_t* ___comparer, const MethodInfo* method);
#define List_1_BinarySearch_m1_19532(__this, ___index, ___count, ___item, ___comparer, method) (( int32_t (*) (List_1_t1_1838 *, int32_t, int32_t, int32_t, Object_t*, const MethodInfo*))List_1_BinarySearch_m1_19532_gshared)(__this, ___index, ___count, ___item, ___comparer, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Clear()
extern "C" void List_1_Clear_m1_19533_gshared (List_1_t1_1838 * __this, const MethodInfo* method);
#define List_1_Clear_m1_19533(__this, method) (( void (*) (List_1_t1_1838 *, const MethodInfo*))List_1_Clear_m1_19533_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Int32>::Contains(T)
extern "C" bool List_1_Contains_m1_19534_gshared (List_1_t1_1838 * __this, int32_t ___item, const MethodInfo* method);
#define List_1_Contains_m1_19534(__this, ___item, method) (( bool (*) (List_1_t1_1838 *, int32_t, const MethodInfo*))List_1_Contains_m1_19534_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::CopyTo(T[])
extern "C" void List_1_CopyTo_m1_19535_gshared (List_1_t1_1838 * __this, Int32U5BU5D_t1_275* ___array, const MethodInfo* method);
#define List_1_CopyTo_m1_19535(__this, ___array, method) (( void (*) (List_1_t1_1838 *, Int32U5BU5D_t1_275*, const MethodInfo*))List_1_CopyTo_m1_19535_gshared)(__this, ___array, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m1_19536_gshared (List_1_t1_1838 * __this, Int32U5BU5D_t1_275* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m1_19536(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_1838 *, Int32U5BU5D_t1_275*, int32_t, const MethodInfo*))List_1_CopyTo_m1_19536_gshared)(__this, ___array, ___arrayIndex, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::CopyTo(System.Int32,T[],System.Int32,System.Int32)
extern "C" void List_1_CopyTo_m1_19537_gshared (List_1_t1_1838 * __this, int32_t ___index, Int32U5BU5D_t1_275* ___array, int32_t ___arrayIndex, int32_t ___count, const MethodInfo* method);
#define List_1_CopyTo_m1_19537(__this, ___index, ___array, ___arrayIndex, ___count, method) (( void (*) (List_1_t1_1838 *, int32_t, Int32U5BU5D_t1_275*, int32_t, int32_t, const MethodInfo*))List_1_CopyTo_m1_19537_gshared)(__this, ___index, ___array, ___arrayIndex, ___count, method)
// System.Boolean System.Collections.Generic.List`1<System.Int32>::Exists(System.Predicate`1<T>)
extern "C" bool List_1_Exists_m1_19538_gshared (List_1_t1_1838 * __this, Predicate_1_t1_2315 * ___match, const MethodInfo* method);
#define List_1_Exists_m1_19538(__this, ___match, method) (( bool (*) (List_1_t1_1838 *, Predicate_1_t1_2315 *, const MethodInfo*))List_1_Exists_m1_19538_gshared)(__this, ___match, method)
// T System.Collections.Generic.List`1<System.Int32>::Find(System.Predicate`1<T>)
extern "C" int32_t List_1_Find_m1_19539_gshared (List_1_t1_1838 * __this, Predicate_1_t1_2315 * ___match, const MethodInfo* method);
#define List_1_Find_m1_19539(__this, ___match, method) (( int32_t (*) (List_1_t1_1838 *, Predicate_1_t1_2315 *, const MethodInfo*))List_1_Find_m1_19539_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m1_19540_gshared (Object_t * __this /* static, unused */, Predicate_1_t1_2315 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m1_19540(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t1_2315 *, const MethodInfo*))List_1_CheckMatch_m1_19540_gshared)(__this /* static, unused */, ___match, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<System.Int32>::FindAll(System.Predicate`1<T>)
extern "C" List_1_t1_1838 * List_1_FindAll_m1_19541_gshared (List_1_t1_1838 * __this, Predicate_1_t1_2315 * ___match, const MethodInfo* method);
#define List_1_FindAll_m1_19541(__this, ___match, method) (( List_1_t1_1838 * (*) (List_1_t1_1838 *, Predicate_1_t1_2315 *, const MethodInfo*))List_1_FindAll_m1_19541_gshared)(__this, ___match, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<System.Int32>::FindAllStackBits(System.Predicate`1<T>)
extern "C" List_1_t1_1838 * List_1_FindAllStackBits_m1_19542_gshared (List_1_t1_1838 * __this, Predicate_1_t1_2315 * ___match, const MethodInfo* method);
#define List_1_FindAllStackBits_m1_19542(__this, ___match, method) (( List_1_t1_1838 * (*) (List_1_t1_1838 *, Predicate_1_t1_2315 *, const MethodInfo*))List_1_FindAllStackBits_m1_19542_gshared)(__this, ___match, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<System.Int32>::FindAllList(System.Predicate`1<T>)
extern "C" List_1_t1_1838 * List_1_FindAllList_m1_19543_gshared (List_1_t1_1838 * __this, Predicate_1_t1_2315 * ___match, const MethodInfo* method);
#define List_1_FindAllList_m1_19543(__this, ___match, method) (( List_1_t1_1838 * (*) (List_1_t1_1838 *, Predicate_1_t1_2315 *, const MethodInfo*))List_1_FindAllList_m1_19543_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::FindIndex(System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m1_19544_gshared (List_1_t1_1838 * __this, Predicate_1_t1_2315 * ___match, const MethodInfo* method);
#define List_1_FindIndex_m1_19544(__this, ___match, method) (( int32_t (*) (List_1_t1_1838 *, Predicate_1_t1_2315 *, const MethodInfo*))List_1_FindIndex_m1_19544_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::FindIndex(System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m1_19545_gshared (List_1_t1_1838 * __this, int32_t ___startIndex, Predicate_1_t1_2315 * ___match, const MethodInfo* method);
#define List_1_FindIndex_m1_19545(__this, ___startIndex, ___match, method) (( int32_t (*) (List_1_t1_1838 *, int32_t, Predicate_1_t1_2315 *, const MethodInfo*))List_1_FindIndex_m1_19545_gshared)(__this, ___startIndex, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::FindIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m1_19546_gshared (List_1_t1_1838 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_2315 * ___match, const MethodInfo* method);
#define List_1_FindIndex_m1_19546(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_1838 *, int32_t, int32_t, Predicate_1_t1_2315 *, const MethodInfo*))List_1_FindIndex_m1_19546_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m1_19547_gshared (List_1_t1_1838 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_2315 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m1_19547(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_1838 *, int32_t, int32_t, Predicate_1_t1_2315 *, const MethodInfo*))List_1_GetIndex_m1_19547_gshared)(__this, ___startIndex, ___count, ___match, method)
// T System.Collections.Generic.List`1<System.Int32>::FindLast(System.Predicate`1<T>)
extern "C" int32_t List_1_FindLast_m1_19548_gshared (List_1_t1_1838 * __this, Predicate_1_t1_2315 * ___match, const MethodInfo* method);
#define List_1_FindLast_m1_19548(__this, ___match, method) (( int32_t (*) (List_1_t1_1838 *, Predicate_1_t1_2315 *, const MethodInfo*))List_1_FindLast_m1_19548_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::FindLastIndex(System.Predicate`1<T>)
extern "C" int32_t List_1_FindLastIndex_m1_19549_gshared (List_1_t1_1838 * __this, Predicate_1_t1_2315 * ___match, const MethodInfo* method);
#define List_1_FindLastIndex_m1_19549(__this, ___match, method) (( int32_t (*) (List_1_t1_1838 *, Predicate_1_t1_2315 *, const MethodInfo*))List_1_FindLastIndex_m1_19549_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::FindLastIndex(System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindLastIndex_m1_19550_gshared (List_1_t1_1838 * __this, int32_t ___startIndex, Predicate_1_t1_2315 * ___match, const MethodInfo* method);
#define List_1_FindLastIndex_m1_19550(__this, ___startIndex, ___match, method) (( int32_t (*) (List_1_t1_1838 *, int32_t, Predicate_1_t1_2315 *, const MethodInfo*))List_1_FindLastIndex_m1_19550_gshared)(__this, ___startIndex, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::FindLastIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindLastIndex_m1_19551_gshared (List_1_t1_1838 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_2315 * ___match, const MethodInfo* method);
#define List_1_FindLastIndex_m1_19551(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_1838 *, int32_t, int32_t, Predicate_1_t1_2315 *, const MethodInfo*))List_1_FindLastIndex_m1_19551_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::GetLastIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetLastIndex_m1_19552_gshared (List_1_t1_1838 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_2315 * ___match, const MethodInfo* method);
#define List_1_GetLastIndex_m1_19552(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_1838 *, int32_t, int32_t, Predicate_1_t1_2315 *, const MethodInfo*))List_1_GetLastIndex_m1_19552_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::ForEach(System.Action`1<T>)
extern "C" void List_1_ForEach_m1_19553_gshared (List_1_t1_1838 * __this, Action_1_t1_2316 * ___action, const MethodInfo* method);
#define List_1_ForEach_m1_19553(__this, ___action, method) (( void (*) (List_1_t1_1838 *, Action_1_t1_2316 *, const MethodInfo*))List_1_ForEach_m1_19553_gshared)(__this, ___action, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Int32>::GetEnumerator()
extern "C" Enumerator_t1_2312  List_1_GetEnumerator_m1_19554_gshared (List_1_t1_1838 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m1_19554(__this, method) (( Enumerator_t1_2312  (*) (List_1_t1_1838 *, const MethodInfo*))List_1_GetEnumerator_m1_19554_gshared)(__this, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<System.Int32>::GetRange(System.Int32,System.Int32)
extern "C" List_1_t1_1838 * List_1_GetRange_m1_19555_gshared (List_1_t1_1838 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_GetRange_m1_19555(__this, ___index, ___count, method) (( List_1_t1_1838 * (*) (List_1_t1_1838 *, int32_t, int32_t, const MethodInfo*))List_1_GetRange_m1_19555_gshared)(__this, ___index, ___count, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m1_19556_gshared (List_1_t1_1838 * __this, int32_t ___item, const MethodInfo* method);
#define List_1_IndexOf_m1_19556(__this, ___item, method) (( int32_t (*) (List_1_t1_1838 *, int32_t, const MethodInfo*))List_1_IndexOf_m1_19556_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::IndexOf(T,System.Int32)
extern "C" int32_t List_1_IndexOf_m1_19557_gshared (List_1_t1_1838 * __this, int32_t ___item, int32_t ___index, const MethodInfo* method);
#define List_1_IndexOf_m1_19557(__this, ___item, ___index, method) (( int32_t (*) (List_1_t1_1838 *, int32_t, int32_t, const MethodInfo*))List_1_IndexOf_m1_19557_gshared)(__this, ___item, ___index, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::IndexOf(T,System.Int32,System.Int32)
extern "C" int32_t List_1_IndexOf_m1_19558_gshared (List_1_t1_1838 * __this, int32_t ___item, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_IndexOf_m1_19558(__this, ___item, ___index, ___count, method) (( int32_t (*) (List_1_t1_1838 *, int32_t, int32_t, int32_t, const MethodInfo*))List_1_IndexOf_m1_19558_gshared)(__this, ___item, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m1_19559_gshared (List_1_t1_1838 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m1_19559(__this, ___start, ___delta, method) (( void (*) (List_1_t1_1838 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m1_19559_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m1_19560_gshared (List_1_t1_1838 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m1_19560(__this, ___index, method) (( void (*) (List_1_t1_1838 *, int32_t, const MethodInfo*))List_1_CheckIndex_m1_19560_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m1_19561_gshared (List_1_t1_1838 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define List_1_Insert_m1_19561(__this, ___index, ___item, method) (( void (*) (List_1_t1_1838 *, int32_t, int32_t, const MethodInfo*))List_1_Insert_m1_19561_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m1_19562_gshared (List_1_t1_1838 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m1_19562(__this, ___collection, method) (( void (*) (List_1_t1_1838 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m1_19562_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::InsertRange(System.Int32,System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_InsertRange_m1_19563_gshared (List_1_t1_1838 * __this, int32_t ___index, Object_t* ___collection, const MethodInfo* method);
#define List_1_InsertRange_m1_19563(__this, ___index, ___collection, method) (( void (*) (List_1_t1_1838 *, int32_t, Object_t*, const MethodInfo*))List_1_InsertRange_m1_19563_gshared)(__this, ___index, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::InsertCollection(System.Int32,System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_InsertCollection_m1_19564_gshared (List_1_t1_1838 * __this, int32_t ___index, Object_t* ___collection, const MethodInfo* method);
#define List_1_InsertCollection_m1_19564(__this, ___index, ___collection, method) (( void (*) (List_1_t1_1838 *, int32_t, Object_t*, const MethodInfo*))List_1_InsertCollection_m1_19564_gshared)(__this, ___index, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::InsertEnumeration(System.Int32,System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_InsertEnumeration_m1_19565_gshared (List_1_t1_1838 * __this, int32_t ___index, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_InsertEnumeration_m1_19565(__this, ___index, ___enumerable, method) (( void (*) (List_1_t1_1838 *, int32_t, Object_t*, const MethodInfo*))List_1_InsertEnumeration_m1_19565_gshared)(__this, ___index, ___enumerable, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::LastIndexOf(T)
extern "C" int32_t List_1_LastIndexOf_m1_19566_gshared (List_1_t1_1838 * __this, int32_t ___item, const MethodInfo* method);
#define List_1_LastIndexOf_m1_19566(__this, ___item, method) (( int32_t (*) (List_1_t1_1838 *, int32_t, const MethodInfo*))List_1_LastIndexOf_m1_19566_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::LastIndexOf(T,System.Int32)
extern "C" int32_t List_1_LastIndexOf_m1_19567_gshared (List_1_t1_1838 * __this, int32_t ___item, int32_t ___index, const MethodInfo* method);
#define List_1_LastIndexOf_m1_19567(__this, ___item, ___index, method) (( int32_t (*) (List_1_t1_1838 *, int32_t, int32_t, const MethodInfo*))List_1_LastIndexOf_m1_19567_gshared)(__this, ___item, ___index, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::LastIndexOf(T,System.Int32,System.Int32)
extern "C" int32_t List_1_LastIndexOf_m1_19568_gshared (List_1_t1_1838 * __this, int32_t ___item, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_LastIndexOf_m1_19568(__this, ___item, ___index, ___count, method) (( int32_t (*) (List_1_t1_1838 *, int32_t, int32_t, int32_t, const MethodInfo*))List_1_LastIndexOf_m1_19568_gshared)(__this, ___item, ___index, ___count, method)
// System.Boolean System.Collections.Generic.List`1<System.Int32>::Remove(T)
extern "C" bool List_1_Remove_m1_19569_gshared (List_1_t1_1838 * __this, int32_t ___item, const MethodInfo* method);
#define List_1_Remove_m1_19569(__this, ___item, method) (( bool (*) (List_1_t1_1838 *, int32_t, const MethodInfo*))List_1_Remove_m1_19569_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m1_19570_gshared (List_1_t1_1838 * __this, Predicate_1_t1_2315 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m1_19570(__this, ___match, method) (( int32_t (*) (List_1_t1_1838 *, Predicate_1_t1_2315 *, const MethodInfo*))List_1_RemoveAll_m1_19570_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m1_19571_gshared (List_1_t1_1838 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m1_19571(__this, ___index, method) (( void (*) (List_1_t1_1838 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1_19571_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::RemoveRange(System.Int32,System.Int32)
extern "C" void List_1_RemoveRange_m1_19572_gshared (List_1_t1_1838 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_RemoveRange_m1_19572(__this, ___index, ___count, method) (( void (*) (List_1_t1_1838 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m1_19572_gshared)(__this, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Reverse()
extern "C" void List_1_Reverse_m1_19573_gshared (List_1_t1_1838 * __this, const MethodInfo* method);
#define List_1_Reverse_m1_19573(__this, method) (( void (*) (List_1_t1_1838 *, const MethodInfo*))List_1_Reverse_m1_19573_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Reverse(System.Int32,System.Int32)
extern "C" void List_1_Reverse_m1_19574_gshared (List_1_t1_1838 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_Reverse_m1_19574(__this, ___index, ___count, method) (( void (*) (List_1_t1_1838 *, int32_t, int32_t, const MethodInfo*))List_1_Reverse_m1_19574_gshared)(__this, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Sort()
extern "C" void List_1_Sort_m1_19575_gshared (List_1_t1_1838 * __this, const MethodInfo* method);
#define List_1_Sort_m1_19575(__this, method) (( void (*) (List_1_t1_1838 *, const MethodInfo*))List_1_Sort_m1_19575_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C" void List_1_Sort_m1_19576_gshared (List_1_t1_1838 * __this, Object_t* ___comparer, const MethodInfo* method);
#define List_1_Sort_m1_19576(__this, ___comparer, method) (( void (*) (List_1_t1_1838 *, Object_t*, const MethodInfo*))List_1_Sort_m1_19576_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m1_19577_gshared (List_1_t1_1838 * __this, Comparison_1_t1_2317 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m1_19577(__this, ___comparison, method) (( void (*) (List_1_t1_1838 *, Comparison_1_t1_2317 *, const MethodInfo*))List_1_Sort_m1_19577_gshared)(__this, ___comparison, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Sort(System.Int32,System.Int32,System.Collections.Generic.IComparer`1<T>)
extern "C" void List_1_Sort_m1_19578_gshared (List_1_t1_1838 * __this, int32_t ___index, int32_t ___count, Object_t* ___comparer, const MethodInfo* method);
#define List_1_Sort_m1_19578(__this, ___index, ___count, ___comparer, method) (( void (*) (List_1_t1_1838 *, int32_t, int32_t, Object_t*, const MethodInfo*))List_1_Sort_m1_19578_gshared)(__this, ___index, ___count, ___comparer, method)
// T[] System.Collections.Generic.List`1<System.Int32>::ToArray()
extern "C" Int32U5BU5D_t1_275* List_1_ToArray_m1_19579_gshared (List_1_t1_1838 * __this, const MethodInfo* method);
#define List_1_ToArray_m1_19579(__this, method) (( Int32U5BU5D_t1_275* (*) (List_1_t1_1838 *, const MethodInfo*))List_1_ToArray_m1_19579_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::TrimExcess()
extern "C" void List_1_TrimExcess_m1_19580_gshared (List_1_t1_1838 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m1_19580(__this, method) (( void (*) (List_1_t1_1838 *, const MethodInfo*))List_1_TrimExcess_m1_19580_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Int32>::TrueForAll(System.Predicate`1<T>)
extern "C" bool List_1_TrueForAll_m1_19581_gshared (List_1_t1_1838 * __this, Predicate_1_t1_2315 * ___match, const MethodInfo* method);
#define List_1_TrueForAll_m1_19581(__this, ___match, method) (( bool (*) (List_1_t1_1838 *, Predicate_1_t1_2315 *, const MethodInfo*))List_1_TrueForAll_m1_19581_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m1_19582_gshared (List_1_t1_1838 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m1_19582(__this, method) (( int32_t (*) (List_1_t1_1838 *, const MethodInfo*))List_1_get_Capacity_m1_19582_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m1_19583_gshared (List_1_t1_1838 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m1_19583(__this, ___value, method) (( void (*) (List_1_t1_1838 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1_19583_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::get_Count()
extern "C" int32_t List_1_get_Count_m1_19584_gshared (List_1_t1_1838 * __this, const MethodInfo* method);
#define List_1_get_Count_m1_19584(__this, method) (( int32_t (*) (List_1_t1_1838 *, const MethodInfo*))List_1_get_Count_m1_19584_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Int32>::get_Item(System.Int32)
extern "C" int32_t List_1_get_Item_m1_19585_gshared (List_1_t1_1838 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m1_19585(__this, ___index, method) (( int32_t (*) (List_1_t1_1838 *, int32_t, const MethodInfo*))List_1_get_Item_m1_19585_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m1_19586_gshared (List_1_t1_1838 * __this, int32_t ___index, int32_t ___value, const MethodInfo* method);
#define List_1_set_Item_m1_19586(__this, ___index, ___value, method) (( void (*) (List_1_t1_1838 *, int32_t, int32_t, const MethodInfo*))List_1_set_Item_m1_19586_gshared)(__this, ___index, ___value, method)
