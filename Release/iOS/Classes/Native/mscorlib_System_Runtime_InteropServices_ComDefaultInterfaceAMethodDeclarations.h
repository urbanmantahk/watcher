﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.InteropServices.ComDefaultInterfaceAttribute
struct ComDefaultInterfaceAttribute_t1_770;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.InteropServices.ComDefaultInterfaceAttribute::.ctor(System.Type)
extern "C" void ComDefaultInterfaceAttribute__ctor_m1_7607 (ComDefaultInterfaceAttribute_t1_770 * __this, Type_t * ___defaultInterface, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Runtime.InteropServices.ComDefaultInterfaceAttribute::get_Value()
extern "C" Type_t * ComDefaultInterfaceAttribute_get_Value_m1_7608 (ComDefaultInterfaceAttribute_t1_770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
