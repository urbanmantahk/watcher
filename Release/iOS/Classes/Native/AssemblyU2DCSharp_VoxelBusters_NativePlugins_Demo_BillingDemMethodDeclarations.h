﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.Demo.BillingDemo
struct BillingDemo_t8_178;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.Demo.BillingDemo::.ctor()
extern "C" void BillingDemo__ctor_m8_1057 (BillingDemo_t8_178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
