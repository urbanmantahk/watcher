﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.MonoTODOAttribute
struct MonoTODOAttribute_t4_1;
// Mono.Xml.Schema.XsdAnySimpleType
struct XsdAnySimpleType_t4_3;
// Mono.Xml.Schema.XdtAnyAtomicType
struct XdtAnyAtomicType_t4_5;
// Mono.Xml.Schema.XdtUntypedAtomic
struct XdtUntypedAtomic_t4_6;
// Mono.Xml.Schema.XsdString
struct XsdString_t4_7;
// Mono.Xml.Schema.XsdNormalizedString
struct XsdNormalizedString_t4_8;
// Mono.Xml.Schema.XsdToken
struct XsdToken_t4_9;
// Mono.Xml.Schema.XsdLanguage
struct XsdLanguage_t4_10;
// Mono.Xml.Schema.XsdNMToken
struct XsdNMToken_t4_11;
// Mono.Xml.Schema.XsdNMTokens
struct XsdNMTokens_t4_12;
// Mono.Xml.Schema.XsdName
struct XsdName_t4_13;
// Mono.Xml.Schema.XsdNCName
struct XsdNCName_t4_14;
// Mono.Xml.Schema.XsdID
struct XsdID_t4_15;
// Mono.Xml.Schema.XsdIDRef
struct XsdIDRef_t4_16;
// Mono.Xml.Schema.XsdIDRefs
struct XsdIDRefs_t4_17;
// Mono.Xml.Schema.XsdEntity
struct XsdEntity_t4_18;
// Mono.Xml.Schema.XsdEntities
struct XsdEntities_t4_19;
// Mono.Xml.Schema.XsdNotation
struct XsdNotation_t4_20;
// Mono.Xml.Schema.XsdDecimal
struct XsdDecimal_t4_21;
// Mono.Xml.Schema.XsdInteger
struct XsdInteger_t4_22;
// Mono.Xml.Schema.XsdLong
struct XsdLong_t4_23;
// Mono.Xml.Schema.XsdInt
struct XsdInt_t4_24;
// Mono.Xml.Schema.XsdShort
struct XsdShort_t4_25;
// Mono.Xml.Schema.XsdByte
struct XsdByte_t4_26;
// Mono.Xml.Schema.XsdNonNegativeInteger
struct XsdNonNegativeInteger_t4_27;
// Mono.Xml.Schema.XsdUnsignedLong
struct XsdUnsignedLong_t4_28;
// Mono.Xml.Schema.XsdUnsignedInt
struct XsdUnsignedInt_t4_29;
// Mono.Xml.Schema.XsdUnsignedShort
struct XsdUnsignedShort_t4_30;
// Mono.Xml.Schema.XsdUnsignedByte
struct XsdUnsignedByte_t4_31;
// Mono.Xml.Schema.XsdPositiveInteger
struct XsdPositiveInteger_t4_32;
// Mono.Xml.Schema.XsdNonPositiveInteger
struct XsdNonPositiveInteger_t4_33;
// Mono.Xml.Schema.XsdNegativeInteger
struct XsdNegativeInteger_t4_34;
// Mono.Xml.Schema.XsdFloat
struct XsdFloat_t4_35;
// Mono.Xml.Schema.XsdDouble
struct XsdDouble_t4_36;
// Mono.Xml.Schema.XsdBase64Binary
struct XsdBase64Binary_t4_37;
// Mono.Xml.Schema.XsdHexBinary
struct XsdHexBinary_t4_38;
// Mono.Xml.Schema.XsdQName
struct XsdQName_t4_39;
// Mono.Xml.Schema.XsdBoolean
struct XsdBoolean_t4_40;
// Mono.Xml.Schema.XsdAnyURI
struct XsdAnyURI_t4_41;
// Mono.Xml.Schema.XsdDuration
struct XsdDuration_t4_42;
// Mono.Xml.Schema.XdtDayTimeDuration
struct XdtDayTimeDuration_t4_43;
// Mono.Xml.Schema.XdtYearMonthDuration
struct XdtYearMonthDuration_t4_44;
// Mono.Xml.Schema.XsdDateTime
struct XsdDateTime_t4_45;
// Mono.Xml.Schema.XsdDate
struct XsdDate_t4_46;
// Mono.Xml.Schema.XsdTime
struct XsdTime_t4_47;
// Mono.Xml.Schema.XsdGYearMonth
struct XsdGYearMonth_t4_48;
// Mono.Xml.Schema.XsdGMonthDay
struct XsdGMonthDay_t4_49;
// Mono.Xml.Schema.XsdGYear
struct XsdGYear_t4_50;
// Mono.Xml.Schema.XsdGMonth
struct XsdGMonth_t4_51;
// Mono.Xml.Schema.XsdGDay
struct XsdGDay_t4_52;
// System.Xml.Schema.XmlSchemaAnnotated
struct XmlSchemaAnnotated_t4_53;
// System.Xml.Schema.XmlSchemaCompilationSettings
struct XmlSchemaCompilationSettings_t4_56;
// System.Xml.Schema.XmlSchemaDatatype
struct XmlSchemaDatatype_t4_4;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t4_71;
// System.String
struct String_t;
// System.Xml.Schema.XmlSchemaInfo
struct XmlSchemaInfo_t4_62;
// System.Xml.Schema.IXmlSchemaInfo
struct IXmlSchemaInfo_t4_119;
// System.Xml.Schema.XmlSchemaSimpleType
struct XmlSchemaSimpleType_t4_63;
// System.Xml.Schema.XmlSchemaAttribute
struct XmlSchemaAttribute_t4_55;
// System.Xml.Schema.XmlSchemaElement
struct XmlSchemaElement_t4_58;
// System.Xml.Schema.XmlSchemaType
struct XmlSchemaType_t4_64;
// System.Xml.Schema.XmlSchemaObject
struct XmlSchemaObject_t4_54;
// System.Xml.Schema.XmlSchemaSet
struct XmlSchemaSet_t4_66;
// System.Xml.XmlNameTable
struct XmlNameTable_t4_67;
// System.Xml.Schema.XmlSchemaSimpleTypeContent
struct XmlSchemaSimpleTypeContent_t4_69;
// System.Xml.Schema.XmlSchemaSimpleTypeList
struct XmlSchemaSimpleTypeList_t4_70;
// System.Xml.Serialization.XmlAttributeAttribute
struct XmlAttributeAttribute_t4_76;
// System.Xml.Serialization.XmlElementAttribute
struct XmlElementAttribute_t4_77;
// System.Type
struct Type_t;
// System.Xml.Serialization.XmlEnumAttribute
struct XmlEnumAttribute_t4_78;
// System.Xml.Serialization.XmlIgnoreAttribute
struct XmlIgnoreAttribute_t4_79;
// System.Xml.Serialization.XmlSerializerNamespaces
struct XmlSerializerNamespaces_t4_65;
// Mono.Xml.DTDAutomataFactory
struct DTDAutomataFactory_t4_81;
// Mono.Xml.DTDObjectModel
struct DTDObjectModel_t4_82;
// System.Xml.XmlResolver
struct XmlResolver_t4_68;
// System.Collections.Hashtable
struct Hashtable_t1_100;
// Mono.Xml.DTDElementDeclarationCollection
struct DTDElementDeclarationCollection_t4_83;
// Mono.Xml.DTDAttListDeclarationCollection
struct DTDAttListDeclarationCollection_t4_84;
// Mono.Xml.DTDEntityDeclarationCollection
struct DTDEntityDeclarationCollection_t4_86;
// Mono.Xml.DTDParameterEntityDeclarationCollection
struct DTDParameterEntityDeclarationCollection_t4_85;
// Mono.Xml.DTDNotationDeclarationCollection
struct DTDNotationDeclarationCollection_t4_87;
// System.Xml.XmlException
struct XmlException_t4_137;
// Mono.Xml2.XmlTextReader
struct XmlTextReader_t4_169;
// System.Xml.XmlParserContext
struct XmlParserContext_t4_157;
// Mono.Xml.DictionaryBase/<>c__Iterator3
struct U3CU3Ec__Iterator3_t4_88;
// Mono.Xml.DTDNode
struct DTDNode_t4_89;
// System.Object
struct Object_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Collections.Generic.IEnumerator`1<Mono.Xml.DTDNode>
struct IEnumerator_1_t1_1807;
// Mono.Xml.DictionaryBase
struct DictionaryBase_t4_90;
// System.Collections.Generic.IEnumerable`1<Mono.Xml.DTDNode>
struct IEnumerable_1_t1_1808;
// Mono.Xml.DTDCollectionBase
struct DTDCollectionBase_t4_91;
// Mono.Xml.DTDElementDeclaration
struct DTDElementDeclaration_t4_94;
// Mono.Xml.DTDAttListDeclaration
struct DTDAttListDeclaration_t4_96;
// Mono.Xml.DTDEntityDeclaration
struct DTDEntityDeclaration_t4_98;
// Mono.Xml.DTDNotationDeclaration
struct DTDNotationDeclaration_t4_99;
// Mono.Xml.DTDContentModel
struct DTDContentModel_t4_92;
// Mono.Xml.DTDContentModelCollection
struct DTDContentModelCollection_t4_93;
// Mono.Xml.DTDAttributeDefinition
struct DTDAttributeDefinition_t4_95;
// System.Collections.IList
struct IList_t1_262;
// Mono.Xml.DTDEntityBase
struct DTDEntityBase_t4_97;
// System.Collections.ArrayList
struct ArrayList_t1_170;
// Mono.Xml.DTDParameterEntityDeclaration
struct DTDParameterEntityDeclaration_t4_100;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array.h"
#include "System_Xml_U3CModuleU3E.h"
#include "System_Xml_U3CModuleU3EMethodDeclarations.h"
#include "System_Xml_System_MonoTODOAttribute.h"
#include "System_Xml_System_MonoTODOAttributeMethodDeclarations.h"
#include "mscorlib_System_Void.h"
#include "mscorlib_System_AttributeMethodDeclarations.h"
#include "mscorlib_System_Attribute.h"
#include "System_Xml_Mono_Xml_Schema_XsdWhitespaceFacet.h"
#include "System_Xml_Mono_Xml_Schema_XsdWhitespaceFacetMethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdAnySimpleType.h"
#include "System_Xml_Mono_Xml_Schema_XsdAnySimpleTypeMethodDeclarations.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaDatatypeMethodDeclarations.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaDatatype.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Char.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaFacet_Facet.h"
#include "System_Xml_System_Xml_XmlTokenizedType.h"
#include "System_Xml_Mono_Xml_Schema_XdtAnyAtomicType.h"
#include "System_Xml_Mono_Xml_Schema_XdtAnyAtomicTypeMethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XdtUntypedAtomic.h"
#include "System_Xml_Mono_Xml_Schema_XdtUntypedAtomicMethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdString.h"
#include "System_Xml_Mono_Xml_Schema_XsdStringMethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdNormalizedString.h"
#include "System_Xml_Mono_Xml_Schema_XsdNormalizedStringMethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdToken.h"
#include "System_Xml_Mono_Xml_Schema_XsdTokenMethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdLanguage.h"
#include "System_Xml_Mono_Xml_Schema_XsdLanguageMethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdNMToken.h"
#include "System_Xml_Mono_Xml_Schema_XsdNMTokenMethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdNMTokens.h"
#include "System_Xml_Mono_Xml_Schema_XsdNMTokensMethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdName.h"
#include "System_Xml_Mono_Xml_Schema_XsdNameMethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdNCName.h"
#include "System_Xml_Mono_Xml_Schema_XsdNCNameMethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdID.h"
#include "System_Xml_Mono_Xml_Schema_XsdIDMethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdIDRef.h"
#include "System_Xml_Mono_Xml_Schema_XsdIDRefMethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdIDRefs.h"
#include "System_Xml_Mono_Xml_Schema_XsdIDRefsMethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdEntity.h"
#include "System_Xml_Mono_Xml_Schema_XsdEntityMethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdEntities.h"
#include "System_Xml_Mono_Xml_Schema_XsdEntitiesMethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdNotation.h"
#include "System_Xml_Mono_Xml_Schema_XsdNotationMethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdDecimal.h"
#include "System_Xml_Mono_Xml_Schema_XsdDecimalMethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdInteger.h"
#include "System_Xml_Mono_Xml_Schema_XsdIntegerMethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdLong.h"
#include "System_Xml_Mono_Xml_Schema_XsdLongMethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdInt.h"
#include "System_Xml_Mono_Xml_Schema_XsdIntMethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdShort.h"
#include "System_Xml_Mono_Xml_Schema_XsdShortMethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdByte.h"
#include "System_Xml_Mono_Xml_Schema_XsdByteMethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdNonNegativeInteger.h"
#include "System_Xml_Mono_Xml_Schema_XsdNonNegativeIntegerMethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdUnsignedLong.h"
#include "System_Xml_Mono_Xml_Schema_XsdUnsignedLongMethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdUnsignedInt.h"
#include "System_Xml_Mono_Xml_Schema_XsdUnsignedIntMethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdUnsignedShort.h"
#include "System_Xml_Mono_Xml_Schema_XsdUnsignedShortMethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdUnsignedByte.h"
#include "System_Xml_Mono_Xml_Schema_XsdUnsignedByteMethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdPositiveInteger.h"
#include "System_Xml_Mono_Xml_Schema_XsdPositiveIntegerMethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdNonPositiveInteger.h"
#include "System_Xml_Mono_Xml_Schema_XsdNonPositiveIntegerMethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdNegativeInteger.h"
#include "System_Xml_Mono_Xml_Schema_XsdNegativeIntegerMethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdFloat.h"
#include "System_Xml_Mono_Xml_Schema_XsdFloatMethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdDouble.h"
#include "System_Xml_Mono_Xml_Schema_XsdDoubleMethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdBase64Binary.h"
#include "System_Xml_Mono_Xml_Schema_XsdBase64BinaryMethodDeclarations.h"
#include "mscorlib_System_StringMethodDeclarations.h"
#include "mscorlib_System_Int32.h"
#include "mscorlib_System_String.h"
#include "mscorlib_System_Byte.h"
#include "System_Xml_Mono_Xml_Schema_XsdHexBinary.h"
#include "System_Xml_Mono_Xml_Schema_XsdHexBinaryMethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdQName.h"
#include "System_Xml_Mono_Xml_Schema_XsdQNameMethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdBoolean.h"
#include "System_Xml_Mono_Xml_Schema_XsdBooleanMethodDeclarations.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaUtil.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaUtilMethodDeclarations.h"
#include "mscorlib_System_Boolean.h"
#include "System_Xml_Mono_Xml_Schema_XsdAnyURI.h"
#include "System_Xml_Mono_Xml_Schema_XsdAnyURIMethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdDuration.h"
#include "System_Xml_Mono_Xml_Schema_XsdDurationMethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XdtDayTimeDuration.h"
#include "System_Xml_Mono_Xml_Schema_XdtDayTimeDurationMethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XdtYearMonthDuration.h"
#include "System_Xml_Mono_Xml_Schema_XdtYearMonthDurationMethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdDateTime.h"
#include "System_Xml_Mono_Xml_Schema_XsdDateTimeMethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdDate.h"
#include "System_Xml_Mono_Xml_Schema_XsdDateMethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdTime.h"
#include "System_Xml_Mono_Xml_Schema_XsdTimeMethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdGYearMonth.h"
#include "System_Xml_Mono_Xml_Schema_XsdGYearMonthMethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdGMonthDay.h"
#include "System_Xml_Mono_Xml_Schema_XsdGMonthDayMethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdGYear.h"
#include "System_Xml_Mono_Xml_Schema_XsdGYearMethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdGMonth.h"
#include "System_Xml_Mono_Xml_Schema_XsdGMonthMethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdGDay.h"
#include "System_Xml_Mono_Xml_Schema_XsdGDayMethodDeclarations.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAnnotated.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAnnotatedMethodDeclarations.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaObjectMethodDeclarations.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaObject.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAttribute.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAttributeMethodDeclarations.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaCompilationSettings.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaCompilationSettingsMethodDeclarations.h"
#include "mscorlib_System_ObjectMethodDeclarations.h"
#include "mscorlib_System_Object.h"
#include "mscorlib_System_Text_StringBuilderMethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder.h"
#include "mscorlib_System_Runtime_CompilerServices_RuntimeHelpersMethodDeclarations.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3EMethodDeclarations.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU2_0.h"
#include "mscorlib_System_RuntimeFieldHandle.h"
#include "System_Xml_System_Xml_XmlQualifiedName.h"
#include "System_Xml_System_Xml_XmlQualifiedNameMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_genMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaDerivationMethod.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaDerivationMethodMethodDeclarations.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaElement.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaElementMethodDeclarations.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaFacet_FacetMethodDeclarations.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaFacet.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaFacetMethodDeclarations.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaInfo.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaInfoMethodDeclarations.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleType.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaType.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaValidity.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializerNamespacesMethodDeclarations.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializerNamespaces.h"
#include "mscorlib_System_Collections_ArrayList.h"
#include "mscorlib_System_Guid.h"
#include "mscorlib_System_GuidMethodDeclarations.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaParticle.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaParticleMethodDeclarations.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSet.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSetMethodDeclarations.h"
#include "System_Xml_System_Xml_NameTableMethodDeclarations.h"
#include "System_Xml_System_Xml_NameTable.h"
#include "System_Xml_System_Xml_XmlNameTable.h"
#include "System_Xml_System_Xml_XmlUrlResolverMethodDeclarations.h"
#include "mscorlib_System_ArgumentNullExceptionMethodDeclarations.h"
#include "mscorlib_System_Collections_ArrayListMethodDeclarations.h"
#include "System_Xml_System_Xml_XmlUrlResolver.h"
#include "System_Xml_System_Xml_XmlResolver.h"
#include "mscorlib_System_ArgumentNullException.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleTypeMethodDeclarations.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaTypeMethodDeclarations.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleTypeListMethodDeclarations.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleTypeList.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleTypeContent.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleTypeContentMethodDeclarations.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleTypeRestriction.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleTypeRestrictionMethodDeclarations.h"
#include "mscorlib_System_Globalization_NumberStyles.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleTypeUnion.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleTypeUnionMethodDeclarations.h"
#include "mscorlib_System_EnvironmentMethodDeclarations.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaValidityMethodDeclarations.h"
#include "System_Xml_System_Xml_Serialization_XmlAttributeAttribute.h"
#include "System_Xml_System_Xml_Serialization_XmlAttributeAttributeMethodDeclarations.h"
#include "System_Xml_System_Xml_Serialization_XmlElementAttribute.h"
#include "System_Xml_System_Xml_Serialization_XmlElementAttributeMethodDeclarations.h"
#include "mscorlib_System_Type.h"
#include "System_Xml_System_Xml_Serialization_XmlEnumAttribute.h"
#include "System_Xml_System_Xml_Serialization_XmlEnumAttributeMethodDeclarations.h"
#include "System_Xml_System_Xml_Serialization_XmlIgnoreAttribute.h"
#include "System_Xml_System_Xml_Serialization_XmlIgnoreAttributeMethodDeclarations.h"
#include "System_System_Collections_Specialized_ListDictionaryMethodDeclarations.h"
#include "System_System_Collections_Specialized_ListDictionary.h"
#include "System_Xml_System_Xml_ConformanceLevel.h"
#include "System_Xml_System_Xml_ConformanceLevelMethodDeclarations.h"
#include "System_Xml_Mono_Xml_DTDAutomataFactory.h"
#include "System_Xml_Mono_Xml_DTDAutomataFactoryMethodDeclarations.h"
#include "System_Xml_Mono_Xml_DTDObjectModel.h"
#include "mscorlib_System_Collections_HashtableMethodDeclarations.h"
#include "mscorlib_System_Collections_Hashtable.h"
#include "System_Xml_Mono_Xml_DTDObjectModelMethodDeclarations.h"
#include "System_Xml_Mono_Xml_DTDElementDeclarationCollectionMethodDeclarations.h"
#include "System_Xml_Mono_Xml_DTDAttListDeclarationCollectionMethodDeclarations.h"
#include "System_Xml_Mono_Xml_DTDEntityDeclarationCollectionMethodDeclarations.h"
#include "System_Xml_Mono_Xml_DTDParameterEntityDeclarationCollectionMethodDeclarations.h"
#include "System_Xml_Mono_Xml_DTDNotationDeclarationCollectionMethodDeclarations.h"
#include "System_Xml_Mono_Xml_DTDElementDeclarationCollection.h"
#include "System_Xml_Mono_Xml_DTDAttListDeclarationCollection.h"
#include "System_Xml_Mono_Xml_DTDEntityDeclarationCollection.h"
#include "System_Xml_Mono_Xml_DTDParameterEntityDeclarationCollection.h"
#include "System_Xml_Mono_Xml_DTDNotationDeclarationCollection.h"
#include "System_Xml_System_Xml_XmlExceptionMethodDeclarations.h"
#include "System_Xml_Mono_Xml_DTDEntityDeclarationMethodDeclarations.h"
#include "System_Xml_Mono_Xml_DTDEntityDeclaration.h"
#include "System_Xml_System_Xml_XmlException.h"
#include "mscorlib_System_Exception.h"
#include "System_Xml_Mono_Xml2_XmlTextReader.h"
#include "System_Xml_System_Xml_XmlParserContext.h"
#include "System_Xml_Mono_Xml_DTDEntityBaseMethodDeclarations.h"
#include "System_System_UriMethodDeclarations.h"
#include "mscorlib_System_TypeMethodDeclarations.h"
#include "System_Xml_Mono_Xml2_XmlTextReaderMethodDeclarations.h"
#include "System_System_Uri.h"
#include "mscorlib_System_IO_Stream.h"
#include "System_Xml_Mono_Xml_DTDEntityBase.h"
#include "System_Xml_Mono_Xml_DTDNode.h"
#include "System_Xml_Mono_Xml_DTDNodeMethodDeclarations.h"
#include "System_Xml_System_Xml_XmlResolverMethodDeclarations.h"
#include "mscorlib_System_RuntimeTypeHandle.h"
#include "System_Xml_System_Xml_XmlNodeType.h"
#include "System_Xml_Mono_Xml_DictionaryBase_U3CU3Ec__Iterator3.h"
#include "System_Xml_Mono_Xml_DictionaryBase_U3CU3Ec__Iterator3MethodDeclarations.h"
#include "mscorlib_System_Threading_InterlockedMethodDeclarations.h"
#include "System_Xml_Mono_Xml_DictionaryBase.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_4MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_1MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_genMethodDeclarations.h"
#include "mscorlib_System_UInt32.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_4.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_1.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen.h"
#include "mscorlib_System_NotSupportedExceptionMethodDeclarations.h"
#include "mscorlib_System_NotSupportedException.h"
#include "System_Xml_Mono_Xml_DictionaryBaseMethodDeclarations.h"
#include "System_Xml_Mono_Xml_DTDCollectionBase.h"
#include "System_Xml_Mono_Xml_DTDCollectionBaseMethodDeclarations.h"
#include "System_Xml_Mono_Xml_DTDElementDeclaration.h"
#include "System_Xml_Mono_Xml_DTDAttListDeclaration.h"
#include "System_Xml_Mono_Xml_DTDAttListDeclarationMethodDeclarations.h"
#include "System_Xml_Mono_Xml_DTDAttributeDefinitionMethodDeclarations.h"
#include "System_Xml_Mono_Xml_DTDAttributeDefinition.h"
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException.h"
#include "System_Xml_Mono_Xml_DTDNotationDeclaration.h"
#include "System_Xml_Mono_Xml_DTDContentModel.h"
#include "System_Xml_Mono_Xml_DTDContentModelMethodDeclarations.h"
#include "System_Xml_Mono_Xml_DTDContentModelCollectionMethodDeclarations.h"
#include "System_Xml_Mono_Xml_DTDContentModelCollection.h"
#include "System_Xml_Mono_Xml_DTDOccurence.h"
#include "System_Xml_Mono_Xml_DTDContentOrderType.h"
#include "System_Xml_Mono_Xml_DTDElementDeclarationMethodDeclarations.h"
#include "mscorlib_System_Globalization_CultureInfoMethodDeclarations.h"
#include "mscorlib_System_Int32MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlCharMethodDeclarations.h"
#include "mscorlib_System_Globalization_CultureInfo.h"
#include "System_System_UriFormatException.h"
#include "mscorlib_System_IO_TextReader.h"
#include "mscorlib_System_IO_TextReaderMethodDeclarations.h"
#include "mscorlib_System_IO_StreamMethodDeclarations.h"
#include "System_Xml_Mono_Xml_DTDNotationDeclarationMethodDeclarations.h"
#include "System_Xml_Mono_Xml_DTDParameterEntityDeclaration.h"
#include "System_Xml_Mono_Xml_DTDParameterEntityDeclarationMethodDeclarations.h"
#include "System_Xml_Mono_Xml_DTDContentOrderTypeMethodDeclarations.h"
#include "System_Xml_Mono_Xml_DTDOccurenceMethodDeclarations.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.MonoTODOAttribute::.ctor()
extern "C" void MonoTODOAttribute__ctor_m4_0 (MonoTODOAttribute_t4_1 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml.Schema.XsdAnySimpleType::.ctor()
extern TypeInfo* XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var;
extern "C" void XsdAnySimpleType__ctor_m4_1 (XsdAnySimpleType_t4_3 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1215);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		XmlSchemaDatatype__ctor_m4_80(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml.Schema.XsdAnySimpleType::.cctor()
extern TypeInfo* CharU5BU5D_t1_16_il2cpp_TypeInfo_var;
extern TypeInfo* XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var;
extern "C" void XsdAnySimpleType__cctor_m4_2 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CharU5BU5D_t1_16_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(15);
		XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1216);
		s_Il2CppMethodIntialized = true;
	}
	{
		CharU5BU5D_t1_16* L_0 = ((CharU5BU5D_t1_16*)SZArrayNew(CharU5BU5D_t1_16_il2cpp_TypeInfo_var, 1));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_0, 0, sizeof(uint16_t))) = (uint16_t)((int32_t)32);
		((XsdAnySimpleType_t4_3_StaticFields*)XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var->static_fields)->___whitespaceArray_56 = L_0;
		((XsdAnySimpleType_t4_3_StaticFields*)XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var->static_fields)->___booleanAllowedFacets_57 = ((int32_t)40);
		((XsdAnySimpleType_t4_3_StaticFields*)XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var->static_fields)->___decimalAllowedFacets_58 = ((int32_t)4088);
		((XsdAnySimpleType_t4_3_StaticFields*)XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var->static_fields)->___durationAllowedFacets_59 = ((int32_t)1016);
		((XsdAnySimpleType_t4_3_StaticFields*)XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var->static_fields)->___stringAllowedFacets_60 = ((int32_t)63);
		XsdAnySimpleType_t4_3 * L_1 = (XsdAnySimpleType_t4_3 *)il2cpp_codegen_object_new (XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var);
		XsdAnySimpleType__ctor_m4_1(L_1, /*hidden argument*/NULL);
		((XsdAnySimpleType_t4_3_StaticFields*)XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var->static_fields)->___instance_55 = L_1;
		return;
	}
}
// Mono.Xml.Schema.XsdAnySimpleType Mono.Xml.Schema.XsdAnySimpleType::get_Instance()
extern TypeInfo* XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var;
extern "C" XsdAnySimpleType_t4_3 * XsdAnySimpleType_get_Instance_m4_3 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1216);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var);
		XsdAnySimpleType_t4_3 * L_0 = ((XsdAnySimpleType_t4_3_StaticFields*)XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var->static_fields)->___instance_55;
		return L_0;
	}
}
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdAnySimpleType::get_TokenizedType()
extern "C" int32_t XsdAnySimpleType_get_TokenizedType_m4_4 (XsdAnySimpleType_t4_3 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(((int32_t)12));
	}
}
// System.Void Mono.Xml.Schema.XdtAnyAtomicType::.ctor()
extern TypeInfo* XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var;
extern "C" void XdtAnyAtomicType__ctor_m4_5 (XdtAnyAtomicType_t4_5 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1216);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var);
		XsdAnySimpleType__ctor_m4_1(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml.Schema.XdtUntypedAtomic::.ctor()
extern "C" void XdtUntypedAtomic__ctor_m4_6 (XdtUntypedAtomic_t4_6 * __this, const MethodInfo* method)
{
	{
		XdtAnyAtomicType__ctor_m4_5(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml.Schema.XsdString::.ctor()
extern TypeInfo* XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var;
extern "C" void XsdString__ctor_m4_7 (XsdString_t4_7 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1216);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var);
		XsdAnySimpleType__ctor_m4_1(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdString::get_TokenizedType()
extern "C" int32_t XsdString_get_TokenizedType_m4_8 (XsdString_t4_7 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(0);
	}
}
// System.Void Mono.Xml.Schema.XsdNormalizedString::.ctor()
extern "C" void XsdNormalizedString__ctor_m4_9 (XsdNormalizedString_t4_8 * __this, const MethodInfo* method)
{
	{
		XsdString__ctor_m4_7(__this, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4 *)__this)->___WhitespaceValue_0 = 1;
		return;
	}
}
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdNormalizedString::get_TokenizedType()
extern "C" int32_t XsdNormalizedString_get_TokenizedType_m4_10 (XsdNormalizedString_t4_8 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(0);
	}
}
// System.Void Mono.Xml.Schema.XsdToken::.ctor()
extern "C" void XsdToken__ctor_m4_11 (XsdToken_t4_9 * __this, const MethodInfo* method)
{
	{
		XsdNormalizedString__ctor_m4_9(__this, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4 *)__this)->___WhitespaceValue_0 = 2;
		return;
	}
}
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdToken::get_TokenizedType()
extern "C" int32_t XsdToken_get_TokenizedType_m4_12 (XsdToken_t4_9 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(0);
	}
}
// System.Void Mono.Xml.Schema.XsdLanguage::.ctor()
extern "C" void XsdLanguage__ctor_m4_13 (XsdLanguage_t4_10 * __this, const MethodInfo* method)
{
	{
		XsdToken__ctor_m4_11(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdLanguage::get_TokenizedType()
extern "C" int32_t XsdLanguage_get_TokenizedType_m4_14 (XsdLanguage_t4_10 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(0);
	}
}
// System.Void Mono.Xml.Schema.XsdNMToken::.ctor()
extern "C" void XsdNMToken__ctor_m4_15 (XsdNMToken_t4_11 * __this, const MethodInfo* method)
{
	{
		XsdToken__ctor_m4_11(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdNMToken::get_TokenizedType()
extern "C" int32_t XsdNMToken_get_TokenizedType_m4_16 (XsdNMToken_t4_11 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(6);
	}
}
// System.Void Mono.Xml.Schema.XsdNMTokens::.ctor()
extern "C" void XsdNMTokens__ctor_m4_17 (XsdNMTokens_t4_12 * __this, const MethodInfo* method)
{
	{
		XsdNMToken__ctor_m4_15(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdNMTokens::get_TokenizedType()
extern "C" int32_t XsdNMTokens_get_TokenizedType_m4_18 (XsdNMTokens_t4_12 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(7);
	}
}
// System.Void Mono.Xml.Schema.XsdName::.ctor()
extern "C" void XsdName__ctor_m4_19 (XsdName_t4_13 * __this, const MethodInfo* method)
{
	{
		XsdToken__ctor_m4_11(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdName::get_TokenizedType()
extern "C" int32_t XsdName_get_TokenizedType_m4_20 (XsdName_t4_13 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(0);
	}
}
// System.Void Mono.Xml.Schema.XsdNCName::.ctor()
extern "C" void XsdNCName__ctor_m4_21 (XsdNCName_t4_14 * __this, const MethodInfo* method)
{
	{
		XsdName__ctor_m4_19(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdNCName::get_TokenizedType()
extern "C" int32_t XsdNCName_get_TokenizedType_m4_22 (XsdNCName_t4_14 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(((int32_t)11));
	}
}
// System.Void Mono.Xml.Schema.XsdID::.ctor()
extern "C" void XsdID__ctor_m4_23 (XsdID_t4_15 * __this, const MethodInfo* method)
{
	{
		XsdName__ctor_m4_19(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdID::get_TokenizedType()
extern "C" int32_t XsdID_get_TokenizedType_m4_24 (XsdID_t4_15 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(1);
	}
}
// System.Void Mono.Xml.Schema.XsdIDRef::.ctor()
extern "C" void XsdIDRef__ctor_m4_25 (XsdIDRef_t4_16 * __this, const MethodInfo* method)
{
	{
		XsdName__ctor_m4_19(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdIDRef::get_TokenizedType()
extern "C" int32_t XsdIDRef_get_TokenizedType_m4_26 (XsdIDRef_t4_16 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(2);
	}
}
// System.Void Mono.Xml.Schema.XsdIDRefs::.ctor()
extern "C" void XsdIDRefs__ctor_m4_27 (XsdIDRefs_t4_17 * __this, const MethodInfo* method)
{
	{
		XsdName__ctor_m4_19(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdIDRefs::get_TokenizedType()
extern "C" int32_t XsdIDRefs_get_TokenizedType_m4_28 (XsdIDRefs_t4_17 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(3);
	}
}
// System.Void Mono.Xml.Schema.XsdEntity::.ctor()
extern "C" void XsdEntity__ctor_m4_29 (XsdEntity_t4_18 * __this, const MethodInfo* method)
{
	{
		XsdName__ctor_m4_19(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdEntity::get_TokenizedType()
extern "C" int32_t XsdEntity_get_TokenizedType_m4_30 (XsdEntity_t4_18 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(4);
	}
}
// System.Void Mono.Xml.Schema.XsdEntities::.ctor()
extern "C" void XsdEntities__ctor_m4_31 (XsdEntities_t4_19 * __this, const MethodInfo* method)
{
	{
		XsdName__ctor_m4_19(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdEntities::get_TokenizedType()
extern "C" int32_t XsdEntities_get_TokenizedType_m4_32 (XsdEntities_t4_19 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(5);
	}
}
// System.Void Mono.Xml.Schema.XsdNotation::.ctor()
extern TypeInfo* XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var;
extern "C" void XsdNotation__ctor_m4_33 (XsdNotation_t4_20 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1216);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var);
		XsdAnySimpleType__ctor_m4_1(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdNotation::get_TokenizedType()
extern "C" int32_t XsdNotation_get_TokenizedType_m4_34 (XsdNotation_t4_20 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(8);
	}
}
// System.Void Mono.Xml.Schema.XsdDecimal::.ctor()
extern TypeInfo* XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var;
extern "C" void XsdDecimal__ctor_m4_35 (XsdDecimal_t4_21 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1216);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var);
		XsdAnySimpleType__ctor_m4_1(__this, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4 *)__this)->___WhitespaceValue_0 = 2;
		return;
	}
}
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdDecimal::get_TokenizedType()
extern "C" int32_t XsdDecimal_get_TokenizedType_m4_36 (XsdDecimal_t4_21 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(((int32_t)12));
	}
}
// System.Void Mono.Xml.Schema.XsdInteger::.ctor()
extern "C" void XsdInteger__ctor_m4_37 (XsdInteger_t4_22 * __this, const MethodInfo* method)
{
	{
		XsdDecimal__ctor_m4_35(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml.Schema.XsdLong::.ctor()
extern "C" void XsdLong__ctor_m4_38 (XsdLong_t4_23 * __this, const MethodInfo* method)
{
	{
		XsdInteger__ctor_m4_37(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml.Schema.XsdInt::.ctor()
extern "C" void XsdInt__ctor_m4_39 (XsdInt_t4_24 * __this, const MethodInfo* method)
{
	{
		XsdLong__ctor_m4_38(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml.Schema.XsdShort::.ctor()
extern "C" void XsdShort__ctor_m4_40 (XsdShort_t4_25 * __this, const MethodInfo* method)
{
	{
		XsdInt__ctor_m4_39(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml.Schema.XsdByte::.ctor()
extern "C" void XsdByte__ctor_m4_41 (XsdByte_t4_26 * __this, const MethodInfo* method)
{
	{
		XsdShort__ctor_m4_40(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml.Schema.XsdNonNegativeInteger::.ctor()
extern "C" void XsdNonNegativeInteger__ctor_m4_42 (XsdNonNegativeInteger_t4_27 * __this, const MethodInfo* method)
{
	{
		XsdInteger__ctor_m4_37(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml.Schema.XsdUnsignedLong::.ctor()
extern "C" void XsdUnsignedLong__ctor_m4_43 (XsdUnsignedLong_t4_28 * __this, const MethodInfo* method)
{
	{
		XsdNonNegativeInteger__ctor_m4_42(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml.Schema.XsdUnsignedInt::.ctor()
extern "C" void XsdUnsignedInt__ctor_m4_44 (XsdUnsignedInt_t4_29 * __this, const MethodInfo* method)
{
	{
		XsdUnsignedLong__ctor_m4_43(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml.Schema.XsdUnsignedShort::.ctor()
extern "C" void XsdUnsignedShort__ctor_m4_45 (XsdUnsignedShort_t4_30 * __this, const MethodInfo* method)
{
	{
		XsdUnsignedInt__ctor_m4_44(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml.Schema.XsdUnsignedByte::.ctor()
extern "C" void XsdUnsignedByte__ctor_m4_46 (XsdUnsignedByte_t4_31 * __this, const MethodInfo* method)
{
	{
		XsdUnsignedShort__ctor_m4_45(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml.Schema.XsdPositiveInteger::.ctor()
extern "C" void XsdPositiveInteger__ctor_m4_47 (XsdPositiveInteger_t4_32 * __this, const MethodInfo* method)
{
	{
		XsdNonNegativeInteger__ctor_m4_42(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml.Schema.XsdNonPositiveInteger::.ctor()
extern "C" void XsdNonPositiveInteger__ctor_m4_48 (XsdNonPositiveInteger_t4_33 * __this, const MethodInfo* method)
{
	{
		XsdInteger__ctor_m4_37(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml.Schema.XsdNegativeInteger::.ctor()
extern "C" void XsdNegativeInteger__ctor_m4_49 (XsdNegativeInteger_t4_34 * __this, const MethodInfo* method)
{
	{
		XsdNonPositiveInteger__ctor_m4_48(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml.Schema.XsdFloat::.ctor()
extern TypeInfo* XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var;
extern "C" void XsdFloat__ctor_m4_50 (XsdFloat_t4_35 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1216);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var);
		XsdAnySimpleType__ctor_m4_1(__this, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4 *)__this)->___WhitespaceValue_0 = 2;
		return;
	}
}
// System.Void Mono.Xml.Schema.XsdDouble::.ctor()
extern TypeInfo* XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var;
extern "C" void XsdDouble__ctor_m4_51 (XsdDouble_t4_36 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1216);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var);
		XsdAnySimpleType__ctor_m4_1(__this, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4 *)__this)->___WhitespaceValue_0 = 2;
		return;
	}
}
// System.Void Mono.Xml.Schema.XsdBase64Binary::.ctor()
extern "C" void XsdBase64Binary__ctor_m4_52 (XsdBase64Binary_t4_37 * __this, const MethodInfo* method)
{
	{
		XsdString__ctor_m4_7(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml.Schema.XsdBase64Binary::.cctor()
extern TypeInfo* XsdBase64Binary_t4_37_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3246;
extern "C" void XsdBase64Binary__cctor_m4_53 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		XsdBase64Binary_t4_37_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1217);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		_stringLiteral3246 = il2cpp_codegen_string_literal_from_index(3246);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	uint16_t V_3 = 0x0;
	{
		((XsdBase64Binary_t4_37_StaticFields*)XsdBase64Binary_t4_37_il2cpp_TypeInfo_var->static_fields)->___ALPHABET_61 = _stringLiteral3246;
		String_t* L_0 = ((XsdBase64Binary_t4_37_StaticFields*)XsdBase64Binary_t4_37_il2cpp_TypeInfo_var->static_fields)->___ALPHABET_61;
		NullCheck(L_0);
		int32_t L_1 = String_get_Length_m1_571(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		((XsdBase64Binary_t4_37_StaticFields*)XsdBase64Binary_t4_37_il2cpp_TypeInfo_var->static_fields)->___decodeTable_62 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, ((int32_t)123)));
		V_1 = 0;
		goto IL_0038;
	}

IL_0028:
	{
		ByteU5BU5D_t1_109* L_2 = ((XsdBase64Binary_t4_37_StaticFields*)XsdBase64Binary_t4_37_il2cpp_TypeInfo_var->static_fields)->___decodeTable_62;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_2, L_3, sizeof(uint8_t))) = (uint8_t)((int32_t)255);
		int32_t L_4 = V_1;
		V_1 = ((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_0038:
	{
		int32_t L_5 = V_1;
		ByteU5BU5D_t1_109* L_6 = ((XsdBase64Binary_t4_37_StaticFields*)XsdBase64Binary_t4_37_il2cpp_TypeInfo_var->static_fields)->___decodeTable_62;
		NullCheck(L_6);
		if ((((int32_t)L_5) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_6)->max_length)))))))
		{
			goto IL_0028;
		}
	}
	{
		V_2 = 0;
		goto IL_0065;
	}

IL_004c:
	{
		String_t* L_7 = ((XsdBase64Binary_t4_37_StaticFields*)XsdBase64Binary_t4_37_il2cpp_TypeInfo_var->static_fields)->___ALPHABET_61;
		int32_t L_8 = V_2;
		NullCheck(L_7);
		uint16_t L_9 = String_get_Chars_m1_442(L_7, L_8, /*hidden argument*/NULL);
		V_3 = L_9;
		ByteU5BU5D_t1_109* L_10 = ((XsdBase64Binary_t4_37_StaticFields*)XsdBase64Binary_t4_37_il2cpp_TypeInfo_var->static_fields)->___decodeTable_62;
		uint16_t L_11 = V_3;
		int32_t L_12 = V_2;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_10, L_11, sizeof(uint8_t))) = (uint8_t)(((int32_t)((uint8_t)L_12)));
		int32_t L_13 = V_2;
		V_2 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0065:
	{
		int32_t L_14 = V_2;
		int32_t L_15 = V_0;
		if ((((int32_t)L_14) < ((int32_t)L_15)))
		{
			goto IL_004c;
		}
	}
	{
		return;
	}
}
// System.Void Mono.Xml.Schema.XsdHexBinary::.ctor()
extern TypeInfo* XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var;
extern "C" void XsdHexBinary__ctor_m4_54 (XsdHexBinary_t4_38 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1216);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var);
		XsdAnySimpleType__ctor_m4_1(__this, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4 *)__this)->___WhitespaceValue_0 = 2;
		return;
	}
}
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdHexBinary::get_TokenizedType()
extern "C" int32_t XsdHexBinary_get_TokenizedType_m4_55 (XsdHexBinary_t4_38 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(((int32_t)12));
	}
}
// System.Void Mono.Xml.Schema.XsdQName::.ctor()
extern "C" void XsdQName__ctor_m4_56 (XsdQName_t4_39 * __this, const MethodInfo* method)
{
	{
		XsdName__ctor_m4_19(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdQName::get_TokenizedType()
extern "C" int32_t XsdQName_get_TokenizedType_m4_57 (XsdQName_t4_39 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(((int32_t)10));
	}
}
// System.Void Mono.Xml.Schema.XsdBoolean::.ctor()
extern TypeInfo* XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var;
extern "C" void XsdBoolean__ctor_m4_58 (XsdBoolean_t4_40 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1216);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var);
		XsdAnySimpleType__ctor_m4_1(__this, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4 *)__this)->___WhitespaceValue_0 = 2;
		return;
	}
}
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdBoolean::get_TokenizedType()
extern TypeInfo* XmlSchemaUtil_t4_74_il2cpp_TypeInfo_var;
extern "C" int32_t XsdBoolean_get_TokenizedType_m4_59 (XsdBoolean_t4_40 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		XmlSchemaUtil_t4_74_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1218);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaUtil_t4_74_il2cpp_TypeInfo_var);
		bool L_0 = ((XmlSchemaUtil_t4_74_StaticFields*)XmlSchemaUtil_t4_74_il2cpp_TypeInfo_var->static_fields)->___StrictMsCompliant_3;
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (int32_t)(((int32_t)12));
	}

IL_000d:
	{
		return (int32_t)(0);
	}
}
// System.Void Mono.Xml.Schema.XsdAnyURI::.ctor()
extern "C" void XsdAnyURI__ctor_m4_60 (XsdAnyURI_t4_41 * __this, const MethodInfo* method)
{
	{
		XsdString__ctor_m4_7(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdAnyURI::get_TokenizedType()
extern "C" int32_t XsdAnyURI_get_TokenizedType_m4_61 (XsdAnyURI_t4_41 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(0);
	}
}
// System.Void Mono.Xml.Schema.XsdDuration::.ctor()
extern TypeInfo* XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var;
extern "C" void XsdDuration__ctor_m4_62 (XsdDuration_t4_42 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1216);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var);
		XsdAnySimpleType__ctor_m4_1(__this, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4 *)__this)->___WhitespaceValue_0 = 2;
		return;
	}
}
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdDuration::get_TokenizedType()
extern "C" int32_t XsdDuration_get_TokenizedType_m4_63 (XsdDuration_t4_42 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(0);
	}
}
// System.Void Mono.Xml.Schema.XdtDayTimeDuration::.ctor()
extern "C" void XdtDayTimeDuration__ctor_m4_64 (XdtDayTimeDuration_t4_43 * __this, const MethodInfo* method)
{
	{
		XsdDuration__ctor_m4_62(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml.Schema.XdtYearMonthDuration::.ctor()
extern "C" void XdtYearMonthDuration__ctor_m4_65 (XdtYearMonthDuration_t4_44 * __this, const MethodInfo* method)
{
	{
		XsdDuration__ctor_m4_62(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml.Schema.XsdDateTime::.ctor()
extern TypeInfo* XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var;
extern "C" void XsdDateTime__ctor_m4_66 (XsdDateTime_t4_45 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1216);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var);
		XsdAnySimpleType__ctor_m4_1(__this, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4 *)__this)->___WhitespaceValue_0 = 2;
		return;
	}
}
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdDateTime::get_TokenizedType()
extern "C" int32_t XsdDateTime_get_TokenizedType_m4_67 (XsdDateTime_t4_45 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(0);
	}
}
// System.Void Mono.Xml.Schema.XsdDate::.ctor()
extern TypeInfo* XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var;
extern "C" void XsdDate__ctor_m4_68 (XsdDate_t4_46 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1216);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var);
		XsdAnySimpleType__ctor_m4_1(__this, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4 *)__this)->___WhitespaceValue_0 = 2;
		return;
	}
}
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdDate::get_TokenizedType()
extern "C" int32_t XsdDate_get_TokenizedType_m4_69 (XsdDate_t4_46 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(0);
	}
}
// System.Void Mono.Xml.Schema.XsdTime::.ctor()
extern TypeInfo* XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var;
extern "C" void XsdTime__ctor_m4_70 (XsdTime_t4_47 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1216);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var);
		XsdAnySimpleType__ctor_m4_1(__this, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4 *)__this)->___WhitespaceValue_0 = 2;
		return;
	}
}
// System.Void Mono.Xml.Schema.XsdTime::.cctor()
extern TypeInfo* StringU5BU5D_t1_238_il2cpp_TypeInfo_var;
extern TypeInfo* XsdTime_t4_47_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1003;
extern Il2CppCodeGenString* _stringLiteral2279;
extern Il2CppCodeGenString* _stringLiteral2280;
extern Il2CppCodeGenString* _stringLiteral2281;
extern Il2CppCodeGenString* _stringLiteral2282;
extern Il2CppCodeGenString* _stringLiteral2283;
extern Il2CppCodeGenString* _stringLiteral2284;
extern Il2CppCodeGenString* _stringLiteral2285;
extern Il2CppCodeGenString* _stringLiteral2286;
extern Il2CppCodeGenString* _stringLiteral2287;
extern Il2CppCodeGenString* _stringLiteral2288;
extern Il2CppCodeGenString* _stringLiteral2289;
extern Il2CppCodeGenString* _stringLiteral2290;
extern Il2CppCodeGenString* _stringLiteral2291;
extern Il2CppCodeGenString* _stringLiteral2292;
extern Il2CppCodeGenString* _stringLiteral2293;
extern Il2CppCodeGenString* _stringLiteral2294;
extern Il2CppCodeGenString* _stringLiteral2295;
extern Il2CppCodeGenString* _stringLiteral2296;
extern Il2CppCodeGenString* _stringLiteral2297;
extern Il2CppCodeGenString* _stringLiteral2298;
extern Il2CppCodeGenString* _stringLiteral2299;
extern Il2CppCodeGenString* _stringLiteral2300;
extern Il2CppCodeGenString* _stringLiteral2301;
extern "C" void XsdTime__cctor_m4_71 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringU5BU5D_t1_238_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(35);
		XsdTime_t4_47_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1219);
		_stringLiteral1003 = il2cpp_codegen_string_literal_from_index(1003);
		_stringLiteral2279 = il2cpp_codegen_string_literal_from_index(2279);
		_stringLiteral2280 = il2cpp_codegen_string_literal_from_index(2280);
		_stringLiteral2281 = il2cpp_codegen_string_literal_from_index(2281);
		_stringLiteral2282 = il2cpp_codegen_string_literal_from_index(2282);
		_stringLiteral2283 = il2cpp_codegen_string_literal_from_index(2283);
		_stringLiteral2284 = il2cpp_codegen_string_literal_from_index(2284);
		_stringLiteral2285 = il2cpp_codegen_string_literal_from_index(2285);
		_stringLiteral2286 = il2cpp_codegen_string_literal_from_index(2286);
		_stringLiteral2287 = il2cpp_codegen_string_literal_from_index(2287);
		_stringLiteral2288 = il2cpp_codegen_string_literal_from_index(2288);
		_stringLiteral2289 = il2cpp_codegen_string_literal_from_index(2289);
		_stringLiteral2290 = il2cpp_codegen_string_literal_from_index(2290);
		_stringLiteral2291 = il2cpp_codegen_string_literal_from_index(2291);
		_stringLiteral2292 = il2cpp_codegen_string_literal_from_index(2292);
		_stringLiteral2293 = il2cpp_codegen_string_literal_from_index(2293);
		_stringLiteral2294 = il2cpp_codegen_string_literal_from_index(2294);
		_stringLiteral2295 = il2cpp_codegen_string_literal_from_index(2295);
		_stringLiteral2296 = il2cpp_codegen_string_literal_from_index(2296);
		_stringLiteral2297 = il2cpp_codegen_string_literal_from_index(2297);
		_stringLiteral2298 = il2cpp_codegen_string_literal_from_index(2298);
		_stringLiteral2299 = il2cpp_codegen_string_literal_from_index(2299);
		_stringLiteral2300 = il2cpp_codegen_string_literal_from_index(2300);
		_stringLiteral2301 = il2cpp_codegen_string_literal_from_index(2301);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringU5BU5D_t1_238* L_0 = ((StringU5BU5D_t1_238*)SZArrayNew(StringU5BU5D_t1_238_il2cpp_TypeInfo_var, ((int32_t)24)));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral1003);
		*((String_t**)(String_t**)SZArrayLdElema(L_0, 0, sizeof(String_t*))) = (String_t*)_stringLiteral1003;
		StringU5BU5D_t1_238* L_1 = L_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, _stringLiteral2279);
		*((String_t**)(String_t**)SZArrayLdElema(L_1, 1, sizeof(String_t*))) = (String_t*)_stringLiteral2279;
		StringU5BU5D_t1_238* L_2 = L_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 2);
		ArrayElementTypeCheck (L_2, _stringLiteral2280);
		*((String_t**)(String_t**)SZArrayLdElema(L_2, 2, sizeof(String_t*))) = (String_t*)_stringLiteral2280;
		StringU5BU5D_t1_238* L_3 = L_2;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 3);
		ArrayElementTypeCheck (L_3, _stringLiteral2281);
		*((String_t**)(String_t**)SZArrayLdElema(L_3, 3, sizeof(String_t*))) = (String_t*)_stringLiteral2281;
		StringU5BU5D_t1_238* L_4 = L_3;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 4);
		ArrayElementTypeCheck (L_4, _stringLiteral2282);
		*((String_t**)(String_t**)SZArrayLdElema(L_4, 4, sizeof(String_t*))) = (String_t*)_stringLiteral2282;
		StringU5BU5D_t1_238* L_5 = L_4;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 5);
		ArrayElementTypeCheck (L_5, _stringLiteral2283);
		*((String_t**)(String_t**)SZArrayLdElema(L_5, 5, sizeof(String_t*))) = (String_t*)_stringLiteral2283;
		StringU5BU5D_t1_238* L_6 = L_5;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 6);
		ArrayElementTypeCheck (L_6, _stringLiteral2284);
		*((String_t**)(String_t**)SZArrayLdElema(L_6, 6, sizeof(String_t*))) = (String_t*)_stringLiteral2284;
		StringU5BU5D_t1_238* L_7 = L_6;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 7);
		ArrayElementTypeCheck (L_7, _stringLiteral2285);
		*((String_t**)(String_t**)SZArrayLdElema(L_7, 7, sizeof(String_t*))) = (String_t*)_stringLiteral2285;
		StringU5BU5D_t1_238* L_8 = L_7;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 8);
		ArrayElementTypeCheck (L_8, _stringLiteral2286);
		*((String_t**)(String_t**)SZArrayLdElema(L_8, 8, sizeof(String_t*))) = (String_t*)_stringLiteral2286;
		StringU5BU5D_t1_238* L_9 = L_8;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, ((int32_t)9));
		ArrayElementTypeCheck (L_9, _stringLiteral2287);
		*((String_t**)(String_t**)SZArrayLdElema(L_9, ((int32_t)9), sizeof(String_t*))) = (String_t*)_stringLiteral2287;
		StringU5BU5D_t1_238* L_10 = L_9;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, ((int32_t)10));
		ArrayElementTypeCheck (L_10, _stringLiteral2288);
		*((String_t**)(String_t**)SZArrayLdElema(L_10, ((int32_t)10), sizeof(String_t*))) = (String_t*)_stringLiteral2288;
		StringU5BU5D_t1_238* L_11 = L_10;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, ((int32_t)11));
		ArrayElementTypeCheck (L_11, _stringLiteral2289);
		*((String_t**)(String_t**)SZArrayLdElema(L_11, ((int32_t)11), sizeof(String_t*))) = (String_t*)_stringLiteral2289;
		StringU5BU5D_t1_238* L_12 = L_11;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, ((int32_t)12));
		ArrayElementTypeCheck (L_12, _stringLiteral2290);
		*((String_t**)(String_t**)SZArrayLdElema(L_12, ((int32_t)12), sizeof(String_t*))) = (String_t*)_stringLiteral2290;
		StringU5BU5D_t1_238* L_13 = L_12;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, ((int32_t)13));
		ArrayElementTypeCheck (L_13, _stringLiteral2291);
		*((String_t**)(String_t**)SZArrayLdElema(L_13, ((int32_t)13), sizeof(String_t*))) = (String_t*)_stringLiteral2291;
		StringU5BU5D_t1_238* L_14 = L_13;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, ((int32_t)14));
		ArrayElementTypeCheck (L_14, _stringLiteral2292);
		*((String_t**)(String_t**)SZArrayLdElema(L_14, ((int32_t)14), sizeof(String_t*))) = (String_t*)_stringLiteral2292;
		StringU5BU5D_t1_238* L_15 = L_14;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, ((int32_t)15));
		ArrayElementTypeCheck (L_15, _stringLiteral2293);
		*((String_t**)(String_t**)SZArrayLdElema(L_15, ((int32_t)15), sizeof(String_t*))) = (String_t*)_stringLiteral2293;
		StringU5BU5D_t1_238* L_16 = L_15;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, ((int32_t)16));
		ArrayElementTypeCheck (L_16, _stringLiteral2294);
		*((String_t**)(String_t**)SZArrayLdElema(L_16, ((int32_t)16), sizeof(String_t*))) = (String_t*)_stringLiteral2294;
		StringU5BU5D_t1_238* L_17 = L_16;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, ((int32_t)17));
		ArrayElementTypeCheck (L_17, _stringLiteral2295);
		*((String_t**)(String_t**)SZArrayLdElema(L_17, ((int32_t)17), sizeof(String_t*))) = (String_t*)_stringLiteral2295;
		StringU5BU5D_t1_238* L_18 = L_17;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, ((int32_t)18));
		ArrayElementTypeCheck (L_18, _stringLiteral2296);
		*((String_t**)(String_t**)SZArrayLdElema(L_18, ((int32_t)18), sizeof(String_t*))) = (String_t*)_stringLiteral2296;
		StringU5BU5D_t1_238* L_19 = L_18;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, ((int32_t)19));
		ArrayElementTypeCheck (L_19, _stringLiteral2297);
		*((String_t**)(String_t**)SZArrayLdElema(L_19, ((int32_t)19), sizeof(String_t*))) = (String_t*)_stringLiteral2297;
		StringU5BU5D_t1_238* L_20 = L_19;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, ((int32_t)20));
		ArrayElementTypeCheck (L_20, _stringLiteral2298);
		*((String_t**)(String_t**)SZArrayLdElema(L_20, ((int32_t)20), sizeof(String_t*))) = (String_t*)_stringLiteral2298;
		StringU5BU5D_t1_238* L_21 = L_20;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, ((int32_t)21));
		ArrayElementTypeCheck (L_21, _stringLiteral2299);
		*((String_t**)(String_t**)SZArrayLdElema(L_21, ((int32_t)21), sizeof(String_t*))) = (String_t*)_stringLiteral2299;
		StringU5BU5D_t1_238* L_22 = L_21;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, ((int32_t)22));
		ArrayElementTypeCheck (L_22, _stringLiteral2300);
		*((String_t**)(String_t**)SZArrayLdElema(L_22, ((int32_t)22), sizeof(String_t*))) = (String_t*)_stringLiteral2300;
		StringU5BU5D_t1_238* L_23 = L_22;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, ((int32_t)23));
		ArrayElementTypeCheck (L_23, _stringLiteral2301);
		*((String_t**)(String_t**)SZArrayLdElema(L_23, ((int32_t)23), sizeof(String_t*))) = (String_t*)_stringLiteral2301;
		((XsdTime_t4_47_StaticFields*)XsdTime_t4_47_il2cpp_TypeInfo_var->static_fields)->___timeFormats_61 = L_23;
		return;
	}
}
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdTime::get_TokenizedType()
extern "C" int32_t XsdTime_get_TokenizedType_m4_72 (XsdTime_t4_47 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(0);
	}
}
// System.Void Mono.Xml.Schema.XsdGYearMonth::.ctor()
extern TypeInfo* XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var;
extern "C" void XsdGYearMonth__ctor_m4_73 (XsdGYearMonth_t4_48 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1216);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var);
		XsdAnySimpleType__ctor_m4_1(__this, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4 *)__this)->___WhitespaceValue_0 = 2;
		return;
	}
}
// System.Void Mono.Xml.Schema.XsdGMonthDay::.ctor()
extern TypeInfo* XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var;
extern "C" void XsdGMonthDay__ctor_m4_74 (XsdGMonthDay_t4_49 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1216);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var);
		XsdAnySimpleType__ctor_m4_1(__this, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4 *)__this)->___WhitespaceValue_0 = 2;
		return;
	}
}
// System.Void Mono.Xml.Schema.XsdGYear::.ctor()
extern TypeInfo* XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var;
extern "C" void XsdGYear__ctor_m4_75 (XsdGYear_t4_50 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1216);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var);
		XsdAnySimpleType__ctor_m4_1(__this, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4 *)__this)->___WhitespaceValue_0 = 2;
		return;
	}
}
// System.Void Mono.Xml.Schema.XsdGMonth::.ctor()
extern TypeInfo* XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var;
extern "C" void XsdGMonth__ctor_m4_76 (XsdGMonth_t4_51 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1216);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var);
		XsdAnySimpleType__ctor_m4_1(__this, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4 *)__this)->___WhitespaceValue_0 = 2;
		return;
	}
}
// System.Void Mono.Xml.Schema.XsdGDay::.ctor()
extern TypeInfo* XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var;
extern "C" void XsdGDay__ctor_m4_77 (XsdGDay_t4_52 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1216);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var);
		XsdAnySimpleType__ctor_m4_1(__this, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4 *)__this)->___WhitespaceValue_0 = 2;
		return;
	}
}
// System.Void System.Xml.Schema.XmlSchemaAnnotated::.ctor()
extern "C" void XmlSchemaAnnotated__ctor_m4_78 (XmlSchemaAnnotated_t4_53 * __this, const MethodInfo* method)
{
	{
		XmlSchemaObject__ctor_m4_93(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.Schema.XmlSchemaCompilationSettings::.ctor()
extern "C" void XmlSchemaCompilationSettings__ctor_m4_79 (XmlSchemaCompilationSettings_t4_56 * __this, const MethodInfo* method)
{
	{
		__this->___enable_upa_check_0 = 1;
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.Schema.XmlSchemaDatatype::.ctor()
extern TypeInfo* StringBuilder_t1_247_il2cpp_TypeInfo_var;
extern "C" void XmlSchemaDatatype__ctor_m4_80 (XmlSchemaDatatype_t4_4 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringBuilder_t1_247_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringBuilder_t1_247 * L_0 = (StringBuilder_t1_247 *)il2cpp_codegen_object_new (StringBuilder_t1_247_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1_12414(L_0, /*hidden argument*/NULL);
		__this->___sb_2 = L_0;
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.Schema.XmlSchemaDatatype::.cctor()
extern TypeInfo* CharU5BU5D_t1_16_il2cpp_TypeInfo_var;
extern TypeInfo* XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var;
extern TypeInfo* XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var;
extern TypeInfo* XsdString_t4_7_il2cpp_TypeInfo_var;
extern TypeInfo* XsdNormalizedString_t4_8_il2cpp_TypeInfo_var;
extern TypeInfo* XsdToken_t4_9_il2cpp_TypeInfo_var;
extern TypeInfo* XsdLanguage_t4_10_il2cpp_TypeInfo_var;
extern TypeInfo* XsdNMToken_t4_11_il2cpp_TypeInfo_var;
extern TypeInfo* XsdNMTokens_t4_12_il2cpp_TypeInfo_var;
extern TypeInfo* XsdName_t4_13_il2cpp_TypeInfo_var;
extern TypeInfo* XsdNCName_t4_14_il2cpp_TypeInfo_var;
extern TypeInfo* XsdID_t4_15_il2cpp_TypeInfo_var;
extern TypeInfo* XsdIDRef_t4_16_il2cpp_TypeInfo_var;
extern TypeInfo* XsdIDRefs_t4_17_il2cpp_TypeInfo_var;
extern TypeInfo* XsdEntity_t4_18_il2cpp_TypeInfo_var;
extern TypeInfo* XsdEntities_t4_19_il2cpp_TypeInfo_var;
extern TypeInfo* XsdNotation_t4_20_il2cpp_TypeInfo_var;
extern TypeInfo* XsdDecimal_t4_21_il2cpp_TypeInfo_var;
extern TypeInfo* XsdInteger_t4_22_il2cpp_TypeInfo_var;
extern TypeInfo* XsdLong_t4_23_il2cpp_TypeInfo_var;
extern TypeInfo* XsdInt_t4_24_il2cpp_TypeInfo_var;
extern TypeInfo* XsdShort_t4_25_il2cpp_TypeInfo_var;
extern TypeInfo* XsdByte_t4_26_il2cpp_TypeInfo_var;
extern TypeInfo* XsdNonNegativeInteger_t4_27_il2cpp_TypeInfo_var;
extern TypeInfo* XsdPositiveInteger_t4_32_il2cpp_TypeInfo_var;
extern TypeInfo* XsdUnsignedLong_t4_28_il2cpp_TypeInfo_var;
extern TypeInfo* XsdUnsignedInt_t4_29_il2cpp_TypeInfo_var;
extern TypeInfo* XsdUnsignedShort_t4_30_il2cpp_TypeInfo_var;
extern TypeInfo* XsdUnsignedByte_t4_31_il2cpp_TypeInfo_var;
extern TypeInfo* XsdNonPositiveInteger_t4_33_il2cpp_TypeInfo_var;
extern TypeInfo* XsdNegativeInteger_t4_34_il2cpp_TypeInfo_var;
extern TypeInfo* XsdFloat_t4_35_il2cpp_TypeInfo_var;
extern TypeInfo* XsdDouble_t4_36_il2cpp_TypeInfo_var;
extern TypeInfo* XsdBase64Binary_t4_37_il2cpp_TypeInfo_var;
extern TypeInfo* XsdBoolean_t4_40_il2cpp_TypeInfo_var;
extern TypeInfo* XsdAnyURI_t4_41_il2cpp_TypeInfo_var;
extern TypeInfo* XsdDuration_t4_42_il2cpp_TypeInfo_var;
extern TypeInfo* XsdDateTime_t4_45_il2cpp_TypeInfo_var;
extern TypeInfo* XsdDate_t4_46_il2cpp_TypeInfo_var;
extern TypeInfo* XsdTime_t4_47_il2cpp_TypeInfo_var;
extern TypeInfo* XsdHexBinary_t4_38_il2cpp_TypeInfo_var;
extern TypeInfo* XsdQName_t4_39_il2cpp_TypeInfo_var;
extern TypeInfo* XsdGYearMonth_t4_48_il2cpp_TypeInfo_var;
extern TypeInfo* XsdGMonthDay_t4_49_il2cpp_TypeInfo_var;
extern TypeInfo* XsdGYear_t4_50_il2cpp_TypeInfo_var;
extern TypeInfo* XsdGMonth_t4_51_il2cpp_TypeInfo_var;
extern TypeInfo* XsdGDay_t4_52_il2cpp_TypeInfo_var;
extern TypeInfo* XdtAnyAtomicType_t4_5_il2cpp_TypeInfo_var;
extern TypeInfo* XdtUntypedAtomic_t4_6_il2cpp_TypeInfo_var;
extern TypeInfo* XdtDayTimeDuration_t4_43_il2cpp_TypeInfo_var;
extern TypeInfo* XdtYearMonthDuration_t4_44_il2cpp_TypeInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t4_192____U24U24fieldU2D23_0_FieldInfo_var;
extern "C" void XmlSchemaDatatype__cctor_m4_81 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CharU5BU5D_t1_16_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(15);
		XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1215);
		XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1216);
		XsdString_t4_7_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1220);
		XsdNormalizedString_t4_8_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1221);
		XsdToken_t4_9_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1222);
		XsdLanguage_t4_10_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1223);
		XsdNMToken_t4_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1224);
		XsdNMTokens_t4_12_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1225);
		XsdName_t4_13_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1226);
		XsdNCName_t4_14_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1227);
		XsdID_t4_15_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1228);
		XsdIDRef_t4_16_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1229);
		XsdIDRefs_t4_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1230);
		XsdEntity_t4_18_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1231);
		XsdEntities_t4_19_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1232);
		XsdNotation_t4_20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1233);
		XsdDecimal_t4_21_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1234);
		XsdInteger_t4_22_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1235);
		XsdLong_t4_23_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1236);
		XsdInt_t4_24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1237);
		XsdShort_t4_25_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1238);
		XsdByte_t4_26_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1239);
		XsdNonNegativeInteger_t4_27_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1240);
		XsdPositiveInteger_t4_32_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1241);
		XsdUnsignedLong_t4_28_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1242);
		XsdUnsignedInt_t4_29_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1243);
		XsdUnsignedShort_t4_30_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1244);
		XsdUnsignedByte_t4_31_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1245);
		XsdNonPositiveInteger_t4_33_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1246);
		XsdNegativeInteger_t4_34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1247);
		XsdFloat_t4_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1248);
		XsdDouble_t4_36_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1249);
		XsdBase64Binary_t4_37_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1217);
		XsdBoolean_t4_40_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1250);
		XsdAnyURI_t4_41_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1251);
		XsdDuration_t4_42_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1252);
		XsdDateTime_t4_45_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1253);
		XsdDate_t4_46_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1254);
		XsdTime_t4_47_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1219);
		XsdHexBinary_t4_38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1255);
		XsdQName_t4_39_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1256);
		XsdGYearMonth_t4_48_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1257);
		XsdGMonthDay_t4_49_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1258);
		XsdGYear_t4_50_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1259);
		XsdGMonth_t4_51_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1260);
		XsdGDay_t4_52_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1261);
		XdtAnyAtomicType_t4_5_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1262);
		XdtUntypedAtomic_t4_6_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1263);
		XdtDayTimeDuration_t4_43_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1264);
		XdtYearMonthDuration_t4_44_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1265);
		U3CPrivateImplementationDetailsU3E_t4_192____U24U24fieldU2D23_0_FieldInfo_var = il2cpp_codegen_field_info_from_index(1266, 0);
		s_Il2CppMethodIntialized = true;
	}
	{
		CharU5BU5D_t1_16* L_0 = ((CharU5BU5D_t1_16*)SZArrayNew(CharU5BU5D_t1_16_il2cpp_TypeInfo_var, 4));
		RuntimeHelpers_InitializeArray_m1_1387(NULL /*static, unused*/, (Array_t *)(Array_t *)L_0, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t4_192____U24U24fieldU2D23_0_FieldInfo_var), /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___wsChars_1 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(XsdAnySimpleType_t4_3_il2cpp_TypeInfo_var);
		XsdAnySimpleType_t4_3 * L_1 = XsdAnySimpleType_get_Instance_m4_3(NULL /*static, unused*/, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeAnySimpleType_3 = L_1;
		XsdString_t4_7 * L_2 = (XsdString_t4_7 *)il2cpp_codegen_object_new (XsdString_t4_7_il2cpp_TypeInfo_var);
		XsdString__ctor_m4_7(L_2, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeString_4 = L_2;
		XsdNormalizedString_t4_8 * L_3 = (XsdNormalizedString_t4_8 *)il2cpp_codegen_object_new (XsdNormalizedString_t4_8_il2cpp_TypeInfo_var);
		XsdNormalizedString__ctor_m4_9(L_3, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeNormalizedString_5 = L_3;
		XsdToken_t4_9 * L_4 = (XsdToken_t4_9 *)il2cpp_codegen_object_new (XsdToken_t4_9_il2cpp_TypeInfo_var);
		XsdToken__ctor_m4_11(L_4, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeToken_6 = L_4;
		XsdLanguage_t4_10 * L_5 = (XsdLanguage_t4_10 *)il2cpp_codegen_object_new (XsdLanguage_t4_10_il2cpp_TypeInfo_var);
		XsdLanguage__ctor_m4_13(L_5, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeLanguage_7 = L_5;
		XsdNMToken_t4_11 * L_6 = (XsdNMToken_t4_11 *)il2cpp_codegen_object_new (XsdNMToken_t4_11_il2cpp_TypeInfo_var);
		XsdNMToken__ctor_m4_15(L_6, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeNMToken_8 = L_6;
		XsdNMTokens_t4_12 * L_7 = (XsdNMTokens_t4_12 *)il2cpp_codegen_object_new (XsdNMTokens_t4_12_il2cpp_TypeInfo_var);
		XsdNMTokens__ctor_m4_17(L_7, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeNMTokens_9 = L_7;
		XsdName_t4_13 * L_8 = (XsdName_t4_13 *)il2cpp_codegen_object_new (XsdName_t4_13_il2cpp_TypeInfo_var);
		XsdName__ctor_m4_19(L_8, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeName_10 = L_8;
		XsdNCName_t4_14 * L_9 = (XsdNCName_t4_14 *)il2cpp_codegen_object_new (XsdNCName_t4_14_il2cpp_TypeInfo_var);
		XsdNCName__ctor_m4_21(L_9, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeNCName_11 = L_9;
		XsdID_t4_15 * L_10 = (XsdID_t4_15 *)il2cpp_codegen_object_new (XsdID_t4_15_il2cpp_TypeInfo_var);
		XsdID__ctor_m4_23(L_10, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeID_12 = L_10;
		XsdIDRef_t4_16 * L_11 = (XsdIDRef_t4_16 *)il2cpp_codegen_object_new (XsdIDRef_t4_16_il2cpp_TypeInfo_var);
		XsdIDRef__ctor_m4_25(L_11, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeIDRef_13 = L_11;
		XsdIDRefs_t4_17 * L_12 = (XsdIDRefs_t4_17 *)il2cpp_codegen_object_new (XsdIDRefs_t4_17_il2cpp_TypeInfo_var);
		XsdIDRefs__ctor_m4_27(L_12, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeIDRefs_14 = L_12;
		XsdEntity_t4_18 * L_13 = (XsdEntity_t4_18 *)il2cpp_codegen_object_new (XsdEntity_t4_18_il2cpp_TypeInfo_var);
		XsdEntity__ctor_m4_29(L_13, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeEntity_15 = L_13;
		XsdEntities_t4_19 * L_14 = (XsdEntities_t4_19 *)il2cpp_codegen_object_new (XsdEntities_t4_19_il2cpp_TypeInfo_var);
		XsdEntities__ctor_m4_31(L_14, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeEntities_16 = L_14;
		XsdNotation_t4_20 * L_15 = (XsdNotation_t4_20 *)il2cpp_codegen_object_new (XsdNotation_t4_20_il2cpp_TypeInfo_var);
		XsdNotation__ctor_m4_33(L_15, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeNotation_17 = L_15;
		XsdDecimal_t4_21 * L_16 = (XsdDecimal_t4_21 *)il2cpp_codegen_object_new (XsdDecimal_t4_21_il2cpp_TypeInfo_var);
		XsdDecimal__ctor_m4_35(L_16, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeDecimal_18 = L_16;
		XsdInteger_t4_22 * L_17 = (XsdInteger_t4_22 *)il2cpp_codegen_object_new (XsdInteger_t4_22_il2cpp_TypeInfo_var);
		XsdInteger__ctor_m4_37(L_17, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeInteger_19 = L_17;
		XsdLong_t4_23 * L_18 = (XsdLong_t4_23 *)il2cpp_codegen_object_new (XsdLong_t4_23_il2cpp_TypeInfo_var);
		XsdLong__ctor_m4_38(L_18, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeLong_20 = L_18;
		XsdInt_t4_24 * L_19 = (XsdInt_t4_24 *)il2cpp_codegen_object_new (XsdInt_t4_24_il2cpp_TypeInfo_var);
		XsdInt__ctor_m4_39(L_19, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeInt_21 = L_19;
		XsdShort_t4_25 * L_20 = (XsdShort_t4_25 *)il2cpp_codegen_object_new (XsdShort_t4_25_il2cpp_TypeInfo_var);
		XsdShort__ctor_m4_40(L_20, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeShort_22 = L_20;
		XsdByte_t4_26 * L_21 = (XsdByte_t4_26 *)il2cpp_codegen_object_new (XsdByte_t4_26_il2cpp_TypeInfo_var);
		XsdByte__ctor_m4_41(L_21, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeByte_23 = L_21;
		XsdNonNegativeInteger_t4_27 * L_22 = (XsdNonNegativeInteger_t4_27 *)il2cpp_codegen_object_new (XsdNonNegativeInteger_t4_27_il2cpp_TypeInfo_var);
		XsdNonNegativeInteger__ctor_m4_42(L_22, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeNonNegativeInteger_24 = L_22;
		XsdPositiveInteger_t4_32 * L_23 = (XsdPositiveInteger_t4_32 *)il2cpp_codegen_object_new (XsdPositiveInteger_t4_32_il2cpp_TypeInfo_var);
		XsdPositiveInteger__ctor_m4_47(L_23, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypePositiveInteger_25 = L_23;
		XsdUnsignedLong_t4_28 * L_24 = (XsdUnsignedLong_t4_28 *)il2cpp_codegen_object_new (XsdUnsignedLong_t4_28_il2cpp_TypeInfo_var);
		XsdUnsignedLong__ctor_m4_43(L_24, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeUnsignedLong_26 = L_24;
		XsdUnsignedInt_t4_29 * L_25 = (XsdUnsignedInt_t4_29 *)il2cpp_codegen_object_new (XsdUnsignedInt_t4_29_il2cpp_TypeInfo_var);
		XsdUnsignedInt__ctor_m4_44(L_25, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeUnsignedInt_27 = L_25;
		XsdUnsignedShort_t4_30 * L_26 = (XsdUnsignedShort_t4_30 *)il2cpp_codegen_object_new (XsdUnsignedShort_t4_30_il2cpp_TypeInfo_var);
		XsdUnsignedShort__ctor_m4_45(L_26, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeUnsignedShort_28 = L_26;
		XsdUnsignedByte_t4_31 * L_27 = (XsdUnsignedByte_t4_31 *)il2cpp_codegen_object_new (XsdUnsignedByte_t4_31_il2cpp_TypeInfo_var);
		XsdUnsignedByte__ctor_m4_46(L_27, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeUnsignedByte_29 = L_27;
		XsdNonPositiveInteger_t4_33 * L_28 = (XsdNonPositiveInteger_t4_33 *)il2cpp_codegen_object_new (XsdNonPositiveInteger_t4_33_il2cpp_TypeInfo_var);
		XsdNonPositiveInteger__ctor_m4_48(L_28, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeNonPositiveInteger_30 = L_28;
		XsdNegativeInteger_t4_34 * L_29 = (XsdNegativeInteger_t4_34 *)il2cpp_codegen_object_new (XsdNegativeInteger_t4_34_il2cpp_TypeInfo_var);
		XsdNegativeInteger__ctor_m4_49(L_29, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeNegativeInteger_31 = L_29;
		XsdFloat_t4_35 * L_30 = (XsdFloat_t4_35 *)il2cpp_codegen_object_new (XsdFloat_t4_35_il2cpp_TypeInfo_var);
		XsdFloat__ctor_m4_50(L_30, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeFloat_32 = L_30;
		XsdDouble_t4_36 * L_31 = (XsdDouble_t4_36 *)il2cpp_codegen_object_new (XsdDouble_t4_36_il2cpp_TypeInfo_var);
		XsdDouble__ctor_m4_51(L_31, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeDouble_33 = L_31;
		XsdBase64Binary_t4_37 * L_32 = (XsdBase64Binary_t4_37 *)il2cpp_codegen_object_new (XsdBase64Binary_t4_37_il2cpp_TypeInfo_var);
		XsdBase64Binary__ctor_m4_52(L_32, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeBase64Binary_34 = L_32;
		XsdBoolean_t4_40 * L_33 = (XsdBoolean_t4_40 *)il2cpp_codegen_object_new (XsdBoolean_t4_40_il2cpp_TypeInfo_var);
		XsdBoolean__ctor_m4_58(L_33, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeBoolean_35 = L_33;
		XsdAnyURI_t4_41 * L_34 = (XsdAnyURI_t4_41 *)il2cpp_codegen_object_new (XsdAnyURI_t4_41_il2cpp_TypeInfo_var);
		XsdAnyURI__ctor_m4_60(L_34, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeAnyURI_36 = L_34;
		XsdDuration_t4_42 * L_35 = (XsdDuration_t4_42 *)il2cpp_codegen_object_new (XsdDuration_t4_42_il2cpp_TypeInfo_var);
		XsdDuration__ctor_m4_62(L_35, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeDuration_37 = L_35;
		XsdDateTime_t4_45 * L_36 = (XsdDateTime_t4_45 *)il2cpp_codegen_object_new (XsdDateTime_t4_45_il2cpp_TypeInfo_var);
		XsdDateTime__ctor_m4_66(L_36, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeDateTime_38 = L_36;
		XsdDate_t4_46 * L_37 = (XsdDate_t4_46 *)il2cpp_codegen_object_new (XsdDate_t4_46_il2cpp_TypeInfo_var);
		XsdDate__ctor_m4_68(L_37, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeDate_39 = L_37;
		XsdTime_t4_47 * L_38 = (XsdTime_t4_47 *)il2cpp_codegen_object_new (XsdTime_t4_47_il2cpp_TypeInfo_var);
		XsdTime__ctor_m4_70(L_38, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeTime_40 = L_38;
		XsdHexBinary_t4_38 * L_39 = (XsdHexBinary_t4_38 *)il2cpp_codegen_object_new (XsdHexBinary_t4_38_il2cpp_TypeInfo_var);
		XsdHexBinary__ctor_m4_54(L_39, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeHexBinary_41 = L_39;
		XsdQName_t4_39 * L_40 = (XsdQName_t4_39 *)il2cpp_codegen_object_new (XsdQName_t4_39_il2cpp_TypeInfo_var);
		XsdQName__ctor_m4_56(L_40, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeQName_42 = L_40;
		XsdGYearMonth_t4_48 * L_41 = (XsdGYearMonth_t4_48 *)il2cpp_codegen_object_new (XsdGYearMonth_t4_48_il2cpp_TypeInfo_var);
		XsdGYearMonth__ctor_m4_73(L_41, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeGYearMonth_43 = L_41;
		XsdGMonthDay_t4_49 * L_42 = (XsdGMonthDay_t4_49 *)il2cpp_codegen_object_new (XsdGMonthDay_t4_49_il2cpp_TypeInfo_var);
		XsdGMonthDay__ctor_m4_74(L_42, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeGMonthDay_44 = L_42;
		XsdGYear_t4_50 * L_43 = (XsdGYear_t4_50 *)il2cpp_codegen_object_new (XsdGYear_t4_50_il2cpp_TypeInfo_var);
		XsdGYear__ctor_m4_75(L_43, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeGYear_45 = L_43;
		XsdGMonth_t4_51 * L_44 = (XsdGMonth_t4_51 *)il2cpp_codegen_object_new (XsdGMonth_t4_51_il2cpp_TypeInfo_var);
		XsdGMonth__ctor_m4_76(L_44, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeGMonth_46 = L_44;
		XsdGDay_t4_52 * L_45 = (XsdGDay_t4_52 *)il2cpp_codegen_object_new (XsdGDay_t4_52_il2cpp_TypeInfo_var);
		XsdGDay__ctor_m4_77(L_45, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeGDay_47 = L_45;
		XdtAnyAtomicType_t4_5 * L_46 = (XdtAnyAtomicType_t4_5 *)il2cpp_codegen_object_new (XdtAnyAtomicType_t4_5_il2cpp_TypeInfo_var);
		XdtAnyAtomicType__ctor_m4_5(L_46, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeAnyAtomicType_48 = L_46;
		XdtUntypedAtomic_t4_6 * L_47 = (XdtUntypedAtomic_t4_6 *)il2cpp_codegen_object_new (XdtUntypedAtomic_t4_6_il2cpp_TypeInfo_var);
		XdtUntypedAtomic__ctor_m4_6(L_47, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeUntypedAtomic_49 = L_47;
		XdtDayTimeDuration_t4_43 * L_48 = (XdtDayTimeDuration_t4_43 *)il2cpp_codegen_object_new (XdtDayTimeDuration_t4_43_il2cpp_TypeInfo_var);
		XdtDayTimeDuration__ctor_m4_64(L_48, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeDayTimeDuration_50 = L_48;
		XdtYearMonthDuration_t4_44 * L_49 = (XdtYearMonthDuration_t4_44 *)il2cpp_codegen_object_new (XdtYearMonthDuration_t4_44_il2cpp_TypeInfo_var);
		XdtYearMonthDuration__ctor_m4_65(L_49, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeYearMonthDuration_51 = L_49;
		return;
	}
}
// System.Xml.Schema.XmlSchemaDatatype System.Xml.Schema.XmlSchemaDatatype::FromName(System.Xml.XmlQualifiedName)
extern TypeInfo* XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var;
extern "C" XmlSchemaDatatype_t4_4 * XmlSchemaDatatype_FromName_m4_82 (Object_t * __this /* static, unused */, XmlQualifiedName_t4_71 * ___qname, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1215);
		s_Il2CppMethodIntialized = true;
	}
	{
		XmlQualifiedName_t4_71 * L_0 = ___qname;
		NullCheck(L_0);
		String_t* L_1 = XmlQualifiedName_get_Name_m4_732(L_0, /*hidden argument*/NULL);
		XmlQualifiedName_t4_71 * L_2 = ___qname;
		NullCheck(L_2);
		String_t* L_3 = XmlQualifiedName_get_Namespace_m4_733(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		XmlSchemaDatatype_t4_4 * L_4 = XmlSchemaDatatype_FromName_m4_83(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Xml.Schema.XmlSchemaDatatype System.Xml.Schema.XmlSchemaDatatype::FromName(System.String,System.String)
extern TypeInfo* XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t1_92_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_14878_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3778;
extern Il2CppCodeGenString* _stringLiteral3779;
extern Il2CppCodeGenString* _stringLiteral3780;
extern Il2CppCodeGenString* _stringLiteral3781;
extern Il2CppCodeGenString* _stringLiteral3782;
extern Il2CppCodeGenString* _stringLiteral3783;
extern Il2CppCodeGenString* _stringLiteral3784;
extern Il2CppCodeGenString* _stringLiteral309;
extern Il2CppCodeGenString* _stringLiteral2275;
extern Il2CppCodeGenString* _stringLiteral2302;
extern Il2CppCodeGenString* _stringLiteral2261;
extern Il2CppCodeGenString* _stringLiteral2271;
extern Il2CppCodeGenString* _stringLiteral2272;
extern Il2CppCodeGenString* _stringLiteral1969;
extern Il2CppCodeGenString* _stringLiteral2268;
extern Il2CppCodeGenString* _stringLiteral2257;
extern Il2CppCodeGenString* _stringLiteral2258;
extern Il2CppCodeGenString* _stringLiteral2259;
extern Il2CppCodeGenString* _stringLiteral2253;
extern Il2CppCodeGenString* _stringLiteral2252;
extern Il2CppCodeGenString* _stringLiteral2276;
extern Il2CppCodeGenString* _stringLiteral3785;
extern Il2CppCodeGenString* _stringLiteral2260;
extern Il2CppCodeGenString* _stringLiteral3786;
extern Il2CppCodeGenString* _stringLiteral307;
extern Il2CppCodeGenString* _stringLiteral3787;
extern Il2CppCodeGenString* _stringLiteral3788;
extern Il2CppCodeGenString* _stringLiteral2274;
extern Il2CppCodeGenString* _stringLiteral2270;
extern Il2CppCodeGenString* _stringLiteral2273;
extern Il2CppCodeGenString* _stringLiteral3789;
extern Il2CppCodeGenString* _stringLiteral3790;
extern Il2CppCodeGenString* _stringLiteral3791;
extern Il2CppCodeGenString* _stringLiteral3792;
extern Il2CppCodeGenString* _stringLiteral2277;
extern Il2CppCodeGenString* _stringLiteral3793;
extern Il2CppCodeGenString* _stringLiteral3794;
extern Il2CppCodeGenString* _stringLiteral2214;
extern Il2CppCodeGenString* _stringLiteral3795;
extern Il2CppCodeGenString* _stringLiteral3796;
extern Il2CppCodeGenString* _stringLiteral2249;
extern Il2CppCodeGenString* _stringLiteral2245;
extern Il2CppCodeGenString* _stringLiteral954;
extern Il2CppCodeGenString* _stringLiteral673;
extern Il2CppCodeGenString* _stringLiteral2255;
extern Il2CppCodeGenString* _stringLiteral2278;
extern Il2CppCodeGenString* _stringLiteral2316;
extern Il2CppCodeGenString* _stringLiteral2267;
extern Il2CppCodeGenString* _stringLiteral2309;
extern Il2CppCodeGenString* _stringLiteral2264;
extern Il2CppCodeGenString* _stringLiteral2248;
extern "C" XmlSchemaDatatype_t4_4 * XmlSchemaDatatype_FromName_m4_83 (Object_t * __this /* static, unused */, String_t* ___localName, String_t* ___ns, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1215);
		Dictionary_2_t1_92_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(129);
		Dictionary_2__ctor_m1_14878_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483658);
		_stringLiteral3778 = il2cpp_codegen_string_literal_from_index(3778);
		_stringLiteral3779 = il2cpp_codegen_string_literal_from_index(3779);
		_stringLiteral3780 = il2cpp_codegen_string_literal_from_index(3780);
		_stringLiteral3781 = il2cpp_codegen_string_literal_from_index(3781);
		_stringLiteral3782 = il2cpp_codegen_string_literal_from_index(3782);
		_stringLiteral3783 = il2cpp_codegen_string_literal_from_index(3783);
		_stringLiteral3784 = il2cpp_codegen_string_literal_from_index(3784);
		_stringLiteral309 = il2cpp_codegen_string_literal_from_index(309);
		_stringLiteral2275 = il2cpp_codegen_string_literal_from_index(2275);
		_stringLiteral2302 = il2cpp_codegen_string_literal_from_index(2302);
		_stringLiteral2261 = il2cpp_codegen_string_literal_from_index(2261);
		_stringLiteral2271 = il2cpp_codegen_string_literal_from_index(2271);
		_stringLiteral2272 = il2cpp_codegen_string_literal_from_index(2272);
		_stringLiteral1969 = il2cpp_codegen_string_literal_from_index(1969);
		_stringLiteral2268 = il2cpp_codegen_string_literal_from_index(2268);
		_stringLiteral2257 = il2cpp_codegen_string_literal_from_index(2257);
		_stringLiteral2258 = il2cpp_codegen_string_literal_from_index(2258);
		_stringLiteral2259 = il2cpp_codegen_string_literal_from_index(2259);
		_stringLiteral2253 = il2cpp_codegen_string_literal_from_index(2253);
		_stringLiteral2252 = il2cpp_codegen_string_literal_from_index(2252);
		_stringLiteral2276 = il2cpp_codegen_string_literal_from_index(2276);
		_stringLiteral3785 = il2cpp_codegen_string_literal_from_index(3785);
		_stringLiteral2260 = il2cpp_codegen_string_literal_from_index(2260);
		_stringLiteral3786 = il2cpp_codegen_string_literal_from_index(3786);
		_stringLiteral307 = il2cpp_codegen_string_literal_from_index(307);
		_stringLiteral3787 = il2cpp_codegen_string_literal_from_index(3787);
		_stringLiteral3788 = il2cpp_codegen_string_literal_from_index(3788);
		_stringLiteral2274 = il2cpp_codegen_string_literal_from_index(2274);
		_stringLiteral2270 = il2cpp_codegen_string_literal_from_index(2270);
		_stringLiteral2273 = il2cpp_codegen_string_literal_from_index(2273);
		_stringLiteral3789 = il2cpp_codegen_string_literal_from_index(3789);
		_stringLiteral3790 = il2cpp_codegen_string_literal_from_index(3790);
		_stringLiteral3791 = il2cpp_codegen_string_literal_from_index(3791);
		_stringLiteral3792 = il2cpp_codegen_string_literal_from_index(3792);
		_stringLiteral2277 = il2cpp_codegen_string_literal_from_index(2277);
		_stringLiteral3793 = il2cpp_codegen_string_literal_from_index(3793);
		_stringLiteral3794 = il2cpp_codegen_string_literal_from_index(3794);
		_stringLiteral2214 = il2cpp_codegen_string_literal_from_index(2214);
		_stringLiteral3795 = il2cpp_codegen_string_literal_from_index(3795);
		_stringLiteral3796 = il2cpp_codegen_string_literal_from_index(3796);
		_stringLiteral2249 = il2cpp_codegen_string_literal_from_index(2249);
		_stringLiteral2245 = il2cpp_codegen_string_literal_from_index(2245);
		_stringLiteral954 = il2cpp_codegen_string_literal_from_index(954);
		_stringLiteral673 = il2cpp_codegen_string_literal_from_index(673);
		_stringLiteral2255 = il2cpp_codegen_string_literal_from_index(2255);
		_stringLiteral2278 = il2cpp_codegen_string_literal_from_index(2278);
		_stringLiteral2316 = il2cpp_codegen_string_literal_from_index(2316);
		_stringLiteral2267 = il2cpp_codegen_string_literal_from_index(2267);
		_stringLiteral2309 = il2cpp_codegen_string_literal_from_index(2309);
		_stringLiteral2264 = il2cpp_codegen_string_literal_from_index(2264);
		_stringLiteral2248 = il2cpp_codegen_string_literal_from_index(2248);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	Dictionary_2_t1_92 * V_1 = {0};
	int32_t V_2 = 0;
	String_t* V_3 = {0};
	int32_t V_4 = 0;
	{
		String_t* L_0 = ___ns;
		V_0 = L_0;
		String_t* L_1 = V_0;
		if (!L_1)
		{
			goto IL_00f7;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		Dictionary_2_t1_92 * L_2 = ((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__switchU24map2B_53;
		if (L_2)
		{
			goto IL_0037;
		}
	}
	{
		Dictionary_2_t1_92 * L_3 = (Dictionary_2_t1_92 *)il2cpp_codegen_object_new (Dictionary_2_t1_92_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_14878(L_3, 2, /*hidden argument*/Dictionary_2__ctor_m1_14878_MethodInfo_var);
		V_1 = L_3;
		Dictionary_2_t1_92 * L_4 = V_1;
		NullCheck(L_4);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_4, _stringLiteral3778, 0);
		Dictionary_2_t1_92 * L_5 = V_1;
		NullCheck(L_5);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_5, _stringLiteral3779, 1);
		Dictionary_2_t1_92 * L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__switchU24map2B_53 = L_6;
	}

IL_0037:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		Dictionary_2_t1_92 * L_7 = ((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__switchU24map2B_53;
		String_t* L_8 = V_0;
		NullCheck(L_7);
		bool L_9 = (bool)VirtFuncInvoker2< bool, String_t*, int32_t* >::Invoke(21 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int32>::TryGetValue(!0,!1&) */, L_7, L_8, (&V_2));
		if (!L_9)
		{
			goto IL_00f7;
		}
	}
	{
		int32_t L_10 = V_2;
		if (!L_10)
		{
			goto IL_005b;
		}
	}
	{
		int32_t L_11 = V_2;
		if ((((int32_t)L_11) == ((int32_t)1)))
		{
			goto IL_0060;
		}
	}
	{
		goto IL_00f7;
	}

IL_005b:
	{
		goto IL_00f9;
	}

IL_0060:
	{
		String_t* L_12 = ___localName;
		V_3 = L_12;
		String_t* L_13 = V_3;
		if (!L_13)
		{
			goto IL_00f5;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		Dictionary_2_t1_92 * L_14 = ((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__switchU24map2A_52;
		if (L_14)
		{
			goto IL_00af;
		}
	}
	{
		Dictionary_2_t1_92 * L_15 = (Dictionary_2_t1_92 *)il2cpp_codegen_object_new (Dictionary_2_t1_92_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_14878(L_15, 4, /*hidden argument*/Dictionary_2__ctor_m1_14878_MethodInfo_var);
		V_1 = L_15;
		Dictionary_2_t1_92 * L_16 = V_1;
		NullCheck(L_16);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_16, _stringLiteral3780, 0);
		Dictionary_2_t1_92 * L_17 = V_1;
		NullCheck(L_17);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_17, _stringLiteral3781, 1);
		Dictionary_2_t1_92 * L_18 = V_1;
		NullCheck(L_18);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_18, _stringLiteral3782, 2);
		Dictionary_2_t1_92 * L_19 = V_1;
		NullCheck(L_19);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_19, _stringLiteral3783, 3);
		Dictionary_2_t1_92 * L_20 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__switchU24map2A_52 = L_20;
	}

IL_00af:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		Dictionary_2_t1_92 * L_21 = ((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__switchU24map2A_52;
		String_t* L_22 = V_3;
		NullCheck(L_21);
		bool L_23 = (bool)VirtFuncInvoker2< bool, String_t*, int32_t* >::Invoke(21 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int32>::TryGetValue(!0,!1&) */, L_21, L_22, (&V_4));
		if (!L_23)
		{
			goto IL_00f5;
		}
	}
	{
		int32_t L_24 = V_4;
		if (L_24 == 0)
		{
			goto IL_00dd;
		}
		if (L_24 == 1)
		{
			goto IL_00e3;
		}
		if (L_24 == 2)
		{
			goto IL_00e9;
		}
		if (L_24 == 3)
		{
			goto IL_00ef;
		}
	}
	{
		goto IL_00f5;
	}

IL_00dd:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		XdtAnyAtomicType_t4_5 * L_25 = ((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeAnyAtomicType_48;
		return L_25;
	}

IL_00e3:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		XdtUntypedAtomic_t4_6 * L_26 = ((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeUntypedAtomic_49;
		return L_26;
	}

IL_00e9:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		XdtDayTimeDuration_t4_43 * L_27 = ((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeDayTimeDuration_50;
		return L_27;
	}

IL_00ef:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		XdtYearMonthDuration_t4_44 * L_28 = ((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeYearMonthDuration_51;
		return L_28;
	}

IL_00f5:
	{
		return (XmlSchemaDatatype_t4_4 *)NULL;
	}

IL_00f7:
	{
		return (XmlSchemaDatatype_t4_4 *)NULL;
	}

IL_00f9:
	{
		String_t* L_29 = ___localName;
		V_0 = L_29;
		String_t* L_30 = V_0;
		if (!L_30)
		{
			goto IL_0538;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		Dictionary_2_t1_92 * L_31 = ((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__switchU24map2C_54;
		if (L_31)
		{
			goto IL_0359;
		}
	}
	{
		Dictionary_2_t1_92 * L_32 = (Dictionary_2_t1_92 *)il2cpp_codegen_object_new (Dictionary_2_t1_92_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_14878(L_32, ((int32_t)45), /*hidden argument*/Dictionary_2__ctor_m1_14878_MethodInfo_var);
		V_1 = L_32;
		Dictionary_2_t1_92 * L_33 = V_1;
		NullCheck(L_33);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_33, _stringLiteral3784, 0);
		Dictionary_2_t1_92 * L_34 = V_1;
		NullCheck(L_34);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_34, _stringLiteral309, 1);
		Dictionary_2_t1_92 * L_35 = V_1;
		NullCheck(L_35);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_35, _stringLiteral2275, 2);
		Dictionary_2_t1_92 * L_36 = V_1;
		NullCheck(L_36);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_36, _stringLiteral2302, 3);
		Dictionary_2_t1_92 * L_37 = V_1;
		NullCheck(L_37);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_37, _stringLiteral2261, 4);
		Dictionary_2_t1_92 * L_38 = V_1;
		NullCheck(L_38);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_38, _stringLiteral2271, 5);
		Dictionary_2_t1_92 * L_39 = V_1;
		NullCheck(L_39);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_39, _stringLiteral2272, 6);
		Dictionary_2_t1_92 * L_40 = V_1;
		NullCheck(L_40);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_40, _stringLiteral1969, 7);
		Dictionary_2_t1_92 * L_41 = V_1;
		NullCheck(L_41);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_41, _stringLiteral2268, 8);
		Dictionary_2_t1_92 * L_42 = V_1;
		NullCheck(L_42);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_42, _stringLiteral2257, ((int32_t)9));
		Dictionary_2_t1_92 * L_43 = V_1;
		NullCheck(L_43);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_43, _stringLiteral2258, ((int32_t)10));
		Dictionary_2_t1_92 * L_44 = V_1;
		NullCheck(L_44);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_44, _stringLiteral2259, ((int32_t)11));
		Dictionary_2_t1_92 * L_45 = V_1;
		NullCheck(L_45);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_45, _stringLiteral2253, ((int32_t)12));
		Dictionary_2_t1_92 * L_46 = V_1;
		NullCheck(L_46);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_46, _stringLiteral2252, ((int32_t)13));
		Dictionary_2_t1_92 * L_47 = V_1;
		NullCheck(L_47);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_47, _stringLiteral2276, ((int32_t)14));
		Dictionary_2_t1_92 * L_48 = V_1;
		NullCheck(L_48);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_48, _stringLiteral3785, ((int32_t)15));
		Dictionary_2_t1_92 * L_49 = V_1;
		NullCheck(L_49);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_49, _stringLiteral2260, ((int32_t)16));
		Dictionary_2_t1_92 * L_50 = V_1;
		NullCheck(L_50);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_50, _stringLiteral3786, ((int32_t)17));
		Dictionary_2_t1_92 * L_51 = V_1;
		NullCheck(L_51);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_51, _stringLiteral307, ((int32_t)18));
		Dictionary_2_t1_92 * L_52 = V_1;
		NullCheck(L_52);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_52, _stringLiteral3787, ((int32_t)19));
		Dictionary_2_t1_92 * L_53 = V_1;
		NullCheck(L_53);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_53, _stringLiteral3788, ((int32_t)20));
		Dictionary_2_t1_92 * L_54 = V_1;
		NullCheck(L_54);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_54, _stringLiteral2274, ((int32_t)21));
		Dictionary_2_t1_92 * L_55 = V_1;
		NullCheck(L_55);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_55, _stringLiteral2270, ((int32_t)22));
		Dictionary_2_t1_92 * L_56 = V_1;
		NullCheck(L_56);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_56, _stringLiteral2273, ((int32_t)23));
		Dictionary_2_t1_92 * L_57 = V_1;
		NullCheck(L_57);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_57, _stringLiteral3789, ((int32_t)24));
		Dictionary_2_t1_92 * L_58 = V_1;
		NullCheck(L_58);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_58, _stringLiteral3790, ((int32_t)25));
		Dictionary_2_t1_92 * L_59 = V_1;
		NullCheck(L_59);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_59, _stringLiteral3791, ((int32_t)26));
		Dictionary_2_t1_92 * L_60 = V_1;
		NullCheck(L_60);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_60, _stringLiteral3792, ((int32_t)27));
		Dictionary_2_t1_92 * L_61 = V_1;
		NullCheck(L_61);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_61, _stringLiteral2277, ((int32_t)28));
		Dictionary_2_t1_92 * L_62 = V_1;
		NullCheck(L_62);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_62, _stringLiteral3793, ((int32_t)29));
		Dictionary_2_t1_92 * L_63 = V_1;
		NullCheck(L_63);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_63, _stringLiteral3794, ((int32_t)30));
		Dictionary_2_t1_92 * L_64 = V_1;
		NullCheck(L_64);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_64, _stringLiteral2214, ((int32_t)31));
		Dictionary_2_t1_92 * L_65 = V_1;
		NullCheck(L_65);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_65, _stringLiteral3795, ((int32_t)32));
		Dictionary_2_t1_92 * L_66 = V_1;
		NullCheck(L_66);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_66, _stringLiteral3796, ((int32_t)33));
		Dictionary_2_t1_92 * L_67 = V_1;
		NullCheck(L_67);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_67, _stringLiteral2249, ((int32_t)34));
		Dictionary_2_t1_92 * L_68 = V_1;
		NullCheck(L_68);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_68, _stringLiteral2245, ((int32_t)35));
		Dictionary_2_t1_92 * L_69 = V_1;
		NullCheck(L_69);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_69, _stringLiteral954, ((int32_t)36));
		Dictionary_2_t1_92 * L_70 = V_1;
		NullCheck(L_70);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_70, _stringLiteral673, ((int32_t)37));
		Dictionary_2_t1_92 * L_71 = V_1;
		NullCheck(L_71);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_71, _stringLiteral2255, ((int32_t)38));
		Dictionary_2_t1_92 * L_72 = V_1;
		NullCheck(L_72);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_72, _stringLiteral2278, ((int32_t)39));
		Dictionary_2_t1_92 * L_73 = V_1;
		NullCheck(L_73);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_73, _stringLiteral2316, ((int32_t)40));
		Dictionary_2_t1_92 * L_74 = V_1;
		NullCheck(L_74);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_74, _stringLiteral2267, ((int32_t)41));
		Dictionary_2_t1_92 * L_75 = V_1;
		NullCheck(L_75);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_75, _stringLiteral2309, ((int32_t)42));
		Dictionary_2_t1_92 * L_76 = V_1;
		NullCheck(L_76);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_76, _stringLiteral2264, ((int32_t)43));
		Dictionary_2_t1_92 * L_77 = V_1;
		NullCheck(L_77);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_77, _stringLiteral2248, ((int32_t)44));
		Dictionary_2_t1_92 * L_78 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__switchU24map2C_54 = L_78;
	}

IL_0359:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		Dictionary_2_t1_92 * L_79 = ((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__switchU24map2C_54;
		String_t* L_80 = V_0;
		NullCheck(L_79);
		bool L_81 = (bool)VirtFuncInvoker2< bool, String_t*, int32_t* >::Invoke(21 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int32>::TryGetValue(!0,!1&) */, L_79, L_80, (&V_2));
		if (!L_81)
		{
			goto IL_0538;
		}
	}
	{
		int32_t L_82 = V_2;
		if (L_82 == 0)
		{
			goto IL_042a;
		}
		if (L_82 == 1)
		{
			goto IL_0430;
		}
		if (L_82 == 2)
		{
			goto IL_0436;
		}
		if (L_82 == 3)
		{
			goto IL_043c;
		}
		if (L_82 == 4)
		{
			goto IL_0442;
		}
		if (L_82 == 5)
		{
			goto IL_0448;
		}
		if (L_82 == 6)
		{
			goto IL_044e;
		}
		if (L_82 == 7)
		{
			goto IL_0454;
		}
		if (L_82 == 8)
		{
			goto IL_045a;
		}
		if (L_82 == 9)
		{
			goto IL_0460;
		}
		if (L_82 == 10)
		{
			goto IL_0466;
		}
		if (L_82 == 11)
		{
			goto IL_046c;
		}
		if (L_82 == 12)
		{
			goto IL_0472;
		}
		if (L_82 == 13)
		{
			goto IL_0478;
		}
		if (L_82 == 14)
		{
			goto IL_047e;
		}
		if (L_82 == 15)
		{
			goto IL_0484;
		}
		if (L_82 == 16)
		{
			goto IL_048a;
		}
		if (L_82 == 17)
		{
			goto IL_0490;
		}
		if (L_82 == 18)
		{
			goto IL_0496;
		}
		if (L_82 == 19)
		{
			goto IL_049c;
		}
		if (L_82 == 20)
		{
			goto IL_04a2;
		}
		if (L_82 == 21)
		{
			goto IL_04a8;
		}
		if (L_82 == 22)
		{
			goto IL_04ae;
		}
		if (L_82 == 23)
		{
			goto IL_04b4;
		}
		if (L_82 == 24)
		{
			goto IL_04ba;
		}
		if (L_82 == 25)
		{
			goto IL_04c0;
		}
		if (L_82 == 26)
		{
			goto IL_04c6;
		}
		if (L_82 == 27)
		{
			goto IL_04cc;
		}
		if (L_82 == 28)
		{
			goto IL_04d2;
		}
		if (L_82 == 29)
		{
			goto IL_04d8;
		}
		if (L_82 == 30)
		{
			goto IL_04de;
		}
		if (L_82 == 31)
		{
			goto IL_04e4;
		}
		if (L_82 == 32)
		{
			goto IL_04ea;
		}
		if (L_82 == 33)
		{
			goto IL_04f0;
		}
		if (L_82 == 34)
		{
			goto IL_04f6;
		}
		if (L_82 == 35)
		{
			goto IL_04fc;
		}
		if (L_82 == 36)
		{
			goto IL_0502;
		}
		if (L_82 == 37)
		{
			goto IL_0508;
		}
		if (L_82 == 38)
		{
			goto IL_050e;
		}
		if (L_82 == 39)
		{
			goto IL_0514;
		}
		if (L_82 == 40)
		{
			goto IL_051a;
		}
		if (L_82 == 41)
		{
			goto IL_0520;
		}
		if (L_82 == 42)
		{
			goto IL_0526;
		}
		if (L_82 == 43)
		{
			goto IL_052c;
		}
		if (L_82 == 44)
		{
			goto IL_0532;
		}
	}
	{
		goto IL_0538;
	}

IL_042a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		XsdAnySimpleType_t4_3 * L_83 = ((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeAnySimpleType_3;
		return L_83;
	}

IL_0430:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		XsdString_t4_7 * L_84 = ((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeString_4;
		return L_84;
	}

IL_0436:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		XsdNormalizedString_t4_8 * L_85 = ((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeNormalizedString_5;
		return L_85;
	}

IL_043c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		XsdToken_t4_9 * L_86 = ((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeToken_6;
		return L_86;
	}

IL_0442:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		XsdLanguage_t4_10 * L_87 = ((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeLanguage_7;
		return L_87;
	}

IL_0448:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		XsdNMToken_t4_11 * L_88 = ((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeNMToken_8;
		return L_88;
	}

IL_044e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		XsdNMTokens_t4_12 * L_89 = ((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeNMTokens_9;
		return L_89;
	}

IL_0454:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		XsdName_t4_13 * L_90 = ((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeName_10;
		return L_90;
	}

IL_045a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		XsdNCName_t4_14 * L_91 = ((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeNCName_11;
		return L_91;
	}

IL_0460:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		XsdID_t4_15 * L_92 = ((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeID_12;
		return L_92;
	}

IL_0466:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		XsdIDRef_t4_16 * L_93 = ((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeIDRef_13;
		return L_93;
	}

IL_046c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		XsdIDRefs_t4_17 * L_94 = ((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeIDRefs_14;
		return L_94;
	}

IL_0472:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		XsdEntity_t4_18 * L_95 = ((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeEntity_15;
		return L_95;
	}

IL_0478:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		XsdEntities_t4_19 * L_96 = ((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeEntities_16;
		return L_96;
	}

IL_047e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		XsdNotation_t4_20 * L_97 = ((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeNotation_17;
		return L_97;
	}

IL_0484:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		XsdDecimal_t4_21 * L_98 = ((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeDecimal_18;
		return L_98;
	}

IL_048a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		XsdInteger_t4_22 * L_99 = ((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeInteger_19;
		return L_99;
	}

IL_0490:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		XsdLong_t4_23 * L_100 = ((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeLong_20;
		return L_100;
	}

IL_0496:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		XsdInt_t4_24 * L_101 = ((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeInt_21;
		return L_101;
	}

IL_049c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		XsdShort_t4_25 * L_102 = ((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeShort_22;
		return L_102;
	}

IL_04a2:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		XsdByte_t4_26 * L_103 = ((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeByte_23;
		return L_103;
	}

IL_04a8:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		XsdNonPositiveInteger_t4_33 * L_104 = ((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeNonPositiveInteger_30;
		return L_104;
	}

IL_04ae:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		XsdNegativeInteger_t4_34 * L_105 = ((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeNegativeInteger_31;
		return L_105;
	}

IL_04b4:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		XsdNonNegativeInteger_t4_27 * L_106 = ((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeNonNegativeInteger_24;
		return L_106;
	}

IL_04ba:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		XsdUnsignedLong_t4_28 * L_107 = ((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeUnsignedLong_26;
		return L_107;
	}

IL_04c0:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		XsdUnsignedInt_t4_29 * L_108 = ((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeUnsignedInt_27;
		return L_108;
	}

IL_04c6:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		XsdUnsignedShort_t4_30 * L_109 = ((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeUnsignedShort_28;
		return L_109;
	}

IL_04cc:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		XsdUnsignedByte_t4_31 * L_110 = ((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeUnsignedByte_29;
		return L_110;
	}

IL_04d2:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		XsdPositiveInteger_t4_32 * L_111 = ((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypePositiveInteger_25;
		return L_111;
	}

IL_04d8:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		XsdFloat_t4_35 * L_112 = ((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeFloat_32;
		return L_112;
	}

IL_04de:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		XsdDouble_t4_36 * L_113 = ((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeDouble_33;
		return L_113;
	}

IL_04e4:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		XsdBase64Binary_t4_37 * L_114 = ((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeBase64Binary_34;
		return L_114;
	}

IL_04ea:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		XsdBoolean_t4_40 * L_115 = ((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeBoolean_35;
		return L_115;
	}

IL_04f0:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		XsdAnyURI_t4_41 * L_116 = ((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeAnyURI_36;
		return L_116;
	}

IL_04f6:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		XsdDuration_t4_42 * L_117 = ((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeDuration_37;
		return L_117;
	}

IL_04fc:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		XsdDateTime_t4_45 * L_118 = ((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeDateTime_38;
		return L_118;
	}

IL_0502:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		XsdDate_t4_46 * L_119 = ((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeDate_39;
		return L_119;
	}

IL_0508:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		XsdTime_t4_47 * L_120 = ((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeTime_40;
		return L_120;
	}

IL_050e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		XsdHexBinary_t4_38 * L_121 = ((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeHexBinary_41;
		return L_121;
	}

IL_0514:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		XsdQName_t4_39 * L_122 = ((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeQName_42;
		return L_122;
	}

IL_051a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		XsdGYearMonth_t4_48 * L_123 = ((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeGYearMonth_43;
		return L_123;
	}

IL_0520:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		XsdGMonthDay_t4_49 * L_124 = ((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeGMonthDay_44;
		return L_124;
	}

IL_0526:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		XsdGYear_t4_50 * L_125 = ((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeGYear_45;
		return L_125;
	}

IL_052c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		XsdGMonth_t4_51 * L_126 = ((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeGMonth_46;
		return L_126;
	}

IL_0532:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		XsdGDay_t4_52 * L_127 = ((XmlSchemaDatatype_t4_4_StaticFields*)XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var->static_fields)->___datatypeGDay_47;
		return L_127;
	}

IL_0538:
	{
		return (XmlSchemaDatatype_t4_4 *)NULL;
	}
}
// System.Void System.Xml.Schema.XmlSchemaFacet::.cctor()
extern TypeInfo* XmlSchemaFacet_t4_61_il2cpp_TypeInfo_var;
extern "C" void XmlSchemaFacet__cctor_m4_84 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		XmlSchemaFacet_t4_61_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1267);
		s_Il2CppMethodIntialized = true;
	}
	{
		((XmlSchemaFacet_t4_61_StaticFields*)XmlSchemaFacet_t4_61_il2cpp_TypeInfo_var->static_fields)->___AllFacets_3 = ((int32_t)4095);
		return;
	}
}
// System.Void System.Xml.Schema.XmlSchemaInfo::.ctor(System.Xml.Schema.IXmlSchemaInfo)
extern TypeInfo* IXmlSchemaInfo_t4_119_il2cpp_TypeInfo_var;
extern "C" void XmlSchemaInfo__ctor_m4_85 (XmlSchemaInfo_t4_62 * __this, Object_t * ___info, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IXmlSchemaInfo_t4_119_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1268);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		Object_t * L_0 = ___info;
		NullCheck(L_0);
		bool L_1 = (bool)InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Xml.Schema.IXmlSchemaInfo::get_IsDefault() */, IXmlSchemaInfo_t4_119_il2cpp_TypeInfo_var, L_0);
		__this->___isDefault_0 = L_1;
		Object_t * L_2 = ___info;
		NullCheck(L_2);
		bool L_3 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Xml.Schema.IXmlSchemaInfo::get_IsNil() */, IXmlSchemaInfo_t4_119_il2cpp_TypeInfo_var, L_2);
		__this->___isNil_1 = L_3;
		Object_t * L_4 = ___info;
		NullCheck(L_4);
		XmlSchemaSimpleType_t4_63 * L_5 = (XmlSchemaSimpleType_t4_63 *)InterfaceFuncInvoker0< XmlSchemaSimpleType_t4_63 * >::Invoke(2 /* System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.IXmlSchemaInfo::get_MemberType() */, IXmlSchemaInfo_t4_119_il2cpp_TypeInfo_var, L_4);
		__this->___memberType_2 = L_5;
		Object_t * L_6 = ___info;
		NullCheck(L_6);
		XmlSchemaAttribute_t4_55 * L_7 = (XmlSchemaAttribute_t4_55 *)InterfaceFuncInvoker0< XmlSchemaAttribute_t4_55 * >::Invoke(3 /* System.Xml.Schema.XmlSchemaAttribute System.Xml.Schema.IXmlSchemaInfo::get_SchemaAttribute() */, IXmlSchemaInfo_t4_119_il2cpp_TypeInfo_var, L_6);
		__this->___attr_3 = L_7;
		Object_t * L_8 = ___info;
		NullCheck(L_8);
		XmlSchemaElement_t4_58 * L_9 = (XmlSchemaElement_t4_58 *)InterfaceFuncInvoker0< XmlSchemaElement_t4_58 * >::Invoke(4 /* System.Xml.Schema.XmlSchemaElement System.Xml.Schema.IXmlSchemaInfo::get_SchemaElement() */, IXmlSchemaInfo_t4_119_il2cpp_TypeInfo_var, L_8);
		__this->___elem_4 = L_9;
		Object_t * L_10 = ___info;
		NullCheck(L_10);
		XmlSchemaType_t4_64 * L_11 = (XmlSchemaType_t4_64 *)InterfaceFuncInvoker0< XmlSchemaType_t4_64 * >::Invoke(5 /* System.Xml.Schema.XmlSchemaType System.Xml.Schema.IXmlSchemaInfo::get_SchemaType() */, IXmlSchemaInfo_t4_119_il2cpp_TypeInfo_var, L_10);
		__this->___type_5 = L_11;
		Object_t * L_12 = ___info;
		NullCheck(L_12);
		int32_t L_13 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(6 /* System.Xml.Schema.XmlSchemaValidity System.Xml.Schema.IXmlSchemaInfo::get_Validity() */, IXmlSchemaInfo_t4_119_il2cpp_TypeInfo_var, L_12);
		__this->___validity_6 = L_13;
		return;
	}
}
// System.Boolean System.Xml.Schema.XmlSchemaInfo::get_IsDefault()
extern "C" bool XmlSchemaInfo_get_IsDefault_m4_86 (XmlSchemaInfo_t4_62 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___isDefault_0);
		return L_0;
	}
}
// System.Boolean System.Xml.Schema.XmlSchemaInfo::get_IsNil()
extern "C" bool XmlSchemaInfo_get_IsNil_m4_87 (XmlSchemaInfo_t4_62 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___isNil_1);
		return L_0;
	}
}
// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaInfo::get_MemberType()
extern "C" XmlSchemaSimpleType_t4_63 * XmlSchemaInfo_get_MemberType_m4_88 (XmlSchemaInfo_t4_62 * __this, const MethodInfo* method)
{
	{
		XmlSchemaSimpleType_t4_63 * L_0 = (__this->___memberType_2);
		return L_0;
	}
}
// System.Xml.Schema.XmlSchemaAttribute System.Xml.Schema.XmlSchemaInfo::get_SchemaAttribute()
extern "C" XmlSchemaAttribute_t4_55 * XmlSchemaInfo_get_SchemaAttribute_m4_89 (XmlSchemaInfo_t4_62 * __this, const MethodInfo* method)
{
	{
		XmlSchemaAttribute_t4_55 * L_0 = (__this->___attr_3);
		return L_0;
	}
}
// System.Xml.Schema.XmlSchemaElement System.Xml.Schema.XmlSchemaInfo::get_SchemaElement()
extern "C" XmlSchemaElement_t4_58 * XmlSchemaInfo_get_SchemaElement_m4_90 (XmlSchemaInfo_t4_62 * __this, const MethodInfo* method)
{
	{
		XmlSchemaElement_t4_58 * L_0 = (__this->___elem_4);
		return L_0;
	}
}
// System.Xml.Schema.XmlSchemaType System.Xml.Schema.XmlSchemaInfo::get_SchemaType()
extern "C" XmlSchemaType_t4_64 * XmlSchemaInfo_get_SchemaType_m4_91 (XmlSchemaInfo_t4_62 * __this, const MethodInfo* method)
{
	{
		XmlSchemaType_t4_64 * L_0 = (__this->___type_5);
		return L_0;
	}
}
// System.Xml.Schema.XmlSchemaValidity System.Xml.Schema.XmlSchemaInfo::get_Validity()
extern "C" int32_t XmlSchemaInfo_get_Validity_m4_92 (XmlSchemaInfo_t4_62 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___validity_6);
		return L_0;
	}
}
// System.Void System.Xml.Schema.XmlSchemaObject::.ctor()
extern TypeInfo* XmlSerializerNamespaces_t4_65_il2cpp_TypeInfo_var;
extern TypeInfo* Guid_t1_319_il2cpp_TypeInfo_var;
extern "C" void XmlSchemaObject__ctor_m4_93 (XmlSchemaObject_t4_54 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		XmlSerializerNamespaces_t4_65_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1269);
		Guid_t1_319_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(208);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		XmlSerializerNamespaces_t4_65 * L_0 = (XmlSerializerNamespaces_t4_65 *)il2cpp_codegen_object_new (XmlSerializerNamespaces_t4_65_il2cpp_TypeInfo_var);
		XmlSerializerNamespaces__ctor_m4_116(L_0, /*hidden argument*/NULL);
		__this->___namespaces_0 = L_0;
		__this->___unhandledAttributeList_1 = (ArrayList_t1_170 *)NULL;
		IL2CPP_RUNTIME_CLASS_INIT(Guid_t1_319_il2cpp_TypeInfo_var);
		Guid_t1_319  L_1 = ((Guid_t1_319_StaticFields*)Guid_t1_319_il2cpp_TypeInfo_var->static_fields)->___Empty_11;
		__this->___CompilationId_2 = L_1;
		return;
	}
}
// System.Void System.Xml.Schema.XmlSchemaSet::.ctor()
extern TypeInfo* NameTable_t4_108_il2cpp_TypeInfo_var;
extern "C" void XmlSchemaSet__ctor_m4_94 (XmlSchemaSet_t4_66 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NameTable_t4_108_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1270);
		s_Il2CppMethodIntialized = true;
	}
	{
		NameTable_t4_108 * L_0 = (NameTable_t4_108 *)il2cpp_codegen_object_new (NameTable_t4_108_il2cpp_TypeInfo_var);
		NameTable__ctor_m4_308(L_0, /*hidden argument*/NULL);
		XmlSchemaSet__ctor_m4_95(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.Schema.XmlSchemaSet::.ctor(System.Xml.XmlNameTable)
extern TypeInfo* XmlUrlResolver_t4_185_il2cpp_TypeInfo_var;
extern TypeInfo* XmlSchemaCompilationSettings_t4_56_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* ArrayList_t1_170_il2cpp_TypeInfo_var;
extern TypeInfo* Guid_t1_319_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3797;
extern "C" void XmlSchemaSet__ctor_m4_95 (XmlSchemaSet_t4_66 * __this, XmlNameTable_t4_67 * ___nameTable, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		XmlUrlResolver_t4_185_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1271);
		XmlSchemaCompilationSettings_t4_56_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1272);
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		ArrayList_t1_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(60);
		Guid_t1_319_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(208);
		_stringLiteral3797 = il2cpp_codegen_string_literal_from_index(3797);
		s_Il2CppMethodIntialized = true;
	}
	{
		XmlUrlResolver_t4_185 * L_0 = (XmlUrlResolver_t4_185 *)il2cpp_codegen_object_new (XmlUrlResolver_t4_185_il2cpp_TypeInfo_var);
		XmlUrlResolver__ctor_m4_1006(L_0, /*hidden argument*/NULL);
		__this->___xmlResolver_1 = L_0;
		XmlSchemaCompilationSettings_t4_56 * L_1 = (XmlSchemaCompilationSettings_t4_56 *)il2cpp_codegen_object_new (XmlSchemaCompilationSettings_t4_56_il2cpp_TypeInfo_var);
		XmlSchemaCompilationSettings__ctor_m4_79(L_1, /*hidden argument*/NULL);
		__this->___settings_3 = L_1;
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		XmlNameTable_t4_67 * L_2 = ___nameTable;
		if (L_2)
		{
			goto IL_002d;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_3 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_3, _stringLiteral3797, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_002d:
	{
		XmlNameTable_t4_67 * L_4 = ___nameTable;
		__this->___nameTable_0 = L_4;
		ArrayList_t1_170 * L_5 = (ArrayList_t1_170 *)il2cpp_codegen_object_new (ArrayList_t1_170_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1_3049(L_5, /*hidden argument*/NULL);
		__this->___schemas_2 = L_5;
		IL2CPP_RUNTIME_CLASS_INIT(Guid_t1_319_il2cpp_TypeInfo_var);
		Guid_t1_319  L_6 = Guid_NewGuid_m1_14146(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___CompilationId_4 = L_6;
		return;
	}
}
// System.Void System.Xml.Schema.XmlSchemaSimpleType::.ctor()
extern "C" void XmlSchemaSimpleType__ctor_m4_96 (XmlSchemaSimpleType_t4_63 * __this, const MethodInfo* method)
{
	{
		__this->___islocal_11 = 1;
		XmlSchemaType__ctor_m4_106(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.Schema.XmlSchemaSimpleType::.cctor()
extern TypeInfo* XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var;
extern TypeInfo* XmlSchemaSimpleTypeList_t4_70_il2cpp_TypeInfo_var;
extern TypeInfo* XmlQualifiedName_t4_71_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3796;
extern Il2CppCodeGenString* _stringLiteral3778;
extern Il2CppCodeGenString* _stringLiteral3784;
extern Il2CppCodeGenString* _stringLiteral309;
extern Il2CppCodeGenString* _stringLiteral3795;
extern Il2CppCodeGenString* _stringLiteral3785;
extern Il2CppCodeGenString* _stringLiteral3793;
extern Il2CppCodeGenString* _stringLiteral3794;
extern Il2CppCodeGenString* _stringLiteral2249;
extern Il2CppCodeGenString* _stringLiteral2245;
extern Il2CppCodeGenString* _stringLiteral673;
extern Il2CppCodeGenString* _stringLiteral954;
extern Il2CppCodeGenString* _stringLiteral2316;
extern Il2CppCodeGenString* _stringLiteral2309;
extern Il2CppCodeGenString* _stringLiteral2267;
extern Il2CppCodeGenString* _stringLiteral2248;
extern Il2CppCodeGenString* _stringLiteral2264;
extern Il2CppCodeGenString* _stringLiteral2255;
extern Il2CppCodeGenString* _stringLiteral2214;
extern Il2CppCodeGenString* _stringLiteral2278;
extern Il2CppCodeGenString* _stringLiteral2276;
extern Il2CppCodeGenString* _stringLiteral2275;
extern Il2CppCodeGenString* _stringLiteral2302;
extern Il2CppCodeGenString* _stringLiteral2261;
extern Il2CppCodeGenString* _stringLiteral2271;
extern Il2CppCodeGenString* _stringLiteral1969;
extern Il2CppCodeGenString* _stringLiteral2268;
extern Il2CppCodeGenString* _stringLiteral2257;
extern Il2CppCodeGenString* _stringLiteral2258;
extern Il2CppCodeGenString* _stringLiteral2253;
extern Il2CppCodeGenString* _stringLiteral2260;
extern Il2CppCodeGenString* _stringLiteral2274;
extern Il2CppCodeGenString* _stringLiteral2270;
extern Il2CppCodeGenString* _stringLiteral3786;
extern Il2CppCodeGenString* _stringLiteral307;
extern Il2CppCodeGenString* _stringLiteral3787;
extern Il2CppCodeGenString* _stringLiteral3788;
extern Il2CppCodeGenString* _stringLiteral2273;
extern Il2CppCodeGenString* _stringLiteral3789;
extern Il2CppCodeGenString* _stringLiteral3790;
extern Il2CppCodeGenString* _stringLiteral3791;
extern Il2CppCodeGenString* _stringLiteral3792;
extern Il2CppCodeGenString* _stringLiteral2277;
extern Il2CppCodeGenString* _stringLiteral3780;
extern Il2CppCodeGenString* _stringLiteral3781;
extern Il2CppCodeGenString* _stringLiteral3782;
extern Il2CppCodeGenString* _stringLiteral3783;
extern "C" void XmlSchemaSimpleType__cctor_m4_97 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		XmlSchemaSimpleTypeList_t4_70_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1274);
		XmlQualifiedName_t4_71_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1275);
		_stringLiteral3796 = il2cpp_codegen_string_literal_from_index(3796);
		_stringLiteral3778 = il2cpp_codegen_string_literal_from_index(3778);
		_stringLiteral3784 = il2cpp_codegen_string_literal_from_index(3784);
		_stringLiteral309 = il2cpp_codegen_string_literal_from_index(309);
		_stringLiteral3795 = il2cpp_codegen_string_literal_from_index(3795);
		_stringLiteral3785 = il2cpp_codegen_string_literal_from_index(3785);
		_stringLiteral3793 = il2cpp_codegen_string_literal_from_index(3793);
		_stringLiteral3794 = il2cpp_codegen_string_literal_from_index(3794);
		_stringLiteral2249 = il2cpp_codegen_string_literal_from_index(2249);
		_stringLiteral2245 = il2cpp_codegen_string_literal_from_index(2245);
		_stringLiteral673 = il2cpp_codegen_string_literal_from_index(673);
		_stringLiteral954 = il2cpp_codegen_string_literal_from_index(954);
		_stringLiteral2316 = il2cpp_codegen_string_literal_from_index(2316);
		_stringLiteral2309 = il2cpp_codegen_string_literal_from_index(2309);
		_stringLiteral2267 = il2cpp_codegen_string_literal_from_index(2267);
		_stringLiteral2248 = il2cpp_codegen_string_literal_from_index(2248);
		_stringLiteral2264 = il2cpp_codegen_string_literal_from_index(2264);
		_stringLiteral2255 = il2cpp_codegen_string_literal_from_index(2255);
		_stringLiteral2214 = il2cpp_codegen_string_literal_from_index(2214);
		_stringLiteral2278 = il2cpp_codegen_string_literal_from_index(2278);
		_stringLiteral2276 = il2cpp_codegen_string_literal_from_index(2276);
		_stringLiteral2275 = il2cpp_codegen_string_literal_from_index(2275);
		_stringLiteral2302 = il2cpp_codegen_string_literal_from_index(2302);
		_stringLiteral2261 = il2cpp_codegen_string_literal_from_index(2261);
		_stringLiteral2271 = il2cpp_codegen_string_literal_from_index(2271);
		_stringLiteral1969 = il2cpp_codegen_string_literal_from_index(1969);
		_stringLiteral2268 = il2cpp_codegen_string_literal_from_index(2268);
		_stringLiteral2257 = il2cpp_codegen_string_literal_from_index(2257);
		_stringLiteral2258 = il2cpp_codegen_string_literal_from_index(2258);
		_stringLiteral2253 = il2cpp_codegen_string_literal_from_index(2253);
		_stringLiteral2260 = il2cpp_codegen_string_literal_from_index(2260);
		_stringLiteral2274 = il2cpp_codegen_string_literal_from_index(2274);
		_stringLiteral2270 = il2cpp_codegen_string_literal_from_index(2270);
		_stringLiteral3786 = il2cpp_codegen_string_literal_from_index(3786);
		_stringLiteral307 = il2cpp_codegen_string_literal_from_index(307);
		_stringLiteral3787 = il2cpp_codegen_string_literal_from_index(3787);
		_stringLiteral3788 = il2cpp_codegen_string_literal_from_index(3788);
		_stringLiteral2273 = il2cpp_codegen_string_literal_from_index(2273);
		_stringLiteral3789 = il2cpp_codegen_string_literal_from_index(3789);
		_stringLiteral3790 = il2cpp_codegen_string_literal_from_index(3790);
		_stringLiteral3791 = il2cpp_codegen_string_literal_from_index(3791);
		_stringLiteral3792 = il2cpp_codegen_string_literal_from_index(3792);
		_stringLiteral2277 = il2cpp_codegen_string_literal_from_index(2277);
		_stringLiteral3780 = il2cpp_codegen_string_literal_from_index(3780);
		_stringLiteral3781 = il2cpp_codegen_string_literal_from_index(3781);
		_stringLiteral3782 = il2cpp_codegen_string_literal_from_index(3782);
		_stringLiteral3783 = il2cpp_codegen_string_literal_from_index(3783);
		s_Il2CppMethodIntialized = true;
	}
	XmlSchemaSimpleType_t4_63 * V_0 = {0};
	XmlSchemaSimpleTypeList_t4_70 * V_1 = {0};
	XmlSchemaSimpleTypeList_t4_70 * V_2 = {0};
	{
		XmlSchemaSimpleType_t4_63 * L_0 = (XmlSchemaSimpleType_t4_63 *)il2cpp_codegen_object_new (XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var);
		XmlSchemaSimpleType__ctor_m4_96(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		XmlSchemaSimpleTypeList_t4_70 * L_1 = (XmlSchemaSimpleTypeList_t4_70 *)il2cpp_codegen_object_new (XmlSchemaSimpleTypeList_t4_70_il2cpp_TypeInfo_var);
		XmlSchemaSimpleTypeList__ctor_m4_102(L_1, /*hidden argument*/NULL);
		V_1 = L_1;
		XmlSchemaSimpleTypeList_t4_70 * L_2 = V_1;
		XmlQualifiedName_t4_71 * L_3 = (XmlQualifiedName_t4_71 *)il2cpp_codegen_object_new (XmlQualifiedName_t4_71_il2cpp_TypeInfo_var);
		XmlQualifiedName__ctor_m4_730(L_3, _stringLiteral3796, _stringLiteral3778, /*hidden argument*/NULL);
		NullCheck(L_2);
		XmlSchemaSimpleTypeList_set_ItemTypeName_m4_103(L_2, L_3, /*hidden argument*/NULL);
		XmlSchemaSimpleType_t4_63 * L_4 = V_0;
		XmlSchemaSimpleTypeList_t4_70 * L_5 = V_1;
		NullCheck(L_4);
		XmlSchemaSimpleType_set_Content_m4_100(L_4, L_5, /*hidden argument*/NULL);
		XmlSchemaSimpleType_t4_63 * L_6 = V_0;
		NullCheck(L_6);
		((XmlSchemaType_t4_64 *)L_6)->___BaseXmlSchemaTypeInternal_4 = (XmlSchemaType_t4_64 *)NULL;
		XmlSchemaSimpleType_t4_63 * L_7 = V_0;
		NullCheck(L_7);
		L_7->___variety_12 = 8;
		XmlSchemaSimpleType_t4_63 * L_8 = V_0;
		((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___schemaLocationType_9 = L_8;
		XmlSchemaSimpleType_t4_63 * L_9 = XmlSchemaSimpleType_BuildSchemaType_m4_98(NULL /*static, unused*/, _stringLiteral3784, (String_t*)NULL, /*hidden argument*/NULL);
		((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsAnySimpleType_13 = L_9;
		XmlSchemaSimpleType_t4_63 * L_10 = XmlSchemaSimpleType_BuildSchemaType_m4_98(NULL /*static, unused*/, _stringLiteral309, _stringLiteral3784, /*hidden argument*/NULL);
		((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsString_14 = L_10;
		XmlSchemaSimpleType_t4_63 * L_11 = XmlSchemaSimpleType_BuildSchemaType_m4_98(NULL /*static, unused*/, _stringLiteral3795, _stringLiteral3784, /*hidden argument*/NULL);
		((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsBoolean_15 = L_11;
		XmlSchemaSimpleType_t4_63 * L_12 = XmlSchemaSimpleType_BuildSchemaType_m4_98(NULL /*static, unused*/, _stringLiteral3785, _stringLiteral3784, /*hidden argument*/NULL);
		((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsDecimal_16 = L_12;
		XmlSchemaSimpleType_t4_63 * L_13 = XmlSchemaSimpleType_BuildSchemaType_m4_98(NULL /*static, unused*/, _stringLiteral3793, _stringLiteral3784, /*hidden argument*/NULL);
		((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsFloat_17 = L_13;
		XmlSchemaSimpleType_t4_63 * L_14 = XmlSchemaSimpleType_BuildSchemaType_m4_98(NULL /*static, unused*/, _stringLiteral3794, _stringLiteral3784, /*hidden argument*/NULL);
		((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsDouble_18 = L_14;
		XmlSchemaSimpleType_t4_63 * L_15 = XmlSchemaSimpleType_BuildSchemaType_m4_98(NULL /*static, unused*/, _stringLiteral2249, _stringLiteral3784, /*hidden argument*/NULL);
		((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsDuration_19 = L_15;
		XmlSchemaSimpleType_t4_63 * L_16 = XmlSchemaSimpleType_BuildSchemaType_m4_98(NULL /*static, unused*/, _stringLiteral2245, _stringLiteral3784, /*hidden argument*/NULL);
		((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsDateTime_20 = L_16;
		XmlSchemaSimpleType_t4_63 * L_17 = XmlSchemaSimpleType_BuildSchemaType_m4_98(NULL /*static, unused*/, _stringLiteral673, _stringLiteral3784, /*hidden argument*/NULL);
		((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsTime_21 = L_17;
		XmlSchemaSimpleType_t4_63 * L_18 = XmlSchemaSimpleType_BuildSchemaType_m4_98(NULL /*static, unused*/, _stringLiteral954, _stringLiteral3784, /*hidden argument*/NULL);
		((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsDate_22 = L_18;
		XmlSchemaSimpleType_t4_63 * L_19 = XmlSchemaSimpleType_BuildSchemaType_m4_98(NULL /*static, unused*/, _stringLiteral2316, _stringLiteral3784, /*hidden argument*/NULL);
		((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsGYearMonth_23 = L_19;
		XmlSchemaSimpleType_t4_63 * L_20 = XmlSchemaSimpleType_BuildSchemaType_m4_98(NULL /*static, unused*/, _stringLiteral2309, _stringLiteral3784, /*hidden argument*/NULL);
		((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsGYear_24 = L_20;
		XmlSchemaSimpleType_t4_63 * L_21 = XmlSchemaSimpleType_BuildSchemaType_m4_98(NULL /*static, unused*/, _stringLiteral2267, _stringLiteral3784, /*hidden argument*/NULL);
		((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsGMonthDay_25 = L_21;
		XmlSchemaSimpleType_t4_63 * L_22 = XmlSchemaSimpleType_BuildSchemaType_m4_98(NULL /*static, unused*/, _stringLiteral2248, _stringLiteral3784, /*hidden argument*/NULL);
		((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsGDay_26 = L_22;
		XmlSchemaSimpleType_t4_63 * L_23 = XmlSchemaSimpleType_BuildSchemaType_m4_98(NULL /*static, unused*/, _stringLiteral2264, _stringLiteral3784, /*hidden argument*/NULL);
		((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsGMonth_27 = L_23;
		XmlSchemaSimpleType_t4_63 * L_24 = XmlSchemaSimpleType_BuildSchemaType_m4_98(NULL /*static, unused*/, _stringLiteral2255, _stringLiteral3784, /*hidden argument*/NULL);
		((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsHexBinary_28 = L_24;
		XmlSchemaSimpleType_t4_63 * L_25 = XmlSchemaSimpleType_BuildSchemaType_m4_98(NULL /*static, unused*/, _stringLiteral2214, _stringLiteral3784, /*hidden argument*/NULL);
		((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsBase64Binary_29 = L_25;
		XmlSchemaSimpleType_t4_63 * L_26 = XmlSchemaSimpleType_BuildSchemaType_m4_98(NULL /*static, unused*/, _stringLiteral3796, _stringLiteral3784, /*hidden argument*/NULL);
		((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsAnyUri_30 = L_26;
		XmlSchemaSimpleType_t4_63 * L_27 = XmlSchemaSimpleType_BuildSchemaType_m4_98(NULL /*static, unused*/, _stringLiteral2278, _stringLiteral3784, /*hidden argument*/NULL);
		((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsQName_31 = L_27;
		XmlSchemaSimpleType_t4_63 * L_28 = XmlSchemaSimpleType_BuildSchemaType_m4_98(NULL /*static, unused*/, _stringLiteral2276, _stringLiteral3784, /*hidden argument*/NULL);
		((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsNotation_32 = L_28;
		XmlSchemaSimpleType_t4_63 * L_29 = XmlSchemaSimpleType_BuildSchemaType_m4_98(NULL /*static, unused*/, _stringLiteral2275, _stringLiteral309, /*hidden argument*/NULL);
		((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsNormalizedString_33 = L_29;
		XmlSchemaSimpleType_t4_63 * L_30 = XmlSchemaSimpleType_BuildSchemaType_m4_98(NULL /*static, unused*/, _stringLiteral2302, _stringLiteral2275, /*hidden argument*/NULL);
		((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsToken_34 = L_30;
		XmlSchemaSimpleType_t4_63 * L_31 = XmlSchemaSimpleType_BuildSchemaType_m4_98(NULL /*static, unused*/, _stringLiteral2261, _stringLiteral2302, /*hidden argument*/NULL);
		((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsLanguage_35 = L_31;
		XmlSchemaSimpleType_t4_63 * L_32 = XmlSchemaSimpleType_BuildSchemaType_m4_98(NULL /*static, unused*/, _stringLiteral2271, _stringLiteral2302, /*hidden argument*/NULL);
		((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsNMToken_36 = L_32;
		XmlSchemaSimpleType_t4_63 * L_33 = XmlSchemaSimpleType_BuildSchemaType_m4_98(NULL /*static, unused*/, _stringLiteral1969, _stringLiteral2302, /*hidden argument*/NULL);
		((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsName_38 = L_33;
		XmlSchemaSimpleType_t4_63 * L_34 = XmlSchemaSimpleType_BuildSchemaType_m4_98(NULL /*static, unused*/, _stringLiteral2268, _stringLiteral1969, /*hidden argument*/NULL);
		((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsNCName_39 = L_34;
		XmlSchemaSimpleType_t4_63 * L_35 = XmlSchemaSimpleType_BuildSchemaType_m4_98(NULL /*static, unused*/, _stringLiteral2257, _stringLiteral2268, /*hidden argument*/NULL);
		((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsID_40 = L_35;
		XmlSchemaSimpleType_t4_63 * L_36 = XmlSchemaSimpleType_BuildSchemaType_m4_98(NULL /*static, unused*/, _stringLiteral2258, _stringLiteral2268, /*hidden argument*/NULL);
		((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsIDRef_41 = L_36;
		XmlSchemaSimpleType_t4_63 * L_37 = XmlSchemaSimpleType_BuildSchemaType_m4_98(NULL /*static, unused*/, _stringLiteral2253, _stringLiteral2268, /*hidden argument*/NULL);
		((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsEntity_43 = L_37;
		XmlSchemaSimpleType_t4_63 * L_38 = XmlSchemaSimpleType_BuildSchemaType_m4_98(NULL /*static, unused*/, _stringLiteral2260, _stringLiteral3785, /*hidden argument*/NULL);
		((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsInteger_45 = L_38;
		XmlSchemaSimpleType_t4_63 * L_39 = XmlSchemaSimpleType_BuildSchemaType_m4_98(NULL /*static, unused*/, _stringLiteral2274, _stringLiteral2260, /*hidden argument*/NULL);
		((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsNonPositiveInteger_46 = L_39;
		XmlSchemaSimpleType_t4_63 * L_40 = XmlSchemaSimpleType_BuildSchemaType_m4_98(NULL /*static, unused*/, _stringLiteral2270, _stringLiteral2274, /*hidden argument*/NULL);
		((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsNegativeInteger_47 = L_40;
		XmlSchemaSimpleType_t4_63 * L_41 = XmlSchemaSimpleType_BuildSchemaType_m4_98(NULL /*static, unused*/, _stringLiteral3786, _stringLiteral2260, /*hidden argument*/NULL);
		((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsLong_48 = L_41;
		XmlSchemaSimpleType_t4_63 * L_42 = XmlSchemaSimpleType_BuildSchemaType_m4_98(NULL /*static, unused*/, _stringLiteral307, _stringLiteral3786, /*hidden argument*/NULL);
		((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsInt_49 = L_42;
		XmlSchemaSimpleType_t4_63 * L_43 = XmlSchemaSimpleType_BuildSchemaType_m4_98(NULL /*static, unused*/, _stringLiteral3787, _stringLiteral307, /*hidden argument*/NULL);
		((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsShort_50 = L_43;
		XmlSchemaSimpleType_t4_63 * L_44 = XmlSchemaSimpleType_BuildSchemaType_m4_98(NULL /*static, unused*/, _stringLiteral3788, _stringLiteral3787, /*hidden argument*/NULL);
		((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsByte_51 = L_44;
		XmlSchemaSimpleType_t4_63 * L_45 = XmlSchemaSimpleType_BuildSchemaType_m4_98(NULL /*static, unused*/, _stringLiteral2273, _stringLiteral2260, /*hidden argument*/NULL);
		((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsNonNegativeInteger_52 = L_45;
		XmlSchemaSimpleType_t4_63 * L_46 = XmlSchemaSimpleType_BuildSchemaType_m4_98(NULL /*static, unused*/, _stringLiteral3789, _stringLiteral2273, /*hidden argument*/NULL);
		((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsUnsignedLong_53 = L_46;
		XmlSchemaSimpleType_t4_63 * L_47 = XmlSchemaSimpleType_BuildSchemaType_m4_98(NULL /*static, unused*/, _stringLiteral3790, _stringLiteral3789, /*hidden argument*/NULL);
		((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsUnsignedInt_54 = L_47;
		XmlSchemaSimpleType_t4_63 * L_48 = XmlSchemaSimpleType_BuildSchemaType_m4_98(NULL /*static, unused*/, _stringLiteral3791, _stringLiteral3790, /*hidden argument*/NULL);
		((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsUnsignedShort_55 = L_48;
		XmlSchemaSimpleType_t4_63 * L_49 = XmlSchemaSimpleType_BuildSchemaType_m4_98(NULL /*static, unused*/, _stringLiteral3792, _stringLiteral3791, /*hidden argument*/NULL);
		((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsUnsignedByte_56 = L_49;
		XmlSchemaSimpleType_t4_63 * L_50 = XmlSchemaSimpleType_BuildSchemaType_m4_98(NULL /*static, unused*/, _stringLiteral2277, _stringLiteral2273, /*hidden argument*/NULL);
		((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsPositiveInteger_57 = L_50;
		XmlSchemaSimpleType_t4_63 * L_51 = XmlSchemaSimpleType_BuildSchemaType_m4_99(NULL /*static, unused*/, _stringLiteral3780, _stringLiteral3784, 1, 0, /*hidden argument*/NULL);
		((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XdtAnyAtomicType_59 = L_51;
		XmlSchemaSimpleType_t4_63 * L_52 = XmlSchemaSimpleType_BuildSchemaType_m4_99(NULL /*static, unused*/, _stringLiteral3781, _stringLiteral3780, 1, 1, /*hidden argument*/NULL);
		((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XdtUntypedAtomic_58 = L_52;
		XmlSchemaSimpleType_t4_63 * L_53 = XmlSchemaSimpleType_BuildSchemaType_m4_99(NULL /*static, unused*/, _stringLiteral3782, _stringLiteral2249, 1, 0, /*hidden argument*/NULL);
		((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XdtDayTimeDuration_61 = L_53;
		XmlSchemaSimpleType_t4_63 * L_54 = XmlSchemaSimpleType_BuildSchemaType_m4_99(NULL /*static, unused*/, _stringLiteral3783, _stringLiteral2249, 1, 0, /*hidden argument*/NULL);
		((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XdtYearMonthDuration_60 = L_54;
		XmlSchemaSimpleType_t4_63 * L_55 = (XmlSchemaSimpleType_t4_63 *)il2cpp_codegen_object_new (XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var);
		XmlSchemaSimpleType__ctor_m4_96(L_55, /*hidden argument*/NULL);
		((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsIDRefs_42 = L_55;
		XmlSchemaSimpleTypeList_t4_70 * L_56 = (XmlSchemaSimpleTypeList_t4_70 *)il2cpp_codegen_object_new (XmlSchemaSimpleTypeList_t4_70_il2cpp_TypeInfo_var);
		XmlSchemaSimpleTypeList__ctor_m4_102(L_56, /*hidden argument*/NULL);
		V_2 = L_56;
		XmlSchemaSimpleTypeList_t4_70 * L_57 = V_2;
		XmlSchemaSimpleType_t4_63 * L_58 = ((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsIDRef_41;
		NullCheck(L_57);
		XmlSchemaSimpleTypeList_set_ItemType_m4_104(L_57, L_58, /*hidden argument*/NULL);
		XmlSchemaSimpleType_t4_63 * L_59 = ((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsIDRefs_42;
		XmlSchemaSimpleTypeList_t4_70 * L_60 = V_2;
		NullCheck(L_59);
		XmlSchemaSimpleType_set_Content_m4_100(L_59, L_60, /*hidden argument*/NULL);
		XmlSchemaSimpleType_t4_63 * L_61 = (XmlSchemaSimpleType_t4_63 *)il2cpp_codegen_object_new (XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var);
		XmlSchemaSimpleType__ctor_m4_96(L_61, /*hidden argument*/NULL);
		((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsEntities_44 = L_61;
		XmlSchemaSimpleTypeList_t4_70 * L_62 = (XmlSchemaSimpleTypeList_t4_70 *)il2cpp_codegen_object_new (XmlSchemaSimpleTypeList_t4_70_il2cpp_TypeInfo_var);
		XmlSchemaSimpleTypeList__ctor_m4_102(L_62, /*hidden argument*/NULL);
		V_2 = L_62;
		XmlSchemaSimpleTypeList_t4_70 * L_63 = V_2;
		XmlSchemaSimpleType_t4_63 * L_64 = ((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsEntity_43;
		NullCheck(L_63);
		XmlSchemaSimpleTypeList_set_ItemType_m4_104(L_63, L_64, /*hidden argument*/NULL);
		XmlSchemaSimpleType_t4_63 * L_65 = ((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsEntities_44;
		XmlSchemaSimpleTypeList_t4_70 * L_66 = V_2;
		NullCheck(L_65);
		XmlSchemaSimpleType_set_Content_m4_100(L_65, L_66, /*hidden argument*/NULL);
		XmlSchemaSimpleType_t4_63 * L_67 = (XmlSchemaSimpleType_t4_63 *)il2cpp_codegen_object_new (XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var);
		XmlSchemaSimpleType__ctor_m4_96(L_67, /*hidden argument*/NULL);
		((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsNMTokens_37 = L_67;
		XmlSchemaSimpleTypeList_t4_70 * L_68 = (XmlSchemaSimpleTypeList_t4_70 *)il2cpp_codegen_object_new (XmlSchemaSimpleTypeList_t4_70_il2cpp_TypeInfo_var);
		XmlSchemaSimpleTypeList__ctor_m4_102(L_68, /*hidden argument*/NULL);
		V_2 = L_68;
		XmlSchemaSimpleTypeList_t4_70 * L_69 = V_2;
		XmlSchemaSimpleType_t4_63 * L_70 = ((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsNMToken_36;
		NullCheck(L_69);
		XmlSchemaSimpleTypeList_set_ItemType_m4_104(L_69, L_70, /*hidden argument*/NULL);
		XmlSchemaSimpleType_t4_63 * L_71 = ((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsNMTokens_37;
		XmlSchemaSimpleTypeList_t4_70 * L_72 = V_2;
		NullCheck(L_71);
		XmlSchemaSimpleType_set_Content_m4_100(L_71, L_72, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::BuildSchemaType(System.String,System.String)
extern TypeInfo* XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var;
extern "C" XmlSchemaSimpleType_t4_63 * XmlSchemaSimpleType_BuildSchemaType_m4_98 (Object_t * __this /* static, unused */, String_t* ___name, String_t* ___baseName, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___name;
		String_t* L_1 = ___baseName;
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var);
		XmlSchemaSimpleType_t4_63 * L_2 = XmlSchemaSimpleType_BuildSchemaType_m4_99(NULL /*static, unused*/, L_0, L_1, 0, 0, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::BuildSchemaType(System.String,System.String,System.Boolean,System.Boolean)
extern TypeInfo* XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var;
extern TypeInfo* XmlQualifiedName_t4_71_il2cpp_TypeInfo_var;
extern TypeInfo* XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3779;
extern Il2CppCodeGenString* _stringLiteral3778;
extern "C" XmlSchemaSimpleType_t4_63 * XmlSchemaSimpleType_BuildSchemaType_m4_99 (Object_t * __this /* static, unused */, String_t* ___name, String_t* ___baseName, bool ___xdt, bool ___baseXdt, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		XmlQualifiedName_t4_71_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1275);
		XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1215);
		_stringLiteral3779 = il2cpp_codegen_string_literal_from_index(3779);
		_stringLiteral3778 = il2cpp_codegen_string_literal_from_index(3778);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	String_t* V_1 = {0};
	XmlSchemaSimpleType_t4_63 * V_2 = {0};
	String_t* G_B3_0 = {0};
	String_t* G_B6_0 = {0};
	{
		bool L_0 = ___xdt;
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		G_B3_0 = _stringLiteral3779;
		goto IL_0015;
	}

IL_0010:
	{
		G_B3_0 = _stringLiteral3778;
	}

IL_0015:
	{
		V_0 = G_B3_0;
		bool L_1 = ___baseXdt;
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		G_B6_0 = _stringLiteral3779;
		goto IL_002b;
	}

IL_0026:
	{
		G_B6_0 = _stringLiteral3778;
	}

IL_002b:
	{
		V_1 = G_B6_0;
		XmlSchemaSimpleType_t4_63 * L_2 = (XmlSchemaSimpleType_t4_63 *)il2cpp_codegen_object_new (XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var);
		XmlSchemaSimpleType__ctor_m4_96(L_2, /*hidden argument*/NULL);
		V_2 = L_2;
		XmlSchemaSimpleType_t4_63 * L_3 = V_2;
		String_t* L_4 = ___name;
		String_t* L_5 = V_0;
		XmlQualifiedName_t4_71 * L_6 = (XmlQualifiedName_t4_71 *)il2cpp_codegen_object_new (XmlQualifiedName_t4_71_il2cpp_TypeInfo_var);
		XmlQualifiedName__ctor_m4_730(L_6, L_4, L_5, /*hidden argument*/NULL);
		NullCheck(L_3);
		((XmlSchemaType_t4_64 *)L_3)->___QNameInternal_6 = L_6;
		String_t* L_7 = ___baseName;
		if (!L_7)
		{
			goto IL_0057;
		}
	}
	{
		XmlSchemaSimpleType_t4_63 * L_8 = V_2;
		String_t* L_9 = ___baseName;
		String_t* L_10 = V_1;
		XmlQualifiedName_t4_71 * L_11 = (XmlQualifiedName_t4_71 *)il2cpp_codegen_object_new (XmlQualifiedName_t4_71_il2cpp_TypeInfo_var);
		XmlQualifiedName__ctor_m4_730(L_11, L_9, L_10, /*hidden argument*/NULL);
		XmlSchemaSimpleType_t4_63 * L_12 = XmlSchemaType_GetBuiltInSimpleType_m4_108(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		NullCheck(L_8);
		((XmlSchemaType_t4_64 *)L_8)->___BaseXmlSchemaTypeInternal_4 = L_12;
	}

IL_0057:
	{
		XmlSchemaSimpleType_t4_63 * L_13 = V_2;
		XmlSchemaSimpleType_t4_63 * L_14 = V_2;
		NullCheck(L_14);
		XmlQualifiedName_t4_71 * L_15 = XmlSchemaType_get_QualifiedName_m4_107(L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t4_4_il2cpp_TypeInfo_var);
		XmlSchemaDatatype_t4_4 * L_16 = XmlSchemaDatatype_FromName_m4_82(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		NullCheck(L_13);
		((XmlSchemaType_t4_64 *)L_13)->___DatatypeInternal_5 = L_16;
		XmlSchemaSimpleType_t4_63 * L_17 = V_2;
		return L_17;
	}
}
// System.Void System.Xml.Schema.XmlSchemaSimpleType::set_Content(System.Xml.Schema.XmlSchemaSimpleTypeContent)
extern "C" void XmlSchemaSimpleType_set_Content_m4_100 (XmlSchemaSimpleType_t4_63 * __this, XmlSchemaSimpleTypeContent_t4_69 * ___value, const MethodInfo* method)
{
	{
		XmlSchemaSimpleTypeContent_t4_69 * L_0 = ___value;
		__this->___content_10 = L_0;
		return;
	}
}
// System.Void System.Xml.Schema.XmlSchemaSimpleTypeContent::.ctor()
extern "C" void XmlSchemaSimpleTypeContent__ctor_m4_101 (XmlSchemaSimpleTypeContent_t4_69 * __this, const MethodInfo* method)
{
	{
		XmlSchemaAnnotated__ctor_m4_78(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.Schema.XmlSchemaSimpleTypeList::.ctor()
extern TypeInfo* XmlQualifiedName_t4_71_il2cpp_TypeInfo_var;
extern "C" void XmlSchemaSimpleTypeList__ctor_m4_102 (XmlSchemaSimpleTypeList_t4_70 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		XmlQualifiedName_t4_71_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1275);
		s_Il2CppMethodIntialized = true;
	}
	{
		XmlSchemaSimpleTypeContent__ctor_m4_101(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(XmlQualifiedName_t4_71_il2cpp_TypeInfo_var);
		XmlQualifiedName_t4_71 * L_0 = ((XmlQualifiedName_t4_71_StaticFields*)XmlQualifiedName_t4_71_il2cpp_TypeInfo_var->static_fields)->___Empty_0;
		XmlSchemaSimpleTypeList_set_ItemTypeName_m4_103(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.Schema.XmlSchemaSimpleTypeList::set_ItemTypeName(System.Xml.XmlQualifiedName)
extern "C" void XmlSchemaSimpleTypeList_set_ItemTypeName_m4_103 (XmlSchemaSimpleTypeList_t4_70 * __this, XmlQualifiedName_t4_71 * ___value, const MethodInfo* method)
{
	{
		XmlQualifiedName_t4_71 * L_0 = ___value;
		__this->___itemTypeName_4 = L_0;
		return;
	}
}
// System.Void System.Xml.Schema.XmlSchemaSimpleTypeList::set_ItemType(System.Xml.Schema.XmlSchemaSimpleType)
extern "C" void XmlSchemaSimpleTypeList_set_ItemType_m4_104 (XmlSchemaSimpleTypeList_t4_70 * __this, XmlSchemaSimpleType_t4_63 * ___value, const MethodInfo* method)
{
	{
		XmlSchemaSimpleType_t4_63 * L_0 = ___value;
		__this->___itemType_3 = L_0;
		return;
	}
}
// System.Void System.Xml.Schema.XmlSchemaSimpleTypeRestriction::.cctor()
extern TypeInfo* XmlSchemaSimpleTypeRestriction_t4_72_il2cpp_TypeInfo_var;
extern "C" void XmlSchemaSimpleTypeRestriction__cctor_m4_105 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		XmlSchemaSimpleTypeRestriction_t4_72_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1276);
		s_Il2CppMethodIntialized = true;
	}
	{
		((XmlSchemaSimpleTypeRestriction_t4_72_StaticFields*)XmlSchemaSimpleTypeRestriction_t4_72_il2cpp_TypeInfo_var->static_fields)->___lengthStyle_3 = 7;
		((XmlSchemaSimpleTypeRestriction_t4_72_StaticFields*)XmlSchemaSimpleTypeRestriction_t4_72_il2cpp_TypeInfo_var->static_fields)->___listFacets_4 = ((int32_t)63);
		return;
	}
}
// System.Void System.Xml.Schema.XmlSchemaType::.ctor()
extern TypeInfo* XmlQualifiedName_t4_71_il2cpp_TypeInfo_var;
extern "C" void XmlSchemaType__ctor_m4_106 (XmlSchemaType_t4_64 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		XmlQualifiedName_t4_71_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1275);
		s_Il2CppMethodIntialized = true;
	}
	{
		XmlSchemaAnnotated__ctor_m4_78(__this, /*hidden argument*/NULL);
		__this->___final_3 = ((int32_t)256);
		IL2CPP_RUNTIME_CLASS_INIT(XmlQualifiedName_t4_71_il2cpp_TypeInfo_var);
		XmlQualifiedName_t4_71 * L_0 = ((XmlQualifiedName_t4_71_StaticFields*)XmlQualifiedName_t4_71_il2cpp_TypeInfo_var->static_fields)->___Empty_0;
		__this->___QNameInternal_6 = L_0;
		return;
	}
}
// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaType::get_QualifiedName()
extern "C" XmlQualifiedName_t4_71 * XmlSchemaType_get_QualifiedName_m4_107 (XmlSchemaType_t4_64 * __this, const MethodInfo* method)
{
	{
		XmlQualifiedName_t4_71 * L_0 = (__this->___QNameInternal_6);
		return L_0;
	}
}
// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaType::GetBuiltInSimpleType(System.Xml.XmlQualifiedName)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* XmlSchemaType_t4_64_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t1_92_il2cpp_TypeInfo_var;
extern TypeInfo* XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_14878_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3779;
extern Il2CppCodeGenString* _stringLiteral3781;
extern Il2CppCodeGenString* _stringLiteral3780;
extern Il2CppCodeGenString* _stringLiteral3783;
extern Il2CppCodeGenString* _stringLiteral3782;
extern Il2CppCodeGenString* _stringLiteral3778;
extern Il2CppCodeGenString* _stringLiteral3784;
extern Il2CppCodeGenString* _stringLiteral309;
extern Il2CppCodeGenString* _stringLiteral3795;
extern Il2CppCodeGenString* _stringLiteral3785;
extern Il2CppCodeGenString* _stringLiteral3793;
extern Il2CppCodeGenString* _stringLiteral3794;
extern Il2CppCodeGenString* _stringLiteral2249;
extern Il2CppCodeGenString* _stringLiteral2245;
extern Il2CppCodeGenString* _stringLiteral673;
extern Il2CppCodeGenString* _stringLiteral954;
extern Il2CppCodeGenString* _stringLiteral2316;
extern Il2CppCodeGenString* _stringLiteral2309;
extern Il2CppCodeGenString* _stringLiteral2267;
extern Il2CppCodeGenString* _stringLiteral2248;
extern Il2CppCodeGenString* _stringLiteral2264;
extern Il2CppCodeGenString* _stringLiteral2255;
extern Il2CppCodeGenString* _stringLiteral2214;
extern Il2CppCodeGenString* _stringLiteral3796;
extern Il2CppCodeGenString* _stringLiteral2278;
extern Il2CppCodeGenString* _stringLiteral2276;
extern Il2CppCodeGenString* _stringLiteral2275;
extern Il2CppCodeGenString* _stringLiteral2302;
extern Il2CppCodeGenString* _stringLiteral2261;
extern Il2CppCodeGenString* _stringLiteral2271;
extern Il2CppCodeGenString* _stringLiteral2272;
extern Il2CppCodeGenString* _stringLiteral1969;
extern Il2CppCodeGenString* _stringLiteral2268;
extern Il2CppCodeGenString* _stringLiteral2257;
extern Il2CppCodeGenString* _stringLiteral2258;
extern Il2CppCodeGenString* _stringLiteral2259;
extern Il2CppCodeGenString* _stringLiteral2253;
extern Il2CppCodeGenString* _stringLiteral2252;
extern Il2CppCodeGenString* _stringLiteral2260;
extern Il2CppCodeGenString* _stringLiteral2274;
extern Il2CppCodeGenString* _stringLiteral2270;
extern Il2CppCodeGenString* _stringLiteral3786;
extern Il2CppCodeGenString* _stringLiteral307;
extern Il2CppCodeGenString* _stringLiteral3787;
extern Il2CppCodeGenString* _stringLiteral3788;
extern Il2CppCodeGenString* _stringLiteral2273;
extern Il2CppCodeGenString* _stringLiteral2277;
extern Il2CppCodeGenString* _stringLiteral3789;
extern Il2CppCodeGenString* _stringLiteral3790;
extern Il2CppCodeGenString* _stringLiteral3791;
extern Il2CppCodeGenString* _stringLiteral3792;
extern "C" XmlSchemaSimpleType_t4_63 * XmlSchemaType_GetBuiltInSimpleType_m4_108 (Object_t * __this /* static, unused */, XmlQualifiedName_t4_71 * ___qualifiedName, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		XmlSchemaType_t4_64_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1277);
		Dictionary_2_t1_92_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(129);
		XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1273);
		Dictionary_2__ctor_m1_14878_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483658);
		_stringLiteral3779 = il2cpp_codegen_string_literal_from_index(3779);
		_stringLiteral3781 = il2cpp_codegen_string_literal_from_index(3781);
		_stringLiteral3780 = il2cpp_codegen_string_literal_from_index(3780);
		_stringLiteral3783 = il2cpp_codegen_string_literal_from_index(3783);
		_stringLiteral3782 = il2cpp_codegen_string_literal_from_index(3782);
		_stringLiteral3778 = il2cpp_codegen_string_literal_from_index(3778);
		_stringLiteral3784 = il2cpp_codegen_string_literal_from_index(3784);
		_stringLiteral309 = il2cpp_codegen_string_literal_from_index(309);
		_stringLiteral3795 = il2cpp_codegen_string_literal_from_index(3795);
		_stringLiteral3785 = il2cpp_codegen_string_literal_from_index(3785);
		_stringLiteral3793 = il2cpp_codegen_string_literal_from_index(3793);
		_stringLiteral3794 = il2cpp_codegen_string_literal_from_index(3794);
		_stringLiteral2249 = il2cpp_codegen_string_literal_from_index(2249);
		_stringLiteral2245 = il2cpp_codegen_string_literal_from_index(2245);
		_stringLiteral673 = il2cpp_codegen_string_literal_from_index(673);
		_stringLiteral954 = il2cpp_codegen_string_literal_from_index(954);
		_stringLiteral2316 = il2cpp_codegen_string_literal_from_index(2316);
		_stringLiteral2309 = il2cpp_codegen_string_literal_from_index(2309);
		_stringLiteral2267 = il2cpp_codegen_string_literal_from_index(2267);
		_stringLiteral2248 = il2cpp_codegen_string_literal_from_index(2248);
		_stringLiteral2264 = il2cpp_codegen_string_literal_from_index(2264);
		_stringLiteral2255 = il2cpp_codegen_string_literal_from_index(2255);
		_stringLiteral2214 = il2cpp_codegen_string_literal_from_index(2214);
		_stringLiteral3796 = il2cpp_codegen_string_literal_from_index(3796);
		_stringLiteral2278 = il2cpp_codegen_string_literal_from_index(2278);
		_stringLiteral2276 = il2cpp_codegen_string_literal_from_index(2276);
		_stringLiteral2275 = il2cpp_codegen_string_literal_from_index(2275);
		_stringLiteral2302 = il2cpp_codegen_string_literal_from_index(2302);
		_stringLiteral2261 = il2cpp_codegen_string_literal_from_index(2261);
		_stringLiteral2271 = il2cpp_codegen_string_literal_from_index(2271);
		_stringLiteral2272 = il2cpp_codegen_string_literal_from_index(2272);
		_stringLiteral1969 = il2cpp_codegen_string_literal_from_index(1969);
		_stringLiteral2268 = il2cpp_codegen_string_literal_from_index(2268);
		_stringLiteral2257 = il2cpp_codegen_string_literal_from_index(2257);
		_stringLiteral2258 = il2cpp_codegen_string_literal_from_index(2258);
		_stringLiteral2259 = il2cpp_codegen_string_literal_from_index(2259);
		_stringLiteral2253 = il2cpp_codegen_string_literal_from_index(2253);
		_stringLiteral2252 = il2cpp_codegen_string_literal_from_index(2252);
		_stringLiteral2260 = il2cpp_codegen_string_literal_from_index(2260);
		_stringLiteral2274 = il2cpp_codegen_string_literal_from_index(2274);
		_stringLiteral2270 = il2cpp_codegen_string_literal_from_index(2270);
		_stringLiteral3786 = il2cpp_codegen_string_literal_from_index(3786);
		_stringLiteral307 = il2cpp_codegen_string_literal_from_index(307);
		_stringLiteral3787 = il2cpp_codegen_string_literal_from_index(3787);
		_stringLiteral3788 = il2cpp_codegen_string_literal_from_index(3788);
		_stringLiteral2273 = il2cpp_codegen_string_literal_from_index(2273);
		_stringLiteral2277 = il2cpp_codegen_string_literal_from_index(2277);
		_stringLiteral3789 = il2cpp_codegen_string_literal_from_index(3789);
		_stringLiteral3790 = il2cpp_codegen_string_literal_from_index(3790);
		_stringLiteral3791 = il2cpp_codegen_string_literal_from_index(3791);
		_stringLiteral3792 = il2cpp_codegen_string_literal_from_index(3792);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	Dictionary_2_t1_92 * V_1 = {0};
	int32_t V_2 = 0;
	{
		XmlQualifiedName_t4_71 * L_0 = ___qualifiedName;
		NullCheck(L_0);
		String_t* L_1 = XmlQualifiedName_get_Namespace_m4_733(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Equality_m1_601(NULL /*static, unused*/, L_1, _stringLiteral3779, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_00b0;
		}
	}
	{
		XmlQualifiedName_t4_71 * L_3 = ___qualifiedName;
		NullCheck(L_3);
		String_t* L_4 = XmlQualifiedName_get_Name_m4_732(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		String_t* L_5 = V_0;
		if (!L_5)
		{
			goto IL_00ae;
		}
	}
	{
		Dictionary_2_t1_92 * L_6 = ((XmlSchemaType_t4_64_StaticFields*)XmlSchemaType_t4_64_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__switchU24map2E_7;
		if (L_6)
		{
			goto IL_0069;
		}
	}
	{
		Dictionary_2_t1_92 * L_7 = (Dictionary_2_t1_92 *)il2cpp_codegen_object_new (Dictionary_2_t1_92_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_14878(L_7, 4, /*hidden argument*/Dictionary_2__ctor_m1_14878_MethodInfo_var);
		V_1 = L_7;
		Dictionary_2_t1_92 * L_8 = V_1;
		NullCheck(L_8);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_8, _stringLiteral3781, 0);
		Dictionary_2_t1_92 * L_9 = V_1;
		NullCheck(L_9);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_9, _stringLiteral3780, 1);
		Dictionary_2_t1_92 * L_10 = V_1;
		NullCheck(L_10);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_10, _stringLiteral3783, 2);
		Dictionary_2_t1_92 * L_11 = V_1;
		NullCheck(L_11);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_11, _stringLiteral3782, 3);
		Dictionary_2_t1_92 * L_12 = V_1;
		((XmlSchemaType_t4_64_StaticFields*)XmlSchemaType_t4_64_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__switchU24map2E_7 = L_12;
	}

IL_0069:
	{
		Dictionary_2_t1_92 * L_13 = ((XmlSchemaType_t4_64_StaticFields*)XmlSchemaType_t4_64_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__switchU24map2E_7;
		String_t* L_14 = V_0;
		NullCheck(L_13);
		bool L_15 = (bool)VirtFuncInvoker2< bool, String_t*, int32_t* >::Invoke(21 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int32>::TryGetValue(!0,!1&) */, L_13, L_14, (&V_2));
		if (!L_15)
		{
			goto IL_00ae;
		}
	}
	{
		int32_t L_16 = V_2;
		if (L_16 == 0)
		{
			goto IL_0096;
		}
		if (L_16 == 1)
		{
			goto IL_009c;
		}
		if (L_16 == 2)
		{
			goto IL_00a2;
		}
		if (L_16 == 3)
		{
			goto IL_00a8;
		}
	}
	{
		goto IL_00ae;
	}

IL_0096:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var);
		XmlSchemaSimpleType_t4_63 * L_17 = ((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XdtUntypedAtomic_58;
		return L_17;
	}

IL_009c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var);
		XmlSchemaSimpleType_t4_63 * L_18 = ((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XdtAnyAtomicType_59;
		return L_18;
	}

IL_00a2:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var);
		XmlSchemaSimpleType_t4_63 * L_19 = ((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XdtYearMonthDuration_60;
		return L_19;
	}

IL_00a8:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var);
		XmlSchemaSimpleType_t4_63 * L_20 = ((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XdtDayTimeDuration_61;
		return L_20;
	}

IL_00ae:
	{
		return (XmlSchemaSimpleType_t4_63 *)NULL;
	}

IL_00b0:
	{
		XmlQualifiedName_t4_71 * L_21 = ___qualifiedName;
		NullCheck(L_21);
		String_t* L_22 = XmlQualifiedName_get_Namespace_m4_733(L_21, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_23 = String_op_Inequality_m1_602(NULL /*static, unused*/, L_22, _stringLiteral3778, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_00c7;
		}
	}
	{
		return (XmlSchemaSimpleType_t4_63 *)NULL;
	}

IL_00c7:
	{
		XmlQualifiedName_t4_71 * L_24 = ___qualifiedName;
		NullCheck(L_24);
		String_t* L_25 = XmlQualifiedName_get_Name_m4_732(L_24, /*hidden argument*/NULL);
		V_0 = L_25;
		String_t* L_26 = V_0;
		if (!L_26)
		{
			goto IL_050b;
		}
	}
	{
		Dictionary_2_t1_92 * L_27 = ((XmlSchemaType_t4_64_StaticFields*)XmlSchemaType_t4_64_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__switchU24map2F_8;
		if (L_27)
		{
			goto IL_032c;
		}
	}
	{
		Dictionary_2_t1_92 * L_28 = (Dictionary_2_t1_92 *)il2cpp_codegen_object_new (Dictionary_2_t1_92_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_14878(L_28, ((int32_t)45), /*hidden argument*/Dictionary_2__ctor_m1_14878_MethodInfo_var);
		V_1 = L_28;
		Dictionary_2_t1_92 * L_29 = V_1;
		NullCheck(L_29);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_29, _stringLiteral3784, 0);
		Dictionary_2_t1_92 * L_30 = V_1;
		NullCheck(L_30);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_30, _stringLiteral309, 1);
		Dictionary_2_t1_92 * L_31 = V_1;
		NullCheck(L_31);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_31, _stringLiteral3795, 2);
		Dictionary_2_t1_92 * L_32 = V_1;
		NullCheck(L_32);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_32, _stringLiteral3785, 3);
		Dictionary_2_t1_92 * L_33 = V_1;
		NullCheck(L_33);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_33, _stringLiteral3793, 4);
		Dictionary_2_t1_92 * L_34 = V_1;
		NullCheck(L_34);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_34, _stringLiteral3794, 5);
		Dictionary_2_t1_92 * L_35 = V_1;
		NullCheck(L_35);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_35, _stringLiteral2249, 6);
		Dictionary_2_t1_92 * L_36 = V_1;
		NullCheck(L_36);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_36, _stringLiteral2245, 7);
		Dictionary_2_t1_92 * L_37 = V_1;
		NullCheck(L_37);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_37, _stringLiteral673, 8);
		Dictionary_2_t1_92 * L_38 = V_1;
		NullCheck(L_38);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_38, _stringLiteral954, ((int32_t)9));
		Dictionary_2_t1_92 * L_39 = V_1;
		NullCheck(L_39);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_39, _stringLiteral2316, ((int32_t)10));
		Dictionary_2_t1_92 * L_40 = V_1;
		NullCheck(L_40);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_40, _stringLiteral2309, ((int32_t)11));
		Dictionary_2_t1_92 * L_41 = V_1;
		NullCheck(L_41);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_41, _stringLiteral2267, ((int32_t)12));
		Dictionary_2_t1_92 * L_42 = V_1;
		NullCheck(L_42);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_42, _stringLiteral2248, ((int32_t)13));
		Dictionary_2_t1_92 * L_43 = V_1;
		NullCheck(L_43);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_43, _stringLiteral2264, ((int32_t)14));
		Dictionary_2_t1_92 * L_44 = V_1;
		NullCheck(L_44);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_44, _stringLiteral2255, ((int32_t)15));
		Dictionary_2_t1_92 * L_45 = V_1;
		NullCheck(L_45);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_45, _stringLiteral2214, ((int32_t)16));
		Dictionary_2_t1_92 * L_46 = V_1;
		NullCheck(L_46);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_46, _stringLiteral3796, ((int32_t)17));
		Dictionary_2_t1_92 * L_47 = V_1;
		NullCheck(L_47);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_47, _stringLiteral2278, ((int32_t)18));
		Dictionary_2_t1_92 * L_48 = V_1;
		NullCheck(L_48);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_48, _stringLiteral2276, ((int32_t)19));
		Dictionary_2_t1_92 * L_49 = V_1;
		NullCheck(L_49);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_49, _stringLiteral2275, ((int32_t)20));
		Dictionary_2_t1_92 * L_50 = V_1;
		NullCheck(L_50);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_50, _stringLiteral2302, ((int32_t)21));
		Dictionary_2_t1_92 * L_51 = V_1;
		NullCheck(L_51);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_51, _stringLiteral2261, ((int32_t)22));
		Dictionary_2_t1_92 * L_52 = V_1;
		NullCheck(L_52);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_52, _stringLiteral2271, ((int32_t)23));
		Dictionary_2_t1_92 * L_53 = V_1;
		NullCheck(L_53);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_53, _stringLiteral2272, ((int32_t)24));
		Dictionary_2_t1_92 * L_54 = V_1;
		NullCheck(L_54);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_54, _stringLiteral1969, ((int32_t)25));
		Dictionary_2_t1_92 * L_55 = V_1;
		NullCheck(L_55);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_55, _stringLiteral2268, ((int32_t)26));
		Dictionary_2_t1_92 * L_56 = V_1;
		NullCheck(L_56);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_56, _stringLiteral2257, ((int32_t)27));
		Dictionary_2_t1_92 * L_57 = V_1;
		NullCheck(L_57);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_57, _stringLiteral2258, ((int32_t)28));
		Dictionary_2_t1_92 * L_58 = V_1;
		NullCheck(L_58);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_58, _stringLiteral2259, ((int32_t)29));
		Dictionary_2_t1_92 * L_59 = V_1;
		NullCheck(L_59);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_59, _stringLiteral2253, ((int32_t)30));
		Dictionary_2_t1_92 * L_60 = V_1;
		NullCheck(L_60);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_60, _stringLiteral2252, ((int32_t)31));
		Dictionary_2_t1_92 * L_61 = V_1;
		NullCheck(L_61);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_61, _stringLiteral2260, ((int32_t)32));
		Dictionary_2_t1_92 * L_62 = V_1;
		NullCheck(L_62);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_62, _stringLiteral2274, ((int32_t)33));
		Dictionary_2_t1_92 * L_63 = V_1;
		NullCheck(L_63);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_63, _stringLiteral2270, ((int32_t)34));
		Dictionary_2_t1_92 * L_64 = V_1;
		NullCheck(L_64);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_64, _stringLiteral3786, ((int32_t)35));
		Dictionary_2_t1_92 * L_65 = V_1;
		NullCheck(L_65);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_65, _stringLiteral307, ((int32_t)36));
		Dictionary_2_t1_92 * L_66 = V_1;
		NullCheck(L_66);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_66, _stringLiteral3787, ((int32_t)37));
		Dictionary_2_t1_92 * L_67 = V_1;
		NullCheck(L_67);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_67, _stringLiteral3788, ((int32_t)38));
		Dictionary_2_t1_92 * L_68 = V_1;
		NullCheck(L_68);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_68, _stringLiteral2273, ((int32_t)39));
		Dictionary_2_t1_92 * L_69 = V_1;
		NullCheck(L_69);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_69, _stringLiteral2277, ((int32_t)40));
		Dictionary_2_t1_92 * L_70 = V_1;
		NullCheck(L_70);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_70, _stringLiteral3789, ((int32_t)41));
		Dictionary_2_t1_92 * L_71 = V_1;
		NullCheck(L_71);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_71, _stringLiteral3790, ((int32_t)42));
		Dictionary_2_t1_92 * L_72 = V_1;
		NullCheck(L_72);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_72, _stringLiteral3791, ((int32_t)43));
		Dictionary_2_t1_92 * L_73 = V_1;
		NullCheck(L_73);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_73, _stringLiteral3792, ((int32_t)44));
		Dictionary_2_t1_92 * L_74 = V_1;
		((XmlSchemaType_t4_64_StaticFields*)XmlSchemaType_t4_64_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__switchU24map2F_8 = L_74;
	}

IL_032c:
	{
		Dictionary_2_t1_92 * L_75 = ((XmlSchemaType_t4_64_StaticFields*)XmlSchemaType_t4_64_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__switchU24map2F_8;
		String_t* L_76 = V_0;
		NullCheck(L_75);
		bool L_77 = (bool)VirtFuncInvoker2< bool, String_t*, int32_t* >::Invoke(21 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int32>::TryGetValue(!0,!1&) */, L_75, L_76, (&V_2));
		if (!L_77)
		{
			goto IL_050b;
		}
	}
	{
		int32_t L_78 = V_2;
		if (L_78 == 0)
		{
			goto IL_03fd;
		}
		if (L_78 == 1)
		{
			goto IL_0403;
		}
		if (L_78 == 2)
		{
			goto IL_0409;
		}
		if (L_78 == 3)
		{
			goto IL_040f;
		}
		if (L_78 == 4)
		{
			goto IL_0415;
		}
		if (L_78 == 5)
		{
			goto IL_041b;
		}
		if (L_78 == 6)
		{
			goto IL_0421;
		}
		if (L_78 == 7)
		{
			goto IL_0427;
		}
		if (L_78 == 8)
		{
			goto IL_042d;
		}
		if (L_78 == 9)
		{
			goto IL_0433;
		}
		if (L_78 == 10)
		{
			goto IL_0439;
		}
		if (L_78 == 11)
		{
			goto IL_043f;
		}
		if (L_78 == 12)
		{
			goto IL_0445;
		}
		if (L_78 == 13)
		{
			goto IL_044b;
		}
		if (L_78 == 14)
		{
			goto IL_0451;
		}
		if (L_78 == 15)
		{
			goto IL_0457;
		}
		if (L_78 == 16)
		{
			goto IL_045d;
		}
		if (L_78 == 17)
		{
			goto IL_0463;
		}
		if (L_78 == 18)
		{
			goto IL_0469;
		}
		if (L_78 == 19)
		{
			goto IL_046f;
		}
		if (L_78 == 20)
		{
			goto IL_0475;
		}
		if (L_78 == 21)
		{
			goto IL_047b;
		}
		if (L_78 == 22)
		{
			goto IL_0481;
		}
		if (L_78 == 23)
		{
			goto IL_0487;
		}
		if (L_78 == 24)
		{
			goto IL_048d;
		}
		if (L_78 == 25)
		{
			goto IL_0493;
		}
		if (L_78 == 26)
		{
			goto IL_0499;
		}
		if (L_78 == 27)
		{
			goto IL_049f;
		}
		if (L_78 == 28)
		{
			goto IL_04a5;
		}
		if (L_78 == 29)
		{
			goto IL_04ab;
		}
		if (L_78 == 30)
		{
			goto IL_04b1;
		}
		if (L_78 == 31)
		{
			goto IL_04b7;
		}
		if (L_78 == 32)
		{
			goto IL_04bd;
		}
		if (L_78 == 33)
		{
			goto IL_04c3;
		}
		if (L_78 == 34)
		{
			goto IL_04c9;
		}
		if (L_78 == 35)
		{
			goto IL_04cf;
		}
		if (L_78 == 36)
		{
			goto IL_04d5;
		}
		if (L_78 == 37)
		{
			goto IL_04db;
		}
		if (L_78 == 38)
		{
			goto IL_04e1;
		}
		if (L_78 == 39)
		{
			goto IL_04e7;
		}
		if (L_78 == 40)
		{
			goto IL_04ed;
		}
		if (L_78 == 41)
		{
			goto IL_04f3;
		}
		if (L_78 == 42)
		{
			goto IL_04f9;
		}
		if (L_78 == 43)
		{
			goto IL_04ff;
		}
		if (L_78 == 44)
		{
			goto IL_0505;
		}
	}
	{
		goto IL_050b;
	}

IL_03fd:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var);
		XmlSchemaSimpleType_t4_63 * L_79 = ((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsAnySimpleType_13;
		return L_79;
	}

IL_0403:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var);
		XmlSchemaSimpleType_t4_63 * L_80 = ((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsString_14;
		return L_80;
	}

IL_0409:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var);
		XmlSchemaSimpleType_t4_63 * L_81 = ((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsBoolean_15;
		return L_81;
	}

IL_040f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var);
		XmlSchemaSimpleType_t4_63 * L_82 = ((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsDecimal_16;
		return L_82;
	}

IL_0415:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var);
		XmlSchemaSimpleType_t4_63 * L_83 = ((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsFloat_17;
		return L_83;
	}

IL_041b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var);
		XmlSchemaSimpleType_t4_63 * L_84 = ((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsDouble_18;
		return L_84;
	}

IL_0421:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var);
		XmlSchemaSimpleType_t4_63 * L_85 = ((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsDuration_19;
		return L_85;
	}

IL_0427:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var);
		XmlSchemaSimpleType_t4_63 * L_86 = ((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsDateTime_20;
		return L_86;
	}

IL_042d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var);
		XmlSchemaSimpleType_t4_63 * L_87 = ((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsTime_21;
		return L_87;
	}

IL_0433:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var);
		XmlSchemaSimpleType_t4_63 * L_88 = ((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsDate_22;
		return L_88;
	}

IL_0439:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var);
		XmlSchemaSimpleType_t4_63 * L_89 = ((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsGYearMonth_23;
		return L_89;
	}

IL_043f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var);
		XmlSchemaSimpleType_t4_63 * L_90 = ((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsGYear_24;
		return L_90;
	}

IL_0445:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var);
		XmlSchemaSimpleType_t4_63 * L_91 = ((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsGMonthDay_25;
		return L_91;
	}

IL_044b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var);
		XmlSchemaSimpleType_t4_63 * L_92 = ((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsGDay_26;
		return L_92;
	}

IL_0451:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var);
		XmlSchemaSimpleType_t4_63 * L_93 = ((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsGMonth_27;
		return L_93;
	}

IL_0457:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var);
		XmlSchemaSimpleType_t4_63 * L_94 = ((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsHexBinary_28;
		return L_94;
	}

IL_045d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var);
		XmlSchemaSimpleType_t4_63 * L_95 = ((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsBase64Binary_29;
		return L_95;
	}

IL_0463:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var);
		XmlSchemaSimpleType_t4_63 * L_96 = ((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsAnyUri_30;
		return L_96;
	}

IL_0469:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var);
		XmlSchemaSimpleType_t4_63 * L_97 = ((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsQName_31;
		return L_97;
	}

IL_046f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var);
		XmlSchemaSimpleType_t4_63 * L_98 = ((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsNotation_32;
		return L_98;
	}

IL_0475:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var);
		XmlSchemaSimpleType_t4_63 * L_99 = ((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsNormalizedString_33;
		return L_99;
	}

IL_047b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var);
		XmlSchemaSimpleType_t4_63 * L_100 = ((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsToken_34;
		return L_100;
	}

IL_0481:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var);
		XmlSchemaSimpleType_t4_63 * L_101 = ((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsLanguage_35;
		return L_101;
	}

IL_0487:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var);
		XmlSchemaSimpleType_t4_63 * L_102 = ((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsNMToken_36;
		return L_102;
	}

IL_048d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var);
		XmlSchemaSimpleType_t4_63 * L_103 = ((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsNMTokens_37;
		return L_103;
	}

IL_0493:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var);
		XmlSchemaSimpleType_t4_63 * L_104 = ((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsName_38;
		return L_104;
	}

IL_0499:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var);
		XmlSchemaSimpleType_t4_63 * L_105 = ((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsNCName_39;
		return L_105;
	}

IL_049f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var);
		XmlSchemaSimpleType_t4_63 * L_106 = ((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsID_40;
		return L_106;
	}

IL_04a5:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var);
		XmlSchemaSimpleType_t4_63 * L_107 = ((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsIDRef_41;
		return L_107;
	}

IL_04ab:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var);
		XmlSchemaSimpleType_t4_63 * L_108 = ((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsIDRefs_42;
		return L_108;
	}

IL_04b1:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var);
		XmlSchemaSimpleType_t4_63 * L_109 = ((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsEntity_43;
		return L_109;
	}

IL_04b7:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var);
		XmlSchemaSimpleType_t4_63 * L_110 = ((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsEntities_44;
		return L_110;
	}

IL_04bd:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var);
		XmlSchemaSimpleType_t4_63 * L_111 = ((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsInteger_45;
		return L_111;
	}

IL_04c3:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var);
		XmlSchemaSimpleType_t4_63 * L_112 = ((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsNonPositiveInteger_46;
		return L_112;
	}

IL_04c9:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var);
		XmlSchemaSimpleType_t4_63 * L_113 = ((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsNegativeInteger_47;
		return L_113;
	}

IL_04cf:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var);
		XmlSchemaSimpleType_t4_63 * L_114 = ((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsLong_48;
		return L_114;
	}

IL_04d5:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var);
		XmlSchemaSimpleType_t4_63 * L_115 = ((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsInt_49;
		return L_115;
	}

IL_04db:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var);
		XmlSchemaSimpleType_t4_63 * L_116 = ((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsShort_50;
		return L_116;
	}

IL_04e1:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var);
		XmlSchemaSimpleType_t4_63 * L_117 = ((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsByte_51;
		return L_117;
	}

IL_04e7:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var);
		XmlSchemaSimpleType_t4_63 * L_118 = ((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsNonNegativeInteger_52;
		return L_118;
	}

IL_04ed:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var);
		XmlSchemaSimpleType_t4_63 * L_119 = ((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsPositiveInteger_57;
		return L_119;
	}

IL_04f3:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var);
		XmlSchemaSimpleType_t4_63 * L_120 = ((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsUnsignedLong_53;
		return L_120;
	}

IL_04f9:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var);
		XmlSchemaSimpleType_t4_63 * L_121 = ((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsUnsignedInt_54;
		return L_121;
	}

IL_04ff:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var);
		XmlSchemaSimpleType_t4_63 * L_122 = ((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsUnsignedShort_55;
		return L_122;
	}

IL_0505:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var);
		XmlSchemaSimpleType_t4_63 * L_123 = ((XmlSchemaSimpleType_t4_63_StaticFields*)XmlSchemaSimpleType_t4_63_il2cpp_TypeInfo_var->static_fields)->___XsUnsignedByte_56;
		return L_123;
	}

IL_050b:
	{
		return (XmlSchemaSimpleType_t4_63 *)NULL;
	}
}
// System.Void System.Xml.Schema.XmlSchemaUtil::.cctor()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* XmlSchemaUtil_t4_74_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3798;
extern Il2CppCodeGenString* _stringLiteral346;
extern "C" void XmlSchemaUtil__cctor_m4_109 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		XmlSchemaUtil_t4_74_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1218);
		_stringLiteral3798 = il2cpp_codegen_string_literal_from_index(3798);
		_stringLiteral346 = il2cpp_codegen_string_literal_from_index(346);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = Environment_GetEnvironmentVariable_m1_14067(NULL /*static, unused*/, _stringLiteral3798, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_op_Equality_m1_601(NULL /*static, unused*/, L_0, _stringLiteral346, /*hidden argument*/NULL);
		((XmlSchemaUtil_t4_74_StaticFields*)XmlSchemaUtil_t4_74_il2cpp_TypeInfo_var->static_fields)->___StrictMsCompliant_3 = L_1;
		((XmlSchemaUtil_t4_74_StaticFields*)XmlSchemaUtil_t4_74_il2cpp_TypeInfo_var->static_fields)->___FinalAllowed_0 = 6;
		int32_t L_2 = ((XmlSchemaUtil_t4_74_StaticFields*)XmlSchemaUtil_t4_74_il2cpp_TypeInfo_var->static_fields)->___FinalAllowed_0;
		((XmlSchemaUtil_t4_74_StaticFields*)XmlSchemaUtil_t4_74_il2cpp_TypeInfo_var->static_fields)->___ComplexTypeBlockAllowed_2 = L_2;
		int32_t L_3 = ((XmlSchemaUtil_t4_74_StaticFields*)XmlSchemaUtil_t4_74_il2cpp_TypeInfo_var->static_fields)->___FinalAllowed_0;
		((XmlSchemaUtil_t4_74_StaticFields*)XmlSchemaUtil_t4_74_il2cpp_TypeInfo_var->static_fields)->___ElementBlockAllowed_1 = ((int32_t)((int32_t)1|(int32_t)L_3));
		return;
	}
}
// System.Void System.Xml.Serialization.XmlAttributeAttribute::.ctor(System.String)
extern "C" void XmlAttributeAttribute__ctor_m4_110 (XmlAttributeAttribute_t4_76 * __this, String_t* ___attributeName, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___attributeName;
		__this->___attributeName_0 = L_0;
		return;
	}
}
// System.Void System.Xml.Serialization.XmlElementAttribute::.ctor(System.String)
extern "C" void XmlElementAttribute__ctor_m4_111 (XmlElementAttribute_t4_77 * __this, String_t* ___elementName, const MethodInfo* method)
{
	{
		__this->___order_2 = (-1);
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___elementName;
		__this->___elementName_0 = L_0;
		return;
	}
}
// System.Void System.Xml.Serialization.XmlElementAttribute::.ctor(System.String,System.Type)
extern "C" void XmlElementAttribute__ctor_m4_112 (XmlElementAttribute_t4_77 * __this, String_t* ___elementName, Type_t * ___type, const MethodInfo* method)
{
	{
		__this->___order_2 = (-1);
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___elementName;
		__this->___elementName_0 = L_0;
		Type_t * L_1 = ___type;
		__this->___type_1 = L_1;
		return;
	}
}
// System.Void System.Xml.Serialization.XmlElementAttribute::set_Type(System.Type)
extern "C" void XmlElementAttribute_set_Type_m4_113 (XmlElementAttribute_t4_77 * __this, Type_t * ___value, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___value;
		__this->___type_1 = L_0;
		return;
	}
}
// System.Void System.Xml.Serialization.XmlEnumAttribute::.ctor(System.String)
extern "C" void XmlEnumAttribute__ctor_m4_114 (XmlEnumAttribute_t4_78 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name;
		__this->___name_0 = L_0;
		return;
	}
}
// System.Void System.Xml.Serialization.XmlIgnoreAttribute::.ctor()
extern "C" void XmlIgnoreAttribute__ctor_m4_115 (XmlIgnoreAttribute_t4_79 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.Serialization.XmlSerializerNamespaces::.ctor()
extern TypeInfo* ListDictionary_t3_16_il2cpp_TypeInfo_var;
extern "C" void XmlSerializerNamespaces__ctor_m4_116 (XmlSerializerNamespaces_t4_65 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ListDictionary_t3_16_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1278);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		ListDictionary_t3_16 * L_0 = (ListDictionary_t3_16 *)il2cpp_codegen_object_new (ListDictionary_t3_16_il2cpp_TypeInfo_var);
		ListDictionary__ctor_m3_22(L_0, /*hidden argument*/NULL);
		__this->___namespaces_0 = L_0;
		return;
	}
}
// System.Void Mono.Xml.DTDAutomataFactory::.ctor(Mono.Xml.DTDObjectModel)
extern TypeInfo* Hashtable_t1_100_il2cpp_TypeInfo_var;
extern "C" void DTDAutomataFactory__ctor_m4_117 (DTDAutomataFactory_t4_81 * __this, DTDObjectModel_t4_82 * ___root, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Hashtable_t1_100_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(119);
		s_Il2CppMethodIntialized = true;
	}
	{
		Hashtable_t1_100 * L_0 = (Hashtable_t1_100 *)il2cpp_codegen_object_new (Hashtable_t1_100_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1_3279(L_0, /*hidden argument*/NULL);
		__this->___choiceTable_1 = L_0;
		Hashtable_t1_100 * L_1 = (Hashtable_t1_100 *)il2cpp_codegen_object_new (Hashtable_t1_100_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1_3279(L_1, /*hidden argument*/NULL);
		__this->___sequenceTable_2 = L_1;
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		DTDObjectModel_t4_82 * L_2 = ___root;
		__this->___root_0 = L_2;
		return;
	}
}
// System.Void Mono.Xml.DTDObjectModel::.ctor(System.Xml.XmlNameTable)
extern TypeInfo* DTDElementDeclarationCollection_t4_83_il2cpp_TypeInfo_var;
extern TypeInfo* DTDAttListDeclarationCollection_t4_84_il2cpp_TypeInfo_var;
extern TypeInfo* DTDEntityDeclarationCollection_t4_86_il2cpp_TypeInfo_var;
extern TypeInfo* DTDParameterEntityDeclarationCollection_t4_85_il2cpp_TypeInfo_var;
extern TypeInfo* DTDNotationDeclarationCollection_t4_87_il2cpp_TypeInfo_var;
extern TypeInfo* DTDAutomataFactory_t4_81_il2cpp_TypeInfo_var;
extern TypeInfo* ArrayList_t1_170_il2cpp_TypeInfo_var;
extern TypeInfo* Hashtable_t1_100_il2cpp_TypeInfo_var;
extern "C" void DTDObjectModel__ctor_m4_118 (DTDObjectModel_t4_82 * __this, XmlNameTable_t4_67 * ___nameTable, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DTDElementDeclarationCollection_t4_83_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1279);
		DTDAttListDeclarationCollection_t4_84_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1280);
		DTDEntityDeclarationCollection_t4_86_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1281);
		DTDParameterEntityDeclarationCollection_t4_85_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1282);
		DTDNotationDeclarationCollection_t4_87_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1283);
		DTDAutomataFactory_t4_81_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1284);
		ArrayList_t1_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(60);
		Hashtable_t1_100_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(119);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		XmlNameTable_t4_67 * L_0 = ___nameTable;
		__this->___nameTable_8 = L_0;
		DTDElementDeclarationCollection_t4_83 * L_1 = (DTDElementDeclarationCollection_t4_83 *)il2cpp_codegen_object_new (DTDElementDeclarationCollection_t4_83_il2cpp_TypeInfo_var);
		DTDElementDeclarationCollection__ctor_m4_165(L_1, __this, /*hidden argument*/NULL);
		__this->___elementDecls_1 = L_1;
		DTDAttListDeclarationCollection_t4_84 * L_2 = (DTDAttListDeclarationCollection_t4_84 *)il2cpp_codegen_object_new (DTDAttListDeclarationCollection_t4_84_il2cpp_TypeInfo_var);
		DTDAttListDeclarationCollection__ctor_m4_167(L_2, __this, /*hidden argument*/NULL);
		__this->___attListDecls_2 = L_2;
		DTDEntityDeclarationCollection_t4_86 * L_3 = (DTDEntityDeclarationCollection_t4_86 *)il2cpp_codegen_object_new (DTDEntityDeclarationCollection_t4_86_il2cpp_TypeInfo_var);
		DTDEntityDeclarationCollection__ctor_m4_170(L_3, __this, /*hidden argument*/NULL);
		__this->___entityDecls_4 = L_3;
		DTDParameterEntityDeclarationCollection_t4_85 * L_4 = (DTDParameterEntityDeclarationCollection_t4_85 *)il2cpp_codegen_object_new (DTDParameterEntityDeclarationCollection_t4_85_il2cpp_TypeInfo_var);
		DTDParameterEntityDeclarationCollection__ctor_m4_250(L_4, __this, /*hidden argument*/NULL);
		__this->___peDecls_3 = L_4;
		DTDNotationDeclarationCollection_t4_87 * L_5 = (DTDNotationDeclarationCollection_t4_87 *)il2cpp_codegen_object_new (DTDNotationDeclarationCollection_t4_87_il2cpp_TypeInfo_var);
		DTDNotationDeclarationCollection__ctor_m4_173(L_5, __this, /*hidden argument*/NULL);
		__this->___notationDecls_5 = L_5;
		DTDAutomataFactory_t4_81 * L_6 = (DTDAutomataFactory_t4_81 *)il2cpp_codegen_object_new (DTDAutomataFactory_t4_81_il2cpp_TypeInfo_var);
		DTDAutomataFactory__ctor_m4_117(L_6, __this, /*hidden argument*/NULL);
		__this->___factory_0 = L_6;
		ArrayList_t1_170 * L_7 = (ArrayList_t1_170 *)il2cpp_codegen_object_new (ArrayList_t1_170_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1_3049(L_7, /*hidden argument*/NULL);
		__this->___validationErrors_6 = L_7;
		Hashtable_t1_100 * L_8 = (Hashtable_t1_100 *)il2cpp_codegen_object_new (Hashtable_t1_100_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1_3279(L_8, /*hidden argument*/NULL);
		__this->___externalResources_9 = L_8;
		return;
	}
}
// System.String Mono.Xml.DTDObjectModel::get_BaseURI()
extern "C" String_t* DTDObjectModel_get_BaseURI_m4_119 (DTDObjectModel_t4_82 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___baseURI_10);
		return L_0;
	}
}
// System.Void Mono.Xml.DTDObjectModel::set_BaseURI(System.String)
extern "C" void DTDObjectModel_set_BaseURI_m4_120 (DTDObjectModel_t4_82 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___baseURI_10 = L_0;
		return;
	}
}
// System.Boolean Mono.Xml.DTDObjectModel::get_IsStandalone()
extern "C" bool DTDObjectModel_get_IsStandalone_m4_121 (DTDObjectModel_t4_82 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___isStandalone_16);
		return L_0;
	}
}
// System.Void Mono.Xml.DTDObjectModel::set_IsStandalone(System.Boolean)
extern "C" void DTDObjectModel_set_IsStandalone_m4_122 (DTDObjectModel_t4_82 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___isStandalone_16 = L_0;
		return;
	}
}
// System.String Mono.Xml.DTDObjectModel::get_Name()
extern "C" String_t* DTDObjectModel_get_Name_m4_123 (DTDObjectModel_t4_82 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___name_11);
		return L_0;
	}
}
// System.Void Mono.Xml.DTDObjectModel::set_Name(System.String)
extern "C" void DTDObjectModel_set_Name_m4_124 (DTDObjectModel_t4_82 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___name_11 = L_0;
		return;
	}
}
// System.Xml.XmlNameTable Mono.Xml.DTDObjectModel::get_NameTable()
extern "C" XmlNameTable_t4_67 * DTDObjectModel_get_NameTable_m4_125 (DTDObjectModel_t4_82 * __this, const MethodInfo* method)
{
	{
		XmlNameTable_t4_67 * L_0 = (__this->___nameTable_8);
		return L_0;
	}
}
// System.String Mono.Xml.DTDObjectModel::get_PublicId()
extern "C" String_t* DTDObjectModel_get_PublicId_m4_126 (DTDObjectModel_t4_82 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___publicId_12);
		return L_0;
	}
}
// System.Void Mono.Xml.DTDObjectModel::set_PublicId(System.String)
extern "C" void DTDObjectModel_set_PublicId_m4_127 (DTDObjectModel_t4_82 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___publicId_12 = L_0;
		return;
	}
}
// System.String Mono.Xml.DTDObjectModel::get_SystemId()
extern "C" String_t* DTDObjectModel_get_SystemId_m4_128 (DTDObjectModel_t4_82 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___systemId_13);
		return L_0;
	}
}
// System.Void Mono.Xml.DTDObjectModel::set_SystemId(System.String)
extern "C" void DTDObjectModel_set_SystemId_m4_129 (DTDObjectModel_t4_82 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___systemId_13 = L_0;
		return;
	}
}
// System.String Mono.Xml.DTDObjectModel::get_InternalSubset()
extern "C" String_t* DTDObjectModel_get_InternalSubset_m4_130 (DTDObjectModel_t4_82 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___intSubset_14);
		return L_0;
	}
}
// System.Void Mono.Xml.DTDObjectModel::set_InternalSubset(System.String)
extern "C" void DTDObjectModel_set_InternalSubset_m4_131 (DTDObjectModel_t4_82 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___intSubset_14 = L_0;
		return;
	}
}
// System.Boolean Mono.Xml.DTDObjectModel::get_InternalSubsetHasPEReference()
extern "C" bool DTDObjectModel_get_InternalSubsetHasPEReference_m4_132 (DTDObjectModel_t4_82 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___intSubsetHasPERef_15);
		return L_0;
	}
}
// System.Void Mono.Xml.DTDObjectModel::set_InternalSubsetHasPEReference(System.Boolean)
extern "C" void DTDObjectModel_set_InternalSubsetHasPEReference_m4_133 (DTDObjectModel_t4_82 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___intSubsetHasPERef_15 = L_0;
		return;
	}
}
// System.Int32 Mono.Xml.DTDObjectModel::get_LineNumber()
extern "C" int32_t DTDObjectModel_get_LineNumber_m4_134 (DTDObjectModel_t4_82 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___lineNumber_17);
		return L_0;
	}
}
// System.Void Mono.Xml.DTDObjectModel::set_LineNumber(System.Int32)
extern "C" void DTDObjectModel_set_LineNumber_m4_135 (DTDObjectModel_t4_82 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___lineNumber_17 = L_0;
		return;
	}
}
// System.Int32 Mono.Xml.DTDObjectModel::get_LinePosition()
extern "C" int32_t DTDObjectModel_get_LinePosition_m4_136 (DTDObjectModel_t4_82 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___linePosition_18);
		return L_0;
	}
}
// System.Void Mono.Xml.DTDObjectModel::set_LinePosition(System.Int32)
extern "C" void DTDObjectModel_set_LinePosition_m4_137 (DTDObjectModel_t4_82 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___linePosition_18 = L_0;
		return;
	}
}
// System.String Mono.Xml.DTDObjectModel::ResolveEntity(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* XmlException_t4_137_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3799;
extern Il2CppCodeGenString* _stringLiteral268;
extern "C" String_t* DTDObjectModel_ResolveEntity_m4_138 (DTDObjectModel_t4_82 * __this, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		XmlException_t4_137_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1285);
		_stringLiteral3799 = il2cpp_codegen_string_literal_from_index(3799);
		_stringLiteral268 = il2cpp_codegen_string_literal_from_index(268);
		s_Il2CppMethodIntialized = true;
	}
	DTDEntityDeclaration_t4_98 * V_0 = {0};
	{
		DTDEntityDeclarationCollection_t4_86 * L_0 = DTDObjectModel_get_EntityDecls_m4_144(__this, /*hidden argument*/NULL);
		String_t* L_1 = ___name;
		NullCheck(L_0);
		DTDEntityDeclaration_t4_98 * L_2 = DTDEntityDeclarationCollection_get_Item_m4_171(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		DTDEntityDeclaration_t4_98 * L_3 = V_0;
		if (L_3)
		{
			goto IL_003c;
		}
	}
	{
		String_t* L_4 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Format_m1_548(NULL /*static, unused*/, _stringLiteral3799, L_4, /*hidden argument*/NULL);
		int32_t L_6 = DTDObjectModel_get_LineNumber_m4_134(__this, /*hidden argument*/NULL);
		int32_t L_7 = DTDObjectModel_get_LinePosition_m4_136(__this, /*hidden argument*/NULL);
		XmlException_t4_137 * L_8 = (XmlException_t4_137 *)il2cpp_codegen_object_new (XmlException_t4_137_il2cpp_TypeInfo_var);
		XmlException__ctor_m4_526(L_8, L_5, (Exception_t1_33 *)NULL, L_6, L_7, /*hidden argument*/NULL);
		DTDObjectModel_AddError_m4_147(__this, L_8, /*hidden argument*/NULL);
		return _stringLiteral268;
	}

IL_003c:
	{
		DTDEntityDeclaration_t4_98 * L_9 = V_0;
		NullCheck(L_9);
		String_t* L_10 = DTDEntityDeclaration_get_EntityValue_m4_237(L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// System.Xml.XmlResolver Mono.Xml.DTDObjectModel::get_Resolver()
extern "C" XmlResolver_t4_68 * DTDObjectModel_get_Resolver_m4_139 (DTDObjectModel_t4_82 * __this, const MethodInfo* method)
{
	{
		XmlResolver_t4_68 * L_0 = (__this->___resolver_7);
		return L_0;
	}
}
// System.Void Mono.Xml.DTDObjectModel::set_XmlResolver(System.Xml.XmlResolver)
extern "C" void DTDObjectModel_set_XmlResolver_m4_140 (DTDObjectModel_t4_82 * __this, XmlResolver_t4_68 * ___value, const MethodInfo* method)
{
	{
		XmlResolver_t4_68 * L_0 = ___value;
		__this->___resolver_7 = L_0;
		return;
	}
}
// System.Collections.Hashtable Mono.Xml.DTDObjectModel::get_ExternalResources()
extern "C" Hashtable_t1_100 * DTDObjectModel_get_ExternalResources_m4_141 (DTDObjectModel_t4_82 * __this, const MethodInfo* method)
{
	{
		Hashtable_t1_100 * L_0 = (__this->___externalResources_9);
		return L_0;
	}
}
// Mono.Xml.DTDElementDeclarationCollection Mono.Xml.DTDObjectModel::get_ElementDecls()
extern "C" DTDElementDeclarationCollection_t4_83 * DTDObjectModel_get_ElementDecls_m4_142 (DTDObjectModel_t4_82 * __this, const MethodInfo* method)
{
	{
		DTDElementDeclarationCollection_t4_83 * L_0 = (__this->___elementDecls_1);
		return L_0;
	}
}
// Mono.Xml.DTDAttListDeclarationCollection Mono.Xml.DTDObjectModel::get_AttListDecls()
extern "C" DTDAttListDeclarationCollection_t4_84 * DTDObjectModel_get_AttListDecls_m4_143 (DTDObjectModel_t4_82 * __this, const MethodInfo* method)
{
	{
		DTDAttListDeclarationCollection_t4_84 * L_0 = (__this->___attListDecls_2);
		return L_0;
	}
}
// Mono.Xml.DTDEntityDeclarationCollection Mono.Xml.DTDObjectModel::get_EntityDecls()
extern "C" DTDEntityDeclarationCollection_t4_86 * DTDObjectModel_get_EntityDecls_m4_144 (DTDObjectModel_t4_82 * __this, const MethodInfo* method)
{
	{
		DTDEntityDeclarationCollection_t4_86 * L_0 = (__this->___entityDecls_4);
		return L_0;
	}
}
// Mono.Xml.DTDParameterEntityDeclarationCollection Mono.Xml.DTDObjectModel::get_PEDecls()
extern "C" DTDParameterEntityDeclarationCollection_t4_85 * DTDObjectModel_get_PEDecls_m4_145 (DTDObjectModel_t4_82 * __this, const MethodInfo* method)
{
	{
		DTDParameterEntityDeclarationCollection_t4_85 * L_0 = (__this->___peDecls_3);
		return L_0;
	}
}
// Mono.Xml.DTDNotationDeclarationCollection Mono.Xml.DTDObjectModel::get_NotationDecls()
extern "C" DTDNotationDeclarationCollection_t4_87 * DTDObjectModel_get_NotationDecls_m4_146 (DTDObjectModel_t4_82 * __this, const MethodInfo* method)
{
	{
		DTDNotationDeclarationCollection_t4_87 * L_0 = (__this->___notationDecls_5);
		return L_0;
	}
}
// System.Void Mono.Xml.DTDObjectModel::AddError(System.Xml.XmlException)
extern "C" void DTDObjectModel_AddError_m4_147 (DTDObjectModel_t4_82 * __this, XmlException_t4_137 * ___ex, const MethodInfo* method)
{
	{
		ArrayList_t1_170 * L_0 = (__this->___validationErrors_6);
		XmlException_t4_137 * L_1 = ___ex;
		NullCheck(L_0);
		VirtFuncInvoker1< int32_t, Object_t * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_0, L_1);
		return;
	}
}
// System.String Mono.Xml.DTDObjectModel::GenerateEntityAttributeText(System.String)
extern "C" String_t* DTDObjectModel_GenerateEntityAttributeText_m4_148 (DTDObjectModel_t4_82 * __this, String_t* ___entityName, const MethodInfo* method)
{
	DTDEntityDeclaration_t4_98 * V_0 = {0};
	{
		DTDEntityDeclarationCollection_t4_86 * L_0 = DTDObjectModel_get_EntityDecls_m4_144(__this, /*hidden argument*/NULL);
		String_t* L_1 = ___entityName;
		NullCheck(L_0);
		DTDEntityDeclaration_t4_98 * L_2 = DTDEntityDeclarationCollection_get_Item_m4_171(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		DTDEntityDeclaration_t4_98 * L_3 = V_0;
		if (L_3)
		{
			goto IL_0015;
		}
	}
	{
		return (String_t*)NULL;
	}

IL_0015:
	{
		DTDEntityDeclaration_t4_98 * L_4 = V_0;
		NullCheck(L_4);
		String_t* L_5 = DTDEntityDeclaration_get_EntityValue_m4_237(L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// Mono.Xml2.XmlTextReader Mono.Xml.DTDObjectModel::GenerateEntityContentReader(System.String,System.Xml.XmlParserContext)
extern const Il2CppType* Stream_t1_405_0_0_0_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Uri_t3_3_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Stream_t1_405_il2cpp_TypeInfo_var;
extern TypeInfo* XmlTextReader_t4_169_il2cpp_TypeInfo_var;
extern "C" XmlTextReader_t4_169 * DTDObjectModel_GenerateEntityContentReader_m4_149 (DTDObjectModel_t4_82 * __this, String_t* ___entityName, XmlParserContext_t4_157 * ___context, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Stream_t1_405_0_0_0_var = il2cpp_codegen_type_from_index(265);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Uri_t3_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Stream_t1_405_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(265);
		XmlTextReader_t4_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1287);
		s_Il2CppMethodIntialized = true;
	}
	DTDEntityDeclaration_t4_98 * V_0 = {0};
	Uri_t3_3 * V_1 = {0};
	Stream_t1_405 * V_2 = {0};
	Uri_t3_3 * G_B6_0 = {0};
	{
		DTDEntityDeclarationCollection_t4_86 * L_0 = DTDObjectModel_get_EntityDecls_m4_144(__this, /*hidden argument*/NULL);
		String_t* L_1 = ___entityName;
		NullCheck(L_0);
		DTDEntityDeclaration_t4_98 * L_2 = DTDEntityDeclarationCollection_get_Item_m4_171(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		DTDEntityDeclaration_t4_98 * L_3 = V_0;
		if (L_3)
		{
			goto IL_0015;
		}
	}
	{
		return (XmlTextReader_t4_169 *)NULL;
	}

IL_0015:
	{
		DTDEntityDeclaration_t4_98 * L_4 = V_0;
		NullCheck(L_4);
		String_t* L_5 = DTDEntityBase_get_SystemId_m4_224(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_007e;
		}
	}
	{
		DTDEntityDeclaration_t4_98 * L_6 = V_0;
		NullCheck(L_6);
		String_t* L_7 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String Mono.Xml.DTDNode::get_BaseURI() */, L_6);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		bool L_9 = String_op_Equality_m1_601(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_003b;
		}
	}
	{
		G_B6_0 = ((Uri_t3_3 *)(NULL));
		goto IL_0046;
	}

IL_003b:
	{
		DTDEntityDeclaration_t4_98 * L_10 = V_0;
		NullCheck(L_10);
		String_t* L_11 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String Mono.Xml.DTDNode::get_BaseURI() */, L_10);
		Uri_t3_3 * L_12 = (Uri_t3_3 *)il2cpp_codegen_object_new (Uri_t3_3_il2cpp_TypeInfo_var);
		Uri__ctor_m3_23(L_12, L_11, /*hidden argument*/NULL);
		G_B6_0 = L_12;
	}

IL_0046:
	{
		V_1 = G_B6_0;
		XmlResolver_t4_68 * L_13 = (__this->___resolver_7);
		XmlResolver_t4_68 * L_14 = (__this->___resolver_7);
		Uri_t3_3 * L_15 = V_1;
		DTDEntityDeclaration_t4_98 * L_16 = V_0;
		NullCheck(L_16);
		String_t* L_17 = DTDEntityBase_get_SystemId_m4_224(L_16, /*hidden argument*/NULL);
		NullCheck(L_14);
		Uri_t3_3 * L_18 = (Uri_t3_3 *)VirtFuncInvoker2< Uri_t3_3 *, Uri_t3_3 *, String_t* >::Invoke(5 /* System.Uri System.Xml.XmlResolver::ResolveUri(System.Uri,System.String) */, L_14, L_15, L_17);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_19 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(Stream_t1_405_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_13);
		Object_t * L_20 = (Object_t *)VirtFuncInvoker3< Object_t *, Uri_t3_3 *, String_t*, Type_t * >::Invoke(4 /* System.Object System.Xml.XmlResolver::GetEntity(System.Uri,System.String,System.Type) */, L_13, L_18, (String_t*)NULL, L_19);
		V_2 = ((Stream_t1_405 *)IsInstClass(L_20, Stream_t1_405_il2cpp_TypeInfo_var));
		Stream_t1_405 * L_21 = V_2;
		XmlParserContext_t4_157 * L_22 = ___context;
		XmlTextReader_t4_169 * L_23 = (XmlTextReader_t4_169 *)il2cpp_codegen_object_new (XmlTextReader_t4_169_il2cpp_TypeInfo_var);
		XmlTextReader__ctor_m4_796(L_23, L_21, 1, L_22, /*hidden argument*/NULL);
		return L_23;
	}

IL_007e:
	{
		DTDEntityDeclaration_t4_98 * L_24 = V_0;
		NullCheck(L_24);
		String_t* L_25 = DTDEntityDeclaration_get_EntityValue_m4_237(L_24, /*hidden argument*/NULL);
		XmlParserContext_t4_157 * L_26 = ___context;
		XmlTextReader_t4_169 * L_27 = (XmlTextReader_t4_169 *)il2cpp_codegen_object_new (XmlTextReader_t4_169_il2cpp_TypeInfo_var);
		XmlTextReader__ctor_m4_799(L_27, L_25, 1, L_26, /*hidden argument*/NULL);
		return L_27;
	}
}
// System.Void Mono.Xml.DictionaryBase/<>c__Iterator3::.ctor()
extern "C" void U3CU3Ec__Iterator3__ctor_m4_150 (U3CU3Ec__Iterator3_t4_88 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// Mono.Xml.DTDNode Mono.Xml.DictionaryBase/<>c__Iterator3::System.Collections.Generic.IEnumerator<Mono.Xml.DTDNode>.get_Current()
extern "C" DTDNode_t4_89 * U3CU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CMono_Xml_DTDNodeU3E_get_Current_m4_151 (U3CU3Ec__Iterator3_t4_88 * __this, const MethodInfo* method)
{
	{
		DTDNode_t4_89 * L_0 = (__this->___U24current_3);
		return L_0;
	}
}
// System.Object Mono.Xml.DictionaryBase/<>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m4_152 (U3CU3Ec__Iterator3_t4_88 * __this, const MethodInfo* method)
{
	{
		DTDNode_t4_89 * L_0 = (__this->___U24current_3);
		return L_0;
	}
}
// System.Collections.IEnumerator Mono.Xml.DictionaryBase/<>c__Iterator3::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * U3CU3Ec__Iterator3_System_Collections_IEnumerable_GetEnumerator_m4_153 (U3CU3Ec__Iterator3_t4_88 * __this, const MethodInfo* method)
{
	{
		Object_t* L_0 = U3CU3Ec__Iterator3_System_Collections_Generic_IEnumerableU3CMono_Xml_DTDNodeU3E_GetEnumerator_m4_154(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<Mono.Xml.DTDNode> Mono.Xml.DictionaryBase/<>c__Iterator3::System.Collections.Generic.IEnumerable<Mono.Xml.DTDNode>.GetEnumerator()
extern TypeInfo* U3CU3Ec__Iterator3_t4_88_il2cpp_TypeInfo_var;
extern "C" Object_t* U3CU3Ec__Iterator3_System_Collections_Generic_IEnumerableU3CMono_Xml_DTDNodeU3E_GetEnumerator_m4_154 (U3CU3Ec__Iterator3_t4_88 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CU3Ec__Iterator3_t4_88_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1288);
		s_Il2CppMethodIntialized = true;
	}
	U3CU3Ec__Iterator3_t4_88 * V_0 = {0};
	{
		int32_t* L_0 = &(__this->___U24PC_2);
		int32_t L_1 = Interlocked_CompareExchange_m1_12664(NULL /*static, unused*/, L_0, 0, ((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CU3Ec__Iterator3_t4_88 * L_2 = (U3CU3Ec__Iterator3_t4_88 *)il2cpp_codegen_object_new (U3CU3Ec__Iterator3_t4_88_il2cpp_TypeInfo_var);
		U3CU3Ec__Iterator3__ctor_m4_150(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		U3CU3Ec__Iterator3_t4_88 * L_3 = V_0;
		DictionaryBase_t4_90 * L_4 = (__this->___U3CU3Ef__this_4);
		NullCheck(L_3);
		L_3->___U3CU3Ef__this_4 = L_4;
		U3CU3Ec__Iterator3_t4_88 * L_5 = V_0;
		return L_5;
	}
}
// System.Boolean Mono.Xml.DictionaryBase/<>c__Iterator3::MoveNext()
extern TypeInfo* Enumerator_t1_1804_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m1_14920_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1_14921_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m1_14922_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1_14923_MethodInfo_var;
extern "C" bool U3CU3Ec__Iterator3_MoveNext_m4_155 (U3CU3Ec__Iterator3_t4_88 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Enumerator_t1_1804_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1291);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		List_1_GetEnumerator_m1_14920_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483727);
		Enumerator_get_Current_m1_14921_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483728);
		KeyValuePair_2_get_Value_m1_14922_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483729);
		Enumerator_MoveNext_m1_14923_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483730);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (__this->___U24PC_2);
		V_0 = L_0;
		__this->___U24PC_2 = (-1);
		V_1 = 0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0023;
		}
		if (L_1 == 1)
		{
			goto IL_0037;
		}
	}
	{
		goto IL_00a9;
	}

IL_0023:
	{
		DictionaryBase_t4_90 * L_2 = (__this->___U3CU3Ef__this_4);
		NullCheck(L_2);
		Enumerator_t1_1804  L_3 = List_1_GetEnumerator_m1_14920(L_2, /*hidden argument*/List_1_GetEnumerator_m1_14920_MethodInfo_var);
		__this->___U3CU24s_431U3E__0_0 = L_3;
		V_0 = ((int32_t)-3);
	}

IL_0037:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_4 = V_0;
			if (((int32_t)((int32_t)L_4-(int32_t)1)) == 0)
			{
				goto IL_0078;
			}
		}

IL_0043:
		{
			goto IL_0078;
		}

IL_0048:
		{
			Enumerator_t1_1804 * L_5 = &(__this->___U3CU24s_431U3E__0_0);
			KeyValuePair_2_t1_1805  L_6 = Enumerator_get_Current_m1_14921(L_5, /*hidden argument*/Enumerator_get_Current_m1_14921_MethodInfo_var);
			__this->___U3CpU3E__1_1 = L_6;
			KeyValuePair_2_t1_1805 * L_7 = &(__this->___U3CpU3E__1_1);
			DTDNode_t4_89 * L_8 = KeyValuePair_2_get_Value_m1_14922(L_7, /*hidden argument*/KeyValuePair_2_get_Value_m1_14922_MethodInfo_var);
			__this->___U24current_3 = L_8;
			__this->___U24PC_2 = 1;
			V_1 = 1;
			IL2CPP_LEAVE(0xAB, FINALLY_008d);
		}

IL_0078:
		{
			Enumerator_t1_1804 * L_9 = &(__this->___U3CU24s_431U3E__0_0);
			bool L_10 = Enumerator_MoveNext_m1_14923(L_9, /*hidden argument*/Enumerator_MoveNext_m1_14923_MethodInfo_var);
			if (L_10)
			{
				goto IL_0048;
			}
		}

IL_0088:
		{
			IL2CPP_LEAVE(0xA2, FINALLY_008d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_008d;
	}

FINALLY_008d:
	{ // begin finally (depth: 1)
		{
			bool L_11 = V_1;
			if (!L_11)
			{
				goto IL_0091;
			}
		}

IL_0090:
		{
			IL2CPP_END_FINALLY(141)
		}

IL_0091:
		{
			Enumerator_t1_1804  L_12 = (__this->___U3CU24s_431U3E__0_0);
			Enumerator_t1_1804  L_13 = L_12;
			Object_t * L_14 = Box(Enumerator_t1_1804_il2cpp_TypeInfo_var, &L_13);
			NullCheck(L_14);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_14);
			IL2CPP_END_FINALLY(141)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(141)
	{
		IL2CPP_JUMP_TBL(0xAB, IL_00ab)
		IL2CPP_JUMP_TBL(0xA2, IL_00a2)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_00a2:
	{
		__this->___U24PC_2 = (-1);
	}

IL_00a9:
	{
		return 0;
	}

IL_00ab:
	{
		return 1;
	}
	// Dead block : IL_00ad: ldloc.2
}
// System.Void Mono.Xml.DictionaryBase/<>c__Iterator3::Dispose()
extern TypeInfo* Enumerator_t1_1804_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern "C" void U3CU3Ec__Iterator3_Dispose_m4_156 (U3CU3Ec__Iterator3_t4_88 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Enumerator_t1_1804_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1291);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (__this->___U24PC_2);
		V_0 = L_0;
		__this->___U24PC_2 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0037;
		}
		if (L_1 == 1)
		{
			goto IL_0021;
		}
	}
	{
		goto IL_0037;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x37, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		Enumerator_t1_1804  L_2 = (__this->___U3CU24s_431U3E__0_0);
		Enumerator_t1_1804  L_3 = L_2;
		Object_t * L_4 = Box(Enumerator_t1_1804_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_4);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_4);
		IL2CPP_END_FINALLY(38)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x37, IL_0037)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0037:
	{
		return;
	}
}
// System.Void Mono.Xml.DictionaryBase/<>c__Iterator3::Reset()
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern "C" void U3CU3Ec__Iterator3_Reset_m4_157 (U3CU3Ec__Iterator3_t4_88 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14416(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void Mono.Xml.DictionaryBase::.ctor()
extern TypeInfo* List_1_t1_1806_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1_14924_MethodInfo_var;
extern "C" void DictionaryBase__ctor_m4_158 (DictionaryBase_t4_90 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t1_1806_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1292);
		List_1__ctor_m1_14924_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483731);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(List_1_t1_1806_il2cpp_TypeInfo_var);
		List_1__ctor_m1_14924(__this, /*hidden argument*/List_1__ctor_m1_14924_MethodInfo_var);
		return;
	}
}
// System.Collections.Generic.IEnumerable`1<Mono.Xml.DTDNode> Mono.Xml.DictionaryBase::get_Values()
extern TypeInfo* U3CU3Ec__Iterator3_t4_88_il2cpp_TypeInfo_var;
extern "C" Object_t* DictionaryBase_get_Values_m4_159 (DictionaryBase_t4_90 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CU3Ec__Iterator3_t4_88_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1288);
		s_Il2CppMethodIntialized = true;
	}
	U3CU3Ec__Iterator3_t4_88 * V_0 = {0};
	{
		U3CU3Ec__Iterator3_t4_88 * L_0 = (U3CU3Ec__Iterator3_t4_88 *)il2cpp_codegen_object_new (U3CU3Ec__Iterator3_t4_88_il2cpp_TypeInfo_var);
		U3CU3Ec__Iterator3__ctor_m4_150(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__Iterator3_t4_88 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3Ef__this_4 = __this;
		U3CU3Ec__Iterator3_t4_88 * L_2 = V_0;
		U3CU3Ec__Iterator3_t4_88 * L_3 = L_2;
		NullCheck(L_3);
		L_3->___U24PC_2 = ((int32_t)-2);
		return L_3;
	}
}
// System.Void Mono.Xml.DTDCollectionBase::.ctor(Mono.Xml.DTDObjectModel)
extern "C" void DTDCollectionBase__ctor_m4_160 (DTDCollectionBase_t4_91 * __this, DTDObjectModel_t4_82 * ___root, const MethodInfo* method)
{
	{
		DictionaryBase__ctor_m4_158(__this, /*hidden argument*/NULL);
		DTDObjectModel_t4_82 * L_0 = ___root;
		__this->___root_5 = L_0;
		return;
	}
}
// Mono.Xml.DTDObjectModel Mono.Xml.DTDCollectionBase::get_Root()
extern "C" DTDObjectModel_t4_82 * DTDCollectionBase_get_Root_m4_161 (DTDCollectionBase_t4_91 * __this, const MethodInfo* method)
{
	{
		DTDObjectModel_t4_82 * L_0 = (__this->___root_5);
		return L_0;
	}
}
// System.Void Mono.Xml.DTDCollectionBase::BaseAdd(System.String,Mono.Xml.DTDNode)
extern const MethodInfo* KeyValuePair_2__ctor_m1_14925_MethodInfo_var;
extern const MethodInfo* List_1_Add_m1_14926_MethodInfo_var;
extern "C" void DTDCollectionBase_BaseAdd_m4_162 (DTDCollectionBase_t4_91 * __this, String_t* ___name, DTDNode_t4_89 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		KeyValuePair_2__ctor_m1_14925_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483732);
		List_1_Add_m1_14926_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483733);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___name;
		DTDNode_t4_89 * L_1 = ___value;
		KeyValuePair_2_t1_1805  L_2 = {0};
		KeyValuePair_2__ctor_m1_14925(&L_2, L_0, L_1, /*hidden argument*/KeyValuePair_2__ctor_m1_14925_MethodInfo_var);
		List_1_Add_m1_14926(__this, L_2, /*hidden argument*/List_1_Add_m1_14926_MethodInfo_var);
		return;
	}
}
// System.Boolean Mono.Xml.DTDCollectionBase::Contains(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t1_1804_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m1_14920_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1_14921_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m1_14927_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1_14923_MethodInfo_var;
extern "C" bool DTDCollectionBase_Contains_m4_163 (DTDCollectionBase_t4_91 * __this, String_t* ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Enumerator_t1_1804_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1291);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		List_1_GetEnumerator_m1_14920_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483727);
		Enumerator_get_Current_m1_14921_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483728);
		KeyValuePair_2_get_Key_m1_14927_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483734);
		Enumerator_MoveNext_m1_14923_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483730);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2_t1_1805  V_0 = {0};
	Enumerator_t1_1804  V_1 = {0};
	bool V_2 = false;
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Enumerator_t1_1804  L_0 = List_1_GetEnumerator_m1_14920(__this, /*hidden argument*/List_1_GetEnumerator_m1_14920_MethodInfo_var);
		V_1 = L_0;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_002d;
		}

IL_000c:
		{
			KeyValuePair_2_t1_1805  L_1 = Enumerator_get_Current_m1_14921((&V_1), /*hidden argument*/Enumerator_get_Current_m1_14921_MethodInfo_var);
			V_0 = L_1;
			String_t* L_2 = KeyValuePair_2_get_Key_m1_14927((&V_0), /*hidden argument*/KeyValuePair_2_get_Key_m1_14927_MethodInfo_var);
			String_t* L_3 = ___key;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_4 = String_op_Equality_m1_601(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
			if (!L_4)
			{
				goto IL_002d;
			}
		}

IL_0026:
		{
			V_2 = 1;
			IL2CPP_LEAVE(0x4C, FINALLY_003e);
		}

IL_002d:
		{
			bool L_5 = Enumerator_MoveNext_m1_14923((&V_1), /*hidden argument*/Enumerator_MoveNext_m1_14923_MethodInfo_var);
			if (L_5)
			{
				goto IL_000c;
			}
		}

IL_0039:
		{
			IL2CPP_LEAVE(0x4A, FINALLY_003e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_003e;
	}

FINALLY_003e:
	{ // begin finally (depth: 1)
		Enumerator_t1_1804  L_6 = V_1;
		Enumerator_t1_1804  L_7 = L_6;
		Object_t * L_8 = Box(Enumerator_t1_1804_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_8);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_8);
		IL2CPP_END_FINALLY(62)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(62)
	{
		IL2CPP_JUMP_TBL(0x4C, IL_004c)
		IL2CPP_JUMP_TBL(0x4A, IL_004a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_004a:
	{
		return 0;
	}

IL_004c:
	{
		bool L_9 = V_2;
		return L_9;
	}
}
// System.Object Mono.Xml.DTDCollectionBase::BaseGet(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t1_1804_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m1_14920_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1_14921_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m1_14927_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m1_14922_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1_14923_MethodInfo_var;
extern "C" Object_t * DTDCollectionBase_BaseGet_m4_164 (DTDCollectionBase_t4_91 * __this, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Enumerator_t1_1804_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1291);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		List_1_GetEnumerator_m1_14920_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483727);
		Enumerator_get_Current_m1_14921_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483728);
		KeyValuePair_2_get_Key_m1_14927_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483734);
		KeyValuePair_2_get_Value_m1_14922_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483729);
		Enumerator_MoveNext_m1_14923_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483730);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2_t1_1805  V_0 = {0};
	Enumerator_t1_1804  V_1 = {0};
	Object_t * V_2 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Enumerator_t1_1804  L_0 = List_1_GetEnumerator_m1_14920(__this, /*hidden argument*/List_1_GetEnumerator_m1_14920_MethodInfo_var);
		V_1 = L_0;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0033;
		}

IL_000c:
		{
			KeyValuePair_2_t1_1805  L_1 = Enumerator_get_Current_m1_14921((&V_1), /*hidden argument*/Enumerator_get_Current_m1_14921_MethodInfo_var);
			V_0 = L_1;
			String_t* L_2 = KeyValuePair_2_get_Key_m1_14927((&V_0), /*hidden argument*/KeyValuePair_2_get_Key_m1_14927_MethodInfo_var);
			String_t* L_3 = ___name;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_4 = String_op_Equality_m1_601(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
			if (!L_4)
			{
				goto IL_0033;
			}
		}

IL_0026:
		{
			DTDNode_t4_89 * L_5 = KeyValuePair_2_get_Value_m1_14922((&V_0), /*hidden argument*/KeyValuePair_2_get_Value_m1_14922_MethodInfo_var);
			V_2 = L_5;
			IL2CPP_LEAVE(0x52, FINALLY_0044);
		}

IL_0033:
		{
			bool L_6 = Enumerator_MoveNext_m1_14923((&V_1), /*hidden argument*/Enumerator_MoveNext_m1_14923_MethodInfo_var);
			if (L_6)
			{
				goto IL_000c;
			}
		}

IL_003f:
		{
			IL2CPP_LEAVE(0x50, FINALLY_0044);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0044;
	}

FINALLY_0044:
	{ // begin finally (depth: 1)
		Enumerator_t1_1804  L_7 = V_1;
		Enumerator_t1_1804  L_8 = L_7;
		Object_t * L_9 = Box(Enumerator_t1_1804_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_9);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_9);
		IL2CPP_END_FINALLY(68)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(68)
	{
		IL2CPP_JUMP_TBL(0x52, IL_0052)
		IL2CPP_JUMP_TBL(0x50, IL_0050)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0050:
	{
		return NULL;
	}

IL_0052:
	{
		Object_t * L_10 = V_2;
		return L_10;
	}
}
// System.Void Mono.Xml.DTDElementDeclarationCollection::.ctor(Mono.Xml.DTDObjectModel)
extern "C" void DTDElementDeclarationCollection__ctor_m4_165 (DTDElementDeclarationCollection_t4_83 * __this, DTDObjectModel_t4_82 * ___root, const MethodInfo* method)
{
	{
		DTDObjectModel_t4_82 * L_0 = ___root;
		DTDCollectionBase__ctor_m4_160(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml.DTDElementDeclarationCollection::Add(System.String,Mono.Xml.DTDElementDeclaration)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* XmlException_t4_137_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3800;
extern "C" void DTDElementDeclarationCollection_Add_m4_166 (DTDElementDeclarationCollection_t4_83 * __this, String_t* ___name, DTDElementDeclaration_t4_94 * ___decl, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		XmlException_t4_137_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1285);
		_stringLiteral3800 = il2cpp_codegen_string_literal_from_index(3800);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___name;
		bool L_1 = DTDCollectionBase_Contains_m4_163(__this, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0029;
		}
	}
	{
		DTDObjectModel_t4_82 * L_2 = DTDCollectionBase_get_Root_m4_161(__this, /*hidden argument*/NULL);
		String_t* L_3 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Format_m1_548(NULL /*static, unused*/, _stringLiteral3800, L_3, /*hidden argument*/NULL);
		XmlException_t4_137 * L_5 = (XmlException_t4_137 *)il2cpp_codegen_object_new (XmlException_t4_137_il2cpp_TypeInfo_var);
		XmlException__ctor_m4_521(L_5, L_4, (Exception_t1_33 *)NULL, /*hidden argument*/NULL);
		NullCheck(L_2);
		DTDObjectModel_AddError_m4_147(L_2, L_5, /*hidden argument*/NULL);
		return;
	}

IL_0029:
	{
		DTDElementDeclaration_t4_94 * L_6 = ___decl;
		DTDObjectModel_t4_82 * L_7 = DTDCollectionBase_get_Root_m4_161(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		DTDNode_SetRoot_m4_193(L_6, L_7, /*hidden argument*/NULL);
		String_t* L_8 = ___name;
		DTDElementDeclaration_t4_94 * L_9 = ___decl;
		DTDCollectionBase_BaseAdd_m4_162(__this, L_8, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml.DTDAttListDeclarationCollection::.ctor(Mono.Xml.DTDObjectModel)
extern "C" void DTDAttListDeclarationCollection__ctor_m4_167 (DTDAttListDeclarationCollection_t4_84 * __this, DTDObjectModel_t4_82 * ___root, const MethodInfo* method)
{
	{
		DTDObjectModel_t4_82 * L_0 = ___root;
		DTDCollectionBase__ctor_m4_160(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// Mono.Xml.DTDAttListDeclaration Mono.Xml.DTDAttListDeclarationCollection::get_Item(System.String)
extern TypeInfo* DTDAttListDeclaration_t4_96_il2cpp_TypeInfo_var;
extern "C" DTDAttListDeclaration_t4_96 * DTDAttListDeclarationCollection_get_Item_m4_168 (DTDAttListDeclarationCollection_t4_84 * __this, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DTDAttListDeclaration_t4_96_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1293);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___name;
		Object_t * L_1 = DTDCollectionBase_BaseGet_m4_164(__this, L_0, /*hidden argument*/NULL);
		return ((DTDAttListDeclaration_t4_96 *)IsInstClass(L_1, DTDAttListDeclaration_t4_96_il2cpp_TypeInfo_var));
	}
}
// System.Void Mono.Xml.DTDAttListDeclarationCollection::Add(System.String,Mono.Xml.DTDAttListDeclaration)
extern TypeInfo* IEnumerable_t1_1677_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t1_193_il2cpp_TypeInfo_var;
extern TypeInfo* DTDAttributeDefinition_t4_95_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern "C" void DTDAttListDeclarationCollection_Add_m4_169 (DTDAttListDeclarationCollection_t4_84 * __this, String_t* ___name, DTDAttListDeclaration_t4_96 * ___decl, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IEnumerable_t1_1677_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(253);
		IEnumerator_t1_193_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(144);
		DTDAttributeDefinition_t4_95_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1294);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		s_Il2CppMethodIntialized = true;
	}
	DTDAttListDeclaration_t4_96 * V_0 = {0};
	DTDAttributeDefinition_t4_95 * V_1 = {0};
	Object_t * V_2 = {0};
	Object_t * V_3 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___name;
		DTDAttListDeclaration_t4_96 * L_1 = DTDAttListDeclarationCollection_get_Item_m4_168(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		DTDAttListDeclaration_t4_96 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_006a;
		}
	}
	{
		DTDAttListDeclaration_t4_96 * L_3 = ___decl;
		NullCheck(L_3);
		Object_t * L_4 = DTDAttListDeclaration_get_Definitions_m4_215(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Object_t * L_5 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t1_1677_il2cpp_TypeInfo_var, L_4);
		V_2 = L_5;
	}

IL_001a:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0043;
		}

IL_001f:
		{
			Object_t * L_6 = V_2;
			NullCheck(L_6);
			Object_t * L_7 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_6);
			V_1 = ((DTDAttributeDefinition_t4_95 *)CastclassClass(L_7, DTDAttributeDefinition_t4_95_il2cpp_TypeInfo_var));
			DTDAttListDeclaration_t4_96 * L_8 = ___decl;
			DTDAttributeDefinition_t4_95 * L_9 = V_1;
			NullCheck(L_9);
			String_t* L_10 = DTDAttributeDefinition_get_Name_m4_203(L_9, /*hidden argument*/NULL);
			NullCheck(L_8);
			DTDAttributeDefinition_t4_95 * L_11 = DTDAttListDeclaration_Get_m4_214(L_8, L_10, /*hidden argument*/NULL);
			if (L_11)
			{
				goto IL_0043;
			}
		}

IL_003c:
		{
			DTDAttListDeclaration_t4_96 * L_12 = V_0;
			DTDAttributeDefinition_t4_95 * L_13 = V_1;
			NullCheck(L_12);
			DTDAttListDeclaration_Add_m4_216(L_12, L_13, /*hidden argument*/NULL);
		}

IL_0043:
		{
			Object_t * L_14 = V_2;
			NullCheck(L_14);
			bool L_15 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_14);
			if (L_15)
			{
				goto IL_001f;
			}
		}

IL_004e:
		{
			IL2CPP_LEAVE(0x65, FINALLY_0053);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0053;
	}

FINALLY_0053:
	{ // begin finally (depth: 1)
		{
			Object_t * L_16 = V_2;
			V_3 = ((Object_t *)IsInst(L_16, IDisposable_t1_1035_il2cpp_TypeInfo_var));
			Object_t * L_17 = V_3;
			if (L_17)
			{
				goto IL_005e;
			}
		}

IL_005d:
		{
			IL2CPP_END_FINALLY(83)
		}

IL_005e:
		{
			Object_t * L_18 = V_3;
			NullCheck(L_18);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_18);
			IL2CPP_END_FINALLY(83)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(83)
	{
		IL2CPP_JUMP_TBL(0x65, IL_0065)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0065:
	{
		goto IL_007e;
	}

IL_006a:
	{
		DTDAttListDeclaration_t4_96 * L_19 = ___decl;
		DTDObjectModel_t4_82 * L_20 = DTDCollectionBase_get_Root_m4_161(__this, /*hidden argument*/NULL);
		NullCheck(L_19);
		DTDNode_SetRoot_m4_193(L_19, L_20, /*hidden argument*/NULL);
		String_t* L_21 = ___name;
		DTDAttListDeclaration_t4_96 * L_22 = ___decl;
		DTDCollectionBase_BaseAdd_m4_162(__this, L_21, L_22, /*hidden argument*/NULL);
	}

IL_007e:
	{
		return;
	}
}
// System.Void Mono.Xml.DTDEntityDeclarationCollection::.ctor(Mono.Xml.DTDObjectModel)
extern "C" void DTDEntityDeclarationCollection__ctor_m4_170 (DTDEntityDeclarationCollection_t4_86 * __this, DTDObjectModel_t4_82 * ___root, const MethodInfo* method)
{
	{
		DTDObjectModel_t4_82 * L_0 = ___root;
		DTDCollectionBase__ctor_m4_160(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// Mono.Xml.DTDEntityDeclaration Mono.Xml.DTDEntityDeclarationCollection::get_Item(System.String)
extern TypeInfo* DTDEntityDeclaration_t4_98_il2cpp_TypeInfo_var;
extern "C" DTDEntityDeclaration_t4_98 * DTDEntityDeclarationCollection_get_Item_m4_171 (DTDEntityDeclarationCollection_t4_86 * __this, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DTDEntityDeclaration_t4_98_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1295);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___name;
		Object_t * L_1 = DTDCollectionBase_BaseGet_m4_164(__this, L_0, /*hidden argument*/NULL);
		return ((DTDEntityDeclaration_t4_98 *)IsInstClass(L_1, DTDEntityDeclaration_t4_98_il2cpp_TypeInfo_var));
	}
}
// System.Void Mono.Xml.DTDEntityDeclarationCollection::Add(System.String,Mono.Xml.DTDEntityDeclaration)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidOperationException_t1_1559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3801;
extern "C" void DTDEntityDeclarationCollection_Add_m4_172 (DTDEntityDeclarationCollection_t4_86 * __this, String_t* ___name, DTDEntityDeclaration_t4_98 * ___decl, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		InvalidOperationException_t1_1559_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(78);
		_stringLiteral3801 = il2cpp_codegen_string_literal_from_index(3801);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___name;
		bool L_1 = DTDCollectionBase_Contains_m4_163(__this, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		String_t* L_2 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Format_m1_548(NULL /*static, unused*/, _stringLiteral3801, L_2, /*hidden argument*/NULL);
		InvalidOperationException_t1_1559 * L_4 = (InvalidOperationException_t1_1559 *)il2cpp_codegen_object_new (InvalidOperationException_t1_1559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1_14171(L_4, L_3, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
	}

IL_001d:
	{
		DTDEntityDeclaration_t4_98 * L_5 = ___decl;
		DTDObjectModel_t4_82 * L_6 = DTDCollectionBase_get_Root_m4_161(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		DTDNode_SetRoot_m4_193(L_5, L_6, /*hidden argument*/NULL);
		String_t* L_7 = ___name;
		DTDEntityDeclaration_t4_98 * L_8 = ___decl;
		DTDCollectionBase_BaseAdd_m4_162(__this, L_7, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml.DTDNotationDeclarationCollection::.ctor(Mono.Xml.DTDObjectModel)
extern "C" void DTDNotationDeclarationCollection__ctor_m4_173 (DTDNotationDeclarationCollection_t4_87 * __this, DTDObjectModel_t4_82 * ___root, const MethodInfo* method)
{
	{
		DTDObjectModel_t4_82 * L_0 = ___root;
		DTDCollectionBase__ctor_m4_160(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml.DTDNotationDeclarationCollection::Add(System.String,Mono.Xml.DTDNotationDeclaration)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidOperationException_t1_1559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3802;
extern "C" void DTDNotationDeclarationCollection_Add_m4_174 (DTDNotationDeclarationCollection_t4_87 * __this, String_t* ___name, DTDNotationDeclaration_t4_99 * ___decl, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		InvalidOperationException_t1_1559_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(78);
		_stringLiteral3802 = il2cpp_codegen_string_literal_from_index(3802);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___name;
		bool L_1 = DTDCollectionBase_Contains_m4_163(__this, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		String_t* L_2 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Format_m1_548(NULL /*static, unused*/, _stringLiteral3802, L_2, /*hidden argument*/NULL);
		InvalidOperationException_t1_1559 * L_4 = (InvalidOperationException_t1_1559 *)il2cpp_codegen_object_new (InvalidOperationException_t1_1559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1_14171(L_4, L_3, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
	}

IL_001d:
	{
		DTDNotationDeclaration_t4_99 * L_5 = ___decl;
		DTDObjectModel_t4_82 * L_6 = DTDCollectionBase_get_Root_m4_161(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		DTDNode_SetRoot_m4_193(L_5, L_6, /*hidden argument*/NULL);
		String_t* L_7 = ___name;
		DTDNotationDeclaration_t4_99 * L_8 = ___decl;
		DTDCollectionBase_BaseAdd_m4_162(__this, L_7, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml.DTDContentModel::.ctor(Mono.Xml.DTDObjectModel,System.String)
extern TypeInfo* DTDContentModelCollection_t4_93_il2cpp_TypeInfo_var;
extern "C" void DTDContentModel__ctor_m4_175 (DTDContentModel_t4_92 * __this, DTDObjectModel_t4_82 * ___root, String_t* ___ownerElementName, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DTDContentModelCollection_t4_93_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1296);
		s_Il2CppMethodIntialized = true;
	}
	{
		DTDContentModelCollection_t4_93 * L_0 = (DTDContentModelCollection_t4_93 *)il2cpp_codegen_object_new (DTDContentModelCollection_t4_93_il2cpp_TypeInfo_var);
		DTDContentModelCollection__ctor_m4_182(L_0, /*hidden argument*/NULL);
		__this->___childModels_9 = L_0;
		DTDNode__ctor_m4_186(__this, /*hidden argument*/NULL);
		DTDObjectModel_t4_82 * L_1 = ___root;
		__this->___root_5 = L_1;
		String_t* L_2 = ___ownerElementName;
		__this->___ownerElementName_6 = L_2;
		return;
	}
}
// Mono.Xml.DTDContentModelCollection Mono.Xml.DTDContentModel::get_ChildModels()
extern "C" DTDContentModelCollection_t4_93 * DTDContentModel_get_ChildModels_m4_176 (DTDContentModel_t4_92 * __this, const MethodInfo* method)
{
	{
		DTDContentModelCollection_t4_93 * L_0 = (__this->___childModels_9);
		return L_0;
	}
}
// System.String Mono.Xml.DTDContentModel::get_ElementName()
extern "C" String_t* DTDContentModel_get_ElementName_m4_177 (DTDContentModel_t4_92 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___elementName_7);
		return L_0;
	}
}
// System.Void Mono.Xml.DTDContentModel::set_ElementName(System.String)
extern "C" void DTDContentModel_set_ElementName_m4_178 (DTDContentModel_t4_92 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___elementName_7 = L_0;
		return;
	}
}
// System.Void Mono.Xml.DTDContentModel::set_Occurence(Mono.Xml.DTDOccurence)
extern "C" void DTDContentModel_set_Occurence_m4_179 (DTDContentModel_t4_92 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___occurence_10 = L_0;
		return;
	}
}
// Mono.Xml.DTDContentOrderType Mono.Xml.DTDContentModel::get_OrderType()
extern "C" int32_t DTDContentModel_get_OrderType_m4_180 (DTDContentModel_t4_92 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___orderType_8);
		return L_0;
	}
}
// System.Void Mono.Xml.DTDContentModel::set_OrderType(Mono.Xml.DTDContentOrderType)
extern "C" void DTDContentModel_set_OrderType_m4_181 (DTDContentModel_t4_92 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___orderType_8 = L_0;
		return;
	}
}
// System.Void Mono.Xml.DTDContentModelCollection::.ctor()
extern TypeInfo* ArrayList_t1_170_il2cpp_TypeInfo_var;
extern "C" void DTDContentModelCollection__ctor_m4_182 (DTDContentModelCollection_t4_93 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArrayList_t1_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(60);
		s_Il2CppMethodIntialized = true;
	}
	{
		ArrayList_t1_170 * L_0 = (ArrayList_t1_170 *)il2cpp_codegen_object_new (ArrayList_t1_170_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1_3049(L_0, /*hidden argument*/NULL);
		__this->___contentModel_0 = L_0;
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// Mono.Xml.DTDContentModel Mono.Xml.DTDContentModelCollection::get_Item(System.Int32)
extern TypeInfo* DTDContentModel_t4_92_il2cpp_TypeInfo_var;
extern "C" DTDContentModel_t4_92 * DTDContentModelCollection_get_Item_m4_183 (DTDContentModelCollection_t4_93 * __this, int32_t ___i, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DTDContentModel_t4_92_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1297);
		s_Il2CppMethodIntialized = true;
	}
	{
		ArrayList_t1_170 * L_0 = (__this->___contentModel_0);
		int32_t L_1 = ___i;
		NullCheck(L_0);
		Object_t * L_2 = (Object_t *)VirtFuncInvoker1< Object_t *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_0, L_1);
		return ((DTDContentModel_t4_92 *)IsInstClass(L_2, DTDContentModel_t4_92_il2cpp_TypeInfo_var));
	}
}
// System.Int32 Mono.Xml.DTDContentModelCollection::get_Count()
extern "C" int32_t DTDContentModelCollection_get_Count_m4_184 (DTDContentModelCollection_t4_93 * __this, const MethodInfo* method)
{
	{
		ArrayList_t1_170 * L_0 = (__this->___contentModel_0);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_0);
		return L_1;
	}
}
// System.Void Mono.Xml.DTDContentModelCollection::Add(Mono.Xml.DTDContentModel)
extern "C" void DTDContentModelCollection_Add_m4_185 (DTDContentModelCollection_t4_93 * __this, DTDContentModel_t4_92 * ___model, const MethodInfo* method)
{
	{
		ArrayList_t1_170 * L_0 = (__this->___contentModel_0);
		DTDContentModel_t4_92 * L_1 = ___model;
		NullCheck(L_0);
		VirtFuncInvoker1< int32_t, Object_t * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_0, L_1);
		return;
	}
}
// System.Void Mono.Xml.DTDNode::.ctor()
extern "C" void DTDNode__ctor_m4_186 (DTDNode_t4_89 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String Mono.Xml.DTDNode::get_BaseURI()
extern "C" String_t* DTDNode_get_BaseURI_m4_187 (DTDNode_t4_89 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___baseURI_2);
		return L_0;
	}
}
// System.Void Mono.Xml.DTDNode::set_BaseURI(System.String)
extern "C" void DTDNode_set_BaseURI_m4_188 (DTDNode_t4_89 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___baseURI_2 = L_0;
		return;
	}
}
// System.Boolean Mono.Xml.DTDNode::get_IsInternalSubset()
extern "C" bool DTDNode_get_IsInternalSubset_m4_189 (DTDNode_t4_89 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___isInternalSubset_1);
		return L_0;
	}
}
// System.Void Mono.Xml.DTDNode::set_IsInternalSubset(System.Boolean)
extern "C" void DTDNode_set_IsInternalSubset_m4_190 (DTDNode_t4_89 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___isInternalSubset_1 = L_0;
		return;
	}
}
// System.Int32 Mono.Xml.DTDNode::get_LineNumber()
extern "C" int32_t DTDNode_get_LineNumber_m4_191 (DTDNode_t4_89 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___lineNumber_3);
		return L_0;
	}
}
// System.Int32 Mono.Xml.DTDNode::get_LinePosition()
extern "C" int32_t DTDNode_get_LinePosition_m4_192 (DTDNode_t4_89 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___linePosition_4);
		return L_0;
	}
}
// System.Void Mono.Xml.DTDNode::SetRoot(Mono.Xml.DTDObjectModel)
extern "C" void DTDNode_SetRoot_m4_193 (DTDNode_t4_89 * __this, DTDObjectModel_t4_82 * ___root, const MethodInfo* method)
{
	{
		DTDObjectModel_t4_82 * L_0 = ___root;
		__this->___root_0 = L_0;
		String_t* L_1 = (__this->___baseURI_2);
		if (L_1)
		{
			goto IL_001e;
		}
	}
	{
		DTDObjectModel_t4_82 * L_2 = ___root;
		NullCheck(L_2);
		String_t* L_3 = DTDObjectModel_get_BaseURI_m4_119(L_2, /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(7 /* System.Void Mono.Xml.DTDNode::set_BaseURI(System.String) */, __this, L_3);
	}

IL_001e:
	{
		return;
	}
}
// Mono.Xml.DTDObjectModel Mono.Xml.DTDNode::get_Root()
extern "C" DTDObjectModel_t4_82 * DTDNode_get_Root_m4_194 (DTDNode_t4_89 * __this, const MethodInfo* method)
{
	{
		DTDObjectModel_t4_82 * L_0 = (__this->___root_0);
		return L_0;
	}
}
// System.Xml.XmlException Mono.Xml.DTDNode::NotWFError(System.String)
extern TypeInfo* XmlException_t4_137_il2cpp_TypeInfo_var;
extern "C" XmlException_t4_137 * DTDNode_NotWFError_m4_195 (DTDNode_t4_89 * __this, String_t* ___message, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		XmlException_t4_137_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1285);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String Mono.Xml.DTDNode::get_BaseURI() */, __this);
		String_t* L_1 = ___message;
		XmlException_t4_137 * L_2 = (XmlException_t4_137 *)il2cpp_codegen_object_new (XmlException_t4_137_il2cpp_TypeInfo_var);
		XmlException__ctor_m4_524(L_2, __this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void Mono.Xml.DTDElementDeclaration::.ctor(Mono.Xml.DTDObjectModel)
extern "C" void DTDElementDeclaration__ctor_m4_196 (DTDElementDeclaration_t4_94 * __this, DTDObjectModel_t4_82 * ___root, const MethodInfo* method)
{
	{
		DTDNode__ctor_m4_186(__this, /*hidden argument*/NULL);
		DTDObjectModel_t4_82 * L_0 = ___root;
		__this->___root_5 = L_0;
		return;
	}
}
// System.String Mono.Xml.DTDElementDeclaration::get_Name()
extern "C" String_t* DTDElementDeclaration_get_Name_m4_197 (DTDElementDeclaration_t4_94 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___name_7);
		return L_0;
	}
}
// System.Void Mono.Xml.DTDElementDeclaration::set_Name(System.String)
extern "C" void DTDElementDeclaration_set_Name_m4_198 (DTDElementDeclaration_t4_94 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___name_7 = L_0;
		return;
	}
}
// System.Void Mono.Xml.DTDElementDeclaration::set_IsEmpty(System.Boolean)
extern "C" void DTDElementDeclaration_set_IsEmpty_m4_199 (DTDElementDeclaration_t4_94 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___isEmpty_8 = L_0;
		return;
	}
}
// System.Void Mono.Xml.DTDElementDeclaration::set_IsAny(System.Boolean)
extern "C" void DTDElementDeclaration_set_IsAny_m4_200 (DTDElementDeclaration_t4_94 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___isAny_9 = L_0;
		return;
	}
}
// System.Void Mono.Xml.DTDElementDeclaration::set_IsMixedContent(System.Boolean)
extern "C" void DTDElementDeclaration_set_IsMixedContent_m4_201 (DTDElementDeclaration_t4_94 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___isMixedContent_10 = L_0;
		return;
	}
}
// Mono.Xml.DTDContentModel Mono.Xml.DTDElementDeclaration::get_ContentModel()
extern TypeInfo* DTDContentModel_t4_92_il2cpp_TypeInfo_var;
extern "C" DTDContentModel_t4_92 * DTDElementDeclaration_get_ContentModel_m4_202 (DTDElementDeclaration_t4_94 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DTDContentModel_t4_92_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1297);
		s_Il2CppMethodIntialized = true;
	}
	{
		DTDContentModel_t4_92 * L_0 = (__this->___contentModel_6);
		if (L_0)
		{
			goto IL_0022;
		}
	}
	{
		DTDObjectModel_t4_82 * L_1 = (__this->___root_5);
		String_t* L_2 = DTDElementDeclaration_get_Name_m4_197(__this, /*hidden argument*/NULL);
		DTDContentModel_t4_92 * L_3 = (DTDContentModel_t4_92 *)il2cpp_codegen_object_new (DTDContentModel_t4_92_il2cpp_TypeInfo_var);
		DTDContentModel__ctor_m4_175(L_3, L_1, L_2, /*hidden argument*/NULL);
		__this->___contentModel_6 = L_3;
	}

IL_0022:
	{
		DTDContentModel_t4_92 * L_4 = (__this->___contentModel_6);
		return L_4;
	}
}
// System.String Mono.Xml.DTDAttributeDefinition::get_Name()
extern "C" String_t* DTDAttributeDefinition_get_Name_m4_203 (DTDAttributeDefinition_t4_95 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___name_5);
		return L_0;
	}
}
// System.Xml.Schema.XmlSchemaDatatype Mono.Xml.DTDAttributeDefinition::get_Datatype()
extern "C" XmlSchemaDatatype_t4_4 * DTDAttributeDefinition_get_Datatype_m4_204 (DTDAttributeDefinition_t4_95 * __this, const MethodInfo* method)
{
	{
		XmlSchemaDatatype_t4_4 * L_0 = (__this->___datatype_6);
		return L_0;
	}
}
// System.String Mono.Xml.DTDAttributeDefinition::get_DefaultValue()
extern "C" String_t* DTDAttributeDefinition_get_DefaultValue_m4_205 (DTDAttributeDefinition_t4_95 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___resolvedDefaultValue_8);
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		String_t* L_1 = DTDAttributeDefinition_ComputeDefaultValue_m4_207(__this, /*hidden argument*/NULL);
		__this->___resolvedDefaultValue_8 = L_1;
	}

IL_0017:
	{
		String_t* L_2 = (__this->___resolvedDefaultValue_8);
		return L_2;
	}
}
// System.String Mono.Xml.DTDAttributeDefinition::get_UnresolvedDefaultValue()
extern "C" String_t* DTDAttributeDefinition_get_UnresolvedDefaultValue_m4_206 (DTDAttributeDefinition_t4_95 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___unresolvedDefault_7);
		return L_0;
	}
}
// System.String Mono.Xml.DTDAttributeDefinition::ComputeDefaultValue()
extern TypeInfo* StringBuilder_t1_247_il2cpp_TypeInfo_var;
extern TypeInfo* CultureInfo_t1_277_il2cpp_TypeInfo_var;
extern TypeInfo* XmlChar_t4_126_il2cpp_TypeInfo_var;
extern "C" String_t* DTDAttributeDefinition_ComputeDefaultValue_m4_207 (DTDAttributeDefinition_t4_95 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringBuilder_t1_247_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		CultureInfo_t1_277_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(31);
		XmlChar_t4_126_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1298);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t1_247 * V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	String_t* V_3 = {0};
	int32_t V_4 = 0;
	uint16_t V_5 = 0x0;
	int32_t V_6 = {0};
	String_t* V_7 = {0};
	String_t* V_8 = {0};
	int32_t V_9 = 0;
	String_t* V_10 = {0};
	{
		String_t* L_0 = DTDAttributeDefinition_get_UnresolvedDefaultValue_m4_206(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (String_t*)NULL;
	}

IL_000d:
	{
		StringBuilder_t1_247 * L_1 = (StringBuilder_t1_247 *)il2cpp_codegen_object_new (StringBuilder_t1_247_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1_12414(L_1, /*hidden argument*/NULL);
		V_0 = L_1;
		V_1 = 0;
		V_2 = 0;
		String_t* L_2 = DTDAttributeDefinition_get_UnresolvedDefaultValue_m4_206(__this, /*hidden argument*/NULL);
		V_3 = L_2;
		goto IL_0102;
	}

IL_0023:
	{
		String_t* L_3 = V_3;
		int32_t L_4 = V_2;
		NullCheck(L_3);
		int32_t L_5 = String_IndexOf_m1_501(L_3, ((int32_t)59), L_4, /*hidden argument*/NULL);
		V_4 = L_5;
		String_t* L_6 = V_3;
		int32_t L_7 = V_2;
		NullCheck(L_6);
		uint16_t L_8 = String_get_Chars_m1_442(L_6, ((int32_t)((int32_t)L_7+(int32_t)1)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_8) == ((uint32_t)((int32_t)35)))))
		{
			goto IL_00aa;
		}
	}
	{
		String_t* L_9 = V_3;
		int32_t L_10 = V_2;
		NullCheck(L_9);
		uint16_t L_11 = String_get_Chars_m1_442(L_9, ((int32_t)((int32_t)L_10+(int32_t)2)), /*hidden argument*/NULL);
		V_5 = L_11;
		V_6 = 7;
		uint16_t L_12 = V_5;
		if ((((int32_t)L_12) == ((int32_t)((int32_t)120))))
		{
			goto IL_005e;
		}
	}
	{
		uint16_t L_13 = V_5;
		if ((!(((uint32_t)L_13) == ((uint32_t)((int32_t)88)))))
		{
			goto IL_007e;
		}
	}

IL_005e:
	{
		String_t* L_14 = V_3;
		int32_t L_15 = V_2;
		int32_t L_16 = V_4;
		int32_t L_17 = V_2;
		NullCheck(L_14);
		String_t* L_18 = String_Substring_m1_455(L_14, ((int32_t)((int32_t)L_15+(int32_t)3)), ((int32_t)((int32_t)((int32_t)((int32_t)L_16-(int32_t)L_17))-(int32_t)3)), /*hidden argument*/NULL);
		V_7 = L_18;
		int32_t L_19 = V_6;
		V_6 = ((int32_t)((int32_t)L_19|(int32_t)((int32_t)515)));
		goto IL_008f;
	}

IL_007e:
	{
		String_t* L_20 = V_3;
		int32_t L_21 = V_2;
		int32_t L_22 = V_4;
		int32_t L_23 = V_2;
		NullCheck(L_20);
		String_t* L_24 = String_Substring_m1_455(L_20, ((int32_t)((int32_t)L_21+(int32_t)2)), ((int32_t)((int32_t)((int32_t)((int32_t)L_22-(int32_t)L_23))-(int32_t)2)), /*hidden argument*/NULL);
		V_7 = L_24;
	}

IL_008f:
	{
		StringBuilder_t1_247 * L_25 = V_0;
		String_t* L_26 = V_7;
		int32_t L_27 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t1_277_il2cpp_TypeInfo_var);
		CultureInfo_t1_277 * L_28 = CultureInfo_get_InvariantCulture_m1_3911(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_29 = Int32_Parse_m1_98(NULL /*static, unused*/, L_26, L_27, L_28, /*hidden argument*/NULL);
		NullCheck(L_25);
		StringBuilder_Append_m1_12452(L_25, (((int32_t)((uint16_t)L_29))), /*hidden argument*/NULL);
		goto IL_00fd;
	}

IL_00aa:
	{
		StringBuilder_t1_247 * L_30 = V_0;
		String_t* L_31 = V_3;
		int32_t L_32 = V_1;
		int32_t L_33 = V_2;
		NullCheck(L_31);
		String_t* L_34 = String_Substring_m1_455(L_31, L_32, ((int32_t)((int32_t)L_33-(int32_t)1)), /*hidden argument*/NULL);
		NullCheck(L_30);
		StringBuilder_Append_m1_12438(L_30, L_34, /*hidden argument*/NULL);
		String_t* L_35 = V_3;
		int32_t L_36 = V_2;
		int32_t L_37 = V_4;
		NullCheck(L_35);
		String_t* L_38 = String_Substring_m1_455(L_35, ((int32_t)((int32_t)L_36+(int32_t)1)), ((int32_t)((int32_t)L_37-(int32_t)2)), /*hidden argument*/NULL);
		V_8 = L_38;
		String_t* L_39 = V_8;
		IL2CPP_RUNTIME_CLASS_INIT(XmlChar_t4_126_il2cpp_TypeInfo_var);
		int32_t L_40 = XmlChar_GetPredefinedEntity_m4_368(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
		V_9 = L_40;
		int32_t L_41 = V_9;
		if ((((int32_t)L_41) < ((int32_t)0)))
		{
			goto IL_00e9;
		}
	}
	{
		StringBuilder_t1_247 * L_42 = V_0;
		int32_t L_43 = V_9;
		NullCheck(L_42);
		StringBuilder_Append_m1_12444(L_42, L_43, /*hidden argument*/NULL);
		goto IL_00fd;
	}

IL_00e9:
	{
		StringBuilder_t1_247 * L_44 = V_0;
		DTDObjectModel_t4_82 * L_45 = DTDNode_get_Root_m4_194(__this, /*hidden argument*/NULL);
		String_t* L_46 = V_8;
		NullCheck(L_45);
		String_t* L_47 = DTDObjectModel_ResolveEntity_m4_138(L_45, L_46, /*hidden argument*/NULL);
		NullCheck(L_44);
		StringBuilder_Append_m1_12438(L_44, L_47, /*hidden argument*/NULL);
	}

IL_00fd:
	{
		int32_t L_48 = V_4;
		V_1 = ((int32_t)((int32_t)L_48+(int32_t)1));
	}

IL_0102:
	{
		String_t* L_49 = V_3;
		int32_t L_50 = V_1;
		NullCheck(L_49);
		int32_t L_51 = String_IndexOf_m1_501(L_49, ((int32_t)38), L_50, /*hidden argument*/NULL);
		int32_t L_52 = L_51;
		V_2 = L_52;
		if ((((int32_t)L_52) >= ((int32_t)0)))
		{
			goto IL_0023;
		}
	}
	{
		StringBuilder_t1_247 * L_53 = V_0;
		String_t* L_54 = V_3;
		int32_t L_55 = V_1;
		NullCheck(L_54);
		String_t* L_56 = String_Substring_m1_454(L_54, L_55, /*hidden argument*/NULL);
		NullCheck(L_53);
		StringBuilder_Append_m1_12438(L_53, L_56, /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_57 = V_0;
		StringBuilder_t1_247 * L_58 = V_0;
		NullCheck(L_58);
		int32_t L_59 = StringBuilder_get_Length_m1_12424(L_58, /*hidden argument*/NULL);
		NullCheck(L_57);
		String_t* L_60 = StringBuilder_ToString_m1_12429(L_57, 1, ((int32_t)((int32_t)L_59-(int32_t)2)), /*hidden argument*/NULL);
		V_10 = L_60;
		StringBuilder_t1_247 * L_61 = V_0;
		NullCheck(L_61);
		StringBuilder_set_Length_m1_12425(L_61, 0, /*hidden argument*/NULL);
		String_t* L_62 = V_10;
		return L_62;
	}
}
// System.Void Mono.Xml.DTDAttListDeclaration::.ctor(Mono.Xml.DTDObjectModel)
extern TypeInfo* Hashtable_t1_100_il2cpp_TypeInfo_var;
extern TypeInfo* ArrayList_t1_170_il2cpp_TypeInfo_var;
extern "C" void DTDAttListDeclaration__ctor_m4_208 (DTDAttListDeclaration_t4_96 * __this, DTDObjectModel_t4_82 * ___root, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Hashtable_t1_100_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(119);
		ArrayList_t1_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(60);
		s_Il2CppMethodIntialized = true;
	}
	{
		Hashtable_t1_100 * L_0 = (Hashtable_t1_100 *)il2cpp_codegen_object_new (Hashtable_t1_100_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1_3279(L_0, /*hidden argument*/NULL);
		__this->___attributeOrders_6 = L_0;
		ArrayList_t1_170 * L_1 = (ArrayList_t1_170 *)il2cpp_codegen_object_new (ArrayList_t1_170_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1_3049(L_1, /*hidden argument*/NULL);
		__this->___attributes_7 = L_1;
		DTDNode__ctor_m4_186(__this, /*hidden argument*/NULL);
		DTDObjectModel_t4_82 * L_2 = ___root;
		DTDNode_SetRoot_m4_193(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.String Mono.Xml.DTDAttListDeclaration::get_Name()
extern "C" String_t* DTDAttListDeclaration_get_Name_m4_209 (DTDAttListDeclaration_t4_96 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___name_5);
		return L_0;
	}
}
// System.Void Mono.Xml.DTDAttListDeclaration::set_Name(System.String)
extern "C" void DTDAttListDeclaration_set_Name_m4_210 (DTDAttListDeclaration_t4_96 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___name_5 = L_0;
		return;
	}
}
// Mono.Xml.DTDAttributeDefinition Mono.Xml.DTDAttListDeclaration::get_Item(System.Int32)
extern "C" DTDAttributeDefinition_t4_95 * DTDAttListDeclaration_get_Item_m4_211 (DTDAttListDeclaration_t4_96 * __this, int32_t ___i, const MethodInfo* method)
{
	{
		int32_t L_0 = ___i;
		DTDAttributeDefinition_t4_95 * L_1 = DTDAttListDeclaration_Get_m4_213(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// Mono.Xml.DTDAttributeDefinition Mono.Xml.DTDAttListDeclaration::get_Item(System.String)
extern "C" DTDAttributeDefinition_t4_95 * DTDAttListDeclaration_get_Item_m4_212 (DTDAttListDeclaration_t4_96 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		DTDAttributeDefinition_t4_95 * L_1 = DTDAttListDeclaration_Get_m4_214(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// Mono.Xml.DTDAttributeDefinition Mono.Xml.DTDAttListDeclaration::Get(System.Int32)
extern TypeInfo* DTDAttributeDefinition_t4_95_il2cpp_TypeInfo_var;
extern "C" DTDAttributeDefinition_t4_95 * DTDAttListDeclaration_Get_m4_213 (DTDAttListDeclaration_t4_96 * __this, int32_t ___i, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DTDAttributeDefinition_t4_95_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1294);
		s_Il2CppMethodIntialized = true;
	}
	{
		ArrayList_t1_170 * L_0 = (__this->___attributes_7);
		int32_t L_1 = ___i;
		NullCheck(L_0);
		Object_t * L_2 = (Object_t *)VirtFuncInvoker1< Object_t *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_0, L_1);
		return ((DTDAttributeDefinition_t4_95 *)IsInstClass(L_2, DTDAttributeDefinition_t4_95_il2cpp_TypeInfo_var));
	}
}
// Mono.Xml.DTDAttributeDefinition Mono.Xml.DTDAttListDeclaration::Get(System.String)
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* DTDAttributeDefinition_t4_95_il2cpp_TypeInfo_var;
extern "C" DTDAttributeDefinition_t4_95 * DTDAttListDeclaration_Get_m4_214 (DTDAttListDeclaration_t4_96 * __this, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		DTDAttributeDefinition_t4_95_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1294);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	{
		Hashtable_t1_100 * L_0 = (__this->___attributeOrders_6);
		String_t* L_1 = ___name;
		NullCheck(L_0);
		Object_t * L_2 = (Object_t *)VirtFuncInvoker1< Object_t *, Object_t * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_0, L_1);
		V_0 = L_2;
		Object_t * L_3 = V_0;
		if (!L_3)
		{
			goto IL_002a;
		}
	}
	{
		ArrayList_t1_170 * L_4 = (__this->___attributes_7);
		Object_t * L_5 = V_0;
		NullCheck(L_4);
		Object_t * L_6 = (Object_t *)VirtFuncInvoker1< Object_t *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_4, ((*(int32_t*)((int32_t*)UnBox (L_5, Int32_t1_3_il2cpp_TypeInfo_var)))));
		return ((DTDAttributeDefinition_t4_95 *)IsInstClass(L_6, DTDAttributeDefinition_t4_95_il2cpp_TypeInfo_var));
	}

IL_002a:
	{
		return (DTDAttributeDefinition_t4_95 *)NULL;
	}
}
// System.Collections.IList Mono.Xml.DTDAttListDeclaration::get_Definitions()
extern "C" Object_t * DTDAttListDeclaration_get_Definitions_m4_215 (DTDAttListDeclaration_t4_96 * __this, const MethodInfo* method)
{
	{
		ArrayList_t1_170 * L_0 = (__this->___attributes_7);
		return L_0;
	}
}
// System.Void Mono.Xml.DTDAttListDeclaration::Add(Mono.Xml.DTDAttributeDefinition)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidOperationException_t1_1559_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3803;
extern "C" void DTDAttListDeclaration_Add_m4_216 (DTDAttListDeclaration_t4_96 * __this, DTDAttributeDefinition_t4_95 * ___def, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		InvalidOperationException_t1_1559_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(78);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral3803 = il2cpp_codegen_string_literal_from_index(3803);
		s_Il2CppMethodIntialized = true;
	}
	{
		Hashtable_t1_100 * L_0 = (__this->___attributeOrders_6);
		DTDAttributeDefinition_t4_95 * L_1 = ___def;
		NullCheck(L_1);
		String_t* L_2 = DTDAttributeDefinition_get_Name_m4_203(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Object_t * L_3 = (Object_t *)VirtFuncInvoker1< Object_t *, Object_t * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_0, L_2);
		if (!L_3)
		{
			goto IL_0032;
		}
	}
	{
		DTDAttributeDefinition_t4_95 * L_4 = ___def;
		NullCheck(L_4);
		String_t* L_5 = DTDAttributeDefinition_get_Name_m4_203(L_4, /*hidden argument*/NULL);
		String_t* L_6 = DTDAttListDeclaration_get_Name_m4_209(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Format_m1_549(NULL /*static, unused*/, _stringLiteral3803, L_5, L_6, /*hidden argument*/NULL);
		InvalidOperationException_t1_1559 * L_8 = (InvalidOperationException_t1_1559 *)il2cpp_codegen_object_new (InvalidOperationException_t1_1559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1_14171(L_8, L_7, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_8);
	}

IL_0032:
	{
		DTDAttributeDefinition_t4_95 * L_9 = ___def;
		DTDObjectModel_t4_82 * L_10 = DTDNode_get_Root_m4_194(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		DTDNode_SetRoot_m4_193(L_9, L_10, /*hidden argument*/NULL);
		Hashtable_t1_100 * L_11 = (__this->___attributeOrders_6);
		DTDAttributeDefinition_t4_95 * L_12 = ___def;
		NullCheck(L_12);
		String_t* L_13 = DTDAttributeDefinition_get_Name_m4_203(L_12, /*hidden argument*/NULL);
		ArrayList_t1_170 * L_14 = (__this->___attributes_7);
		NullCheck(L_14);
		int32_t L_15 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_14);
		int32_t L_16 = L_15;
		Object_t * L_17 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_16);
		NullCheck(L_11);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_11, L_13, L_17);
		ArrayList_t1_170 * L_18 = (__this->___attributes_7);
		DTDAttributeDefinition_t4_95 * L_19 = ___def;
		NullCheck(L_18);
		VirtFuncInvoker1< int32_t, Object_t * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_18, L_19);
		return;
	}
}
// System.Void Mono.Xml.DTDEntityBase::.ctor(Mono.Xml.DTDObjectModel)
extern "C" void DTDEntityBase__ctor_m4_217 (DTDEntityBase_t4_97 * __this, DTDObjectModel_t4_82 * ___root, const MethodInfo* method)
{
	{
		DTDNode__ctor_m4_186(__this, /*hidden argument*/NULL);
		DTDObjectModel_t4_82 * L_0 = ___root;
		DTDNode_SetRoot_m4_193(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Mono.Xml.DTDEntityBase::get_IsInvalid()
extern "C" bool DTDEntityBase_get_IsInvalid_m4_218 (DTDEntityBase_t4_97 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___isInvalid_12);
		return L_0;
	}
}
// System.Void Mono.Xml.DTDEntityBase::set_LoadFailed(System.Boolean)
extern "C" void DTDEntityBase_set_LoadFailed_m4_219 (DTDEntityBase_t4_97 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___loadFailed_13 = L_0;
		return;
	}
}
// System.String Mono.Xml.DTDEntityBase::get_Name()
extern "C" String_t* DTDEntityBase_get_Name_m4_220 (DTDEntityBase_t4_97 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___name_5);
		return L_0;
	}
}
// System.Void Mono.Xml.DTDEntityBase::set_Name(System.String)
extern "C" void DTDEntityBase_set_Name_m4_221 (DTDEntityBase_t4_97 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___name_5 = L_0;
		return;
	}
}
// System.String Mono.Xml.DTDEntityBase::get_PublicId()
extern "C" String_t* DTDEntityBase_get_PublicId_m4_222 (DTDEntityBase_t4_97 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___publicId_6);
		return L_0;
	}
}
// System.Void Mono.Xml.DTDEntityBase::set_PublicId(System.String)
extern "C" void DTDEntityBase_set_PublicId_m4_223 (DTDEntityBase_t4_97 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___publicId_6 = L_0;
		return;
	}
}
// System.String Mono.Xml.DTDEntityBase::get_SystemId()
extern "C" String_t* DTDEntityBase_get_SystemId_m4_224 (DTDEntityBase_t4_97 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___systemId_7);
		return L_0;
	}
}
// System.Void Mono.Xml.DTDEntityBase::set_SystemId(System.String)
extern "C" void DTDEntityBase_set_SystemId_m4_225 (DTDEntityBase_t4_97 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___systemId_7 = L_0;
		return;
	}
}
// System.String Mono.Xml.DTDEntityBase::get_LiteralEntityValue()
extern "C" String_t* DTDEntityBase_get_LiteralEntityValue_m4_226 (DTDEntityBase_t4_97 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___literalValue_8);
		return L_0;
	}
}
// System.Void Mono.Xml.DTDEntityBase::set_LiteralEntityValue(System.String)
extern "C" void DTDEntityBase_set_LiteralEntityValue_m4_227 (DTDEntityBase_t4_97 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___literalValue_8 = L_0;
		return;
	}
}
// System.String Mono.Xml.DTDEntityBase::get_ReplacementText()
extern "C" String_t* DTDEntityBase_get_ReplacementText_m4_228 (DTDEntityBase_t4_97 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___replacementText_9);
		return L_0;
	}
}
// System.Void Mono.Xml.DTDEntityBase::set_ReplacementText(System.String)
extern "C" void DTDEntityBase_set_ReplacementText_m4_229 (DTDEntityBase_t4_97 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___replacementText_9 = L_0;
		return;
	}
}
// System.Void Mono.Xml.DTDEntityBase::set_XmlResolver(System.Xml.XmlResolver)
extern "C" void DTDEntityBase_set_XmlResolver_m4_230 (DTDEntityBase_t4_97 * __this, XmlResolver_t4_68 * ___value, const MethodInfo* method)
{
	{
		XmlResolver_t4_68 * L_0 = ___value;
		__this->___resolver_14 = L_0;
		return;
	}
}
// System.String Mono.Xml.DTDEntityBase::get_ActualUri()
extern TypeInfo* Uri_t3_3_il2cpp_TypeInfo_var;
extern TypeInfo* UriFormatException_t3_19_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" String_t* DTDEntityBase_get_ActualUri_m4_231 (DTDEntityBase_t4_97 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Uri_t3_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		UriFormatException_t3_19_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1299);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	Uri_t3_3 * V_0 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	DTDEntityBase_t4_97 * G_B13_0 = {0};
	DTDEntityBase_t4_97 * G_B12_0 = {0};
	String_t* G_B14_0 = {0};
	DTDEntityBase_t4_97 * G_B14_1 = {0};
	{
		String_t* L_0 = (__this->___uriString_10);
		if (L_0)
		{
			goto IL_00bb;
		}
	}
	{
		XmlResolver_t4_68 * L_1 = (__this->___resolver_14);
		if (!L_1)
		{
			goto IL_0031;
		}
	}
	{
		String_t* L_2 = DTDEntityBase_get_SystemId_m4_224(__this, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0031;
		}
	}
	{
		String_t* L_3 = DTDEntityBase_get_SystemId_m4_224(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = String_get_Length_m1_571(L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0042;
		}
	}

IL_0031:
	{
		String_t* L_5 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String Mono.Xml.DTDNode::get_BaseURI() */, __this);
		__this->___uriString_10 = L_5;
		goto IL_00bb;
	}

IL_0042:
	{
		V_0 = (Uri_t3_3 *)NULL;
	}

IL_0044:
	try
	{ // begin try (depth: 1)
		{
			String_t* L_6 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String Mono.Xml.DTDNode::get_BaseURI() */, __this);
			if (!L_6)
			{
				goto IL_006c;
			}
		}

IL_004f:
		{
			String_t* L_7 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String Mono.Xml.DTDNode::get_BaseURI() */, __this);
			NullCheck(L_7);
			int32_t L_8 = String_get_Length_m1_571(L_7, /*hidden argument*/NULL);
			if ((((int32_t)L_8) <= ((int32_t)0)))
			{
				goto IL_006c;
			}
		}

IL_0060:
		{
			String_t* L_9 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String Mono.Xml.DTDNode::get_BaseURI() */, __this);
			Uri_t3_3 * L_10 = (Uri_t3_3 *)il2cpp_codegen_object_new (Uri_t3_3_il2cpp_TypeInfo_var);
			Uri__ctor_m3_23(L_10, L_9, /*hidden argument*/NULL);
			V_0 = L_10;
		}

IL_006c:
		{
			goto IL_0077;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1_33 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (UriFormatException_t3_19_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0071;
		throw e;
	}

CATCH_0071:
	{ // begin catch(System.UriFormatException)
		goto IL_0077;
	} // end catch (depth: 1)

IL_0077:
	{
		XmlResolver_t4_68 * L_11 = (__this->___resolver_14);
		Uri_t3_3 * L_12 = V_0;
		String_t* L_13 = DTDEntityBase_get_SystemId_m4_224(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Uri_t3_3 * L_14 = (Uri_t3_3 *)VirtFuncInvoker2< Uri_t3_3 *, Uri_t3_3 *, String_t* >::Invoke(5 /* System.Uri System.Xml.XmlResolver::ResolveUri(System.Uri,System.String) */, L_11, L_12, L_13);
		__this->___absUri_11 = L_14;
		Uri_t3_3 * L_15 = (__this->___absUri_11);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		bool L_16 = Uri_op_Inequality_m3_24(NULL /*static, unused*/, L_15, (Uri_t3_3 *)NULL, /*hidden argument*/NULL);
		G_B12_0 = __this;
		if (!L_16)
		{
			G_B13_0 = __this;
			goto IL_00b1;
		}
	}
	{
		Uri_t3_3 * L_17 = (__this->___absUri_11);
		NullCheck(L_17);
		String_t* L_18 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Uri::ToString() */, L_17);
		G_B14_0 = L_18;
		G_B14_1 = G_B12_0;
		goto IL_00b6;
	}

IL_00b1:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		G_B14_0 = L_19;
		G_B14_1 = G_B13_0;
	}

IL_00b6:
	{
		NullCheck(G_B14_1);
		G_B14_1->___uriString_10 = G_B14_0;
	}

IL_00bb:
	{
		String_t* L_20 = (__this->___uriString_10);
		return L_20;
	}
}
// System.Void Mono.Xml.DTDEntityBase::Resolve()
extern const Il2CppType* Stream_t1_405_0_0_0_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Stream_t1_405_il2cpp_TypeInfo_var;
extern TypeInfo* XmlTextReader_t4_169_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidOperationException_t1_1559_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t1_33_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3804;
extern "C" void DTDEntityBase_Resolve_m4_232 (DTDEntityBase_t4_97 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Stream_t1_405_0_0_0_var = il2cpp_codegen_type_from_index(265);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Stream_t1_405_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(265);
		XmlTextReader_t4_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1287);
		InvalidOperationException_t1_1559_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(78);
		Exception_t1_33_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(49);
		_stringLiteral3804 = il2cpp_codegen_string_literal_from_index(3804);
		s_Il2CppMethodIntialized = true;
	}
	Stream_t1_405 * V_0 = {0};
	XmlTextReader_t4_169 * V_1 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = DTDEntityBase_get_ActualUri_m4_231(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		bool L_2 = String_op_Equality_m1_601(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		DTDEntityBase_set_LoadFailed_m4_219(__this, 1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		DTDEntityBase_set_LiteralEntityValue_m4_227(__this, L_3, /*hidden argument*/NULL);
		return;
	}

IL_0028:
	{
		DTDObjectModel_t4_82 * L_4 = DTDNode_get_Root_m4_194(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Hashtable_t1_100 * L_5 = DTDObjectModel_get_ExternalResources_m4_141(L_4, /*hidden argument*/NULL);
		String_t* L_6 = DTDEntityBase_get_ActualUri_m4_231(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		bool L_7 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, L_5, L_6);
		if (!L_7)
		{
			goto IL_0064;
		}
	}
	{
		DTDObjectModel_t4_82 * L_8 = DTDNode_get_Root_m4_194(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Hashtable_t1_100 * L_9 = DTDObjectModel_get_ExternalResources_m4_141(L_8, /*hidden argument*/NULL);
		String_t* L_10 = DTDEntityBase_get_ActualUri_m4_231(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		Object_t * L_11 = (Object_t *)VirtFuncInvoker1< Object_t *, Object_t * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_9, L_10);
		DTDEntityBase_set_LiteralEntityValue_m4_227(__this, ((String_t*)CastclassSealed(L_11, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
	}

IL_0064:
	{
		V_0 = (Stream_t1_405 *)NULL;
	}

IL_0066:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			{
				XmlResolver_t4_68 * L_12 = (__this->___resolver_14);
				Uri_t3_3 * L_13 = (__this->___absUri_11);
				IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
				Type_t * L_14 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(Stream_t1_405_0_0_0_var), /*hidden argument*/NULL);
				NullCheck(L_12);
				Object_t * L_15 = (Object_t *)VirtFuncInvoker3< Object_t *, Uri_t3_3 *, String_t*, Type_t * >::Invoke(4 /* System.Object System.Xml.XmlResolver::GetEntity(System.Uri,System.String,System.Type) */, L_12, L_13, (String_t*)NULL, L_14);
				V_0 = ((Stream_t1_405 *)IsInstClass(L_15, Stream_t1_405_il2cpp_TypeInfo_var));
				String_t* L_16 = DTDEntityBase_get_ActualUri_m4_231(__this, /*hidden argument*/NULL);
				Stream_t1_405 * L_17 = V_0;
				DTDObjectModel_t4_82 * L_18 = DTDNode_get_Root_m4_194(__this, /*hidden argument*/NULL);
				NullCheck(L_18);
				XmlNameTable_t4_67 * L_19 = DTDObjectModel_get_NameTable_m4_125(L_18, /*hidden argument*/NULL);
				XmlTextReader_t4_169 * L_20 = (XmlTextReader_t4_169 *)il2cpp_codegen_object_new (XmlTextReader_t4_169_il2cpp_TypeInfo_var);
				XmlTextReader__ctor_m4_797(L_20, L_16, L_17, L_19, /*hidden argument*/NULL);
				V_1 = L_20;
				XmlTextReader_t4_169 * L_21 = V_1;
				NullCheck(L_21);
				TextReader_t1_246 * L_22 = XmlTextReader_GetRemainder_m4_833(L_21, /*hidden argument*/NULL);
				NullCheck(L_22);
				String_t* L_23 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(12 /* System.String System.IO.TextReader::ReadToEnd() */, L_22);
				DTDEntityBase_set_LiteralEntityValue_m4_227(__this, L_23, /*hidden argument*/NULL);
				DTDObjectModel_t4_82 * L_24 = DTDNode_get_Root_m4_194(__this, /*hidden argument*/NULL);
				NullCheck(L_24);
				Hashtable_t1_100 * L_25 = DTDObjectModel_get_ExternalResources_m4_141(L_24, /*hidden argument*/NULL);
				String_t* L_26 = DTDEntityBase_get_ActualUri_m4_231(__this, /*hidden argument*/NULL);
				String_t* L_27 = DTDEntityBase_get_LiteralEntityValue_m4_226(__this, /*hidden argument*/NULL);
				NullCheck(L_25);
				VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_25, L_26, L_27);
				DTDObjectModel_t4_82 * L_28 = DTDNode_get_Root_m4_194(__this, /*hidden argument*/NULL);
				NullCheck(L_28);
				Hashtable_t1_100 * L_29 = DTDObjectModel_get_ExternalResources_m4_141(L_28, /*hidden argument*/NULL);
				NullCheck(L_29);
				int32_t L_30 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.Hashtable::get_Count() */, L_29);
				if ((((int32_t)L_30) <= ((int32_t)((int32_t)256))))
				{
					goto IL_00f2;
				}
			}

IL_00e7:
			{
				InvalidOperationException_t1_1559 * L_31 = (InvalidOperationException_t1_1559 *)il2cpp_codegen_object_new (InvalidOperationException_t1_1559_il2cpp_TypeInfo_var);
				InvalidOperationException__ctor_m1_14171(L_31, _stringLiteral3804, /*hidden argument*/NULL);
				il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_31);
			}

IL_00f2:
			{
				IL2CPP_LEAVE(0x11C, FINALLY_010f);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t1_33 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (Exception_t1_33_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_00f7;
			throw e;
		}

CATCH_00f7:
		{ // begin catch(System.Exception)
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_32 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
			DTDEntityBase_set_LiteralEntityValue_m4_227(__this, L_32, /*hidden argument*/NULL);
			DTDEntityBase_set_LoadFailed_m4_219(__this, 1, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x11C, FINALLY_010f);
		} // end catch (depth: 2)
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_010f;
	}

FINALLY_010f:
	{ // begin finally (depth: 1)
		{
			Stream_t1_405 * L_33 = V_0;
			if (!L_33)
			{
				goto IL_011b;
			}
		}

IL_0115:
		{
			Stream_t1_405 * L_34 = V_0;
			NullCheck(L_34);
			VirtActionInvoker0::Invoke(13 /* System.Void System.IO.Stream::Close() */, L_34);
		}

IL_011b:
		{
			IL2CPP_END_FINALLY(271)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(271)
	{
		IL2CPP_JUMP_TBL(0x11C, IL_011c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_011c:
	{
		return;
	}
}
// System.Void Mono.Xml.DTDEntityDeclaration::.ctor(Mono.Xml.DTDObjectModel)
extern TypeInfo* ArrayList_t1_170_il2cpp_TypeInfo_var;
extern "C" void DTDEntityDeclaration__ctor_m4_233 (DTDEntityDeclaration_t4_98 * __this, DTDObjectModel_t4_82 * ___root, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArrayList_t1_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(60);
		s_Il2CppMethodIntialized = true;
	}
	{
		ArrayList_t1_170 * L_0 = (ArrayList_t1_170 *)il2cpp_codegen_object_new (ArrayList_t1_170_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1_3049(L_0, /*hidden argument*/NULL);
		__this->___ReferencingEntities_17 = L_0;
		DTDObjectModel_t4_82 * L_1 = ___root;
		DTDEntityBase__ctor_m4_217(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String Mono.Xml.DTDEntityDeclaration::get_NotationName()
extern "C" String_t* DTDEntityDeclaration_get_NotationName_m4_234 (DTDEntityDeclaration_t4_98 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___notationName_16);
		return L_0;
	}
}
// System.Void Mono.Xml.DTDEntityDeclaration::set_NotationName(System.String)
extern "C" void DTDEntityDeclaration_set_NotationName_m4_235 (DTDEntityDeclaration_t4_98 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___notationName_16 = L_0;
		return;
	}
}
// System.Boolean Mono.Xml.DTDEntityDeclaration::get_HasExternalReference()
extern TypeInfo* ArrayList_t1_170_il2cpp_TypeInfo_var;
extern "C" bool DTDEntityDeclaration_get_HasExternalReference_m4_236 (DTDEntityDeclaration_t4_98 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArrayList_t1_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(60);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (__this->___scanned_18);
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		ArrayList_t1_170 * L_1 = (ArrayList_t1_170 *)il2cpp_codegen_object_new (ArrayList_t1_170_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1_3049(L_1, /*hidden argument*/NULL);
		DTDEntityDeclaration_ScanEntityValue_m4_238(__this, L_1, /*hidden argument*/NULL);
	}

IL_0016:
	{
		bool L_2 = (__this->___hasExternalReference_20);
		return L_2;
	}
}
// System.String Mono.Xml.DTDEntityDeclaration::get_EntityValue()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArrayList_t1_170_il2cpp_TypeInfo_var;
extern "C" String_t* DTDEntityDeclaration_get_EntityValue_m4_237 (DTDEntityDeclaration_t4_98 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		ArrayList_t1_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(60);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = DTDEntityBase_get_IsInvalid_m4_218(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		return L_1;
	}

IL_0011:
	{
		String_t* L_2 = DTDEntityBase_get_PublicId_m4_222(__this, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0038;
		}
	}
	{
		String_t* L_3 = DTDEntityBase_get_SystemId_m4_224(__this, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0038;
		}
	}
	{
		String_t* L_4 = DTDEntityBase_get_LiteralEntityValue_m4_226(__this, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0038;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		return L_5;
	}

IL_0038:
	{
		String_t* L_6 = (__this->___entityValue_15);
		if (L_6)
		{
			goto IL_00bc;
		}
	}
	{
		String_t* L_7 = DTDEntityDeclaration_get_NotationName_m4_234(__this, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_005e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___entityValue_15 = L_8;
		goto IL_00b1;
	}

IL_005e:
	{
		String_t* L_9 = DTDEntityBase_get_SystemId_m4_224(__this, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_007e;
		}
	}
	{
		String_t* L_10 = DTDEntityBase_get_SystemId_m4_224(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		bool L_12 = String_op_Equality_m1_601(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_00a5;
		}
	}

IL_007e:
	{
		String_t* L_13 = DTDEntityBase_get_ReplacementText_m4_228(__this, /*hidden argument*/NULL);
		__this->___entityValue_15 = L_13;
		String_t* L_14 = (__this->___entityValue_15);
		if (L_14)
		{
			goto IL_00a0;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___entityValue_15 = L_15;
	}

IL_00a0:
	{
		goto IL_00b1;
	}

IL_00a5:
	{
		String_t* L_16 = DTDEntityBase_get_ReplacementText_m4_228(__this, /*hidden argument*/NULL);
		__this->___entityValue_15 = L_16;
	}

IL_00b1:
	{
		ArrayList_t1_170 * L_17 = (ArrayList_t1_170 *)il2cpp_codegen_object_new (ArrayList_t1_170_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1_3049(L_17, /*hidden argument*/NULL);
		DTDEntityDeclaration_ScanEntityValue_m4_238(__this, L_17, /*hidden argument*/NULL);
	}

IL_00bc:
	{
		String_t* L_18 = (__this->___entityValue_15);
		return L_18;
	}
}
// System.Void Mono.Xml.DTDEntityDeclaration::ScanEntityValue(System.Collections.ArrayList)
extern TypeInfo* IEnumerator_t1_193_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern TypeInfo* XmlChar_t4_126_il2cpp_TypeInfo_var;
extern TypeInfo* XmlException_t4_137_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3805;
extern Il2CppCodeGenString* _stringLiteral3806;
extern Il2CppCodeGenString* _stringLiteral3807;
extern Il2CppCodeGenString* _stringLiteral3808;
extern "C" void DTDEntityDeclaration_ScanEntityValue_m4_238 (DTDEntityDeclaration_t4_98 * __this, ArrayList_t1_170 * ___refs, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IEnumerator_t1_193_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(144);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		XmlChar_t4_126_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1298);
		XmlException_t4_137_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1285);
		_stringLiteral3805 = il2cpp_codegen_string_literal_from_index(3805);
		_stringLiteral3806 = il2cpp_codegen_string_literal_from_index(3806);
		_stringLiteral3807 = il2cpp_codegen_string_literal_from_index(3807);
		_stringLiteral3808 = il2cpp_codegen_string_literal_from_index(3808);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	String_t* V_1 = {0};
	Object_t * V_2 = {0};
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	String_t* V_6 = {0};
	DTDEntityDeclaration_t4_98 * V_7 = {0};
	String_t* V_8 = {0};
	Object_t * V_9 = {0};
	Object_t * V_10 = {0};
	uint16_t V_11 = 0x0;
	Object_t * V_12 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = DTDEntityDeclaration_get_EntityValue_m4_237(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = DTDEntityBase_get_SystemId_m4_224(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		__this->___hasExternalReference_20 = 1;
	}

IL_0019:
	{
		bool L_2 = (__this->___recursed_19);
		if (!L_2)
		{
			goto IL_0030;
		}
	}
	{
		XmlException_t4_137 * L_3 = DTDNode_NotWFError_m4_195(__this, _stringLiteral3805, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0030:
	{
		__this->___recursed_19 = 1;
		bool L_4 = (__this->___scanned_18);
		if (!L_4)
		{
			goto IL_00b0;
		}
	}
	{
		ArrayList_t1_170 * L_5 = ___refs;
		NullCheck(L_5);
		Object_t * L_6 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(50 /* System.Collections.IEnumerator System.Collections.ArrayList::GetEnumerator() */, L_5);
		V_2 = L_6;
	}

IL_0049:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0083;
		}

IL_004e:
		{
			Object_t * L_7 = V_2;
			NullCheck(L_7);
			Object_t * L_8 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_7);
			V_1 = ((String_t*)CastclassSealed(L_8, String_t_il2cpp_TypeInfo_var));
			ArrayList_t1_170 * L_9 = (__this->___ReferencingEntities_17);
			String_t* L_10 = V_1;
			NullCheck(L_9);
			bool L_11 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(32 /* System.Boolean System.Collections.ArrayList::Contains(System.Object) */, L_9, L_10);
			if (!L_11)
			{
				goto IL_0083;
			}
		}

IL_006b:
		{
			String_t* L_12 = V_1;
			String_t* L_13 = DTDEntityBase_get_Name_m4_220(__this, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_14 = String_Format_m1_549(NULL /*static, unused*/, _stringLiteral3806, L_12, L_13, /*hidden argument*/NULL);
			XmlException_t4_137 * L_15 = DTDNode_NotWFError_m4_195(__this, L_14, /*hidden argument*/NULL);
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_15);
		}

IL_0083:
		{
			Object_t * L_16 = V_2;
			NullCheck(L_16);
			bool L_17 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_16);
			if (L_17)
			{
				goto IL_004e;
			}
		}

IL_008e:
		{
			IL2CPP_LEAVE(0xA8, FINALLY_0093);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0093;
	}

FINALLY_0093:
	{ // begin finally (depth: 1)
		{
			Object_t * L_18 = V_2;
			V_10 = ((Object_t *)IsInst(L_18, IDisposable_t1_1035_il2cpp_TypeInfo_var));
			Object_t * L_19 = V_10;
			if (L_19)
			{
				goto IL_00a0;
			}
		}

IL_009f:
		{
			IL2CPP_END_FINALLY(147)
		}

IL_00a0:
		{
			Object_t * L_20 = V_10;
			NullCheck(L_20);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_20);
			IL2CPP_END_FINALLY(147)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(147)
	{
		IL2CPP_JUMP_TBL(0xA8, IL_00a8)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_00a8:
	{
		__this->___recursed_19 = 0;
		return;
	}

IL_00b0:
	{
		String_t* L_21 = V_0;
		NullCheck(L_21);
		int32_t L_22 = String_get_Length_m1_571(L_21, /*hidden argument*/NULL);
		V_3 = L_22;
		V_4 = 0;
		V_5 = 0;
		goto IL_0243;
	}

IL_00c2:
	{
		String_t* L_23 = V_0;
		int32_t L_24 = V_5;
		NullCheck(L_23);
		uint16_t L_25 = String_get_Chars_m1_442(L_23, L_24, /*hidden argument*/NULL);
		V_11 = L_25;
		uint16_t L_26 = V_11;
		if ((((int32_t)L_26) == ((int32_t)((int32_t)38))))
		{
			goto IL_00e3;
		}
	}
	{
		uint16_t L_27 = V_11;
		if ((((int32_t)L_27) == ((int32_t)((int32_t)59))))
		{
			goto IL_00ee;
		}
	}
	{
		goto IL_023d;
	}

IL_00e3:
	{
		int32_t L_28 = V_5;
		V_4 = ((int32_t)((int32_t)L_28+(int32_t)1));
		goto IL_023d;
	}

IL_00ee:
	{
		int32_t L_29 = V_4;
		if (L_29)
		{
			goto IL_00fa;
		}
	}
	{
		goto IL_023d;
	}

IL_00fa:
	{
		String_t* L_30 = V_0;
		int32_t L_31 = V_4;
		int32_t L_32 = V_5;
		int32_t L_33 = V_4;
		NullCheck(L_30);
		String_t* L_34 = String_Substring_m1_455(L_30, L_31, ((int32_t)((int32_t)L_32-(int32_t)L_33)), /*hidden argument*/NULL);
		V_6 = L_34;
		String_t* L_35 = V_6;
		NullCheck(L_35);
		int32_t L_36 = String_get_Length_m1_571(L_35, /*hidden argument*/NULL);
		if (L_36)
		{
			goto IL_0121;
		}
	}
	{
		XmlException_t4_137 * L_37 = DTDNode_NotWFError_m4_195(__this, _stringLiteral3807, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_37);
	}

IL_0121:
	{
		String_t* L_38 = V_6;
		NullCheck(L_38);
		uint16_t L_39 = String_get_Chars_m1_442(L_38, 0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_39) == ((uint32_t)((int32_t)35)))))
		{
			goto IL_0135;
		}
	}
	{
		goto IL_023d;
	}

IL_0135:
	{
		String_t* L_40 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(XmlChar_t4_126_il2cpp_TypeInfo_var);
		int32_t L_41 = XmlChar_GetPredefinedEntity_m4_368(NULL /*static, unused*/, L_40, /*hidden argument*/NULL);
		if ((((int32_t)L_41) < ((int32_t)0)))
		{
			goto IL_0147;
		}
	}
	{
		goto IL_023d;
	}

IL_0147:
	{
		ArrayList_t1_170 * L_42 = (__this->___ReferencingEntities_17);
		String_t* L_43 = V_6;
		NullCheck(L_42);
		VirtFuncInvoker1< int32_t, Object_t * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_42, L_43);
		DTDObjectModel_t4_82 * L_44 = DTDNode_get_Root_m4_194(__this, /*hidden argument*/NULL);
		NullCheck(L_44);
		DTDEntityDeclarationCollection_t4_86 * L_45 = DTDObjectModel_get_EntityDecls_m4_144(L_44, /*hidden argument*/NULL);
		String_t* L_46 = V_6;
		NullCheck(L_45);
		DTDEntityDeclaration_t4_98 * L_47 = DTDEntityDeclarationCollection_get_Item_m4_171(L_45, L_46, /*hidden argument*/NULL);
		V_7 = L_47;
		DTDEntityDeclaration_t4_98 * L_48 = V_7;
		if (!L_48)
		{
			goto IL_0235;
		}
	}
	{
		DTDEntityDeclaration_t4_98 * L_49 = V_7;
		NullCheck(L_49);
		String_t* L_50 = DTDEntityBase_get_SystemId_m4_224(L_49, /*hidden argument*/NULL);
		if (!L_50)
		{
			goto IL_0183;
		}
	}
	{
		__this->___hasExternalReference_20 = 1;
	}

IL_0183:
	{
		ArrayList_t1_170 * L_51 = ___refs;
		String_t* L_52 = DTDEntityBase_get_Name_m4_220(__this, /*hidden argument*/NULL);
		NullCheck(L_51);
		VirtFuncInvoker1< int32_t, Object_t * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_51, L_52);
		DTDEntityDeclaration_t4_98 * L_53 = V_7;
		ArrayList_t1_170 * L_54 = ___refs;
		NullCheck(L_53);
		DTDEntityDeclaration_ScanEntityValue_m4_238(L_53, L_54, /*hidden argument*/NULL);
		DTDEntityDeclaration_t4_98 * L_55 = V_7;
		NullCheck(L_55);
		ArrayList_t1_170 * L_56 = (L_55->___ReferencingEntities_17);
		NullCheck(L_56);
		Object_t * L_57 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(50 /* System.Collections.IEnumerator System.Collections.ArrayList::GetEnumerator() */, L_56);
		V_9 = L_57;
	}

IL_01a6:
	try
	{ // begin try (depth: 1)
		{
			goto IL_01c7;
		}

IL_01ab:
		{
			Object_t * L_58 = V_9;
			NullCheck(L_58);
			Object_t * L_59 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_58);
			V_8 = ((String_t*)CastclassSealed(L_59, String_t_il2cpp_TypeInfo_var));
			ArrayList_t1_170 * L_60 = (__this->___ReferencingEntities_17);
			String_t* L_61 = V_8;
			NullCheck(L_60);
			VirtFuncInvoker1< int32_t, Object_t * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_60, L_61);
		}

IL_01c7:
		{
			Object_t * L_62 = V_9;
			NullCheck(L_62);
			bool L_63 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_62);
			if (L_63)
			{
				goto IL_01ab;
			}
		}

IL_01d3:
		{
			IL2CPP_LEAVE(0x1EE, FINALLY_01d8);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_01d8;
	}

FINALLY_01d8:
	{ // begin finally (depth: 1)
		{
			Object_t * L_64 = V_9;
			V_12 = ((Object_t *)IsInst(L_64, IDisposable_t1_1035_il2cpp_TypeInfo_var));
			Object_t * L_65 = V_12;
			if (L_65)
			{
				goto IL_01e6;
			}
		}

IL_01e5:
		{
			IL2CPP_END_FINALLY(472)
		}

IL_01e6:
		{
			Object_t * L_66 = V_12;
			NullCheck(L_66);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_66);
			IL2CPP_END_FINALLY(472)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(472)
	{
		IL2CPP_JUMP_TBL(0x1EE, IL_01ee)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_01ee:
	{
		ArrayList_t1_170 * L_67 = ___refs;
		String_t* L_68 = DTDEntityBase_get_Name_m4_220(__this, /*hidden argument*/NULL);
		NullCheck(L_67);
		VirtActionInvoker1< Object_t * >::Invoke(42 /* System.Void System.Collections.ArrayList::Remove(System.Object) */, L_67, L_68);
		String_t* L_69 = V_0;
		int32_t L_70 = V_4;
		String_t* L_71 = V_6;
		NullCheck(L_71);
		int32_t L_72 = String_get_Length_m1_571(L_71, /*hidden argument*/NULL);
		NullCheck(L_69);
		String_t* L_73 = String_Remove_m1_539(L_69, ((int32_t)((int32_t)L_70-(int32_t)1)), ((int32_t)((int32_t)L_72+(int32_t)2)), /*hidden argument*/NULL);
		V_0 = L_73;
		String_t* L_74 = V_0;
		int32_t L_75 = V_4;
		DTDEntityDeclaration_t4_98 * L_76 = V_7;
		NullCheck(L_76);
		String_t* L_77 = DTDEntityDeclaration_get_EntityValue_m4_237(L_76, /*hidden argument*/NULL);
		NullCheck(L_74);
		String_t* L_78 = String_Insert_m1_565(L_74, ((int32_t)((int32_t)L_75-(int32_t)1)), L_77, /*hidden argument*/NULL);
		V_0 = L_78;
		int32_t L_79 = V_5;
		String_t* L_80 = V_6;
		NullCheck(L_80);
		int32_t L_81 = String_get_Length_m1_571(L_80, /*hidden argument*/NULL);
		V_5 = ((int32_t)((int32_t)L_79-(int32_t)((int32_t)((int32_t)L_81+(int32_t)1))));
		String_t* L_82 = V_0;
		NullCheck(L_82);
		int32_t L_83 = String_get_Length_m1_571(L_82, /*hidden argument*/NULL);
		V_3 = L_83;
	}

IL_0235:
	{
		V_4 = 0;
		goto IL_023d;
	}

IL_023d:
	{
		int32_t L_84 = V_5;
		V_5 = ((int32_t)((int32_t)L_84+(int32_t)1));
	}

IL_0243:
	{
		int32_t L_85 = V_5;
		int32_t L_86 = V_3;
		if ((((int32_t)L_85) < ((int32_t)L_86)))
		{
			goto IL_00c2;
		}
	}
	{
		int32_t L_87 = V_4;
		if (!L_87)
		{
			goto IL_026e;
		}
	}
	{
		DTDObjectModel_t4_82 * L_88 = DTDNode_get_Root_m4_194(__this, /*hidden argument*/NULL);
		String_t* L_89 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String Mono.Xml.DTDNode::get_BaseURI() */, __this);
		XmlException_t4_137 * L_90 = (XmlException_t4_137 *)il2cpp_codegen_object_new (XmlException_t4_137_il2cpp_TypeInfo_var);
		XmlException__ctor_m4_524(L_90, __this, L_89, _stringLiteral3808, /*hidden argument*/NULL);
		NullCheck(L_88);
		DTDObjectModel_AddError_m4_147(L_88, L_90, /*hidden argument*/NULL);
	}

IL_026e:
	{
		__this->___scanned_18 = 1;
		__this->___recursed_19 = 0;
		return;
	}
}
// System.Void Mono.Xml.DTDNotationDeclaration::.ctor(Mono.Xml.DTDObjectModel)
extern "C" void DTDNotationDeclaration__ctor_m4_239 (DTDNotationDeclaration_t4_99 * __this, DTDObjectModel_t4_82 * ___root, const MethodInfo* method)
{
	{
		DTDNode__ctor_m4_186(__this, /*hidden argument*/NULL);
		DTDObjectModel_t4_82 * L_0 = ___root;
		DTDNode_SetRoot_m4_193(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String Mono.Xml.DTDNotationDeclaration::get_Name()
extern "C" String_t* DTDNotationDeclaration_get_Name_m4_240 (DTDNotationDeclaration_t4_99 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___name_5);
		return L_0;
	}
}
// System.Void Mono.Xml.DTDNotationDeclaration::set_Name(System.String)
extern "C" void DTDNotationDeclaration_set_Name_m4_241 (DTDNotationDeclaration_t4_99 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___name_5 = L_0;
		return;
	}
}
// System.String Mono.Xml.DTDNotationDeclaration::get_PublicId()
extern "C" String_t* DTDNotationDeclaration_get_PublicId_m4_242 (DTDNotationDeclaration_t4_99 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___publicId_8);
		return L_0;
	}
}
// System.Void Mono.Xml.DTDNotationDeclaration::set_PublicId(System.String)
extern "C" void DTDNotationDeclaration_set_PublicId_m4_243 (DTDNotationDeclaration_t4_99 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___publicId_8 = L_0;
		return;
	}
}
// System.String Mono.Xml.DTDNotationDeclaration::get_SystemId()
extern "C" String_t* DTDNotationDeclaration_get_SystemId_m4_244 (DTDNotationDeclaration_t4_99 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___systemId_9);
		return L_0;
	}
}
// System.Void Mono.Xml.DTDNotationDeclaration::set_SystemId(System.String)
extern "C" void DTDNotationDeclaration_set_SystemId_m4_245 (DTDNotationDeclaration_t4_99 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___systemId_9 = L_0;
		return;
	}
}
// System.String Mono.Xml.DTDNotationDeclaration::get_LocalName()
extern "C" String_t* DTDNotationDeclaration_get_LocalName_m4_246 (DTDNotationDeclaration_t4_99 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___localName_6);
		return L_0;
	}
}
// System.Void Mono.Xml.DTDNotationDeclaration::set_LocalName(System.String)
extern "C" void DTDNotationDeclaration_set_LocalName_m4_247 (DTDNotationDeclaration_t4_99 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___localName_6 = L_0;
		return;
	}
}
// System.String Mono.Xml.DTDNotationDeclaration::get_Prefix()
extern "C" String_t* DTDNotationDeclaration_get_Prefix_m4_248 (DTDNotationDeclaration_t4_99 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___prefix_7);
		return L_0;
	}
}
// System.Void Mono.Xml.DTDNotationDeclaration::set_Prefix(System.String)
extern "C" void DTDNotationDeclaration_set_Prefix_m4_249 (DTDNotationDeclaration_t4_99 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___prefix_7 = L_0;
		return;
	}
}
// System.Void Mono.Xml.DTDParameterEntityDeclarationCollection::.ctor(Mono.Xml.DTDObjectModel)
extern TypeInfo* Hashtable_t1_100_il2cpp_TypeInfo_var;
extern "C" void DTDParameterEntityDeclarationCollection__ctor_m4_250 (DTDParameterEntityDeclarationCollection_t4_85 * __this, DTDObjectModel_t4_82 * ___root, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Hashtable_t1_100_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(119);
		s_Il2CppMethodIntialized = true;
	}
	{
		Hashtable_t1_100 * L_0 = (Hashtable_t1_100 *)il2cpp_codegen_object_new (Hashtable_t1_100_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1_3279(L_0, /*hidden argument*/NULL);
		__this->___peDecls_0 = L_0;
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		DTDObjectModel_t4_82 * L_1 = ___root;
		__this->___root_1 = L_1;
		return;
	}
}
// Mono.Xml.DTDParameterEntityDeclaration Mono.Xml.DTDParameterEntityDeclarationCollection::get_Item(System.String)
extern TypeInfo* DTDParameterEntityDeclaration_t4_100_il2cpp_TypeInfo_var;
extern "C" DTDParameterEntityDeclaration_t4_100 * DTDParameterEntityDeclarationCollection_get_Item_m4_251 (DTDParameterEntityDeclarationCollection_t4_85 * __this, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DTDParameterEntityDeclaration_t4_100_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1300);
		s_Il2CppMethodIntialized = true;
	}
	{
		Hashtable_t1_100 * L_0 = (__this->___peDecls_0);
		String_t* L_1 = ___name;
		NullCheck(L_0);
		Object_t * L_2 = (Object_t *)VirtFuncInvoker1< Object_t *, Object_t * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_0, L_1);
		return ((DTDParameterEntityDeclaration_t4_100 *)IsInstClass(L_2, DTDParameterEntityDeclaration_t4_100_il2cpp_TypeInfo_var));
	}
}
// System.Void Mono.Xml.DTDParameterEntityDeclarationCollection::Add(System.String,Mono.Xml.DTDParameterEntityDeclaration)
extern "C" void DTDParameterEntityDeclarationCollection_Add_m4_252 (DTDParameterEntityDeclarationCollection_t4_85 * __this, String_t* ___name, DTDParameterEntityDeclaration_t4_100 * ___decl, const MethodInfo* method)
{
	{
		Hashtable_t1_100 * L_0 = (__this->___peDecls_0);
		String_t* L_1 = ___name;
		NullCheck(L_0);
		Object_t * L_2 = (Object_t *)VirtFuncInvoker1< Object_t *, Object_t * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_0, L_1);
		if (!L_2)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		DTDParameterEntityDeclaration_t4_100 * L_3 = ___decl;
		DTDObjectModel_t4_82 * L_4 = (__this->___root_1);
		NullCheck(L_3);
		DTDNode_SetRoot_m4_193(L_3, L_4, /*hidden argument*/NULL);
		Hashtable_t1_100 * L_5 = (__this->___peDecls_0);
		String_t* L_6 = ___name;
		DTDParameterEntityDeclaration_t4_100 * L_7 = ___decl;
		NullCheck(L_5);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_5, L_6, L_7);
		return;
	}
}
// System.Void Mono.Xml.DTDParameterEntityDeclaration::.ctor(Mono.Xml.DTDObjectModel)
extern "C" void DTDParameterEntityDeclaration__ctor_m4_253 (DTDParameterEntityDeclaration_t4_100 * __this, DTDObjectModel_t4_82 * ___root, const MethodInfo* method)
{
	{
		DTDObjectModel_t4_82 * L_0 = ___root;
		DTDEntityBase__ctor_m4_217(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
