﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Xml.DictionaryBase
struct DictionaryBase_t4_90;
// System.Collections.Generic.IEnumerable`1<Mono.Xml.DTDNode>
struct IEnumerable_1_t1_1808;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Xml.DictionaryBase::.ctor()
extern "C" void DictionaryBase__ctor_m4_158 (DictionaryBase_t4_90 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Mono.Xml.DTDNode> Mono.Xml.DictionaryBase::get_Values()
extern "C" Object_t* DictionaryBase_get_Values_m4_159 (DictionaryBase_t4_90 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
