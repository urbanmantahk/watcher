﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.DataConverter/CopyConverter
struct CopyConverter_t1_253;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.DataConverter/CopyConverter::.ctor()
extern "C" void CopyConverter__ctor_m1_2723 (CopyConverter_t1_253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double Mono.DataConverter/CopyConverter::GetDouble(System.Byte[],System.Int32)
extern "C" double CopyConverter_GetDouble_m1_2724 (CopyConverter_t1_253 * __this, ByteU5BU5D_t1_109* ___data, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 Mono.DataConverter/CopyConverter::GetUInt64(System.Byte[],System.Int32)
extern "C" uint64_t CopyConverter_GetUInt64_m1_2725 (CopyConverter_t1_253 * __this, ByteU5BU5D_t1_109* ___data, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Mono.DataConverter/CopyConverter::GetInt64(System.Byte[],System.Int32)
extern "C" int64_t CopyConverter_GetInt64_m1_2726 (CopyConverter_t1_253 * __this, ByteU5BU5D_t1_109* ___data, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Mono.DataConverter/CopyConverter::GetFloat(System.Byte[],System.Int32)
extern "C" float CopyConverter_GetFloat_m1_2727 (CopyConverter_t1_253 * __this, ByteU5BU5D_t1_109* ___data, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.DataConverter/CopyConverter::GetInt32(System.Byte[],System.Int32)
extern "C" int32_t CopyConverter_GetInt32_m1_2728 (CopyConverter_t1_253 * __this, ByteU5BU5D_t1_109* ___data, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Mono.DataConverter/CopyConverter::GetUInt32(System.Byte[],System.Int32)
extern "C" uint32_t CopyConverter_GetUInt32_m1_2729 (CopyConverter_t1_253 * __this, ByteU5BU5D_t1_109* ___data, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 Mono.DataConverter/CopyConverter::GetInt16(System.Byte[],System.Int32)
extern "C" int16_t CopyConverter_GetInt16_m1_2730 (CopyConverter_t1_253 * __this, ByteU5BU5D_t1_109* ___data, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 Mono.DataConverter/CopyConverter::GetUInt16(System.Byte[],System.Int32)
extern "C" uint16_t CopyConverter_GetUInt16_m1_2731 (CopyConverter_t1_253 * __this, ByteU5BU5D_t1_109* ___data, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.DataConverter/CopyConverter::PutBytes(System.Byte[],System.Int32,System.Double)
extern "C" void CopyConverter_PutBytes_m1_2732 (CopyConverter_t1_253 * __this, ByteU5BU5D_t1_109* ___dest, int32_t ___destIdx, double ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.DataConverter/CopyConverter::PutBytes(System.Byte[],System.Int32,System.Single)
extern "C" void CopyConverter_PutBytes_m1_2733 (CopyConverter_t1_253 * __this, ByteU5BU5D_t1_109* ___dest, int32_t ___destIdx, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.DataConverter/CopyConverter::PutBytes(System.Byte[],System.Int32,System.Int32)
extern "C" void CopyConverter_PutBytes_m1_2734 (CopyConverter_t1_253 * __this, ByteU5BU5D_t1_109* ___dest, int32_t ___destIdx, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.DataConverter/CopyConverter::PutBytes(System.Byte[],System.Int32,System.UInt32)
extern "C" void CopyConverter_PutBytes_m1_2735 (CopyConverter_t1_253 * __this, ByteU5BU5D_t1_109* ___dest, int32_t ___destIdx, uint32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.DataConverter/CopyConverter::PutBytes(System.Byte[],System.Int32,System.Int64)
extern "C" void CopyConverter_PutBytes_m1_2736 (CopyConverter_t1_253 * __this, ByteU5BU5D_t1_109* ___dest, int32_t ___destIdx, int64_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.DataConverter/CopyConverter::PutBytes(System.Byte[],System.Int32,System.UInt64)
extern "C" void CopyConverter_PutBytes_m1_2737 (CopyConverter_t1_253 * __this, ByteU5BU5D_t1_109* ___dest, int32_t ___destIdx, uint64_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.DataConverter/CopyConverter::PutBytes(System.Byte[],System.Int32,System.Int16)
extern "C" void CopyConverter_PutBytes_m1_2738 (CopyConverter_t1_253 * __this, ByteU5BU5D_t1_109* ___dest, int32_t ___destIdx, int16_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.DataConverter/CopyConverter::PutBytes(System.Byte[],System.Int32,System.UInt16)
extern "C" void CopyConverter_PutBytes_m1_2739 (CopyConverter_t1_253 * __this, ByteU5BU5D_t1_109* ___dest, int32_t ___destIdx, uint16_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
