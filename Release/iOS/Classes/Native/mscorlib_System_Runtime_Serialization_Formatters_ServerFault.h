﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Exception
struct Exception_t1_33;

#include "mscorlib_System_Object.h"

// System.Runtime.Serialization.Formatters.ServerFault
struct  ServerFault_t1_1070  : public Object_t
{
	// System.String System.Runtime.Serialization.Formatters.ServerFault::exceptionType
	String_t* ___exceptionType_0;
	// System.String System.Runtime.Serialization.Formatters.ServerFault::message
	String_t* ___message_1;
	// System.String System.Runtime.Serialization.Formatters.ServerFault::stackTrace
	String_t* ___stackTrace_2;
	// System.Exception System.Runtime.Serialization.Formatters.ServerFault::exception
	Exception_t1_33 * ___exception_3;
};
