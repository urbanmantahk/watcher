﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.CompilerServices.FixedBufferAttribute
struct FixedBufferAttribute_t1_56;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.CompilerServices.FixedBufferAttribute::.ctor(System.Type,System.Int32)
extern "C" void FixedBufferAttribute__ctor_m1_1323 (FixedBufferAttribute_t1_56 * __this, Type_t * ___elementType, int32_t ___length, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Runtime.CompilerServices.FixedBufferAttribute::get_ElementType()
extern "C" Type_t * FixedBufferAttribute_get_ElementType_m1_1324 (FixedBufferAttribute_t1_56 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.CompilerServices.FixedBufferAttribute::get_Length()
extern "C" int32_t FixedBufferAttribute_get_Length_m1_1325 (FixedBufferAttribute_t1_56 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
