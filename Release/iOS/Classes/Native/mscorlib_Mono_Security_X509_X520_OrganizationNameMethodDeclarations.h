﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.X509.X520/OrganizationName
struct OrganizationName_t1_206;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Security.X509.X520/OrganizationName::.ctor()
extern "C" void OrganizationName__ctor_m1_2406 (OrganizationName_t1_206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
