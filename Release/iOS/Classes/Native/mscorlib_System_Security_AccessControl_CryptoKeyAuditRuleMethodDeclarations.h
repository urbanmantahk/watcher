﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.AccessControl.CryptoKeyAuditRule
struct CryptoKeyAuditRule_t1_1140;
// System.Security.Principal.IdentityReference
struct IdentityReference_t1_1120;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_AccessControl_CryptoKeyRights.h"
#include "mscorlib_System_Security_AccessControl_AuditFlags.h"

// System.Void System.Security.AccessControl.CryptoKeyAuditRule::.ctor(System.Security.Principal.IdentityReference,System.Security.AccessControl.CryptoKeyRights,System.Security.AccessControl.AuditFlags)
extern "C" void CryptoKeyAuditRule__ctor_m1_9787 (CryptoKeyAuditRule_t1_1140 * __this, IdentityReference_t1_1120 * ___identity, int32_t ___cryptoKeyRights, int32_t ___flags, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.CryptoKeyAuditRule::.ctor(System.String,System.Security.AccessControl.CryptoKeyRights,System.Security.AccessControl.AuditFlags)
extern "C" void CryptoKeyAuditRule__ctor_m1_9788 (CryptoKeyAuditRule_t1_1140 * __this, String_t* ___identity, int32_t ___cryptoKeyRights, int32_t ___flags, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.AccessControl.CryptoKeyRights System.Security.AccessControl.CryptoKeyAuditRule::get_CryptoKeyRights()
extern "C" int32_t CryptoKeyAuditRule_get_CryptoKeyRights_m1_9789 (CryptoKeyAuditRule_t1_1140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
