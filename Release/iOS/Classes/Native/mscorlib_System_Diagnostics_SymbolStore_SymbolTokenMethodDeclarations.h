﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Diagnostics_SymbolStore_SymbolToken.h"

// System.Void System.Diagnostics.SymbolStore.SymbolToken::.ctor(System.Int32)
extern "C" void SymbolToken__ctor_m1_3566 (SymbolToken_t1_322 * __this, int32_t ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Diagnostics.SymbolStore.SymbolToken::Equals(System.Object)
extern "C" bool SymbolToken_Equals_m1_3567 (SymbolToken_t1_322 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Diagnostics.SymbolStore.SymbolToken::Equals(System.Diagnostics.SymbolStore.SymbolToken)
extern "C" bool SymbolToken_Equals_m1_3568 (SymbolToken_t1_322 * __this, SymbolToken_t1_322  ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Diagnostics.SymbolStore.SymbolToken::GetHashCode()
extern "C" int32_t SymbolToken_GetHashCode_m1_3569 (SymbolToken_t1_322 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Diagnostics.SymbolStore.SymbolToken::GetToken()
extern "C" int32_t SymbolToken_GetToken_m1_3570 (SymbolToken_t1_322 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Diagnostics.SymbolStore.SymbolToken::op_Equality(System.Diagnostics.SymbolStore.SymbolToken,System.Diagnostics.SymbolStore.SymbolToken)
extern "C" bool SymbolToken_op_Equality_m1_3571 (Object_t * __this /* static, unused */, SymbolToken_t1_322  ___a, SymbolToken_t1_322  ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Diagnostics.SymbolStore.SymbolToken::op_Inequality(System.Diagnostics.SymbolStore.SymbolToken,System.Diagnostics.SymbolStore.SymbolToken)
extern "C" bool SymbolToken_op_Inequality_m1_3572 (Object_t * __this /* static, unused */, SymbolToken_t1_322  ___a, SymbolToken_t1_322  ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
