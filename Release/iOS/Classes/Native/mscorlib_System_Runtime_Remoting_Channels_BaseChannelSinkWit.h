﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Runtime_Remoting_Channels_BaseChannelObjectW.h"

// System.Runtime.Remoting.Channels.BaseChannelSinkWithProperties
struct  BaseChannelSinkWithProperties_t1_865  : public BaseChannelObjectWithProperties_t1_864
{
};
