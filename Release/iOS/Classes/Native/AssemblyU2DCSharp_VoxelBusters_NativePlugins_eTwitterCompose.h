﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eTwitterCompose.h"

// VoxelBusters.NativePlugins.eTwitterComposerResult
struct  eTwitterComposerResult_t8_293 
{
	// System.Int32 VoxelBusters.NativePlugins.eTwitterComposerResult::value__
	int32_t ___value___1;
};
