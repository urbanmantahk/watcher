﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_ConsoleColor.h"

// System.ConsoleColor
struct  ConsoleColor_t1_1514 
{
	// System.Int32 System.ConsoleColor::value__
	int32_t ___value___1;
};
