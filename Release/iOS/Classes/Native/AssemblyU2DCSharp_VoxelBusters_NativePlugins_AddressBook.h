﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VoxelBusters.NativePlugins.AddressBook/RequestAccessCompletion
struct RequestAccessCompletion_t8_193;
// VoxelBusters.NativePlugins.AddressBook/ReadContactsCompletion
struct ReadContactsCompletion_t8_194;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// VoxelBusters.NativePlugins.AddressBook
struct  AddressBook_t8_196  : public MonoBehaviour_t6_91
{
	// VoxelBusters.NativePlugins.AddressBook/RequestAccessCompletion VoxelBusters.NativePlugins.AddressBook::RequestAccessFinishedEvent
	RequestAccessCompletion_t8_193 * ___RequestAccessFinishedEvent_2;
	// VoxelBusters.NativePlugins.AddressBook/ReadContactsCompletion VoxelBusters.NativePlugins.AddressBook::ReadContactsFinishedEvent
	ReadContactsCompletion_t8_194 * ___ReadContactsFinishedEvent_3;
};
