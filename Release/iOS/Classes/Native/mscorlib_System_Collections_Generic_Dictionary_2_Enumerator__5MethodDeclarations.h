﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>
struct Dictionary_2_t1_2221;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__5.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_7.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m1_18376_gshared (Enumerator_t1_2227 * __this, Dictionary_2_t1_2221 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m1_18376(__this, ___dictionary, method) (( void (*) (Enumerator_t1_2227 *, Dictionary_2_t1_2221 *, const MethodInfo*))Enumerator__ctor_m1_18376_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m1_18377_gshared (Enumerator_t1_2227 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_18377(__this, method) (( Object_t * (*) (Enumerator_t1_2227 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_18377_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m1_18378_gshared (Enumerator_t1_2227 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1_18378(__this, method) (( void (*) (Enumerator_t1_2227 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_18378_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t1_284  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_18379_gshared (Enumerator_t1_2227 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_18379(__this, method) (( DictionaryEntry_t1_284  (*) (Enumerator_t1_2227 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_18379_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_18380_gshared (Enumerator_t1_2227 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_18380(__this, method) (( Object_t * (*) (Enumerator_t1_2227 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_18380_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_18381_gshared (Enumerator_t1_2227 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_18381(__this, method) (( Object_t * (*) (Enumerator_t1_2227 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_18381_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::MoveNext()
extern "C" bool Enumerator_MoveNext_m1_18382_gshared (Enumerator_t1_2227 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1_18382(__this, method) (( bool (*) (Enumerator_t1_2227 *, const MethodInfo*))Enumerator_MoveNext_m1_18382_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::get_Current()
extern "C" KeyValuePair_2_t1_2223  Enumerator_get_Current_m1_18383_gshared (Enumerator_t1_2227 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1_18383(__this, method) (( KeyValuePair_2_t1_2223  (*) (Enumerator_t1_2227 *, const MethodInfo*))Enumerator_get_Current_m1_18383_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m1_18384_gshared (Enumerator_t1_2227 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m1_18384(__this, method) (( Object_t * (*) (Enumerator_t1_2227 *, const MethodInfo*))Enumerator_get_CurrentKey_m1_18384_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::get_CurrentValue()
extern "C" bool Enumerator_get_CurrentValue_m1_18385_gshared (Enumerator_t1_2227 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m1_18385(__this, method) (( bool (*) (Enumerator_t1_2227 *, const MethodInfo*))Enumerator_get_CurrentValue_m1_18385_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::Reset()
extern "C" void Enumerator_Reset_m1_18386_gshared (Enumerator_t1_2227 * __this, const MethodInfo* method);
#define Enumerator_Reset_m1_18386(__this, method) (( void (*) (Enumerator_t1_2227 *, const MethodInfo*))Enumerator_Reset_m1_18386_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::VerifyState()
extern "C" void Enumerator_VerifyState_m1_18387_gshared (Enumerator_t1_2227 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m1_18387(__this, method) (( void (*) (Enumerator_t1_2227 *, const MethodInfo*))Enumerator_VerifyState_m1_18387_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m1_18388_gshared (Enumerator_t1_2227 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m1_18388(__this, method) (( void (*) (Enumerator_t1_2227 *, const MethodInfo*))Enumerator_VerifyCurrent_m1_18388_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::Dispose()
extern "C" void Enumerator_Dispose_m1_18389_gshared (Enumerator_t1_2227 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1_18389(__this, method) (( void (*) (Enumerator_t1_2227 *, const MethodInfo*))Enumerator_Dispose_m1_18389_gshared)(__this, method)
