﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.AssemblyCultureAttribute
struct AssemblyCultureAttribute_t1_49;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Reflection.AssemblyCultureAttribute::.ctor(System.String)
extern "C" void AssemblyCultureAttribute__ctor_m1_1311 (AssemblyCultureAttribute_t1_49 * __this, String_t* ___culture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.AssemblyCultureAttribute::get_Culture()
extern "C" String_t* AssemblyCultureAttribute_get_Culture_m1_1312 (AssemblyCultureAttribute_t1_49 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
