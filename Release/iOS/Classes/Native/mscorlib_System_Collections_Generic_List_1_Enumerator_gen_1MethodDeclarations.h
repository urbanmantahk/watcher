﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_14MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode>>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1_18023(__this, ___l, method) (( void (*) (Enumerator_t1_1804 *, List_1_t1_1806 *, const MethodInfo*))Enumerator__ctor_m1_17879_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1_18024(__this, method) (( void (*) (Enumerator_t1_1804 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_17880_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_18025(__this, method) (( Object_t * (*) (Enumerator_t1_1804 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_17881_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode>>::Dispose()
#define Enumerator_Dispose_m1_18026(__this, method) (( void (*) (Enumerator_t1_1804 *, const MethodInfo*))Enumerator_Dispose_m1_17882_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode>>::VerifyState()
#define Enumerator_VerifyState_m1_18027(__this, method) (( void (*) (Enumerator_t1_1804 *, const MethodInfo*))Enumerator_VerifyState_m1_17883_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode>>::MoveNext()
#define Enumerator_MoveNext_m1_14923(__this, method) (( bool (*) (Enumerator_t1_1804 *, const MethodInfo*))Enumerator_MoveNext_m1_17884_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode>>::get_Current()
#define Enumerator_get_Current_m1_14921(__this, method) (( KeyValuePair_2_t1_1805  (*) (Enumerator_t1_1804 *, const MethodInfo*))Enumerator_get_Current_m1_17885_gshared)(__this, method)
