﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_Mutabl.h"

// VoxelBusters.NativePlugins.Internal.EditorBillingProduct
struct  EditorBillingProduct_t8_208  : public MutableBillingProduct_t8_206
{
};
