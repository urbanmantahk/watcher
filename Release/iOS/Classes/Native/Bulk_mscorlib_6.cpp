﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.Runtime.CompilerServices.HasCopySemanticsAttribute
struct HasCopySemanticsAttribute_t1_687;
// System.Runtime.CompilerServices.IDispatchConstantAttribute
struct IDispatchConstantAttribute_t1_688;
// System.Object
struct Object_t;
// System.Runtime.CompilerServices.IUnknownConstantAttribute
struct IUnknownConstantAttribute_t1_689;
// System.Runtime.CompilerServices.NativeCppClassAttribute
struct NativeCppClassAttribute_t1_705;
// System.Runtime.CompilerServices.RuntimeWrappedException
struct RuntimeWrappedException_t1_706;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;
// System.Runtime.CompilerServices.ScopelessEnumAttribute
struct ScopelessEnumAttribute_t1_707;
// System.Runtime.CompilerServices.SpecialNameAttribute
struct SpecialNameAttribute_t1_708;
// System.Runtime.CompilerServices.StringFreezingAttribute
struct StringFreezingAttribute_t1_709;
// System.Runtime.CompilerServices.SuppressIldasmAttribute
struct SuppressIldasmAttribute_t1_710;
// System.Runtime.ConstrainedExecution.CriticalFinalizerObject
struct CriticalFinalizerObject_t1_713;
// System.Runtime.ConstrainedExecution.PrePrepareMethodAttribute
struct PrePrepareMethodAttribute_t1_714;
// System.Runtime.ConstrainedExecution.ReliabilityContractAttribute
struct ReliabilityContractAttribute_t1_715;
// System.Runtime.Hosting.ActivationArguments
struct ActivationArguments_t1_716;
// System.ActivationContext
struct ActivationContext_t1_717;
// System.ApplicationIdentity
struct ApplicationIdentity_t1_718;
// System.String[]
struct StringU5BU5D_t1_238;
// System.Runtime.Hosting.ApplicationActivator
struct ApplicationActivator_t1_719;
// System.Runtime.Remoting.ObjectHandle
struct ObjectHandle_t1_1019;
// System.AppDomainSetup
struct AppDomainSetup_t1_1497;
// System.Runtime.InteropServices.AllowReversePInvokeCallsAttribute
struct AllowReversePInvokeCallsAttribute_t1_751;
// System.Runtime.InteropServices.AutomationProxyAttribute
struct AutomationProxyAttribute_t1_754;
// System.Runtime.InteropServices.BStrWrapper
struct BStrWrapper_t1_757;
// System.String
struct String_t;
// System.Runtime.InteropServices.BestFitMappingAttribute
struct BestFitMappingAttribute_t1_758;
// System.Runtime.InteropServices.COMException
struct COMException_t1_760;
// System.Exception
struct Exception_t1_33;
// System.Runtime.InteropServices.ClassInterfaceAttribute
struct ClassInterfaceAttribute_t1_765;
// System.Runtime.InteropServices.ComAliasNameAttribute
struct ComAliasNameAttribute_t1_767;
// System.Runtime.InteropServices.ComCompatibleVersionAttribute
struct ComCompatibleVersionAttribute_t1_768;
// System.Runtime.InteropServices.ComConversionLossAttribute
struct ComConversionLossAttribute_t1_769;
// System.Runtime.InteropServices.ComDefaultInterfaceAttribute
struct ComDefaultInterfaceAttribute_t1_770;
// System.Type
struct Type_t;
// System.Runtime.InteropServices.ComEventInterfaceAttribute
struct ComEventInterfaceAttribute_t1_771;
// System.Runtime.InteropServices.ComRegisterFunctionAttribute
struct ComRegisterFunctionAttribute_t1_774;
// System.Runtime.InteropServices.ComSourceInterfacesAttribute
struct ComSourceInterfacesAttribute_t1_775;
// System.Runtime.InteropServices.ComUnregisterFunctionAttribute
struct ComUnregisterFunctionAttribute_t1_776;
// System.Runtime.InteropServices.CriticalHandle
struct CriticalHandle_t1_83;
// System.Runtime.InteropServices.CurrencyWrapper
struct CurrencyWrapper_t1_777;
// System.Runtime.InteropServices.DispIdAttribute
struct DispIdAttribute_t1_780;
// System.Runtime.InteropServices.DispatchWrapper
struct DispatchWrapper_t1_781;
// System.Runtime.InteropServices.ErrorWrapper
struct ErrorWrapper_t1_788;
// System.Runtime.InteropServices.ExtensibleClassFactory
struct ExtensibleClassFactory_t1_790;
// System.Runtime.InteropServices.ObjectCreationDelegate
struct ObjectCreationDelegate_t1_1621;
// System.Runtime.InteropServices.ExternalException
struct ExternalException_t1_761;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array.h"
#include "mscorlib_System_Runtime_CompilerServices_HasCopySemanticsAtt.h"
#include "mscorlib_System_Runtime_CompilerServices_HasCopySemanticsAttMethodDeclarations.h"
#include "mscorlib_System_Void.h"
#include "mscorlib_System_AttributeMethodDeclarations.h"
#include "mscorlib_System_Attribute.h"
#include "mscorlib_System_Runtime_CompilerServices_IDispatchConstantAt.h"
#include "mscorlib_System_Runtime_CompilerServices_IDispatchConstantAtMethodDeclarations.h"
#include "mscorlib_System_Runtime_CompilerServices_CustomConstantAttriMethodDeclarations.h"
#include "mscorlib_System_Runtime_CompilerServices_CustomConstantAttri.h"
#include "mscorlib_System_Object.h"
#include "mscorlib_System_Runtime_CompilerServices_IUnknownConstantAtt.h"
#include "mscorlib_System_Runtime_CompilerServices_IUnknownConstantAttMethodDeclarations.h"
#include "mscorlib_System_Runtime_CompilerServices_IsBoxed.h"
#include "mscorlib_System_Runtime_CompilerServices_IsBoxedMethodDeclarations.h"
#include "mscorlib_System_Runtime_CompilerServices_IsByValue.h"
#include "mscorlib_System_Runtime_CompilerServices_IsByValueMethodDeclarations.h"
#include "mscorlib_System_Runtime_CompilerServices_IsConst.h"
#include "mscorlib_System_Runtime_CompilerServices_IsConstMethodDeclarations.h"
#include "mscorlib_System_Runtime_CompilerServices_IsCopyConstructed.h"
#include "mscorlib_System_Runtime_CompilerServices_IsCopyConstructedMethodDeclarations.h"
#include "mscorlib_System_Runtime_CompilerServices_IsExplicitlyDerefer.h"
#include "mscorlib_System_Runtime_CompilerServices_IsExplicitlyDereferMethodDeclarations.h"
#include "mscorlib_System_Runtime_CompilerServices_IsImplicitlyDerefer.h"
#include "mscorlib_System_Runtime_CompilerServices_IsImplicitlyDereferMethodDeclarations.h"
#include "mscorlib_System_Runtime_CompilerServices_IsJitIntrinsic.h"
#include "mscorlib_System_Runtime_CompilerServices_IsJitIntrinsicMethodDeclarations.h"
#include "mscorlib_System_Runtime_CompilerServices_IsLong.h"
#include "mscorlib_System_Runtime_CompilerServices_IsLongMethodDeclarations.h"
#include "mscorlib_System_Runtime_CompilerServices_IsPinned.h"
#include "mscorlib_System_Runtime_CompilerServices_IsPinnedMethodDeclarations.h"
#include "mscorlib_System_Runtime_CompilerServices_IsSignUnspecifiedBy.h"
#include "mscorlib_System_Runtime_CompilerServices_IsSignUnspecifiedByMethodDeclarations.h"
#include "mscorlib_System_Runtime_CompilerServices_IsUdtReturn.h"
#include "mscorlib_System_Runtime_CompilerServices_IsUdtReturnMethodDeclarations.h"
#include "mscorlib_System_Runtime_CompilerServices_IsVolatile.h"
#include "mscorlib_System_Runtime_CompilerServices_IsVolatileMethodDeclarations.h"
#include "mscorlib_System_Runtime_CompilerServices_LoadHint.h"
#include "mscorlib_System_Runtime_CompilerServices_LoadHintMethodDeclarations.h"
#include "mscorlib_System_Runtime_CompilerServices_MethodCodeType.h"
#include "mscorlib_System_Runtime_CompilerServices_MethodCodeTypeMethodDeclarations.h"
#include "mscorlib_System_Runtime_CompilerServices_MethodImplOptions.h"
#include "mscorlib_System_Runtime_CompilerServices_MethodImplOptionsMethodDeclarations.h"
#include "mscorlib_System_Runtime_CompilerServices_NativeCppClassAttri.h"
#include "mscorlib_System_Runtime_CompilerServices_NativeCppClassAttriMethodDeclarations.h"
#include "mscorlib_System_Runtime_CompilerServices_RuntimeWrappedExcep.h"
#include "mscorlib_System_Runtime_CompilerServices_RuntimeWrappedExcepMethodDeclarations.h"
#include "mscorlib_System_ExceptionMethodDeclarations.h"
#include "mscorlib_System_Exception.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Runtime_Serialization_SerializationInfo.h"
#include "mscorlib_System_Runtime_Serialization_SerializationInfoMethodDeclarations.h"
#include "mscorlib_System_String.h"
#include "mscorlib_System_Runtime_CompilerServices_ScopelessEnumAttrib.h"
#include "mscorlib_System_Runtime_CompilerServices_ScopelessEnumAttribMethodDeclarations.h"
#include "mscorlib_System_Runtime_CompilerServices_SpecialNameAttribut.h"
#include "mscorlib_System_Runtime_CompilerServices_SpecialNameAttributMethodDeclarations.h"
#include "mscorlib_System_Runtime_CompilerServices_StringFreezingAttri.h"
#include "mscorlib_System_Runtime_CompilerServices_StringFreezingAttriMethodDeclarations.h"
#include "mscorlib_System_Runtime_CompilerServices_SuppressIldasmAttri.h"
#include "mscorlib_System_Runtime_CompilerServices_SuppressIldasmAttriMethodDeclarations.h"
#include "mscorlib_System_Runtime_ConstrainedExecution_Cer.h"
#include "mscorlib_System_Runtime_ConstrainedExecution_CerMethodDeclarations.h"
#include "mscorlib_System_Runtime_ConstrainedExecution_Consistency.h"
#include "mscorlib_System_Runtime_ConstrainedExecution_ConsistencyMethodDeclarations.h"
#include "mscorlib_System_Runtime_ConstrainedExecution_CriticalFinaliz.h"
#include "mscorlib_System_Runtime_ConstrainedExecution_CriticalFinalizMethodDeclarations.h"
#include "mscorlib_System_ObjectMethodDeclarations.h"
#include "mscorlib_System_Runtime_ConstrainedExecution_PrePrepareMetho.h"
#include "mscorlib_System_Runtime_ConstrainedExecution_PrePrepareMethoMethodDeclarations.h"
#include "mscorlib_System_Runtime_ConstrainedExecution_ReliabilityCont.h"
#include "mscorlib_System_Runtime_ConstrainedExecution_ReliabilityContMethodDeclarations.h"
#include "mscorlib_System_Runtime_Hosting_ActivationArguments.h"
#include "mscorlib_System_Runtime_Hosting_ActivationArgumentsMethodDeclarations.h"
#include "mscorlib_System_ActivationContext.h"
#include "mscorlib_System_ArgumentNullExceptionMethodDeclarations.h"
#include "mscorlib_System_ActivationContextMethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException.h"
#include "mscorlib_System_ApplicationIdentity.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Runtime_Hosting_ApplicationActivator.h"
#include "mscorlib_System_Runtime_Hosting_ApplicationActivatorMethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_ObjectHandle.h"
#include "mscorlib_System_AppDomainSetupMethodDeclarations.h"
#include "mscorlib_System_AppDomainSetup.h"
#include "mscorlib_LocaleMethodDeclarations.h"
#include "mscorlib_System_StringMethodDeclarations.h"
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
#include "mscorlib_System_AppDomainMethodDeclarations.h"
#include "mscorlib_System_Security_HostSecurityManagerMethodDeclarations.h"
#include "mscorlib_System_Security_Policy_EvidenceMethodDeclarations.h"
#include "mscorlib_System_Security_Policy_TrustManagerContextMethodDeclarations.h"
#include "mscorlib_System_Security_Policy_ApplicationTrustMethodDeclarations.h"
#include "mscorlib_System_Security_Policy_PolicyExceptionMethodDeclarations.h"
#include "mscorlib_System_Security_HostSecurityManager.h"
#include "mscorlib_System_Security_Policy_Evidence.h"
#include "mscorlib_System_Security_Policy_TrustManagerContext.h"
#include "mscorlib_System_Security_Policy_ApplicationTrust.h"
#include "mscorlib_System_AppDomain.h"
#include "mscorlib_System_ArgumentException.h"
#include "mscorlib_System_AppDomainManager.h"
#include "mscorlib_System_AppDomainManagerMethodDeclarations.h"
#include "mscorlib_System_Boolean.h"
#include "mscorlib_System_Security_Policy_PolicyException.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_BINDPTR.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_BINDPTRMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_BIND_OPTS.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_BIND_OPTSMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_CALLCONV.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_CALLCONVMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_CONNECTDATA.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_CONNECTDATAMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_DESCKIND.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_DESCKINDMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_DISPPARAMS.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_DISPPARAMSMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_ELEMDESC_DE.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_ELEMDESC_DEMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_IDLDESC.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_IDLDESCMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_PARAMDESC.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_PARAMDESCMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_ELEMDESC.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_ELEMDESCMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_TYPEDESC.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_EXCEPINFO.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_EXCEPINFOMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_FILETIME.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_FILETIMEMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_FUNCDESC.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_FUNCDESCMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_FUNCKIND.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_INVOKEKIND.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_FUNCFLAGS.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_FUNCFLAGSMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_FUNCKINDMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_IDLFLAG.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_IDLFLAGMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_IMPLTYPEFLA.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_IMPLTYPEFLAMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_INVOKEKINDMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_LIBFLAGS.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_LIBFLAGSMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_PARAMFLAG.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_PARAMFLAGMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_STATSTG.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_STATSTGMethodDeclarations.h"
#include "mscorlib_System_Guid.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_SYSKIND.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_SYSKINDMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_TYPEATTR.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_TYPEATTRMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_TYPEKIND.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_TYPEFLAGS.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_TYPEDESCMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_TYPEFLAGSMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_TYPEKINDMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_TYPELIBATTR.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_TYPELIBATTRMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_VARDESC_DES.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_VARDESC_DESMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_VARDESC.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_VARDESCMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_VARKIND.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_VARFLAGS.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_VARFLAGSMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_VARKINDMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_AllowReversePInvokeC.h"
#include "mscorlib_System_Runtime_InteropServices_AllowReversePInvokeCMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ArrayWithOffset.h"
#include "mscorlib_System_Runtime_InteropServices_ArrayWithOffsetMethodDeclarations.h"
#include "mscorlib_System_Int32.h"
#include "mscorlib_System_Runtime_InteropServices_AssemblyRegistration.h"
#include "mscorlib_System_Runtime_InteropServices_AssemblyRegistrationMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_AutomationProxyAttri.h"
#include "mscorlib_System_Runtime_InteropServices_AutomationProxyAttriMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_BINDPTR.h"
#include "mscorlib_System_Runtime_InteropServices_BINDPTRMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_BIND_OPTS.h"
#include "mscorlib_System_Runtime_InteropServices_BIND_OPTSMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_BStrWrapper.h"
#include "mscorlib_System_Runtime_InteropServices_BStrWrapperMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_BestFitMappingAttrib.h"
#include "mscorlib_System_Runtime_InteropServices_BestFitMappingAttribMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_CALLCONV.h"
#include "mscorlib_System_Runtime_InteropServices_CALLCONVMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_COMException.h"
#include "mscorlib_System_Runtime_InteropServices_COMExceptionMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ExternalExceptionMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ExternalException.h"
#include "mscorlib_System_EnvironmentMethodDeclarations.h"
#include "mscorlib_System_Type.h"
#include "mscorlib_System_Runtime_InteropServices_CONNECTDATA.h"
#include "mscorlib_System_Runtime_InteropServices_CONNECTDATAMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_CallingConvention.h"
#include "mscorlib_System_Runtime_InteropServices_CallingConventionMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_CharSet.h"
#include "mscorlib_System_Runtime_InteropServices_CharSetMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ClassInterfaceAttrib.h"
#include "mscorlib_System_Runtime_InteropServices_ClassInterfaceAttribMethodDeclarations.h"
#include "mscorlib_System_Int16.h"
#include "mscorlib_System_Runtime_InteropServices_ClassInterfaceType.h"
#include "mscorlib_System_Runtime_InteropServices_ClassInterfaceTypeMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ComAliasNameAttribut.h"
#include "mscorlib_System_Runtime_InteropServices_ComAliasNameAttributMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ComCompatibleVersion.h"
#include "mscorlib_System_Runtime_InteropServices_ComCompatibleVersionMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ComConversionLossAtt.h"
#include "mscorlib_System_Runtime_InteropServices_ComConversionLossAttMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ComDefaultInterfaceA.h"
#include "mscorlib_System_Runtime_InteropServices_ComDefaultInterfaceAMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ComEventInterfaceAtt.h"
#include "mscorlib_System_Runtime_InteropServices_ComEventInterfaceAttMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ComInterfaceType.h"
#include "mscorlib_System_Runtime_InteropServices_ComInterfaceTypeMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ComMemberType.h"
#include "mscorlib_System_Runtime_InteropServices_ComMemberTypeMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ComRegisterFunctionA.h"
#include "mscorlib_System_Runtime_InteropServices_ComRegisterFunctionAMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ComSourceInterfacesA.h"
#include "mscorlib_System_Runtime_InteropServices_ComSourceInterfacesAMethodDeclarations.h"
#include "mscorlib_System_TypeMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ComUnregisterFunctio.h"
#include "mscorlib_System_Runtime_InteropServices_ComUnregisterFunctioMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_CriticalHandle.h"
#include "mscorlib_System_Runtime_InteropServices_CriticalHandleMethodDeclarations.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_GCMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_CurrencyWrapper.h"
#include "mscorlib_System_Runtime_InteropServices_CurrencyWrapperMethodDeclarations.h"
#include "mscorlib_System_Decimal.h"
#include "mscorlib_System_RuntimeTypeHandle.h"
#include "mscorlib_System_Runtime_InteropServices_DESCKIND.h"
#include "mscorlib_System_Runtime_InteropServices_DESCKINDMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_DISPPARAMS.h"
#include "mscorlib_System_Runtime_InteropServices_DISPPARAMSMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_DispIdAttribute.h"
#include "mscorlib_System_Runtime_InteropServices_DispIdAttributeMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_DispatchWrapper.h"
#include "mscorlib_System_Runtime_InteropServices_DispatchWrapperMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_MarshalMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ELEMDESC_DESCUNION.h"
#include "mscorlib_System_Runtime_InteropServices_ELEMDESC_DESCUNIONMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_IDLDESC.h"
#include "mscorlib_System_Runtime_InteropServices_IDLDESCMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_PARAMDESC.h"
#include "mscorlib_System_Runtime_InteropServices_PARAMDESCMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ELEMDESC.h"
#include "mscorlib_System_Runtime_InteropServices_ELEMDESCMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_TYPEDESC.h"
#include "mscorlib_System_Runtime_InteropServices_EXCEPINFO.h"
#include "mscorlib_System_Runtime_InteropServices_EXCEPINFOMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ErrorWrapper.h"
#include "mscorlib_System_Runtime_InteropServices_ErrorWrapperMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ExporterEventKind.h"
#include "mscorlib_System_Runtime_InteropServices_ExporterEventKindMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ExtensibleClassFacto.h"
#include "mscorlib_System_Runtime_InteropServices_ExtensibleClassFactoMethodDeclarations.h"
#include "mscorlib_System_Collections_HashtableMethodDeclarations.h"
#include "mscorlib_System_Collections_Hashtable.h"
#include "mscorlib_System_Runtime_InteropServices_ObjectCreationDelega.h"
#include "mscorlib_System_Diagnostics_StackTraceMethodDeclarations.h"
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
#include "mscorlib_System_Diagnostics_StackTrace.h"
#include "mscorlib_System_Diagnostics_StackFrame.h"
#include "mscorlib_System_Reflection_MethodBase.h"
#include "mscorlib_System_Diagnostics_StackFrameMethodDeclarations.h"
#include "mscorlib_System_Reflection_MemberInfo.h"
#include "mscorlib_System_Reflection_MemberInfoMethodDeclarations.h"
#include "mscorlib_System_Reflection_MemberTypes.h"
#include "mscorlib_System_Reflection_MethodBaseMethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException.h"
#include "mscorlib_System_SystemExceptionMethodDeclarations.h"
#include "mscorlib_System_SystemException.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Runtime.CompilerServices.HasCopySemanticsAttribute::.ctor()
extern "C" void HasCopySemanticsAttribute__ctor_m1_7545 (HasCopySemanticsAttribute_t1_687 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.CompilerServices.IDispatchConstantAttribute::.ctor()
extern "C" void IDispatchConstantAttribute__ctor_m1_7546 (IDispatchConstantAttribute_t1_688 * __this, const MethodInfo* method)
{
	{
		CustomConstantAttribute__ctor_m1_7534(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object System.Runtime.CompilerServices.IDispatchConstantAttribute::get_Value()
extern "C" Object_t * IDispatchConstantAttribute_get_Value_m1_7547 (IDispatchConstantAttribute_t1_688 * __this, const MethodInfo* method)
{
	{
		return NULL;
	}
}
// System.Void System.Runtime.CompilerServices.IUnknownConstantAttribute::.ctor()
extern "C" void IUnknownConstantAttribute__ctor_m1_7548 (IUnknownConstantAttribute_t1_689 * __this, const MethodInfo* method)
{
	{
		CustomConstantAttribute__ctor_m1_7534(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object System.Runtime.CompilerServices.IUnknownConstantAttribute::get_Value()
extern "C" Object_t * IUnknownConstantAttribute_get_Value_m1_7549 (IUnknownConstantAttribute_t1_689 * __this, const MethodInfo* method)
{
	{
		return NULL;
	}
}
// System.Void System.Runtime.CompilerServices.NativeCppClassAttribute::.ctor()
extern "C" void NativeCppClassAttribute__ctor_m1_7550 (NativeCppClassAttribute_t1_705 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.CompilerServices.RuntimeWrappedException::.ctor()
extern "C" void RuntimeWrappedException__ctor_m1_7551 (RuntimeWrappedException_t1_706 * __this, const MethodInfo* method)
{
	{
		Exception__ctor_m1_1237(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object System.Runtime.CompilerServices.RuntimeWrappedException::get_WrappedException()
extern "C" Object_t * RuntimeWrappedException_get_WrappedException_m1_7552 (RuntimeWrappedException_t1_706 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___wrapped_exception_11);
		return L_0;
	}
}
// System.Void System.Runtime.CompilerServices.RuntimeWrappedException::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern Il2CppCodeGenString* _stringLiteral2096;
extern "C" void RuntimeWrappedException_GetObjectData_m1_7553 (RuntimeWrappedException_t1_706 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral2096 = il2cpp_codegen_string_literal_from_index(2096);
		s_Il2CppMethodIntialized = true;
	}
	{
		SerializationInfo_t1_293 * L_0 = ___info;
		StreamingContext_t1_1050  L_1 = ___context;
		Exception_GetObjectData_m1_1256(__this, L_0, L_1, /*hidden argument*/NULL);
		SerializationInfo_t1_293 * L_2 = ___info;
		Object_t * L_3 = (__this->___wrapped_exception_11);
		NullCheck(L_2);
		SerializationInfo_AddValue_m1_9642(L_2, _stringLiteral2096, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.CompilerServices.ScopelessEnumAttribute::.ctor()
extern "C" void ScopelessEnumAttribute__ctor_m1_7554 (ScopelessEnumAttribute_t1_707 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.CompilerServices.SpecialNameAttribute::.ctor()
extern "C" void SpecialNameAttribute__ctor_m1_7555 (SpecialNameAttribute_t1_708 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.CompilerServices.StringFreezingAttribute::.ctor()
extern "C" void StringFreezingAttribute__ctor_m1_7556 (StringFreezingAttribute_t1_709 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.CompilerServices.SuppressIldasmAttribute::.ctor()
extern "C" void SuppressIldasmAttribute__ctor_m1_7557 (SuppressIldasmAttribute_t1_710 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.ConstrainedExecution.CriticalFinalizerObject::.ctor()
extern "C" void CriticalFinalizerObject__ctor_m1_7558 (CriticalFinalizerObject_t1_713 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.ConstrainedExecution.CriticalFinalizerObject::Finalize()
extern "C" void CriticalFinalizerObject_Finalize_m1_7559 (CriticalFinalizerObject_t1_713 * __this, const MethodInfo* method)
{
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0xC, FINALLY_0005);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0005;
	}

FINALLY_0005:
	{ // begin finally (depth: 1)
		Object_Finalize_m1_3(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(5)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(5)
	{
		IL2CPP_JUMP_TBL(0xC, IL_000c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_000c:
	{
		return;
	}
}
// System.Void System.Runtime.ConstrainedExecution.PrePrepareMethodAttribute::.ctor()
extern "C" void PrePrepareMethodAttribute__ctor_m1_7560 (PrePrepareMethodAttribute_t1_714 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.ConstrainedExecution.ReliabilityContractAttribute::.ctor(System.Runtime.ConstrainedExecution.Consistency,System.Runtime.ConstrainedExecution.Cer)
extern "C" void ReliabilityContractAttribute__ctor_m1_7561 (ReliabilityContractAttribute_t1_715 * __this, int32_t ___consistencyGuarantee, int32_t ___cer, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___consistencyGuarantee;
		__this->___consistency_0 = L_0;
		int32_t L_1 = ___cer;
		__this->___cer_1 = L_1;
		return;
	}
}
// System.Runtime.ConstrainedExecution.Cer System.Runtime.ConstrainedExecution.ReliabilityContractAttribute::get_Cer()
extern "C" int32_t ReliabilityContractAttribute_get_Cer_m1_7562 (ReliabilityContractAttribute_t1_715 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___cer_1);
		return L_0;
	}
}
// System.Runtime.ConstrainedExecution.Consistency System.Runtime.ConstrainedExecution.ReliabilityContractAttribute::get_ConsistencyGuarantee()
extern "C" int32_t ReliabilityContractAttribute_get_ConsistencyGuarantee_m1_7563 (ReliabilityContractAttribute_t1_715 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___consistency_0);
		return L_0;
	}
}
// System.Void System.Runtime.Hosting.ActivationArguments::.ctor(System.ActivationContext)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2097;
extern "C" void ActivationArguments__ctor_m1_7564 (ActivationArguments_t1_716 * __this, ActivationContext_t1_717 * ___activationData, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		_stringLiteral2097 = il2cpp_codegen_string_literal_from_index(2097);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		ActivationContext_t1_717 * L_0 = ___activationData;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, _stringLiteral2097, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0017:
	{
		ActivationContext_t1_717 * L_2 = ___activationData;
		__this->____context_0 = L_2;
		ActivationContext_t1_717 * L_3 = ___activationData;
		NullCheck(L_3);
		ApplicationIdentity_t1_718 * L_4 = ActivationContext_get_Identity_m1_13010(L_3, /*hidden argument*/NULL);
		__this->____identity_1 = L_4;
		return;
	}
}
// System.Void System.Runtime.Hosting.ActivationArguments::.ctor(System.ApplicationIdentity)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1184;
extern "C" void ActivationArguments__ctor_m1_7565 (ActivationArguments_t1_716 * __this, ApplicationIdentity_t1_718 * ___applicationIdentity, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		_stringLiteral1184 = il2cpp_codegen_string_literal_from_index(1184);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		ApplicationIdentity_t1_718 * L_0 = ___applicationIdentity;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, _stringLiteral1184, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0017:
	{
		ApplicationIdentity_t1_718 * L_2 = ___applicationIdentity;
		__this->____identity_1 = L_2;
		return;
	}
}
// System.Void System.Runtime.Hosting.ActivationArguments::.ctor(System.ActivationContext,System.String[])
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2098;
extern "C" void ActivationArguments__ctor_m1_7566 (ActivationArguments_t1_716 * __this, ActivationContext_t1_717 * ___activationContext, StringU5BU5D_t1_238* ___activationData, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		_stringLiteral2098 = il2cpp_codegen_string_literal_from_index(2098);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		ActivationContext_t1_717 * L_0 = ___activationContext;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, _stringLiteral2098, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0017:
	{
		ActivationContext_t1_717 * L_2 = ___activationContext;
		__this->____context_0 = L_2;
		ActivationContext_t1_717 * L_3 = ___activationContext;
		NullCheck(L_3);
		ApplicationIdentity_t1_718 * L_4 = ActivationContext_get_Identity_m1_13010(L_3, /*hidden argument*/NULL);
		__this->____identity_1 = L_4;
		StringU5BU5D_t1_238* L_5 = ___activationData;
		__this->____data_2 = L_5;
		return;
	}
}
// System.Void System.Runtime.Hosting.ActivationArguments::.ctor(System.ApplicationIdentity,System.String[])
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1184;
extern "C" void ActivationArguments__ctor_m1_7567 (ActivationArguments_t1_716 * __this, ApplicationIdentity_t1_718 * ___applicationIdentity, StringU5BU5D_t1_238* ___activationData, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		_stringLiteral1184 = il2cpp_codegen_string_literal_from_index(1184);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		ApplicationIdentity_t1_718 * L_0 = ___applicationIdentity;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, _stringLiteral1184, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0017:
	{
		ApplicationIdentity_t1_718 * L_2 = ___applicationIdentity;
		__this->____identity_1 = L_2;
		StringU5BU5D_t1_238* L_3 = ___activationData;
		__this->____data_2 = L_3;
		return;
	}
}
// System.ActivationContext System.Runtime.Hosting.ActivationArguments::get_ActivationContext()
extern "C" ActivationContext_t1_717 * ActivationArguments_get_ActivationContext_m1_7568 (ActivationArguments_t1_716 * __this, const MethodInfo* method)
{
	{
		ActivationContext_t1_717 * L_0 = (__this->____context_0);
		return L_0;
	}
}
// System.String[] System.Runtime.Hosting.ActivationArguments::get_ActivationData()
extern "C" StringU5BU5D_t1_238* ActivationArguments_get_ActivationData_m1_7569 (ActivationArguments_t1_716 * __this, const MethodInfo* method)
{
	{
		StringU5BU5D_t1_238* L_0 = (__this->____data_2);
		return L_0;
	}
}
// System.ApplicationIdentity System.Runtime.Hosting.ActivationArguments::get_ApplicationIdentity()
extern "C" ApplicationIdentity_t1_718 * ActivationArguments_get_ApplicationIdentity_m1_7570 (ActivationArguments_t1_716 * __this, const MethodInfo* method)
{
	{
		ApplicationIdentity_t1_718 * L_0 = (__this->____identity_1);
		return L_0;
	}
}
// System.Void System.Runtime.Hosting.ApplicationActivator::.ctor()
extern "C" void ApplicationActivator__ctor_m1_7571 (ApplicationActivator_t1_719 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Runtime.Remoting.ObjectHandle System.Runtime.Hosting.ApplicationActivator::CreateInstance(System.ActivationContext)
extern "C" ObjectHandle_t1_1019 * ApplicationActivator_CreateInstance_m1_7572 (ApplicationActivator_t1_719 * __this, ActivationContext_t1_717 * ___activationContext, const MethodInfo* method)
{
	{
		ActivationContext_t1_717 * L_0 = ___activationContext;
		ObjectHandle_t1_1019 * L_1 = (ObjectHandle_t1_1019 *)VirtFuncInvoker2< ObjectHandle_t1_1019 *, ActivationContext_t1_717 *, StringU5BU5D_t1_238* >::Invoke(5 /* System.Runtime.Remoting.ObjectHandle System.Runtime.Hosting.ApplicationActivator::CreateInstance(System.ActivationContext,System.String[]) */, __this, L_0, (StringU5BU5D_t1_238*)(StringU5BU5D_t1_238*)NULL);
		return L_1;
	}
}
// System.Runtime.Remoting.ObjectHandle System.Runtime.Hosting.ApplicationActivator::CreateInstance(System.ActivationContext,System.String[])
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* AppDomainSetup_t1_1497_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2098;
extern "C" ObjectHandle_t1_1019 * ApplicationActivator_CreateInstance_m1_7573 (ApplicationActivator_t1_719 * __this, ActivationContext_t1_717 * ___activationContext, StringU5BU5D_t1_238* ___activationCustomData, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		AppDomainSetup_t1_1497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(587);
		_stringLiteral2098 = il2cpp_codegen_string_literal_from_index(2098);
		s_Il2CppMethodIntialized = true;
	}
	AppDomainSetup_t1_1497 * V_0 = {0};
	{
		ActivationContext_t1_717 * L_0 = ___activationContext;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, _stringLiteral2098, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		ActivationContext_t1_717 * L_2 = ___activationContext;
		AppDomainSetup_t1_1497 * L_3 = (AppDomainSetup_t1_1497 *)il2cpp_codegen_object_new (AppDomainSetup_t1_1497_il2cpp_TypeInfo_var);
		AppDomainSetup__ctor_m1_13194(L_3, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		AppDomainSetup_t1_1497 * L_4 = V_0;
		ObjectHandle_t1_1019 * L_5 = ApplicationActivator_CreateInstanceHelper_m1_7574(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Runtime.Remoting.ObjectHandle System.Runtime.Hosting.ApplicationActivator::CreateInstanceHelper(System.AppDomainSetup)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern TypeInfo* HostSecurityManager_t1_1389_il2cpp_TypeInfo_var;
extern TypeInfo* Evidence_t1_398_il2cpp_TypeInfo_var;
extern TypeInfo* TrustManagerContext_t1_1365_il2cpp_TypeInfo_var;
extern TypeInfo* PolicyException_t1_1356_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2099;
extern Il2CppCodeGenString* _stringLiteral2100;
extern Il2CppCodeGenString* _stringLiteral2101;
extern Il2CppCodeGenString* _stringLiteral2102;
extern Il2CppCodeGenString* _stringLiteral2103;
extern Il2CppCodeGenString* _stringLiteral2104;
extern Il2CppCodeGenString* _stringLiteral1922;
extern Il2CppCodeGenString* _stringLiteral237;
extern "C" ObjectHandle_t1_1019 * ApplicationActivator_CreateInstanceHelper_m1_7574 (Object_t * __this /* static, unused */, AppDomainSetup_t1_1497 * ___adSetup, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		HostSecurityManager_t1_1389_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(588);
		Evidence_t1_398_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(363);
		TrustManagerContext_t1_1365_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(589);
		PolicyException_t1_1356_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(590);
		_stringLiteral2099 = il2cpp_codegen_string_literal_from_index(2099);
		_stringLiteral2100 = il2cpp_codegen_string_literal_from_index(2100);
		_stringLiteral2101 = il2cpp_codegen_string_literal_from_index(2101);
		_stringLiteral2102 = il2cpp_codegen_string_literal_from_index(2102);
		_stringLiteral2103 = il2cpp_codegen_string_literal_from_index(2103);
		_stringLiteral2104 = il2cpp_codegen_string_literal_from_index(2104);
		_stringLiteral1922 = il2cpp_codegen_string_literal_from_index(1922);
		_stringLiteral237 = il2cpp_codegen_string_literal_from_index(237);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	HostSecurityManager_t1_1389 * V_1 = {0};
	Evidence_t1_398 * V_2 = {0};
	TrustManagerContext_t1_1365 * V_3 = {0};
	ApplicationTrust_t1_1333 * V_4 = {0};
	String_t* V_5 = {0};
	AppDomain_t1_1403 * V_6 = {0};
	{
		AppDomainSetup_t1_1497 * L_0 = ___adSetup;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, _stringLiteral2099, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		AppDomainSetup_t1_1497 * L_2 = ___adSetup;
		NullCheck(L_2);
		ActivationArguments_t1_716 * L_3 = AppDomainSetup_get_ActivationArguments_m1_13224(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0042;
		}
	}
	{
		String_t* L_4 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2100, /*hidden argument*/NULL);
		V_0 = L_4;
		String_t* L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Format_m1_549(NULL /*static, unused*/, L_5, _stringLiteral2101, _stringLiteral2102, /*hidden argument*/NULL);
		ArgumentException_t1_1425 * L_7 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13262(L_7, L_6, _stringLiteral2099, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_7);
	}

IL_0042:
	{
		V_1 = (HostSecurityManager_t1_1389 *)NULL;
		AppDomain_t1_1403 * L_8 = AppDomain_get_CurrentDomain_m1_13075(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		AppDomainManager_t1_1493 * L_9 = AppDomain_get_DomainManager_m1_13168(L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0068;
		}
	}
	{
		AppDomain_t1_1403 * L_10 = AppDomain_get_CurrentDomain_m1_13075(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_10);
		AppDomainManager_t1_1493 * L_11 = AppDomain_get_DomainManager_m1_13168(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		HostSecurityManager_t1_1389 * L_12 = (HostSecurityManager_t1_1389 *)VirtFuncInvoker0< HostSecurityManager_t1_1389 * >::Invoke(9 /* System.Security.HostSecurityManager System.AppDomainManager::get_HostSecurityManager() */, L_11);
		V_1 = L_12;
		goto IL_006e;
	}

IL_0068:
	{
		HostSecurityManager_t1_1389 * L_13 = (HostSecurityManager_t1_1389 *)il2cpp_codegen_object_new (HostSecurityManager_t1_1389_il2cpp_TypeInfo_var);
		HostSecurityManager__ctor_m1_11934(L_13, /*hidden argument*/NULL);
		V_1 = L_13;
	}

IL_006e:
	{
		Evidence_t1_398 * L_14 = (Evidence_t1_398 *)il2cpp_codegen_object_new (Evidence_t1_398_il2cpp_TypeInfo_var);
		Evidence__ctor_m1_11454(L_14, /*hidden argument*/NULL);
		V_2 = L_14;
		Evidence_t1_398 * L_15 = V_2;
		AppDomainSetup_t1_1497 * L_16 = ___adSetup;
		NullCheck(L_16);
		ActivationArguments_t1_716 * L_17 = AppDomainSetup_get_ActivationArguments_m1_13224(L_16, /*hidden argument*/NULL);
		NullCheck(L_15);
		Evidence_AddHost_m1_11466(L_15, L_17, /*hidden argument*/NULL);
		TrustManagerContext_t1_1365 * L_18 = (TrustManagerContext_t1_1365 *)il2cpp_codegen_object_new (TrustManagerContext_t1_1365_il2cpp_TypeInfo_var);
		TrustManagerContext__ctor_m1_11713(L_18, /*hidden argument*/NULL);
		V_3 = L_18;
		HostSecurityManager_t1_1389 * L_19 = V_1;
		Evidence_t1_398 * L_20 = V_2;
		TrustManagerContext_t1_1365 * L_21 = V_3;
		NullCheck(L_19);
		ApplicationTrust_t1_1333 * L_22 = (ApplicationTrust_t1_1333 *)VirtFuncInvoker3< ApplicationTrust_t1_1333 *, Evidence_t1_398 *, Evidence_t1_398 *, TrustManagerContext_t1_1365 * >::Invoke(6 /* System.Security.Policy.ApplicationTrust System.Security.HostSecurityManager::DetermineApplicationTrust(System.Security.Policy.Evidence,System.Security.Policy.Evidence,System.Security.Policy.TrustManagerContext) */, L_19, L_20, (Evidence_t1_398 *)NULL, L_21);
		V_4 = L_22;
		ApplicationTrust_t1_1333 * L_23 = V_4;
		NullCheck(L_23);
		bool L_24 = ApplicationTrust_get_IsApplicationTrustedToRun_m1_11365(L_23, /*hidden argument*/NULL);
		if (L_24)
		{
			goto IL_00b1;
		}
	}
	{
		String_t* L_25 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2103, /*hidden argument*/NULL);
		V_5 = L_25;
		String_t* L_26 = V_5;
		PolicyException_t1_1356 * L_27 = (PolicyException_t1_1356 *)il2cpp_codegen_object_new (PolicyException_t1_1356_il2cpp_TypeInfo_var);
		PolicyException__ctor_m1_11583(L_27, L_26, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_27);
	}

IL_00b1:
	{
		AppDomainSetup_t1_1497 * L_28 = ___adSetup;
		AppDomain_t1_1403 * L_29 = AppDomain_CreateDomain_m1_13150(NULL /*static, unused*/, _stringLiteral2104, (Evidence_t1_398 *)NULL, L_28, /*hidden argument*/NULL);
		V_6 = L_29;
		AppDomain_t1_1403 * L_30 = V_6;
		NullCheck(L_30);
		ObjectHandle_t1_1019 * L_31 = AppDomain_CreateInstance_m1_13082(L_30, _stringLiteral1922, _stringLiteral237, (ObjectU5BU5D_t1_272*)(ObjectU5BU5D_t1_272*)NULL, /*hidden argument*/NULL);
		return L_31;
	}
}
// Conversion methods for marshalling of: System.Runtime.InteropServices.ComTypes.ELEMDESC/DESCUNION
extern "C" void DESCUNION_t1_726_marshal(const DESCUNION_t1_726& unmarshaled, DESCUNION_t1_726_marshaled& marshaled)
{
	IDLDESC_t1_727_marshal(unmarshaled.___idldesc_0, marshaled.___idldesc_0);
	PARAMDESC_t1_728_marshal(unmarshaled.___paramdesc_1, marshaled.___paramdesc_1);
}
extern "C" void DESCUNION_t1_726_marshal_back(const DESCUNION_t1_726_marshaled& marshaled, DESCUNION_t1_726& unmarshaled)
{
	IDLDESC_t1_727_marshal_back(marshaled.___idldesc_0, unmarshaled.___idldesc_0);
	PARAMDESC_t1_728_marshal_back(marshaled.___paramdesc_1, unmarshaled.___paramdesc_1);
}
// Conversion method for clean up from marshalling of: System.Runtime.InteropServices.ComTypes.ELEMDESC/DESCUNION
extern "C" void DESCUNION_t1_726_marshal_cleanup(DESCUNION_t1_726_marshaled& marshaled)
{
	IDLDESC_t1_727_marshal_cleanup(marshaled.___idldesc_0);
	PARAMDESC_t1_728_marshal_cleanup(marshaled.___paramdesc_1);
}
// Conversion methods for marshalling of: System.Runtime.InteropServices.ComTypes.ELEMDESC
extern "C" void ELEMDESC_t1_729_marshal(const ELEMDESC_t1_729& unmarshaled, ELEMDESC_t1_729_marshaled& marshaled)
{
	marshaled.___tdesc_0 = unmarshaled.___tdesc_0;
	DESCUNION_t1_726_marshal(unmarshaled.___desc_1, marshaled.___desc_1);
}
extern "C" void ELEMDESC_t1_729_marshal_back(const ELEMDESC_t1_729_marshaled& marshaled, ELEMDESC_t1_729& unmarshaled)
{
	unmarshaled.___tdesc_0 = marshaled.___tdesc_0;
	DESCUNION_t1_726_marshal_back(marshaled.___desc_1, unmarshaled.___desc_1);
}
// Conversion method for clean up from marshalling of: System.Runtime.InteropServices.ComTypes.ELEMDESC
extern "C" void ELEMDESC_t1_729_marshal_cleanup(ELEMDESC_t1_729_marshaled& marshaled)
{
	DESCUNION_t1_726_marshal_cleanup(marshaled.___desc_1);
}
// Conversion methods for marshalling of: System.Runtime.InteropServices.ComTypes.EXCEPINFO
extern "C" void EXCEPINFO_t1_731_marshal(const EXCEPINFO_t1_731& unmarshaled, EXCEPINFO_t1_731_marshaled& marshaled)
{
	marshaled.___wCode_0 = unmarshaled.___wCode_0;
	marshaled.___wReserved_1 = unmarshaled.___wReserved_1;
	marshaled.___bstrSource_2 = il2cpp_codegen_marshal_string(unmarshaled.___bstrSource_2);
	marshaled.___bstrDescription_3 = il2cpp_codegen_marshal_string(unmarshaled.___bstrDescription_3);
	marshaled.___bstrHelpFile_4 = il2cpp_codegen_marshal_string(unmarshaled.___bstrHelpFile_4);
	marshaled.___dwHelpContext_5 = unmarshaled.___dwHelpContext_5;
	marshaled.___pvReserved_6 = reinterpret_cast<intptr_t>((unmarshaled.___pvReserved_6).___m_value_0);
	marshaled.___pfnDeferredFillIn_7 = reinterpret_cast<intptr_t>((unmarshaled.___pfnDeferredFillIn_7).___m_value_0);
	marshaled.___scode_8 = unmarshaled.___scode_8;
}
extern "C" void EXCEPINFO_t1_731_marshal_back(const EXCEPINFO_t1_731_marshaled& marshaled, EXCEPINFO_t1_731& unmarshaled)
{
	unmarshaled.___wCode_0 = marshaled.___wCode_0;
	unmarshaled.___wReserved_1 = marshaled.___wReserved_1;
	unmarshaled.___bstrSource_2 = il2cpp_codegen_marshal_string_result(marshaled.___bstrSource_2);
	unmarshaled.___bstrDescription_3 = il2cpp_codegen_marshal_string_result(marshaled.___bstrDescription_3);
	unmarshaled.___bstrHelpFile_4 = il2cpp_codegen_marshal_string_result(marshaled.___bstrHelpFile_4);
	unmarshaled.___dwHelpContext_5 = marshaled.___dwHelpContext_5;
	(unmarshaled.___pvReserved_6).___m_value_0 = reinterpret_cast<void*>(marshaled.___pvReserved_6);
	(unmarshaled.___pfnDeferredFillIn_7).___m_value_0 = reinterpret_cast<void*>(marshaled.___pfnDeferredFillIn_7);
	unmarshaled.___scode_8 = marshaled.___scode_8;
}
// Conversion method for clean up from marshalling of: System.Runtime.InteropServices.ComTypes.EXCEPINFO
extern "C" void EXCEPINFO_t1_731_marshal_cleanup(EXCEPINFO_t1_731_marshaled& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___bstrSource_2);
	marshaled.___bstrSource_2 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___bstrDescription_3);
	marshaled.___bstrDescription_3 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___bstrHelpFile_4);
	marshaled.___bstrHelpFile_4 = NULL;
}
// Conversion methods for marshalling of: System.Runtime.InteropServices.ComTypes.FUNCDESC
extern "C" void FUNCDESC_t1_733_marshal(const FUNCDESC_t1_733& unmarshaled, FUNCDESC_t1_733_marshaled& marshaled)
{
	marshaled.___memid_0 = unmarshaled.___memid_0;
	marshaled.___lprgscode_1 = reinterpret_cast<intptr_t>((unmarshaled.___lprgscode_1).___m_value_0);
	marshaled.___lprgelemdescParam_2 = reinterpret_cast<intptr_t>((unmarshaled.___lprgelemdescParam_2).___m_value_0);
	marshaled.___funckind_3 = unmarshaled.___funckind_3;
	marshaled.___invkind_4 = unmarshaled.___invkind_4;
	marshaled.___callconv_5 = unmarshaled.___callconv_5;
	marshaled.___cParams_6 = unmarshaled.___cParams_6;
	marshaled.___cParamsOpt_7 = unmarshaled.___cParamsOpt_7;
	marshaled.___oVft_8 = unmarshaled.___oVft_8;
	marshaled.___cScodes_9 = unmarshaled.___cScodes_9;
	ELEMDESC_t1_729_marshal(unmarshaled.___elemdescFunc_10, marshaled.___elemdescFunc_10);
	marshaled.___wFuncFlags_11 = unmarshaled.___wFuncFlags_11;
}
extern "C" void FUNCDESC_t1_733_marshal_back(const FUNCDESC_t1_733_marshaled& marshaled, FUNCDESC_t1_733& unmarshaled)
{
	unmarshaled.___memid_0 = marshaled.___memid_0;
	(unmarshaled.___lprgscode_1).___m_value_0 = reinterpret_cast<void*>(marshaled.___lprgscode_1);
	(unmarshaled.___lprgelemdescParam_2).___m_value_0 = reinterpret_cast<void*>(marshaled.___lprgelemdescParam_2);
	unmarshaled.___funckind_3 = marshaled.___funckind_3;
	unmarshaled.___invkind_4 = marshaled.___invkind_4;
	unmarshaled.___callconv_5 = marshaled.___callconv_5;
	unmarshaled.___cParams_6 = marshaled.___cParams_6;
	unmarshaled.___cParamsOpt_7 = marshaled.___cParamsOpt_7;
	unmarshaled.___oVft_8 = marshaled.___oVft_8;
	unmarshaled.___cScodes_9 = marshaled.___cScodes_9;
	ELEMDESC_t1_729_marshal_back(marshaled.___elemdescFunc_10, unmarshaled.___elemdescFunc_10);
	unmarshaled.___wFuncFlags_11 = marshaled.___wFuncFlags_11;
}
// Conversion method for clean up from marshalling of: System.Runtime.InteropServices.ComTypes.FUNCDESC
extern "C" void FUNCDESC_t1_733_marshal_cleanup(FUNCDESC_t1_733_marshaled& marshaled)
{
	ELEMDESC_t1_729_marshal_cleanup(marshaled.___elemdescFunc_10);
}
// Conversion methods for marshalling of: System.Runtime.InteropServices.ComTypes.IDLDESC
extern "C" void IDLDESC_t1_727_marshal(const IDLDESC_t1_727& unmarshaled, IDLDESC_t1_727_marshaled& marshaled)
{
	marshaled.___dwReserved_0 = reinterpret_cast<intptr_t>((unmarshaled.___dwReserved_0).___m_value_0);
	marshaled.___wIDLFlags_1 = unmarshaled.___wIDLFlags_1;
}
extern "C" void IDLDESC_t1_727_marshal_back(const IDLDESC_t1_727_marshaled& marshaled, IDLDESC_t1_727& unmarshaled)
{
	(unmarshaled.___dwReserved_0).___m_value_0 = reinterpret_cast<void*>(marshaled.___dwReserved_0);
	unmarshaled.___wIDLFlags_1 = marshaled.___wIDLFlags_1;
}
// Conversion method for clean up from marshalling of: System.Runtime.InteropServices.ComTypes.IDLDESC
extern "C" void IDLDESC_t1_727_marshal_cleanup(IDLDESC_t1_727_marshaled& marshaled)
{
}
// Conversion methods for marshalling of: System.Runtime.InteropServices.ComTypes.PARAMDESC
extern "C" void PARAMDESC_t1_728_marshal(const PARAMDESC_t1_728& unmarshaled, PARAMDESC_t1_728_marshaled& marshaled)
{
	marshaled.___lpVarValue_0 = reinterpret_cast<intptr_t>((unmarshaled.___lpVarValue_0).___m_value_0);
	marshaled.___wParamFlags_1 = unmarshaled.___wParamFlags_1;
}
extern "C" void PARAMDESC_t1_728_marshal_back(const PARAMDESC_t1_728_marshaled& marshaled, PARAMDESC_t1_728& unmarshaled)
{
	(unmarshaled.___lpVarValue_0).___m_value_0 = reinterpret_cast<void*>(marshaled.___lpVarValue_0);
	unmarshaled.___wParamFlags_1 = marshaled.___wParamFlags_1;
}
// Conversion method for clean up from marshalling of: System.Runtime.InteropServices.ComTypes.PARAMDESC
extern "C" void PARAMDESC_t1_728_marshal_cleanup(PARAMDESC_t1_728_marshaled& marshaled)
{
}
// Conversion methods for marshalling of: System.Runtime.InteropServices.ComTypes.STATSTG
extern "C" void STATSTG_t1_741_marshal(const STATSTG_t1_741& unmarshaled, STATSTG_t1_741_marshaled& marshaled)
{
	marshaled.___pwcsName_0 = il2cpp_codegen_marshal_string(unmarshaled.___pwcsName_0);
	marshaled.___type_1 = unmarshaled.___type_1;
	marshaled.___cbSize_2 = unmarshaled.___cbSize_2;
	marshaled.___mtime_3 = unmarshaled.___mtime_3;
	marshaled.___ctime_4 = unmarshaled.___ctime_4;
	marshaled.___atime_5 = unmarshaled.___atime_5;
	marshaled.___grfMode_6 = unmarshaled.___grfMode_6;
	marshaled.___grfLocksSupported_7 = unmarshaled.___grfLocksSupported_7;
	marshaled.___clsid_8 = unmarshaled.___clsid_8;
	marshaled.___grfStateBits_9 = unmarshaled.___grfStateBits_9;
	marshaled.___reserved_10 = unmarshaled.___reserved_10;
}
extern "C" void STATSTG_t1_741_marshal_back(const STATSTG_t1_741_marshaled& marshaled, STATSTG_t1_741& unmarshaled)
{
	unmarshaled.___pwcsName_0 = il2cpp_codegen_marshal_string_result(marshaled.___pwcsName_0);
	unmarshaled.___type_1 = marshaled.___type_1;
	unmarshaled.___cbSize_2 = marshaled.___cbSize_2;
	unmarshaled.___mtime_3 = marshaled.___mtime_3;
	unmarshaled.___ctime_4 = marshaled.___ctime_4;
	unmarshaled.___atime_5 = marshaled.___atime_5;
	unmarshaled.___grfMode_6 = marshaled.___grfMode_6;
	unmarshaled.___grfLocksSupported_7 = marshaled.___grfLocksSupported_7;
	unmarshaled.___clsid_8 = marshaled.___clsid_8;
	unmarshaled.___grfStateBits_9 = marshaled.___grfStateBits_9;
	unmarshaled.___reserved_10 = marshaled.___reserved_10;
}
// Conversion method for clean up from marshalling of: System.Runtime.InteropServices.ComTypes.STATSTG
extern "C" void STATSTG_t1_741_marshal_cleanup(STATSTG_t1_741_marshaled& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___pwcsName_0);
	marshaled.___pwcsName_0 = NULL;
}
// Conversion methods for marshalling of: System.Runtime.InteropServices.ComTypes.TYPEATTR
extern "C" void TYPEATTR_t1_743_marshal(const TYPEATTR_t1_743& unmarshaled, TYPEATTR_t1_743_marshaled& marshaled)
{
	marshaled.___guid_1 = unmarshaled.___guid_1;
	marshaled.___lcid_2 = unmarshaled.___lcid_2;
	marshaled.___dwReserved_3 = unmarshaled.___dwReserved_3;
	marshaled.___memidConstructor_4 = unmarshaled.___memidConstructor_4;
	marshaled.___memidDestructor_5 = unmarshaled.___memidDestructor_5;
	marshaled.___lpstrSchema_6 = reinterpret_cast<intptr_t>((unmarshaled.___lpstrSchema_6).___m_value_0);
	marshaled.___cbSizeInstance_7 = unmarshaled.___cbSizeInstance_7;
	marshaled.___typekind_8 = unmarshaled.___typekind_8;
	marshaled.___cFuncs_9 = unmarshaled.___cFuncs_9;
	marshaled.___cVars_10 = unmarshaled.___cVars_10;
	marshaled.___cImplTypes_11 = unmarshaled.___cImplTypes_11;
	marshaled.___cbSizeVft_12 = unmarshaled.___cbSizeVft_12;
	marshaled.___cbAlignment_13 = unmarshaled.___cbAlignment_13;
	marshaled.___wTypeFlags_14 = unmarshaled.___wTypeFlags_14;
	marshaled.___wMajorVerNum_15 = unmarshaled.___wMajorVerNum_15;
	marshaled.___wMinorVerNum_16 = unmarshaled.___wMinorVerNum_16;
	marshaled.___tdescAlias_17 = unmarshaled.___tdescAlias_17;
	IDLDESC_t1_727_marshal(unmarshaled.___idldescType_18, marshaled.___idldescType_18);
}
extern "C" void TYPEATTR_t1_743_marshal_back(const TYPEATTR_t1_743_marshaled& marshaled, TYPEATTR_t1_743& unmarshaled)
{
	unmarshaled.___guid_1 = marshaled.___guid_1;
	unmarshaled.___lcid_2 = marshaled.___lcid_2;
	unmarshaled.___dwReserved_3 = marshaled.___dwReserved_3;
	unmarshaled.___memidConstructor_4 = marshaled.___memidConstructor_4;
	unmarshaled.___memidDestructor_5 = marshaled.___memidDestructor_5;
	(unmarshaled.___lpstrSchema_6).___m_value_0 = reinterpret_cast<void*>(marshaled.___lpstrSchema_6);
	unmarshaled.___cbSizeInstance_7 = marshaled.___cbSizeInstance_7;
	unmarshaled.___typekind_8 = marshaled.___typekind_8;
	unmarshaled.___cFuncs_9 = marshaled.___cFuncs_9;
	unmarshaled.___cVars_10 = marshaled.___cVars_10;
	unmarshaled.___cImplTypes_11 = marshaled.___cImplTypes_11;
	unmarshaled.___cbSizeVft_12 = marshaled.___cbSizeVft_12;
	unmarshaled.___cbAlignment_13 = marshaled.___cbAlignment_13;
	unmarshaled.___wTypeFlags_14 = marshaled.___wTypeFlags_14;
	unmarshaled.___wMajorVerNum_15 = marshaled.___wMajorVerNum_15;
	unmarshaled.___wMinorVerNum_16 = marshaled.___wMinorVerNum_16;
	unmarshaled.___tdescAlias_17 = marshaled.___tdescAlias_17;
	IDLDESC_t1_727_marshal_back(marshaled.___idldescType_18, unmarshaled.___idldescType_18);
}
// Conversion method for clean up from marshalling of: System.Runtime.InteropServices.ComTypes.TYPEATTR
extern "C" void TYPEATTR_t1_743_marshal_cleanup(TYPEATTR_t1_743_marshaled& marshaled)
{
	IDLDESC_t1_727_marshal_cleanup(marshaled.___idldescType_18);
}
// Conversion methods for marshalling of: System.Runtime.InteropServices.ComTypes.TYPELIBATTR
extern "C" void TYPELIBATTR_t1_746_marshal(const TYPELIBATTR_t1_746& unmarshaled, TYPELIBATTR_t1_746_marshaled& marshaled)
{
	marshaled.___guid_0 = unmarshaled.___guid_0;
	marshaled.___lcid_1 = unmarshaled.___lcid_1;
	marshaled.___syskind_2 = unmarshaled.___syskind_2;
	marshaled.___wMajorVerNum_3 = unmarshaled.___wMajorVerNum_3;
	marshaled.___wMinorVerNum_4 = unmarshaled.___wMinorVerNum_4;
	marshaled.___wLibFlags_5 = unmarshaled.___wLibFlags_5;
}
extern "C" void TYPELIBATTR_t1_746_marshal_back(const TYPELIBATTR_t1_746_marshaled& marshaled, TYPELIBATTR_t1_746& unmarshaled)
{
	unmarshaled.___guid_0 = marshaled.___guid_0;
	unmarshaled.___lcid_1 = marshaled.___lcid_1;
	unmarshaled.___syskind_2 = marshaled.___syskind_2;
	unmarshaled.___wMajorVerNum_3 = marshaled.___wMajorVerNum_3;
	unmarshaled.___wMinorVerNum_4 = marshaled.___wMinorVerNum_4;
	unmarshaled.___wLibFlags_5 = marshaled.___wLibFlags_5;
}
// Conversion method for clean up from marshalling of: System.Runtime.InteropServices.ComTypes.TYPELIBATTR
extern "C" void TYPELIBATTR_t1_746_marshal_cleanup(TYPELIBATTR_t1_746_marshaled& marshaled)
{
}
// Conversion methods for marshalling of: System.Runtime.InteropServices.ComTypes.VARDESC
extern "C" void VARDESC_t1_748_marshal(const VARDESC_t1_748& unmarshaled, VARDESC_t1_748_marshaled& marshaled)
{
	marshaled.___memid_0 = unmarshaled.___memid_0;
	marshaled.___lpstrSchema_1 = il2cpp_codegen_marshal_string(unmarshaled.___lpstrSchema_1);
	marshaled.___desc_2 = unmarshaled.___desc_2;
	ELEMDESC_t1_729_marshal(unmarshaled.___elemdescVar_3, marshaled.___elemdescVar_3);
	marshaled.___wVarFlags_4 = unmarshaled.___wVarFlags_4;
	marshaled.___varkind_5 = unmarshaled.___varkind_5;
}
extern "C" void VARDESC_t1_748_marshal_back(const VARDESC_t1_748_marshaled& marshaled, VARDESC_t1_748& unmarshaled)
{
	unmarshaled.___memid_0 = marshaled.___memid_0;
	unmarshaled.___lpstrSchema_1 = il2cpp_codegen_marshal_string_result(marshaled.___lpstrSchema_1);
	unmarshaled.___desc_2 = marshaled.___desc_2;
	ELEMDESC_t1_729_marshal_back(marshaled.___elemdescVar_3, unmarshaled.___elemdescVar_3);
	unmarshaled.___wVarFlags_4 = marshaled.___wVarFlags_4;
	unmarshaled.___varkind_5 = marshaled.___varkind_5;
}
// Conversion method for clean up from marshalling of: System.Runtime.InteropServices.ComTypes.VARDESC
extern "C" void VARDESC_t1_748_marshal_cleanup(VARDESC_t1_748_marshaled& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___lpstrSchema_1);
	marshaled.___lpstrSchema_1 = NULL;
	ELEMDESC_t1_729_marshal_cleanup(marshaled.___elemdescVar_3);
}
// System.Void System.Runtime.InteropServices.AllowReversePInvokeCallsAttribute::.ctor()
extern "C" void AllowReversePInvokeCallsAttribute__ctor_m1_7575 (AllowReversePInvokeCallsAttribute_t1_751 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.ArrayWithOffset::.ctor(System.Object,System.Int32)
extern "C" void ArrayWithOffset__ctor_m1_7576 (ArrayWithOffset_t1_752 * __this, Object_t * ___array, int32_t ___offset, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___array;
		__this->___array_0 = L_0;
		int32_t L_1 = ___offset;
		__this->___offset_1 = L_1;
		return;
	}
}
// System.Boolean System.Runtime.InteropServices.ArrayWithOffset::Equals(System.Object)
extern TypeInfo* ArrayWithOffset_t1_752_il2cpp_TypeInfo_var;
extern "C" bool ArrayWithOffset_Equals_m1_7577 (ArrayWithOffset_t1_752 * __this, Object_t * ___obj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArrayWithOffset_t1_752_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(591);
		s_Il2CppMethodIntialized = true;
	}
	ArrayWithOffset_t1_752  V_0 = {0};
	int32_t G_B7_0 = 0;
	{
		Object_t * L_0 = ___obj;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return 0;
	}

IL_0008:
	{
		Object_t * L_1 = ___obj;
		if (((Object_t *)IsInstSealed(L_1, ArrayWithOffset_t1_752_il2cpp_TypeInfo_var)))
		{
			goto IL_0015;
		}
	}
	{
		return 0;
	}

IL_0015:
	{
		Object_t * L_2 = ___obj;
		V_0 = ((*(ArrayWithOffset_t1_752 *)((ArrayWithOffset_t1_752 *)UnBox (L_2, ArrayWithOffset_t1_752_il2cpp_TypeInfo_var))));
		Object_t * L_3 = ((&V_0)->___array_0);
		Object_t * L_4 = (__this->___array_0);
		if ((!(((Object_t*)(Object_t *)L_3) == ((Object_t*)(Object_t *)L_4))))
		{
			goto IL_003f;
		}
	}
	{
		int32_t L_5 = ((&V_0)->___offset_1);
		int32_t L_6 = (__this->___offset_1);
		G_B7_0 = ((((int32_t)L_5) == ((int32_t)L_6))? 1 : 0);
		goto IL_0040;
	}

IL_003f:
	{
		G_B7_0 = 0;
	}

IL_0040:
	{
		return G_B7_0;
	}
}
// System.Boolean System.Runtime.InteropServices.ArrayWithOffset::Equals(System.Runtime.InteropServices.ArrayWithOffset)
extern "C" bool ArrayWithOffset_Equals_m1_7578 (ArrayWithOffset_t1_752 * __this, ArrayWithOffset_t1_752  ___obj, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Object_t * L_0 = ((&___obj)->___array_0);
		Object_t * L_1 = (__this->___array_0);
		if ((!(((Object_t*)(Object_t *)L_0) == ((Object_t*)(Object_t *)L_1))))
		{
			goto IL_0023;
		}
	}
	{
		int32_t L_2 = ((&___obj)->___offset_1);
		int32_t L_3 = (__this->___offset_1);
		G_B3_0 = ((((int32_t)L_2) == ((int32_t)L_3))? 1 : 0);
		goto IL_0024;
	}

IL_0023:
	{
		G_B3_0 = 0;
	}

IL_0024:
	{
		return G_B3_0;
	}
}
// System.Int32 System.Runtime.InteropServices.ArrayWithOffset::GetHashCode()
extern "C" int32_t ArrayWithOffset_GetHashCode_m1_7579 (ArrayWithOffset_t1_752 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___offset_1);
		return L_0;
	}
}
// System.Object System.Runtime.InteropServices.ArrayWithOffset::GetArray()
extern "C" Object_t * ArrayWithOffset_GetArray_m1_7580 (ArrayWithOffset_t1_752 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___array_0);
		return L_0;
	}
}
// System.Int32 System.Runtime.InteropServices.ArrayWithOffset::GetOffset()
extern "C" int32_t ArrayWithOffset_GetOffset_m1_7581 (ArrayWithOffset_t1_752 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___offset_1);
		return L_0;
	}
}
// System.Boolean System.Runtime.InteropServices.ArrayWithOffset::op_Equality(System.Runtime.InteropServices.ArrayWithOffset,System.Runtime.InteropServices.ArrayWithOffset)
extern "C" bool ArrayWithOffset_op_Equality_m1_7582 (Object_t * __this /* static, unused */, ArrayWithOffset_t1_752  ___a, ArrayWithOffset_t1_752  ___b, const MethodInfo* method)
{
	{
		ArrayWithOffset_t1_752  L_0 = ___b;
		bool L_1 = ArrayWithOffset_Equals_m1_7578((&___a), L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Runtime.InteropServices.ArrayWithOffset::op_Inequality(System.Runtime.InteropServices.ArrayWithOffset,System.Runtime.InteropServices.ArrayWithOffset)
extern "C" bool ArrayWithOffset_op_Inequality_m1_7583 (Object_t * __this /* static, unused */, ArrayWithOffset_t1_752  ___a, ArrayWithOffset_t1_752  ___b, const MethodInfo* method)
{
	{
		ArrayWithOffset_t1_752  L_0 = ___b;
		bool L_1 = ArrayWithOffset_Equals_m1_7578((&___a), L_0, /*hidden argument*/NULL);
		return ((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Runtime.InteropServices.AutomationProxyAttribute::.ctor(System.Boolean)
extern "C" void AutomationProxyAttribute__ctor_m1_7584 (AutomationProxyAttribute_t1_754 * __this, bool ___val, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		bool L_0 = ___val;
		__this->___val_0 = L_0;
		return;
	}
}
// System.Boolean System.Runtime.InteropServices.AutomationProxyAttribute::get_Value()
extern "C" bool AutomationProxyAttribute_get_Value_m1_7585 (AutomationProxyAttribute_t1_754 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___val_0);
		return L_0;
	}
}
// System.Void System.Runtime.InteropServices.BStrWrapper::.ctor(System.String)
extern "C" void BStrWrapper__ctor_m1_7586 (BStrWrapper_t1_757 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___value;
		__this->____value_0 = L_0;
		return;
	}
}
// System.String System.Runtime.InteropServices.BStrWrapper::get_WrappedObject()
extern "C" String_t* BStrWrapper_get_WrappedObject_m1_7587 (BStrWrapper_t1_757 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->____value_0);
		return L_0;
	}
}
// System.Void System.Runtime.InteropServices.BestFitMappingAttribute::.ctor(System.Boolean)
extern "C" void BestFitMappingAttribute__ctor_m1_7588 (BestFitMappingAttribute_t1_758 * __this, bool ___BestFitMapping, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		bool L_0 = ___BestFitMapping;
		__this->___bfm_0 = L_0;
		return;
	}
}
// System.Boolean System.Runtime.InteropServices.BestFitMappingAttribute::get_BestFitMapping()
extern "C" bool BestFitMappingAttribute_get_BestFitMapping_m1_7589 (BestFitMappingAttribute_t1_758 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___bfm_0);
		return L_0;
	}
}
// System.Void System.Runtime.InteropServices.COMException::.ctor()
extern "C" void COMException__ctor_m1_7590 (COMException_t1_760 * __this, const MethodInfo* method)
{
	{
		ExternalException__ctor_m1_7643(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.COMException::.ctor(System.String)
extern "C" void COMException__ctor_m1_7591 (COMException_t1_760 * __this, String_t* ___message, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message;
		ExternalException__ctor_m1_7644(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.COMException::.ctor(System.String,System.Exception)
extern "C" void COMException__ctor_m1_7592 (COMException_t1_760 * __this, String_t* ___message, Exception_t1_33 * ___inner, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message;
		Exception_t1_33 * L_1 = ___inner;
		ExternalException__ctor_m1_7646(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.COMException::.ctor(System.String,System.Int32)
extern "C" void COMException__ctor_m1_7593 (COMException_t1_760 * __this, String_t* ___message, int32_t ___errorCode, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message;
		int32_t L_1 = ___errorCode;
		ExternalException__ctor_m1_7647(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.COMException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void COMException__ctor_m1_7594 (COMException_t1_760 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method)
{
	{
		SerializationInfo_t1_293 * L_0 = ___info;
		StreamingContext_t1_1050  L_1 = ___context;
		ExternalException__ctor_m1_7645(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String System.Runtime.InteropServices.COMException::ToString()
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2105;
extern "C" String_t* COMException_ToString_m1_7595 (COMException_t1_760 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral2105 = il2cpp_codegen_string_literal_from_index(2105);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B2_0 = 0;
	ObjectU5BU5D_t1_272* G_B2_1 = {0};
	ObjectU5BU5D_t1_272* G_B2_2 = {0};
	String_t* G_B2_3 = {0};
	int32_t G_B1_0 = 0;
	ObjectU5BU5D_t1_272* G_B1_1 = {0};
	ObjectU5BU5D_t1_272* G_B1_2 = {0};
	String_t* G_B1_3 = {0};
	String_t* G_B3_0 = {0};
	int32_t G_B3_1 = 0;
	ObjectU5BU5D_t1_272* G_B3_2 = {0};
	ObjectU5BU5D_t1_272* G_B3_3 = {0};
	String_t* G_B3_4 = {0};
	int32_t G_B5_0 = 0;
	ObjectU5BU5D_t1_272* G_B5_1 = {0};
	ObjectU5BU5D_t1_272* G_B5_2 = {0};
	String_t* G_B5_3 = {0};
	int32_t G_B4_0 = 0;
	ObjectU5BU5D_t1_272* G_B4_1 = {0};
	ObjectU5BU5D_t1_272* G_B4_2 = {0};
	String_t* G_B4_3 = {0};
	String_t* G_B6_0 = {0};
	int32_t G_B6_1 = 0;
	ObjectU5BU5D_t1_272* G_B6_2 = {0};
	ObjectU5BU5D_t1_272* G_B6_3 = {0};
	String_t* G_B6_4 = {0};
	{
		ObjectU5BU5D_t1_272* L_0 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 6));
		Type_t * L_1 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Exception::GetType() */, __this);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_1;
		ObjectU5BU5D_t1_272* L_2 = L_0;
		int32_t L_3 = Exception_get_HResult_m1_1244(__this, /*hidden argument*/NULL);
		int32_t L_4 = L_3;
		Object_t * L_5 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_5);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1, sizeof(Object_t *))) = (Object_t *)L_5;
		ObjectU5BU5D_t1_272* L_6 = L_2;
		String_t* L_7 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(21 /* System.String System.Exception::get_Message() */, __this);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 2, sizeof(Object_t *))) = (Object_t *)L_7;
		ObjectU5BU5D_t1_272* L_8 = L_6;
		Exception_t1_33 * L_9 = (Exception_t1_33 *)VirtFuncInvoker0< Exception_t1_33 * >::Invoke(7 /* System.Exception System.Exception::get_InnerException() */, __this);
		G_B1_0 = 3;
		G_B1_1 = L_8;
		G_B1_2 = L_8;
		G_B1_3 = _stringLiteral2105;
		if (L_9)
		{
			G_B2_0 = 3;
			G_B2_1 = L_8;
			G_B2_2 = L_8;
			G_B2_3 = _stringLiteral2105;
			goto IL_0042;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		G_B3_0 = L_10;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		G_B3_4 = G_B1_3;
		goto IL_004d;
	}

IL_0042:
	{
		Exception_t1_33 * L_11 = (Exception_t1_33 *)VirtFuncInvoker0< Exception_t1_33 * >::Invoke(7 /* System.Exception System.Exception::get_InnerException() */, __this);
		NullCheck(L_11);
		String_t* L_12 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Exception::ToString() */, L_11);
		G_B3_0 = L_12;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
		G_B3_4 = G_B2_3;
	}

IL_004d:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		*((Object_t **)(Object_t **)SZArrayLdElema(G_B3_2, G_B3_1, sizeof(Object_t *))) = (Object_t *)G_B3_0;
		ObjectU5BU5D_t1_272* L_13 = G_B3_3;
		String_t* L_14 = Environment_get_NewLine_m1_14052(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, L_14);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_13, 4, sizeof(Object_t *))) = (Object_t *)L_14;
		ObjectU5BU5D_t1_272* L_15 = L_13;
		String_t* L_16 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(24 /* System.String System.Exception::get_StackTrace() */, __this);
		G_B4_0 = 5;
		G_B4_1 = L_15;
		G_B4_2 = L_15;
		G_B4_3 = G_B3_4;
		if (!L_16)
		{
			G_B5_0 = 5;
			G_B5_1 = L_15;
			G_B5_2 = L_15;
			G_B5_3 = G_B3_4;
			goto IL_006e;
		}
	}
	{
		String_t* L_17 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(24 /* System.String System.Exception::get_StackTrace() */, __this);
		G_B6_0 = L_17;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		G_B6_4 = G_B4_3;
		goto IL_0073;
	}

IL_006e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		G_B6_0 = L_18;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
		G_B6_4 = G_B5_3;
	}

IL_0073:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		*((Object_t **)(Object_t **)SZArrayLdElema(G_B6_2, G_B6_1, sizeof(Object_t *))) = (Object_t *)G_B6_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Format_m1_551(NULL /*static, unused*/, G_B6_4, G_B6_3, /*hidden argument*/NULL);
		return L_19;
	}
}
// System.Void System.Runtime.InteropServices.ClassInterfaceAttribute::.ctor(System.Int16)
extern "C" void ClassInterfaceAttribute__ctor_m1_7596 (ClassInterfaceAttribute_t1_765 * __this, int16_t ___classInterfaceType, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		int16_t L_0 = ___classInterfaceType;
		__this->___ciType_0 = L_0;
		return;
	}
}
// System.Void System.Runtime.InteropServices.ClassInterfaceAttribute::.ctor(System.Runtime.InteropServices.ClassInterfaceType)
extern "C" void ClassInterfaceAttribute__ctor_m1_7597 (ClassInterfaceAttribute_t1_765 * __this, int32_t ___classInterfaceType, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___classInterfaceType;
		__this->___ciType_0 = L_0;
		return;
	}
}
// System.Runtime.InteropServices.ClassInterfaceType System.Runtime.InteropServices.ClassInterfaceAttribute::get_Value()
extern "C" int32_t ClassInterfaceAttribute_get_Value_m1_7598 (ClassInterfaceAttribute_t1_765 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___ciType_0);
		return L_0;
	}
}
// System.Void System.Runtime.InteropServices.ComAliasNameAttribute::.ctor(System.String)
extern "C" void ComAliasNameAttribute__ctor_m1_7599 (ComAliasNameAttribute_t1_767 * __this, String_t* ___alias, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___alias;
		__this->___val_0 = L_0;
		return;
	}
}
// System.String System.Runtime.InteropServices.ComAliasNameAttribute::get_Value()
extern "C" String_t* ComAliasNameAttribute_get_Value_m1_7600 (ComAliasNameAttribute_t1_767 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___val_0);
		return L_0;
	}
}
// System.Void System.Runtime.InteropServices.ComCompatibleVersionAttribute::.ctor(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" void ComCompatibleVersionAttribute__ctor_m1_7601 (ComCompatibleVersionAttribute_t1_768 * __this, int32_t ___major, int32_t ___minor, int32_t ___build, int32_t ___revision, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___major;
		__this->___major_0 = L_0;
		int32_t L_1 = ___minor;
		__this->___minor_1 = L_1;
		int32_t L_2 = ___build;
		__this->___build_2 = L_2;
		int32_t L_3 = ___revision;
		__this->___revision_3 = L_3;
		return;
	}
}
// System.Int32 System.Runtime.InteropServices.ComCompatibleVersionAttribute::get_MajorVersion()
extern "C" int32_t ComCompatibleVersionAttribute_get_MajorVersion_m1_7602 (ComCompatibleVersionAttribute_t1_768 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___major_0);
		return L_0;
	}
}
// System.Int32 System.Runtime.InteropServices.ComCompatibleVersionAttribute::get_MinorVersion()
extern "C" int32_t ComCompatibleVersionAttribute_get_MinorVersion_m1_7603 (ComCompatibleVersionAttribute_t1_768 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___minor_1);
		return L_0;
	}
}
// System.Int32 System.Runtime.InteropServices.ComCompatibleVersionAttribute::get_BuildNumber()
extern "C" int32_t ComCompatibleVersionAttribute_get_BuildNumber_m1_7604 (ComCompatibleVersionAttribute_t1_768 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___build_2);
		return L_0;
	}
}
// System.Int32 System.Runtime.InteropServices.ComCompatibleVersionAttribute::get_RevisionNumber()
extern "C" int32_t ComCompatibleVersionAttribute_get_RevisionNumber_m1_7605 (ComCompatibleVersionAttribute_t1_768 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___revision_3);
		return L_0;
	}
}
// System.Void System.Runtime.InteropServices.ComConversionLossAttribute::.ctor()
extern "C" void ComConversionLossAttribute__ctor_m1_7606 (ComConversionLossAttribute_t1_769 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.ComDefaultInterfaceAttribute::.ctor(System.Type)
extern "C" void ComDefaultInterfaceAttribute__ctor_m1_7607 (ComDefaultInterfaceAttribute_t1_770 * __this, Type_t * ___defaultInterface, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		Type_t * L_0 = ___defaultInterface;
		__this->____type_0 = L_0;
		return;
	}
}
// System.Type System.Runtime.InteropServices.ComDefaultInterfaceAttribute::get_Value()
extern "C" Type_t * ComDefaultInterfaceAttribute_get_Value_m1_7608 (ComDefaultInterfaceAttribute_t1_770 * __this, const MethodInfo* method)
{
	{
		Type_t * L_0 = (__this->____type_0);
		return L_0;
	}
}
// System.Void System.Runtime.InteropServices.ComEventInterfaceAttribute::.ctor(System.Type,System.Type)
extern "C" void ComEventInterfaceAttribute__ctor_m1_7609 (ComEventInterfaceAttribute_t1_771 * __this, Type_t * ___SourceInterface, Type_t * ___EventProvider, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		Type_t * L_0 = ___SourceInterface;
		__this->___si_0 = L_0;
		Type_t * L_1 = ___EventProvider;
		__this->___ep_1 = L_1;
		return;
	}
}
// System.Type System.Runtime.InteropServices.ComEventInterfaceAttribute::get_EventProvider()
extern "C" Type_t * ComEventInterfaceAttribute_get_EventProvider_m1_7610 (ComEventInterfaceAttribute_t1_771 * __this, const MethodInfo* method)
{
	{
		Type_t * L_0 = (__this->___ep_1);
		return L_0;
	}
}
// System.Type System.Runtime.InteropServices.ComEventInterfaceAttribute::get_SourceInterface()
extern "C" Type_t * ComEventInterfaceAttribute_get_SourceInterface_m1_7611 (ComEventInterfaceAttribute_t1_771 * __this, const MethodInfo* method)
{
	{
		Type_t * L_0 = (__this->___si_0);
		return L_0;
	}
}
// System.Void System.Runtime.InteropServices.ComRegisterFunctionAttribute::.ctor()
extern "C" void ComRegisterFunctionAttribute__ctor_m1_7612 (ComRegisterFunctionAttribute_t1_774 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.ComSourceInterfacesAttribute::.ctor(System.String)
extern "C" void ComSourceInterfacesAttribute__ctor_m1_7613 (ComSourceInterfacesAttribute_t1_775 * __this, String_t* ___sourceInterfaces, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___sourceInterfaces;
		__this->___internalValue_0 = L_0;
		return;
	}
}
// System.Void System.Runtime.InteropServices.ComSourceInterfacesAttribute::.ctor(System.Type)
extern "C" void ComSourceInterfacesAttribute__ctor_m1_7614 (ComSourceInterfacesAttribute_t1_775 * __this, Type_t * ___sourceInterface, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		Type_t * L_0 = ___sourceInterface;
		NullCheck(L_0);
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Type::ToString() */, L_0);
		__this->___internalValue_0 = L_1;
		return;
	}
}
// System.Void System.Runtime.InteropServices.ComSourceInterfacesAttribute::.ctor(System.Type,System.Type)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void ComSourceInterfacesAttribute__ctor_m1_7615 (ComSourceInterfacesAttribute_t1_775 * __this, Type_t * ___sourceInterface1, Type_t * ___sourceInterface2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		Type_t * L_0 = ___sourceInterface1;
		NullCheck(L_0);
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Type::ToString() */, L_0);
		Type_t * L_2 = ___sourceInterface2;
		NullCheck(L_2);
		String_t* L_3 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Type::ToString() */, L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m1_559(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		__this->___internalValue_0 = L_4;
		return;
	}
}
// System.Void System.Runtime.InteropServices.ComSourceInterfacesAttribute::.ctor(System.Type,System.Type,System.Type)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void ComSourceInterfacesAttribute__ctor_m1_7616 (ComSourceInterfacesAttribute_t1_775 * __this, Type_t * ___sourceInterface1, Type_t * ___sourceInterface2, Type_t * ___sourceInterface3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		Type_t * L_0 = ___sourceInterface1;
		NullCheck(L_0);
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Type::ToString() */, L_0);
		Type_t * L_2 = ___sourceInterface2;
		NullCheck(L_2);
		String_t* L_3 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Type::ToString() */, L_2);
		Type_t * L_4 = ___sourceInterface3;
		NullCheck(L_4);
		String_t* L_5 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Type::ToString() */, L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m1_560(NULL /*static, unused*/, L_1, L_3, L_5, /*hidden argument*/NULL);
		__this->___internalValue_0 = L_6;
		return;
	}
}
// System.Void System.Runtime.InteropServices.ComSourceInterfacesAttribute::.ctor(System.Type,System.Type,System.Type,System.Type)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void ComSourceInterfacesAttribute__ctor_m1_7617 (ComSourceInterfacesAttribute_t1_775 * __this, Type_t * ___sourceInterface1, Type_t * ___sourceInterface2, Type_t * ___sourceInterface3, Type_t * ___sourceInterface4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		Type_t * L_0 = ___sourceInterface1;
		NullCheck(L_0);
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Type::ToString() */, L_0);
		Type_t * L_2 = ___sourceInterface2;
		NullCheck(L_2);
		String_t* L_3 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Type::ToString() */, L_2);
		Type_t * L_4 = ___sourceInterface3;
		NullCheck(L_4);
		String_t* L_5 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Type::ToString() */, L_4);
		Type_t * L_6 = ___sourceInterface4;
		NullCheck(L_6);
		String_t* L_7 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Type::ToString() */, L_6);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m1_561(NULL /*static, unused*/, L_1, L_3, L_5, L_7, /*hidden argument*/NULL);
		__this->___internalValue_0 = L_8;
		return;
	}
}
// System.String System.Runtime.InteropServices.ComSourceInterfacesAttribute::get_Value()
extern "C" String_t* ComSourceInterfacesAttribute_get_Value_m1_7618 (ComSourceInterfacesAttribute_t1_775 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___internalValue_0);
		return L_0;
	}
}
// System.Void System.Runtime.InteropServices.ComUnregisterFunctionAttribute::.ctor()
extern "C" void ComUnregisterFunctionAttribute__ctor_m1_7619 (ComUnregisterFunctionAttribute_t1_776 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.CriticalHandle::.ctor(System.IntPtr)
extern "C" void CriticalHandle__ctor_m1_7620 (CriticalHandle_t1_83 * __this, IntPtr_t ___invalidHandleValue, const MethodInfo* method)
{
	{
		CriticalFinalizerObject__ctor_m1_7558(__this, /*hidden argument*/NULL);
		IntPtr_t L_0 = ___invalidHandleValue;
		__this->___handle_0 = L_0;
		return;
	}
}
// System.Void System.Runtime.InteropServices.CriticalHandle::Finalize()
extern "C" void CriticalHandle_Finalize_m1_7621 (CriticalHandle_t1_83 * __this, const MethodInfo* method)
{
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		VirtActionInvoker1< bool >::Invoke(5 /* System.Void System.Runtime.InteropServices.CriticalHandle::Dispose(System.Boolean) */, __this, 0);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		CriticalFinalizerObject_Finalize_m1_7559(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0013:
	{
		return;
	}
}
// System.Void System.Runtime.InteropServices.CriticalHandle::Close()
extern "C" void CriticalHandle_Close_m1_7622 (CriticalHandle_t1_83 * __this, const MethodInfo* method)
{
	{
		VirtActionInvoker1< bool >::Invoke(5 /* System.Void System.Runtime.InteropServices.CriticalHandle::Dispose(System.Boolean) */, __this, 1);
		return;
	}
}
// System.Void System.Runtime.InteropServices.CriticalHandle::Dispose()
extern "C" void CriticalHandle_Dispose_m1_7623 (CriticalHandle_t1_83 * __this, const MethodInfo* method)
{
	{
		VirtActionInvoker1< bool >::Invoke(5 /* System.Void System.Runtime.InteropServices.CriticalHandle::Dispose(System.Boolean) */, __this, 1);
		return;
	}
}
// System.Void System.Runtime.InteropServices.CriticalHandle::Dispose(System.Boolean)
extern "C" void CriticalHandle_Dispose_m1_7624 (CriticalHandle_t1_83 * __this, bool ___disposing, const MethodInfo* method)
{
	{
		bool L_0 = (__this->____disposed_1);
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		__this->____disposed_1 = 1;
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Runtime.InteropServices.CriticalHandle::get_IsInvalid() */, __this);
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		bool L_2 = ___disposing;
		if (!L_2)
		{
			goto IL_0046;
		}
	}
	{
		bool L_3 = (bool)VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Runtime.InteropServices.CriticalHandle::get_IsInvalid() */, __this);
		if (L_3)
		{
			goto IL_0046;
		}
	}
	{
		bool L_4 = (bool)VirtFuncInvoker0< bool >::Invoke(6 /* System.Boolean System.Runtime.InteropServices.CriticalHandle::ReleaseHandle() */, __this);
		if (L_4)
		{
			goto IL_0046;
		}
	}
	{
		GC_SuppressFinalize_m1_14113(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		goto IL_0046;
	}

IL_0046:
	{
		return;
	}
}
// System.Void System.Runtime.InteropServices.CriticalHandle::SetHandle(System.IntPtr)
extern "C" void CriticalHandle_SetHandle_m1_7625 (CriticalHandle_t1_83 * __this, IntPtr_t ___handle, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = ___handle;
		__this->___handle_0 = L_0;
		return;
	}
}
// System.Void System.Runtime.InteropServices.CriticalHandle::SetHandleAsInvalid()
extern "C" void CriticalHandle_SetHandleAsInvalid_m1_7626 (CriticalHandle_t1_83 * __this, const MethodInfo* method)
{
	{
		__this->____disposed_1 = 1;
		return;
	}
}
// System.Boolean System.Runtime.InteropServices.CriticalHandle::get_IsClosed()
extern "C" bool CriticalHandle_get_IsClosed_m1_7627 (CriticalHandle_t1_83 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->____disposed_1);
		return L_0;
	}
}
// System.Void System.Runtime.InteropServices.CurrencyWrapper::.ctor(System.Decimal)
extern "C" void CurrencyWrapper__ctor_m1_7628 (CurrencyWrapper_t1_777 * __this, Decimal_t1_19  ___obj, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		Decimal_t1_19  L_0 = ___obj;
		__this->___currency_0 = L_0;
		return;
	}
}
// System.Void System.Runtime.InteropServices.CurrencyWrapper::.ctor(System.Object)
extern const Il2CppType* Decimal_t1_19_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern TypeInfo* Decimal_t1_19_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2106;
extern "C" void CurrencyWrapper__ctor_m1_7629 (CurrencyWrapper_t1_777 * __this, Object_t * ___obj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Decimal_t1_19_0_0_0_var = il2cpp_codegen_type_from_index(50);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		Decimal_t1_19_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(50);
		_stringLiteral2106 = il2cpp_codegen_string_literal_from_index(2106);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		Object_t * L_0 = ___obj;
		NullCheck(L_0);
		Type_t * L_1 = Object_GetType_m1_5(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(Decimal_t1_19_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_1) == ((Object_t*)(Type_t *)L_2)))
		{
			goto IL_0026;
		}
	}
	{
		ArgumentException_t1_1425 * L_3 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13260(L_3, _stringLiteral2106, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0026:
	{
		Object_t * L_4 = ___obj;
		__this->___currency_0 = ((*(Decimal_t1_19 *)((Decimal_t1_19 *)UnBox (L_4, Decimal_t1_19_il2cpp_TypeInfo_var))));
		return;
	}
}
// System.Decimal System.Runtime.InteropServices.CurrencyWrapper::get_WrappedObject()
extern "C" Decimal_t1_19  CurrencyWrapper_get_WrappedObject_m1_7630 (CurrencyWrapper_t1_777 * __this, const MethodInfo* method)
{
	{
		Decimal_t1_19  L_0 = (__this->___currency_0);
		return L_0;
	}
}
// System.Void System.Runtime.InteropServices.DispIdAttribute::.ctor(System.Int32)
extern "C" void DispIdAttribute__ctor_m1_7631 (DispIdAttribute_t1_780 * __this, int32_t ___dispId, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___dispId;
		__this->___id_0 = L_0;
		return;
	}
}
// System.Int32 System.Runtime.InteropServices.DispIdAttribute::get_Value()
extern "C" int32_t DispIdAttribute_get_Value_m1_7632 (DispIdAttribute_t1_780 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___id_0);
		return L_0;
	}
}
// System.Void System.Runtime.InteropServices.DispatchWrapper::.ctor(System.Object)
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern "C" void DispatchWrapper__ctor_m1_7633 (DispatchWrapper_t1_781 * __this, Object_t * ___obj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		Object_t * L_0 = ___obj;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		Marshal_GetIDispatchForObject_m1_7752(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Object_t * L_1 = ___obj;
		__this->___wrappedObject_0 = L_1;
		return;
	}
}
// System.Object System.Runtime.InteropServices.DispatchWrapper::get_WrappedObject()
extern "C" Object_t * DispatchWrapper_get_WrappedObject_m1_7634 (DispatchWrapper_t1_781 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___wrappedObject_0);
		return L_0;
	}
}
// Conversion methods for marshalling of: System.Runtime.InteropServices.ELEMDESC/DESCUNION
extern "C" void DESCUNION_t1_782_marshal(const DESCUNION_t1_782& unmarshaled, DESCUNION_t1_782_marshaled& marshaled)
{
	IDLDESC_t1_783_marshal(unmarshaled.___idldesc_0, marshaled.___idldesc_0);
	PARAMDESC_t1_784_marshal(unmarshaled.___paramdesc_1, marshaled.___paramdesc_1);
}
extern "C" void DESCUNION_t1_782_marshal_back(const DESCUNION_t1_782_marshaled& marshaled, DESCUNION_t1_782& unmarshaled)
{
	IDLDESC_t1_783_marshal_back(marshaled.___idldesc_0, unmarshaled.___idldesc_0);
	PARAMDESC_t1_784_marshal_back(marshaled.___paramdesc_1, unmarshaled.___paramdesc_1);
}
// Conversion method for clean up from marshalling of: System.Runtime.InteropServices.ELEMDESC/DESCUNION
extern "C" void DESCUNION_t1_782_marshal_cleanup(DESCUNION_t1_782_marshaled& marshaled)
{
	IDLDESC_t1_783_marshal_cleanup(marshaled.___idldesc_0);
	PARAMDESC_t1_784_marshal_cleanup(marshaled.___paramdesc_1);
}
// Conversion methods for marshalling of: System.Runtime.InteropServices.ELEMDESC
extern "C" void ELEMDESC_t1_785_marshal(const ELEMDESC_t1_785& unmarshaled, ELEMDESC_t1_785_marshaled& marshaled)
{
	marshaled.___tdesc_0 = unmarshaled.___tdesc_0;
	DESCUNION_t1_782_marshal(unmarshaled.___desc_1, marshaled.___desc_1);
}
extern "C" void ELEMDESC_t1_785_marshal_back(const ELEMDESC_t1_785_marshaled& marshaled, ELEMDESC_t1_785& unmarshaled)
{
	unmarshaled.___tdesc_0 = marshaled.___tdesc_0;
	DESCUNION_t1_782_marshal_back(marshaled.___desc_1, unmarshaled.___desc_1);
}
// Conversion method for clean up from marshalling of: System.Runtime.InteropServices.ELEMDESC
extern "C" void ELEMDESC_t1_785_marshal_cleanup(ELEMDESC_t1_785_marshaled& marshaled)
{
	DESCUNION_t1_782_marshal_cleanup(marshaled.___desc_1);
}
// Conversion methods for marshalling of: System.Runtime.InteropServices.EXCEPINFO
extern "C" void EXCEPINFO_t1_787_marshal(const EXCEPINFO_t1_787& unmarshaled, EXCEPINFO_t1_787_marshaled& marshaled)
{
	marshaled.___wCode_0 = unmarshaled.___wCode_0;
	marshaled.___wReserved_1 = unmarshaled.___wReserved_1;
	marshaled.___bstrSource_2 = il2cpp_codegen_marshal_string(unmarshaled.___bstrSource_2);
	marshaled.___bstrDescription_3 = il2cpp_codegen_marshal_string(unmarshaled.___bstrDescription_3);
	marshaled.___bstrHelpFile_4 = il2cpp_codegen_marshal_string(unmarshaled.___bstrHelpFile_4);
	marshaled.___dwHelpContext_5 = unmarshaled.___dwHelpContext_5;
	marshaled.___pvReserved_6 = reinterpret_cast<intptr_t>((unmarshaled.___pvReserved_6).___m_value_0);
	marshaled.___pfnDeferredFillIn_7 = reinterpret_cast<intptr_t>((unmarshaled.___pfnDeferredFillIn_7).___m_value_0);
}
extern "C" void EXCEPINFO_t1_787_marshal_back(const EXCEPINFO_t1_787_marshaled& marshaled, EXCEPINFO_t1_787& unmarshaled)
{
	unmarshaled.___wCode_0 = marshaled.___wCode_0;
	unmarshaled.___wReserved_1 = marshaled.___wReserved_1;
	unmarshaled.___bstrSource_2 = il2cpp_codegen_marshal_string_result(marshaled.___bstrSource_2);
	unmarshaled.___bstrDescription_3 = il2cpp_codegen_marshal_string_result(marshaled.___bstrDescription_3);
	unmarshaled.___bstrHelpFile_4 = il2cpp_codegen_marshal_string_result(marshaled.___bstrHelpFile_4);
	unmarshaled.___dwHelpContext_5 = marshaled.___dwHelpContext_5;
	(unmarshaled.___pvReserved_6).___m_value_0 = reinterpret_cast<void*>(marshaled.___pvReserved_6);
	(unmarshaled.___pfnDeferredFillIn_7).___m_value_0 = reinterpret_cast<void*>(marshaled.___pfnDeferredFillIn_7);
}
// Conversion method for clean up from marshalling of: System.Runtime.InteropServices.EXCEPINFO
extern "C" void EXCEPINFO_t1_787_marshal_cleanup(EXCEPINFO_t1_787_marshaled& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___bstrSource_2);
	marshaled.___bstrSource_2 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___bstrDescription_3);
	marshaled.___bstrDescription_3 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___bstrHelpFile_4);
	marshaled.___bstrHelpFile_4 = NULL;
}
// System.Void System.Runtime.InteropServices.ErrorWrapper::.ctor(System.Exception)
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern "C" void ErrorWrapper__ctor_m1_7635 (ErrorWrapper_t1_788 * __this, Exception_t1_33 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		Exception_t1_33 * L_0 = ___e;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		int32_t L_1 = Marshal_GetHRForException_m1_7749(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->___errorCode_0 = L_1;
		return;
	}
}
// System.Void System.Runtime.InteropServices.ErrorWrapper::.ctor(System.Int32)
extern "C" void ErrorWrapper__ctor_m1_7636 (ErrorWrapper_t1_788 * __this, int32_t ___errorCode, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___errorCode;
		__this->___errorCode_0 = L_0;
		return;
	}
}
// System.Void System.Runtime.InteropServices.ErrorWrapper::.ctor(System.Object)
extern const Il2CppType* Int32_t1_3_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2107;
extern "C" void ErrorWrapper__ctor_m1_7637 (ErrorWrapper_t1_788 * __this, Object_t * ___errorCode, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t1_3_0_0_0_var = il2cpp_codegen_type_from_index(11);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral2107 = il2cpp_codegen_string_literal_from_index(2107);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		Object_t * L_0 = ___errorCode;
		NullCheck(L_0);
		Type_t * L_1 = Object_GetType_m1_5(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(Int32_t1_3_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_1) == ((Object_t*)(Type_t *)L_2)))
		{
			goto IL_0026;
		}
	}
	{
		ArgumentException_t1_1425 * L_3 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13260(L_3, _stringLiteral2107, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0026:
	{
		Object_t * L_4 = ___errorCode;
		__this->___errorCode_0 = ((*(int32_t*)((int32_t*)UnBox (L_4, Int32_t1_3_il2cpp_TypeInfo_var))));
		return;
	}
}
// System.Int32 System.Runtime.InteropServices.ErrorWrapper::get_ErrorCode()
extern "C" int32_t ErrorWrapper_get_ErrorCode_m1_7638 (ErrorWrapper_t1_788 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___errorCode_0);
		return L_0;
	}
}
// System.Void System.Runtime.InteropServices.ExtensibleClassFactory::.ctor()
extern "C" void ExtensibleClassFactory__ctor_m1_7639 (ExtensibleClassFactory_t1_790 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.ExtensibleClassFactory::.cctor()
extern TypeInfo* Hashtable_t1_100_il2cpp_TypeInfo_var;
extern TypeInfo* ExtensibleClassFactory_t1_790_il2cpp_TypeInfo_var;
extern "C" void ExtensibleClassFactory__cctor_m1_7640 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Hashtable_t1_100_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(119);
		ExtensibleClassFactory_t1_790_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(592);
		s_Il2CppMethodIntialized = true;
	}
	{
		Hashtable_t1_100 * L_0 = (Hashtable_t1_100 *)il2cpp_codegen_object_new (Hashtable_t1_100_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1_3279(L_0, /*hidden argument*/NULL);
		((ExtensibleClassFactory_t1_790_StaticFields*)ExtensibleClassFactory_t1_790_il2cpp_TypeInfo_var->static_fields)->___hashtable_0 = L_0;
		return;
	}
}
// System.Runtime.InteropServices.ObjectCreationDelegate System.Runtime.InteropServices.ExtensibleClassFactory::GetObjectCreationCallback(System.Type)
extern TypeInfo* ExtensibleClassFactory_t1_790_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectCreationDelegate_t1_1621_il2cpp_TypeInfo_var;
extern "C" ObjectCreationDelegate_t1_1621 * ExtensibleClassFactory_GetObjectCreationCallback_m1_7641 (Object_t * __this /* static, unused */, Type_t * ___t, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensibleClassFactory_t1_790_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(592);
		ObjectCreationDelegate_t1_1621_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(593);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ExtensibleClassFactory_t1_790_il2cpp_TypeInfo_var);
		Hashtable_t1_100 * L_0 = ((ExtensibleClassFactory_t1_790_StaticFields*)ExtensibleClassFactory_t1_790_il2cpp_TypeInfo_var->static_fields)->___hashtable_0;
		Type_t * L_1 = ___t;
		NullCheck(L_0);
		Object_t * L_2 = (Object_t *)VirtFuncInvoker1< Object_t *, Object_t * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_0, L_1);
		return ((ObjectCreationDelegate_t1_1621 *)IsInstSealed(L_2, ObjectCreationDelegate_t1_1621_il2cpp_TypeInfo_var));
	}
}
// System.Void System.Runtime.InteropServices.ExtensibleClassFactory::RegisterObjectCreationCallback(System.Runtime.InteropServices.ObjectCreationDelegate)
extern TypeInfo* StackTrace_t1_336_il2cpp_TypeInfo_var;
extern TypeInfo* ExtensibleClassFactory_t1_790_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidOperationException_t1_1559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2108;
extern "C" void ExtensibleClassFactory_RegisterObjectCreationCallback_m1_7642 (Object_t * __this /* static, unused */, ObjectCreationDelegate_t1_1621 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StackTrace_t1_336_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		ExtensibleClassFactory_t1_790_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(592);
		InvalidOperationException_t1_1559_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(78);
		_stringLiteral2108 = il2cpp_codegen_string_literal_from_index(2108);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	StackTrace_t1_336 * V_1 = {0};
	StackFrame_t1_334 * V_2 = {0};
	MethodBase_t1_335 * V_3 = {0};
	{
		V_0 = 1;
		StackTrace_t1_336 * L_0 = (StackTrace_t1_336 *)il2cpp_codegen_object_new (StackTrace_t1_336_il2cpp_TypeInfo_var);
		StackTrace__ctor_m1_3639(L_0, 0, /*hidden argument*/NULL);
		V_1 = L_0;
		goto IL_004a;
	}

IL_000e:
	{
		StackTrace_t1_336 * L_1 = V_1;
		int32_t L_2 = V_0;
		NullCheck(L_1);
		StackFrame_t1_334 * L_3 = (StackFrame_t1_334 *)VirtFuncInvoker1< StackFrame_t1_334 *, int32_t >::Invoke(5 /* System.Diagnostics.StackFrame System.Diagnostics.StackTrace::GetFrame(System.Int32) */, L_1, L_2);
		V_2 = L_3;
		StackFrame_t1_334 * L_4 = V_2;
		NullCheck(L_4);
		MethodBase_t1_335 * L_5 = (MethodBase_t1_335 *)VirtFuncInvoker0< MethodBase_t1_335 * >::Invoke(8 /* System.Reflection.MethodBase System.Diagnostics.StackFrame::GetMethod() */, L_4);
		V_3 = L_5;
		MethodBase_t1_335 * L_6 = V_3;
		NullCheck(L_6);
		int32_t L_7 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Reflection.MemberTypes System.Reflection.MemberInfo::get_MemberType() */, L_6);
		if ((!(((uint32_t)L_7) == ((uint32_t)1))))
		{
			goto IL_0046;
		}
	}
	{
		MethodBase_t1_335 * L_8 = V_3;
		NullCheck(L_8);
		bool L_9 = (bool)VirtFuncInvoker0< bool >::Invoke(61 /* System.Boolean System.Reflection.MethodBase::get_IsStatic() */, L_8);
		if (!L_9)
		{
			goto IL_0046;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ExtensibleClassFactory_t1_790_il2cpp_TypeInfo_var);
		Hashtable_t1_100 * L_10 = ((ExtensibleClassFactory_t1_790_StaticFields*)ExtensibleClassFactory_t1_790_il2cpp_TypeInfo_var->static_fields)->___hashtable_0;
		MethodBase_t1_335 * L_11 = V_3;
		NullCheck(L_11);
		Type_t * L_12 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(22 /* System.Type System.Reflection.MemberInfo::get_DeclaringType() */, L_11);
		ObjectCreationDelegate_t1_1621 * L_13 = ___callback;
		NullCheck(L_10);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_10, L_12, L_13);
		return;
	}

IL_0046:
	{
		int32_t L_14 = V_0;
		V_0 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_004a:
	{
		int32_t L_15 = V_0;
		StackTrace_t1_336 * L_16 = V_1;
		NullCheck(L_16);
		int32_t L_17 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 System.Diagnostics.StackTrace::get_FrameCount() */, L_16);
		if ((((int32_t)L_15) < ((int32_t)L_17)))
		{
			goto IL_000e;
		}
	}
	{
		InvalidOperationException_t1_1559 * L_18 = (InvalidOperationException_t1_1559 *)il2cpp_codegen_object_new (InvalidOperationException_t1_1559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1_14171(L_18, _stringLiteral2108, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_18);
	}
}
// System.Void System.Runtime.InteropServices.ExternalException::.ctor()
extern Il2CppCodeGenString* _stringLiteral2109;
extern "C" void ExternalException__ctor_m1_7643 (ExternalException_t1_761 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral2109 = il2cpp_codegen_string_literal_from_index(2109);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2109, /*hidden argument*/NULL);
		SystemException__ctor_m1_14605(__this, L_0, /*hidden argument*/NULL);
		Exception_set_HResult_m1_1245(__this, ((int32_t)-2147467259), /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.ExternalException::.ctor(System.String)
extern "C" void ExternalException__ctor_m1_7644 (ExternalException_t1_761 * __this, String_t* ___message, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message;
		SystemException__ctor_m1_14605(__this, L_0, /*hidden argument*/NULL);
		Exception_set_HResult_m1_1245(__this, ((int32_t)-2147467259), /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.ExternalException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void ExternalException__ctor_m1_7645 (ExternalException_t1_761 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method)
{
	{
		SerializationInfo_t1_293 * L_0 = ___info;
		StreamingContext_t1_1050  L_1 = ___context;
		SystemException__ctor_m1_14606(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.ExternalException::.ctor(System.String,System.Exception)
extern "C" void ExternalException__ctor_m1_7646 (ExternalException_t1_761 * __this, String_t* ___message, Exception_t1_33 * ___inner, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message;
		Exception_t1_33 * L_1 = ___inner;
		SystemException__ctor_m1_14607(__this, L_0, L_1, /*hidden argument*/NULL);
		Exception_set_HResult_m1_1245(__this, ((int32_t)-2147467259), /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.ExternalException::.ctor(System.String,System.Int32)
extern "C" void ExternalException__ctor_m1_7647 (ExternalException_t1_761 * __this, String_t* ___message, int32_t ___errorCode, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message;
		SystemException__ctor_m1_14605(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = ___errorCode;
		Exception_set_HResult_m1_1245(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 System.Runtime.InteropServices.ExternalException::get_ErrorCode()
extern "C" int32_t ExternalException_get_ErrorCode_m1_7648 (ExternalException_t1_761 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Exception_get_HResult_m1_1244(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
