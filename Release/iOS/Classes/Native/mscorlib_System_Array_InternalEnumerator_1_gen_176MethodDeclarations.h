﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_176.h"
#include "AssemblyU2DCSharp_ExifLibrary_MathEx_UFraction32.h"

// System.Void System.Array/InternalEnumerator`1<ExifLibrary.MathEx/UFraction32>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_26305_gshared (InternalEnumerator_1_t1_2659 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1_26305(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1_2659 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m1_26305_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<ExifLibrary.MathEx/UFraction32>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_26306_gshared (InternalEnumerator_1_t1_2659 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_26306(__this, method) (( void (*) (InternalEnumerator_1_t1_2659 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_26306_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<ExifLibrary.MathEx/UFraction32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_26307_gshared (InternalEnumerator_1_t1_2659 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_26307(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1_2659 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_26307_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<ExifLibrary.MathEx/UFraction32>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_26308_gshared (InternalEnumerator_1_t1_2659 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1_26308(__this, method) (( void (*) (InternalEnumerator_1_t1_2659 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1_26308_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<ExifLibrary.MathEx/UFraction32>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_26309_gshared (InternalEnumerator_1_t1_2659 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1_26309(__this, method) (( bool (*) (InternalEnumerator_1_t1_2659 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1_26309_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<ExifLibrary.MathEx/UFraction32>::get_Current()
extern "C" UFraction32_t8_121  InternalEnumerator_1_get_Current_m1_26310_gshared (InternalEnumerator_1_t1_2659 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1_26310(__this, method) (( UFraction32_t8_121  (*) (InternalEnumerator_1_t1_2659 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1_26310_gshared)(__this, method)
