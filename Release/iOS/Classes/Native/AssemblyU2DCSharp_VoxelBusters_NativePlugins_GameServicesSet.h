﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1_238;

#include "mscorlib_System_Object.h"

// VoxelBusters.NativePlugins.GameServicesSettings/AndroidSettings
struct  AndroidSettings_t8_236  : public Object_t
{
	// System.String VoxelBusters.NativePlugins.GameServicesSettings/AndroidSettings::m_playServicesApplicationID
	String_t* ___m_playServicesApplicationID_0;
	// System.String[] VoxelBusters.NativePlugins.GameServicesSettings/AndroidSettings::m_achievedDescriptionFormats
	StringU5BU5D_t1_238* ___m_achievedDescriptionFormats_1;
};
