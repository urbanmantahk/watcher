﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.IAsyncResult
struct IAsyncResult_t1_27;

#include "mscorlib_System_Object.h"
#include "mscorlib_System_IntPtr.h"

// System.Threading.Overlapped
struct  Overlapped_t1_1472  : public Object_t
{
	// System.IAsyncResult System.Threading.Overlapped::ares
	Object_t * ___ares_0;
	// System.Int32 System.Threading.Overlapped::offsetL
	int32_t ___offsetL_1;
	// System.Int32 System.Threading.Overlapped::offsetH
	int32_t ___offsetH_2;
	// System.Int32 System.Threading.Overlapped::evt
	int32_t ___evt_3;
	// System.IntPtr System.Threading.Overlapped::evt_ptr
	IntPtr_t ___evt_ptr_4;
};
