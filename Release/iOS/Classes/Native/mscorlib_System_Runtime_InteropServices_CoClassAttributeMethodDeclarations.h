﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.InteropServices.CoClassAttribute
struct CoClassAttribute_t1_52;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.InteropServices.CoClassAttribute::.ctor(System.Type)
extern "C" void CoClassAttribute__ctor_m1_1316 (CoClassAttribute_t1_52 * __this, Type_t * ___coClass, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Runtime.InteropServices.CoClassAttribute::get_CoClass()
extern "C" Type_t * CoClassAttribute_get_CoClass_m1_1317 (CoClassAttribute_t1_52 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
