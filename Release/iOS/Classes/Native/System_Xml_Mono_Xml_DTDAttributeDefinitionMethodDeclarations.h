﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Xml.DTDAttributeDefinition
struct DTDAttributeDefinition_t4_95;
// System.String
struct String_t;
// System.Xml.Schema.XmlSchemaDatatype
struct XmlSchemaDatatype_t4_4;

#include "codegen/il2cpp-codegen.h"

// System.String Mono.Xml.DTDAttributeDefinition::get_Name()
extern "C" String_t* DTDAttributeDefinition_get_Name_m4_203 (DTDAttributeDefinition_t4_95 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Schema.XmlSchemaDatatype Mono.Xml.DTDAttributeDefinition::get_Datatype()
extern "C" XmlSchemaDatatype_t4_4 * DTDAttributeDefinition_get_Datatype_m4_204 (DTDAttributeDefinition_t4_95 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Xml.DTDAttributeDefinition::get_DefaultValue()
extern "C" String_t* DTDAttributeDefinition_get_DefaultValue_m4_205 (DTDAttributeDefinition_t4_95 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Xml.DTDAttributeDefinition::get_UnresolvedDefaultValue()
extern "C" String_t* DTDAttributeDefinition_get_UnresolvedDefaultValue_m4_206 (DTDAttributeDefinition_t4_95 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Xml.DTDAttributeDefinition::ComputeDefaultValue()
extern "C" String_t* DTDAttributeDefinition_ComputeDefaultValue_m4_207 (DTDAttributeDefinition_t4_95 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
