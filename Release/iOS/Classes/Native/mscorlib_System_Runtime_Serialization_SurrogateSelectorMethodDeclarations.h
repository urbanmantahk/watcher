﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Serialization.SurrogateSelector
struct SurrogateSelector_t1_1099;
// System.Type
struct Type_t;
// System.Runtime.Serialization.ISerializationSurrogate
struct ISerializationSurrogate_t1_1085;
// System.Runtime.Serialization.ISurrogateSelector
struct ISurrogateSelector_t1_959;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.Runtime.Serialization.SurrogateSelector::.ctor()
extern "C" void SurrogateSelector__ctor_m1_9680 (SurrogateSelector_t1_1099 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.SurrogateSelector::AddSurrogate(System.Type,System.Runtime.Serialization.StreamingContext,System.Runtime.Serialization.ISerializationSurrogate)
extern "C" void SurrogateSelector_AddSurrogate_m1_9681 (SurrogateSelector_t1_1099 * __this, Type_t * ___type, StreamingContext_t1_1050  ___context, Object_t * ___surrogate, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.SurrogateSelector::ChainSelector(System.Runtime.Serialization.ISurrogateSelector)
extern "C" void SurrogateSelector_ChainSelector_m1_9682 (SurrogateSelector_t1_1099 * __this, Object_t * ___selector, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Serialization.ISurrogateSelector System.Runtime.Serialization.SurrogateSelector::GetNextSelector()
extern "C" Object_t * SurrogateSelector_GetNextSelector_m1_9683 (SurrogateSelector_t1_1099 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Serialization.ISerializationSurrogate System.Runtime.Serialization.SurrogateSelector::GetSurrogate(System.Type,System.Runtime.Serialization.StreamingContext,System.Runtime.Serialization.ISurrogateSelector&)
extern "C" Object_t * SurrogateSelector_GetSurrogate_m1_9684 (SurrogateSelector_t1_1099 * __this, Type_t * ___type, StreamingContext_t1_1050  ___context, Object_t ** ___selector, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.SurrogateSelector::RemoveSurrogate(System.Type,System.Runtime.Serialization.StreamingContext)
extern "C" void SurrogateSelector_RemoveSurrogate_m1_9685 (SurrogateSelector_t1_1099 * __this, Type_t * ___type, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
