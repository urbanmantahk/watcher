﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.Schema.XmlSchemaObject
struct XmlSchemaObject_t4_54;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Xml.Schema.XmlSchemaObject::.ctor()
extern "C" void XmlSchemaObject__ctor_m4_93 (XmlSchemaObject_t4_54 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
