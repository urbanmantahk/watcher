﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Text_Encoder.h"

// System.Text.UTF7Encoding/UTF7Encoder
struct  UTF7Encoder_t1_1448  : public Encoder_t1_1428
{
	// System.Boolean System.Text.UTF7Encoding/UTF7Encoder::allowOptionals
	bool ___allowOptionals_2;
	// System.Int32 System.Text.UTF7Encoding/UTF7Encoder::leftOver
	int32_t ___leftOver_3;
	// System.Boolean System.Text.UTF7Encoding/UTF7Encoder::isInShifted
	bool ___isInShifted_4;
};
