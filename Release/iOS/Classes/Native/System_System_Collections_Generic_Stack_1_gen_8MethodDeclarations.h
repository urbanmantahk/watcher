﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Stack_1_gen_1MethodDeclarations.h"

// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Color32>>::.ctor()
#define Stack_1__ctor_m3_2061(__this, method) (( void (*) (Stack_1_t3_274 *, const MethodInfo*))Stack_1__ctor_m3_1816_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Color32>>::System.Collections.ICollection.get_IsSynchronized()
#define Stack_1_System_Collections_ICollection_get_IsSynchronized_m3_2062(__this, method) (( bool (*) (Stack_1_t3_274 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_IsSynchronized_m3_1818_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Color32>>::System.Collections.ICollection.get_SyncRoot()
#define Stack_1_System_Collections_ICollection_get_SyncRoot_m3_2063(__this, method) (( Object_t * (*) (Stack_1_t3_274 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_SyncRoot_m3_1820_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Color32>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Stack_1_System_Collections_ICollection_CopyTo_m3_2064(__this, ___dest, ___idx, method) (( void (*) (Stack_1_t3_274 *, Array_t *, int32_t, const MethodInfo*))Stack_1_System_Collections_ICollection_CopyTo_m3_1822_gshared)(__this, ___dest, ___idx, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Color32>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3_2065(__this, method) (( Object_t* (*) (Stack_1_t3_274 *, const MethodInfo*))Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3_1824_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Color32>>::System.Collections.IEnumerable.GetEnumerator()
#define Stack_1_System_Collections_IEnumerable_GetEnumerator_m3_2066(__this, method) (( Object_t * (*) (Stack_1_t3_274 *, const MethodInfo*))Stack_1_System_Collections_IEnumerable_GetEnumerator_m3_1826_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Color32>>::Contains(T)
#define Stack_1_Contains_m3_2067(__this, ___t, method) (( bool (*) (Stack_1_t3_274 *, List_1_t1_1837 *, const MethodInfo*))Stack_1_Contains_m3_1827_gshared)(__this, ___t, method)
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Color32>>::Peek()
#define Stack_1_Peek_m3_2068(__this, method) (( List_1_t1_1837 * (*) (Stack_1_t3_274 *, const MethodInfo*))Stack_1_Peek_m3_1829_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Color32>>::Pop()
#define Stack_1_Pop_m3_2069(__this, method) (( List_1_t1_1837 * (*) (Stack_1_t3_274 *, const MethodInfo*))Stack_1_Pop_m3_1830_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Color32>>::Push(T)
#define Stack_1_Push_m3_2070(__this, ___t, method) (( void (*) (Stack_1_t3_274 *, List_1_t1_1837 *, const MethodInfo*))Stack_1_Push_m3_1831_gshared)(__this, ___t, method)
// System.Int32 System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Color32>>::get_Count()
#define Stack_1_get_Count_m3_2071(__this, method) (( int32_t (*) (Stack_1_t3_274 *, const MethodInfo*))Stack_1_get_Count_m3_1833_gshared)(__this, method)
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Color32>>::GetEnumerator()
#define Stack_1_GetEnumerator_m3_2072(__this, method) (( Enumerator_t3_294  (*) (Stack_1_t3_274 *, const MethodInfo*))Stack_1_GetEnumerator_m3_1835_gshared)(__this, method)
