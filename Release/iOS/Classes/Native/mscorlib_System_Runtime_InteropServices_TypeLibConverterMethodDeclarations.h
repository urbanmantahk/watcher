﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.InteropServices.TypeLibConverter
struct TypeLibConverter_t1_831;
// System.Object
struct Object_t;
// System.Reflection.Assembly
struct Assembly_t1_467;
// System.String
struct String_t;
// System.Runtime.InteropServices.ITypeLibExporterNotifySink
struct ITypeLibExporterNotifySink_t1_1700;
// System.Reflection.Emit.AssemblyBuilder
struct AssemblyBuilder_t1_466;
// System.Runtime.InteropServices.ITypeLibImporterNotifySink
struct ITypeLibImporterNotifySink_t1_1701;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Reflection.StrongNameKeyPair
struct StrongNameKeyPair_t1_577;
// System.Version
struct Version_t1_578;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_InteropServices_TypeLibExporterFlags.h"
#include "mscorlib_System_Runtime_InteropServices_TypeLibImporterFlags.h"
#include "mscorlib_System_Guid.h"

// System.Void System.Runtime.InteropServices.TypeLibConverter::.ctor()
extern "C" void TypeLibConverter__ctor_m1_7917 (TypeLibConverter_t1_831 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.InteropServices.TypeLibConverter::ConvertAssemblyToTypeLib(System.Reflection.Assembly,System.String,System.Runtime.InteropServices.TypeLibExporterFlags,System.Runtime.InteropServices.ITypeLibExporterNotifySink)
extern "C" Object_t * TypeLibConverter_ConvertAssemblyToTypeLib_m1_7918 (TypeLibConverter_t1_831 * __this, Assembly_t1_467 * ___assembly, String_t* ___strTypeLibName, int32_t ___flags, Object_t * ___notifySink, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.AssemblyBuilder System.Runtime.InteropServices.TypeLibConverter::ConvertTypeLibToAssembly(System.Object,System.String,System.Int32,System.Runtime.InteropServices.ITypeLibImporterNotifySink,System.Byte[],System.Reflection.StrongNameKeyPair,System.Boolean)
extern "C" AssemblyBuilder_t1_466 * TypeLibConverter_ConvertTypeLibToAssembly_m1_7919 (TypeLibConverter_t1_831 * __this, Object_t * ___typeLib, String_t* ___asmFileName, int32_t ___flags, Object_t * ___notifySink, ByteU5BU5D_t1_109* ___publicKey, StrongNameKeyPair_t1_577 * ___keyPair, bool ___unsafeInterfaces, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.AssemblyBuilder System.Runtime.InteropServices.TypeLibConverter::ConvertTypeLibToAssembly(System.Object,System.String,System.Runtime.InteropServices.TypeLibImporterFlags,System.Runtime.InteropServices.ITypeLibImporterNotifySink,System.Byte[],System.Reflection.StrongNameKeyPair,System.String,System.Version)
extern "C" AssemblyBuilder_t1_466 * TypeLibConverter_ConvertTypeLibToAssembly_m1_7920 (TypeLibConverter_t1_831 * __this, Object_t * ___typeLib, String_t* ___asmFileName, int32_t ___flags, Object_t * ___notifySink, ByteU5BU5D_t1_109* ___publicKey, StrongNameKeyPair_t1_577 * ___keyPair, String_t* ___asmNamespace, Version_t1_578 * ___asmVersion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.InteropServices.TypeLibConverter::GetPrimaryInteropAssembly(System.Guid,System.Int32,System.Int32,System.Int32,System.String&,System.String&)
extern "C" bool TypeLibConverter_GetPrimaryInteropAssembly_m1_7921 (TypeLibConverter_t1_831 * __this, Guid_t1_319  ___g, int32_t ___major, int32_t ___minor, int32_t ___lcid, String_t** ___asmName, String_t** ___asmCodeBase, const MethodInfo* method) IL2CPP_METHOD_ATTR;
