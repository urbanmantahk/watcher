﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.AccessControl.CryptoKeyAccessRule
struct CryptoKeyAccessRule_t1_1139;
// System.Security.Principal.IdentityReference
struct IdentityReference_t1_1120;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_AccessControl_CryptoKeyRights.h"
#include "mscorlib_System_Security_AccessControl_AccessControlType.h"

// System.Void System.Security.AccessControl.CryptoKeyAccessRule::.ctor(System.Security.Principal.IdentityReference,System.Security.AccessControl.CryptoKeyRights,System.Security.AccessControl.AccessControlType)
extern "C" void CryptoKeyAccessRule__ctor_m1_9784 (CryptoKeyAccessRule_t1_1139 * __this, IdentityReference_t1_1120 * ___identity, int32_t ___cryptoKeyRights, int32_t ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.CryptoKeyAccessRule::.ctor(System.String,System.Security.AccessControl.CryptoKeyRights,System.Security.AccessControl.AccessControlType)
extern "C" void CryptoKeyAccessRule__ctor_m1_9785 (CryptoKeyAccessRule_t1_1139 * __this, String_t* ___identity, int32_t ___cryptoKeyRights, int32_t ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.AccessControl.CryptoKeyRights System.Security.AccessControl.CryptoKeyAccessRule::get_CryptoKeyRights()
extern "C" int32_t CryptoKeyAccessRule_get_CryptoKeyRights_m1_9786 (CryptoKeyAccessRule_t1_1139 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
