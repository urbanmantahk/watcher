﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "mscorlib_System_Array.h"

#pragma once
// System.Object[]
// System.Object[]
struct ObjectU5BU5D_t1_272  : public Array_t { };
// System.Attribute[]
// System.Attribute[]
struct AttributeU5BU5D_t1_1662  : public Array_t { };
// System.Runtime.InteropServices._Attribute[]
// System.Runtime.InteropServices._Attribute[]
struct _AttributeU5BU5D_t1_2907  : public Array_t { };
// System.Char[]
// System.Char[]
struct CharU5BU5D_t1_16  : public Array_t { };
// System.IConvertible[]
// System.IConvertible[]
struct IConvertibleU5BU5D_t1_2908  : public Array_t { };
// System.IComparable[]
// System.IComparable[]
struct IComparableU5BU5D_t1_2909  : public Array_t { };
// System.IComparable`1<System.Char>[]
// System.IComparable`1<System.Char>[]
struct IComparable_1U5BU5D_t1_2910  : public Array_t { };
// System.IEquatable`1<System.Char>[]
// System.IEquatable`1<System.Char>[]
struct IEquatable_1U5BU5D_t1_2911  : public Array_t { };
// System.ValueType[]
// System.ValueType[]
struct ValueTypeU5BU5D_t1_2912  : public Array_t { };
// System.String[]
// System.String[]
struct StringU5BU5D_t1_238  : public Array_t { };
// System.Collections.IEnumerable[]
// System.Collections.IEnumerable[]
struct IEnumerableU5BU5D_t1_2913  : public Array_t { };
// System.ICloneable[]
// System.ICloneable[]
struct ICloneableU5BU5D_t1_2914  : public Array_t { };
// System.IComparable`1<System.String>[]
// System.IComparable`1<System.String>[]
struct IComparable_1U5BU5D_t1_2915  : public Array_t { };
// System.IEquatable`1<System.String>[]
// System.IEquatable`1<System.String>[]
struct IEquatable_1U5BU5D_t1_2916  : public Array_t { };
// System.Type[]
// System.Type[]
struct TypeU5BU5D_t1_31  : public Array_t { };
// System.Reflection.IReflect[]
// System.Reflection.IReflect[]
struct IReflectU5BU5D_t1_2917  : public Array_t { };
// System.Runtime.InteropServices._Type[]
// System.Runtime.InteropServices._Type[]
struct _TypeU5BU5D_t1_2918  : public Array_t { };
// System.Reflection.MemberInfo[]
// System.Reflection.MemberInfo[]
struct MemberInfoU5BU5D_t1_1054  : public Array_t { };
// System.Reflection.ICustomAttributeProvider[]
// System.Reflection.ICustomAttributeProvider[]
struct ICustomAttributeProviderU5BU5D_t1_2919  : public Array_t { };
// System.Runtime.InteropServices._MemberInfo[]
// System.Runtime.InteropServices._MemberInfo[]
struct _MemberInfoU5BU5D_t1_2920  : public Array_t { };
// System.Int32[]
// System.Int32[]
struct Int32U5BU5D_t1_275  : public Array_t { };
// System.IFormattable[]
// System.IFormattable[]
struct IFormattableU5BU5D_t1_2921  : public Array_t { };
// System.IComparable`1<System.Int32>[]
// System.IComparable`1<System.Int32>[]
struct IComparable_1U5BU5D_t1_2922  : public Array_t { };
// System.IEquatable`1<System.Int32>[]
// System.IEquatable`1<System.Int32>[]
struct IEquatable_1U5BU5D_t1_2923  : public Array_t { };
// System.Double[]
// System.Double[]
struct DoubleU5BU5D_t1_1666  : public Array_t { };
// System.IComparable`1<System.Double>[]
// System.IComparable`1<System.Double>[]
struct IComparable_1U5BU5D_t1_2924  : public Array_t { };
// System.IEquatable`1<System.Double>[]
// System.IEquatable`1<System.Double>[]
struct IEquatable_1U5BU5D_t1_2925  : public Array_t { };
// System.Byte[]
// System.Byte[]
struct ByteU5BU5D_t1_109  : public Array_t { };
// System.IComparable`1<System.Byte>[]
// System.IComparable`1<System.Byte>[]
struct IComparable_1U5BU5D_t1_2926  : public Array_t { };
// System.IEquatable`1<System.Byte>[]
// System.IEquatable`1<System.Byte>[]
struct IEquatable_1U5BU5D_t1_2927  : public Array_t { };
// System.Single[,]
// System.Single[,]
struct SingleU5BU2CU5D_t1_2928  : public Array_t { };
// System.Single[]
// System.Single[]
struct SingleU5BU5D_t1_1695  : public Array_t { };
// System.IComparable`1<System.Single>[]
// System.IComparable`1<System.Single>[]
struct IComparable_1U5BU5D_t1_2929  : public Array_t { };
// System.IEquatable`1<System.Single>[]
// System.IEquatable`1<System.Single>[]
struct IEquatable_1U5BU5D_t1_2930  : public Array_t { };
// System.Delegate[]
// System.Delegate[]
struct DelegateU5BU5D_t1_1664  : public Array_t { };
// System.Runtime.Serialization.ISerializable[]
// System.Runtime.Serialization.ISerializable[]
struct ISerializableU5BU5D_t1_2931  : public Array_t { };
// System.Reflection.ParameterInfo[]
// System.Reflection.ParameterInfo[]
struct ParameterInfoU5BU5D_t1_1685  : public Array_t { };
// System.Runtime.InteropServices._ParameterInfo[]
// System.Runtime.InteropServices._ParameterInfo[]
struct _ParameterInfoU5BU5D_t1_2932  : public Array_t { };
// System.Reflection.ParameterModifier[]
// System.Reflection.ParameterModifier[]
struct ParameterModifierU5BU5D_t1_1669  : public Array_t { };
// System.UInt16[]
// System.UInt16[]
struct UInt16U5BU5D_t1_1231  : public Array_t { };
// System.IComparable`1<System.UInt16>[]
// System.IComparable`1<System.UInt16>[]
struct IComparable_1U5BU5D_t1_2933  : public Array_t { };
// System.IEquatable`1<System.UInt16>[]
// System.IEquatable`1<System.UInt16>[]
struct IEquatable_1U5BU5D_t1_2934  : public Array_t { };
// System.UInt32[]
// System.UInt32[]
struct UInt32U5BU5D_t1_142  : public Array_t { };
// System.IComparable`1<System.UInt32>[]
// System.IComparable`1<System.UInt32>[]
struct IComparable_1U5BU5D_t1_2935  : public Array_t { };
// System.IEquatable`1<System.UInt32>[]
// System.IEquatable`1<System.UInt32>[]
struct IEquatable_1U5BU5D_t1_2936  : public Array_t { };
// System.UInt64[]
// System.UInt64[]
struct UInt64U5BU5D_t1_1256  : public Array_t { };
// System.IComparable`1<System.UInt64>[]
// System.IComparable`1<System.UInt64>[]
struct IComparable_1U5BU5D_t1_2937  : public Array_t { };
// System.IEquatable`1<System.UInt64>[]
// System.IEquatable`1<System.UInt64>[]
struct IEquatable_1U5BU5D_t1_2938  : public Array_t { };
// System.Int16[]
// System.Int16[]
struct Int16U5BU5D_t1_1694  : public Array_t { };
// System.IComparable`1<System.Int16>[]
// System.IComparable`1<System.Int16>[]
struct IComparable_1U5BU5D_t1_2939  : public Array_t { };
// System.IEquatable`1<System.Int16>[]
// System.IEquatable`1<System.Int16>[]
struct IEquatable_1U5BU5D_t1_2940  : public Array_t { };
// System.SByte[]
// System.SByte[]
struct SByteU5BU5D_t1_1450  : public Array_t { };
// System.IComparable`1<System.SByte>[]
// System.IComparable`1<System.SByte>[]
struct IComparable_1U5BU5D_t1_2941  : public Array_t { };
// System.IEquatable`1<System.SByte>[]
// System.IEquatable`1<System.SByte>[]
struct IEquatable_1U5BU5D_t1_2942  : public Array_t { };
// System.Int64[]
// System.Int64[]
struct Int64U5BU5D_t1_1665  : public Array_t { };
// System.IComparable`1<System.Int64>[]
// System.IComparable`1<System.Int64>[]
struct IComparable_1U5BU5D_t1_2943  : public Array_t { };
// System.IEquatable`1<System.Int64>[]
// System.IEquatable`1<System.Int64>[]
struct IEquatable_1U5BU5D_t1_2944  : public Array_t { };
// System.Reflection.MethodInfo[]
// System.Reflection.MethodInfo[]
struct MethodInfoU5BU5D_t1_603  : public Array_t { };
// System.Runtime.InteropServices._MethodInfo[]
// System.Runtime.InteropServices._MethodInfo[]
struct _MethodInfoU5BU5D_t1_2945  : public Array_t { };
// System.Reflection.MethodBase[]
// System.Reflection.MethodBase[]
struct MethodBaseU5BU5D_t1_1689  : public Array_t { };
// System.Runtime.InteropServices._MethodBase[]
// System.Runtime.InteropServices._MethodBase[]
struct _MethodBaseU5BU5D_t1_2946  : public Array_t { };
// System.Reflection.EventInfo[]
// System.Reflection.EventInfo[]
struct EventInfoU5BU5D_t1_1667  : public Array_t { };
// System.Runtime.InteropServices._EventInfo[]
// System.Runtime.InteropServices._EventInfo[]
struct _EventInfoU5BU5D_t1_2947  : public Array_t { };
// System.Reflection.FieldInfo[]
// System.Reflection.FieldInfo[]
struct FieldInfoU5BU5D_t1_1668  : public Array_t { };
// System.Runtime.InteropServices._FieldInfo[]
// System.Runtime.InteropServices._FieldInfo[]
struct _FieldInfoU5BU5D_t1_2948  : public Array_t { };
// System.Reflection.PropertyInfo[]
// System.Reflection.PropertyInfo[]
struct PropertyInfoU5BU5D_t1_1670  : public Array_t { };
// System.Runtime.InteropServices._PropertyInfo[]
// System.Runtime.InteropServices._PropertyInfo[]
struct _PropertyInfoU5BU5D_t1_2949  : public Array_t { };
// System.Reflection.ConstructorInfo[]
// System.Reflection.ConstructorInfo[]
struct ConstructorInfoU5BU5D_t1_1671  : public Array_t { };
// System.Runtime.InteropServices._ConstructorInfo[]
// System.Runtime.InteropServices._ConstructorInfo[]
struct _ConstructorInfoU5BU5D_t1_2950  : public Array_t { };
// System.IntPtr[]
// System.IntPtr[]
struct IntPtrU5BU5D_t1_34  : public Array_t { };
// System.RuntimeTypeHandle[]
// System.RuntimeTypeHandle[]
struct RuntimeTypeHandleU5BU5D_t1_1672  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>[]
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>[]
struct KeyValuePair_2U5BU5D_t1_2769  : public Array_t { };
// System.Collections.Generic.Link[]
// System.Collections.Generic.Link[]
struct LinkU5BU5D_t1_1975  : public Array_t { };
// System.Collections.DictionaryEntry[]
// System.Collections.DictionaryEntry[]
struct DictionaryEntryU5BU5D_t1_869  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>[]
// System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>[]
struct KeyValuePair_2U5BU5D_t1_2768  : public Array_t { };
// System.IO.DirectoryInfo[]
// System.IO.DirectoryInfo[]
struct DirectoryInfoU5BU5D_t1_1681  : public Array_t { };
// System.IO.FileSystemInfo[]
// System.IO.FileSystemInfo[]
struct FileSystemInfoU5BU5D_t1_1679  : public Array_t { };
// System.MarshalByRefObject[]
// System.MarshalByRefObject[]
struct MarshalByRefObjectU5BU5D_t1_2951  : public Array_t { };
// Mono.Globalization.Unicode.CodePointIndexer/TableRange[]
// Mono.Globalization.Unicode.CodePointIndexer/TableRange[]
struct TableRangeU5BU5D_t1_106  : public Array_t { };
// Mono.Globalization.Unicode.TailoringInfo[]
// Mono.Globalization.Unicode.TailoringInfo[]
struct TailoringInfoU5BU5D_t1_114  : public Array_t { };
// Mono.Globalization.Unicode.Contraction[]
// Mono.Globalization.Unicode.Contraction[]
struct ContractionU5BU5D_t1_125  : public Array_t { };
// Mono.Globalization.Unicode.Level2Map[]
// Mono.Globalization.Unicode.Level2Map[]
struct Level2MapU5BU5D_t1_126  : public Array_t { };
// Mono.Math.BigInteger[]
// Mono.Math.BigInteger[]
struct BigIntegerU5BU5D_t1_1673  : public Array_t { };
// System.Security.Cryptography.KeySizes[]
// System.Security.Cryptography.KeySizes[]
struct KeySizesU5BU5D_t1_1182  : public Array_t { };
// Mono.Security.X509.X509Certificate[]
// Mono.Security.X509.X509Certificate[]
struct X509CertificateU5BU5D_t1_1674  : public Array_t { };
// Mono.Security.X509.X509Extension[]
// Mono.Security.X509.X509Extension[]
struct X509ExtensionU5BU5D_t1_1675  : public Array_t { };
// Mono.Security.Uri/UriScheme[]
// Mono.Security.Uri/UriScheme[]
struct UriSchemeU5BU5D_t1_239  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t1_2186  : public Array_t { };
// System.Boolean[]
// System.Boolean[]
struct BooleanU5BU5D_t1_458  : public Array_t { };
// System.IComparable`1<System.Boolean>[]
// System.IComparable`1<System.Boolean>[]
struct IComparable_1U5BU5D_t1_2952  : public Array_t { };
// System.IEquatable`1<System.Boolean>[]
// System.IEquatable`1<System.Boolean>[]
struct IEquatable_1U5BU5D_t1_2953  : public Array_t { };
// System.Collections.Hashtable/Slot[]
// System.Collections.Hashtable/Slot[]
struct SlotU5BU5D_t1_292  : public Array_t { };
// System.Collections.SortedList/Slot[]
// System.Collections.SortedList/Slot[]
struct SlotU5BU5D_t1_308  : public Array_t { };
// System.Diagnostics.SymbolStore.ISymbolVariable[]
// System.Diagnostics.SymbolStore.ISymbolVariable[]
struct ISymbolVariableU5BU5D_t1_2954  : public Array_t { };
// System.Diagnostics.SymbolStore.ISymbolDocument[]
// System.Diagnostics.SymbolStore.ISymbolDocument[]
struct ISymbolDocumentU5BU5D_t1_2955  : public Array_t { };
// System.Diagnostics.SymbolStore.ISymbolNamespace[]
// System.Diagnostics.SymbolStore.ISymbolNamespace[]
struct ISymbolNamespaceU5BU5D_t1_2956  : public Array_t { };
// System.Diagnostics.SymbolStore.ISymbolScope[]
// System.Diagnostics.SymbolStore.ISymbolScope[]
struct ISymbolScopeU5BU5D_t1_2957  : public Array_t { };
// System.Diagnostics.StackFrame[]
// System.Diagnostics.StackFrame[]
struct StackFrameU5BU5D_t1_337  : public Array_t { };
// System.Globalization.Calendar[]
// System.Globalization.Calendar[]
struct CalendarU5BU5D_t1_363  : public Array_t { };
// System.Globalization.CultureInfo[]
// System.Globalization.CultureInfo[]
struct CultureInfoU5BU5D_t1_1678  : public Array_t { };
// System.IFormatProvider[]
// System.IFormatProvider[]
struct IFormatProviderU5BU5D_t1_2958  : public Array_t { };
// System.IO.FileInfo[]
// System.IO.FileInfo[]
struct FileInfoU5BU5D_t1_1680  : public Array_t { };
// System.IO.DriveInfo[]
// System.IO.DriveInfo[]
struct DriveInfoU5BU5D_t1_1682  : public Array_t { };
// System.Reflection.Emit.ModuleBuilder[]
// System.Reflection.Emit.ModuleBuilder[]
struct ModuleBuilderU5BU5D_t1_468  : public Array_t { };
// System.Runtime.InteropServices._ModuleBuilder[]
// System.Runtime.InteropServices._ModuleBuilder[]
struct _ModuleBuilderU5BU5D_t1_2959  : public Array_t { };
// System.Reflection.Module[]
// System.Reflection.Module[]
struct ModuleU5BU5D_t1_471  : public Array_t { };
// System.Runtime.InteropServices._Module[]
// System.Runtime.InteropServices._Module[]
struct _ModuleU5BU5D_t1_2960  : public Array_t { };
// System.Reflection.Emit.CustomAttributeBuilder[]
// System.Reflection.Emit.CustomAttributeBuilder[]
struct CustomAttributeBuilderU5BU5D_t1_469  : public Array_t { };
// System.Runtime.InteropServices._CustomAttributeBuilder[]
// System.Runtime.InteropServices._CustomAttributeBuilder[]
struct _CustomAttributeBuilderU5BU5D_t1_2961  : public Array_t { };
// System.Reflection.Emit.MonoResource[]
// System.Reflection.Emit.MonoResource[]
struct MonoResourceU5BU5D_t1_470  : public Array_t { };
// System.Reflection.Emit.MonoWin32Resource[]
// System.Reflection.Emit.MonoWin32Resource[]
struct MonoWin32ResourceU5BU5D_t1_472  : public Array_t { };
// System.Reflection.Emit.RefEmitPermissionSet[]
// System.Reflection.Emit.RefEmitPermissionSet[]
struct RefEmitPermissionSetU5BU5D_t1_473  : public Array_t { };
// System.Resources.ICONDIRENTRY[]
// System.Resources.ICONDIRENTRY[]
struct ICONDIRENTRYU5BU5D_t1_1693  : public Array_t { };
// System.Resources.Win32IconResource[]
// System.Resources.Win32IconResource[]
struct Win32IconResourceU5BU5D_t1_669  : public Array_t { };
// System.Resources.Win32Resource[]
// System.Resources.Win32Resource[]
struct Win32ResourceU5BU5D_t1_2962  : public Array_t { };
// System.IO.FileStream[]
// System.IO.FileStream[]
struct FileStreamU5BU5D_t1_1683  : public Array_t { };
// System.IO.Stream[]
// System.IO.Stream[]
struct StreamU5BU5D_t1_2963  : public Array_t { };
// System.IDisposable[]
// System.IDisposable[]
struct IDisposableU5BU5D_t1_2964  : public Array_t { };
// System.Reflection.Emit.ParameterBuilder[]
// System.Reflection.Emit.ParameterBuilder[]
struct ParameterBuilderU5BU5D_t1_482  : public Array_t { };
// System.Runtime.InteropServices._ParameterBuilder[]
// System.Runtime.InteropServices._ParameterBuilder[]
struct _ParameterBuilderU5BU5D_t1_2965  : public Array_t { };
// System.Type[][]
// System.Type[][]
struct TypeU5BU5DU5BU5D_t1_483  : public Array_t { };
// System.Array[]
// System.Array[]
struct ArrayU5BU5D_t1_2966  : public Array_t { };
// System.Collections.ICollection[]
// System.Collections.ICollection[]
struct ICollectionU5BU5D_t1_2967  : public Array_t { };
// System.Collections.IList[]
// System.Collections.IList[]
struct IListU5BU5D_t1_2968  : public Array_t { };
// System.Reflection.Emit.MethodBuilder[]
// System.Reflection.Emit.MethodBuilder[]
struct MethodBuilderU5BU5D_t1_502  : public Array_t { };
// System.Runtime.InteropServices._MethodBuilder[]
// System.Runtime.InteropServices._MethodBuilder[]
struct _MethodBuilderU5BU5D_t1_2969  : public Array_t { };
// System.Reflection.Emit.ILExceptionBlock[]
// System.Reflection.Emit.ILExceptionBlock[]
struct ILExceptionBlockU5BU5D_t1_512  : public Array_t { };
// System.Reflection.Emit.LocalBuilder[]
// System.Reflection.Emit.LocalBuilder[]
struct LocalBuilderU5BU5D_t1_517  : public Array_t { };
// System.Runtime.InteropServices._LocalBuilder[]
// System.Runtime.InteropServices._LocalBuilder[]
struct _LocalBuilderU5BU5D_t1_2970  : public Array_t { };
// System.Reflection.LocalVariableInfo[]
// System.Reflection.LocalVariableInfo[]
struct LocalVariableInfoU5BU5D_t1_612  : public Array_t { };
// System.Reflection.Emit.ILExceptionInfo[]
// System.Reflection.Emit.ILExceptionInfo[]
struct ILExceptionInfoU5BU5D_t1_518  : public Array_t { };
// System.Reflection.Emit.ILTokenInfo[]
// System.Reflection.Emit.ILTokenInfo[]
struct ILTokenInfoU5BU5D_t1_519  : public Array_t { };
// System.Reflection.Emit.ILGenerator/LabelData[]
// System.Reflection.Emit.ILGenerator/LabelData[]
struct LabelDataU5BU5D_t1_520  : public Array_t { };
// System.Reflection.Emit.ILGenerator/LabelFixup[]
// System.Reflection.Emit.ILGenerator/LabelFixup[]
struct LabelFixupU5BU5D_t1_521  : public Array_t { };
// System.Reflection.Emit.Label[]
// System.Reflection.Emit.Label[]
struct LabelU5BU5D_t1_1687  : public Array_t { };
// System.Reflection.Emit.SequencePoint[]
// System.Reflection.Emit.SequencePoint[]
struct SequencePointU5BU5D_t1_524  : public Array_t { };
// System.Reflection.Emit.GenericTypeParameterBuilder[]
// System.Reflection.Emit.GenericTypeParameterBuilder[]
struct GenericTypeParameterBuilderU5BU5D_t1_529  : public Array_t { };
// System.Reflection.Emit.TypeBuilder[]
// System.Reflection.Emit.TypeBuilder[]
struct TypeBuilderU5BU5D_t1_533  : public Array_t { };
// System.Runtime.InteropServices._TypeBuilder[]
// System.Runtime.InteropServices._TypeBuilder[]
struct _TypeBuilderU5BU5D_t1_2971  : public Array_t { };
// System.Reflection.Emit.FieldBuilder[]
// System.Reflection.Emit.FieldBuilder[]
struct FieldBuilderU5BU5D_t1_534  : public Array_t { };
// System.Runtime.InteropServices._FieldBuilder[]
// System.Runtime.InteropServices._FieldBuilder[]
struct _FieldBuilderU5BU5D_t1_2972  : public Array_t { };
// System.Reflection.Emit.ConstructorBuilder[]
// System.Reflection.Emit.ConstructorBuilder[]
struct ConstructorBuilderU5BU5D_t1_555  : public Array_t { };
// System.Runtime.InteropServices._ConstructorBuilder[]
// System.Runtime.InteropServices._ConstructorBuilder[]
struct _ConstructorBuilderU5BU5D_t1_2973  : public Array_t { };
// System.Reflection.Emit.PropertyBuilder[]
// System.Reflection.Emit.PropertyBuilder[]
struct PropertyBuilderU5BU5D_t1_556  : public Array_t { };
// System.Runtime.InteropServices._PropertyBuilder[]
// System.Runtime.InteropServices._PropertyBuilder[]
struct _PropertyBuilderU5BU5D_t1_2974  : public Array_t { };
// System.Reflection.Emit.EventBuilder[]
// System.Reflection.Emit.EventBuilder[]
struct EventBuilderU5BU5D_t1_557  : public Array_t { };
// System.Runtime.InteropServices._EventBuilder[]
// System.Runtime.InteropServices._EventBuilder[]
struct _EventBuilderU5BU5D_t1_2975  : public Array_t { };
// System.Reflection.AssemblyName[]
// System.Reflection.AssemblyName[]
struct AssemblyNameU5BU5D_t1_1688  : public Array_t { };
// System.Runtime.InteropServices._AssemblyName[]
// System.Runtime.InteropServices._AssemblyName[]
struct _AssemblyNameU5BU5D_t1_2976  : public Array_t { };
// System.Runtime.Serialization.IDeserializationCallback[]
// System.Runtime.Serialization.IDeserializationCallback[]
struct IDeserializationCallbackU5BU5D_t1_2977  : public Array_t { };
// System.Reflection.CustomAttributeTypedArgument[]
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t1_1748  : public Array_t { };
// System.Reflection.CustomAttributeNamedArgument[]
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t1_1749  : public Array_t { };
// System.Reflection.ExceptionHandlingClause[]
// System.Reflection.ExceptionHandlingClause[]
struct ExceptionHandlingClauseU5BU5D_t1_611  : public Array_t { };
// System.Runtime.CompilerServices.DecimalConstantAttribute[]
// System.Runtime.CompilerServices.DecimalConstantAttribute[]
struct DecimalConstantAttributeU5BU5D_t1_1760  : public Array_t { };
// System.Runtime.CompilerServices.DateTimeConstantAttribute[]
// System.Runtime.CompilerServices.DateTimeConstantAttribute[]
struct DateTimeConstantAttributeU5BU5D_t1_1761  : public Array_t { };
// System.Runtime.CompilerServices.CustomConstantAttribute[]
// System.Runtime.CompilerServices.CustomConstantAttribute[]
struct CustomConstantAttributeU5BU5D_t1_2978  : public Array_t { };
// System.Exception[]
// System.Exception[]
struct ExceptionU5BU5D_t1_634  : public Array_t { };
// System.Runtime.InteropServices._Exception[]
// System.Runtime.InteropServices._Exception[]
struct _ExceptionU5BU5D_t1_2979  : public Array_t { };
// System.Resources.ResourceReader/ResourceInfo[]
// System.Resources.ResourceReader/ResourceInfo[]
struct ResourceInfoU5BU5D_t1_652  : public Array_t { };
// System.Resources.ResourceReader/ResourceCacheItem[]
// System.Resources.ResourceReader/ResourceCacheItem[]
struct ResourceCacheItemU5BU5D_t1_653  : public Array_t { };
// System.Runtime.InteropServices.ComTypes.IConnectionPoint[]
// System.Runtime.InteropServices.ComTypes.IConnectionPoint[]
struct IConnectionPointU5BU5D_t1_2980  : public Array_t { };
// System.Runtime.InteropServices.ComTypes.CONNECTDATA[]
// System.Runtime.InteropServices.ComTypes.CONNECTDATA[]
struct CONNECTDATAU5BU5D_t1_2981  : public Array_t { };
// System.Runtime.InteropServices.ComTypes.IMoniker[]
// System.Runtime.InteropServices.ComTypes.IMoniker[]
struct IMonikerU5BU5D_t1_2982  : public Array_t { };
// System.Runtime.InteropServices.ComTypes.ITypeInfo[]
// System.Runtime.InteropServices.ComTypes.ITypeInfo[]
struct ITypeInfoU5BU5D_t1_2983  : public Array_t { };
// System.Runtime.InteropServices.UCOMIConnectionPoint[]
// System.Runtime.InteropServices.UCOMIConnectionPoint[]
struct UCOMIConnectionPointU5BU5D_t1_2984  : public Array_t { };
// System.Runtime.InteropServices.CONNECTDATA[]
// System.Runtime.InteropServices.CONNECTDATA[]
struct CONNECTDATAU5BU5D_t1_2985  : public Array_t { };
// System.Runtime.InteropServices.UCOMIMoniker[]
// System.Runtime.InteropServices.UCOMIMoniker[]
struct UCOMIMonikerU5BU5D_t1_2986  : public Array_t { };
// System.Runtime.InteropServices.UCOMITypeInfo[]
// System.Runtime.InteropServices.UCOMITypeInfo[]
struct UCOMITypeInfoU5BU5D_t1_2987  : public Array_t { };
// System.Collections.IDictionary[]
// System.Collections.IDictionary[]
struct IDictionaryU5BU5D_t1_861  : public Array_t { };
// System.Runtime.Remoting.Channels.IChannel[]
// System.Runtime.Remoting.Channels.IChannel[]
struct IChannelU5BU5D_t1_1704  : public Array_t { };
// System.Runtime.Remoting.Contexts.IContextProperty[]
// System.Runtime.Remoting.Contexts.IContextProperty[]
struct IContextPropertyU5BU5D_t1_1715  : public Array_t { };
// System.Decimal[]
// System.Decimal[]
struct DecimalU5BU5D_t1_1772  : public Array_t { };
// System.IComparable`1<System.Decimal>[]
// System.IComparable`1<System.Decimal>[]
struct IComparable_1U5BU5D_t1_2988  : public Array_t { };
// System.IEquatable`1<System.Decimal>[]
// System.IEquatable`1<System.Decimal>[]
struct IEquatable_1U5BU5D_t1_2989  : public Array_t { };
// System.Runtime.Remoting.Messaging.Header[]
// System.Runtime.Remoting.Messaging.Header[]
struct HeaderU5BU5D_t1_927  : public Array_t { };
// System.Runtime.Remoting.Services.ITrackingHandler[]
// System.Runtime.Remoting.Services.ITrackingHandler[]
struct ITrackingHandlerU5BU5D_t1_1720  : public Array_t { };
// System.Runtime.Remoting.Contexts.IContextAttribute[]
// System.Runtime.Remoting.Contexts.IContextAttribute[]
struct IContextAttributeU5BU5D_t1_1722  : public Array_t { };
// System.Runtime.Remoting.ActivatedClientTypeEntry[]
// System.Runtime.Remoting.ActivatedClientTypeEntry[]
struct ActivatedClientTypeEntryU5BU5D_t1_1723  : public Array_t { };
// System.Runtime.Remoting.TypeEntry[]
// System.Runtime.Remoting.TypeEntry[]
struct TypeEntryU5BU5D_t1_2990  : public Array_t { };
// System.Runtime.Remoting.ActivatedServiceTypeEntry[]
// System.Runtime.Remoting.ActivatedServiceTypeEntry[]
struct ActivatedServiceTypeEntryU5BU5D_t1_1724  : public Array_t { };
// System.Runtime.Remoting.WellKnownClientTypeEntry[]
// System.Runtime.Remoting.WellKnownClientTypeEntry[]
struct WellKnownClientTypeEntryU5BU5D_t1_1725  : public Array_t { };
// System.Runtime.Remoting.WellKnownServiceTypeEntry[]
// System.Runtime.Remoting.WellKnownServiceTypeEntry[]
struct WellKnownServiceTypeEntryU5BU5D_t1_1726  : public Array_t { };
// System.DateTime[]
// System.DateTime[]
struct DateTimeU5BU5D_t1_1775  : public Array_t { };
// System.IComparable`1<System.DateTime>[]
// System.IComparable`1<System.DateTime>[]
struct IComparable_1U5BU5D_t1_2991  : public Array_t { };
// System.IEquatable`1<System.DateTime>[]
// System.IEquatable`1<System.DateTime>[]
struct IEquatable_1U5BU5D_t1_2992  : public Array_t { };
// System.TimeSpan[]
// System.TimeSpan[]
struct TimeSpanU5BU5D_t1_1776  : public Array_t { };
// System.IComparable`1<System.TimeSpan>[]
// System.IComparable`1<System.TimeSpan>[]
struct IComparable_1U5BU5D_t1_2993  : public Array_t { };
// System.IEquatable`1<System.TimeSpan>[]
// System.IEquatable`1<System.TimeSpan>[]
struct IEquatable_1U5BU5D_t1_2994  : public Array_t { };
// System.Runtime.Serialization.Formatters.Binary.TypeTag[]
// System.Runtime.Serialization.Formatters.Binary.TypeTag[]
struct TypeTagU5BU5D_t1_1777  : public Array_t { };
// System.Enum[]
// System.Enum[]
struct EnumU5BU5D_t1_2995  : public Array_t { };
// System.MonoType[]
// System.MonoType[]
struct MonoTypeU5BU5D_t1_1780  : public Array_t { };
// System.Runtime.Serialization.SerializationEntry[]
// System.Runtime.Serialization.SerializationEntry[]
struct SerializationEntryU5BU5D_t1_1727  : public Array_t { };
// System.Security.AccessControl.AuthorizationRule[]
// System.Security.AccessControl.AuthorizationRule[]
struct AuthorizationRuleU5BU5D_t1_1728  : public Array_t { };
// System.Security.AccessControl.GenericAce[]
// System.Security.AccessControl.GenericAce[]
struct GenericAceU5BU5D_t1_1729  : public Array_t { };
// System.Security.AccessControl.AccessRule[]
// System.Security.AccessControl.AccessRule[]
struct AccessRuleU5BU5D_t1_2135  : public Array_t { };
// System.Security.AccessControl.AuditRule[]
// System.Security.AccessControl.AuditRule[]
struct AuditRuleU5BU5D_t1_2141  : public Array_t { };
// System.Byte[,]
// System.Byte[,]
struct ByteU5BU2CU5D_t1_1200  : public Array_t { };
// System.Security.Permissions.KeyContainerPermissionAccessEntry[]
// System.Security.Permissions.KeyContainerPermissionAccessEntry[]
struct KeyContainerPermissionAccessEntryU5BU5D_t1_1730  : public Array_t { };
// System.Security.Policy.StrongName[]
// System.Security.Policy.StrongName[]
struct StrongNameU5BU5D_t1_2148  : public Array_t { };
// System.Security.Policy.ApplicationTrust[]
// System.Security.Policy.ApplicationTrust[]
struct ApplicationTrustU5BU5D_t1_1732  : public Array_t { };
// System.Security.ISecurityEncodable[]
// System.Security.ISecurityEncodable[]
struct ISecurityEncodableU5BU5D_t1_2996  : public Array_t { };
// System.Security.Policy.CodeConnectAccess[]
// System.Security.Policy.CodeConnectAccess[]
struct CodeConnectAccessU5BU5D_t1_1733  : public Array_t { };
// System.Security.Principal.IdentityReference[]
// System.Security.Principal.IdentityReference[]
struct IdentityReferenceU5BU5D_t1_1734  : public Array_t { };
// System.Security.Policy.Evidence[]
// System.Security.Policy.Evidence[]
struct EvidenceU5BU5D_t1_1736  : public Array_t { };
// System.Text.EncodingInfo[]
// System.Text.EncodingInfo[]
struct EncodingInfoU5BU5D_t1_1437  : public Array_t { };
// System.Threading.WaitHandle[]
// System.Threading.WaitHandle[]
struct WaitHandleU5BU5D_t1_1737  : public Array_t { };
// System.Reflection.Assembly[]
// System.Reflection.Assembly[]
struct AssemblyU5BU5D_t1_1738  : public Array_t { };
// System.Runtime.InteropServices._Assembly[]
// System.Runtime.InteropServices._Assembly[]
struct _AssemblyU5BU5D_t1_2997  : public Array_t { };
// System.Reflection.CustomAttributeData[]
// System.Reflection.CustomAttributeData[]
struct CustomAttributeDataU5BU5D_t1_1741  : public Array_t { };
// System.Byte[][]
// System.Byte[][]
struct ByteU5BU5DU5BU5D_t1_1802  : public Array_t { };
// System.Security.Cryptography.X509Certificates.X509Certificate[]
// System.Security.Cryptography.X509Certificates.X509Certificate[]
struct X509CertificateU5BU5D_t1_1803  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode>[]
// System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode>[]
struct KeyValuePair_2U5BU5D_t1_2184  : public Array_t { };
// System.ArraySegment`1<System.Byte>[]
// System.ArraySegment`1<System.Byte>[]
struct ArraySegment_1U5BU5D_t1_2998  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>[]
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>[]
struct KeyValuePair_2U5BU5D_t1_2787  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>[]
// System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>[]
struct KeyValuePair_2U5BU5D_t1_2786  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.String,System.String>[]
// System.Collections.Generic.KeyValuePair`2<System.String,System.String>[]
struct KeyValuePair_2U5BU5D_t1_2811  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>[]
struct KeyValuePair_2U5BU5D_t1_2825  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>[]
struct KeyValuePair_2U5BU5D_t1_2824  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>[]
// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>[]
struct KeyValuePair_2U5BU5D_t1_2830  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>[]
// System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>[]
struct KeyValuePair_2U5BU5D_t1_2833  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>[]
struct KeyValuePair_2U5BU5D_t1_2832  : public Array_t { };
// System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>[]
// System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>[]
struct List_1U5BU5D_t1_2464  : public Array_t { };
// System.Collections.Generic.List`1<System.Object>[]
// System.Collections.Generic.List`1<System.Object>[]
struct List_1U5BU5D_t1_2465  : public Array_t { };
// System.Collections.Generic.List`1<UnityEngine.Component>[]
// System.Collections.Generic.List`1<UnityEngine.Component>[]
struct List_1U5BU5D_t1_2466  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>[]
struct KeyValuePair_2U5BU5D_t1_2844  : public Array_t { };
// System.Collections.Generic.List`1<UnityEngine.Canvas>[]
// System.Collections.Generic.List`1<UnityEngine.Canvas>[]
struct List_1U5BU5D_t1_2531  : public Array_t { };
// System.Collections.Generic.List`1<UnityEngine.UI.Text>[]
// System.Collections.Generic.List`1<UnityEngine.UI.Text>[]
struct List_1U5BU5D_t1_2532  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>[]
struct KeyValuePair_2U5BU5D_t1_2850  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>[]
struct KeyValuePair_2U5BU5D_t1_2853  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>[]
struct KeyValuePair_2U5BU5D_t1_2854  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.ICanvasElement,System.Int32>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.ICanvasElement,System.Int32>[]
struct KeyValuePair_2U5BU5D_t1_2849  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.IClipper,System.Int32>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.IClipper,System.Int32>[]
struct KeyValuePair_2U5BU5D_t1_2864  : public Array_t { };
// System.Collections.Generic.List`1<UnityEngine.Vector3>[]
// System.Collections.Generic.List`1<UnityEngine.Vector3>[]
struct List_1U5BU5D_t1_2616  : public Array_t { };
// System.Collections.Generic.List`1<UnityEngine.Color32>[]
// System.Collections.Generic.List`1<UnityEngine.Color32>[]
struct List_1U5BU5D_t1_2617  : public Array_t { };
// System.Collections.Generic.List`1<UnityEngine.Vector2>[]
// System.Collections.Generic.List`1<UnityEngine.Vector2>[]
struct List_1U5BU5D_t1_2618  : public Array_t { };
// System.Collections.Generic.List`1<UnityEngine.Vector4>[]
// System.Collections.Generic.List`1<UnityEngine.Vector4>[]
struct List_1U5BU5D_t1_2619  : public Array_t { };
// System.Collections.Generic.List`1<System.Int32>[]
// System.Collections.Generic.List`1<System.Int32>[]
struct List_1U5BU5D_t1_2620  : public Array_t { };
// System.Collections.Generic.List`1<UnityEngine.UIVertex>[]
// System.Collections.Generic.List`1<UnityEngine.UIVertex>[]
struct List_1U5BU5D_t1_2621  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.String,Boomlagoon.JSON.JSONValue>[]
// System.Collections.Generic.KeyValuePair`2<System.String,Boomlagoon.JSON.JSONValue>[]
struct KeyValuePair_2U5BU5D_t1_2867  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.String,System.Object>[]
// System.Collections.Generic.KeyValuePair`2<System.String,System.Object>[]
struct KeyValuePair_2U5BU5D_t1_2870  : public Array_t { };
// System.IComparable`1<ExifLibrary.MathEx/UFraction32>[]
// System.IComparable`1<ExifLibrary.MathEx/UFraction32>[]
struct IComparable_1U5BU5D_t1_2999  : public Array_t { };
// System.IEquatable`1<ExifLibrary.MathEx/UFraction32>[]
// System.IEquatable`1<ExifLibrary.MathEx/UFraction32>[]
struct IEquatable_1U5BU5D_t1_3000  : public Array_t { };
// System.IComparable`1<ExifLibrary.MathEx/Fraction32>[]
// System.IComparable`1<ExifLibrary.MathEx/Fraction32>[]
struct IComparable_1U5BU5D_t1_3001  : public Array_t { };
// System.IEquatable`1<ExifLibrary.MathEx/Fraction32>[]
// System.IEquatable`1<ExifLibrary.MathEx/Fraction32>[]
struct IEquatable_1U5BU5D_t1_3002  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<ExifLibrary.ExifTag,System.Object>[]
// System.Collections.Generic.KeyValuePair`2<ExifLibrary.ExifTag,System.Object>[]
struct KeyValuePair_2U5BU5D_t1_2874  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>[]
// System.Collections.Generic.KeyValuePair`2<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>[]
struct KeyValuePair_2U5BU5D_t1_2873  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Int32,ExifLibrary.IFD>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,ExifLibrary.IFD>[]
struct KeyValuePair_2U5BU5D_t1_2680  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.String,System.Single>[]
// System.Collections.Generic.KeyValuePair`2<System.String,System.Single>[]
struct KeyValuePair_2U5BU5D_t1_2886  : public Array_t { };
// System.Collections.Generic.Dictionary`2<System.String,System.Single>[]
// System.Collections.Generic.Dictionary`2<System.String,System.Single>[]
struct Dictionary_2U5BU5D_t1_2692  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Action,System.Collections.Generic.Dictionary`2<System.String,System.Single>>[]
// System.Collections.Generic.KeyValuePair`2<System.Action,System.Collections.Generic.Dictionary`2<System.String,System.Single>>[]
struct KeyValuePair_2U5BU5D_t1_2883  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>[]
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>[]
struct KeyValuePair_2U5BU5D_t1_2889  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.String,VoxelBusters.NativePlugins.UI/AlertDialogCompletion>[]
// System.Collections.Generic.KeyValuePair`2<System.String,VoxelBusters.NativePlugins.UI/AlertDialogCompletion>[]
struct KeyValuePair_2U5BU5D_t1_2898  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.String,VoxelBusters.NativePlugins.WebView>[]
// System.Collections.Generic.KeyValuePair`2<System.String,VoxelBusters.NativePlugins.WebView>[]
struct KeyValuePair_2U5BU5D_t1_2901  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.String,VoxelBusters.NativePlugins.Internal.NPObject>[]
// System.Collections.Generic.KeyValuePair`2<System.String,VoxelBusters.NativePlugins.Internal.NPObject>[]
struct KeyValuePair_2U5BU5D_t1_2904  : public Array_t { };
