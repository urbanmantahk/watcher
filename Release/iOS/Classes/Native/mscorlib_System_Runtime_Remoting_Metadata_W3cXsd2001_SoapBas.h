﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "mscorlib_System_Object.h"

// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapBase64Binary
struct  SoapBase64Binary_t1_966  : public Object_t
{
	// System.Byte[] System.Runtime.Remoting.Metadata.W3cXsd2001.SoapBase64Binary::_value
	ByteU5BU5D_t1_109* ____value_0;
};
