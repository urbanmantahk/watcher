﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

extern "C" void PARAMDESC_t1_784_marshal(const PARAMDESC_t1_784& unmarshaled, PARAMDESC_t1_784_marshaled& marshaled);
extern "C" void PARAMDESC_t1_784_marshal_back(const PARAMDESC_t1_784_marshaled& marshaled, PARAMDESC_t1_784& unmarshaled);
extern "C" void PARAMDESC_t1_784_marshal_cleanup(PARAMDESC_t1_784_marshaled& marshaled);
