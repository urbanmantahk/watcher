﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Reflection_Emit_NativeResourceType.h"

// System.Reflection.Emit.NativeResourceType
struct  NativeResourceType_t1_462 
{
	// System.Int32 System.Reflection.Emit.NativeResourceType::value__
	int32_t ___value___1;
};
