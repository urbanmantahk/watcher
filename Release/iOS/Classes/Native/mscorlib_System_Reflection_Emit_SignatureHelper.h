﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Reflection.Emit.ModuleBuilder
struct ModuleBuilder_t1_475;
// System.Type[]
struct TypeU5BU5D_t1_31;
// System.Type
struct Type_t;
// System.Type[][]
struct TypeU5BU5DU5BU5D_t1_483;

#include "mscorlib_System_Object.h"
#include "mscorlib_System_Reflection_Emit_SignatureHelper_SignatureHel.h"
#include "mscorlib_System_Reflection_CallingConventions.h"
#include "mscorlib_System_Runtime_InteropServices_CallingConvention.h"

// System.Reflection.Emit.SignatureHelper
struct  SignatureHelper_t1_551  : public Object_t
{
	// System.Reflection.Emit.ModuleBuilder System.Reflection.Emit.SignatureHelper::module
	ModuleBuilder_t1_475 * ___module_0;
	// System.Type[] System.Reflection.Emit.SignatureHelper::arguments
	TypeU5BU5D_t1_31* ___arguments_1;
	// System.Reflection.Emit.SignatureHelper/SignatureHelperType System.Reflection.Emit.SignatureHelper::type
	int32_t ___type_2;
	// System.Type System.Reflection.Emit.SignatureHelper::returnType
	Type_t * ___returnType_3;
	// System.Reflection.CallingConventions System.Reflection.Emit.SignatureHelper::callConv
	int32_t ___callConv_4;
	// System.Runtime.InteropServices.CallingConvention System.Reflection.Emit.SignatureHelper::unmanagedCallConv
	int32_t ___unmanagedCallConv_5;
	// System.Type[][] System.Reflection.Emit.SignatureHelper::modreqs
	TypeU5BU5DU5BU5D_t1_483* ___modreqs_6;
	// System.Type[][] System.Reflection.Emit.SignatureHelper::modopts
	TypeU5BU5DU5BU5D_t1_483* ___modopts_7;
};
