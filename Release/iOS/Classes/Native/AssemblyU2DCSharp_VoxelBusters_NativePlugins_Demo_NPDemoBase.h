﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1_238;

#include "AssemblyU2DCSharp_VoxelBusters_AssetStoreProductUtility_Demo_1.h"

// VoxelBusters.NativePlugins.Demo.NPDemoBase
struct  NPDemoBase_t8_174  : public DemoSubMenu_t8_18
{
	// System.Boolean VoxelBusters.NativePlugins.Demo.NPDemoBase::m_showThingsToKnow
	bool ___m_showThingsToKnow_10;
	// System.String VoxelBusters.NativePlugins.Demo.NPDemoBase::m_featureInterfaceInfoText
	String_t* ___m_featureInterfaceInfoText_11;
	// System.String[] VoxelBusters.NativePlugins.Demo.NPDemoBase::m_additionalInfoTexts
	StringU5BU5D_t1_238* ___m_additionalInfoTexts_12;
};
