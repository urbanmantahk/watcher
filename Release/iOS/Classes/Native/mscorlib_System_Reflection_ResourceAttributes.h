﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Reflection_ResourceAttributes.h"

// System.Reflection.ResourceAttributes
struct  ResourceAttributes_t1_635 
{
	// System.Int32 System.Reflection.ResourceAttributes::value__
	int32_t ___value___1;
};
