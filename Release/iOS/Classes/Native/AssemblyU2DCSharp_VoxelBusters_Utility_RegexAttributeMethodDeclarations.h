﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.Utility.RegexAttribute
struct RegexAttribute_t8_149;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.Utility.RegexAttribute::.ctor(System.String,System.String)
extern "C" void RegexAttribute__ctor_m8_869 (RegexAttribute_t8_149 * __this, String_t* ___pattern, String_t* ___helpMessage, const MethodInfo* method) IL2CPP_METHOD_ATTR;
